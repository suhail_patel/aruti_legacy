﻿Option Strict On

#Region "Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCaptionViewSetting

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCaptionViewSetting"
    Private objFieldMaster As clsAssess_Field_Master
    Private mdtData As DataTable = Nothing

#End Region

#Region " Private Methods "

    Private Sub Set_Grid_Data()
        Try
            mdtData = objFieldMaster.GetFieldsForViewSetting()

            'S.SANDEEP |20-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#0004155}
            'If ConfigParameter._Object._MakeEmpAssessCommentsMandatory = False Then
            '    mdtData.Select("Id = " & clsAssess_Field_Master.enOtherInfoField.EMP_REMARK).ToList().ForEach(Function(x) UpdateValue(x))
            'End If
            'If ConfigParameter._Object._MakeAsrAssessCommentsMandatory = False Then
            '    mdtData.Select("Id = " & clsAssess_Field_Master.enOtherInfoField.ASR_REMARK).ToList().ForEach(Function(x) UpdateValue(x))
            'End If
            'If ConfigParameter._Object._MakeRevAssessCommentsMandatory = False Then
            '    mdtData.Select("Id = " & clsAssess_Field_Master.enOtherInfoField.REV_REMARK).ToList().ForEach(Function(x) UpdateValue(x))
            'End If
            'mdtData.AcceptChanges()
            'S.SANDEEP |20-SEP-2019| -- END

            dgvData.AutoGenerateColumns = False
            dgcolhCaption.DataPropertyName = "Name"
            dgcolhInEvaluation.DataPropertyName = "SE"
            dgcolhInPlanning.DataPropertyName = "SP"
            objdgcolhFieldId.DataPropertyName = "Id"
            dgcolhPWidth.DataPropertyName = "PW"
            dgcolhEWidth.DataPropertyName = "EW"
            dgvData.DataSource = mdtData
            Call Grid_Controls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Grid_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |20-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#0004155}
    'Private Function UpdateValue(ByVal dr As DataRow) As Boolean
    '    Try
    '        dr("SE") = False : dr("RE") = False
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "UpdateValue", mstrModuleName)
    '    Finally
    '    End Try
    '    Return True
    'End Function
    'S.SANDEEP |20-SEP-2019| -- END

    Private Sub Grid_Controls()
        Try
            Dim iPlan(), iEval() As String : iPlan = Nothing : iEval = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split(CChar("|"))
            End If

            If ConfigParameter._Object._ViewTitles_InEvaluation.Trim.Length > 0 Then
                iEval = ConfigParameter._Object._ViewTitles_InEvaluation.Split(CChar("|"))
            End If
            If iPlan IsNot Nothing Or iEval IsNot Nothing Then
                For Each xRow As DataGridViewRow In dgvData.Rows
                    RemoveHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged
                    xRow.Cells(dgcolhPWidth.Index).Value = objFieldMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, CInt(xRow.Cells(objdgcolhFieldId.Index).Value))
                    xRow.Cells(dgcolhEWidth.Index).Value = objFieldMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, CInt(xRow.Cells(objdgcolhFieldId.Index).Value))
                    AddHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged

                    If iPlan IsNot Nothing Then
                        If Array.IndexOf(iPlan, xRow.Cells(objdgcolhFieldId.Index).Value.ToString) >= 0 Then
                            xRow.Cells(dgcolhInPlanning.Index).Value = True
                        End If
                    End If
                    If iEval IsNot Nothing Then
                        If Array.IndexOf(iEval, xRow.Cells(objdgcolhFieldId.Index).Value.ToString) >= 0 Then
                            xRow.Cells(dgcolhInEvaluation.Index).Value = True
                        End If
                    End If
                    Call SetCellReadOnly(xRow)
                Next
            Else
                For Each xRow As DataGridViewRow In dgvData.Rows
                    'S.SANDEEP [07 FEB 2015] -- START
                    'If CInt(mdtData.Rows(xRow.Index).Item("Id")) = clsAssess_Field_Master.enOtherInfoField.SCORE Then
                    '    xRow.Cells(dgcolhInPlanning.Index).Value = False
                    'Else
                    '    xRow.Cells(dgcolhInPlanning.Index).Value = True
                    'End If
                    Select Case CInt(mdtData.Rows(xRow.Index).Item("Id"))
                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
                            xRow.Cells(dgcolhInPlanning.Index).Value = False
                        Case clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, clsAssess_Field_Master.enOtherInfoField.REV_REMARK
                        xRow.Cells(dgcolhInPlanning.Index).Value = False
                            xRow.Cells(dgcolhPWidth.Index).ReadOnly = True
                        Case Else
                        xRow.Cells(dgcolhInPlanning.Index).Value = True
                    End Select
                    'S.SANDEEP [07 FEB 2015] -- END
                    xRow.Cells(dgcolhInEvaluation.Index).Value = True
                    Call SetCellReadOnly(xRow)
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Grid_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCellReadOnly(ByVal gRow As DataGridViewRow)
        Try
            If mdtData IsNot Nothing Then
                If CBool(mdtData.Rows(gRow.Index).Item("RP")) = True Then
                    gRow.Cells(dgcolhInPlanning.Index).ReadOnly = True
                    'S.SANDEEP [07 FEB 2015] -- START
                    Select Case CInt(mdtData.Rows(gRow.Index).Item("Id"))
                        Case clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, clsAssess_Field_Master.enOtherInfoField.REV_REMARK
                            gRow.Cells(dgcolhInPlanning.Index).Value = False
                            gRow.Cells(dgcolhPWidth.Index).ReadOnly = True
                    End Select
                    'S.SANDEEP [07 FEB 2015] -- END
                End If

                If CBool(mdtData.Rows(gRow.Index).Item("RE")) = True Then
                    gRow.Cells(dgcolhInEvaluation.Index).ReadOnly = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCaptionViewSetting_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objFieldMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCaptionViewSetting_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCaptionViewSetting_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFieldMaster = New clsAssess_Field_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Grid_Data()
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnSave.Enabled = User._Object.Privilege._AllowtoSaveBSCTitlesViewSetting
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCaptionViewSetting_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssess_Field_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsAssess_Field_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid

            Dim iChkItemIds As String = String.Empty
            Dim selectedTags As List(Of String) = (From p In mdtData.AsEnumerable() Where p.Field(Of Boolean)("SP") = True Select (p.Item("Id").ToString)).ToList
            iChkItemIds = String.Join("|", selectedTags.ToArray())

            objConfig._ViewTitles_InPlanning = iChkItemIds
            iChkItemIds = ""

            selectedTags = (From p In mdtData.AsEnumerable() Where p.Field(Of Boolean)("SE") = True Select (p.Item("Id").ToString)).ToList
            iChkItemIds = String.Join("|", selectedTags.ToArray())
            objConfig._ViewTitles_InEvaluation = iChkItemIds

            selectedTags = (From p In mdtData.AsEnumerable() Select (p.Item("PW").ToString)).ToList
            iChkItemIds = String.Join("|", selectedTags.ToArray())
            objConfig._ColWidth_InPlanning = iChkItemIds


            selectedTags = (From p In mdtData.AsEnumerable() Select (p.Item("EW").ToString)).ToList
            iChkItemIds = String.Join("|", selectedTags.ToArray())
            objConfig._ColWidth_InEvaluation = iChkItemIds

            objConfig.updateParam()
            ConfigParameter._Object.Refresh()
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case dgcolhPWidth.Index, dgcolhEWidth.Index
                    SendKeys.Send("{F2}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case dgcolhPWidth.Index, dgcolhEWidth.Index
                    If CInt(dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) < 5 Then
                        dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 5
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhCaption.HeaderText = Language._Object.getCaption(Me.dgcolhCaption.Name, Me.dgcolhCaption.HeaderText)
			Me.dgcolhInPlanning.HeaderText = Language._Object.getCaption(Me.dgcolhInPlanning.Name, Me.dgcolhInPlanning.HeaderText)
			Me.dgcolhInEvaluation.HeaderText = Language._Object.getCaption(Me.dgcolhInEvaluation.Name, Me.dgcolhInEvaluation.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'If lvBSCTitles.CheckedItems.Count > 0 Then
'    Dim iChkItemIds As String = String.Empty
'    iChkItemIds = String.Join("|", lvBSCTitles.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
'    objConfig._ViewBSC_Titles_PerfEvaluation = iChkItemIds
'End If