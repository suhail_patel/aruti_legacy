﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_ObjectiveList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_ObjectiveList))
        Me.pnlParameterList = New System.Windows.Forms.Panel
        Me.lvObjective = New eZee.Common.eZeeListView(Me.components)
        Me.colhPerspective = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhWeight = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmployee = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriodId = New System.Windows.Forms.ColumnHeader
        Me.objcolhYearId = New System.Windows.Forms.ColumnHeader
        Me.gbUnlockRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objpnlRemark = New System.Windows.Forms.Panel
        Me.txtUnlockRemark = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.fpnlObjectives = New System.Windows.Forms.FlowLayoutPanel
        Me.pnlEmployee = New System.Windows.Forms.Panel
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.pnlPerspective = New System.Windows.Forms.Panel
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.cboPerspective = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.lblCaption2 = New System.Windows.Forms.Label
        Me.objpnl2 = New System.Windows.Forms.Panel
        Me.objpnl1 = New System.Windows.Forms.Panel
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSubmitApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApproveSubmitted = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUnlockFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportBSC_Planning = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPrintBSCForm = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlParameterList.SuspendLayout()
        Me.gbUnlockRemark.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.objpnlRemark.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.fpnlObjectives.SuspendLayout()
        Me.pnlEmployee.SuspendLayout()
        Me.pnlPerspective.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlParameterList
        '
        Me.pnlParameterList.Controls.Add(Me.lvObjective)
        Me.pnlParameterList.Controls.Add(Me.gbUnlockRemark)
        Me.pnlParameterList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlParameterList.Controls.Add(Me.objFooter)
        Me.pnlParameterList.Controls.Add(Me.eZeeHeader)
        Me.pnlParameterList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlParameterList.Location = New System.Drawing.Point(0, 0)
        Me.pnlParameterList.Name = "pnlParameterList"
        Me.pnlParameterList.Size = New System.Drawing.Size(682, 420)
        Me.pnlParameterList.TabIndex = 1
        '
        'lvObjective
        '
        Me.lvObjective.BackColorOnChecked = False
        Me.lvObjective.ColumnHeaders = Nothing
        Me.lvObjective.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPerspective, Me.colhCode, Me.colhName, Me.colhWeight, Me.colhDescription, Me.objcolhEmployee, Me.objcolhEmpId, Me.objcolhPeriodId, Me.objcolhYearId})
        Me.lvObjective.CompulsoryColumns = ""
        Me.lvObjective.FullRowSelect = True
        Me.lvObjective.GridLines = True
        Me.lvObjective.GroupingColumn = Nothing
        Me.lvObjective.HideSelection = False
        Me.lvObjective.Location = New System.Drawing.Point(9, 141)
        Me.lvObjective.MinColumnWidth = 50
        Me.lvObjective.MultiSelect = False
        Me.lvObjective.Name = "lvObjective"
        Me.lvObjective.OptionalColumns = ""
        Me.lvObjective.ShowMoreItem = False
        Me.lvObjective.ShowSaveItem = False
        Me.lvObjective.ShowSelectAll = True
        Me.lvObjective.ShowSizeAllColumnsToFit = True
        Me.lvObjective.Size = New System.Drawing.Size(661, 218)
        Me.lvObjective.Sortable = True
        Me.lvObjective.TabIndex = 3
        Me.lvObjective.UseCompatibleStateImageBehavior = False
        Me.lvObjective.View = System.Windows.Forms.View.Details
        '
        'colhPerspective
        '
        Me.colhPerspective.Tag = "colhPerspective"
        Me.colhPerspective.Text = "Perspective"
        Me.colhPerspective.Width = 150
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 80
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 180
        '
        'colhWeight
        '
        Me.colhWeight.Tag = "colhWeight"
        Me.colhWeight.Text = "Weight (%) "
        Me.colhWeight.Width = 80
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 150
        '
        'objcolhEmployee
        '
        Me.objcolhEmployee.Tag = "objcolhEmployee"
        Me.objcolhEmployee.Text = ""
        Me.objcolhEmployee.Width = 0
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.Tag = "objcolhEmpId"
        Me.objcolhEmpId.Text = ""
        Me.objcolhEmpId.Width = 0
        '
        'objcolhPeriodId
        '
        Me.objcolhPeriodId.Tag = "objcolhPeriodId"
        Me.objcolhPeriodId.Text = "objcolhPeriodId"
        Me.objcolhPeriodId.Width = 0
        '
        'objcolhYearId
        '
        Me.objcolhYearId.Tag = "objcolhYearId"
        Me.objcolhYearId.Text = ""
        Me.objcolhYearId.Width = 0
        '
        'gbUnlockRemark
        '
        Me.gbUnlockRemark.BorderColor = System.Drawing.Color.Black
        Me.gbUnlockRemark.Checked = False
        Me.gbUnlockRemark.CollapseAllExceptThis = False
        Me.gbUnlockRemark.CollapsedHoverImage = Nothing
        Me.gbUnlockRemark.CollapsedNormalImage = Nothing
        Me.gbUnlockRemark.CollapsedPressedImage = Nothing
        Me.gbUnlockRemark.CollapseOnLoad = False
        Me.gbUnlockRemark.Controls.Add(Me.EZeeFooter1)
        Me.gbUnlockRemark.Controls.Add(Me.objpnlRemark)
        Me.gbUnlockRemark.ExpandedHoverImage = Nothing
        Me.gbUnlockRemark.ExpandedNormalImage = Nothing
        Me.gbUnlockRemark.ExpandedPressedImage = Nothing
        Me.gbUnlockRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUnlockRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUnlockRemark.HeaderHeight = 25
        Me.gbUnlockRemark.HeaderMessage = ""
        Me.gbUnlockRemark.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUnlockRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUnlockRemark.HeightOnCollapse = 0
        Me.gbUnlockRemark.LeftTextSpace = 0
        Me.gbUnlockRemark.Location = New System.Drawing.Point(172, 141)
        Me.gbUnlockRemark.Name = "gbUnlockRemark"
        Me.gbUnlockRemark.OpenHeight = 300
        Me.gbUnlockRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUnlockRemark.ShowBorder = True
        Me.gbUnlockRemark.ShowCheckBox = False
        Me.gbUnlockRemark.ShowCollapseButton = False
        Me.gbUnlockRemark.ShowDefaultBorderColor = True
        Me.gbUnlockRemark.ShowDownButton = False
        Me.gbUnlockRemark.ShowHeader = True
        Me.gbUnlockRemark.Size = New System.Drawing.Size(354, 218)
        Me.gbUnlockRemark.TabIndex = 4
        Me.gbUnlockRemark.Temp = 0
        Me.gbUnlockRemark.Text = "Enter Unlock Remark"
        Me.gbUnlockRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnOk)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 168)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(354, 50)
        Me.EZeeFooter1.TabIndex = 5
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(164, 9)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(86, 30)
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(256, 9)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(86, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "C&ancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'objpnlRemark
        '
        Me.objpnlRemark.Controls.Add(Me.txtUnlockRemark)
        Me.objpnlRemark.Location = New System.Drawing.Point(1, 25)
        Me.objpnlRemark.Name = "objpnlRemark"
        Me.objpnlRemark.Size = New System.Drawing.Size(352, 143)
        Me.objpnlRemark.TabIndex = 1
        '
        'txtUnlockRemark
        '
        Me.txtUnlockRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtUnlockRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnlockRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtUnlockRemark.Multiline = True
        Me.txtUnlockRemark.Name = "txtUnlockRemark"
        Me.txtUnlockRemark.Size = New System.Drawing.Size(352, 143)
        Me.txtUnlockRemark.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.fpnlObjectives)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(661, 71)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fpnlObjectives
        '
        Me.fpnlObjectives.AutoSize = True
        Me.fpnlObjectives.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.fpnlObjectives.Controls.Add(Me.pnlEmployee)
        Me.fpnlObjectives.Controls.Add(Me.pnlPerspective)
        Me.fpnlObjectives.Location = New System.Drawing.Point(3, 30)
        Me.fpnlObjectives.Name = "fpnlObjectives"
        Me.fpnlObjectives.Size = New System.Drawing.Size(642, 34)
        Me.fpnlObjectives.TabIndex = 0
        '
        'pnlEmployee
        '
        Me.pnlEmployee.Controls.Add(Me.lblEmployee)
        Me.pnlEmployee.Controls.Add(Me.cboEmployee)
        Me.pnlEmployee.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlEmployee.Location = New System.Drawing.Point(3, 3)
        Me.pnlEmployee.Name = "pnlEmployee"
        Me.pnlEmployee.Size = New System.Drawing.Size(329, 28)
        Me.pnlEmployee.TabIndex = 14
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(3, 7)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(90, 4)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(210, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(306, 5)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'pnlPerspective
        '
        Me.pnlPerspective.Controls.Add(Me.lblPerspective)
        Me.pnlPerspective.Controls.Add(Me.cboPerspective)
        Me.pnlPerspective.Location = New System.Drawing.Point(338, 3)
        Me.pnlPerspective.Name = "pnlPerspective"
        Me.pnlPerspective.Size = New System.Drawing.Size(301, 28)
        Me.pnlPerspective.TabIndex = 13
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(3, 7)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(80, 15)
        Me.lblPerspective.TabIndex = 0
        Me.lblPerspective.Text = "Perspective"
        Me.lblPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPerspective
        '
        Me.cboPerspective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective.DropDownWidth = 200
        Me.cboPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective.FormattingEnabled = True
        Me.cboPerspective.Location = New System.Drawing.Point(93, 3)
        Me.cboPerspective.Name = "cboPerspective"
        Me.cboPerspective.Size = New System.Drawing.Size(205, 21)
        Me.cboPerspective.TabIndex = 1
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(634, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(611, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblCaption1)
        Me.objFooter.Controls.Add(Me.lblCaption2)
        Me.objFooter.Controls.Add(Me.objpnl2)
        Me.objFooter.Controls.Add(Me.objpnl1)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 365)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(682, 55)
        Me.objFooter.TabIndex = 2
        '
        'lblCaption1
        '
        Me.lblCaption1.Location = New System.Drawing.Point(153, 10)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(149, 17)
        Me.lblCaption1.TabIndex = 129
        Me.lblCaption1.Text = "Submitted for Approval(s)."
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption2
        '
        Me.lblCaption2.Location = New System.Drawing.Point(153, 31)
        Me.lblCaption2.Name = "lblCaption2"
        Me.lblCaption2.Size = New System.Drawing.Size(149, 17)
        Me.lblCaption2.TabIndex = 129
        Me.lblCaption2.Text = "Finally Approved"
        Me.lblCaption2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnl2
        '
        Me.objpnl2.BackColor = System.Drawing.Color.Blue
        Me.objpnl2.Location = New System.Drawing.Point(112, 31)
        Me.objpnl2.Name = "objpnl2"
        Me.objpnl2.Size = New System.Drawing.Size(35, 17)
        Me.objpnl2.TabIndex = 128
        '
        'objpnl1
        '
        Me.objpnl1.BackColor = System.Drawing.Color.Green
        Me.objpnl1.Location = New System.Drawing.Point(112, 10)
        Me.objpnl1.Name = "objpnl1"
        Me.objpnl1.Size = New System.Drawing.Size(35, 17)
        Me.objpnl1.TabIndex = 127
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(9, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(97, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 126
        Me.btnOperations.Text = "Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSubmitApproval, Me.mnuApproveSubmitted, Me.mnuFinalSave, Me.mnuUnlockFinalSave, Me.objSep2, Me.mnuGetFileFormat, Me.mnuImportBSC_Planning, Me.objSep1, Me.mnuPrintBSCForm})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(207, 192)
        '
        'mnuSubmitApproval
        '
        Me.mnuSubmitApproval.Name = "mnuSubmitApproval"
        Me.mnuSubmitApproval.Size = New System.Drawing.Size(206, 22)
        Me.mnuSubmitApproval.Tag = "mnuSubmitApproval"
        Me.mnuSubmitApproval.Text = "S&ubmit for Approval"
        '
        'mnuApproveSubmitted
        '
        Me.mnuApproveSubmitted.Name = "mnuApproveSubmitted"
        Me.mnuApproveSubmitted.Size = New System.Drawing.Size(206, 22)
        Me.mnuApproveSubmitted.Tag = "mnuApproveSubmitted"
        Me.mnuApproveSubmitted.Text = "&Approve/Reject BSC Plan"
        '
        'mnuFinalSave
        '
        Me.mnuFinalSave.Name = "mnuFinalSave"
        Me.mnuFinalSave.Size = New System.Drawing.Size(206, 22)
        Me.mnuFinalSave.Tag = "mnuFinalSave"
        Me.mnuFinalSave.Text = "&Final Save"
        Me.mnuFinalSave.Visible = False
        '
        'mnuUnlockFinalSave
        '
        Me.mnuUnlockFinalSave.Name = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Size = New System.Drawing.Size(206, 22)
        Me.mnuUnlockFinalSave.Tag = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Text = "&Unlock Final Save"
        '
        'mnuImportBSC_Planning
        '
        Me.mnuImportBSC_Planning.Name = "mnuImportBSC_Planning"
        Me.mnuImportBSC_Planning.Size = New System.Drawing.Size(206, 22)
        Me.mnuImportBSC_Planning.Tag = "mnuImportBSC_Planning"
        Me.mnuImportBSC_Planning.Text = "Import BSC Planning"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(203, 6)
        Me.objSep1.Tag = "objSep1"
        '
        'mnuPrintBSCForm
        '
        Me.mnuPrintBSCForm.Name = "mnuPrintBSCForm"
        Me.mnuPrintBSCForm.Size = New System.Drawing.Size(206, 22)
        Me.mnuPrintBSCForm.Tag = "mnuPrintBSCForm"
        Me.mnuPrintBSCForm.Text = "&Print BSC Form"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(492, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(86, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(400, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(86, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(308, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(86, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(584, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(86, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(682, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Objective List"
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(203, 6)
        Me.objSep2.Tag = "objSep2"
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(206, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get &File Format"
        '
        'frmBSC_ObjectiveList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 420)
        Me.Controls.Add(Me.pnlParameterList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_ObjectiveList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Objective List"
        Me.pnlParameterList.ResumeLayout(False)
        Me.gbUnlockRemark.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.objpnlRemark.ResumeLayout(False)
        Me.objpnlRemark.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.fpnlObjectives.ResumeLayout(False)
        Me.pnlEmployee.ResumeLayout(False)
        Me.pnlPerspective.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlParameterList As System.Windows.Forms.Panel
    Friend WithEvents lvObjective As eZee.Common.eZeeListView
    Friend WithEvents colhPerspective As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPerspective As System.Windows.Forms.ComboBox
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objcolhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhWeight As System.Windows.Forms.ColumnHeader
    Friend WithEvents fpnlObjectives As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pnlEmployee As System.Windows.Forms.Panel
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlPerspective As System.Windows.Forms.Panel
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPrintBSCForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlockFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPeriodId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhYearId As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuSubmitApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuApproveSubmitted As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objpnl2 As System.Windows.Forms.Panel
    Friend WithEvents objpnl1 As System.Windows.Forms.Panel
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption2 As System.Windows.Forms.Label
    Friend WithEvents gbUnlockRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnlRemark As System.Windows.Forms.Panel
    Friend WithEvents txtUnlockRemark As System.Windows.Forms.TextBox
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents mnuImportBSC_Planning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
End Class
