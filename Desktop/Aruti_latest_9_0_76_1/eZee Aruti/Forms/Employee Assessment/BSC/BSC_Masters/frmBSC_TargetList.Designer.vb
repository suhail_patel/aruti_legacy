﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_TargetList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_TargetList))
        Me.pnlParameterList = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.fpnlTarget = New System.Windows.Forms.FlowLayoutPanel
        Me.pnlEmployee = New System.Windows.Forms.Panel
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objpnlPeriod = New System.Windows.Forms.Panel
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.pnlObjective = New System.Windows.Forms.Panel
        Me.lblObjective = New System.Windows.Forms.Label
        Me.objbtnSearchObjective = New eZee.Common.eZeeGradientButton
        Me.cboObjective = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objbtnSearchKPI = New eZee.Common.eZeeGradientButton
        Me.lblKPI = New System.Windows.Forms.Label
        Me.cboKPI = New System.Windows.Forms.ComboBox
        Me.lvTarget = New eZee.Common.eZeeListView(Me.components)
        Me.colhObjective = New System.Windows.Forms.ColumnHeader
        Me.colhKPI = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objColhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhWeight = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportTargets = New System.Windows.Forms.ToolStripMenuItem
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.pnlParameterList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fpnlTarget.SuspendLayout()
        Me.pnlEmployee.SuspendLayout()
        Me.objpnlPeriod.SuspendLayout()
        Me.pnlObjective.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlParameterList
        '
        Me.pnlParameterList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlParameterList.Controls.Add(Me.lvTarget)
        Me.pnlParameterList.Controls.Add(Me.objFooter)
        Me.pnlParameterList.Controls.Add(Me.eZeeHeader)
        Me.pnlParameterList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlParameterList.Location = New System.Drawing.Point(0, 0)
        Me.pnlParameterList.Name = "pnlParameterList"
        Me.pnlParameterList.Size = New System.Drawing.Size(689, 422)
        Me.pnlParameterList.TabIndex = 2
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.fpnlTarget)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(665, 101)
        Me.gbFilterCriteria.TabIndex = 4
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(635, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(611, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'fpnlTarget
        '
        Me.fpnlTarget.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.fpnlTarget.Controls.Add(Me.pnlEmployee)
        Me.fpnlTarget.Controls.Add(Me.objpnlPeriod)
        Me.fpnlTarget.Controls.Add(Me.pnlObjective)
        Me.fpnlTarget.Controls.Add(Me.Panel1)
        Me.fpnlTarget.Location = New System.Drawing.Point(3, 30)
        Me.fpnlTarget.Name = "fpnlTarget"
        Me.fpnlTarget.Size = New System.Drawing.Size(659, 68)
        Me.fpnlTarget.TabIndex = 5
        '
        'pnlEmployee
        '
        Me.pnlEmployee.Controls.Add(Me.lblEmployee)
        Me.pnlEmployee.Controls.Add(Me.cboEmployee)
        Me.pnlEmployee.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlEmployee.Location = New System.Drawing.Point(3, 3)
        Me.pnlEmployee.Name = "pnlEmployee"
        Me.pnlEmployee.Size = New System.Drawing.Size(317, 28)
        Me.pnlEmployee.TabIndex = 5
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(6, 7)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 15)
        Me.lblEmployee.TabIndex = 321
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(79, 4)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(210, 21)
        Me.cboEmployee.TabIndex = 320
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(294, 5)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchEmployee.TabIndex = 322
        '
        'objpnlPeriod
        '
        Me.objpnlPeriod.Controls.Add(Me.cboPeriod)
        Me.objpnlPeriod.Controls.Add(Me.lblPeriod)
        Me.objpnlPeriod.Location = New System.Drawing.Point(326, 3)
        Me.objpnlPeriod.Name = "objpnlPeriod"
        Me.objpnlPeriod.Size = New System.Drawing.Size(329, 25)
        Me.objpnlPeriod.TabIndex = 342
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(90, 2)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(210, 21)
        Me.cboPeriod.TabIndex = 340
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(7, 5)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 15)
        Me.lblPeriod.TabIndex = 339
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlObjective
        '
        Me.pnlObjective.Controls.Add(Me.lblObjective)
        Me.pnlObjective.Controls.Add(Me.objbtnSearchObjective)
        Me.pnlObjective.Controls.Add(Me.cboObjective)
        Me.pnlObjective.Location = New System.Drawing.Point(3, 37)
        Me.pnlObjective.Name = "pnlObjective"
        Me.pnlObjective.Size = New System.Drawing.Size(317, 28)
        Me.pnlObjective.TabIndex = 6
        '
        'lblObjective
        '
        Me.lblObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObjective.Location = New System.Drawing.Point(5, 7)
        Me.lblObjective.Name = "lblObjective"
        Me.lblObjective.Size = New System.Drawing.Size(67, 15)
        Me.lblObjective.TabIndex = 8
        Me.lblObjective.Text = "Objective"
        Me.lblObjective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchObjective
        '
        Me.objbtnSearchObjective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchObjective.BorderSelected = False
        Me.objbtnSearchObjective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchObjective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchObjective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchObjective.Location = New System.Drawing.Point(294, 4)
        Me.objbtnSearchObjective.Name = "objbtnSearchObjective"
        Me.objbtnSearchObjective.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchObjective.TabIndex = 325
        '
        'cboObjective
        '
        Me.cboObjective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObjective.DropDownWidth = 400
        Me.cboObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObjective.FormattingEnabled = True
        Me.cboObjective.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboObjective.Location = New System.Drawing.Point(78, 3)
        Me.cboObjective.Name = "cboObjective"
        Me.cboObjective.Size = New System.Drawing.Size(210, 21)
        Me.cboObjective.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objbtnSearchKPI)
        Me.Panel1.Controls.Add(Me.lblKPI)
        Me.Panel1.Controls.Add(Me.cboKPI)
        Me.Panel1.Location = New System.Drawing.Point(326, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(329, 27)
        Me.Panel1.TabIndex = 7
        '
        'objbtnSearchKPI
        '
        Me.objbtnSearchKPI.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchKPI.BorderSelected = False
        Me.objbtnSearchKPI.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchKPI.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchKPI.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchKPI.Location = New System.Drawing.Point(306, 4)
        Me.objbtnSearchKPI.Name = "objbtnSearchKPI"
        Me.objbtnSearchKPI.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchKPI.TabIndex = 326
        '
        'lblKPI
        '
        Me.lblKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKPI.Location = New System.Drawing.Point(7, 6)
        Me.lblKPI.Name = "lblKPI"
        Me.lblKPI.Size = New System.Drawing.Size(80, 15)
        Me.lblKPI.TabIndex = 11
        Me.lblKPI.Text = "KPI / Measures"
        Me.lblKPI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboKPI
        '
        Me.cboKPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKPI.DropDownWidth = 400
        Me.cboKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboKPI.FormattingEnabled = True
        Me.cboKPI.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboKPI.Location = New System.Drawing.Point(90, 3)
        Me.cboKPI.Name = "cboKPI"
        Me.cboKPI.Size = New System.Drawing.Size(210, 21)
        Me.cboKPI.TabIndex = 10
        '
        'lvTarget
        '
        Me.lvTarget.BackColorOnChecked = False
        Me.lvTarget.ColumnHeaders = Nothing
        Me.lvTarget.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhObjective, Me.colhKPI, Me.colhCode, Me.colhName, Me.colhDescription, Me.objColhEmployee, Me.colhWeight})
        Me.lvTarget.CompulsoryColumns = ""
        Me.lvTarget.FullRowSelect = True
        Me.lvTarget.GridLines = True
        Me.lvTarget.GroupingColumn = Nothing
        Me.lvTarget.HideSelection = False
        Me.lvTarget.Location = New System.Drawing.Point(12, 171)
        Me.lvTarget.MinColumnWidth = 50
        Me.lvTarget.MultiSelect = False
        Me.lvTarget.Name = "lvTarget"
        Me.lvTarget.OptionalColumns = ""
        Me.lvTarget.ShowMoreItem = False
        Me.lvTarget.ShowSaveItem = False
        Me.lvTarget.ShowSelectAll = True
        Me.lvTarget.ShowSizeAllColumnsToFit = True
        Me.lvTarget.Size = New System.Drawing.Size(665, 191)
        Me.lvTarget.Sortable = True
        Me.lvTarget.TabIndex = 3
        Me.lvTarget.UseCompatibleStateImageBehavior = False
        Me.lvTarget.View = System.Windows.Forms.View.Details
        '
        'colhObjective
        '
        Me.colhObjective.Tag = "colhObjective"
        Me.colhObjective.Text = "Objective"
        Me.colhObjective.Width = 130
        '
        'colhKPI
        '
        Me.colhKPI.Tag = "colhKPI"
        Me.colhKPI.Text = "KPI/Measures"
        Me.colhKPI.Width = 120
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 90
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 130
        '
        'colhDescription
        '
        Me.colhDescription.DisplayIndex = 5
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 125
        '
        'objColhEmployee
        '
        Me.objColhEmployee.DisplayIndex = 6
        Me.objColhEmployee.Tag = "objColhEmployee"
        Me.objColhEmployee.Text = ""
        Me.objColhEmployee.Width = 0
        '
        'colhWeight
        '
        Me.colhWeight.DisplayIndex = 4
        Me.colhWeight.Tag = "colhWeight"
        Me.colhWeight.Text = "Weight"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 367)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(689, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(477, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(374, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(271, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(580, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(689, 58)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Targets List"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGetFileFormat, Me.mnuImportTargets})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(155, 48)
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(154, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get &File Format"
        '
        'mnuImportTargets
        '
        Me.mnuImportTargets.Name = "mnuImportTargets"
        Me.mnuImportTargets.Size = New System.Drawing.Size(154, 22)
        Me.mnuImportTargets.Tag = "mnuImportTargets"
        Me.mnuImportTargets.Text = "&Import Targets"
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(103, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 9
        Me.btnOperation.Text = "Operation"
        '
        'frmBSC_TargetList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(689, 422)
        Me.Controls.Add(Me.pnlParameterList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_TargetList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Targets List"
        Me.pnlParameterList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fpnlTarget.ResumeLayout(False)
        Me.pnlEmployee.ResumeLayout(False)
        Me.objpnlPeriod.ResumeLayout(False)
        Me.pnlObjective.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlParameterList As System.Windows.Forms.Panel
    Friend WithEvents lvTarget As eZee.Common.eZeeListView
    Friend WithEvents colhObjective As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboObjective As System.Windows.Forms.ComboBox
    Friend WithEvents lblObjective As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents colhKPI As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents fpnlTarget As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pnlEmployee As System.Windows.Forms.Panel
    Friend WithEvents pnlObjective As System.Windows.Forms.Panel
    Friend WithEvents objColhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblKPI As System.Windows.Forms.Label
    Friend WithEvents cboKPI As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchObjective As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchKPI As eZee.Common.eZeeGradientButton
    Friend WithEvents colhWeight As System.Windows.Forms.ColumnHeader
    Friend WithEvents objpnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportTargets As System.Windows.Forms.ToolStripMenuItem
End Class
