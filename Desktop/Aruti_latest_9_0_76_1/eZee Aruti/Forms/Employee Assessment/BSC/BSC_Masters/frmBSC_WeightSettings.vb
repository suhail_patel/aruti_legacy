﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSC_WeightSettings

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmBSC_WeightSettings"
    Private mblnCancel As Boolean = True
    Private mintSettingUnkid As Integer = 0
    Private objWSetting As clsWeight_Setting
    Private mintOldWOptionId As Integer = 0
    Private mintOldWTypeId As Integer = 0

#End Region

#Region " Form's Event "

    Private Sub frmBSC_WeightSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objWSetting = New clsWeight_Setting
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_WeightSettings_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_WeightSettings_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_WeightSettings_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_WeightSettings_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_WeightSettings_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid_Data() = False Then Exit Sub

            Call SetValue()

            If objWSetting._Weight_Optionid <> mintOldWOptionId Then
                If objWSetting.Is_Valid_Setting(mintOldWOptionId, mintOldWTypeId) = False Then
emsg:               eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot set this setting. Reason : Some data is already defined with previous setting." & vbCrLf & _
                                                        "To change the weight setting please remove them first."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If objWSetting._Weight_Typeid <> mintOldWTypeId Then
                    If objWSetting.Is_Valid_Setting(mintOldWOptionId, mintOldWTypeId) = False Then
                        GoTo emsg
                    End If
                End If
            End If

            If mintSettingUnkid <= 0 Then
                blnFlag = objWSetting.Insert()
            Else
                blnFlag = objWSetting.Update()
            End If

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Setting saved successfully."), enMsgBoxStyle.Information)
                Call btnClose_Click(sender, e)
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub GetValue()
        Try
            If objWSetting.Is_Setting_Present(mintSettingUnkid) = True Then
                If mintSettingUnkid > 0 Then
                    objWSetting._Wsettingunkid = mintSettingUnkid
                    Select Case objWSetting._Weight_Optionid
                        Case enWeight_Options.WEIGHT_EACH_ITEM
                            radOption1.Checked = True
                        Case enWeight_Options.WEIGHT_BASED_ON
                            radOption2.Checked = True
                    End Select
                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD1
                            radObjective.Checked = True
                        Case enWeight_Types.WEIGHT_FIELD2
                            radKPI.Checked = True
                        Case enWeight_Types.WEIGHT_FIELD3
                            radTargets.Checked = True
                        Case enWeight_Types.WEIGHT_FIELD4
                            radInitiative.Checked = True
                    End Select
                End If
                mintOldWOptionId = objWSetting._Weight_Optionid
                mintOldWTypeId = objWSetting._Weight_Typeid
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objWSetting._Wsettingunkid = CInt(IIf(mintSettingUnkid <= 0, 1, mintSettingUnkid))
            If radOption1.Checked = True Then
                objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM
                objWSetting._Weight_Typeid = 0
            ElseIf radOption2.Checked = True Then
                objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON
            End If
            If radObjective.Checked = True Then
                objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1
            ElseIf radKPI.Checked = True Then
                objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2
            ElseIf radTargets.Checked = True Then
                objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3
            ElseIf radInitiative.Checked = True Then
                objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            If radOption1.Checked = False AndAlso radOption2.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set balance score card weigthed option."), enMsgBoxStyle.Information)
                Return False
            End If

            If radOption2.Checked = True Then
                If radObjective.Checked = False AndAlso _
                   radKPI.Checked = False AndAlso _
                   radTargets.Checked = False AndAlso _
                   radInitiative.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please set balance score card weigthed based on."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "isValid_Data", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Controls Events "

    Private Sub radOption1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOption1.CheckedChanged, radOption2.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case "RADOPTION1"
                    If CType(sender, RadioButton).Checked = True Then
                        objgbTypes.Enabled = False
                        radObjective.Checked = False
                        radKPI.Checked = False
                        radTargets.Checked = False
                        radInitiative.Checked = False
                    End If
                Case "RADOPTION2"
                    If CType(sender, RadioButton).Checked = True Then
                        objgbTypes.Enabled = True
                    End If
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "radOption1_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbWeightSetting.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbWeightSetting.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbWeightSetting.Text = Language._Object.getCaption(Me.gbWeightSetting.Name, Me.gbWeightSetting.Text)
			Me.radObjective.Text = Language._Object.getCaption(Me.radObjective.Name, Me.radObjective.Text)
			Me.radInitiative.Text = Language._Object.getCaption(Me.radInitiative.Name, Me.radInitiative.Text)
			Me.radKPI.Text = Language._Object.getCaption(Me.radKPI.Name, Me.radKPI.Text)
			Me.radOption2.Text = Language._Object.getCaption(Me.radOption2.Name, Me.radOption2.Text)
			Me.radTargets.Text = Language._Object.getCaption(Me.radTargets.Name, Me.radTargets.Text)
			Me.radOption1.Text = Language._Object.getCaption(Me.radOption1.Name, Me.radOption1.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please set balance score card weigthed option.")
			Language.setMessage(mstrModuleName, 2, "Please set balance score card weigthed based on.")
			Language.setMessage(mstrModuleName, 3, "Setting saved successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class