﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_Planning
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_Planning))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvPlanning = New eZee.Common.eZeeListView(Me.components)
        Me.colhKPI = New System.Windows.Forms.ColumnHeader
        Me.colhTargets = New System.Windows.Forms.ColumnHeader
        Me.colhinitiatives = New System.Windows.Forms.ColumnHeader
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.pnlObjective = New System.Windows.Forms.Panel
        Me.txtObjective = New System.Windows.Forms.TextBox
        Me.lblObjective = New System.Windows.Forms.Label
        Me.pnlLinks = New System.Windows.Forms.Panel
        Me.lnkIntiatives = New System.Windows.Forms.LinkLabel
        Me.lnkAddTargets = New System.Windows.Forms.LinkLabel
        Me.lnkAddKPI = New System.Windows.Forms.LinkLabel
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.EZeeStraightLine3 = New eZee.Common.eZeeStraightLine
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnlObjective.SuspendLayout()
        Me.pnlLinks.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(655, 378)
        Me.pnlMain.TabIndex = 0
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 328)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(655, 50)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(553, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvPlanning
        '
        Me.lvPlanning.BackColorOnChecked = False
        Me.lvPlanning.ColumnHeaders = Nothing
        Me.lvPlanning.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhKPI, Me.colhTargets, Me.colhinitiatives})
        Me.lvPlanning.CompulsoryColumns = ""
        Me.lvPlanning.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPlanning.FullRowSelect = True
        Me.lvPlanning.GridLines = True
        Me.lvPlanning.GroupingColumn = Nothing
        Me.lvPlanning.HideSelection = False
        Me.lvPlanning.Location = New System.Drawing.Point(5, 75)
        Me.lvPlanning.MinColumnWidth = 50
        Me.lvPlanning.MultiSelect = False
        Me.lvPlanning.Name = "lvPlanning"
        Me.lvPlanning.OptionalColumns = ""
        Me.lvPlanning.ShowMoreItem = False
        Me.lvPlanning.ShowSaveItem = False
        Me.lvPlanning.ShowSelectAll = True
        Me.lvPlanning.ShowSizeAllColumnsToFit = True
        Me.lvPlanning.Size = New System.Drawing.Size(645, 248)
        Me.lvPlanning.Sortable = True
        Me.lvPlanning.TabIndex = 1
        Me.lvPlanning.UseCompatibleStateImageBehavior = False
        Me.lvPlanning.View = System.Windows.Forms.View.Details
        '
        'colhKPI
        '
        Me.colhKPI.Tag = "colhKPI"
        Me.colhKPI.Text = "Key Performance Indicators"
        Me.colhKPI.Width = 215
        '
        'colhTargets
        '
        Me.colhTargets.Tag = "colhTargets"
        Me.colhTargets.Text = "Targets"
        Me.colhTargets.Width = 215
        '
        'colhinitiatives
        '
        Me.colhinitiatives.Tag = "colhinitiatives"
        Me.colhinitiatives.Text = "Initiatives / Actions"
        Me.colhinitiatives.Width = 210
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lvPlanning, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.pnlObjective, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pnlLinks, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(655, 328)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'pnlObjective
        '
        Me.pnlObjective.Controls.Add(Me.txtObjective)
        Me.pnlObjective.Controls.Add(Me.lblObjective)
        Me.pnlObjective.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlObjective.Location = New System.Drawing.Point(5, 5)
        Me.pnlObjective.Name = "pnlObjective"
        Me.pnlObjective.Size = New System.Drawing.Size(645, 26)
        Me.pnlObjective.TabIndex = 2
        '
        'txtObjective
        '
        Me.txtObjective.BackColor = System.Drawing.Color.White
        Me.txtObjective.Location = New System.Drawing.Point(111, 3)
        Me.txtObjective.Name = "txtObjective"
        Me.txtObjective.ReadOnly = True
        Me.txtObjective.Size = New System.Drawing.Size(531, 21)
        Me.txtObjective.TabIndex = 1
        '
        'lblObjective
        '
        Me.lblObjective.Location = New System.Drawing.Point(2, 5)
        Me.lblObjective.Name = "lblObjective"
        Me.lblObjective.Size = New System.Drawing.Size(103, 16)
        Me.lblObjective.TabIndex = 0
        Me.lblObjective.Text = "Objective"
        Me.lblObjective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLinks
        '
        Me.pnlLinks.Controls.Add(Me.lnkIntiatives)
        Me.pnlLinks.Controls.Add(Me.lnkAddTargets)
        Me.pnlLinks.Controls.Add(Me.lnkAddKPI)
        Me.pnlLinks.Controls.Add(Me.EZeeStraightLine1)
        Me.pnlLinks.Controls.Add(Me.EZeeStraightLine3)
        Me.pnlLinks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLinks.Location = New System.Drawing.Point(5, 39)
        Me.pnlLinks.Name = "pnlLinks"
        Me.pnlLinks.Size = New System.Drawing.Size(645, 28)
        Me.pnlLinks.TabIndex = 3
        '
        'lnkIntiatives
        '
        Me.lnkIntiatives.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkIntiatives.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkIntiatives.Location = New System.Drawing.Point(474, 9)
        Me.lnkIntiatives.Name = "lnkIntiatives"
        Me.lnkIntiatives.Size = New System.Drawing.Size(156, 16)
        Me.lnkIntiatives.TabIndex = 6
        Me.lnkIntiatives.TabStop = True
        Me.lnkIntiatives.Text = "Add Intiatives"
        Me.lnkIntiatives.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lnkAddTargets
        '
        Me.lnkAddTargets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAddTargets.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddTargets.Location = New System.Drawing.Point(259, 9)
        Me.lnkAddTargets.Name = "lnkAddTargets"
        Me.lnkAddTargets.Size = New System.Drawing.Size(128, 16)
        Me.lnkAddTargets.TabIndex = 5
        Me.lnkAddTargets.TabStop = True
        Me.lnkAddTargets.Text = "Add Targets"
        Me.lnkAddTargets.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lnkAddKPI
        '
        Me.lnkAddKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAddKPI.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddKPI.Location = New System.Drawing.Point(7, 9)
        Me.lnkAddKPI.Name = "lnkAddKPI"
        Me.lnkAddKPI.Size = New System.Drawing.Size(165, 16)
        Me.lnkAddKPI.TabIndex = 4
        Me.lnkAddKPI.TabStop = True
        Me.lnkAddKPI.Text = "Add KPI"
        Me.lnkAddKPI.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(393, 6)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(75, 17)
        Me.EZeeStraightLine1.TabIndex = 3
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'EZeeStraightLine3
        '
        Me.EZeeStraightLine3.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeStraightLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine3.Location = New System.Drawing.Point(178, 6)
        Me.EZeeStraightLine3.Name = "EZeeStraightLine3"
        Me.EZeeStraightLine3.Size = New System.Drawing.Size(75, 17)
        Me.EZeeStraightLine3.TabIndex = 2
        Me.EZeeStraightLine3.Text = "EZeeStraightLine3"
        '
        'frmBSC_Planning
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 378)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_Planning"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BSC Planning"
        Me.pnlMain.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.pnlObjective.ResumeLayout(False)
        Me.pnlObjective.PerformLayout()
        Me.pnlLinks.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lvPlanning As eZee.Common.eZeeListView
    Friend WithEvents colhKPI As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTargets As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhinitiatives As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlObjective As System.Windows.Forms.Panel
    Friend WithEvents txtObjective As System.Windows.Forms.TextBox
    Friend WithEvents lblObjective As System.Windows.Forms.Label
    Friend WithEvents pnlLinks As System.Windows.Forms.Panel
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents EZeeStraightLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents lnkIntiatives As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAddTargets As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAddKPI As System.Windows.Forms.LinkLabel
End Class
