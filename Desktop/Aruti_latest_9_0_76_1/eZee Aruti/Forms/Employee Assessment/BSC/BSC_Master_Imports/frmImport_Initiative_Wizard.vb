﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImport_Initiative_Wizard

#Region " Private Variables "

    Private mstrModuleName As String = "frmImport_Initiative_Wizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private objWSetting As clsWeight_Setting

#End Region

#Region " Form's Events "

    Private Sub frmImport_Target_Wizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objWSetting = New clsWeight_Setting(, True)
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp

            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                lblValue.Text = Language.getMessage(mstrModuleName, 21, "Please make sure that if BSC is based on each item setting make sure that 'weight' should not exceed the Targets.")
                objlblSign4.Visible = True
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
                    lblValue.Text = Language.getMessage(mstrModuleName, 22, "Please make sure that weight should not exceed 100 for each employee in the file you are trying to import.")
                End If
                objlblSign4.Visible = False
            End If

            Select Case objWSetting._Weight_Optionid
                Case enWeight_Options.WEIGHT_BASED_ON
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
                        Call Enable_Disable_Weight(True)
                    Else
                        Call Enable_Disable_Weight(False)
                    End If
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    Call Enable_Disable_Weight(True)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_Target_Wizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub InitiativeWizard_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles InitiativeWizard.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case InitiativeWizard.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InitiativeWizard_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub InitiativeWizard_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles InitiativeWizard.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case InitiativeWizard.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select proper file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)
                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select proper file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                Case InitiativeWizard.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                                    If CType(ctrl, ComboBox).Text = "" Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                        e.Cancel = True
                                        Exit For
                                    End If
                                Else
                                    If CType(ctrl, ComboBox).Name.ToString.ToUpper = cboTargetCode.Name.ToString.ToUpper Then Continue For
                                    If CType(ctrl, ComboBox).Text = "" AndAlso CType(ctrl, ComboBox).Enabled = True Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                        e.Cancel = True
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    End If
                Case InitiativeWizard.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InitiativeWizard_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboObjetiveCode.Items.Add(dtColumns.ColumnName)
                cboTargetCode.Items.Add(dtColumns.ColumnName)
                cboInitiativeCode.Items.Add(dtColumns.ColumnName)
                cboInitiativeName.Items.Add(dtColumns.ColumnName)
                cboWeight.Items.Add(dtColumns.ColumnName)
                cboPeriod.Items.Add(dtColumns.ColumnName)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Enable_Disable_Weight(ByVal oPr As Boolean)
        Try
            objlblSign6.Enabled = oPr
            lblWeight.Enabled = oPr
            cboWeight.Enabled = oPr
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Enable_Disable_Weight", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Try
            ezWait.Active = True
            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ocode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("tcode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("icode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("iname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("weight", System.Type.GetType("System.Decimal"))
            mdt_ImportData_Others.Columns.Add("period", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("ocode") = dtRow.Item(cboObjetiveCode.Text).ToString.Trim

                If dtRow.Item(cboTargetCode.Text).ToString.Trim.Length > 0 Then
                    drNewRow.Item("tcode") = dtRow.Item(cboTargetCode.Text).ToString.Trim
                Else
                    drNewRow.Item("tcode") = ""
                End If
                drNewRow.Item("icode") = dtRow.Item(cboInitiativeCode.Text).ToString.Trim
                drNewRow.Item("iname") = dtRow.Item(cboInitiativeName.Text).ToString.Trim
                drNewRow.Item("period") = dtRow.Item(cboPeriod.Text).ToString.Trim

                If cboWeight.Enabled = True Then
                    drNewRow.Item("weight") = dtRow.Item(cboWeight.Text).ToString.Trim
                Else
                    drNewRow.Item("weight") = 0
                End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ecode"
                dgcolhCode.DataPropertyName = "ocode"
                dgcolhTgtCode.DataPropertyName = "tcode"
                dgcolhName.DataPropertyName = "iname"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            InitiativeWizard.BackEnabled = False
            InitiativeWizard.CancelText = "Finish"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboObjetiveCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Objective Code is mandatory information. Please select Objective Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                    If .Item(cboTargetCode.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Target Code is mandatory information. Please select KPI Code to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If .Item(cboInitiativeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Target Code is mandatory information. Please select Target Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboInitiativeName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Target Name is mandatory information. Please select Target Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboWeight.Enabled = True Then
                    If .Item(cboWeight.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Weight is mandatory information. Please select Weight to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If .Item(cboPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        btnFilter.Enabled = False
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then Exit Sub
            Dim iEmpId, iObjectiveId, iPeriodId, iTargetId, iYearId As Integer
            Dim objEmp As New clsEmployee_Master
            Dim objPrd As New clscommom_period_Tran
            Dim objObjective As New clsObjective_Master
            Dim objTarget As New clstarget_master
            Dim objInitiative As clsinitiative_master

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                iEmpId = 0 : iObjectiveId = 0 : iPeriodId = 0 : iTargetId = 0 : iYearId = 0

                '>>>>>>>>>>>>>>>>>>>>> IF EMPLOYEE PERESET IN THE SYSTEM
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF PERIOD IN THE SYSTEM
                If dtRow.Item("period").ToString.Trim.Length > 0 Then
                    iPeriodId = objPrd.GetPeriodByName(dtRow.Item("period").ToString.Trim, enModuleReference.Assessment)
                    If iPeriodId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Period Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPrd._Periodunkid = iPeriodId
                        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
                        'Sohail (21 Aug 2015) -- End
                        iYearId = objPrd._Yearunkid
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF OBJECTIVE PERESET IN THE SYSTEM
                If dtRow.Item("ocode").ToString.Trim.Length > 0 Then
                    iObjectiveId = objObjective.GetObjectiveUnkid(dtRow.Item("ocode").ToString.Trim, "", iPeriodId, iEmpId)
                    If iObjectiveId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Objective Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF TARGET PERESET IN THE SYSTEM
                If dtRow.Item("tcode").ToString.Trim.Length > 0 Then
                    iTargetId = objTarget.GetTargetUnkid(CStr(dtRow.Item("tcode")), "", iObjectiveId)
                    If iTargetId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Target Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                Dim iInitiativeId As Integer = 0

                '>>>>>>>>>>>>>>>>>>>>> IF VALID WEIGHT IN THE FILE
                If cboWeight.Enabled = True Then
                    If dtRow.Item("weight").ToString.Trim.Length > 0 Then
                        If Val(dtRow.Item("weight").ToString.Trim) <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Invalid Weigth. Weigth should be in between (1 to 100)")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'Else
                            '    Dim mDecTotal, mDecAssign As Decimal
                            '    mDecTotal = 0 : mDecAssign = 0
                            '    Select Case objWSetting._Weight_Optionid
                            '        Case enWeight_Options.WEIGHT_EACH_ITEM
                            '            objWSetting.Get_Each_Item_Weight(enWeight_Types.WEIGHT_FIELD4, iTargetId, iTargetId, mDecAssign, mDecTotal)
                            '            If mDecTotal < mDecAssign + Val(dtRow.Item("weight").ToString.Trim) Then
                            '                dtRow.Item("image") = imgError
                            '                dtRow.Item("message") = Language.getMessage(mstrModuleName, 23, "Invalid Weigth. Weigth exceeds the total of Target weight set.")
                            '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            '                dtRow.Item("objStatus") = 2
                            '                objError.Text = CStr(Val(objError.Text) + 1)
                            '                Continue For
                            '            End If
                            '        Case enWeight_Options.WEIGHT_BASED_ON
                            '            objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD4, iInitiativeId, mDecAssign, mDecTotal, iEmpId, iYearId)
                            '            If mDecTotal < mDecAssign + Val(dtRow.Item("weight").ToString.Trim) Then
                            '                dtRow.Item("image") = imgError
                            '                dtRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Invalid Weigth. Weigth exceeds the total of 100 for the particular employee.")
                            '                dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            '                dtRow.Item("objStatus") = 2
                            '                objError.Text = CStr(Val(objError.Text) + 1)
                            '                Continue For
                            '            End If
                            '    End Select
                        End If
                    End If
                End If

                objInitiative = New clsinitiative_master

                iInitiativeId = objInitiative.GetInitiativeUnkid("", dtRow.Item("iname").ToString.Trim, iObjectiveId, iTargetId)

                If iInitiativeId <= 0 Then
                    objInitiative._Code = dtRow.Item("icode").ToString.Trim
                    objInitiative._Description = ""
                    objInitiative._EmployeeunkId = iEmpId
                    objInitiative._Isactive = True
                    objInitiative._Name = dtRow.Item("iname").ToString.Trim
                    objInitiative._Name1 = objInitiative._Name
                    objInitiative._Name2 = objInitiative._Name
                    objInitiative._Objectiveunkid = iObjectiveId
                    objInitiative._Targetunkid = iTargetId
                    If cboWeight.Enabled = True Then
                        objInitiative._Weight = CDec(dtRow.Item("weight"))
                    Else
                        objInitiative._Weight = 0
                    End If
                    If objInitiative.Insert(User._Object._Userunkid) = True Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 18, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objObjective._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                Else
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Data Already Exists.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Warning")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            If objWSetting.Is_Setting_Present(-1) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeLightButton1.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeLightButton1.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeLightButton2.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeLightButton2.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeLightButton3.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeLightButton3.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeLightButton4.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeLightButton4.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeLightButton5.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeLightButton5.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeLightButton6.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeLightButton6.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.InitiativeWizard.CancelText = Language._Object.getCaption(Me.InitiativeWizard.Name & "_CancelText" , Me.InitiativeWizard.CancelText)
			Me.InitiativeWizard.NextText = Language._Object.getCaption(Me.InitiativeWizard.Name & "_NextText" , Me.InitiativeWizard.NextText)
			Me.InitiativeWizard.BackText = Language._Object.getCaption(Me.InitiativeWizard.Name & "_BackText" , Me.InitiativeWizard.BackText)
			Me.InitiativeWizard.FinishText = Language._Object.getCaption(Me.InitiativeWizard.Name & "_FinishText" , Me.InitiativeWizard.FinishText)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.EZeeLightButton1.Text = Language._Object.getCaption(Me.EZeeLightButton1.Name, Me.EZeeLightButton1.Text)
			Me.EZeeLightButton2.Text = Language._Object.getCaption(Me.EZeeLightButton2.Name, Me.EZeeLightButton2.Text)
			Me.EZeeLightButton3.Text = Language._Object.getCaption(Me.EZeeLightButton3.Name, Me.EZeeLightButton3.Text)
			Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
			Me.lblInI_Name.Text = Language._Object.getCaption(Me.lblInI_Name.Name, Me.lblInI_Name.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblTargetCode.Text = Language._Object.getCaption(Me.lblTargetCode.Name, Me.lblTargetCode.Text)
			Me.lblObjectiveCode.Text = Language._Object.getCaption(Me.lblObjectiveCode.Name, Me.lblObjectiveCode.Text)
			Me.lblInI_Code.Text = Language._Object.getCaption(Me.lblInI_Code.Name, Me.lblInI_Code.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.EZeeLightButton4.Text = Language._Object.getCaption(Me.EZeeLightButton4.Name, Me.EZeeLightButton4.Text)
			Me.EZeeLightButton5.Text = Language._Object.getCaption(Me.EZeeLightButton5.Name, Me.EZeeLightButton5.Text)
			Me.EZeeLightButton6.Text = Language._Object.getCaption(Me.EZeeLightButton6.Name, Me.EZeeLightButton6.Text)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
			Me.dgcolhTgtCode.HeaderText = Language._Object.getCaption(Me.dgcolhTgtCode.Name, Me.dgcolhTgtCode.HeaderText)
			Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.Name, Me.lblValue.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select proper file to Import Data.")
			Language.setMessage(mstrModuleName, 2, "Please select proper field to Import Data.")
			Language.setMessage(mstrModuleName, 3, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting.")
			Language.setMessage(mstrModuleName, 4, "Employee Code is mandatory information. Please select Employee Code to continue.")
			Language.setMessage(mstrModuleName, 5, "Objective Code is mandatory information. Please select Objective Code to continue.")
			Language.setMessage(mstrModuleName, 6, "Target Code is mandatory information. Please select KPI Code to continue.")
			Language.setMessage(mstrModuleName, 7, "Target Code is mandatory information. Please select Target Code to continue.")
			Language.setMessage(mstrModuleName, 8, "Target Name is mandatory information. Please select Target Name to continue.")
			Language.setMessage(mstrModuleName, 9, "Weight is mandatory information. Please select Weight to continue.")
			Language.setMessage(mstrModuleName, 10, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 11, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 12, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 13, "Fail")
			Language.setMessage(mstrModuleName, 14, "Period Not Found.")
			Language.setMessage(mstrModuleName, 15, "Objective Not Found.")
			Language.setMessage(mstrModuleName, 16, "Target Not Found.")
			Language.setMessage(mstrModuleName, 17, "Invalid Weigth. Weigth should be in between (1 to 100)")
			Language.setMessage(mstrModuleName, 18, "Success")
			Language.setMessage(mstrModuleName, 19, "Data Already Exists.")
			Language.setMessage(mstrModuleName, 20, "Warning")
			Language.setMessage(mstrModuleName, 21, "Please make sure that if BSC is based on each item setting make sure that 'weight' should not exceed the Targets.")
			Language.setMessage(mstrModuleName, 22, "Please make sure that weight should not exceed 100 for each employee in the file you are trying to import.")
			Language.setMessage(mstrModuleName, 23, "Invalid Weigth. Weigth exceeds the total of Target weight set.")
			Language.setMessage(mstrModuleName, 24, "Invalid Weigth. Weigth exceeds the total of 100 for the particular employee.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class