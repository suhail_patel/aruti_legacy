﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImport_Objective_Wizard

#Region " Private Variables "

    Private mstrModuleName As String = "frmImport_Objective_Wizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private objWSetting As clsWeight_Setting

#End Region

#Region " Form's Events "

    Private Sub frmImport_Objective_Wizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objWSetting = New clsWeight_Setting(, True)
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp

            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Or objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                lblValue.Text = Language.getMessage(mstrModuleName, 21, "Please make sure that weight should not exceed 100 for each employee in the file you are trying to import.")
            Else
                lblValue.Text = ""
            End If

            Select Case objWSetting._Weight_Optionid
                Case enWeight_Options.WEIGHT_BASED_ON
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                        Call Enable_Disable_Weight(True)
                    Else
                        Call Enable_Disable_Weight(False)
                    End If
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    Call Enable_Disable_Weight(True)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_Objective_Wizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub ObjectiveWizard_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles ObjectiveWizard.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case ObjectiveWizard.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ObjectiveWizard_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ObjectiveWizard_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles ObjectiveWizard.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case ObjectiveWizard.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select proper file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)
                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select proper file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                Case ObjectiveWizard.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If CType(ctrl, ComboBox).Text = "" AndAlso CType(ctrl, ComboBox).Enabled = True Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case ObjectiveWizard.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ObjectiveWizard_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboObjectiveName.Items.Add(dtColumns.ColumnName)
                cboObjetiveCode.Items.Add(dtColumns.ColumnName)
                cboPeriod.Items.Add(dtColumns.ColumnName)
                cboPerspective.Items.Add(dtColumns.ColumnName)
                cboResultGroup.Items.Add(dtColumns.ColumnName)
                cboWeight.Items.Add(dtColumns.ColumnName)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Try
            ezWait.Active = True
            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("perspectiveid", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ocode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("oname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("rgroup", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("weight", System.Type.GetType("System.Decimal"))
            mdt_ImportData_Others.Columns.Add("period", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("perspectiveid") = dtRow.Item(cboPerspective.Text).ToString.Trim
                drNewRow.Item("ocode") = dtRow.Item(cboObjetiveCode.Text).ToString.Trim
                drNewRow.Item("oname") = dtRow.Item(cboObjectiveName.Text).ToString.Trim
                drNewRow.Item("rgroup") = dtRow.Item(cboResultGroup.Text).ToString.Trim
                If cboWeight.Enabled = True Then
                    drNewRow.Item("weight") = dtRow.Item(cboWeight.Text).ToString.Trim
                Else
                    drNewRow.Item("weight") = 0
                End If
                drNewRow.Item("period") = dtRow.Item(cboPeriod.Text).ToString.Trim


                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ecode"
                dgcolhCode.DataPropertyName = "ocode"
                dgcolhName.DataPropertyName = "oname"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            ObjectiveWizard.BackEnabled = False
            ObjectiveWizard.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Weight(ByVal oPr As Boolean)
        Try
            objlblSign6.Enabled = oPr
            lblWeight.Enabled = oPr
            cboWeight.Enabled = oPr
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Enable_Disable_Weight", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboPerspective.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Perspective is mandatory information. Please select Perspective to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboObjetiveCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Objective Code is mandatory information. Please select Objective Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboObjectiveName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Objective Name is mandatory information. Please select Objective Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboResultGroup.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Result Group is mandatory information. Please select Result Group to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboWeight.Enabled = True Then
                    If .Item(cboWeight.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Weight is mandatory information. Please select Weight to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        btnFilter.Enabled = False
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then Exit Sub
            Dim iEmpId, iYearunkid, iResultGrpId, iPeriodId As Integer
            Dim objEmp As New clsEmployee_Master
            Dim objReG As New clsCommon_Master
            Dim objPrd As New clscommom_period_Tran
            Dim objObjective As clsObjective_Master

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                iEmpId = 0 : iYearunkid = 0 : iResultGrpId = 0 : iPeriodId = 0

                '>>>>>>>>>>>>>>>>>>>>> IF EMPLOYEE PERESET IN THE SYSTEM
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '>>>>>>>>>>>>>>>>>>>>> IF RESULT GROUP PERESET IN THE SYSTEM
                If dtRow.Item("rgroup").ToString.Trim.Length > 0 Then
                    iResultGrpId = objReG.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RESULT_GROUP, dtRow.Item("rgroup").ToString.Trim)
                    If iResultGrpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Result Group Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '>>>>>>>>>>>>>>>>>>>>> IF PERIOD IN THE SYSTEM
                If dtRow.Item("period").ToString.Trim.Length > 0 Then
                    iPeriodId = objPrd.GetPeriodByName(dtRow.Item("period").ToString.Trim, enModuleReference.Assessment)
                    If iPeriodId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Period Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPrd._Periodunkid = iPeriodId
                        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
                        'Sohail (21 Aug 2015) -- End
                        iYearunkid = objPrd._Yearunkid
                    End If
                End If
                '>>>>>>>>>>>>>>>>>>>>> IF VALID PERSPECTIVE IN THE FILE
                If dtRow.Item("perspectiveid").ToString.Trim.Length > 0 Then
                    If Val(dtRow.Item("perspectiveid").ToString.Trim) <= 0 OrElse Val(dtRow.Item("perspectiveid").ToString.Trim) > 5 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Invalid Perspective Id. Perspective Id should be in between (1 to 4)")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                Dim iObjectiveId As Integer = 0

                '>>>>>>>>>>>>>>>>>>>>> IF VALID WEIGHT IN THE FILE
                If cboWeight.Enabled = True Then
                    If dtRow.Item("weight").ToString.Trim.Length > 0 Then
                        If Val(dtRow.Item("weight").ToString.Trim) <= 0 Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Invalid Weigth. Weigth should be in between (1 to 100)")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'Else
                            '    Dim mDecTotal, mDecAssign As Decimal
                            '    mDecTotal = 0 : mDecAssign = 0
                            '    objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD1, iObjectiveId, mDecAssign, mDecTotal, iEmpId, iYearunkid)
                            '    If mDecTotal < mDecAssign + Val(dtRow.Item("weight").ToString.Trim) Then
                            '        dtRow.Item("image") = imgError
                            '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 22, "Invalid Weigth. Weigth exceeds the total of 100 for the particular employee.")
                            '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            '        dtRow.Item("objStatus") = 2
                            '        objError.Text = CStr(Val(objError.Text) + 1)
                            '        Continue For
                            '    End If
                        End If
                    End If
                End If

                objObjective = New clsObjective_Master

                iObjectiveId = objObjective.GetObjectiveUnkid("", dtRow.Item("oname").ToString.Trim, iPeriodId, iEmpId)

                If iObjectiveId <= 0 Then

                    objObjective._Code = dtRow.Item("ocode").ToString.Trim
                    objObjective._Description = ""
                    objObjective._EmployeeId = iEmpId
                    objObjective._Isactive = True
                    objObjective._Isfinal = False
                    objObjective._Name = dtRow.Item("oname").ToString.Trim
                    objObjective._Periodunkid = iPeriodId
                    objObjective._Perspectiveunkid = CInt(Val(dtRow.Item("perspectiveid").ToString.Trim))
                    objObjective._Referenceunkid = enAllocation.EMPLOYEE
                    objObjective._ResultGroupunkid = iResultGrpId
                    If cboWeight.Enabled = True Then
                        objObjective._Weight = CDec(dtRow.Item("weight"))
                    Else
                        objObjective._Weight = 0
                    End If
                    objObjective._Yearunkid = iYearunkid

                    If objObjective.Insert(User._Object._Userunkid) = True Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objObjective._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                Else
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Data Already Exists.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 19, "Warning")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            If objWSetting.Is_Setting_Present(-1) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.lblObjectiveName.Text = Language._Object.getCaption(Me.lblObjectiveName.Name, Me.lblObjectiveName.Text)
			Me.lblObjectiveCode.Text = Language._Object.getCaption(Me.lblObjectiveCode.Name, Me.lblObjectiveCode.Text)
			Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
			Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.Name, Me.lblValue.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select proper file to Import Data.")
			Language.setMessage(mstrModuleName, 2, "Please select proper field to Import Data.")
			Language.setMessage(mstrModuleName, 3, "Sorry, No weightage setting defined for Balance Score Card. Please define weightage setting.")
			Language.setMessage(mstrModuleName, 4, "Employee Code is mandatory information. Please select Employee Code to continue.")
			Language.setMessage(mstrModuleName, 5, "Perspective is mandatory information. Please select Perspective to continue.")
			Language.setMessage(mstrModuleName, 6, "Objective Code is mandatory information. Please select Objective Code to continue.")
			Language.setMessage(mstrModuleName, 7, "Objective Name is mandatory information. Please select Objective Name to continue.")
			Language.setMessage(mstrModuleName, 8, "Result Group is mandatory information. Please select Result Group to continue.")
			Language.setMessage(mstrModuleName, 9, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 10, "Weight is mandatory information. Please select Weight to continue.")
			Language.setMessage(mstrModuleName, 11, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 12, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 13, "Fail")
			Language.setMessage(mstrModuleName, 14, "Result Group Not Found.")
			Language.setMessage(mstrModuleName, 15, "Period Not Found.")
			Language.setMessage(mstrModuleName, 16, "Invalid Perspective Id. Perspective Id should be in between (1 to 4)")
			Language.setMessage(mstrModuleName, 17, "Invalid Weigth. Weigth should be in between (1 to 100)")
			Language.setMessage(mstrModuleName, 18, "Data Already Exists.")
			Language.setMessage(mstrModuleName, 19, "Warning")
			Language.setMessage(mstrModuleName, 20, "Success")
			Language.setMessage(mstrModuleName, 21, "Please make sure that weight should not exceed 100 for each employee in the file you are trying to import.")
			Language.setMessage(mstrModuleName, 22, "Invalid Weigth. Weigth exceeds the total of 100 for the particular employee.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class