﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSCSelfAssessmentList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSCSelfAssessmentList))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.chkShowUncommited = New System.Windows.Forms.CheckBox
        Me.chkShowCommited = New System.Windows.Forms.CheckBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpAssessmentdate = New System.Windows.Forms.DateTimePicker
        Me.lblAssessmentdate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblYears = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblAssessmentPeriods = New System.Windows.Forms.Label
        Me.lvSelfBSCList = New eZee.Common.eZeeListView(Me.components)
        Me.colhYear = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhAssessmentdate = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhPercent = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriodId = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnUnlockCommit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhYearId = New System.Windows.Forms.ColumnHeader
        Me.gbEmployeeInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(673, 58)
        Me.eZeeHeader.TabIndex = 116
        Me.eZeeHeader.Title = "Self Balance Score Card List"
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.lnkAllocation)
        Me.gbEmployeeInfo.Controls.Add(Me.chkShowUncommited)
        Me.gbEmployeeInfo.Controls.Add(Me.chkShowCommited)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeInfo.Controls.Add(Me.dtpAssessmentdate)
        Me.gbEmployeeInfo.Controls.Add(Me.lblAssessmentdate)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.cboYear)
        Me.gbEmployeeInfo.Controls.Add(Me.lblYears)
        Me.gbEmployeeInfo.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeInfo.Controls.Add(Me.lblAssessmentPeriods)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(9, 64)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(654, 90)
        Me.gbEmployeeInfo.TabIndex = 117
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Filter Criteria"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(512, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(85, 17)
        Me.lnkAllocation.TabIndex = 243
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'chkShowUncommited
        '
        Me.chkShowUncommited.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkShowUncommited.Location = New System.Drawing.Point(292, 62)
        Me.chkShowUncommited.Name = "chkShowUncommited"
        Me.chkShowUncommited.Size = New System.Drawing.Size(125, 17)
        Me.chkShowUncommited.TabIndex = 240
        Me.chkShowUncommited.Text = "Show Uncommited"
        Me.chkShowUncommited.UseVisualStyleBackColor = True
        '
        'chkShowCommited
        '
        Me.chkShowCommited.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkShowCommited.Location = New System.Drawing.Point(181, 62)
        Me.chkShowCommited.Name = "chkShowCommited"
        Me.chkShowCommited.Size = New System.Drawing.Size(105, 17)
        Me.chkShowCommited.TabIndex = 239
        Me.chkShowCommited.Text = "Show Commited"
        Me.chkShowCommited.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(627, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(603, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'dtpAssessmentdate
        '
        Me.dtpAssessmentdate.Checked = False
        Me.dtpAssessmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssessmentdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssessmentdate.Location = New System.Drawing.Point(79, 60)
        Me.dtpAssessmentdate.Name = "dtpAssessmentdate"
        Me.dtpAssessmentdate.ShowCheckBox = True
        Me.dtpAssessmentdate.Size = New System.Drawing.Size(96, 21)
        Me.dtpAssessmentdate.TabIndex = 232
        '
        'lblAssessmentdate
        '
        Me.lblAssessmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentdate.Location = New System.Drawing.Point(8, 63)
        Me.lblAssessmentdate.Name = "lblAssessmentdate"
        Me.lblAssessmentdate.Size = New System.Drawing.Size(65, 15)
        Me.lblAssessmentdate.TabIndex = 231
        Me.lblAssessmentdate.Text = "Date"
        Me.lblAssessmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(423, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 215
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(79, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(338, 21)
        Me.cboEmployee.TabIndex = 229
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(65, 15)
        Me.lblEmployee.TabIndex = 227
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(520, 33)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(121, 21)
        Me.cboYear.TabIndex = 7
        '
        'lblYears
        '
        Me.lblYears.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYears.Location = New System.Drawing.Point(450, 36)
        Me.lblYears.Name = "lblYears"
        Me.lblYears.Size = New System.Drawing.Size(64, 15)
        Me.lblYears.TabIndex = 225
        Me.lblYears.Text = "Year"
        Me.lblYears.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(520, 60)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'lblAssessmentPeriods
        '
        Me.lblAssessmentPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentPeriods.Location = New System.Drawing.Point(450, 63)
        Me.lblAssessmentPeriods.Name = "lblAssessmentPeriods"
        Me.lblAssessmentPeriods.Size = New System.Drawing.Size(64, 15)
        Me.lblAssessmentPeriods.TabIndex = 223
        Me.lblAssessmentPeriods.Text = "Period"
        Me.lblAssessmentPeriods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvSelfBSCList
        '
        Me.lvSelfBSCList.BackColorOnChecked = False
        Me.lvSelfBSCList.ColumnHeaders = Nothing
        Me.lvSelfBSCList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhYear, Me.colhPeriod, Me.colhAssessmentdate, Me.colhEmployee, Me.colhPercent, Me.objcolhPeriodId, Me.objcolhEmpId, Me.objcolhYearId})
        Me.lvSelfBSCList.CompulsoryColumns = ""
        Me.lvSelfBSCList.FullRowSelect = True
        Me.lvSelfBSCList.GridLines = True
        Me.lvSelfBSCList.GroupingColumn = Nothing
        Me.lvSelfBSCList.HideSelection = False
        Me.lvSelfBSCList.Location = New System.Drawing.Point(9, 160)
        Me.lvSelfBSCList.MinColumnWidth = 50
        Me.lvSelfBSCList.MultiSelect = False
        Me.lvSelfBSCList.Name = "lvSelfBSCList"
        Me.lvSelfBSCList.OptionalColumns = ""
        Me.lvSelfBSCList.ShowMoreItem = False
        Me.lvSelfBSCList.ShowSaveItem = False
        Me.lvSelfBSCList.ShowSelectAll = True
        Me.lvSelfBSCList.ShowSizeAllColumnsToFit = True
        Me.lvSelfBSCList.Size = New System.Drawing.Size(654, 191)
        Me.lvSelfBSCList.Sortable = True
        Me.lvSelfBSCList.TabIndex = 118
        Me.lvSelfBSCList.UseCompatibleStateImageBehavior = False
        Me.lvSelfBSCList.View = System.Windows.Forms.View.Details
        '
        'colhYear
        '
        Me.colhYear.Tag = "colhYear"
        Me.colhYear.Text = "Year"
        Me.colhYear.Width = 210
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 210
        '
        'colhAssessmentdate
        '
        Me.colhAssessmentdate.Tag = "colhAssessmentdate"
        Me.colhAssessmentdate.Text = "Assessment Date"
        Me.colhAssessmentdate.Width = 130
        '
        'colhEmployee
        '
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhPercent
        '
        Me.colhPercent.Tag = "colhPercent"
        Me.colhPercent.Text = "Score"
        Me.colhPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhPercent.Width = 98
        '
        'objcolhPeriodId
        '
        Me.objcolhPeriodId.Tag = "objcolhPeriodId"
        Me.objcolhPeriodId.Text = ""
        Me.objcolhPeriodId.Width = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnUnlockCommit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 357)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(673, 55)
        Me.objFooter.TabIndex = 119
        '
        'btnUnlockCommit
        '
        Me.btnUnlockCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnlockCommit.BackColor = System.Drawing.Color.White
        Me.btnUnlockCommit.BackgroundImage = CType(resources.GetObject("btnUnlockCommit.BackgroundImage"), System.Drawing.Image)
        Me.btnUnlockCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnlockCommit.BorderColor = System.Drawing.Color.Empty
        Me.btnUnlockCommit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnlockCommit.FlatAppearance.BorderSize = 0
        Me.btnUnlockCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnlockCommit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnlockCommit.ForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnlockCommit.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlockCommit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.Location = New System.Drawing.Point(9, 13)
        Me.btnUnlockCommit.Name = "btnUnlockCommit"
        Me.btnUnlockCommit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlockCommit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.Size = New System.Drawing.Size(118, 30)
        Me.btnUnlockCommit.TabIndex = 79
        Me.btnUnlockCommit.Text = "&Unlock Commited"
        Me.btnUnlockCommit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(278, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(472, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(375, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 71
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(568, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.Text = "objcolhEmpId"
        Me.objcolhEmpId.Width = 0
        '
        'objcolhYearId
        '
        Me.objcolhYearId.Text = "objcolhYearId"
        Me.objcolhYearId.Width = 0
        '
        'frmBSCSelfAssessmentList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(673, 412)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.lvSelfBSCList)
        Me.Controls.Add(Me.gbEmployeeInfo)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSCSelfAssessmentList"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Self Balanced Score Card List"
        Me.gbEmployeeInfo.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkShowUncommited As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCommited As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dtpAssessmentdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAssessmentdate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYears As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessmentPeriods As System.Windows.Forms.Label
    Friend WithEvents lvSelfBSCList As eZee.Common.eZeeListView
    Friend WithEvents colhYear As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAssessmentdate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents colhPercent As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnUnlockCommit As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhPeriodId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhYearId As System.Windows.Forms.ColumnHeader
End Class
