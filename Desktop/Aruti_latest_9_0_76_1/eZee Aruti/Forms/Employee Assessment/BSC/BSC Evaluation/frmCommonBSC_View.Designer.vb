﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonBSC_View
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonBSC_View))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objlblColon = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.lblComment = New System.Windows.Forms.Label
        Me.pnlResultInfo = New System.Windows.Forms.Panel
        Me.lvAssessments = New eZee.Common.eZeeListView(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnFinalSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOpen_Close = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.pnlResultInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.SplitContainer1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(867, 490)
        Me.pnlMain.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.objlblCaption)
        Me.SplitContainer1.Panel1.Controls.Add(Me.objlblColon)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblEmployee)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtComments)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblComment)
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlResultInfo)
        Me.SplitContainer1.Size = New System.Drawing.Size(867, 435)
        Me.SplitContainer1.SplitterDistance = 34
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 15
        '
        'objlblCaption
        '
        Me.objlblCaption.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblCaption.Location = New System.Drawing.Point(144, 8)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(327, 15)
        Me.objlblCaption.TabIndex = 2
        Me.objlblCaption.Text = "##Value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblColon
        '
        Me.objlblColon.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblColon.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColon.Location = New System.Drawing.Point(128, 9)
        Me.objlblColon.Name = "objlblColon"
        Me.objlblColon.Size = New System.Drawing.Size(10, 13)
        Me.objlblColon.TabIndex = 1
        Me.objlblColon.Text = ":"
        Me.objlblColon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblEmployee.Location = New System.Drawing.Point(12, 8)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(110, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(97, 334)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtComments.Size = New System.Drawing.Size(763, 57)
        Me.txtComments.TabIndex = 17
        '
        'lblComment
        '
        Me.lblComment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblComment.Location = New System.Drawing.Point(10, 338)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(81, 16)
        Me.lblComment.TabIndex = 16
        Me.lblComment.Text = "Comments"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlResultInfo
        '
        Me.pnlResultInfo.Controls.Add(Me.lvAssessments)
        Me.pnlResultInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlResultInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlResultInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlResultInfo.Name = "pnlResultInfo"
        Me.pnlResultInfo.Size = New System.Drawing.Size(863, 327)
        Me.pnlResultInfo.TabIndex = 15
        '
        'lvAssessments
        '
        Me.lvAssessments.BackColorOnChecked = False
        Me.lvAssessments.ColumnHeaders = Nothing
        Me.lvAssessments.CompulsoryColumns = ""
        Me.lvAssessments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAssessments.FullRowSelect = True
        Me.lvAssessments.GridLines = True
        Me.lvAssessments.GroupingColumn = Nothing
        Me.lvAssessments.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAssessments.HideSelection = False
        Me.lvAssessments.Location = New System.Drawing.Point(0, 0)
        Me.lvAssessments.MinColumnWidth = 50
        Me.lvAssessments.MultiSelect = False
        Me.lvAssessments.Name = "lvAssessments"
        Me.lvAssessments.OptionalColumns = ""
        Me.lvAssessments.ShowMoreItem = False
        Me.lvAssessments.ShowSaveItem = False
        Me.lvAssessments.ShowSelectAll = True
        Me.lvAssessments.ShowSizeAllColumnsToFit = True
        Me.lvAssessments.Size = New System.Drawing.Size(863, 327)
        Me.lvAssessments.Sortable = True
        Me.lvAssessments.TabIndex = 14
        Me.lvAssessments.UseCompatibleStateImageBehavior = False
        Me.lvAssessments.View = System.Windows.Forms.View.Details
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnFinalSave)
        Me.objFooter.Controls.Add(Me.btnOpen_Close)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 435)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(867, 55)
        Me.objFooter.TabIndex = 13
        '
        'btnFinalSave
        '
        Me.btnFinalSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinalSave.BackColor = System.Drawing.Color.White
        Me.btnFinalSave.BackgroundImage = CType(resources.GetObject("btnFinalSave.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalSave.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalSave.FlatAppearance.BorderSize = 0
        Me.btnFinalSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalSave.ForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Location = New System.Drawing.Point(552, 13)
        Me.btnFinalSave.Name = "btnFinalSave"
        Me.btnFinalSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Size = New System.Drawing.Size(97, 30)
        Me.btnFinalSave.TabIndex = 9
        Me.btnFinalSave.Text = "&Final Save"
        Me.btnFinalSave.UseVisualStyleBackColor = True
        '
        'btnOpen_Close
        '
        Me.btnOpen_Close.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOpen_Close.BackColor = System.Drawing.Color.White
        Me.btnOpen_Close.BackgroundImage = CType(resources.GetObject("btnOpen_Close.BackgroundImage"), System.Drawing.Image)
        Me.btnOpen_Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpen_Close.BorderColor = System.Drawing.Color.Empty
        Me.btnOpen_Close.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpen_Close.FlatAppearance.BorderSize = 0
        Me.btnOpen_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpen_Close.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen_Close.ForeColor = System.Drawing.Color.Black
        Me.btnOpen_Close.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpen_Close.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpen_Close.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen_Close.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen_Close.Location = New System.Drawing.Point(655, 13)
        Me.btnOpen_Close.Name = "btnOpen_Close"
        Me.btnOpen_Close.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen_Close.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen_Close.Size = New System.Drawing.Size(97, 30)
        Me.btnOpen_Close.TabIndex = 8
        Me.btnOpen_Close.Text = "&Open Changes"
        Me.btnOpen_Close.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(758, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCommonBSC_View
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(867, 490)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonBSC_View"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessments"
        Me.pnlMain.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.pnlResultInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvAssessments As eZee.Common.eZeeListView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objlblColon As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents pnlResultInfo As System.Windows.Forms.Panel
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents btnFinalSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnOpen_Close As eZee.Common.eZeeLightButton
End Class
