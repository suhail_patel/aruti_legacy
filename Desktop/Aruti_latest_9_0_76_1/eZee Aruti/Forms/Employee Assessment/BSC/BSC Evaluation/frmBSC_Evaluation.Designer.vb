﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_Evaluation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_Evaluation))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbBSCEvaluation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnHide = New eZee.Common.eZeeGradientButton
        Me.radExternalAssessor = New System.Windows.Forms.RadioButton
        Me.radInternalAssessor = New System.Windows.Forms.RadioButton
        Me.spc1 = New System.Windows.Forms.SplitContainer
        Me.lblAssessor = New System.Windows.Forms.Label
        Me.lblReviewer = New System.Windows.Forms.Label
        Me.pnlContainer = New System.Windows.Forms.Panel
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchInitiative = New eZee.Common.eZeeGradientButton
        Me.lblObjective = New System.Windows.Forms.Label
        Me.btnDeleteEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboInitiative = New System.Windows.Forms.ComboBox
        Me.btnEditEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboObjective = New System.Windows.Forms.ComboBox
        Me.lblInitiative = New System.Windows.Forms.Label
        Me.objbtnSearchObjective = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchTraget = New eZee.Common.eZeeGradientButton
        Me.lblResult = New System.Windows.Forms.Label
        Me.cboTraget = New System.Windows.Forms.ComboBox
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.lblTarget = New System.Windows.Forms.Label
        Me.objbtnSearchResult = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchKPI = New eZee.Common.eZeeGradientButton
        Me.txtRemark = New System.Windows.Forms.RichTextBox
        Me.cboKPI = New System.Windows.Forms.ComboBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblKPI = New System.Windows.Forms.Label
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.lblAssessDate = New System.Windows.Forms.Label
        Me.lblWeight = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboPerspective = New System.Windows.Forms.ComboBox
        Me.txtWeightage = New System.Windows.Forms.TextBox
        Me.dtpAssessdate = New System.Windows.Forms.DateTimePicker
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lnkBSCPlanning = New System.Windows.Forms.LinkLabel
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboReviewer = New System.Windows.Forms.ComboBox
        Me.cboAssessor = New System.Windows.Forms.ComboBox
        Me.objbtnSearchReviewer = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchAssessor = New eZee.Common.eZeeGradientButton
        Me.pnlEvaluationList = New System.Windows.Forms.Panel
        Me.lvEvaluation = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhPerspective = New System.Windows.Forms.ColumnHeader
        Me.colhObjective = New System.Windows.Forms.ColumnHeader
        Me.colhKPI = New System.Windows.Forms.ColumnHeader
        Me.colhTargets = New System.Windows.Forms.ColumnHeader
        Me.colhInitiative = New System.Windows.Forms.ColumnHeader
        Me.colhResult = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhobjectiveId = New System.Windows.Forms.ColumnHeader
        Me.objcolhKpiId = New System.Windows.Forms.ColumnHeader
        Me.objcolhTargetId = New System.Windows.Forms.ColumnHeader
        Me.objcolhInitiativeId = New System.Windows.Forms.ColumnHeader
        Me.objcolhResultId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsGrp = New System.Windows.Forms.ColumnHeader
        Me.objcolhSortId = New System.Windows.Forms.ColumnHeader
        Me.objbtnShow = New eZee.Common.eZeeGradientButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkViewBSC = New System.Windows.Forms.LinkLabel
        Me.btnSaveCommit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbBSCEvaluation.SuspendLayout()
        Me.spc1.Panel1.SuspendLayout()
        Me.spc1.Panel2.SuspendLayout()
        Me.spc1.SuspendLayout()
        Me.pnlContainer.SuspendLayout()
        Me.pnlEvaluationList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbBSCEvaluation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(947, 509)
        Me.pnlMain.TabIndex = 0
        '
        'gbBSCEvaluation
        '
        Me.gbBSCEvaluation.BorderColor = System.Drawing.Color.Black
        Me.gbBSCEvaluation.Checked = False
        Me.gbBSCEvaluation.CollapseAllExceptThis = False
        Me.gbBSCEvaluation.CollapsedHoverImage = Nothing
        Me.gbBSCEvaluation.CollapsedNormalImage = Nothing
        Me.gbBSCEvaluation.CollapsedPressedImage = Nothing
        Me.gbBSCEvaluation.CollapseOnLoad = False
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnHide)
        Me.gbBSCEvaluation.Controls.Add(Me.radExternalAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.radInternalAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.spc1)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnShow)
        Me.gbBSCEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBSCEvaluation.ExpandedHoverImage = Nothing
        Me.gbBSCEvaluation.ExpandedNormalImage = Nothing
        Me.gbBSCEvaluation.ExpandedPressedImage = Nothing
        Me.gbBSCEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBSCEvaluation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBSCEvaluation.HeaderHeight = 25
        Me.gbBSCEvaluation.HeaderMessage = ""
        Me.gbBSCEvaluation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBSCEvaluation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBSCEvaluation.HeightOnCollapse = 0
        Me.gbBSCEvaluation.LeftTextSpace = 0
        Me.gbBSCEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.gbBSCEvaluation.Name = "gbBSCEvaluation"
        Me.gbBSCEvaluation.OpenHeight = 300
        Me.gbBSCEvaluation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBSCEvaluation.ShowBorder = True
        Me.gbBSCEvaluation.ShowCheckBox = False
        Me.gbBSCEvaluation.ShowCollapseButton = False
        Me.gbBSCEvaluation.ShowDefaultBorderColor = True
        Me.gbBSCEvaluation.ShowDownButton = False
        Me.gbBSCEvaluation.ShowHeader = True
        Me.gbBSCEvaluation.Size = New System.Drawing.Size(947, 454)
        Me.gbBSCEvaluation.TabIndex = 1
        Me.gbBSCEvaluation.Temp = 0
        Me.gbBSCEvaluation.Text = "Evaluation Info"
        Me.gbBSCEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnHide
        '
        Me.objbtnHide.BackColor = System.Drawing.Color.Transparent
        Me.objbtnHide.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnHide.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnHide.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnHide.BorderSelected = False
        Me.objbtnHide.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnHide.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_16
        Me.objbtnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnHide.Location = New System.Drawing.Point(922, 2)
        Me.objbtnHide.Name = "objbtnHide"
        Me.objbtnHide.Size = New System.Drawing.Size(21, 21)
        Me.objbtnHide.TabIndex = 389
        '
        'radExternalAssessor
        '
        Me.radExternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radExternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExternalAssessor.Location = New System.Drawing.Point(789, 5)
        Me.radExternalAssessor.Name = "radExternalAssessor"
        Me.radExternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radExternalAssessor.TabIndex = 378
        Me.radExternalAssessor.TabStop = True
        Me.radExternalAssessor.Text = "External Assessor"
        Me.radExternalAssessor.UseVisualStyleBackColor = False
        '
        'radInternalAssessor
        '
        Me.radInternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radInternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radInternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInternalAssessor.Location = New System.Drawing.Point(656, 5)
        Me.radInternalAssessor.Name = "radInternalAssessor"
        Me.radInternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radInternalAssessor.TabIndex = 378
        Me.radInternalAssessor.TabStop = True
        Me.radInternalAssessor.Text = "Internal Assessor"
        Me.radInternalAssessor.UseVisualStyleBackColor = False
        '
        'spc1
        '
        Me.spc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.spc1.Location = New System.Drawing.Point(2, 26)
        Me.spc1.Name = "spc1"
        '
        'spc1.Panel1
        '
        Me.spc1.Panel1.Controls.Add(Me.lblAssessor)
        Me.spc1.Panel1.Controls.Add(Me.lblReviewer)
        Me.spc1.Panel1.Controls.Add(Me.pnlContainer)
        Me.spc1.Panel1.Controls.Add(Me.cboReviewer)
        Me.spc1.Panel1.Controls.Add(Me.cboAssessor)
        Me.spc1.Panel1.Controls.Add(Me.objbtnSearchReviewer)
        Me.spc1.Panel1.Controls.Add(Me.objbtnSearchAssessor)
        '
        'spc1.Panel2
        '
        Me.spc1.Panel2.Controls.Add(Me.pnlEvaluationList)
        Me.spc1.Size = New System.Drawing.Size(941, 426)
        Me.spc1.SplitterDistance = 332
        Me.spc1.SplitterWidth = 1
        Me.spc1.TabIndex = 387
        '
        'lblAssessor
        '
        Me.lblAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessor.Location = New System.Drawing.Point(16, 14)
        Me.lblAssessor.Name = "lblAssessor"
        Me.lblAssessor.Size = New System.Drawing.Size(62, 15)
        Me.lblAssessor.TabIndex = 343
        Me.lblAssessor.Text = "Assessor"
        Me.lblAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReviewer
        '
        Me.lblReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReviewer.Location = New System.Drawing.Point(16, 14)
        Me.lblReviewer.Name = "lblReviewer"
        Me.lblReviewer.Size = New System.Drawing.Size(62, 15)
        Me.lblReviewer.TabIndex = 347
        Me.lblReviewer.Text = "Reviewer"
        Me.lblReviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlContainer
        '
        Me.pnlContainer.Controls.Add(Me.cboEmployee)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchInitiative)
        Me.pnlContainer.Controls.Add(Me.lblObjective)
        Me.pnlContainer.Controls.Add(Me.btnDeleteEvaluation)
        Me.pnlContainer.Controls.Add(Me.btnAddEvaluation)
        Me.pnlContainer.Controls.Add(Me.cboInitiative)
        Me.pnlContainer.Controls.Add(Me.btnEditEvaluation)
        Me.pnlContainer.Controls.Add(Me.cboObjective)
        Me.pnlContainer.Controls.Add(Me.lblInitiative)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchObjective)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchTraget)
        Me.pnlContainer.Controls.Add(Me.lblResult)
        Me.pnlContainer.Controls.Add(Me.cboTraget)
        Me.pnlContainer.Controls.Add(Me.cboResult)
        Me.pnlContainer.Controls.Add(Me.lblTarget)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchResult)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchKPI)
        Me.pnlContainer.Controls.Add(Me.txtRemark)
        Me.pnlContainer.Controls.Add(Me.cboKPI)
        Me.pnlContainer.Controls.Add(Me.lblRemark)
        Me.pnlContainer.Controls.Add(Me.lblKPI)
        Me.pnlContainer.Controls.Add(Me.lblPerspective)
        Me.pnlContainer.Controls.Add(Me.lblAssessDate)
        Me.pnlContainer.Controls.Add(Me.lblWeight)
        Me.pnlContainer.Controls.Add(Me.lblEmployee)
        Me.pnlContainer.Controls.Add(Me.cboPerspective)
        Me.pnlContainer.Controls.Add(Me.txtWeightage)
        Me.pnlContainer.Controls.Add(Me.dtpAssessdate)
        Me.pnlContainer.Controls.Add(Me.cboPeriod)
        Me.pnlContainer.Controls.Add(Me.lnkBSCPlanning)
        Me.pnlContainer.Controls.Add(Me.lblYear)
        Me.pnlContainer.Controls.Add(Me.cboYear)
        Me.pnlContainer.Controls.Add(Me.lblPeriod)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlContainer.Location = New System.Drawing.Point(11, 35)
        Me.pnlContainer.Name = "pnlContainer"
        Me.pnlContainer.Size = New System.Drawing.Size(318, 365)
        Me.pnlContainer.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 250
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(73, 3)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(216, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'objbtnSearchInitiative
        '
        Me.objbtnSearchInitiative.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInitiative.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInitiative.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInitiative.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInitiative.BorderSelected = False
        Me.objbtnSearchInitiative.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInitiative.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInitiative.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInitiative.Location = New System.Drawing.Point(295, 192)
        Me.objbtnSearchInitiative.Name = "objbtnSearchInitiative"
        Me.objbtnSearchInitiative.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInitiative.TabIndex = 395
        '
        'lblObjective
        '
        Me.lblObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObjective.Location = New System.Drawing.Point(5, 114)
        Me.lblObjective.Name = "lblObjective"
        Me.lblObjective.Size = New System.Drawing.Size(62, 15)
        Me.lblObjective.TabIndex = 351
        Me.lblObjective.Text = "Objective"
        '
        'btnDeleteEvaluation
        '
        Me.btnDeleteEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteEvaluation.BackColor = System.Drawing.Color.White
        Me.btnDeleteEvaluation.BackgroundImage = CType(resources.GetObject("btnDeleteEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteEvaluation.FlatAppearance.BorderSize = 0
        Me.btnDeleteEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.Location = New System.Drawing.Point(210, 329)
        Me.btnDeleteEvaluation.Name = "btnDeleteEvaluation"
        Me.btnDeleteEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteEvaluation.Size = New System.Drawing.Size(79, 30)
        Me.btnDeleteEvaluation.TabIndex = 372
        Me.btnDeleteEvaluation.Text = "&Delete"
        Me.btnDeleteEvaluation.UseVisualStyleBackColor = True
        '
        'btnAddEvaluation
        '
        Me.btnAddEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddEvaluation.BackColor = System.Drawing.Color.White
        Me.btnAddEvaluation.BackgroundImage = CType(resources.GetObject("btnAddEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnAddEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnAddEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddEvaluation.FlatAppearance.BorderSize = 0
        Me.btnAddEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.Location = New System.Drawing.Point(40, 329)
        Me.btnAddEvaluation.Name = "btnAddEvaluation"
        Me.btnAddEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddEvaluation.Size = New System.Drawing.Size(79, 30)
        Me.btnAddEvaluation.TabIndex = 370
        Me.btnAddEvaluation.Text = "&Add"
        Me.btnAddEvaluation.UseVisualStyleBackColor = True
        '
        'cboInitiative
        '
        Me.cboInitiative.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInitiative.DropDownWidth = 400
        Me.cboInitiative.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInitiative.FormattingEnabled = True
        Me.cboInitiative.Location = New System.Drawing.Point(73, 192)
        Me.cboInitiative.Name = "cboInitiative"
        Me.cboInitiative.Size = New System.Drawing.Size(216, 21)
        Me.cboInitiative.TabIndex = 394
        '
        'btnEditEvaluation
        '
        Me.btnEditEvaluation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditEvaluation.BackColor = System.Drawing.Color.White
        Me.btnEditEvaluation.BackgroundImage = CType(resources.GetObject("btnEditEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnEditEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnEditEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditEvaluation.FlatAppearance.BorderSize = 0
        Me.btnEditEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.Location = New System.Drawing.Point(125, 329)
        Me.btnEditEvaluation.Name = "btnEditEvaluation"
        Me.btnEditEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditEvaluation.Size = New System.Drawing.Size(79, 30)
        Me.btnEditEvaluation.TabIndex = 371
        Me.btnEditEvaluation.Text = "&Edit"
        Me.btnEditEvaluation.UseVisualStyleBackColor = True
        '
        'cboObjective
        '
        Me.cboObjective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObjective.DropDownWidth = 400
        Me.cboObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObjective.FormattingEnabled = True
        Me.cboObjective.Location = New System.Drawing.Point(73, 111)
        Me.cboObjective.Name = "cboObjective"
        Me.cboObjective.Size = New System.Drawing.Size(216, 21)
        Me.cboObjective.TabIndex = 352
        '
        'lblInitiative
        '
        Me.lblInitiative.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitiative.Location = New System.Drawing.Point(5, 195)
        Me.lblInitiative.Name = "lblInitiative"
        Me.lblInitiative.Size = New System.Drawing.Size(62, 15)
        Me.lblInitiative.TabIndex = 393
        Me.lblInitiative.Text = "Initiative"
        '
        'objbtnSearchObjective
        '
        Me.objbtnSearchObjective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchObjective.BorderSelected = False
        Me.objbtnSearchObjective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchObjective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchObjective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchObjective.Location = New System.Drawing.Point(295, 111)
        Me.objbtnSearchObjective.Name = "objbtnSearchObjective"
        Me.objbtnSearchObjective.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchObjective.TabIndex = 353
        '
        'objbtnSearchTraget
        '
        Me.objbtnSearchTraget.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTraget.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraget.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraget.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTraget.BorderSelected = False
        Me.objbtnSearchTraget.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTraget.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTraget.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTraget.Location = New System.Drawing.Point(295, 165)
        Me.objbtnSearchTraget.Name = "objbtnSearchTraget"
        Me.objbtnSearchTraget.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTraget.TabIndex = 392
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(5, 249)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(62, 15)
        Me.lblResult.TabIndex = 364
        Me.lblResult.Text = "Result"
        '
        'cboTraget
        '
        Me.cboTraget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTraget.DropDownWidth = 400
        Me.cboTraget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTraget.FormattingEnabled = True
        Me.cboTraget.Location = New System.Drawing.Point(73, 165)
        Me.cboTraget.Name = "cboTraget"
        Me.cboTraget.Size = New System.Drawing.Size(216, 21)
        Me.cboTraget.TabIndex = 391
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.DropDownWidth = 200
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(73, 246)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(216, 21)
        Me.cboResult.TabIndex = 365
        '
        'lblTarget
        '
        Me.lblTarget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTarget.Location = New System.Drawing.Point(5, 168)
        Me.lblTarget.Name = "lblTarget"
        Me.lblTarget.Size = New System.Drawing.Size(62, 15)
        Me.lblTarget.TabIndex = 390
        Me.lblTarget.Text = "Target"
        '
        'objbtnSearchResult
        '
        Me.objbtnSearchResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResult.BorderSelected = False
        Me.objbtnSearchResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResult.Location = New System.Drawing.Point(295, 246)
        Me.objbtnSearchResult.Name = "objbtnSearchResult"
        Me.objbtnSearchResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResult.TabIndex = 366
        '
        'objbtnSearchKPI
        '
        Me.objbtnSearchKPI.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchKPI.BorderSelected = False
        Me.objbtnSearchKPI.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchKPI.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchKPI.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchKPI.Location = New System.Drawing.Point(295, 138)
        Me.objbtnSearchKPI.Name = "objbtnSearchKPI"
        Me.objbtnSearchKPI.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchKPI.TabIndex = 389
        '
        'txtRemark
        '
        Me.txtRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(73, 273)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.txtRemark.Size = New System.Drawing.Size(216, 50)
        Me.txtRemark.TabIndex = 367
        Me.txtRemark.Text = ""
        '
        'cboKPI
        '
        Me.cboKPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKPI.DropDownWidth = 400
        Me.cboKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboKPI.FormattingEnabled = True
        Me.cboKPI.Location = New System.Drawing.Point(73, 138)
        Me.cboKPI.Name = "cboKPI"
        Me.cboKPI.Size = New System.Drawing.Size(216, 21)
        Me.cboKPI.TabIndex = 388
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(5, 276)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(62, 15)
        Me.lblRemark.TabIndex = 368
        Me.lblRemark.Text = "Remark"
        '
        'lblKPI
        '
        Me.lblKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKPI.Location = New System.Drawing.Point(5, 141)
        Me.lblKPI.Name = "lblKPI"
        Me.lblKPI.Size = New System.Drawing.Size(62, 15)
        Me.lblKPI.TabIndex = 387
        Me.lblKPI.Text = "KPI"
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(5, 87)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(62, 15)
        Me.lblPerspective.TabIndex = 375
        Me.lblPerspective.Text = "Perspective"
        '
        'lblAssessDate
        '
        Me.lblAssessDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessDate.Location = New System.Drawing.Point(161, 33)
        Me.lblAssessDate.Name = "lblAssessDate"
        Me.lblAssessDate.Size = New System.Drawing.Size(38, 15)
        Me.lblAssessDate.TabIndex = 5
        Me.lblAssessDate.Text = "Date"
        Me.lblAssessDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(5, 222)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(62, 15)
        Me.lblWeight.TabIndex = 1
        Me.lblWeight.Text = "Weightage"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(5, 6)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(62, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPerspective
        '
        Me.cboPerspective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective.DropDownWidth = 300
        Me.cboPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective.FormattingEnabled = True
        Me.cboPerspective.Location = New System.Drawing.Point(73, 84)
        Me.cboPerspective.Name = "cboPerspective"
        Me.cboPerspective.Size = New System.Drawing.Size(216, 21)
        Me.cboPerspective.TabIndex = 376
        '
        'txtWeightage
        '
        Me.txtWeightage.BackColor = System.Drawing.Color.White
        Me.txtWeightage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeightage.Location = New System.Drawing.Point(73, 219)
        Me.txtWeightage.Name = "txtWeightage"
        Me.txtWeightage.ReadOnly = True
        Me.txtWeightage.Size = New System.Drawing.Size(59, 21)
        Me.txtWeightage.TabIndex = 380
        Me.txtWeightage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtpAssessdate
        '
        Me.dtpAssessdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssessdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssessdate.Location = New System.Drawing.Point(205, 30)
        Me.dtpAssessdate.Name = "dtpAssessdate"
        Me.dtpAssessdate.Size = New System.Drawing.Size(84, 21)
        Me.dtpAssessdate.TabIndex = 6
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(73, 57)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(216, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'lnkBSCPlanning
        '
        Me.lnkBSCPlanning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkBSCPlanning.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkBSCPlanning.Location = New System.Drawing.Point(140, 220)
        Me.lnkBSCPlanning.Name = "lnkBSCPlanning"
        Me.lnkBSCPlanning.Size = New System.Drawing.Size(146, 17)
        Me.lnkBSCPlanning.TabIndex = 382
        Me.lnkBSCPlanning.TabStop = True
        Me.lnkBSCPlanning.Text = "View BSC Planning "
        Me.lnkBSCPlanning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(5, 33)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(62, 15)
        Me.lblYear.TabIndex = 3
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 150
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(73, 30)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(82, 21)
        Me.cboYear.TabIndex = 4
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(5, 60)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(62, 15)
        Me.lblPeriod.TabIndex = 7
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(295, 4)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboReviewer
        '
        Me.cboReviewer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReviewer.FormattingEnabled = True
        Me.cboReviewer.Location = New System.Drawing.Point(84, 11)
        Me.cboReviewer.Name = "cboReviewer"
        Me.cboReviewer.Size = New System.Drawing.Size(216, 21)
        Me.cboReviewer.TabIndex = 384
        '
        'cboAssessor
        '
        Me.cboAssessor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessor.FormattingEnabled = True
        Me.cboAssessor.Location = New System.Drawing.Point(84, 11)
        Me.cboAssessor.Name = "cboAssessor"
        Me.cboAssessor.Size = New System.Drawing.Size(216, 21)
        Me.cboAssessor.TabIndex = 344
        '
        'objbtnSearchReviewer
        '
        Me.objbtnSearchReviewer.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReviewer.BorderSelected = False
        Me.objbtnSearchReviewer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReviewer.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReviewer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReviewer.Location = New System.Drawing.Point(306, 11)
        Me.objbtnSearchReviewer.Name = "objbtnSearchReviewer"
        Me.objbtnSearchReviewer.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReviewer.TabIndex = 385
        '
        'objbtnSearchAssessor
        '
        Me.objbtnSearchAssessor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAssessor.BorderSelected = False
        Me.objbtnSearchAssessor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAssessor.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAssessor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAssessor.Location = New System.Drawing.Point(306, 11)
        Me.objbtnSearchAssessor.Name = "objbtnSearchAssessor"
        Me.objbtnSearchAssessor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAssessor.TabIndex = 345
        '
        'pnlEvaluationList
        '
        Me.pnlEvaluationList.Controls.Add(Me.lvEvaluation)
        Me.pnlEvaluationList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEvaluationList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEvaluationList.Location = New System.Drawing.Point(0, 0)
        Me.pnlEvaluationList.Name = "pnlEvaluationList"
        Me.pnlEvaluationList.Size = New System.Drawing.Size(608, 426)
        Me.pnlEvaluationList.TabIndex = 374
        '
        'lvEvaluation
        '
        Me.lvEvaluation.BackColorOnChecked = False
        Me.lvEvaluation.ColumnHeaders = Nothing
        Me.lvEvaluation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhPerspective, Me.colhObjective, Me.colhKPI, Me.colhTargets, Me.colhInitiative, Me.colhResult, Me.colhRemark, Me.objcolhobjectiveId, Me.objcolhKpiId, Me.objcolhTargetId, Me.objcolhInitiativeId, Me.objcolhResultId, Me.objcolhGUID, Me.objcolhIsGrp, Me.objcolhSortId})
        Me.lvEvaluation.CompulsoryColumns = ""
        Me.lvEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEvaluation.FullRowSelect = True
        Me.lvEvaluation.GridLines = True
        Me.lvEvaluation.GroupingColumn = Nothing
        Me.lvEvaluation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEvaluation.HideSelection = False
        Me.lvEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.lvEvaluation.MinColumnWidth = 50
        Me.lvEvaluation.MultiSelect = False
        Me.lvEvaluation.Name = "lvEvaluation"
        Me.lvEvaluation.OptionalColumns = ""
        Me.lvEvaluation.ShowMoreItem = False
        Me.lvEvaluation.ShowSaveItem = False
        Me.lvEvaluation.ShowSelectAll = False
        Me.lvEvaluation.ShowSizeAllColumnsToFit = True
        Me.lvEvaluation.Size = New System.Drawing.Size(608, 426)
        Me.lvEvaluation.Sortable = True
        Me.lvEvaluation.TabIndex = 0
        Me.lvEvaluation.UseCompatibleStateImageBehavior = False
        Me.lvEvaluation.View = System.Windows.Forms.View.Details
        '
        'objcolhPerspective
        '
        Me.objcolhPerspective.Tag = "objcolhPerspective"
        Me.objcolhPerspective.Text = ""
        Me.objcolhPerspective.Width = 0
        '
        'colhObjective
        '
        Me.colhObjective.Tag = "colhObjective"
        Me.colhObjective.Text = "Objective"
        Me.colhObjective.Width = 200
        '
        'colhKPI
        '
        Me.colhKPI.Tag = "colhKPI"
        Me.colhKPI.Text = "Key Performace Indicators"
        Me.colhKPI.Width = 150
        '
        'colhTargets
        '
        Me.colhTargets.Tag = "colhTargets"
        Me.colhTargets.Text = "Targets"
        Me.colhTargets.Width = 150
        '
        'colhInitiative
        '
        Me.colhInitiative.Tag = "colhInitiative"
        Me.colhInitiative.Text = "Initiative/Actions"
        Me.colhInitiative.Width = 150
        '
        'colhResult
        '
        Me.colhResult.Tag = "colhResult"
        Me.colhResult.Text = ""
        Me.colhResult.Width = 100
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 170
        '
        'objcolhobjectiveId
        '
        Me.objcolhobjectiveId.Tag = "objcolhobjectiveId"
        Me.objcolhobjectiveId.Width = 0
        '
        'objcolhKpiId
        '
        Me.objcolhKpiId.Tag = "objcolhKpiId"
        Me.objcolhKpiId.Width = 0
        '
        'objcolhTargetId
        '
        Me.objcolhTargetId.Tag = "objcolhTargetId"
        Me.objcolhTargetId.Width = 0
        '
        'objcolhInitiativeId
        '
        Me.objcolhInitiativeId.Tag = "objcolhInitiativeId"
        Me.objcolhInitiativeId.Width = 0
        '
        'objcolhResultId
        '
        Me.objcolhResultId.Tag = "objcolhResultId"
        Me.objcolhResultId.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Width = 0
        '
        'objcolhIsGrp
        '
        Me.objcolhIsGrp.Tag = "objcolhIsGrp"
        Me.objcolhIsGrp.Text = ""
        Me.objcolhIsGrp.Width = 0
        '
        'objcolhSortId
        '
        Me.objcolhSortId.Tag = "objcolhSortId"
        Me.objcolhSortId.Text = "objcolhSortId"
        Me.objcolhSortId.Width = 0
        '
        'objbtnShow
        '
        Me.objbtnShow.BackColor = System.Drawing.Color.Transparent
        Me.objbtnShow.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnShow.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnShow.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnShow.BorderSelected = False
        Me.objbtnShow.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnShow.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.objbtnShow.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnShow.Location = New System.Drawing.Point(922, 2)
        Me.objbtnShow.Name = "objbtnShow"
        Me.objbtnShow.Size = New System.Drawing.Size(21, 21)
        Me.objbtnShow.TabIndex = 390
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkViewBSC)
        Me.objFooter.Controls.Add(Me.btnSaveCommit)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 454)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(947, 55)
        Me.objFooter.TabIndex = 2
        '
        'lnkViewBSC
        '
        Me.lnkViewBSC.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewBSC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkViewBSC.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkViewBSC.Location = New System.Drawing.Point(12, 20)
        Me.lnkViewBSC.Name = "lnkViewBSC"
        Me.lnkViewBSC.Size = New System.Drawing.Size(216, 16)
        Me.lnkViewBSC.TabIndex = 373
        Me.lnkViewBSC.TabStop = True
        Me.lnkViewBSC.Text = "View Balance Score Card"
        Me.lnkViewBSC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveCommit
        '
        Me.btnSaveCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveCommit.BackColor = System.Drawing.Color.White
        Me.btnSaveCommit.BackgroundImage = CType(resources.GetObject("btnSaveCommit.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveCommit.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveCommit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveCommit.FlatAppearance.BorderSize = 0
        Me.btnSaveCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveCommit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveCommit.ForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveCommit.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Location = New System.Drawing.Point(619, 13)
        Me.btnSaveCommit.Name = "btnSaveCommit"
        Me.btnSaveCommit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Size = New System.Drawing.Size(110, 30)
        Me.btnSaveCommit.TabIndex = 0
        Me.btnSaveCommit.Text = "S&ave && Commit"
        Me.btnSaveCommit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(735, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(838, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmBSC_Evaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(947, 509)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_Evaluation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " "
        Me.pnlMain.ResumeLayout(False)
        Me.gbBSCEvaluation.ResumeLayout(False)
        Me.spc1.Panel1.ResumeLayout(False)
        Me.spc1.Panel2.ResumeLayout(False)
        Me.spc1.ResumeLayout(False)
        Me.pnlContainer.ResumeLayout(False)
        Me.pnlContainer.PerformLayout()
        Me.pnlEvaluationList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSaveCommit As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbBSCEvaluation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAssessDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dtpAssessdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboAssessor As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessor As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAssessor As eZee.Common.eZeeGradientButton
    Friend WithEvents lblReviewer As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchObjective As eZee.Common.eZeeGradientButton
    Friend WithEvents cboObjective As System.Windows.Forms.ComboBox
    Friend WithEvents lblObjective As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchResult As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.RichTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents btnDeleteEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents lnkViewBSC As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlEvaluationList As System.Windows.Forms.Panel
    Friend WithEvents lvEvaluation As eZee.Common.eZeeListView
    Friend WithEvents cboPerspective As System.Windows.Forms.ComboBox
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents objcolhPerspective As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhObjective As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhKPI As System.Windows.Forms.ColumnHeader
    Private WithEvents colhTargets As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInitiative As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResult As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents radExternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents radInternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents objcolhobjectiveId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhKpiId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTargetId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInitiativeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhResultId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtWeightage As System.Windows.Forms.TextBox
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents objcolhIsGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkBSCPlanning As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchReviewer As eZee.Common.eZeeGradientButton
    Friend WithEvents cboReviewer As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchInitiative As eZee.Common.eZeeGradientButton
    Friend WithEvents cboInitiative As System.Windows.Forms.ComboBox
    Friend WithEvents lblInitiative As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTraget As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTraget As System.Windows.Forms.ComboBox
    Friend WithEvents lblTarget As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchKPI As eZee.Common.eZeeGradientButton
    Friend WithEvents cboKPI As System.Windows.Forms.ComboBox
    Friend WithEvents lblKPI As System.Windows.Forms.Label
    Friend WithEvents pnlContainer As System.Windows.Forms.Panel
    Friend WithEvents spc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objbtnShow As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnHide As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhSortId As System.Windows.Forms.ColumnHeader
End Class
