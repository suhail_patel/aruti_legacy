﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmAddEditOwrField1

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "objfrmAddEditOwrField1"
    Private mblnCancel As Boolean = True
    Private mintOwrField1Unkid As Integer = 0
    Private menAction As enAction = enAction.ADD_ONE
    Private objOwrField1 As clsassess_owrfield1_master
    Private mintFieldUnkid As Integer
    Private objFieldMaster As New clsAssess_Field_Master(True)
    Private mdicFieldData As New Dictionary(Of Integer, String)
    Private objWSetting As New clsWeight_Setting(True)
    Private mintOwnerTypeId As Integer = 0
    Private mintOwnerId As Integer = 0
    Private mblnDropDownClosed As Boolean = False
    Private mintSelectedPeriodId As Integer = 0
    Private mintLinkedFieldId As Integer = -1
    Private objOwrOwner As clsassess_owrowner_tran
    Private mdtOwner As DataTable
    Private dtOwnerView As DataView

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal intFieldId As Integer, _
                                  ByVal iOwnerTypeId As Integer, _
                                  ByVal iOwnerId As Integer, _
                                  ByVal iPeriodId As Integer) As Boolean
        Try
            mintOwrField1Unkid = intUnkId
            mintFieldUnkid = intFieldId
            menAction = eAction

            mintOwnerTypeId = iOwnerTypeId
            mintOwnerId = iOwnerId
            mintSelectedPeriodId = iPeriodId

            Dim objMapping As New clsAssess_Field_Mapping
            mintLinkedFieldId = objMapping.Get_Map_FieldId(mintSelectedPeriodId)
            objMapping = Nothing

            If mintLinkedFieldId <> mintFieldUnkid Then
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If

            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = mintSelectedPeriodId
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = mintSelectedPeriodId
            'Sohail (21 Aug 2015) -- End
            txtPeriod.Text = objPrd._Period_Name
            objPrd = Nothing

            Call Set_Form_Information()

            Me.ShowDialog()

            intUnkId = mintOwrField1Unkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Form_Information()
        Try
            Me.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field1_Caption & " " & _
                      Language.getMessage(mstrModuleName, 2, "Information")

            objlblField1.Text = Language.getMessage(mstrModuleName, 16, "Company") & " " & objFieldMaster._Field1_Caption

            objlblOwrField1.Text = objFieldMaster._Field1_Caption
            objtxtOwrField1.Tag = objFieldMaster._Field1Unkid


            If mintFieldUnkid = mintLinkedFieldId Then
                If objFieldMaster._Field6_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark1)
                Else
                    objtabpRemark1.Text = objFieldMaster._Field6_Caption
                    txtRemark1.Tag = objFieldMaster._Field6Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark2)
                Else
                    objtabpRemark2.Text = objFieldMaster._Field7_Caption
                    txtRemark2.Tag = objFieldMaster._Field7Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark3)
                Else
                    objtabpRemark3.Text = objFieldMaster._Field8_Caption
                    txtRemark3.Tag = objFieldMaster._Field8Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicFieldData.Keys.Count > 0 Then objtabcRemarks.Enabled = True
                tblpAssessorEmployee.Enabled = ConfigParameter._Object._FollowEmployeeHierarchy
            Else
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Information", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objCoyField1 As New clsassess_coyfield1_master
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
                .Enabled = False
                objbtnSearchOwner.Enabled = False
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                'S.SANDEEP [16 JUN 2015] -- START
                .Text = ""
                'S.SANDEEP [16 JUN 2015] -- END
            End With

            dsList = objCoyField1.getComboList(mintSelectedPeriodId, "List", True, True)
            With cboFieldValue1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboFieldValue1.DrawMode = DrawMode.OwnerDrawFixed
            AddHandler cboFieldValue1.DrawItem, AddressOf cboFieldValue1_DrawItem
            AddHandler cboFieldValue1.DropDownClosed, AddressOf cboFieldValue1_DropDownClosed

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtPeriod.BackColor = GUI.ColorComp
            objtxtOwrField1.BackColor = GUI.ColorComp
            cboAllocations.BackColor = GUI.ColorComp
            cboOwner.BackColor = GUI.ColorComp
            cboFieldValue1.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objOwrField1._Coyfield1unkid = CInt(cboFieldValue1.SelectedValue)
            If dtpEndDate.Checked = True Then
                objOwrField1._Enddate = dtpEndDate.Value
            Else
                objOwrField1._Enddate = Nothing
            End If
            objOwrField1._Field_Data = objtxtOwrField1.Text
            objOwrField1._Fieldunkid = mintFieldUnkid
            objOwrField1._Isvoid = False
            objOwrField1._Owenerrefid = CInt(cboAllocations.SelectedValue)
            objOwrField1._Ownerunkid = CInt(cboOwner.SelectedValue)
            objOwrField1._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD1
            objOwrField1._Periodunkid = mintSelectedPeriodId
            If dtpStartDate.Checked = True Then
                objOwrField1._Startdate = dtpStartDate.Value
            Else
                objOwrField1._Startdate = Nothing
            End If
            objOwrField1._Statusunkid = CInt(cboStatus.SelectedValue)
            objOwrField1._Userunkid = User._Object._Userunkid
            objOwrField1._Voiddatetime = Nothing
            objOwrField1._Voiduserunkid = 0
            objOwrField1._Voidreason = ""
            objOwrField1._Weight = txtWeight.Decimal
            objOwrField1._Yearunkid = 0
            objOwrField1._Pct_Completed = txtPercent.Decimal
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objOwrField1._Enddate <> Nothing Then
                dtpEndDate.Value = objOwrField1._Enddate
                dtpEndDate.Checked = True
            End If
            If objOwrField1._Startdate <> Nothing Then
                dtpStartDate.Value = objOwrField1._Startdate
                dtpStartDate.Checked = True
            End If
            objtxtOwrField1.Text = objOwrField1._Field_Data
            cboAllocations.SelectedValue = IIf(objOwrField1._Owenerrefid <= 0, mintOwnerTypeId, objOwrField1._Owenerrefid)
            cboFieldValue1.SelectedValue = objOwrField1._Coyfield1unkid
            cboOwner.SelectedValue = IIf(objOwrField1._Ownerunkid <= 0, mintOwnerId, objOwrField1._Ownerunkid)

            'S.SANDEEP [16 JUN 2015] -- START
            'cboStatus.SelectedValue = IIf(objOwrField1._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objOwrField1._Statusunkid)
            If objOwrField1._Statusunkid <= 0 Then
                cboStatus.SelectedValue = 0 : cboStatus.Text = ""
            Else
                cboStatus.SelectedValue = objOwrField1._Statusunkid
            End If
            'S.SANDEEP [16 JUN 2015] -- END

            txtWeight.Decimal = CDec(objOwrField1._Weight)
            txtPercent.Decimal = objOwrField1._Pct_Completed
            If menAction = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                mdicFieldData = objInfoField.Get_Data(mintOwrField1Unkid, enWeight_Types.WEIGHT_FIELD1)
                If mdicFieldData.Keys.Count > 0 Then
                    If mdicFieldData.ContainsKey(CInt(txtRemark1.Tag)) Then
                        txtRemark1.Text = mdicFieldData(CInt(txtRemark1.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark2.Tag)) Then
                        txtRemark2.Text = mdicFieldData(CInt(txtRemark2.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark3.Tag)) Then
                        txtRemark3.Text = mdicFieldData(CInt(txtRemark3.Tag))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboOwner.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Owner is mandatory information. Please select Owner to continue."), enMsgBoxStyle.Information)
                cboOwner.Focus()
                Return False
            End If

            Select Case ConfigParameter._Object._CascadingTypeId
                Case enPACascading.STRICT_CASCADING, enPACascading.STRICT_GOAL_ALIGNMENT
                    If CInt(cboFieldValue1.SelectedValue) <= 0 Then
                        iMsg = Language.getMessage(mstrModuleName, 4, "Sorry Company, ") & objFieldMaster._Field1_Caption & _
                               Language.getMessage(mstrModuleName, 5, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                               Language.getMessage(mstrModuleName, 6, " to continue.")
                        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                        cboFieldValue1.Focus()
                        Return False
                    End If
            End Select

            If objtxtOwrField1.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 7, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName, 5, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName, 6, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                objtxtOwrField1.Focus()
                Return False
            End If

            If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                If dtpEndDate.Value.Date < dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, End Date cannot be less then Start Date."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            If mintFieldUnkid = mintLinkedFieldId Then
                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Weight is mandatory information. Please provide Weight to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If

                Dim objMapping As New clsAssess_Field_Mapping
                If txtWeight.Decimal > 0 Then
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, enWeight_Types.WEIGHT_FIELD1, txtWeight.Decimal, mintSelectedPeriodId, mintLinkedFieldId, mintOwnerId, 0, menAction, mintOwrField1Unkid)
                    If iMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                        txtWeight.Focus()
                        Return False
                    End If
                End If

                'If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                '    iMsg = objMapping.IsValid_Date(mintLinkedFieldId, dtpStartDate.Value.Date, dtpEndDate.Value.Date, clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL)
                '    If iMsg.Trim.Length > 0 Then
                '        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                '        Return False
                '    End If
                'End If
                objMapping = Nothing

                'If txtWeight.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100."), enMsgBoxStyle.Information)
                '    txtWeight.Focus()
                '    Return False
                'End If

                'S.SANDEEP [16 JUN 2015] -- START

                'If txtPercent.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, Percentage Completed cannot be greater than 100."), enMsgBoxStyle.Information)
                '    txtPercent.Focus()
                '    Return False
                'End If

                'If CInt(cboStatus.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue."), enMsgBoxStyle.Information)
                '    cboStatus.Focus()
                '    Return False
                'End If

                'S.SANDEEP [16 JUN 2015] -- END


            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Try
            RemoveHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            Dim iFilter As String = "hremployee_master.isapproved = 1"

            'S.SANDEEP [08-MAY-2017] -- START
            'Select Case mintOwnerTypeId
            '    Case enAllocation.BRANCH
            '        iFilter &= " AND hremployee_master.stationunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.DEPARTMENT_GROUP
            '        iFilter &= " AND hremployee_master.deptgroupunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.DEPARTMENT
            '        iFilter &= " AND hremployee_master.departmentunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.SECTION_GROUP
            '        iFilter &= " AND hremployee_master.sectiongroupunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.SECTION
            '        iFilter &= " AND hremployee_master.sectionunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.UNIT_GROUP
            '        iFilter &= " AND hremployee_master.unitgroupunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.UNIT
            '        iFilter &= " AND hremployee_master.unitunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.TEAM
            '        iFilter &= " AND hremployee_master.teamunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.JOB_GROUP
            '        iFilter &= " AND hremployee_master.jobgroupunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.JOBS
            '        iFilter &= " AND hremployee_master.jobunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.CLASS_GROUP
            '        iFilter &= " AND hremployee_master.classgroupunkid = '" & mintOwnerId & "' "
            '    Case enAllocation.CLASSES
            '        iFilter &= " AND hremployee_master.classunkid = '" & mintOwnerId & "' "
            'End Select

            Select Case mintOwnerTypeId
                Case enAllocation.BRANCH
                    iFilter &= " AND ISNULL(ETRF.stationunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    iFilter &= " AND ISNULL(ETRF.deptgroupunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT
                    iFilter &= " AND ISNULL(ETRF.departmentunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.SECTION_GROUP
                    iFilter &= " AND ISNULL(ETRF.sectiongroupunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.SECTION
                    iFilter &= " AND ISNULL(ETRF.sectionunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.UNIT_GROUP
                    iFilter &= " ISNULL(ETRF.unitgroupunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.UNIT
                    iFilter &= " AND ISNULL(ETRF.unitunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.TEAM
                    iFilter &= " AND ISNULL(ETRF.teamunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.JOB_GROUP
                    iFilter &= " AND ISNULL(ERECAT.jobgroupunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.JOBS
                    iFilter &= " AND ISNULL(ERECAT.jobunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.CLASS_GROUP
                    iFilter &= " AND ISNULL(ETRF.classgroupunkid,0) = '" & mintOwnerId & "' "
                Case enAllocation.CLASSES
                    iFilter &= " AND ISNULL(ETRF.classunkid,0) = '" & mintOwnerId & "' "
            End Select
            'S.SANDEEP [08-MAY-2017] -- END


            

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , iFilter)
            dList = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        ConfigParameter._Object._UserAccessModeSetting, _
                                        True, False, "iList", _
                                        ConfigParameter._Object._ShowFirstAppointmentDate, , , _
                                        iFilter)
            'S.SANDEEP [04 JUN 2015] -- END

            'Shani (24-May-2016) -- Start
            dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Shani (24-May-2016) -- End

            If dList.Tables(0).Rows.Count > 0 Then
                'Shani (24-May-2016) -- Start
                'dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
                'Shani (24-May-2016) -- End
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    dtRow.Item("ischeck") = False
                    If mdtOwner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,name"

            dgvOwner.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "name"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvOwner.DataSource = dtOwnerView

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            AddHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GoalOwnerOperation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            If mdtOwner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("owrfieldunkid") = mintOwrField1Unkid
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("owrfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwner.Rows.Add(dRow)
                    End If
                    mdtOwner.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GoalOwnerOperation", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmAddEditOwrField1_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objOwrField1 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmAddEditOwrField1_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objfrmAddEditOwrField1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objOwrField1 = New clsassess_owrfield1_master
        objOwrOwner = New clsassess_owrowner_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objOwrField1._Owrfield1unkid = mintOwrField1Unkid
                cboAllocations.Enabled = False
            End If
            mdtOwner = objOwrOwner.Get_Data(mintOwrField1Unkid, enWeight_Types.WEIGHT_FIELD1)
            Call Fill_Data()
            Call FillCombo()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmAddEditOwrField1_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_owrfield1_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_owrfield1_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

            Me.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field1_Caption & " " & _
                      Language.getMessage(mstrModuleName, 2, "Information")

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iblnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            Call SetValue()
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objOwrField1.Update(mdicFieldData, mdtOwner)
            Else
                iblnFlag = objOwrField1.Insert(mdicFieldData, mdtOwner)
            End If
            If iblnFlag = False Then
                If objOwrField1._Message <> "" Then
                    eZeeMsgBox.Show(objOwrField1._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, problem in saving Owner Goals."), enMsgBoxStyle.Information)
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Owner Goals are saved successfully."), enMsgBoxStyle.Information)
                If menAction = enAction.ADD_CONTINUE Then
                    objOwrField1 = New clsassess_owrfield1_master
                    Call GetValue()
                    txtRemark1.Text = "" : txtRemark2.Text = "" : txtRemark3.Text = ""
                Else
                    Call btnClose_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchOwner_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOwner.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboOwner.ValueMember
                .DisplayMember = cboOwner.DisplayMember
                .DataSource = CType(cboOwner.DataSource, DataTable)
                If .DisplayDialog Then
                    cboOwner.SelectedValue = .SelectedValue
                    cboOwner.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOwner_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchField1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchField1.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboFieldValue1.ValueMember
                .DisplayMember = cboFieldValue1.DisplayMember
                .DataSource = CType(cboFieldValue1.DataSource, DataTable)
                If .DisplayDialog Then
                    cboFieldValue1.SelectedValue = .SelectedValue
                    cboFieldValue1.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Contols Events "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "stationunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0).Copy
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "deptgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "departmentunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "sectiongroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "sectionunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "unitgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "unitunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "teamunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "jobgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "jobunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "classgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "classesunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = mintOwnerId
                        .Enabled = False
                    End With
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark1.TextChanged
        Try
            mdicFieldData(CInt(txtRemark1.Tag)) = txtRemark1.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark1_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark2.TextChanged
        Try
            mdicFieldData(CInt(txtRemark2.Tag)) = txtRemark2.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark2_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark3.TextChanged
        Try
            mdicFieldData(CInt(txtRemark3.Tag)) = txtRemark3.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark3_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ToolTip1.Hide(cboFieldValue1) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs)
        Try
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = cboFieldValue1.GetItemText(cboFieldValue1.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, cboFieldValue1, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvOwner.Rows.Count > 0 Then
                        If dgvOwner.SelectedRows(0).Index = dgvOwner.Rows(dgvOwner.RowCount - 1).Index Then Exit Sub
                        dgvOwner.Rows(dgvOwner.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvOwner.Rows.Count > 0 Then
                        If dgvOwner.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvOwner.Rows(dgvOwner.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dtOwnerView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOwner_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOwner.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvOwner.IsCurrentCellDirty Then
                    Me.dgvOwner.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Call GoalOwnerOperation(CInt(dgvOwner.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value), CBool(dgvOwner.Rows(e.RowIndex).Cells(objdgcolhECheck.Index).Value))

                Dim drRow As DataRow() = dtOwnerView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtOwnerView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOwner_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
            For Each dr As DataRowView In dtOwnerView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
                Call GoalOwnerOperation(CInt(dr.Item("employeeunkid")), CBool(objchkEmployee.CheckState))
            Next
            dgvOwner.Refresh()
            AddHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOwner_ColumnWidthChanged(ByVal sender As Object, ByVal e As DataGridViewColumnEventArgs) Handles dgvOwner.ColumnWidthChanged
        Dim rtHeader As Rectangle = Me.dgvOwner.DisplayRectangle
        rtHeader.Height = CInt(Me.dgvOwner.ColumnHeadersHeight / 2)
        Me.dgvOwner.Invalidate(rtHeader)
    End Sub

    Private Sub dgvOwner_Scroll(ByVal sender As Object, ByVal e As ScrollEventArgs) Handles dgvOwner.Scroll
        Dim rtHeader As Rectangle = Me.dgvOwner.DisplayRectangle
        rtHeader.Height = CInt(Me.dgvOwner.ColumnHeadersHeight / 2)
        Me.dgvOwner.Invalidate(rtHeader)
    End Sub

    Private Sub dgvOwner_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles dgvOwner.Paint
        Dim j As Integer = 1
        While j < dgvOwner.ColumnCount - 1
            Dim r1 As Rectangle = Me.dgvOwner.GetCellDisplayRectangle(j, -1, True)
            Dim w2 As Integer = Me.dgvOwner.GetCellDisplayRectangle(j + 1, -1, True).Width
            Dim w3 As Integer = Me.dgvOwner.GetCellDisplayRectangle(j + 1, -1, True).Height
            'r1.X += 1
            r1.Y += 1
            r1.Width = r1.Width + w2 - 2
            'r1.Height = CInt(r1.Height / 2 - 2)
            r1.Height = w3 - 5
            e.Graphics.FillRectangle(New SolidBrush(Me.dgvOwner.ColumnHeadersDefaultCellStyle.BackColor), r1)
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Center
            format.LineAlignment = StringAlignment.Center
            e.Graphics.DrawString(Language.getMessage(mstrModuleName, 18, "Assigned To"), Me.dgvOwner.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.dgvOwner.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
            j += 2
        End While
    End Sub

    Private Sub dgvOwner_CellPainting(ByVal sender As Object, ByVal e As DataGridViewCellPaintingEventArgs) Handles dgvOwner.CellPainting
        If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
            Dim r2 As Rectangle = e.CellBounds
            r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
            r2.Height = CInt(e.CellBounds.Height / 2)
            e.PaintBackground(r2, True)
            e.PaintContent(r2)
            e.Handled = True
        End If
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblOwner.Text = Language._Object.getCaption(Me.lblOwner.Name, Me.lblOwner.Text)
			Me.lblGoalOwner.Text = Language._Object.getCaption(Me.lblGoalOwner.Name, Me.lblGoalOwner.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
			Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Add/Edit Goal Owner")
			Language.setMessage(mstrModuleName, 2, "Information")
			Language.setMessage(mstrModuleName, 3, "Owner is mandatory information. Please select Owner to continue.")
			Language.setMessage(mstrModuleName, 4, "Sorry Company,")
			Language.setMessage(mstrModuleName, 5, " is mandatory information. Please provide")
			Language.setMessage(mstrModuleName, 6, " to continue.")
			Language.setMessage(mstrModuleName, 7, "Sorry,")
			Language.setMessage(mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue.")
			Language.setMessage(mstrModuleName, 11, "Sorry, problem in saving Owner Goals.")
			Language.setMessage(mstrModuleName, 12, "Owner Goals are saved successfully.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Weight is mandatory information. Please provide Weight to continue.")
			Language.setMessage(mstrModuleName, 16, "Company")
			Language.setMessage(mstrModuleName, 18, "Assigned To")
			Language.setMessage(mstrModuleName, 19, "Sorry, End Date cannot be less then Start Date.")
			Language.setMessage(mstrModuleName, 20, "Sorry, Percentage Completed cannot be greater than 100.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class