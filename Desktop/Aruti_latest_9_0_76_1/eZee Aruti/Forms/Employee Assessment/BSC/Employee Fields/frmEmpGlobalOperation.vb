﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmpGlobalOperation

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmEmpGlobalOperation"
    Private objassess_Empfield1Mst As clsassess_empfield1_master
    Private mintTotalCount As Integer = 0
    Dim drRow() As DataRow = Nothing
    Dim iUnlockComments As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region "Private Method(S)"

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 2, "Submit For Approval"))
            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 3, "Approve Goals"))
            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 4, "Reject Goals"))
            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 5, "Unlock Approve Goals"))

            cboOperation.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = CType(dgvData.DataSource, DataTable).Select("ischeck = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub FillGrid()
        Dim dsList As DataSet = Nothing
        Try
            dgvData.AutoGenerateColumns = False

            If CInt(cboOperation.SelectedIndex) = 1 Then
                'S.SANDEEP [07-JUN-2017] -- START
                'ISSUE/ENHANCEMENT : No Data Coming For Open Changes
                'dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.OPEN_CHANGES, -1)
                Dim mdtTab As DataTable = Nothing
                dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.OPEN_CHANGES, -1, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START
                mdtTab = dsList.Tables(0).Copy
                dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.NOT_SUBMIT, -1, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START
                mdtTab.Merge(dsList.Tables(0).Copy)

                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.PERIODIC_REVIEW, -1, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START
                mdtTab.Merge(dsList.Tables(0).Copy)
                'S.SANDEEP |18-JAN-2020| -- END


                'S.SANDEEP [28-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002073}
                'dsList.Tables.RemoveAt(0) : dsList.Tables.Add(mdtTab.DefaultView.ToTable(True, "ischeck", "employee", "Weight"))
                dsList.Tables.RemoveAt(0) : dsList.Tables.Add(mdtTab.DefaultView.ToTable(True, "ischeck", "employee", "Weight", "employeeunkid"))
                'S.SANDEEP [28-Feb-2018] -- END

                'S.SANDEEP [07-JUN-2017] -- END

                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 75
                If User._Object.Privilege._AllowtoSubmitGoalsforApproval = False Then
                    dsList.Tables(0).Rows.Clear()
                End If
                'S.SANDEEP [27-SEP-2017] -- END

            ElseIf CInt(cboOperation.SelectedIndex) = 2 OrElse CInt(cboOperation.SelectedIndex) = 3 Then
                dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.SUBMIT_APPROVAL, User._Object._Userunkid, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START

                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 75
                If User._Object.Privilege._AllowtoApproveRejectGoalsPlanning = False Then
                    dsList.Tables(0).Rows.Clear()
                End If
                'S.SANDEEP [27-SEP-2017] -- END

            ElseIf CInt(cboOperation.SelectedIndex) = 4 Then
                dsList = objassess_Empfield1Mst.GetGlobalEmployeeeList(CInt(cboPeriod.SelectedValue), enObjective_Status.FINAL_SAVE, User._Object._Userunkid, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, mstrAdvanceFilter) 'S.SANDEEP |05-MAY-2019| -- START

                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 75
                If User._Object.Privilege._AllowtoUnlockFinalSavedGoals = False Then
                    dsList.Tables(0).Rows.Clear()
                End If
                'S.SANDEEP [27-SEP-2017] -- END
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("Message") = False Then
                dsList.Tables(0).Columns.Add("Message", Type.GetType("System.String"))
            End If

            'Shani (05-Dec-2017) -- Start
            'Issue - 
            'objcolhCheck.DataPropertyName = "ischeck"
            'colhEmployee.DataPropertyName = "employee"
            'colhWeight.DataPropertyName = "Weight"
            'dgcolhError.DataPropertyName = "Message"
            'dgvData.DataSource = dsList.Tables(0)
            If dsList IsNot Nothing Then
            objcolhCheck.DataPropertyName = "ischeck"
            colhEmployee.DataPropertyName = "employee"
            colhWeight.DataPropertyName = "Weight"
            dgcolhError.DataPropertyName = "Message"
            dgvData.DataSource = dsList.Tables(0)
            End If
            objbtnSearch.ShowResult(dgvData.RowCount)
            'Shani (05-Dec-2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmOwrFieldsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objassess_Empfield1Mst = New clsassess_empfield1_master

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
            objlblValue.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOwrFieldsList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empfield1_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empfield1_master"
            objfrm.displayDialog(Me)

            SetMessages()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region "Button's Events"

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Period is compulsory information.Please select period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Exit Sub
            End If

            If CInt(cboOperation.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Operation is compulsory information.Please select Operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboOperation.Focus()
                Exit Sub
            End If
            Call FillGrid()
            Call objbtnSearch.ShowResult(CStr(dgvData.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboOperation.SelectedIndex = 0
            chkSelectAll.Checked = False
            dgvData.DataSource = Nothing
            mstrAdvanceFilter = ""
            Call objbtnSearch.ShowResult(CStr(dgvData.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, there is no goal defined in order to") & " " & cboOperation.Text & ".", enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim dtTable As DataTable = Nothing

                If CType(dgvData.DataSource, DataTable) IsNot Nothing AndAlso CType(dgvData.DataSource, DataTable).Rows.Count > 0 Then
                    drRow = CType(dgvData.DataSource, DataTable).Select("ischeck = True")

                    If drRow.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected period"), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    mintTotalCount = drRow.Length
                    Dim iMsgText As String = ""

                    If CInt(cboOperation.SelectedIndex) = 1 Then 'Submit For Approval
                        iMsgText = Language.getMessage(mstrModuleName, 9, "You are about to Submit this Performance Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                          "Do you wish to continue?")

                    ElseIf CInt(cboOperation.SelectedIndex) = 2 Then 'Approve
                        iMsgText = Language.getMessage(mstrModuleName, 2, "You are about to Approve this BSC Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & _
                                                                          "Do you wish to continue?")

                    ElseIf CInt(cboOperation.SelectedIndex) = 3 Then 'Reject
                        iMsgText = Language.getMessage(mstrModuleName, 3, "You are about to Disapprove this BSC Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & _
                                                                          "Do you wish to continue?")

                    ElseIf CInt(cboOperation.SelectedIndex) = 4 Then 'Unlock Approve Goals
                        iMsgText = Language.getMessage(mstrModuleName, 15, "You are about to unlock the employee performance planning. This will result in opening of all operation on employee performance planning." & vbCrLf & _
                                                                           "Do you wish to continue?")
                    End If


                    If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

                    If CInt(cboOperation.SelectedIndex) = 2 OrElse CInt(cboOperation.SelectedIndex) = 3 OrElse CInt(cboOperation.SelectedIndex) = 4 Then   '  Approve/Reject/UnCommit
                        iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 17, "Enter reason to") & " " & cboOperation.Text, Me.Text)

                        If (CInt(cboOperation.SelectedIndex) = 3 OrElse CInt(cboOperation.SelectedIndex) = 4) AndAlso iUnlockComments.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(cboOperation.Text & " " & Language.getMessage(mstrModuleName, 18, "reason is mandaroty information."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Me.Cursor = Cursors.WaitCursor

                    Application.DoEvents()
                    objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & "0/" & drRow.Length.ToString

                    Me.SuspendLayout()
                    RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

                    objbgWorker.RunWorkerAsync()

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvData.DataSource Is Nothing Then Exit Sub
            Dim drRow() As DataRow = CType(dgvData.DataSource, DataTable).Select("1=1")
            If drRow.Length > 0 Then
                For Each dr In drRow
                    dr("ischeck") = chkSelectAll.Checked
                    dr.AcceptChanges()
                Next
            End If
            CType(dgvData.DataSource, DataTable).AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhCheck.Index Then

                If dgvData.IsCurrentCellDirty Then
                    dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                CType(dgvData.DataSource, DataTable).AcceptChanges()
                SetCheckBoxValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboOperation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperation.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            chkSelectAll.Checked = False
            dgvData.DataSource = Nothing
            btnProcess.Enabled = True
            objlblValue.Text = ""
            objbtnSearch.ShowResult(dgvData.RowCount)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOperation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Background Work Event"

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgWorker.DoWork
        Try
            Me.ControlBox = False
            Me.Enabled = False

            Dim xCount As Integer = 0
            Dim iMsgText As String = ""
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim objCmpt_Mast As New clsassess_competence_assign_master
            Dim objAssessor As New clsAssessor_tran
            Dim objEStatusTran As New clsassess_empstatus_tran
            Dim dsList As DataSet = Nothing


            If CInt(cboOperation.SelectedIndex) = 2 Then 'Approve
                Dim objFMaster As New clsAssess_Field_Master
                If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    objbgWorker.CancelAsync()
                    Me.ControlBox = True
                    Me.Enabled = True
                    Exit Sub
                End If
                objFMaster = Nothing

                If objFMapping.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    objbgWorker.CancelAsync()
                    Me.ControlBox = True
                    Me.Enabled = True
                    Exit Sub
                End If


                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This approving process will transfer the goals to your subordinate employee(s), Those employee(s) who has already committed their goals will not be affected by this process. Do you wish to continue?"), _
                                           CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    e.Cancel = True
                    objbgWorker.CancelAsync()
                    Me.ControlBox = True
                    Me.Enabled = True
                    Exit Sub
                End If

            End If

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            Dim blnVoidProgress As Boolean = False
            If CInt(cboOperation.SelectedIndex) = 4 Then 'For Unlock Approve Goals
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You are about to unlock the employee performance planning. Do you want to void approved progress update(s)?"), _
                                           CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    blnVoidProgress = True
                End If
            End If
            'S.SANDEEP |09-JUL-2019| -- END

            For Each dr As DataRow In drRow
                Try
                    dgvData.FirstDisplayedScrollingRowIndex = CType(dgvData.DataSource, DataTable).Rows.IndexOf(dr) - 5
                    Application.DoEvents()
                Catch ex As Exception
                End Try

                iMsgText = ""

                If CInt(cboOperation.SelectedIndex) = 1 Then 'Submit For approval

                    If ConfigParameter._Object._Self_Assign_Competencies = True Then
                        dsList = objCmpt_Mast.GetList("List", eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), 0, CInt(cboPeriod.SelectedValue), CInt(dr("employeeunkid")), ConfigParameter._Object._Self_Assign_Competencies)

                        If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then
                            dr("Message") = Language.getMessage(mstrModuleName, 19, "Sorry, there is no list of competencies assigned to this employee or some defined assessment group does not fall in the allocation of the selected employee.")
                            xCount += 1
                            objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                            Continue For
                        Else
                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                Dim xGrpWeight As Decimal = 0 : Dim xGrpId As Integer = -1
                                For Each xRow As DataRow In dsList.Tables(0).Rows
                                    If xGrpId <> xRow.Item("assessgroupunkid") Then
                                        Dim objAGroup As New clsassess_group_master
                                        objAGroup._Assessgroupunkid = xRow.Item("assessgroupunkid")
                                        xGrpWeight = xGrpWeight + objAGroup._Weight
                                        objAGroup = Nothing
                                        xGrpId = xRow.Item("assessgroupunkid")
                                    End If
                                Next
                                If CInt(dsList.Tables(0).Compute("SUM(assigned_weight)", "")) <> xGrpWeight Then
                                    dr("Message") = Language.getMessage(mstrModuleName, 18, "Sorry, you cannot Submit this Performance Planning for Approval, Reason Competencies are not adding upto assessment group weight.")
                                    xCount += 1
                                    objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                                    Continue For
                                End If

                            End If

                        End If

                    End If


                    Dim sMsg As String = String.Empty
                    sMsg = objAssessor.IsAssessorPresent(CInt(dr("employeeunkid")))
                    If sMsg <> "" Then
                        dr("Message") = sMsg
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    End If

                    sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(dr("employeeunkid")))
                    If sMsg.Trim.Length > 0 Then
                        dr("Message") = sMsg
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    End If


                    objEStatusTran._Assessoremployeeunkid = 0
                    objEStatusTran._Assessormasterunkid = 0
                    objEStatusTran._Commtents = ""
                    objEStatusTran._Employeeunkid = CInt(dr("employeeunkid"))
                    objEStatusTran._Isunlock = False
                    objEStatusTran._Loginemployeeunkid = 0
                    objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                    objEStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
                    objEStatusTran._Userunkid = User._Object._Userunkid

                    If objEStatusTran.Insert() = False Then
                        dr("Message") = objEStatusTran._Message
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    Else
                        If ConfigParameter._Object._ArutiSelfServiceURL.Trim.Length > 0 Then
                            Dim dtmp() As DataRow = CType(cboPeriod.DataSource, DataTable).Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'")
                            If dtmp.Length > 0 Then
                                objassess_Empfield1Mst.Send_Notification_Assessor(CInt(dr("employeeunkid")), CInt(cboPeriod.SelectedValue), _
                                                                       CInt(dtmp(0).Item("yearunkid")), FinancialYear._Object._FinancialYear_Name, FinancialYear._Object._DatabaseName, _
                                                                       ConfigParameter._Object._Companyunkid, ConfigParameter._Object._ArutiSelfServiceURL.Trim, enLogin_Mode.DESKTOP, 0, 0)
                                dr("Message") = cboOperation.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                                xCount += 1
                                objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                            End If

                        End If

                    End If

                ElseIf CInt(cboOperation.SelectedIndex) = 2 Then  'For Approve
                    Dim mdtEmpGoal As DataTable = objassess_Empfield1Mst.GetDisplayList(CInt(dr("employeeunkid")), CInt(cboPeriod.SelectedValue), "List")

                    Dim xRow() As DataRow = Nothing
                    If mdtEmpGoal IsNot Nothing Then
                        xRow = mdtEmpGoal.Select("OwnerIds <> ''")
                    End If

                    Dim blnFlag As Boolean = False
                    If xRow.Length > 0 Then

                        'Pinkal (07-Dec-2020) -- Start
                        'Enhancement NMB - Cascading PM UAT Comments.
                        'blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow)
                        blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                        'Pinkal (07-Dec-2020) -- End

                        If blnFlag = False Then
                            dr("Message") = objassess_Empfield1Mst._Message
                            xCount += 1
                            objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                            Continue For
                        End If
                    End If
                    'S.SANDEEP |08-DEC-2020| -- START
                    'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
                    'Dim objGrp As New clsGroup_Master(True)
                    'If objGrp._Groupname.ToString.ToUpper() = "NMB PLC" Then
                    '    If xRow.Length <= 0 Then
                    '        xRow = mdtEmpGoal.DefaultView.ToTable(True, "employeeunkid").Select("employeeunkid >0 ")

                    '        'Pinkal (07-Dec-2020) -- Start
                    '        'Enhancement NMB - Cascading PM UAT Comments.
                    '        'blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow)
                    '        blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, True)
                    '        'Pinkal (07-Dec-2020) -- End

                    '        If blnFlag = False Then
                    '            dr("Message") = objassess_Empfield1Mst._Message
                    '            xCount += 1
                    '            objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    '            Continue For
                    '        End If
                    '    End If
                    'End If
                    'objGrp = Nothing
                    'S.SANDEEP |08-DEC-2020| -- END

                    objEStatusTran._Assessoremployeeunkid = CInt(dr("assessorEmpid"))
                    objEStatusTran._Assessormasterunkid = CInt(dr("assessormasterunkid"))
                    objEStatusTran._Commtents = iUnlockComments
                    objEStatusTran._Employeeunkid = CInt(dr("employeeunkid"))
                    objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                    objEStatusTran._Statustypeid = enObjective_Status.FINAL_SAVE
                    objEStatusTran._Isunlock = False
                    objEStatusTran._Userunkid = User._Object._Userunkid
                    If objEStatusTran.Insert(Nothing, ConfigParameter._Object._Self_Assign_Competencies) = False Then
                        dr("Message") = objEStatusTran._Message
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    Else
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), "", True, False)
                        objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), "", True, False, Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                        dr("Message") = cboOperation.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    End If


                ElseIf CInt(cboOperation.SelectedIndex) = 3 Then  'For Reject

                    objEStatusTran._Assessoremployeeunkid = CInt(dr("assessorEmpid"))
                    objEStatusTran._Assessormasterunkid = CInt(dr("assessormasterunkid"))
                    objEStatusTran._Commtents = iUnlockComments
                    objEStatusTran._Employeeunkid = CInt(dr("employeeunkid"))
                    objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                    objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                    objEStatusTran._Isunlock = False
                    objEStatusTran._Userunkid = User._Object._Userunkid

                    If objEStatusTran.Insert(Nothing, ConfigParameter._Object._Self_Assign_Competencies) = False Then
                        dr("Message") = objEStatusTran._Message
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    Else
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), iUnlockComments, False, False)
                        objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), iUnlockComments, False, False, Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                        dr("Message") = cboOperation.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    End If


                ElseIf CInt(cboOperation.SelectedIndex) = 4 Then  'For Unlock Approve Goals

                    Dim objAnalysis_Master As New clsevaluation_analysis_master
                    If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(dr("employeeunkid")), CInt(cboPeriod.SelectedValue)) = True Then
                        dr("Message") = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period.")
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    End If

                    If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(dr("employeeunkid")), CInt(cboPeriod.SelectedValue)) = True Then
                        dr("Message") = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period.")
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    End If

                    If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(dr("employeeunkid")), CInt(cboPeriod.SelectedValue)) = True Then
                        dr("Message") = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period.")
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    End If
                    objAnalysis_Master = Nothing

                    'S.SANDEEP |27-NOV-2020| -- START
                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                    'Dim xMsg As String = ""
                    'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(dr("employeeunkid")))
                    'If xMsg.Trim.Length > 0 Then
                    '    dr("Message") = xMsg
                    '    xCount += 1
                    '    objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    '    Continue For
                    'End If
                    'S.SANDEEP |27-NOV-2020| -- END
                    
                    objEStatusTran._Assessoremployeeunkid = 0
                    objEStatusTran._Assessormasterunkid = 0
                    objEStatusTran._Commtents = iUnlockComments
                    objEStatusTran._Employeeunkid = CInt(dr("employeeunkid"))
                    objEStatusTran._Isunlock = True
                    objEStatusTran._Loginemployeeunkid = 0
                    objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                    objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                    objEStatusTran._Userunkid = User._Object._Userunkid

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'If objEStatusTran.Insert(, ConfigParameter._Object._Self_Assign_Competencies) = False Then
                    If objEStatusTran.Insert(, ConfigParameter._Object._Self_Assign_Competencies, blnVoidProgress) = False Then
                        'S.SANDEEP |09-JUL-2019| -- END
                        dr("Message") = objEStatusTran._Message
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Continue For
                    Else
                        'S.SANDEEP [25-JAN-2017] -- START
                        'ISSUE/ENHANCEMENT :
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), iUnlockComments, False, True)

                        'S.SANDEEP [15-Feb-2018] -- START
                        'ISSUE/ENHANCEMENT : {#38}
                        'objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), iUnlockComments, False, True, Company._Object._Companyunkid)

                        'S.SANDEEP |09-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : PA CHANGES
                        'objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), iUnlockComments, False, True, Company._Object._Companyunkid, , , , ConfigParameter._Object._Ntf_GoalsUnlockUserIds)
                        objassess_Empfield1Mst.Send_Notification_Employee(CInt(dr("employeeunkid")), dr("AssessorName").ToString(), iUnlockComments, False, True, Company._Object._Companyunkid, , , , ConfigParameter._Object._Ntf_GoalsUnlockUserIds, blnVoidProgress)
                        'S.SANDEEP |09-JUL-2019| -- END

                        'S.SANDEEP [15-Feb-2018] -- END

                        'Sohail (30 Nov 2017) -- End
                        'S.SANDEEP [25-JAN-2017] -- END
                        dr("Message") = cboOperation.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                        xCount += 1
                        objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    End If
                End If


            Next

            objAssessor = Nothing
            objEStatusTran = Nothing
            If objFMapping IsNot Nothing Then objFMapping = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_DoWork", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub bgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgWorker.ProgressChanged
    '    Try
    '        objlblValue.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintTotalCount & " ]"
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "bgWorker_ProgressChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgWorker.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            Else
                If objassess_Empfield1Mst._Message <> "" Then
                    eZeeMsgBox.Show(objassess_Empfield1Mst._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(cboOperation.Text & " " & Language.getMessage(mstrModuleName, 16, "Operation completed successfully."), enMsgBoxStyle.Information)
                    btnProcess.Enabled = False
                End If
                objlblValue.Text = ""
            End If
            Me.ControlBox = True
            Me.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_RunWorkerCompleted", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAdvFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvFilter_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.Name, Me.lblOperation.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhWeight.HeaderText = Language._Object.getCaption(Me.colhWeight.Name, Me.colhWeight.HeaderText)
            Me.dgcolhError.HeaderText = Language._Object.getCaption(Me.dgcolhError.Name, Me.dgcolhError.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Commit")
            Language.setMessage(mstrModuleName, 3, "Uncommit")
            Language.setMessage(mstrModuleName, 4, "Operation is compulsory information.Please select Operation.")
            Language.setMessage(mstrModuleName, 5, "Sorry, there is no goal defined in order to")
            Language.setMessage(mstrModuleName, 6, "Sorry in order to do commit operation please select following things Owner.")
            Language.setMessage(mstrModuleName, 7, "You are about to commit the owner goals for the selected period. This will tranfer all goals to employee goals.")
            Language.setMessage(mstrModuleName, 8, "Due to this you will not be allowed to do any operation(s) on owner goals.")
            Language.setMessage(mstrModuleName, 10, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.")
            Language.setMessage(mstrModuleName, 11, "You are about to commit the owner goals for the selected period.")
            Language.setMessage(mstrModuleName, 12, "Succesfully.")
            Language.setMessage(mstrModuleName, 13, "Period is compulsory information.Please select period.")
            Language.setMessage(mstrModuleName, 14, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 15, "Processing :")
            Language.setMessage(mstrModuleName, 16, "Operation completed successfully.")
            Language.setMessage(mstrModuleName, 17, "Enter reason to uncommit")
            Language.setMessage(mstrModuleName, 18, "Sorry, Unlock reason is mandaroty information.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class