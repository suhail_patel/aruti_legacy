﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalVoidAllEmpGoals
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalVoidAllEmpGoals))
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhPreview = New System.Windows.Forms.DataGridViewImageColumn
        Me.objcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlnkProcess = New System.Windows.Forms.LinkLabel
        Me.btnVoid = New eZee.Common.eZeeSplitButton
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lnkAdvFilter = New System.Windows.Forms.LinkLabel
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel2.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.tblpAssessorEmployee)
        Me.Panel2.Controls.Add(Me.objFooter)
        Me.Panel2.Controls.Add(Me.gbFilterCriteria)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(493, 492)
        Me.Panel2.TabIndex = 1
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(0, 69)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(493, 368)
        Me.tblpAssessorEmployee.TabIndex = 479
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(487, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvData)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(487, 336)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(32, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvData.ColumnHeadersHeight = 21
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhPreview, Me.objcolhCheck, Me.colhEmployee, Me.colhWeight, Me.objdgcolhEmpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(487, 336)
        Me.dgvData.TabIndex = 105
        '
        'objdgcolhPreview
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.NullValue = CType(resources.GetObject("DataGridViewCellStyle1.NullValue"), Object)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White
        Me.objdgcolhPreview.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhPreview.HeaderText = ""
        Me.objdgcolhPreview.Image = CType(resources.GetObject("objdgcolhPreview.Image"), System.Drawing.Image)
        Me.objdgcolhPreview.Name = "objdgcolhPreview"
        Me.objdgcolhPreview.ReadOnly = True
        Me.objdgcolhPreview.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhPreview.Width = 25
        '
        'objcolhCheck
        '
        Me.objcolhCheck.HeaderText = ""
        Me.objcolhCheck.Name = "objcolhCheck"
        Me.objcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhCheck.Width = 25
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhWeight
        '
        Me.colhWeight.HeaderText = "Weight"
        Me.colhWeight.Name = "colhWeight"
        Me.colhWeight.ReadOnly = True
        Me.colhWeight.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhWeight.Width = 70
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlnkProcess)
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 437)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(493, 55)
        Me.objFooter.TabIndex = 478
        '
        'objlnkProcess
        '
        Me.objlnkProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.objlnkProcess.Location = New System.Drawing.Point(12, 20)
        Me.objlnkProcess.Name = "objlnkProcess"
        Me.objlnkProcess.Size = New System.Drawing.Size(263, 16)
        Me.objlnkProcess.TabIndex = 9
        Me.objlnkProcess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(281, 13)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.ShowDefaultBorderColor = True
        Me.btnVoid.Size = New System.Drawing.Size(97, 30)
        Me.btnVoid.SplitButtonMenu = Nothing
        Me.btnVoid.TabIndex = 7
        Me.btnVoid.Text = "&Void"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(384, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAdvFilter)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(493, 69)
        Me.gbFilterCriteria.TabIndex = 488
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(465, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 485
        Me.objbtnReset.TabStop = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(10, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(72, 15)
        Me.lblPeriod.TabIndex = 476
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvFilter
        '
        Me.lnkAdvFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAdvFilter.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAdvFilter.Location = New System.Drawing.Point(308, 4)
        Me.lnkAdvFilter.Name = "lnkAdvFilter"
        Me.lnkAdvFilter.Size = New System.Drawing.Size(126, 17)
        Me.lnkAdvFilter.TabIndex = 487
        Me.lnkAdvFilter.TabStop = True
        Me.lnkAdvFilter.Text = "Advance Filter"
        Me.lnkAdvFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(88, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(367, 21)
        Me.cboPeriod.TabIndex = 477
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(440, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 484
        Me.objbtnSearch.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Weight"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmGlobalVoidAllEmpGoals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(493, 492)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalVoidAllEmpGoals"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Void All Employee Goals"
        Me.Panel2.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnVoid As eZee.Common.eZeeSplitButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objdgcolhPreview As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlnkProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAdvFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
End Class
