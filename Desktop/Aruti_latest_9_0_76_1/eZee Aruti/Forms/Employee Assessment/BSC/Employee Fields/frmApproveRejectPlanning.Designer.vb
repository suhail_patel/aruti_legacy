﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproveRejectPlanning
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproveRejectPlanning))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objSC1 = New System.Windows.Forms.SplitContainer
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objlblColon = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.lblComment = New System.Windows.Forms.Label
        Me.pnlResultInfo = New System.Windows.Forms.Panel
        Me.tbcPlanningView = New System.Windows.Forms.TabControl
        Me.tabpBSC = New System.Windows.Forms.TabPage
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.tabpCompetencies = New System.Windows.Forms.TabPage
        Me.lvAssignedCompetencies = New eZee.Common.eZeeListView(Me.components)
        Me.colhCompetencies = New System.Windows.Forms.ColumnHeader
        Me.colhWeight = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnFinalSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOpen_Changes = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.objSC1.Panel1.SuspendLayout()
        Me.objSC1.Panel2.SuspendLayout()
        Me.objSC1.SuspendLayout()
        Me.pnlResultInfo.SuspendLayout()
        Me.tbcPlanningView.SuspendLayout()
        Me.tabpBSC.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpCompetencies.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.Panel1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(924, 516)
        Me.pnlMain.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objSC1)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(924, 516)
        Me.Panel1.TabIndex = 1
        '
        'objSC1
        '
        Me.objSC1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objSC1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSC1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSC1.IsSplitterFixed = True
        Me.objSC1.Location = New System.Drawing.Point(0, 0)
        Me.objSC1.Name = "objSC1"
        Me.objSC1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSC1.Panel1
        '
        Me.objSC1.Panel1.Controls.Add(Me.objlblCaption)
        Me.objSC1.Panel1.Controls.Add(Me.objlblColon)
        Me.objSC1.Panel1.Controls.Add(Me.lblEmployee)
        '
        'objSC1.Panel2
        '
        Me.objSC1.Panel2.Controls.Add(Me.txtComments)
        Me.objSC1.Panel2.Controls.Add(Me.lblComment)
        Me.objSC1.Panel2.Controls.Add(Me.pnlResultInfo)
        Me.objSC1.Size = New System.Drawing.Size(924, 461)
        Me.objSC1.SplitterDistance = 34
        Me.objSC1.SplitterWidth = 2
        Me.objSC1.TabIndex = 15
        '
        'objlblCaption
        '
        Me.objlblCaption.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblCaption.Location = New System.Drawing.Point(107, 8)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(803, 15)
        Me.objlblCaption.TabIndex = 2
        Me.objlblCaption.Text = "##Value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblColon
        '
        Me.objlblColon.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objlblColon.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColon.Location = New System.Drawing.Point(91, 9)
        Me.objlblColon.Name = "objlblColon"
        Me.objlblColon.Size = New System.Drawing.Size(10, 13)
        Me.objlblColon.TabIndex = 1
        Me.objlblColon.Text = ":"
        Me.objlblColon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblEmployee.Location = New System.Drawing.Point(12, 8)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(97, 360)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtComments.Size = New System.Drawing.Size(813, 57)
        Me.txtComments.TabIndex = 17
        '
        'lblComment
        '
        Me.lblComment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblComment.Location = New System.Drawing.Point(10, 362)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(81, 16)
        Me.lblComment.TabIndex = 16
        Me.lblComment.Text = "Comments"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlResultInfo
        '
        Me.pnlResultInfo.Controls.Add(Me.tbcPlanningView)
        Me.pnlResultInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlResultInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlResultInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlResultInfo.Name = "pnlResultInfo"
        Me.pnlResultInfo.Size = New System.Drawing.Size(922, 354)
        Me.pnlResultInfo.TabIndex = 15
        '
        'tbcPlanningView
        '
        Me.tbcPlanningView.Controls.Add(Me.tabpBSC)
        Me.tbcPlanningView.Controls.Add(Me.tabpCompetencies)
        Me.tbcPlanningView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbcPlanningView.Location = New System.Drawing.Point(0, 0)
        Me.tbcPlanningView.Name = "tbcPlanningView"
        Me.tbcPlanningView.SelectedIndex = 0
        Me.tbcPlanningView.Size = New System.Drawing.Size(922, 354)
        Me.tbcPlanningView.TabIndex = 1
        '
        'tabpBSC
        '
        Me.tabpBSC.Controls.Add(Me.dgvData)
        Me.tabpBSC.Location = New System.Drawing.Point(4, 22)
        Me.tabpBSC.Name = "tabpBSC"
        Me.tabpBSC.Size = New System.Drawing.Size(914, 328)
        Me.tabpBSC.TabIndex = 0
        Me.tabpBSC.Text = "Employee Goals"
        Me.tabpBSC.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(914, 328)
        Me.dgvData.TabIndex = 6
        '
        'tabpCompetencies
        '
        Me.tabpCompetencies.Controls.Add(Me.lvAssignedCompetencies)
        Me.tabpCompetencies.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompetencies.Name = "tabpCompetencies"
        Me.tabpCompetencies.Size = New System.Drawing.Size(914, 328)
        Me.tabpCompetencies.TabIndex = 1
        Me.tabpCompetencies.Text = "Employee Competencies"
        Me.tabpCompetencies.UseVisualStyleBackColor = True
        '
        'lvAssignedCompetencies
        '
        Me.lvAssignedCompetencies.BackColorOnChecked = False
        Me.lvAssignedCompetencies.ColumnHeaders = Nothing
        Me.lvAssignedCompetencies.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCompetencies, Me.colhWeight})
        Me.lvAssignedCompetencies.CompulsoryColumns = ""
        Me.lvAssignedCompetencies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAssignedCompetencies.FullRowSelect = True
        Me.lvAssignedCompetencies.GridLines = True
        Me.lvAssignedCompetencies.GroupingColumn = Nothing
        Me.lvAssignedCompetencies.HideSelection = False
        Me.lvAssignedCompetencies.Location = New System.Drawing.Point(0, 0)
        Me.lvAssignedCompetencies.MinColumnWidth = 50
        Me.lvAssignedCompetencies.MultiSelect = False
        Me.lvAssignedCompetencies.Name = "lvAssignedCompetencies"
        Me.lvAssignedCompetencies.OptionalColumns = ""
        Me.lvAssignedCompetencies.ShowMoreItem = False
        Me.lvAssignedCompetencies.ShowSaveItem = False
        Me.lvAssignedCompetencies.ShowSelectAll = True
        Me.lvAssignedCompetencies.ShowSizeAllColumnsToFit = True
        Me.lvAssignedCompetencies.Size = New System.Drawing.Size(914, 328)
        Me.lvAssignedCompetencies.Sortable = True
        Me.lvAssignedCompetencies.TabIndex = 11
        Me.lvAssignedCompetencies.UseCompatibleStateImageBehavior = False
        Me.lvAssignedCompetencies.View = System.Windows.Forms.View.Details
        '
        'colhCompetencies
        '
        Me.colhCompetencies.Tag = "colhCompetencies"
        Me.colhCompetencies.Text = "Competencies"
        Me.colhCompetencies.Width = 760
        '
        'colhWeight
        '
        Me.colhWeight.Tag = "colhWeight"
        Me.colhWeight.Text = "Weight"
        Me.colhWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhWeight.Width = 70
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnFinalSave)
        Me.objFooter.Controls.Add(Me.btnOpen_Changes)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 461)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(924, 55)
        Me.objFooter.TabIndex = 13
        '
        'btnFinalSave
        '
        Me.btnFinalSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinalSave.BackColor = System.Drawing.Color.White
        Me.btnFinalSave.BackgroundImage = CType(resources.GetObject("btnFinalSave.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalSave.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalSave.FlatAppearance.BorderSize = 0
        Me.btnFinalSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalSave.ForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Location = New System.Drawing.Point(609, 13)
        Me.btnFinalSave.Name = "btnFinalSave"
        Me.btnFinalSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalSave.Size = New System.Drawing.Size(97, 30)
        Me.btnFinalSave.TabIndex = 9
        Me.btnFinalSave.Text = "&Approve"
        Me.btnFinalSave.UseVisualStyleBackColor = True
        '
        'btnOpen_Changes
        '
        Me.btnOpen_Changes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOpen_Changes.BackColor = System.Drawing.Color.White
        Me.btnOpen_Changes.BackgroundImage = CType(resources.GetObject("btnOpen_Changes.BackgroundImage"), System.Drawing.Image)
        Me.btnOpen_Changes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpen_Changes.BorderColor = System.Drawing.Color.Empty
        Me.btnOpen_Changes.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpen_Changes.FlatAppearance.BorderSize = 0
        Me.btnOpen_Changes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpen_Changes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen_Changes.ForeColor = System.Drawing.Color.Black
        Me.btnOpen_Changes.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpen_Changes.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpen_Changes.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen_Changes.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen_Changes.Location = New System.Drawing.Point(712, 13)
        Me.btnOpen_Changes.Name = "btnOpen_Changes"
        Me.btnOpen_Changes.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen_Changes.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen_Changes.Size = New System.Drawing.Size(97, 30)
        Me.btnOpen_Changes.TabIndex = 8
        Me.btnOpen_Changes.Text = "&Reject"
        Me.btnOpen_Changes.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(815, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmApproveRejectPlanning
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(924, 516)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproveRejectPlanning"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve/Reject BSC Planning"
        Me.pnlMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.objSC1.Panel1.ResumeLayout(False)
        Me.objSC1.Panel2.ResumeLayout(False)
        Me.objSC1.Panel2.PerformLayout()
        Me.objSC1.ResumeLayout(False)
        Me.pnlResultInfo.ResumeLayout(False)
        Me.tbcPlanningView.ResumeLayout(False)
        Me.tabpBSC.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpCompetencies.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objSC1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objlblColon As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents lblComment As System.Windows.Forms.Label
    Friend WithEvents pnlResultInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnFinalSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnOpen_Changes As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents tbcPlanningView As System.Windows.Forms.TabControl
    Friend WithEvents tabpBSC As System.Windows.Forms.TabPage
    Friend WithEvents tabpCompetencies As System.Windows.Forms.TabPage
    Friend WithEvents lvAssignedCompetencies As eZee.Common.eZeeListView
    Friend WithEvents colhCompetencies As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhWeight As System.Windows.Forms.ColumnHeader
End Class
