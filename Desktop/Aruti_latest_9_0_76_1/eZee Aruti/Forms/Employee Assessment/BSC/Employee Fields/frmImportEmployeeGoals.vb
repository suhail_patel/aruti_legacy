﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportEmployeeGoalsWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportEmployeeGoalsWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private mdtGridViewSource As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private mintImgRefId As Integer = 0
    Private mintLinkedFieldId As Integer = 0
    Private mdecMaxWeightAssigned As Decimal = 0

#End Region

#Region " Private Methods "

    'S.SANDEEP [23-OCT-2018] -- START
    'Private Sub GetFileFormat()
    '    'S.SANDEEP [12-Jan-2018] -- START
    '    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    '    'Dim IExcel As New ExcelData
    '    'S.SANDEEP [12-Jan-2018] -- END
    '    Dim dsList As New DataSet
    '    Dim path As String = String.Empty
    '    Dim strFilePath As String = String.Empty
    '    Dim dlgSaveFile As New SaveFileDialog
    '    Dim ObjFile As System.IO.FileInfo
    '    Try
    '        dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
    '        If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
    '            Dim dTable As New DataTable("Import_Format")

    '            'S.SANDEEP [08-DEC-2017] -- START
    '            'ISSUE/ENHANCEMENT :#0001717 {Import goals on desktop application is not working properly}
    '            'dTable.Columns.Add("Employee_Code") : dTable.Columns.Add("Period_Code") : dTable.Columns.Add("Perspective_Name")
    '            dTable.Columns.Add("Employee_Code") : dTable.Columns.Add("Period_Name") : dTable.Columns.Add("Perspective_Name")
    '            'S.SANDEEP [08-DEC-2017] -- END
    '            dTable.Columns.Add("Field_1") : dTable.Columns.Add("Field_2") : dTable.Columns.Add("Field_3")
    '            dTable.Columns.Add("Field_4") : dTable.Columns.Add("Field_5") : dTable.Columns.Add("Weight")
    '            dsList.Tables.Add(dTable.Copy)
    '            'S.SANDEEP [12-Jan-2018] -- START
    '            'ISSUE/ENHANCEMENT : REF-ID # 0001843
    '            'IExcel.Export(dlgSaveFile.FileName, dsList)
    '            OpenXML_Export(dlgSaveFile.FileName, dsList)
    '            'S.SANDEEP [12-Jan-2018] -- END
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Template Exported Successfully."), enMsgBoxStyle.Information)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetFileFormat", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub GetFileFormat()
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                Dim dTable As New DataTable("Import_Format")

                dTable.Columns.Add("Employee_Code") : dTable.Columns.Add("Period_Name") : dTable.Columns.Add("Perspective_Name")
                dTable.Columns.Add("Field_1") : dTable.Columns.Add("Field_2") : dTable.Columns.Add("Field_3")
                dTable.Columns.Add("Field_4") : dTable.Columns.Add("Field_5") : dTable.Columns.Add("Weight")
                'S.SANDEEP [23-OCT-2018] -- START
                dTable.Columns.Add("GoalType") : dTable.Columns.Add("GoalValue")
                'S.SANDEEP [23-OCT-2018] -- END

                'S.SANDEEP |16-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                dTable.Columns.Add("UoMType")
                'S.SANDEEP |16-JUL-2019| -- END

                dTable.Columns.Add("StartDate") : dTable.Columns.Add("EndDate")

                dsList.Tables.Add(dTable.Copy)
                OpenXML_Export(dlgSaveFile.FileName, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFileFormat", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [23-OCT-2018] -- END



    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("employcode", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("periodcode", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("perspective", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("field1", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("field2", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("field3", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("field4", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("field5", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("weight", GetType(System.Decimal)).DefaultValue = 0
            'S.SANDEEP [23-OCT-2018] -- START
            mdt_ImportData_Others.Columns.Add("goaltype", GetType(System.Int32)).DefaultValue = 0
            mdt_ImportData_Others.Columns.Add("goalvalue", GetType(System.Decimal)).DefaultValue = 0
            'S.SANDEEP [23-OCT-2018] -- END

            'S.SANDEEP |16-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
            mdt_ImportData_Others.Columns.Add("uomtype", GetType(System.String)).DefaultValue = ""
            'S.SANDEEP |16-JUL-2019| -- END

            mdt_ImportData_Others.Columns.Add("stdate", GetType(System.DateTime)).DefaultValue = Nothing
            mdt_ImportData_Others.Columns.Add("eddate", GetType(System.DateTime)).DefaultValue = Nothing

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = Nothing
            dtTemp = mds_ImportData.Tables(0).Select("" & cboEmployee_Code.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            Dim img As Image = New Drawing.Bitmap(1, 1).Clone
            mds_ImportData.AcceptChanges()
            Dim drNewRow As DataRow = Nothing
            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow()

                drNewRow("employcode") = dtRow(cboEmployee_Code.Text).ToString
                drNewRow("periodcode") = dtRow(cboPeriod_Name.Text).ToString
                drNewRow("perspective") = dtRow(cboPerspective_Name.Text).ToString
                If cboField_1.Enabled = True Then drNewRow("field1") = dtRow(cboField_1.Text).ToString
                If cboField_2.Enabled = True Then drNewRow("field2") = dtRow(cboField_2.Text).ToString
                If cboField_3.Enabled = True Then drNewRow("field3") = dtRow(cboField_3.Text).ToString
                If cboField_4.Enabled = True Then drNewRow("field4") = dtRow(cboField_4.Text).ToString
                If cboField_5.Enabled = True Then drNewRow("field5") = dtRow(cboField_5.Text).ToString
                Try
                    drNewRow("weight") = Convert.ToDecimal(dtRow(cboWeight.Text).ToString)
                Catch ex As Exception
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Invalid data set on weigh column in the file."), enMsgBoxStyle.Information)
                    Exit Sub
                End Try
                'S.SANDEEP [23-OCT-2018] -- START
                Try
                    drNewRow("goaltype") = Convert.ToInt32(dtRow(cboGoalType.Text).ToString)
                Catch ex As Exception
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Invalid data set on goal type column in the file Please set value to either (1 or 2)."), enMsgBoxStyle.Information)
                    Exit Sub
                End Try
                Try
                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    'drNewRow("goalvalue") = Convert.ToInt32(dtRow(cboGoalValue.Text).ToString)
                    drNewRow("goalvalue") = Convert.ToDecimal(dtRow(cboGoalValue.Text).ToString)
                    'S.SANDEEP |16-JUL-2019| -- END
                Catch ex As Exception
                    If drNewRow("goaltype") = 2 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 53, "Invalid data set on goal value column in the file Please set value greater than zero."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End Try
                'S.SANDEEP [23-OCT-2018] -- END

                'S.SANDEEP |16-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                drNewRow("uomtype") = dtRow(cboUomType.Text).ToString
                If drNewRow("goaltype") = 2 Then
                    If drNewRow("uomtype").ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, UoM cannot be blank when goal type is '2' i.e Quantitative."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |16-JUL-2019| -- END

                If cboStartDate.Text <> "" Then
                    If dtRow(cboStartDate.Text).ToString.Trim.Length > 0 Then
                        If IsDate(dtRow(cboStartDate.Text).ToString) = True Then
                            drNewRow("stdate") = CDate(dtRow(cboStartDate.Text))
                        Else
                            drNewRow("stdate") = DBNull.Value
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 201, "Sorry, Invalid Start Date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If

                If cboEndDate.Text <> "" Then
                    If dtRow(cboEndDate.Text).ToString.Trim.Length > 0 Then
                        If IsDate(dtRow(cboEndDate.Text).ToString) = True Then
                            drNewRow("eddate") = CDate(dtRow(cboEndDate.Text))
                        Else
                            drNewRow("eddate") = DBNull.Value
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 202, "Sorry, Invalid End Date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If                    
                End If

                If IsDBNull(drNewRow("eddate")) = False AndAlso IsDBNull(drNewRow("stdate")) = False Then
                    If CDate(drNewRow("eddate")) < CDate(drNewRow("stdate")) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 203, "Sorry, End Date cannot be less than the Start Date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                drNewRow.Item("image") = img
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
            Next

            If blnIsNotThrown = True Then
                mdtGridViewSource = mdt_ImportData_Others.DefaultView.ToTable(True, "employcode", "image", "message", "status", "objStatus").Copy
                objTotal.Text = mdtGridViewSource.Rows.Count
            End If


            If blnIsNotThrown = True Then
                dgData.AutoGenerateColumns = False
                colhEmployee.DataPropertyName = "employcode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dvGriddata = New DataView(mdtGridViewSource)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            ezeeWizardImportEmployeeGoal.BackEnabled = False
            ezeeWizardImportEmployeeGoal.CancelText = "Finish"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub EnableDisableField()
        Try
            Dim objfieldmst As New clsAssess_Field_Master(True)
            If objfieldmst._Field1_Caption.Trim.Length <= 0 Then
                objlblASign5.Enabled = False : lblField1.Enabled = False : cboField_1.Enabled = False
            End If
            If objfieldmst._Field2_Caption.Trim.Length <= 0 Then
                objlblASign6.Enabled = False : lblField2.Enabled = False : cboField_2.Enabled = False
            End If
            If objfieldmst._Field3_Caption.Trim.Length <= 0 Then
                objlblASign7.Enabled = False : lblField3.Enabled = False : cboField_3.Enabled = False
            End If
            If objfieldmst._Field4_Caption.Trim.Length <= 0 Then
                objlblASign8.Enabled = False : lblField4.Enabled = False : cboField_4.Enabled = False
            End If
            If objfieldmst._Field5_Caption.Trim.Length <= 0 Then
                objlblASign9.Enabled = False : lblField5.Enabled = False : cboField_5.Enabled = False
            End If
            objfieldmst = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisableField", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Get_Assessor(ByVal intEmpid As Integer, ByRef iAssessorEmpId As Integer) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet
            Dim objEvalutionMst As New clsevaluation_analysis_master
            dLst = objEvalutionMst.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        True, False, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False, intEmpid)
            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                iAssessorEmpId = objEvalutionMst.GetAssessorEmpId(iAssessor)
            End If

            objEvalutionMst = Nothing

            Return iAssessor

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Assessor", mstrModuleName)
        End Try
    End Function

    Private Function CheckInvalidData(Optional ByVal dtRow As DataRow = Nothing) As Boolean
        Try
            If dtRow IsNot Nothing Then
                With dtRow
                    If .Item(cboEmployee_Code.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If .Item(cboPeriod_Name.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period Code cannot be blank. Please set Period Code in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If .Item(cboPerspective_Name.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Perspective cannot be blank. Please set Perspective in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If cboField_1.Enabled = True AndAlso .Item(cboField_1.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Field 1 cannot be blank. Please set Field 1 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If cboField_2.Enabled = True AndAlso .Item(cboField_2.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Field 2 cannot be blank. Please set Field 2 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If cboField_3.Enabled = True AndAlso .Item(cboField_3.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Field 3 cannot be blank. Please set Field 3 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If cboField_4.Enabled = True AndAlso .Item(cboField_4.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Field 4 cannot be blank. Please set Field 4 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If cboField_5.Enabled = True AndAlso .Item(cboField_5.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Field 5 cannot be blank. Please set Field 5 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If .Item(cboWeight.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Weight cannot be blank. Please set Weight in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    'S.SANDEEP [23-OCT-2018] -- START
                    If .Item(cboGoalType.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Goal Type cannot be blank. Please set Goal Type in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                        Return False
                    Else
                        Dim intValue As Integer = 0
                        Integer.TryParse(.Item(cboGoalType.Text), intValue)
                        Select Case intValue
                            Case 1, 2
                            Case Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Goal Type cannot be blank. Please set Goal Type in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                                Return False
                        End Select
                    End If

                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    If .Item(cboUomType.Text).ToString.Trim.Length <= 0 Then
                        Dim intValue As Integer = 0
                        Integer.TryParse(.Item(cboGoalType.Text), intValue)
                        If intValue = 2 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "UoM type cannot be blank. Please set UoM type in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    Else
                        Dim intValue As Integer = 0
                        Integer.TryParse(.Item(cboGoalType.Text), intValue)
                        Select Case intValue
                            Case 1
                                If .Item(cboUomType.Text).ToString.Trim.Length > 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "UoM type cannot be set when goal type is '1', Please remove UoM type where goal type is '1' in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                            Case 2
                                If .Item(cboUomType.Text).ToString.Trim.Length <= 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "UoM type cannot be blank. Please set UoM type in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                        End Select
                    End If
                    'S.SANDEEP |16-JUL-2019| -- END

                    If .Item(cboGoalValue.Text).ToString.Trim.Length <= 0 Then
                        Dim intValue As Integer = 0
                        Integer.TryParse(.Item(cboGoalType.Text), intValue)
                        Select Case intValue
                            Case 2
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Goal Value cannot be blank. Please set Goal Value in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                                Return False
                        End Select                       
                    Else
                        Dim intValue As Integer = 0
                        Integer.TryParse(.Item(cboGoalType.Text), intValue)
                        Select Case intValue
                            Case 1
                                If .Item(cboGoalValue.Text).ToString.Trim.Length > 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 54, "Goal value cannot be set when goal type is '1', Please remove goal value where goal type is '1' in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                            Case 2
                                If .Item(cboGoalValue.Text).ToString.Trim.Length <= 0 Then
                                    Dim decValue As Decimal = 0
                                    Integer.TryParse(.Item(cboGoalValue.Text), decValue)
                                    If decValue <= 0 Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Goal Value cannot be blank. Please set Goal Value in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                                        Return False
                                    End If
                                End If
                        End Select
                    End If
                    'S.SANDEEP [23-OCT-2018] -- END
                End With
            Else
                If (cboEmployee_Code.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If (cboPeriod_Name.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period Code cannot be blank. Please set Period Code in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If (cboPerspective_Name.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Perspective cannot be blank. Please set Perspective in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboField_1.Enabled = True AndAlso (cboField_1.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Field 1 cannot be blank. Please set Field 1 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboField_2.Enabled = True AndAlso (cboField_2.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Field 2 cannot be blank. Please set Field 2 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboField_3.Enabled = True AndAlso (cboField_3.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Field 3 cannot be blank. Please set Field 3 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboField_4.Enabled = True AndAlso (cboField_4.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Field 4 cannot be blank. Please set Field 4 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboField_5.Enabled = True AndAlso (cboField_5.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Field 5 cannot be blank. Please set Field 5 in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If (cboWeight.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Weight cannot be blank. Please set Weight in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP [23-OCT-2018] -- START
                If (cboGoalType.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Goal Type cannot be blank. Please set Goal Type in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If (cboGoalValue.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Goal Value cannot be blank. Please set Goal Value in order to import Employee Goal(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP [23-OCT-2018] -- END
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In pnlGoalMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next
            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployee_Code.Items.Add(dtColumns.ColumnName)
                cboPeriod_Name.Items.Add(dtColumns.ColumnName)
                cboPerspective_Name.Items.Add(dtColumns.ColumnName)
                cboField_1.Items.Add(dtColumns.ColumnName)
                cboField_2.Items.Add(dtColumns.ColumnName)
                cboField_3.Items.Add(dtColumns.ColumnName)
                cboField_4.Items.Add(dtColumns.ColumnName)
                cboField_5.Items.Add(dtColumns.ColumnName)
                cboWeight.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [23-OCT-2018] -- START
                cboGoalType.Items.Add(dtColumns.ColumnName)
                cboGoalValue.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP [23-OCT-2018] -- END

                'S.SANDEEP |16-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                cboUomType.Items.Add(dtColumns.ColumnName)
                'S.SANDEEP |16-JUL-2019| -- END
                cboStartDate.Items.Add(dtColumns.ColumnName)
                cboEndDate.Items.Add(dtColumns.ColumnName)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim intEmpId, intPeriodId, intPerspId As Integer
            Dim intEmpFld1, intEmpFld2, intEmpFld3, intEmpFld4, intEmpFld5 As Integer
            Dim intLinkedField As Integer = 0
            Dim iWeight1, iWeight2, iWeight3, iWeight4, iWeight5 As Decimal
            'S.SANDEEP [23-OCT-2018] -- START
            Dim iGoalType1, iGoalType2, iGoalType3, iGoalType4, iGoalType5 As Integer
            Dim iGoalValue1, iGoalValue2, iGoalValue3, iGoalValue4, iGoalValue5 As Decimal
            'S.SANDEEP [23-OCT-2018] -- END

            Dim iStDate1, iStDate2, iStDate3, iStDate4, iStDate5 As Date
            Dim iEdDate1, iEdDate2, iEdDate3, iEdDate4, iEdDate5 As Date

            'S.SANDEEP |16-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
            Dim iUoMTypeId1, iUoMTypeId2, iUoMTypeId3, iUoMTypeId4, iUoMTypeId5, iCUoMTypeId As Integer
            'S.SANDEEP |16-JUL-2019| -- END

            Dim objEmp As New clsEmployee_Master
            Dim objPrd As New clscommom_period_Tran
            Dim objPerspective As New clsassess_perspective_master
            Dim objEmpFld1 As New clsassess_empfield1_master
            Dim objEmpFld2 As New clsassess_empfield2_master
            Dim objEmpFld3 As New clsassess_empfield3_master
            Dim objEmpFld4 As New clsassess_empfield4_master
            Dim objEmpFld5 As New clsassess_empfield5_master
            Dim objFldMapping As New clsAssess_Field_Mapping
            Dim objFldMaster As New clsAssess_Field_Master
            Dim iDicFinalStatus As New Dictionary(Of String, String)
            Dim strPrevPeriodCode As String = String.Empty
            Dim blnIsInserted As Boolean = False
            For Each row As DataRow In mdtGridViewSource.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(row) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                Dim dtFilter As DataTable = mdt_ImportData_Others.Select("employcode = '" & row("employcode") & "'").CopyToDataTable()
                Dim blnIsValid As Boolean = False
                blnIsInserted = False
                For Each dtRow As DataRow In dtFilter.Rows
                    Application.DoEvents() : ezWait.Refresh()
                    'Try
                    '    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    '    Application.DoEvents()
                    '    ezWait.Refresh()
                    'Catch ex As Exception
                    'End Try
                    intEmpId = 0 : intPeriodId = 0 : intPerspId = 0
                    intEmpFld1 = 0 : intEmpFld2 = 0 : intEmpFld3 = 0 : intEmpFld4 = 0 : intLinkedField = 0 : intEmpFld5 = 0
                    iWeight1 = 0 : iWeight2 = 0 : iWeight3 = 0 : iWeight4 = 0 : iWeight5 = 0

                    'S.SANDEEP [23-OCT-2018] -- START
                    iGoalType1 = 0 : iGoalType2 = 0 : iGoalType3 = 0 : iGoalType4 = 0 : iGoalType5 = 0
                    iGoalValue1 = 0 : iGoalValue2 = 0 : iGoalValue3 = 0 : iGoalValue4 = 0 : iGoalValue5 = 0
                    'S.SANDEEP [23-OCT-2018] -- END

                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    iCUoMTypeId = 0 : iUoMTypeId1 = 0 : iUoMTypeId2 = 0 : iUoMTypeId3 = 0 : iUoMTypeId4 = 0 : iUoMTypeId5 = 0
                    'S.SANDEEP |16-JUL-2019| -- END

                    iStDate1 = Nothing : iEdDate1 = Nothing : iStDate2 = Nothing : iEdDate2 = Nothing
                    iStDate3 = Nothing : iEdDate3 = Nothing : iStDate4 = Nothing : iEdDate4 = Nothing
                    iStDate5 = Nothing : iEdDate5 = Nothing

                    If dtRow("employcode").ToString.Trim.Length > 0 Then
                        intEmpId = objEmp.GetEmployeeUnkid("", dtRow("employcode").ToString.Trim)
                        If intEmpId <= 0 Then
                            row("image") = imgError
                            row("message") = Language.getMessage(mstrModuleName, 100, "Employee Not Found.")
                            row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                            row("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Exit For
                        End If
                    End If

                    If dtRow("periodcode").ToString.Trim.Length > 0 Then
                        intPeriodId = objPrd.GetPeriodByName(dtRow("periodcode").ToString.Trim, enModuleReference.Assessment)
                        If intPeriodId <= 0 Then
                            row("image") = imgError
                            row("message") = Language.getMessage(mstrModuleName, 102, "Period Not Found.")
                            row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                            row("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Exit For
                        End If
                    End If

                    If dtRow("perspective").ToString.Trim.Length > 0 Then
                        intPerspId = objPerspective.GetPerspectiveId(dtRow("perspective").ToString.Trim)
                        If intPerspId <= 0 Then
                            row("image") = imgError
                            row("message") = Language.getMessage(mstrModuleName, 103, "Perspective Not Found.")
                            row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                            row("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Exit For
                        End If
                    End If

                    If intLinkedField <= 0 Then
                        intLinkedField = objFldMapping.Get_Map_FieldId(intPeriodId)
                        If intLinkedField <= 0 Then
                            row("image") = imgError
                            row("message") = Language.getMessage(mstrModuleName, 104, "No Linked Field Found in period mentioned in File.")
                            row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                            row("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Exit For
                        End If
                    End If

                    If strPrevPeriodCode <> dtRow("periodcode") Then
                        blnIsValid = False
                        strPrevPeriodCode = dtRow("periodcode")
                    End If

                    If blnIsValid = False Then
                        Dim iMappingId As Integer = objFldMapping.Get_MappingUnkId(intPeriodId)
                        If iMappingId > 0 Then
                            Dim weight As Decimal = 0
                            objFldMapping._Mappingunkid = iMappingId
                            weight = dtFilter.Compute("SUM(weight)", "periodcode = '" & dtRow("periodcode") & "'")
                            If weight > objFldMapping._Weight Then
                                blnIsValid = True
                                row("image") = imgError
                                row("message") = Language.getMessage(mstrModuleName, 105, "Sorry, weight total either exceeding or it's not adding up to the total weight defined in the system.")
                                row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                                row("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit For
                            End If
                        End If
                    End If

                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    If dtRow("uomtype").ToString.Trim.Length > 0 Then
                        Dim objCMaster As New clsCommon_Master
                        iCUoMTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.GOAL_UNIT_OF_MEASURE, dtRow("uomtype").ToString.Trim)
                        If iCUoMTypeId <= 0 Then
                            row("image") = imgError
                            row("message") = Language.getMessage(mstrModuleName, 111, "Unit Of Measure Not Found.")
                            row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                            row("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Exit For
                        End If
                        objCMaster = Nothing
                    End If
                    'S.SANDEEP |16-JUL-2019| -- END

                    Select Case intLinkedField
                        Case enWeight_Types.WEIGHT_FIELD1
                            iWeight1 = Convert.ToDecimal(dtRow("weight"))
                            'S.SANDEEP [23-OCT-2018] -- START
                            iGoalType1 = Convert.ToDecimal(dtRow("goaltype"))
                            iGoalValue1 = Convert.ToDecimal(dtRow("goalvalue"))
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            iUoMTypeId1 = iCUoMTypeId
                            'S.SANDEEP |16-JUL-2019| -- END

                            If IsDBNull(dtRow("stdate")) = False Then iStDate1 = dtRow("stdate")
                            If IsDBNull(dtRow("eddate")) = False Then iEdDate1 = dtRow("eddate")

                        Case enWeight_Types.WEIGHT_FIELD2
                            iWeight2 = Convert.ToDecimal(dtRow("weight"))
                            'S.SANDEEP [23-OCT-2018] -- START
                            iGoalType2 = Convert.ToDecimal(dtRow("goaltype"))
                            iGoalValue2 = Convert.ToDecimal(dtRow("goalvalue"))
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            iUoMTypeId2 = iCUoMTypeId
                            'S.SANDEEP |16-JUL-2019| -- END

                            If IsDBNull(dtRow("stdate")) = False Then iStDate2 = dtRow("stdate")
                            If IsDBNull(dtRow("eddate")) = False Then iEdDate2 = dtRow("eddate")

                        Case enWeight_Types.WEIGHT_FIELD3
                            iWeight3 = Convert.ToDecimal(dtRow("weight"))
                            'S.SANDEEP [23-OCT-2018] -- START
                            iGoalType3 = Convert.ToDecimal(dtRow("goaltype"))
                            iGoalValue3 = Convert.ToDecimal(dtRow("goalvalue"))
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            iUoMTypeId3 = iCUoMTypeId
                            'S.SANDEEP |16-JUL-2019| -- END

                            If IsDBNull(dtRow("stdate")) = False Then iStDate3 = dtRow("stdate")
                            If IsDBNull(dtRow("eddate")) = False Then iEdDate3 = dtRow("eddate")

                        Case enWeight_Types.WEIGHT_FIELD4
                            iWeight4 = Convert.ToDecimal(dtRow("weight"))
                            'S.SANDEEP [23-OCT-2018] -- START
                            iGoalType4 = Convert.ToDecimal(dtRow("goaltype"))
                            iGoalValue4 = Convert.ToDecimal(dtRow("goalvalue"))
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            iUoMTypeId4 = iCUoMTypeId
                            'S.SANDEEP |16-JUL-2019| -- END

                            If IsDBNull(dtRow("stdate")) = False Then iStDate4 = dtRow("stdate")
                            If IsDBNull(dtRow("eddate")) = False Then iEdDate4 = dtRow("eddate")

                        Case enWeight_Types.WEIGHT_FIELD5
                            iWeight5 = Convert.ToDecimal(dtRow("weight"))
                            'S.SANDEEP [23-OCT-2018] -- START
                            iGoalType5 = Convert.ToDecimal(dtRow("goaltype"))
                            iGoalValue5 = Convert.ToDecimal(dtRow("goalvalue"))
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            iUoMTypeId5 = iCUoMTypeId
                            'S.SANDEEP |16-JUL-2019| -- END

                            If IsDBNull(dtRow("stdate")) = False Then iStDate5 = dtRow("stdate")
                            If IsDBNull(dtRow("eddate")) = False Then iEdDate5 = dtRow("eddate")

                    End Select

                    'If objFldMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, intPeriodId, 0, intEmpId) <> "" Then
                    '    row("image") = imgError
                    '    row("message") = Language.getMessage(mstrModuleName, 105, "Sorry, weight total either exceeding or it's not adding up to the total weight defined in the system.")
                    '    row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                    '    row("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Exit For
                    'End If

                    If iDicFinalStatus.ContainsKey(intEmpId.ToString & "~" & intPeriodId) = False Then
                        iDicFinalStatus.Add(intEmpId.ToString & "~" & intPeriodId, intEmpId.ToString & "~" & intPeriodId)
                    End If

                    If dtRow("field1").ToString.Trim.Length > 0 Then
                        intEmpFld1 = objEmpFld1.GetFieldDataId(dtRow("field1").ToString.Trim, intPeriodId, intEmpId, enWeight_Types.WEIGHT_FIELD1, 0)
                        If intEmpFld1 <= 0 Then
                            objEmpFld1._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                            objEmpFld1._Employeeunkid = intEmpId
                            objEmpFld1._Enddate = iEdDate1
                            objEmpFld1._Field_Data = dtRow("field1").ToString.Trim
                            objEmpFld1._Fieldunkid = enWeight_Types.WEIGHT_FIELD1
                            objEmpFld1._Isfinal = False
                            objEmpFld1._Isvoid = False
                            objEmpFld1._Owrfield1unkid = 0
                            objEmpFld1._Pct_Completed = 0
                            objEmpFld1._Periodunkid = intPeriodId
                            objEmpFld1._Startdate = iStDate1
                            objEmpFld1._Statusunkid = 0
                            objEmpFld1._Userunkid = User._Object._Userunkid
                            objEmpFld1._Voiddatetime = Nothing
                            objEmpFld1._Voidreason = ""
                            objEmpFld1._Voiduserunkid = -1
                            objEmpFld1._Weight = iWeight1
                            objEmpFld1._Yearunkid = 0
                            objEmpFld1._Perspectiveunkid = intPerspId
                            'S.SANDEEP [23-OCT-2018] -- START
                            objEmpFld1._GoalTypeid = iGoalType1
                            objEmpFld1._GoalValue = iGoalValue1
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            objEmpFld1._UnitOfMeasure = iUoMTypeId1
                            'S.SANDEEP |16-JUL-2019| -- END

                            If objEmpFld1.Insert(Nothing, Nothing) = False Then
                                row("image") = imgError
                                row("message") = objEmpFld1._Message
                                row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                                row("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit For
                            Else
                                intEmpFld1 = objEmpFld1._Empfield1unkid
                                blnIsInserted = True
                            End If
                        End If
                    End If
                    If dtRow("field2").ToString.Trim.Length > 0 Then
                        intEmpFld2 = objEmpFld1.GetFieldDataId(dtRow("field2").ToString.Trim, intPeriodId, intEmpId, enWeight_Types.WEIGHT_FIELD2, intEmpFld1)
                        If intEmpFld2 <= 0 Then
                            objEmpFld2._Empfield1unkid = intEmpFld1
                            objEmpFld2._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                            objEmpFld2._Field_Data = dtRow("field2").ToString.Trim
                            objEmpFld2._Fieldunkid = enWeight_Types.WEIGHT_FIELD2
                            objEmpFld2._Isvoid = False
                            objEmpFld2._Pct_Completed = 0
                            objEmpFld2._Startdate = iStDate2
                            objEmpFld2._Enddate = iEdDate2
                            objEmpFld2._Statusunkid = 0
                            objEmpFld2._Userunkid = User._Object._Userunkid
                            objEmpFld2._Voiddatetime = Nothing
                            objEmpFld2._Voidreason = ""
                            objEmpFld2._Voiduserunkid = -1
                            objEmpFld2._Weight = iWeight2
                            objEmpFld2._Employeeunkid = intEmpId
                            objEmpFld2._Periodunkid = intPeriodId
                            'S.SANDEEP [23-OCT-2018] -- START
                            objEmpFld2._GoalTypeid = iGoalType2
                            objEmpFld2._GoalValue = iGoalValue2
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            objEmpFld2._UnitOfMeasure = iUoMTypeId2
                            'S.SANDEEP |16-JUL-2019| -- END

                            If objEmpFld2.Insert(Nothing, Nothing) = False Then
                                row("image") = imgError
                                row("message") = objEmpFld2._Message
                                row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                                row("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit For
                            Else
                                intEmpFld2 = objEmpFld2._Empfield2unkid
                                blnIsInserted = True
                            End If
                        End If
                    End If
                    If dtRow("field3").ToString.Trim.Length > 0 Then
                        intEmpFld3 = objEmpFld1.GetFieldDataId(dtRow("field3").ToString.Trim, intPeriodId, intEmpId, enWeight_Types.WEIGHT_FIELD3, intEmpFld2)
                        If intEmpFld3 <= 0 Then
                            objEmpFld3._Empfield2unkid = intEmpFld2
                            objEmpFld3._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                            objEmpFld3._Enddate = iEdDate3
                            objEmpFld3._Field_Data = dtRow("field3").ToString.Trim
                            objEmpFld3._Fieldunkid = enWeight_Types.WEIGHT_FIELD3
                            objEmpFld3._Isvoid = False
                            objEmpFld3._Pct_Completed = 0
                            objEmpFld3._Startdate = iStDate3
                            objEmpFld3._Statusunkid = 0
                            objEmpFld3._Userunkid = User._Object._Userunkid
                            objEmpFld3._Voiddatetime = Nothing
                            objEmpFld3._Voidreason = ""
                            objEmpFld3._Voiduserunkid = -1
                            objEmpFld3._Weight = iWeight3
                            objEmpFld3._Employeeunkid = intEmpId
                            objEmpFld3._Periodunkid = intPeriodId
                            'S.SANDEEP [23-OCT-2018] -- START
                            objEmpFld3._GoalTypeid = iGoalType3
                            objEmpFld3._GoalValue = iGoalValue3
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            objEmpFld3._UnitOfMeasure = iUoMTypeId3
                            'S.SANDEEP |16-JUL-2019| -- END

                            If objEmpFld3.Insert(Nothing, Nothing) = False Then
                                row("image") = imgError
                                row("message") = objEmpFld3._Message
                                row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                                row("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit For
                            Else
                                intEmpFld3 = objEmpFld3._Empfield3unkid
                                blnIsInserted = True
                            End If
                        End If
                    End If
                    If dtRow("field4").ToString.Trim.Length > 0 Then
                        intEmpFld4 = objEmpFld1.GetFieldDataId(dtRow("field4").ToString.Trim, intPeriodId, intEmpId, enWeight_Types.WEIGHT_FIELD4, intEmpFld3)
                        If intEmpFld4 <= 0 Then
                            objEmpFld4._Empfield3unkid = intEmpFld3
                            objEmpFld4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                            objEmpFld4._Enddate = iEdDate4
                            objEmpFld4._Field_Data = dtRow("field4").ToString.Trim
                            objEmpFld4._Fieldunkid = enWeight_Types.WEIGHT_FIELD4
                            objEmpFld4._Isvoid = False
                            objEmpFld4._Pct_Completed = 0
                            objEmpFld4._Startdate = iStDate4
                            objEmpFld4._Statusunkid = 0
                            objEmpFld4._Userunkid = User._Object._Userunkid
                            objEmpFld4._Voiddatetime = Nothing
                            objEmpFld4._Voidreason = ""
                            objEmpFld4._Voiduserunkid = -1
                            objEmpFld4._Weight = iWeight4
                            objEmpFld4._Employeeunkid = intEmpId
                            objEmpFld4._Periodunkid = intPeriodId
                            'S.SANDEEP [23-OCT-2018] -- START
                            objEmpFld4._GoalTypeid = iGoalType4
                            objEmpFld4._GoalValue = iGoalValue4
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            objEmpFld4._UnitOfMeasure = iUoMTypeId4
                            'S.SANDEEP |16-JUL-2019| -- END

                            If objEmpFld4.Insert(Nothing, Nothing) = False Then
                                row("image") = imgError
                                row("message") = objEmpFld4._Message
                                row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                                row("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit For
                            Else
                                intEmpFld4 = objEmpFld4._Empfield4unkid
                                blnIsInserted = True
                            End If
                        End If
                    End If
                    If dtRow("field5").ToString.Trim.Length > 0 Then
                        intEmpFld5 = objEmpFld1.GetFieldDataId(dtRow("field5").ToString.Trim, intPeriodId, intEmpId, enWeight_Types.WEIGHT_FIELD5, intEmpFld4)
                        If intEmpFld5 <= 0 Then
                            objEmpFld5._Empfield4unkid = intEmpFld4
                            objEmpFld5._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD5
                            objEmpFld5._Enddate = iEdDate5
                            objEmpFld5._Field_Data = dtRow("field5").ToString.Trim
                            objEmpFld5._Fieldunkid = enWeight_Types.WEIGHT_FIELD5
                            objEmpFld5._Isvoid = False
                            objEmpFld5._Pct_Completed = 0
                            objEmpFld5._Startdate = iStDate5
                            objEmpFld5._Statusunkid = 0
                            objEmpFld5._Userunkid = User._Object._Userunkid
                            objEmpFld5._Voiddatetime = Nothing
                            objEmpFld5._Voidreason = ""
                            objEmpFld5._Voiduserunkid = -1
                            objEmpFld5._Weight = iWeight5
                            objEmpFld5._Employeeunkid = intEmpId
                            objEmpFld5._Periodunkid = intPeriodId
                            'S.SANDEEP [23-OCT-2018] -- START
                            objEmpFld5._GoalTypeid = iGoalType5
                            objEmpFld5._GoalValue = iGoalValue5
                            'S.SANDEEP [23-OCT-2018] -- END

                            'S.SANDEEP |16-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                            objEmpFld5._UnitOfMeasure = iUoMTypeId5
                            'S.SANDEEP |16-JUL-2019| -- END

                            If objEmpFld5.Insert(Nothing, Nothing) = False Then
                                row("image") = imgError
                                row("message") = objEmpFld5._Message
                                row("status") = Language.getMessage(mstrModuleName, 101, "Fail")
                                row("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Exit For
                            Else
                                blnIsInserted = True
                            End If
                        End If
                    End If
                Next
                If blnIsInserted Then
                    If row("status").ToString.Trim.Length <= 0 Then
                        row("image") = imgAccept
                        row("message") = ""
                        row("status") = Language.getMessage(mstrModuleName, 106, "Success")
                        row("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    End If
                Else
                    If row("status").ToString.Trim.Length <= 0 Then
                        row("image") = imgWarring
                        row("message") = ""
                        row("status") = Language.getMessage(mstrModuleName, 107, "Already Exists")
                        row("objStatus") = 0
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    End If
                End If
            Next

            If blnIsInserted Then
                If chkImportWithApprovedStatus.Checked = True Then
                    If iDicFinalStatus.Keys.Count > 0 Then
                        objlblGoalApproval.Visible = True
                        For Each iKey As String In iDicFinalStatus.Keys
                            Application.DoEvents()
                            Dim iAssessorMasterId, iAssessorEmpId As Integer
                            iAssessorMasterId = 0 : iAssessorEmpId = 0
                            iAssessorMasterId = Get_Assessor(CInt(iKey.Split("~")(0)), iAssessorEmpId)

                            Dim objStatus As New clsassess_empstatus_tran
                            objStatus._Assessoremployeeunkid = iAssessorEmpId
                            objStatus._Assessormasterunkid = iAssessorMasterId
                            objStatus._Commtents = ""
                            objStatus._Employeeunkid = CInt(iKey.Split("~")(0))
                            objStatus._Periodunkid = CInt(iKey.Split("~")(1))
                            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                            objStatus._Statustypeid = enObjective_Status.FINAL_SAVE
                            objStatus._Isunlock = False
                            objStatus._Userunkid = User._Object._Userunkid
                            If objStatus.Insert(, ConfigParameter._Object._Self_Assign_Competencies) = False Then
                                eZeeMsgBox.Show(objStatus._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            objStatus = Nothing
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            objlblGoalApproval.Visible = False
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub ezeeWizardImportEmployeeGoal_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles ezeeWizardImportEmployeeGoal.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case ezeeWizardImportEmployeeGoal.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ezeeWizardImportEmployeeGoal_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ezeeWizardImportEmployeeGoal_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles ezeeWizardImportEmployeeGoal.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case ezeeWizardImportEmployeeGoal.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                    Call EnableDisableField()
                Case ezeeWizardImportEmployeeGoal.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        If CheckInvalidData(Nothing) = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case ezeeWizardImportEmployeeGoal.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ezeeWizardImportEmployeeGoal_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form Event(s) "

    Private Sub frmImportEmployeeGoalsWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            txtFilePath.BackColor = GUI.ColorComp
            objlblAdditionalNote.Text = Language.getMessage(mstrModuleName, 13, "1. Always use the file generated from 'Get File Format' link given below.") & vbCrLf & vbCrLf
            objlblAdditionalNote.Text &= Language.getMessage(mstrModuleName, 14, "2. This tool will import data once only.") & vbCrLf & vbCrLf
            objlblAdditionalNote.Text &= Language.getMessage(mstrModuleName, 15, "3. PLEASE DO NOT ALTER ANY COLUMN NAME OR SHEET NAME, ONLY PASTE YOUR DATA AND USE IT FOR IMPORATION.") & vbCrLf & vbCrLf

            objlblOtherNote.Text = Language.getMessage(mstrModuleName, 17, "1. PLEASE MAKE SURE THAT YOU ENTER '1' FOR") & " " & Language.getMessage("clsMasterData", 843, "Qualitative").ToUpper & _
                                   Language.getMessage(mstrModuleName, 18, " AND '2' FOR ") & " " & Language.getMessage("clsMasterData", 844, "Quantitative").ToUpper & vbCrLf & vbCrLf
            objlblOtherNote.Text &= Language.getMessage(mstrModuleName, 19, "2. MAKE SURE THAT ONCE YOU SET GOAL TYPE TO '2', PLEASE PROVIDE GOAL VALUE.") & vbCrLf & vbCrLf
            objlblOtherNote.Text &= Language.getMessage(mstrModuleName, 200, "3. MAKE SURE THAT THE START AND END DATE FORMAT SHOULD BE IN (DD-MMM-YYYY) FOR E.G. (25-NOV-1983).") & vbCrLf

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportEmployeeGoalsWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            objfrm.displayDialog(Me)
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event(s) "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkGenerateFile_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkGenerateFile.LinkClicked
        Try
            Call GetFileFormat()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkGenerateFile_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In pnlGoalMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In pnlGoalMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 55, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text.Trim.Length <= 0 Then Exit Sub
            cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).ToString
            For Each cr As Control In pnlGoalMapping.Controls
                If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                    If CType(cr, ComboBox).Name = cmb.Name Then Continue For
                    If CType(cr, ComboBox).Text = cmb.Tag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        cmb.SelectedIndex = -1
                        cmb.Select()
                        Exit Sub
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Menu Event(s) "

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.ezeeWizardImportEmployeeGoal.CancelText = Language._Object.getCaption(Me.ezeeWizardImportEmployeeGoal.Name & "_CancelText", Me.ezeeWizardImportEmployeeGoal.CancelText)
            Me.ezeeWizardImportEmployeeGoal.NextText = Language._Object.getCaption(Me.ezeeWizardImportEmployeeGoal.Name & "_NextText", Me.ezeeWizardImportEmployeeGoal.NextText)
            Me.ezeeWizardImportEmployeeGoal.BackText = Language._Object.getCaption(Me.ezeeWizardImportEmployeeGoal.Name & "_BackText", Me.ezeeWizardImportEmployeeGoal.BackText)
            Me.ezeeWizardImportEmployeeGoal.FinishText = Language._Object.getCaption(Me.ezeeWizardImportEmployeeGoal.Name & "_FinishText", Me.ezeeWizardImportEmployeeGoal.FinishText)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lnkGenerateFile.Text = Language._Object.getCaption(Me.lnkGenerateFile.Name, Me.lnkGenerateFile.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblField5.Text = Language._Object.getCaption(Me.lblField5.Name, Me.lblField5.Text)
            Me.lblField4.Text = Language._Object.getCaption(Me.lblField4.Name, Me.lblField4.Text)
            Me.lblField3.Text = Language._Object.getCaption(Me.lblField3.Name, Me.lblField3.Text)
            Me.lblField2.Text = Language._Object.getCaption(Me.lblField2.Name, Me.lblField2.Text)
            Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
            Me.lblField1.Text = Language._Object.getCaption(Me.lblField1.Name, Me.lblField1.Text)
            Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblPeriodCode.Text = Language._Object.getCaption(Me.lblPeriodCode.Name, Me.lblPeriodCode.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.chkImportWithApprovedStatus.Text = Language._Object.getCaption(Me.chkImportWithApprovedStatus.Name, Me.chkImportWithApprovedStatus.Text)
            Me.lblGoalValue.Text = Language._Object.getCaption(Me.lblGoalValue.Name, Me.lblGoalValue.Text)
            Me.lblGoalType.Text = Language._Object.getCaption(Me.lblGoalType.Name, Me.lblGoalType.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 843, "Qualitative")
            Language.setMessage("clsMasterData", 844, "Quantitative")
            Language.setMessage(mstrModuleName, 1, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 2, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 3, "Period Code cannot be blank. Please set Period Code in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 4, "Perspective cannot be blank. Please set Perspective in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 5, "Field 1 cannot be blank. Please set Field 1 in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 6, "Field 2 cannot be blank. Please set Field 2 in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 7, "Field 3 cannot be blank. Please set Field 3 in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 8, "Field 4 cannot be blank. Please set Field 4 in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 9, "Field 5 cannot be blank. Please set Field 5 in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 10, "Weight cannot be blank. Please set Weight in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 11, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 12, "Please select proper file to Import Data from.")
            Language.setMessage(mstrModuleName, 13, "1. Always use the file generated from 'Get File Format' link given below.")
            Language.setMessage(mstrModuleName, 14, "2. This tool will import data once only.")
            Language.setMessage(mstrModuleName, 15, "3. PLEASE DO NOT ALTER ANY COLUMN NAME OR SHEET NAME, ONLY PASTE YOUR DATA AND USE IT FOR IMPORATION.")
            Language.setMessage(mstrModuleName, 16, "Invalid data set on weigh column in the file.")
            Language.setMessage(mstrModuleName, 17, "1. PLEASE MAKE SURE THAT YOU ENTER '1' FOR")
            Language.setMessage(mstrModuleName, 18, " AND '2' FOR")
            Language.setMessage(mstrModuleName, 19, "2. MAKE SURE THAT ONCE YOU SET GOAL TYPE TO '2', PLEASE PROVIDE GOAL VALUE.")
            Language.setMessage(mstrModuleName, 41, " Fields From File.")
            Language.setMessage(mstrModuleName, 42, "Please select different Field.")
            Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 50, "Goal Type cannot be blank. Please set Goal Type in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 51, "Goal Value cannot be blank. Please set Goal Value in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 52, "Invalid data set on goal type column in the file Please set value to either (1 or 2).")
            Language.setMessage(mstrModuleName, 53, "Invalid data set on goal value column in the file Please set value greater than zero.")
            Language.setMessage(mstrModuleName, 54, "Goal value cannot be set when goal type is '1', Please remove goal value where goal type is '1' in order to import Employee Goal(s).")
            Language.setMessage(mstrModuleName, 55, "is already Mapped with")
            Language.setMessage(mstrModuleName, 100, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 101, "Fail")
            Language.setMessage(mstrModuleName, 102, "Period Not Found.")
            Language.setMessage(mstrModuleName, 103, "Perspective Not Found.")
            Language.setMessage(mstrModuleName, 104, "No Linked Field Found in period mentioned in File.")
            Language.setMessage(mstrModuleName, 105, "Sorry, weight total either exceeding or it's not adding up to the total weight defined in the system.")
            Language.setMessage(mstrModuleName, 106, "Success")
            Language.setMessage(mstrModuleName, 107, "Already Exists")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class