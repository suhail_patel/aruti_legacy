﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrintExportEmployeeForm
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrintExportEmployeeForm))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkProbationTemplate = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboExportFormat = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnStopProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.pbProgress_Report = New System.Windows.Forms.ProgressBar
        Me.btnPrint = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlnkValue = New System.Windows.Forms.LinkLabel
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.gbMultiperiod = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.bgWorker = New System.ComponentModel.BackgroundWorker
        Me.pnlMain.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbEmployee.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbMultiperiod.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.objlnkValue)
        Me.pnlMain.Controls.Add(Me.gbEmployee)
        Me.pnlMain.Controls.Add(Me.gbMultiperiod)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(376, 482)
        Me.pnlMain.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkProbationTemplate)
        Me.gbFilterCriteria.Controls.Add(Me.Label1)
        Me.gbFilterCriteria.Controls.Add(Me.cboExportFormat)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(372, 94)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Mandatory Info"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkProbationTemplate
        '
        Me.chkProbationTemplate.AutoSize = True
        Me.chkProbationTemplate.BackColor = System.Drawing.Color.Transparent
        Me.chkProbationTemplate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkProbationTemplate.Location = New System.Drawing.Point(246, 4)
        Me.chkProbationTemplate.Name = "chkProbationTemplate"
        Me.chkProbationTemplate.Size = New System.Drawing.Size(119, 17)
        Me.chkProbationTemplate.TabIndex = 81
        Me.chkProbationTemplate.Text = "Probation Template"
        Me.chkProbationTemplate.UseVisualStyleBackColor = False
        Me.chkProbationTemplate.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 15)
        Me.Label1.TabIndex = 84
        Me.Label1.Text = "Export Format"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExportFormat
        '
        Me.cboExportFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExportFormat.DropDownWidth = 180
        Me.cboExportFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExportFormat.FormattingEnabled = True
        Me.cboExportFormat.Location = New System.Drawing.Point(94, 35)
        Me.cboExportFormat.Name = "cboExportFormat"
        Me.cboExportFormat.Size = New System.Drawing.Size(267, 21)
        Me.cboExportFormat.TabIndex = 83
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 65)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(80, 15)
        Me.lblPeriod.TabIndex = 79
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(94, 62)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(267, 21)
        Me.cboPeriod.TabIndex = 77
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnStopProcess)
        Me.EZeeFooter1.Controls.Add(Me.pbProgress_Report)
        Me.EZeeFooter1.Controls.Add(Me.btnPrint)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 419)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(376, 63)
        Me.EZeeFooter1.TabIndex = 310
        '
        'btnStopProcess
        '
        Me.btnStopProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStopProcess.BackColor = System.Drawing.Color.White
        Me.btnStopProcess.BackgroundImage = CType(resources.GetObject("btnStopProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnStopProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStopProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnStopProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStopProcess.Enabled = False
        Me.btnStopProcess.FlatAppearance.BorderSize = 0
        Me.btnStopProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStopProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStopProcess.ForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStopProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStopProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.Location = New System.Drawing.Point(6, 10)
        Me.btnStopProcess.Name = "btnStopProcess"
        Me.btnStopProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStopProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.Size = New System.Drawing.Size(55, 30)
        Me.btnStopProcess.TabIndex = 37
        Me.btnStopProcess.Text = "&Stop"
        Me.btnStopProcess.UseVisualStyleBackColor = True
        '
        'pbProgress_Report
        '
        Me.pbProgress_Report.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pbProgress_Report.Location = New System.Drawing.Point(0, 49)
        Me.pbProgress_Report.Name = "pbProgress_Report"
        Me.pbProgress_Report.Size = New System.Drawing.Size(376, 14)
        Me.pbProgress_Report.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress_Report.TabIndex = 36
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.Color.White
        Me.btnPrint.BackgroundImage = CType(resources.GetObject("btnPrint.BackgroundImage"), System.Drawing.Image)
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrint.BorderColor = System.Drawing.Color.Empty
        Me.btnPrint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.Black
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Location = New System.Drawing.Point(83, 10)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Size = New System.Drawing.Size(90, 30)
        Me.btnPrint.TabIndex = 35
        Me.btnPrint.Text = "&Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(179, 10)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(275, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objlnkValue
        '
        Me.objlnkValue.BackColor = System.Drawing.Color.Transparent
        Me.objlnkValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkValue.ForeColor = System.Drawing.Color.White
        Me.objlnkValue.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkValue.LinkColor = System.Drawing.SystemColors.ControlText
        Me.objlnkValue.Location = New System.Drawing.Point(3, 400)
        Me.objlnkValue.Name = "objlnkValue"
        Me.objlnkValue.Size = New System.Drawing.Size(371, 17)
        Me.objlnkValue.TabIndex = 37
        Me.objlnkValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbEmployee.Controls.Add(Me.lnkAllocation)
        Me.gbEmployee.Controls.Add(Me.objAlloacationReset)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(2, 102)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(372, 296)
        Me.gbEmployee.TabIndex = 307
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.Text = "Employee"
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(2, 26)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(368, 268)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(362, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(362, 236)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 22
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(362, 236)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(260, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 15)
        Me.lnkAllocation.TabIndex = 305
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(344, 2)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 306
        '
        'gbMultiperiod
        '
        Me.gbMultiperiod.BorderColor = System.Drawing.Color.Black
        Me.gbMultiperiod.Checked = False
        Me.gbMultiperiod.CollapseAllExceptThis = False
        Me.gbMultiperiod.CollapsedHoverImage = Nothing
        Me.gbMultiperiod.CollapsedNormalImage = Nothing
        Me.gbMultiperiod.CollapsedPressedImage = Nothing
        Me.gbMultiperiod.CollapseOnLoad = False
        Me.gbMultiperiod.Controls.Add(Me.lblToPeriod)
        Me.gbMultiperiod.Controls.Add(Me.lblFromPeriod)
        Me.gbMultiperiod.Controls.Add(Me.cboToPeriod)
        Me.gbMultiperiod.Controls.Add(Me.cboFromPeriod)
        Me.gbMultiperiod.ExpandedHoverImage = Nothing
        Me.gbMultiperiod.ExpandedNormalImage = Nothing
        Me.gbMultiperiod.ExpandedPressedImage = Nothing
        Me.gbMultiperiod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMultiperiod.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMultiperiod.HeaderHeight = 25
        Me.gbMultiperiod.HeaderMessage = ""
        Me.gbMultiperiod.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMultiperiod.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMultiperiod.HeightOnCollapse = 0
        Me.gbMultiperiod.LeftTextSpace = 0
        Me.gbMultiperiod.Location = New System.Drawing.Point(82, 18)
        Me.gbMultiperiod.Name = "gbMultiperiod"
        Me.gbMultiperiod.OpenHeight = 300
        Me.gbMultiperiod.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMultiperiod.ShowBorder = True
        Me.gbMultiperiod.ShowCheckBox = False
        Me.gbMultiperiod.ShowCollapseButton = False
        Me.gbMultiperiod.ShowDefaultBorderColor = True
        Me.gbMultiperiod.ShowDownButton = False
        Me.gbMultiperiod.ShowHeader = True
        Me.gbMultiperiod.Size = New System.Drawing.Size(185, 62)
        Me.gbMultiperiod.TabIndex = 123
        Me.gbMultiperiod.Temp = 0
        Me.gbMultiperiod.Text = "Period Selection"
        Me.gbMultiperiod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbMultiperiod.Visible = False
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(8, 74)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(80, 15)
        Me.lblToPeriod.TabIndex = 84
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 74)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(80, 15)
        Me.lblFromPeriod.TabIndex = 82
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 180
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(94, 71)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(290, 21)
        Me.cboToPeriod.TabIndex = 83
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.DropDownWidth = 180
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(94, 71)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(290, 21)
        Me.cboFromPeriod.TabIndex = 81
        '
        'bgWorker
        '
        Me.bgWorker.WorkerReportsProgress = True
        Me.bgWorker.WorkerSupportsCancellation = True
        '
        'frmPrintExportEmployeeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(376, 482)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPrintExportEmployeeForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Score Card"
        Me.pnlMain.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbMultiperiod.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboExportFormat As System.Windows.Forms.ComboBox
    Friend WithEvents gbMultiperiod As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkProbationTemplate As System.Windows.Forms.CheckBox
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objlnkValue As System.Windows.Forms.LinkLabel
    Friend WithEvents pbProgress_Report As System.Windows.Forms.ProgressBar
    Friend WithEvents btnPrint As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Private WithEvents bgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnStopProcess As eZee.Common.eZeeLightButton
End Class
