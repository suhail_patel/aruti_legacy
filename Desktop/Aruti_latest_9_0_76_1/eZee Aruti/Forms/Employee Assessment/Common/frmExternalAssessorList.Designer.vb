﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExternalAssessorList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExternalAssessorList))
        Me.pnlSkillList = New System.Windows.Forms.Panel
        Me.gbExternalAssessor = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchdName = New eZee.Common.eZeeGradientButton
        Me.cboRelation = New System.Windows.Forms.ComboBox
        Me.lblRelation = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboDisplayName = New System.Windows.Forms.ComboBox
        Me.lblExternalAssessor = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lvExternalAssessorList = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhAssessorName = New System.Windows.Forms.ColumnHeader
        Me.colhDisplayName = New System.Windows.Forms.ColumnHeader
        Me.colhRelation = New System.Windows.Forms.ColumnHeader
        Me.colhPhNo = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.pnlSkillList.SuspendLayout()
        Me.gbExternalAssessor.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlSkillList
        '
        Me.pnlSkillList.Controls.Add(Me.gbExternalAssessor)
        Me.pnlSkillList.Controls.Add(Me.lvExternalAssessorList)
        Me.pnlSkillList.Controls.Add(Me.objFooter)
        Me.pnlSkillList.Controls.Add(Me.EZeeHeader1)
        Me.pnlSkillList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSkillList.Location = New System.Drawing.Point(0, 0)
        Me.pnlSkillList.Name = "pnlSkillList"
        Me.pnlSkillList.Size = New System.Drawing.Size(744, 443)
        Me.pnlSkillList.TabIndex = 0
        '
        'gbExternalAssessor
        '
        Me.gbExternalAssessor.BorderColor = System.Drawing.Color.Black
        Me.gbExternalAssessor.Checked = False
        Me.gbExternalAssessor.CollapseAllExceptThis = False
        Me.gbExternalAssessor.CollapsedHoverImage = Nothing
        Me.gbExternalAssessor.CollapsedNormalImage = Nothing
        Me.gbExternalAssessor.CollapsedPressedImage = Nothing
        Me.gbExternalAssessor.CollapseOnLoad = False
        Me.gbExternalAssessor.Controls.Add(Me.objbtnSearchdName)
        Me.gbExternalAssessor.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbExternalAssessor.Controls.Add(Me.cboRelation)
        Me.gbExternalAssessor.Controls.Add(Me.lblRelation)
        Me.gbExternalAssessor.Controls.Add(Me.cboDisplayName)
        Me.gbExternalAssessor.Controls.Add(Me.lblExternalAssessor)
        Me.gbExternalAssessor.Controls.Add(Me.objbtnReset)
        Me.gbExternalAssessor.Controls.Add(Me.objbtnSearch)
        Me.gbExternalAssessor.Controls.Add(Me.cboEmployee)
        Me.gbExternalAssessor.Controls.Add(Me.lblEmployee)
        Me.gbExternalAssessor.ExpandedHoverImage = Nothing
        Me.gbExternalAssessor.ExpandedNormalImage = Nothing
        Me.gbExternalAssessor.ExpandedPressedImage = Nothing
        Me.gbExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExternalAssessor.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExternalAssessor.HeaderHeight = 25
        Me.gbExternalAssessor.HeaderMessage = ""
        Me.gbExternalAssessor.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbExternalAssessor.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExternalAssessor.HeightOnCollapse = 0
        Me.gbExternalAssessor.LeftTextSpace = 0
        Me.gbExternalAssessor.Location = New System.Drawing.Point(9, 66)
        Me.gbExternalAssessor.Name = "gbExternalAssessor"
        Me.gbExternalAssessor.OpenHeight = 91
        Me.gbExternalAssessor.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExternalAssessor.ShowBorder = True
        Me.gbExternalAssessor.ShowCheckBox = False
        Me.gbExternalAssessor.ShowCollapseButton = False
        Me.gbExternalAssessor.ShowDefaultBorderColor = True
        Me.gbExternalAssessor.ShowDownButton = False
        Me.gbExternalAssessor.ShowHeader = True
        Me.gbExternalAssessor.Size = New System.Drawing.Size(723, 66)
        Me.gbExternalAssessor.TabIndex = 9
        Me.gbExternalAssessor.Temp = 0
        Me.gbExternalAssessor.Text = "Filter Criteria"
        Me.gbExternalAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchdName
        '
        Me.objbtnSearchdName.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchdName.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchdName.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchdName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchdName.BorderSelected = False
        Me.objbtnSearchdName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchdName.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchdName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchdName.Location = New System.Drawing.Point(509, 33)
        Me.objbtnSearchdName.Name = "objbtnSearchdName"
        Me.objbtnSearchdName.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchdName.TabIndex = 240
        '
        'cboRelation
        '
        Me.cboRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRelation.DropDownWidth = 200
        Me.cboRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRelation.FormattingEnabled = True
        Me.cboRelation.Location = New System.Drawing.Point(591, 33)
        Me.cboRelation.Name = "cboRelation"
        Me.cboRelation.Size = New System.Drawing.Size(121, 21)
        Me.cboRelation.TabIndex = 239
        '
        'lblRelation
        '
        Me.lblRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRelation.Location = New System.Drawing.Point(536, 36)
        Me.lblRelation.Name = "lblRelation"
        Me.lblRelation.Size = New System.Drawing.Size(53, 15)
        Me.lblRelation.TabIndex = 238
        Me.lblRelation.Text = "Relation"
        Me.lblRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(258, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 236
        '
        'cboDisplayName
        '
        Me.cboDisplayName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplayName.DropDownWidth = 250
        Me.cboDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisplayName.FormattingEnabled = True
        Me.cboDisplayName.Location = New System.Drawing.Point(365, 33)
        Me.cboDisplayName.Name = "cboDisplayName"
        Me.cboDisplayName.Size = New System.Drawing.Size(138, 21)
        Me.cboDisplayName.TabIndex = 90
        '
        'lblExternalAssessor
        '
        Me.lblExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExternalAssessor.Location = New System.Drawing.Point(285, 36)
        Me.lblExternalAssessor.Name = "lblExternalAssessor"
        Me.lblExternalAssessor.Size = New System.Drawing.Size(74, 15)
        Me.lblExternalAssessor.TabIndex = 89
        Me.lblExternalAssessor.Text = "Display Name"
        Me.lblExternalAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(696, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(673, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(89, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(163, 21)
        Me.cboEmployee.TabIndex = 87
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvExternalAssessorList
        '
        Me.lvExternalAssessorList.BackColorOnChecked = True
        Me.lvExternalAssessorList.ColumnHeaders = Nothing
        Me.lvExternalAssessorList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhAssessorName, Me.colhDisplayName, Me.colhRelation, Me.colhPhNo})
        Me.lvExternalAssessorList.CompulsoryColumns = ""
        Me.lvExternalAssessorList.FullRowSelect = True
        Me.lvExternalAssessorList.GridLines = True
        Me.lvExternalAssessorList.GroupingColumn = Nothing
        Me.lvExternalAssessorList.HideSelection = False
        Me.lvExternalAssessorList.Location = New System.Drawing.Point(9, 138)
        Me.lvExternalAssessorList.MinColumnWidth = 50
        Me.lvExternalAssessorList.MultiSelect = False
        Me.lvExternalAssessorList.Name = "lvExternalAssessorList"
        Me.lvExternalAssessorList.OptionalColumns = ""
        Me.lvExternalAssessorList.ShowMoreItem = False
        Me.lvExternalAssessorList.ShowSaveItem = False
        Me.lvExternalAssessorList.ShowSelectAll = True
        Me.lvExternalAssessorList.ShowSizeAllColumnsToFit = True
        Me.lvExternalAssessorList.Size = New System.Drawing.Size(723, 245)
        Me.lvExternalAssessorList.Sortable = True
        Me.lvExternalAssessorList.TabIndex = 2
        Me.lvExternalAssessorList.UseCompatibleStateImageBehavior = False
        Me.lvExternalAssessorList.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhAssessorName
        '
        Me.colhAssessorName.Tag = "colhAssessorName"
        Me.colhAssessorName.Text = "Assessor"
        Me.colhAssessorName.Width = 200
        '
        'colhDisplayName
        '
        Me.colhDisplayName.Tag = "colhDisplayName"
        Me.colhDisplayName.Text = "Display Name"
        Me.colhDisplayName.Width = 200
        '
        'colhRelation
        '
        Me.colhRelation.Tag = "colhRelation"
        Me.colhRelation.Text = "Relation"
        Me.colhRelation.Width = 165
        '
        'colhPhNo
        '
        Me.colhPhNo.Tag = "colhPhNo"
        Me.colhPhNo.Text = "Phone No"
        Me.colhPhNo.Width = 150
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 388)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(744, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(642, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(546, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(450, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(354, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(744, 60)
        Me.EZeeHeader1.TabIndex = 0
        Me.EZeeHeader1.Title = "External Assessor "
        '
        'frmExternalAssessorList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(744, 443)
        Me.Controls.Add(Me.pnlSkillList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExternalAssessorList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "External Assessor List"
        Me.pnlSkillList.ResumeLayout(False)
        Me.gbExternalAssessor.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlSkillList As System.Windows.Forms.Panel
    Friend WithEvents lvExternalAssessorList As eZee.Common.eZeeListView
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents gbExternalAssessor As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents cboDisplayName As System.Windows.Forms.ComboBox
    Friend WithEvents lblExternalAssessor As System.Windows.Forms.Label
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAssessorName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDisplayName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPhNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRelation As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchdName As eZee.Common.eZeeGradientButton
    Friend WithEvents cboRelation As System.Windows.Forms.ComboBox
    Friend WithEvents lblRelation As System.Windows.Forms.Label
End Class
