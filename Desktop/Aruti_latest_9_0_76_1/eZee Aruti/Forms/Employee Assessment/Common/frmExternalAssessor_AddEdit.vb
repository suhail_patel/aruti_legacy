﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

Public Class frmExternalAssessor_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmExternalAssessor_AddEdit"
    Private mblnCancel As Boolean = True
    Private objExternalAssessor As clsexternal_assessor_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintExternalAssessorunkid As Integer = -1
    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintExternalAssessorunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintExternalAssessorunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmExternalAssessor_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExternalAssessor = New clsexternal_assessor_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objExternalAssessor._Ext_Assessorunkid = mintExternalAssessorunkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboRelation.Enabled = False
                objbtnAddRelation.Enabled = False
                txtDisplayName.ReadOnly = True
            End If
            FillCombo()
            GetValue()
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExternalAssessor_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalAssessor_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExternalAssessor_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalAssessor_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExternalAssessor_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvertise_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objExternalAssessor = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsexternal_assessor_master.SetMessages()
            objfrm._Other_ModuleNames = "clsexternal_assessor_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                    txtEmpCode.Text = objfrm.SelectedAlias
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddRelation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddRelation.Click
        Try
            Dim objCommonMaster As New frmCommonMaster
            If User._Object._Isrighttoleft = True Then
                objCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommonMaster)
            End If
            objCommonMaster.displayDialog(-1, clsCommon_Master.enCommonMaster.RELATIONS, enAction.ADD_ONE)
            FillRelation()
            cboRelation.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddRelation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Validation() Then

                Call SetValue()

                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objExternalAssessor.Update()
                Else
                    blnFlag = objExternalAssessor.Insert()
                End If

                If blnFlag = False And objExternalAssessor._Message <> "" Then
                    eZeeMsgBox.Show(objExternalAssessor._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    mblnCancel = False
                    If menAction = enAction.ADD_CONTINUE Then
                        objExternalAssessor = Nothing
                        objExternalAssessor = New clsexternal_assessor_master
                        Call GetValue()
                        cboEmployee.Select()
                    Else
                        mintExternalAssessorunkid = objExternalAssessor._Ext_Assessorunkid
                        Me.Close()
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Function Validation() As Boolean
        Try
            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim EmailExpression As New RegularExpressions.Regex(strEmailExpression)
            Dim EmailExpression As New RegularExpressions.Regex(iEmailRegxExpression)
            'S.SANDEEP [ 11 MAR 2014 ] -- END
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployee.Select()
                Return False

            ElseIf CInt(cboRelation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Relation is compulsory information.Please Select Relation."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboRelation.Select()
                Return False

            ElseIf txtDisplayName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Display Name cannot be blank.Display Name is required information."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboRelation.Select()
                Return False

            ElseIf txtAssessorName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Assessor Name cannot be blank.Assessor Name is required information."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtAssessorName.Select()
                Return False

            ElseIf txtPhone.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Phone No cannot be blank.Phone No is required information."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtPhone.Select()
                Return False

            ElseIf txtEmail.Text.Trim.Length > 0 Then
                If EmailExpression.IsMatch(txtEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid Email.Please Enter Valid Email."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtEmail.Focus()
                    Return False
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub setColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboRelation.BackColor = GUI.ColorComp
            txtEmpCode.BackColor = GUI.ColorComp
            txtDisplayName.BackColor = GUI.ColorComp
            txtAssessorName.BackColor = GUI.ColorComp
            txtPhone.BackColor = GUI.ColorComp
            txtaddress.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtCompany.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboEmployee.SelectedValue = objExternalAssessor._Employeeunkid
            cboRelation.SelectedValue = objExternalAssessor._Relationunkid
            If objExternalAssessor._Displayname.Trim.Length > 0 Then
                txtDisplayName.Text = objExternalAssessor._Displayname.Substring(objExternalAssessor._Displayname.IndexOf("-") + 1, objExternalAssessor._Displayname.Length - (objExternalAssessor._Displayname.IndexOf("-") + 1))
            Else
                txtDisplayName.Text = ""
            End If
            txtAssessorName.Text = objExternalAssessor._Assessorname
            txtaddress.Text = objExternalAssessor._Address
            cboCountry.SelectedValue = objExternalAssessor._Countryunkid
            cboState.SelectedValue = objExternalAssessor._Stateunkid
            cboCity.SelectedValue = objExternalAssessor._Cityunkid
            txtPhone.Text = objExternalAssessor._Phone
            txtEmail.Text = objExternalAssessor._Email
            txtCompany.Text = objExternalAssessor._Company
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objExternalAssessor._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objExternalAssessor._Relationunkid = CInt(cboRelation.SelectedValue)
            objExternalAssessor._Displayname = txtEmpCode.Text.Trim & " - " & txtDisplayName.Text.Trim
            objExternalAssessor._Assessorname = txtAssessorName.Text.Trim
            objExternalAssessor._Address = txtaddress.Text.Trim
            objExternalAssessor._Countryunkid = CInt(cboCountry.SelectedValue)
            objExternalAssessor._Stateunkid = CInt(cboState.SelectedValue)
            objExternalAssessor._Cityunkid = CInt(cboCity.SelectedValue)
            objExternalAssessor._Phone = txtPhone.Text.Trim
            objExternalAssessor._Email = txtEmail.Text.Trim
            objExternalAssessor._Company = txtCompany.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objEmployee As New clsEmployee_Master
            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsList = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Anjan (17 Apr 2012)-End 
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables("Employee")


            RemoveHandler cboCountry.SelectedIndexChanged, AddressOf cboCountry_SelectedIndexChanged

            dsList = Nothing
            Dim objMasterdata As New clsMasterData
            dsList = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsList.Tables(0)

            AddHandler cboCountry.SelectedIndexChanged, AddressOf cboCountry_SelectedIndexChanged

            FillRelation()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillRelation()
        Dim dsList As DataSet = Nothing
        Try
            Dim objMaster As New clsCommon_Master
            dsList = objMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "List")
            cboRelation.ValueMember = "masterunkid"
            cboRelation.DisplayMember = "name"
            cboRelation.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillRelation", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Dim dsCity As DataSet = Nothing
        Try
            Dim objCitymaster As New clscity_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsCity = objCitymaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCitymaster.GetList("City", True, True)
            End If
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddRelation.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "DropDown Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim dr As DataRowView = CType(cboEmployee.SelectedItem, DataRowView)
                If dr IsNot Nothing Then txtEmpCode.Text = dr("employeecode").ToString()
            Else
                txtEmpCode.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbExternalAssessor.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExternalAssessor.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbExternalAssessor.Text = Language._Object.getCaption(Me.gbExternalAssessor.Name, Me.gbExternalAssessor.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblAssessorName.Text = Language._Object.getCaption(Me.lblAssessorName.Name, Me.lblAssessorName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.Name, Me.lblRelation.Text)
			Me.lblstate.Text = Language._Object.getCaption(Me.lblstate.Name, Me.lblstate.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblPhone.Text = Language._Object.getCaption(Me.lblPhone.Name, Me.lblPhone.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblDisplayName.Text = Language._Object.getCaption(Me.lblDisplayName.Name, Me.lblDisplayName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
			Language.setMessage(mstrModuleName, 2, "Relation is compulsory information.Please Select Relation.")
			Language.setMessage(mstrModuleName, 3, "Display Name cannot be blank.Display Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Assessor Name cannot be blank.Assessor Name is required information.")
			Language.setMessage(mstrModuleName, 5, "Phone No cannot be blank.Phone No is required information.")
			Language.setMessage(mstrModuleName, 6, "Invalid Email.Please Enter Valid Email.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class