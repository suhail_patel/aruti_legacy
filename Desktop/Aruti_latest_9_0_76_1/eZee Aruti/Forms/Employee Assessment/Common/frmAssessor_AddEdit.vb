﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

#End Region

Public Class frmAssessor_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessor_AddEdit"
    Private objAssessor As clsAssessor
    Private mstrAssessorIds As String = String.Empty
    Private mstrToDeleteIds As String = String.Empty
    Private mstrReviewerIds As String = String.Empty
    Private dgAEView As DataView
    Private dgREView As DataView
    Private dgAView As DataView
    Private dgRView As DataView
    Private dsTran As New DataSet

#End Region

#Region " Private Function "

    Private Sub Fill_Employee_List()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = ObjEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                dsList = ObjEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
            dsList = ObjEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            End If
            'Sohail (23 Nov 2012) -- End
            dsList.Tables("Emp").Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

            For i As Integer = 0 To dsList.Tables("Emp").Rows.Count - 1
                dsList.Tables("Emp").Rows(i)("IsCheck") = False
            Next

            dsList.Tables("Emp").AcceptChanges()

            Dim dtRE As DataTable
            dtRE = dsList.Tables("Emp").Copy
            dtRE.TableName = "REmp"

            If mstrAssessorIds.Trim.Length > 0 Then
                dgAEView = New DataView(dsList.Tables("Emp"), "employeeunkid NOT IN(" & mstrAssessorIds & ")", "", DataViewRowState.CurrentRows)
                dsList.Tables.Remove("Emp") : dsList.Tables.Add(dgAEView.ToTable)
            Else
                dgAEView = New DataView(dsList.Tables("Emp"), "", "", DataViewRowState.CurrentRows)
            End If

            If mstrReviewerIds.Trim.Length > 0 Then
                dgREView = New DataView(dtRE, "employeeunkid NOT IN(" & mstrReviewerIds & ")", "", DataViewRowState.CurrentRows)
                dtRE = dgREView.ToTable
            Else
                dgREView = New DataView(dtRE, "", "", DataViewRowState.CurrentRows)
            End If

            dsTran.Tables.Add(dsList.Tables("Emp").Copy)
            dsTran.Tables.Add(dtRE.Copy)


            Call SetDataSources(dgvAEmployee)
            Call SetDataSources(dgvREmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assessor_List()
        Dim dsList As New DataSet
        Try
            dsList = objAssessor.GetList("Assessor", False)
            dsList.Tables("Assessor").Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

            If dsList.Tables("Assessor").Rows.Count > 0 Then
                mstrAssessorIds = dsList.Tables("Assessor").Rows(0)("AssessIds").ToString
            End If

            For i As Integer = 0 To dsList.Tables("Assessor").Rows.Count - 1
                dsList.Tables("Assessor").Rows(i)("IsCheck") = False
            Next

            dsTran.Tables.Add(dsList.Tables("Assessor").Copy)

            dgAView = New DataView(dsTran.Tables("Assessor"), "", "", DataViewRowState.CurrentRows)

            Call SetDataSources(dgvAssessor)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assessor_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Reviewer_List()
        Dim dsList As New DataSet
        Try
            dsList = objAssessor.GetList("Reviewer", True)
            dsList.Tables("Reviewer").Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

            If dsList.Tables("Reviewer").Rows.Count > 0 Then
                mstrReviewerIds = dsList.Tables("Reviewer").Rows(0)("AssessIds").ToString
            End If

            For i As Integer = 0 To dsList.Tables("Reviewer").Rows.Count - 1
                dsList.Tables("Reviewer").Rows(i)("IsCheck") = False
            Next

            dsTran.Tables.Add(dsList.Tables("Reviewer").Copy)

            dgRView = New DataView(dsTran.Tables("Reviewer"), "", "", DataViewRowState.CurrentRows)

            Call SetDataSources(dgvReviewer)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Reviewer_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowAddAssessor
            objbtnAUnAssign.Enabled = User._Object.Privilege._AllowRemoveAssessor
            If ConfigParameter._Object._IsCompanyNeedReviewer = False Then
                tabcAssessorReviewerInfo.TabPages.Remove(tabpReviewer)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataSources(ByVal dgvData As DataGridView)
        Try
            Select Case dgvData.Name.ToUpper
                Case "DGVAEMPLOYEE"
                    dgvAEmployee.AutoGenerateColumns = False
                    objdgcolhECheck.DataPropertyName = "IsCheck"
                    dgcolhEcode.DataPropertyName = "employeecode"
                    dgcolhEName.DataPropertyName = "employeename"
                    objdgcolhEmpId.DataPropertyName = "employeeunkid"
                    dgvAEmployee.DataSource = dgAEView
                Case "DGVASSESSOR"
                    dgvAssessor.AutoGenerateColumns = False
                    objdgcolhACheck.DataPropertyName = "IsCheck"
                    dgcolhACode.DataPropertyName = "employeecode"
                    dgcolhAName.DataPropertyName = "assessorname"
                    dgcolhAEmpId.DataPropertyName = "employeeunkid"
                    dgvAssessor.DataSource = dgAView
                Case "DGVREMPLOYEE"
                    dgvREmployee.AutoGenerateColumns = False
                    objdgcolhRECheck.DataPropertyName = "IsCheck"
                    dgcolhRECode.DataPropertyName = "employeecode"
                    dgcolhREmployee.DataPropertyName = "employeename"
                    objdgcolhREmpId.DataPropertyName = "employeeunkid"
                    dgvREmployee.DataSource = dgREView
                Case "DGVREVIEWER"
                    dgvReviewer.AutoGenerateColumns = False
                    objdgcolhRCheck.DataPropertyName = "IsCheck"
                    dgcolhRCode.DataPropertyName = "employeecode"
                    dgcolhReviewer.DataPropertyName = "assessorname"
                    objdgcolhRUnkid.DataPropertyName = "employeeunkid"
                    dgvReviewer.DataSource = dgRView
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSources", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Forms Events "

    Private Sub frmAssessor_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAssessor = New clsAssessor
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call Fill_Assessor_List()
            Call Fill_Reviewer_List()
            Call Fill_Employee_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessor.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessor"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnAAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAAssign.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim StrRowFilter As String = dgAEView.RowFilter
            dgAEView.RowFilter = ""
            dsTran.Tables.Remove("Emp")
            dsTran.Tables.Add(dgAEView.ToTable)
            Dim dTemp() As DataRow = dsTran.Tables("Emp").Select("IsCheck = true")

            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Check atleast one employee to assign as Assessor."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            For i As Integer = 0 To dTemp.Length - 1
                Dim dtRow As DataRow = dsTran.Tables("Assessor").NewRow

                dtRow.Item("assessormasterunkid") = -1
                dtRow.Item("employeeunkid") = dTemp(i)("employeeunkid")
                dtRow.Item("userunkid") = User._Object._Userunkid
                dtRow.Item("employeecode") = dTemp(i)("employeecode")
                dtRow.Item("assessorname") = dTemp(i)("employeename")
                dtRow.Item("AssessIds") = ""
                dtRow.Item("isreviewer") = False

                dsTran.Tables("Assessor").Rows.Add(dtRow)
                dsTran.Tables("Emp").Rows.Remove(dTemp(i))
                dsTran.Tables("Emp").AcceptChanges() : dsTran.Tables("Assessor").AcceptChanges()
            Next


            dgAEView = dsTran.Tables("Emp").DefaultView
            dgAEView.RowFilter = StrRowFilter
            Call SetDataSources(dgvAEmployee)

            dgAView = dsTran.Tables("Assessor").DefaultView
            Call SetDataSources(dgvAssessor)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAAssign_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnAUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAUnAssign.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim StrRowFilter As String = dgAView.RowFilter
            dgAView.RowFilter = ""
            dsTran.Tables.Remove("Assessor")
            dsTran.Tables.Add(dgAView.ToTable)
            Dim dTemp() As DataRow = dsTran.Tables("Assessor").Select("IsCheck = true")

            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Check atleast one Assessor to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If

                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    Dim mstrReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrReason)
                    If mstrReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objAssessor._Voidreason = mstrReason
                    End If
                    frm = Nothing
                    objAssessor._Isvoid = True
                    objAssessor._Voiduserunkid = User._Object._Userunkid
                    objAssessor._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objAssessor._Voidreason = mstrReason
                    'S.SANDEEP [ 18 APRIL 2012 ] -- END

            Dim dtRow As DataRow = Nothing
            Dim blnFlag As Boolean = False
            For i As Integer = 0 To dTemp.Length - 1
                blnFlag = False
                If CInt(dTemp(i)("assessormasterunkid")) > 0 Then
                    objAssessor._AssessormasterIds = dTemp(i)("assessormasterunkid").ToString
                    If objAssessor.Delete = True Then
                        If objAssessor._Message <> "" Then
                            eZeeMsgBox.Show(objAssessor._Message, enMsgBoxStyle.Information)
                            blnFlag = False
                        Else
                            blnFlag = True
                        End If
                    End If
                Else
                    blnFlag = True
                End If

                If blnFlag = True Then
                    dtRow = dsTran.Tables("Emp").NewRow
                    dtRow.Item("employeeunkid") = dTemp(i)("employeeunkid")
                    dtRow.Item("employeecode") = dTemp(i)("employeecode")
                    dtRow.Item("employeename") = dTemp(i)("assessorname")

                    dsTran.Tables("Emp").Rows.Add(dtRow)
                    dsTran.Tables("Assessor").Rows.Remove(dTemp(i))
                    dsTran.Tables("Assessor").AcceptChanges()
                End If
            Next

            dgAView = dsTran.Tables("Assessor").DefaultView
            dgAView.RowFilter = StrRowFilter
            Call SetDataSources(dgvAssessor)

            dgAEView = dsTran.Tables("Emp").DefaultView
            Call SetDataSources(dgvAEmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAUnAssign_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnRAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnRAssign.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim StrRowFilter As String = dgREView.RowFilter
            dgREView.RowFilter = ""
            dsTran.Tables.Remove("REmp")
            dsTran.Tables.Add(dgREView.ToTable)
            Dim dTemp() As DataRow = dsTran.Tables("REmp").Select("IsCheck = true")

            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Plese Check atleast one employee to assign as Reviewer."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            For i As Integer = 0 To dTemp.Length - 1
                Dim dtRow As DataRow = dsTran.Tables("Reviewer").NewRow

                dtRow.Item("assessormasterunkid") = -1
                dtRow.Item("employeeunkid") = dTemp(i)("employeeunkid")
                dtRow.Item("userunkid") = User._Object._Userunkid
                dtRow.Item("employeecode") = dTemp(i)("employeecode")
                dtRow.Item("assessorname") = dTemp(i)("employeename")
                dtRow.Item("AssessIds") = ""
                dtRow.Item("isreviewer") = True

                dsTran.Tables("Reviewer").Rows.Add(dtRow)
                dsTran.Tables("REmp").Rows.Remove(dTemp(i))
                dsTran.Tables("REmp").AcceptChanges() : dsTran.Tables("Reviewer").AcceptChanges()
            Next

            dgREView = dsTran.Tables("REmp").DefaultView
            dgREView.RowFilter = StrRowFilter
            Call SetDataSources(dgvREmployee)

            dgRView = dsTran.Tables("Reviewer").DefaultView
            Call SetDataSources(dgvReviewer)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnRAssign_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnRUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnRUnAssign.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim StrRowFilter As String = dgRView.RowFilter
            dgRView.RowFilter = ""
            dsTran.Tables.Remove("Reviewer")
            dsTran.Tables.Add(dgRView.ToTable)

            Dim dTemp() As DataRow = dsTran.Tables("Reviewer").Select("IsCheck = true")

            If dTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Reviewer to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow = Nothing
            Dim blnFlag As Boolean = False

                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    Dim mstrReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrReason)
                    If mstrReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objAssessor._Voidreason = mstrReason
                    End If
                    frm = Nothing
                    objAssessor._Isvoid = True
                    objAssessor._Voiduserunkid = User._Object._Userunkid
                    objAssessor._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objAssessor._Voidreason = mstrReason
                    'S.SANDEEP [ 18 APRIL 2012 ] -- END

            For i As Integer = 0 To dTemp.Length - 1
                blnFlag = False
                If CInt(dTemp(i)("assessormasterunkid")) > 0 Then
                    objAssessor._AssessormasterIds = dTemp(i)("assessormasterunkid").ToString
                    If objAssessor.Delete = True Then
                        If objAssessor._Message <> "" Then
                            eZeeMsgBox.Show(objAssessor._Message, enMsgBoxStyle.Information)
                            blnFlag = False
                        Else
                            blnFlag = True
                        End If
                    End If
                Else
                    blnFlag = True
                End If

                If blnFlag = True Then
                    dtRow = dsTran.Tables("REmp").NewRow
                    dtRow.Item("employeeunkid") = dTemp(i)("employeeunkid")
                    dtRow.Item("employeecode") = dTemp(i)("employeecode")
                    dtRow.Item("employeename") = dTemp(i)("assessorname")

                    dsTran.Tables("REmp").Rows.Add(dtRow)
                    dsTran.Tables("Reviewer").Rows.Remove(dTemp(i))
                    dsTran.Tables("Reviewer").AcceptChanges()
                End If
            Next


            dgRView = dsTran.Tables("Reviewer").DefaultView
            dgRView.RowFilter = StrRowFilter
            Call SetDataSources(dgvReviewer)

            dgREView = dsTran.Tables("REmp").DefaultView
            Call SetDataSources(dgvREmployee)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnRUnAssign_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnAFlag As Boolean = False : Dim blnRFlag As Boolean = False
        Try
            For Each dARow As DataRow In dsTran.Tables("Assessor").Rows
                If objAssessor.isExist(CInt(dARow.Item("employeeunkid")), False) = False Then
                    objAssessor._EmployeeId = CInt(dARow.Item("employeeunkid"))
                    objAssessor._Isreviewer = False
                    objAssessor._Userunkid = User._Object._Userunkid
                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objAssessor._Isvoid = False
                    objAssessor._Voiddatetime = Nothing
                    objAssessor._Voidreason = ""
                    objAssessor._Voiduserunkid = -1
                    'S.SANDEEP [ 18 APRIL 2012 ] -- END
                    If objAssessor.Insert = False Then
                        blnAFlag = True
                        Exit For
                    End If
                End If
            Next

            For Each dRRow As DataRow In dsTran.Tables("Reviewer").Rows
                If objAssessor.isExist(CInt(dRRow.Item("employeeunkid")), True) = False Then
                    objAssessor._EmployeeId = CInt(dRRow.Item("employeeunkid"))
                    objAssessor._Isreviewer = True
                    objAssessor._Userunkid = User._Object._Userunkid
                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objAssessor._Isvoid = False
                    objAssessor._Voiddatetime = Nothing
                    objAssessor._Voidreason = ""
                    objAssessor._Voiduserunkid = -1
                    'S.SANDEEP [ 18 APRIL 2012 ] -- END
                    If objAssessor.Insert = False Then
                        blnRFlag = True
                        Exit For
                    End If
                End If
            Next

            If blnAFlag = False AndAlso blnRFlag = False Then
                Me.Close()
            ElseIf blnAFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Problem in Inserting Assessor(s)."), enMsgBoxStyle.Information)
            ElseIf blnRFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Problem in Inserting Reviewer(s)."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub txtAssessorEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssessorEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssessorEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssessorEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtAssessorEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtAssessorEmp.Text & "%' OR " & _
                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtAssessorEmp.Text & "%'"
                objchkEmployee.Checked = False
                objchkEmployee.Enabled = False
            Else
                objchkEmployee.Enabled = True
            End If

            dgAEView.RowFilter = ""
            If dgAEView.RowFilter.Trim.Length <= 0 Then
                If strSearch.Trim.Length > 0 Then
                    If mstrAssessorIds.Trim.Length > 0 Then
                        dgAEView.RowFilter = "employeeunkid NOT IN(" & mstrAssessorIds & ") AND (" & strSearch & ")"
                    Else
                        dgAEView.RowFilter = strSearch
                    End If
                Else
                    If mstrAssessorIds.Trim.Length > 0 Then
                        dgAEView.RowFilter = "employeeunkid NOT IN(" & mstrAssessorIds & ")"
                    Else
                        dgAEView.RowFilter = strSearch
                    End If
                End If
            End If


        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtReviewerEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReviewerEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtReviewerEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhREmployee.DataPropertyName & " LIKE '%" & txtReviewerEmp.Text & "%' OR " & _
                            dgcolhRECode.DataPropertyName & " LIKE '%" & txtReviewerEmp.Text & "%'"
                chkREmployee.Checked = False : chkREmployee.Enabled = False
            Else
                chkREmployee.Enabled = True
            End If

            dgREView.RowFilter = ""
            If dgREView.RowFilter.Trim.Length <= 0 Then
                If strSearch.Trim.Length > 0 Then
                    If mstrReviewerIds.Trim.Length > 0 Then
                        dgREView.RowFilter = "employeeunkid NOT IN(" & mstrReviewerIds & ") AND (" & strSearch & ")"
                    Else
                        dgREView.RowFilter = strSearch
                    End If
                Else
                    If mstrReviewerIds.Trim.Length > 0 Then
                        dgREView.RowFilter = "employeeunkid NOT IN(" & mstrReviewerIds & ")"
                    Else
                        dgREView.RowFilter = strSearch
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtReviewerEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtReviewerEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReviewerEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvREmployee.Rows.Count > 0 Then
                        If dgvREmployee.SelectedRows(0).Index = dgvREmployee.Rows(dgvREmployee.RowCount - 1).Index Then Exit Sub
                        dgvREmployee.Rows(dgvREmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvREmployee.Rows.Count > 0 Then
                        If dgvREmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvREmployee.Rows(dgvREmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtReviewerEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssessor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAssessor.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAssessor.Rows.Count > 0 Then
                        If dgvAssessor.SelectedRows(0).Index = dgvAssessor.Rows(dgvAssessor.RowCount - 1).Index Then Exit Sub
                        dgvAssessor.Rows(dgvAssessor.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAssessor.Rows.Count > 0 Then
                        If dgvAssessor.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAssessor.Rows(dgvAssessor.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessor_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtAssessor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAssessor.TextChanged
        Dim strSearch As String = ""
        Try

            If txtAssessor.Text.Trim.Length > 0 Then
                strSearch = dgcolhAName.DataPropertyName & " LIKE '%" & txtAssessor.Text & "%' OR " & _
                            dgcolhACode.DataPropertyName & " LIKE '%" & txtAssessor.Text & "%'"
                objchkAssessor.Checked = False : objchkAssessor.Enabled = False
            Else
                objchkAssessor.Enabled = True
            End If

            dgAView.RowFilter = strSearch

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessor_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtReviewer_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReviewer.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvReviewer.Rows.Count > 0 Then
                        If dgvReviewer.SelectedRows(0).Index = dgvReviewer.Rows(dgvReviewer.RowCount - 1).Index Then Exit Sub
                        dgvReviewer.Rows(dgvReviewer.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvReviewer.Rows.Count > 0 Then
                        If dgvReviewer.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvReviewer.Rows(dgvReviewer.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtReviewer_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtReviewer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReviewer.TextChanged
        Dim strSearch As String = ""
        Try

            If txtReviewer.Text.Trim.Length > 0 Then
                strSearch = dgcolhReviewer.DataPropertyName & " LIKE '%" & txtReviewer.Text & "%' OR " & _
                            dgcolhRCode.DataPropertyName & " LIKE '%" & txtReviewer.Text & "%'"
                chkReviewer.Checked = False : chkReviewer.Enabled = False
            Else
                chkReviewer.Enabled = True
            End If

            dgRView.RowFilter = strSearch

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtReviewer_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick

            Dim StrFilter As String = dgAEView.RowFilter
            If dsTran.Tables("Emp") IsNot Nothing AndAlso dsTran.Tables("Emp").Rows.Count > 0 Then
                If objchkEmployee.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In dsTran.Tables("Emp").Rows
                        dr.Item("IsCheck") = CBool(objchkEmployee.CheckState)
                    Next
                    dgAEView = dsTran.Tables("Emp").DefaultView
                    dgAEView.RowFilter = StrFilter
                    SetDataSources(dgvAEmployee)
                End If
            End If

            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intEmpId As Integer = CInt(dgvAEmployee.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value)
                Dim dTemp() As DataRow = dsTran.Tables("Emp").Select(objdgcolhEmpId.DataPropertyName & " = '" & intEmpId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(objdgcolhEmpId.DataPropertyName.ToString)) = True Then
                        dsTran.Tables("Emp").Rows(dsTran.Tables("Emp").Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName) = CBool(dsTran.Tables("Emp").Rows(dsTran.Tables("Emp").Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName))
                    Else
                        dsTran.Tables("Emp").Rows(dsTran.Tables("Emp").Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName) = Not CBool(dsTran.Tables("Emp").Rows(dsTran.Tables("Emp").Rows.IndexOf(dTemp(0)))(objdgcolhECheck.DataPropertyName))
                    End If
                Else
                dsTran.Tables("Emp").Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName) = Not CBool(dsTran.Tables("Emp").Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName))
                End If

                Dim drRow As DataRow() = dsTran.Tables("Emp").Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If dsTran.Tables("Emp").Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAssessor.CheckedChanged
        Try
            RemoveHandler dgvAssessor.CellContentClick, AddressOf dgvAssessor_CellContentClick
            Dim StrFilter As String = dgAView.RowFilter
            If dsTran.Tables("Assessor") IsNot Nothing AndAlso dsTran.Tables("Assessor").Rows.Count > 0 Then
                If objchkAssessor.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In dsTran.Tables("Assessor").Rows
                        dr.Item("IsCheck") = CBool(objchkAssessor.CheckState)
                    Next
                    dgAView = dsTran.Tables("Assessor").DefaultView
                    dgAView.RowFilter = StrFilter
                    SetDataSources(dgvAssessor)
                End If
            End If
            AddHandler dgvAssessor.CellContentClick, AddressOf dgvAssessor_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAssessor_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAssessor.CellContentClick
        Try
            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
            If e.ColumnIndex = objdgcolhACheck.Index Then

                If Me.dgvAssessor.IsCurrentCellDirty Then
                    Me.dgvAssessor.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intEmpId As Integer = CInt(dgvAEmployee.Rows(e.RowIndex).Cells(dgcolhAEmpId.Index).Value)
                Dim dTemp() As DataRow = dsTran.Tables("Assessor").Select(dgcolhAEmpId.DataPropertyName & " = '" & intEmpId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(dgcolhAEmpId.DataPropertyName.ToString)) = True Then
                        dsTran.Tables("Assessor").Rows(dsTran.Tables("Assessor").Rows.IndexOf(dTemp(0)))(objdgcolhACheck.DataPropertyName) = CBool(dsTran.Tables("Assessor").Rows(dsTran.Tables("Assessor").Rows.IndexOf(dTemp(0)))(objdgcolhACheck.DataPropertyName))
                    Else
                        dsTran.Tables("Assessor").Rows(dsTran.Tables("Assessor").Rows.IndexOf(dTemp(0)))(objdgcolhACheck.DataPropertyName) = Not CBool(dsTran.Tables("Assessor").Rows(dsTran.Tables("Assessor").Rows.IndexOf(dTemp(0)))(objdgcolhACheck.DataPropertyName))
                    End If
                    dsTran.Tables("Assessor").Rows(e.RowIndex)(objdgcolhECheck.DataPropertyName) = Not CBool(dsTran.Tables("Assessor").Rows(e.RowIndex)(objdgcolhACheck.DataPropertyName))
                End If

                Dim drRow As DataRow() = dsTran.Tables("Assessor").Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If dsTran.Tables("Assessor").Rows.Count = drRow.Length Then
                        objchkAssessor.CheckState = CheckState.Checked
                    Else
                        objchkAssessor.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAssessor.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssessor_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkREmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkREmployee.CheckedChanged
        Try
            RemoveHandler dgvREmployee.CellContentClick, AddressOf dgvREmployee_CellContentClick
            Dim StrRowFilter As String = dgREView.RowFilter
            If dsTran.Tables("REmp") IsNot Nothing AndAlso dsTran.Tables("REmp").Rows.Count > 0 Then
                If chkREmployee.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In dsTran.Tables("REmp").Rows
                        dr.Item("IsCheck") = CBool(chkREmployee.CheckState)
                    Next
                    dgREView = dsTran.Tables("REmp").DefaultView
                    dgREView.RowFilter = StrRowFilter
                    SetDataSources(dgvREmployee)
                End If
            End If
            AddHandler dgvREmployee.CellContentClick, AddressOf dgvREmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkREmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvREmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvREmployee.CellContentClick
        Try
            RemoveHandler chkREmployee.CheckedChanged, AddressOf chkREmployee_CheckedChanged
            If e.ColumnIndex = objdgcolhRECheck.Index Then

                If Me.dgvREmployee.IsCurrentCellDirty Then
                    Me.dgvREmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intEmpId As Integer = CInt(dgvREmployee.Rows(e.RowIndex).Cells(objdgcolhREmpId.Index).Value)
                Dim dTemp() As DataRow = dsTran.Tables("REmp").Select(objdgcolhREmpId.DataPropertyName & " = '" & intEmpId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(objdgcolhREmpId.DataPropertyName.ToString)) = True Then
                        dsTran.Tables("REmp").Rows(dsTran.Tables("REmp").Rows.IndexOf(dTemp(0)))(objdgcolhRECheck.DataPropertyName) = CBool(dsTran.Tables("REmp").Rows(dsTran.Tables("REmp").Rows.IndexOf(dTemp(0)))(objdgcolhRECheck.DataPropertyName))
                    Else
                        dsTran.Tables("REmp").Rows(dsTran.Tables("REmp").Rows.IndexOf(dTemp(0)))(objdgcolhRECheck.DataPropertyName) = Not CBool(dsTran.Tables("REmp").Rows(dsTran.Tables("REmp").Rows.IndexOf(dTemp(0)))(objdgcolhRECheck.DataPropertyName))
                    End If
                Else
                    dsTran.Tables("REmp").Rows(e.RowIndex)(objdgcolhRECheck.DataPropertyName) = Not CBool(dsTran.Tables("REmp").Rows(e.RowIndex)(objdgcolhRECheck.DataPropertyName))
                End If

                Dim drRow As DataRow() = dsTran.Tables("REmp").Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If dsTran.Tables("REmp").Rows.Count = drRow.Length Then
                        chkREmployee.CheckState = CheckState.Checked
                    Else
                        chkREmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    chkREmployee.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler chkREmployee.CheckedChanged, AddressOf chkREmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvREmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkReviewer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReviewer.CheckedChanged
        Try
            RemoveHandler dgvReviewer.CellContentClick, AddressOf dgvReviewer_CellContentClick
            Dim StrFilter As String = dgRView.RowFilter
            If dsTran.Tables("Reviewer") IsNot Nothing AndAlso dsTran.Tables("Reviewer").Rows.Count > 0 Then
                If chkReviewer.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In dsTran.Tables("Reviewer").Rows
                        dr.Item("IsCheck") = CBool(chkReviewer.CheckState)
                    Next
                    dgRView = dsTran.Tables("Reviewer").DefaultView
                    dgRView.RowFilter = StrFilter
                    SetDataSources(dgvReviewer)
                End If
            End If
            AddHandler dgvReviewer.CellContentClick, AddressOf dgvReviewer_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkReviewer_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvReviewer_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReviewer.CellContentClick
        Try
            RemoveHandler chkReviewer.CheckedChanged, AddressOf chkReviewer_CheckedChanged
            If e.ColumnIndex = objdgcolhRCheck.Index Then

                If Me.dgvReviewer.IsCurrentCellDirty Then
                    Me.dgvReviewer.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim intEmpId As Integer = CInt(dgvReviewer.Rows(e.RowIndex).Cells(objdgcolhRUnkid.Index).Value)
                Dim dTemp() As DataRow = dsTran.Tables("Reviewer").Select(objdgcolhRUnkid.DataPropertyName & " = '" & intEmpId & "'")

                If dTemp.Length > 0 Then
                    If CBool(dTemp(0)(objdgcolhRUnkid.DataPropertyName.ToString)) = True Then
                        dsTran.Tables("Reviewer").Rows(dsTran.Tables("Reviewer").Rows.IndexOf(dTemp(0)))(objdgcolhRCheck.DataPropertyName) = CBool(dsTran.Tables("Reviewer").Rows(dsTran.Tables("Reviewer").Rows.IndexOf(dTemp(0)))(objdgcolhRCheck.DataPropertyName))
                    Else
                        dsTran.Tables("Reviewer").Rows(dsTran.Tables("Reviewer").Rows.IndexOf(dTemp(0)))(objdgcolhRCheck.DataPropertyName) = Not CBool(dsTran.Tables("Reviewer").Rows(dsTran.Tables("Reviewer").Rows.IndexOf(dTemp(0)))(objdgcolhRCheck.DataPropertyName))
                    End If
                Else
                    dsTran.Tables("Reviewer").Rows(e.RowIndex)(objdgcolhRCheck.DataPropertyName) = Not CBool(dsTran.Tables("Reviewer").Rows(e.RowIndex)(objdgcolhRCheck.DataPropertyName))
                End If

                Dim drRow As DataRow() = dsTran.Tables("Reviewer").Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If dsTran.Tables("Reviewer").Rows.Count = drRow.Length Then
                        chkReviewer.CheckState = CheckState.Checked
                    Else
                        chkReviewer.CheckState = CheckState.Indeterminate
                    End If
                Else
                    chkReviewer.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler chkReviewer.CheckedChanged, AddressOf chkReviewer_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvReviewer_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAUnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAUnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAAssign.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnRAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnRAssign.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnRUnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnRUnAssign.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.tabpAssessor.Text = Language._Object.getCaption(Me.tabpAssessor.Name, Me.tabpAssessor.Text)
			Me.tabpReviewer.Text = Language._Object.getCaption(Me.tabpReviewer.Name, Me.tabpReviewer.Text)
			Me.dgcolhACode.HeaderText = Language._Object.getCaption(Me.dgcolhACode.Name, Me.dgcolhACode.HeaderText)
			Me.dgcolhAName.HeaderText = Language._Object.getCaption(Me.dgcolhAName.Name, Me.dgcolhAName.HeaderText)
			Me.dgcolhAEmpId.HeaderText = Language._Object.getCaption(Me.dgcolhAEmpId.Name, Me.dgcolhAEmpId.HeaderText)
			Me.chkREmployee.Text = Language._Object.getCaption(Me.chkREmployee.Name, Me.chkREmployee.Text)
			Me.chkReviewer.Text = Language._Object.getCaption(Me.chkReviewer.Name, Me.chkReviewer.Text)
			Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
			Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
			Me.dgcolhRECode.HeaderText = Language._Object.getCaption(Me.dgcolhRECode.Name, Me.dgcolhRECode.HeaderText)
			Me.dgcolhREmployee.HeaderText = Language._Object.getCaption(Me.dgcolhREmployee.Name, Me.dgcolhREmployee.HeaderText)
			Me.dgcolhRCode.HeaderText = Language._Object.getCaption(Me.dgcolhRCode.Name, Me.dgcolhRCode.HeaderText)
			Me.dgcolhReviewer.HeaderText = Language._Object.getCaption(Me.dgcolhReviewer.Name, Me.dgcolhReviewer.HeaderText)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Check atleast one employee to assign as Assessor.")
			Language.setMessage(mstrModuleName, 2, "Please Check atleast one Assessor to unassign.")
			Language.setMessage(mstrModuleName, 3, "Plese Check atleast one employee to assign as Reviewer.")
			Language.setMessage(mstrModuleName, 4, "Please Check atleast one Reviewer to unassign.")
			Language.setMessage(mstrModuleName, 5, "Problem in Inserting Assessor(s).")
			Language.setMessage(mstrModuleName, 6, "Problem in Inserting Reviewer(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class

'Public Class frmAssessor_AddEdit

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmAssessor_AddEdit"
'    Private objAssessor As clsAssessor
'    Private mstrAssessorIds As String = String.Empty
'    Private mstrToDeleteIds As String = String.Empty
'#End Region

'#Region " Private Function "

'    Private Sub Fill_Employee_List()
'        Dim ObjEmp As New clsEmployee_Master
'        Dim dsList As New DataSet
'        Dim lvItem As ListViewItem
'        Dim dtTable As DataTable
'        Try

'            'S.SANDEEP [ 29 JUNE 2011 ] -- START
'            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'            'dsList = ObjEmp.GetEmployeeList("Emp", False, True)
'            'Sohail (06 Jan 2012) -- Start
'            'TRA - ENHANCEMENT
'            'dsList = ObjEmp.GetEmployeeList("Emp", False, Not ConfigParameter._Object._IsIncludeInactiveEmp) 
'            dsList = ObjEmp.GetEmployeeList("Emp", False, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
'            'Sohail (06 Jan 2012) -- End
'            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

'            lvEmployee.Items.Clear()

'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END


'            If mstrAssessorIds.Length > 0 Then
'                mstrAssessorIds = Mid(mstrAssessorIds, 2)
'                dtTable = New DataView(dsList.Tables("Emp"), "employeeunkid NOT IN(" & mstrAssessorIds & ")", "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtTable = New DataView(dsList.Tables("Emp"), "", "", DataViewRowState.CurrentRows).ToTable
'            End If

'            For Each dtRow As DataRow In dtTable.Rows
'                lvItem = New ListViewItem

'                lvItem.Text = ""
'                lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
'                lvItem.Tag = dtRow.Item("employeeunkid")

'                lvEmployee.Items.Add(lvItem)

'                lvItem = Nothing
'            Next

'            If lvEmployee.Items.Count > 10 Then
'                colhEmployee.Width = colhEmployee.Width - 20
'            Else
'                colhEmployee.Width = 210
'            End If
'            lvEmployee.GridLines = False

'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Fill_Assessor_List()
'        Dim dsList As New DataSet
'        Dim lvItem As ListViewItem
'        Try


'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            RemoveHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END


'            dsList = objAssessor.GetList("Assessor")
'            lvAssessor.Items.Clear()

'            For Each dtRow As DataRow In dsList.Tables("Assessor").Rows
'                lvItem = New ListViewItem

'                lvItem.Text = ""
'                lvItem.SubItems.Add(dtRow.Item("assessorname").ToString)
'                lvItem.Tag = dtRow.Item("assessormasterunkid")

'                mstrAssessorIds &= "," & dtRow.Item("employeeunkid").ToString

'                lvAssessor.Items.Add(lvItem)

'                lvItem = Nothing
'            Next

'            If lvAssessor.Items.Count > 10 Then
'                colhAssessor.Width = colhAssessor.Width - 20
'            Else
'                colhAssessor.Width = 210
'            End If
'            lvAssessor.GridLines = False

'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            AddHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Assessor_List", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Check_State(ByVal lvView As ListView, ByVal chkState As Boolean)
'        Try
'            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            RemoveHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            For i As Integer = 0 To lvView.Items.Count - 1
'                lvView.Items(i).Checked = chkState
'            Next
'                AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            AddHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            If lvAssessor.Items.Count > 0 Then
'                For Each lvItem As ListViewItem In lvAssessor.Items
'                    objAssessor._EmployeeIds &= "," & lvItem.Tag.ToString
'                Next

'                objAssessor._EmployeeIds = Mid(objAssessor._EmployeeIds, 2)
'                objAssessor._Userunkid = User._Object._Userunkid
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetVisibility()

'        Try
'            btnSave.Enabled = User._Object.Privilege._AllowAddAssessor
'            objbtnUnAssign.Enabled = User._Object.Privilege._AllowRemoveAssessor
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'#End Region

'#Region " Form's Events "
'    Private Sub frmAssessor_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objAssessor = New clsAssessor
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            Call SetVisibility()
'            Call Fill_Assessor_List()
'            Call Fill_Employee_List()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 20 AUG 2011 ] -- START
'    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsAssessor.SetMessages()
'            objfrm._Other_ModuleNames = "clsAssessor"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 AUG 2011 ] -- END

'#End Region

'#Region " Button's Events "

'    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
'        Try
'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            RemoveHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END

'            If lvEmployee.CheckedItems.Count > 0 Then
'                For Each lvItems As ListViewItem In lvEmployee.CheckedItems
'                    Dim lvListItem As New ListViewItem
'                    lvListItem = lvItems
'                    lvEmployee.Items.Remove(lvItems)
'                    lvAssessor.Items.Add(lvListItem)
'                    lvListItem.Checked = False
'                    lvListItem = Nothing
'                Next
'            End If
'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            AddHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
'        Try
'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            RemoveHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END

'            If lvAssessor.CheckedItems.Count > 0 Then
'                mstrToDeleteIds = ""
'                For Each lvItems As ListViewItem In lvAssessor.CheckedItems
'                    Dim lvListItem As New ListViewItem
'                    lvListItem = lvItems
'                    mstrToDeleteIds &= "," & lvItems.Tag.ToString
'                    lvAssessor.Items.Remove(lvItems)
'                    lvEmployee.Items.Add(lvListItem)
'                    lvListItem.Checked = False
'                    lvListItem = Nothing
'                Next
'                mstrToDeleteIds = Mid(mstrToDeleteIds, 2)
'                objAssessor._AssessormasterIds = mstrToDeleteIds

'                objAssessor.Delete()
'            End If
'                'Pinkal (27-oct-2010) -- START
'            Fill_Assessor_List()
'            'Pinkal (27-oct-2010) -- END

'            'S.SANDEEP [ 29 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES 
'            'TYPE : EMPLOYEMENT CONTRACT PROCESS
'            AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
'            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            AddHandler lvAssessor.ItemChecked, AddressOf lvAssessor_ItemChecked
'            AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            'S.SANDEEP [ 29 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        If lvAssessor.Items.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set at least one assessor in order to perform save operation."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If
'        Dim blnFlag As Boolean = False
'        Try
'            Call SetValue()


'            blnFlag = objAssessor.Insert

'            If blnFlag = True Then
'                Me.Close()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
'        Try
'            Select Case objchkEmployee.CheckState
'                Case CheckState.Checked, CheckState.Unchecked
'                    Call Check_State(lvEmployee, CBool(objchkEmployee.CheckState))
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvEmployee_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployee.ItemChecked
'        Try
'            If lvEmployee.CheckedItems.Count <= 0 Then
'                RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'                objchkEmployee.CheckState = CheckState.Unchecked
'                AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
'                RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'                objchkEmployee.CheckState = CheckState.Indeterminate
'                AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
'                RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'                objchkEmployee.CheckState = CheckState.Checked
'                AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvEmployee_ItemChecked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvAssessor_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAssessor.ItemChecked
'        Try
'            If lvAssessor.CheckedItems.Count <= 0 Then
'                RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'                objchkAssessor.CheckState = CheckState.Unchecked
'                AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            ElseIf lvAssessor.CheckedItems.Count < lvAssessor.Items.Count Then
'                RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'                objchkAssessor.CheckState = CheckState.Indeterminate
'                AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            ElseIf lvAssessor.CheckedItems.Count = lvAssessor.Items.Count Then
'                RemoveHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'                objchkAssessor.CheckState = CheckState.Checked
'                AddHandler objchkAssessor.CheckedChanged, AddressOf objchkAssessor_CheckedChanged
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAssessor_ItemChecked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objchkAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAssessor.CheckedChanged
'        Try
'            Select Case objchkAssessor.CheckState
'                Case CheckState.Checked, CheckState.Unchecked
'                    Call Check_State(lvAssessor, CBool(objchkAssessor.CheckState))
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkAssessor_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region


'End Class