﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessmentRatio_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessmentRatio_List))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvRatioList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhcheck = New System.Windows.Forms.ColumnHeader
        Me.colhAllocation = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhGeneralEvalution = New System.Windows.Forms.ColumnHeader
        Me.colhBSC = New System.Windows.Forms.ColumnHeader
        Me.objcolhTranId = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowClosePeriodData = New System.Windows.Forms.CheckBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchAllocation = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboAllocationData = New System.Windows.Forms.ComboBox
        Me.objlblAllocationCaption = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(839, 58)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Assessment Ratio List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 424)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(839, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(449, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 130
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(545, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 129
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(641, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 128
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(737, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvRatioList
        '
        Me.lvRatioList.BackColorOnChecked = False
        Me.lvRatioList.CheckBoxes = True
        Me.lvRatioList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhcheck, Me.colhAllocation, Me.colhPeriod, Me.colhGeneralEvalution, Me.colhBSC, Me.objcolhTranId})
        Me.lvRatioList.FullRowSelect = True
        Me.lvRatioList.GridLines = True
        Me.lvRatioList.GroupingColumn = Nothing
        Me.lvRatioList.HideSelection = False
        Me.lvRatioList.Location = New System.Drawing.Point(12, 134)
        Me.lvRatioList.MultiSelect = False
        Me.lvRatioList.Name = "lvRatioList"
        Me.lvRatioList.ShowSelectAll = True
        Me.lvRatioList.Size = New System.Drawing.Size(815, 285)
        Me.lvRatioList.Sortable = True
        Me.lvRatioList.TabIndex = 5
        Me.lvRatioList.UseCompatibleStateImageBehavior = False
        Me.lvRatioList.View = System.Windows.Forms.View.Details
        '
        'objcolhcheck
        '
        Me.objcolhcheck.Tag = "objcolhcheck"
        Me.objcolhcheck.Text = ""
        Me.objcolhcheck.Width = 25
        '
        'colhAllocation
        '
        Me.colhAllocation.Tag = "colhAllocation"
        Me.colhAllocation.Text = "Ratio By Allocation"
        Me.colhAllocation.Width = 530
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'colhGeneralEvalution
        '
        Me.colhGeneralEvalution.Tag = "colhGeneralEvalution"
        Me.colhGeneralEvalution.Text = "General Evaluation (%)"
        Me.colhGeneralEvalution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhGeneralEvalution.Width = 125
        '
        'colhBSC
        '
        Me.colhBSC.Tag = "colhBSC"
        Me.colhBSC.Text = "BSC (%)"
        Me.colhBSC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhBSC.Width = 110
        '
        'objcolhTranId
        '
        Me.objcolhTranId.Tag = "objcolhTranId"
        Me.objcolhTranId.Text = "objcolhTranId"
        Me.objcolhTranId.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowClosePeriodData)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocations)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocationData)
        Me.gbFilterCriteria.Controls.Add(Me.objlblAllocationCaption)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(815, 64)
        Me.gbFilterCriteria.TabIndex = 6
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowClosePeriodData
        '
        Me.chkShowClosePeriodData.BackColor = System.Drawing.Color.Transparent
        Me.chkShowClosePeriodData.Location = New System.Drawing.Point(553, 4)
        Me.chkShowClosePeriodData.Name = "chkShowClosePeriodData"
        Me.chkShowClosePeriodData.Size = New System.Drawing.Size(206, 17)
        Me.chkShowClosePeriodData.TabIndex = 177
        Me.chkShowClosePeriodData.Text = "Show Close Period Data"
        Me.chkShowClosePeriodData.UseVisualStyleBackColor = False
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(10, 36)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(63, 15)
        Me.lblAllocation.TabIndex = 175
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(79, 33)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(149, 21)
        Me.cboAllocations.TabIndex = 174
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(784, 33)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 167
        '
        'objbtnSearchAllocation
        '
        Me.objbtnSearchAllocation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAllocation.BorderSelected = False
        Me.objbtnSearchAllocation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAllocation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAllocation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAllocation.Location = New System.Drawing.Point(553, 33)
        Me.objbtnSearchAllocation.Name = "objbtnSearchAllocation"
        Me.objbtnSearchAllocation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAllocation.TabIndex = 165
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(636, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(142, 21)
        Me.cboPeriod.TabIndex = 164
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(580, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(50, 14)
        Me.lblPeriod.TabIndex = 163
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocationData
        '
        Me.cboAllocationData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocationData.DropDownWidth = 350
        Me.cboAllocationData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocationData.FormattingEnabled = True
        Me.cboAllocationData.Location = New System.Drawing.Point(344, 33)
        Me.cboAllocationData.Name = "cboAllocationData"
        Me.cboAllocationData.Size = New System.Drawing.Size(203, 21)
        Me.cboAllocationData.TabIndex = 162
        '
        'objlblAllocationCaption
        '
        Me.objlblAllocationCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAllocationCaption.Location = New System.Drawing.Point(234, 36)
        Me.objlblAllocationCaption.Name = "objlblAllocationCaption"
        Me.objlblAllocationCaption.Size = New System.Drawing.Size(104, 14)
        Me.objlblAllocationCaption.TabIndex = 161
        Me.objlblAllocationCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(788, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(765, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(18, 140)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 7
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'frmAssessmentRatio_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 479)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.lvRatioList)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessmentRatio_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessment Ratio List"
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvRatioList As eZee.Common.eZeeListView
    Friend WithEvents colhAllocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGeneralEvalution As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objlblAllocationCaption As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboAllocationData As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchAllocation As eZee.Common.eZeeGradientButton
    Friend WithEvents colhBSC As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowClosePeriodData As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhTranId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhcheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
End Class
