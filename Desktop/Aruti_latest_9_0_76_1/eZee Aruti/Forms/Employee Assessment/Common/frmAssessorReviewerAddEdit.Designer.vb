﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessorReviewerAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessorReviewerAddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbAssignedEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tabcAssignedEmployee = New System.Windows.Forms.TabControl
        Me.tabpAssessor = New System.Windows.Forms.TabPage
        Me.pnlAData = New System.Windows.Forms.Panel
        Me.tblpAssessor = New System.Windows.Forms.TableLayoutPanel
        Me.txtaSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlAssessor = New System.Windows.Forms.Panel
        Me.objchkAssessor = New System.Windows.Forms.CheckBox
        Me.dgvAssessor = New System.Windows.Forms.DataGridView
        Me.objdgcolhaCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhaCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhaEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhaEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhATranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnDeleteA = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabpReviewer = New System.Windows.Forms.TabPage
        Me.pnlRData = New System.Windows.Forms.Panel
        Me.tblpReviewer = New System.Windows.Forms.TableLayoutPanel
        Me.txtrSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlReviewer = New System.Windows.Forms.Panel
        Me.objchkReviewer = New System.Windows.Forms.CheckBox
        Me.dgvReviewer = New System.Windows.Forms.DataGridView
        Me.objdgcolhrCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhrCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhrDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhrJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnDeleteR = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkIsExternalEntity = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.lblApproverName = New System.Windows.Forms.Label
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.LblUser = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbAssignedEmployee.SuspendLayout()
        Me.tabcAssignedEmployee.SuspendLayout()
        Me.tabpAssessor.SuspendLayout()
        Me.pnlAData.SuspendLayout()
        Me.tblpAssessor.SuspendLayout()
        Me.objpnlAssessor.SuspendLayout()
        CType(Me.dgvAssessor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpReviewer.SuspendLayout()
        Me.pnlRData.SuspendLayout()
        Me.tblpReviewer.SuspendLayout()
        Me.objpnlReviewer.SuspendLayout()
        CType(Me.dgvReviewer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbInfo.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbAssignedEmployee)
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(944, 531)
        Me.pnlMain.TabIndex = 0
        '
        'gbAssignedEmployee
        '
        Me.gbAssignedEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbAssignedEmployee.Checked = False
        Me.gbAssignedEmployee.CollapseAllExceptThis = False
        Me.gbAssignedEmployee.CollapsedHoverImage = Nothing
        Me.gbAssignedEmployee.CollapsedNormalImage = Nothing
        Me.gbAssignedEmployee.CollapsedPressedImage = Nothing
        Me.gbAssignedEmployee.CollapseOnLoad = False
        Me.gbAssignedEmployee.Controls.Add(Me.tabcAssignedEmployee)
        Me.gbAssignedEmployee.ExpandedHoverImage = Nothing
        Me.gbAssignedEmployee.ExpandedNormalImage = Nothing
        Me.gbAssignedEmployee.ExpandedPressedImage = Nothing
        Me.gbAssignedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignedEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignedEmployee.HeaderHeight = 25
        Me.gbAssignedEmployee.HeaderMessage = ""
        Me.gbAssignedEmployee.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssignedEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignedEmployee.HeightOnCollapse = 0
        Me.gbAssignedEmployee.LeftTextSpace = 0
        Me.gbAssignedEmployee.Location = New System.Drawing.Point(339, 3)
        Me.gbAssignedEmployee.Name = "gbAssignedEmployee"
        Me.gbAssignedEmployee.OpenHeight = 300
        Me.gbAssignedEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignedEmployee.ShowBorder = True
        Me.gbAssignedEmployee.ShowCheckBox = False
        Me.gbAssignedEmployee.ShowCollapseButton = False
        Me.gbAssignedEmployee.ShowDefaultBorderColor = True
        Me.gbAssignedEmployee.ShowDownButton = False
        Me.gbAssignedEmployee.ShowHeader = True
        Me.gbAssignedEmployee.Size = New System.Drawing.Size(602, 476)
        Me.gbAssignedEmployee.TabIndex = 6
        Me.gbAssignedEmployee.Temp = 0
        Me.gbAssignedEmployee.Text = "Assigned Employee"
        Me.gbAssignedEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcAssignedEmployee
        '
        Me.tabcAssignedEmployee.Controls.Add(Me.tabpAssessor)
        Me.tabcAssignedEmployee.Controls.Add(Me.tabpReviewer)
        Me.tabcAssignedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcAssignedEmployee.Location = New System.Drawing.Point(4, 27)
        Me.tabcAssignedEmployee.Multiline = True
        Me.tabcAssignedEmployee.Name = "tabcAssignedEmployee"
        Me.tabcAssignedEmployee.SelectedIndex = 0
        Me.tabcAssignedEmployee.Size = New System.Drawing.Size(594, 445)
        Me.tabcAssignedEmployee.TabIndex = 5
        '
        'tabpAssessor
        '
        Me.tabpAssessor.Controls.Add(Me.pnlAData)
        Me.tabpAssessor.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssessor.Name = "tabpAssessor"
        Me.tabpAssessor.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpAssessor.Size = New System.Drawing.Size(586, 419)
        Me.tabpAssessor.TabIndex = 0
        Me.tabpAssessor.Text = "Assessor to Employee(s)"
        Me.tabpAssessor.UseVisualStyleBackColor = True
        '
        'pnlAData
        '
        Me.pnlAData.Controls.Add(Me.tblpAssessor)
        Me.pnlAData.Controls.Add(Me.btnDeleteA)
        Me.pnlAData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAData.Location = New System.Drawing.Point(3, 3)
        Me.pnlAData.Name = "pnlAData"
        Me.pnlAData.Size = New System.Drawing.Size(580, 413)
        Me.pnlAData.TabIndex = 306
        '
        'tblpAssessor
        '
        Me.tblpAssessor.ColumnCount = 1
        Me.tblpAssessor.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessor.Controls.Add(Me.txtaSearch, 0, 0)
        Me.tblpAssessor.Controls.Add(Me.objpnlAssessor, 0, 1)
        Me.tblpAssessor.Dock = System.Windows.Forms.DockStyle.Top
        Me.tblpAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessor.Location = New System.Drawing.Point(0, 0)
        Me.tblpAssessor.Name = "tblpAssessor"
        Me.tblpAssessor.RowCount = 2
        Me.tblpAssessor.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessor.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessor.Size = New System.Drawing.Size(580, 374)
        Me.tblpAssessor.TabIndex = 304
        '
        'txtaSearch
        '
        Me.txtaSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtaSearch.Flags = 0
        Me.txtaSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtaSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtaSearch.Name = "txtaSearch"
        Me.txtaSearch.Size = New System.Drawing.Size(574, 21)
        Me.txtaSearch.TabIndex = 106
        '
        'objpnlAssessor
        '
        Me.objpnlAssessor.Controls.Add(Me.objchkAssessor)
        Me.objpnlAssessor.Controls.Add(Me.dgvAssessor)
        Me.objpnlAssessor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlAssessor.Location = New System.Drawing.Point(3, 29)
        Me.objpnlAssessor.Name = "objpnlAssessor"
        Me.objpnlAssessor.Size = New System.Drawing.Size(574, 342)
        Me.objpnlAssessor.TabIndex = 107
        '
        'objchkAssessor
        '
        Me.objchkAssessor.AutoSize = True
        Me.objchkAssessor.Location = New System.Drawing.Point(7, 5)
        Me.objchkAssessor.Name = "objchkAssessor"
        Me.objchkAssessor.Size = New System.Drawing.Size(15, 14)
        Me.objchkAssessor.TabIndex = 104
        Me.objchkAssessor.UseVisualStyleBackColor = True
        '
        'dgvAssessor
        '
        Me.dgvAssessor.AllowUserToAddRows = False
        Me.dgvAssessor.AllowUserToDeleteRows = False
        Me.dgvAssessor.AllowUserToResizeColumns = False
        Me.dgvAssessor.AllowUserToResizeRows = False
        Me.dgvAssessor.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAssessor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAssessor.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAssessor.ColumnHeadersHeight = 21
        Me.dgvAssessor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAssessor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhaCheck, Me.objdgcolhaCode, Me.objdgcolhaEmp, Me.dgcolhaDepartment, Me.dgcolhaJob, Me.objdgcolhaEmpId, Me.objdgcolhAMasterId, Me.objdgcolhATranId})
        Me.dgvAssessor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAssessor.Location = New System.Drawing.Point(0, 0)
        Me.dgvAssessor.MultiSelect = False
        Me.dgvAssessor.Name = "dgvAssessor"
        Me.dgvAssessor.RowHeadersVisible = False
        Me.dgvAssessor.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAssessor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssessor.Size = New System.Drawing.Size(574, 342)
        Me.dgvAssessor.TabIndex = 105
        '
        'objdgcolhaCheck
        '
        Me.objdgcolhaCheck.HeaderText = ""
        Me.objdgcolhaCheck.Name = "objdgcolhaCheck"
        Me.objdgcolhaCheck.Width = 25
        '
        'objdgcolhaCode
        '
        Me.objdgcolhaCode.HeaderText = "Code"
        Me.objdgcolhaCode.Name = "objdgcolhaCode"
        Me.objdgcolhaCode.ReadOnly = True
        Me.objdgcolhaCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhaCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhaCode.Width = 70
        '
        'objdgcolhaEmp
        '
        Me.objdgcolhaEmp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.objdgcolhaEmp.HeaderText = "Employee"
        Me.objdgcolhaEmp.Name = "objdgcolhaEmp"
        Me.objdgcolhaEmp.ReadOnly = True
        Me.objdgcolhaEmp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhaEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhaDepartment
        '
        Me.dgcolhaDepartment.HeaderText = "Department"
        Me.dgcolhaDepartment.Name = "dgcolhaDepartment"
        '
        'dgcolhaJob
        '
        Me.dgcolhaJob.HeaderText = "Job"
        Me.dgcolhaJob.Name = "dgcolhaJob"
        Me.dgcolhaJob.Width = 150
        '
        'objdgcolhaEmpId
        '
        Me.objdgcolhaEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhaEmpId.Name = "objdgcolhaEmpId"
        Me.objdgcolhaEmpId.Visible = False
        '
        'objdgcolhAMasterId
        '
        Me.objdgcolhAMasterId.HeaderText = "objdgcolhAMasterId"
        Me.objdgcolhAMasterId.Name = "objdgcolhAMasterId"
        Me.objdgcolhAMasterId.Visible = False
        '
        'objdgcolhATranId
        '
        Me.objdgcolhATranId.HeaderText = "objdgcolhATranId"
        Me.objdgcolhATranId.Name = "objdgcolhATranId"
        Me.objdgcolhATranId.Visible = False
        '
        'btnDeleteA
        '
        Me.btnDeleteA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteA.BackColor = System.Drawing.Color.White
        Me.btnDeleteA.BackgroundImage = CType(resources.GetObject("btnDeleteA.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteA.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteA.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteA.FlatAppearance.BorderSize = 0
        Me.btnDeleteA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteA.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteA.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteA.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.Location = New System.Drawing.Point(484, 380)
        Me.btnDeleteA.Name = "btnDeleteA"
        Me.btnDeleteA.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteA.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.Size = New System.Drawing.Size(93, 29)
        Me.btnDeleteA.TabIndex = 305
        Me.btnDeleteA.Text = "&Delete"
        Me.btnDeleteA.UseVisualStyleBackColor = True
        '
        'tabpReviewer
        '
        Me.tabpReviewer.Controls.Add(Me.pnlRData)
        Me.tabpReviewer.Location = New System.Drawing.Point(4, 22)
        Me.tabpReviewer.Name = "tabpReviewer"
        Me.tabpReviewer.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpReviewer.Size = New System.Drawing.Size(586, 419)
        Me.tabpReviewer.TabIndex = 1
        Me.tabpReviewer.Text = "Reviewer to Employee(s)"
        Me.tabpReviewer.UseVisualStyleBackColor = True
        '
        'pnlRData
        '
        Me.pnlRData.Controls.Add(Me.tblpReviewer)
        Me.pnlRData.Controls.Add(Me.btnDeleteR)
        Me.pnlRData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlRData.Location = New System.Drawing.Point(3, 3)
        Me.pnlRData.Name = "pnlRData"
        Me.pnlRData.Size = New System.Drawing.Size(580, 413)
        Me.pnlRData.TabIndex = 308
        '
        'tblpReviewer
        '
        Me.tblpReviewer.ColumnCount = 1
        Me.tblpReviewer.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpReviewer.Controls.Add(Me.txtrSearch, 0, 0)
        Me.tblpReviewer.Controls.Add(Me.objpnlReviewer, 0, 1)
        Me.tblpReviewer.Dock = System.Windows.Forms.DockStyle.Top
        Me.tblpReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpReviewer.Location = New System.Drawing.Point(0, 0)
        Me.tblpReviewer.Name = "tblpReviewer"
        Me.tblpReviewer.RowCount = 2
        Me.tblpReviewer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpReviewer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpReviewer.Size = New System.Drawing.Size(580, 374)
        Me.tblpReviewer.TabIndex = 306
        '
        'txtrSearch
        '
        Me.txtrSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtrSearch.Flags = 0
        Me.txtrSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtrSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtrSearch.Name = "txtrSearch"
        Me.txtrSearch.Size = New System.Drawing.Size(574, 21)
        Me.txtrSearch.TabIndex = 106
        '
        'objpnlReviewer
        '
        Me.objpnlReviewer.Controls.Add(Me.objchkReviewer)
        Me.objpnlReviewer.Controls.Add(Me.dgvReviewer)
        Me.objpnlReviewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlReviewer.Location = New System.Drawing.Point(3, 29)
        Me.objpnlReviewer.Name = "objpnlReviewer"
        Me.objpnlReviewer.Size = New System.Drawing.Size(574, 342)
        Me.objpnlReviewer.TabIndex = 107
        '
        'objchkReviewer
        '
        Me.objchkReviewer.AutoSize = True
        Me.objchkReviewer.Location = New System.Drawing.Point(7, 5)
        Me.objchkReviewer.Name = "objchkReviewer"
        Me.objchkReviewer.Size = New System.Drawing.Size(15, 14)
        Me.objchkReviewer.TabIndex = 104
        Me.objchkReviewer.UseVisualStyleBackColor = True
        '
        'dgvReviewer
        '
        Me.dgvReviewer.AllowUserToAddRows = False
        Me.dgvReviewer.AllowUserToDeleteRows = False
        Me.dgvReviewer.AllowUserToResizeColumns = False
        Me.dgvReviewer.AllowUserToResizeRows = False
        Me.dgvReviewer.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvReviewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvReviewer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvReviewer.ColumnHeadersHeight = 21
        Me.dgvReviewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvReviewer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhrCheck, Me.objdgcolhrCode, Me.objdgcolhrEmp, Me.dgcolhrDepartment, Me.dgcolhrJob, Me.objdgcolhrEmpId, Me.objdgcolhRMasterId, Me.objdgcolhRTranId})
        Me.dgvReviewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvReviewer.Location = New System.Drawing.Point(0, 0)
        Me.dgvReviewer.MultiSelect = False
        Me.dgvReviewer.Name = "dgvReviewer"
        Me.dgvReviewer.RowHeadersVisible = False
        Me.dgvReviewer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvReviewer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReviewer.Size = New System.Drawing.Size(574, 342)
        Me.dgvReviewer.TabIndex = 105
        '
        'objdgcolhrCheck
        '
        Me.objdgcolhrCheck.HeaderText = ""
        Me.objdgcolhrCheck.Name = "objdgcolhrCheck"
        Me.objdgcolhrCheck.Width = 25
        '
        'objdgcolhrCode
        '
        Me.objdgcolhrCode.HeaderText = "Code"
        Me.objdgcolhrCode.Name = "objdgcolhrCode"
        Me.objdgcolhrCode.ReadOnly = True
        Me.objdgcolhrCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhrCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhrCode.Width = 70
        '
        'objdgcolhrEmp
        '
        Me.objdgcolhrEmp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.objdgcolhrEmp.HeaderText = "Employee"
        Me.objdgcolhrEmp.Name = "objdgcolhrEmp"
        Me.objdgcolhrEmp.ReadOnly = True
        Me.objdgcolhrEmp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhrEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhrDepartment
        '
        Me.dgcolhrDepartment.HeaderText = "Department"
        Me.dgcolhrDepartment.Name = "dgcolhrDepartment"
        '
        'dgcolhrJob
        '
        Me.dgcolhrJob.HeaderText = "Job"
        Me.dgcolhrJob.Name = "dgcolhrJob"
        Me.dgcolhrJob.Width = 150
        '
        'objdgcolhrEmpId
        '
        Me.objdgcolhrEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhrEmpId.Name = "objdgcolhrEmpId"
        Me.objdgcolhrEmpId.Visible = False
        '
        'objdgcolhRMasterId
        '
        Me.objdgcolhRMasterId.HeaderText = "objdgcolhAMasterId"
        Me.objdgcolhRMasterId.Name = "objdgcolhRMasterId"
        Me.objdgcolhRMasterId.Visible = False
        '
        'objdgcolhRTranId
        '
        Me.objdgcolhRTranId.HeaderText = "objdgcolhATranId"
        Me.objdgcolhRTranId.Name = "objdgcolhRTranId"
        Me.objdgcolhRTranId.Visible = False
        '
        'btnDeleteR
        '
        Me.btnDeleteR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteR.BackColor = System.Drawing.Color.White
        Me.btnDeleteR.BackgroundImage = CType(resources.GetObject("btnDeleteR.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteR.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteR.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteR.FlatAppearance.BorderSize = 0
        Me.btnDeleteR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteR.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteR.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteR.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteR.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteR.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteR.Location = New System.Drawing.Point(484, 380)
        Me.btnDeleteR.Name = "btnDeleteR"
        Me.btnDeleteR.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteR.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteR.Size = New System.Drawing.Size(93, 29)
        Me.btnDeleteR.TabIndex = 307
        Me.btnDeleteR.Text = "&Delete"
        Me.btnDeleteR.UseVisualStyleBackColor = True
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.objchkIsExternalEntity)
        Me.gbInfo.Controls.Add(Me.lnkAllocation)
        Me.gbInfo.Controls.Add(Me.btnAdd)
        Me.gbInfo.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbInfo.Controls.Add(Me.objAlloacationReset)
        Me.gbInfo.Controls.Add(Me.objbtnSearchUser)
        Me.gbInfo.Controls.Add(Me.lblApproverName)
        Me.gbInfo.Controls.Add(Me.cboUser)
        Me.gbInfo.Controls.Add(Me.LblUser)
        Me.gbInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbInfo.Controls.Add(Me.txtName)
        Me.gbInfo.Controls.Add(Me.EZeeLine1)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(333, 476)
        Me.gbInfo.TabIndex = 5
        Me.gbInfo.Temp = 0
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkIsExternalEntity
        '
        Me.objchkIsExternalEntity.BackColor = System.Drawing.Color.Transparent
        Me.objchkIsExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkIsExternalEntity.Location = New System.Drawing.Point(8, 4)
        Me.objchkIsExternalEntity.Name = "objchkIsExternalEntity"
        Me.objchkIsExternalEntity.Size = New System.Drawing.Size(288, 17)
        Me.objchkIsExternalEntity.TabIndex = 306
        Me.objchkIsExternalEntity.UseVisualStyleBackColor = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(218, 86)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 15)
        Me.lnkAllocation.TabIndex = 236
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(225, 432)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(98, 29)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(7, 110)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(319, 316)
        Me.tblpAssessorEmployee.TabIndex = 303
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(313, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(313, 284)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 21
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(313, 284)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(302, 83)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 238
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(302, 60)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 302
        '
        'lblApproverName
        '
        Me.lblApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverName.Location = New System.Drawing.Point(11, 35)
        Me.lblApproverName.Name = "lblApproverName"
        Me.lblApproverName.Size = New System.Drawing.Size(69, 16)
        Me.lblApproverName.TabIndex = 5
        Me.lblApproverName.Text = "#Caption"
        Me.lblApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 200
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(86, 60)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(210, 21)
        Me.cboUser.TabIndex = 300
        '
        'LblUser
        '
        Me.LblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUser.Location = New System.Drawing.Point(11, 63)
        Me.LblUser.Name = "LblUser"
        Me.LblUser.Size = New System.Drawing.Size(69, 16)
        Me.LblUser.TabIndex = 301
        Me.LblUser.Text = "User"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(302, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 95
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(86, 33)
        Me.txtName.Name = "txtName"
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(210, 21)
        Me.txtName.TabIndex = 94
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(14, 87)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(282, 13)
        Me.EZeeLine1.TabIndex = 5
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 481)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 50)
        Me.objFooter.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(732, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(835, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAssessorReviewerAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 531)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessorReviewerAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMain.ResumeLayout(False)
        Me.gbAssignedEmployee.ResumeLayout(False)
        Me.tabcAssignedEmployee.ResumeLayout(False)
        Me.tabpAssessor.ResumeLayout(False)
        Me.pnlAData.ResumeLayout(False)
        Me.tblpAssessor.ResumeLayout(False)
        Me.tblpAssessor.PerformLayout()
        Me.objpnlAssessor.ResumeLayout(False)
        Me.objpnlAssessor.PerformLayout()
        CType(Me.dgvAssessor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpReviewer.ResumeLayout(False)
        Me.pnlRData.ResumeLayout(False)
        Me.tblpReviewer.ResumeLayout(False)
        Me.tblpReviewer.PerformLayout()
        Me.objpnlReviewer.ResumeLayout(False)
        Me.objpnlReviewer.PerformLayout()
        CType(Me.dgvReviewer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAssignedEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApproverName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents LblUser As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents tabcAssignedEmployee As System.Windows.Forms.TabControl
    Friend WithEvents tabpAssessor As System.Windows.Forms.TabPage
    Friend WithEvents tabpReviewer As System.Windows.Forms.TabPage
    Friend WithEvents tblpAssessor As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtaSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlAssessor As System.Windows.Forms.Panel
    Friend WithEvents objchkAssessor As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAssessor As System.Windows.Forms.DataGridView
    Friend WithEvents btnDeleteA As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeleteR As eZee.Common.eZeeLightButton
    Friend WithEvents tblpReviewer As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtrSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlReviewer As System.Windows.Forms.Panel
    Friend WithEvents objchkReviewer As System.Windows.Forms.CheckBox
    Friend WithEvents dgvReviewer As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhaCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhaCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhaEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhaEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhATranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhrCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhrDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhrJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlAData As System.Windows.Forms.Panel
    Friend WithEvents pnlRData As System.Windows.Forms.Panel
    Friend WithEvents objchkIsExternalEntity As System.Windows.Forms.CheckBox
End Class
