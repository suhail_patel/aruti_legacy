﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMigration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMigration))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMigration = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.radOverWriteAssessment = New System.Windows.Forms.RadioButton
        Me.radVoidAssessment = New System.Windows.Forms.RadioButton
        Me.elOperationType = New eZee.Common.eZeeLine
        Me.tabcTo = New System.Windows.Forms.TabControl
        Me.tabpMigrated = New System.Windows.Forms.TabPage
        Me.tblpNewData = New System.Windows.Forms.TableLayoutPanel
        Me.txtNewEmp = New eZee.TextBox.AlphanumericTextBox
        Me.pnlNewData = New System.Windows.Forms.Panel
        Me.objchkNewEmp = New System.Windows.Forms.CheckBox
        Me.dgvNewEmp = New System.Windows.Forms.DataGridView
        Me.objdgcolhNewChek = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhNewCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNewEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhNewEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhNewMasterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhNewTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabpAssigned = New System.Windows.Forms.TabPage
        Me.tblAssigned = New System.Windows.Forms.TableLayoutPanel
        Me.txtAssignedEmp = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvAssigned = New System.Windows.Forms.DataGridView
        Me.dgcolhACode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblToValue = New System.Windows.Forms.Label
        Me.lblFromValue = New System.Windows.Forms.Label
        Me.objbtnSearchOldValue = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchNewValue = New eZee.Common.eZeeGradientButton
        Me.objbtnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnUnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboTo = New System.Windows.Forms.ComboBox
        Me.cboFrom = New System.Windows.Forms.ComboBox
        Me.tblpOldData = New System.Windows.Forms.TableLayoutPanel
        Me.txtOldEmp = New eZee.TextBox.AlphanumericTextBox
        Me.pnlOldData = New System.Windows.Forms.Panel
        Me.objchkOldEmp = New System.Windows.Forms.CheckBox
        Me.dgvOldEmp = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOldMasterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOldTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.radReviewer = New System.Windows.Forms.RadioButton
        Me.radAssessor = New System.Windows.Forms.RadioButton
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbMigration.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.tabcTo.SuspendLayout()
        Me.tabpMigrated.SuspendLayout()
        Me.tblpNewData.SuspendLayout()
        Me.pnlNewData.SuspendLayout()
        CType(Me.dgvNewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpAssigned.SuspendLayout()
        Me.tblAssigned.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvAssigned, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpOldData.SuspendLayout()
        Me.pnlOldData.SuspendLayout()
        CType(Me.dgvOldEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbMigration)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(820, 556)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 501)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(820, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(608, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(711, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbMigration
        '
        Me.gbMigration.BorderColor = System.Drawing.Color.Black
        Me.gbMigration.Checked = False
        Me.gbMigration.CollapseAllExceptThis = False
        Me.gbMigration.CollapsedHoverImage = Nothing
        Me.gbMigration.CollapsedNormalImage = Nothing
        Me.gbMigration.CollapsedPressedImage = Nothing
        Me.gbMigration.CollapseOnLoad = False
        Me.gbMigration.Controls.Add(Me.Panel2)
        Me.gbMigration.Controls.Add(Me.elOperationType)
        Me.gbMigration.Controls.Add(Me.tabcTo)
        Me.gbMigration.Controls.Add(Me.lblToValue)
        Me.gbMigration.Controls.Add(Me.lblFromValue)
        Me.gbMigration.Controls.Add(Me.objbtnSearchOldValue)
        Me.gbMigration.Controls.Add(Me.objbtnSearchNewValue)
        Me.gbMigration.Controls.Add(Me.objbtnAssign)
        Me.gbMigration.Controls.Add(Me.objbtnUnAssign)
        Me.gbMigration.Controls.Add(Me.cboTo)
        Me.gbMigration.Controls.Add(Me.cboFrom)
        Me.gbMigration.Controls.Add(Me.tblpOldData)
        Me.gbMigration.Controls.Add(Me.radReviewer)
        Me.gbMigration.Controls.Add(Me.radAssessor)
        Me.gbMigration.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbMigration.ExpandedHoverImage = Nothing
        Me.gbMigration.ExpandedNormalImage = Nothing
        Me.gbMigration.ExpandedPressedImage = Nothing
        Me.gbMigration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMigration.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMigration.HeaderHeight = 26
        Me.gbMigration.HeaderMessage = ""
        Me.gbMigration.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbMigration.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMigration.HeightOnCollapse = 0
        Me.gbMigration.LeftTextSpace = 0
        Me.gbMigration.Location = New System.Drawing.Point(0, 0)
        Me.gbMigration.Name = "gbMigration"
        Me.gbMigration.OpenHeight = 300
        Me.gbMigration.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMigration.ShowBorder = True
        Me.gbMigration.ShowCheckBox = False
        Me.gbMigration.ShowCollapseButton = False
        Me.gbMigration.ShowDefaultBorderColor = True
        Me.gbMigration.ShowDownButton = False
        Me.gbMigration.ShowHeader = True
        Me.gbMigration.Size = New System.Drawing.Size(820, 556)
        Me.gbMigration.TabIndex = 1
        Me.gbMigration.Temp = 0
        Me.gbMigration.Text = "Migration"
        Me.gbMigration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.radOverWriteAssessment)
        Me.Panel2.Controls.Add(Me.radVoidAssessment)
        Me.Panel2.Location = New System.Drawing.Point(2, 451)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(816, 49)
        Me.Panel2.TabIndex = 176
        '
        'radOverWriteAssessment
        '
        Me.radOverWriteAssessment.ForeColor = System.Drawing.Color.Red
        Me.radOverWriteAssessment.Location = New System.Drawing.Point(35, 5)
        Me.radOverWriteAssessment.Name = "radOverWriteAssessment"
        Me.radOverWriteAssessment.Size = New System.Drawing.Size(771, 17)
        Me.radOverWriteAssessment.TabIndex = 171
        Me.radOverWriteAssessment.TabStop = True
        Me.radOverWriteAssessment.Text = "Overwrite assessment done for migrating assessor/reviewer for the selected employ" & _
            "ee for all open assessment period."
        Me.radOverWriteAssessment.UseVisualStyleBackColor = True
        '
        'radVoidAssessment
        '
        Me.radVoidAssessment.ForeColor = System.Drawing.Color.Red
        Me.radVoidAssessment.Location = New System.Drawing.Point(35, 27)
        Me.radVoidAssessment.Name = "radVoidAssessment"
        Me.radVoidAssessment.Size = New System.Drawing.Size(771, 17)
        Me.radVoidAssessment.TabIndex = 172
        Me.radVoidAssessment.TabStop = True
        Me.radVoidAssessment.Text = "Void assessment done for migrating assessor/reviewer for the selected employee fo" & _
            "r all open assessment period."
        Me.radVoidAssessment.UseVisualStyleBackColor = True
        '
        'elOperationType
        '
        Me.elOperationType.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elOperationType.Location = New System.Drawing.Point(12, 435)
        Me.elOperationType.Name = "elOperationType"
        Me.elOperationType.Size = New System.Drawing.Size(796, 13)
        Me.elOperationType.TabIndex = 174
        Me.elOperationType.Text = "Operation Type"
        Me.elOperationType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcTo
        '
        Me.tabcTo.Controls.Add(Me.tabpMigrated)
        Me.tabcTo.Controls.Add(Me.tabpAssigned)
        Me.tabcTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcTo.Location = New System.Drawing.Point(437, 61)
        Me.tabcTo.Name = "tabcTo"
        Me.tabcTo.SelectedIndex = 0
        Me.tabcTo.Size = New System.Drawing.Size(371, 368)
        Me.tabcTo.TabIndex = 169
        '
        'tabpMigrated
        '
        Me.tabpMigrated.Controls.Add(Me.tblpNewData)
        Me.tabpMigrated.Location = New System.Drawing.Point(4, 22)
        Me.tabpMigrated.Margin = New System.Windows.Forms.Padding(0)
        Me.tabpMigrated.Name = "tabpMigrated"
        Me.tabpMigrated.Size = New System.Drawing.Size(363, 342)
        Me.tabpMigrated.TabIndex = 0
        Me.tabpMigrated.Text = "Migrated Employe(s)"
        Me.tabpMigrated.UseVisualStyleBackColor = True
        '
        'tblpNewData
        '
        Me.tblpNewData.ColumnCount = 1
        Me.tblpNewData.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpNewData.Controls.Add(Me.txtNewEmp, 0, 0)
        Me.tblpNewData.Controls.Add(Me.pnlNewData, 0, 1)
        Me.tblpNewData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblpNewData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpNewData.Location = New System.Drawing.Point(0, 0)
        Me.tblpNewData.Name = "tblpNewData"
        Me.tblpNewData.RowCount = 2
        Me.tblpNewData.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpNewData.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpNewData.Size = New System.Drawing.Size(363, 342)
        Me.tblpNewData.TabIndex = 109
        '
        'txtNewEmp
        '
        Me.txtNewEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNewEmp.Flags = 0
        Me.txtNewEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtNewEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtNewEmp.Name = "txtNewEmp"
        Me.txtNewEmp.Size = New System.Drawing.Size(357, 21)
        Me.txtNewEmp.TabIndex = 106
        '
        'pnlNewData
        '
        Me.pnlNewData.Controls.Add(Me.objchkNewEmp)
        Me.pnlNewData.Controls.Add(Me.dgvNewEmp)
        Me.pnlNewData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlNewData.Location = New System.Drawing.Point(3, 29)
        Me.pnlNewData.Name = "pnlNewData"
        Me.pnlNewData.Size = New System.Drawing.Size(357, 310)
        Me.pnlNewData.TabIndex = 107
        '
        'objchkNewEmp
        '
        Me.objchkNewEmp.AutoSize = True
        Me.objchkNewEmp.Location = New System.Drawing.Point(7, 5)
        Me.objchkNewEmp.Name = "objchkNewEmp"
        Me.objchkNewEmp.Size = New System.Drawing.Size(15, 14)
        Me.objchkNewEmp.TabIndex = 104
        Me.objchkNewEmp.UseVisualStyleBackColor = True
        '
        'dgvNewEmp
        '
        Me.dgvNewEmp.AllowUserToAddRows = False
        Me.dgvNewEmp.AllowUserToDeleteRows = False
        Me.dgvNewEmp.AllowUserToResizeColumns = False
        Me.dgvNewEmp.AllowUserToResizeRows = False
        Me.dgvNewEmp.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvNewEmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvNewEmp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvNewEmp.ColumnHeadersHeight = 21
        Me.dgvNewEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNewEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhNewChek, Me.dgcolhNewCode, Me.dgcolhNewEmp, Me.objdgcolhNewEmpId, Me.objdgcolhNewMasterunkid, Me.objdgcolhNewTranId})
        Me.dgvNewEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvNewEmp.Location = New System.Drawing.Point(0, 0)
        Me.dgvNewEmp.MultiSelect = False
        Me.dgvNewEmp.Name = "dgvNewEmp"
        Me.dgvNewEmp.RowHeadersVisible = False
        Me.dgvNewEmp.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvNewEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNewEmp.Size = New System.Drawing.Size(357, 310)
        Me.dgvNewEmp.TabIndex = 105
        '
        'objdgcolhNewChek
        '
        Me.objdgcolhNewChek.HeaderText = ""
        Me.objdgcolhNewChek.Name = "objdgcolhNewChek"
        Me.objdgcolhNewChek.Width = 25
        '
        'dgcolhNewCode
        '
        Me.dgcolhNewCode.HeaderText = "Code"
        Me.dgcolhNewCode.Name = "dgcolhNewCode"
        Me.dgcolhNewCode.ReadOnly = True
        Me.dgcolhNewCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhNewCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhNewCode.Width = 70
        '
        'dgcolhNewEmp
        '
        Me.dgcolhNewEmp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhNewEmp.HeaderText = "Employee"
        Me.dgcolhNewEmp.Name = "dgcolhNewEmp"
        Me.dgcolhNewEmp.ReadOnly = True
        Me.dgcolhNewEmp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhNewEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhNewEmpId
        '
        Me.objdgcolhNewEmpId.HeaderText = "objdgcolhNewEmpId"
        Me.objdgcolhNewEmpId.Name = "objdgcolhNewEmpId"
        Me.objdgcolhNewEmpId.Visible = False
        '
        'objdgcolhNewMasterunkid
        '
        Me.objdgcolhNewMasterunkid.HeaderText = "objdgcolhNewMasterunkid"
        Me.objdgcolhNewMasterunkid.Name = "objdgcolhNewMasterunkid"
        Me.objdgcolhNewMasterunkid.Visible = False
        '
        'objdgcolhNewTranId
        '
        Me.objdgcolhNewTranId.HeaderText = "objdgcolhNewTranId"
        Me.objdgcolhNewTranId.Name = "objdgcolhNewTranId"
        Me.objdgcolhNewTranId.Visible = False
        '
        'tabpAssigned
        '
        Me.tabpAssigned.Controls.Add(Me.tblAssigned)
        Me.tabpAssigned.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssigned.Margin = New System.Windows.Forms.Padding(0)
        Me.tabpAssigned.Name = "tabpAssigned"
        Me.tabpAssigned.Size = New System.Drawing.Size(363, 342)
        Me.tabpAssigned.TabIndex = 1
        Me.tabpAssigned.Text = "Assigned Employee(s)"
        Me.tabpAssigned.UseVisualStyleBackColor = True
        '
        'tblAssigned
        '
        Me.tblAssigned.ColumnCount = 1
        Me.tblAssigned.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblAssigned.Controls.Add(Me.txtAssignedEmp, 0, 0)
        Me.tblAssigned.Controls.Add(Me.Panel1, 0, 1)
        Me.tblAssigned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblAssigned.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblAssigned.Location = New System.Drawing.Point(0, 0)
        Me.tblAssigned.Name = "tblAssigned"
        Me.tblAssigned.RowCount = 2
        Me.tblAssigned.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblAssigned.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblAssigned.Size = New System.Drawing.Size(363, 342)
        Me.tblAssigned.TabIndex = 110
        '
        'txtAssignedEmp
        '
        Me.txtAssignedEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssignedEmp.Flags = 0
        Me.txtAssignedEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtAssignedEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtAssignedEmp.Name = "txtAssignedEmp"
        Me.txtAssignedEmp.Size = New System.Drawing.Size(357, 21)
        Me.txtAssignedEmp.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvAssigned)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(357, 310)
        Me.Panel1.TabIndex = 107
        '
        'dgvAssigned
        '
        Me.dgvAssigned.AllowUserToAddRows = False
        Me.dgvAssigned.AllowUserToDeleteRows = False
        Me.dgvAssigned.AllowUserToResizeColumns = False
        Me.dgvAssigned.AllowUserToResizeRows = False
        Me.dgvAssigned.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAssigned.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAssigned.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAssigned.ColumnHeadersHeight = 21
        Me.dgvAssigned.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAssigned.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhACode, Me.dgcolhAName, Me.objdgcolhAEmpId})
        Me.dgvAssigned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAssigned.Location = New System.Drawing.Point(0, 0)
        Me.dgvAssigned.MultiSelect = False
        Me.dgvAssigned.Name = "dgvAssigned"
        Me.dgvAssigned.RowHeadersVisible = False
        Me.dgvAssigned.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAssigned.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssigned.Size = New System.Drawing.Size(357, 310)
        Me.dgvAssigned.TabIndex = 105
        '
        'dgcolhACode
        '
        Me.dgcolhACode.HeaderText = "Code"
        Me.dgcolhACode.Name = "dgcolhACode"
        Me.dgcolhACode.ReadOnly = True
        Me.dgcolhACode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhACode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhACode.Width = 70
        '
        'dgcolhAName
        '
        Me.dgcolhAName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAName.HeaderText = "Employee"
        Me.dgcolhAName.Name = "dgcolhAName"
        Me.dgcolhAName.ReadOnly = True
        Me.dgcolhAName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhAEmpId
        '
        Me.objdgcolhAEmpId.HeaderText = "objdgcolhAEmpId"
        Me.objdgcolhAEmpId.Name = "objdgcolhAEmpId"
        Me.objdgcolhAEmpId.Visible = False
        '
        'lblToValue
        '
        Me.lblToValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToValue.Location = New System.Drawing.Point(437, 37)
        Me.lblToValue.Name = "lblToValue"
        Me.lblToValue.Size = New System.Drawing.Size(96, 16)
        Me.lblToValue.TabIndex = 167
        Me.lblToValue.Text = "To Value"
        Me.lblToValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFromValue
        '
        Me.lblFromValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromValue.Location = New System.Drawing.Point(12, 36)
        Me.lblFromValue.Name = "lblFromValue"
        Me.lblFromValue.Size = New System.Drawing.Size(96, 16)
        Me.lblFromValue.TabIndex = 167
        Me.lblFromValue.Text = "From Value"
        Me.lblFromValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOldValue
        '
        Me.objbtnSearchOldValue.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOldValue.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldValue.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldValue.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOldValue.BorderSelected = False
        Me.objbtnSearchOldValue.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOldValue.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOldValue.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOldValue.Location = New System.Drawing.Point(362, 34)
        Me.objbtnSearchOldValue.Name = "objbtnSearchOldValue"
        Me.objbtnSearchOldValue.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOldValue.TabIndex = 166
        '
        'objbtnSearchNewValue
        '
        Me.objbtnSearchNewValue.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchNewValue.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewValue.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewValue.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchNewValue.BorderSelected = False
        Me.objbtnSearchNewValue.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchNewValue.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchNewValue.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchNewValue.Location = New System.Drawing.Point(787, 34)
        Me.objbtnSearchNewValue.Name = "objbtnSearchNewValue"
        Me.objbtnSearchNewValue.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchNewValue.TabIndex = 166
        '
        'objbtnAssign
        '
        Me.objbtnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAssign.BackgroundImage = CType(resources.GetObject("objbtnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.objbtnAssign.Location = New System.Drawing.Point(389, 216)
        Me.objbtnAssign.Name = "objbtnAssign"
        Me.objbtnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssign.TabIndex = 111
        Me.objbtnAssign.UseVisualStyleBackColor = True
        '
        'objbtnUnAssign
        '
        Me.objbtnUnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnUnAssign.BackgroundImage = CType(resources.GetObject("objbtnUnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_16
        Me.objbtnUnAssign.Location = New System.Drawing.Point(389, 262)
        Me.objbtnUnAssign.Name = "objbtnUnAssign"
        Me.objbtnUnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnAssign.TabIndex = 112
        Me.objbtnUnAssign.UseVisualStyleBackColor = True
        '
        'cboTo
        '
        Me.cboTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTo.DropDownWidth = 350
        Me.cboTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTo.FormattingEnabled = True
        Me.cboTo.Location = New System.Drawing.Point(539, 34)
        Me.cboTo.Name = "cboTo"
        Me.cboTo.Size = New System.Drawing.Size(242, 21)
        Me.cboTo.TabIndex = 110
        '
        'cboFrom
        '
        Me.cboFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFrom.DropDownWidth = 350
        Me.cboFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFrom.FormattingEnabled = True
        Me.cboFrom.Location = New System.Drawing.Point(114, 34)
        Me.cboFrom.Name = "cboFrom"
        Me.cboFrom.Size = New System.Drawing.Size(242, 21)
        Me.cboFrom.TabIndex = 110
        '
        'tblpOldData
        '
        Me.tblpOldData.ColumnCount = 1
        Me.tblpOldData.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpOldData.Controls.Add(Me.txtOldEmp, 0, 0)
        Me.tblpOldData.Controls.Add(Me.pnlOldData, 0, 1)
        Me.tblpOldData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpOldData.Location = New System.Drawing.Point(12, 61)
        Me.tblpOldData.Name = "tblpOldData"
        Me.tblpOldData.RowCount = 2
        Me.tblpOldData.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpOldData.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpOldData.Size = New System.Drawing.Size(371, 367)
        Me.tblpOldData.TabIndex = 108
        '
        'txtOldEmp
        '
        Me.txtOldEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtOldEmp.Flags = 0
        Me.txtOldEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtOldEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtOldEmp.Name = "txtOldEmp"
        Me.txtOldEmp.Size = New System.Drawing.Size(365, 21)
        Me.txtOldEmp.TabIndex = 106
        '
        'pnlOldData
        '
        Me.pnlOldData.Controls.Add(Me.objchkOldEmp)
        Me.pnlOldData.Controls.Add(Me.dgvOldEmp)
        Me.pnlOldData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlOldData.Location = New System.Drawing.Point(3, 29)
        Me.pnlOldData.Name = "pnlOldData"
        Me.pnlOldData.Size = New System.Drawing.Size(365, 335)
        Me.pnlOldData.TabIndex = 107
        '
        'objchkOldEmp
        '
        Me.objchkOldEmp.AutoSize = True
        Me.objchkOldEmp.Location = New System.Drawing.Point(7, 5)
        Me.objchkOldEmp.Name = "objchkOldEmp"
        Me.objchkOldEmp.Size = New System.Drawing.Size(15, 14)
        Me.objchkOldEmp.TabIndex = 104
        Me.objchkOldEmp.UseVisualStyleBackColor = True
        '
        'dgvOldEmp
        '
        Me.dgvOldEmp.AllowUserToAddRows = False
        Me.dgvOldEmp.AllowUserToDeleteRows = False
        Me.dgvOldEmp.AllowUserToResizeColumns = False
        Me.dgvOldEmp.AllowUserToResizeRows = False
        Me.dgvOldEmp.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvOldEmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvOldEmp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvOldEmp.ColumnHeadersHeight = 21
        Me.dgvOldEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvOldEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId, Me.objdgcolhOldMasterunkid, Me.objdgcolhOldTranId})
        Me.dgvOldEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOldEmp.Location = New System.Drawing.Point(0, 0)
        Me.dgvOldEmp.MultiSelect = False
        Me.dgvOldEmp.Name = "dgvOldEmp"
        Me.dgvOldEmp.RowHeadersVisible = False
        Me.dgvOldEmp.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOldEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOldEmp.Size = New System.Drawing.Size(365, 335)
        Me.dgvOldEmp.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhOldMasterunkid
        '
        Me.objdgcolhOldMasterunkid.HeaderText = "objdgcolhOldMasterunkid"
        Me.objdgcolhOldMasterunkid.Name = "objdgcolhOldMasterunkid"
        Me.objdgcolhOldMasterunkid.Visible = False
        '
        'objdgcolhOldTranId
        '
        Me.objdgcolhOldTranId.HeaderText = "objdgcolhOldTranId"
        Me.objdgcolhOldTranId.Name = "objdgcolhOldTranId"
        Me.objdgcolhOldTranId.Visible = False
        '
        'radReviewer
        '
        Me.radReviewer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radReviewer.BackColor = System.Drawing.Color.Transparent
        Me.radReviewer.Location = New System.Drawing.Point(711, 5)
        Me.radReviewer.Name = "radReviewer"
        Me.radReviewer.Size = New System.Drawing.Size(103, 17)
        Me.radReviewer.TabIndex = 1
        Me.radReviewer.TabStop = True
        Me.radReviewer.Text = "Reviewer"
        Me.radReviewer.UseVisualStyleBackColor = False
        '
        'radAssessor
        '
        Me.radAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radAssessor.Location = New System.Drawing.Point(602, 5)
        Me.radAssessor.Name = "radAssessor"
        Me.radAssessor.Size = New System.Drawing.Size(103, 17)
        Me.radAssessor.TabIndex = 1
        Me.radAssessor.TabStop = True
        Me.radAssessor.Text = "Assessor"
        Me.radAssessor.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhNewEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhOldMasterunkid"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'frmMigration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 556)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMigration"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessor/Reviewer Migration"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbMigration.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.tabcTo.ResumeLayout(False)
        Me.tabpMigrated.ResumeLayout(False)
        Me.tblpNewData.ResumeLayout(False)
        Me.tblpNewData.PerformLayout()
        Me.pnlNewData.ResumeLayout(False)
        Me.pnlNewData.PerformLayout()
        CType(Me.dgvNewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpAssigned.ResumeLayout(False)
        Me.tblAssigned.ResumeLayout(False)
        Me.tblAssigned.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvAssigned, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpOldData.ResumeLayout(False)
        Me.tblpOldData.PerformLayout()
        Me.pnlOldData.ResumeLayout(False)
        Me.pnlOldData.PerformLayout()
        CType(Me.dgvOldEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbMigration As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents radReviewer As System.Windows.Forms.RadioButton
    Friend WithEvents tblpOldData As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtOldEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlOldData As System.Windows.Forms.Panel
    Friend WithEvents objchkOldEmp As System.Windows.Forms.CheckBox
    Friend WithEvents dgvOldEmp As System.Windows.Forms.DataGridView
    Friend WithEvents cboTo As System.Windows.Forms.ComboBox
    Friend WithEvents cboFrom As System.Windows.Forms.ComboBox
    Friend WithEvents tblpNewData As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtNewEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlNewData As System.Windows.Forms.Panel
    Friend WithEvents objchkNewEmp As System.Windows.Forms.CheckBox
    Friend WithEvents dgvNewEmp As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchOldValue As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchNewValue As eZee.Common.eZeeGradientButton
    Friend WithEvents lblToValue As System.Windows.Forms.Label
    Friend WithEvents lblFromValue As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tabcTo As System.Windows.Forms.TabControl
    Friend WithEvents tabpMigrated As System.Windows.Forms.TabPage
    Friend WithEvents tabpAssigned As System.Windows.Forms.TabPage
    Friend WithEvents tblAssigned As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAssignedEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvAssigned As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhACode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhNewChek As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhNewCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNewEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhNewEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhNewMasterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhNewTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOldMasterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOldTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents radVoidAssessment As System.Windows.Forms.RadioButton
    Friend WithEvents radOverWriteAssessment As System.Windows.Forms.RadioButton
    Friend WithEvents elOperationType As eZee.Common.eZeeLine
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class
