﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAddEditCustomItems

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmAddEditCustomItems"
    Private mblnCancel As Boolean = True
    Private objCItems As clsassess_custom_items
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCustomItemUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCustomItemUnkid = intUnkId
            menAction = eAction

            objlblNotes.Text = Language.getMessage("frmCustomItemsList", 4, "Note: You are allowed to define only 10 Custom Items per Custom Header.")

            Me.ShowDialog()

            intUnkId = mintCustomItemUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorComp
            cboCHeader.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            cboMode.BackColor = GUI.ColorComp
            cboItemType.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objCItems._Custom_Field = txtName.Text
            objCItems._Customheaderunkid = CInt(cboCHeader.SelectedValue)
            objCItems._Isactive = True
            objCItems._Periodunkid = CInt(cboPeriod.SelectedValue)
            objCItems._ItemTypeId = CInt(cboItemType.SelectedValue)
            objCItems._SelectionModeid = CInt(cboMode.SelectedValue)
            If radEmployee.Checked = True Then
                objCItems._ViewModeId = 0   'INTENSIONALLY PASSED - TO BE ALLOWED FOR ALL
            ElseIf radAssessor.Checked = True Then
                objCItems._ViewModeId = enAssessmentMode.APPRAISER_ASSESSMENT
            ElseIf radReviewer.Checked = True Then
                objCItems._ViewModeId = enAssessmentMode.REVIEWER_ASSESSMENT
            End If
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objCItems._IsDefualtEntry = False
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            objCItems._IsCompletedTraining = chkCompletedTraining.Checked
            'S.SANDEEP |08-JAN-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtName.Text = objCItems._Custom_Field
            cboPeriod.SelectedValue = objCItems._Periodunkid
            cboCHeader.SelectedValue = objCItems._Customheaderunkid
            cboItemType.SelectedValue = objCItems._ItemTypeId
            cboMode.SelectedValue = objCItems._SelectionModeid
            Select Case objCItems._ViewModeId
                Case 0 'INTENSIONALLY PASSED - TO BE ALLOWED FOR ALL
                    radEmployee.Checked = True
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    radAssessor.Checked = True
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    radReviewer.Checked = True
            End Select
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            pnlIsDefualtEntry.Enabled = Not objCItems._IsDefualtEntry
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            chkCompletedTraining.Checked = objCItems._IsCompletedTraining
            'S.SANDEEP |08-JAN-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 1)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            dsCombo = objCItems.GetList_CustomTypes("List", False)
            With cboItemType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
            End With

            dsCombo = objCItems.GetList_SelectionMode("List", False)
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboCHeader.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Custom Header is mandatory information. Please select Custom Header to continue."), enMsgBoxStyle.Information)
                cboCHeader.Focus()
                Return False
            End If

            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Custom Item is mandatory information. Please provide Custom Item to continue."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            If radEmployee.Checked = False AndAlso _
               radAssessor.Checked = False AndAlso _
               radReviewer.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Please select atleast one of the visible type in order to view item in performance assessment."), enMsgBoxStyle.Information)
                Return False
            End If

            Dim xMessage As String = String.Empty
            'S.SANDEEP [04 OCT 2016] -- START
            'xMessage = objCItems.IsValidItemCount(CInt(cboCHeader.SelectedValue))
            xMessage = objCItems.IsValidItemCount(CInt(cboCHeader.SelectedValue), CInt(cboPeriod.SelectedValue))
            'S.SANDEEP [04 OCT 2016] -- END
            If xMessage <> "" Then
                eZeeMsgBox.Show(xMessage, enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmAddEditCustomItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCItems = New clsassess_custom_items
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objCItems._Customitemunkid = mintCustomItemUnkid
                cboPeriod.Enabled = False : objbtnSearchPeriod.Enabled = False
                cboCHeader.Enabled = False : objbtnSearchHeader.Enabled = False
            End If
            Call FillCombo()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCustomItems_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAddEditCustomItems_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCustomItems_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAddEditCustomItems_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCustomItems_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAddEditCustomItems_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCItems = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_custom_items.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_custom_items"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objCItems.Update(User._Object._Userunkid)
            Else
                blnFlag = objCItems.Insert(User._Object._Userunkid)
            End If

            If blnFlag = False And objCItems._Message <> "" Then
                eZeeMsgBox.Show(objCItems._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objCItems = Nothing
                    objCItems = New clsassess_custom_items
                    cboPeriod.Tag = CInt(cboPeriod.SelectedValue)
                    cboCHeader.Tag = CInt(cboCHeader.SelectedValue)

                    Call GetValue()
                    cboPeriod.SelectedValue = CInt(cboPeriod.Tag)
                    cboPeriod.Select()

                    cboCHeader.SelectedValue = CInt(cboCHeader.Tag)
                    cboCHeader.Select()
                Else
                    mintCustomItemUnkid = objCItems._Customitemunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchHeader_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchHeader.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboCHeader.ValueMember
                .DisplayMember = cboCHeader.DisplayMember
                .DataSource = CType(cboCHeader.DataSource, DataTable)
                If .DisplayDialog Then
                    cboCHeader.SelectedValue = .SelectedValue
                    cboCHeader.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchHeader_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Control's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objCHeader As New clsassess_custom_header
            dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), True, "List")
            With cboCHeader
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboItemType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboItemType.SelectedIndexChanged
        Try
            Select Case CInt(cboItemType.SelectedValue)
                Case clsassess_custom_items.enCustomType.SELECTION
                    cboMode.Enabled = True : cboMode.SelectedIndex = 0
                Case Else
                    cboMode.Enabled = False : cboMode.SelectedIndex = -1
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboItemType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |08-JAN-2019| -- START
    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            Select Case CInt(cboMode.SelectedValue)
                Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, _
                     clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, _
                     clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE

                    chkCompletedTraining.Enabled = True
                Case Else
                    chkCompletedTraining.Enabled = False
                    chkCompletedTraining.Checked = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |08-JAN-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbCustomItems.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCustomItems.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbCustomItems.Text = Language._Object.getCaption(Me.gbCustomItems.Name, Me.gbCustomItems.Text)
            Me.lblCustomItem.Text = Language._Object.getCaption(Me.lblCustomItem.Name, Me.lblCustomItem.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblCustomHeader.Text = Language._Object.getCaption(Me.lblCustomHeader.Name, Me.lblCustomHeader.Text)
            Me.lblItemType.Text = Language._Object.getCaption(Me.lblItemType.Name, Me.lblItemType.Text)
            Me.elItemAccess.Text = Language._Object.getCaption(Me.elItemAccess.Name, Me.elItemAccess.Text)
            Me.radReviewer.Text = Language._Object.getCaption(Me.radReviewer.Name, Me.radReviewer.Text)
            Me.radAssessor.Text = Language._Object.getCaption(Me.radAssessor.Name, Me.radAssessor.Text)
            Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("frmCustomItemsList", 4, "Note: You are allowed to define only 10 Custom Items per Custom Header.")
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 2, "Custom Header is mandatory information. Please select Custom Header to continue.")
            Language.setMessage(mstrModuleName, 3, "Custom Item is mandatory information. Please provide Custom Item to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Please select atleast one of the visible type in order to view item in performance assessment.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class