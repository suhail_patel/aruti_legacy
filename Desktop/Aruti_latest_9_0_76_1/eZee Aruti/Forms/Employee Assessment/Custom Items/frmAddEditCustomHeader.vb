﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAddEditCustomHeader

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmAddEditCustomHeader"
    Private mblnCancel As Boolean = True
    Private objCHeader As clsassess_custom_header
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCustomHeaderUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCustomHeaderUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCustomHeaderUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objCHeader._Is_Allow_Ess = chkShowinESS.Checked
            objCHeader._Is_Allow_Mss = chkShowinMSS.Checked
            objCHeader._Is_Allow_Multiple = chkMultiple.Checked
            objCHeader._Isactive = True
            objCHeader._Name = txtName.Text

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objCHeader._IsMatch_With_Competency = chkMatchStructureToCompetencies.Checked
            objCHeader._IsAllowAddPastPeriod = chkCompetencies_from_PastPeriod.Checked
            objCHeader._IsInclude_Planning = chkIncludeCustomItemInPlanning.Checked
            objCHeader._MappedPeriodUnkid = CInt(cboPastPeriod.SelectedValue)
            objCHeader._ScaleUnkId = CInt(cboScale.SelectedValue)
            objCHeader._Min_Score = nudMinScore.Value
            'Shani (26-Sep-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            chkShowinESS.Checked = objCHeader._Is_Allow_Ess
            chkShowinMSS.Checked = objCHeader._Is_Allow_Mss
            chkMultiple.Checked = objCHeader._Is_Allow_Multiple
            txtName.Text = objCHeader._Name

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            chkMatchStructureToCompetencies.Checked = objCHeader._IsMatch_With_Competency
            chkCompetencies_from_PastPeriod.Checked = objCHeader._IsAllowAddPastPeriod
            chkIncludeCustomItemInPlanning.Checked = objCHeader._IsInclude_Planning
            cboPastPeriod.SelectedValue = objCHeader._MappedPeriodUnkid
            cboScale.SelectedValue = objCHeader._ScaleUnkId
            nudMinScore.Value = objCHeader._Min_Score
            'Shani (26-Sep-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)


            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Close)
            Dim blnIsCompetence As Boolean = False
            If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                blnIsCompetence = True
            End If
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Close, False, blnIsCompetence, CBool(IIf(blnIsCompetence = True, True, False)))            
            'S.SANDEEP |02-MAR-2021| -- END


            With cboPastPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Shani (26-Sep-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Return False
            'End If

            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Custom Header is mandatory information. Please provide Custom Header to continue."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If chkMatchStructureToCompetencies.Checked Then
                If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                    If CInt(cboScale.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Mininum Score is mandatory information. Please select Mininum Score to continue."), enMsgBoxStyle.Information)
                        cboScale.Focus()
                        Return False
                    End If
                Else
                    If nudMinScore.Value <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Mininum Score is mandatory information. Please Enter Mininum Score to continue."), enMsgBoxStyle.Information)
                        nudMinScore.Focus()
                        Return False
                    End If
                End If


                If CInt(cboPastPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Past Period is mandatory information. Please select Past Period to continue."), enMsgBoxStyle.Information)
                    cboPastPeriod.Focus()
                    Return False
                End If
            End If
            'Shani (26-Sep-2016) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmAddEditCustomHeader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCHeader = New clsassess_custom_header
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objCHeader._Customheaderunkid = mintCustomHeaderUnkid
                cboPeriod.Enabled = False : objbtnSearchPeriod.Enabled = False
            End If
            Call FillCombo()
            Call GetValue()

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If ConfigParameter._Object._ScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                cboScale.Visible = True
                nudMinScore.Visible = False
                objlblPer.Visible = False
            Else
                cboScale.Visible = False
                nudMinScore.Visible = True
                objlblPer.Visible = True
            End If
            'Shani (26-Sep-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCustomHeader_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAddEditCustomHeader_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCustomHeader_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAddEditCustomHeader_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCustomHeader_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompetencies_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCHeader = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_custom_header.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_custom_header"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            Dim intCurrentPeriod As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime.Date, FinancialYear._Object._YearUnkid, 1, False, True, Nothing, False)
            'Shani (26-Sep-2016) -- End
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objCHeader.Update(User._Object._Userunkid, intCurrentPeriod)
            Else
                blnFlag = objCHeader.Insert(User._Object._Userunkid, intCurrentPeriod)
            End If

            If blnFlag = False And objCHeader._Message <> "" Then
                eZeeMsgBox.Show(objCHeader._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objCHeader = Nothing
                    objCHeader = New clsassess_custom_header
                    cboPeriod.Tag = CInt(cboPeriod.SelectedValue)
                    Call GetValue()
                    cboPeriod.SelectedValue = CInt(cboPeriod.Tag)
                    cboPeriod.Select()
                Else
                    mintCustomHeaderUnkid = objCHeader._Customheaderunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            Call objFrmLangPopup.displayDialog(txtName.Text, objCHeader._Name1, objCHeader._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .DataSource = CType(cboPeriod.DataSource, DataTable)
                If .DisplayDialog Then
                    cboPeriod.SelectedValue = .SelectedValue
                    cboPeriod.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
#Region " Control(S) Event'S "

    Private Sub chkMatchStructureToCompetencies_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMatchStructureToCompetencies.CheckedChanged
        Try
            RemoveHandler chkMatchStructureToCompetencies.CheckedChanged, AddressOf chkMatchStructureToCompetencies_CheckedChanged
            If menAction = enAction.EDIT_ONE AndAlso chkMatchStructureToCompetencies.Checked = False Then
                Dim dsItem As DataSet = (New clsassess_custom_items).GetList("List")
                If dsItem IsNot Nothing AndAlso dsItem.Tables(0).Rows.Count > 0 Then
                    Dim dRow() As DataRow = dsItem.Tables(0).Select("customheaderunkid = '" & mintCustomHeaderUnkid & "' AND isdefaultentry = 1")
                    If dRow.Length > 0 Then
                        Dim strItemsIds As String = String.Join(",", dRow.Cast(Of DataRow).Select(Function(x) x.Field(Of Integer)("customitemunkid").ToString).ToArray)
                        If (New clsassess_custom_items).isUsed_MultipleItemIds(strItemsIds) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "sorry,you are not allowed to untick Match Structure To Competencies.reason assessment has alrady been done for this custome header and items."), enMsgBoxStyle.Information)
                            chkMatchStructureToCompetencies.Checked = True
                            Exit Sub
                        End If
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You have untick Match Structure To Competencies, this will remove the defualt entry in custom items. do you wish to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            chkMatchStructureToCompetencies.Checked = True
                            Exit Sub
                        End If
                    End If
                End If
            End If
            chkCompetencies_from_PastPeriod.Checked = chkMatchStructureToCompetencies.Checked
            cboScale.Enabled = chkMatchStructureToCompetencies.Checked
            cboPastPeriod.Enabled = chkMatchStructureToCompetencies.Checked
            nudMinScore.Enabled = chkMatchStructureToCompetencies.Checked
            cboPastPeriod.SelectedValue = 0
            nudMinScore.Value = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMatchStructureToCompetencies_CheckedChanged", mstrModuleName)
        Finally
            AddHandler chkMatchStructureToCompetencies.CheckedChanged, AddressOf chkMatchStructureToCompetencies_CheckedChanged
        End Try
    End Sub

    Private Sub cboPastPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPastPeriod.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = (New clsAssessment_Scale).getComboList("List", CInt(cboPastPeriod.SelectedValue), True, True)
            With cboScale
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMinScore_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboScale.SelectedIndexChanged
        Try
            If CInt(cboScale.SelectedValue) > 0 Then
                nudMinScore.Value = (New clsAssessment_Scale).getScale(CInt(cboScale.SelectedValue))
            Else
                nudMinScore.Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMinScore_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Shani (26-Sep-2016) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbCustomHeader.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCustomHeader.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbCustomHeader.Text = Language._Object.getCaption(Me.gbCustomHeader.Name, Me.gbCustomHeader.Text)
			Me.lblCustomHeader.Text = Language._Object.getCaption(Me.lblCustomHeader.Name, Me.lblCustomHeader.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.chkMultiple.Text = Language._Object.getCaption(Me.chkMultiple.Name, Me.chkMultiple.Text)
			Me.chkShowinMSS.Text = Language._Object.getCaption(Me.chkShowinMSS.Name, Me.chkShowinMSS.Text)
			Me.chkShowinESS.Text = Language._Object.getCaption(Me.chkShowinESS.Name, Me.chkShowinESS.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.chkCompetencies_from_PastPeriod.Text = Language._Object.getCaption(Me.chkCompetencies_from_PastPeriod.Name, Me.chkCompetencies_from_PastPeriod.Text)
            Me.chkMatchStructureToCompetencies.Text = Language._Object.getCaption(Me.chkMatchStructureToCompetencies.Name, Me.chkMatchStructureToCompetencies.Text)
            Me.chkIncludeCustomItemInPlanning.Text = Language._Object.getCaption(Me.chkIncludeCustomItemInPlanning.Name, Me.chkIncludeCustomItemInPlanning.Text)
            Me.lblMinScore.Text = Language._Object.getCaption(Me.lblMinScore.Name, Me.lblMinScore.Text)
            Me.lblPastPeriod.Text = Language._Object.getCaption(Me.lblPastPeriod.Name, Me.lblPastPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Custom Header is mandatory information. Please provide Custom Header to continue.")
            Language.setMessage(mstrModuleName, 3, "Mininum Score is mandatory information. Please select Mininum Score to continue.")
            Language.setMessage(mstrModuleName, 4, "Past Period is mandatory information. Please select Past Period to continue.")
            Language.setMessage(mstrModuleName, 5, "Mininum Score is mandatory information. Please Enter Mininum Score to continue.")
            Language.setMessage(mstrModuleName, 6, "You have untick Match Structure To Competencies, this will remove the defualt entry in custom items. do you wish to continue?")
            Language.setMessage(mstrModuleName, 7, "sorry,you are not allowed to untick Match Structure To Competencies.reason assessment has alrady been done for this custome header and items.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class