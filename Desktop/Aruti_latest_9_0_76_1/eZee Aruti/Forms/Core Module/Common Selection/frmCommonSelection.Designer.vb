﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonSelection
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonSelection))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOpen = New eZee.Common.eZeeLightButton(Me.components)
        Me.picSideImage = New System.Windows.Forms.PictureBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lvCompany = New System.Windows.Forms.ListView
        Me.colhcode = New System.Windows.Forms.ColumnHeader
        Me.colhProperty = New System.Windows.Forms.ColumnHeader
        Me.lvDatabase = New eZee.Common.eZeeListView(Me.components)
        Me.colhDatabase = New System.Windows.Forms.ColumnHeader
        Me.objcolCompany = New System.Windows.Forms.ColumnHeader
        Me.btnUnlock = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.picSideImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.btnUnlock)
        Me.pnlMainInfo.Controls.Add(Me.objelLine1)
        Me.pnlMainInfo.Controls.Add(Me.btnCancel)
        Me.pnlMainInfo.Controls.Add(Me.btnOpen)
        Me.pnlMainInfo.Controls.Add(Me.picSideImage)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.lvCompany)
        Me.pnlMainInfo.Controls.Add(Me.lvDatabase)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(567, 283)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(172, 219)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(385, 12)
        Me.objelLine1.TabIndex = 16
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.No
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Image = Global.Aruti.Main.My.Resources.Resources.Close_24x24
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(449, 234)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(108, 40)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOpen
        '
        Me.btnOpen.BackColor = System.Drawing.Color.White
        Me.btnOpen.BackgroundImage = CType(resources.GetObject("btnOpen.BackgroundImage"), System.Drawing.Image)
        Me.btnOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpen.BorderColor = System.Drawing.Color.Empty
        Me.btnOpen.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpen.FlatAppearance.BorderSize = 0
        Me.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen.ForeColor = System.Drawing.Color.Black
        Me.btnOpen.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpen.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpen.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen.Image = Global.Aruti.Main.My.Resources.Resources.BackOffice_32
        Me.btnOpen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOpen.Location = New System.Drawing.Point(449, 176)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen.Size = New System.Drawing.Size(108, 40)
        Me.btnOpen.TabIndex = 15
        Me.btnOpen.Text = "&Open"
        Me.btnOpen.UseCompatibleTextRendering = True
        Me.btnOpen.UseVisualStyleBackColor = False
        '
        'picSideImage
        '
        Me.picSideImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picSideImage.Location = New System.Drawing.Point(0, 60)
        Me.picSideImage.Name = "picSideImage"
        Me.picSideImage.Size = New System.Drawing.Size(166, 223)
        Me.picSideImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picSideImage.TabIndex = 13
        Me.picSideImage.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(567, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Company Selection"
        '
        'lvCompany
        '
        Me.lvCompany.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhcode, Me.colhProperty})
        Me.lvCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvCompany.FullRowSelect = True
        Me.lvCompany.GridLines = True
        Me.lvCompany.HideSelection = False
        Me.lvCompany.Location = New System.Drawing.Point(172, 66)
        Me.lvCompany.MultiSelect = False
        Me.lvCompany.Name = "lvCompany"
        Me.lvCompany.Size = New System.Drawing.Size(271, 150)
        Me.lvCompany.TabIndex = 14
        Me.lvCompany.UseCompatibleStateImageBehavior = False
        Me.lvCompany.View = System.Windows.Forms.View.Details
        '
        'colhcode
        '
        Me.colhcode.Tag = "colhcode"
        Me.colhcode.Text = "code"
        '
        'colhProperty
        '
        Me.colhProperty.Tag = "colhProperty"
        Me.colhProperty.Text = "Company"
        Me.colhProperty.Width = 205
        '
        'lvDatabase
        '
        Me.lvDatabase.BackColorOnChecked = True
        Me.lvDatabase.ColumnHeaders = Nothing
        Me.lvDatabase.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDatabase, Me.objcolCompany})
        Me.lvDatabase.CompulsoryColumns = ""
        Me.lvDatabase.FullRowSelect = True
        Me.lvDatabase.GridLines = True
        Me.lvDatabase.GroupingColumn = Nothing
        Me.lvDatabase.HideSelection = False
        Me.lvDatabase.Location = New System.Drawing.Point(172, 66)
        Me.lvDatabase.MinColumnWidth = 50
        Me.lvDatabase.MultiSelect = False
        Me.lvDatabase.Name = "lvDatabase"
        Me.lvDatabase.OptionalColumns = ""
        Me.lvDatabase.ShowMoreItem = False
        Me.lvDatabase.ShowSaveItem = False
        Me.lvDatabase.ShowSelectAll = True
        Me.lvDatabase.ShowSizeAllColumnsToFit = True
        Me.lvDatabase.Size = New System.Drawing.Size(271, 150)
        Me.lvDatabase.Sortable = True
        Me.lvDatabase.TabIndex = 18
        Me.lvDatabase.UseCompatibleStateImageBehavior = False
        Me.lvDatabase.View = System.Windows.Forms.View.Details
        '
        'colhDatabase
        '
        Me.colhDatabase.Tag = "colhDatabase"
        Me.colhDatabase.Text = "Database"
        Me.colhDatabase.Width = 266
        '
        'objcolCompany
        '
        Me.objcolCompany.Tag = "objcolCompany"
        Me.objcolCompany.Text = ""
        Me.objcolCompany.Width = 0
        '
        'btnUnlock
        '
        Me.btnUnlock.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnlock.BackColor = System.Drawing.Color.White
        Me.btnUnlock.BackgroundImage = CType(resources.GetObject("btnUnlock.BackgroundImage"), System.Drawing.Image)
        Me.btnUnlock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnlock.BorderColor = System.Drawing.Color.Empty
        Me.btnUnlock.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnlock.FlatAppearance.BorderSize = 0
        Me.btnUnlock.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnlock.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnlock.ForeColor = System.Drawing.Color.Black
        Me.btnUnlock.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnlock.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlock.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.Location = New System.Drawing.Point(175, 239)
        Me.btnUnlock.Name = "btnUnlock"
        Me.btnUnlock.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlock.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlock.Size = New System.Drawing.Size(92, 30)
        Me.btnUnlock.TabIndex = 19
        Me.btnUnlock.Text = "&Register"
        Me.btnUnlock.UseVisualStyleBackColor = False
        Me.btnUnlock.Visible = False
        '
        'frmCommonSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(567, 283)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonSelection"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Company Selection"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.picSideImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents picSideImage As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOpen As eZee.Common.eZeeLightButton
    Friend WithEvents lvCompany As System.Windows.Forms.ListView
    Friend WithEvents colhProperty As System.Windows.Forms.ColumnHeader
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lvDatabase As eZee.Common.eZeeListView
    Friend WithEvents colhDatabase As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhcode As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnUnlock As eZee.Common.eZeeLightButton
End Class
