﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Aruti Report Designer")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Aruti")> 
<Assembly: AssemblyProduct("Aruti")> 
<Assembly: AssemblyCopyright(acore32.core.Copyrightversion)> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("91a0ab0e-8823-41b5-8e0f-2ae044ba4355")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

'<Assembly: AssemblyVersion("9.0.0.1")> 
'<Assembly: AssemblyFileVersion("9.0.0.1")> 
<Assembly: AssemblyVersion(acore32.core.Version)> 
<Assembly: AssemblyFileVersion(acore32.core.Version)> 