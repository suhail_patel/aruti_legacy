Imports System.Xml
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports ArutiReport.Engine.Library.eZee_Sample
Imports System.Windows.Forms
Imports System.Text
Imports System.IO
Imports eZee.Common
Imports Aruti.Data

Public Class FrmReportDesigner
    Private bmpBitmap As Bitmap
    Private mintGridX As Integer
    Private mintGridY As Integer
    Private dsMstXML As New DataSet
    'Private dsDetXML As New DataSet
    Private mintX As Integer
    Private mintY As Integer
    Private mctrl As TemplateBoxField
    Private mctrlLine As TemplateLineField
    Private mctrlImage As TemplateImageField
    Private tnSelectedNode As TreeNode
    Private m_last_selection_rectangle As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
    Private m_is_selecting As Boolean = False
    Private m_start_selection As Point = New Point(0)
    Private intId As Integer = 0
    Private mouseCurrentPoisiton As Point

    'Image Path 
    Private aImagePath() As String
    Private aCpyImgPath() As String

    Public mblnShowGrid As Boolean
    Public arrIdList As ArrayList
    Public arrObjectList As ArrayList
    Public arrLineObjectList As ArrayList


    Public arrCopyCutList As New ArrayList
    Private blnCopy As Boolean = False

    Private sMst As String = AppSettings._Object._ApplicationPath & "\TemplateMst"
    Private sDet As String = AppSettings._Object._ApplicationPath & "\TemplateDet"
    Private sFormula As String = AppSettings._Object._ApplicationPath & "\TemplateFormula"

    Private nFormulaCtr As Integer = 0
    'Forms
    Dim frmProcess1 As New frmProcess
    Dim tpPrintSample As New ToolTip

    'Suppress    
    Dim lSuppreHeaderPnl As Boolean = False
    Dim lSuppreFooterPnl As Boolean = False
    Dim lSuppreDetPnl As Boolean = False
    Dim lSuppreDetFotPnl As Boolean = False
    Dim lSuppreGroup1Pnl As Boolean = False
    Dim lSuppreGroupFooter1Pnl As Boolean = False
    Dim lSuppreCompHeaderPnl As Boolean = False

    'Prnting    
    Private oPrintPreDiag As New PrintPreviewDialog
    Private PageNumber As Integer = 0
    Dim string_format As New StringFormat
    Dim nCompHeadery As Integer = 0
    Dim nHeadery As Integer = 0
    Dim nDetHeadery As Integer = 0
    Dim nDetDety As Integer = 0
    Dim nDetFootery As Integer = 0
    Dim nGroup1y As Integer = 0
    Dim nGrpFooter1y As Integer = 0
    Dim dtTable As New DataTable
    Dim sMstXMLPaths As String = ""
    Dim dsMst As New DataSet
    Dim dsDet As New DataSet
    Dim dsFormula As New DataSet
    Dim nPageNo As Integer = 0
    Dim nPrintCtr As Integer = 0
    Dim iCtr As Integer = 0
    Dim nCounter As Integer = 0

    'Size
    Dim CanvasHeight As Decimal = 12
    Dim nTop As Decimal = 0.25
    Dim nLeft As Decimal = 0.25
    Dim nBottom As Decimal = 0.25
    Dim nRight As Decimal = 0.25
    Dim nVScroll As Int16 = 0
    'Html 
    Dim nImg As Int16 = 0
    Dim strHtmlDir As String = ""

    Dim sPaperSize As String = "Manual"


    Private Sub chkGrid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGrid.CheckedChanged
        CreateGridPiece(mintGridX, mintGridY, True, True, True)
        If chkGrid.Checked = True Then
            mblnShowGrid = True
        Else
            mblnShowGrid = False
        End If

        If chkGrid.Checked = True Then
            pnlCompHeader.BackgroundImage = bmpBitmap
            pnlCompHeader.BackgroundImageLayout = ImageLayout.Tile
            pnlHeader.BackgroundImage = bmpBitmap
            pnlHeader.BackgroundImageLayout = ImageLayout.Tile
            pnlGroup1.BackgroundImage = bmpBitmap
            pnlGroup1.BackgroundImageLayout = ImageLayout.Tile
            pnlDetail.BackgroundImage = bmpBitmap
            pnlDetail.BackgroundImageLayout = ImageLayout.Tile
            pnlGroupFooter1.BackgroundImage = bmpBitmap
            pnlGroupFooter1.BackgroundImageLayout = ImageLayout.Tile
            pnlDetailFooter.BackgroundImage = bmpBitmap
            pnlDetailFooter.BackgroundImageLayout = ImageLayout.Tile
            pnlFooter.BackgroundImage = bmpBitmap
            pnlFooter.BackgroundImageLayout = ImageLayout.Tile
        Else
            pnlCompHeader.BackgroundImage = Nothing
            pnlHeader.BackgroundImage = Nothing
            pnlDetail.BackgroundImage = Nothing
            pnlDetailFooter.BackgroundImage = Nothing
            pnlFooter.BackgroundImage = Nothing
            pnlGroupFooter1.BackgroundImage = Nothing
            pnlGroup1.BackgroundImage = Nothing
        End If
    End Sub

    Private Sub CreateGridPiece(ByVal iWidth As Integer, ByVal iHeight As Integer, ByVal bCenter As Boolean, ByVal bWidthCenter As Boolean, ByVal bHeightCenter As Boolean)
        If chkGrid.Checked = True Then
            bmpBitmap = New Bitmap(iWidth, iHeight)
            For y As Integer = 0 To bmpBitmap.Width - 1
                For x As Integer = 0 To bmpBitmap.Height - 1
                    bmpBitmap.SetPixel(x, y, Color.FromArgb(255, 255, 255, 255))
                Next
            Next
            bmpBitmap.SetPixel(0, 0, Color.FromArgb(255, 0, 0, 0))
        Else
            bmpBitmap.SetPixel(0, 0, Color.White)
        End If
        Me.Refresh()
    End Sub

    Private Sub Form1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Set_Logo(Me, gApplicationType)
        Select Case e.KeyData
            Case Keys.A + Keys.Control
                SelectAllFields()
                MyBase.Refresh()
            Case Keys.U + Keys.Control
                UnselectFields()
                MyBase.Refresh()
            Case Keys.Shift + Keys.Delete
                DeleteAllFields()
            Case Keys.C + Keys.Control
                CopyValue()
            Case Keys.V + Keys.Control
                Pastevalue()
                'Case Keys.Left
                '    SelectedFieldMovement(pnlHeader.Controls, "Left")
                'Case Keys.Right
                '    SelectedFieldMovement(pnlHeader.Controls, "Right")
                'Case Keys.Up
                '    SelectedFieldMovement(pnlHeader.Controls, "Up")
                'Case Keys.Down
                '    SelectedFieldMovement(pnlHeader.Controls, "Down")
        End Select
    End Sub
#Region "Select and Delete all Fields with Keyboard"
    '********Select and Delete all fields with keyboard.****************
    'Public Sub SelectedFieldMovement(ByVal objField As Object, ByVal sMove As String)
    '    'For Each objField As Object In pnlHeader.Controls
    '    If sMove = "Left" Then
    '        For i As Integer = 0 To CType(objField, Panel).Controls.Count - 1
    '            CType(objField, Panel).Controls(i).Location = New Point(CType(objField, Panel).Controls(i).Location.X + 1, CType(objField, Panel).Controls(i).Location.Y)
    '        Next
    '    ElseIf sMove = "Right" Then
    '        For i As Integer = 0 To CType(objField, Panel).Controls.Count - 1
    '            CType(objField, Panel).Controls(i).Location = New Point(CType(objField, Panel).Controls(i).Location.X - 1, CType(objField, Panel).Controls(i).Location.Y)
    '        Next
    '    ElseIf sMove = "Up" Then
    '        For i As Integer = 0 To CType(objField, Panel).Controls.Count - 1
    '            CType(objField, Panel).Controls(i).Location = New Point(CType(objField, Panel).Controls(i).Location.X, CType(objField, Panel).Controls(i).Location.Y + 1)
    '        Next
    '    ElseIf sMove = "Down" Then
    '        For i As Integer = 0 To CType(objField, Panel).Controls.Count - 1
    '            CType(objField, Panel).Controls(i).Location = New Point(CType(objField, Panel).Controls(i).Location.X, CType(objField, Panel).Controls(i).Location.Y - 1)
    '        Next
    '    End If

    'End Sub

    Public Sub SelectAllFields()
        'Header
        For Each objField As Object In pnlCompHeader.Controls
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If
            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next

        'Header
        For Each objField As Object In pnlHeader.Controls
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If
            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next

        'Group 1
        For Each objField As Object In pnlGroup1.Controls
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If
            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next

        'Detail
        For Each objField As Object In pnlDetail.Controls
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If

            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next

        'Group Footer 1
        For Each objField As Object In pnlGroupFooter1.Controls
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If
            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next


        'DetailFooter
        For Each objField As Object In pnlDetailFooter.Controls
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If

            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next

        'Footer
        For Each objField As Object In pnlFooter.Controls
            'If TypeOf objField Is TemplateBoxField Then
            objField.Select()
            If arrIdList.Contains(objField._FieldId) = False Then
                arrIdList.Add(objField._FieldId)
                arrObjectList.Add(objField)
            End If

            If TypeOf objField Is TemplateBoxField Then
                mctrl = objField
            End If
            If TypeOf objField Is TemplateLineField Then
                mctrlLine = objField
            End If
            If TypeOf objField Is TemplateImageField Then
                mctrlImage = objField
            End If
        Next

        objlblProperty.Text = "Property:" & ""
        pgTemplateField.SelectedObject = Nothing

        '*****Toolbar Buttons Enabled******
        If arrIdList.Count = 1 Then
            DisabledToolbarButtons()
            edit_Delete_Button.Enabled = True
        ElseIf arrIdList.Count = 0 Then
            DisabledToolbarButtons()
            edit_Delete_Button.Enabled = False
        ElseIf arrIdList.Count > 1 Then
            EnabledToolbarButtons()
            edit_Delete_Button.Enabled = True
        End If
    End Sub

    Public Sub DeleteAllFields()
        Dim tempArr As New ArrayList
        '''''Company Header'''''''''
        For Each objField As Object In pnlCompHeader.Controls
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlCompHeader.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        '''''Header'''''''''
        For Each objField As Object In pnlHeader.Controls
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlHeader.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        '''''Group 1 '''''''''
        For Each objField As Object In pnlGroup1.Controls
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlGroup1.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        ''''Detail'''''''''
        For Each objField As Object In pnlDetail.Controls
            'If TypeOf objField Is TemplateBoxField Then
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlDetail.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        '''''Group Footer 1 '''''''''
        For Each objField As Object In pnlGroupFooter1.Controls
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlGroupFooter1.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        ''''''DetailFooter'''''''
        For Each objField As Object In pnlDetailFooter.Controls
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlDetailFooter.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        '''''Footer'''''''
        For Each objField As Object In pnlFooter.Controls
            If arrIdList.Contains(objField._FieldId) = True Then
                arrIdList.Remove(objField._FieldId)
                cboField.Items.Remove(objField._FieldName & objField._FieldId & " : " & objField._Value)
                tempArr.Add(objField)
            End If
        Next

        For Each objCtrl As Object In tempArr
            arrObjectList.Remove(objCtrl)
            pnlFooter.Controls.Remove(objCtrl)
        Next

        tempArr.Clear()

        objlblProperty.Text = "Property:" & ""
        pgTemplateField.SelectedObject = Nothing
    End Sub
#End Region


    Private mstrTemplate As String = ""
    Public Property Template() As String
        Get
            Return mstrTemplate
        End Get
        Set(ByVal value As String)
            mstrTemplate = value
        End Set
    End Property

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mintGridX = 6
        mintGridY = 6
        pnlCanvas.Height = 1000
        pnlCanvasRuler.Height = 1000
        txtItemPrint.Text = "6.00"
        DefaultPnlHeight()
        btnSave.Enabled = False
        btnDelete.Enabled = False
        btnCancel.Enabled = False
        btnConverter.Enabled = False
        cmbType.Tag = "0"
        cmbTypeList()
        arrIdList = New ArrayList
        arrObjectList = New ArrayList
        arrLineObjectList = New ArrayList

        If mstrTemplate <> "" Then
            cmbType.Text = mstrTemplate
            btnLoad_Click(Nothing, Nothing)
        End If
        'Dim Graphics As Graphics
        'Graphics = CreateGraphics()
        'MsgBox(Graphics.DpiX.ToString + Graphics.DpiY.ToString)
        'Graphics.Dispose()

    End Sub

    Dim mblnLibraryOnly As Boolean = False
    Public Sub ShowMeNow(ByVal sTypeName As String, ByVal FolioXMLPath As String)
        Dim ds As New DataSet
        Try
            ds.ReadXml(sMst)
            If ds.Tables.Count > 0 Then
                Dim dRow() As DataRow = ds.Tables(0).Select("trnasid = " & sTypeName)
                If dRow.Length > 0 Then
                    mintGridX = 6
                    mintGridY = 6
                    pnlCanvas.Height = 1000
                    pnlCanvasRuler.Height = 1000
                    txtItemPrint.Text = "6.00"
                    DefaultPnlHeight()
                    btnSave.Enabled = False
                    btnDelete.Enabled = False
                    btnCancel.Enabled = False
                    btnConverter.Enabled = False
                    cmbType.Tag = "0"
                    cmbTypeList()
                    arrIdList = New ArrayList
                    arrObjectList = New ArrayList
                    arrLineObjectList = New ArrayList

                    cmbType.Text = dRow(0)("PrintType")
                    cmbType.Tag = dRow(0)("trnasid")
                    txtMstXMLPath.Text = FolioXMLPath
                    mblnLibraryOnly = True
                    'btnLoad.PerformClick()
                    'btnPreview.PerformClick()
                    btnLoad_Click(Nothing, Nothing)
                    btnPreview_Click(Nothing, Nothing)
                Else
                    MsgBox("No Data in data row", MsgBoxStyle.OkOnly, "ShowMe")
                End If
            Else
                MsgBox("Out", MsgBoxStyle.OkOnly, "ShowMe")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ShowMeNow")
        End Try
    End Sub
    Private Sub cmbTypeList()
        Try
            cmbType.Items.Clear()
            Dim ds As New DataSet
            ds.ReadXml(sMst)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dr() As DataRow = ds.Tables(0).Select("", "PrintType")
                    For i As Integer = 0 To dr.Length - 1
                        cmbType.Items.Add(dr(i)("PrintType"))
                    Next
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "cmbTypeList")
        End Try
    End Sub

    Private Sub chkSnapGrid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapGrid.CheckedChanged
        If chkSnapGrid.Checked = True Then
            mblnSnapToGrid = True
        Else
            mblnSnapToGrid = False
        End If
    End Sub
    Private Sub FillTreeView()
        Try
            tvControls.Font = New Font("Arial", 9, FontStyle.Bold)
            tvControls.Nodes.Clear()

            tvControls.Nodes.Add("1.Common Tools")
            tvControls.Nodes(0).ForeColor = Color.Chocolate

            tvControls.Nodes(0).Nodes.Add("Text")
            tvControls.Nodes(0).Nodes.Item(0).Name = "Text"
            tvControls.Nodes(0).Nodes.Item(0).Text = "Text"
            tvControls.Nodes(0).Nodes.Item(0).Tag = "String|0"
            tvControls.Nodes(0).Nodes.Item(0).ToolTipText = "Text"

            tvControls.Nodes(0).Nodes.Add("Rectangle")
            tvControls.Nodes(0).Nodes.Item(1).Name = "Rectangle"
            tvControls.Nodes(0).Nodes.Item(1).Text = "Rectangle"
            tvControls.Nodes(0).Nodes.Item(1).Tag = "String|4"
            tvControls.Nodes(0).Nodes.Item(1).ToolTipText = "Rectangle"

            tvControls.Nodes(0).Nodes.Add("Line")
            tvControls.Nodes(0).Nodes.Item(2).Name = "Line"
            tvControls.Nodes(0).Nodes.Item(2).Text = "Line"
            tvControls.Nodes(0).Nodes.Item(2).Tag = "String|5"
            tvControls.Nodes(0).Nodes.Item(2).ToolTipText = "Line"

            tvControls.Nodes(0).Nodes.Add("Image")
            tvControls.Nodes(0).Nodes.Item(3).Name = "Image"
            tvControls.Nodes(0).Nodes.Item(3).Text = "Image"
            tvControls.Nodes(0).Nodes.Item(3).Tag = "Image|6"
            tvControls.Nodes(0).Nodes.Item(3).ToolTipText = "Image"

            Dim nCtr As Int16 = 0
            'Master XML
            dsMstXML.Clear()
            dsMstXML.Tables.Clear()
            If txtMstXMLPath.Text <> "" Then
                dsMstXML.ReadXml(txtMstXMLPath.Text)
            End If

            tvControls.Nodes.Add("Group")
            tvControls.Nodes(1).ForeColor = Color.Chocolate
            nCtr += 1

            If dsMstXML.Tables.Count > 0 Then
                If dsMstXML.Tables(0).Columns.Count > 0 Then
                    tvControls.Nodes.Add("2.Master")
                    nCtr += 1
                    tvControls.Nodes(nCtr).ForeColor = Color.Chocolate

                    For i As Integer = 0 To dsMstXML.Tables(0).Columns.Count - 1
                        If dsMstXML.Tables(0).Columns(i).ColumnName Like ".Formula_*" Then
                            'If nFormulaCtr < dsMstXML.Tables(0).Columns(i).ColumnName.Split("_")(1) Then nFormulaCtr = dsMstXML.Tables(0).Columns(i).ColumnName.Split("_")(1)
                            'tvControls.Nodes(nCtr).Nodes.Add(dsMstXML.Tables(0).Columns(i).ColumnName)
                            'If dsMstXML.Tables(0).Rows(0)(i) IsNot DBNull.Value Then
                            '    tvControls.Nodes(nCtr).Nodes.Item(i).Tag = dsMstXML.Tables(0).Rows(0)(i) & "|" & "1"
                            '    tvControls.Nodes(nCtr).Nodes.Item(i).ToolTipText = dsMstXML.Tables(0).Rows(0)(i).ToString
                            'End If
                        Else
                            tvControls.Nodes(nCtr).Nodes.Add(dsMstXML.Tables(0).Columns(i).ColumnName)
                            tvControls.Nodes(nCtr).Nodes.Item(i).Tag = dsMstXML.Tables(0).Columns(i).DataType.Name & "|" & "1"
                            tvControls.Nodes(nCtr).Nodes.Item(i).ToolTipText = dsMstXML.Tables(0).Columns(i).ColumnName
                        End If
                    Next
                End If
                If dsMstXML.Tables.Count > 1 Then
                    If dsMstXML.Tables(1).Columns.Count > 0 Then
                        tvControls.Nodes.Add("3.Detail")
                        nCtr += 1
                        tvControls.Nodes(nCtr).ForeColor = Color.Chocolate
                        For i As Integer = 0 To dsMstXML.Tables(1).Columns.Count - 1
                            If dsMstXML.Tables(1).Columns(i).ColumnName Like ".Formula_*" Then
                                'If nFormulaCtr < dsMstXML.Tables(1).Columns(i).ColumnName.Split("_")(1) Then nFormulaCtr = dsMstXML.Tables(1).Columns(i).ColumnName.Split("_")(1)
                                'tvControls.Nodes(nCtr).Nodes.Add(dsMstXML.Tables(1).Columns(i).ColumnName)
                                'If dsMstXML.Tables(1).Rows(0)(i) IsNot DBNull.Value Then
                                '    tvControls.Nodes(nCtr).Nodes.Item(i).Tag = dsMstXML.Tables(1).Rows(0)(i) & "|" & "1"
                                '    tvControls.Nodes(nCtr).Nodes.Item(i).ToolTipText = dsMstXML.Tables(1).Rows(0)(i).ToString
                                'End If
                            Else
                                tvControls.Nodes(nCtr).Nodes.Add(dsMstXML.Tables(1).Columns(i).ColumnName)
                                tvControls.Nodes(nCtr).Nodes.Item(i).Tag = dsMstXML.Tables(1).Columns(i).DataType.Name & "|" & "1"
                                tvControls.Nodes(nCtr).Nodes.Item(i).ToolTipText = dsMstXML.Tables(1).Columns(i).ColumnName
                            End If
                        Next
                    End If
                End If
            End If

            Dim dsFormula As New DataSet
            dsFormula.ReadXml(sFormula)
            If dsFormula.Tables.Count > 0 Then
                With dsFormula.Tables(0)
                    Dim drow() As DataRow = .Select(" isnull(TypeId,0) = '" & cmbType.Tag & "'")
                    If drow.Length > 0 Then
                        For Each drows As DataRow In drow
                            If drows("Form") = "2.Master" Then
                                tvControls.Nodes(2).Nodes.Add(drows("FormulaName"))
                                tvControls.Nodes(2).Nodes.Item(tvControls.Nodes(2).Nodes.Count - 1).Tag = drows("FormulaExp").ToString & "|" & "1"
                                tvControls.Nodes(2).Nodes.Item(tvControls.Nodes(2).Nodes.Count - 1).ToolTipText = drows("FormulaExp").ToString
                            ElseIf drows("Form") = "3.Detail" Then
                                tvControls.Nodes(3).Nodes.Add(drows("FormulaName"))
                                tvControls.Nodes(3).Nodes.Item(tvControls.Nodes(3).Nodes.Count - 1).Tag = drows("FormulaExp").ToString & "|" & "1"
                                tvControls.Nodes(3).Nodes.Item(tvControls.Nodes(3).Nodes.Count - 1).ToolTipText = drows("FormulaExp").ToString
                            ElseIf drows("Form") = "Group" Then
                                tvControls.Nodes(1).Nodes.Add(drows("FormulaName"))
                                tvControls.Nodes(1).Nodes.Item(tvControls.Nodes(1).Nodes.Count - 1).Tag = drows("FormulaExp").ToString
                                tvControls.Nodes(1).Nodes.Item(tvControls.Nodes(1).Nodes.Count - 1).ToolTipText = drows("FormulaExp").ToString
                            End If
                        Next
                    End If
                End With
            End If

            tvControls.ExpandAll()
            tvControls.Refresh()
            tvControls.SelectedNode = tvControls.Nodes(0)
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub

    ' ''Private Sub btnPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMstPath.Click, btnDetPath.Click
    ' ''    Dim opFileDialog As New OpenFileDialog
    ' ''    opFileDialog.Filter = "XML(*.XML)|*.XML|All(*.*)|*.*"
    ' ''    opFileDialog.ShowDialog()
    ' ''    If opFileDialog.FileName.Trim <> "" Then
    ' ''        If CType(sender, Button).Name = "btnMstPath" Then txtMstXMLPath.Text = opFileDialog.FileName Else txtDetXMLPath.Text = opFileDialog.FileName
    ' ''        FillTreeView()
    ' ''        ClearDataPnl()
    ' ''    End If
    ' ''End Sub
    Private Sub pnlHeader_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles pnlHeader.DragEnter, pnlDetail.DragEnter, pnlDetailFooter.DragEnter, pnlFooter.DragEnter, pnlGroup1.DragEnter, pnlGroupFooter1.DragEnter, pnlCompHeader.DragEnter
        If Not tvControls.Nodes.Contains(tnSelectedNode) Then
            If Not tvControls.SelectedNode.Parent.Text = "Group" Then
                e.Effect = e.AllowedEffect
            End If
        End If
    End Sub

    Private Sub pnlHeader_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles pnlHeader.DragDrop, pnlDetail.DragDrop, pnlDetailFooter.DragDrop, pnlFooter.DragDrop, pnlGroup1.DragDrop, pnlGroupFooter1.DragDrop, pnlCompHeader.DragDrop
        Dim objField As New TemplateBoxField
        Dim objLine As New TemplateLineField
        Dim objImage As New TemplateImageField

        Try
            e.Effect = DragDropEffects.Copy
            If Not tvControls.Nodes.Contains(tnSelectedNode) Then
                Dim arr() As String = tvControls.Nodes(tnSelectedNode.Parent.Index).Nodes(tnSelectedNode.Index).Tag.ToString.Split("|")
                Dim targetPoint As Point = sender.PointToClient(New Point(e.X, e.Y))

                mintX = targetPoint.X
                mintY = targetPoint.Y

                If arr(arr.Length - 1) = 5 Then
                    objLine.BackColor = Color.Transparent
                    objLine.Tag = arr(0)
                    objLine.FieldType = arr(arr.Length - 1)
                    objLine.Name = "Line"
                    objLine.Text = ""
                    objLine.Vertical = False
                    objLine.TextAlign = ContentAlignment.MiddleLeft
                    objLine.Font = pnlCanvas.Font
                    setLineField(objLine, 1, objLine.FieldType)
                    CreateField(objLine, IIf(sender.name = "pnlCompHeader", 0, IIf(sender.name = "pnlHeader", 1, IIf(sender.name = "pnlDetail", 3, IIf(sender.name = "pnlDetailFooter", 4, IIf(sender.name = "pnlFooter", 5, IIf(sender.name = "pnlGroup1", 6, 7)))))), "L")
                ElseIf arr(arr.Length - 1) = 6 Then
                    objImage.BackColor = Color.Transparent
                    objImage.Tag = arr(0)
                    objImage.FieldType = arr(arr.Length - 1)
                    objImage.Name = "Image"
                    objImage.Text = ""
                    objImage.Font = pnlCanvas.Font
                    'setLineField(objImage, 1, objLine.FieldType)
                    CreateField(objImage, IIf(sender.name = "pnlCompHeader", 0, IIf(sender.name = "pnlHeader", 1, IIf(sender.name = "pnlDetail", 3, IIf(sender.name = "pnlDetailFooter", 4, IIf(sender.name = "pnlFooter", 5, IIf(sender.name = "pnlGroup1", 6, 7)))))), "I")
                Else
                    If arr(arr.Length - 1) = 4 Then
                        objField.BackColor = Color.Transparent
                    Else
                        objField.BackColor = Color.White
                    End If
                    objField.Tag = arr(0)
                    objField.FieldType = arr(arr.Length - 1)
                    objField.Name = tvControls.Nodes(tnSelectedNode.Parent.Index).Nodes(tnSelectedNode.Index).Name
                    objField.Text = tvControls.Nodes(tnSelectedNode.Parent.Index).Nodes(tnSelectedNode.Index).Text
                    objField.TextAlign = ContentAlignment.MiddleLeft
                    'objField.Font = pnlCanvas.Font
                    objField.ForeColor = Color.Black
                    setField(objField, 1, objField.FieldType)
                    CreateField(objField, IIf(sender.name = "pnlCompHeader", 0, IIf(sender.name = "pnlHeader", 1, IIf(sender.name = "pnlDetail", 3, IIf(sender.name = "pnlDetailFooter", 4, IIf(sender.name = "pnlFooter", 5, IIf(sender.name = "pnlGroup1", 6, 7)))))), "B")
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
    Private Sub CreateField(ByVal obj As Object, ByVal Counter As Int16, ByVal sType As String)
        Dim objLine As New TemplateLineField
        Dim objField As New TemplateBoxField
        Dim objImage As New TemplateImageField

        Dim sFeildType As String = ""
        intId += 1
        Try
            If sType = "L" Then
                sFeildType = CType(obj, TemplateLineField).FieldType
            ElseIf sType = "I" Then
                sFeildType = 6
            Else
                sFeildType = CType(obj, TemplateBoxField).FieldType
            End If
            If sFeildType = 5 Then         ''Field_Type : 5 = Static_Line
                objLine = obj
                objLine._FieldId = intId
                objLine.Left = mintX
                objLine.Top = mintY
                objLine.Text = "Line"
                Select Case Counter
                    Case 0 '"cmnuCompHeader" 
                        pnlCompHeader.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlCompHeader.Height < objLine.Top + objLine.Height Then
                            pnlCompHeader.Height = objLine.Top + objLine.Height
                        End If
                    Case 1 '"cmnuHeader" 
                        pnlHeader.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlHeader.Height < objLine.Top + objLine.Height Then
                            pnlHeader.Height = objLine.Top + objLine.Height
                        End If
                    Case 3 '"cmnuDetail"
                        pnlDetail.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlDetail.Height < objLine.Top + objLine.Height Then
                            pnlDetail.Height = objLine.Top + objLine.Height
                        End If
                    Case 4 '"cmnuDetailFooter"
                        pnlDetailFooter.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlDetailFooter.Height < objLine.Top + objLine.Height Then
                            pnlDetailFooter.Height = objLine.Top + objLine.Height
                        End If
                    Case 5 '"cmnuFooter"
                        pnlFooter.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlFooter.Height < objLine.Top + objLine.Height Then
                            pnlFooter.Height = objLine.Top + objLine.Height
                        End If
                    Case 6 '"cmnuGroup1"
                        pnlGroup1.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlGroup1.Height < objLine.Top + objLine.Height Then
                            pnlGroup1.Height = objLine.Top + objLine.Height
                        End If
                    Case 7 '"cmnuGroupFooter"
                        pnlGroupFooter1.Controls.Add(objLine)
                        cboField.Items.Add(objLine._FieldName & objLine._FieldId & " : " & objLine._Value)
                        If pnlGroupFooter1.Height < objLine.Top + objLine.Height Then
                            pnlGroupFooter1.Height = objLine.Top + objLine.Height
                        End If
                End Select

                AddHandler objLine.MouseDown, AddressOf ctl_Click

                SetLineProperty(objLine)
                If objLine.FieldType = 4 Then
                    'objLine.SendToBack()
                Else
                    objLine.BringToFront()
                End If

                'If mAction <> enAction.EDIT_ONE Then
                '    objlblProperty.Text = "Property:" & objLine._FieldName
                '    pgTemplateField.SelectedObject = objLine
                'End If
            ElseIf sFeildType = 6 Then
                objImage = obj
                objImage._FieldId = intId
                objImage.Left = mintX
                objImage.Top = mintY
                Select Case sFeildType
                    Case 2 'Image Field
                        objImage.Text = CType(obj, TemplateImageField)._FieldName
                End Select

                Select Case Counter
                    Case 0 '"cmnuCompHeader" 
                        pnlCompHeader.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlCompHeader.Height < objImage.Height + objImage.Top Then
                            pnlCompHeader.Height = objImage.Height + objImage.Top
                        End If
                    Case 1 '"cmnuHeader" 
                        pnlHeader.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlHeader.Height < objImage.Height + objImage.Top Then
                            pnlHeader.Height = objImage.Height + objImage.Top
                        End If
                    Case 3 '"cmnuDetail"
                        pnlDetail.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlDetail.Height < objImage.Height + objImage.Top Then
                            pnlDetail.Height = objImage.Height + objImage.Top
                        End If
                    Case 4 '"cmnuDetailFooter"
                        pnlDetailFooter.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlDetailFooter.Height < objImage.Height + objImage.Top Then
                            pnlDetailFooter.Height = objImage.Height + objImage.Top
                        End If
                    Case 5 '"cmnuFooter"
                        pnlFooter.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlFooter.Height < objImage.Height + objImage.Top Then
                            pnlFooter.Height = objImage.Height + objImage.Top
                        End If
                    Case 6 '"cmnuGroup1"
                        pnlGroup1.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlGroup1.Height < objImage.Height + objImage.Top Then
                            pnlGroup1.Height = objImage.Height + objImage.Top
                        End If
                    Case 7 '"cmnuGroupFooter"
                        pnlGroupFooter1.Controls.Add(objImage)
                        cboField.Items.Add(objImage._FieldName & objImage._FieldId & " : " & objImage._Value)
                        If pnlGroupFooter1.Height < objImage.Height + objImage.Top Then
                            pnlGroupFooter1.Height = objImage.Height + objImage.Top
                        End If
                End Select

                AddHandler objImage.MouseDown, AddressOf ctl_Click
                'objField.ContextMenuStrip = cmnuDelete
                SetImageProperty(objImage)
                objImage.BringToFront()
            Else

                objField = obj
                objField._FieldId = intId
                objField.Left = mintX
                objField.Top = mintY
                objField.Height = 18
                objField.Font = New Font("Arial", 9, FontStyle.Regular)
                objField.CanDraw = False
                Select Case sFeildType
                    Case 0 'Text Field
                        objField.Text = CType(obj, TemplateBoxField)._Value
                    Case 1 'Data Field
                        objField.Text = CType(obj, TemplateBoxField).Text
                    Case 2 'Image Field
                        objField.Text = CType(obj, TemplateBoxField)._FieldName
                    Case 3 'Custom Data
                        'objField.Text = objPOSTemplate.Field._FieldName
                        'objField.TextAlign = ContentAlignment.TopCenter
                    Case 4 'Static Box
                        objField.Text = CType(obj, TemplateBoxField)._FieldName
                        objField.TextAlign = ContentAlignment.MiddleCenter
                End Select

                'Manish Tanna (12 May 2012) -- Start 
                objField.Format = CType(obj, TemplateBoxField).Format
                'Manish Tanna (12 May 2012) -- end 
                Select Case Counter
                    Case 0 'Comp Header
                        pnlCompHeader.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlCompHeader.Height < objField.Height + objField.Top Then
                            pnlCompHeader.Height = objField.Height + objField.Top
                        End If
                    Case 1 '"cmnuHeader" 
                        If CType(obj, TemplateBoxField).FieldType = 2 Then
                            objField.TextAlign = ContentAlignment.MiddleCenter
                            objField.MinimumSize = New Size(10, 10)
                        End If
                        pnlHeader.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlHeader.Height < objField.Height + objField.Top Then
                            pnlHeader.Height = objField.Height + objField.Top
                        End If
                    Case 3 '"cmnuDetail"
                        pnlDetail.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlDetail.Height < objField.Height + objField.Top Then
                            pnlDetail.Height = objField.Height + objField.Top
                        End If
                    Case 4 '"cmnuDetailFooter"
                        pnlDetailFooter.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlDetailFooter.Height < objField.Height + objField.Top Then
                            pnlDetailFooter.Height = objField.Height + objField.Top
                        End If
                    Case 5 '"cmnuFooter"
                        pnlFooter.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlFooter.Height < objField.Height + objField.Top Then
                            pnlFooter.Height = objField.Height + objField.Top
                        End If
                    Case 6 '"cmnuGroup1"
                        pnlGroup1.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlGroup1.Height < objField.Height + objField.Top Then
                            pnlGroup1.Height = objField.Height + objField.Top
                        End If
                    Case 7 '"cmnuGroupFooter"
                        pnlGroupFooter1.Controls.Add(objField)
                        cboField.Items.Add(objField._FieldName & objField._FieldId & " : " & objField._Value)
                        If pnlGroupFooter1.Height < objField.Height + objField.Top Then
                            pnlGroupFooter1.Height = objField.Height + objField.Top
                        End If
                End Select

                AddHandler objField.MouseDown, AddressOf ctl_Click

                'objField.ContextMenuStrip = cmnuDelete

                SetProperty(objField)

                If objField.FieldType = 4 Then
                    'objField.SendToBack()
                Else
                    objField.BringToFront()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "CreateField")
        End Try
    End Sub
    Private Sub setLineField(ByVal objLine As TemplateLineField, ByVal intSection As Integer, ByVal intFieldType As String)
        Try

            objLine.Tag = Guid.NewGuid.ToString

        Catch ex As Exception
            MessageBox.Show(ex.Message, "setLineField")
        End Try
    End Sub
    Dim m_resSetTable As TemplateBoxField
    Dim m_resLineTable As TemplateLineField
    Dim m_resImageTable As TemplateImageField
    Private Sub ctl_Click(ByVal sender As System.Object, ByVal e As MouseEventArgs)

        If TypeOf sender Is TemplateLineField Then   '''''Line Control

            '******Left and Shift Key Press. Multiple fields select******
            If e.Button = Windows.Forms.MouseButtons.Left AndAlso Control.ModifierKeys = Keys.Control Then

                m_resLineTable = DirectCast(sender, LineControl)
                m_resLineTable.Select()

                If arrIdList.Contains(m_resLineTable._FieldId) = False Then
                    arrIdList.Add(m_resLineTable._FieldId)
                    arrObjectList.Add(m_resLineTable)
                    arrLineObjectList.Add(m_resLineTable)
                Else
                    If Not m_resLineTable Is Nothing Then
                        m_resLineTable.Unselect()
                        arrIdList.Remove(m_resLineTable._FieldId)
                        arrObjectList.Remove(m_resLineTable)
                        arrLineObjectList.Remove(m_resLineTable)
                        m_resLineTable = Nothing
                    End If
                End If

                objlblProperty.Text = "Property:" & ""
                pgTemplateField.SelectedObject = Nothing

                '*******Left Key Press. Only one Field Select******
            ElseIf e.Button = Windows.Forms.MouseButtons.Left Then
                If arrIdList IsNot Nothing Then
                    If arrIdList.Count = 1 Then  '''''For Select control and unselect previous control
                        If Not m_resLineTable Is Nothing Then
                            arrIdList.Remove(m_resLineTable._FieldId)
                            arrObjectList.Remove(m_resLineTable)
                            arrLineObjectList.Remove(m_resLineTable)
                            m_resLineTable.Unselect()
                            m_resLineTable = Nothing
                        End If
                        If Not m_resSetTable Is Nothing Then
                            arrIdList.Remove(m_resSetTable._FieldId)
                            arrObjectList.Remove(m_resSetTable)
                            m_resSetTable.Unselect()
                            m_resSetTable = Nothing
                        End If
                    End If
                End If

                m_resLineTable = DirectCast(sender, LineControl)
                m_resLineTable.Select()

                For i As Integer = 0 To cboField.Items.Count - 1
                    If (m_resLineTable._FieldName & m_resLineTable._FieldId) = cboField.Items(i) Then
                        cboField.SelectedItem = cboField.Items(i)
                    End If
                Next

                If arrIdList IsNot Nothing Then
                    If arrIdList.Count > 1 AndAlso arrIdList.Contains(m_resLineTable._FieldId) = False Then
                        For Each objField As Object In arrObjectList
                            If TypeOf objField Is TemplateBoxField Then
                                arrIdList.Remove(objField._FieldId)
                                objField.Unselect()
                            End If
                            If TypeOf objField Is TemplateLineField Then
                                arrIdList.Remove(objField._FieldId)
                                objField.Unselect()
                            End If
                        Next
                        arrObjectList.Clear()
                        arrLineObjectList.Clear()
                    End If
                End If

                If arrIdList.Contains(1) = False Then   '''''When multiple controls selected and then select one of them control at that time check array
                    arrIdList.Add(m_resLineTable._FieldId)
                    arrObjectList.Add(m_resLineTable)
                    arrLineObjectList.Add(m_resLineTable)
                End If

                objlblProperty.Text = "Property:" & m_resLineTable._FieldName
                pgTemplateField.SelectedObject = m_resLineTable

                mctrlLine = m_resLineTable

                '*******Right Key Press. For Delete selected Field*******
            ElseIf e.Button = Windows.Forms.MouseButtons.Right Then
                If Not m_resLineTable Is Nothing Then
                    m_resLineTable.Unselect()
                    m_resLineTable = Nothing
                End If

                m_resLineTable = DirectCast(sender, LineControl)
                m_resLineTable.Select()

                objlblProperty.Text = "Property:" & m_resLineTable._FieldName
                pgTemplateField.SelectedObject = m_resLineTable
            Else
                m_resLineTable.Unselect()
                m_resLineTable = Nothing
            End If
            m_resLineTable.Refresh()
        ElseIf TypeOf sender Is ImageControls Then   '''''Image Control

            '******Left and Shift Key Press. Multiple fields select******
            If e.Button = Windows.Forms.MouseButtons.Left AndAlso Control.ModifierKeys = Keys.Control Then

                m_resImageTable = DirectCast(sender, ImageControls)
                m_resImageTable.Select()

                If m_resImageTable.FieldType = 4 Then
                    'm_resImageTable.SendToBack()
                Else
                    m_resImageTable.BringToFront()
                End If

                If arrIdList.Contains(m_resImageTable._FieldId) = False Then
                    arrIdList.Add(m_resImageTable._FieldId)
                    arrObjectList.Add(m_resImageTable)
                Else
                    If Not m_resImageTable Is Nothing Then
                        m_resImageTable.Unselect()
                        arrIdList.Remove(m_resImageTable._FieldId)
                        arrObjectList.Remove(m_resImageTable)
                        m_resImageTable = Nothing
                    End If
                End If

                objlblProperty.Text = "Property:" & ""
                pgTemplateField.SelectedObject = Nothing


                '*******Left Key Press. Only one Field Select****** 
            ElseIf e.Button = Windows.Forms.MouseButtons.Left Then
                If arrIdList.Count = 1 Then                                 '''''For Select control and unselect previous control
                    If Not m_resImageTable Is Nothing Then
                        arrIdList.Remove(m_resImageTable._FieldId)
                        arrObjectList.Remove(m_resImageTable)
                        m_resImageTable.Unselect()
                        m_resImageTable = Nothing
                    End If

                    If Not m_resSetTable Is Nothing Then
                        arrIdList.Remove(m_resSetTable._FieldId)
                        arrObjectList.Remove(m_resSetTable)

                        m_resSetTable.Unselect()
                        m_resSetTable = Nothing
                    End If

                    If Not m_resLineTable Is Nothing Then
                        arrIdList.Remove(m_resLineTable._FieldId)
                        arrObjectList.Remove(m_resLineTable)
                        arrLineObjectList.Remove(m_resLineTable)
                        m_resLineTable.Unselect()
                        m_resLineTable = Nothing
                    End If
                End If

                m_resImageTable = DirectCast(sender, ImageControls)
                m_resImageTable.Select()
                For i As Integer = 0 To cboField.Items.Count - 1
                    If (m_resImageTable._FieldName & m_resImageTable._FieldId) = cboField.Items(i) Then
                        cboField.SelectedItem = cboField.Items(i)
                    End If
                Next

                'If m_resImageTable.FieldType = 4 Then
                m_resImageTable.SendToBack()
                'Else
                '    m_resImageTable.BringToFront()
                'End If


                If arrIdList.Count > 1 AndAlso arrIdList.Contains(m_resImageTable._FieldId) = False Then
                    For Each objField As Object In arrObjectList
                        If TypeOf objField Is TemplateBoxField Then
                            arrIdList.Remove(objField._FieldId)
                            objField.Unselect()
                        End If
                        If TypeOf objField Is TemplateLineField Then
                            arrIdList.Remove(objField._FieldId)
                            objField.Unselect()
                        End If
                    Next
                    arrObjectList.Clear()
                    arrLineObjectList.Clear()
                End If

                If arrIdList.Contains(m_resImageTable._FieldId) = False Then   '''''When multiple controls selected and then select one of them control at that time check array
                    arrIdList.Add(m_resImageTable._FieldId)
                    arrObjectList.Add(m_resImageTable)
                End If

                objlblProperty.Text = "Property:" & m_resImageTable._FieldName
                pgTemplateField.SelectedObject = m_resImageTable

                mctrlImage = m_resImageTable

                '*******Right Key Press. For Delete selected Field*******
            ElseIf e.Button = Windows.Forms.MouseButtons.Right Then
                If Not m_resImageTable Is Nothing Then
                    m_resImageTable.Unselect()
                    m_resImageTable = Nothing
                End If

                m_resImageTable = DirectCast(sender, ImageControls)
                m_resImageTable.Select()
                If m_resImageTable.FieldType = 4 Then
                    'm_resImageTable.SendToBack()
                Else
                    m_resImageTable.BringToFront()
                End If
                'cms.Show(m_resImageTable, e.X, e.Y)
                objlblProperty.Text = "Property:" & m_resImageTable._FieldName
                pgTemplateField.SelectedObject = m_resImageTable

            Else
                m_resImageTable.BackColor = Color.White
                m_resImageTable.Unselect()
                m_resImageTable = Nothing
            End If

        Else   '''''Box Control

            '******Left and Shift Key Press. Multiple fields select******
            If e.Button = Windows.Forms.MouseButtons.Left AndAlso Control.ModifierKeys = Keys.Control Then

                m_resSetTable = DirectCast(sender, BoxControl)
                m_resSetTable.Select()

                If m_resSetTable.FieldType = 4 Then
                    'm_resSetTable.SendToBack()
                Else
                    m_resSetTable.BringToFront()
                End If

                If arrIdList.Contains(m_resSetTable._FieldId) = False Then
                    arrIdList.Add(m_resSetTable._FieldId)
                    arrObjectList.Add(m_resSetTable)
                Else
                    If Not m_resSetTable Is Nothing Then
                        m_resSetTable.Unselect()
                        arrIdList.Remove(m_resSetTable._FieldId)
                        arrObjectList.Remove(m_resSetTable)
                        m_resSetTable = Nothing
                    End If
                End If

                objlblProperty.Text = "Property:" & ""
                pgTemplateField.SelectedObject = Nothing


                '*******Left Key Press. Only one Field Select****** 
            ElseIf e.Button = Windows.Forms.MouseButtons.Left Then
                If arrIdList.Count = 1 Then                                 '''''For Select control and unselect previous control
                    If Not m_resSetTable Is Nothing Then
                        arrIdList.Remove(m_resSetTable._FieldId)
                        arrObjectList.Remove(m_resSetTable)

                        m_resSetTable.Unselect()
                        m_resSetTable = Nothing
                    End If

                    If Not m_resImageTable Is Nothing Then
                        arrIdList.Remove(m_resImageTable._FieldId)
                        arrObjectList.Remove(m_resImageTable)

                        m_resImageTable.Unselect()
                        m_resImageTable = Nothing
                    End If

                    If Not m_resLineTable Is Nothing Then
                        arrIdList.Remove(m_resLineTable._FieldId)
                        arrObjectList.Remove(m_resLineTable)
                        arrLineObjectList.Remove(m_resLineTable)
                        m_resLineTable.Unselect()
                        m_resLineTable = Nothing
                    End If
                End If

                m_resSetTable = DirectCast(sender, BoxControl)
                m_resSetTable.Select()
                For i As Integer = 0 To cboField.Items.Count - 1
                    If (m_resSetTable._FieldName & m_resSetTable._FieldId) = cboField.Items(i) Then
                        cboField.SelectedItem = cboField.Items(i)
                    End If
                Next

                If m_resSetTable.FieldType = 4 Then
                    'm_resSetTable.SendToBack()
                Else
                    m_resSetTable.BringToFront()
                End If

                If arrIdList.Count > 1 AndAlso arrIdList.Contains(m_resSetTable._FieldId) = False Then
                    For Each objField As Object In arrObjectList
                        If TypeOf objField Is TemplateBoxField Then
                            arrIdList.Remove(objField._FieldId)
                            objField.Unselect()
                        End If
                        If TypeOf objField Is TemplateLineField Then
                            arrIdList.Remove(objField._FieldId)
                            objField.Unselect()
                        End If
                        If TypeOf objField Is ImageControls Then
                            arrIdList.Remove(objField._FieldId)
                            objField.Unselect()
                        End If

                    Next
                    arrObjectList.Clear()
                    arrLineObjectList.Clear()
                End If

                If arrIdList.Contains(m_resSetTable._FieldId) = False Then   '''''When multiple controls selected and then select one of them control at that time check array
                    arrIdList.Add(m_resSetTable._FieldId)
                    arrObjectList.Add(m_resSetTable)
                End If

                objlblProperty.Text = "Property:" & m_resSetTable._FieldName
                If m_resSetTable IsNot Nothing AndAlso m_resSetTable IsNot DBNull.Value Then
                    pgTemplateField.SelectedObject = m_resSetTable

                    mctrl = m_resSetTable
                End If
                '*******Right Key Press. For Delete selected Field*******
            ElseIf e.Button = Windows.Forms.MouseButtons.Right Then
                If Not m_resSetTable Is Nothing Then
                    m_resSetTable.Unselect()
                    m_resSetTable = Nothing
                End If

                m_resSetTable = DirectCast(sender, BoxControl)
                m_resSetTable.Select()
                If m_resSetTable.FieldType = 4 Then
                    'm_resSetTable.SendToBack()
                Else
                    m_resSetTable.BringToFront()
                End If
                'cms.Show(m_resSetTable, e.X, e.Y)
                objlblProperty.Text = "Property:" & m_resSetTable._FieldName
                pgTemplateField.SelectedObject = m_resSetTable
            Else
                m_resSetTable.BackColor = Color.White
                m_resSetTable.Unselect()
                m_resSetTable = Nothing
            End If
        End If


        '*****Toolbar Buttons Enabled******
        If arrIdList.Count = 1 Then
            DisabledToolbarButtons()
            edit_Delete_Button.Enabled = True
            edit_Copy_Button.Enabled = True
            edit_Cut_Button.Enabled = False
        ElseIf arrIdList.Count = 0 Then
            DisabledToolbarButtons()
            edit_Delete_Button.Enabled = False
            edit_Copy_Button.Enabled = False
            edit_Cut_Button.Enabled = False
            edit_Paste_Button.Enabled = False
        ElseIf arrIdList.Count > 1 Then
            EnabledToolbarButtons()
            edit_Delete_Button.Enabled = True
            edit_Copy_Button.Enabled = True
            edit_Cut_Button.Enabled = False
        End If

    End Sub

    Private Sub SetProperty(ByVal obj As TemplateBoxField)
        obj.PublicProperties.Add(New TemplateProperty("NamesText", "Name", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Height", "Height", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Width", "Width", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Top", "Top", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Left", "Left", "Appearance"))

        If obj.FieldType = 2 Then
            obj.PublicProperties.Add(New TemplateProperty("Text", "Path", "Appearance"))
        End If

        If obj.FieldType <> 4 Then
            obj.PublicProperties.Add(New TemplateProperty("Font", "Font", "Appearance"))
            obj.PublicProperties.Add(New TemplateProperty("TextAlign", "TextAlign", "Appearance"))
            obj.PublicProperties.Add(New TemplateProperty("ForeColor", "ForeColor", "Appearance"))
            obj.PublicProperties.Add(New TemplateProperty("BackColor", "BackColor", "Appearance"))
        Else
            obj.PublicProperties.Add(New TemplateProperty("LineStyle", "Line Style", "Appearance"))
            obj.PublicProperties.Add(New TemplateProperty("LineThickness", "Line Thickness", "Appearance"))
            obj.PublicProperties.Add(New TemplateProperty("LineColor", "Line Color", "Appearance"))
            obj.PublicProperties.Add(New TemplateProperty("AllR", "All Corner Curve", "Appearance"))
        End If
        obj.PublicProperties.Add(New TemplateProperty("Suppress", "Suppress", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("CanDraw", "CanDraw", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("DupSupp", "Dup.Suppress", "Appearance"))

        'Manish Tanna (12 May 2012) -- Start 
        obj.PublicProperties.Add(New TemplateProperty("Format", "Format", "Appearance"))
        'Manish Tanna (12 May 2012) -- End

        If obj.FieldType = 0 Then
            obj.PublicProperties.Add(New TemplateProperty("_Value", "Caption", "Appearance"))
        End If
    End Sub

    Private Sub SetImageProperty(ByVal obj As TemplateImageField)
        obj.PublicProperties.Add(New TemplateProperty("NamesText", "Name", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Height", "Height", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Width", "Width", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Top", "Top", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Left", "Left", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("Suppress", "Suppress", "Appearance"))
        obj.PublicProperties.Add(New TemplateProperty("FilePath", "Path", "Appearance"))
    End Sub
    Private Sub SetLineProperty(ByVal objLine As TemplateLineField)
        objLine.PublicProperties.Add(New TemplateProperty("NamesText", "Name", "Appearance"))
        objLine.PublicProperties.Add(New TemplateProperty("Top", "Top", "Appearance"))
        objLine.PublicProperties.Add(New TemplateProperty("Left", "Left", "Appearance"))

        objLine.PublicProperties.Add(New TemplateProperty("Vertical", "Vertical", "Appearance"))
        objLine.PublicProperties.Add(New TemplateProperty("LineStyle", "Line Style", "Appearance"))
        objLine.PublicProperties.Add(New TemplateProperty("LineThickness", "Line Thickness", "Appearance"))
        objLine.PublicProperties.Add(New TemplateProperty("LineColor", "Line Color", "Appearance"))
        objLine.PublicProperties.Add(New TemplateProperty("Suppress", "Suppress", "Appearance"))
    End Sub

    Private Sub DisabledToolbarButtons()
        format_Align_Bottom_Button.Enabled = False
        format_Align_Left.Enabled = False
        format_Align_Right_Button.Enabled = False
        format_Align_Top_Button.Enabled = False
        format_Halign_Button.Enabled = False
        format_Same_Height_Button.Enabled = False
        format_Same_Size_Button.Enabled = False
        format_Same_Width_Button.Enabled = False
        format_Valign_Button.Enabled = False
    End Sub

    Private Sub EnabledToolbarButtons()
        format_Align_Bottom_Button.Enabled = True
        format_Align_Left.Enabled = True
        format_Align_Right_Button.Enabled = True
        format_Align_Top_Button.Enabled = True
        format_Halign_Button.Enabled = True
        format_Same_Height_Button.Enabled = True
        format_Same_Size_Button.Enabled = True
        format_Same_Width_Button.Enabled = True
        format_Valign_Button.Enabled = True
    End Sub
    Private Sub setField(ByVal objField As TemplateBoxField, ByVal intSection As Integer, ByVal intFieldType As String)
        Try

        Catch ex As Exception
            MessageBox.Show(ex.Message, "setField")
        End Try
    End Sub

    Private Sub tvControls_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles tvControls.ItemDrag
        If e.Button = Windows.Forms.MouseButtons.Right Then Exit Sub
        tnSelectedNode = CType(e.Item, TreeNode)
        tvControls.SelectedNode = tnSelectedNode
        DoDragDrop(e.Item, DragDropEffects.Move)
    End Sub
#Region "Select controls on mouse click and move"
    '****Select controls when mouse click and move mouse*************
    Private Sub pnlHeader_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlDetail.MouseDown _
                                     , pnlDetailFooter.MouseDown _
                                     , pnlHeader.MouseDown _
                                     , pnlFooter.MouseDown, pnlGroup1.MouseDown, pnlGroupFooter1.MouseDown, pnlCompHeader.MouseDown
        'intMouseX = e.X
        'intMouseY = e.Y

        If e.Button = Windows.Forms.MouseButtons.Left Then
            Dim container As Panel = CType(sender, Panel)
            Me.m_is_selecting = True
            Me.m_start_selection = container.PointToScreen(New Point(e.X, e.Y))
            Me.m_last_selection_rectangle = New System.Drawing.Rectangle(Me.m_start_selection, New System.Drawing.Size())
        End If

        'If blnBox = True Or blnLine = True Then
        '    Cursor.Current = Cursors.Cross
        'End If

    End Sub

    Private Sub pnlHeader_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlDetail.MouseMove _
                                     , pnlDetailFooter.MouseMove _
                                     , pnlHeader.MouseMove _
                                     , pnlFooter.MouseMove, pnlGroup1.MouseMove, pnlGroupFooter1.MouseMove, pnlCompHeader.MouseMove

        If e.Button = Windows.Forms.MouseButtons.Left Then
            Dim container As Panel = CType(sender, Panel)
            If (Me.m_is_selecting) Then
                ControlPaint.DrawReversibleFrame(Me.m_last_selection_rectangle, Color.DarkBlue, FrameStyle.Dashed)
                Dim act_pos As Point = container.PointToScreen(New Point(e.X, e.Y))
                Me.m_last_selection_rectangle = New System.Drawing.Rectangle(Me.m_start_selection, New System.Drawing.Size(act_pos.X - Me.m_start_selection.X, act_pos.Y - Me.m_start_selection.Y))
                ControlPaint.DrawReversibleFrame(Me.m_last_selection_rectangle, Color.DarkBlue, FrameStyle.Dashed)
            End If
        End If
        mouseCurrentPoisiton = New Point(e.X, e.Y)
        pnlRuler.Invalidate()
        'AddHandler pnlRuler.Paint, AddressOf pnlRuler_Paint
        'pnlRuler.Refresh()
        'Invalidate()
    End Sub

    Private Sub DoMouseUp(ByVal container As Panel)
        Dim selection_rect As Rectangle = container.RectangleToClient(Me.m_last_selection_rectangle)
        For Each objField As Object In container.Controls
            If TypeOf objField Is TemplateBoxField Then
                If (selection_rect.IntersectsWith(New Rectangle(objField.Location, objField.Size))) Then
                    objField.Select()
                    arrIdList.Add(objField._FieldId)
                    arrObjectList.Add(objField)
                    If arrObjectList.Count > 1 Then
                        EnabledToolbarButtons()
                    End If
                    edit_Copy_Button.Enabled = True
                    edit_Cut_Button.Enabled = False

                    mctrl = objField
                End If
            End If
            If TypeOf objField Is TemplateImageField Then
                If (selection_rect.IntersectsWith(New Rectangle(objField.Location, objField.Size))) Then
                    objField.Select()
                    arrIdList.Add(objField._FieldId)
                    arrObjectList.Add(objField)
                    If arrObjectList.Count > 1 Then
                        EnabledToolbarButtons()
                    End If
                    edit_Copy_Button.Enabled = True
                    edit_Cut_Button.Enabled = False
                    mctrlImage = objField
                End If
            End If
            If TypeOf objField Is TemplateLineField Then
                If (selection_rect.IntersectsWith(New Rectangle(objField.Location, objField.Size))) Then
                    objField.Select()
                    arrIdList.Add(objField._FieldId)
                    arrObjectList.Add(objField)
                    arrLineObjectList.Add(objField)
                    If arrLineObjectList.Count > 1 Then
                        EnabledToolbarButtons()
                    End If
                    edit_Copy_Button.Enabled = True
                    edit_Cut_Button.Enabled = False

                    mctrlLine = objField
                End If
            End If
        Next
    End Sub

#End Region

#Region "Field on Mouse Click"

    '*******For insert field at location when mouse click.***********
    Private Sub pnlHeader_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlDetail.MouseUp _
                                     , pnlDetailFooter.MouseUp _
                                     , pnlHeader.MouseUp _
                                     , pnlFooter.MouseUp, pnlGroup1.MouseUp, pnlGroupFooter1.MouseUp, pnlCompHeader.MouseUp

        If e.Button = Windows.Forms.MouseButtons.Right Then
            mintX = e.X
            mintY = e.Y
        End If


        If e.Button = Windows.Forms.MouseButtons.Right Then Exit Sub

        If e.Button = Windows.Forms.MouseButtons.Left Then
            Dim container As Panel = CType(sender, Panel)
            ControlPaint.DrawReversibleFrame(Me.m_last_selection_rectangle, Color.DarkBlue, FrameStyle.Dashed)

            If (Me.m_last_selection_rectangle.Width < 0) Then
                Me.m_last_selection_rectangle.X += Me.m_last_selection_rectangle.Width
                Me.m_last_selection_rectangle.Width = -Me.m_last_selection_rectangle.Width
            End If

            If (Me.m_last_selection_rectangle.Height < 0) Then
                Me.m_last_selection_rectangle.Y += Me.m_last_selection_rectangle.Height
                Me.m_last_selection_rectangle.Height = -Me.m_last_selection_rectangle.Height
            End If

            Me.DoMouseUp(container)
            Me.m_is_selecting = False

            '*****Toolbar Buttons Enabled******
            If arrIdList.Count = 1 Then
                DisabledToolbarButtons()
                edit_Delete_Button.Enabled = True
            ElseIf arrIdList.Count = 0 Then
                DisabledToolbarButtons()
                edit_Delete_Button.Enabled = False
            ElseIf arrIdList.Count > 1 Then
                EnabledToolbarButtons()
                edit_Delete_Button.Enabled = True
            End If
        End If
        MyBase.Refresh()
    End Sub
#End Region

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lSave As Boolean = True
            If cmbType.Text.Trim = "" Then MessageBox.Show("Print Type could not be empty", "eZee Message") : cmbType.Focus() : Exit Sub
            lSave = Validation() 'All pnl are chk
            If lSave Then lSave = TypeValidating(cmbType)
            If lSave Then
                Dim nCnt As Integer
                'If txtMstXMLPath.Text <> "" Then
                '    'Deleting Master XML Formulas
                '    If dsMstXML.Tables.Count > 0 Then
                '        For i As Integer = 0 To dsMstXML.Tables.Count - 1
                '            For k As Integer = dsMstXML.Tables(i).Columns.Count - 1 To 0 Step -1
                '                If dsMstXML.Tables(i).Columns(k).Caption Like ".Formula_*" Then dsMstXML.Tables(i).Columns.Remove(dsMstXML.Tables(i).Columns(k))
                '            Next
                '        Next
                '    End If
                '    'Inserting Master XML Formulas
                '    For i As Int16 = 0 To tvControls.Nodes.Count - 1
                '        For j As Int16 = 0 To tvControls.Nodes(i).Nodes.Count - 1
                '            If tvControls.Nodes(i).Text = "2.Master" AndAlso tvControls.Nodes(i).Nodes(j).Text Like ".Formula_*" Then
                '                dsMstXML.Tables(0).Columns.Add(tvControls.Nodes(i).Nodes(j).Text)
                '                If dsMstXML.Tables(0).Rows.Count = 0 Then dsMstXML.Tables(0).Rows.Add()
                '                dsMstXML.Tables(0).Rows(0)(tvControls.Nodes(i).Nodes(j).Text) = tvControls.Nodes(i).Nodes(j).ToolTipText
                '            ElseIf tvControls.Nodes(i).Text = "3.Detail" AndAlso tvControls.Nodes(i).Nodes(j).Text Like ".Formula_*" Then
                '                dsMstXML.Tables(1).Columns.Add(tvControls.Nodes(i).Nodes(j).Text)
                '                If dsMstXML.Tables(1).Rows.Count = 0 Then dsMstXML.Tables(1).Rows.Add()
                '                dsMstXML.Tables(1).Rows(0)(tvControls.Nodes(i).Nodes(j).Text) = tvControls.Nodes(i).Nodes(j).Tag
                '            End If

                '        Next
                '    Next
                '    dsMstXML.WriteXml(txtMstXMLPath.Text, XmlWriteMode.WriteSchema)
                'End If


                Dim dsMst As New DataSet
                dsMst.ReadXml(sMst)
                If cmbType.Tag IsNot Nothing Then
                    If dsMst.Tables(0).Select("trnasid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'").Length > 0 Then
                        dsMst.Tables(0).Rows.Remove(dsMst.Tables(0).Select("trnasid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'")(0))
                    End If
                End If
                With dsMst.Tables(0)

                    If .Rows.Count > 0 Then
                        .Rows.Add()
                        If Not chkSaveAsNew.Checked AndAlso (cmbType.Tag <> 0) Then
                            nCnt = cmbType.Tag
                        Else
                            nCnt = .Compute("max(trnasid)", "") + 1
                        End If
                    Else
                        .Rows.Add()
                        nCnt = .Rows.Count + 1
                    End If
                    .Rows(.Rows.Count - 1)("trnasid") = nCnt
                    .Rows(.Rows.Count - 1)("PrintType") = cmbType.Text
                    .Rows(.Rows.Count - 1)("ItemPrint") = IIf(txtItemPrint.Text = "", 0, txtItemPrint.Text)
                    .Rows(.Rows.Count - 1)("MstXMLPath") = txtMstXMLPath.Text
                    .Rows(.Rows.Count - 1)("DetXMLPath") = txtDetXMLPath.Text
                    .Rows(.Rows.Count - 1)("HeightCompHeader") = pnlCompHeader.Height
                    .Rows(.Rows.Count - 1)("HeightHeader") = pnlHeader.Height
                    .Rows(.Rows.Count - 1)("HeightDetail") = pnlDetail.Height
                    .Rows(.Rows.Count - 1)("HeightDetailFooter") = pnlDetailFooter.Height
                    .Rows(.Rows.Count - 1)("HeightFooter") = pnlFooter.Height
                    .Rows(.Rows.Count - 1)("HeightGroup1") = pnlGroup1.Height
                    .Rows(.Rows.Count - 1)("HeightGroupFooter1") = pnlGroupFooter1.Height
                    .Rows(.Rows.Count - 1)("Width") = pnlCanvas.Width
                    .Rows(.Rows.Count - 1)("SuppressHeader") = lSuppreHeaderPnl
                    .Rows(.Rows.Count - 1)("SuppressGroup1") = lSuppreGroup1Pnl
                    .Rows(.Rows.Count - 1)("SuppressDetail") = lSuppreDetPnl
                    .Rows(.Rows.Count - 1)("SuppressGroupFooter1") = lSuppreGroupFooter1Pnl
                    .Rows(.Rows.Count - 1)("SuppressDetailFooter") = lSuppreDetFotPnl
                    .Rows(.Rows.Count - 1)("SuppressFooter") = lSuppreFooterPnl
                    .Rows(.Rows.Count - 1)("SuppressCompHeader") = lSuppreCompHeaderPnl
                    .Rows(.Rows.Count - 1)("CanvasWidth") = txtCanvasWidth.Text
                    .Rows(.Rows.Count - 1)("Landspace") = chkLandspace.Checked
                    .Rows(.Rows.Count - 1)("RepHeader") = chkRepHeader.Checked
                    .Rows(.Rows.Count - 1)("MultPrint") = txtMultPrint.Text
                    .Rows(.Rows.Count - 1)("CanvasHeight") = CanvasHeight
                    .Rows(.Rows.Count - 1)("Top") = nTop
                    .Rows(.Rows.Count - 1)("Left") = nLeft
                    .Rows(.Rows.Count - 1)("Right") = nRight
                    .Rows(.Rows.Count - 1)("Bottom") = nBottom
                    .Rows(.Rows.Count - 1)("PaperSize") = sPaperSize
                    .Rows(.Rows.Count - 1)("PageWise") = chkPageWise.Checked
                End With
                dsMst.WriteXml(sMst, XmlWriteMode.WriteSchema)

                Dim dsFormula As New DataSet
                dsFormula.ReadXml(sFormula)
                With dsFormula.Tables(0)
                    If cmbType.Tag IsNot Nothing Then
                        If dsFormula.Tables(0).Select("typeid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'").Length > 0 Then
                            For i As Integer = dsFormula.Tables(0).Select("typeid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'").Length - 1 To 0 Step -1
                                dsFormula.Tables(0).Rows.Remove(dsFormula.Tables(0).Select("typeid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'")(i))
                            Next
                        End If
                    End If
                    dsFormula.WriteXml(sFormula, XmlWriteMode.WriteSchema)

                    For i As Int16 = 0 To tvControls.Nodes.Count - 1
                        For j As Int16 = 0 To tvControls.Nodes(i).Nodes.Count - 1
                            If tvControls.Nodes(i).Text = "2.Master" AndAlso tvControls.Nodes(i).Nodes(j).Text Like ".Formula_*" Then
                                .Rows.Add()
                                .Rows(.Rows.Count - 1)("TrnasId") = .Rows.Count + 1
                                .Rows(.Rows.Count - 1)("TypeId") = nCnt
                                .Rows(.Rows.Count - 1)("Form") = tvControls.Nodes(i).Text
                                .Rows(.Rows.Count - 1)("FormulaName") = tvControls.Nodes(i).Nodes(j).Text
                                .Rows(.Rows.Count - 1)("FormulaExp") = tvControls.Nodes(i).Nodes(j).ToolTipText
                                dsFormula.WriteXml(sFormula, XmlWriteMode.WriteSchema)
                            ElseIf tvControls.Nodes(i).Text = "3.Detail" AndAlso tvControls.Nodes(i).Nodes(j).Text Like ".Formula_*" Then
                                .Rows.Add()
                                .Rows(.Rows.Count - 1)("TrnasId") = .Rows.Count + 1
                                .Rows(.Rows.Count - 1)("TypeId") = nCnt
                                .Rows(.Rows.Count - 1)("Form") = tvControls.Nodes(i).Text
                                .Rows(.Rows.Count - 1)("FormulaName") = tvControls.Nodes(i).Nodes(j).Text
                                .Rows(.Rows.Count - 1)("FormulaExp") = tvControls.Nodes(i).Nodes(j).ToolTipText
                                dsFormula.WriteXml(sFormula, XmlWriteMode.WriteSchema)
                            ElseIf tvControls.Nodes(i).Text = "Group" AndAlso tvControls.Nodes(i).Nodes.Count > 0 Then
                                .Rows.Add()
                                .Rows(.Rows.Count - 1)("TrnasId") = .Rows.Count + 1
                                .Rows(.Rows.Count - 1)("TypeId") = nCnt
                                .Rows(.Rows.Count - 1)("Form") = tvControls.Nodes(i).Text
                                .Rows(.Rows.Count - 1)("FormulaName") = tvControls.Nodes(i).Nodes(j).Text
                                .Rows(.Rows.Count - 1)("FormulaExp") = tvControls.Nodes(i).Nodes(j).ToolTipText
                                dsFormula.WriteXml(sFormula, XmlWriteMode.WriteSchema)
                            End If
                        Next
                    Next
                End With

                'pnlHeader
                Dim dsDet As New DataSet
                dsDet.ReadXml(sDet)
                With dsDet.Tables(0)
                    If cmbType.Tag IsNot Nothing Then
                        If dsDet.Tables(0).Select("typeid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'").Length > 0 Then
                            For i As Integer = dsDet.Tables(0).Select("typeid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'").Length - 1 To 0 Step -1
                                dsDet.Tables(0).Rows.Remove(dsDet.Tables(0).Select("typeid = '" & IIf(chkSaveAsNew.Checked, "0", cmbType.Tag) & "'")(i))
                            Next
                            If aImagePath IsNot Nothing AndAlso aImagePath.Length <> 0 Then
                                For i As Int16 = 0 To aImagePath.Length - 1
                                    If aImagePath(i) <> "" Then System.IO.File.Delete(aImagePath(i))
                                Next
                                ReDim aImagePath(0)
                            End If
                        End If
                    End If

                    If pnlHeader.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlHeader.Controls.Count - 1
                            Saving(pnlHeader, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With
                'pnlCompHeader
                With dsDet.Tables(0)
                    If pnlCompHeader.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlCompHeader.Controls.Count - 1
                            Saving(pnlCompHeader, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With

                'pnlGroup1
                With dsDet.Tables(0)
                    If pnlGroup1.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlGroup1.Controls.Count - 1
                            Saving(pnlGroup1, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With


                'pnlDetail
                With dsDet.Tables(0)
                    If pnlDetail.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlDetail.Controls.Count - 1
                            Saving(pnlDetail, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With

                'pnlGroupFooter1
                With dsDet.Tables(0)
                    If pnlGroupFooter1.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlGroupFooter1.Controls.Count - 1
                            Saving(pnlGroupFooter1, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With


                'pnlDetailFooter
                With dsDet.Tables(0)
                    If pnlDetailFooter.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlDetailFooter.Controls.Count - 1
                            Saving(pnlDetailFooter, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With

                'pnlFooter
                With dsDet.Tables(0)
                    If pnlFooter.Controls.Count > 0 Then
                        For i As Integer = 0 To pnlFooter.Controls.Count - 1
                            Saving(pnlFooter, dsDet.Tables(0), i, nCnt)
                        Next
                    End If
                End With
                dsDet.WriteXml(sDet, XmlWriteMode.WriteSchema)

                'CancelIt()
                cmbType.Tag = nCnt
                chkSaveAsNew.Checked = False
                MessageBox.Show("Data are saved", "eZee Message")
            Else
                MessageBox.Show("No Data are added", "eZee Message")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
    Private Sub Saving(ByVal sender As Object, ByVal dt As DataTable, ByVal i As Integer, ByVal Id As Integer)
        With dt
            .Rows.Add()
            If sender.Controls(i).Text = "Line" Then
                .Rows(.Rows.Count - 1)("TrnasId") = .Rows.Count + 1
                .Rows(.Rows.Count - 1)("TypeId") = Id
                .Rows(.Rows.Count - 1)("FieldId") = CType(sender.Controls(i), TemplateLineField)._FieldId
                .Rows(.Rows.Count - 1)("SectionId") = CType(sender.Controls(i), TemplateLineField)._SectionId
                .Rows(.Rows.Count - 1)("Tag") = CType(sender.Controls(i), TemplateLineField).Tag
                .Rows(.Rows.Count - 1)("Name") = CType(sender.Controls(i), TemplateLineField).Name
                .Rows(.Rows.Count - 1)("TextAlign") = CType(sender.Controls(i), TemplateLineField).TextAlign
                .Rows(.Rows.Count - 1)("Font") = CType(sender.Controls(i), TemplateLineField).Font.Name
                .Rows(.Rows.Count - 1)("Size") = CType(sender.Controls(i), TemplateLineField).Font.Size
                .Rows(.Rows.Count - 1)("Style") = CType(sender.Controls(i), TemplateLineField).Font.Style
                .Rows(.Rows.Count - 1)("ForeColor") = CType(sender.Controls(i), TemplateLineField).ForeColor.Name
                .Rows(.Rows.Count - 1)("BackColor") = CType(sender.Controls(i), TemplateLineField).BackColor.Name
                .Rows(.Rows.Count - 1)("Height") = CType(sender.Controls(i), TemplateLineField).Height
                .Rows(.Rows.Count - 1)("Width") = CType(sender.Controls(i), TemplateLineField).Width
                .Rows(.Rows.Count - 1)("X") = CType(sender.Controls(i), TemplateLineField).Left 'X
                .Rows(.Rows.Count - 1)("Y") = CType(sender.Controls(i), TemplateLineField).Top 'Y
                .Rows(.Rows.Count - 1)("FieldType") = CType(sender.Controls(i), TemplateLineField).FieldType
                .Rows(.Rows.Count - 1)("FieldName") = CType(sender.Controls(i), TemplateLineField)._FieldName
                .Rows(.Rows.Count - 1)("Value") = CType(sender.Controls(i), TemplateLineField)._Value
                .Rows(.Rows.Count - 1)("Vertical") = CType(sender.Controls(i), TemplateLineField).Vertical
                .Rows(.Rows.Count - 1)("LineStyle") = CType(sender.Controls(i), TemplateLineField).LineStyle
                .Rows(.Rows.Count - 1)("LineThickness") = CType(sender.Controls(i), TemplateLineField).LineThickness
                .Rows(.Rows.Count - 1)("LineColor") = CType(sender.Controls(i), TemplateLineField).LineColor.Name
                .Rows(.Rows.Count - 1)("Text") = CType(sender.Controls(i), TemplateLineField).Text
                .Rows(.Rows.Count - 1)("Suppress") = CType(sender.Controls(i), TemplateLineField).Suppress
                .Rows(.Rows.Count - 1)("panel") = CType(sender, Panel).Name
            ElseIf sender.Controls(i).Name = "Image" Then
                .Rows(.Rows.Count - 1)("TrnasId") = .Rows.Count + 1
                .Rows(.Rows.Count - 1)("TypeId") = Id
                .Rows(.Rows.Count - 1)("FieldId") = CType(sender.Controls(i), TemplateImageField)._FieldId
                .Rows(.Rows.Count - 1)("SectionId") = CType(sender.Controls(i), TemplateImageField)._SectionId
                .Rows(.Rows.Count - 1)("Tag") = CType(sender.Controls(i), TemplateImageField).Tag
                .Rows(.Rows.Count - 1)("Name") = CType(sender.Controls(i), TemplateImageField).Name
                .Rows(.Rows.Count - 1)("TextAlign") = CType(sender.Controls(i), TemplateImageField).TextAlign
                .Rows(.Rows.Count - 1)("Font") = CType(sender.Controls(i), TemplateImageField).Font.Name
                .Rows(.Rows.Count - 1)("Size") = CType(sender.Controls(i), TemplateImageField).Font.Size
                .Rows(.Rows.Count - 1)("Style") = CType(sender.Controls(i), TemplateImageField).Font.Style
                .Rows(.Rows.Count - 1)("ForeColor") = CType(sender.Controls(i), TemplateImageField).ForeColor.Name
                .Rows(.Rows.Count - 1)("BackColor") = CType(sender.Controls(i), TemplateImageField).BackColor.Name
                .Rows(.Rows.Count - 1)("Height") = CType(sender.Controls(i), TemplateImageField).Height
                .Rows(.Rows.Count - 1)("Width") = CType(sender.Controls(i), TemplateImageField).Width
                .Rows(.Rows.Count - 1)("X") = CType(sender.Controls(i), TemplateImageField).Left 'X
                .Rows(.Rows.Count - 1)("Y") = CType(sender.Controls(i), TemplateImageField).Top 'Y
                .Rows(.Rows.Count - 1)("FieldType") = CType(sender.Controls(i), TemplateImageField).FieldType
                .Rows(.Rows.Count - 1)("FieldName") = CType(sender.Controls(i), TemplateImageField)._FieldName
                .Rows(.Rows.Count - 1)("Value") = CType(sender.Controls(i), TemplateImageField)._Value
                .Rows(.Rows.Count - 1)("LineStyle") = CType(sender.Controls(i), TemplateImageField).LineStyle
                .Rows(.Rows.Count - 1)("LineThickness") = CType(sender.Controls(i), TemplateImageField).LineThickness
                .Rows(.Rows.Count - 1)("LineColor") = CType(sender.Controls(i), TemplateImageField).LineColor.Name
                .Rows(.Rows.Count - 1)("Text") = CType(sender.Controls(i), TemplateImageField).Text
                .Rows(.Rows.Count - 1)("AllR") = CType(sender.Controls(i), TemplateImageField).AllR
                .Rows(.Rows.Count - 1)("Suppress") = CType(sender.Controls(i), TemplateImageField).Suppress
                Dim ms As New IO.MemoryStream()
                CType(sender.Controls(i), TemplateImageField).BackgroundImage.Save(ms, Imaging.ImageFormat.Jpeg)
                Dim arrImage() As Byte = ms.GetBuffer
                ms.Close()
                ms.Dispose()
                .Rows(.Rows.Count - 1)("Picture") = arrImage
                .Rows(.Rows.Count - 1)("panel") = CType(sender, Panel).Name
            Else
                .Rows(.Rows.Count - 1)("TrnasId") = .Rows.Count + 1
                .Rows(.Rows.Count - 1)("TypeId") = Id
                .Rows(.Rows.Count - 1)("FieldId") = CType(sender.Controls(i), TemplateBoxField)._FieldId
                .Rows(.Rows.Count - 1)("SectionId") = CType(sender.Controls(i), TemplateBoxField)._SectionId
                .Rows(.Rows.Count - 1)("Tag") = CType(sender.Controls(i), TemplateBoxField).Tag
                .Rows(.Rows.Count - 1)("Name") = CType(sender.Controls(i), TemplateBoxField).Name
                .Rows(.Rows.Count - 1)("TextAlign") = CType(sender.Controls(i), TemplateBoxField).TextAlign
                .Rows(.Rows.Count - 1)("Font") = CType(sender.Controls(i), TemplateBoxField).Font.Name
                .Rows(.Rows.Count - 1)("Size") = CType(sender.Controls(i), TemplateBoxField).Font.Size
                .Rows(.Rows.Count - 1)("Style") = CType(sender.Controls(i), TemplateBoxField).Font.Style
                .Rows(.Rows.Count - 1)("ForeColor") = CType(sender.Controls(i), TemplateBoxField).ForeColor.Name
                .Rows(.Rows.Count - 1)("BackColor") = CType(sender.Controls(i), TemplateBoxField).BackColor.Name
                .Rows(.Rows.Count - 1)("Height") = CType(sender.Controls(i), TemplateBoxField).Height
                .Rows(.Rows.Count - 1)("Width") = CType(sender.Controls(i), TemplateBoxField).Width
                .Rows(.Rows.Count - 1)("X") = CType(sender.Controls(i), TemplateBoxField).Left 'X
                .Rows(.Rows.Count - 1)("Y") = CType(sender.Controls(i), TemplateBoxField).Top 'Y
                .Rows(.Rows.Count - 1)("FieldType") = CType(sender.Controls(i), TemplateBoxField).FieldType
                .Rows(.Rows.Count - 1)("FieldName") = CType(sender.Controls(i), TemplateBoxField)._FieldName
                .Rows(.Rows.Count - 1)("Value") = CType(sender.Controls(i), TemplateBoxField)._Value
                .Rows(.Rows.Count - 1)("LineStyle") = CType(sender.Controls(i), TemplateBoxField).LineStyle
                .Rows(.Rows.Count - 1)("LineThickness") = CType(sender.Controls(i), TemplateBoxField).LineThickness
                .Rows(.Rows.Count - 1)("LineColor") = CType(sender.Controls(i), TemplateBoxField).LineColor.Name
                .Rows(.Rows.Count - 1)("Text") = CType(sender.Controls(i), TemplateBoxField).Text
                .Rows(.Rows.Count - 1)("AllR") = CType(sender.Controls(i), TemplateBoxField).AllR
                .Rows(.Rows.Count - 1)("Suppress") = CType(sender.Controls(i), TemplateBoxField).Suppress
                .Rows(.Rows.Count - 1)("CanDraw") = CType(sender.Controls(i), TemplateBoxField).CanDraw
                .Rows(.Rows.Count - 1)("DupSupp") = CType(sender.Controls(i), TemplateBoxField).DupSupp
                'Manish Tanna (12 May 2012) -- start 
                .Rows(.Rows.Count - 1)("Format") = CType(sender.Controls(i), TemplateBoxField).Format
                'Manish Tanna (12 May 2012) -- end
                .Rows(.Rows.Count - 1)("panel") = CType(sender, Panel).Name
            End If
        End With
    End Sub
    Private Function Validation()
        Dim lRetVal As Boolean = True
        If pnlCompHeader.Controls.Count = 0 AndAlso pnlHeader.Controls.Count = 0 AndAlso pnlDetail.Controls.Count = 0 AndAlso pnlDetailFooter.Controls.Count = 0 AndAlso pnlFooter.Controls.Count = 0 AndAlso pnlGroup1.Controls.Count = 0 AndAlso pnlGroupFooter1.Controls.Count = 0 Then
            lRetVal = False
        End If
        Return lRetVal
    End Function
#Region "Alignment through ToolbarButtons"
    Private Sub tb_Designer_ToolBar_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tb_Designer_ToolBar.ButtonClick
        Select Case tb_Designer_ToolBar.Buttons.IndexOf(e.Button)
            Case 0  'Delete Selected items
                DeleteAllFields()

            Case 2   'Left Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Left = mctrl.Left
                        End If
                    End If

                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Left = mctrlImage.Left
                        End If
                    End If

                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Left = mctrlLine.Left
                        End If
                    End If
                Next
            Case 3   'V Align Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Left = mctrl.Left + (mctrl.Width * 0.5F) - (objField.Width * 0.5F)
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Left = mctrlImage.Left + (mctrl.Width * 0.5F) - (objField.Width * 0.5F)
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Left = mctrlLine.Left + (mctrlLine.Width * 0.5F) - (objField.Width * 0.5F)
                        End If
                    End If
                Next

            Case 4   'Right Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Left = mctrl.Left + mctrl.Width - objField.Width
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Left = mctrlImage.Left + mctrl.Width - objField.Width
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Left = mctrlLine.Left + mctrlLine.Width - objField.Width
                        End If
                    End If
                Next

            Case 6   'Top Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Top = mctrl.Top
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Top = mctrlImage.Top
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Top = mctrlLine.Top
                        End If
                    End If
                Next

            Case 7   'H Align Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Top = mctrl.Top + (mctrl.Height * 0.5F) - (objField.Height * 0.5F)
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Top = mctrlImage.Top + (mctrl.Height * 0.5F) - (objField.Height * 0.5F)
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Top = mctrlLine.Top + (mctrlLine.Height * 0.5F) - (objField.Height * 0.5F)
                        End If
                    End If
                Next

            Case 8   'Bottom Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Top = mctrl.Top + mctrl.Height - objField.Height
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Top = mctrlImage.Top + mctrl.Height - objField.Height
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Top = mctrlLine.Top + mctrlLine.Height - objField.Height
                        End If
                    End If
                Next

            Case 10   'Same Width Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Width = mctrl.Width
                            objField.Refresh()
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Width = mctrlImage.Width
                            objField.Refresh()
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Width = mctrlLine.Width
                            objField.Refresh()
                        End If
                    End If
                Next

            Case 11   'Same Height Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Height = mctrl.Height
                            objField.Refresh()
                        End If
                    End If

                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Height = mctrlImage.Height
                            objField.Refresh()
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Height = mctrlLine.Height
                            objField.Refresh()
                        End If
                    End If
                Next

            Case 12   'Same Size Button
                For Each objField As Object In arrObjectList
                    If TypeOf objField Is TemplateBoxField Then
                        If objField._FieldId <> mctrl._FieldId Then
                            objField.Width = mctrl.Width
                            objField.Height = mctrl.Height
                            objField.Refresh()
                        End If
                    End If
                    If TypeOf objField Is TemplateImageField Then
                        If objField._FieldId <> mctrlImage._FieldId Then
                            objField.Width = mctrlImage.Width
                            objField.Height = mctrlImage.Height
                            objField.Refresh()
                        End If
                    End If
                    If TypeOf objField Is TemplateLineField Then
                        If objField._FieldId <> mctrlLine._FieldId Then
                            objField.Width = mctrlLine.Width
                            objField.Height = mctrlLine.Height
                            objField.Refresh()
                        End If
                    End If
                Next
            Case 14  'Cut Selected items
            Case 15  'Copy Selected items
                CopyValue()
            Case 16  'Paste Selected items
                Pastevalue()
            Case 18
                'blnBox = True
            Case 19
                'blnLine = True
        End Select
    End Sub
    Private Sub CopyValue()
        arrCopyCutList = arrObjectList
        'blnCut = False
        blnCopy = True
        edit_Paste_Button.Enabled = True
    End Sub
    Private Sub Pastevalue()
        ''Paste from COPY..................
        If blnCopy = True Then
            For Each objFields As Object In arrCopyCutList
                If TypeOf objFields Is TemplateBoxField Then
                    Dim objPasteField As New TemplateBoxField
                    'objPasteField = objFields
                    objPasteField._FieldId = 0

                    objPasteField._SectionId = objFields._Sectionid

                    objPasteField.Tag = objFields.Tag
                    objPasteField.Name = objFields._FieldUnkID
                    objPasteField.TextAlign = objFields.TextAlign
                    objPasteField.Font = New Font(CType(objFields.Font.Name, String), CType(objFields.Font.Size, Single), CType(objFields.Font.Style, FontStyle))
                    objPasteField.BackColor = objFields.BackColor
                    objPasteField.ForeColor = objFields.ForeColor
                    objPasteField.Height = objFields.Height
                    objPasteField.Width = objFields.Width

                    objPasteField.FieldType = objFields.FieldType
                    'objPOSTemplate.Field._FieldName = objPOSTemplate.Field._FieldName & intId
                    objPasteField._FieldName = objFields._FieldName
                    objPasteField._Value = objFields._Value

                    objPasteField.LineStyle = objFields.LineStyle
                    objPasteField.LineThickness = objFields.LineThickness
                    objPasteField.LineColor = objFields.LineColor
                    objPasteField.Text = objFields.Text
                    objPasteField.Name = objFields.Name
                    objPasteField.LineColor = objFields.LineColor
                    objPasteField.AllR = objFields.AllR

                    'If blnCopy = True Then
                    mintX = objFields.Left + 6
                    mintY = objFields.Top + 6

                    'End If

                    Select Case objPasteField._SectionId
                        Case 1
                            setField(objPasteField, 1, objPasteField.FieldType)
                        Case 2
                            setField(objPasteField, 2, objPasteField.FieldType)
                        Case 3
                            setField(objPasteField, 3, objPasteField.FieldType)
                        Case 4
                            setField(objPasteField, 4, objPasteField.FieldType)
                        Case 5
                            setField(objPasteField, 5, objPasteField.FieldType)
                    End Select

                    'objPOSTemplate.Field._FieldName.Substring(objPOSTemplate.Field._FieldName, 1)
                    If objFields.Parent IsNot Nothing Then
                        CreateField(objPasteField, IIf(objFields.Parent.Name = "pnlCompHeader", 0, IIf(objFields.Parent.Name = "pnlHeader", 1, IIf(objFields.Parent.Name = "pnlDetail", 3, IIf(objFields.Parent.Name = "pnlDetailFooter", 4, IIf(objFields.Parent.Name = "pnlFooter", 5, IIf(objFields.Parent.Name = "pnlGroup1", 6, 7)))))), "B")
                    End If
                    objFields.Unselect()
                End If
                'Image Controls
                If TypeOf objFields Is TemplateImageField Then
                    'If blnCopy = True Then
                    Dim objPasteImageField As New TemplateImageField
                    objPasteImageField._FieldId = 0
                    mintX = objFields.Left + 6
                    mintY = objFields.Top + 6
                    'objField.Tag = 0
                    'End If

                    objPasteImageField._FieldId = objFields._FieldID
                    objPasteImageField._SectionId = objFields._SectionId

                    objPasteImageField.Tag = objFields.Tag
                    objPasteImageField.Name = objFields.Name
                    objPasteImageField.TextAlign = objFields.TextAlign
                    objPasteImageField.Font = New Font(CType(objFields.Font.Name, String), CType(objFields.Font.Size, Single), CType(objFields.Font.Style, FontStyle))
                    objPasteImageField.ForeColor = objFields.ForeColor
                    objPasteImageField.BackColor = objFields.BackColor
                    objPasteImageField.Height = objFields.Height
                    objPasteImageField.Width = objFields.Width

                    objPasteImageField.Left = objFields.Left
                    objPasteImageField.Top = objFields.Top

                    objPasteImageField.FieldType = objFields.FieldType
                    'objPOSTemplate.Field._FieldName = objPOSTemplate.Field._FieldName & intId
                    objPasteImageField._FieldName = objFields._FieldName
                    objPasteImageField._Value = objFields._Value

                    objPasteImageField.LineStyle = objFields.LineStyle
                    objPasteImageField.LineThickness = objFields.LineThickness
                    objPasteImageField.LineColor = objFields.LineColor

                    'objLine.ContextMenu = mnuDel

                    'Select Case objPOSTemplate.Field._FieldType
                    '    Case 5 'Static Line
                    objPasteImageField.Text = objFields.Text
                    'End Select


                    Select Case objFields._SectionId
                        Case 1
                            setLineField(objFields, 1, objFields.FieldType)
                        Case 2
                            setLineField(objFields, 2, objFields.FieldType)
                        Case 3
                            setLineField(objFields, 3, objFields.FieldType)
                        Case 4
                            setLineField(objFields, 4, objFields.FieldType)
                        Case 5
                            setLineField(objFields, 5, objFields.FieldType)
                    End Select
                    CreateField(objPasteImageField, IIf(objFields.Parent.Name = "pnlCompHeader", 0, IIf(objFields.Parent.name = "pnlHeader", 1, IIf(objFields.Parent.name = "pnlDetail", 3, IIf(objFields.Parent.name = "pnlDetailFooter", 4, IIf(objFields.Parent.Name = "pnlFooter", 5, IIf(objFields.Parent.Name = "pnlGroup1", 6, 7)))))), "I")
                    objFields.Unselect()
                End If

                If TypeOf objFields Is TemplateLineField Then
                    'If blnCopy = True Then
                    Dim objPasteLineField As New TemplateLineField
                    objPasteLineField._FieldId = 0
                    mintX = objFields.Left + 6
                    mintY = objFields.Top + 6
                    'objField.Tag = 0
                    'End If

                    objPasteLineField._FieldId = objFields._FieldID
                    objPasteLineField._SectionId = objFields._SectionId

                    objPasteLineField.Tag = objFields.Tag
                    objPasteLineField.Name = objFields.Name
                    objPasteLineField.TextAlign = objFields.TextAlign
                    objPasteLineField.Font = New Font(CType(objFields.Font.Name, String), CType(objFields.Font.Size, Single), CType(objFields.Font.Style, FontStyle))
                    objPasteLineField.ForeColor = objFields.ForeColor
                    objPasteLineField.BackColor = objFields.BackColor
                    objPasteLineField.Height = objFields.Height
                    objPasteLineField.Width = objFields.Width

                    objPasteLineField.Left = objFields.Left
                    objPasteLineField.Top = objFields.Top

                    objPasteLineField.FieldType = objFields.FieldType
                    'objPOSTemplate.Field._FieldName = objPOSTemplate.Field._FieldName & intId
                    objPasteLineField._FieldName = objFields._FieldName
                    objPasteLineField._Value = objFields._Value

                    objPasteLineField.Vertical = objFields.Vertical
                    objPasteLineField.LineStyle = objFields.LineStyle
                    objPasteLineField.LineThickness = objFields.LineThickness
                    objPasteLineField.LineColor = objFields.LineColor

                    'objLine.ContextMenu = mnuDel

                    'Select Case objPOSTemplate.Field._FieldType
                    '    Case 5 'Static Line
                    objPasteLineField.Text = objFields.Text
                    'End Select


                    Select Case objFields._SectionId
                        Case 1
                            setLineField(objFields, 1, objFields.FieldType)
                        Case 2
                            setLineField(objFields, 2, objFields.FieldType)
                        Case 3
                            setLineField(objFields, 3, objFields.FieldType)
                        Case 4
                            setLineField(objFields, 4, objFields.FieldType)
                        Case 5
                            setLineField(objFields, 5, objFields.FieldType)
                    End Select
                    CreateField(objPasteLineField, IIf(objFields.Parent.Name = "pnlCompHeader", 0, IIf(objFields.Parent.name = "pnlHeader", 1, IIf(objFields.Parent.name = "pnlDetail", 3, IIf(objFields.Parent.name = "pnlDetailFooter", 4, IIf(objFields.Parent.Name = "pnlFooter", 5, IIf(objFields.Parent.Name = "pnlGroup1", 6, 7)))))), "L")
                    objFields.Unselect()
                End If
            Next
        End If
        arrIdList.Clear()
        arrObjectList.Clear()
        arrCopyCutList.Clear()
        arrLineObjectList.Clear()
        blnCopy = False
        'blnCut = False
        edit_Paste_Button.Enabled = False
        edit_Copy_Button.Enabled = False
        edit_Cut_Button.Enabled = False

        'blnCut = False
        blnCopy = False
    End Sub
#End Region
#Region "Unselect all Fields when click on panels"
    '********Unselect all fields when mouse click not on fields.****************
    Public Sub UnselectFields()
        'Company Header
        For Each objField As Object In pnlCompHeader.Controls
            objField.Unselect()
        Next

        'Header
        For Each objField As Object In pnlHeader.Controls
            objField.Unselect()
        Next

        'Group 1
        For Each objField As Object In pnlGroup1.Controls
            objField.Unselect()
        Next

        'Detail
        For Each objField As Object In pnlDetail.Controls
            objField.Unselect()
        Next

        'Group Footer 1
        For Each objField As Object In pnlGroupFooter1.Controls
            objField.Unselect()
        Next

        'DetailFooter
        For Each objField As Object In pnlDetailFooter.Controls
            objField.Unselect()
        Next

        'Footer
        For Each objField As Object In pnlFooter.Controls
            objField.Unselect()
        Next

        objlblProperty.Text = "Property:" & ""
        pgTemplateField.SelectedObject = Nothing
        arrIdList.Clear()
        arrObjectList.Clear()
        arrLineObjectList.Clear()

        DisabledToolbarButtons()
        edit_Delete_Button.Enabled = False
        edit_Copy_Button.Enabled = False
        edit_Cut_Button.Enabled = False
        edit_Paste_Button.Enabled = False

        MyBase.Refresh()

    End Sub
#End Region
    Private Sub ClearDataPnl()
        objlblProperty.Text = "Property:" & ""
        pgTemplateField.SelectedObject = Nothing
        arrIdList.Clear()
        arrObjectList.Clear()
        arrLineObjectList.Clear()


        For Each ctrl As Control In pnlHeader.Controls
            If TypeOf ctrl Is TemplateImageField Then
                ctrl.BackgroundImage.Dispose()
                ctrl.BackgroundImage = Nothing
                ctrl.Dispose()
            End If
        Next

        pnlCompHeader.Controls.Clear()
        pnlHeader.Controls.Clear()
        pnlDetail.Controls.Clear()
        pnlDetailFooter.Controls.Clear()
        pnlFooter.Controls.Clear()
        pnlGroup1.Controls.Clear()
        pnlGroupFooter1.Controls.Clear()

    End Sub
    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        Try
            Dim lFinish As Boolean = True
            If cmbType.Tag IsNot Nothing Then
                If cmbType.Text.Trim = "" Then MessageBox.Show("Print Type could not be empty", "eZee Message") : cmbType.Focus() : Exit Sub
                ClearDataPnl()
                'Dim frmprocessing As New frmProcess
                frmProcess1 = New frmProcess
                frmProcess1.Show()
                Application.DoEvents()
                'BackgroundWorker1.RunWorkerAsync()
                Dim dsMst As New DataSet
                dsMst.ReadXml(sMst)
                If dsMst.Tables.Count > 0 Then
                    With dsMst.Tables(0)
                        Dim drow() As DataRow = .Select(" printtype = '" & cmbType.Text & "'")
                        If drow.Length > 0 Then
                            cmbType.Text = drow(0)("PrintType")
                            cmbType.Tag = drow(0)("trnasid")
                            pnlCompHeader.Height = IIf(drow(0)("HeightCompHeader") Is DBNull.Value, 90, drow(0)("HeightCompHeader"))
                            pnlHeader.Height = drow(0)("HeightHeader")
                            pnlDetail.Height = drow(0)("HeightDetail")
                            pnlDetailFooter.Height = drow(0)("HeightDetailFooter")
                            pnlFooter.Height = drow(0)("HeightFooter")
                            pnlGroup1.Height = IIf(drow(0)("HeightGroup1") Is DBNull.Value, 32, drow(0)("HeightGroup1"))
                            pnlGroupFooter1.Height = IIf(drow(0)("HeightGroupFooter1") Is DBNull.Value, 32, drow(0)("HeightGroupFooter1"))
                            txtItemPrint.Text = drow(0)("ItemPrint")
                            If Not mblnLibraryOnly Then
                                txtMstXMLPath.Text = drow(0)("MstXMLPath")
                            End If
                            tpPrintSample.SetToolTip(txtMstXMLPath, txtMstXMLPath.Text)
                            txtDetXMLPath.Text = drow(0)("DetXMLPath")
                            lSuppreCompHeaderPnl = IIf(drow(0)("SuppressCompHeader") Is DBNull.Value, False, drow(0)("SuppressCompHeader"))
                            lSuppreHeaderPnl = IIf(drow(0)("SuppressHeader") Is DBNull.Value, False, drow(0)("SuppressHeader"))
                            lSuppreGroup1Pnl = IIf(drow(0)("SuppressGroup1") Is DBNull.Value, False, drow(0)("SuppressGroup1"))
                            lSuppreDetPnl = IIf(drow(0)("SuppressDetail") Is DBNull.Value, False, drow(0)("SuppressDetail"))
                            lSuppreGroupFooter1Pnl = IIf(drow(0)("SuppressGroupFooter1") Is DBNull.Value, False, drow(0)("SuppressGroupFooter1"))
                            lSuppreDetFotPnl = IIf(drow(0)("SuppressDetailFooter") Is DBNull.Value, False, drow(0)("SuppressDetailFooter"))
                            lSuppreFooterPnl = IIf(drow(0)("SuppressFooter") Is DBNull.Value, False, drow(0)("SuppressFooter"))
                            txtCanvasWidth.Text = IIf(drow(0)("CanvasWidth") Is DBNull.Value, 10, drow(0)("CanvasWidth"))
                            chkLandspace.Checked = IIf(drow(0)("Landspace") Is DBNull.Value, False, drow(0)("Landspace"))
                            txtMultPrint.Text = IIf(drow(0)("MultPrint") Is DBNull.Value, 1, drow(0)("MultPrint"))
                            chkRepHeader.Checked = IIf(drow(0)("RepHeader") Is DBNull.Value, True, drow(0)("RepHeader"))

                            If drow(0)("CanvasWidth") Is DBNull.Value Then
                                pnlCanvas.Width = 956
                            Else
                                pnlCanvas.Width = ((drow(0)("CanvasWidth") - IIf(drow(0)("Left") Is DBNull.Value, 0.25, drow(0)("Left")) - IIf(drow(0)("Right") Is DBNull.Value, 0.25, drow(0)("Right"))) * 100) + 6
                            End If

                            CanvasHeight = IIf(drow(0)("CanvasHeight") Is DBNull.Value, 12, drow(0)("CanvasHeight"))
                            nTop = IIf(drow(0)("Top") Is DBNull.Value, 0.25, drow(0)("Top"))
                            nBottom = IIf(drow(0)("Bottom") Is DBNull.Value, 0.25, drow(0)("Bottom"))
                            nLeft = IIf(drow(0)("Left") Is DBNull.Value, 0.25, drow(0)("Left"))
                            nRight = IIf(drow(0)("Right") Is DBNull.Value, 0.25, drow(0)("Right"))
                            sPaperSize = IIf(drow(0)("PaperSize") Is DBNull.Value, "Manual", drow(0)("PaperSize"))
                            chkPageWise.Checked = IIf(drow(0)("PageWise") Is DBNull.Value, False, drow(0)("PageWise"))
                            'cmbType.Enabled = False
                            cmbType.ForeColor = Color.Brown
                            cmbType.DropDownStyle = ComboBoxStyle.Simple
                            FillTreeView()
                        Else
                            DefaultPnlHeight()
                            txtMstXMLPath.Text = ""
                            tpPrintSample.SetToolTip(txtMstXMLPath, txtMstXMLPath.Text)
                            txtDetXMLPath.Text = ""
                            tvControls.Nodes.Clear()
                            txtCanvasWidth.Text = 10
                            txtMultPrint.Text = 1
                            chkRepHeader.Checked = True
                            cmbType.Tag = "0"
                            If cmbType.Text.Trim <> "" Then
                                Dim mess As DialogResult = MessageBox.Show("Type does not exists in database, do you want to create it?", "eZee Message", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk)
                                If mess = Windows.Forms.DialogResult.Yes Then
                                    Dim frmWizard As New frmNewWizard
                                    frmWizard._Type = cmbType.Text
                                    frmWizard._MasterTable = dsMst
                                    frmWizard.BringToFront()
                                    frmWizard.ShowDialog()
                                    lFinish = frmWizard._IsFinish
                                    If frmWizard._IsFinish Then
                                        cmbType.Text = frmWizard._Type
                                        txtMstXMLPath.Text = frmWizard._XmlPath
                                        tpPrintSample.SetToolTip(txtMstXMLPath, txtMstXMLPath.Text)
                                        cmbType.ForeColor = Color.Brown
                                        cmbType.DropDownStyle = ComboBoxStyle.Simple
                                        Call FillTreeView()
                                    End If
                                    frmWizard.Dispose()
                                Else
                                    lFinish = False
                                    cmbType.Focus()
                                End If
                            End If
                        End If
                    End With
                End If

                Dim dsDet As New DataSet
                dsDet.ReadXml(sDet)

                If dsDet.Tables.Count > 0 Then
                    With dsDet.Tables(0)
                        Dim drow() As DataRow = .Select(" typeid = '" & cmbType.Tag & "'", "panel,y,x")
                        If drow.Length > 0 Then
                            Dim nImgCtr As Int16 = 0
                            For i As Integer = 0 To drow.Length - 1
                                Dim objLine As New TemplateLineField
                                Dim objField As New TemplateBoxField
                                Dim objImage As New TemplateImageField
                                Dim style As New FontStyle
                                style = drow(i)("Style")
                                If drow(i)("Text") = "Line" Then
                                    'objLine._FieldId = drow(i)("FieldId")
                                    objLine._FieldId = drow(i)("trnasid")
                                    intId = objLine._FieldId
                                    objLine._SectionId = drow(i)("SectionId")
                                    objLine.Tag = drow(i)("Tag")
                                    objLine.Name = drow(i)("Name")
                                    objLine.TextAlign = IIf(drow(i)("TextAlign") = "BottomCenter", 512, IIf(drow(i)("TextAlign") = "BottomLeft", 256, IIf(drow(i)("TextAlign") = "BottomRight", 1024, IIf(drow(i)("TextAlign") = "MiddleCenter", 32, IIf(drow(i)("TextAlign") = "MiddleLeft", 16, IIf(drow(i)("TextAlign") = "MiddleRight", 64, IIf(drow(i)("TextAlign") = "TopCenter", 2, IIf(drow(i)("TextAlign") = "TopLeft", 1, 4))))))))
                                    objLine.Font = New Font(drow(i)("Font").ToString, CType(drow(i)("Size"), Decimal), style)
                                    objLine.ForeColor = Color.FromName(drow(i)("ForeColor"))
                                    objLine.BackColor = Color.FromName(drow(i)("BackColor"))
                                    objLine.Height = drow(i)("Height")
                                    objLine.Width = drow(i)("Width")
                                    objLine.Left = drow(i)("x")
                                    objLine.Top = drow(i)("y")

                                    objLine.FieldType = drow(i)("FieldType")
                                    objLine._FieldName = drow(i)("FieldName")
                                    objLine._Value = drow(i)("Value")
                                    objLine.Vertical = drow(i)("Vertical")
                                    objLine.LineStyle = IIf(drow(i)("LineStyle") = "Custom", 5, IIf(drow(i)("LineStyle") = "Dash", 1, IIf(drow(i)("LineStyle") = "DashDot", 3, IIf(drow(i)("LineStyle") = "DashDotDot", 4, IIf(drow(i)("LineStyle") = "Dot", 2, 0)))))
                                    objLine.LineThickness = drow(i)("LineThickness")

                                    objLine.Suppress = IIf(drow(i)("Suppress") Is Nothing OrElse drow(i)("Suppress") Is DBNull.Value, False, drow(i)("Suppress"))

                                    objLine.LineColor = Color.FromName(drow(i)("LineColor"))
                                    objLine.Text = drow(i)("Text")
                                    If drow(i)("panel") = "pnlCompHeader" Then
                                        pnlCompHeader.Controls.Add(objLine)
                                    ElseIf drow(i)("panel") = "pnlHeader" Then
                                        pnlHeader.Controls.Add(objLine)
                                    ElseIf drow(i)("panel") = "pnlDetail" Then
                                        pnlDetail.Controls.Add(objLine)
                                    ElseIf drow(i)("panel") = "pnlDetailFooter" Then
                                        pnlDetailFooter.Controls.Add(objLine)
                                    ElseIf drow(i)("panel") = "pnlFooter" Then
                                        pnlFooter.Controls.Add(objLine)
                                    ElseIf drow(i)("panel") = "pnlGroup1" Then
                                        pnlGroup1.Controls.Add(objLine)
                                    ElseIf drow(i)("panel") = "pnlGroupFooter1" Then
                                        pnlGroupFooter1.Controls.Add(objLine)
                                    End If
                                    AddHandler objLine.MouseDown, AddressOf ctl_Click

                                    'objField.ContextMenuStrip = cmnuDelete

                                    SetLineProperty(objLine)

                                    If objLine.FieldType = 4 Then
                                        'objField.SendToBack()
                                    Else
                                        objLine.BringToFront()
                                    End If
                                ElseIf drow(i)("Name") = "Image" Then
                                    'If IO.File.Exists(ImagePath() + "\Image" + cmbType.Tag.ToString + drow(i)("trnasid").ToString + ".jpg") Then
                                    'ReDim Preserve aImagePath(nImgCtr)
                                    'aImagePath(nImgCtr) = ImagePath() + "\Image" + cmbType.Tag.ToString + drow(i)("trnasid").ToString + ".jpg"
                                    ''Copy File 
                                    'System.IO.File.Copy(aImagePath(nImgCtr), ImagePath() + "\copyImage" + cmbType.Tag.ToString + drow(i)("trnasid").ToString + ".jpg", True)
                                    'ReDim Preserve aCpyImgPath(nImgCtr)
                                    'aCpyImgPath(nImgCtr) = ImagePath() + "\copyImage" + cmbType.Tag.ToString + drow(i)("trnasid").ToString + ".jpg"
                                    'objImage.FilePath = ImageClone(Image.FromFile(aCpyImgPath(nImgCtr)))
                                    'objImage.Refresh()
                                    'nImgCtr += 1
                                    Dim pictureData As Byte() = DirectCast(drow(i)("Picture"), Byte())
                                    Using stream As New IO.MemoryStream(pictureData)
                                        'Read the stream and create an Image object from the data.
                                        objImage.FilePath = ImageClone(Image.FromStream(stream))
                                    End Using
                                    'Else
                                    '    MessageBox.Show("No Data Found for " + ImagePath() + "\Image" + cmbType.Tag.ToString + drow(i)("trnasid").ToString + ".jpg", "eZee Message")
                                    'End If
                                    'objImage._FieldId = drow(i)("FieldId")
                                    objImage._FieldId = drow(i)("trnasid")

                                    intId = objImage._FieldId
                                    objImage._SectionId = drow(i)("SectionId")
                                    objImage.Tag = drow(i)("Tag")
                                    objImage.Name = drow(i)("Name")
                                    objImage.TextAlign = IIf(drow(i)("TextAlign") = "BottomCenter", 512, IIf(drow(i)("TextAlign") = "BottomLeft", 256, IIf(drow(i)("TextAlign") = "BottomRight", 1024, IIf(drow(i)("TextAlign") = "MiddleCenter", 32, IIf(drow(i)("TextAlign") = "MiddleLeft", 16, IIf(drow(i)("TextAlign") = "MiddleRight", 64, IIf(drow(i)("TextAlign") = "TopCenter", 2, IIf(drow(i)("TextAlign") = "TopLeft", 1, 4))))))))
                                    objImage.Font = New Font(drow(i)("Font").ToString, CType(drow(i)("Size"), Decimal), style)
                                    objImage.ForeColor = Color.FromName(drow(i)("ForeColor"))
                                    objImage.BackColor = Color.FromName(drow(i)("BackColor"))
                                    objImage.Height = drow(i)("Height")
                                    objImage.Width = drow(i)("Width")
                                    objImage.Left = drow(i)("x")
                                    objImage.Top = drow(i)("y")
                                    objImage.Suppress = IIf(drow(i)("Suppress") Is Nothing OrElse drow(i)("Suppress") Is DBNull.Value, False, drow(i)("Suppress"))


                                    objImage.FieldType = drow(i)("FieldType")
                                    objImage._FieldName = drow(i)("FieldName")
                                    objImage._Value = drow(i)("Value")
                                    objImage.LineStyle = IIf(drow(i)("LineStyle") = "Custom", 5, IIf(drow(i)("LineStyle") = "Dash", 1, IIf(drow(i)("LineStyle") = "DashDot", 3, IIf(drow(i)("LineStyle") = "DashDotDot", 4, IIf(drow(i)("LineStyle") = "Dot", 2, 0)))))
                                    objImage.LineThickness = drow(i)("LineThickness")
                                    objImage.LineColor = Color.FromName(drow(i)("LineColor"))
                                    objImage.Text = drow(i)("Text")
                                    objImage.AllR = drow(i)("AllR")
                                    If drow(i)("panel") = "pnlCompHeader" Then
                                        pnlCompHeader.Controls.Add(objImage)
                                    ElseIf drow(i)("panel") = "pnlHeader" Then
                                        pnlHeader.Controls.Add(objImage)
                                    ElseIf drow(i)("panel") = "pnlDetail" Then
                                        pnlDetail.Controls.Add(objImage)
                                    ElseIf drow(i)("panel") = "pnlDetailFooter" Then
                                        pnlDetailFooter.Controls.Add(objImage)
                                    ElseIf drow(i)("panel") = "pnlFooter" Then
                                        pnlFooter.Controls.Add(objImage)
                                    ElseIf drow(i)("panel") = "pnlGroup1" Then
                                        pnlGroup1.Controls.Add(objImage)
                                    ElseIf drow(i)("panel") = "pnlGroupFooter1" Then
                                        pnlGroupFooter1.Controls.Add(objImage)
                                    End If
                                    AddHandler objImage.MouseDown, AddressOf ctl_Click

                                    SetImageProperty(objImage)
                                    objImage.SendToBack()
                                Else
                                    'objField._FieldId = drow(i)("FieldId")
                                    objField._FieldId = drow(i)("trnasid")
                                    intId = objField._FieldId
                                    objField._SectionId = drow(i)("SectionId")
                                    objField.Tag = drow(i)("Tag")
                                    objField.Name = drow(i)("Name")
                                    objField.TextAlign = IIf(drow(i)("TextAlign") = "BottomCenter", 512, IIf(drow(i)("TextAlign") = "BottomLeft", 256, IIf(drow(i)("TextAlign") = "BottomRight", 1024, IIf(drow(i)("TextAlign") = "MiddleCenter", 32, IIf(drow(i)("TextAlign") = "MiddleLeft", 16, IIf(drow(i)("TextAlign") = "MiddleRight", 64, IIf(drow(i)("TextAlign") = "TopCenter", 2, IIf(drow(i)("TextAlign") = "TopLeft", 1, 4))))))))
                                    objField.Font = New Font(drow(i)("Font").ToString, CType(drow(i)("Size"), Decimal), style)
                                    objField.ForeColor = Color.FromName(drow(i)("ForeColor"))
                                    objField.BackColor = Color.FromName(drow(i)("BackColor"))
                                    objField.Height = drow(i)("Height")
                                    objField.Width = drow(i)("Width")
                                    objField.Left = drow(i)("x")
                                    objField.Top = drow(i)("y")

                                    objField.Suppress = IIf(drow(i)("Suppress") Is Nothing OrElse drow(i)("Suppress") Is DBNull.Value, False, drow(i)("Suppress"))
                                    objField.CanDraw = IIf(drow(i)("CanDraw") Is Nothing OrElse drow(i)("CanDraw") Is DBNull.Value, False, drow(i)("CanDraw"))
                                    objField.DupSupp = IIf(drow(i)("DupSupp") Is Nothing OrElse drow(i)("DupSupp") Is DBNull.Value, False, drow(i)("DupSupp"))

                                    'Manish Tanna (12 May 2012) -- Start 
                                    objField.Format = IIf(drow(i)("Format") Is Nothing OrElse drow(i)("Format") Is DBNull.Value, "", drow(i)("Format"))
                                    'Manish Tanna (12 May 2012) -- End 
                                    objField.FieldType = drow(i)("FieldType")
                                    objField._FieldName = drow(i)("FieldName")
                                    objField._Value = drow(i)("Value")
                                    objField.LineStyle = IIf(drow(i)("LineStyle") = "Custom", 5, IIf(drow(i)("LineStyle") = "Dash", 1, IIf(drow(i)("LineStyle") = "DashDot", 3, IIf(drow(i)("LineStyle") = "DashDotDot", 4, IIf(drow(i)("LineStyle") = "Dot", 2, 0)))))
                                    objField.LineThickness = drow(i)("LineThickness")
                                    objField.LineColor = Color.FromName(drow(i)("LineColor"))
                                    objField.Text = drow(i)("Text")
                                    objField.AllR = drow(i)("AllR")
                                    If drow(i)("panel") = "pnlCompHeader" Then
                                        pnlCompHeader.Controls.Add(objField)
                                    ElseIf drow(i)("panel") = "pnlHeader" Then
                                        pnlHeader.Controls.Add(objField)
                                    ElseIf drow(i)("panel") = "pnlDetail" Then
                                        pnlDetail.Controls.Add(objField)
                                    ElseIf drow(i)("panel") = "pnlDetailFooter" Then
                                        pnlDetailFooter.Controls.Add(objField)
                                    ElseIf drow(i)("panel") = "pnlFooter" Then
                                        pnlFooter.Controls.Add(objField)
                                    ElseIf drow(i)("panel") = "pnlGroup1" Then
                                        pnlGroup1.Controls.Add(objField)
                                    ElseIf drow(i)("panel") = "pnlGroupFooter1" Then
                                        pnlGroupFooter1.Controls.Add(objField)
                                    End If
                                    AddHandler objField.MouseDown, AddressOf ctl_Click
                                    'objField.ContextMenuStrip = cmnuDelete

                                    SetProperty(objField)

                                    If objField.FieldType = 4 Then
                                        'objField.SendToBack()
                                    Else
                                        objField.BringToFront()
                                    End If
                                End If
                            Next
                        End If
                    End With
                End If
                'BackgroundWorker1.WorkerSupportsCancellation = True
                'BackgroundWorker1.CancelAsync()
                'BackgroundWorker1.Dispose()
                'BackgroundWorker1.Dispose()
                frmProcess1.Close()
                frmProcess1.Dispose()
            End If
            If lFinish Then
                btnLoad.Enabled = False

                btnMstPath.Enabled = True
                '' 'btnDetPath.Enabled = True
                btnSave.Enabled = True
                btnDelete.Enabled = True
                btnCancel.Enabled = True
                btnConverter.Enabled = True
            End If
            pnlCompHeader.Refresh()
            pnlHeader.Refresh()
            pnlDetail.Refresh()
            pnlDetailFooter.Refresh()
            pnlFooter.Refresh()
            pnlGroup1.Refresh()
            pnlGroupFooter1.Refresh()
            Me.BringToFront()
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If cmbType.Text.Trim = "" Then MessageBox.Show("Print Type could not be empty", "eZee Message") : cmbType.Focus() : Exit Sub
            If MessageBox.Show("Are you sure you want to Delete the data?", "eZee Message", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                Dim dsMst As New DataSet
                dsMst.ReadXml(sMst)
                If cmbType.Tag IsNot Nothing Then
                    If dsMst.Tables(0).Select("trnasid = '" & cmbType.Tag & "'").Length > 0 Then
                        dsMst.Tables(0).Rows.Remove(dsMst.Tables(0).Select("trnasid = '" & cmbType.Tag & "'")(0))
                    End If
                End If
                dsMst.WriteXml(sMst, XmlWriteMode.WriteSchema)

                Dim dsDet As New DataSet
                dsDet.ReadXml(sDet)
                With dsDet.Tables(0)
                    If cmbType.Tag IsNot Nothing Then
                        If dsDet.Tables(0).Select("typeid = '" & cmbType.Tag & "'").Length > 0 Then
                            For i As Integer = dsDet.Tables(0).Select("typeid = '" & cmbType.Tag & "'").Length - 1 To 0 Step -1
                                dsDet.Tables(0).Rows.Remove(dsDet.Tables(0).Select("typeid = '" & cmbType.Tag & "'")(i))
                            Next
                        End If
                    End If
                End With
                dsDet.WriteXml(sDet, XmlWriteMode.WriteSchema)
                CancelIt()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub

    Private Sub btnClearData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ClearDataPnl()
    End Sub
#Region "Windows Print"
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'WindowPrint()
        WindowPrintPre()
    End Sub

    Private Sub WindowPrintPre()
        Dim oPrint As New eZeePrint
        Dim nSize As Integer = 1
        nPrintCtr = 0
        If chkLandspace.Checked Then
            nSize = 2
        Else
            nSize = 1
        End If
        nCompHeadery = 0
        nHeadery = 0
        nDetHeadery = 0
        nDetDety = 0
        nDetFootery = 0
        nGroup1y = 0
        nGrpFooter1y = 0
        dtTable = New DataTable
        oPrint.nLeft = nLeft * 100
        oPrint.nRight = nRight * 100
        oPrint.nTop = nTop * 100
        oPrint.nBottom = nBottom * 100
        If chkLandspace.Checked Then
            oPrint.PageLen(CanvasHeight * 100, pnlCanvas.Width)
        Else
            oPrint.PageLen(pnlCanvas.Width + ((nLeft - 0.25) * 100) + ((nRight - 0.25) * 100), CanvasHeight * 100)
        End If

        Dim dsTempMst As New DataSet
        dsTempMst.ReadXml(sMst)
        If dsTempMst.Tables.Count > 0 Then
            If dsTempMst.Tables(0).Rows.Count > 0 Then
                Dim drow() As DataRow = dsTempMst.Tables(0).Select("trnasid = '" & cmbType.Tag & "'")
                If drow.Length > 0 Then
                    If mblnLibraryOnly Then
                        sMstXMLPaths = txtMstXMLPath.Text
                    Else
                        sMstXMLPaths = drow(0)("MstXMLPath")
                    End If
                    nPageNo = 0
                    WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                    WindowsBasedHeader(oPrint)
                    WindowsBasedDetail(oPrint)
                    WindowsBasedFooter(oPrint)
                End If
            End If
        End If
        oPrint.PageEject()

        oPrint.Show(Me.MdiParent, nSize)

    End Sub

    Private Sub WindowsBasedCompHeader(ByVal oPrint As eZeePrint, ByVal sMstXMLPath As String)
        Try
            Dim lFlag As Boolean = True
            nPageNo += 1
            If Not chkRepHeader.Checked Then
                lFlag = (nPageNo = 1)
            End If

            dsMst.Tables.Clear()
            dsMst.ReadXml(sMstXMLPath)
            If dsMst.Tables(0).Rows.Count > 0 Then
                'Company Header Part 
                With pnlCompHeader
                    If lFlag Then
                        If Not lSuppreCompHeaderPnl Then
                            For i As Integer = .Controls.Count - 1 To 0 Step -1
                                nPrintCtr += 1
                                If nCompHeadery < .Controls(i).Location.Y + .Controls(i).Height Then
                                    nCompHeadery = .Controls(i).Location.Y + .Controls(i).Height
                                End If
                                If .Controls(i).Name = "Text" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        oPrint.printText(.Controls(i).Text, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                    End If
                                ElseIf .Controls(i).Name = "Image" Then
                                    If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                        oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
                                    End If
                                ElseIf .Controls(i).Name = "Line" Then
                                    If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                        If CType(.Controls(i), TemplateLineField).Vertical Then
                                            Dim pPen As New Pen(.Controls(i).ForeColor)
                                            pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                            pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                            pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                            oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                        Else
                                            Dim pPen As New Pen(.Controls(i).ForeColor)
                                            pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                            pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                            pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                            oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                        End If
                                    End If
                                ElseIf .Controls(i).Name = "Rectangle" Then
                                    'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height - 2, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                                    End If
                                    'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
                                Else
                                    If .Controls(i).Text <> "" Then
                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                            'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
                                            dtTable = Nothing
                                            If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                                dtTable = dsMst.Tables(0)
                                            ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                                dtTable = dsMst.Tables(1)
                                            End If
                                            If dtTable IsNot Nothing Then
                                                'Manish Tanna (12 May 2012) -- start 
                                                Dim strFormula As String
                                                If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                                    If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                        strFormula = Format(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                                    Else
                                                        strFormula = CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                                    End If
                                                    oPrint.printText(strFormula, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                Else
                                                    If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                        strFormula = Format(dtTable.Rows(0)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                                    Else
                                                        strFormula = dtTable.Rows(0)(.Controls(i).Text).ToString
                                                    End If
                                                    oPrint.printText(strFormula, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                End If
                                                'Manish Tanna (12 May 2012) -- start 
                                            End If
                                            If .Controls(i).Text Like ".Formula_*" Then
                                                Dim strSelect As String = ""
                                                Dim frmFormula As New frmFormula
                                                frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                Dim sFormat As String = ""
                                                For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                    sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                    If sFormat <> "" Then
                                                        Exit For
                                                    End If
                                                Next
                                                frmFormula.mstrFormat = sFormat
                                                frmFormula.mdtTable = dtTable
                                                frmFormula.ExecuteFormula()
                                                strSelect = frmFormula.mstrSelect
                                                oPrint.printText(strSelect, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Else
                        nCompHeadery = 0
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
    Private Sub WindowsBasedHeader(ByVal oPrint As eZeePrint)
        Dim y As Integer = 0
        Dim nTops As Integer = 0
        'Dim grpDetHght As Integer = 0
        'Dim grpFoterDetHght As Integer = 0
        Try
            If dsMst.Tables(0).Rows.Count > 0 Then
                'Master Part 
                With pnlHeader
                    If Not lSuppreHeaderPnl Then
                        For i As Integer = .Controls.Count - 1 To 0 Step -1
                            nPrintCtr += 1
                            If nHeadery < .Controls(i).Location.Y + .Controls(i).Height Then
                                nHeadery = .Controls(i).Location.Y + .Controls(i).Height
                            End If
                            If .Controls(i).Name = "Text" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    oPrint.printText(.Controls(i).Text, .Controls(i).Location.X, nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                End If
                            ElseIf .Controls(i).Name = "Image" Then
                                If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                    oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
                                End If
                            ElseIf .Controls(i).Name = "Line" Then
                                If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                    If CType(.Controls(i), TemplateLineField).Vertical Then
                                        Dim pPen As New Pen(.Controls(i).ForeColor)
                                        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                        oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                    Else
                                        Dim pPen As New Pen(.Controls(i).ForeColor)
                                        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                        oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                    End If
                                End If
                            ElseIf .Controls(i).Name = "Rectangle" Then
                                'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, y + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height - 2, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                                End If
                                'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
                            Else
                                If .Controls(i).Text <> "" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
                                        dtTable = Nothing
                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(0)
                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(1)
                                        End If
                                        If dtTable IsNot Nothing Then
                                            'Manish Tanna (12 May 2012) -- start 
                                            Dim strFormula As String
                                            If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                                If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                    strFormula = Format(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                                Else
                                                    strFormula = CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                                End If

                                                oPrint.printText(strFormula, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            Else
                                                If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                    strFormula = Format(dtTable.Rows(0)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                                Else
                                                    strFormula = dtTable.Rows(0)(.Controls(i).Text).ToString
                                                End If
                                                oPrint.printText(strFormula, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            End If
                                            'Manish Tanna (12 May 2012) -- start 
                                        End If
                                        If .Controls(i).Text Like ".Formula_*" Then
                                            Dim strSelect As String = ""
                                            Dim frmFormula As New frmFormula
                                            frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                            Dim sFormat As String = ""
                                            For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                If sFormat <> "" Then
                                                    Exit For
                                                End If
                                            Next
                                            frmFormula.mstrFormat = sFormat
                                            frmFormula.mdtTable = dtTable
                                            frmFormula.ExecuteFormula()
                                            strSelect = frmFormula.mstrSelect
                                            oPrint.printText(strSelect, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
    Private Sub WindowsBasedDetail(ByVal oPrint As eZeePrint)
        Dim Graphics As Graphics
        Dim yCanDraw As Integer = 0
        Dim yScale As Integer = 0
        Graphics = CreateGraphics()

        Try
            iCtr = 0
            Dim strGroup As String = ""
            Dim strGroupFooter As String = ""
            'Detail Part
            If pnlDetail.Controls.Count > 0 Then
                If dsMst.Tables.Count > 1 Then
                    If dsMst.Tables(1).Rows.Count > 0 Then
                        nCounter = 0
                        With pnlDetail
                            IsGroups()
                            For j As Integer = 0 To dsMst.Tables(1).Rows.Count - 1
                                'Group Checking
                                nPrintCtr += 1
                                If tvControls.Nodes(1).Nodes.Count > 0 Then
                                    Dim strSel As String = ""
                                    For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                                        strSel += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
                                    Next
                                    If strGroup <> strSel Then
                                        nGroup1y += FunctGrouping(oPrint, j)
                                        strGroup = strSel
                                    End If
                                End If

                                If Not lSuppreDetPnl Then
                                    Dim k As Integer = 0
                                    'Before Print Counting     
                                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                                        Dim m As Object
                                        m = 1
                                        If TypeOf .Controls(i) Is TemplateBoxField Then
                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                Dim orgWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                Dim TotalWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font).Width
                                                If orgWidht < TotalWidht Then
                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                End If
                                            End If
                                        End If
                                        If k < .Controls(i).Location.Y + .Controls(i).Height * m Then k = .Controls(i).Location.Y + .Controls(i).Height * m
                                    Next
                                    nCounter += k
                                    iCtr += k
                                    If nGroup1y + iCtr + nGrpFooter1y > ItemHeight() Then ' (txtItemPrint.Text * 105) 
                                        nDetDety = ItemHeight() - nGroup1y - nGrpFooter1y '(txtItemPrint.Text * 105) 
                                        WindowsBasedFooter(oPrint, True)
                                        oPrint.PageEject()
                                        nCounter = 0
                                        nDetDety = 0
                                        iCtr = 0
                                        nPrintCtr = 0
                                        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                                        WindowsBasedHeader(oPrint)
                                        IsGroups()
                                        nGroup1y = 0
                                        nGrpFooter1y = 0
                                        yCanDraw = 0
                                        nCounter += k
                                        iCtr += k
                                    End If
                                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                                        If .Controls(i).Name = "Text" Then
                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                'Can Draw 
                                                If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                    Dim m As Object
                                                    m = 0
                                                    Dim orgWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                    Dim TotalWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font).Width
                                                    If orgWidht < TotalWidht Then
                                                        m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                        oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                    Else
                                                        oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                    End If
                                                End If
                                            Else
                                                oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            End If
                                        ElseIf .Controls(i).Name = "Image" Then
                                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                                oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height + 2)
                                            End If
                                        ElseIf .Controls(i).Name = "Line" Then
                                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                                Else
                                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                                End If
                                            End If
                                        ElseIf .Controls(i).Name = "Rectangle" Then
                                            'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
                                            'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y - k + nCounter + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top - 1, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                                            End If
                                        Else
                                            If .Controls(i).Text.Trim <> "" Then
                                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                    'If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
                                                    dtTable = Nothing
                                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                                        dtTable = dsMst.Tables(0)
                                                        If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                                            'Can Draw
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                Else
                                                                    oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                End If
                                                            Else
                                                                oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                            End If
                                                        Else
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                Else
                                                                    oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                End If
                                                            Else
                                                                oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                            End If
                                                        End If
                                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                                        dtTable = dsMst.Tables(1)
                                                        'If .Controls(i).Text Like ".Formula_*" Then
                                                        '    Dim strSelect As String = ""
                                                        '    Dim frmFormula As New frmFormula
                                                        '    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                        '    Dim sFormat As String = ""
                                                        '    For l As Integer = 0 To tvControls.Nodes.Count - 1
                                                        '        sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                        '        If sFormat <> "" Then
                                                        '            Exit For
                                                        '        End If
                                                        '    Next
                                                        '    frmFormula.sFormat = sFormat
                                                        '    frmFormula.nRow = j
                                                        '    frmFormula.dtTable = dtTable
                                                        '    frmFormula.ExecuteFormula()
                                                        '    strSelect = frmFormula.strSelect
                                                        '    oPrint.printText(strSelect, .Controls(i).Location.X, nCounter + nDetHeadery + nHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                        'Else
                                                        'Manish Tanna (12 May 2012) -- start 
                                                        Dim strFormula As String
                                                        If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                                            If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                                strFormula = Format(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                                            Else
                                                                strFormula = CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                                            End If
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                Else
                                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                End If
                                                            Else
                                                                oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                            End If
                                                        Else
                                                            If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                                strFormula = Format(dtTable.Rows(j)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                                            Else
                                                                strFormula = dtTable.Rows(j)(.Controls(i).Text).ToString
                                                            End If
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                Else
                                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                                End If
                                                            Else
                                                                oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                                'Manish Tanna (12 May 2012) -- end
                                                If .Controls(i).Text Like ".Formula_*" Then
                                                    Dim strSelect As String = ""
                                                    Dim frmFormula As New frmFormula
                                                    frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                    Dim sFormat As String = ""
                                                    For l As Integer = 0 To tvControls.Nodes.Count - 1
                                                        sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(l).Text)
                                                        If sFormat <> "" Then
                                                            Exit For
                                                        End If
                                                    Next
                                                    frmFormula.mstrFormat = sFormat
                                                    frmFormula.mdtTable = dtTable
                                                    If dtTable.TableName = "Details" OrElse dtTable.TableName = "Table1" Then
                                                        frmFormula.mintRow = j
                                                    End If
                                                    frmFormula.ExecuteFormula()
                                                    strSelect = frmFormula.mstrSelect
                                                    If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                        Dim m As Object
                                                        m = 0
                                                        Dim orgWidht As Object = Graphics.MeasureString(strSelect, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                        Dim TotalWidht As Object = Graphics.MeasureString(strSelect, .Controls(i).Font).Width
                                                        If orgWidht < TotalWidht Then
                                                            m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                            oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                        Else
                                                            oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                        End If
                                                    Else
                                                        oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                    End If
                                                End If
                                                If dtTable IsNot Nothing Then
                                                End If
                                            End If
                                        End If
                                    Next
                                    'If Not chkPageWise.Checked Then
                                    '    If nGroup1y + iCtr + nGrpFooter1y >= (txtItemPrint.Text * 105) Then
                                    '        nDetDety = nGroup1y + iCtr + nGrpFooter1y
                                    '        WindowsBasedFooter(oPrint, True)
                                    '        oPrint.PageEject()
                                    '        nCounter = 0
                                    '        nDetDety = 0
                                    '        iCtr = 0
                                    '        nPrintCtr = 0
                                    '        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                                    '        WindowsBasedHeader(oPrint)
                                    '        IsGroups()
                                    '        nGroup1y = 0
                                    '        nGrpFooter1y = 0
                                    '    End If
                                    'Else
                                    '    If nPrintCtr >= 150 Then
                                    '        nDetDety = nCounter
                                    '        oPrint.PageEject()
                                    '        nCounter = 0
                                    '        nDetDety = 0
                                    '        iCtr = 0
                                    '        nPrintCtr = 0
                                    '        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                                    '        WindowsBasedHeader(oPrint)
                                    '        IsGroups()
                                    '        nGroup1y = 0
                                    '        nGrpFooter1y = 0
                                    '    End If
                                    'End If
                                End If
                                If tvControls.Nodes(1).Nodes.Count > 0 Then
                                    If j <> dsMst.Tables(1).Rows.Count - 1 Then
                                        Dim strSel As String = ""
                                        Dim strSelOrginal As String = ""
                                        For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                                            strSel += dsMst.Tables(1).Rows(j + 1).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
                                            strSelOrginal += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
                                        Next
                                        If strSelOrginal <> strSel Then
                                            nGrpFooter1y += FunctGroupFooter1(oPrint, j)
                                            strGroupFooter = strSel
                                        End If
                                    End If
                                End If
                            Next
                            nDetDety = nCounter
                        End With
                    End If
                    If tvControls.Nodes(1).Nodes.Count > 0 Then
                        nGrpFooter1y += FunctGroupFooter1(oPrint, dsMst.Tables(1).Rows.Count - 1)
                    End If
                    'Detail Footer Part
                    With pnlDetailFooter
                        If Not lSuppreDetFotPnl Then
                            For i As Integer = .Controls.Count - 1 To 0 Step -1
                                nPrintCtr += 1
                                If .Controls(i).Name = "Text" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                    End If
                                ElseIf .Controls(i).Name = "Image" Then
                                    If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                        oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
                                    End If
                                ElseIf .Controls(i).Name = "Line" Then
                                    If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                        If CType(.Controls(i), TemplateLineField).Vertical Then
                                            Dim pPen As New Pen(.Controls(i).ForeColor)
                                            pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                            pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                            pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                            oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                        Else
                                            Dim pPen As New Pen(.Controls(i).ForeColor)
                                            pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                            pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                            pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                            oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                        End If
                                    End If
                                ElseIf .Controls(i).Name = "Rectangle" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                                    End If
                                Else
                                    If .Controls(i).Text.Trim <> "" Then
                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                            dtTable = Nothing
                                            If dsMst.Tables(0).Columns.Contains(.Controls(i).Tag) Then
                                                dtTable = dsMst.Tables(0)
                                            ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                                dtTable = dsMst.Tables(1)
                                            End If
                                            'Manish Tanna (12 May 2012) -- start 
                                            If dtTable IsNot Nothing Then
                                                Dim strFormula As String
                                                If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
                                                    Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", "")
                                                    If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                        strFormula = Format(nSum, CType(.Controls(i), TemplateBoxField).Format)
                                                    Else
                                                        strFormula = nSum.ToString
                                                    End If
                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                Else
                                                    If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                                        If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                            strFormula = Format(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                                        Else
                                                            strFormula = CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                                        End If
                                                        oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                    Else
                                                        oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                    End If
                                                End If
                                            End If
                                            If .Controls(i).Text Like ".Formula_*" Then
                                                Dim strSelect As String = ""
                                                Dim objFormula As Object = Nothing
                                                Dim sFormat As String = ""
                                                For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                    sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                    If sFormat <> "" Then
                                                        Exit For
                                                    End If
                                                Next
                                                For k As Integer = 0 To dtTable.Rows.Count - 1
                                                    Dim frmFormula As New frmFormula
                                                    frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                    frmFormula.mstrFormat = sFormat
                                                    frmFormula.mdtTable = dtTable
                                                    If dtTable.TableName = "Details" OrElse dtTable.TableName = "Table1" OrElse dtTable.TableName = "List1" Then
                                                        frmFormula.mintRow = k
                                                    End If
                                                    frmFormula.ExecuteFormula()
                                                    If IsNumeric(frmFormula.mstrSelect) AndAlso sFormat <> "" Then
                                                        objFormula += Val(frmFormula.mstrSelect)
                                                        objFormula = Str(objFormula, 12, sFormat).ToString.Trim()
                                                    Else
                                                        objFormula += frmFormula.mstrSelect
                                                    End If
                                                Next
                                                strSelect = objFormula
                                                oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            End If
                                            'Manish Tanna (12 May 2012) -- end 
                                        End If
                                    End If
                                End If
                                If nDetFootery < .Controls(i).Location.Y + .Controls(i).Height Then
                                    nDetFootery = .Controls(i).Location.Y + .Controls(i).Height
                                End If
                            Next
                        End If
                    End With
                    If Not chkPageWise.Checked Then
                        With pnlDetail
                            If pnlDetail.Controls.Count > 0 Then
                                If Not lSuppreDetPnl Then
                                    'For i = 0 To Math.Round(((((txtItemPrint.Text * 105) - iCtr) / 105)), 0) 'dsMst.Tables(1).Rows.Count
                                    '    nDetDety += .Controls(.Controls.Count - 1).Location.Y + .Controls(.Controls.Count - 1).Height
                                    '    oPrint.printText(SrNo.ToString + " ", .Controls(.Controls.Count - 1).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.MiddleLeft, False)
                                    '    SrNo += 1
                                    'Next
                                    nDetDety += ItemHeight() - iCtr - nGroup1y - nGrpFooter1y - nDetFootery '(txtItemPrint.Text * 105)
                                    oPrint.printText("", .Controls(.Controls.Count - 1).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.BottomLeft, False)
                                    'oPrint.printText("space", .Controls(.Controls.Count - 1).Location.X, 262.5, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.BottomLeft, False)
                                End If
                            End If
                        End With

                    End If

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub

    Private Sub IsGroups()
        'If Groups is given Order by is check
        If tvControls.Nodes(1).Nodes.Count > 0 Then
            Dim strGroupBy As String = ""
            Dim dt As New DataTable
            For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                strGroupBy = tvControls.Nodes(1).Nodes(k).Text + ","
            Next
            strGroupBy = strGroupBy.Substring(0, strGroupBy.Length - 1)
            dt = dsMst.Tables(1).Clone
            dt.Rows.Clear()
            For Each dr As DataRow In dsMst.Tables(1).Rows
                dt.Rows.Add()
                For Each dc As DataColumn In dsMst.Tables(1).Columns
                    dt.Rows(dt.Rows.Count - 1).Item(dc.Caption) = dr.Item(dc.Caption)
                Next
            Next

            Dim dRow() As DataRow = dt.Select("", strGroupBy)
            dsMst.Tables(1).Clear()
            For Each dr As DataRow In dRow
                dsMst.Tables(1).Rows.Add()
                For Each dc As DataColumn In dt.Columns
                    dsMst.Tables(1).Rows(dsMst.Tables(1).Rows.Count - 1).Item(dc.Caption) = dr.Item(dc.Caption)
                Next
            Next
        End If
    End Sub

    Private Function FunctGrouping(ByVal oPrint As eZeePrint, ByVal k As Integer)
        Dim lRetVal As Boolean = True
        Dim nGrp1 As Integer = 0
        If tvControls.Nodes(1).Nodes.Count > 0 Then
            With pnlGroup1
                If Not lSuppreGroup1Pnl Then
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        If nGrp1 < .Controls(i).Location.Y + .Controls(i).Height Then
                            nGrp1 = .Controls(i).Location.Y + .Controls(i).Height
                        End If
                    Next

                    If nGrp1 + nGrpFooter1y + iCtr > ItemHeight() Then '(txtItemPrint.Text * 105)
                        nDetDety = ItemHeight() - nGrp1 '(txtItemPrint.Text * 105)
                        WindowsBasedFooter(oPrint, True)
                        oPrint.PageEject()
                        nCounter = 0
                        nDetDety = 0
                        iCtr = 0
                        nPrintCtr = 0
                        nGroup1y = 0
                        nGrpFooter1y = 0
                        'nGrp1 = 0
                        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                        WindowsBasedHeader(oPrint)
                        IsGroups()
                    End If
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        nPrintCtr += 1
                        'If nGrp1 < .Controls(i).Location.Y + .Controls(i).Height Then
                        '    nGrp1 = .Controls(i).Location.Y + .Controls(i).Height
                        'End If
                        If .Controls(i).Name = "Text" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                oPrint.printText(.Controls(i).Text, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                            End If
                        ElseIf .Controls(i).Name = "Image" Then
                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
                            End If
                        ElseIf .Controls(i).Name = "Line" Then
                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                Else
                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                End If
                            End If
                        ElseIf .Controls(i).Name = "Rectangle" Then
                            'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                            End If
                            'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
                        Else
                            If .Controls(i).Text <> "" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
                                    dtTable = Nothing
                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(0)
                                        'Manish Tanna (12 May 2012) -- start 
                                        Dim strFormula As String
                                        If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                            If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                strFormula = Format(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                            Else
                                                strFormula = CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                            End If
                                            oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        Else
                                            If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                strFormula = Format(dtTable.Rows(0)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                            Else
                                                strFormula = dtTable.Rows(0)(.Controls(i).Text).ToString
                                            End If
                                            oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        End If
                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(1)
                                        Dim strFormula As String
                                        If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                            If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                strFormula = Format(CDate(dtTable.Rows(k)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                            Else
                                                strFormula = CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                            End If
                                            oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        Else
                                            If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                strFormula = Format(dtTable.Rows(k)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                            Else
                                                strFormula = dtTable.Rows(k)(.Controls(i).Text).ToString
                                            End If
                                            oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        End If
                                    End If
                                    'Manish Tanna (12 May 2012) -- end 
                                    If .Controls(i).Text Like ".Formula_*" Then
                                        Dim strSelect As String = ""
                                        Dim frmFormula As New frmFormula
                                        frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                        Dim sFormat As String = ""
                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                            If sFormat <> "" Then
                                                Exit For
                                            End If
                                        Next
                                        frmFormula.mstrFormat = sFormat
                                        frmFormula.mdtTable = dtTable
                                        frmFormula.ExecuteFormula()
                                        strSelect = frmFormula.mstrSelect
                                        oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
                'If Not chkPageWise.Checked Then
                '    'If nGrp1 >= (txtItemPrint.Text * 105) Then
                '    '    nDetDety = nCounter
                '    '    WindowsBasedFooter(oPrint, True)
                '    '    oPrint.PageEject()
                '    '    nCounter = 0
                '    '    nDetDety = 0
                '    '    iCtr = 0
                '    '    nPrintCtr = 0
                '    '    nGroup1y = 0
                '    '    nGrpFooter1y = 0
                '    '    nGrp1 = 0
                '    '    WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                '    '    WindowsBasedHeader(oPrint)
                '    '    IsGroups()
                '    'End If
                'Else
                '    If nPrintCtr >= 150 Then
                '        nDetDety = nCounter
                '        oPrint.PageEject()
                '        nCounter = 0
                '        nDetDety = 0
                '        iCtr = 0
                '        nPrintCtr = 0
                '        nGroup1y = 0
                '        nGrpFooter1y = 0
                '        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                '        WindowsBasedHeader(oPrint)
                '        IsGroups()
                '    End If
                'End If
            End With
        End If
        Return nGrp1
    End Function
    Private Function FunctGroupFooter1(ByVal oPrint As eZeePrint, ByVal k As Integer)
        Dim lRetVal As Boolean = True
        Dim nGrpFot1 As Integer = 0
        If tvControls.Nodes(1).Nodes.Count > 0 Then
            With pnlGroupFooter1
                If Not lSuppreGroupFooter1Pnl Then
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        If nGrpFot1 < .Controls(i).Location.Y + .Controls(i).Height Then
                            nGrpFot1 = .Controls(i).Location.Y + .Controls(i).Height
                        End If
                    Next
                    If Not chkPageWise.Checked Then
                        If nGroup1y + iCtr + nGrpFot1 + nGrpFooter1y > ItemHeight() Then '(txtItemPrint.Text * 105)
                            nDetDety = ItemHeight() - nGroup1y - nGrpFooter1y '(txtItemPrint.Text * 105)
                            WindowsBasedFooter(oPrint, True)
                            oPrint.PageEject()
                            nCounter = 0
                            nDetDety = 0
                            iCtr = 0
                            nPrintCtr = 0
                            nGroup1y = 0
                            nGrpFooter1y = 0
                            'nGrpFot1 = 0
                            WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                            WindowsBasedHeader(oPrint)
                            IsGroups()
                        End If
                    Else
                        If nPrintCtr >= 150 Then
                            nDetDety = nCounter
                            oPrint.PageEject()
                            nCounter = 0
                            nDetDety = 0
                            iCtr = 0
                            nPrintCtr = 0
                            nGroup1y = 0
                            nGrpFooter1y = 0
                            WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                            WindowsBasedHeader(oPrint)
                            IsGroups()
                        End If
                    End If

                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        nPrintCtr += 1
                        If .Controls(i).Name = "Text" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                oPrint.printText(.Controls(i).Text, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                            End If
                        ElseIf .Controls(i).Name = "Image" Then
                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
                            End If
                        ElseIf .Controls(i).Name = "Line" Then
                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                Else
                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                End If
                            End If
                        ElseIf .Controls(i).Name = "Rectangle" Then
                            'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                            End If
                            'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
                        Else
                            If .Controls(i).Text <> "" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
                                    dtTable = Nothing
                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(0)
                                        If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                            oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        Else
                                            oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        End If
                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(1)
                                        If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                            oPrint.printText(CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        Else
                                            'Manish Tanna (12 May 2012) -- start 
                                            Dim strFormula As String
                                            If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
                                                Dim strCon As String = ""
                                                For p As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                                                    strCon += tvControls.Nodes(1).Nodes(p).Text + " = '" + dtTable.Rows(k)(tvControls.Nodes(1).Nodes(p).Text).ToString + "',"
                                                Next
                                                strCon = strCon.Substring(0, strCon.Length - 1)
                                                Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", strCon)
                                                If CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                    strFormula = Format(nSum, CType(.Controls(i), TemplateBoxField).Format)
                                                Else
                                                    strFormula = nSum.ToString
                                                End If
                                                oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            Else
                                                If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                    strFormula = Format(dtTable.Rows(k)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                                Else
                                                    strFormula = dtTable.Rows(k)(.Controls(i).Text).ToString
                                                End If
                                                oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                            End If
                                            'Manish Tanna (12 May 2012) -- end    
                                        End If
                                    End If
                                    If .Controls(i).Text Like ".Formula_*" Then
                                        Dim strSelect As String = ""
                                        Dim frmFormula As New frmFormula
                                        frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                        Dim sFormat As String = ""
                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                            If sFormat <> "" Then
                                                Exit For
                                            End If
                                        Next
                                        frmFormula.mstrFormat = sFormat
                                        frmFormula.mdtTable = dtTable
                                        frmFormula.ExecuteFormula()
                                        strSelect = frmFormula.mstrSelect
                                        oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            End With
        End If
        Return nGrpFot1
    End Function
    Private Sub WindowsBasedFooter(ByVal oPrint As eZeePrint, Optional ByVal lDetCall As Boolean = False)
        Try
            With pnlFooter
                If Not lSuppreFooterPnl Then
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        nPrintCtr += 1
                        If .Controls(i).Name = "Text" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
                                    '                                                                     nGrpFooter1y + nGroup1y +               nDetDety + nDetHeadery + nHeadery + nCompHeadery +
                                    oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                End If
                            End If
                        ElseIf .Controls(i).Name = "Image" Then
                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
                            End If
                        ElseIf .Controls(i).Name = "Line" Then
                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
                                Else
                                    Dim pPen As New Pen(.Controls(i).ForeColor)
                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
                                    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
                                    oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
                                End If
                            End If
                        ElseIf .Controls(i).Name = "Rectangle" Then
                            'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
                            End If
                            'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
                        Else
                            If .Controls(i).Text.Trim <> "" Then
                                'If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    dtTable = Nothing
                                    If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(0)
                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(1)
                                        End If
                                        If .Controls(i).Text Like ".Formula_*" Then
                                            Dim strSelect As String = ""
                                            Dim frmFormula As New frmFormula
                                            frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                            Dim sFormat As String = ""
                                            For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                If sFormat <> "" Then
                                                    Exit For
                                                End If
                                            Next
                                            frmFormula.mstrFormat = sFormat
                                            frmFormula.mdtTable = dtTable
                                            frmFormula.ExecuteFormula()
                                            strSelect = frmFormula.mstrSelect
                                            oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                        Else
                                            'Manish Tanna (12 May 2012) -- start 
                                            If dtTable IsNot Nothing Then
                                                Dim strFormula As String
                                                If dtTable.Columns(.Controls(i).Text).DataType.Name = "DateTime" Then
                                                    If CType(.Controls(i), TemplateBoxField).Format <> "" Then
                                                        strFormula = Format(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString), CType(.Controls(i), TemplateBoxField).Format)
                                                    Else
                                                        strFormula = CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy")
                                                    End If
                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                Else
                                                    If dtTable.Columns(.Controls(i).Text).DataType.Name.ToUpper <> "STRING" AndAlso CType(.Controls(i), TemplateBoxField).Format <> "" AndAlso IsNumeric(CType(.Controls(i), TemplateBoxField).Format) Then
                                                        strFormula = Format(dtTable.Rows(0)(.Controls(i).Text), CType(.Controls(i), TemplateBoxField).Format)
                                                    Else
                                                        strFormula = dtTable.Rows(0)(.Controls(i).Text).ToString
                                                    End If
                                                    oPrint.printText(strFormula, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
                                                End If
                                            End If
                                            'Manish Tanna (12 May 2012) -- start 
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If chkPageWise.Checked Then
                            If nPrintCtr >= 150 Then
                                oPrint.PageEject()
                                nDetDety = 0
                                nPrintCtr = 0
                                WindowsBasedCompHeader(oPrint, sMstXMLPaths)
                                WindowsBasedHeader(oPrint)
                            End If
                        End If
                    Next
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
#End Region
    Private Sub DefaultPnlHeight()
        pnlCompHeader.Height = 90
        pnlHeader.Height = 70
        pnlDetail.Height = 90
        pnlDetailFooter.Height = 43
        pnlFooter.Height = 105
        pnlGroup1.Height = 32
        pnlGroupFooter1.Height = 30
        pnlCanvas.Width = 956
        pnlRuler.Width = 1030
    End Sub

    'Private Sub cms_Copy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cms_Copy.Click
    '    CopyValue()
    'End Sub

    'Private Sub cms_Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cms_Delete.Click
    '    DeleteAllFields()
    'End Sub

    'Private Sub cms_Paste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cms_Paste.Click
    '    Pastevalue()
    'End Sub

    Private Sub txtItemPrint_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtItemPrint.KeyPress
        If InStr("0123456789.", e.KeyChar) = 0 Then
            If e.KeyChar.ToString.Split(".").Length > 1 Then
                If e.KeyChar.ToString.Split(".")(1).Length > 2 Then
                    e.KeyChar.ToString.Split(".")(1).Remove(e.KeyChar.ToString.Split(".")(1).Length - 1)
                End If
            End If
            If Asc(e.KeyChar) <> 8 Then
                e.Handled = True
            End If
        End If
    End Sub
    Public Function IsNumber(ByVal Obj As Object)
        If InStr("0123456789.", Obj) = 0 Then
            If Asc(Obj) <> 8 Then
                Return True
            End If
        End If
        Return False
    End Function

    Private Sub txtXMLPath_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMstXMLPath.KeyDown, txtDetXMLPath.KeyDown
        e.Handled = True
    End Sub

    Private Sub txtXMLPath_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMstXMLPath.KeyPress, txtDetXMLPath.KeyPress
        e.Handled = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        CancelIt()
    End Sub
    Private Sub CancelIt()
        txtMstXMLPath.Text = ""
        txtDetXMLPath.Text = ""
        tvControls.Nodes.Clear()
        ClearDataPnl()
        nFormulaCtr = 0
        txtItemPrint.Text = "6.00"
        cmbType.ForeColor = Color.Black
        cmbType.DropDownStyle = ComboBoxStyle.DropDown
        cmbType.Enabled = True
        cmbType.Text = ""
        cmbType.Tag = "0"
        chkSaveAsNew.Checked = False
        txtCanvasWidth.Text = 10
        chkLandspace.Checked = False
        txtMultPrint.Text = 1
        chkRepHeader.Checked = True

        lSuppreCompHeaderPnl = False
        lSuppreHeaderPnl = False
        lSuppreDetPnl = False
        lSuppreDetFotPnl = False
        lSuppreFooterPnl = False
        lSuppreGroup1Pnl = False
        lSuppreGroupFooter1Pnl = False

        cmbTypeList()

        DefaultPnlHeight()
        'Delete Copy
        If aCpyImgPath IsNot Nothing AndAlso aCpyImgPath.Length <> 0 Then

            For i As Int16 = 0 To aCpyImgPath.Length - 1
                'Dim stream As New System.IO.FileStream(aCpyImgPath(i), IO.FileMode.Open)
                'stream.Close()
                If aCpyImgPath(i) <> "" Then System.IO.File.Delete(aCpyImgPath(i))
            Next
            ReDim aCpyImgPath(0)
        End If

        btnLoad.Enabled = True

        btnMstPath.Enabled = True
        '' 'btnDetPath.Enabled = True
        btnSave.Enabled = False
        btnDelete.Enabled = False
        btnCancel.Enabled = False
        btnConverter.Enabled = False
        pnlCompHeader.Refresh()
        pnlHeader.Refresh()
        pnlDetail.Refresh()
        pnlDetailFooter.Refresh()
        pnlFooter.Refresh()
        pnlGroup1.Refresh()
        pnlGroupFooter1.Refresh()
    End Sub

    Private Sub cmbType_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbType.Validating
        TypeValidating(sender)
    End Sub
    Private Function TypeValidating(ByVal sender As Object)
        Dim lRetVal As Boolean = True
        Try
            If Not btnLoad.Enabled Then
                Dim ds As New DataSet
                ds.ReadXml(sMst)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim dr() As DataRow = ds.Tables(0).Select("trnasid <> '" & IIf(chkSaveAsNew.Checked, "0", sender.tag) & "' and PrintType = '" & sender.text & "'", "")
                        If dr.Length > 0 Then
                            MessageBox.Show("Type alredy exist..... ", "eZee Message")
                            cmbType.Focus()
                            lRetVal = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
        Return lRetVal
    End Function
    Private Function ImagePath() As String
        Dim lRetVal As String = ""
        If Not System.IO.Directory.Exists(AppSettings._Object._ApplicationPath + "\Image") Then
            System.IO.Directory.CreateDirectory(AppSettings._Object._ApplicationPath + "\Image")
        End If
        lRetVal = AppSettings._Object._ApplicationPath + "\Image"
        Return lRetVal
    End Function
    Public Function ImageClone(ByVal pImg As Image) As Image
        Dim TmpBitmap As Image = New Drawing.Bitmap(pImg)
        Dim TmpBitmapGr As Graphics = Graphics.FromImage(TmpBitmap)
        TmpBitmapGr.DrawImage(pImg, New Rectangle(0, 0, pImg.Width, pImg.Height))
        Return TmpBitmap
    End Function

    Private Sub tvControls_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tvControls.KeyDown
        If e.KeyCode = Keys.Insert Then
            Formula()
        ElseIf e.KeyCode = Keys.Delete Then
            If tvControls.SelectedNode.Text Like ".Formula_*" Then
                Dim lFlag As Boolean = True
                With pnlCompHeader
                    For i As Int32 = 0 To .Controls.Count - 1
                        If .Controls(i).Text = tvControls.SelectedNode.Text Then
                            MessageBox.Show("Company Header panel contains the data.... ", "eZee Message")
                            lFlag = False
                            Exit Sub
                        End If
                    Next
                    If lFlag Then

                        With pnlHeader
                            For i As Int32 = 0 To .Controls.Count - 1
                                If .Controls(i).Text = tvControls.SelectedNode.Text Then
                                    MessageBox.Show("Header panel contains the data.... ", "eZee Message")
                                    lFlag = False
                                    Exit Sub
                                End If
                            Next
                        End With
                        If lFlag Then
                            With pnlGroup1
                                For i As Int32 = 0 To .Controls.Count - 1
                                    If .Controls(i).Text = tvControls.SelectedNode.Text Then
                                        MessageBox.Show("Group 1 panel contains the data.... ", "eZee Message")
                                        lFlag = False
                                        Exit Sub
                                    End If
                                Next
                            End With
                            If lFlag Then
                                With pnlDetail
                                    For i As Int32 = 0 To .Controls.Count - 1
                                        If .Controls(i).Text = tvControls.SelectedNode.Text Then
                                            MessageBox.Show("Detail panel contains the data.... ", "eZee Message")
                                            lFlag = False
                                            Exit Sub
                                        End If
                                    Next
                                End With
                                If lFlag Then
                                    With pnlGroupFooter1
                                        For i As Int32 = 0 To .Controls.Count - 1
                                            If .Controls(i).Text = tvControls.SelectedNode.Text Then
                                                MessageBox.Show("Group Footer 1 panel contains the data.... ", "eZee Message")
                                                lFlag = False
                                                Exit Sub
                                            End If
                                        Next
                                    End With
                                    If lFlag Then
                                        With pnlDetailFooter
                                            For i As Int32 = 0 To .Controls.Count - 1
                                                If .Controls(i).Text = tvControls.SelectedNode.Text Then
                                                    MessageBox.Show("Detail Footer panel contains the data.... ", "eZee Message")
                                                    lFlag = False
                                                    Exit Sub
                                                End If
                                            Next
                                        End With
                                        If lFlag Then
                                            With pnlFooter
                                                For i As Int32 = 0 To .Controls.Count - 1
                                                    If .Controls(i).Text = tvControls.SelectedNode.Text Then
                                                        MessageBox.Show("Footer panel contains the data.... ", "eZee Message")
                                                        lFlag = False
                                                        Exit Sub
                                                    End If
                                                Next
                                            End With
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End With

                If lFlag Then
                    If nFormulaCtr = tvControls.SelectedNode.Text.Split("_")(1) Then
                        nFormulaCtr -= 1
                    End If
                    tvControls.SelectedNode.Remove()
                End If
            End If
        End If
    End Sub

    Private Sub tvControls_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvControls.DoubleClick
        Formula()
    End Sub
    Private Sub Formula()
        If tvControls.SelectedNode.Parent IsNot Nothing AndAlso tvControls.SelectedNode.Parent.Text <> "1.Common Tools" AndAlso tvControls.SelectedNode.Parent.Text <> "Group" Then
            Dim nInd As Int16
            With tvControls.Nodes(tvControls.SelectedNode.Parent.Index)
                Dim frmFormulas As New frmFormula()
                frmFormulas.lblTree.Text = tvControls.SelectedNode.Parent.Text
                If tvControls.SelectedNode.Parent.Text = "2.Master" Then
                    frmFormulas.mdtTable = dsMstXML.Tables(0)
                Else
                    frmFormulas.mdtTable = dsMstXML.Tables(1)
                End If
                If tvControls.SelectedNode.Text Like ".Formula_*" Then
                    frmFormulas.txtExpression.Text = tvControls.SelectedNode.ToolTipText.Split("|")(0)
                    If tvControls.SelectedNode.ToolTipText.Split("|").Length > 1 Then
                        frmFormulas.mstrFormat = tvControls.SelectedNode.ToolTipText.Split("|")(1)
                        If tvControls.SelectedNode.ToolTipText.Split("|").Length > 2 Then
                            frmFormulas.mstrFormat += "|" + tvControls.SelectedNode.ToolTipText.Split("|")(2)
                        End If
                    End If
                    nInd = tvControls.SelectedNode.Index
                End If

                frmFormulas.DisplayDialog(tvControls.Nodes(tvControls.SelectedNode.Parent.Index))
                If Not tvControls.SelectedNode.Text Like ".Formula_*" Then
                    Dim a() As String = Nothing
                    Dim iCtr As Integer = 0
                    If frmFormulas.mblnFlag Then
                        For i As Integer = 0 To tvControls.Nodes.Count - 1
                            For j As Integer = 0 To tvControls.Nodes(i).Nodes.Count - 1
                                If tvControls.Nodes(i).Nodes(j).Text Like ".Formula_*" Then
                                    ReDim Preserve a(iCtr)
                                    a(iCtr) = tvControls.Nodes(i).Nodes(j).Text
                                    iCtr += 1
                                End If
                            Next
                        Next
                        If a IsNot Nothing AndAlso a IsNot DBNull.Value AndAlso a.Length > 0 Then
                            Array.Sort(a)
                            iCtr = CInt(a(iCtr - 1).Split("_")(1))
                        End If
                        nInd = .Nodes.Add(".Formula_" + (iCtr + 1).ToString).Index
                    End If
                    .Nodes(nInd).Tag = frmFormulas.mstrExpression.Trim + "|" + frmFormulas.mstrFormat + "|1"
                    .Nodes(nInd).ToolTipText = frmFormulas.mstrExpression.Trim + "|" + frmFormulas.mstrFormat
                    tvControls.Refresh()
                    tvControls.SelectedNode = .Nodes(nInd)
                End If
            End With
        ElseIf tvControls.SelectedNode.Text = "Group" Then
            Dim frmGrping As New frmGrouping
            frmGrping.dtTable = dsMstXML.Tables(1)
            For i As Integer = 0 To tvControls.Nodes(tvControls.SelectedNode.Index).Nodes.Count - 1
                frmGrping.strSelect += tvControls.Nodes(tvControls.SelectedNode.Index).Nodes(i).Text + ","
            Next
            frmGrping.ShowMeNow(tvControls.Nodes(3))
            If frmGrping.lOk Then
                tvControls.Nodes(tvControls.SelectedNode.Index).Nodes.Clear()
                For i As Integer = 0 To frmGrping.strSelect.Split(",").Length - 1
                    If frmGrping.strSelect.Split(",")(i) <> "" Then tvControls.Nodes(tvControls.SelectedNode.Index).Nodes.Add(frmGrping.strSelect.Split(",")(i))
                Next
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Close()
    End Sub
#Region "Radom "
    Dim tsRight As New ToolStrip
    Dim tsLeft As New ToolStrip
    Dim frm As New frmTool_Property
    'Private Sub LeftsideButton()
    '    With ToolStripContainer1
    '        'LeftSide      
    '        tsLeft.AllowDrop = False
    '        tsLeft.GripStyle = ToolStripGripStyle.Hidden
    '        tsLeft.BackColor = Color.PaleTurquoise
    '        tsLeft.Font = New Font("Courier", 9, FontStyle.Bold)
    '        tsLeft.Items.Add("Tool Box")
    '        tsLeft.TextDirection = ToolStripTextDirection.Vertical270
    '        .LeftToolStripPanel.Controls.Add(tsLeft)
    '        .LeftToolStripPanel.BackColor = Color.WhiteSmoke
    '    End With
    'End Sub
    'Private Sub RightsideButton()
    '    With ToolStripContainer1
    '        'Right Side      
    '        tsRight.AllowDrop = False
    '        tsRight.GripStyle = ToolStripGripStyle.Hidden
    '        tsRight.BackColor = Color.PaleTurquoise
    '        tsRight.Font = New Font("Courier", 9, FontStyle.Bold)
    '        tsRight.Items.Add("Property")
    '        tsRight.Items(0).TextDirection = ToolStripTextDirection.Vertical90
    '        tsRight.TextDirection = ToolStripTextDirection.Vertical270
    '        .RightToolStripPanel.Controls.Add(tsRight)
    '        .RightToolStripPanel.BackColor = Color.WhiteSmoke
    '    End With
    'End Sub

    Private Sub frmTrial_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '' 'LeftsideButton()
        '' 'RightsideButton()
        '' 'AddHandler tsLeft.Click, AddressOf tsLeft_Click
        '' 'AddHandler tsRight.Click, AddressOf tsRight_Click
    End Sub
    Private Sub tsLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Do Until tlpLeft.Width > 100
            tlpLeft.Width += 50
            pnlPrint.Width -= 50
        Loop
    End Sub
    Private Sub tsRight_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Do Until tlpRight.Width > 100
            tlpRight.Width += 50
            pnlPrint.Width -= 50
        Loop
    End Sub


    Private Sub btnMaxMin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Do Until tlpLeft.Width = 0
            tlpLeft.Width -= 50
            pnlPrint.Width += 50
        Loop
    End Sub

    Private Sub btnMinMaxR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMinMaxR.Click
        Do Until tlpRight.Width = 0
            tlpRight.Width -= 50
            pnlPrint.Width += 50
        Loop
    End Sub

    Private Sub btnCloseR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseR.Click
        If sender.name = "btnCloseR" Then
            pnlPrint.Width += tlpRight.Width
            tlpRight.Width = 0
        Else
            pnlPrint.Width += tlpLeft.Width
            tlpLeft.Width = 0
        End If
    End Sub
#End Region

#Region "Ruler"
    Dim countX1 As Decimal = 0

    Private Sub pnlRuler_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlRuler.Paint
        Dim beginX As Integer = 0
        Dim num As String
        Dim countX As Integer = countX1
        Dim g As Graphics = e.Graphics
        g.Clear(Color.GhostWhite)
        Dim l As New LinearGradientBrush(New Rectangle(0, 0, pnlRuler.Width, pnlRuler.Height), Color.Wheat, Color.BurlyWood, 0, True)
        g.FillRectangle(l, 0, 0, Me.Width, Me.Height)

        Dim horzontalRec As New Rectangle(0, 0, (MyBase.Width - &H16), 0)
        'Dim verticalRec As New Rectangle(0, 20, 20, (MyBase.Height - &H16))
        g.DrawRectangle(Pens.Black, horzontalRec)
        'g.DrawRectangle(Pens.Black, verticalRec)
        Do While (beginX < MyBase.Width)
            If ((countX Mod 10) = 0) Then
                num = (countX / 10).ToString()
                g.DrawLine(Pens.Black, New Point(beginX, 5), New Point(beginX, 15))
                g.DrawString(num, New Font("Arial", 8.0!), Brushes.Black, New Point(beginX, 8))
            ElseIf ((countX Mod 5) = 0) Then
                g.DrawLine(Pens.Black, New Point(beginX, 5), New Point(beginX, 12))
            Else
                g.DrawLine(Pens.Black, New Point(beginX, 5), New Point(beginX, 8))
            End If
            countX += 1
            beginX = (beginX + 10)
        Loop
        g.DrawLine(Pens.Red, New Point(Me.mouseCurrentPoisiton.X - (countX1 * 10), 0), New Point(Me.mouseCurrentPoisiton.X - (countX1 * 10), 20))
    End Sub
#End Region

    Private Sub pnlMainCanvas_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlMainCanvas.MouseWheel
        pnlVScroll.VerticalScroll.Value = pnlMainCanvas.VerticalScroll.Value
    End Sub

    Private Sub pnlMainCanvas_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles pnlMainCanvas.Scroll
        If e.ScrollOrientation = ScrollOrientation.HorizontalScroll Then
            countX1 = CInt(e.NewValue / 10)
            AddHandler pnlRuler.Paint, AddressOf pnlRuler_Paint
            pnlRuler.Invalidate()
        Else
            nVScroll = e.NewValue
            pnlVScroll.VerticalScroll.Value = pnlMainCanvas.VerticalScroll.Value
        End If
    End Sub
    Private Sub tsmSuppress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmSuppress.Click
        If cmslbl.SourceControl.Name = "lblCompHeader" Then
            If lSuppreCompHeaderPnl Then
                lSuppreCompHeaderPnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreCompHeaderPnl = True
            End If
            pnlCompHeader.Refresh()
        ElseIf cmslbl.SourceControl.Name = "lblHeader" Then
            If lSuppreHeaderPnl Then
                lSuppreHeaderPnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreHeaderPnl = True
            End If
            pnlHeader.Refresh()
        ElseIf cmslbl.SourceControl.Name = "lblGroup1" Then
            If lSuppreGroup1Pnl Then
                lSuppreGroup1Pnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreGroup1Pnl = True
            End If
            pnlGroup1.Refresh()
        ElseIf cmslbl.SourceControl.Name = "lblDetail" Then
            If lSuppreDetPnl Then
                lSuppreDetPnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreDetPnl = True
            End If
            pnlDetail.Refresh()
        ElseIf cmslbl.SourceControl.Name = "lblGroupFooter1" Then
            If lSuppreGroupFooter1Pnl Then
                lSuppreGroupFooter1Pnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreGroupFooter1Pnl = True
            End If
            pnlGroupFooter1.Refresh()
        ElseIf cmslbl.SourceControl.Name = "lblFooter" Then
            If lSuppreDetFotPnl Then
                lSuppreDetFotPnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreDetFotPnl = True
            End If
            pnlDetailFooter.Refresh()
        ElseIf cmslbl.SourceControl.Name = "lblBottomFooter" Then
            If lSuppreFooterPnl Then
                lSuppreFooterPnl = False
                tsmSuppress.Image = Nothing
            Else
                'tsmSuppress.Image = Image.FromFile("c:\RightMenu.gif")
                lSuppreFooterPnl = True
            End If
            pnlFooter.Refresh()
        End If
    End Sub

    Private Sub pnlCompHeader_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlCompHeader.Paint
        If lSuppreCompHeaderPnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlCompHeader.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If
    End Sub
    Private Sub pnlHeader_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlHeader.Paint
        If lSuppreHeaderPnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlHeader.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If
    End Sub
    Private Sub pnlGroup1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlGroup1.Paint
        If lSuppreGroup1Pnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlGroup1.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If
    End Sub

    Private Sub pnlDetail_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlDetail.Paint
        If lSuppreDetPnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlDetail.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If
    End Sub

    Private Sub pnlGroupFooter1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlGroupFooter1.Paint
        If lSuppreGroupFooter1Pnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlGroupFooter1.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If

    End Sub

    Private Sub pnlDetailFooter_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlDetailFooter.Paint
        If lSuppreDetFotPnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlDetailFooter.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If
    End Sub
    Private Sub pnlFooter_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlFooter.Paint
        If lSuppreFooterPnl Then
            'e.Graphics.DrawString("Supress", New Font("Arial", 9, FontStyle.Bold), Brushes.LightGray, 10, 20)
            For i As Integer = 0 To sender.Width - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, i, 0, i, sender.Height - 1)
            Next
            For i As Integer = 0 To pnlFooter.Height - 1
                i += 20
                e.Graphics.DrawLine(Pens.LightGray, 0, i, sender.Width - 1, i)
            Next
        End If
    End Sub

    Private Sub txtCanvasWidth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCanvasWidth.KeyPress, txtMultPrint.KeyPress
        e.Handled = IsNumberD(e.KeyChar)
    End Sub
    Public Function IsNumberD(ByVal Obj As Object)
        If InStr("0123456789.", Obj) = 0 Then
            If Asc(Obj) <> 8 Then
                Return True
            End If
        End If
        Return False
    End Function

    Private Sub txtCanvasWidth_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCanvasWidth.Validating
    End Sub
    Private Sub txtCanvasWidth_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCanvasWidth.KeyDown, txtMultPrint.KeyDown
        If e.KeyCode = Keys.Return OrElse e.KeyCode = Keys.Tab Then
            If IsNumeric(sender.text) Then
                pnlCanvas.Width = ((sender.text - nLeft - nRight) * 100) + 6
                countX1 = 0
                AddHandler pnlRuler.Paint, AddressOf pnlRuler_Paint
                pnlRuler.Invalidate()
            End If
        End If
    End Sub

    Private Sub tsm_PageSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsm_PageSetup.Click
        Dim frmParperSett As New frmParperSetting
        frmParperSett.sPaperSize = sPaperSize
        frmParperSett.lLandscape = chkLandspace.Checked
        If frmParperSett.lLandscape Then
            frmParperSett.nWidht = CanvasHeight
            frmParperSett.nHeight = txtCanvasWidth.Text
        Else
            frmParperSett.nWidht = txtCanvasWidth.Text
            frmParperSett.nHeight = CanvasHeight
        End If

        frmParperSett.nBottom = nBottom
        frmParperSett.nTop = nTop
        frmParperSett.nLeft = nLeft
        frmParperSett.nRight = nRight


        frmParperSett.ShowDialog()
        If frmParperSett.lOk Then
            sPaperSize = frmParperSett.sPaperSize
            chkLandspace.Checked = frmParperSett.lLandscape
            If chkLandspace.Checked Then
                txtCanvasWidth.Text = frmParperSett.nHeight
                CanvasHeight = frmParperSett.nWidht
            Else
                txtCanvasWidth.Text = frmParperSett.nWidht
                CanvasHeight = frmParperSett.nHeight
            End If

            nBottom = frmParperSett.nBottom
            nTop = frmParperSett.nTop
            nLeft = frmParperSett.nLeft
            nRight = frmParperSett.nRight

            If IsNumeric(txtCanvasWidth.Text) Then
                pnlCanvas.Width = ((txtCanvasWidth.Text - nLeft - nRight) * 100) + 6
                countX1 = 0
                AddHandler pnlRuler.Paint, AddressOf pnlRuler_Paint
                pnlRuler.Invalidate()
            End If
        End If
    End Sub
    Private Function TreeViewFind(ByVal treenode1 As TreeNodeCollection, ByVal txtControl As String, ByVal sTreeView As String)
        Dim sRetval As String = ""
        For k As Integer = 0 To treenode1.Count - 1
            If treenode1.Item(k).Text = txtControl Then
                If sTreeView = "2.Master" Then
                    dtTable = dsMst.Tables(0)
                ElseIf sTreeView = "3.Detail" Then
                    dtTable = dsMst.Tables(1)
                End If
                If treenode1.Item(k).ToolTipText.ToString.Split("|").Length > 1 Then
                    sRetval = treenode1.Item(k).ToolTipText.ToString.Split("|")(1)
                    If treenode1.Item(k).ToolTipText.ToString.Split("|").Length > 2 Then
                        sRetval += "|" + treenode1.Item(k).ToolTipText.ToString.Split("|")(2)
                    End If

                End If
                Exit For
            End If
        Next
        Return sRetval
    End Function
    Private Sub pnlHeaderRuler_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlHeaderRuler.Paint, pnlDetailRuler.Paint, pnlDetFooterRuler.Paint, pnlFooterRuler.Paint, pnlGrpRuler1.Paint, pnlGrpFooterRuler1.Paint, pnlCompHeaderRuler.Paint
        Dim num As String
        Dim g As Graphics = e.Graphics
        'Dim horzontalRec As New Rectangle(20, 0, (MyBase.Width - &H16), 20)
        If pnlHeaderRuler.Height > 0 Then
            Dim l As New LinearGradientBrush(New Rectangle(0, 0, pnlHeaderRuler.Width, pnlHeaderRuler.Height), Color.Wheat, Color.BurlyWood, 0, True)
            g.FillRectangle(l, 0, 0, Me.Width, Me.Height)

            Dim verticalRec As New Rectangle(0, 0, 0, (pnlHeaderRuler.Height - &H16))
            g.DrawRectangle(Pens.Black, verticalRec)
            Dim CountY As Integer = 1
            Dim beginY As Integer = 0
            Do While (beginY < MyBase.Height)
                If ((CountY Mod 10) = 0) Then
                    num = (CountY / 10).ToString()
                    g.DrawLine(Pens.Black, New Point(5, beginY), New Point(15, beginY))
                    Dim format As New StringFormat(StringFormatFlags.DirectionVertical)
                    g.DrawString(num, New Font("Arial", 8.0!), Brushes.Black, New Point(8, beginY), format)
                ElseIf ((CountY Mod 5) = 0) Then
                    g.DrawLine(Pens.Black, New Point(5, beginY), New Point(12, beginY))
                Else
                    g.DrawLine(Pens.Black, New Point(5, beginY), New Point(8, beginY))
                End If
                CountY += 1
                beginY = (beginY + 10)
            Loop
        End If
        'g.DrawLine(Pens.Red, New Point(0, Me.mouseCurrentPoisiton.Y), New Point(20, Me.mouseCurrentPoisiton.Y))
    End Sub
    Private Sub spltHeader_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles spltHeader.Move, spltDetailFooter.Move, spltFooter.Move, spltDetail.Move, spltGoup1.Move, spltGroupFooter1.Move, spltCompHeader.Move
        If sender.name = "spltHeader" Then
            pnlHeaderRuler.Size = New Size(pnlHeaderRuler.Size.Width, pnlHeader.Height)
            pnlHeaderRuler.Location = New Size(pnlHeader.Location.X, pnlHeader.Location.Y) ' - nVScroll)
        ElseIf sender.name = "spltDetail" Then
            pnlDetailRuler.Location = New Size(pnlDetail.Location.X, pnlDetail.Location.Y) ' - nVScroll)
            pnlDetailRuler.Size = New Size(pnlDetailRuler.Size.Width, pnlDetail.Height)
        ElseIf sender.name = "spltDetailFooter" Then
            pnlDetFooterRuler.Location = New Size(pnlDetailFooter.Location.X, pnlDetailFooter.Location.Y) ' - nVScroll)
            pnlDetFooterRuler.Size = New Size(pnlDetFooterRuler.Size.Width, pnlDetailFooter.Height)
        ElseIf sender.name = "spltFooter" Then
            pnlFooterRuler.Size = New Size(pnlFooterRuler.Size.Width, pnlFooter.Size.Height)
            pnlFooterRuler.Location = New Point(pnlFooter.Location.X, pnlFooter.Location.Y) '- nVScroll)
        ElseIf sender.name = "spltGroupFooter1" Then
            pnlGrpFooterRuler1.Size = New Size(pnlGrpFooterRuler1.Size.Width, pnlGroupFooter1.Size.Height)
            pnlGrpFooterRuler1.Location = New Point(pnlGroupFooter1.Location.X, pnlGroupFooter1.Location.Y) '- nVScroll)
        ElseIf sender.name = "spltGoup1" Then
            pnlGrpRuler1.Size = New Size(pnlGrpRuler1.Size.Width, pnlGroup1.Size.Height)
            pnlGrpRuler1.Location = New Point(pnlGroup1.Location.X, pnlGroup1.Location.Y) '- nVScroll)
        ElseIf sender.name = "spltCompHeader" Then
            pnlCompHeaderRuler.Size = New Size(pnlCompHeaderRuler.Size.Width, pnlCompHeader.Size.Height)
            pnlCompHeaderRuler.Location = New Point(pnlCompHeader.Location.X, pnlCompHeader.Location.Y) '- nVScroll)
        End If
    End Sub

    Private Sub txtItemPrint_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtItemPrint.Leave
        Dim nDec As Integer = 0
        If sender.Text.Split(".").Length = 1 Then
            nDec = 2
            sender.Text += "." + replicate("0", nDec)
        Else
            nDec = 2 - sender.Text.Split(".")(1).Length
            sender.Text += replicate("0", nDec)
        End If
    End Sub

    Private Sub txtItemPrint_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtItemPrint.Validating
        Dim lVld As Boolean = True
        If sender.Text.Split(".").Length > 2 Then
            lVld = False
        ElseIf sender.Text.Split(".").Length > 1 AndAlso sender.Text.Split(".")(1).Length > 2 Then
            lVld = False
        End If
        If Not lVld Then
            MessageBox.Show("Please Insert Proper Number", "Numeric error", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, False)
            e.Cancel = True
        End If
    End Sub
    Private Function ItemHeight() As Decimal
        Dim nItemHeight As Decimal = 0
        Dim lFlag As Boolean = True
        Dim nFooterHght As Double = 0
        Dim nHeaderHght As Double = 0
        Dim nCompHeaderHght As Double = 0
        If Not chkRepHeader.Checked Then
            lFlag = (nPageNo = 1)
        End If

        Try
            If chkPageWise.Checked Then
                If pnlFooter.Controls.Count > 0 Then
                    For i As Integer = 0 To pnlFooter.Controls.Count - 1
                        If nFooterHght < pnlFooter.Controls(i).Location.Y + pnlFooter.Controls(i).Height Then
                            nFooterHght = pnlFooter.Controls(i).Location.Y + pnlFooter.Controls(i).Height
                        End If
                    Next
                End If

                If pnlHeader.Controls.Count > 0 Then
                    For i As Integer = 0 To pnlHeader.Controls.Count - 1
                        If nHeaderHght < pnlHeader.Controls(i).Location.Y + pnlHeader.Controls(i).Height Then
                            nHeaderHght = pnlHeader.Controls(i).Location.Y + pnlHeader.Controls(i).Height
                        End If
                    Next
                End If
                If pnlCompHeader.Controls.Count > 0 Then
                    For i As Integer = 0 To pnlCompHeader.Controls.Count - 1
                        If nCompHeaderHght < pnlCompHeader.Controls(i).Location.Y + pnlCompHeader.Controls(i).Height Then
                            nCompHeaderHght = pnlCompHeader.Controls(i).Location.Y + pnlCompHeader.Controls(i).Height
                        End If
                    Next
                End If
                'Item height            Page Height 
                nItemHeight = ((CanvasHeight - nTop - nBottom) * 100) - IIf(lFlag, nCompHeaderHght, 0) - nHeaderHght - nFooterHght
            Else
                nItemHeight = (txtItemPrint.Text * 100)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "eZee Message - ItemHeight")
        End Try
        Return nItemHeight
    End Function

#Region "Word Transfer"

    Private Sub WordTransfer()
        Dim TextWriter As IO.TextWriter
        'Dim io As IO.Stream
        Try
            TextWriter.Write("\red{0}\green{1}\blue{2};", "Red")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "eZee Message")
        End Try

        'poApp = New Word.Application
        'poApp.DisplayAlerts = Word.wdAlertLevel.wdAlertsNone
        'poDoc = poApp.Documents.Open("C:\temp\test.doc")
        ''If you want to change what printer it goes to, it's poApp.ActivePrinter = "System Printer Name"
        ''print it out
        'poDoc.PrintOut()
        'poDoc.Close(Word.wdSaveOptions.wdDoNotSaveChanges)
        ''clean up
        'poDoc = Nothing
        ''close word
        'poApp.Quit(Word.wdSaveOptions.wdDoNotSaveChanges)
        'poApp = Nothing

    End Sub

#End Region
#Region "HTML "
    Dim ComY As Integer = 0
    Private Sub HTMLConvertor()
        Dim strBuilder As New StringBuilder
        Dim nSize As Integer = 1
        Dim strFile As String
        If Not Directory.Exists(AppSettings._Object._ApplicationPath & "\HTML") Then
            Directory.CreateDirectory(AppSettings._Object._ApplicationPath & "\HTML")
        End If
        If Not Directory.Exists(AppSettings._Object._ApplicationPath & "\HTML\Images") Then
            Directory.CreateDirectory(AppSettings._Object._ApplicationPath & "\HTML\Images")
        End If
        strHtmlDir = AppSettings._Object._ApplicationPath & "\HTML\Images"
        strFile = AppSettings._Object._ApplicationPath & "\HTML\Demo.Html"

        nPrintCtr = 0
        If chkLandspace.Checked Then
            nSize = 2
        Else
            nSize = 1
        End If
        nCompHeadery = 0
        nHeadery = 0
        nDetHeadery = 0
        nDetDety = 0
        nDetFootery = 0
        nGroup1y = 0
        nGrpFooter1y = 0
        dtTable = New DataTable
        Dim Graphics As Graphics
        Graphics = CreateGraphics()
        Dim dsTempMst As New DataSet
        dsTempMst.ReadXml(sMst)
        strBuilder.Append(" <TITLE> " & cmbType.Text & " </TITLE> " & vbCrLf)
        strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
        strBuilder.Append(" <BR> " & vbCrLf)
        nImg = 0
        If dsTempMst.Tables.Count > 0 Then
            If dsTempMst.Tables(0).Rows.Count > 0 Then
                Dim drow() As DataRow = dsTempMst.Tables(0).Select("trnasid = '" & cmbType.Tag & "'")
                If drow.Length > 0 Then
                    sMstXMLPaths = drow(0)("MstXMLPath")
                    nPageNo = 0
                    HtmlCompHeader(strBuilder, sMstXMLPaths, Graphics.DpiX)
                    HtmlBasedHeader(strBuilder, Graphics.DpiX)
                    HtmlBasedDetail(strBuilder, Graphics.DpiX)
                    HtmlBasedFooter(strBuilder, Graphics.DpiX)
                End If
            End If
        End If
        strBuilder.Append(" </TR>  " & vbCrLf)
        strBuilder.Append(" </TABLE> " & vbCrLf)
        strBuilder.Append(" </HTML> " & vbCrLf)
        If SaveFile(strFile, strBuilder) Then
            Shell("rundll32.exe url.dll,FileProtocolHandler " & strFile, vbMaximizedFocus)
        End If
    End Sub
    Public Function HTMLSetAlign(ByVal Align As ContentAlignment)
        Dim sRetVal As String = ""
        Select Case Align
            Case ContentAlignment.BottomLeft, ContentAlignment.MiddleLeft, ContentAlignment.TopLeft
                sRetVal = "Left"
            Case ContentAlignment.TopRight, ContentAlignment.MiddleRight, ContentAlignment.TopRight
                sRetVal = "Right"
            Case ContentAlignment.BottomCenter, ContentAlignment.MiddleCenter, ContentAlignment.TopCenter
                sRetVal = "Center"
        End Select
        Return sRetVal
    End Function
    Private Sub HtmlCompHeader(ByVal strBuilder As StringBuilder, ByVal sMstXMLPath As String, ByVal xDip As Single)
        Try
            Dim lFlag As Boolean = True
            nPageNo += 1
            If Not chkRepHeader.Checked Then
                lFlag = (nPageNo = 1)
            End If
            dsMst.Tables.Clear()
            dsMst.ReadXml(sMstXMLPath)
            If dsMst.Tables(0).Rows.Count > 0 Then
                'Company Header Part 
                With pnlCompHeader
                    If lFlag Then
                        If Not lSuppreCompHeaderPnl Then
                            For i As Integer = .Controls.Count - 1 To 0 Step -1
                                nPrintCtr += 1
                                If nCompHeadery < ComY + .Controls(i).Location.Y + .Controls(i).Height Then
                                    nCompHeadery = ComY + .Controls(i).Location.Y + .Controls(i).Height
                                End If
                                If .Controls(i).Name = "Text" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";Top:" & Math.Round((((ComY + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                    End If
                                ElseIf .Controls(i).Name = "Image" Then
                                    If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                        nImg += 1
                                        .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                        strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((ComY + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    End If
                                ElseIf .Controls(i).Name = "Line" Then
                                    If CType(.Controls(i), TemplateLineField).Vertical Then
                                        Dim sStr As String = ""
                                        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        End If
                                        strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((ComY + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    Else
                                        Dim sStr As String = ""
                                        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                        End If
                                        strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((ComY + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    End If
                                ElseIf .Controls(i).Name = "Rectangle" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        Dim sStr As String = ""
                                        If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
                                            sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px solid " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
                                            sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px dotted " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
                                            sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px dashed " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
                                            sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px dashed " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                        End If
                                        strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((ComY + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    End If
                                Else
                                    If .Controls(i).Text <> "" Then
                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                            dtTable = Nothing
                                            If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                                dtTable = dsMst.Tables(0)
                                            ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                                dtTable = dsMst.Tables(1)
                                            End If
                                            If dtTable IsNot Nothing Then
                                                If .Controls(i).Tag.ToString = "DateTime" Then
                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";Top:" & Math.Round(((ComY + .Controls(i).Top * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                Else
                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";Top:" & Math.Round(((ComY + .Controls(i).Top * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                End If
                                            End If
                                            If .Controls(i).Text Like ".Formula_*" Then
                                                Dim strSelect As String = ""
                                                Dim frmFormula As New frmFormula
                                                frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                Dim sFormat As String = ""
                                                For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                    sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                    If sFormat <> "" Then
                                                        Exit For
                                                    End If
                                                Next
                                                frmFormula.mstrFormat = sFormat
                                                frmFormula.mdtTable = dtTable
                                                frmFormula.ExecuteFormula()
                                                strSelect = frmFormula.mstrSelect
                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round(((ComY + .Controls(i).Top * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Else
                        nCompHeadery = 0
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
    Private Sub HtmlBasedHeader(ByVal strBuilder As StringBuilder, ByVal xDip As Single)
        Dim y As Integer = 0
        Dim nTops As Integer = 0
        Try
            If dsMst.Tables(0).Rows.Count > 0 Then
                'Master Part 
                With pnlHeader
                    If Not lSuppreHeaderPnl Then
                        For i As Integer = .Controls.Count - 1 To 0 Step -1
                            nPrintCtr += 1
                            If nHeadery < .Controls(i).Location.Y + .Controls(i).Height Then
                                nHeadery = .Controls(i).Location.Y + .Controls(i).Height
                            End If
                            If .Controls(i).Name = "Text" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='center' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                End If
                            ElseIf .Controls(i).Name = "Image" Then
                                If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                    nImg += 1
                                    .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                    strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((y + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                End If
                            ElseIf .Controls(i).Name = "Line" Then
                                If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                    If CType(.Controls(i), TemplateLineField).Vertical Then
                                        Dim sStr As String = ""
                                        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                            sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                        End If
                                        strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    Else
                                        Dim pPen As New Pen(.Controls(i).ForeColor)
                                        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
                                        Dim sStr As String = ""
                                        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                            sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                        End If
                                        strBuilder.Append("<div> <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    End If
                                End If
                            ElseIf .Controls(i).Name = "Rectangle" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px solid " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                    ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px dotted " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                    ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px dashed " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                    ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateBoxField).LineThickness & "px dashed " & CType(.Controls(i), TemplateBoxField).LineColor.Name & ";border-radius:" & (CType(.Controls(i), TemplateBoxField).AllR * 2) & ";"
                                    End If
                                    strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((y + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                End If
                            Else
                                If .Controls(i).Text <> "" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        dtTable = Nothing
                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(0)
                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(1)
                                        End If
                                        If dtTable IsNot Nothing Then
                                            If .Controls(i).Tag.ToString = "DateTime" Then
                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((y + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                            Else
                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((y + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                            End If
                                        End If
                                        If .Controls(i).Text Like ".Formula_*" Then
                                            Dim strSelect As String = ""
                                            Dim frmFormula As New frmFormula
                                            frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                            Dim sFormat As String = ""
                                            For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                If sFormat <> "" Then
                                                    Exit For
                                                End If
                                            Next
                                            frmFormula.mstrFormat = sFormat
                                            frmFormula.mdtTable = dtTable
                                            frmFormula.ExecuteFormula()
                                            strSelect = frmFormula.mstrSelect
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((y + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
    Private Sub HtmlBasedDetail(ByVal strBuilder As StringBuilder, ByVal xDip As Single)
        Dim Graphics As Graphics
        Dim yCanDraw As Integer = 0
        Dim yScale As Integer = 0
        Graphics = CreateGraphics()

        Try
            iCtr = 0
            Dim strGroup As String = ""
            Dim strGroupFooter As String = ""
            'Detail Part
            If pnlDetail.Controls.Count > 0 Then
                If dsMst.Tables.Count > 1 Then
                    If dsMst.Tables(1).Rows.Count > 0 Then
                        nCounter = 0
                        With pnlDetail
                            IsGroups()
                            For j As Integer = 0 To dsMst.Tables(1).Rows.Count - 1
                                'Group Checking
                                nPrintCtr += 1
                                If tvControls.Nodes(1).Nodes.Count > 0 Then
                                    Dim strSel As String = ""
                                    For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                                        strSel += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
                                    Next
                                    If strGroup <> strSel Then
                                        nGroup1y += HtmlFunctGrouping(strBuilder, j, xDip)
                                        strGroup = strSel
                                    End If
                                End If
                                If Not lSuppreDetPnl Then
                                    Dim k As Integer = 0
                                    'Before Print Counting     
                                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                                        Dim m As Object
                                        m = 1
                                        If TypeOf pnlDetail.Controls(i) Is TemplateBoxField Then

                                            If CType(pnlDetail.Controls(i), TemplateBoxField).CanDraw Then
                                                Dim orgWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                Dim TotalWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font).Width
                                                If orgWidht < TotalWidht Then
                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                End If
                                            End If
                                        End If
                                        If k < .Controls(i).Location.Y + .Controls(i).Height * m Then k = .Controls(i).Location.Y + .Controls(i).Height * m
                                    Next
                                    nCounter += k
                                    iCtr += k
                                    If nGroup1y + iCtr + nGrpFooter1y > ItemHeight() Then ' (txtItemPrint.Text * 105) 
                                        nDetDety = ItemHeight() - nGroup1y - nGrpFooter1y '(txtItemPrint.Text * 105) 
                                        HtmlBasedFooter(strBuilder, xDip, True)
                                        nCounter = 0
                                        nDetDety = 0
                                        iCtr = 0
                                        nPrintCtr = 0
                                        HtmlCompHeader(strBuilder, sMstXMLPaths, xDip)
                                        HtmlBasedHeader(strBuilder, xDip)
                                        IsGroups()
                                        nGroup1y = 0
                                        nGrpFooter1y = 0
                                        yCanDraw = 0
                                        nCounter += k
                                        iCtr += k
                                    End If
                                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                                        If .Controls(i).Name = "Text" Then
                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                'Can Draw 
                                                If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                    Dim m As Object
                                                    m = 0
                                                    Dim orgWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                    Dim TotalWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font).Width
                                                    If orgWidht < TotalWidht Then
                                                        m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text.ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                    Else
                                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text.ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                    End If
                                                End If
                                            Else
                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text.ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                            End If
                                        ElseIf .Controls(i).Name = "Image" Then
                                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                                nImg += 1
                                                .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                                strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                            End If
                                        ElseIf .Controls(i).Name = "Line" Then
                                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                                    Dim sStr As String = ""
                                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                                    End If
                                                    strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                                Else
                                                    Dim sStr As String = ""
                                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                    End If
                                                    strBuilder.Append("<div> <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                                End If
                                            End If
                                        ElseIf .Controls(i).Name = "Rectangle" Then
                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                    Dim sStr As String = ""
                                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                    End If
                                                    strBuilder.Append("<div> size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                                End If
                                            End If
                                        Else
                                            If .Controls(i).Text.Trim <> "" Then
                                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                    dtTable = Nothing
                                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                                        dtTable = dsMst.Tables(0)
                                                        If .Controls(i).Tag = "DateTime" Then
                                                            'Can Draw
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                Else
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                End If
                                                            Else
                                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                            End If
                                                        Else
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                Else
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                End If
                                                            Else
                                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                            End If
                                                        End If
                                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                                        dtTable = dsMst.Tables(1)
                                                        If .Controls(i).Tag = "DateTime" Then
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                Else
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                End If
                                                            Else
                                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                            End If
                                                        Else
                                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                                Dim m As Object
                                                                m = 0
                                                                Dim orgWidht As Object = Graphics.MeasureString(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                                Dim TotalWidht As Object = Graphics.MeasureString(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Font).Width
                                                                If orgWidht < TotalWidht Then
                                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(j)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                Else
                                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(j)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                                End If
                                                            Else
                                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(j)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                                If .Controls(i).Text Like ".Formula_*" Then
                                                    Dim strSelect As String = ""
                                                    Dim frmFormula As New frmFormula
                                                    frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                    Dim sFormat As String = ""
                                                    For l As Integer = 0 To tvControls.Nodes.Count - 1
                                                        sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(l).Text)
                                                        If sFormat <> "" Then
                                                            Exit For
                                                        End If
                                                    Next
                                                    frmFormula.mstrFormat = sFormat
                                                    frmFormula.mdtTable = dtTable
                                                    If dtTable.TableName = "Details" OrElse dtTable.TableName = "Table1" Then
                                                        frmFormula.mintRow = j
                                                    End If
                                                    frmFormula.ExecuteFormula()
                                                    strSelect = frmFormula.mstrSelect
                                                    If CType(.Controls(i), TemplateBoxField).CanDraw Then
                                                        Dim m As Object
                                                        m = 0
                                                        Dim orgWidht As Object = Graphics.MeasureString(strSelect, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
                                                        Dim TotalWidht As Object = Graphics.MeasureString(strSelect, .Controls(i).Font).Width
                                                        If orgWidht < TotalWidht Then
                                                            m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                        Else
                                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                        End If
                                                    Else
                                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                    End If
                                                End If
                                                If dtTable IsNot Nothing Then
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                                If tvControls.Nodes(1).Nodes.Count > 0 Then
                                    If j <> dsMst.Tables(1).Rows.Count - 1 Then
                                        Dim strSel As String = ""
                                        Dim strSelOrginal As String = ""
                                        For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                                            strSel += dsMst.Tables(1).Rows(j + 1).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
                                            strSelOrginal += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
                                        Next
                                        If strSelOrginal <> strSel Then
                                            nGrpFooter1y += HtmlFunctGroupFooter1(strBuilder, j, xDip)
                                            strGroupFooter = strSel
                                        End If
                                    End If
                                End If
                            Next
                            nDetDety = nCounter
                        End With
                    End If
                    If tvControls.Nodes(1).Nodes.Count > 0 Then
                        nGrpFooter1y += HtmlFunctGroupFooter1(strBuilder, dsMst.Tables(1).Rows.Count - 1, xDip)
                    End If
                    'Detail Footer Part
                    With pnlDetailFooter
                        If Not lSuppreDetFotPnl Then
                            For i As Integer = .Controls.Count - 1 To 0 Step -1
                                nPrintCtr += 1
                                If .Controls(i).Name = "Text" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text.ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                    End If
                                ElseIf .Controls(i).Name = "Image" Then
                                    If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                        nImg += 1
                                        .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                        strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                    End If
                                ElseIf .Controls(i).Name = "Line" Then
                                    If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                        If CType(.Controls(i), TemplateLineField).Vertical Then
                                            Dim sStr As String = ""
                                            If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                                sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                                sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                                sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                                sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                            End If
                                            strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                        Else
                                            Dim sStr As String = ""
                                            If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                                sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                                sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                                sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                                sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                            End If
                                            strBuilder.Append("<div> <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                        End If
                                    End If
                                ElseIf .Controls(i).Name = "Rectangle" Then
                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                                Dim sStr As String = ""
                                                If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                                    sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                                    sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                                    sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                                    sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                                End If
                                                strBuilder.Append("<div> size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                            End If
                                        End If
                                    End If
                                Else
                                    If .Controls(i).Text.Trim <> "" Then
                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                            dtTable = Nothing
                                            If dsMst.Tables(0).Columns.Contains(.Controls(i).Tag) Then
                                                dtTable = dsMst.Tables(0)
                                            ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                                dtTable = dsMst.Tables(1)
                                            End If
                                            If dtTable IsNot Nothing Then
                                                If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
                                                    Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", "")
                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & nSum.ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                Else
                                                    If .Controls(i).Text Like ".Formula_*" Then
                                                        Dim strSelect As String = ""
                                                        Dim frmFormula As New frmFormula
                                                        frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                                        Dim sFormat As String = ""
                                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                            If sFormat <> "" Then
                                                                Exit For
                                                            End If
                                                        Next
                                                        frmFormula.mstrFormat = sFormat
                                                        frmFormula.mdtTable = dtTable
                                                        frmFormula.ExecuteFormula()
                                                        strSelect = frmFormula.mstrSelect
                                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                    Else
                                                        If .Controls(i).Tag = "DateTime" Then
                                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                        Else
                                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                If nDetFootery < ComY + .Controls(i).Location.Y + .Controls(i).Height Then
                                    nDetFootery = ComY + .Controls(i).Location.Y + .Controls(i).Height
                                End If
                            Next
                        End If
                    End With
                    If Not chkPageWise.Checked Then
                        With pnlDetail
                            If pnlDetail.Controls.Count > 0 Then
                                If Not lSuppreDetPnl Then
                                    nDetDety += ItemHeight() - iCtr - nGroup1y - nGrpFooter1y - nDetFootery '(txtItemPrint.Text * 105)
                                    strBuilder.Append("<div align='" & HTMLSetAlign(CType(.Controls(.Controls.Count - 1), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(.Controls.Count - 1).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(.Controls.Count - 1).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Top) * xDip) / 72), 0) & ";color:" & .Controls(.Controls.Count - 1).ForeColor.Name & ";font-family:" & .Controls(.Controls.Count - 1).Font.Name & "; font-size:" & Math.Round(((.Controls(.Controls.Count - 1).Font.Size * xDip) / 72), 0) & "px;'></div>")
                                End If
                            End If
                        End With
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub


    Private Function HtmlFunctGrouping(ByVal strBuilder As StringBuilder, ByVal k As Integer, ByVal xDip As Single)
        Dim lRetVal As Boolean = True
        Dim nGrp1 As Integer = 0
        If tvControls.Nodes(1).Nodes.Count > 0 Then
            With pnlGroup1
                If Not lSuppreGroup1Pnl Then
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        If nGrp1 < .Controls(i).Location.Y + .Controls(i).Height Then
                            nGrp1 = .Controls(i).Location.Y + .Controls(i).Height
                        End If
                    Next
                    If nGrp1 + nGrpFooter1y + iCtr > ItemHeight() Then '(txtItemPrint.Text * 105)
                        nDetDety = ItemHeight() - nGrp1  '(txtItemPrint.Text * 105)
                        HtmlBasedFooter(strBuilder, xDip, True)
                        nCounter = 0
                        nDetDety = 0
                        iCtr = 0
                        nPrintCtr = 0
                        nGroup1y = 0
                        nGrpFooter1y = 0
                        HtmlCompHeader(strBuilder, sMstXMLPaths, xDip)
                        HtmlBasedHeader(strBuilder, xDip)
                        IsGroups()
                    End If
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        nPrintCtr += 1
                        If .Controls(i).Name = "Text" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                            End If
                        ElseIf .Controls(i).Name = "Image" Then
                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                nImg += 1
                                .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                            End If
                        ElseIf .Controls(i).Name = "Line" Then
                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    End If
                                    strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                Else
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    End If
                                    strBuilder.Append("<div> <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                End If
                            End If
                        ElseIf .Controls(i).Name = "Rectangle" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    End If
                                    strBuilder.Append("<div> size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                End If
                            End If
                        Else
                            If .Controls(i).Text <> "" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    dtTable = Nothing
                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(0)
                                        If .Controls(i).Tag.ToString = "DateTime" Then
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        Else
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        End If
                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(1)
                                        If .Controls(i).Tag.ToString = "DateTime" Then
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        Else
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(k)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        End If
                                    End If

                                    If .Controls(i).Text Like ".Formula_*" Then
                                        Dim strSelect As String = ""
                                        Dim frmFormula As New frmFormula
                                        frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                        Dim sFormat As String = ""
                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                            If sFormat <> "" Then
                                                Exit For
                                            End If
                                        Next
                                        frmFormula.mstrFormat = sFormat
                                        frmFormula.mdtTable = dtTable
                                        frmFormula.ExecuteFormula()
                                        strSelect = frmFormula.mstrSelect
                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            End With
        End If
        Return nGrp1
    End Function
    Private Function HtmlFunctGroupFooter1(ByVal strBuilder As StringBuilder, ByVal k As Integer, ByVal xDip As Single)
        Dim lRetVal As Boolean = True
        Dim nGrpFot1 As Integer = 0
        If tvControls.Nodes(1).Nodes.Count > 0 Then
            With pnlGroupFooter1
                If Not lSuppreGroupFooter1Pnl Then
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        If nGrpFot1 < .Controls(i).Location.Y + .Controls(i).Height Then
                            nGrpFot1 = .Controls(i).Location.Y + .Controls(i).Height
                        End If
                    Next
                    If Not chkPageWise.Checked Then
                        If nGroup1y + iCtr + nGrpFot1 + nGrpFooter1y > ItemHeight() Then '(txtItemPrint.Text * 105)
                            nDetDety = ItemHeight() - nGroup1y - nGrpFooter1y '(txtItemPrint.Text * 105)
                            HtmlBasedFooter(strBuilder, xDip, True)
                            nCounter = 0
                            nDetDety = 0
                            iCtr = 0
                            nPrintCtr = 0
                            nGroup1y = 0
                            nGrpFooter1y = 0
                            HtmlCompHeader(strBuilder, sMstXMLPaths, xDip)
                            HtmlBasedHeader(strBuilder, xDip)
                            IsGroups()
                        End If
                    Else
                        If nPrintCtr >= 150 Then
                            nDetDety = nCounter
                            nCounter = 0
                            nDetDety = 0
                            iCtr = 0
                            nPrintCtr = 0
                            nGroup1y = 0
                            nGrpFooter1y = 0
                            HtmlCompHeader(strBuilder, sMstXMLPaths, xDip)
                            HtmlBasedHeader(strBuilder, xDip)
                            IsGroups()
                        End If
                    End If

                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        nPrintCtr += 1
                        If .Controls(i).Name = "Text" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                            End If
                        ElseIf .Controls(i).Name = "Image" Then
                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                nImg += 1
                                .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                            End If
                        ElseIf .Controls(i).Name = "Line" Then
                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    End If
                                    strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                Else
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    End If
                                    strBuilder.Append("<div> <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")

                                End If
                            End If
                        ElseIf .Controls(i).Name = "Rectangle" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    End If
                                    strBuilder.Append("<div> size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                End If
                            End If
                        Else
                            If .Controls(i).Text <> "" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    dtTable = Nothing
                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(0)
                                        If .Controls(i).Tag.ToString = "DateTime" Then
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        Else
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        End If
                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                        dtTable = dsMst.Tables(1)
                                        If .Controls(i).Tag.ToString = "DateTime" Then
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        Else
                                            If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
                                                Dim strCon As String = ""
                                                For p As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
                                                    strCon += tvControls.Nodes(1).Nodes(p).Text + " = '" + dtTable.Rows(k)(tvControls.Nodes(1).Nodes(p).Text).ToString + "',"
                                                Next
                                                strCon = strCon.Substring(0, strCon.Length - 1)
                                                Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", strCon)
                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & nSum.ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                            Else
                                                strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(k)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                            End If
                                        End If
                                    End If
                                    If .Controls(i).Text Like ".Formula_*" Then
                                        Dim strSelect As String = ""
                                        Dim frmFormula As New frmFormula
                                        frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                        Dim sFormat As String = ""
                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                            If sFormat <> "" Then
                                                Exit For
                                            End If
                                        Next
                                        frmFormula.mstrFormat = sFormat
                                        frmFormula.mdtTable = dtTable
                                        frmFormula.ExecuteFormula()
                                        strSelect = frmFormula.mstrSelect
                                        strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            End With
        End If
        Return nGrpFot1
    End Function
    Private Sub HtmlBasedFooter(ByVal strBuilder As StringBuilder, ByVal xDip As Single, Optional ByVal lDetCall As Boolean = False)
        Try
            Dim Footery As Integer = 0
            With pnlFooter
                If Not lSuppreFooterPnl Then
                    For i As Integer = .Controls.Count - 1 To 0 Step -1
                        If ComY < nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y + .Controls(i).Height Then
                            ComY = nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y + .Controls(i).Height
                        End If
                        nPrintCtr += 1
                        If .Controls(i).Name = "Text" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & .Controls(i).Text & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                End If
                            End If
                        ElseIf .Controls(i).Name = "Image" Then
                            If Not CType(.Controls(i), TemplateImageField).Suppress Then
                                nImg += 1
                                .Controls(i).BackgroundImage.Save(strHtmlDir & "/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg")
                                strBuilder.Append("<div> <img  src='./Images/" & CType(.Controls(i), TemplateImageField).Name & nImg.ToString & ".jpg' Height=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateImageField).TextAlign) & "'  vspace='3' valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                            End If
                        ElseIf .Controls(i).Name = "Line" Then
                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
                                If CType(.Controls(i), TemplateLineField).Vertical Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border-right: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-left: 0px dashed #000000;"
                                    End If
                                    strBuilder.Append("<div> <hr size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:1;Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")
                                Else
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    End If
                                    strBuilder.Append("<div> <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")

                                End If

                            End If
                        ElseIf .Controls(i).Name = "Rectangle" Then
                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    Dim sStr As String = ""
                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px solid " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: solid none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dotted " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
                                        sStr = "border: " & CType(.Controls(i), TemplateLineField).LineThickness & "px dashed " & CType(.Controls(i), TemplateLineField).LineColor.Name & ";border-style: dashed none none;"
                                    End If
                                    strBuilder.Append("<div> size=" & Math.Round(((.Controls(i).Height * xDip) / 72), 0) - 10 & "px <hr align='" & HTMLSetAlign(CType(.Controls(i), TemplateLineField).TextAlign) & "'  valign='TOP' style='position:absolute;" & sStr & "left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'/></div>")

                                End If
                            End If
                        Else
                            If .Controls(i).Text.Trim <> "" Then
                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
                                    dtTable = Nothing
                                    If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(0)
                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
                                            dtTable = dsMst.Tables(1)
                                        End If
                                        If .Controls(i).Text Like ".Formula_*" Then
                                            Dim strSelect As String = ""
                                            Dim frmFormula As New frmFormula
                                            frmFormula.mstrFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
                                            Dim sFormat As String = ""
                                            For j As Integer = 0 To tvControls.Nodes.Count - 1
                                                sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
                                                If sFormat <> "" Then
                                                    Exit For
                                                End If
                                            Next
                                            frmFormula.mstrFormat = sFormat
                                            frmFormula.mdtTable = dtTable
                                            frmFormula.ExecuteFormula()
                                            strSelect = frmFormula.mstrSelect
                                            strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & strSelect & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                        Else
                                            If dtTable IsNot Nothing Then
                                                If .Controls(i).Tag = "DateTime" Then
                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy") & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                Else
                                                    strBuilder.Append(IIf(.Controls(i).Font.Bold, "<strong>", "") & IIf(.Controls(i).Font.Underline, "<u>", "") & "<div align='" & HTMLSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign) & "'  valign='TOP' style='position:absolute;left:" & Math.Round(((.Controls(i).Left * xDip) / 72), 0) & ";width:" & Math.Round(((.Controls(i).Width * xDip) / 72), 0) & ";Top:" & Math.Round((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Top) * xDip) / 72), 0) & ";color:" & .Controls(i).ForeColor.Name & ";font-family:" & .Controls(i).Font.Name & "; font-size:" & Math.Round(((.Controls(i).Font.Size * xDip) / 72), 0) & "px;'>" & dtTable.Rows(0)(.Controls(i).Text).ToString & "</div>" & IIf(.Controls(i).Font.Underline, "</u>", "") & IIf(.Controls(i).Font.Bold, "</strong>", ""))
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If chkPageWise.Checked Then
                            If nPrintCtr >= 150 Then
                                nDetDety = 0
                                nPrintCtr = 0
                                HtmlCompHeader(strBuilder, sMstXMLPaths, xDip)
                                HtmlBasedHeader(strBuilder, xDip)
                            End If
                        End If
                    Next

                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub
#End Region
    'Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
    '    Dim strBuilder As New StringBuilder
    '    Dim blnFlag As Boolean = False
    '    Try

    '        'HEADER PART
    '        strBuilder.Append(" <TITLE> " & Me._ReportName & " </TITLE> " & vbCrLf)
    '        strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
    '        strBuilder.Append(" <BR> " & vbCrLf)
    '        strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

    '        'Sandeep [ 10 FEB 2011 ] -- Start
    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        If ConfigParameter._Object._IsDisplayLogo = True Then
    '            strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
    '            strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
    '            Dim objAppSett As New clsApplicationSettings
    '            Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
    '            objAppSett = Nothing
    '            If Company._Object._Image IsNot Nothing Then
    '                Company._Object._Image.Save(strImPath)
    '            End If
    '            strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
    '            strBuilder.Append(" </TD> " & vbCrLf)
    '            strBuilder.Append(" </TR> " & vbCrLf)
    '        End If
    '        'Sandeep [ 10 FEB 2011 ] -- End

    '        strBuilder.Append(" <TR> " & vbCrLf)
    '        strBuilder.Append(" <TD width='10%'> " & vbCrLf)
    '        strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 12, "Prepared By :") & " </B></FONT> " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
    '        strBuilder.Append(User._Object._Username & vbCrLf)
    '        strBuilder.Append(" </FONT></TD> " & vbCrLf)
    '        strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
    '        strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
    '        strBuilder.Append(" &nbsp; " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
    '        strBuilder.Append(" &nbsp; " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" </TR> " & vbCrLf)
    '        strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
    '        strBuilder.Append(" <TD width='10%'> " & vbCrLf)
    '        strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Date :") & "</B></FONT> " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
    '        strBuilder.Append(Now.Date & vbCrLf)
    '        strBuilder.Append(" </FONT></TD> " & vbCrLf)
    '        strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
    '        strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
    '        strBuilder.Append(" &nbsp; " & vbCrLf)
    '        strBuilder.Append(" </TD> " & vbCrLf)
    '        strBuilder.Append(" </TR> " & vbCrLf)
    '        strBuilder.Append(" </TABLE> " & vbCrLf)
    '        strBuilder.Append(" <HR> " & vbCrLf)
    '        strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
    '        strBuilder.Append(" </HR> " & vbCrLf)
    '        strBuilder.Append(" <BR> " & vbCrLf)
    '        strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
    '        strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

    '        'Report Column Caption
    '        For j As Integer = 0 To objDataReader.Columns.Count - 1
    '            'Sohail (26 Nov 2011) -- Start
    '            If j = 2 OrElse j = 3 Then
    '                '*** Do Nothing (GroupID and GroupName column)
    '                'strBuilder.Append("<TD BORDER=1 WIDTH='0px' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
    '            Else
    '                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
    '            End If
    '            'Sohail (26 Nov 2011) -- End
    '        Next

    '        strBuilder.Append(" </TR> " & vbCrLf)


    '        'Data Part
    '        'Sohail (26 Nov 2011) -- Start
    '        Dim strPrevGrpID As String = ""
    '        Dim GroupArrayTotal(objDataReader.Columns.Count - 5) As Decimal
    '        'Sohail (26 Nov 2011) -- End
    '        For i As Integer = 0 To objDataReader.Rows.Count - 1

    '            'Sohail (26 Nov 2011) -- Start
    '            If strPrevGrpID <> objDataReader.Rows(i)("GrpId").ToString Then

    '                If i > 0 Then
    '                    '**** Sub Total ****
    '                    strBuilder.Append(" <TR> " & vbCrLf)
    '                    For p As Integer = 0 To objDataReader.Columns.Count - 1
    '                        Select Case p
    '                            Case 0
    '                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
    '                            Case 1
    '                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
    '                            Case 2 To 3
    '                                '*** Do Nothing (GroupID and GroupName column)
    '                            Case Else
    '                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 4)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
    '                        End Select
    '                    Next
    '                    strBuilder.Append(" </TR> " & vbCrLf)
    '                    ReDim GroupArrayTotal(objDataReader.Columns.Count - 5)
    '                End If

    '                '**** Group Header ****
    '                strBuilder.Append(" <TR> " & vbCrLf)
    '                strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - 2 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & mstrReport_GroupName & " " & objDataReader.Rows(i)("GrpName") & "</B></FONT></TD>" & vbCrLf)
    '                strBuilder.Append(" </TR> " & vbCrLf)
    '            End If
    '            'Sohail (26 Nov 2011) -- End

    '            strBuilder.Append(" <TR> " & vbCrLf)
    '            For k As Integer = 0 To objDataReader.Columns.Count - 1
    '                'S.SANDEEP [ 17 AUG 2011 ] -- START
    '                'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '                'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)

    '                Select Case k
    '                    Case 0, 1
    '                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
    '                        'Sohail (26 Nov 2011) -- Start
    '                    Case 2 To 3
    '                        '*** Do Nothing (GroupID and GroupName column)
    '                        'Sohail (26 Nov 2011) -- End
    '                    Case Else
    '                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
    '                End Select
    '                'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '                'Sohail (26 Nov 2011) -- Start
    '                If mintViewIndex > 0 AndAlso k > 3 Then 'i > 0 AndAlso
    '                    GroupArrayTotal(k - 4) += CDec(objDataReader.Rows(i)(k))
    '                End If
    '                'Sohail (26 Nov 2011) -- End
    '            Next
    '            strBuilder.Append(" </TR> " & vbCrLf)

    '            strPrevGrpID = objDataReader.Rows(i)("GrpId").ToString 'Sohail (26 Nov 2011)
    '        Next

    '        'Sohail (26 Nov 2011) -- Start
    '        If mintViewIndex > 0 Then
    '            '**** Sub Total for Last Group ****
    '            strBuilder.Append(" <TR> " & vbCrLf)
    '            For p As Integer = 0 To objDataReader.Columns.Count - 1
    '                Select Case p
    '                    Case 0
    '                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
    '                    Case 1
    '                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
    '                    Case 2 To 3
    '                        '*** Do Nothing (GroupID and GroupName column)
    '                    Case Else
    '                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 4)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
    '                End Select
    '            Next
    '            strBuilder.Append(" </TR> " & vbCrLf)
    '        End If
    '        'Sohail (26 Nov 2011) -- End

    '        strBuilder.Append(" <TR> " & vbCrLf)
    '        For k As Integer = 0 To objDataReader.Columns.Count - 1
    '            'Sohail (26 Nov 2011) -- Start
    '            'If k = 0 Then
    '            '    'S.SANDEEP [ 17 AUG 2011 ] -- START
    '            '    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '            '    'strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
    '            '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
    '            '    'S.SANDEEP [ 17 AUG 2011 ] -- END 
    '            'ElseIf k <= 1 Then
    '            '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
    '            'Else
    '            '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
    '            'End If
    '            Select Case k
    '                Case 0
    '                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
    '                Case 1
    '                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
    '                Case 2 To 3
    '                    '*** Do Nothing (GroupID and GroupName column)
    '                Case Else
    '                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
    '            End Select
    '            'Sohail (26 Nov 2011) -- End
    '        Next
    '        strBuilder.Append(" </TR>  " & vbCrLf)
    '        strBuilder.Append(" </TABLE> " & vbCrLf)
    '        strBuilder.Append(" </HTML> " & vbCrLf)


    '        'Sandeep [ 09 MARCH 2011 ] -- Start
    '        If System.IO.Directory.Exists(SavePath) = False Then
    '            Dim dig As New Windows.Forms.FolderBrowserDialog
    '            dig.Description = "Select Folder Where to export report."

    '            If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                SavePath = dig.SelectedPath
    '            Else
    '                Exit Function
    '            End If
    '        End If
    '        'Sandeep [ 09 MARCH 2011 ] -- End 

    '        'Anjan (14 Apr 2011)-Start
    '        'Issue : To remove "\" from path which comes at end in path.
    '        If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
    '            SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
    '        End If
    '        'Anjan (14 Apr 2011)-End

    '        If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
    '            'Sandeep [ 09 MARCH 2011 ] -- Start
    '            StrFinalPath = SavePath & "\" & flFileName & ".xls"
    '            'Sandeep [ 09 MARCH 2011 ] -- End 
    '            blnFlag = True
    '        Else
    '            blnFlag = False
    '        End If

    '        Return blnFlag

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
    '        Return False
    '    End Try
    ' End Function
    Private Function SaveFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Save File")
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#Region "Rtf Transfer"
    Private Sub RtfTranfer()
        Dim rtb As New RichTextBox
        Dim Position As Int16 = 0
        Dim string1 As String = "This is string1. Font=Arial: FontSize=14: Color=Blue" & vbNewLine
        Dim myFont As New Font("Arial", 14, FontStyle.Regular, GraphicsUnit.Point)
        Dim myColor As Color = Color.Blue
        rtb.Select(Position, 1)
        rtb.SelectionFont = myFont
        rtb.SelectionColor = myColor
        rtb.SelectedText = string1
        Position += string1.Length

        ' String 2
        string1 = "This is string2. Font=microsoft sans serif: FontSize=10: Color=Red" & vbNewLine
        myFont = New Font("microsoft sans serif", 10, FontStyle.Regular, GraphicsUnit.Point)
        myColor = Color.Red
        rtb.Select(Position, 1)
        rtb.SelectionFont = myFont
        rtb.SelectionColor = myColor
        rtb.SelectedText = string1
        'Position += string1.Length

        ' String 3
        'string1 = "This is string3. Font=courier new: FontSize=30: Color=Green: BOLD" & vbNewLine
        'myFont = New Font("courier new", 30, FontStyle.Bold, GraphicsUnit.Point)
        string1 = "This is string2.Font=microsoft sans serif : FontSize=10 : Color=Red" & vbNewLine
        myFont = New Font("microsoft sans serif", 10, FontStyle.Regular, GraphicsUnit.Point)
        myColor = Color.Green
        rtb.Select(Position, 0)
        rtb.SelectionFont = myFont
        rtb.SelectionColor = myColor
        rtb.SelectedText = string1
        Position += string1.Length
        rtb.SaveFile("c:\Demo.rtf")
    End Sub
#End Region

    Private Sub PdfToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PdfToolStripMenuItem.Click
        'PDFConvertor()
    End Sub

    Private Sub HTMLToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTMLToolStripMenuItem.Click
        HTMLConvertor()
    End Sub

    Private Sub WordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WordToolStripMenuItem.Click
        HTMLConvertor()

        '        SaveFile("c:\demo.doc", StringBuilder)
    End Sub

    Private Sub ExeclToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExeclToolStripMenuItem.Click
        RtfTranfer()
    End Sub
#Region "PDF (1T3XT)"
    'Private Sub PDFConvertor()

    '    Dim oPDF As New ItextDocument
    '    Dim strFile As String = Environment.CurrentDirectory & "\" & cmbType.Text & ".pdf"
    '    'oPDF.CreatePDF(strFile,New iTextSharp.text.Rectangle(0, 0, pnlCanvas.Width + 44, CanvasHeight * 100))
    '    oPDF.CreatePDF(strFile, New iTextSharp.text.Rectangle(0, 0, pnlCanvas.Width, CanvasHeight * 100))

    '    Dim dsTempMst As New DataSet
    '    dsTempMst.ReadXml(sMst)
    '    If dsTempMst.Tables.Count > 0 Then
    '        If dsTempMst.Tables(0).Rows.Count > 0 Then
    '            Dim drow() As DataRow = dsTempMst.Tables(0).Select("trnasid = '" & cmbType.Tag & "'")
    '            If drow.Length > 0 Then
    '                sMstXMLPaths = drow(0)("MstXMLPath")
    '                nPageNo = 0
    '                PDFCompHeader(sMstXMLPaths, oPDF)
    '                ''PDFBasedHeader(clPDF, Graphics.DpiX)
    '                ''PDFBasedDetail(clPDF, Graphics.DpiX)
    '                ''PDFBasedFooter(clPDF, Graphics.DpiX)
    '            End If
    '        End If
    '    End If
    '    oPDF.ClosePDF()

    '    Shell("rundll32.exe url.dll,FileProtocolHandler " & (strFile), vbMaximizedFocus)
    'End Sub

    'Private Sub PDFCompHeader(ByVal sMstXMLPath As String, ByVal oPDF As ItextDocument)
    '    Try
    '        Dim lFlag As Boolean = True
    '        nPageNo += 1
    '        If Not chkRepHeader.Checked Then
    '            lFlag = (nPageNo = 1)
    '        End If
    '        'oPDF.DrawImg("Img1", 1, 10, 4, 4)
    '        dsMst.Tables.Clear()
    '        dsMst.ReadXml(sMstXMLPath)
    '        If dsMst.Tables(0).Rows.Count > 0 Then
    '            'Company Header Part 
    '            With pnlCompHeader
    '                If lFlag Then
    '                    If Not lSuppreCompHeaderPnl Then
    '                        For i As Integer = .Controls.Count - 1 To 0 Step -1
    '                            nPrintCtr += 1
    '                            If nCompHeadery < .Controls(i).Location.Y + .Controls(i).Height Then
    '                                nCompHeadery = .Controls(i).Location.Y + .Controls(i).Height
    '                            End If
    '                            If .Controls(i).Name = "Text" Then
    '                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
    '                                    'oPDF.DrawText(0, 8, "emeas", fFontStyle(.Controls(i).Font), 10, clsPDFCreator.pdfTextAlign.pdfAlignLeft)
    '                                    oPDF.WriteString(.Controls(i).Text, .Controls(i).Font.Name, .Controls(i).Font.Size, .Controls(i).ForeColor, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
    '                                End If
    '                                'ElseIf .Controls(i).Name = "Image" Then
    '                                '    If Not CType(.Controls(i), TemplateImageField).Suppress Then
    '                                '        'oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
    '                                '    End If
    '                                'ElseIf .Controls(i).Name = "Line" Then
    '                                '    'If Not CType(.Controls(i), TemplateLineField).Suppress Then
    '                                '    '    If CType(.Controls(i), TemplateLineField).Vertical Then
    '                                '    '        Dim pPen As New Pen(.Controls(i).ForeColor)
    '                                '    '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
    '                                '    '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
    '                                '    '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
    '                                '    '        'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
    '                                '    '    Else
    '                                '    '        Dim pPen As New Pen(.Controls(i).ForeColor)
    '                                '    '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
    '                                '    '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
    '                                '    '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
    '                                '    '        'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, CType(.Controls(i), TemplateLineField).Top + 3, pPen)
    '                                '    '    End If
    '                                '    'End If
    '                                '    If CType(.Controls(i), TemplateLineField).Vertical Then
    '                                '        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
    '                                '            oPDF.SetDash(0, 0)
    '                                '        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
    '                                '            oPDF.SetDash(0.05, 0.05)
    '                                '        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
    '                                '            oPDF.SetDash(0.07, 0.07)
    '                                '        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
    '                                '            oPDF.SetDash(0.1, 0.05)
    '                                '        End If
    '                                '        oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
    '                                '        oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
    '                                '                             CType(.Controls(i), TemplateLineField).LineColor.G, _
    '                                '                             CType(.Controls(i), TemplateLineField).LineColor.B))
    '                                '        'oPDF.MoveTo(1, 10.3)
    '                                '        'oPDF.LineTo(1, 10.9)
    '                                '        oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
    '                                '        oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip))
    '                                '        'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - (((CType(.Controls(i), TemplateLineField).Top)) / xDip) - 0.06) '- (CType(.Controls(i), TemplateLineField).Height / xDip)
    '                                '        'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), (CanvasHeight - ((((CType(.Controls(i), TemplateLineField).Top) + CType(.Controls(i), TemplateLineField).Height))) / xDip) + 0.06)
    '                                '    Else
    '                                '        Dim pPen As New Pen(.Controls(i).ForeColor)
    '                                '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
    '                                '        oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
    '                                '                             CType(.Controls(i), TemplateLineField).LineColor.G, _
    '                                '                             CType(.Controls(i), TemplateLineField).LineColor.B))
    '                                '        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
    '                                '            oPDF.SetDash(0, 0)
    '                                '        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
    '                                '            oPDF.SetDash(0.05, 0.05)
    '                                '        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
    '                                '            oPDF.SetDash(0.07, 0.07)
    '                                '        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
    '                                '            oPDF.SetDash(0.1, 0.05)
    '                                '        End If
    '                                '        oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
    '                                '        'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
    '                                '        'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

    '                                '        oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip))
    '                                '        oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - 0.1)


    '                                '    End If
    '                                'ElseIf .Controls(i).Name = "Rectangle" Then
    '                                '    'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
    '                                '    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
    '                                '        'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height - 2, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
    '                                '        'oPDF.Rectangle(2.9, 11.27, 0.9, 0.3, clsPDFCreator.pdfPathOptions.Stroked, 0.1)
    '                                '        If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
    '                                '            oPDF.SetDash(0, 0)
    '                                '        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
    '                                '            oPDF.SetDash(0.05, 0.05)
    '                                '        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
    '                                '            oPDF.SetDash(0.07, 0.07)
    '                                '        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
    '                                '            oPDF.SetDash(0.1, 0.05)
    '                                '        End If
    '                                '        oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
    '                                '        oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))
    '                                '        'oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (CType(.Controls(i), TemplateBoxField).Top) / xDip) - 0.6 - (CType(.Controls(i), TemplateBoxField).Top / xDip), (CType(.Controls(i), TemplateBoxField).Width / xDip) - 0.06, ((CType(.Controls(i), TemplateBoxField).Height) / xDip), clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
    '                                '        oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), CanvasHeight - ((CType(.Controls(i), TemplateBoxField).Top) / xDip) - ((CType(.Controls(i), TemplateBoxField).Height) / xDip), (CType(.Controls(i), TemplateBoxField).Width / xDip) - 0.06, ((CType(.Controls(i), TemplateBoxField).Height) / xDip), clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
    '                                '    End If
    '                                '    'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
    '                                'Else
    '                                '    If .Controls(i).Text <> "" Then
    '                                '        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
    '                                '            'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
    '                                '            dtTable = Nothing
    '                                '            If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
    '                                '                dtTable = dsMst.Tables(0)
    '                                '            ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
    '                                '                dtTable = dsMst.Tables(1)
    '                                '            End If
    '                                '            If dtTable IsNot Nothing Then
    '                                '                If .Controls(i).Tag.ToString = "DateTime" Then
    '                                '                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
    '                                '                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
    '                                '                    'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
    '                                '                Else
    '                                '                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
    '                                '                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
    '                                '                    'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
    '                                '                End If
    '                                '            End If
    '                                '            If .Controls(i).Text Like ".Formula_*" Then
    '                                '                Dim strSelect As String = ""
    '                                '                Dim frmFormula As New frmFormula
    '                                '                frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
    '                                '                Dim sFormat As String = ""
    '                                '                For j As Integer = 0 To tvControls.Nodes.Count - 1
    '                                '                    sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
    '                                '                    If sFormat <> "" Then
    '                                '                        Exit For
    '                                '                    End If
    '                                '                Next
    '                                '                frmFormula.sFormat = sFormat
    '                                '                frmFormula.dtTable = dtTable
    '                                '                frmFormula.ExecuteFormula()
    '                                '                strSelect = frmFormula.strSelect
    '                                '                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
    '                                '                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
    '                                '            End If
    '                                '        End If
    '                                'End If
    '                            End If
    '                        Next
    '                    End If
    '                Else
    '                    nCompHeadery = 0
    '                End If
    '            End With
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, My.Application.Info.Title)
    '    End Try
    'End Sub
#End Region

    Private Sub Panel1_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel1.MouseEnter
        Panel1.BackColor = Color.Orange
    End Sub

    Private Sub Panel1_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel1.MouseLeave
        Panel1.BackColor = Color.PaleGoldenrod
    End Sub

    Private Sub Panel1_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel1.Click
        If tlpLeft.Visible = False Then
            tlpLeft.Visible = True
        Else
            tlpLeft.Visible = False
        End If
    End Sub

    Private Sub Panel2_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel2.MouseEnter
        Panel2.BackColor = Color.Orange
    End Sub

    Private Sub Panel2_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel2.MouseLeave
        Panel2.BackColor = Color.PaleGoldenrod
    End Sub

    Private Sub Panel2_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel2.Click
        If tlpProperties.Visible = False Then
            tlpProperties.Visible = True
        Else
            tlpProperties.Visible = False
        End If
    End Sub
End Class

#Region "Commented"

'' ''Private Sub PDFConvertor()
'' ''    Dim clPDF As New clsPDFCreator
'' ''    Dim nSize As Integer = 1
'' ''    Dim strFile As String
'' ''    strFile = "c:\Demo.pdf"

'' ''    nPrintCtr = 0
'' ''    If chkLandspace.Checked Then
'' ''        nSize = 2
'' ''    Else
'' ''        nSize = 1
'' ''    End If
'' ''    nCompHeadery = 0
'' ''    nHeadery = 0
'' ''    nDetHeadery = 0
'' ''    nDetDety = 0
'' ''    nDetFootery = 0
'' ''    nGroup1y = 0
'' ''    nGrpFooter1y = 0
'' ''    dtTable = New DataTable
'' ''    'oPrint.nLeft = nLeft * 100
'' ''    'oPrint.nRight = nRight * 100
'' ''    'oPrint.nTop = nTop * 100
'' ''    'oPrint.nBottom = nBottom * 100
'' ''    With clPDF
'' ''        .Title = cmbType.Text          ' Titolo
'' ''        Dim Graphics As Graphics
'' ''        Graphics = CreateGraphics()
'' ''        .ScaleMode = clsPDFCreator.pdfScaleMode.pdfInch                 ' Unit� di misura
'' ''        .PaperSize = clsPDFCreator.pdfPaperSize.pdfUser                         ' Formato pagina
'' ''        If chkLandspace.Checked Then
'' ''            .PaperHeight = CanvasHeight * 100
'' ''            .PaperWidth = pnlCanvas.Width / 100
'' ''        Else
'' ''            .PaperWidth = ((pnlCanvas.Width + ((nLeft - 0.25) * 100) + ((nRight - 0.25) * 100)) / 100)
'' ''            .PaperHeight = CanvasHeight
'' ''        End If
'' ''        If .InitPDFFile(strFile) Then                     'inizializza il file
'' ''            '.LoadFont(fFontStyle(.Controls(i).Font), "TIMESNEWROMAN")                              ' Tipo TrueType
'' ''            LoadFonts(clPDF)
'' ''            .BeginPage()
'' ''            Dim dsTempMst As New DataSet
'' ''            dsTempMst.ReadXml(sMst)
'' ''            If dsTempMst.Tables.Count > 0 Then
'' ''                If dsTempMst.Tables(0).Rows.Count > 0 Then
'' ''                    Dim drow() As DataRow = dsTempMst.Tables(0).Select("trnasid = '" & cmbType.Tag & "'")
'' ''                    If drow.Length > 0 Then
'' ''                        sMstXMLPaths = drow(0)("MstXMLPath")
'' ''                        nPageNo = 0
'' ''                        PDFCompHeader(clPDF, sMstXMLPaths, Graphics.DpiX)
'' ''                        PDFBasedHeader(clPDF, Graphics.DpiX)
'' ''                        PDFBasedDetail(clPDF, Graphics.DpiX)
'' ''                        PDFBasedFooter(clPDF, Graphics.DpiX)
'' ''                    End If
'' ''                End If
'' ''            End If
'' ''            .EndPage()
'' ''            ' Definisce una risorsa da stampare su tutte le pagine
'' ''            .StartObject("Footers", clsPDFCreator.pdfObjectType.pdfAllPages)
'' ''            .DrawText(20, 1.5, 0, "di " & Trim(CStr(.Pages)), "Default", 12, clsPDFCreator.pdfTextAlign.pdfAlignRight)
'' ''            .EndObject()
'' ''            ' Chiude il documento
'' ''            .ClosePDFFile()
'' ''            Call Shell("rundll32.exe url.dll,FileProtocolHandler " & (strFile), vbMaximizedFocus)
'' ''        End If
'' ''    End With
'' ''    'oPrint.PageEject()

'' ''    'oPrint.Show(Me.MdiParent, nSize)

'' ''End Sub
'' ''Private Sub LoadFonts(ByVal clPDF As clsPDFCreator)
'' ''    With clPDF
'' ''        .LoadFont("Arial", "Arial")                                ' Tipo TrueType
'' ''        .LoadFont("Arial_IT", "Arial", clsPDFCreator.pdfFontStyle.pdfItalic)                                ' Tipo TrueType
'' ''        .LoadFont("Arial_BD", "Arial", clsPDFCreator.pdfFontStyle.pdfBold)                                ' Tipo TrueType
'' ''        .LoadFont("Arial_IT_BD", "Arial", clsPDFCreator.pdfFontStyle.pdfBoldItalic)                                ' Tipo TrueType

'' ''        .LoadFont("Courier", "Courier New")                                      ' Tipo TrueType
'' ''        .LoadFont("Courier_IT", "Courier New", clsPDFCreator.pdfFontStyle.pdfItalic) ' Tipo TrueType
'' ''        .LoadFont("Courier_BL", "Courier New", clsPDFCreator.pdfFontStyle.pdfBold)   ' Tipo TrueType
'' ''        .LoadFont("Courier_IT_BL", "Courier New", clsPDFCreator.pdfFontStyle.pdfBoldItalic) ' Tipo TrueType

'' ''        .LoadFont("TIMES", "TIMESNEWROMAN")                                      ' Tipo TrueType
'' ''        .LoadFont("TIMES_IT", "TIMESNEWROMAN", clsPDFCreator.pdfFontStyle.pdfItalic) ' Tipo TrueType
'' ''        .LoadFont("TIMES_BD", "TIMESNEWROMAN", clsPDFCreator.pdfFontStyle.pdfBold)   ' Tipo TrueType
'' ''        .LoadFont("TIMES_IT_BL", "TIMESNEWROMAN", clsPDFCreator.pdfFontStyle.pdfItalic) ' Tipo TrueType


'' ''        .LoadFontStandard("Default", "Courier New", clsPDFCreator.pdfFontStyle.pdfBoldItalic)      ' Tipo Type1
'' ''        .LoadImgFromBMPFile("Img1", "C:\Documents and Settings\All Users\Documents\My Pictures\Sample Pictures\Water lilies.bmp")
'' ''    End With
'' ''End Sub
'' ''Private Function fFontStyle(ByVal fFont As Font) As String
'' ''    Dim lRetStr As String = "Default"
'' ''    If fFont.Name = "Arial" Then
'' ''        If fFont.Style = FontStyle.Regular Then
'' ''            lRetStr = "Arial"
'' ''        ElseIf fFont.Style = FontStyle.Bold Then
'' ''            lRetStr = "Arial_BD"
'' ''        ElseIf fFont.Style = FontStyle.Italic Then
'' ''            lRetStr = "Arial_IT"
'' ''        ElseIf fFont.Style = FontStyle.Italic And fFont.Style = FontStyle.Bold Then
'' ''            lRetStr = "Arial_IT_BL"
'' ''        End If
'' ''    ElseIf fFont.Name = "Courier New" Then
'' ''        If fFont.Style = FontStyle.Regular Then
'' ''            lRetStr = "Courier"
'' ''        ElseIf fFont.Style = FontStyle.Bold Then
'' ''            lRetStr = "Courier_BD"
'' ''        ElseIf fFont.Style = FontStyle.Italic Then
'' ''            lRetStr = "Courier_IT"
'' ''        ElseIf fFont.Style = FontStyle.Italic And fFont.Style = FontStyle.Bold Then
'' ''            lRetStr = "Courier_IT_BL"
'' ''        End If
'' ''    ElseIf fFont.Name = "TIMESNEWROMAN" Then
'' ''        If fFont.Style = FontStyle.Regular Then
'' ''            lRetStr = "TIMES"
'' ''        ElseIf fFont.Style = FontStyle.Bold Then
'' ''            lRetStr = "TIMES_BD"
'' ''        ElseIf fFont.Style = FontStyle.Italic Then
'' ''            lRetStr = "TIMES_IT"
'' ''        ElseIf fFont.Style = FontStyle.Italic And fFont.Style = FontStyle.Bold Then
'' ''            lRetStr = "TIMES_IT_BL"
'' ''        End If
'' ''    End If
'' ''    Return lRetStr
'' ''End Function
'' ''Public Function PDFSetAlign(ByVal Align As ContentAlignment)
'' ''    Dim nRetNo As Int32 = 0
'' ''    Select Case Align
'' ''        Case ContentAlignment.BottomLeft, ContentAlignment.MiddleLeft, ContentAlignment.TopLeft
'' ''            nRetNo = 0
'' ''        Case ContentAlignment.TopRight, ContentAlignment.MiddleRight, ContentAlignment.TopRight
'' ''            nRetNo = 1
'' ''        Case ContentAlignment.BottomCenter, ContentAlignment.MiddleCenter, ContentAlignment.TopCenter
'' ''            nRetNo = 2
'' ''    End Select
'' ''    Return nRetNo
'' ''End Function
'' ''Private Sub PDFCompHeader(ByVal oPDF As clsPDFCreator, ByVal sMstXMLPath As String, ByVal xDip As Single)
'' ''    Try
'' ''        Dim lFlag As Boolean = True
'' ''        nPageNo += 1
'' ''        If Not chkRepHeader.Checked Then
'' ''            lFlag = (nPageNo = 1)
'' ''        End If
'' ''        'oPDF.DrawImg("Img1", 1, 10, 4, 4)
'' ''        dsMst.Tables.Clear()
'' ''        dsMst.ReadXml(sMstXMLPath)
'' ''        If dsMst.Tables(0).Rows.Count > 0 Then
'' ''            'Company Header Part 
'' ''            With pnlCompHeader
'' ''                If lFlag Then
'' ''                    If Not lSuppreCompHeaderPnl Then
'' ''                        For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                            nPrintCtr += 1
'' ''                            If nCompHeadery < .Controls(i).Location.Y + .Controls(i).Height Then
'' ''                                nCompHeadery = .Controls(i).Location.Y + .Controls(i).Height
'' ''                            End If
'' ''                            If .Controls(i).Name = "Text" Then
'' ''                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                    'oPDF.DrawText(0, 8, "emeas", fFontStyle(.Controls(i).Font), 10, clsPDFCreator.pdfTextAlign.pdfAlignLeft)
'' ''                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                    oPDF.DrawText(((.Controls(i).Location.X) / xDip), CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                End If
'' ''                            ElseIf .Controls(i).Name = "Image" Then
'' ''                                If Not CType(.Controls(i), TemplateImageField).Suppress Then
'' ''                                    'oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'' ''                                End If
'' ''                            ElseIf .Controls(i).Name = "Line" Then
'' ''                                'If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                                '    If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                '        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                                '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                                '        'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'' ''                                '    Else
'' ''                                '        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                                '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                                '        'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'' ''                                '    End If
'' ''                                'End If
'' ''                                If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                        oPDF.SetDash(0, 0)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                        oPDF.SetDash(0.05, 0.05)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                        oPDF.SetDash(0.07, 0.07)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                        oPDF.SetDash(0.1, 0.05)
'' ''                                    End If
'' ''                                    oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                    oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                    'oPDF.MoveTo(1, 10.3)
'' ''                                    'oPDF.LineTo(1, 10.9)
'' ''                                    oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                                    oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - (((CType(.Controls(i), TemplateLineField).Top)) / xDip) - 0.06) '- (CType(.Controls(i), TemplateLineField).Height / xDip)
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), (CanvasHeight - ((((CType(.Controls(i), TemplateLineField).Top) + CType(.Controls(i), TemplateLineField).Height))) / xDip) + 0.06)
'' ''                                Else
'' ''                                    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                    oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                        oPDF.SetDash(0, 0)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                        oPDF.SetDash(0.05, 0.05)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                        oPDF.SetDash(0.07, 0.07)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                        oPDF.SetDash(0.1, 0.05)
'' ''                                    End If
'' ''                                    oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

'' ''                                    oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip))
'' ''                                    oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - 0.1)


'' ''                                End If
'' ''                            ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                                'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'' ''                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                    'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height - 2, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                                    'oPDF.Rectangle(2.9, 11.27, 0.9, 0.3, clsPDFCreator.pdfPathOptions.Stroked, 0.1)
'' ''                                    If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                        oPDF.SetDash(0, 0)
'' ''                                    ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                        oPDF.SetDash(0.05, 0.05)
'' ''                                    ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                        oPDF.SetDash(0.07, 0.07)
'' ''                                    ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                        oPDF.SetDash(0.1, 0.05)
'' ''                                    End If
'' ''                                    oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                                    oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))
'' ''                                    'oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (CType(.Controls(i), TemplateBoxField).Top) / xDip) - 0.6 - (CType(.Controls(i), TemplateBoxField).Top / xDip), (CType(.Controls(i), TemplateBoxField).Width / xDip) - 0.06, ((CType(.Controls(i), TemplateBoxField).Height) / xDip), clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                                    oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), CanvasHeight - ((CType(.Controls(i), TemplateBoxField).Top) / xDip) - ((CType(.Controls(i), TemplateBoxField).Height) / xDip), (CType(.Controls(i), TemplateBoxField).Width / xDip) - 0.06, ((CType(.Controls(i), TemplateBoxField).Height) / xDip), clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                                End If
'' ''                                'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'' ''                            Else
'' ''                                If .Controls(i).Text <> "" Then
'' ''                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                        'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'' ''                                        dtTable = Nothing
'' ''                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'' ''                                            dtTable = dsMst.Tables(0)
'' ''                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                            dtTable = dsMst.Tables(1)
'' ''                                        End If
'' ''                                        If dtTable IsNot Nothing Then
'' ''                                            If .Controls(i).Tag.ToString = "DateTime" Then
'' ''                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                            Else
'' ''                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                            End If
'' ''                                        End If
'' ''                                        If .Controls(i).Text Like ".Formula_*" Then
'' ''                                            Dim strSelect As String = ""
'' ''                                            Dim frmFormula As New frmFormula
'' ''                                            frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                            Dim sFormat As String = ""
'' ''                                            For j As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                                sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                                If sFormat <> "" Then
'' ''                                                    Exit For
'' ''                                                End If
'' ''                                            Next
'' ''                                            frmFormula.sFormat = sFormat
'' ''                                            frmFormula.dtTable = dtTable
'' ''                                            frmFormula.ExecuteFormula()
'' ''                                            strSelect = frmFormula.strSelect
'' ''                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - (.Controls(i).Location.Y / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        End If
'' ''                                    End If
'' ''                                End If
'' ''                            End If
'' ''                        Next
'' ''                    End If
'' ''                Else
'' ''                    nCompHeadery = 0
'' ''                End If
'' ''            End With
'' ''        End If
'' ''    Catch ex As Exception
'' ''        MessageBox.Show(ex.Message, My.Application.Info.Title)
'' ''    End Try
'' ''End Sub

'' ''Private Sub PDFBasedHeader(ByVal oPDF As clsPDFCreator, ByVal xDip As Single)
'' ''    Dim y As Integer = 0
'' ''    Dim nTops As Integer = 0
'' ''    'Dim grpDetHght As Integer = 0
'' ''    'Dim grpFoterDetHght As Integer = 0
'' ''    Try
'' ''        If dsMst.Tables(0).Rows.Count > 0 Then
'' ''            'Master Part 
'' ''            With pnlHeader
'' ''                If Not lSuppreHeaderPnl Then
'' ''                    For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                        nPrintCtr += 1
'' ''                        If nHeadery < .Controls(i).Location.Y + .Controls(i).Height Then
'' ''                            nHeadery = .Controls(i).Location.Y + .Controls(i).Height
'' ''                        End If
'' ''                        If .Controls(i).Name = "Text" Then
'' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                            End If
'' ''                        ElseIf .Controls(i).Name = "Image" Then
'' ''                            If Not CType(.Controls(i), TemplateImageField).Suppress Then

'' ''                                'oPDF.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'' ''                            End If
'' ''                        ElseIf .Controls(i).Name = "Line" Then
'' ''                            If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                                If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                        oPDF.SetDash(0, 0)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                        oPDF.SetDash(0.05, 0.05)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                        oPDF.SetDash(0.07, 0.07)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                        oPDF.SetDash(0.1, 0.05)
'' ''                                    End If
'' ''                                    oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                    oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.B))

'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - (((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip) + 0.1) ' - 0.06) ' - (CType(.Controls(i), TemplateLineField).Height / xDip))
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), (CanvasHeight + ((((CType(.Controls(i), TemplateLineField).Top) + CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), CanvasHeight - (((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip))
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), (CanvasHeight - (((y + nCompHeadery + (CType(.Controls(i), TemplateLineField).Top) - CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                                    oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip))
'' ''                                    oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - 0.1)

'' ''                                Else
'' ''                                    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                    oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                         CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                    If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                        oPDF.SetDash(0, 0)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                        oPDF.SetDash(0.05, 0.05)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                        oPDF.SetDash(0.07, 0.07)
'' ''                                    ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                        oPDF.SetDash(0.1, 0.05)
'' ''                                    End If
'' ''                                    oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip), CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))

'' ''                                    oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                    oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - (((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip) - 0.06) '- (CType(.Controls(i), TemplateLineField).Height / xDip)
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), (CanvasHeight - ((((y + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) + CType(.Controls(i), TemplateLineField).Height))) / xDip) + 0.06)
'' ''                                    'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                    'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - (y + nCompHeadery + (CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                End If
'' ''                            End If
'' ''                        ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                            'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                    oPDF.SetDash(0, 0)
'' ''                                ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                    oPDF.SetDash(0.05, 0.05)
'' ''                                ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                    oPDF.SetDash(0.07, 0.07)
'' ''                                ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                    oPDF.SetDash(0.1, 0.05)
'' ''                                End If
'' ''                                oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                                oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))
'' ''                                oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (y + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                                'oPDF.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, y + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height - 2, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                                'oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (y + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height - 2) / xDip, 0.1)
'' ''                            End If
'' ''                            'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'' ''                        Else
'' ''                            If .Controls(i).Text <> "" Then
'' ''                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                    'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'' ''                                    dtTable = Nothing
'' ''                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'' ''                                        dtTable = dsMst.Tables(0)
'' ''                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                        dtTable = dsMst.Tables(1)
'' ''                                    End If
'' ''                                    If dtTable IsNot Nothing Then
'' ''                                        If .Controls(i).Tag.ToString = "DateTime" Then
'' ''                                            'oPDF.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((y + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        Else
'' ''                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((y + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                            'oPDF.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                        End If
'' ''                                    End If
'' ''                                    If .Controls(i).Text Like ".Formula_*" Then
'' ''                                        Dim strSelect As String = ""
'' ''                                        Dim frmFormula As New frmFormula
'' ''                                        frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                        Dim sFormat As String = ""
'' ''                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                            If sFormat <> "" Then
'' ''                                                Exit For
'' ''                                            End If
'' ''                                        Next
'' ''                                        frmFormula.sFormat = sFormat
'' ''                                        frmFormula.dtTable = dtTable
'' ''                                        frmFormula.ExecuteFormula()
'' ''                                        strSelect = frmFormula.strSelect
'' ''                                        'oPDF.printText(strSelect, .Controls(i).Location.X, y + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((y + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                    End If
'' ''                                End If
'' ''                            End If
'' ''                        End If
'' ''                    Next
'' ''                End If
'' ''            End With
'' ''        End If
'' ''    Catch ex As Exception
'' ''        MessageBox.Show(ex.Message, My.Application.Info.Title)
'' ''    End Try
'' ''End Sub

' '' ''---Detail start 

'' ''Private Sub PDFBasedDetail(ByVal oPDF As clsPDFCreator, ByVal xDip As Single)
'' ''    Dim Graphics As Graphics
'' ''    Dim yCanDraw As Integer = 0
'' ''    Dim yScale As Integer = 0
'' ''    Graphics = CreateGraphics()

'' ''    Try
'' ''        iCtr = 0
'' ''        Dim strGroup As String = ""
'' ''        Dim strGroupFooter As String = ""
'' ''        'Detail Part
'' ''        If pnlDetail.Controls.Count > 0 Then
'' ''            If dsMst.Tables.Count > 1 Then
'' ''                If dsMst.Tables(1).Rows.Count > 0 Then
'' ''                    nCounter = 0
'' ''                    With pnlDetail
'' ''                        IsGroups()
'' ''                        For j As Integer = 0 To dsMst.Tables(1).Rows.Count - 1
'' ''                            'Group Checking
'' ''                            nPrintCtr += 1
'' ''                            If tvControls.Nodes(1).Nodes.Count > 0 Then
'' ''                                Dim strSel As String = ""
'' ''                                For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
'' ''                                    strSel += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
'' ''                                Next
'' ''                                If strGroup <> strSel Then
'' ''                                    nGroup1y += PDFFunctGrouping(oPDF, j, xDip)
'' ''                                    strGroup = strSel
'' ''                                End If
'' ''                            End If

'' ''                            If Not lSuppreDetPnl Then
'' ''                                Dim k As Integer = 0
'' ''                                'Before Print Counting     
'' ''                                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                                    Dim m As Object
'' ''                                    m = 1
'' ''                                    If TypeOf pnlDetail.Controls(i) Is TemplateBoxField Then

'' ''                                        If CType(pnlDetail.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                            Dim orgWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                            Dim TotalWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font).Width
'' ''                                            If orgWidht < TotalWidht Then
'' ''                                                m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                            End If
'' ''                                        End If
'' ''                                    End If
'' ''                                    If k < .Controls(i).Location.Y + .Controls(i).Height * m Then k = .Controls(i).Location.Y + .Controls(i).Height * m
'' ''                                Next
'' ''                                nCounter += k
'' ''                                iCtr += k
'' ''                                If nGroup1y + iCtr + nGrpFooter1y > ItemHeight() Then ' (txtItemPrint.Text * 105) 
'' ''                                    nDetDety = ItemHeight() - nGroup1y - nGrpFooter1y '(txtItemPrint.Text * 105) 
'' ''                                    PDFBasedFooter(oPDF, xDip, True)
'' ''                                    oPDF.EndPage()
'' ''                                    nCounter = 0
'' ''                                    nDetDety = 0
'' ''                                    iCtr = 0
'' ''                                    nPrintCtr = 0
'' ''                                    oPDF.BeginPage()
'' ''                                    PDFCompHeader(oPDF, sMstXMLPaths, xDip)
'' ''                                    PDFBasedHeader(oPDF, xDip)
'' ''                                    IsGroups()
'' ''                                    nGroup1y = 0
'' ''                                    nGrpFooter1y = 0
'' ''                                    yCanDraw = 0
'' ''                                    nCounter += k
'' ''                                    iCtr += k
'' ''                                End If
'' ''                                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                                    If .Controls(i).Name = "Text" Then
'' ''                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                            'Can Draw 
'' ''                                            If CType(.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                                Dim m As Object
'' ''                                                m = 0
'' ''                                                Dim orgWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                                Dim TotalWidht As Object = Graphics.MeasureString(.Controls(i).Text.ToString, .Controls(i).Font).Width
'' ''                                                If orgWidht < TotalWidht Then
'' ''                                                    m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text.ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                Else
'' ''                                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                    'oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text.ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                End If
'' ''                                            End If
'' ''                                        Else
'' ''                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text.ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                            'oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                        End If
'' ''                                    ElseIf .Controls(i).Name = "Image" Then
'' ''                                        If Not CType(.Controls(i), TemplateImageField).Suppress Then
'' ''                                            'oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height + 2)
'' ''                                        End If
'' ''                                    ElseIf .Controls(i).Name = "Line" Then
'' ''                                        'If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                                        '    If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                        '        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                        '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                        '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                                        '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                                        '        'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'' ''                                        '    Else
'' ''                                        '        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                        '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                        '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                                        '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                                        '        'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'' ''                                        '    End If
'' ''                                        'End If
'' ''                                        If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                            If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                                oPDF.SetDash(0, 0)
'' ''                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                                oPDF.SetDash(0.05, 0.05)
'' ''                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                                oPDF.SetDash(0.07, 0.07)
'' ''                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                                oPDF.SetDash(0.1, 0.05)
'' ''                                            End If
'' ''                                            oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                                 CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                                 CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                            oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                                            oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                            'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), CanvasHeight - (((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip))
'' ''                                            'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), (CanvasHeight - ((((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) - CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                                        Else
'' ''                                            Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                            pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                                 CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                                 CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                            If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                                oPDF.SetDash(0, 0)
'' ''                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                                oPDF.SetDash(0.05, 0.05)
'' ''                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                                oPDF.SetDash(0.07, 0.07)
'' ''                                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                                oPDF.SetDash(0.1, 0.05)
'' ''                                            End If

'' ''                                            oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                            oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

'' ''                                            'oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                            'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                            'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip) + ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                            oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                        End If
'' ''                                    ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                                        'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'' ''                                        'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'' ''                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                            If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                                oPDF.SetDash(0, 0)
'' ''                                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                                oPDF.SetDash(0.05, 0.05)
'' ''                                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                                oPDF.SetDash(0.07, 0.07)
'' ''                                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                                oPDF.SetDash(0.1, 0.05)
'' ''                                            End If
'' ''                                            oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))

'' ''                                            oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (nGrpFooter1y + nGroup1y - k + nCounter + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                                            'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y - k + nCounter + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top - 1, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                                        End If
'' ''                                    Else
'' ''                                        If .Controls(i).Text.Trim <> "" Then
'' ''                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                                'If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'' ''                                                dtTable = Nothing
'' ''                                                If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'' ''                                                    dtTable = dsMst.Tables(0)
'' ''                                                    If .Controls(i).Tag = "DateTime" Then
'' ''                                                        'Can Draw
'' ''                                                        If CType(.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                                            Dim m As Object
'' ''                                                            m = 0
'' ''                                                            Dim orgWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                                            Dim TotalWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font).Width
'' ''                                                            If orgWidht < TotalWidht Then
'' ''                                                                m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))

'' ''                                                                'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            Else
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                                'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            End If
'' ''                                                        Else
'' ''                                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                            'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                        End If
'' ''                                                    Else
'' ''                                                        If CType(.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                                            Dim m As Object
'' ''                                                            m = 0
'' ''                                                            Dim orgWidht As Object = Graphics.MeasureString(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                                            Dim TotalWidht As Object = Graphics.MeasureString(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Font).Width
'' ''                                                            If orgWidht < TotalWidht Then
'' ''                                                                m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                                'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            Else
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                            End If
'' ''                                                        Else
'' ''                                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                            'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                        End If
'' ''                                                    End If
'' ''                                                ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                                    dtTable = dsMst.Tables(1)
'' ''                                                    'If .Controls(i).Text Like ".Formula_*" Then
'' ''                                                    '    Dim strSelect As String = ""
'' ''                                                    '    Dim frmFormula As New frmFormula
'' ''                                                    '    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                                    '    Dim sFormat As String = ""
'' ''                                                    '    For l As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                                    '        sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                                    '        If sFormat <> "" Then
'' ''                                                    '            Exit For
'' ''                                                    '        End If
'' ''                                                    '    Next
'' ''                                                    '    frmFormula.sFormat = sFormat
'' ''                                                    '    frmFormula.nRow = j
'' ''                                                    '    frmFormula.dtTable = dtTable
'' ''                                                    '    frmFormula.ExecuteFormula()
'' ''                                                    '    strSelect = frmFormula.strSelect
'' ''                                                    '    oPrint.printText(strSelect, .Controls(i).Location.X, nCounter + nDetHeadery + nHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                    'Else
'' ''                                                    If .Controls(i).Tag = "DateTime" Then
'' ''                                                        If CType(.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                                            Dim m As Object
'' ''                                                            m = 0
'' ''                                                            Dim orgWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                                            Dim TotalWidht As Object = Graphics.MeasureString(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Font).Width
'' ''                                                            If orgWidht < TotalWidht Then
'' ''                                                                m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                                'oPrint.printText(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            Else
'' ''                                                                'oPrint.printText(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                            End If
'' ''                                                        Else
'' ''                                                            'oPrint.printText(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                        End If
'' ''                                                    Else
'' ''                                                        If CType(.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                                            Dim m As Object
'' ''                                                            m = 0
'' ''                                                            Dim orgWidht As Object = Graphics.MeasureString(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                                            Dim TotalWidht As Object = Graphics.MeasureString(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Font).Width
'' ''                                                            If orgWidht < TotalWidht Then
'' ''                                                                m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                                'oPrint.printText(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            Else
'' ''                                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                                'oPrint.printText(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                            End If
'' ''                                                        Else
'' ''                                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                            'oPrint.printText(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                        End If
'' ''                                                    End If
'' ''                                                End If
'' ''                                            End If
'' ''                                            If .Controls(i).Text Like ".Formula_*" Then
'' ''                                                Dim strSelect As String = ""
'' ''                                                Dim frmFormula As New frmFormula
'' ''                                                frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                                Dim sFormat As String = ""
'' ''                                                For l As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                                    sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(l).Text)
'' ''                                                    If sFormat <> "" Then
'' ''                                                        Exit For
'' ''                                                    End If
'' ''                                                Next
'' ''                                                frmFormula.sFormat = sFormat
'' ''                                                frmFormula.dtTable = dtTable
'' ''                                                If dtTable.TableName = "Details" OrElse dtTable.TableName = "Table1" Then
'' ''                                                    frmFormula.nRow = j
'' ''                                                End If
'' ''                                                frmFormula.ExecuteFormula()
'' ''                                                strSelect = frmFormula.strSelect
'' ''                                                If CType(.Controls(i), TemplateBoxField).CanDraw Then
'' ''                                                    Dim m As Object
'' ''                                                    m = 0
'' ''                                                    Dim orgWidht As Object = Graphics.MeasureString(strSelect, .Controls(i).Font, New SizeF(.Controls(i).Width, .Controls(i).Height), string_format).Width
'' ''                                                    Dim TotalWidht As Object = Graphics.MeasureString(strSelect, .Controls(i).Font).Width
'' ''                                                    If orgWidht < TotalWidht Then
'' ''                                                        m = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
'' ''                                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                        'oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, m * .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                    Else
'' ''                                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                        'oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                    End If
'' ''                                                Else
'' ''                                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                    'oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y - k + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                End If
'' ''                                            End If
'' ''                                            If dtTable IsNot Nothing Then
'' ''                                            End If
'' ''                                        End If
'' ''                                    End If
'' ''                                Next
'' ''                                'If Not chkPageWise.Checked Then
'' ''                                '    If nGroup1y + iCtr + nGrpFooter1y >= (txtItemPrint.Text * 105) Then
'' ''                                '        nDetDety = nGroup1y + iCtr + nGrpFooter1y
'' ''                                '        WindowsBasedFooter(oPrint, True)
'' ''                                '        oPrint.PageEject()
'' ''                                '        nCounter = 0
'' ''                                '        nDetDety = 0
'' ''                                '        iCtr = 0
'' ''                                '        nPrintCtr = 0
'' ''                                '        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
'' ''                                '        WindowsBasedHeader(oPrint)
'' ''                                '        IsGroups()
'' ''                                '        nGroup1y = 0
'' ''                                '        nGrpFooter1y = 0
'' ''                                '    End If
'' ''                                'Else
'' ''                                '    If nPrintCtr >= 150 Then
'' ''                                '        nDetDety = nCounter
'' ''                                '        oPrint.PageEject()
'' ''                                '        nCounter = 0
'' ''                                '        nDetDety = 0
'' ''                                '        iCtr = 0
'' ''                                '        nPrintCtr = 0
'' ''                                '        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
'' ''                                '        WindowsBasedHeader(oPrint)
'' ''                                '        IsGroups()
'' ''                                '        nGroup1y = 0
'' ''                                '        nGrpFooter1y = 0
'' ''                                '    End If
'' ''                                'End If
'' ''                            End If
'' ''                            If tvControls.Nodes(1).Nodes.Count > 0 Then
'' ''                                If j <> dsMst.Tables(1).Rows.Count - 1 Then
'' ''                                    Dim strSel As String = ""
'' ''                                    Dim strSelOrginal As String = ""
'' ''                                    For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
'' ''                                        strSel += dsMst.Tables(1).Rows(j + 1).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
'' ''                                        strSelOrginal += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
'' ''                                    Next
'' ''                                    If strSelOrginal <> strSel Then
'' ''                                        nGrpFooter1y += PDFFunctGroupFooter1(oPDF, j, xDip)
'' ''                                        strGroupFooter = strSel
'' ''                                    End If
'' ''                                End If
'' ''                            End If
'' ''                        Next
'' ''                        nDetDety = nCounter
'' ''                    End With
'' ''                End If
'' ''                If tvControls.Nodes(1).Nodes.Count > 0 Then
'' ''                    nGrpFooter1y += PDFFunctGroupFooter1(oPDF, dsMst.Tables(1).Rows.Count - 1, xDip)
'' ''                End If
'' ''                'Detail Footer Part
'' ''                With pnlDetailFooter
'' ''                    If Not lSuppreDetFotPnl Then
'' ''                        For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                            nPrintCtr += 1
'' ''                            If .Controls(i).Name = "Text" Then
'' ''                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text.ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                End If
'' ''                            ElseIf .Controls(i).Name = "Image" Then
'' ''                                If Not CType(.Controls(i), TemplateImageField).Suppress Then
'' ''                                    'oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'' ''                                End If
'' ''                            ElseIf .Controls(i).Name = "Line" Then
'' ''                                If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                                    'If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                    '    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                    '    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                    '    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                                    '    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                                    '    'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'' ''                                    'Else
'' ''                                    '    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                    '    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                    '    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                                    '    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                                    '    'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'' ''                                    'End If
'' ''                                    If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                            oPDF.SetDash(0, 0)
'' ''                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                            oPDF.SetDash(0.05, 0.05)
'' ''                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                            oPDF.SetDash(0.1, 0.05)
'' ''                                        End If
'' ''                                        oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                        oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                             CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                             CType(.Controls(i), TemplateLineField).LineColor.B))

'' ''                                        oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                        oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                                        'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), CanvasHeight - (((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip))
'' ''                                        'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), (CanvasHeight - ((((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) - CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                                    Else
'' ''                                        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                        oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                             CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                             CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                        If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                            oPDF.SetDash(0, 0)
'' ''                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                            oPDF.SetDash(0.05, 0.05)
'' ''                                        ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                            oPDF.SetDash(0.1, 0.05)
'' ''                                        End If
'' ''                                        oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)

'' ''                                        oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                        oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

'' ''                                        'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                        'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                    End If
'' ''                                End If
'' ''                            ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then

'' ''                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                        If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                            oPDF.SetDash(0, 0)
'' ''                                        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                            oPDF.SetDash(0.05, 0.05)
'' ''                                        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                            oPDF.SetDash(0.07, 0.07)
'' ''                                        ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                            oPDF.SetDash(0.1, 0.05)
'' ''                                        End If
'' ''                                        oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                                        oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))

'' ''                                        oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                                        'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y - k + nCounter + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top - 1, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                                    End If
'' ''                                    'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                                End If
'' ''                            Else
'' ''                                If .Controls(i).Text.Trim <> "" Then
'' ''                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                        dtTable = Nothing
'' ''                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Tag) Then
'' ''                                            dtTable = dsMst.Tables(0)
'' ''                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                            dtTable = dsMst.Tables(1)
'' ''                                        End If
'' ''                                        If dtTable IsNot Nothing Then
'' ''                                            If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
'' ''                                                Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", "")
'' ''                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, nSum.ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                'oPrint.printText(nSum.ToString, .Controls(i).Location.X,       nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                            Else
'' ''                                                If .Controls(i).Text Like ".Formula_*" Then
'' ''                                                    Dim strSelect As String = ""
'' ''                                                    Dim frmFormula As New frmFormula
'' ''                                                    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                                    Dim sFormat As String = ""
'' ''                                                    For j As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                                        sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                                        If sFormat <> "" Then
'' ''                                                            Exit For
'' ''                                                        End If
'' ''                                                    Next
'' ''                                                    frmFormula.sFormat = sFormat
'' ''                                                    frmFormula.dtTable = dtTable
'' ''                                                    frmFormula.ExecuteFormula()
'' ''                                                    strSelect = frmFormula.strSelect
'' ''                                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                    'oPrint.printText(strSelect, .Controls(i).Location.X,          nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                Else
'' ''                                                    If .Controls(i).Tag = "DateTime" Then
'' ''                                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                        'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                    Else
'' ''                                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                                        'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                                    End If
'' ''                                                End If
'' ''                                            End If
'' ''                                        End If
'' ''                                    End If
'' ''                                End If
'' ''                            End If
'' ''                            If nDetFootery < .Controls(i).Location.Y + .Controls(i).Height Then
'' ''                                nDetFootery = .Controls(i).Location.Y + .Controls(i).Height
'' ''                            End If
'' ''                        Next
'' ''                    End If
'' ''                End With
'' ''                If Not chkPageWise.Checked Then
'' ''                    With pnlDetail
'' ''                        If pnlDetail.Controls.Count > 0 Then
'' ''                            If Not lSuppreDetPnl Then
'' ''                                'For i = 0 To Math.Round(((((txtItemPrint.Text * 105) - iCtr) / 105)), 0) 'dsMst.Tables(1).Rows.Count
'' ''                                '    nDetDety += .Controls(.Controls.Count - 1).Location.Y + .Controls(.Controls.Count - 1).Height
'' ''                                '    oPrint.printText(SrNo.ToString + " ", .Controls(.Controls.Count - 1).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.MiddleLeft, False)
'' ''                                '    SrNo += 1
'' ''                                'Next
'' ''                                nDetDety += ItemHeight() - iCtr - nGroup1y - nGrpFooter1y - nDetFootery '(txtItemPrint.Text * 105)
'' ''                                oPDF.DrawText(.Controls(.Controls.Count - 1).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y) / xDip), 0, "", fFontStyle(.Controls(.Controls.Count - 1).Font), .Controls(.Controls.Count - 1).Font.Size, CType(.Controls(.Controls.Count - 1), TemplateBoxField).TextAlign)
'' ''                                'oPrint.printText("", .Controls(.Controls.Count - 1).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.BottomLeft, False)
'' ''                                'oPrint.printText("space", .Controls(.Controls.Count - 1).Location.X, 262.5, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.BottomLeft, False)
'' ''                            End If
'' ''                        End If
'' ''                    End With
'' ''                End If
'' ''            End If
'' ''        End If
'' ''    Catch ex As Exception
'' ''        MessageBox.Show(ex.Message, My.Application.Info.Title)
'' ''    End Try
'' ''End Sub


'' ''Private Function PDFFunctGrouping(ByVal oPDF As clsPDFCreator, ByVal k As Integer, ByVal xDip As Single)
'' ''    Dim lRetVal As Boolean = True
'' ''    Dim nGrp1 As Integer = 0
'' ''    If tvControls.Nodes(1).Nodes.Count > 0 Then
'' ''        With pnlGroup1
'' ''            If Not lSuppreGroup1Pnl Then
'' ''                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                    If nGrp1 < .Controls(i).Location.Y + .Controls(i).Height Then
'' ''                        nGrp1 = .Controls(i).Location.Y + .Controls(i).Height
'' ''                    End If
'' ''                Next
'' ''                If nGrp1 + nGrpFooter1y + iCtr > ItemHeight() Then '(txtItemPrint.Text * 105)
'' ''                    nDetDety = ItemHeight() - nGrp1  '(txtItemPrint.Text * 105)
'' ''                    PDFBasedFooter(oPDF, xDip, True)
'' ''                    oPDF.EndPage()
'' ''                    nCounter = 0
'' ''                    nDetDety = 0
'' ''                    iCtr = 0
'' ''                    nPrintCtr = 0
'' ''                    nGroup1y = 0
'' ''                    nGrpFooter1y = 0
'' ''                    'nGrp1 = 0
'' ''                    oPDF.BeginPage()
'' ''                    PDFCompHeader(oPDF, sMstXMLPaths, xDip)
'' ''                    PDFBasedHeader(oPDF, xDip)
'' ''                    IsGroups()
'' ''                End If
'' ''                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                    nPrintCtr += 1
'' ''                    'If nGrp1 < .Controls(i).Location.Y + .Controls(i).Height Then
'' ''                    '    nGrp1 = .Controls(i).Location.Y + .Controls(i).Height
'' ''                    'End If
'' ''                    If .Controls(i).Name = "Text" Then
'' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                            'oPrint.printText(.Controls(i).Text, .Controls(i).Location.X,  nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Image" Then
'' ''                        If Not CType(.Controls(i), TemplateImageField).Suppress Then
'' ''                            'oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Line" Then
'' ''                        If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                            'If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                            '    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                            '    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                            '    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                            '    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                            '    'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'' ''                            'Else
'' ''                            '    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                            '    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                            '    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                            '    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                            '    'oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'' ''                            'End If
'' ''                            If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                    oPDF.SetDash(0, 0)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                    oPDF.SetDash(0.07, 0.07)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                    oPDF.SetDash(0.05, 0.05)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                    oPDF.SetDash(0.1, 0.05)
'' ''                                End If
'' ''                                oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.B))

'' ''                                oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

'' ''                                'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), CanvasHeight - (((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip))
'' ''                                'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), (CanvasHeight - ((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) - CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                            Else
'' ''                                Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                    oPDF.SetDash(0, 0)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                    oPDF.SetDash(0.05, 0.05)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                    oPDF.SetDash(0.07, 0.07)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                    oPDF.SetDash(0.1, 0.05)
'' ''                                End If
'' ''                                oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                                oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                            End If
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                        'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                            'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                            If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                oPDF.SetDash(0, 0)
'' ''                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                oPDF.SetDash(0.05, 0.05)
'' ''                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                oPDF.SetDash(0.07, 0.07)
'' ''                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                oPDF.SetDash(0.1, 0.05)
'' ''                            End If
'' ''                            oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))
'' ''                            oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                        End If
'' ''                        'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'' ''                    Else
'' ''                        If .Controls(i).Text <> "" Then
'' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'' ''                                dtTable = Nothing
'' ''                                If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'' ''                                    dtTable = dsMst.Tables(0)
'' ''                                    If .Controls(i).Tag.ToString = "DateTime" Then
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    Else
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    End If
'' ''                                ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                    dtTable = dsMst.Tables(1)
'' ''                                    If .Controls(i).Tag.ToString = "DateTime" Then
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPrint.printText(CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    Else
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(k)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPrint.printText(dtTable.Rows(k)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    End If
'' ''                                End If

'' ''                                If .Controls(i).Text Like ".Formula_*" Then
'' ''                                    Dim strSelect As String = ""
'' ''                                    Dim frmFormula As New frmFormula
'' ''                                    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                    Dim sFormat As String = ""
'' ''                                    For j As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                        sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                        If sFormat <> "" Then
'' ''                                            Exit For
'' ''                                        End If
'' ''                                    Next
'' ''                                    frmFormula.sFormat = sFormat
'' ''                                    frmFormula.dtTable = dtTable
'' ''                                    frmFormula.ExecuteFormula()
'' ''                                    strSelect = frmFormula.strSelect
'' ''                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                End If
'' ''                            End If
'' ''                        End If
'' ''                    End If
'' ''                Next
'' ''            End If
'' ''            'If Not chkPageWise.Checked Then
'' ''            '    'If nGrp1 >= (txtItemPrint.Text * 105) Then
'' ''            '    '    nDetDety = nCounter
'' ''            '    '    WindowsBasedFooter(oPrint, True)
'' ''            '    '    oPrint.PageEject()
'' ''            '    '    nCounter = 0
'' ''            '    '    nDetDety = 0
'' ''            '    '    iCtr = 0
'' ''            '    '    nPrintCtr = 0
'' ''            '    '    nGroup1y = 0
'' ''            '    '    nGrpFooter1y = 0
'' ''            '    '    nGrp1 = 0
'' ''            '    '    WindowsBasedCompHeader(oPrint, sMstXMLPaths)
'' ''            '    '    WindowsBasedHeader(oPrint)
'' ''            '    '    IsGroups()
'' ''            '    'End If
'' ''            'Else
'' ''            '    If nPrintCtr >= 150 Then
'' ''            '        nDetDety = nCounter
'' ''            '        oPrint.PageEject()
'' ''            '        nCounter = 0
'' ''            '        nDetDety = 0
'' ''            '        iCtr = 0
'' ''            '        nPrintCtr = 0
'' ''            '        nGroup1y = 0
'' ''            '        nGrpFooter1y = 0
'' ''            '        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
'' ''            '        WindowsBasedHeader(oPrint)
'' ''            '        IsGroups()
'' ''            '    End If
'' ''            'End If
'' ''        End With
'' ''    End If
'' ''    Return nGrp1
'' ''End Function
'' ''Private Function PDFFunctGroupFooter1(ByVal oPDF As clsPDFCreator, ByVal k As Integer, ByVal xDip As Single)
'' ''    Dim lRetVal As Boolean = True
'' ''    Dim nGrpFot1 As Integer = 0
'' ''    If tvControls.Nodes(1).Nodes.Count > 0 Then
'' ''        With pnlGroupFooter1
'' ''            If Not lSuppreGroupFooter1Pnl Then
'' ''                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                    If nGrpFot1 < .Controls(i).Location.Y + .Controls(i).Height Then
'' ''                        nGrpFot1 = .Controls(i).Location.Y + .Controls(i).Height
'' ''                    End If
'' ''                Next
'' ''                If Not chkPageWise.Checked Then
'' ''                    If nGroup1y + iCtr + nGrpFot1 + nGrpFooter1y > ItemHeight() Then '(txtItemPrint.Text * 105)
'' ''                        nDetDety = ItemHeight() - nGroup1y - nGrpFooter1y '(txtItemPrint.Text * 105)
'' ''                        PDFBasedFooter(oPDF, xDip, True)
'' ''                        oPDF.EndPage()
'' ''                        nCounter = 0
'' ''                        nDetDety = 0
'' ''                        iCtr = 0
'' ''                        nPrintCtr = 0
'' ''                        nGroup1y = 0
'' ''                        nGrpFooter1y = 0
'' ''                        'nGrpFot1 = 0
'' ''                        oPDF.BeginPage()
'' ''                        PDFCompHeader(oPDF, sMstXMLPaths, xDip)
'' ''                        PDFBasedHeader(oPDF, xDip)
'' ''                        IsGroups()
'' ''                    End If
'' ''                Else
'' ''                    If nPrintCtr >= 150 Then
'' ''                        nDetDety = nCounter
'' ''                        oPDF.EndPage()
'' ''                        nCounter = 0
'' ''                        nDetDety = 0
'' ''                        iCtr = 0
'' ''                        nPrintCtr = 0
'' ''                        nGroup1y = 0
'' ''                        nGrpFooter1y = 0
'' ''                        oPDF.BeginPage()
'' ''                        PDFCompHeader(oPDF, sMstXMLPaths, xDip)
'' ''                        PDFBasedHeader(oPDF, xDip)
'' ''                        IsGroups()
'' ''                    End If
'' ''                End If

'' ''                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                    nPrintCtr += 1
'' ''                    If .Controls(i).Name = "Text" Then
'' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                            'oPDF.printText(.Controls(i).Text, .Controls(i).Location.X,    nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Image" Then
'' ''                        If Not CType(.Controls(i), TemplateImageField).Suppress Then
'' ''                            'oPDF.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Line" Then
'' ''                        'If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                        '    If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                        '        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                        '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                        '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                        '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                        '        'oPDF.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'' ''                        '    Else
'' ''                        '        Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                        '        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                        '        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                        '        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                        '        'oPDF.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'' ''                        '    End If
'' ''                        'End If
'' ''                        If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                            If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                oPDF.SetDash(0, 0)
'' ''                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                oPDF.SetDash(0.05, 0.05)
'' ''                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                oPDF.SetDash(0.07, 0.07)
'' ''                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                oPDF.SetDash(0.1, 0.05)
'' ''                            End If
'' ''                            oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                 CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                 CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                            oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                            oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                            'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), CanvasHeight - (((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip))
'' ''                            'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), (CanvasHeight - ((((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) - CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                        Else
'' ''                            Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                            pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                 CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                 CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                            If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                oPDF.SetDash(0, 0)
'' ''                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                oPDF.SetDash(0.07, 0.07)
'' ''                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                oPDF.SetDash(0.05, 0.05)
'' ''                            ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                oPDF.SetDash(0.1, 0.05)
'' ''                            End If

'' ''                            oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                            oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)


'' ''                            'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                            'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                            'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 15) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                            'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                        'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then

'' ''                            If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                oPDF.SetDash(0, 0)
'' ''                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                oPDF.SetDash(0.05, 0.05)
'' ''                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                oPDF.SetDash(0.07, 0.07)
'' ''                            ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                oPDF.SetDash(0.1, 0.05)
'' ''                            End If
'' ''                            oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                            oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))
'' ''                            oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                            'oPDF.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left,                      nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                        End If
'' ''                        'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'' ''                    Else
'' ''                        If .Controls(i).Text <> "" Then
'' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                'If .Controls(i).Tag.ToString = "Decimal" OrElse .Controls(i).Tag.ToString = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'' ''                                dtTable = Nothing
'' ''                                If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'' ''                                    dtTable = dsMst.Tables(0)
'' ''                                    If .Controls(i).Tag.ToString = "DateTime" Then
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPDF.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    Else
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPDF.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    End If
'' ''                                ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                    dtTable = dsMst.Tables(1)
'' ''                                    If .Controls(i).Tag.ToString = "DateTime" Then
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        'oPDF.printText(CDate(dtTable.Rows(k)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                    Else
'' ''                                        If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
'' ''                                            Dim strCon As String = ""
'' ''                                            For p As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
'' ''                                                strCon += tvControls.Nodes(1).Nodes(p).Text + " = '" + dtTable.Rows(k)(tvControls.Nodes(1).Nodes(p).Text).ToString + "',"
'' ''                                            Next
'' ''                                            strCon = strCon.Substring(0, strCon.Length - 1)
'' ''                                            Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", strCon)
'' ''                                            'oPDF.printText(nSum.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, nSum.ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                        Else
'' ''                                            oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                            oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(k)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                            'oPDF.printText(dtTable.Rows(k)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign), CType(.Controls(i), TemplateBoxField).CanDraw)
'' ''                                        End If
'' ''                                    End If
'' ''                                End If

'' ''                                If .Controls(i).Text Like ".Formula_*" Then
'' ''                                    Dim strSelect As String = ""
'' ''                                    Dim frmFormula As New frmFormula
'' ''                                    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                    Dim sFormat As String = ""
'' ''                                    For j As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                        sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                        If sFormat <> "" Then
'' ''                                            Exit For
'' ''                                        End If
'' ''                                    Next
'' ''                                    frmFormula.sFormat = sFormat
'' ''                                    frmFormula.dtTable = dtTable
'' ''                                    frmFormula.ExecuteFormula()
'' ''                                    strSelect = frmFormula.strSelect
'' ''                                    oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                    oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, PDFSetAlign(CType(.Controls(i), TemplateBoxField).TextAlign))
'' ''                                End If
'' ''                            End If
'' ''                        End If
'' ''                    End If
'' ''                Next
'' ''            End If
'' ''        End With
'' ''    End If
'' ''    Return nGrpFot1
'' ''End Function
' '' ''Private Sub PDFBasedFooter(ByVal oPrint As eZeePrint, Optional ByVal lDetCall As Boolean = False)
' '' ''    Try
' '' ''        With pnlFooter
' '' ''            If Not lSuppreFooterPnl Then
' '' ''                For i As Integer = .Controls.Count - 1 To 0 Step -1
' '' ''                    nPrintCtr += 1
' '' ''                    If .Controls(i).Name = "Text" Then
' '' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
' '' ''                            If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then

' '' ''                                oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
' '' ''                            End If
' '' ''                        End If
' '' ''                    ElseIf .Controls(i).Name = "Image" Then
' '' ''                        If Not CType(.Controls(i), TemplateImageField).Suppress Then
' '' ''                            oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
' '' ''                        End If
' '' ''                    ElseIf .Controls(i).Name = "Line" Then
' '' ''                        If Not CType(.Controls(i), TemplateLineField).Suppress Then
' '' ''                            If CType(.Controls(i), TemplateLineField).Vertical Then
' '' ''                                Dim pPen As New Pen(.Controls(i).ForeColor)
' '' ''                                pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
' '' ''                                pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
' '' ''                                pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
' '' ''                                oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
' '' ''                            Else
' '' ''                                Dim pPen As New Pen(.Controls(i).ForeColor)
' '' ''                                pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
' '' ''                                pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
' '' ''                                pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
' '' ''                                oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
' '' ''                            End If
' '' ''                        End If
' '' ''                    ElseIf .Controls(i).Name = "Rectangle" Then
' '' ''                        'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
' '' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
' '' ''                            oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
' '' ''                        End If
' '' ''                        'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
' '' ''                    Else
' '' ''                        If .Controls(i).Text.Trim <> "" Then
' '' ''                            'If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
' '' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
' '' ''                                dtTable = Nothing
' '' ''                                If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
' '' ''                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
' '' ''                                        dtTable = dsMst.Tables(0)
' '' ''                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
' '' ''                                        dtTable = dsMst.Tables(1)
' '' ''                                    End If
' '' ''                                    If .Controls(i).Text Like ".Formula_*" Then
' '' ''                                        Dim strSelect As String = ""
' '' ''                                        Dim frmFormula As New frmFormula
' '' ''                                        frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
' '' ''                                        Dim sFormat As String = ""
' '' ''                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
' '' ''                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
' '' ''                                            If sFormat <> "" Then
' '' ''                                                Exit For
' '' ''                                            End If
' '' ''                                        Next
' '' ''                                        frmFormula.sFormat = sFormat
' '' ''                                        frmFormula.dtTable = dtTable
' '' ''                                        frmFormula.ExecuteFormula()
' '' ''                                        strSelect = frmFormula.strSelect
' '' ''                                        oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
' '' ''                                    Else
' '' ''                                        If dtTable IsNot Nothing Then
' '' ''                                            If .Controls(i).Tag = "DateTime" Then
' '' ''                                                oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
' '' ''                                            Else
' '' ''                                                oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
' '' ''                                            End If
' '' ''                                        End If
' '' ''                                    End If
' '' ''                                End If
' '' ''                            End If
' '' ''                        End If
' '' ''                    End If
' '' ''                    If chkPageWise.Checked Then
' '' ''                        If nPrintCtr >= 150 Then
' '' ''                            oPrint.PageEject()
' '' ''                            nDetDety = 0
' '' ''                            nPrintCtr = 0
' '' ''                            WindowsBasedCompHeader(oPrint, sMstXMLPaths)
' '' ''                            WindowsBasedHeader(oPrint)
' '' ''                        End If
' '' ''                    End If
' '' ''                Next
' '' ''            End If
' '' ''        End With
' '' ''    Catch ex As Exception
' '' ''        MessageBox.Show(ex.Message, My.Application.Info.Title)
' '' ''    End Try
' '' ''End Sub


' '' ''---Detail End 



'' ''Private Sub PDFBasedFooter(ByVal oPDF As clsPDFCreator, ByVal xDip As Single, Optional ByVal lDetCall As Boolean = False)
'' ''    Try
'' ''        With pnlFooter
'' ''            If Not lSuppreFooterPnl Then
'' ''                For i As Integer = .Controls.Count - 1 To 0 Step -1
'' ''                    nPrintCtr += 1
'' ''                    If .Controls(i).Name = "Text" Then
'' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                            If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
'' ''                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, .Controls(i).Text, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, CType(.Controls(i), TemplateBoxField).TextAlign)
'' ''                            End If
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Image" Then
'' ''                        If Not CType(.Controls(i), TemplateImageField).Suppress Then
'' ''                            'oPDF.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Line" Then
'' ''                        If Not CType(.Controls(i), TemplateLineField).Suppress Then
'' ''                            'If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                            '    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                            '    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                            '    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                            '    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                            '    'oPDF.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'' ''                            'Else
'' ''                            '    Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                            '    pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                            '    pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'' ''                            '    pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'' ''                            '    'oPDF.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'' ''                            'End If
'' ''                            If CType(.Controls(i), TemplateLineField).Vertical Then
'' ''                                If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                    oPDF.SetDash(0, 0)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                    oPDF.SetDash(0.05, 0.05)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                    oPDF.SetDash(0.07, 0.07)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                    oPDF.SetDash(0.1, 0.05)
'' ''                                End If
'' ''                                oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)
'' ''                                oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), CanvasHeight - (((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top)) / xDip))
'' ''                                'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left + 30) / xDip), (CanvasHeight - ((((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) - CType(.Controls(i), TemplateLineField).Height))) / xDip))
'' ''                            Else
'' ''                                Dim pPen As New Pen(.Controls(i).ForeColor)
'' ''                                pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'' ''                                oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateLineField).LineColor.R, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.G, _
'' ''                                                     CType(.Controls(i), TemplateLineField).LineColor.B))
'' ''                                If CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Solid Then
'' ''                                    oPDF.SetDash(0, 0)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dot Then
'' ''                                    oPDF.SetDash(0.05, 0.05)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.Dash Then
'' ''                                    oPDF.SetDash(0.07, 0.07)
'' ''                                ElseIf CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateLineField).LineStyle = DashStyle.DashDot Then
'' ''                                    oPDF.SetDash(0.1, 0.05)
'' ''                                End If
'' ''                                oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateLineField).LineThickness)

'' ''                                oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + 0.03, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)

'' ''                                'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + CType(.Controls(i), TemplateLineField).Height) / xDip) + 0.08)
'' ''                                'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip) - 0.1, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip) - ((CType(.Controls(i), TemplateLineField).Height) / xDip) - 0.03)
'' ''                                'oPDF.MoveTo(((CType(.Controls(i), TemplateLineField).Left) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                ''oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                                'oPDF.LineTo(((CType(.Controls(i), TemplateLineField).Left) / xDip) + ((CType(.Controls(i), TemplateLineField).Width) / xDip), CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top) / xDip))
'' ''                            End If

'' ''                        End If
'' ''                    ElseIf .Controls(i).Name = "Rectangle" Then
'' ''                        'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'' ''                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                If CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Custom Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Solid Then
'' ''                                    oPDF.SetDash(0, 0)
'' ''                                ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dot Then
'' ''                                    oPDF.SetDash(0.05, 0.05)
'' ''                                ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.Dash Then
'' ''                                    oPDF.SetDash(0.07, 0.07)
'' ''                                ElseIf CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDotDot Or CType(.Controls(i), TemplateBoxField).LineStyle = DashStyle.DashDot Then
'' ''                                    oPDF.SetDash(0.1, 0.05)
'' ''                                End If
'' ''                                oPDF.SetLineWidth(0.01 * CType(.Controls(i), TemplateBoxField).LineThickness)
'' ''                                oPDF.SetColorStroke(RGB(CType(.Controls(i), TemplateBoxField).LineColor.R, CType(.Controls(i), TemplateBoxField).LineColor.G, CType(.Controls(i), TemplateBoxField).LineColor.B))

'' ''                                oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                                'oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y - k + nCounter + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top - 1, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                            End If

'' ''                            oPDF.Rectangle((CType(.Controls(i), TemplateBoxField).Left / xDip), (CanvasHeight - (nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top) / xDip) - (.Controls(i).Height / xDip), CType(.Controls(i), TemplateBoxField).Width / xDip, (CType(.Controls(i), TemplateBoxField).Height) / xDip, clsPDFCreator.pdfPathOptions.Stroked, (0.1 * CType(.Controls(i), TemplateBoxField).AllR) - 0.1)
'' ''                            'oPDF.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left,                      nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'' ''                        End If
'' ''                        'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'' ''                    Else
'' ''                        If .Controls(i).Text.Trim <> "" Then
'' ''                            'If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'' ''                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'' ''                                dtTable = Nothing
'' ''                                If Not (CType(.Controls(i), TemplateBoxField).DupSupp AndAlso lDetCall) Then
'' ''                                    If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'' ''                                        dtTable = dsMst.Tables(0)
'' ''                                    ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'' ''                                        dtTable = dsMst.Tables(1)
'' ''                                    End If
'' ''                                    If .Controls(i).Text Like ".Formula_*" Then
'' ''                                        Dim strSelect As String = ""
'' ''                                        Dim frmFormula As New frmFormula
'' ''                                        frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'' ''                                        Dim sFormat As String = ""
'' ''                                        For j As Integer = 0 To tvControls.Nodes.Count - 1
'' ''                                            sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'' ''                                            If sFormat <> "" Then
'' ''                                                Exit For
'' ''                                            End If
'' ''                                        Next
'' ''                                        frmFormula.sFormat = sFormat
'' ''                                        frmFormula.dtTable = dtTable
'' ''                                        frmFormula.ExecuteFormula()
'' ''                                        strSelect = frmFormula.strSelect
'' ''                                        oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                        oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, strSelect, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, CType(.Controls(i), TemplateBoxField).TextAlign)
'' ''                                    Else
'' ''                                        If dtTable IsNot Nothing Then
'' ''                                            If .Controls(i).Tag = "DateTime" Then
'' ''                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, CType(.Controls(i), TemplateBoxField).TextAlign)
'' ''                                            Else
'' ''                                                oPDF.SetColorFill(RGB(.Controls(i).ForeColor.R, .Controls(i).ForeColor.G, .Controls(i).ForeColor.B))
'' ''                                                oPDF.DrawText(.Controls(i).Location.X / xDip, CanvasHeight - ((nGrpFooter1y + nGroup1y + nDetFootery + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y) / xDip) - (.Controls(i).Height / xDip), .Controls(i).Width, dtTable.Rows(0)(.Controls(i).Text).ToString, fFontStyle(.Controls(i).Font), .Controls(i).Font.Size, CType(.Controls(i), TemplateBoxField).TextAlign)
'' ''                                            End If
'' ''                                        End If
'' ''                                    End If
'' ''                                End If
'' ''                            End If
'' ''                        End If
'' ''                    End If
'' ''                    If chkPageWise.Checked Then
'' ''                        If nPrintCtr >= 150 Then
'' ''                            oPDF.EndPage()
'' ''                            nDetDety = 0
'' ''                            nPrintCtr = 0
'' ''                            oPDF.BeginPage()
'' ''                            PDFCompHeader(oPDF, sMstXMLPaths, xDip)
'' ''                            PDFBasedHeader(oPDF, xDip)
'' ''                        End If
'' ''                    End If
'' ''                Next
'' ''            End If
'' ''        End With
'' ''    Catch ex As Exception
'' ''        MessageBox.Show(ex.Message, My.Application.Info.Title)
'' ''    End Try
'' ''End Sub
#End Region
#Region "Original "
'Private Sub WindowsBasedDetail(ByVal oPrint As eZeePrint)
'    Try
'        iCtr = 0
'        Dim strGroup As String = ""
'        Dim strGroupFooter As String = ""
'        'Detail Part
'        If pnlDetail.Controls.Count > 0 Then
'            If dsMst.Tables.Count > 1 Then
'                If dsMst.Tables(1).Rows.Count > 0 Then
'                    nCounter = 0
'                    With pnlDetail
'                        IsGroups()
'                        For j As Integer = 0 To dsMst.Tables(1).Rows.Count - 1
'                            'Group Checking
'                            nPrintCtr += 1
'                            If tvControls.Nodes(1).Nodes.Count > 0 Then
'                                Dim strSel As String = ""
'                                For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
'                                    strSel += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
'                                Next
'                                If strGroup <> strSel Then
'                                    nGroup1y += FunctGrouping(oPrint, j)
'                                    strGroup = strSel
'                                End If
'                            End If
'                            If Not lSuppreDetPnl Then
'                                Dim k As Integer = 0
'                                For i As Integer = .Controls.Count - 1 To 0 Step -1
'                                    If .Controls(i).Name = "Text" Then
'                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'                                            oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                        End If
'                                    ElseIf .Controls(i).Name = "Image" Then
'                                        If Not CType(.Controls(i), TemplateImageField).Suppress Then
'                                            oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height + 2)
'                                        End If
'                                    ElseIf .Controls(i).Name = "Line" Then
'                                        If Not CType(.Controls(i), TemplateLineField).Suppress Then
'                                            If CType(.Controls(i), TemplateLineField).Vertical Then
'                                                Dim pPen As New Pen(.Controls(i).ForeColor)
'                                                pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'                                                pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'                                                pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'                                                oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'                                            Else
'                                                Dim pPen As New Pen(.Controls(i).ForeColor)
'                                                pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'                                                pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'                                                pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'                                                oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'                                            End If
'                                        End If
'                                    ElseIf .Controls(i).Name = "Rectangle" Then
'                                        'e.Graphics.DrawRectangle(New Pen(.Controls(i).ForeColor), .Controls(i).Location.X, .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height) '.Controls(i).Location.Y
'                                        'e.Graphics.DrawRectangle(p, DataCollection.Data(iCnt).X, DataCollection.Data(iCnt).Y - LstY, DataCollection.Data(iCnt).Width, DataCollection.Data(iCnt).Height)      
'                                        If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'                                            oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nCounter + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top - 1, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'                                        End If
'                                    Else
'                                        If .Controls(i).Text.Trim <> "" Then
'                                            If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'                                                'If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then string_format.Alignment = StringAlignment.Far Else string_format.Alignment = StringAlignment.Near
'                                                dtTable = Nothing
'                                                If dsMst.Tables(0).Columns.Contains(.Controls(i).Text) Then
'                                                    dtTable = dsMst.Tables(0)
'                                                    If .Controls(i).Tag = "DateTime" Then
'                                                        oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    Else
'                                                        oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    End If
'                                                ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'                                                    dtTable = dsMst.Tables(1)
'                                                    'If .Controls(i).Text Like ".Formula_*" Then
'                                                    '    Dim strSelect As String = ""
'                                                    '    Dim frmFormula As New frmFormula
'                                                    '    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'                                                    '    Dim sFormat As String = ""
'                                                    '    For l As Integer = 0 To tvControls.Nodes.Count - 1
'                                                    '        sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'                                                    '        If sFormat <> "" Then
'                                                    '            Exit For
'                                                    '        End If
'                                                    '    Next
'                                                    '    frmFormula.sFormat = sFormat
'                                                    '    frmFormula.nRow = j
'                                                    '    frmFormula.dtTable = dtTable
'                                                    '    frmFormula.ExecuteFormula()
'                                                    '    strSelect = frmFormula.strSelect
'                                                    '    oPrint.printText(strSelect, .Controls(i).Location.X, nCounter + nDetHeadery + nHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    'Else
'                                                    If .Controls(i).Tag = "DateTime" Then
'                                                        oPrint.printText(CDate(dtTable.Rows(j)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    Else
'                                                        oPrint.printText(dtTable.Rows(j)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    End If
'                                                    'End If
'                                                End If
'                                            End If
'                                            If .Controls(i).Text Like ".Formula_*" Then
'                                                Dim strSelect As String = ""
'                                                Dim frmFormula As New frmFormula
'                                                frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'                                                Dim sFormat As String = ""
'                                                For l As Integer = 0 To tvControls.Nodes.Count - 1
'                                                    sFormat = TreeViewFind(tvControls.Nodes(l).Nodes, .Controls(i).Text, tvControls.Nodes(l).Text)
'                                                    If sFormat <> "" Then
'                                                        Exit For
'                                                    End If
'                                                Next
'                                                frmFormula.sFormat = sFormat
'                                                frmFormula.dtTable = dtTable
'                                                If dtTable.TableName = "Details" OrElse dtTable.TableName = "Table1" Then
'                                                    frmFormula.nRow = j
'                                                End If
'                                                frmFormula.ExecuteFormula()
'                                                strSelect = frmFormula.strSelect
'                                                oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nCounter + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                            End If
'                                            If dtTable IsNot Nothing Then
'                                            End If
'                                        End If
'                                    End If
'                                    If k < .Controls(i).Location.Y + .Controls(i).Height Then k = .Controls(i).Location.Y + .Controls(i).Height
'                                Next
'                                nCounter += k
'                                iCtr += k

'                                If Not chkPageWise.Checked Then
'                                    If nGroup1y + iCtr + nGrpFooter1y >= (txtItemPrint.Text * 105) Then
'                                        nDetDety = nGroup1y + iCtr + nGrpFooter1y
'                                        WindowsBasedFooter(oPrint, True)
'                                        oPrint.PageEject()
'                                        nCounter = 0
'                                        nDetDety = 0
'                                        iCtr = 0
'                                        nPrintCtr = 0
'                                        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
'                                        WindowsBasedHeader(oPrint)
'                                        IsGroups()
'                                        nGroup1y = 0
'                                        nGrpFooter1y = 0
'                                    End If
'                                Else
'                                    If nPrintCtr >= 150 Then
'                                        nDetDety = nCounter
'                                        oPrint.PageEject()
'                                        nCounter = 0
'                                        nDetDety = 0
'                                        iCtr = 0
'                                        nPrintCtr = 0
'                                        WindowsBasedCompHeader(oPrint, sMstXMLPaths)
'                                        WindowsBasedHeader(oPrint)
'                                        IsGroups()
'                                        nGroup1y = 0
'                                        nGrpFooter1y = 0
'                                    End If
'                                End If
'                            End If
'                            If tvControls.Nodes(1).Nodes.Count > 0 Then
'                                If j <> dsMst.Tables(1).Rows.Count - 1 Then
'                                    Dim strSel As String = ""
'                                    Dim strSelOrginal As String = ""
'                                    For k As Integer = 0 To tvControls.Nodes(1).Nodes.Count - 1
'                                        strSel += dsMst.Tables(1).Rows(j + 1).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
'                                        strSelOrginal += dsMst.Tables(1).Rows(j).Item(tvControls.Nodes(1).Nodes(k).Text).ToString + " "
'                                    Next
'                                    If strSelOrginal <> strSel Then
'                                        nGrpFooter1y += FunctGroupFooter1(oPrint, j)
'                                        strGroupFooter = strSel
'                                    End If
'                                End If
'                            End If
'                        Next
'                        nDetDety = nCounter
'                    End With
'                End If
'                If tvControls.Nodes(1).Nodes.Count > 0 Then
'                    nGrpFooter1y += FunctGroupFooter1(oPrint, dsMst.Tables(1).Rows.Count - 1)
'                End If
'                'Detail Footer Part
'                With pnlDetailFooter
'                    If Not lSuppreDetFotPnl Then
'                        For i As Integer = .Controls.Count - 1 To 0 Step -1
'                            nPrintCtr += 1
'                            If .Controls(i).Name = "Text" Then
'                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'                                    oPrint.printText(.Controls(i).Text.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                End If
'                            ElseIf .Controls(i).Name = "Image" Then
'                                If Not CType(.Controls(i), TemplateImageField).Suppress Then
'                                    oPrint.printImage(.Controls(i).BackgroundImage, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height)
'                                End If
'                            ElseIf .Controls(i).Name = "Line" Then
'                                If Not CType(.Controls(i), TemplateLineField).Suppress Then
'                                    If CType(.Controls(i), TemplateLineField).Vertical Then
'                                        Dim pPen As New Pen(.Controls(i).ForeColor)
'                                        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'                                        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'                                        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'                                        oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 6, CType(.Controls(i), TemplateLineField).Left + 3, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top - 6 + CType(.Controls(i), TemplateLineField).Height, pPen)
'                                    Else
'                                        Dim pPen As New Pen(.Controls(i).ForeColor)
'                                        pPen.Width = CType(.Controls(i), TemplateLineField).LineThickness
'                                        pPen.Color = CType(.Controls(i), TemplateLineField).LineColor
'                                        pPen.DashStyle = CType(.Controls(i), TemplateLineField).LineStyle
'                                        oPrint.LineDraw(CType(.Controls(i), TemplateLineField).Left + 6, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, CType(.Controls(i), TemplateLineField).Left - 6 + CType(.Controls(i), TemplateLineField).Width, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateLineField).Top + 3, pPen)
'                                    End If
'                                End If
'                            ElseIf .Controls(i).Name = "Rectangle" Then
'                                If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'                                    oPrint.RectangleDraw(CType(.Controls(i), TemplateBoxField).Left, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + CType(.Controls(i), TemplateBoxField).Top, CType(.Controls(i), TemplateBoxField).Width, CType(.Controls(i), TemplateBoxField).Height, CType(.Controls(i), TemplateBoxField).AllR, CType(.Controls(i), TemplateBoxField).LineColor, CType(.Controls(i), TemplateBoxField).LineThickness, CType(.Controls(i), TemplateBoxField).LineStyle)
'                                End If
'                            Else
'                                If .Controls(i).Text.Trim <> "" Then
'                                    If Not CType(.Controls(i), TemplateBoxField).Suppress Then
'                                        dtTable = Nothing
'                                        If dsMst.Tables(0).Columns.Contains(.Controls(i).Tag) Then
'                                            dtTable = dsMst.Tables(0)
'                                        ElseIf dsMst.Tables(1).Columns.Contains(.Controls(i).Text) Then
'                                            dtTable = dsMst.Tables(1)
'                                        End If
'                                        If dtTable IsNot Nothing Then
'                                            If .Controls(i).Tag = "Decimal" OrElse .Controls(i).Tag = "Int32" Then
'                                                Dim nSum As Object = dtTable.Compute("sum(" & .Controls(i).Text & ")", "")
'                                                oPrint.printText(nSum.ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                            Else
'                                                If .Controls(i).Text Like ".Formula_*" Then
'                                                    Dim strSelect As String = ""
'                                                    Dim frmFormula As New frmFormula
'                                                    frmFormula.sFormula = .Controls(i).Tag.ToString.Split("|")(0).Replace("+", ";+;").Replace("-", ";-;").Replace("*", ";*;").Replace("/", ";/;").Replace("iif(", "�iif(�").Replace("Iif(", "�iif(�").Replace("IIf(", "�iif(�").Replace("IIF(", "�iif(�").Replace("iIF(", "�iif(�").Replace("IiF(", "�iif(�").Replace("iIf(", "�iif(�").Replace("iiF(", "�iif(�").Replace(",", "�,�").Replace(")", "�)�").Replace("=", "�=�").Replace(">", "�>�").Replace("<", "�<�")
'                                                    Dim sFormat As String = ""
'                                                    For j As Integer = 0 To tvControls.Nodes.Count - 1
'                                                        sFormat = TreeViewFind(tvControls.Nodes(j).Nodes, .Controls(i).Text, tvControls.Nodes(j).Text)
'                                                        If sFormat <> "" Then
'                                                            Exit For
'                                                        End If
'                                                    Next
'                                                    frmFormula.sFormat = sFormat
'                                                    frmFormula.dtTable = dtTable
'                                                    frmFormula.ExecuteFormula()
'                                                    strSelect = frmFormula.strSelect
'                                                    oPrint.printText(strSelect, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                Else
'                                                    If .Controls(i).Tag = "DateTime" Then
'                                                        oPrint.printText(CDate(dtTable.Rows(0)(.Controls(i).Text).ToString).ToString("dd-MM-yyyy"), .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    Else
'                                                        oPrint.printText(dtTable.Rows(0)(.Controls(i).Text).ToString, .Controls(i).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(i).Location.Y, .Controls(i).Width, .Controls(i).Height, .Controls(i).ForeColor, .Controls(i).Font, CType(.Controls(i), TemplateBoxField).TextAlign, CType(.Controls(i), TemplateBoxField).CanDraw)
'                                                    End If
'                                                End If
'                                            End If
'                                        End If
'                                    End If
'                                End If
'                            End If
'                            If nDetFootery < .Controls(i).Location.Y + .Controls(i).Height Then
'                                nDetFootery = .Controls(i).Location.Y + .Controls(i).Height
'                            End If
'                        Next
'                    End If
'                End With
'                With pnlDetail
'                    If pnlDetail.Controls.Count > 0 Then
'                        If Not lSuppreDetPnl AndAlso Not chkPageWise.Checked Then
'                            'For i = 0 To Math.Round(((((txtItemPrint.Text * 105) - iCtr) / 105)), 0) 'dsMst.Tables(1).Rows.Count
'                            '    nDetDety += .Controls(.Controls.Count - 1).Location.Y + .Controls(.Controls.Count - 1).Height
'                            '    oPrint.printText(SrNo.ToString + " ", .Controls(.Controls.Count - 1).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.MiddleLeft, False)
'                            '    SrNo += 1
'                            'Next
'                            nDetDety += (txtItemPrint.Text * 105) - iCtr - nGroup1y - nGrpFooter1y
'                            oPrint.printText("space", .Controls(.Controls.Count - 1).Location.X, nGrpFooter1y + nGroup1y + nDetDety + nDetHeadery + nHeadery + nCompHeadery + .Controls(.Controls.Count - 1).Location.Y, .Controls(.Controls.Count - 1).Width, .Controls(.Controls.Count - 1).Height, .Controls(.Controls.Count - 1).ForeColor, .Controls(.Controls.Count - 1).Font, ContentAlignment.BottomLeft, False)
'                        End If
'                    End If
'                End With
'            End If
'        End If
'    Catch ex As Exception
'        MessageBox.Show(ex.Message, My.Application.Info.Title)
'    End Try
'End Sub
#End Region
