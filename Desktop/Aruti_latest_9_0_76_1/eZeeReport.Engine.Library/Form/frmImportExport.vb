﻿Imports System.IO
Imports Aruti.Data

Public Class frmImportExport
    Private sMst As String
    Private sDet As String
    Private sFormula As String
    Private dsMst As New DataSet
    Private dsDet As New DataSet
    Private dsFormula As New DataSet
    Private _sImpExp As String

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Public Sub ShowMeNow(ByVal sImpExp As String)
        _sImpExp = sImpExp
        If sImpExp = "Export" Then
            lblWelcome.Text = "Welcome to Export Templated Print out"
            lblImpExp.Text = "Export"
            sMst = AppSettings._Object._ApplicationPath & "\TemplateMst"
            sDet = AppSettings._Object._ApplicationPath & "\TemplateDet"
            sFormula = AppSettings._Object._ApplicationPath & "\TemplateFormula"
        Else
            lblWelcome.Text = "Welcome to Import Templated Print out"
            lblImpExp.Text = "Import"
            sMst = AppSettings._Object._ApplicationPath & "\CopyTemplateMst"
            sDet = AppSettings._Object._ApplicationPath & "\CopyTemplateDet"
            sFormula = AppSettings._Object._ApplicationPath & "\CopyTemplateFormula"
        End If

        dsMst.ReadXml(sMst)
        dsDet.ReadXml(sDet)
        dsFormula.ReadXml(sFormula)

        Dim nCtr As Integer = 0
        tvControl.Font = New Font("Arial", 8, FontStyle.Bold)
        tvGrouping.Font = New Font("Arial", 8, FontStyle.Bold)
        With dsMst.Tables(0)
            For i As Int16 = 0 To .Rows.Count - 1
                If .Rows(i)("PrintType") IsNot DBNull.Value Then
                    tvControl.Nodes.Add(.Rows(i)("PrintType"))
                End If
            Next
        End With
        tvControl.Sort()

        'ShowDialog()
    End Sub

    Private Sub AddDetailPart()
        If tvControl.SelectedNode IsNot Nothing Then
            tvGrouping.Nodes.Add(tvControl.SelectedNode.Text)
            tvGrouping.Nodes.Remove(tvControl.SelectedNode)
            btnOk.Enabled = True
        End If
    End Sub

    Private Sub tvControl_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvControl.DoubleClick
        AddDetailPart()
    End Sub

    Private Sub tvGrouping_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvGrouping.DoubleClick
        Remove()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        AddDetailPart()
    End Sub

    Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnSelect.Click
        Remove()
    End Sub

    Private Sub Remove()
        If tvGrouping.SelectedNode IsNot Nothing Then
            tvControl.Nodes.Add(tvGrouping.SelectedNode.Text)
            tvControl.Sort()
            If tvGrouping.Nodes.Count > 0 Then
                tvGrouping.Nodes.Remove(tvGrouping.SelectedNode)
            End If
        End If
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If tvGrouping.Nodes.Count > 0 Then
                If _sImpExp = "Export" Then
                    ExportData()
                Else
                    ImportData()
                End If
                Close()
            Else
                MessageBox.Show("No Data are selected.", "eZee Message")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "eZee Message")
        End Try
    End Sub

    Private Sub ExportData()
        Dim sCopyMst As String = AppSettings._Object._ApplicationPath & "\CopyTemplateMst"
        Dim sCopyDet As String = AppSettings._Object._ApplicationPath & "\CopyTemplateDet"
        Dim sCopyFormula As String = AppSettings._Object._ApplicationPath & "\CopyTemplateFormula"

        Dim dsCopy As New DataSet

        Dim strSelect As String = ""
        Dim sIndex As String = ""
        Dim lYes As Boolean = True
        If File.Exists(sCopyMst) Then
            If MessageBox.Show("CopyTemplate Already exist, dou want to still excute?", "eZee Message", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
                lYes = False
            End If
        End If
        If lYes Then
            If File.Exists(sCopyMst) Then
                File.Delete(sCopyMst)
            End If
            If File.Exists(sCopyDet) Then
                File.Delete(sCopyDet)
            End If
            If File.Exists(sCopyFormula) Then
                File.Delete(sCopyFormula)
            End If

            If tvGrouping.Nodes.Count > 0 Then
                For i As Integer = 0 To tvGrouping.Nodes.Count - 1
                    strSelect += "'" + tvGrouping.Nodes(i).Text + "',"
                Next
            End If
            If strSelect.Length > 0 Then
                strSelect = strSelect.Substring(0, strSelect.Length - 1)
            End If

            dsCopy.Clear()
            dsCopy.Tables.Add("CopyTable")
            sIndex = ""

            For i As Integer = 0 To dsMst.Tables(0).Columns.Count - 1
                dsCopy.Tables(0).Columns.Add(dsMst.Tables(0).Columns(i).Caption, dsMst.Tables(0).Columns(i).DataType)
            Next

            For Each drow As DataRow In dsMst.Tables(0).Select(" PrintType in (" & strSelect & ")")
                dsCopy.Tables(0).Rows.Add(drow.ItemArray)
                sIndex += drow("trnasid").ToString + ","
            Next
            If sIndex.Length > 0 Then
                sIndex = sIndex.Substring(0, sIndex.Length - 1)
                dsCopy.WriteXml(sCopyMst, XmlWriteMode.WriteSchema)

                dsCopy.Clear()
                dsCopy.Tables.Remove("CopyTable")
                dsCopy.Tables.Add("CopyTable")
                For Each dCol As DataColumn In dsDet.Tables(0).Columns
                    dsCopy.Tables("CopyTable").Columns.Add(dCol.Caption, dCol.DataType)
                Next

                For Each drow As DataRow In dsDet.Tables(0).Select(" TypeId in (" & sIndex & ")")
                    dsCopy.Tables(0).Rows.Add(drow.ItemArray)
                Next
                dsCopy.WriteXml(sCopyDet, XmlWriteMode.WriteSchema)

                dsCopy.Clear()
                dsCopy.Tables.Remove("CopyTable")
                dsCopy.Tables.Add("CopyTable")
                For Each dCol As DataColumn In dsFormula.Tables(0).Columns
                    dsCopy.Tables("CopyTable").Columns.Add(dCol.Caption, dCol.DataType)
                Next
                For Each drow As DataRow In dsFormula.Tables(0).Select(" TypeId in (" & sIndex & ")")
                    dsCopy.Tables(0).Rows.Add(drow.ItemArray)
                Next
                dsCopy.WriteXml(sCopyFormula, XmlWriteMode.WriteSchema)
                MessageBox.Show("Exported.............", "eZee Message")
            End If
        End If
    End Sub

    Private Sub ImportData()
        Dim sCopyMst As String = AppSettings._Object._ApplicationPath & "\TemplateMst"
        Dim sCopyDet As String = AppSettings._Object._ApplicationPath & "\TemplateDet"
        Dim sCopyFormula As String = AppSettings._Object._ApplicationPath & "\TemplateFormula"
        Dim dsCopy As New DataSet
        Dim dsTemp As New DataSet
        Dim sIndex As String = ""
        Dim strSelect As String = ""
        If tvGrouping.Nodes.Count > 0 Then
            For i As Integer = 0 To tvGrouping.Nodes.Count - 1
                strSelect += "'" + tvGrouping.Nodes(i).Text + "',"
            Next
        End If
        If strSelect.Length > 0 Then
            strSelect = strSelect.Substring(0, strSelect.Length - 1)
        End If
        dsCopy.Clear()

        dsCopy.ReadXml(sCopyMst)


        'Master Part
        dsTemp.Tables.Clear()
        dsTemp.Tables.Add("Copy")
        For i As Integer = 0 To dsMst.Tables(0).Columns.Count - 1
            dsTemp.Tables(0).Columns.Add(dsMst.Tables(0).Columns(i).Caption, dsMst.Tables(0).Columns(i).DataType)
        Next

        For Each drow As DataRow In dsMst.Tables(0).Select(" PrintType in (" & strSelect & ")")
            dsTemp.Tables(0).Rows.Add(drow.ItemArray)
            sIndex += drow("trnasid").ToString + ","
        Next
        If sIndex.Length > 0 Then
            sIndex = sIndex.Substring(0, sIndex.Length - 1)

            Dim nid As Integer
            If dsCopy.Tables(0).Rows.Count > 0 Then
                nid = dsCopy.Tables(0).Select("trnasid = max(trnasid)")(0)(0)
            Else
                nid = 0
            End If
            For i As Integer = 0 To dsTemp.Tables(0).Rows.Count - 1
                For j As Integer = 0 To dsMst.Tables(0).Columns.Count - 1
                    If j = 0 Then
                        nid += 1
                        dsCopy.Tables(0).Rows.Add()
                        dsCopy.Tables(0).Rows(dsCopy.Tables(0).Rows.Count - 1)(j) = nid
                    Else
                        dsCopy.Tables(0).Rows(dsCopy.Tables(0).Rows.Count - 1)(j) = dsTemp.Tables(0).Rows(i)(j)
                    End If
                Next
            Next
            dsCopy.WriteXml(sCopyMst, XmlWriteMode.WriteSchema)

            'Detail Part
            dsTemp.Tables.Clear()
            dsTemp.Tables.Add("Copy")

            dsCopy.Clear()
            dsCopy.Tables.Clear()
            dsCopy.ReadXml(sCopyDet)

            For i As Integer = 0 To dsDet.Tables(0).Columns.Count - 1
                dsTemp.Tables(0).Columns.Add(dsDet.Tables(0).Columns(i).Caption, dsDet.Tables(0).Columns(i).DataType)
            Next

            For Each drow As DataRow In dsDet.Tables(0).Select(" TypeId in (" & sIndex & ")")
                dsTemp.Tables(0).Rows.Add(drow.ItemArray)
            Next

            For i As Integer = 0 To dsTemp.Tables(0).Rows.Count - 1
                For j As Integer = 0 To dsDet.Tables(0).Columns.Count - 1
                    If j = 0 Then
                        nid += 1
                        dsCopy.Tables(0).Rows.Add()
                        dsCopy.Tables(0).Rows(dsCopy.Tables(0).Rows.Count - 1)(j) = nid
                    Else

                        dsCopy.Tables(0).Rows(dsCopy.Tables(0).Rows.Count - 1)(j) = dsTemp.Tables(0).Rows(i)(j)
                    End If
                Next
            Next
            dsCopy.WriteXml(sCopyDet, XmlWriteMode.WriteSchema)

            'Formula Part
            dsTemp.Tables.Clear()
            dsCopy.Clear()
            dsCopy.Tables.Clear()
            dsCopy.ReadXml(sCopyFormula)
            dsTemp.Tables.Add("Copy")
            For i As Integer = 0 To dsFormula.Tables(0).Columns.Count - 1
                dsTemp.Tables(0).Columns.Add(dsFormula.Tables(0).Columns(i).Caption, dsFormula.Tables(0).Columns(i).DataType)
            Next

            For Each drow As DataRow In dsFormula.Tables(0).Select(" TypeId in (" & sIndex & ")")
                dsFormula.Tables(0).Rows.Add(drow.ItemArray)
            Next

            For i As Integer = 0 To dsFormula.Tables(0).Rows.Count - 1
                For j As Integer = 0 To dsFormula.Tables(0).Columns.Count - 1
                    If j = 0 Then
                        nid += 1
                        dsCopy.Tables(0).Rows.Add()
                        dsCopy.Tables(0).Rows(dsCopy.Tables(0).Rows.Count - 1)(j) = nid
                    Else
                        dsCopy.Tables(0).Rows(dsCopy.Tables(0).Rows.Count - 1)(j) = dsTemp.Tables(0).Rows(i)(j)
                    End If
                Next
            Next
            dsCopy.WriteXml(sCopyFormula, XmlWriteMode.WriteSchema)

            MessageBox.Show("Imported.............", "eZee Message")
        End If
    End Sub

    Private Sub frmImportExport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowMeNow("Import")
    End Sub

    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click
        If tvControl.Nodes.Count > 0 Then
            For i As Integer = tvControl.Nodes.Count - 1 To 0 Step -1
                tvGrouping.Nodes.Add(tvControl.Nodes(i).Text)
                tvGrouping.Nodes.Remove(tvControl.Nodes(i))
            Next
            tvGrouping.Sort()
        End If
    End Sub

    Private Sub btnUnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnSelectAll.Click
        If tvGrouping.Nodes.Count > 0 Then
            For i As Integer = tvGrouping.Nodes.Count - 1 To 0 Step -1
                tvControl.Nodes.Add(tvGrouping.Nodes(i).Text)
                tvControl.Nodes.Remove(tvGrouping.Nodes(i))
            Next
            tvControl.Sort()
        End If
    End Sub

End Class
