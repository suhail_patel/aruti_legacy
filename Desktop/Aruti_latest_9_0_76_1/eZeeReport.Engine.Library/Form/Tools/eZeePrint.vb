Imports ArutiReport.Engine.Library.eZee_Sample
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.IO

Public Class eZeePrint
    Dim Ev1
    Dim iCtr As Integer = 0
    Public lOnLine As Boolean = False
    Public nPgHeight, nPgWidth As Double
    Public nLine As Double
    Public aPrint As New ArrayList
    'Dim PrintDialog1 As New Windows.Forms.PrintDialog
    Dim PrintDocument1 As New System.Drawing.Printing.PrintDocument
    Dim PrintPreviewDialog1 As New Windows.Forms.PrintPreviewDialog
    Dim PageSetupDialog As New Windows.Forms.PageSetupDialog
    Public nTotLine As Integer
    Public nHt As Integer
    Public nWidth As Integer
    Dim ImageFile As String = ""
    Dim nX As Single
    Dim nY As Single
    Dim nWdth As Single
    Dim nHght As Single
    Dim string_format As StringFormat
    Dim pgLen As Object
    Public yCanDraw As Object = 0
    Public yScale As Object
    Dim Origin As Object = -1
    Public k As Object

    Public nTop As Single = 25
    Public nLeft As Single = 25
    Public nRight As Single = 25
    Public nBottom As Single = 25


    Dim bm As Bitmap
    Sub New()
        ' Add any initialization after the InitializeComponent() call.
        '        PrintDialog1.Document = PrintDocument1
        PageSetupDialog.Document = PrintDocument1
        PrintPreviewDialog1.PrintPreviewControl.Zoom = 1.0
    End Sub
    Sub PageLen(ByVal nWidth As Integer, ByVal nHeight As Integer)
        Dim pkCustomSize1 As New Printing.PaperSize("Custom Paper Size", nWidth, nHeight)
        PageSetupDialog.PageSettings.PaperSize = pkCustomSize1
        'PageSetupDialog.PageSettings.Landscape = True
    End Sub
    Sub Show(ByVal frm As Windows.Forms.Form, Optional ByRef nSize As Integer = 1)

        If lOnLine = False Then
            'PrintDialog1.ShowDialog()
        End If
        iCtr = 0
        PrintPreviewDialog1.Top = My.Computer.Screen.Bounds.Top
        PrintPreviewDialog1.Width = My.Computer.Screen.Bounds.Width - 70
        PrintPreviewDialog1.Height = My.Computer.Screen.Bounds.Height - 220



        'PrintDocument1.DefaultPageSettings.Margins.Left = 60
        'PrintDocument1.DefaultPageSettings.Margins.Top = 60
        ''PrintDocument1.DefaultPageSettings

        'PrintDocument1.OriginAtMargins = True

        'PrintDocument1.DefaultPageSettings.Margins = New Printing.Margins.
        'PrintDocument1.DefaultPageSettings.Margins = New Printing.Margins(20, 60, 0, 0)
        PrintDocument1.DefaultPageSettings.Margins = New Printing.Margins(nLeft, 0, nTop, 0)

        PrintDocument1.OriginAtMargins = True
        'PrintDocument1.PrinterSettings = PrintSetting
        'PrintDocument1.DefaultPageSettings.PaperSize.PaperName = PaperKind.A4
        'PrintPreviewDialog1.Owner.StartPosition = Windows.Forms.FormStartPosition.CenterScreen
        AddHandler PrintDocument1.BeginPrint, AddressOf Me.PrintDocument1_BeginPrint
        AddHandler PrintDocument1.PrintPage, AddressOf Me.PrintDocument1_PrintPage
        PrintPreviewDialog1.Document = PrintDocument1

        If nSize = 1 Then 'OrElse Not PrintSetting.DefaultPageSettings.Landscape 
            PrintPreviewDialog1.Document.DefaultPageSettings.Landscape = False
            pgLen = PrintDocument1.DefaultPageSettings.PaperSize.Width - (nLeft - 25) - (nRight - 25)
        ElseIf nSize = 2 Then 'OrElse PrintSetting.DefaultPageSettings.Landscape 
            PrintPreviewDialog1.Document.DefaultPageSettings.Landscape = True
            pgLen = PrintDocument1.DefaultPageSettings.PaperSize.Height - (nLeft - 25) - (nRight - 25)
        End If
        Dim printArea As RectangleF = Me.PrintDocument1.DefaultPageSettings.PrintableArea
        bm = New Bitmap(Convert.ToInt32(Math.Floor(printArea.Width)), Convert.ToInt32(Math.Floor(printArea.Height)))

        Aruti.Data.Set_Logo(PrintPreviewDialog1, Aruti.Data.enArutiApplicatinType.Aruti_Payroll)

        PrintPreviewDialog1.MdiParent = frm
        'PrintPreviewDialog1.Owner.WindowState = Windows.Forms.FormWindowState.Maximized
        PrintPreviewDialog1.ShowDialog()

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        iCtr = 0
    End Sub

    'Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
    '    bm = New Bitmap(e.PageBounds.Width * 2, e.PageBounds.Height * 2, e.Graphics)

    '    bm.SetResolution(2 * 100, 2 * 100)
    '    Dim G As Graphics
    '    G = Graphics.FromImage(bm)
    '    G.Clear(Color.White)
    '    DoDrawing(e, G)
    'End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        'Dim PrintFont As Font
        'While orgWidht < TotalWidht
        '    k += e.Graphics.MeasureString(aPrint(iCtr), aPrint(iCtr + 8), New SizeF(IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)), string_format).Height
        '    orgWidht += orgWidht
        'End While

        'Do While orgWidht < TotalWidht
        '    k += aPrint(iCtr + 4)
        '    orgWidht += e.Graphics.MeasureString(aPrint(iCtr), aPrint(iCtr + 8), New SizeF(IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)), string_format).Width
        'Loop
        Dim nsize As Single = 0
        If ImageFile.Trim <> "" Then
            Dim newImage As Image = Image.FromFile(ImageFile)
            e.Graphics.DrawImage(newImage, nX, nY, nWdth, nHght)
        End If

        While iCtr <= UBound(aPrint.ToArray)
            string_format = New StringFormat
            'If aPrint(iCtr + 11).ToString <> "EJECT" Then
            '    If Origin < aPrint(iCtr + 2) Then
            '        Origin = aPrint(iCtr + 2)
            '        yScale += yCanDraw
            '        yCanDraw = 0
            '    End If
            'End If
            If aPrint(iCtr + 11).ToString = "EJECT" Then
                If iCtr + 11 < aPrint.Count - 1 Then
                    e.HasMorePages = True
                    iCtr = iCtr + 12
                    yScale = 0
                    Origin = 0
                Else
                    e.HasMorePages = False
                End If
                Exit While
            Else
                If aPrint(iCtr + 11) = "TEXT" Then
                    SetAlign(aPrint(iCtr + 9), aPrint(iCtr + 10))
                    If Origin < aPrint(iCtr + 2) Then
                        Origin = aPrint(iCtr + 2)
                        yScale += yCanDraw
                        yCanDraw = 0
                    End If
                    If aPrint(iCtr + 1) < pgLen Then
                        'If aPrint(iCtr + 10) Then ' Can Draw True then 
                        '    'Dim k As Object
                        '    k = 0
                        '    Dim orgWidht As Object = e.Graphics.MeasureString(aPrint(iCtr), aPrint(iCtr + 8), New SizeF(IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)), string_format).Width
                        '    Dim TotalWidht As Object = e.Graphics.MeasureString(aPrint(iCtr), aPrint(iCtr + 8)).Width
                        '    If orgWidht < TotalWidht Then
                        '        k = Math.Ceiling(Math.Round(TotalWidht / orgWidht, TotalWidht.ToString.Split(".")(1).Length))    'aPrint(iCtr + 4)
                        '        If yCanDraw < aPrint(iCtr + 4) * (k - 1) Then
                        '            yCanDraw += aPrint(iCtr + 4) * (k - 1)
                        '        End If
                        '        e.Graphics.DrawString(aPrint(iCtr), aPrint(iCtr + 8), New SolidBrush(aPrint(iCtr + 7)), New RectangleF(aPrint(iCtr + 1), yScale + aPrint(iCtr + 2), IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4) * k), string_format) 'aPrint(iCtr + 4) +
                        '    Else
                        '        e.Graphics.DrawString(aPrint(iCtr), aPrint(iCtr + 8), New SolidBrush(aPrint(iCtr + 7)), New RectangleF(aPrint(iCtr + 1), yScale + aPrint(iCtr + 2), IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)), string_format)
                        '    End If

                        'Else
                        e.Graphics.DrawString(aPrint(iCtr), aPrint(iCtr + 8), New SolidBrush(aPrint(iCtr + 7)), New RectangleF(aPrint(iCtr + 1), yScale + aPrint(iCtr + 2), IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)), string_format)
                        'End If

                        'e.Graphics.DrawString(aPrint(iCtr), aPrint(iCtr + 8), New SolidBrush(aPrint(iCtr + 7)), New RectangleF(aPrint(iCtr + 1), aPrint(iCtr + 2), IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)), string_format)
                    End If
                ElseIf aPrint(iCtr + 11) = "LINE" Then
                    If aPrint(iCtr) < pgLen Then
                        If Origin < aPrint(iCtr + 1) Then
                            Origin = aPrint(iCtr + 1)
                            yScale += yCanDraw
                            yCanDraw = 0
                        End If
                        e.Graphics.DrawLine(aPrint(iCtr + 7), aPrint(iCtr), CType(yScale, Single) + aPrint(iCtr + 1), IIf(aPrint(iCtr + 2) > pgLen, aPrint(iCtr + 2) - (aPrint(iCtr + 2) - pgLen), aPrint(iCtr + 2)), CType(yScale, Single) + aPrint(iCtr + 3))
                    End If
                ElseIf aPrint(iCtr + 11) = "RECTANGLE" Then
                    If aPrint(iCtr) + 2 < pgLen Then
                        Dim gpGraphicsPath As New GraphicsPath
                        Dim objBox As New BoxControl
                        gpGraphicsPath = objBox.DrawCurveRectangle(aPrint(iCtr) + 2, yScale + aPrint(iCtr + 1) + 3, IIf((aPrint(iCtr) + 2) + (aPrint(iCtr + 2) - 4) > pgLen, pgLen - (aPrint(iCtr) + 2), aPrint(iCtr + 2) - 4), aPrint(iCtr + 3) - 2, aPrint(iCtr + 6), aPrint(iCtr + 6), aPrint(iCtr + 6), aPrint(iCtr + 6))
                        Dim p As New Pen(CType(aPrint(iCtr + 7), Color), aPrint(iCtr + 4))
                        p.DashStyle = aPrint(iCtr + 5)
                        e.Graphics.DrawPath(p, gpGraphicsPath)
                    End If
                ElseIf aPrint(iCtr + 11) = "IMAGE" Then
                    If aPrint(iCtr + 1) + 2 < pgLen Then
                        If Origin < aPrint(iCtr + 2) Then
                            Origin = aPrint(iCtr + 2)
                            yScale += yCanDraw
                            yCanDraw = 0
                        End If

                        Dim ms As New MemoryStream()
                        CType(aPrint(iCtr), Image).Save(ms, ImageFormat.Jpeg)
                        e.Graphics.DrawImage(Image.FromStream(ms), New Rectangle(aPrint(iCtr + 1), yScale + aPrint(iCtr + 2), IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)))
                        ms.Dispose()
                        ms = Nothing
                        'e.Graphics.DrawImage(aPrint(iCtr), New Rectangle(aPrint(iCtr + 1), yScale + aPrint(iCtr + 2), IIf(aPrint(iCtr + 1) + aPrint(iCtr + 3) > pgLen, pgLen - aPrint(iCtr + 1), aPrint(iCtr + 3)), aPrint(iCtr + 4)))
                    End If
                End If
            End If
            iCtr = iCtr + 12
        End While

    End Sub
    Public Sub printImage(ByVal cImage As Image, ByVal nRow1 As Single, ByVal nCol1 As Single, ByVal nRow2 As Single, ByVal nCol2 As Single)
        aPrint.Add(cImage)
        aPrint.Add(nRow1)
        aPrint.Add(nCol1)
        aPrint.Add(nRow2)
        aPrint.Add(nCol2)
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("IMAGE")
    End Sub

    Public Sub printText(ByVal cString As String, ByVal nRow1 As Single, ByVal nCol1 As Single, ByVal nRow2 As Single, ByVal nCol2 As Single, ByVal pPen As Color, ByVal fFont As Font, ByVal TextAlgin As ContentAlignment, ByVal lNoWrap As Boolean)
        aPrint.Add(cString)
        aPrint.Add(nRow1)
        aPrint.Add(nCol1)
        aPrint.Add(nRow2)
        aPrint.Add(nCol2)
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add(pPen)
        aPrint.Add(fFont)
        aPrint.Add(TextAlgin)
        aPrint.Add(lNoWrap)
        aPrint.Add("TEXT")
    End Sub
    Public Sub LineDraw(ByVal nRow1 As Single, ByVal nCol1 As Single, ByVal nRow2 As Single, ByVal nCol2 As Single, ByVal pPen As Pen)
        aPrint.Add(nRow1)
        aPrint.Add(nCol1)
        aPrint.Add(nRow2)
        aPrint.Add(nCol2)
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add(pPen)
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("LINE")
    End Sub
    Public Sub RectangleDraw(ByVal nRow1 As Single, ByVal nCol1 As Single, ByVal nRow2 As Single, ByVal nCol2 As Single, ByVal nCurve As Single, ByVal pPen As Color, ByVal LineThicknes As Single, ByVal dsDashStyle As DashStyle)
        aPrint.Add(nRow1)
        aPrint.Add(nCol1)
        aPrint.Add(nRow2)
        aPrint.Add(nCol2)
        aPrint.Add(LineThicknes)
        aPrint.Add(dsDashStyle)
        aPrint.Add(nCurve)
        aPrint.Add(pPen)
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("RECTANGLE")
    End Sub
    'Public Sub Center(ByVal cString As String, ByVal nRow As Double, ByVal lBold As Boolean, ByVal nFontSize As Double)
    '    aPrint.Add(cString)
    '    aPrint.Add(nRow)
    '    aPrint.Add("")
    '    aPrint.Add(lBold)
    '    aPrint.Add(nFontSize)
    '    aPrint.Add("CENTER")
    '    aPrint.Add("")
    '    aPrint.Add("")
    'End Sub
    Public Sub PageEject()
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("")
        aPrint.Add("EJECT")
    End Sub
    Public Sub Images(ByVal cImage As String, Optional ByVal X As Single = 60, Optional ByVal Y As Single = 10, Optional ByVal Width As Single = 700, Optional ByVal Height As Single = 150)
        ImageFile = cImage
        nX = X
        nY = Y
        nWdth = Width
        nHght = Height
    End Sub
    Sub DoPrint(Optional ByVal nSize As Integer = 1)
        Try

            iCtr = 0
            AddHandler PrintDocument1.PrintPage, AddressOf Me.PrintDocument1_PrintPage

            PrintPreviewDialog1.Document = PrintDocument1
            If nSize = 1 Then
                PrintPreviewDialog1.Document.DefaultPageSettings.Landscape = False
            ElseIf nSize = 2 Then
                PrintPreviewDialog1.Document.DefaultPageSettings.Landscape = True
            End If
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "eZee Message", Windows.Forms.MessageBoxButtons.OK)
        End Try
    End Sub
    Public Sub SetAlign(ByVal Align As ContentAlignment, ByVal lCanDraw As Boolean)
        Select Case Align
            Case ContentAlignment.BottomLeft
                string_format.Alignment = StringAlignment.Near
                string_format.LineAlignment = StringAlignment.Far
            Case ContentAlignment.MiddleLeft
                string_format.Alignment = StringAlignment.Near
                string_format.LineAlignment = StringAlignment.Center
            Case ContentAlignment.TopRight
                string_format.Alignment = StringAlignment.Near
                string_format.LineAlignment = StringAlignment.Near
            Case ContentAlignment.BottomCenter
                string_format.Alignment = StringAlignment.Center
                string_format.LineAlignment = StringAlignment.Far
            Case ContentAlignment.MiddleCenter
                string_format.Alignment = StringAlignment.Center
                string_format.LineAlignment = StringAlignment.Center
            Case ContentAlignment.TopCenter
                string_format.Alignment = StringAlignment.Center
                string_format.LineAlignment = StringAlignment.Near
            Case ContentAlignment.BottomRight
                string_format.Alignment = StringAlignment.Far
                string_format.LineAlignment = StringAlignment.Far
            Case ContentAlignment.MiddleRight
                string_format.Alignment = StringAlignment.Far
                string_format.LineAlignment = StringAlignment.Center
            Case ContentAlignment.TopRight
                string_format.Alignment = StringAlignment.Far
                string_format.LineAlignment = StringAlignment.Near
        End Select
        If Not lCanDraw Then
            string_format.FormatFlags = StringFormatFlags.NoWrap
            ''Else
            ''    string_format.FormatFlags = StringFormatFlags.
        End If
    End Sub
    Public Shared Function ConvertImageToByteArray(ByVal imageToConvert As Image) 'As Byte()
        Using ms = New MemoryStream()
            Dim format As ImageFormat
            format = ImageFormat.Jpeg

            imageToConvert.Save(ms, format)
            Return ms
        End Using
    End Function
End Class
