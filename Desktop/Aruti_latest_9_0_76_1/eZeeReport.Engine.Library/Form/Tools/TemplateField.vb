Imports System.Windows.Forms
Imports System.Drawing
Imports System.ComponentModel

Public Class TemplateField
    Inherits ResizingControl

    Implements ITemplatePropertyClass

    Private m_publicproperties As New TemplatePropertyList()
    Public Property PublicProperties() As TemplatePropertyList Implements ITemplatePropertyClass.PublicProperties
        Get
            Return m_publicproperties
        End Get
        Set(ByVal value As TemplatePropertyList)
            m_publicproperties = value
        End Set
    End Property

    Private mstrFieldName As String = ""
    Public Property _FieldName() As String
        Get
            Return mstrFieldName
        End Get
        Set(ByVal value As String)
            mstrFieldName = value
        End Set
    End Property

    Private intFieldUnkID As Integer = -1
    Public Property _FieldUnkID() As Integer
        Get
            Return intFieldUnkID
        End Get
        Set(ByVal value As Integer)
            intFieldUnkID = value
        End Set
    End Property

    Private intFieldType As Integer = 0
    Public Property _FieldType() As Integer
        Get
            Return intFieldType
        End Get
        Set(ByVal value As Integer)
            intFieldType = value
        End Set
    End Property

    Private strValue As String = ""

    <Editor("System.ComponentModel.Design.MultilineStringEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", GetType(Drawing.Design.UITypeEditor))> _
    Public Property _Value() As String
        Get
            Return strValue
        End Get
        Set(ByVal value As String)
            strValue = value
            Me.Text = value
        End Set
    End Property

#Region "Base Property "
    <Browsable(True)> _
    Public Shadows Property Height() As Integer
        Get
            Return MyBase.Height
        End Get
        Set(ByVal value As Integer)
            MyBase.Height = value
        End Set
    End Property

    <Browsable(True)> _
   Public Shadows Property Width() As Integer
        Get
            Return MyBase.Width
        End Get
        Set(ByVal value As Integer)
            MyBase.Width = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Top() As Integer
        Get
            Return MyBase.Top
        End Get
        Set(ByVal value As Integer)
            MyBase.Top = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Left() As Integer
        Get
            Return MyBase.Left
        End Get
        Set(ByVal value As Integer)
            MyBase.Left = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Font() As System.Drawing.Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As System.Drawing.Font)
            MyBase.Font = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property TextAlign() As ContentAlignment
        Get
            Return MyBase.TextAlign
        End Get
        Set(ByVal value As ContentAlignment)
            MyBase.TextAlign = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property ForeColor() As Color
        Get
            Return MyBase.ForeColor
        End Get
        Set(ByVal value As Color)
            MyBase.ForeColor = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As Color)
            MyBase.BackColor = value
        End Set
    End Property
#End Region

End Class