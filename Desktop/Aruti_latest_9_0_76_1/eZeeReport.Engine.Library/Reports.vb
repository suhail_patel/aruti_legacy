Public Class Reports
    Private mstrModuleName As String = "Reports"

    Private mintTemplate As Integer = 0
    Public Property _TemplateID() As Integer
        Get
            Return mintTemplate
        End Get
        Set(ByVal value As Integer)
            mintTemplate = value
        End Set
    End Property

    Private mstrXMLDataPath As String = ""
    Public Property _XMLDataPath() As String
        Get
            Return mstrXMLDataPath
        End Get
        Set(ByVal value As String)
            mstrXMLDataPath = value
        End Set
    End Property

    Private mstrErroMessage As String = ""
    Public ReadOnly Property _ErrorMessage() As String
        Get
            Return mstrErroMessage
        End Get
    End Property

    Public Function PrintReport(Optional ByVal oenPrintOption As enPrintOption = enPrintOption.Preview, Optional ByVal oenExportOption As enExportOption = enExportOption.HTML) As Boolean
        Try
            If mintTemplate <= 0 Then
                Throw New Exception("Please select template first.")
            End If

            If mstrXMLDataPath = "" Then
                Throw New Exception("Data Source file not found." & mstrXMLDataPath)
            End If

            Dim frmFolioPrint As New Form1Des
            frmFolioPrint.ShowMeNow(mintTemplate, mstrXMLDataPath)
            frmFolioPrint.Dispose()
       
            Return True
        Catch ex As Exception
            mstrErroMessage = ex.Message & vbCrLf & " <PrintReport> - [" & mstrModuleName & "]"
            Return False
        End Try
    End Function
End Class
