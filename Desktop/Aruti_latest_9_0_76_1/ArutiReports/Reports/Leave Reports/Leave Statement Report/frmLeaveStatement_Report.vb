Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmLeaveStatement_Report

    Private mstrModuleName As String = "frmLeaveStatement_Report"
    Private objLeaveStatement As clsLeaveStatement_Report

    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mintLeaveBalanceSetting As Integer = -1
    'Pinkal (06-Feb-2013) -- End

#Region "Constructor"

    Public Sub New()
        objLeaveStatement = New clsLeaveStatement_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLeaveStatement.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployee.Select()
                Return False

            ElseIf CInt(cboLeave.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboLeave.Select()
                Return False

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmployee.GetList("Employee", False, True)
            'Else
            '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
                                         "Employee", _
                                         ConfigParameter._Object._ShowFirstAppointmentDate)
            'S.SANDEEP [04 JUN 2015] -- END

            Dim dRow As DataRow = dsList.Tables(0).NewRow
            dRow("employeeunkid") = 0
            dRow("name") = Language.getMessage(mstrModuleName, 3, "Select")
            dsList.Tables(0).Rows.InsertAt(dRow, 0)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmployee = Nothing

            Dim objLeave As New clsleavetype_master

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'dsList = objLeave.getListForCombo("Leave", True, 1)
            dsList = objLeave.getListForCombo("Leave", True, 1, "", "ISNULL(deductfromlvtypeunkid,0) <=0")
            'Pinkal (03-May-2019) -- End
            With cboLeave
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLeave = Nothing

      

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpAsonDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedIndex = 0
            cboLeave.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLeaveStatement.SetDefaultValue()
            objLeaveStatement._AsOnDate = dtpAsonDate.Value.Date
            objLeaveStatement._EmployeeId = cboEmployee.SelectedValue
            objLeaveStatement._EmployeeName = cboEmployee.Text
            Dim drRow As DataRowView = CType(cboEmployee.SelectedItem, DataRowView)
            If drRow IsNot Nothing Then
                objLeaveStatement._EmployeeCode = drRow("employeecode").ToString()
                objLeaveStatement._JobId = drRow("jobunkid").ToString()
                objLeaveStatement._Job = drRow("job_name").ToString()
                objLeaveStatement._DepartmentId = drRow("departmentunkid").ToString()
                objLeaveStatement._Department = drRow("DeptName").ToString()
            End If
            objLeaveStatement._LeaveId = cboLeave.SelectedValue
            objLeaveStatement._LeaveName = cboLeave.Text

            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objLeaveStatement._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            'Pinkal (06-Feb-2013) -- End


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            objLeaveStatement._YearId = FinancialYear._Object._YearUnkid
            'Pinkal (24-May-2014) -- End



            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
            objLeaveStatement._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
            objLeaveStatement._DBEnddate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (25-Jul-2015) -- End


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objLeaveStatement._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objLeaveStatement._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End

            'S.SANDEEP |11-APR-2019| -- START
            objLeaveStatement._ShowAdjustmentRemark = chkShowAdjustmentRemark.Checked
            'S.SANDEEP |11-APR-2019| -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmLeaveStatement_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLeaveStatement_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objLeaveStatement._ReportName
            Me._Message = objLeaveStatement._ReportDesc
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveStatement_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmLeaveStatement_Report_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveStatement_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmLeaveStatement_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objLeaveStatement.generateReport(0, e.Type, enExportAction.None)

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                'objLeaveStatement.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                         , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                                         , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                '                                                         , 0, e.Type, enExportAction.None, -1)
                objLeaveStatement.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    dtpAsonDate.Value.Date, _
                                                    dtpAsonDate.Value.Date, _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._ExportReportPath, _
                                                    ConfigParameter._Object._OpenAfterExport, _
                                                    0, e.Type, enExportAction.None, -1)
                'Shani(11-Feb-2016) -- End
                'Pinkal (24-Aug-2015) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveStatement_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objLeaveStatement.generateReport(0, e.Type, enExportAction.None)

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                'objLeaveStatement.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                                        , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                '                                                        , 0, enExportAction.None, e.Type, -1)
                objLeaveStatement.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    dtpAsonDate.Value.Date, _
                                                    dtpAsonDate.Value.Date, _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._ExportReportPath, _
                                                    ConfigParameter._Object._OpenAfterExport, _
                                                    0, enExportAction.None, e.Type, -1)
                'Shani(11-Feb-2016) -- End
                'Pinkal (24-Aug-2015) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveStatement_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveStatement_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveStatement_Report_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLeaveStatement_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLeaveStatement_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsLeaveStatement_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLeaveStatement_Report_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboLeave.DataSource
            frm.ValueMember = cboLeave.ValueMember
            frm.DisplayMember = cboLeave.DisplayMember
            If frm.DisplayDialog Then
                cboLeave.SelectedValue = frm.SelectedValue
                cboLeave.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.Name, Me.lblLeaveName.Text)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
			Me.LblAsOnDate.Text = Language._Object.getCaption(Me.LblAsOnDate.Name, Me.LblAsOnDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
			Language.setMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
