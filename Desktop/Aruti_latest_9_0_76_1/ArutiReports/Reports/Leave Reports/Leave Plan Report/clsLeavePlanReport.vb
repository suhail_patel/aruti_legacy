'************************************************************************************************************************************
'Class Name : clsLeavePlanReport.vb
'Purpose    :
'Date       : 23/11/2013
'Written By :
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsLeavePlanReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeavePlanReport"
    Private mstrReportId As String = enArutiReport.LeavePlanReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtLeaveFromDate As DateTime = Nothing
    Private mdtLeaveToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployee As String = ""
    Private mintLeaveId As Integer = 0
    Private mstrLeave As String = ""
    Private mintJobId As Integer = 0
    Private mstrJob As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False

    'Pinkal (01-Feb-2014) -- End

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
    Private mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year
    'Pinkal (24-Aug-2015) -- End

    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Pinkal (06-Jan-2016) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtLeaveFromDate = value
        End Set
    End Property

    Public WriteOnly Property _LeaveToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtLeaveToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _Employee() As String
        Set(ByVal value As String)
            mstrEmployee = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _Leave() As String
        Set(ByVal value As String)
            mstrLeave = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property



    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'Pinkal (01-Feb-2014) -- End


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property
    'Pinkal (24-Aug-2015) -- End


    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    'Pinkal (06-Jan-2016) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtLeaveFromDate = Nothing
            mdtLeaveToDate = Nothing
            mintEmployeeId = 0
            mintJobId = 0
            mintLeaveId = 0
            mstrEmployee = ""
            mstrLeave = ""
            mstrJob = ""
            mstrOrderByQuery = ""
            mstrAdvance_Filter = ""

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            mblnIncludeAccessFilterQry = True
            'Pinkal (06-Jan-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            Me._FilterQuery &= " AND lvleaveplanner.startdate BETWEEN @LeaveFromDate  AND @LeaveToDate "
            objDataOperation.AddParameter("@LeaveFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLeaveFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Leave From Date: ") & " " & mdtLeaveFromDate.ToShortDateString & " "

            Me._FilterQuery &= " AND  lvleaveplanner.stopdate <= @LeaveToDate "
            objDataOperation.AddParameter("@LeaveToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLeaveToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Leave To Date: ") & " " & mdtLeaveToDate.ToShortDateString & " "


            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND lvleaveplanner.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Employee : ") & " " & mstrEmployee & " "
            End If

            If mintJobId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Job : ") & " " & mstrJob & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterQuery &= " AND lvleaveplanner.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Leave Name: ") & " " & mstrLeave & " "
            End If

            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If


            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Pinkal (01-Feb-2014) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    'Pinkal (01-Feb-2014) -- End

        '    objRpt = Generate_DetailReport()

        '    'Pinkal (01-Feb-2014) -- Start
        '    'Enhancement : TRA Changes
        '    Rpt = objRpt
        '    'Pinkal (01-Feb-2014) -- End


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                              , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                              , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                              , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xOnlyApproved, xUserModeSetting)

            Rpt = objRpt
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 1, "Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hrjob_master.job_name,'')", Language.getMessage(mstrModuleName, 3, "Job")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(CHAR(8), appointeddate, 112)", Language.getMessage(mstrModuleName, 4, "Appointment Date")))
            iColumn_DetailReport.Add(New IColumn("lvleavetype_master.leavename", Language.getMessage(mstrModuleName, 5, "Leave")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(CHAR(8), lvleaveplanner.startdate, 112)", Language.getMessage(mstrModuleName, 6, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(CHAR(8), lvleaveplanner.stopdate, 112)", Language.getMessage(mstrModuleName, 7, "End Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Male"))
    '        objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Female"))

    '        StrQ = "SELECT  hremployee_master.employeeunkid AS EmpId  " & _
    '                  ", hremployee_master.employeecode AS EmpCode  " & _
    '                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  AS EmpName  " & _
    '                  ", ISNULL(hrjob_master.job_name,'') AS Job  " & _
    '                  ", CONVERT(CHAR(8), appointeddate, 112) AS AppointmentDate  " & _
    '                  ", CASE WHEN hremployee_master.gender = 1 THEN @Male WHEN hremployee_master.gender = 2 THEN @Female ELSE '' END AS Gender  " & _
    '                  ", lvleavetype_master.leavename AS LeaveName  " & _
    '                  ", ISNULL(( SELECT CAST (lvleavebalance_tran.accrue_amount AS DECIMAL(10,2)) FROM   lvleavebalance_tran  WHERE  lvleavebalance_tran.employeeunkid = lvleaveplanner.employeeunkid " & _
    '                  " AND lvleavebalance_tran.leavetypeunkid = lvleaveplanner.leavetypeunkid AND lvleavebalance_tran.isvoid = 0), 0) AS AccrueAmt  " & _
    '                  ", CONVERT(CHAR(8), lvleaveplanner.startdate, 112) AS StartDate  " & _
    '                  ", CONVERT(CHAR(8), lvleaveplanner.stopdate, 112) AS StopDate  " & _
    '                  ", DATEDIFF(DAY, lvleaveplanner.startdate, lvleaveplanner.stopdate) + 1 AS Days  " & _
    '                  ", ISNULL(remarks, '') AS remarks "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= " FROM  lvleaveplanner " & _
    '                  " JOIN lvleavetype_master ON lvleaveplanner.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
    '                  " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveplanner.employeeunkid " & _
    '                  " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE lvleaveplanner.isvoid = 0 "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes

    '        If mblnIncludeInactiveEmp <> ConfigParameter._Object._IsIncludeInactiveEmp Then
    '            mblnIncludeInactiveEmp = ConfigParameter._Object._IsIncludeInactiveEmp
    '        End If

    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If


    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        If mstrUserAccessFilter.Trim.Length <= 0 Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            StrQ &= mstrUserAccessFilter
    '        End If

    '        'Pinkal (01-Feb-2014) -- End


    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("EmpCode")
    '            rpt_Row.Item("Column2") = dtRow.Item("EmpName")
    '            rpt_Row.Item("Column3") = dtRow.Item("Job")
    '            rpt_Row.Item("Column4") = dtRow.Item("Leavename")
    '            rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("AppointmentDate").ToString).ToShortDateString
    '            rpt_Row.Item("Column6") = dtRow.Item("Gender")
    '            rpt_Row.Item("Column7") = dtRow.Item("AccrueAmt")
    '            rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString
    '            rpt_Row.Item("Column9") = eZeeDate.convertDate(dtRow.Item("StopDate").ToString).ToShortDateString
    '            rpt_Row.Item("Column10") = dtRow.Item("Days")
    '            rpt_Row.Item("Column11") = dtRow.Item("remarks")
    '            rpt_Row.Item("Column12") = dtRow.Item("GName")

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptLeavePlanReport

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
    '        End If


    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 26, "Code : "))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpname", Language.getMessage(mstrModuleName, 27, "Employee : "))
    '        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 28, "Job : "))
    '        Call ReportFunction.TextChange(objRpt, "txtAppointmentdate", Language.getMessage(mstrModuleName, 29, "Appointment Date : "))
    '        Call ReportFunction.TextChange(objRpt, "txtLeavename", Language.getMessage(mstrModuleName, 5, "Leave"))
    '        Call ReportFunction.TextChange(objRpt, "txtStartdate", Language.getMessage(mstrModuleName, 6, "Start Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 7, "End Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 14, "Gender : "))
    '        Call ReportFunction.TextChange(objRpt, "txtAcrrueAmount", Language.getMessage(mstrModuleName, 15, "Accrue Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 16, "Days"))
    '        Call ReportFunction.TextChange(objRpt, "txtRemarks", Language.getMessage(mstrModuleName, 17, "Remarks"))

    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Female"))

            StrQ = "SELECT  hremployee_master.employeeunkid AS EmpId  " & _
                      ", hremployee_master.employeecode AS EmpCode  " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  AS EmpName  " & _
                      ", ISNULL(jb.job_name,'') AS Job  " & _
                      ", CONVERT(CHAR(8), appointeddate, 112) AS AppointmentDate  " & _
                      ", CASE WHEN hremployee_master.gender = 1 THEN @Male WHEN hremployee_master.gender = 2 THEN @Female ELSE '' END AS Gender  " & _
                      ", lvleavetype_master.leavename AS LeaveName  "


            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                StrQ &= ", ISNULL(( SELECT CAST (lvleavebalance_tran.accrue_amount AS DECIMAL(10,2)) FROM   lvleavebalance_tran  WHERE  lvleavebalance_tran.employeeunkid = lvleaveplanner.employeeunkid " & _
                          " AND lvleavebalance_tran.leavetypeunkid = lvleaveplanner.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 AND isclose_fy = 0), 0) AS AccrueAmt  "
            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= ", ISNULL(( SELECT CAST (lvleavebalance_tran.accrue_amount AS DECIMAL(10,2)) FROM   lvleavebalance_tran  WHERE  lvleavebalance_tran.employeeunkid = lvleaveplanner.employeeunkid " & _
                            " AND lvleavebalance_tran.leavetypeunkid = lvleaveplanner.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 AND iselc = 1 AND isopenelc = 1), 0) AS AccrueAmt  "
            End If


            StrQ &= ", CONVERT(CHAR(8), lvleaveplanner.startdate, 112) AS StartDate  " & _
                      ", CONVERT(CHAR(8), lvleaveplanner.stopdate, 112) AS StopDate  " & _
                         ", Fraction.DayFraction AS Days " & _
                      ", ISNULL(remarks, '') AS remarks "

            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on CCK Leave Planner Changes & Budget Timesheet changes.
            '", DATEDIFF(DAY, lvleaveplanner.startdate, lvleaveplanner.stopdate) + 1 AS Days  " & _
            'Pinkal (12-Oct-2017) -- End


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on CCK Leave Planner Changes & Budget Timesheet changes.
            StrQ &= " FROM  lvleaveplanner " & _
                         " JOIN (SELECT " & _
                         "               leaveplannerunkid " & _
                         "              ,ISNULL(SUM(lvplannerday_fraction.dayfraction),0.0) AS DayFraction " & _
                         "          FROM lvplannerday_fraction " & _
                         "          WHERE isvoid = 0 GROUP BY leaveplannerunkid " & _
                         "         ) AS Fraction ON Fraction.leaveplannerunkid = lvleaveplanner.leaveplannerunkid " & _
                      " JOIN lvleavetype_master ON lvleaveplanner.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveplanner.employeeunkid " & _
                      " LEFT JOIN " & _
                         " ( " & _
                      "    SELECT " & _
                      "         jobunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN hrjob_master jb ON jb.jobunkid = Jobs.jobunkid "

            'Pinkal (12-Oct-2017) -- End


            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE lvleaveplanner.isvoid = 0 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            'If mblnIncludeInactiveEmp <> ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    mblnIncludeInactiveEmp = ConfigParameter._Object._IsIncludeInactiveEmp
            'End If

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EmpCode")
                rpt_Row.Item("Column2") = dtRow.Item("EmpName")
                rpt_Row.Item("Column3") = dtRow.Item("Job")
                rpt_Row.Item("Column4") = dtRow.Item("Leavename")
                rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("AppointmentDate").ToString).ToShortDateString
                rpt_Row.Item("Column6") = dtRow.Item("Gender")
                rpt_Row.Item("Column7") = dtRow.Item("AccrueAmt")
                rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString
                rpt_Row.Item("Column9") = eZeeDate.convertDate(dtRow.Item("StopDate").ToString).ToShortDateString
                rpt_Row.Item("Column10") = dtRow.Item("Days")
                rpt_Row.Item("Column11") = dtRow.Item("remarks")
                rpt_Row.Item("Column12") = dtRow.Item("GName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptLeavePlanReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 26, "Code : "))
            Call ReportFunction.TextChange(objRpt, "txtEmpname", Language.getMessage(mstrModuleName, 27, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 28, "Job : "))
            Call ReportFunction.TextChange(objRpt, "txtAppointmentdate", Language.getMessage(mstrModuleName, 29, "Appointment Date : "))
            Call ReportFunction.TextChange(objRpt, "txtLeavename", Language.getMessage(mstrModuleName, 5, "Leave"))
            Call ReportFunction.TextChange(objRpt, "txtStartdate", Language.getMessage(mstrModuleName, 6, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 7, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 14, "Gender : "))
            Call ReportFunction.TextChange(objRpt, "txtAcrrueAmount", Language.getMessage(mstrModuleName, 15, "Accrue Amount"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 16, "Days"))
            Call ReportFunction.TextChange(objRpt, "txtRemarks", Language.getMessage(mstrModuleName, 17, "Remarks"))

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Job")
            Language.setMessage(mstrModuleName, 4, "Appointment Date")
            Language.setMessage(mstrModuleName, 5, "Leave")
            Language.setMessage(mstrModuleName, 6, "Start Date")
            Language.setMessage(mstrModuleName, 7, "End Date")
            Language.setMessage(mstrModuleName, 8, "Male")
            Language.setMessage(mstrModuleName, 9, "Female")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By :")
            Language.setMessage(mstrModuleName, 13, "Received By :")
            Language.setMessage(mstrModuleName, 14, "Gender :")
            Language.setMessage(mstrModuleName, 15, "Accrue Amount")
            Language.setMessage(mstrModuleName, 16, "Days")
            Language.setMessage(mstrModuleName, 17, "Remarks")
            Language.setMessage(mstrModuleName, 18, "Printed By :")
            Language.setMessage(mstrModuleName, 19, "Printed Date :")
            Language.setMessage(mstrModuleName, 20, "Leave From Date:")
            Language.setMessage(mstrModuleName, 21, "Leave To Date:")
            Language.setMessage(mstrModuleName, 22, "Employee :")
            Language.setMessage(mstrModuleName, 23, "Job :")
            Language.setMessage(mstrModuleName, 24, "Leave Name:")
            Language.setMessage(mstrModuleName, 25, " Order By :")
            Language.setMessage(mstrModuleName, 26, "Code :")
            Language.setMessage(mstrModuleName, 27, "Employee :")
            Language.setMessage(mstrModuleName, 28, "Job :")
            Language.setMessage(mstrModuleName, 29, "Appointment Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
