'************************************************************************************************************************************
'Class Name : clsLeaveIssue_AT_Report.vb
'Purpose    :
'Date       : 24/02/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsLeaveIssue_AT_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveIssue_AT_Report"
    Private mstrReportId As String = enArutiReport.LeaveIssue_AT_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mdtAuditFromDate As DateTime = Nothing
    Private mdtAuditToDate As DateTime = Nothing
    Private mdtLeaveFromDate As DateTime = Nothing
    Private mdtLeaveToDate As DateTime = Nothing

    Private mintUserId As Integer = 0
    Private mintLeaveId As Integer = 0
    Private mintAuditTypeId As Integer = 0

    Private mstrUserName As String = ""
    Private mstrLeaveName As String = ""
    Private mstrAuditType As String = ""


    Private mstrOrderByQuery As String = ""


    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIncludeInactiveEmp As Boolean = False
    'Pinkal (11-MAY-2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AudtiFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditFromDate = value
        End Set
    End Property

    Public WriteOnly Property _AuditToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditToDate = value
        End Set
    End Property

    Public WriteOnly Property _LeaveFromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtLeaveFromDate = value
        End Set
    End Property

    Public WriteOnly Property _LeaveToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtLeaveToDate = value
        End Set
    End Property

    Public WriteOnly Property _UserId() As Integer
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeId() As Integer
        Set(ByVal value As Integer)
            mintAuditTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _UName() As String
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public WriteOnly Property _AudtiType() As String
        Set(ByVal value As String)
            mstrAuditType = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property


    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'Pinkal (11-MAY-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""

            mdtAuditFromDate = Nothing
            mdtAuditToDate = Nothing
            mdtLeaveFromDate = Nothing
            mdtLeaveToDate = Nothing

            mintUserId = 0
            mintAuditTypeId = 0
            mintLeaveId = 0

            mstrUserName = ""
            mstrLeaveName = ""
            mstrAuditType = ""

            mstrOrderByQuery = ""


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            mblnIncludeInactiveEmp = False
            'Pinkal (11-MAY-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@AuditFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Audit From Date: ") & " " & mdtAuditFromDate.ToShortDateString & " "

            objDataOperation.AddParameter("@AuditToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, " Audit To Date: ") & " " & mdtAuditToDate.ToShortDateString & " "

            If mdtLeaveFromDate <> Nothing And mdtLeaveToDate <> Nothing Then

                Me._FilterQuery &= " AND atlvleaveIssue_tran.leavedate >= @LeaveFromDate "
                objDataOperation.AddParameter("@LeaveFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLeaveFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Leave From Date: ") & " " & mdtLeaveFromDate.ToShortDateString & " "

                Me._FilterQuery &= " AND atlvleaveIssue_tran.leavedate <= @LeaveToDate "
                objDataOperation.AddParameter("@LeaveToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLeaveToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Leave To Date: ") & " " & mdtLeaveToDate.ToShortDateString & " "
            End If

            If mintUserId > 0 Then
                Me._FilterQuery &= " AND atlvleaveIssue_tran.audituserunkid = @UserId "
                objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "User Name: ") & " " & mstrUserName & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterQuery &= " AND lvleaveIssue_master.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Leave Name: ") & " " & mstrLeaveName & " "
            End If

            If mintAuditTypeId > 0 Then
                Me._FilterQuery &= " AND atlvleaveIssue_tran.audittype = @AuditTypeId "
                objDataOperation.AddParameter("@AuditTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, " Audit Type : ") & " " & mstrAuditType & " "
            End If


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            'Pinkal (11-MAY-2012) -- End


            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'objRpt = Generate_DetailReport()
            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try

    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("auditdate", Language.getMessage(mstrModuleName, 1, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("Type", Language.getMessage(mstrModuleName, 32, "Audit Type")))
            iColumn_DetailReport.Add(New IColumn("LeaveName", Language.getMessage(mstrModuleName, 3, "Leave Name")))
            iColumn_DetailReport.Add(New IColumn("Leavedate", Language.getMessage(mstrModuleName, 30, "Leave Date")))
            iColumn_DetailReport.Add(New IColumn("UserName", Language.getMessage(mstrModuleName, 31, "User Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        objDataOperation.ClearParameters()

    '        objDataOperation.AddParameter("@add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Add"))
    '        objDataOperation.AddParameter("@edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Edit"))
    '        objDataOperation.AddParameter("@delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Delete"))
    '        objDataOperation.AddParameter("@paid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Paid Leave"))
    '        objDataOperation.AddParameter("@unpaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Unpaid Leave"))


    '        StrQ = " SELECT hremployee_master.employeecode,hremployee_master.firstname + ' ' + hremployee_master.surname Employee," & _
    '                   " LeaveName,CONVERT(VARCHAR(20),Leavedate,112) Leavedate," & _
    '                   " CASE WHEN audittype = 1 THEN @add  " & _
    '                   " WHEN audittype = 2 THEN @edit	 " & _
    '                   " WHEN audittype = 3 THEN @delete END  'Type', " & _
    '                   " ISNULL(hrmsConfiguration..cfuser_master.username,'') UserName, " & _
    '                   " CONVERT(CHAR(8),auditdatetime,112) auditdate , " & _
    '                   " CONVERT(CHAR(8),auditdatetime,108) audittime,ip,machine_name, " & _
    '                   " CASE WHEN atlvleaveIssue_tran.ispaid = 1 THEN @paid ELSE @unpaid END Leavetype " & _
    '                   " FROM atlvleaveIssue_tran " & _
    '                   " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = atlvleaveIssue_tran.leaveissueunkid " & _
    '                   " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
    '                   " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
    '                   " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlvleaveIssue_tran.audituserunkid " & _
    '                   " WHERE CONVERT(CHAR(8),atlvleaveIssue_tran.auditdatetime,112) >= @AuditFromDate AND " & _
    '                   " CONVERT(CHAR(8),atlvleaveIssue_tran.auditdatetime,112) <= @AuditToDate "



    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End


    '        'Pinkal (11-MAY-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        ''Anjan (24 Jun 2011)-Start
    '        ''Issue : According to privilege that lower level user should not see superior level employees.
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If
    '        ''Anjan (24 Jun 2011)-End 

    '        'Pinkal (11-MAY-2012) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("LeaveName")
    '            rpt_Row.Item("Column2") = eZeeDate.convertDate(dtRow.Item("Leavedate").ToString).ToShortDateString
    '            rpt_Row.Item("Column3") = dtRow.Item("Type")
    '            rpt_Row.Item("Column4") = dtRow.Item("UserName")
    '            rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("auditdate").ToString).ToShortDateString
    '            rpt_Row.Item("Column6") = dtRow.Item("audittime")
    '            rpt_Row.Item("Column7") = dtRow.Item("ip")
    '            rpt_Row.Item("Column8") = dtRow.Item("machine_name")
    '            rpt_Row.Item("Column9") = dtRow.Item("Leavetype")
    '            rpt_Row.Item("Column10") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column11") = dtRow.Item("Employee")

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptLeaveIssue_AT_Report

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 25, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 26, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 27, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 28, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtAuditdate", Language.getMessage(mstrModuleName, 1, "Audit Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtAudittype", Language.getMessage(mstrModuleName, 32, "Audit Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeavename", Language.getMessage(mstrModuleName, 3, "Leave Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeavedate", Language.getMessage(mstrModuleName, 30, "Leave Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtUsername", Language.getMessage(mstrModuleName, 31, "User Name"))

    '        Call ReportFunction.TextChange(objRpt, "txtAudittime", Language.getMessage(mstrModuleName, 11, "Audit Time"))
    '        Call ReportFunction.TextChange(objRpt, "txtIp", Language.getMessage(mstrModuleName, 12, "Machine IP"))
    '        Call ReportFunction.TextChange(objRpt, "txtMachinename", Language.getMessage(mstrModuleName, 13, "Machine "))
    '        Call ReportFunction.TextChange(objRpt, "txtLeavetype", Language.getMessage(mstrModuleName, 14, "Leave Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpcode", Language.getMessage(mstrModuleName, 23, "Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpname", Language.getMessage(mstrModuleName, 24, "Employee"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))


    '        'Pinkal (11-MAY-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
    '        'Pinkal (11-MAY-2012) -- End



    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function


    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                              , ByVal mdtEmployeeAsOnDate As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)
            'S.SANDEEP [15 NOV 2016] -- START
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsOnDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'S.SANDEEP [15 NOV 2016] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Add"))
            objDataOperation.AddParameter("@edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Edit"))
            objDataOperation.AddParameter("@delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Delete"))
            objDataOperation.AddParameter("@paid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Paid Leave"))
            objDataOperation.AddParameter("@unpaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Unpaid Leave"))


            StrQ = " SELECT hremployee_master.employeecode,hremployee_master.firstname + ' ' + hremployee_master.surname Employee," & _
                       " LeaveName,CONVERT(VARCHAR(20),Leavedate,112) Leavedate," & _
                       " CASE WHEN audittype = 1 THEN @add  " & _
                       " WHEN audittype = 2 THEN @edit	 " & _
                       " WHEN audittype = 3 THEN @delete END  'Type', " & _
                       " ISNULL(hrmsConfiguration..cfuser_master.username,'') UserName, " & _
                       " CONVERT(CHAR(8),auditdatetime,112) auditdate , " & _
                       " CONVERT(CHAR(8),auditdatetime,108) audittime,ip,machine_name, " & _
                       " CASE WHEN atlvleaveIssue_tran.ispaid = 1 THEN @paid ELSE @unpaid END Leavetype " & _
                       " FROM atlvleaveIssue_tran " & _
                       " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = atlvleaveIssue_tran.leaveissueunkid " & _
                       " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                       " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = atlvleaveIssue_tran.audituserunkid "


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= " WHERE CONVERT(CHAR(8),atlvleaveIssue_tran.auditdatetime,112) >= @AuditFromDate AND " & _
                         " CONVERT(CHAR(8),atlvleaveIssue_tran.auditdatetime,112) <= @AuditToDate "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If


            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("LeaveName")
                rpt_Row.Item("Column2") = eZeeDate.convertDate(dtRow.Item("Leavedate").ToString).ToShortDateString
                rpt_Row.Item("Column3") = dtRow.Item("Type")
                rpt_Row.Item("Column4") = dtRow.Item("UserName")
                rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("auditdate").ToString).ToShortDateString
                rpt_Row.Item("Column6") = dtRow.Item("audittime")
                rpt_Row.Item("Column7") = dtRow.Item("ip")
                rpt_Row.Item("Column8") = dtRow.Item("machine_name")
                rpt_Row.Item("Column9") = dtRow.Item("Leavetype")
                rpt_Row.Item("Column10") = dtRow.Item("employeecode")
                rpt_Row.Item("Column11") = dtRow.Item("Employee")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptLeaveIssue_AT_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 25, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 26, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 27, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 28, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtAuditdate", Language.getMessage(mstrModuleName, 1, "Audit Date"))
            Call ReportFunction.TextChange(objRpt, "txtAudittype", Language.getMessage(mstrModuleName, 32, "Audit Type"))
            Call ReportFunction.TextChange(objRpt, "txtLeavename", Language.getMessage(mstrModuleName, 3, "Leave Name"))
            Call ReportFunction.TextChange(objRpt, "txtLeavedate", Language.getMessage(mstrModuleName, 30, "Leave Date"))
            Call ReportFunction.TextChange(objRpt, "txtUsername", Language.getMessage(mstrModuleName, 31, "User Name"))

            Call ReportFunction.TextChange(objRpt, "txtAudittime", Language.getMessage(mstrModuleName, 11, "Audit Time"))
            Call ReportFunction.TextChange(objRpt, "txtIp", Language.getMessage(mstrModuleName, 12, "Machine IP"))
            Call ReportFunction.TextChange(objRpt, "txtMachinename", Language.getMessage(mstrModuleName, 13, "Machine "))
            Call ReportFunction.TextChange(objRpt, "txtLeavetype", Language.getMessage(mstrModuleName, 14, "Leave Type"))
            Call ReportFunction.TextChange(objRpt, "txtEmpcode", Language.getMessage(mstrModuleName, 23, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpname", Language.getMessage(mstrModuleName, 24, "Employee"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
            'Pinkal (11-MAY-2012) -- End



            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Audit Date")
            Language.setMessage(mstrModuleName, 2, "Leave Name:")
            Language.setMessage(mstrModuleName, 3, "Leave Name")
            Language.setMessage(mstrModuleName, 4, "User Name:")
            Language.setMessage(mstrModuleName, 6, "Add")
            Language.setMessage(mstrModuleName, 7, "Edit")
            Language.setMessage(mstrModuleName, 8, "Delete")
            Language.setMessage(mstrModuleName, 9, "Paid Leave")
            Language.setMessage(mstrModuleName, 10, "Unpaid Leave")
            Language.setMessage(mstrModuleName, 11, "Audit Time")
            Language.setMessage(mstrModuleName, 12, "Machine IP")
            Language.setMessage(mstrModuleName, 13, "Machine")
            Language.setMessage(mstrModuleName, 14, "Leave Type")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")
            Language.setMessage(mstrModuleName, 18, " Audit From Date:")
            Language.setMessage(mstrModuleName, 19, " Audit To Date:")
            Language.setMessage(mstrModuleName, 20, " Leave From Date:")
            Language.setMessage(mstrModuleName, 21, " Leave To Date:")
            Language.setMessage(mstrModuleName, 22, " Order By :")
            Language.setMessage(mstrModuleName, 23, "Code")
            Language.setMessage(mstrModuleName, 24, "Employee")
            Language.setMessage(mstrModuleName, 25, "Prepared By :")
            Language.setMessage(mstrModuleName, 26, "Checked By :")
            Language.setMessage(mstrModuleName, 27, "Approved By :")
            Language.setMessage(mstrModuleName, 28, "Received By :")
            Language.setMessage(mstrModuleName, 29, " Audit Type :")
            Language.setMessage(mstrModuleName, 30, "Leave Date")
            Language.setMessage(mstrModuleName, 31, "User Name")
            Language.setMessage(mstrModuleName, 32, "Audit Type")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
