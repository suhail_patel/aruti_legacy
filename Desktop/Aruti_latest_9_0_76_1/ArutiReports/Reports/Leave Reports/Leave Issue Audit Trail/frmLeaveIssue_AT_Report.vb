Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 2

Public Class frmLeaveIssue_AT_Report

    Private mstrModuleName As String = "frmLeaveIssue_AT_Report"
    Private objLeaveIssue As clsLeaveIssue_AT_Report

#Region "Private Variables"

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Constructor"

    Public Sub New()
        objLeaveIssue = New clsLeaveIssue_AT_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLeaveIssue.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region "Private Function"

    Public Function Validation() As Boolean
        Try
            If dtpLeaveFromDate.Checked And dtpLeaveToDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave To Date. Leave To Date is compulsory information."), enMsgBoxStyle.Information)
                dtpLeaveToDate.Focus()
                Return False
            ElseIf dtpLeaveFromDate.Checked = False And dtpLeaveToDate.Checked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Leave From Date. Leave From Date is compulsory information."), enMsgBoxStyle.Information)
                dtpLeaveFromDate.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Public Sub FillCombo()
        Try

            Dim objUser As New clsUserAddEdit
            Dim dsList As New DataSet

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsList = objUser.getComboList("User", True)

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUser.getNewComboList("User", , True)
            dsList = objUser.getNewComboList("User", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objUser = Nothing

            Dim objLeave As New clsleavetype_master
            dsList = objLeave.getListForCombo("Leave", True)
            With cboLeave
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLeave = Nothing

            Dim objAudit As New clsMasterData
            dsList = objAudit.GetAuditTypeList("Audit", True, False, True)
            With cboAuditType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objAudit = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            dtpAuditFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAuditToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

            dtpLeaveFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpLeaveFromDate.Checked = False
            dtpLeaveToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpLeaveToDate.Checked = False

            cboUser.SelectedIndex = 0
            cboLeave.SelectedIndex = 0
            cboAuditType.SelectedIndex = 0
            objLeaveIssue.setDefaultOrderBy(0)
            txtOrderBy.Text = objLeaveIssue.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLeaveIssue.SetDefaultValue()

            objLeaveIssue._UserId = cboUser.SelectedValue
            objLeaveIssue._UName = cboUser.Text

            objLeaveIssue._LeaveId = cboLeave.SelectedValue
            objLeaveIssue._LeaveName = cboLeave.Text

            objLeaveIssue._AuditTypeId = cboAuditType.SelectedIndex
            objLeaveIssue._AudtiType = cboAuditType.Text

            objLeaveIssue._AudtiFromDate = dtpAuditFromDate.Value.Date
            objLeaveIssue._AuditToDate = dtpAuditToDate.Value.Date

            If dtpLeaveFromDate.Checked Then
                objLeaveIssue._LeaveFromDate = dtpLeaveFromDate.Value.Date
            End If

            If dtpLeaveToDate.Checked Then
                objLeaveIssue._LeaveToDate = dtpLeaveToDate.Value.Date
            End If

            

            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            objLeaveIssue._IncludeInactiveEmp = chkInactiveemp.Checked
            'Pinkal (11-MAY-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objLeaveIssue._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End
            
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmLeaveIssue_AT_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLeaveIssue = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLeaveIssue_AT_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objLeaveIssue._ReportName
            Me._Message = objLeaveIssue._ReportDesc

            'dtpAuditFromDate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpAuditFromDate.MaxDate = FinancialYear._Object._Database_End_Date

            'dtpAuditToDate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpAuditToDate.MaxDate = FinancialYear._Object._Database_End_Date

            'dtpLeaveFromDate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpLeaveFromDate.MaxDate = FinancialYear._Object._Database_End_Date

            'dtpLeaveToDate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpLeaveToDate.MaxDate = FinancialYear._Object._Database_End_Date

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AT_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmLeaveIssue_AT_Report_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AT_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmLeaveIssue_AT_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                ' objLeaveIssue.generateReport(0, e.Type, enExportAction.None)
                objLeaveIssue.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                  , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                  , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
                'Pinkal (24-Aug-2015) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AT_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub
                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objLeaveIssue.generateReport(0, enPrintAction.None, e.Type)
                objLeaveIssue.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
                'Pinkal (24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AT_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveIssue_AT_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveIssue_AT_Report_Cancel_Click", mstrModuleName)
        End Try

    End Sub



    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmLeaveIssue_AT_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLeaveIssue_AT_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsLeaveIssue_AT_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLeaveIssue_AT_Report_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


    'Pinkal (06-May-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboLeave
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-May-2013) -- End



#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objLeaveIssue.setOrderBy(0)
            txtOrderBy.Text = objLeaveIssue.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAuditToDate.Text = Language._Object.getCaption(Me.lblAuditToDate.Name, Me.lblAuditToDate.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
			Me.lblAuditDate.Text = Language._Object.getCaption(Me.lblAuditDate.Name, Me.lblAuditDate.Text)
			Me.lblAuditType.Text = Language._Object.getCaption(Me.lblAuditType.Name, Me.lblAuditType.Text)
			Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.Name, Me.lblLeaveName.Text)
			Me.lblLeaveTodate.Text = Language._Object.getCaption(Me.lblLeaveTodate.Name, Me.lblLeaveTodate.Text)
			Me.lblLeaveFromDate.Text = Language._Object.getCaption(Me.lblLeaveFromDate.Name, Me.lblLeaveFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Leave To Date. Leave To Date is compulsory information.")
			Language.setMessage(mstrModuleName, 2, "Please select Leave From Date. Leave From Date is compulsory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
