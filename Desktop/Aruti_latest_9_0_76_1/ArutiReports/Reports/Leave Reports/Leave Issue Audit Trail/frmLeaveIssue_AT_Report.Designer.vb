﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveIssue_AT_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblLeaveTodate = New System.Windows.Forms.Label
        Me.dtpLeaveToDate = New System.Windows.Forms.DateTimePicker
        Me.lblLeaveFromDate = New System.Windows.Forms.Label
        Me.dtpLeaveFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblLeaveName = New System.Windows.Forms.Label
        Me.cboLeave = New System.Windows.Forms.ComboBox
        Me.lblAuditType = New System.Windows.Forms.Label
        Me.cboAuditType = New System.Windows.Forms.ComboBox
        Me.lblAuditToDate = New System.Windows.Forms.Label
        Me.dtpAuditToDate = New System.Windows.Forms.DateTimePicker
        Me.lblUserName = New System.Windows.Forms.Label
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblAuditDate = New System.Windows.Forms.Label
        Me.dtpAuditFromDate = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveTodate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLeaveToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLeaveFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveName)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditType)
        Me.gbFilterCriteria.Controls.Add(Me.cboAuditType)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAuditToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblUserName)
        Me.gbFilterCriteria.Controls.Add(Me.cboUser)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAuditFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(390, 170)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchLeaveType
        '
        Me.objbtnSearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveType.BorderSelected = False
        Me.objbtnSearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveType.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveType.Location = New System.Drawing.Point(362, 86)
        Me.objbtnSearchLeaveType.Name = "objbtnSearchLeaveType"
        Me.objbtnSearchLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveType.TabIndex = 104
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(199, 115)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(155, 18)
        Me.chkInactiveemp.TabIndex = 202
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblLeaveTodate
        '
        Me.lblLeaveTodate.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveTodate.Location = New System.Drawing.Point(210, 61)
        Me.lblLeaveTodate.Name = "lblLeaveTodate"
        Me.lblLeaveTodate.Size = New System.Drawing.Size(33, 13)
        Me.lblLeaveTodate.TabIndex = 194
        Me.lblLeaveTodate.Text = "To"
        '
        'dtpLeaveToDate
        '
        Me.dtpLeaveToDate.Checked = False
        Me.dtpLeaveToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLeaveToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLeaveToDate.Location = New System.Drawing.Point(260, 57)
        Me.dtpLeaveToDate.Name = "dtpLeaveToDate"
        Me.dtpLeaveToDate.ShowCheckBox = True
        Me.dtpLeaveToDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpLeaveToDate.TabIndex = 4
        '
        'lblLeaveFromDate
        '
        Me.lblLeaveFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveFromDate.Location = New System.Drawing.Point(8, 61)
        Me.lblLeaveFromDate.Name = "lblLeaveFromDate"
        Me.lblLeaveFromDate.Size = New System.Drawing.Size(80, 13)
        Me.lblLeaveFromDate.TabIndex = 192
        Me.lblLeaveFromDate.Text = "Leave Date"
        '
        'dtpLeaveFromDate
        '
        Me.dtpLeaveFromDate.Checked = False
        Me.dtpLeaveFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLeaveFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLeaveFromDate.Location = New System.Drawing.Point(96, 57)
        Me.dtpLeaveFromDate.Name = "dtpLeaveFromDate"
        Me.dtpLeaveFromDate.ShowCheckBox = True
        Me.dtpLeaveFromDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpLeaveFromDate.TabIndex = 3
        '
        'lblLeaveName
        '
        Me.lblLeaveName.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveName.Location = New System.Drawing.Point(8, 89)
        Me.lblLeaveName.Name = "lblLeaveName"
        Me.lblLeaveName.Size = New System.Drawing.Size(80, 13)
        Me.lblLeaveName.TabIndex = 190
        Me.lblLeaveName.Text = "Leave "
        '
        'cboLeave
        '
        Me.cboLeave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave.DropDownWidth = 120
        Me.cboLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave.FormattingEnabled = True
        Me.cboLeave.Location = New System.Drawing.Point(96, 85)
        Me.cboLeave.Name = "cboLeave"
        Me.cboLeave.Size = New System.Drawing.Size(261, 21)
        Me.cboLeave.TabIndex = 5
        '
        'lblAuditType
        '
        Me.lblAuditType.BackColor = System.Drawing.Color.Transparent
        Me.lblAuditType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditType.Location = New System.Drawing.Point(8, 143)
        Me.lblAuditType.Name = "lblAuditType"
        Me.lblAuditType.Size = New System.Drawing.Size(80, 13)
        Me.lblAuditType.TabIndex = 188
        Me.lblAuditType.Text = "Audit Type"
        '
        'cboAuditType
        '
        Me.cboAuditType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuditType.DropDownWidth = 120
        Me.cboAuditType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuditType.FormattingEnabled = True
        Me.cboAuditType.Location = New System.Drawing.Point(96, 139)
        Me.cboAuditType.Name = "cboAuditType"
        Me.cboAuditType.Size = New System.Drawing.Size(97, 21)
        Me.cboAuditType.TabIndex = 7
        '
        'lblAuditToDate
        '
        Me.lblAuditToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblAuditToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditToDate.Location = New System.Drawing.Point(210, 34)
        Me.lblAuditToDate.Name = "lblAuditToDate"
        Me.lblAuditToDate.Size = New System.Drawing.Size(33, 13)
        Me.lblAuditToDate.TabIndex = 175
        Me.lblAuditToDate.Text = "To "
        '
        'dtpAuditToDate
        '
        Me.dtpAuditToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAuditToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAuditToDate.Location = New System.Drawing.Point(260, 30)
        Me.dtpAuditToDate.Name = "dtpAuditToDate"
        Me.dtpAuditToDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpAuditToDate.TabIndex = 2
        '
        'lblUserName
        '
        Me.lblUserName.BackColor = System.Drawing.Color.Transparent
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(8, 116)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(80, 13)
        Me.lblUserName.TabIndex = 167
        Me.lblUserName.Text = "User"
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 180
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(96, 112)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(97, 21)
        Me.cboUser.TabIndex = 6
        '
        'lblAuditDate
        '
        Me.lblAuditDate.BackColor = System.Drawing.Color.Transparent
        Me.lblAuditDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditDate.Location = New System.Drawing.Point(8, 34)
        Me.lblAuditDate.Name = "lblAuditDate"
        Me.lblAuditDate.Size = New System.Drawing.Size(80, 13)
        Me.lblAuditDate.TabIndex = 151
        Me.lblAuditDate.Text = "Audit Date"
        '
        'dtpAuditFromDate
        '
        Me.dtpAuditFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAuditFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAuditFromDate.Location = New System.Drawing.Point(96, 30)
        Me.dtpAuditFromDate.Name = "dtpAuditFromDate"
        Me.dtpAuditFromDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpAuditFromDate.TabIndex = 1
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 241)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(390, 63)
        Me.gbSortBy.TabIndex = 17
        Me.gbSortBy.TabStop = True
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(336, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 9
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(254, 21)
        Me.txtOrderBy.TabIndex = 8
        '
        'frmLeaveIssue_AT_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.KeyPreview = True
        Me.Name = "frmLeaveIssue_AT_Report"
        Me.Text = "frmLeaveIssue_AT_Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents lblAuditToDate As System.Windows.Forms.Label
    Public WithEvents dtpAuditToDate As System.Windows.Forms.DateTimePicker
    Private WithEvents lblUserName As System.Windows.Forms.Label
    Public WithEvents cboUser As System.Windows.Forms.ComboBox
    Public WithEvents lblAuditDate As System.Windows.Forms.Label
    Public WithEvents dtpAuditFromDate As System.Windows.Forms.DateTimePicker
    Private WithEvents lblAuditType As System.Windows.Forms.Label
    Public WithEvents cboAuditType As System.Windows.Forms.ComboBox
    Private WithEvents lblLeaveName As System.Windows.Forms.Label
    Public WithEvents cboLeave As System.Windows.Forms.ComboBox
    Public WithEvents lblLeaveTodate As System.Windows.Forms.Label
    Public WithEvents dtpLeaveToDate As System.Windows.Forms.DateTimePicker
    Public WithEvents lblLeaveFromDate As System.Windows.Forms.Label
    Public WithEvents dtpLeaveFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchLeaveType As eZee.Common.eZeeGradientButton
End Class
