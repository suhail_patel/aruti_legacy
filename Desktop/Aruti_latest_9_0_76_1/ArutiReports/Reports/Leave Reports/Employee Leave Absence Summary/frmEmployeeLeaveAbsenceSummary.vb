#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
#End Region


Public Class frmEmployeeLeaveAbsenceSummary

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmEmployeeLeaveAbsenceSummaryReport"
    Private objEmpLeaveAbsence As clsEmployeeLeaveAbsenceSummary
#End Region

#Region "Constructor"
    Public Sub New()
        objEmpLeaveAbsence = New clsEmployeeLeaveAbsenceSummary(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpLeaveAbsence.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Public Function"

    Public Sub FillCombo()
        Try
            cboLeaveType.DataSource = (New clsleavetype_master).getListForCombo(, True).Tables(0)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboLeaveType.SelectedValue = 0

            dtStartDateFrom.Value = DateTime.Now
            dtStartDateTo.Value = DateTime.Now

            'Vimal (10 Dec 2010) -- Start 
            'objEmpLeaveAbsence.setDefaultOrderBy(cboLeaveType.SelectedIndex)
            objEmpLeaveAbsence.setDefaultOrderBy(0)
            'Vimal (10 Dec 2010) -- End
            txtOrderBy.Text = objEmpLeaveAbsence.OrderByDisplay

           
            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpLeaveAbsence.SetDefaultValue()

            objEmpLeaveAbsence._LeaveTypeId = cboLeaveType.SelectedValue
            objEmpLeaveAbsence._LeaveTypeName = cboLeaveType.Text

            objEmpLeaveAbsence._StartDateFrom = dtStartDateFrom.Value
            objEmpLeaveAbsence._StartDateTo = dtStartDateTo.Value



            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            objEmpLeaveAbsence._IncludeInactiveEmp = chkInactiveemp.Checked
            'Pinkal (11-MAY-2012) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function
#End Region

#Region "Form's Events"

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpLeaveAbsence = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)


            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End


            Me._Title = objEmpLeaveAbsence._ReportName
            Me._Message = objEmpLeaveAbsence._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_Load", mstrModuleName)
        End Try
    End Sub


    Private Sub frmEmployeeLeaveAbsenceSummaryReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeLeaveAbsenceSummaryReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"
    Private Sub frmEmployeeLeaveAbsenceSummaryReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objEmpLeaveAbsence.generateReport(0, e.Type, enExportAction.None)
            objEmpLeaveAbsence.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                         , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                         , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                         , 0, e.Type, enExportAction.None, 0)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objEmpLeaveAbsence.generateReport(0, enPrintAction.None, e.Type)
            objEmpLeaveAbsence.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                 , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                 , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                 , 0, enPrintAction.None, e.Type)
            'Pinkal (24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeLeaveAbsenceSummaryReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeLeaveAbsenceSummary.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeLeaveAbsenceSummaryReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeLeaveAbsenceSummaryReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub




#End Region

#Region "Controls"
    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpLeaveAbsence.setOrderBy(0)
            txtOrderBy.Text = objEmpLeaveAbsence.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblStartDateTo.Text = Language._Object.getCaption(Me.lblStartDateTo.Name, Me.lblStartDateTo.Text)
			Me.lblStartDateFrom.Text = Language._Object.getCaption(Me.lblStartDateFrom.Name, Me.lblStartDateFrom.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
