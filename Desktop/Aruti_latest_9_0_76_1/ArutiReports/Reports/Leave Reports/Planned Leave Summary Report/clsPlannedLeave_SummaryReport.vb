'************************************************************************************************************************************
'Class Name : clsPlannedLeave_SummaryReport.vb
'Purpose    :
'Date       : 04/10/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsPlannedLeave_SummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPlannedLeave_SummaryReport"
    Private mstrReportId As String = enArutiReport.Planned_Leave_Summary_Report  '277
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeaveTypeId As Integer = 0
    Private mstrLeaveTypeName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeId() As Integer
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeName() As String
        Set(ByVal value As String)
            mstrLeaveTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintLeaveTypeId = 0
            mstrLeaveTypeName = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mdtFromDate <> Nothing Then
                Me._FilterQuery &= " AND (CONVERT(CHAR(8),lvleaveplanner.startdate,112) BETWEEN @startdate AND @enddate "
                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "From Date: ") & " " & mdtFromDate.Date & " "
            End If

            If mdtToDate <> Nothing Then
                Me._FilterQuery &= " OR CONVERT(CHAR(8),lvleaveplanner.stopdate,112) BETWEEN @startdate AND @enddate) "
                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "To Date: ") & " " & mdtToDate.Date & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND lvleaveplanner.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintLeaveTypeId > 0 Then
                Me._FilterQuery &= " AND lvleaveplanner.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Leave Type: ") & " " & mstrLeaveTypeName & " "
            End If

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", " & Me.OrderByQuery
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "  ORDER BY  " & Me.OrderByQuery
                End If
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("lvleavetype_master.leavename", Language.getMessage(mstrModuleName, 5, "Leave Type")))
            iColumn_DetailReport.Add(New IColumn("lvleaveplanner.Startdate", Language.getMessage(mstrModuleName, 3, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("lvleaveplanner.Stopdate", Language.getMessage(mstrModuleName, 4, "End Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objDataOperation = New clsDataOperation

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrConditionQry As String

            StrInnerQry = "" : StrConditionQry = ""

            Dim objLeaveApprover As New clsleaveapprover_master
            dsCompany = objLeaveApprover.GetExternalApproverList(objDataOperation, "List")
            objLeaveApprover = Nothing


            objDataOperation.ClearParameters()


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)
            


            StrQ = " SELECT  " & _
                       " lvleaveplanner.leaveplannerunkid " & _
                       ", lvleaveplanner.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode,'') AS Empcode"

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
            End If

            StrQ &= ", CONVERT(CHAR(8),lvleaveplanner.Startdate,112) AS Startdate  " & _
                         ", CONVERT(CHAR(8),lvleaveplanner.Stopdate,112) AS Stopdate " & _
                         ", lvleavetype_master.leavename AS LeaveType " & _
                         ", ISNULL(lv.DayFraction,0.00) AS Days "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM lvleaveplanner " & _
                         " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveplanner.leavetypeunkid " & _
                         " JOIN ( " & _
                         "          SELECT leaveplannerunkid, SUM(dayfraction) AS DayFraction " & _
                         "          FROM lvplannerday_fraction WHERE isvoid = 0 group by leaveplannerunkid " & _
                         " ) AS lv on lv.leaveplannerunkid = lvleaveplanner.leaveplannerunkid " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveplanner.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE lvleaveplanner.isvoid = 0 "


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0).Copy


            If mintViewIndex > 0 Then
                mdtTableExcel = New DataView(mdtTableExcel, "", "GName", DataViewRowState.CurrentRows).ToTable
            End If

            If mdtTableExcel.Columns.Contains("Id") Then
                mdtTableExcel.Columns.Remove("Id")
            End If

            If mdtTableExcel.Columns.Contains("leaveplannerunkid") Then
                mdtTableExcel.Columns.Remove("leaveplannerunkid")
            End If

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

          
            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName", "Empcode"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
                Dim strGrpCols As String() = {"Empcode"}
                strarrGroupColumns = strGrpCols
            End If

            Dim columnNames As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, columnNames)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------



            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
                If mdtTableExcel.Rows(i)("Startdate").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Startdate") = FormatDateTime(eZeeDate.convertDate(mdtTableExcel.Rows(i)("Startdate").ToString()), DateFormat.ShortDate)
                End If
                If mdtTableExcel.Rows(i)("Stopdate").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Stopdate") = FormatDateTime(eZeeDate.convertDate(mdtTableExcel.Rows(i)("Stopdate").ToString()), DateFormat.ShortDate)
                End If
            Next


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("Empcode").Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 2, "Employee")
            mdtTableExcel.Columns("Startdate").Caption = Language.getMessage(mstrModuleName, 3, "Start Date")
            mdtTableExcel.Columns("Stopdate").Caption = Language.getMessage(mstrModuleName, 4, "End Date")
            mdtTableExcel.Columns("LeaveType").Caption = Language.getMessage(mstrModuleName, 5, "Leave Type")
            mdtTableExcel.Columns("Days").Caption = Language.getMessage(mstrModuleName, 6, "Days")
            'mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 7, "Status")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Code")
			Language.setMessage(mstrModuleName, 2, "Employee")
			Language.setMessage(mstrModuleName, 3, "Start Date")
			Language.setMessage(mstrModuleName, 4, "End Date")
			Language.setMessage(mstrModuleName, 5, "Leave Type")
			Language.setMessage(mstrModuleName, 6, "Days")
			Language.setMessage(mstrModuleName, 8, "Prepared By :")
			Language.setMessage(mstrModuleName, 9, "Checked By :")
			Language.setMessage(mstrModuleName, 10, "Approved By")
			Language.setMessage(mstrModuleName, 11, "Received By :")
			Language.setMessage(mstrModuleName, 12, "From Date:")
			Language.setMessage(mstrModuleName, 13, "To Date:")
			Language.setMessage(mstrModuleName, 14, "Employee:")
			Language.setMessage(mstrModuleName, 15, "Leave Type:")
			Language.setMessage(mstrModuleName, 16, " Order By :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
