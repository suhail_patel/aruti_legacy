﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSDLReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCountEarning = New System.Windows.Forms.Label
        Me.rdbTaxablelEarning = New System.Windows.Forms.RadioButton
        Me.rdbAllEarning = New System.Windows.Forms.RadioButton
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.chkIngnoreZeroSDL = New System.Windows.Forms.CheckBox
        Me.objCheckAll = New System.Windows.Forms.CheckBox
        Me.lblPeriodList = New System.Windows.Forms.Label
        Me.lvPeriodList = New System.Windows.Forms.ListView
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.lblEmployerContribution = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objcolhPeriodStart = New System.Windows.Forms.ColumnHeader
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(769, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lblCountEarning)
        Me.gbMandatoryInfo.Controls.Add(Me.rdbTaxablelEarning)
        Me.gbMandatoryInfo.Controls.Add(Me.rdbAllEarning)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.chkIngnoreZeroSDL)
        Me.gbMandatoryInfo.Controls.Add(Me.objCheckAll)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriodList)
        Me.gbMandatoryInfo.Controls.Add(Me.lvPeriodList)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmployerContribution)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboTranHead)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.lblReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboReportType)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(437, 381)
        Me.gbMandatoryInfo.TabIndex = 2
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Info"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCountEarning
        '
        Me.lblCountEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountEarning.Location = New System.Drawing.Point(8, 331)
        Me.lblCountEarning.Name = "lblCountEarning"
        Me.lblCountEarning.Size = New System.Drawing.Size(87, 16)
        Me.lblCountEarning.TabIndex = 74
        Me.lblCountEarning.Text = "Count Earning"
        Me.lblCountEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdbTaxablelEarning
        '
        Me.rdbTaxablelEarning.Checked = True
        Me.rdbTaxablelEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbTaxablelEarning.Location = New System.Drawing.Point(235, 331)
        Me.rdbTaxablelEarning.Name = "rdbTaxablelEarning"
        Me.rdbTaxablelEarning.Size = New System.Drawing.Size(125, 17)
        Me.rdbTaxablelEarning.TabIndex = 73
        Me.rdbTaxablelEarning.TabStop = True
        Me.rdbTaxablelEarning.Text = "Taxable Earning"
        Me.rdbTaxablelEarning.UseVisualStyleBackColor = True
        '
        'rdbAllEarning
        '
        Me.rdbAllEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbAllEarning.Location = New System.Drawing.Point(101, 331)
        Me.rdbAllEarning.Name = "rdbAllEarning"
        Me.rdbAllEarning.Size = New System.Drawing.Size(125, 17)
        Me.rdbAllEarning.TabIndex = 72
        Me.rdbAllEarning.Text = "All Earning"
        Me.rdbAllEarning.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(339, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 70
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkIngnoreZeroSDL
        '
        Me.chkIngnoreZeroSDL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIngnoreZeroSDL.Location = New System.Drawing.Point(101, 354)
        Me.chkIngnoreZeroSDL.Name = "chkIngnoreZeroSDL"
        Me.chkIngnoreZeroSDL.Size = New System.Drawing.Size(125, 17)
        Me.chkIngnoreZeroSDL.TabIndex = 14
        Me.chkIngnoreZeroSDL.Text = "Ignore Zero SDL"
        Me.chkIngnoreZeroSDL.UseVisualStyleBackColor = True
        '
        'objCheckAll
        '
        Me.objCheckAll.AutoSize = True
        Me.objCheckAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCheckAll.Location = New System.Drawing.Point(108, 93)
        Me.objCheckAll.Name = "objCheckAll"
        Me.objCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objCheckAll.TabIndex = 12
        Me.objCheckAll.UseVisualStyleBackColor = True
        '
        'lblPeriodList
        '
        Me.lblPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodList.Location = New System.Drawing.Point(8, 92)
        Me.lblPeriodList.Name = "lblPeriodList"
        Me.lblPeriodList.Size = New System.Drawing.Size(87, 16)
        Me.lblPeriodList.TabIndex = 11
        Me.lblPeriodList.Text = "Period List"
        Me.lblPeriodList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvPeriodList
        '
        Me.lvPeriodList.CheckBoxes = True
        Me.lvPeriodList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhPeriodName, Me.objcolhPeriodStart})
        Me.lvPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriodList.FullRowSelect = True
        Me.lvPeriodList.GridLines = True
        Me.lvPeriodList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriodList.Location = New System.Drawing.Point(101, 89)
        Me.lvPeriodList.Name = "lvPeriodList"
        Me.lvPeriodList.Size = New System.Drawing.Size(324, 236)
        Me.lvPeriodList.TabIndex = 10
        Me.lvPeriodList.UseCompatibleStateImageBehavior = False
        Me.lvPeriodList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Period"
        Me.colhPeriodName.Width = 295
        '
        'lblEmployerContribution
        '
        Me.lblEmployerContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerContribution.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployerContribution.Name = "lblEmployerContribution"
        Me.lblEmployerContribution.Size = New System.Drawing.Size(87, 16)
        Me.lblEmployerContribution.TabIndex = 8
        Me.lblEmployerContribution.Text = "Co. Contribution"
        Me.lblEmployerContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(232, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(62, 16)
        Me.lblPeriod.TabIndex = 7
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod.Visible = False
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(101, 61)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(125, 21)
        Me.cboTranHead.TabIndex = 6
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(300, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(125, 21)
        Me.cboPeriod.TabIndex = 5
        Me.cboPeriod.Visible = False
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(87, 16)
        Me.lblReportType.TabIndex = 3
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(101, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(324, 21)
        Me.cboReportType.TabIndex = 4
        '
        'objcolhPeriodStart
        '
        Me.objcolhPeriodStart.Tag = "objcolhPeriodStart"
        Me.objcolhPeriodStart.Width = 0
        '
        'frmSDLReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 567)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSDLReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployerContribution As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lvPeriodList As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPeriodList As System.Windows.Forms.Label
    Friend WithEvents objCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkIngnoreZeroSDL As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents rdbTaxablelEarning As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAllEarning As System.Windows.Forms.RadioButton
    Friend WithEvents lblCountEarning As System.Windows.Forms.Label
    Friend WithEvents objcolhPeriodStart As System.Windows.Forms.ColumnHeader
End Class
