'************************************************************************************************************************************
'Class Name :clsSDLReport.vb
'Purpose    :
'Date       :11 Apr 2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsSDLReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsSDLReport"
    Private mstrReportId As String = enArutiReport.SDL_Statutory_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "

    Private mintReport_TypeId As Integer = 0
    Private mstrReport_TypeName As String = String.Empty
    Private mintSDLTranHeadId As Integer = 0
    Private mstrSDLTranHeadName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrPeriodIds As String = String.Empty
    Private mstrComma_PeriodName As String = String.Empty
    Private mblnIngnoreZeroSDL As Boolean = False

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    'Pinkal (24-Jul-2012) -- End

    'Sohail (21 Feb 2017) -- Start
    'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
    Private mblnCountAllEarning As Boolean = False
    Private mblnCountTaxableEarning As Boolean = False
    'Sohail (21 Feb 2017) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _Report_TypeId() As Integer
        Set(ByVal value As Integer)
            mintReport_TypeId = value
        End Set
    End Property

    Public WriteOnly Property _Report_TypeName() As String
        Set(ByVal value As String)
            mstrReport_TypeName = value
        End Set
    End Property

    Public WriteOnly Property _SDLTranHeadId() As Integer
        Set(ByVal value As Integer)
            mintSDLTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _SDLTranHeadName() As String
        Set(ByVal value As String)
            mstrSDLTranHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    ''' <summary>
    ''' Use Only for Half Year Certificate
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property _Period_Ids() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    ''' <summary>
    ''' Use Only for Half Year Certificate
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property _Comma_PeriodName() As String
        Set(ByVal value As String)
            mstrComma_PeriodName = value
        End Set
    End Property

    ''' <summary>
    ''' Use Only for Half Year Certificate
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property _IngnoreZeroSDL() As Boolean
        Set(ByVal value As Boolean)
            mblnIngnoreZeroSDL = value
        End Set
    End Property

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 


    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Pinkal (24-Jul-2012) -- End
    'Sohail (21 Feb 2017) -- Start
    'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
    Public WriteOnly Property _AllEarning() As Boolean
        Set(ByVal value As Boolean)
            mblnCountAllEarning = value
        End Set
    End Property

    Public WriteOnly Property _TaxableEarning() As Boolean
        Set(ByVal value As Boolean)
            mblnCountTaxableEarning = value
        End Set
    End Property
    'Sohail (21 Feb 2017) -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReport_TypeId = 0
            mstrReport_TypeName = String.Empty
            mintSDLTranHeadId = 0
            mstrSDLTranHeadName = String.Empty
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrPeriodIds = String.Empty
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIncludeInactiveEmp = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Pinkal (24-Jul-2012) -- End
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            mblnCountAllEarning = False
            mblnCountTaxableEarning = False
            'Sohail (21 Feb 2017) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    Select Case pintReportType
        '        Case 0
        '            objRpt = Generate_DetailReport()
        '        Case 1
        '            objRpt = Generate_HalfYear_Certificate()
        '    End Select
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            Select Case pintReportType
                Case 0
                    objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
                Case 1
                    objRpt = Generate_HalfYear_Certificate(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            End Select

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_ReportType(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @MonthReport AS Name " & _
                   "UNION SELECT 1 AS Id, @HalfYear AS Name "

            objDataOperation.AddParameter("@MonthReport", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Month Report"))
            objDataOperation.AddParameter("@HalfYear", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Half Year Certificate Report"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_ReportType; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsListEmolumentA As New DataSet
        Dim dsListEmolumentB As New DataSet
        Dim dsSDL As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Sohail (17 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (17 Aug 2012) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
            'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes


            'StrQ = "SELECT " & _
            '       "  ISNULL(prtranhead_master.trnheadname,'') AS EmolumentA " & _
            '       " ,ISNULL(SUM(prpayrollprocess_tran.amount),0) AS Amount " & _
            '       "FROM prpayrollprocess_tran " & _
            '       "  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '       "  JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '       "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '       "WHERE prtnaleave_tran.payperiodunkid = @PeriodId " & _
            '       "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '       "  AND prtranhead_master.typeof_id NOT IN (" & enTypeOf.Allowance & ") " & _
            '       "  AND prpayrollprocess_tran.isvoid = 0 " & _
            '       "  AND prtnaleave_tran.isvoid = 0 " & _
            '       "  AND prtranhead_master.isvoid = 0 " & _
            '       "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "


            StrQ = "SELECT " & _
                   "  ISNULL(prtranhead_master.trnheadname,'') AS EmolumentA " & _
                   " ,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                   "FROM prpayrollprocess_tran " & _
                   "  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "  JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 26 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= " WHERE prtnaleave_tran.payperiodunkid = @PeriodId " & _
            '        "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '        "  AND prtranhead_master.typeof_id NOT IN (" & enTypeOf.Allowance & ") " & _
            '        "  AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "  AND prtnaleave_tran.isvoid = 0 " & _
            '        "  AND prtranhead_master.isvoid = 0 " & _
            '        "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "

            StrQ &= " WHERE prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                   "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                   "  AND prtranhead_master.typeof_id NOT IN (" & enTypeOf.Allowance & ") " & _
                   "  AND prpayrollprocess_tran.isvoid = 0 " & _
                   "  AND prtnaleave_tran.isvoid = 0 " & _
                   "  AND prtranhead_master.isvoid = 0 "
            'Sohail (30 Sep 2017) - [WHERE prtnaleave_tran.payperiodunkid = @PeriodId] = [WHERE prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")]
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            '"  AND prtranhead_master.istaxable = 1 "
            If mblnCountTaxableEarning = True Then
                StrQ &= "  AND prtranhead_master.istaxable = 1 "
            End If
            'Sohail (21 Feb 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "
            'S.SANDEEP [ 26 SEPT 2013 ] -- END


            'Pinkal (24-Jul-2012) -- End


            'objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId) 'Sohail (30 Sep 2017)

            dsListEmolumentA = objDataOperation.ExecQuery(StrQ, "EmolA")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
            'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            'StrQ = "SELECT " & _
            '        "  ISNULL(prtranhead_master.trnheadname,'') AS EmolumentB " & _
            '        " ,ISNULL(SUM(prpayrollprocess_tran.amount),0) AS Amount " & _
            '        "FROM prpayrollprocess_tran " & _
            '        "  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "  JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '        "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '        "WHERE prtnaleave_tran.payperiodunkid = @PeriodId " & _
            '        "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '        "  AND prtranhead_master.typeof_id IN (" & enTypeOf.Allowance & ") " & _
            '        "  AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "  AND prtnaleave_tran.isvoid = 0 " & _
            '        "  AND prtranhead_master.isvoid = 0 " & _
            '        "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "

            StrQ = "SELECT " & _
                   "  ISNULL(prtranhead_master.trnheadname,'') AS EmolumentB " & _
                   " ,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                   "FROM prpayrollprocess_tran " & _
                   "  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "  JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 26 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= " WHERE prtnaleave_tran.payperiodunkid = @PeriodId " & _
            '        "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '        "  AND prtranhead_master.typeof_id IN (" & enTypeOf.Allowance & ") " & _
            '        "  AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "  AND prtnaleave_tran.isvoid = 0 " & _
            '        "  AND prtranhead_master.isvoid = 0 " & _
            '        "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "

            StrQ &= " WHERE prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                   "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                   "  AND prtranhead_master.typeof_id IN (" & enTypeOf.Allowance & ") " & _
                   "  AND prpayrollprocess_tran.isvoid = 0 " & _
                   "  AND prtnaleave_tran.isvoid = 0 " & _
                   "  AND prtranhead_master.isvoid = 0 "
            'Sohail (30 Sep 2017) - [WHERE prtnaleave_tran.payperiodunkid = @PeriodId] = [WHERE prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")]
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            '"  AND prtranhead_master.istaxable = 1 "
            If mblnCountTaxableEarning = True Then
                StrQ &= "  AND prtranhead_master.istaxable = 1 "
            End If
            'Sohail (21 Feb 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "
            'S.SANDEEP [ 26 SEPT 2013 ] -- END


            'Pinkal (24-Jul-2012) -- End

            dsListEmolumentB = objDataOperation.ExecQuery(StrQ, "EmolB")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dblSDLAmount As Decimal = 0

            'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
            'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            'StrQ = "SELECT " & _
            '       "   ISNULL(SUM(amount),0) AS Amount " & _
            '       "FROM prpayrollprocess_tran " & _
            '       "   JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '       "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '       "WHERE tranheadunkid = @TranHeadId " & _
            '       "   AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
            '       "   AND prpayrollprocess_tran.isvoid = 0 " & _
            '       "   AND prtnaleave_tran.isvoid = 0 "

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT - Rounding Issue at TWAWEZA
            'StrQ = "SELECT " & _
            '       "   SUM(CAST(ISNULL(amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
            '       "FROM prpayrollprocess_tran " & _
            '       "   JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '   "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "
            StrQ = "SELECT  SUM(ISNULL(amount, 0)) AS Amount " & _
                   "FROM prpayrollprocess_tran " & _
                   "   JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
               "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "
            'Sohail (28 Aug 2013) -- End

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= " WHERE tranheadunkid = @TranHeadId " & _
                   "   AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                   "   AND prpayrollprocess_tran.isvoid = 0 " & _
                   "   AND prtnaleave_tran.isvoid = 0 "
            'Sohail (30 Sep 2017) - [AND prtnaleave_tran.payperiodunkid = @PeriodId] = [AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")]

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (24-Jul-2012) -- End

            objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSDLTranHeadId)

            dsSDL = objDataOperation.ExecQuery(StrQ, "SDL")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsSDL.Tables("SDL").Rows.Count > 0 Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'ISSUE : GETTING ISDBNULL ERROR WHEN NO EMPLOYEE FOUND IN QUERY
                'dblSDLAmount = dsSDL.Tables("SDL").Rows(0)("Amount")
                If IsDBNull(dsSDL.Tables("SDL").Rows(0)("Amount")) = False Then
                    dblSDLAmount = dsSDL.Tables("SDL").Rows(0)("Amount")
                End If
                'S.SANDEEP [04 JUN 2015] -- END
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim dblSubTotalA As Decimal = 0
            Dim dblSubTotalB As Decimal = 0

            Dim intEmolACount As Integer = dsListEmolumentA.Tables("EmolA").Rows.Count
            Dim intEmolBCount As Integer = dsListEmolumentB.Tables("EmolB").Rows.Count

            If intEmolACount >= intEmolBCount Then
                For i As Integer = 0 To intEmolACount - 1
                    Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dsListEmolumentA.Tables("EmolA").Rows(i)("EmolumentA")
                    rpt_Row.Item("Column2") = Format(CDec(dsListEmolumentA.Tables("EmolA").Rows(i)("Amount")), GUI.fmtCurrency)
                    dblSubTotalA = dblSubTotalA + CDec(rpt_Row.Item("Column2"))
                    For j As Integer = i To i
                        If i < intEmolBCount Then
                            rpt_Row.Item("Column3") = dsListEmolumentB.Tables("EmolB").Rows(j)("EmolumentB")
                            rpt_Row.Item("Column4") = Format(CDec(dsListEmolumentB.Tables("EmolB").Rows(j)("Amount")), GUI.fmtCurrency)
                            dblSubTotalB = dblSubTotalB + CDec(rpt_Row.Item("Column4"))
                        Else
                            rpt_Row.Item("Column3") = ""
                            rpt_Row.Item("Column4") = ""
                            dblSubTotalB = dblSubTotalB + CDec(0)
                        End If
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next
            ElseIf intEmolACount < intEmolBCount Then
                For i As Integer = 0 To intEmolBCount - 1
                    Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column3") = dsListEmolumentB.Tables("EmolB").Rows(i)("EmolumentB")
                    rpt_Row.Item("Column4") = Format(CDec(dsListEmolumentB.Tables("EmolB").Rows(i)("Amount")), GUI.fmtCurrency)
                    dblSubTotalB = dblSubTotalB + CDec(rpt_Row.Item("Column4"))
                    For j As Integer = i To i
                        If i < intEmolACount Then
                            rpt_Row.Item("Column1") = dsListEmolumentA.Tables("EmolA").Rows(j)("EmolumentA")
                            rpt_Row.Item("Column2") = Format(CDec(dsListEmolumentA.Tables("EmolA").Rows(j)("Amount")), GUI.fmtCurrency)
                            dblSubTotalA = dblSubTotalA + CDec(rpt_Row.Item("Column2"))
                        Else
                            rpt_Row.Item("Column1") = ""
                            rpt_Row.Item("Column2") = ""
                            dblSubTotalA = dblSubTotalA + CDec(0)
                        End If
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next
            End If

            objRpt = New ArutiReport.Designer.rptSDLMonthlyReport
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 3, "TANZANIA REVENUE AUTHORITY"))
            Call ReportFunction.TextChange(objRpt, "txtHeading2", Language.getMessage(mstrModuleName, 4, "SKILLS AND DEVELOPMENT LEVY"))
            Call ReportFunction.TextChange(objRpt, "txtMonthReturn", Language.getMessage(mstrModuleName, 5, "MONTHLY RETURN"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "YEAR:"))
            Call ReportFunction.TextChange(objRpt, "txtYearData", FinancialYear._Object._FinancialYear_Name)

            Call ReportFunction.TextChange(objRpt, "txtTRA", Language.getMessage(mstrModuleName, 7, "(To be submitted to the TRA office within 30 days after the end of each six-month calendar period)"))
            Call ReportFunction.TextChange(objRpt, "txtEmplrInfo", Language.getMessage(mstrModuleName, 8, "EMPLOYER’S INFORMATION"))
            Call ReportFunction.TextChange(objRpt, "lblTin", Language.getMessage(mstrModuleName, 9, "TIN:"))
            Call ReportFunction.TextChange(objRpt, "txtTinNo", Company._Object._Tinno)

            Call ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 10, "Name of Employer:"))
            Call ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)

            Call ReportFunction.TextChange(objRpt, "txtPostalAddress", Language.getMessage(mstrModuleName, 11, "Postal Address:"))
            Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 12, " P.O. Box"))
            Call ReportFunction.TextChange(objRpt, "txtPOBox", Company._Object._Post_Code_No)

            Call ReportFunction.TextChange(objRpt, "lblPostalCity", Language.getMessage(mstrModuleName, 13, "Postal City"))
            Call ReportFunction.TextChange(objRpt, "txtPostalCity", Company._Object._City_Name)

            Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Language.getMessage(mstrModuleName, 14, "Physical Address:"))
            Call ReportFunction.TextChange(objRpt, "lblPlotNo", Language.getMessage(mstrModuleName, 15, "Plot Number"))
            Call ReportFunction.TextChange(objRpt, "txtPlotNo", "")

            Call ReportFunction.TextChange(objRpt, "lblBlockNo", Language.getMessage(mstrModuleName, 16, "Block Number"))
            Call ReportFunction.TextChange(objRpt, "txtBlockNo", "")

            Call ReportFunction.TextChange(objRpt, "lblStreet", Language.getMessage(mstrModuleName, 17, "Street/Location"))
            Call ReportFunction.TextChange(objRpt, "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)

            Call ReportFunction.TextChange(objRpt, "txtForwardCaption", Language.getMessage(mstrModuleName, 72, "I forward herewith SDL Return for the month of"))
            'Sohail (30 Sep 2017) -- Start
            'Enhancement - 70.1 - Allow multiple period selection on month SDL  [Anatory Rutta] - (RefNo: 83).
            'Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtMonth", mstrComma_PeriodName)
            'Sohail (30 Sep 2017) -- End
            Call ReportFunction.TextChange(objRpt, "txtMonthYear", Year(FinancialYear._Object._Database_End_Date))

            Dim objExRate As New clsExchangeRate

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            Call ReportFunction.TextChange(objRpt, "txtEmolumentA", Language.getMessage(mstrModuleName, 18, "EMOLUMENTS"))
            Call ReportFunction.TextChange(objRpt, "txtAmountA", Language.getMessage(mstrModuleName, 19, "AMOUNT") & "/" & objExRate._Currency_Sign)
            Call ReportFunction.TextChange(objRpt, "txtEmolumentB", Language.getMessage(mstrModuleName, 18, "EMOLUMENTS"))
            Call ReportFunction.TextChange(objRpt, "txtAmountB", Language.getMessage(mstrModuleName, 19, "AMOUNT") & "/" & objExRate._Currency_Sign)

            Call ReportFunction.TextChange(objRpt, "lblSubTotalA", Language.getMessage(mstrModuleName, 22, "Subtotal A"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotalA", Format(CDec(dblSubTotalA), GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "lblSubTotalB", Language.getMessage(mstrModuleName, 21, "Subtotal B"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotalB", Format(CDec(dblSubTotalB), GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "lblGrandTotal", Language.getMessage(mstrModuleName, 73, "Grand Total (A+B)"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Format(CDec(dblSubTotalA + dblSubTotalB), GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "lblSDLAmount", Language.getMessage(mstrModuleName, 23, "Whereof SDL at 5% amounts to"))
            Call ReportFunction.TextChange(objRpt, "txtSDLAmount", Format(CDec(dblSDLAmount), GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "txtPBB", Language.getMessage(mstrModuleName, 24, "Payment made at the Bank Branch"))
            Call ReportFunction.TextChange(objRpt, "txtPSDS", Language.getMessage(mstrModuleName, 25, "Through Payment Slip/Deposit Slip"))
            Call ReportFunction.TextChange(objRpt, "txtPSDSDate", Language.getMessage(mstrModuleName, 26, "Date"))

            Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 27, "Signature:"))
            Call ReportFunction.TextChange(objRpt, "txtSignatureDate", Language.getMessage(mstrModuleName, 26, "Date"))

            Call ReportFunction.TextChange(objRpt, "txtStamp", Language.getMessage(mstrModuleName, 28, "Rubber Stamp:"))
            Call ReportFunction.TextChange(objRpt, "txtNotes", Language.getMessage(mstrModuleName, 29, "NOTES"))

            Call ReportFunction.TextChange(objRpt, "txtNCaption1", Language.getMessage(mstrModuleName, 30, "1.	This Return to be submitted within 7 days after the end of the month to which it refers."))
            Call ReportFunction.TextChange(objRpt, "txtNCaption2", Language.getMessage(mstrModuleName, 31, "2.	The Return is in respect of gross emoluments paid by an employer to his employees during a month."))
            Call ReportFunction.TextChange(objRpt, "txtNCaption3", Language.getMessage(mstrModuleName, 32, "3.	State in") & objExRate._Currency_Sign & Language.getMessage(mstrModuleName, 71, " only rounding up to the next higher unit where applicable."))
            Call ReportFunction.TextChange(objRpt, "txtNCaption4", Language.getMessage(mstrModuleName, 33, "4.	Employees includes any person engaged by any one employer for part-time or temporary employment or casual employment which is not regular and lasts for a period of less than one month emoluments for which are calculated on an hourly or daily basis."))
            Call ReportFunction.TextChange(objRpt, "txtNCaption5", Language.getMessage(mstrModuleName, 34, "5.	*) If authority to exclude these allowances has been obtained from the Commissioner, please enclose the original copy of such authority."))
            Call ReportFunction.TextChange(objRpt, "txtNCaption6", Language.getMessage(mstrModuleName, 35, "6.	Any other allowances includes fuel allowances, hardship allowances, lunch allowances, meal coupons, overtime allowances, responsibility allowances, risk/hazard allowances, transport allowances etc."))
            Call ReportFunction.TextChange(objRpt, "txtNCaption7", Language.getMessage(mstrModuleName, 36, "7.	If NO PAYMENT is made, state ""NIL"". Do not leave the space blank or place a dash (-) therein."))

            Call ReportFunction.TextChange(objRpt, "txtSDLReturn", Language.getMessage(mstrModuleName, 37, "ITX219.01.E - SDL Monthly Return"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_HalfYear_Certificate(ByVal strDatabaseName As String, _
                                                   ByVal intUserUnkid As Integer, _
                                                   ByVal intYearUnkid As Integer, _
                                                   ByVal intCompanyUnkid As Integer, _
                                                   ByVal dtPeriodEnd As Date, _
                                                   ByVal strUserModeSetting As String, _
                                                   ByVal blnOnlyApproved As Boolean, _
                                                   ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_HalfYear_Certificate() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Sohail (17 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (17 Aug 2012) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            'StrQ = "SELECT " & _
            '        "     periodunkid AS PeriodId " & _
            '        "    ,ISNULL(cfcommon_period_tran.period_name, '') AS PName " & _
            '        "    ,ISNULL(PAmount, 0) AS PAmount " & _
            '        "    ,ISNULL(OAmount,0) AS OAmount " & _
            '        "    ,ISNULL(EAmount, 0) AS EAmount " & _
            '        "    ,ISNULL(SDLAmount, 0) AS SDLAmount " & _
            '        "FROM cfcommon_period_tran " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "         prtnaleave_tran.payperiodunkid AS Pid " & _
            '        "        ,ISNULL(cfcommon_master.name, '') AS PEmplType " & _
            '        "        ,ISNULL(SUM(amount), 0) AS PAmount " & _
            '        "    FROM prpayrollprocess_tran " & _
            '        "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "		 JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
            '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '        "	WHERE 1 = 1  " & _
            '        "		AND cfcommon_master.name LIKE '%Permanent%' " & _
            '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "        AND prtnaleave_tran.isvoid = 0 " & _
            '        "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "    GROUP BY prtnaleave_tran.payperiodunkid,ISNULL(cfcommon_master.name, '') " & _
            '        "    ) AS PERM ON PERM.Pid = cfcommon_period_tran.periodunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "         prtnaleave_tran.payperiodunkid AS OPid " & _
            '        "        ,ISNULL(cfcommon_master.name, '') AS OEmplType " & _
            '        "        ,ISNULL(SUM(amount), 0) AS OAmount " & _
            '        "    FROM prpayrollprocess_tran " & _
            '        "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "		 JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
            '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '        "	WHERE 1 = 1 " & _
            '        "		 AND cfcommon_master.name NOT LIKE '%Permanent%' " & _
            '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "        AND prtnaleave_tran.isvoid = 0 " & _
            '        "		 AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "    GROUP BY prtnaleave_tran.payperiodunkid,ISNULL(cfcommon_master.name, '') " & _
            '        "    ) AS OTHE ON OTHE.OPid = cfcommon_period_tran.periodunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "         prtnaleave_tran.payperiodunkid AS EPId " & _
            '        "        ,ISNULL(SUM(prpayrollprocess_tran.amount), 0) AS EAmount " & _
            '        "     FROM prpayrollprocess_tran " & _
            '        "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '        "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '        "	WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "        AND prtnaleave_tran.isvoid = 0 " & _
            '        "        AND prtranhead_master.isvoid = 0 " & _
            '        "     GROUP BY prtnaleave_tran.payperiodunkid " & _
            '        "    ) AS Emol ON Emol.EPId = cfcommon_period_tran.periodunkid "


            StrQ = "SELECT " & _
                    "     periodunkid AS PeriodId " & _
                    "    ,ISNULL(cfcommon_period_tran.period_name, '') AS PName " & _
                    "    ,ISNULL(PAmount, 0) AS PAmount " & _
                    "    ,ISNULL(OAmount,0) AS OAmount " & _
                    "    ,ISNULL(EAmount, 0) AS EAmount " & _
                    "    ,ISNULL(SDLAmount, 0) AS SDLAmount " & _
                    "FROM cfcommon_period_tran " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "         prtnaleave_tran.payperiodunkid AS Pid " & _
                    "        ,SUM(CAST(ISNULL(amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS PAmount " & _
                    "    FROM prpayrollprocess_tran " & _
                    "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "		 JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
                  "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE 1 = 1  " & _
                    "		AND cfcommon_master.name LIKE '%Permanent%' " & _
                    "        AND prpayrollprocess_tran.isvoid = 0 " & _
                    "        AND prtnaleave_tran.isvoid = 0 " & _
                    "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                  "         AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")"
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            If mblnCountTaxableEarning = True Then
                StrQ &= " AND prtranhead_master.istaxable = 1 "
            End If
            'Sohail (21 Feb 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "    GROUP BY prtnaleave_tran.payperiodunkid " & _
                    "    ) AS PERM ON PERM.Pid = cfcommon_period_tran.periodunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "         prtnaleave_tran.payperiodunkid AS OPid " & _
                    "        ,SUM(CAST(ISNULL(amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS OAmount " & _
                    "    FROM prpayrollprocess_tran " & _
                    "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "		 JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
                  "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE 1 = 1 " & _
                    "		 AND cfcommon_master.name NOT LIKE '%Permanent%' " & _
                    "        AND prpayrollprocess_tran.isvoid = 0 " & _
                    "        AND prtnaleave_tran.isvoid = 0 " & _
                    "		 AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                  "         AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")"
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            If mblnCountTaxableEarning = True Then
                StrQ &= " AND prtranhead_master.istaxable = 1 "
            End If
            'Sohail (21 Feb 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "    GROUP BY prtnaleave_tran.payperiodunkid " & _
                    "    ) AS OTHE ON OTHE.OPid = cfcommon_period_tran.periodunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "         prtnaleave_tran.payperiodunkid AS EPId " & _
                    "        ,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS EAmount " & _
                    "     FROM prpayrollprocess_tran " & _
                    "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                  "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                    "        AND prpayrollprocess_tran.isvoid = 0 " & _
                    "        AND prtnaleave_tran.isvoid = 0 " & _
                    "        AND prtranhead_master.isvoid = 0 " & _
                 "         AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")"
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            If mblnCountTaxableEarning = True Then
                StrQ &= " AND prtranhead_master.istaxable = 1 "
            End If
            'Sohail (21 Feb 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "     GROUP BY prtnaleave_tran.payperiodunkid " & _
                    "    ) AS Emol ON Emol.EPId = cfcommon_period_tran.periodunkid "


            'Pinkal (24-Jul-2012) -- End


            If mblnIngnoreZeroSDL = False Then
                StrQ &= "LEFT JOIN "
            Else
                StrQ &= "JOIN "
            End If


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            'StrQ &= "    (SELECT " & _
            '        "         ISNULL(SUM(amount), 0) AS SDLAmount " & _
            '        "        ,prtnaleave_tran.payperiodunkid AS SPid " & _
            '        "     FROM prpayrollprocess_tran " & _
            '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '        "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '        "     WHERE   tranheadunkid = @TranHeadId " & _
            '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
            '        "        AND prtnaleave_tran.isvoid = 0 " & _
            '        "     GROUP BY prtnaleave_tran.payperiodunkid " & _
            '        "    ) AS SDL ON SDL.SPid = cfcommon_period_tran.periodunkid " & _
            '        "WHERE modulerefid = " & enModuleReference.Payroll & " AND yearunkid = " & FinancialYear._Object._YearUnkid & " " & _
            '        " AND cfcommon_period_tran.isactive = 1 " & _
            '        " AND periodunkid IN (" & mstrPeriodIds & ") "

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT - Rounding Issue at TWAWEZA - 6% of Gross Pay is not tallying
            'StrQ &= "    (SELECT " & _
            '        "         SUM(CAST(ISNULL(amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLAmount " & _
            '        "        ,prtnaleave_tran.payperiodunkid AS SPid " & _
            '        "     FROM prpayrollprocess_tran " & _
            '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '     "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "
            StrQ &= "    (SELECT    SUM(ISNULL(amount, 0)) AS SDLAmount " & _
                    "        ,prtnaleave_tran.payperiodunkid AS SPid " & _
                    "     FROM prpayrollprocess_tran " & _
                    "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                 "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid "
            'Sohail (07 Sep 2013) -- End

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "     WHERE   tranheadunkid = @TranHeadId " & _
                    "        AND prpayrollprocess_tran.isvoid = 0 " & _
                    "        AND prtnaleave_tran.isvoid = 0 " & _
                 "        AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")"

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "     GROUP BY prtnaleave_tran.payperiodunkid " & _
                    "    ) AS SDL ON SDL.SPid = cfcommon_period_tran.periodunkid " & _
                    "WHERE modulerefid = " & enModuleReference.Payroll & " AND yearunkid = " & FinancialYear._Object._YearUnkid & " " & _
                 " AND cfcommon_period_tran.isactive = 1 "

            'Pinkal (24-Jul-2012) -- End


            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            StrQ &= " ORDER BY cfcommon_period_tran.start_date "
            'S.SANDEEP [ 05 JULY 2011 ] -- END 



            objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSDLTranHeadId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim dblEmolumentAmount As Decimal = 0
            Dim dblSDLAmount As Decimal = 0


            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            Dim decColumn2Total, decColumn3Total, decColumn4Total, decColumn5Total As Decimal
            decColumn2Total = 0 : decColumn3Total = 0 : decColumn4Total = 0 : decColumn5Total = 0
            'S.SANDEEP [ 05 JULY 2011 ] -- END 



            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("PName")
                rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("PAmount")), GUI.fmtCurrency)
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("OAmount")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("EAmount")), GUI.fmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("SDLAmount")), GUI.fmtCurrency)
                If CDec(rpt_Row.Item("Column5")) > 0 Then
                    dblEmolumentAmount = dblEmolumentAmount + CDec(rpt_Row.Item("Column4"))
                End If

                dblSDLAmount = dblSDLAmount + CDec(rpt_Row.Item("Column5"))
                'S.SANDEEP [ 05 JULY 2011 ] -- START
                'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
                decColumn2Total = decColumn2Total + CDec(dtRow.Item("PAmount"))
                decColumn3Total = decColumn3Total + CDec(dtRow.Item("OAmount"))
                decColumn4Total = decColumn4Total + CDec(dtRow.Item("EAmount"))
                decColumn5Total = decColumn5Total + CDec(dtRow.Item("SDLAmount"))
                'S.SANDEEP [ 05 JULY 2011 ] -- END 
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptSDLHalfYearCertificateReport
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 3, "TANZANIA REVENUE AUTHORITY"))
            Call ReportFunction.TextChange(objRpt, "txtHeading2", Language.getMessage(mstrModuleName, 4, "SKILLS AND DEVELOPMENT LEVY"))
            Call ReportFunction.TextChange(objRpt, "txtHalfYear", Language.getMessage(mstrModuleName, 69, "EMPLOYER’S") & " " & mstrComma_PeriodName & " " & _
                                                                  Language.getMessage(mstrModuleName, 70, "CERTIFICATE"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "YEAR:"))
            Call ReportFunction.TextChange(objRpt, "txtYearData", FinancialYear._Object._FinancialYear_Name)

            Call ReportFunction.TextChange(objRpt, "txtTRA", Language.getMessage(mstrModuleName, 7, "(To be submitted to the TRA office within 30 days after the end of each six-month calendar period)"))

            Call ReportFunction.TextChange(objRpt, "txtEmplrInfo", Language.getMessage(mstrModuleName, 8, "EMPLOYER’S INFORMATION"))
            Call ReportFunction.TextChange(objRpt, "lblTin", Language.getMessage(mstrModuleName, 9, "TIN:"))
            Call ReportFunction.TextChange(objRpt, "txtTinNo", Company._Object._Tinno)

            Call ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 10, "Name of Employer:"))
            Call ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)

            Call ReportFunction.TextChange(objRpt, "txtPostalAddress", Language.getMessage(mstrModuleName, 11, "Postal Address:"))
            Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 12, " P.O. Box"))
            Call ReportFunction.TextChange(objRpt, "txtPOBox", Company._Object._Post_Code_No)

            Call ReportFunction.TextChange(objRpt, "lblPostalCity", Language.getMessage(mstrModuleName, 13, "Postal City"))
            Call ReportFunction.TextChange(objRpt, "txtPostalCity", Company._Object._City_Name)

            Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Language.getMessage(mstrModuleName, 14, "Physical Address:"))

            Call ReportFunction.TextChange(objRpt, "lblPlotNo", Language.getMessage(mstrModuleName, 15, "Plot Number"))
            Call ReportFunction.TextChange(objRpt, "txtPlotNo", "")

            Call ReportFunction.TextChange(objRpt, "lblBlockNo", Language.getMessage(mstrModuleName, 16, "Block Number"))
            Call ReportFunction.TextChange(objRpt, "txtBlockNo", "")

            Call ReportFunction.TextChange(objRpt, "lblStreet", Language.getMessage(mstrModuleName, 17, "Street/Location"))
            Call ReportFunction.TextChange(objRpt, "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)

            Call ReportFunction.TextChange(objRpt, "lblBusinessNature", Language.getMessage(mstrModuleName, 39, "Nature of business:"))
            Call ReportFunction.TextChange(objRpt, "txtBusinessNature", "")

            Call ReportFunction.TextChange(objRpt, "lblEntityOnIndv", Language.getMessage(mstrModuleName, 40, "State whether an Entity or Individual:"))
            Call ReportFunction.TextChange(objRpt, "txtEntityOnIndv", "")

            Call ReportFunction.TextChange(objRpt, "txtSummaryGross", Language.getMessage(mstrModuleName, 41, "SUMMARY OF GROSS EMOLUMENTS AND TAX PAID DURING THE YEAR"))

            Dim objExRate As New clsExchangeRate

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 42, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtPayPerm", Language.getMessage(mstrModuleName, 43, "Payment to Permanent employees/") & objExRate._Currency_Sign.ToString)
            Call ReportFunction.TextChange(objRpt, "txtPayOther", Language.getMessage(mstrModuleName, 44, "Payment to Other employees/") & objExRate._Currency_Sign.ToString)
            Call ReportFunction.TextChange(objRpt, "txtEmoluments", Language.getMessage(mstrModuleName, 45, "Total gross emoluments") & objExRate._Currency_Sign.ToString)
            Call ReportFunction.TextChange(objRpt, "txtSDLAmount", Language.getMessage(mstrModuleName, 46, "Amount of SDL paid") & objExRate._Currency_Sign.ToString)
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 47, "Total"))

            Call ReportFunction.TextChange(objRpt, "txtAppropriateBox", Language.getMessage(mstrModuleName, 48, "The amount of gross emoluments paid during the period from (please tick the appropriate box)"))
            Call ReportFunction.TextChange(objRpt, "txtFirstSixMonths", Language.getMessage(mstrModuleName, 49, "1st January to 30th June"))
            Call ReportFunction.TextChange(objRpt, "txtNextSixMonths", Language.getMessage(mstrModuleName, 50, "1st July to 31st  December"))
            Call ReportFunction.TextChange(objRpt, "txtAddedUpto", Language.getMessage(mstrModuleName, 51, "added up to TZS"))
            Call ReportFunction.TextChange(objRpt, "txtEmolAmount", Format(CDec(dblEmolumentAmount), GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "txtSixPercent", Language.getMessage(mstrModuleName, 52, "and thereof is"))
            Call ReportFunction.TextChange(objRpt, "txtSDLSixPerAmount", Format(CDec(dblSDLAmount), GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "txtDeclaration", Language.getMessage(mstrModuleName, 53, "DECLARATION"))
            Call ReportFunction.TextChange(objRpt, "txtCertified", Language.getMessage(mstrModuleName, 54, "I certify that the particulars entered on the form SDL already submitted monthly for the period indicated above are correct."))
            Call ReportFunction.TextChange(objRpt, "txtNameOfPaying", Language.getMessage(mstrModuleName, 55, "Name of the Employer/Paying Officer"))

            Call ReportFunction.TextChange(objRpt, "txtTitle", Language.getMessage(mstrModuleName, 56, "Title:"))
            Call ReportFunction.TextChange(objRpt, "txtMr", Language.getMessage(mstrModuleName, 57, "Mr."))
            Call ReportFunction.TextChange(objRpt, "txtMrs", Language.getMessage(mstrModuleName, 58, "Mrs."))
            Call ReportFunction.TextChange(objRpt, "txtMs", Language.getMessage(mstrModuleName, 59, "Ms."))

            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 60, "First Name"))
            Call ReportFunction.TextChange(objRpt, "txtMiddleName", Language.getMessage(mstrModuleName, 61, "Middle Name"))
            Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 62, "Surname"))

            Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 63, "Signature and rubber stamp of the Employer/Paying Officer "))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 64, "Date:"))
            Call ReportFunction.TextChange(objRpt, "txtDay", Language.getMessage(mstrModuleName, 65, "Day"))
            Call ReportFunction.TextChange(objRpt, "txtDMonth", Language.getMessage(mstrModuleName, 66, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtDYear", Language.getMessage(mstrModuleName, 67, "Year"))
            Call ReportFunction.TextChange(objRpt, "txtITXHYC", Language.getMessage(mstrModuleName, 68, "ITX 220.01.E - Employer’s Half Year Certificate "))


            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot21")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot31")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot41")
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot51")

            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn2Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn3Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn4Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn5Total, GUI.fmtCurrency))
            'S.SANDEEP [ 05 JULY 2011 ] -- END 



            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsListEmolumentA As New DataSet
    '    Dim dsListEmolumentB As New DataSet
    '    Dim dsSDL As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation


    '        StrQ = "SELECT " & _
    '               "  ISNULL(prtranhead_master.trnheadname,'') AS EmolumentA " & _
    '               " ,ISNULL(SUM(prpayrollprocess_tran.amount),0) AS Amount " & _
    '               "FROM prpayrollprocess_tran " & _
    '               "  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '               "  JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '               "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '               "WHERE prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '               "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '               "  AND prtranhead_master.typeof_id NOT IN (" & enTypeOf.Allowance & ") " & _
    '               "  AND prpayrollprocess_tran.isvoid = 0 " & _
    '               "  AND prtnaleave_tran.isvoid = 0 " & _
    '               "  AND prtranhead_master.isvoid = 0 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        dsListEmolumentA = objDataOperation.ExecQuery(StrQ, "EmolA")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "SELECT " & _
    '               "  ISNULL(prtranhead_master.trnheadname,'') AS EmolumentB " & _
    '               " ,ISNULL(SUM(prpayrollprocess_tran.amount),0) AS Amount " & _
    '               "FROM prpayrollprocess_tran " & _
    '               "  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '               "  JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '               "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '               "WHERE prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '               "  AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '               "  AND prtranhead_master.typeof_id IN (" & enTypeOf.Allowance & ") " & _
    '               "  AND prpayrollprocess_tran.isvoid = 0 " & _
    '               "  AND prtnaleave_tran.isvoid = 0 " & _
    '               "  AND prtranhead_master.isvoid = 0 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "GROUP BY ISNULL(prtranhead_master.trnheadname,'') "

    '        dsListEmolumentB = objDataOperation.ExecQuery(StrQ, "EmolB")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dblSDLAmount As Decimal = 0

    '        StrQ = "SELECT " & _
    '               "   ISNULL(SUM(amount),0) AS Amount " & _
    '               "FROM prpayrollprocess_tran " & _
    '               "   JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '               "  JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '               "WHERE tranheadunkid = @TranHeadId " & _
    '               "   AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '               "   AND prpayrollprocess_tran.isvoid = 0 " & _
    '               "   AND prtnaleave_tran.isvoid = 0 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSDLTranHeadId)

    '        dsSDL = objDataOperation.ExecQuery(StrQ, "SDL")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsSDL.Tables("SDL").Rows.Count > 0 Then
    '            dblSDLAmount = dsSDL.Tables("SDL").Rows(0)("Amount")
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim dblSubTotalA As Decimal = 0
    '        Dim dblSubTotalB As Decimal = 0

    '        Dim intEmolACount As Integer = dsListEmolumentA.Tables("EmolA").Rows.Count
    '        Dim intEmolBCount As Integer = dsListEmolumentB.Tables("EmolB").Rows.Count

    '        If intEmolACount >= intEmolBCount Then
    '            For i As Integer = 0 To intEmolACount - 1
    '                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
    '                rpt_Row.Item("Column1") = dsListEmolumentA.Tables("EmolA").Rows(i)("EmolumentA")
    '                rpt_Row.Item("Column2") = Format(CDec(dsListEmolumentA.Tables("EmolA").Rows(i)("Amount")), GUI.fmtCurrency)
    '                dblSubTotalA = dblSubTotalA + CDec(rpt_Row.Item("Column2"))
    '                For j As Integer = i To i
    '                    If i < intEmolBCount Then
    '                        rpt_Row.Item("Column3") = dsListEmolumentB.Tables("EmolB").Rows(j)("EmolumentB")
    '                        rpt_Row.Item("Column4") = Format(CDec(dsListEmolumentB.Tables("EmolB").Rows(j)("Amount")), GUI.fmtCurrency)
    '                        dblSubTotalB = dblSubTotalB + CDec(rpt_Row.Item("Column4"))
    '                    Else
    '                        rpt_Row.Item("Column3") = ""
    '                        rpt_Row.Item("Column4") = ""
    '                        dblSubTotalB = dblSubTotalB + CDec(0)
    '                    End If
    '                Next
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next
    '        ElseIf intEmolACount < intEmolBCount Then
    '            For i As Integer = 0 To intEmolBCount - 1
    '                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
    '                rpt_Row.Item("Column3") = dsListEmolumentB.Tables("EmolB").Rows(i)("EmolumentB")
    '                rpt_Row.Item("Column4") = Format(CDec(dsListEmolumentB.Tables("EmolB").Rows(i)("Amount")), GUI.fmtCurrency)
    '                dblSubTotalB = dblSubTotalB + CDec(rpt_Row.Item("Column4"))
    '                For j As Integer = i To i
    '                    If i < intEmolACount Then
    '                        rpt_Row.Item("Column1") = dsListEmolumentA.Tables("EmolA").Rows(j)("EmolumentA")
    '                        rpt_Row.Item("Column2") = Format(CDec(dsListEmolumentA.Tables("EmolA").Rows(j)("Amount")), GUI.fmtCurrency)
    '                        dblSubTotalA = dblSubTotalA + CDec(rpt_Row.Item("Column2"))
    '                    Else
    '                        rpt_Row.Item("Column1") = ""
    '                        rpt_Row.Item("Column2") = ""
    '                        dblSubTotalA = dblSubTotalA + CDec(0)
    '                    End If
    '                Next
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next
    '        End If

    '        objRpt = New ArutiReport.Designer.rptSDLMonthlyReport
    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 3, "TANZANIA REVENUE AUTHORITY"))
    '        Call ReportFunction.TextChange(objRpt, "txtHeading2", Language.getMessage(mstrModuleName, 4, "SKILLS AND DEVELOPMENT LEVY"))
    '        Call ReportFunction.TextChange(objRpt, "txtMonthReturn", Language.getMessage(mstrModuleName, 5, "MONTHLY RETURN"))
    '        Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "YEAR:"))
    '        Call ReportFunction.TextChange(objRpt, "txtYearData", FinancialYear._Object._FinancialYear_Name)

    '        Call ReportFunction.TextChange(objRpt, "txtTRA", Language.getMessage(mstrModuleName, 7, "(To be submitted to the TRA office within 30 days after the end of each six-month calendar period)"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmplrInfo", Language.getMessage(mstrModuleName, 8, "EMPLOYER’S INFORMATION"))
    '        Call ReportFunction.TextChange(objRpt, "lblTin", Language.getMessage(mstrModuleName, 9, "TIN:"))
    '        Call ReportFunction.TextChange(objRpt, "txtTinNo", Company._Object._Tinno)

    '        Call ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 10, "Name of Employer:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)

    '        Call ReportFunction.TextChange(objRpt, "txtPostalAddress", Language.getMessage(mstrModuleName, 11, "Postal Address:"))
    '        Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 12, " P.O. Box"))
    '        Call ReportFunction.TextChange(objRpt, "txtPOBox", Company._Object._Post_Code_No)

    '        Call ReportFunction.TextChange(objRpt, "lblPostalCity", Language.getMessage(mstrModuleName, 13, "Postal City"))
    '        Call ReportFunction.TextChange(objRpt, "txtPostalCity", Company._Object._City_Name)

    '        Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Language.getMessage(mstrModuleName, 14, "Physical Address:"))
    '        Call ReportFunction.TextChange(objRpt, "lblPlotNo", Language.getMessage(mstrModuleName, 15, "Plot Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtPlotNo", "")

    '        Call ReportFunction.TextChange(objRpt, "lblBlockNo", Language.getMessage(mstrModuleName, 16, "Block Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtBlockNo", "")

    '        Call ReportFunction.TextChange(objRpt, "lblStreet", Language.getMessage(mstrModuleName, 17, "Street/Location"))
    '        Call ReportFunction.TextChange(objRpt, "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)

    '        Call ReportFunction.TextChange(objRpt, "txtForwardCaption", Language.getMessage(mstrModuleName, 17, "I forward herewith SDL Return for the month of"))
    '        Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)
    '        Call ReportFunction.TextChange(objRpt, "txtMonthYear", Year(FinancialYear._Object._Database_End_Date))

    '        Dim objExRate As New clsExchangeRate
    '        objExRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        Call ReportFunction.TextChange(objRpt, "txtEmolumentA", Language.getMessage(mstrModuleName, 18, "EMOLUMENTS"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmountA", Language.getMessage(mstrModuleName, 19, "AMOUNT") & "/" & objExRate._Currency_Sign)
    '        Call ReportFunction.TextChange(objRpt, "txtEmolumentB", Language.getMessage(mstrModuleName, 18, "EMOLUMENTS"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmountB", Language.getMessage(mstrModuleName, 19, "AMOUNT") & "/" & objExRate._Currency_Sign)

    '        Call ReportFunction.TextChange(objRpt, "lblSubTotalA", Language.getMessage(mstrModuleName, 22, "Subtotal A"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotalA", Format(CDec(dblSubTotalA), GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "lblSubTotalB", Language.getMessage(mstrModuleName, 21, "Subtotal B"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotalB", Format(CDec(dblSubTotalB), GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "lblGrandTotal", Language.getMessage(mstrModuleName, 22, "Grand Total (A+B)"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Format(CDec(dblSubTotalA + dblSubTotalB), GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "lblSDLAmount", Language.getMessage(mstrModuleName, 23, "Whereof SDL at 5% amounts to"))
    '        Call ReportFunction.TextChange(objRpt, "txtSDLAmount", Format(CDec(dblSDLAmount), GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "txtPBB", Language.getMessage(mstrModuleName, 24, "Payment made at the Bank Branch"))
    '        Call ReportFunction.TextChange(objRpt, "txtPSDS", Language.getMessage(mstrModuleName, 25, "Through Payment Slip/Deposit Slip"))
    '        Call ReportFunction.TextChange(objRpt, "txtPSDSDate", Language.getMessage(mstrModuleName, 26, "Date"))

    '        Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 27, "Signature:"))
    '        Call ReportFunction.TextChange(objRpt, "txtSignatureDate", Language.getMessage(mstrModuleName, 26, "Date"))

    '        Call ReportFunction.TextChange(objRpt, "txtStamp", Language.getMessage(mstrModuleName, 28, "Rubber Stamp:"))
    '        Call ReportFunction.TextChange(objRpt, "txtNotes", Language.getMessage(mstrModuleName, 29, "NOTES"))

    '        Call ReportFunction.TextChange(objRpt, "txtNCaption1", Language.getMessage(mstrModuleName, 30, "1.	This Return to be submitted within 7 days after the end of the month to which it refers."))
    '        Call ReportFunction.TextChange(objRpt, "txtNCaption2", Language.getMessage(mstrModuleName, 31, "2.	The Return is in respect of gross emoluments paid by an employer to his employees during a month."))
    '        Call ReportFunction.TextChange(objRpt, "txtNCaption3", Language.getMessage(mstrModuleName, 32, "3.	State in" & objExRate._Currency_Sign & " only rounding up to the next higher unit where applicable."))
    '        Call ReportFunction.TextChange(objRpt, "txtNCaption4", Language.getMessage(mstrModuleName, 33, "4.	Employees includes any person engaged by any one employer for part-time or temporary employment or casual employment which is not regular and lasts for a period of less than one month emoluments for which are calculated on an hourly or daily basis."))
    '        Call ReportFunction.TextChange(objRpt, "txtNCaption5", Language.getMessage(mstrModuleName, 34, "5.	*) If authority to exclude these allowances has been obtained from the Commissioner, please enclose the original copy of such authority."))
    '        Call ReportFunction.TextChange(objRpt, "txtNCaption6", Language.getMessage(mstrModuleName, 35, "6.	Any other allowances includes fuel allowances, hardship allowances, lunch allowances, meal coupons, overtime allowances, responsibility allowances, risk/hazard allowances, transport allowances etc."))
    '        Call ReportFunction.TextChange(objRpt, "txtNCaption7", Language.getMessage(mstrModuleName, 36, "7.	If NO PAYMENT is made, state ""NIL"". Do not leave the space blank or place a dash (-) therein."))

    '        Call ReportFunction.TextChange(objRpt, "txtSDLReturn", Language.getMessage(mstrModuleName, 37, "ITX219.01.E - SDL Monthly Return"))

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Private Function Generate_HalfYear_Certificate() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'StrQ = "SELECT " & _
    '        '        "     periodunkid AS PeriodId " & _
    '        '        "    ,ISNULL(cfcommon_period_tran.period_name, '') AS PName " & _
    '        '        "    ,ISNULL(PAmount, 0) AS PAmount " & _
    '        '        "    ,ISNULL(OAmount,0) AS OAmount " & _
    '        '        "    ,ISNULL(EAmount, 0) AS EAmount " & _
    '        '        "    ,ISNULL(SDLAmount, 0) AS SDLAmount " & _
    '        '        "FROM cfcommon_period_tran " & _
    '        '        "LEFT JOIN " & _
    '        '        "    (SELECT " & _
    '        '        "         prtnaleave_tran.payperiodunkid AS Pid " & _
    '        '        "        ,ISNULL(cfcommon_master.name, '') AS PEmplType " & _
    '        '        "        ,ISNULL(SUM(amount), 0) AS PAmount " & _
    '        '        "    FROM prpayrollprocess_tran " & _
    '        '        "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "        JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = 8 " & _
    '        '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '        '        "    WHERE hremployee_master.isactive = 1 " & _
    '        '        "        AND cfcommon_master.name LIKE '%Permanent%' " & _
    '        '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '        '        "        AND prtnaleave_tran.isvoid = 0 " & _
    '        '        "        AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "    GROUP BY prtnaleave_tran.payperiodunkid,ISNULL(cfcommon_master.name, '') " & _
    '        '        "    ) AS PERM ON PERM.Pid = cfcommon_period_tran.periodunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "    (SELECT " & _
    '        '        "         prtnaleave_tran.payperiodunkid AS OPid " & _
    '        '        "        ,ISNULL(cfcommon_master.name, '') AS OEmplType " & _
    '        '        "        ,ISNULL(SUM(amount), 0) AS OAmount " & _
    '        '        "    FROM prpayrollprocess_tran " & _
    '        '        "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "        JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = 8 " & _
    '        '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '        '        "    WHERE hremployee_master.isactive = 1 " & _
    '        '        "        AND cfcommon_master.name NOT LIKE '%Permanent%' " & _
    '        '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '        '        "        AND prtnaleave_tran.isvoid = 0 " & _
    '        '        "        AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "    GROUP BY prtnaleave_tran.payperiodunkid,ISNULL(cfcommon_master.name, '') " & _
    '        '        "    ) AS OTHE ON OTHE.OPid = cfcommon_period_tran.periodunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "    (SELECT " & _
    '        '        "         prtnaleave_tran.payperiodunkid AS EPId " & _
    '        '        "        ,ISNULL(SUM(prpayrollprocess_tran.amount), 0) AS EAmount " & _
    '        '        "     FROM prpayrollprocess_tran " & _
    '        '        "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '        '        "     WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '        '        "        AND prtnaleave_tran.isvoid = 0 " & _
    '        '        "        AND prtranhead_master.isvoid = 0 " & _
    '        '        "     GROUP BY prtnaleave_tran.payperiodunkid " & _
    '        '        "    ) AS Emol ON Emol.EPId = cfcommon_period_tran.periodunkid "

    '        StrQ = "SELECT " & _
    '                "     periodunkid AS PeriodId " & _
    '                "    ,ISNULL(cfcommon_period_tran.period_name, '') AS PName " & _
    '                "    ,ISNULL(PAmount, 0) AS PAmount " & _
    '                "    ,ISNULL(OAmount,0) AS OAmount " & _
    '                "    ,ISNULL(EAmount, 0) AS EAmount " & _
    '                "    ,ISNULL(SDLAmount, 0) AS SDLAmount " & _
    '                "FROM cfcommon_period_tran " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "         prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "        ,ISNULL(cfcommon_master.name, '') AS PEmplType " & _
    '                "        ,ISNULL(SUM(amount), 0) AS PAmount " & _
    '                "    FROM prpayrollprocess_tran " & _
    '                "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "		JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
    '                "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                "	WHERE 1 = 1  "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "   AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND cfcommon_master.name LIKE '%Permanent%' " & _
    '                "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '                "        AND prtnaleave_tran.isvoid = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '                "    GROUP BY prtnaleave_tran.payperiodunkid,ISNULL(cfcommon_master.name, '') " & _
    '                "    ) AS PERM ON PERM.Pid = cfcommon_period_tran.periodunkid " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "         prtnaleave_tran.payperiodunkid AS OPid " & _
    '                "        ,ISNULL(cfcommon_master.name, '') AS OEmplType " & _
    '                "        ,ISNULL(SUM(amount), 0) AS OAmount " & _
    '                "    FROM prpayrollprocess_tran " & _
    '                "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "		JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
    '                "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                "	WHERE 1 = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "   AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND cfcommon_master.name NOT LIKE '%Permanent%' " & _
    '                "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '                "        AND prtnaleave_tran.isvoid = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '                "    GROUP BY prtnaleave_tran.payperiodunkid,ISNULL(cfcommon_master.name, '') " & _
    '                "    ) AS OTHE ON OTHE.OPid = cfcommon_period_tran.periodunkid " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "         prtnaleave_tran.payperiodunkid AS EPId " & _
    '                "        ,ISNULL(SUM(prpayrollprocess_tran.amount), 0) AS EAmount " & _
    '                "     FROM prpayrollprocess_tran " & _
    '                "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '                "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '                "        AND prtnaleave_tran.isvoid = 0 " & _
    '                "        AND prtranhead_master.isvoid = 0 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "     GROUP BY prtnaleave_tran.payperiodunkid " & _
    '                "    ) AS Emol ON Emol.EPId = cfcommon_period_tran.periodunkid "

    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 


    '        If mblnIngnoreZeroSDL = False Then
    '            StrQ &= "LEFT JOIN "
    '        Else
    '            StrQ &= "JOIN "
    '        End If
    '        StrQ &= "    (SELECT " & _
    '                "         ISNULL(SUM(amount), 0) AS SDLAmount " & _
    '                "        ,prtnaleave_tran.payperiodunkid AS SPid " & _
    '                "     FROM prpayrollprocess_tran " & _
    '                "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                "        JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "     WHERE   tranheadunkid = @TranHeadId " & _
    '                "        AND prpayrollprocess_tran.isvoid = 0 " & _
    '                "        AND prtnaleave_tran.isvoid = 0 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "     GROUP BY prtnaleave_tran.payperiodunkid " & _
    '                "    ) AS SDL ON SDL.SPid = cfcommon_period_tran.periodunkid " & _
    '                "WHERE modulerefid = " & enModuleReference.Payroll & " AND yearunkid = " & FinancialYear._Object._YearUnkid & " " & _
    '                " AND cfcommon_period_tran.isactive = 1 " & _
    '                " AND periodunkid IN (" & mstrPeriodIds & ") "


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        StrQ &= " ORDER BY cfcommon_period_tran.start_date "
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 



    '        objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSDLTranHeadId)

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim dblEmolumentAmount As Decimal = 0
    '        Dim dblSDLAmount As Decimal = 0


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn2Total, decColumn3Total, decColumn4Total, decColumn5Total As Decimal
    '        decColumn2Total = 0 : decColumn3Total = 0 : decColumn4Total = 0 : decColumn5Total = 0
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 



    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("PName")
    '            rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("PAmount")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("OAmount")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("EAmount")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("SDLAmount")), GUI.fmtCurrency)
    '            If CDec(rpt_Row.Item("Column5")) > 0 Then
    '                dblEmolumentAmount = dblEmolumentAmount + CDec(rpt_Row.Item("Column4"))
    '            End If

    '            dblSDLAmount = dblSDLAmount + CDec(rpt_Row.Item("Column5"))
    '            'S.SANDEEP [ 05 JULY 2011 ] -- START
    '            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '            decColumn2Total = decColumn2Total + CDec(dtRow.Item("PAmount"))
    '            decColumn3Total = decColumn3Total + CDec(dtRow.Item("OAmount"))
    '            decColumn4Total = decColumn4Total + CDec(dtRow.Item("EAmount"))
    '            decColumn5Total = decColumn5Total + CDec(dtRow.Item("SDLAmount"))
    '            'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next


    '        objRpt = New ArutiReport.Designer.rptSDLHalfYearCertificateReport
    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 3, "TANZANIA REVENUE AUTHORITY"))
    '        Call ReportFunction.TextChange(objRpt, "txtHeading2", Language.getMessage(mstrModuleName, 4, "SKILLS AND DEVELOPMENT LEVY"))
    '        Call ReportFunction.TextChange(objRpt, "txtHalfYear", Language.getMessage(mstrModuleName, 69, "EMPLOYER’S") & " " & mstrComma_PeriodName & " " & _
    '                                                              Language.getMessage(mstrModuleName, 70, "CERTIFICATE"))
    '        Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "YEAR:"))
    '        Call ReportFunction.TextChange(objRpt, "txtYearData", FinancialYear._Object._FinancialYear_Name)

    '        Call ReportFunction.TextChange(objRpt, "txtTRA", Language.getMessage(mstrModuleName, 7, "(To be submitted to the TRA office within 30 days after the end of each six-month calendar period)"))

    '        Call ReportFunction.TextChange(objRpt, "txtEmplrInfo", Language.getMessage(mstrModuleName, 8, "EMPLOYER’S INFORMATION"))
    '        Call ReportFunction.TextChange(objRpt, "lblTin", Language.getMessage(mstrModuleName, 9, "TIN:"))
    '        Call ReportFunction.TextChange(objRpt, "txtTinNo", Company._Object._Tinno)

    '        Call ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 10, "Name of Employer:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)

    '        Call ReportFunction.TextChange(objRpt, "txtPostalAddress", Language.getMessage(mstrModuleName, 11, "Postal Address:"))
    '        Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 12, " P.O. Box"))
    '        Call ReportFunction.TextChange(objRpt, "txtPOBox", Company._Object._Post_Code_No)

    '        Call ReportFunction.TextChange(objRpt, "lblPostalCity", Language.getMessage(mstrModuleName, 13, "Postal City"))
    '        Call ReportFunction.TextChange(objRpt, "txtPostalCity", Company._Object._City_Name)

    '        Call ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Language.getMessage(mstrModuleName, 14, "Physical Address:"))

    '        Call ReportFunction.TextChange(objRpt, "lblPlotNo", Language.getMessage(mstrModuleName, 15, "Plot Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtPlotNo", "")

    '        Call ReportFunction.TextChange(objRpt, "lblBlockNo", Language.getMessage(mstrModuleName, 16, "Block Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtBlockNo", "")

    '        Call ReportFunction.TextChange(objRpt, "lblStreet", Language.getMessage(mstrModuleName, 17, "Street/Location"))
    '        Call ReportFunction.TextChange(objRpt, "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)

    '        Call ReportFunction.TextChange(objRpt, "lblBusinessNature", Language.getMessage(mstrModuleName, 39, "Nature of business:"))
    '        Call ReportFunction.TextChange(objRpt, "txtBusinessNature", "")

    '        Call ReportFunction.TextChange(objRpt, "lblEntityOnIndv", Language.getMessage(mstrModuleName, 40, "State whether an Entity or Individual:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEntityOnIndv", "")

    '        Call ReportFunction.TextChange(objRpt, "txtSummaryGross", Language.getMessage(mstrModuleName, 41, "SUMMARY OF GROSS EMOLUMENTS AND TAX PAID DURING THE YEAR"))

    '        Dim objExRate As New clsExchangeRate
    '        objExRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 42, "Month"))
    '        Call ReportFunction.TextChange(objRpt, "txtPayPerm", Language.getMessage(mstrModuleName, 43, "Payment to Permanent employees/") & objExRate._Currency_Sign.ToString)
    '        Call ReportFunction.TextChange(objRpt, "txtPayOther", Language.getMessage(mstrModuleName, 44, "Payment to Other employees/") & objExRate._Currency_Sign.ToString)
    '        Call ReportFunction.TextChange(objRpt, "txtEmoluments", Language.getMessage(mstrModuleName, 45, "Total gross emoluments") & objExRate._Currency_Sign.ToString)
    '        Call ReportFunction.TextChange(objRpt, "txtSDLAmount", Language.getMessage(mstrModuleName, 46, "Amount of SDL paid") & objExRate._Currency_Sign.ToString)
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 47, "Total"))

    '        Call ReportFunction.TextChange(objRpt, "txtAppropriateBox", Language.getMessage(mstrModuleName, 48, "The amount of gross emoluments paid during the period from (please tick the appropriate box)"))
    '        Call ReportFunction.TextChange(objRpt, "txtFirstSixMonths", Language.getMessage(mstrModuleName, 49, "1st January to 30th June"))
    '        Call ReportFunction.TextChange(objRpt, "txtNextSixMonths", Language.getMessage(mstrModuleName, 50, "1st July to 31st  December"))
    '        Call ReportFunction.TextChange(objRpt, "txtAddedUpto", Language.getMessage(mstrModuleName, 51, "added up to TZS"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmolAmount", Format(CDec(dblEmolumentAmount), GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "txtSixPercent", Language.getMessage(mstrModuleName, 52, "and 5% thereof is"))
    '        Call ReportFunction.TextChange(objRpt, "txtSDLSixPerAmount", Format(CDec(dblSDLAmount), GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "txtDeclaration", Language.getMessage(mstrModuleName, 53, "DECLARATION"))
    '        Call ReportFunction.TextChange(objRpt, "txtCertified", Language.getMessage(mstrModuleName, 54, "I certify that the particulars entered on the form SDL already submitted monthly for the period indicated above are correct."))
    '        Call ReportFunction.TextChange(objRpt, "txtNameOfPaying", Language.getMessage(mstrModuleName, 55, "Name of the Employer/Paying Officer"))

    '        Call ReportFunction.TextChange(objRpt, "txtTitle", Language.getMessage(mstrModuleName, 56, "Title:"))
    '        Call ReportFunction.TextChange(objRpt, "txtMr", Language.getMessage(mstrModuleName, 57, "Mr."))
    '        Call ReportFunction.TextChange(objRpt, "txtMrs", Language.getMessage(mstrModuleName, 58, "Mrs."))
    '        Call ReportFunction.TextChange(objRpt, "txtMs", Language.getMessage(mstrModuleName, 59, "Ms."))

    '        Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 60, "First Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtMiddleName", Language.getMessage(mstrModuleName, 61, "Middle Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 62, "Surname"))

    '        Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 63, "Signature and rubber stamp of the Employer/Paying Officer "))
    '        Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 64, "Date:"))
    '        Call ReportFunction.TextChange(objRpt, "txtDay", Language.getMessage(mstrModuleName, 65, "Day"))
    '        Call ReportFunction.TextChange(objRpt, "txtDMonth", Language.getMessage(mstrModuleName, 66, "Month"))
    '        Call ReportFunction.TextChange(objRpt, "txtDYear", Language.getMessage(mstrModuleName, 67, "Year"))
    '        Call ReportFunction.TextChange(objRpt, "txtITXHYC", Language.getMessage(mstrModuleName, 68, "ITX 220.01.E - Employer’s Half Year Certificate "))


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot21")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot31")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot41")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot51")

    '        Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn2Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn3Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn4Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn5Total, GUI.fmtCurrency))
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 



    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Month Report")
            Language.setMessage(mstrModuleName, 2, "Half Year Certificate Report")
            Language.setMessage(mstrModuleName, 3, "TANZANIA REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 4, "SKILLS AND DEVELOPMENT LEVY")
            Language.setMessage(mstrModuleName, 5, "MONTHLY RETURN")
            Language.setMessage(mstrModuleName, 6, "YEAR:")
            Language.setMessage(mstrModuleName, 7, "(To be submitted to the TRA office within 30 days after the end of each six-month calendar period)")
            Language.setMessage(mstrModuleName, 8, "EMPLOYER’S INFORMATION")
            Language.setMessage(mstrModuleName, 9, "TIN:")
            Language.setMessage(mstrModuleName, 10, "Name of Employer:")
            Language.setMessage(mstrModuleName, 11, "Postal Address:")
            Language.setMessage(mstrModuleName, 12, " P.O. Box")
            Language.setMessage(mstrModuleName, 13, "Postal City")
            Language.setMessage(mstrModuleName, 14, "Physical Address:")
            Language.setMessage(mstrModuleName, 15, "Plot Number")
            Language.setMessage(mstrModuleName, 16, "Block Number")
            Language.setMessage(mstrModuleName, 17, "Street/Location")
            Language.setMessage(mstrModuleName, 18, "EMOLUMENTS")
            Language.setMessage(mstrModuleName, 19, "AMOUNT")
            Language.setMessage(mstrModuleName, 21, "Subtotal B")
            Language.setMessage(mstrModuleName, 22, "Subtotal A")
            Language.setMessage(mstrModuleName, 23, "Whereof SDL at 5% amounts to")
            Language.setMessage(mstrModuleName, 24, "Payment made at the Bank Branch")
            Language.setMessage(mstrModuleName, 25, "Through Payment Slip/Deposit Slip")
            Language.setMessage(mstrModuleName, 26, "Date")
            Language.setMessage(mstrModuleName, 27, "Signature:")
            Language.setMessage(mstrModuleName, 28, "Rubber Stamp:")
            Language.setMessage(mstrModuleName, 29, "NOTES")
            Language.setMessage(mstrModuleName, 30, "1.	This Return to be submitted within 7 days after the end of the month to which it refers.")
            Language.setMessage(mstrModuleName, 31, "2.	The Return is in respect of gross emoluments paid by an employer to his employees during a month.")
            Language.setMessage(mstrModuleName, 32, "3.	State in")
            Language.setMessage(mstrModuleName, 33, "4.	Employees includes any person engaged by any one employer for part-time or temporary employment or casual employment which is not regular and lasts for a period of less than one month emoluments for which are calculated on an hourly or daily basis.")
            Language.setMessage(mstrModuleName, 34, "5.	*) If authority to exclude these allowances has been obtained from the Commissioner, please enclose the original copy of such authority.")
            Language.setMessage(mstrModuleName, 35, "6.	Any other allowances includes fuel allowances, hardship allowances, lunch allowances, meal coupons, overtime allowances, responsibility allowances, risk/hazard allowances, transport allowances etc.")
            Language.setMessage(mstrModuleName, 36, "7.	If NO PAYMENT is made, state ""NIL"". Do not leave the space blank or place a dash (-) therein.")
            Language.setMessage(mstrModuleName, 37, "ITX219.01.E - SDL Monthly Return")
            Language.setMessage(mstrModuleName, 39, "Nature of business:")
            Language.setMessage(mstrModuleName, 40, "State whether an Entity or Individual:")
            Language.setMessage(mstrModuleName, 41, "SUMMARY OF GROSS EMOLUMENTS AND TAX PAID DURING THE YEAR")
            Language.setMessage(mstrModuleName, 42, "Month")
            Language.setMessage(mstrModuleName, 43, "Payment to Permanent employees/")
            Language.setMessage(mstrModuleName, 44, "Payment to Other employees/")
            Language.setMessage(mstrModuleName, 45, "Total gross emoluments")
            Language.setMessage(mstrModuleName, 46, "Amount of SDL paid")
            Language.setMessage(mstrModuleName, 47, "Total")
            Language.setMessage(mstrModuleName, 48, "The amount of gross emoluments paid during the period from (please tick the appropriate box)")
            Language.setMessage(mstrModuleName, 49, "1st January to 30th June")
            Language.setMessage(mstrModuleName, 50, "1st July to 31st  December")
            Language.setMessage(mstrModuleName, 51, "added up to TZS")
            Language.setMessage(mstrModuleName, 52, "and thereof is")
            Language.setMessage(mstrModuleName, 53, "DECLARATION")
            Language.setMessage(mstrModuleName, 54, "I certify that the particulars entered on the form SDL already submitted monthly for the period indicated above are correct.")
            Language.setMessage(mstrModuleName, 55, "Name of the Employer/Paying Officer")
            Language.setMessage(mstrModuleName, 56, "Title:")
            Language.setMessage(mstrModuleName, 57, "Mr.")
            Language.setMessage(mstrModuleName, 58, "Mrs.")
            Language.setMessage(mstrModuleName, 59, "Ms.")
            Language.setMessage(mstrModuleName, 60, "First Name")
            Language.setMessage(mstrModuleName, 61, "Middle Name")
            Language.setMessage(mstrModuleName, 62, "Surname")
            Language.setMessage(mstrModuleName, 63, "Signature and rubber stamp of the Employer/Paying Officer")
            Language.setMessage(mstrModuleName, 64, "Date:")
            Language.setMessage(mstrModuleName, 65, "Day")
            Language.setMessage(mstrModuleName, 66, "Month")
            Language.setMessage(mstrModuleName, 67, "Year")
            Language.setMessage(mstrModuleName, 68, "ITX 220.01.E - Employer’s Half Year Certificate")
            Language.setMessage(mstrModuleName, 69, "EMPLOYER’S")
            Language.setMessage(mstrModuleName, 70, "CERTIFICATE")
            Language.setMessage(mstrModuleName, 71, " only rounding up to the next higher unit where applicable.")
            Language.setMessage(mstrModuleName, 72, "I forward herewith SDL Return for the month of")
            Language.setMessage(mstrModuleName, 73, "Grand Total (A+B)")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
