Option Strict On

'************************************************************************************************************************************
'Class Name : frmSDLReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSDLReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSDLReport"
    Private objSDLReport As clsSDLReport

    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (24-Jul-2012) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objSDLReport = New clsSDLReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objSDLReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objSDLHead As New clsTransactionHead
        Dim dsCombo As New DataSet
        Try
            'Sohail (30 Sep 2017) -- Start
            'Enhancement - 70.1 - Allow multiple period selection on month SDL  [Anatory Rutta] - (RefNo: 83).
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            ''dsCombo = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            'dsCombo = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            ''Sohail (21 Aug 2015) -- End
            'With cboPeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("List")
            'End With
            'Sohail (30 Sep 2017) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objSDLHead.getComboList("List", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            dsCombo = objSDLHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'Sohail (21 Aug 2015) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objSDLReport.Get_ReportType("List")
            With cboReportType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", False)
            dsCombo = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False)
            'Sohail (21 Aug 2015) -- End
            lvPeriodList.Items.Clear()
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dsCombo.Tables("List").Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.SubItems.Add(dtRow.Item("name").ToString)
                    lvItem.Tag = dtRow.Item("periodunkid")

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    lvItem.SubItems.Add(dtRow.Item("start_Date").ToString)
                    lvItem.SubItems(objcolhPeriodStart.Index).Tag = dtRow.Item("end_Date").ToString
                    'Sohail (03 Aug 2019) -- End

                    lvPeriodList.Items.Add(lvItem)

                    lvItem = Nothing
                Next
                If lvPeriodList.Items.Count > 5 Then
                    colhPeriodName.Width = colhPeriodName.Width - 20
                Else
                    colhPeriodName.Width = colhPeriodName.Width
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'cboPeriod.SelectedValue = 0 'Sohail (30 Sep 2017)
            cboReportType.SelectedValue = 0
            cboTranHead.SelectedValue = 0
            objCheckAll.CheckState = CheckState.Unchecked

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            'Pinkal (24-Jul-2012) -- End
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            rdbAllEarning.Checked = False
            rdbTaxablelEarning.Checked = True
            'Sohail (21 Feb 2017) -- End
            Call DoOperation(False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSDLReport.SetDefaultValue()
            Select Case CInt(cboReportType.SelectedValue)
                'Sohail (30 Sep 2017) -- Start
                'Enhancement - 70.1 - Allow multiple period selection on month SDL  [Anatory Rutta] - (RefNo: 83).
                'Case 0  'Monthly                    
                '    If CInt(cboPeriod.SelectedValue) <= 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select period to continue."), enMsgBoxStyle.Information)
                '        cboPeriod.Focus()
                '        Return False
                '    End If
                'Case 1  'Half Year
                Case 0, 1
                    'Sohail (30 Sep 2017) -- End
                    If lvPeriodList.CheckedItems.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one period to view report."), enMsgBoxStyle.Information)
                        lvPeriodList.Focus()
                        Return False
                    End If

                    Dim StrPeriodIds As String = String.Empty
                    Dim StrPeriodName As String = String.Empty

                    For Each LvItem As ListViewItem In lvPeriodList.CheckedItems
                        StrPeriodIds &= "," & LvItem.Tag.ToString
                        StrPeriodName &= "," & LvItem.SubItems(colhPeriodName.Index).Text.ToString
                    Next


                    StrPeriodName = Mid(StrPeriodName, 2)
                    If StrPeriodName.Contains("(") = False Then
                        StrPeriodName = StrPeriodName.Insert(0, "(")
                    End If
                    If StrPeriodName.Contains(")") = False Then
                        StrPeriodName = StrPeriodName.Insert(StrPeriodName.Length, ")")
                    End If
                    StrPeriodIds = Mid(StrPeriodIds, 2)

                    objSDLReport._Period_Ids = StrPeriodIds
                    objSDLReport._Comma_PeriodName = StrPeriodName
                    objSDLReport._IngnoreZeroSDL = CBool(chkIngnoreZeroSDL.CheckState)
            End Select
            

            If CInt(cboTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Transaction Head to continue."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Return False
            End If

            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            If rdbAllEarning.Checked = False AndAlso rdbTaxablelEarning.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Count Earning option."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (21 Feb 2017) -- End

            objSDLReport._Report_TypeId = CInt(cboReportType.SelectedValue)
            objSDLReport._Report_TypeName = cboReportType.Text

            objSDLReport._SDLTranHeadId = CInt(cboTranHead.SelectedValue)
            objSDLReport._SDLTranHeadName = cboTranHead.Text

            'Sohail (30 Sep 2017) -- Start
            'Enhancement - 70.1 - Allow multiple period selection on month SDL  [Anatory Rutta] - (RefNo: 83).
            'objSDLReport._PeriodId = CInt(cboPeriod.SelectedValue)
            'objSDLReport._PeriodName = cboPeriod.Text
            'Sohail (30 Sep 2017) -- End

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            objSDLReport._ViewByIds = mstrStringIds
            objSDLReport._ViewIndex = mintViewIdx
            objSDLReport._ViewByName = mstrStringName
            objSDLReport._Analysis_Fields = mstrAnalysis_Fields
            objSDLReport._Analysis_Join = mstrAnalysis_Join
            objSDLReport._Report_GroupName = mstrReport_GroupName

            'Pinkal (24-Jul-2012) -- End
            'Sohail (21 Feb 2017) -- Start
            'Enhancement - 64.1 - Give option to count  All Earning or Taxable Earning on SDL Report.
            objSDLReport._AllEarning = rdbAllEarning.Checked
            objSDLReport._TaxableEarning = rdbTaxablelEarning.Checked
            'Sohail (21 Feb 2017) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub DoOperation(ByVal blnOpreration As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriodList.Items
                lvItem.Checked = blnOpreration
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmSDLReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSDLReport = Nothing
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmSDLReport", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSDLReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objSDLReport._ReportName
            Me._Message = objSDLReport._ReportDesc

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END


            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSDLReport", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objSDLReport.generateReport(CInt(cboReportType.SelectedValue), CType(e.Type, enPrintAction), enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objSDLReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedValue), CType(e.Type, enPrintAction), enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objSDLReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max)

            objSDLReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                           eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedValue), CType(e.Type, enPrintAction), enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objSDLReport.generateReport(CInt(cboReportType.SelectedValue), enPrintAction.None, CType(e.Type, enExportAction))
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objSDLReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedValue), enPrintAction.None, CType(e.Type, enExportAction), ConfigParameter._Object._Base_CurrencyId)
            objSDLReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max)

            objSDLReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                           eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedValue), enPrintAction.None, CType(e.Type, enExportAction), ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSDLReport.SetMessages()
            objfrm._Other_ModuleNames = "clsSDLReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(CStr(-1), ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END


#End Region

#Region " Controls Events "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            'Pinkal (24-Jul-2012) -- End

            Select Case CInt(cboReportType.SelectedValue)
                'Sohail (30 Sep 2017) -- Start
                'Enhancement - 70.1 - Allow multiple period selection on month SDL  [Anatory Rutta] - (RefNo: 83).
                'Case 0  'Monthly
                '    lvPeriodList.Enabled = False
                '    objCheckAll.CheckState = CheckState.Unchecked
                '    cboPeriod.SelectedValue = 0
                '    objCheckAll.Enabled = False
                '    cboPeriod.Enabled = True
                '    chkIngnoreZeroSDL.CheckState = CheckState.Unchecked
                '    chkIngnoreZeroSDL.Enabled = False
                'Case 1  'Half Yearly
                'cboPeriod.SelectedValue = 0
                'cboPeriod.Enabled = False
                Case 0, 1
                    'Sohail (30 Sep 2017) -- End                    
                    lvPeriodList.Enabled = True
                    objCheckAll.Enabled = True
                    chkIngnoreZeroSDL.CheckState = CheckState.Unchecked
                    chkIngnoreZeroSDL.Enabled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objCheckAll.CheckedChanged
        Try
            Call DoOperation(CBool(objCheckAll.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriodList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriodList.ItemChecked
        Try
            RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
            If lvPeriodList.CheckedItems.Count <= 0 Then
                objCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvPeriodList.CheckedItems.Count < lvPeriodList.Items.Count Then
                objCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvPeriodList.CheckedItems.Count = lvPeriodList.Items.Count Then
                objCheckAll.CheckState = CheckState.Checked
            End If
            AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriodList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    'Pinkal (24-Jul-2012) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblEmployerContribution.Text = Language._Object.getCaption(Me.lblEmployerContribution.Name, Me.lblEmployerContribution.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lblPeriodList.Text = Language._Object.getCaption(Me.lblPeriodList.Name, Me.lblPeriodList.Text)
			Me.chkIngnoreZeroSDL.Text = Language._Object.getCaption(Me.chkIngnoreZeroSDL.Name, Me.chkIngnoreZeroSDL.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.rdbTaxablelEarning.Text = Language._Object.getCaption(Me.rdbTaxablelEarning.Name, Me.rdbTaxablelEarning.Text)
			Me.rdbAllEarning.Text = Language._Object.getCaption(Me.rdbAllEarning.Name, Me.rdbAllEarning.Text)
			Me.lblCountEarning.Text = Language._Object.getCaption(Me.lblCountEarning.Name, Me.lblCountEarning.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select period to continue.")
			Language.setMessage(mstrModuleName, 2, "Please select Transaction Head to continue.")
			Language.setMessage(mstrModuleName, 3, "Please select atleast one period to view report.")
			Language.setMessage(mstrModuleName, 4, "Please select Count Earning option.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
