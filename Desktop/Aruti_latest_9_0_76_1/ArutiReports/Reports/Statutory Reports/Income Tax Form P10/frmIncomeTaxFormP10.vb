'************************************************************************************************************************************
'Class Name : frmIncomeTaxFormP10.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmIncomeTaxFormP10

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmIncomeTaxFormP10"
    Private objIncomeTaxP10 As clsIncomeTaxFormP10
    Private mstrEndPeriodName As String = String.Empty
    Private mdtEndPeriodDate As DateTime 'Sohail (21 Nov 2011)

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    'Sohail (18 Mar 2013) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objIncomeTaxP10 = New clsIncomeTaxFormP10(User._Object._Languageunkid,Company._Object._Companyunkid)
        objIncomeTaxP10.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        'Dim objExcessSlan As New clsTranheadInexcessslabTran 'Sohail (21 Nov 2011) 'Sohail (04 Dec 2014)
        Dim dsCombos As New DataSet
        Try
            'Sohail (21 Nov 2011) -- Start
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            'lvPeriod.Items.Clear()
            'Dim lvItem As ListViewItem
            'For Each dtRow As DataRow In dsCombos.Tables(0).Rows
            '    lvItem = New ListViewItem

            '    lvItem.Text = ""
            '    lvItem.SubItems.Add(dtRow.Item("name").ToString)
            '    lvItem.Tag = dtRow.Item("periodunkid")
            '    lvPeriod.Items.Add(lvItem)
            '    lvItem = Nothing
            'Next
            'lvPeriod.GridLines = False
            'Sohail (21 Nov 2011) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Housing", True, enTranHeadType.EarningForEmployees)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Put all earnings head except salary on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Housing", True, enTranHeadType.EarningForEmployees)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Housing", True, enTranHeadType.EarningForEmployees, , , , , "typeof_id NOT IN ( " & enTypeOf.Salary & ")")
            'Sohail (21 Aug 2015) -- End
            'Dim dtTable As DataTable = Nothing
            'Dim strTypeOfIds As String = enTypeOf.Salary & "," & enTypeOf.Allowance & "," & enTypeOf.BENEFIT
            'dtTable = New DataView(dsCombos.Tables(0), "typeof_id NOT IN ( " & strTypeOfIds & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboHousing
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            'Sohail (21 Aug 2015) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Nov 2011) -- Start
            'Sohail (04 Dec 2014) -- Start
            'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
            'dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
            'With cboSlabEffectivePeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Slab")
            '    .SelectedValue = 0
            'End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("inexcess", False, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "inexcess", False, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            'Sohail (21 Aug 2015) -- End
            With cboEffectSlabHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("inexcess")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
            'Sohail (04 Dec 2014) -- End
            'Sohail (21 Nov 2011) -- End

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With
            'Sohail (07 Sep 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (04 Dec 2014) -- Start
            'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
        Finally
            objperiod = Nothing
            objTranHead = Nothing
            'Sohail (04 Dec 2014) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboHousing.SelectedValue = 0
            cboTranHead.SelectedValue = 0
            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            cboOtherEarning.SelectedValue = 0
            chkIncludeNonTaxableEarningInGrossPay.Checked = False
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport
            'Sohail (07 Sep 2013) -- End

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
            'Sohail (18 Mar 2013) -- End

            Call DoOperation(False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub DoOperation(ByVal blnOperation As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriod.Items
                lvItem.Checked = blnOperation
            Next
            lvPeriod.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub GetCommaSeparatedTags(ByRef mstrIds As String)
        Dim intCnt As Integer = 1
        Try
            For Each lvItem As ListViewItem In lvPeriod.CheckedItems
                mstrIds &= "," & lvItem.Tag.ToString
                If intCnt = lvPeriod.CheckedItems.Count Then
                    mstrEndPeriodName = lvItem.SubItems(colhPeriodName.Index).Text
                    mdtEndPeriodDate = eZeeDate.convertDate(lvItem.SubItems(colhPeriodName.Index).Tag.ToString) 'Sohail (21 Nov 2011)
                End If
                intCnt += 1
            Next
            mstrIds = Mid(mstrIds, 2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommaSeparatedTags", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objIncomeTaxP10.SetDefaultValue()

            If cboHousing.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Housing Head is compulsory information.Please select Housing Head to continue."), enMsgBoxStyle.Information)
                cboHousing.Focus()
                Return False
            End If

            If cboTranHead.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Slab Tax Head is compulsory information.Please select Slab Tax Head to continue."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Return False
            End If

            If lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one Period to view report."), enMsgBoxStyle.Information)
                lvPeriod.Focus()
                Return False
            End If

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If
            'Sohail (07 Sep 2013) -- End

            objIncomeTaxP10._HousingTranId = cboHousing.SelectedValue
            objIncomeTaxP10._SlabTranId = cboTranHead.SelectedValue

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True Then
                objIncomeTaxP10._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objIncomeTaxP10._OtherEarningTranId = 0
            End If
            objIncomeTaxP10._IncludeNonTaxableEarningInGrossPay = chkIncludeNonTaxableEarningInGrossPay.Checked
            'Sohail (07 Sep 2013) -- End

            Dim strPreriodIds As String = ""
            Call GetCommaSeparatedTags(strPreriodIds)
            objIncomeTaxP10._PeriodIds = strPreriodIds
            objIncomeTaxP10._EndPeriodName = mstrEndPeriodName
            objIncomeTaxP10._EndPeriodDate = mdtEndPeriodDate 'Sohail (21 Nov 2011)

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIncomeTaxP10._PeriodCount = lvPeriod.CheckedItems.Count
            'S.SANDEEP [ 04 JULY 2012 ] -- END

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            objIncomeTaxP10._ViewByIds = mstrStringIds
            objIncomeTaxP10._ViewIndex = mintViewIdx
            objIncomeTaxP10._ViewByName = mstrStringName
            objIncomeTaxP10._Analysis_Fields = mstrAnalysis_Fields
            objIncomeTaxP10._Analysis_Join = mstrAnalysis_Join
            objIncomeTaxP10._Report_GroupName = mstrReport_GroupName
            objIncomeTaxP10._Analysis_OrderBy = mstrAnalysis_OrderBy_GName
            'Sohail (18 Mar 2013) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objIncomeTaxP10._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtEndPeriodDate))
            'Sohail (03 Aug 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmIncomeTaxFormP10_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objIncomeTaxP10 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIncomeTaxFormP10_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIncomeTaxFormP10_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END


            Me._Title = objIncomeTaxP10._ReportName
            Me._Message = objIncomeTaxP10._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIncomeTaxFormP10_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIncomeTaxP10.generateReport(0, e.Type, enExportAction.None)
            objIncomeTaxP10.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIncomeTaxP10.generateReport(0, enPrintAction.None, e.Type)
            objIncomeTaxP10.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsIncomeTaxFormP10.SetMessages()
            objfrm._Other_ModuleNames = "clsIncomeTaxFormP10"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call DoOperation(CBool(objchkAllPeriod.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Nov 2011) -- Start
    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End


                'Sohail (04 Dec 2014) -- Start
                'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
                'Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
                Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboEffectSlabHead.SelectedValue))
                'Sohail (04 Dec 2014) -- End
                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
            End If

            lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("periodunkid")
                lvItem.SubItems(colhPeriodName.Index).Tag = dtRow.Item("end_date").ToString
                lvPeriod.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvPeriod.GridLines = False

            If lvPeriod.Items.Count > 6 Then
                colhPeriodName.Width = 200 - 18
            Else
                colhPeriodName.Width = 200
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Nov 2011) -- End

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (18 Mar 2013) -- End

    'Sohail (07 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Sep 2013) -- End

    'Sohail (04 Dec 2014) -- Start
    'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
    Private Sub cboEffectSlabHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEffectSlabHead.SelectedIndexChanged
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Try
            dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboEffectSlabHead.SelectedValue))
            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEffectSlabHead_SelectedIndexChanged", mstrModuleName)
        Finally
            objExcessSlan = Nothing
        End Try
    End Sub
    'Sohail (04 Dec 2014) -- End
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblSlabTranHead.Text = Language._Object.getCaption(Me.lblSlabTranHead.Name, Me.lblSlabTranHead.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lblHousing.Text = Language._Object.getCaption(Me.lblHousing.Name, Me.lblHousing.Text)
			Me.lblSlabEffectivePeriod.Text = Language._Object.getCaption(Me.lblSlabEffectivePeriod.Name, Me.lblSlabEffectivePeriod.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.chkIncludeNonTaxableEarningInGrossPay.Text = Language._Object.getCaption(Me.chkIncludeNonTaxableEarningInGrossPay.Name, Me.chkIncludeNonTaxableEarningInGrossPay.Text)
			Me.lblEffectSlabHead.Text = Language._Object.getCaption(Me.lblEffectSlabHead.Name, Me.lblEffectSlabHead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Housing Head is compulsory information.Please select Housing Head to continue.")
			Language.setMessage(mstrModuleName, 2, "Slab Tax Head is compulsory information.Please select Slab Tax Head to continue.")
			Language.setMessage(mstrModuleName, 3, "Please check atleast one Period to view report.")
			Language.setMessage(mstrModuleName, 4, "Please select transaction head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
