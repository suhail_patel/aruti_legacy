#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsEmpLeavingCertificate
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpLeavingCertificate"
    Private mstrReportId As String = enArutiReport.Employee_Leaving_Certificate

    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintPeriodId As Integer
    Private mstrPeriod As String = ""
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = ""
    Private mstrMembershipNo As String = ""
    Private mintPayeHeadID As Integer = 0
    Private mstrPayeHeadName As String = String.Empty
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipNo() As String
        Set(ByVal value As String)
            mstrMembershipNo = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period() As String
        Set(ByVal value As String)
            mstrPeriod = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadID() As Integer
        Set(ByVal value As Integer)
            mintPayeHeadID = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrPayeHeadName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmpId = 0
            mstrEmpName = ""
            mintPeriodId = 0
            mstrPeriod = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mstrMembershipNo = ""
            mintPayeHeadID = 0
            mstrPayeHeadName = ""
            mdtPeriodStartDate = Nothing
            mdtPeriodEndDate = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            Dim mdecGrossAmt As Decimal = 0
            Dim mdecTotalPAYE As Decimal = 0
            Dim mstrLastPeriodName As String = ""

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            strBaseCurrencySign = objExchangeRate._Currency_Sign
            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                dtPeriodStart = mdtPeriodStartDate : dtPeriodEnd = mdtPeriodEndDate
            End If


            Dim mdtEmpEndDate As DateTime = Nothing
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(dtPeriodEnd) = mintEmpId


            If objEmployee._Termination_From_Date.Date <> Nothing OrElse objEmployee._Empl_Enddate.Date <> Nothing Then
                If objEmployee._Termination_From_Date.Date <> Nothing AndAlso objEmployee._Termination_From_Date.Date < objEmployee._Termination_To_Date.Date Then
                    mdtEmpEndDate = objEmployee._Termination_From_Date.Date
                ElseIf objEmployee._Empl_Enddate.Date <> Nothing AndAlso objEmployee._Empl_Enddate.Date < objEmployee._Termination_To_Date.Date Then
                    mdtEmpEndDate = objEmployee._Empl_Enddate.Date
                End If
            Else
                mdtEmpEndDate = objEmployee._Termination_To_Date.Date
            End If

            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim xPeriodId As Integer = 0
            xPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, mdtEmpEndDate.Date)
            objPeriod._Periodunkid(strDatabaseName) = xPeriodId
            If objPeriod._Yearunkid > 0 Then
                intYearUnkid = objPeriod._Yearunkid
            End If
            xPeriodId = 0

            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid
            objCompany._YearUnkid = intYearUnkid

            If objCompany._DatabaseName <> "" Then
                strDatabaseName = objCompany._DatabaseName
            End If

            If objEmployee._Appointeddate.Date >= objCompany._Database_Start_Date.Date Then
                xPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, objEmployee._Appointeddate.Date, intYearUnkid)
                objPeriod._Periodunkid(strDatabaseName) = xPeriodId
            ElseIf objEmployee._Appointeddate.Date < objCompany._Database_Start_Date.Date Then
                xPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, objCompany._Database_Start_Date.Date, intYearUnkid)
                objPeriod._Periodunkid(strDatabaseName) = xPeriodId
            End If


            StrQ = "SELECT top 1 cfcommon_period_tran.period_name  " & _
                       " FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                       " LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                       " JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                       " WHERE prpayrollprocess_tran.isvoid = 0 And prtnaleave_tran.isvoid = 0  " & _
                       " AND prpayrollprocess_tran.employeeunkid = @employeeunkid " & _
                       " ORDER BY cfcommon_period_tran.end_date DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            Dim dsData As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                mstrLastPeriodName = dsData.Tables(0).Rows(0)("period_name").ToString()
            End If

            StrQ = "SELECT  1 As ID, 'Total Gross Amount' AS Particular, ISNULL(SUM(amount),0.00) AS Amount  " & _
                       " FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                       " LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                       " WHERE prpayrollprocess_tran.isvoid = 0 And prtnaleave_tran.isvoid = 0 AND prpayrollprocess_tran.employeeunkid = @employeeunkid " & _
                       " AND prpayrollprocess_tran.add_deduct = 1 " & _
                       " UNION " & _
                       " SELECT 2 AS ID, 'Total PAYE Tax' As Particular, ISNULL(SUM(amount),0.00) AS Amount  " & _
                       " FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                       " LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                       " WHERE prpayrollprocess_tran.isvoid = 0 And prtnaleave_tran.isvoid = 0  " & _
                       " AND prpayrollprocess_tran.employeeunkid = @employeeunkid AND tranheadunkid = @tranheadId "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            objDataOperation.AddParameter("@tranheadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayeHeadID)
            dsData = objDataOperation.ExecQuery(StrQ, "List")

            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                mdecGrossAmt = CDec(dsData.Tables(0).Rows(0)("Amount"))
                mdecTotalPAYE = CDec(dsData.Tables(0).Rows(1)("Amount"))
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing




            rpt_Row = rpt_Data.Tables(0).NewRow

            rpt_Row.Item("Column1") = mstrMembershipNo
            rpt_Row.Item("Column2") = objEmployee._Surname.ToString()
            rpt_Row.Item("Column3") = objEmployee._Firstname.ToString() + " " + objEmployee._Othername.ToString()
            rpt_Row.Item("Column4") = objEmployee._Birthdate.ToShortDateString()

            If objEmployee._Nationalityunkid > 0 Then
                Dim objCountry As New clsMasterData
                rpt_Row.Item("Column5") = objCountry.getCountryList("Country", False, objEmployee._Nationalityunkid).Tables(0).Rows(0)("country_name").ToString()
                objCountry = Nothing
            Else
                rpt_Row.Item("Column5") = ""
            End If

            rpt_Row.Item("Column6") = objEmployee._Present_Address1.ToString()

            Dim mstrPhysicalAddress As String = ""
            If objEmployee._Present_Plotno.ToString().Length > 0 Then
                mstrPhysicalAddress = objEmployee._Present_Plotno.ToString()
            End If

            If objEmployee._Present_Road.ToString().Length > 0 Then
                If mstrPhysicalAddress.Trim.Length > 0 Then
                    mstrPhysicalAddress &= "," + objEmployee._Present_Road.ToString()
                Else
                    mstrPhysicalAddress = objEmployee._Present_Road.ToString()
                End If
            End If

            If CInt(objEmployee._Present_Town1unkid) > 0 Then
                Dim objCommon As New clsCommon_Master
                objCommon._Masterunkid = objEmployee._Present_Post_Townunkid
                If objCommon._Masterunkid > 0 Then
                    If mstrPhysicalAddress.Trim.Length > 0 Then
                        mstrPhysicalAddress &= "," + objCommon._Name.ToString()
                    Else
                        mstrPhysicalAddress = objCommon._Name.ToString()
                    End If
                End If
                objCommon = Nothing
            End If

            rpt_Row.Item("Column7") = mstrPhysicalAddress
            rpt_Row.Item("Column8") = objEmployee._Present_Mobile.ToString()

            If objEmployee._Maritalstatusunkid > 0 Then
                Dim objCommonMst As New clsCommon_Master
                objCommonMst._Masterunkid = objEmployee._Maritalstatusunkid
                rpt_Row.Item("Column9") = objCommonMst._Name.ToString()
                objCommonMst = Nothing
            Else
                rpt_Row.Item("Column9") = ""
            End If

            If objEmployee._Jobunkid > 0 Then
                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                rpt_Row.Item("Column10") = objJob._Job_Name.ToString()
                objJob = Nothing
            Else
                rpt_Row.Item("Column10") = ""
            End If

            rpt_Row.Item("Column11") = objCompany._FinancialYear_Name.ToString()
            rpt_Row.Item("Column12") = objPeriod._Period_Name
            rpt_Row.Item("Column13") = mstrLastPeriodName
            rpt_Row.Item("Column14") = strBaseCurrencySign & " " & Format(mdecGrossAmt, GUI.fmtCurrency)
            rpt_Row.Item("Column15") = Format(mdecTotalPAYE, GUI.fmtCurrency)
            rpt_Row.Item("Column16") = objCompany._Tinno.ToString()
            rpt_Row.Item("Column17") = objCompany._Name.ToString()
            rpt_Row.Item("Column18") = objCompany._Address1.ToString() + " " + objCompany._Address2.ToString()
            rpt_Row.Item("Column19") = objCompany._City_Name
            rpt_Row.Item("Column20") = objCompany._Email

            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)


            objPeriod = Nothing
            objMaster = Nothing

            objRpt = New ArutiReport.Designer.rptEmp_Leaving_Certificate
            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtMalawiRevenue", Language.getMessage(mstrModuleName, 1, "MALAWI REVENUE AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtDomesticTaxRevenue", Language.getMessage(mstrModuleName, 2, "DOMESTIC TAX REVENUE"))
            ReportFunction.TextChange(objRpt, "txtEmpLeavingCertificate", Language.getMessage(mstrModuleName, 3, "CERTIFICATE OF EMPLOYEE LEAVING"))
            ReportFunction.TextChange(objRpt, "txtEmployeeDetails", Language.getMessage(mstrModuleName, 4, "1. EMPLOYEE’S DETAILS"))
            ReportFunction.TextChange(objRpt, "txtP5", Language.getMessage(mstrModuleName, 5, "P5"))
            ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 6, "SURNAME"))
            ReportFunction.TextChange(objRpt, "txtOtherNames", Language.getMessage(mstrModuleName, 7, "OTHER NAMES"))
            ReportFunction.TextChange(objRpt, "txtDateofBirth", Language.getMessage(mstrModuleName, 8, "DATE OF BIRTH"))
            ReportFunction.TextChange(objRpt, "txtNationality", Language.getMessage(mstrModuleName, 9, "NATIONALITY"))
            ReportFunction.TextChange(objRpt, "txtPostalAddress", Language.getMessage(mstrModuleName, 10, "POSTAL ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Language.getMessage(mstrModuleName, 11, "PHYSICAL ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtContactTelNo", Language.getMessage(mstrModuleName, 12, "CONTACT TELEPHONE NUMBER :"))
            ReportFunction.TextChange(objRpt, "txtMaritalStatus", Language.getMessage(mstrModuleName, 13, "MARITAL STATUS"))
            ReportFunction.TextChange(objRpt, "txtCapacityEmployeed", Language.getMessage(mstrModuleName, 14, "CAPACITY EMPLOYED"))
            ReportFunction.TextChange(objRpt, "txtEmploymentPeriod", Language.getMessage(mstrModuleName, 15, "PERIOD OF EMPLOYMENT DURING THE YEAR"))
            ReportFunction.TextChange(objRpt, "txtFrom", Language.getMessage(mstrModuleName, 16, "FROM"))
            ReportFunction.TextChange(objRpt, "txtTo", Language.getMessage(mstrModuleName, 17, "TO"))
            ReportFunction.TextChange(objRpt, "txtTotalGrossEmiluments", Language.getMessage(mstrModuleName, 18, "TOTAL GROSS EMILUMENTS FOR THE PERIOD:"))
            ReportFunction.TextChange(objRpt, "txtTotalTexDeducted", Language.getMessage(mstrModuleName, 19, "TOTAL TAX (PAYE) DEDUCTED TO DATE"))
            ReportFunction.TextChange(objRpt, "txtParticularsEmployer", Language.getMessage(mstrModuleName, 20, "2. PARTICULARS OF EMPLOYER"))
            ReportFunction.TextChange(objRpt, "txtTPin", Language.getMessage(mstrModuleName, 21, "TPIN :"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", Language.getMessage(mstrModuleName, 22, "NAME OF EMPLOYER"))
            ReportFunction.TextChange(objRpt, "txtEmployerPostalAddress", Language.getMessage(mstrModuleName, 10, "POSTAL ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtEmployerPhysicalAddress", Language.getMessage(mstrModuleName, 11, "PHYSICAL ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 23, "E-MAIL"))
            ReportFunction.TextChange(objRpt, "txtDeclaration", Language.getMessage(mstrModuleName, 24, "I declare that the information given above is correct and complete."))
            ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 25, "Date"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "MALAWI REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 2, "DOMESTIC TAX REVENUE")
            Language.setMessage(mstrModuleName, 3, "CERTIFICATE OF EMPLOYEE LEAVING")
            Language.setMessage(mstrModuleName, 4, "1. EMPLOYEE’S DETAILS")
            Language.setMessage(mstrModuleName, 5, "P5")
            Language.setMessage(mstrModuleName, 6, "SURNAME")
            Language.setMessage(mstrModuleName, 7, "OTHER NAMES")
            Language.setMessage(mstrModuleName, 8, "DATE OF BIRTH")
            Language.setMessage(mstrModuleName, 9, "NATIONALITY")
            Language.setMessage(mstrModuleName, 10, "POSTAL ADDRESS")
            Language.setMessage(mstrModuleName, 11, "PHYSICAL ADDRESS")
            Language.setMessage(mstrModuleName, 12, "CONTACT TELEPHONE NUMBER :")
            Language.setMessage(mstrModuleName, 13, "MARITAL STATUS")
            Language.setMessage(mstrModuleName, 14, "CAPACITY EMPLOYED")
            Language.setMessage(mstrModuleName, 15, "PERIOD OF EMPLOYMENT DURING THE YEAR")
            Language.setMessage(mstrModuleName, 16, "FROM")
            Language.setMessage(mstrModuleName, 17, "TO")
            Language.setMessage(mstrModuleName, 18, "TOTAL GROSS EMILUMENTS FOR THE PERIOD:")
            Language.setMessage(mstrModuleName, 19, "TOTAL TAX (PAYE) DEDUCTED TO DATE")
            Language.setMessage(mstrModuleName, 20, "2. PARTICULARS OF EMPLOYER")
            Language.setMessage(mstrModuleName, 21, "TPIN :")
            Language.setMessage(mstrModuleName, 22, "NAME OF EMPLOYER")
            Language.setMessage(mstrModuleName, 23, "E-MAIL")
            Language.setMessage(mstrModuleName, 24, "I declare that the information given above is correct and complete.")
            Language.setMessage(mstrModuleName, 25, "Date")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
