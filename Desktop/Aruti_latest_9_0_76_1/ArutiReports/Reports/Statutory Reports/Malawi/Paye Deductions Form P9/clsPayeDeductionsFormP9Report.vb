'************************************************************************************************************************************
'Class Name : clsPayeDeductionsFormP9Report.vb
'Purpose    :
'Date       :18/04/2018
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>

Public Class clsPayeDeductionsFormP9Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayeDeductionsFormP9Report"
    Private mstrReportId As String = enArutiReport.PAYE_DEDUCTION_FORM_P9_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

    Private mintEmployeeID As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintRelationID As Integer = 0
    Private mstrRelationName As String = String.Empty
    Private mstrPeriodIDs As String = String.Empty
    Private mstrPeriodNames As String = String.Empty
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mstrFmtCurrency As String = GUI.fmtCurrency
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mdtDatabaseEndDate As Date = FinancialYear._Object._Database_End_Date
    Private mblnApplyUserAccessFilter As Boolean = True

    Private mintPayeHeadID As Integer = 0
    Private mstrPayeHeadName As String = String.Empty
    Private mintGrossBasicHeadID As Integer = 0
    Private mstrGrossBasicHeadName As String = String.Empty
    Private mintHouseAllowanceHeadID As Integer = 0
    Private mstrHouseAllowanceHeadName As String = String.Empty
    Private mintFuelLightHeadID As Integer = 0
    Private mstrFuelLightHeadName As String = String.Empty
    Private mintEduAllowanceHeadID As Integer = 0
    Private mstrEduAllowanceHeadName As String = String.Empty
    Private mintOtherBenefitHeadID As Integer = 0
    Private mstrOtherBenefitHeadName As String = String.Empty
    Private mintLeavePassageHeadID As Integer = 0
    Private mstrLeavePassageHeadName As String = String.Empty
    Private mintGratuityHeadID As Integer = 0
    Private mstrGratuityHeadName As String = String.Empty
    Private mintPensionHeadID As Integer = 0
    Private mstrPensionHeadName As String = String.Empty
    Private mintTaxFreeHouseAllowanceHeadID As Integer = 0
    Private mstrTaxFreeHouseAllowanceHeadName As String = String.Empty

    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _RelationID() As Integer
        Set(ByVal value As Integer)
            mintRelationID = value
        End Set
    End Property

    Public WriteOnly Property _RelationName() As String
        Set(ByVal value As String)
            mstrRelationName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _FmtCurrency() As String
        Set(ByVal value As String)
            mstrFmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtDatabaseEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadID() As Integer
        Set(ByVal value As Integer)
            mintPayeHeadID = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrPayeHeadName = value
        End Set
    End Property

    Public WriteOnly Property _GrossBasicHeadID() As Integer
        Set(ByVal value As Integer)
            mintGrossBasicHeadID = value
        End Set
    End Property

    Public WriteOnly Property _GrossBasicHeadName() As String
        Set(ByVal value As String)
            mstrGrossBasicHeadName = value
        End Set
    End Property

    Public WriteOnly Property _HouseAllowanceHeadID() As Integer
        Set(ByVal value As Integer)
            mintHouseAllowanceHeadID = value
        End Set
    End Property

    Public WriteOnly Property _HouseAllowanceHeadName() As String
        Set(ByVal value As String)
            mstrHouseAllowanceHeadName = value
        End Set
    End Property

    Public WriteOnly Property _FuelLightHeadID() As Integer
        Set(ByVal value As Integer)
            mintFuelLightHeadID = value
        End Set
    End Property

    Public WriteOnly Property _FuelLightHeadName() As String
        Set(ByVal value As String)
            mstrFuelLightHeadName = value
        End Set
    End Property

    Public WriteOnly Property _EduAllowanceHeadID() As Integer
        Set(ByVal value As Integer)
            mintEduAllowanceHeadID = value
        End Set
    End Property

    Public WriteOnly Property _EduAllowanceHeadName() As String
        Set(ByVal value As String)
            mstrEduAllowanceHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OtherBenefitHeadID() As Integer
        Set(ByVal value As Integer)
            mintOtherBenefitHeadID = value
        End Set
    End Property

    Public WriteOnly Property _OtherBenefitHeadName() As String
        Set(ByVal value As String)
            mstrOtherBenefitHeadName = value
        End Set
    End Property

    Public WriteOnly Property _LeavePassageHeadID() As Integer
        Set(ByVal value As Integer)
            mintLeavePassageHeadID = value
        End Set
    End Property

    Public WriteOnly Property _LeavePassageHeadName() As String
        Set(ByVal value As String)
            mstrLeavePassageHeadName = value
        End Set
    End Property

    Public WriteOnly Property _GratuityHeadID() As Integer
        Set(ByVal value As Integer)
            mintGratuityHeadID = value
        End Set
    End Property

    Public WriteOnly Property _GratuityHeadName() As String
        Set(ByVal value As String)
            mstrGratuityHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PensionHeadID() As Integer
        Set(ByVal value As Integer)
            mintPensionHeadID = value
        End Set
    End Property

    Public WriteOnly Property _PensionHeadName() As String
        Set(ByVal value As String)
            mstrPensionHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TaxFreeHouseAllowanceHeadID() As Integer
        Set(ByVal value As Integer)
            mintTaxFreeHouseAllowanceHeadID = value
        End Set
    End Property

    Public WriteOnly Property _TaxFreeHouseAllowanceHeadName() As String
        Set(ByVal value As String)
            mstrTaxFreeHouseAllowanceHeadName = value
        End Set
    End Property


    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = String.Empty
            
            mintEmployeeID = 0
            mstrEmployeeName = ""
            mintRelationID = 0
            mstrRelationName = ""
            mstrPeriodIDs = ""
            mstrPeriodNames = ""
            mstrFmtCurrency = ""

            mintPayeHeadID = 0
            mstrPayeHeadName = ""
            mintGrossBasicHeadID = 0
            mstrGrossBasicHeadName = ""
            mintHouseAllowanceHeadID = 0
            mstrHouseAllowanceHeadName = ""
            mintFuelLightHeadID = 0
            mstrFuelLightHeadName = ""
            mintEduAllowanceHeadID = 0
            mstrEduAllowanceHeadName = ""
            mintOtherBenefitHeadID = 0
            mstrOtherBenefitHeadName = ""
            mintLeavePassageHeadID = 0
            mstrLeavePassageHeadName = ""
            mintGratuityHeadID = 0
            mstrGratuityHeadName = ""
            mintPensionHeadID = 0
            mstrPensionHeadName = ""
            mintTaxFreeHouseAllowanceHeadID = 0
            mstrTaxFreeHouseAllowanceHeadName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeID > 0 Then
                objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
            End If

            objDataOperation.AddParameter("@relationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationID)

            objDataOperation.AddParameter("@Payeheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayeHeadID)
            objDataOperation.AddParameter("@GrossBasicheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrossBasicHeadID)
            objDataOperation.AddParameter("@HouseAllowanceheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHouseAllowanceHeadID)
            objDataOperation.AddParameter("@FuelLightheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFuelLightHeadID)
            objDataOperation.AddParameter("@EduAllowanceheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEduAllowanceHeadID)
            objDataOperation.AddParameter("@OtherBenefitheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherBenefitHeadID)
            objDataOperation.AddParameter("@LeavePassageheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavePassageHeadID)
            objDataOperation.AddParameter("@Gratuityheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGratuityHeadID)
            objDataOperation.AddParameter("@Pensionheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPensionHeadID)
            objDataOperation.AddParameter("@TaxFreeHouseAllowanceheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxFreeHouseAllowanceHeadID)

            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                         ", ISNULL(hremployee_master.othername, '') AS othername " & _
                         ", ISNULL(hremployee_master.surname, '') AS surname " & _
                         ", ISNULL(Title.name, '') AS EmpTitle " & _
                         ", ISNULL(Marital.name, '') AS MaritalStatus " & _
                         ", ISNULL(hrjob_master.job_name, '') AS JobName " & _
                         ", hremployee_master.present_address1 " & _
                         ", hremployee_master.present_address2 " & _
                         ", ISNULL(cfstate_master.name, '') as StateName " & _
                         ", ISNULL(cfcountry_master.country_name, '') AS CountryName " & _
                         ", ISNULL(cfzipcode_master.zipcode_no, '') AS ZipCode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS employeename "
            Else
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS employeename "
            End If

            StrQ &= "INTO #TableEmp " & _
                    "FROM " & strDatabaseName & "..hremployee_master " & _
                    "LEFT JOIN hrmsConfiguration..cfstate_master ON cfstate_master.stateunkid = hremployee_master.present_stateunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfcountry_master ON cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfzipcode_master ON cfzipcode_master.zipcodeunkid = hremployee_master.present_postcodeunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..cfcommon_master AS Marital ON maritalstatusunkid = Marital.masterunkid " & _
                        "AND Marital.mastertype = " & CInt(clsCommon_Master.enCommonMaster.MARRIED_STATUS) & " " & _
                    "LEFT JOIN " & strDatabaseName & "..cfcommon_master AS Title ON titleunkid = Title.masterunkid " & _
                        "AND Title.mastertype = " & CInt(clsCommon_Master.enCommonMaster.TITLE) & " " & _
                    "LEFT JOIN " & _
                        "( " & _
                          "SELECT  jobgroupunkid " & _
                                ", jobunkid " & _
                                ", employeeunkid " & _
                                ", ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                          "FROM hremployee_categorization_tran " & _
                          "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                          ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & strDatabaseName & "..hrjob_master ON J.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 "

            StrQ &= mstrAnalysis_Join

            If mblnApplyUserAccessFilter = True AndAlso xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "WHERE hremployee_master.isactive = 1 "

            If mblnApplyUserAccessFilter = True AndAlso xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mintEmployeeID > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @EmployeeID "
            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM " & strDatabaseName & "..hrdependant_beneficiaries_status_tran "

            If mintEmployeeID > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeID > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @EmployeeId "
            End If

            If mdtPeriodEndDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @end_date "
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= "SELECT A.employeeunkid " & _
                         ", ISNULL(STUFF( " & _
                                  "( " & _
                                      "SELECT DISTINCT ', ' +  hrdependants_beneficiaries_tran.first_name + ' ' + hrdependants_beneficiaries_tran.last_name " & _
                                      "FROM " & strDatabaseName & "..hrdependants_beneficiaries_tran " & _
                                          "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                          "LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = relationunkid " & _
                                          "JOIN #TableEmp ON #TableEmp.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                                      "WHERE hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                            "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.RELATIONS) & " " & _
                                            "AND cfcommon_master.masterunkid = @relationID " & _
                                            "AND hrdependants_beneficiaries_tran.employeeunkid = A.employeeunkid " & _
                                            "AND #TableDepn.isactive = 1 " & _
                                      "FOR XML PATH('') " & _
                                  "),1,1,''),'') AS SpouseName " & _
                         ", ISNULL(STUFF( " & _
                                  "( " & _
                                      "SELECT DISTINCT ', ' +  hrdependants_beneficiaries_tran.address " & _
                                      "FROM " & strDatabaseName & "..hrdependants_beneficiaries_tran " & _
                                          "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                          "LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = relationunkid " & _
                                          "JOIN #TableEmp ON #TableEmp.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                                      "WHERE hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                            "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.RELATIONS) & " " & _
                                            "AND cfcommon_master.masterunkid = @relationID " & _
                                            "AND hrdependants_beneficiaries_tran.employeeunkid = A.employeeunkid " & _
                                            "AND #TableDepn.isactive = 1 " & _
                                      "FOR XML PATH('') " & _
                                  "),1,1,''),'') AS SpouseAddress " & _
                    "INTO #TableSpouse " & _
                    "FROM " & strDatabaseName & "..hremployee_master AS A " & _
                    "JOIN #TableEmp ON #TableEmp.employeeunkid = A.employeeunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.employeeunkid = A.employeeunkid " & _
                    "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                        "AND #TableDepn.isactive = 1 " & _
                    "GROUP BY A.employeeunkid "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

            StrQ &= "SELECT  #TableEmp.employeeunkid " & _
                          ", #TableEmp.employeecode " & _
                          ", #TableEmp.employeename " & _
                          ", #TableEmp.EmpTitle " & _
                          ", #TableEmp.MaritalStatus " & _
                          ", #TableEmp.JobName " & _
                          ", #TableSpouse.SpouseName " & _
                          ", #TableSpouse.SpouseAddress " & _
                          ", #TableEmp.JobName " & _
                          ", #TableEmp.present_address1 " & _
                          ", #TableEmp.present_address2 " & _
                          ", #TableEmp.StateName " & _
                          ", #TableEmp.CountryName " & _
                          ", #TableEmp.ZipCode " & _
                          ", A.Paye " & _
                          ", A.GrossBasic " & _
                          ", A.HouseAllowance " & _
                          ", A.FuelLight " & _
                          ", A.EduAllowance " & _
                          ", A.OtherBenefit " & _
                          ", A.LeavePassage " & _
                          ", A.Gratuity " & _
                          ", A.Pension " & _
                          ", A.TaxFreeHouseAllowance " & _
                    "FROM    ( SELECT    SUM(ISNULL(PAYE.Paye, 0)) AS Paye " & _
                                      ", SUM(ISNULL(PAYE.GrossBasic, 0)) AS GrossBasic " & _
                                      ", SUM(ISNULL(PAYE.HouseAllowance, 0)) AS HouseAllowance " & _
                                      ", SUM(ISNULL(PAYE.FuelLight, 0)) AS FuelLight " & _
                                      ", SUM(ISNULL(PAYE.EduAllowance, 0)) AS EduAllowance " & _
                                      ", SUM(ISNULL(PAYE.OtherBenefit, 0)) AS OtherBenefit " & _
                                      ", SUM(ISNULL(PAYE.LeavePassage, 0)) AS LeavePassage " & _
                                      ", SUM(ISNULL(PAYE.Gratuity, 0)) AS Gratuity " & _
                                      ", SUM(ISNULL(PAYE.Pension, 0)) AS Pension " & _
                                      ", SUM(ISNULL(PAYE.TaxFreeHouseAllowance, 0)) AS TaxFreeHouseAllowance " & _
                                      ", paye.empid AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @Payeheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @GrossBasicheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @HouseAllowanceheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @FuelLightheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @EduAllowanceheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @OtherBenefitheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @LeavePassageheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @Gratuityheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Pension " & _
                                                  ", 0 AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @Pensionheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS Paye " & _
                                                  ", 0 AS GrossBasic " & _
                                                  ", 0 AS HouseAllowance " & _
                                                  ", 0 AS FuelLight " & _
                                                  ", 0 AS EduAllowance " & _
                                                  ", 0 AS OtherBenefit " & _
                                                  ", 0 AS LeavePassage " & _
                                                  ", 0 AS Gratuity " & _
                                                  ", 0 AS Pension " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxFreeHouseAllowance " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "LEFT JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                          "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @TaxFreeHouseAllowanceheadid " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") "


            StrQ &= "                      ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                              "/*HAVING    SUM(ISNULL(PAYE.paye, 0)) <> 0*/ " & _
                            ") AS A " & _
                            "LEFT JOIN #TableEmp ON #TableEmp.employeeunkid = A.empid " & _
                            "LEFT JOIN #TableSpouse ON #TableSpouse.employeeunkid = A.empid "



            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If mblnFirstNamethenSurname = True Then
                StrQ &= " ORDER BY  ISNULL(#TableEmp.firstname, '') + ' ' + ISNULL(#TableEmp.othername, '') + ' ' + ISNULL(#TableEmp.surname, '') "
            Else
                StrQ &= " ORDER BY  ISNULL(#TableEmp.surname, '') + ' ' + ISNULL(#TableEmp.firstname, '') + ' ' + ISNULL(#TableEmp.othername, '')  "
            End If

            StrQ &= " DROP TABLE #TableEmp " & _
                    " DROP TABLE #TableSpouse "

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing

            Dim decPaye As Decimal = 0
            Dim decGrossBasic As Decimal = 0
            Dim decHouseAllowance As Decimal = 0
            Dim decFuelLight As Decimal = 0
            Dim decEduAllowance As Decimal = 0
            Dim decOtherBenefit As Decimal = 0
            Dim decLeavePassage As Decimal = 0
            Dim decGratuity As Decimal = 0
            Dim decGrossIncome As Decimal = 0
            Dim decPension As Decimal = 0
            Dim decTaxFreeHouseAllowance As Decimal = 0
            Dim decTotDeduction As Decimal = 0

            For Each dRow As DataRow In dsList.Tables(0).Rows
                rpt_Row = rpt_Data.Tables(0).NewRow

                rpt_Row.Item("Column1") = dRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dRow.Item("employeecode")
                If dRow.Item("EmpTitle").ToString().Trim.Length > 0 Then
                    rpt_Row.Item("Column3") = dRow.Item("EmpTitle").ToString() & " " & dRow.Item("employeename")
                Else
                    rpt_Row.Item("Column3") = dRow.Item("employeename")
                End If
                rpt_Row.Item("Column4") = dRow.Item("MaritalStatus")
                rpt_Row.Item("Column5") = dRow.Item("JobName")
                rpt_Row.Item("Column6") = dRow.Item("SpouseName")
                rpt_Row.Item("Column7") = dRow.Item("present_address1")
                rpt_Row.Item("Column8") = dRow.Item("present_address2")
                rpt_Row.Item("Column9") = dRow.Item("StateName")
                rpt_Row.Item("Column10") = dRow.Item("CountryName")
                rpt_Row.Item("Column11") = dRow.Item("ZipCode")
                rpt_Row.Item("Column12") = dRow.Item("SpouseAddress")
                rpt_Row.Item("Column21") = Format(CDec(dRow.Item("Paye")), mstrFmtCurrency)
                rpt_Row.Item("Column22") = Format(CDec(dRow.Item("GrossBasic")), mstrFmtCurrency)
                rpt_Row.Item("Column23") = Format(CDec(dRow.Item("HouseAllowance")), mstrFmtCurrency)
                rpt_Row.Item("Column24") = Format(CDec(dRow.Item("FuelLight")), mstrFmtCurrency)
                rpt_Row.Item("Column25") = Format(CDec(dRow.Item("EduAllowance")), mstrFmtCurrency)
                rpt_Row.Item("Column26") = Format(CDec(dRow.Item("OtherBenefit")), mstrFmtCurrency)
                rpt_Row.Item("Column27") = Format(CDec(dRow.Item("LeavePassage")), mstrFmtCurrency)
                rpt_Row.Item("Column28") = Format(CDec(dRow.Item("Gratuity")), mstrFmtCurrency)
                rpt_Row.Item("Column29") = Format(CDec(dRow.Item("Pension")), mstrFmtCurrency)
                rpt_Row.Item("Column30") = Format(CDec(dRow.Item("TaxFreeHouseAllowance")), mstrFmtCurrency)

                decPaye = CDec(Format(CDec(dRow.Item("Paye")), mstrFmtCurrency))
                decGrossBasic = CDec(Format(CDec(dRow.Item("GrossBasic")), mstrFmtCurrency))
                decHouseAllowance = CDec(Format(CDec(dRow.Item("HouseAllowance")), mstrFmtCurrency))
                decFuelLight = CDec(Format(CDec(dRow.Item("FuelLight")), mstrFmtCurrency))
                decEduAllowance = CDec(Format(CDec(dRow.Item("EduAllowance")), mstrFmtCurrency))
                decOtherBenefit = CDec(Format(CDec(dRow.Item("OtherBenefit")), mstrFmtCurrency))
                decLeavePassage = CDec(Format(CDec(dRow.Item("LeavePassage")), mstrFmtCurrency))
                decGratuity = CDec(Format(CDec(dRow.Item("Gratuity")), mstrFmtCurrency))

                decPension = CDec(Format(CDec(dRow.Item("Pension")), mstrFmtCurrency))
                decTaxFreeHouseAllowance = CDec(Format(CDec(dRow.Item("TaxFreeHouseAllowance")), mstrFmtCurrency))

                decGrossIncome = decPaye + decGrossBasic + decHouseAllowance + decFuelLight + decEduAllowance + decOtherBenefit + decLeavePassage + decGratuity
                decTotDeduction = decPension + decTaxFreeHouseAllowance

                rpt_Row.Item("Column31") = Format(decGrossIncome, mstrFmtCurrency)
                rpt_Row.Item("Column32") = Format(decGrossIncome - decTotDeduction, mstrFmtCurrency)



                rpt_Data.Tables(0).Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rpt_Paye_Deduction_FormP9

            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid
            Dim strAdd3 As String = ""
            If objCompany._City_Name.Trim.Length > 0 Then
                strAdd3 += objCompany._City_Name
            End If
            If objCompany._State_Name.Trim.Length > 0 Then
                strAdd3 += " " + objCompany._State_Name
            End If
            If objCompany._Country_Name.Trim.Length > 0 Then
                strAdd3 += " " + objCompany._Country_Name
            End If
            If objCompany._Post_Code_No.Trim.Length > 0 Then
                strAdd3 += " " + objCompany._Post_Code_No
            End If
            strAdd3 = Trim(strAdd3)


            Dim dsPerm As New DataSet : Dim dsTemp As New DataSet

            ReportFunction.TextChange(objRpt, "txtMRA", Language.getMessage(mstrModuleName, 1, "MALAWI REVENUE AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtFormP9", Language.getMessage(mstrModuleName, 2, "Form P.9"))
            ReportFunction.TextChange(objRpt, "txtITD", Language.getMessage(mstrModuleName, 3, "INCOME TAX DIVISION"))
            ReportFunction.TextChange(objRpt, "txtPAYECerti", Language.getMessage(mstrModuleName, 4, "P.A.Y.E CERTIFICATE OF TOTAL EMOLUMENTS AND TAX DEDUCTION"))
            ReportFunction.TextChange(objRpt, "lblYearEnd1", Language.getMessage(mstrModuleName, 5, "YEAR ENDED"))
            ReportFunction.TextChange(objRpt, "txtYearEnd1", mdtDatabaseEndDate.ToNthDateFormat())

            ReportFunction.TextChange(objRpt, "lblFullName", Language.getMessage(mstrModuleName, 6, "FULL NAME AND ADDRESS OF EMPLOYEE"))
            ReportFunction.TextChange(objRpt, "lblStateTitle", Language.getMessage(mstrModuleName, 7, "(STATE TITLE)"))

            ReportFunction.TextChange(objRpt, "lblCompNameAndAddress", Language.getMessage(mstrModuleName, 8, "NAME AND ADDRESS OF EMPLOYER"))
            ReportFunction.TextChange(objRpt, "txtCompName", objCompany._Name)
            ReportFunction.TextChange(objRpt, "txtCompAddress1", objCompany._Address1)
            ReportFunction.TextChange(objRpt, "txtCompAddress2", objCompany._Address2)
            ReportFunction.TextChange(objRpt, "txtCompAddress3", strAdd3)

            ReportFunction.TextChange(objRpt, "lblamtoftaxdeduct", Language.getMessage(mstrModuleName, 9, "AMOUNT OF TAX DEDUCTED IN FIGURES"))
            ReportFunction.TextChange(objRpt, "lblemployeetpin", Language.getMessage(mstrModuleName, 10, "EMPLOYER’S TPIN"))
            ReportFunction.TextChange(objRpt, "txtemployeetpin", objCompany._Tinno)
            ReportFunction.TextChange(objRpt, "lblamtoftaxdeductWord", Language.getMessage(mstrModuleName, 11, "AMOUNT OF TAX DEDUCTED IN WORDS"))
            ReportFunction.TextChange(objRpt, "lblMarriedStatus", Language.getMessage(mstrModuleName, 12, "State whether Single, Married, Widowed, or Divorced"))
            ReportFunction.TextChange(objRpt, "lblCapicity", Language.getMessage(mstrModuleName, 13, "Capacity in which employed"))
            ReportFunction.TextChange(objRpt, "lblPeriodEmployed", Language.getMessage(mstrModuleName, 14, "Period employed during the"))
            ReportFunction.TextChange(objRpt, "lblYearEnd2", Language.getMessage(mstrModuleName, 15, "Year ended"))
            ReportFunction.TextChange(objRpt, "txtYearEnd2", mdtDatabaseEndDate.ToNthDateFormat())
            ReportFunction.TextChange(objRpt, "lblMarriedSpouse", Language.getMessage(mstrModuleName, 16, "If married give full names and address of spouse:"))

            ReportFunction.TextChange(objRpt, "lblYearEnd3", Language.getMessage(mstrModuleName, 17, "Particulars of emoluments which accrued to the above named during the year ended"))
            ReportFunction.TextChange(objRpt, "txtYearEnd3", mdtDatabaseEndDate.ToNthDateFormat())
            ReportFunction.TextChange(objRpt, "lblA", Language.getMessage(mstrModuleName, 18, "(a)"))
            ReportFunction.TextChange(objRpt, "lblGrossSalary", Language.getMessage(mstrModuleName, 19, "Gross salary or wages or pension and overtime"))
            ReportFunction.TextChange(objRpt, "lblB", Language.getMessage(mstrModuleName, 20, "(b)"))
            ReportFunction.TextChange(objRpt, "lblHouseAllowance", Language.getMessage(mstrModuleName, 21, "Housing Allowance K"))
            ReportFunction.TextChange(objRpt, "lblHouseAllowancePerAnnum", Language.getMessage(mstrModuleName, 22, "per annum"))
            ReportFunction.TextChange(objRpt, "lblC", Language.getMessage(mstrModuleName, 23, "(c)"))
            ReportFunction.TextChange(objRpt, "lblFualCost", Language.getMessage(mstrModuleName, 24, "Fuel, Light, water and Laundry Cost K"))
            ReportFunction.TextChange(objRpt, "lblFualCostPerAnnum", Language.getMessage(mstrModuleName, 25, "per annum"))
            ReportFunction.TextChange(objRpt, "lblD", Language.getMessage(mstrModuleName, 26, "(d)"))
            ReportFunction.TextChange(objRpt, "lblEducationAllowance", Language.getMessage(mstrModuleName, 27, "Educational Allowances"))
            ReportFunction.TextChange(objRpt, "lblE", Language.getMessage(mstrModuleName, 28, "(e)"))
            ReportFunction.TextChange(objRpt, "lblOtherBenefits", Language.getMessage(mstrModuleName, 29, "Other benefits or emoluments"))
            ReportFunction.TextChange(objRpt, "lblF", Language.getMessage(mstrModuleName, 30, "(f)"))
            ReportFunction.TextChange(objRpt, "lblLeavePassages", Language.getMessage(mstrModuleName, 31, "Leave passages paid by employer"))
            ReportFunction.TextChange(objRpt, "lblOtherSchemes", Language.getMessage(mstrModuleName, 32, "(g)"))
            ReportFunction.TextChange(objRpt, "lblG", Language.getMessage(mstrModuleName, 33, "(g)"))
            ReportFunction.TextChange(objRpt, "lblGratuity", Language.getMessage(mstrModuleName, 34, "Gratuity"))
            ReportFunction.TextChange(objRpt, "lblGrossIncome", Language.getMessage(mstrModuleName, 35, "GROSS INCOME"))
            ReportFunction.TextChange(objRpt, "lblLess", Language.getMessage(mstrModuleName, 36, "Less:"))
            ReportFunction.TextChange(objRpt, "lblPension", Language.getMessage(mstrModuleName, 37, "Pension"))
            ReportFunction.TextChange(objRpt, "lblTaxFreeHousingAllowance", Language.getMessage(mstrModuleName, 38, "Tax Free housing allowance"))
            ReportFunction.TextChange(objRpt, "lblTaxableIncome", Language.getMessage(mstrModuleName, 39, "TAXABLE INCOME"))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 40, "Date:"))
            ReportFunction.TextChange(objRpt, "txtDate", Format(DateAndTime.Today.Date, "dd-MMM-yyyy"))
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 41, "Signature:"))
            ReportFunction.TextChange(objRpt, "lblRepresentetive", Language.getMessage(mstrModuleName, 42, "(Employer’s Representative)"))
            ReportFunction.TextChange(objRpt, "lblFillCerti", Language.getMessage(mstrModuleName, 43, "Fill this certificate in triplicate and distribute a copy to each of the following MRA, Employee and Employer."))

            objRpt.SetDataSource(rpt_Data)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "MALAWI REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 2, "Form P.9")
            Language.setMessage(mstrModuleName, 3, "INCOME TAX DIVISION")
            Language.setMessage(mstrModuleName, 4, "P.A.Y.E CERTIFICATE OF TOTAL EMOLUMENTS AND TAX DEDUCTION")
            Language.setMessage(mstrModuleName, 5, "YEAR ENDED")
            Language.setMessage(mstrModuleName, 6, "FULL NAME AND ADDRESS OF EMPLOYEE")
            Language.setMessage(mstrModuleName, 7, "(STATE TITLE)")
            Language.setMessage(mstrModuleName, 8, "NAME AND ADDRESS OF EMPLOYER")
            Language.setMessage(mstrModuleName, 9, "AMOUNT OF TAX DEDUCTED IN FIGURES")
            Language.setMessage(mstrModuleName, 10, "EMPLOYER’S TPIN")
            Language.setMessage(mstrModuleName, 11, "AMOUNT OF TAX DEDUCTED IN WORDS")
            Language.setMessage(mstrModuleName, 12, "State whether Single, Married, Widowed, or Divorced")
            Language.setMessage(mstrModuleName, 13, "Capacity in which employed")
            Language.setMessage(mstrModuleName, 14, "Period employed during the")
            Language.setMessage(mstrModuleName, 15, "Year ended")
            Language.setMessage(mstrModuleName, 16, "If married give full names and address of spouse:")
            Language.setMessage(mstrModuleName, 17, "Particulars of emoluments which accrued to the above named during the year ended")
            Language.setMessage(mstrModuleName, 18, "(a)")
            Language.setMessage(mstrModuleName, 19, "Gross salary or wages or pension and overtime")
            Language.setMessage(mstrModuleName, 20, "(b)")
            Language.setMessage(mstrModuleName, 21, "Housing Allowance K")
            Language.setMessage(mstrModuleName, 22, "per annum")
            Language.setMessage(mstrModuleName, 23, "(c)")
            Language.setMessage(mstrModuleName, 24, "Fuel, Light, water and Laundry Cost K")
            Language.setMessage(mstrModuleName, 25, "per annum")
            Language.setMessage(mstrModuleName, 26, "(d)")
            Language.setMessage(mstrModuleName, 27, "Educational Allowances")
            Language.setMessage(mstrModuleName, 28, "(e)")
            Language.setMessage(mstrModuleName, 29, "Other benefits or emoluments")
            Language.setMessage(mstrModuleName, 30, "(f)")
            Language.setMessage(mstrModuleName, 31, "Leave passages paid by employer")
            Language.setMessage(mstrModuleName, 32, "(g)")
            Language.setMessage(mstrModuleName, 33, "(g)")
            Language.setMessage(mstrModuleName, 34, "Gratuity")
            Language.setMessage(mstrModuleName, 35, "GROSS INCOME")
            Language.setMessage(mstrModuleName, 36, "Less:")
            Language.setMessage(mstrModuleName, 37, "Pension")
            Language.setMessage(mstrModuleName, 38, "Tax Free housing allowance")
            Language.setMessage(mstrModuleName, 39, "TAXABLE INCOME")
            Language.setMessage(mstrModuleName, 40, "Date:")
            Language.setMessage(mstrModuleName, 41, "Signature:")
            Language.setMessage(mstrModuleName, 42, "(Employer’s Representative)")
            Language.setMessage(mstrModuleName, 43, "Fill this certificate in triplicate and distribute a copy to each of the following MRA, Employee and Employer.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
