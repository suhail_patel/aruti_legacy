﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayeDeductionsFormP9Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayeDeductionsFormP9Report))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblFYear = New System.Windows.Forms.Label
        Me.cboFYear = New System.Windows.Forms.ComboBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objchkPeriodSelectAll = New System.Windows.Forms.CheckBox
        Me.dgPeriod = New System.Windows.Forms.DataGridView
        Me.txtSearchPeriod = New System.Windows.Forms.TextBox
        Me.cboTaxFreeHouseAllowance = New System.Windows.Forms.ComboBox
        Me.lblTaxFreeHouseAllowance = New System.Windows.Forms.Label
        Me.cboPension = New System.Windows.Forms.ComboBox
        Me.lblPension = New System.Windows.Forms.Label
        Me.cboGratuity = New System.Windows.Forms.ComboBox
        Me.lblGratuity = New System.Windows.Forms.Label
        Me.cboLeavePassage = New System.Windows.Forms.ComboBox
        Me.lblLeavePassage = New System.Windows.Forms.Label
        Me.cboOtherBenefit = New System.Windows.Forms.ComboBox
        Me.lblOtherBenefit = New System.Windows.Forms.Label
        Me.lblRelation = New System.Windows.Forms.Label
        Me.cboRelation = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboEduAllowance = New System.Windows.Forms.ComboBox
        Me.lblEduAllowance = New System.Windows.Forms.Label
        Me.cboFuelLight = New System.Windows.Forms.ComboBox
        Me.lblFuelLight = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblHouseAllowance = New System.Windows.Forms.Label
        Me.cboHouseAllowance = New System.Windows.Forms.ComboBox
        Me.lblGrossBasic = New System.Windows.Forms.Label
        Me.cboGrossBasic = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblPAYE = New System.Windows.Forms.Label
        Me.cboPAYE = New System.Windows.Forms.ComboBox
        Me.objdgperiodcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriodCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPeriodEnd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 431)
        Me.NavPanel.Size = New System.Drawing.Size(679, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblFYear)
        Me.gbFilterCriteria.Controls.Add(Me.cboFYear)
        Me.gbFilterCriteria.Controls.Add(Me.Panel2)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxFreeHouseAllowance)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxFreeHouseAllowance)
        Me.gbFilterCriteria.Controls.Add(Me.cboPension)
        Me.gbFilterCriteria.Controls.Add(Me.lblPension)
        Me.gbFilterCriteria.Controls.Add(Me.cboGratuity)
        Me.gbFilterCriteria.Controls.Add(Me.lblGratuity)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeavePassage)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeavePassage)
        Me.gbFilterCriteria.Controls.Add(Me.cboOtherBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.lblOtherBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.lblRelation)
        Me.gbFilterCriteria.Controls.Add(Me.cboRelation)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboEduAllowance)
        Me.gbFilterCriteria.Controls.Add(Me.lblEduAllowance)
        Me.gbFilterCriteria.Controls.Add(Me.cboFuelLight)
        Me.gbFilterCriteria.Controls.Add(Me.lblFuelLight)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.lblHouseAllowance)
        Me.gbFilterCriteria.Controls.Add(Me.cboHouseAllowance)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrossBasic)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrossBasic)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYE)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(656, 359)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFYear
        '
        Me.lblFYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFYear.Location = New System.Drawing.Point(340, 36)
        Me.lblFYear.Name = "lblFYear"
        Me.lblFYear.Size = New System.Drawing.Size(97, 15)
        Me.lblFYear.TabIndex = 263
        Me.lblFYear.Text = "Financial Year"
        Me.lblFYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFYear
        '
        Me.cboFYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFYear.FormattingEnabled = True
        Me.cboFYear.Location = New System.Drawing.Point(443, 33)
        Me.cboFYear.Name = "cboFYear"
        Me.cboFYear.Size = New System.Drawing.Size(202, 21)
        Me.cboFYear.TabIndex = 12
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.objchkPeriodSelectAll)
        Me.Panel2.Controls.Add(Me.dgPeriod)
        Me.Panel2.Controls.Add(Me.txtSearchPeriod)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(443, 58)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(202, 258)
        Me.Panel2.TabIndex = 13
        '
        'objchkPeriodSelectAll
        '
        Me.objchkPeriodSelectAll.AutoSize = True
        Me.objchkPeriodSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkPeriodSelectAll.Name = "objchkPeriodSelectAll"
        Me.objchkPeriodSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkPeriodSelectAll.TabIndex = 18
        Me.objchkPeriodSelectAll.UseVisualStyleBackColor = True
        '
        'dgPeriod
        '
        Me.dgPeriod.AllowUserToAddRows = False
        Me.dgPeriod.AllowUserToDeleteRows = False
        Me.dgPeriod.AllowUserToResizeRows = False
        Me.dgPeriod.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgPeriod.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgPeriod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgperiodcolhCheck, Me.objdgcolhPeriodunkid, Me.dgColhPeriodCode, Me.dgColhPeriod, Me.dgcolhPeriodStart, Me.dgcolhPeriodEnd})
        Me.dgPeriod.Location = New System.Drawing.Point(0, 28)
        Me.dgPeriod.Name = "dgPeriod"
        Me.dgPeriod.RowHeadersVisible = False
        Me.dgPeriod.RowHeadersWidth = 5
        Me.dgPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPeriod.Size = New System.Drawing.Size(199, 227)
        Me.dgPeriod.TabIndex = 286
        '
        'txtSearchPeriod
        '
        Me.txtSearchPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchPeriod.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchPeriod.Location = New System.Drawing.Point(0, 3)
        Me.txtSearchPeriod.Name = "txtSearchPeriod"
        Me.txtSearchPeriod.Size = New System.Drawing.Size(199, 21)
        Me.txtSearchPeriod.TabIndex = 12
        '
        'cboTaxFreeHouseAllowance
        '
        Me.cboTaxFreeHouseAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxFreeHouseAllowance.FormattingEnabled = True
        Me.cboTaxFreeHouseAllowance.Location = New System.Drawing.Point(129, 330)
        Me.cboTaxFreeHouseAllowance.Name = "cboTaxFreeHouseAllowance"
        Me.cboTaxFreeHouseAllowance.Size = New System.Drawing.Size(188, 21)
        Me.cboTaxFreeHouseAllowance.TabIndex = 11
        '
        'lblTaxFreeHouseAllowance
        '
        Me.lblTaxFreeHouseAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxFreeHouseAllowance.Location = New System.Drawing.Point(9, 332)
        Me.lblTaxFreeHouseAllowance.Name = "lblTaxFreeHouseAllowance"
        Me.lblTaxFreeHouseAllowance.Size = New System.Drawing.Size(114, 15)
        Me.lblTaxFreeHouseAllowance.TabIndex = 258
        Me.lblTaxFreeHouseAllowance.Text = "Tax Free House Allw."
        Me.lblTaxFreeHouseAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPension
        '
        Me.cboPension.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPension.FormattingEnabled = True
        Me.cboPension.Location = New System.Drawing.Point(129, 303)
        Me.cboPension.Name = "cboPension"
        Me.cboPension.Size = New System.Drawing.Size(188, 21)
        Me.cboPension.TabIndex = 10
        '
        'lblPension
        '
        Me.lblPension.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPension.Location = New System.Drawing.Point(9, 305)
        Me.lblPension.Name = "lblPension"
        Me.lblPension.Size = New System.Drawing.Size(114, 15)
        Me.lblPension.TabIndex = 256
        Me.lblPension.Text = "Pension"
        Me.lblPension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGratuity
        '
        Me.cboGratuity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGratuity.FormattingEnabled = True
        Me.cboGratuity.Location = New System.Drawing.Point(129, 276)
        Me.cboGratuity.Name = "cboGratuity"
        Me.cboGratuity.Size = New System.Drawing.Size(188, 21)
        Me.cboGratuity.TabIndex = 9
        '
        'lblGratuity
        '
        Me.lblGratuity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGratuity.Location = New System.Drawing.Point(9, 278)
        Me.lblGratuity.Name = "lblGratuity"
        Me.lblGratuity.Size = New System.Drawing.Size(114, 15)
        Me.lblGratuity.TabIndex = 254
        Me.lblGratuity.Text = "Gratuity"
        Me.lblGratuity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeavePassage
        '
        Me.cboLeavePassage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeavePassage.FormattingEnabled = True
        Me.cboLeavePassage.Location = New System.Drawing.Point(129, 249)
        Me.cboLeavePassage.Name = "cboLeavePassage"
        Me.cboLeavePassage.Size = New System.Drawing.Size(188, 21)
        Me.cboLeavePassage.TabIndex = 8
        '
        'lblLeavePassage
        '
        Me.lblLeavePassage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeavePassage.Location = New System.Drawing.Point(9, 251)
        Me.lblLeavePassage.Name = "lblLeavePassage"
        Me.lblLeavePassage.Size = New System.Drawing.Size(114, 15)
        Me.lblLeavePassage.TabIndex = 252
        Me.lblLeavePassage.Text = "Leave Passage"
        Me.lblLeavePassage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherBenefit
        '
        Me.cboOtherBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherBenefit.FormattingEnabled = True
        Me.cboOtherBenefit.Location = New System.Drawing.Point(129, 222)
        Me.cboOtherBenefit.Name = "cboOtherBenefit"
        Me.cboOtherBenefit.Size = New System.Drawing.Size(188, 21)
        Me.cboOtherBenefit.TabIndex = 7
        '
        'lblOtherBenefit
        '
        Me.lblOtherBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherBenefit.Location = New System.Drawing.Point(9, 224)
        Me.lblOtherBenefit.Name = "lblOtherBenefit"
        Me.lblOtherBenefit.Size = New System.Drawing.Size(114, 15)
        Me.lblOtherBenefit.TabIndex = 250
        Me.lblOtherBenefit.Text = "Other Benefit"
        Me.lblOtherBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRelation
        '
        Me.lblRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRelation.Location = New System.Drawing.Point(8, 63)
        Me.lblRelation.Name = "lblRelation"
        Me.lblRelation.Size = New System.Drawing.Size(114, 15)
        Me.lblRelation.TabIndex = 248
        Me.lblRelation.Text = "Spouse"
        Me.lblRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRelation
        '
        Me.cboRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRelation.FormattingEnabled = True
        Me.cboRelation.Location = New System.Drawing.Point(129, 60)
        Me.cboRelation.Name = "cboRelation"
        Me.cboRelation.Size = New System.Drawing.Size(188, 21)
        Me.cboRelation.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(340, 62)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(97, 15)
        Me.lblPeriod.TabIndex = 244
        Me.lblPeriod.Text = "Deduction"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEduAllowance
        '
        Me.cboEduAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEduAllowance.FormattingEnabled = True
        Me.cboEduAllowance.Location = New System.Drawing.Point(129, 195)
        Me.cboEduAllowance.Name = "cboEduAllowance"
        Me.cboEduAllowance.Size = New System.Drawing.Size(188, 21)
        Me.cboEduAllowance.TabIndex = 6
        '
        'lblEduAllowance
        '
        Me.lblEduAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEduAllowance.Location = New System.Drawing.Point(9, 197)
        Me.lblEduAllowance.Name = "lblEduAllowance"
        Me.lblEduAllowance.Size = New System.Drawing.Size(114, 15)
        Me.lblEduAllowance.TabIndex = 242
        Me.lblEduAllowance.Text = "Educational Allowance"
        Me.lblEduAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFuelLight
        '
        Me.cboFuelLight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFuelLight.FormattingEnabled = True
        Me.cboFuelLight.Location = New System.Drawing.Point(129, 168)
        Me.cboFuelLight.Name = "cboFuelLight"
        Me.cboFuelLight.Size = New System.Drawing.Size(188, 21)
        Me.cboFuelLight.TabIndex = 5
        '
        'lblFuelLight
        '
        Me.lblFuelLight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFuelLight.Location = New System.Drawing.Point(9, 170)
        Me.lblFuelLight.Name = "lblFuelLight"
        Me.lblFuelLight.Size = New System.Drawing.Size(114, 15)
        Me.lblFuelLight.TabIndex = 95
        Me.lblFuelLight.Text = "Fuel, Light, Water"
        Me.lblFuelLight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(443, 322)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 14
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblHouseAllowance
        '
        Me.lblHouseAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHouseAllowance.Location = New System.Drawing.Point(9, 143)
        Me.lblHouseAllowance.Name = "lblHouseAllowance"
        Me.lblHouseAllowance.Size = New System.Drawing.Size(114, 15)
        Me.lblHouseAllowance.TabIndex = 93
        Me.lblHouseAllowance.Text = "Housing Allowance"
        Me.lblHouseAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHouseAllowance
        '
        Me.cboHouseAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHouseAllowance.FormattingEnabled = True
        Me.cboHouseAllowance.Location = New System.Drawing.Point(129, 141)
        Me.cboHouseAllowance.Name = "cboHouseAllowance"
        Me.cboHouseAllowance.Size = New System.Drawing.Size(188, 21)
        Me.cboHouseAllowance.TabIndex = 4
        '
        'lblGrossBasic
        '
        Me.lblGrossBasic.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrossBasic.Location = New System.Drawing.Point(9, 116)
        Me.lblGrossBasic.Name = "lblGrossBasic"
        Me.lblGrossBasic.Size = New System.Drawing.Size(114, 15)
        Me.lblGrossBasic.TabIndex = 90
        Me.lblGrossBasic.Text = "Gross Salary / wages"
        Me.lblGrossBasic.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrossBasic
        '
        Me.cboGrossBasic.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrossBasic.FormattingEnabled = True
        Me.cboGrossBasic.Location = New System.Drawing.Point(129, 114)
        Me.cboGrossBasic.Name = "cboGrossBasic"
        Me.cboGrossBasic.Size = New System.Drawing.Size(188, 21)
        Me.cboGrossBasic.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(114, 15)
        Me.lblEmployee.TabIndex = 88
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(129, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(188, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(547, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 85
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPAYE
        '
        Me.lblPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYE.Location = New System.Drawing.Point(9, 89)
        Me.lblPAYE.Name = "lblPAYE"
        Me.lblPAYE.Size = New System.Drawing.Size(114, 15)
        Me.lblPAYE.TabIndex = 1
        Me.lblPAYE.Text = "P.A.Y.E."
        Me.lblPAYE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYE
        '
        Me.cboPAYE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYE.FormattingEnabled = True
        Me.cboPAYE.Location = New System.Drawing.Point(129, 87)
        Me.cboPAYE.Name = "cboPAYE"
        Me.cboPAYE.Size = New System.Drawing.Size(188, 21)
        Me.cboPAYE.TabIndex = 2
        '
        'objdgperiodcolhCheck
        '
        Me.objdgperiodcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgperiodcolhCheck.HeaderText = ""
        Me.objdgperiodcolhCheck.Name = "objdgperiodcolhCheck"
        Me.objdgperiodcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgperiodcolhCheck.Width = 25
        '
        'objdgcolhPeriodunkid
        '
        Me.objdgcolhPeriodunkid.HeaderText = "periodunkid"
        Me.objdgcolhPeriodunkid.Name = "objdgcolhPeriodunkid"
        Me.objdgcolhPeriodunkid.ReadOnly = True
        Me.objdgcolhPeriodunkid.Visible = False
        '
        'dgColhPeriodCode
        '
        Me.dgColhPeriodCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhPeriodCode.HeaderText = "Code"
        Me.dgColhPeriodCode.Name = "dgColhPeriodCode"
        Me.dgColhPeriodCode.ReadOnly = True
        Me.dgColhPeriodCode.Width = 70
        '
        'dgColhPeriod
        '
        Me.dgColhPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhPeriod.HeaderText = "Period Name"
        Me.dgColhPeriod.Name = "dgColhPeriod"
        Me.dgColhPeriod.ReadOnly = True
        '
        'dgcolhPeriodStart
        '
        Me.dgcolhPeriodStart.HeaderText = "Period Start"
        Me.dgcolhPeriodStart.Name = "dgcolhPeriodStart"
        Me.dgcolhPeriodStart.ReadOnly = True
        Me.dgcolhPeriodStart.Visible = False
        '
        'dgcolhPeriodEnd
        '
        Me.dgcolhPeriodEnd.HeaderText = "Period End"
        Me.dgcolhPeriodEnd.Name = "dgcolhPeriodEnd"
        Me.dgcolhPeriodEnd.ReadOnly = True
        Me.dgcolhPeriodEnd.Visible = False
        '
        'frmPayeDeductionsFormP9Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 486)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmPayeDeductionsFormP9Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmPayeDeductionsFormP9Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPAYE As System.Windows.Forms.Label
    Friend WithEvents cboPAYE As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrossBasic As System.Windows.Forms.Label
    Friend WithEvents cboGrossBasic As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblFuelLight As System.Windows.Forms.Label
    Friend WithEvents cboFuelLight As System.Windows.Forms.ComboBox
    Friend WithEvents lblHouseAllowance As System.Windows.Forms.Label
    Friend WithEvents cboHouseAllowance As System.Windows.Forms.ComboBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboEduAllowance As System.Windows.Forms.ComboBox
    Friend WithEvents lblEduAllowance As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblRelation As System.Windows.Forms.Label
    Friend WithEvents cboRelation As System.Windows.Forms.ComboBox
    Friend WithEvents cboOtherBenefit As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherBenefit As System.Windows.Forms.Label
    Friend WithEvents cboTaxFreeHouseAllowance As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxFreeHouseAllowance As System.Windows.Forms.Label
    Friend WithEvents cboPension As System.Windows.Forms.ComboBox
    Friend WithEvents lblPension As System.Windows.Forms.Label
    Friend WithEvents cboGratuity As System.Windows.Forms.ComboBox
    Friend WithEvents lblGratuity As System.Windows.Forms.Label
    Friend WithEvents cboLeavePassage As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeavePassage As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objchkPeriodSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgPeriod As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblFYear As System.Windows.Forms.Label
    Friend WithEvents cboFYear As System.Windows.Forms.ComboBox
    Friend WithEvents objdgperiodcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriodCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPeriodEnd As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
