﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIncomeTaxDivisionP12
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowEmployeeTINNo = New System.Windows.Forms.CheckBox
        Me.chkShowPayrollNo = New System.Windows.Forms.CheckBox
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboSlabEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblPAYE = New System.Windows.Forms.Label
        Me.cboPAYE = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objchkAllPeriod = New System.Windows.Forms.CheckBox
        Me.lvPeriod = New eZee.Common.eZeeListView(Me.components)
        Me.objColhPCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.objchkAllTranHead = New System.Windows.Forms.CheckBox
        Me.lvDeduction = New eZee.Common.eZeeListView(Me.components)
        Me.objColhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhTranHeadName = New System.Windows.Forms.ColumnHeader
        Me.objcolhTypeOf = New System.Windows.Forms.ColumnHeader
        Me.lblDeduction = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.cboPAYEPeriod = New System.Windows.Forms.ComboBox
        Me.lblPAYEPeriod = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 503)
        Me.NavPanel.Size = New System.Drawing.Size(736, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYEPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYEPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmployeeTINNo)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowPayrollNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboSlabEffectivePeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAllPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lvPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAllTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lvDeduction)
        Me.gbFilterCriteria.Controls.Add(Me.lblDeduction)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(653, 302)
        Me.gbFilterCriteria.TabIndex = 86
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEmployeeTINNo
        '
        Me.chkShowEmployeeTINNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmployeeTINNo.Location = New System.Drawing.Point(375, 267)
        Me.chkShowEmployeeTINNo.Name = "chkShowEmployeeTINNo"
        Me.chkShowEmployeeTINNo.Size = New System.Drawing.Size(234, 17)
        Me.chkShowEmployeeTINNo.TabIndex = 88
        Me.chkShowEmployeeTINNo.Text = "Show Employee TIN No"
        Me.chkShowEmployeeTINNo.UseVisualStyleBackColor = True
        Me.chkShowEmployeeTINNo.Visible = False
        '
        'chkShowPayrollNo
        '
        Me.chkShowPayrollNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPayrollNo.Location = New System.Drawing.Point(375, 244)
        Me.chkShowPayrollNo.Name = "chkShowPayrollNo"
        Me.chkShowPayrollNo.Size = New System.Drawing.Size(140, 17)
        Me.chkShowPayrollNo.TabIndex = 87
        Me.chkShowPayrollNo.Text = "Show Payroll No."
        Me.chkShowPayrollNo.UseVisualStyleBackColor = True
        Me.chkShowPayrollNo.Visible = False
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(375, 132)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(82, 21)
        Me.cboMembership.TabIndex = 85
        Me.cboMembership.Visible = False
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(375, 110)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(69, 15)
        Me.lblMembership.TabIndex = 84
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMembership.Visible = False
        '
        'cboSlabEffectivePeriod
        '
        Me.cboSlabEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSlabEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSlabEffectivePeriod.FormattingEnabled = True
        Me.cboSlabEffectivePeriod.Location = New System.Drawing.Point(462, 33)
        Me.cboSlabEffectivePeriod.Name = "cboSlabEffectivePeriod"
        Me.cboSlabEffectivePeriod.Size = New System.Drawing.Size(174, 21)
        Me.cboSlabEffectivePeriod.TabIndex = 83
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(353, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(103, 15)
        Me.lblPeriod.TabIndex = 82
        Me.lblPeriod.Text = "Effective Periods"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPAYE
        '
        Me.lblPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYE.Location = New System.Drawing.Point(8, 36)
        Me.lblPAYE.Name = "lblPAYE"
        Me.lblPAYE.Size = New System.Drawing.Size(85, 15)
        Me.lblPAYE.TabIndex = 81
        Me.lblPAYE.Text = "P. A. Y. E."
        Me.lblPAYE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYE
        '
        Me.cboPAYE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPAYE.DropDownWidth = 180
        Me.cboPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYE.FormattingEnabled = True
        Me.cboPAYE.Location = New System.Drawing.Point(99, 33)
        Me.cboPAYE.Name = "cboPAYE"
        Me.cboPAYE.Size = New System.Drawing.Size(213, 21)
        Me.cboPAYE.TabIndex = 80
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(549, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 78
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objchkAllPeriod
        '
        Me.objchkAllPeriod.AutoSize = True
        Me.objchkAllPeriod.Location = New System.Drawing.Point(382, 181)
        Me.objchkAllPeriod.Name = "objchkAllPeriod"
        Me.objchkAllPeriod.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllPeriod.TabIndex = 4
        Me.objchkAllPeriod.UseVisualStyleBackColor = True
        Me.objchkAllPeriod.Visible = False
        '
        'lvPeriod
        '
        Me.lvPeriod.BackColorOnChecked = False
        Me.lvPeriod.CheckBoxes = True
        Me.lvPeriod.ColumnHeaders = Nothing
        Me.lvPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhPCheck, Me.colhPeriodName})
        Me.lvPeriod.CompulsoryColumns = ""
        Me.lvPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriod.FullRowSelect = True
        Me.lvPeriod.GridLines = True
        Me.lvPeriod.GroupingColumn = Nothing
        Me.lvPeriod.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriod.HideSelection = False
        Me.lvPeriod.Location = New System.Drawing.Point(375, 177)
        Me.lvPeriod.MinColumnWidth = 50
        Me.lvPeriod.MultiSelect = False
        Me.lvPeriod.Name = "lvPeriod"
        Me.lvPeriod.OptionalColumns = ""
        Me.lvPeriod.ShowMoreItem = False
        Me.lvPeriod.ShowSaveItem = False
        Me.lvPeriod.ShowSelectAll = True
        Me.lvPeriod.ShowSizeAllColumnsToFit = True
        Me.lvPeriod.Size = New System.Drawing.Size(69, 61)
        Me.lvPeriod.Sortable = True
        Me.lvPeriod.TabIndex = 75
        Me.lvPeriod.UseCompatibleStateImageBehavior = False
        Me.lvPeriod.View = System.Windows.Forms.View.Details
        Me.lvPeriod.Visible = False
        '
        'objColhPCheck
        '
        Me.objColhPCheck.Tag = "objColhPCheck"
        Me.objColhPCheck.Text = ""
        Me.objColhPCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Periods"
        Me.colhPeriodName.Width = 205
        '
        'objchkAllTranHead
        '
        Me.objchkAllTranHead.AutoSize = True
        Me.objchkAllTranHead.Location = New System.Drawing.Point(106, 91)
        Me.objchkAllTranHead.Name = "objchkAllTranHead"
        Me.objchkAllTranHead.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllTranHead.TabIndex = 3
        Me.objchkAllTranHead.UseVisualStyleBackColor = True
        '
        'lvDeduction
        '
        Me.lvDeduction.BackColorOnChecked = False
        Me.lvDeduction.CheckBoxes = True
        Me.lvDeduction.ColumnHeaders = Nothing
        Me.lvDeduction.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhCheck, Me.colhTranHeadName, Me.objcolhTypeOf})
        Me.lvDeduction.CompulsoryColumns = ""
        Me.lvDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDeduction.FullRowSelect = True
        Me.lvDeduction.GridLines = True
        Me.lvDeduction.GroupingColumn = Nothing
        Me.lvDeduction.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDeduction.HideSelection = False
        Me.lvDeduction.Location = New System.Drawing.Point(99, 87)
        Me.lvDeduction.MinColumnWidth = 50
        Me.lvDeduction.MultiSelect = False
        Me.lvDeduction.Name = "lvDeduction"
        Me.lvDeduction.OptionalColumns = ""
        Me.lvDeduction.ShowMoreItem = False
        Me.lvDeduction.ShowSaveItem = False
        Me.lvDeduction.ShowSelectAll = True
        Me.lvDeduction.ShowSizeAllColumnsToFit = True
        Me.lvDeduction.Size = New System.Drawing.Size(240, 203)
        Me.lvDeduction.Sortable = True
        Me.lvDeduction.TabIndex = 60
        Me.lvDeduction.UseCompatibleStateImageBehavior = False
        Me.lvDeduction.View = System.Windows.Forms.View.Details
        '
        'objColhCheck
        '
        Me.objColhCheck.Tag = "objColhCheck"
        Me.objColhCheck.Text = ""
        Me.objColhCheck.Width = 25
        '
        'colhTranHeadName
        '
        Me.colhTranHeadName.Tag = "colhTranHeadName"
        Me.colhTranHeadName.Text = "Transaction Head"
        Me.colhTranHeadName.Width = 209
        '
        'objcolhTypeOf
        '
        Me.objcolhTypeOf.Tag = "objcolhTypeOf"
        Me.objcolhTypeOf.Text = ""
        Me.objcolhTypeOf.Width = 0
        '
        'lblDeduction
        '
        Me.lblDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeduction.Location = New System.Drawing.Point(8, 91)
        Me.lblDeduction.Name = "lblDeduction"
        Me.lblDeduction.Size = New System.Drawing.Size(85, 15)
        Me.lblDeduction.TabIndex = 59
        Me.lblDeduction.Text = "Deduction"
        Me.lblDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(318, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 56
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(99, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(213, 21)
        Me.cboEmployee.TabIndex = 55
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(85, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(12, 374)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(653, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 87
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(318, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(289, 3)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 3)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 56
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYEPeriod
        '
        Me.cboPAYEPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPAYEPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYEPeriod.FormattingEnabled = True
        Me.cboPAYEPeriod.Location = New System.Drawing.Point(462, 60)
        Me.cboPAYEPeriod.Name = "cboPAYEPeriod"
        Me.cboPAYEPeriod.Size = New System.Drawing.Size(174, 21)
        Me.cboPAYEPeriod.TabIndex = 91
        '
        'lblPAYEPeriod
        '
        Me.lblPAYEPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYEPeriod.Location = New System.Drawing.Point(353, 63)
        Me.lblPAYEPeriod.Name = "lblPAYEPeriod"
        Me.lblPAYEPeriod.Size = New System.Drawing.Size(103, 15)
        Me.lblPAYEPeriod.TabIndex = 90
        Me.lblPAYEPeriod.Text = "P.A.Y.E. Period"
        Me.lblPAYEPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmIncomeTaxDivisionP12
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(736, 558)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Name = "frmIncomeTaxDivisionP12"
        Me.Text = "frmIncomeTaxDivisionP12"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbBasicSalaryOtherEarning, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkShowEmployeeTINNo As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowPayrollNo As System.Windows.Forms.CheckBox
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboSlabEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPAYE As System.Windows.Forms.Label
    Friend WithEvents cboPAYE As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objchkAllPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents lvPeriod As eZee.Common.eZeeListView
    Friend WithEvents objColhPCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAllTranHead As System.Windows.Forms.CheckBox
    Friend WithEvents lvDeduction As eZee.Common.eZeeListView
    Friend WithEvents objColhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHeadName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTypeOf As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblDeduction As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents cboPAYEPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPAYEPeriod As System.Windows.Forms.Label
End Class
