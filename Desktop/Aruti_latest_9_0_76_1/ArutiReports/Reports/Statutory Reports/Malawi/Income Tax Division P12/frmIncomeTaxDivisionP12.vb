'************************************************************************************************************************************
'Class Name : frmIncomeTaxDivisionP12.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmIncomeTaxDivisionP12

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmIncomeTaxDivisionP12"
    Private objTaxReport As clsIncomeTaxDivisionP12Report

    Private mstrPeriodsName As String = ""


    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        objTaxReport = New clsIncomeTaxDivisionP12Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTaxReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMembership As New clsmembership_master

        Dim dsCombos As DataSet
        Dim lvItem As ListViewItem

        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Heads", False, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            'Sohail (21 Aug 2015) -- End
            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                If dsCombos.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With
            dsCombos = objTranHead.GetHeadListTypeWise(enTranHeadType.EmployeesStatutoryDeductions, enTypeOf.Employee_Statutory_Contributions)
            lvDeduction.Items.Clear()
            For Each dtRow As DataRow In dsCombos.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.SubItems.Add(dtRow.Item("TypeOf").ToString)
                lvItem.Tag = dtRow.Item("Id")
                lvDeduction.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvDeduction.GroupingColumn = objcolhTypeOf
            lvDeduction.DisplayGroups(True)

            If lvDeduction.Items.Count > 8 Then
                colhTranHeadName.Width = 205 - 18
            Else
                colhTranHeadName.Width = 205
            End If

            dsCombos = objMembership.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objTranHead = Nothing
            objMembership = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0
            chkShowPayrollNo.Checked = False
            chkShowEmployeeTINNo.Checked = False
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport

            Call DoOperation(lvDeduction, False)
            Call DoOperation(lvPeriod, False)

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objTaxReport.SetDefaultValue()

            mstrPeriodsName = String.Empty

            If cboPAYE.Items.Count <= 0 OrElse CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select PAYE head."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            ElseIf CInt(cboSlabEffectivePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Effective Period is mandatory information. Please select Effective Period."), enMsgBoxStyle.Information)
                cboSlabEffectivePeriod.Focus()
                Return False
            ElseIf lvDeduction.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one Employee statutary deduction head."), enMsgBoxStyle.Information)
                lvDeduction.Focus()
                Return False
                'ElseIf lvPeriod.CheckedItems.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check Periods to view report."), enMsgBoxStyle.Information)
                '    lvPeriod.Focus()
                '    Return False
            ElseIf CInt(cboPAYEPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Period to view report."), enMsgBoxStyle.Information)
                cboPAYEPeriod.Focus()
                Return False
            End If

            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If

            objTaxReport._EmpId = cboEmployee.SelectedValue
            objTaxReport._EmpName = cboEmployee.Text


            Dim strDeductionHeadIds As String = String.Join(",", (From p In lvDeduction.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            'Call GetCommaSeparatedTags(lvDeduction, strSatutaryIds)
            objTaxReport._DeductionHeadIds = strDeductionHeadIds

            Dim strPreriodIDs As String = String.Join(",", (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
            'Call GetCommaSeparatedTags(lvPeriod, strPreriodIds)
            objTaxReport._PeriodIds = strPreriodIDs

            'mstrPeriodsName = String.Join(",", (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhPeriodName.Index).Text.ToString)).ToArray)
            'objTaxReport._PeriodNames = mstrPeriodsName

            objTaxReport._PeriodIds = cboPAYEPeriod.SelectedValue.ToString
            objTaxReport._PeriodNames = cboPAYEPeriod.Text

            objTaxReport._PeriodStartDate = eZeeDate.convertDate(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
            objTaxReport._PeriodEndDate = eZeeDate.convertDate(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Dim objMaster As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTaxReport._YearName = objMaster.GetFinancialYear_Name(CInt(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("yearunkid").ToString))
            objTaxReport._YearName = objMaster.GetFinancialYear_Name(CInt(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("yearunkid").ToString), Company._Object._Companyunkid)
            'S.SANDEEP [04 JUN 2015] -- END


            objTaxReport._PayeHeadId = CInt(cboPAYE.SelectedValue)
            objTaxReport._PayeHeadName = cboPAYE.Text

            objTaxReport._MembershipId = CInt(cboMembership.SelectedValue)
            objTaxReport._MembershipName = cboMembership.Text

            If gbBasicSalaryOtherEarning.Checked = True Then
                objTaxReport._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objTaxReport._OtherEarningTranId = 0
            End If

            objTaxReport._ShowPayrollNo = chkShowPayrollNo.Checked
            objTaxReport._ShowEmployeeTINNo = chkShowEmployeeTINNo.Checked

            objTaxReport._ViewByIds = mstrStringIds
            objTaxReport._ViewIndex = mintViewIdx
            objTaxReport._ViewByName = mstrStringName
            objTaxReport._Analysis_Fields = mstrAnalysis_Fields
            objTaxReport._Analysis_Join = mstrAnalysis_Join
            objTaxReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTaxReport._Report_GroupName = mstrReport_GroupName

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objTaxReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            'Sohail (03 Aug 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub DoOperation(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub GetCommaSeparatedTags(ByVal LV As ListView, ByRef mstrIds As String)
        Try
            Dim iCnt As Integer = 1
            For Each Item As ListViewItem In LV.CheckedItems
                mstrIds &= "," & Item.Tag.ToString
                If LV.Name.ToUpper = "LVPERIOD" Then
                    mstrPeriodsName &= ", " & Item.SubItems(colhPeriodName.Index).Text.ToString
                    If iCnt = LV.CheckedItems.Count Then
                        mdtPeriodEndDate = eZeeDate.convertDate(Item.SubItems(colhPeriodName.Index).Tag.ToString)
                    End If
                    iCnt += 1
                End If
            Next
            mstrIds = mstrIds.Substring(1)
            If LV.Name.ToUpper = "LVPERIOD" Then
                mstrPeriodsName = mstrPeriodsName.Substring(2)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommaSeparatedTags", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmIncomeTaxDivisionP12_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTaxReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIncomeTaxDivisionP12_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIncomeTaxDivisionP12_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIncomeTaxDivisionP12_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objTaxReport._ReportName
            Me._Message = objTaxReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIncomeTaxDivisionP12_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTaxReport.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, _
            '                               ConfigParameter._Object._Base_CurrencyId)
            objTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("start_date").ToString), _
                                           eZeeDate.convertDate(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("end_date").ToString), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, _
                                           ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTaxReport.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, _
            '                               ConfigParameter._Object._Base_CurrencyId)
            objTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("start_date").ToString), _
                                           eZeeDate.convertDate(CType(cboPAYEPeriod.SelectedItem, DataRowView).Item("end_date").ToString), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, _
                                           ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsIncomeTaxDivisionP12Report.SetMessages()
            objfrm._Other_ModuleNames = "clsIncomeTaxDivisionP12Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call DoOperation(lvPeriod, CBool(objchkAllPeriod.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAllTranHead_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllTranHead.CheckedChanged
        Try
            Call DoOperation(lvDeduction, CBool(objchkAllTranHead.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllTranHead_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvDeduction_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDeduction.ItemChecked
        Try
            RemoveHandler objchkAllTranHead.CheckedChanged, AddressOf objchkAllTranHead_CheckedChanged
            If lvDeduction.CheckedItems.Count <= 0 Then
                objchkAllTranHead.CheckState = CheckState.Unchecked
            ElseIf lvDeduction.CheckedItems.Count < lvDeduction.Items.Count Then
                objchkAllTranHead.CheckState = CheckState.Indeterminate
            ElseIf lvDeduction.CheckedItems.Count = lvDeduction.Items.Count Then
                objchkAllTranHead.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllTranHead.CheckedChanged, AddressOf objchkAllTranHead_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDeduction_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub cboPAYE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPAYE.SelectedIndexChanged
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Try
            If CInt(cboPAYE.SelectedValue) <= 0 Then Exit Try

            Dim objHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objHead._Tranheadunkid = CInt(cboPAYE.SelectedValue)
            objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboPAYE.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objHead._Calctype_Id = enCalcType.AsComputedValue Then
                Dim objSimpleSlab As New clsTranheadSlabTran
                dsCombos = objSimpleSlab.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
                objSimpleSlab = Nothing
            ElseIf objHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
            Else
                Dim objMaster As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim intFirstPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, , , True)
                Dim intFirstPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, , , True)
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Slab", True, 0)
                dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Slab", True, 0)
                'Sohail (21 Aug 2015) -- End
                Dim dtTable As DataTable = New DataView(dsCombos.Tables("Slab"), "periodunkid IN (0, " & intFirstPeriod & ")", "", DataViewRowState.CurrentRows).ToTable
                dsCombos.Tables.Clear()
                dsCombos.Tables.Add(dtTable)
            End If

            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPAYE_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End


                Dim objHead As New clsTransactionHead
                Dim ds As DataSet
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objHead._Tranheadunkid = CInt(cboPAYE.SelectedValue)
                objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboPAYE.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                If objHead._Calctype_Id = enCalcType.AsComputedValue Then
                    Dim objSimpleSlab As New clsTranheadSlabTran
                    ds = objSimpleSlab.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
                    objSimpleSlab = Nothing
                ElseIf objHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                    ds = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
                Else
                    Dim objPeriodTmp As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'ds = objPeriodTmp.getListForCombo(enModuleReference.Payroll, 0, "Slab", False)
                    ds = objPeriodTmp.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Slab", False)
                    'Sohail (21 Aug 2015) -- End
                End If

                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 And objHead._Calctype_Id <> enCalcType.FlatRate_Others Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
                dtTable.Rows.Clear()
            End If

            lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("periodunkid")
                lvItem.SubItems(colhPeriodName.Index).Tag = dtRow.Item("end_date").ToString
                lvPeriod.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvPeriod.GridLines = False

            If lvPeriod.Items.Count > 12 Then
                colhPeriodName.Width = 205 - 18
            Else
                colhPeriodName.Width = 205
            End If

            Dim mintFirstPeiodid As Integer = -1
            Dim objMaster As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstPeiodid = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1)
            mintFirstPeiodid = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPAYEPeriod
                .DataSource = Nothing
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                If dtTable.Select("periodunkid = " & mintFirstPeiodid).Length > 0 Then
                    .SelectedValue = mintFirstPeiodid
                Else
                If .Items.Count > 0 Then .SelectedIndex = 0
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
            Me.colhTranHeadName.Text = Language._Object.getCaption(CStr(Me.colhTranHeadName.Tag), Me.colhTranHeadName.Text)
            Me.lblDeduction.Text = Language._Object.getCaption(Me.lblDeduction.Name, Me.lblDeduction.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPAYE.Text = Language._Object.getCaption(Me.lblPAYE.Name, Me.lblPAYE.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
            Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select PAYE head.")
            Language.setMessage(mstrModuleName, 2, "Effective Period is mandatory information. Please select Effective Period.")
            Language.setMessage(mstrModuleName, 3, "Please check atleast one Employee statutary deduction head.")
            Language.setMessage(mstrModuleName, 4, "Please check Periods to view report.")
            Language.setMessage(mstrModuleName, 5, "Please select transaction head.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
