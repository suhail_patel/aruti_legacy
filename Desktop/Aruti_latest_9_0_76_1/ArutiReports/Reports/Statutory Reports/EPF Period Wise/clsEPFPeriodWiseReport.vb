'************************************************************************************************************************************
'Class Name :clsEPFPeriodWiseReport.vb
'Purpose    :
'Date       :12 Aug 2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsEPFPeriodWiseReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEPFPeriodWiseReport"
    Private mstrReportId As String = enArutiReport.EPF_PeriodWise_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub
#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mblnShowBasicSalary As Boolean = True
    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrEmployeeIds As String = String.Empty
    Private mintFromPeriodId As Integer = 0
    Private mstrFromPeriodName As String = String.Empty
    Private mintToPeriodId As Integer = 0
    Private mstrToPeriodName As String = String.Empty
    Private mintOtherEarningTranId As Integer = 0
    Private mstrPeriodIds As String = String.Empty
    'Sohail (19 Mar 2015) -- Start
    'Enhancement - Providing closed year to date period on EPF Period Wise Report.
    Private marrDatabaseName As New ArrayList
    Private marrAllDatabaseName As New ArrayList
    Private mstrTranDatabaseName As String = FinancialYear._Object._DatabaseName
    Private mstrUserAccessLevelFilterString As String = ""
    'Sohail (19 Mar 2015) -- End

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mDicDataBaseYearId As Dictionary(Of String, Integer)
    Private mDicDataBaseEndDate As Dictionary(Of String, String)
    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (03 Aug 2019) -- Start
    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    'Sohail (03 Aug 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ShowBasicSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBasicSalary = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeIds() As String
        Set(ByVal value As String)
            mstrEmployeeIds = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    'Sohail (19 Mar 2015) -- Start
    'Enhancement - Providing closed year to date period on EPF Period Wise Report.
    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Arr_AllDatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrAllDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessLevelFilterString() As String
        Set(ByVal value As String)
            mstrUserAccessLevelFilterString = value
        End Set
    End Property

    Public WriteOnly Property _TranDatabaseName() As String
        Set(ByVal value As String)
            mstrTranDatabaseName = value
        End Set
    End Property
    'Sohail (19 Mar 2015) -- End


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public WriteOnly Property _DicDataBaseYearId() As Dictionary(Of String, Integer)
        Set(ByVal value As Dictionary(Of String, Integer))
            mDicDataBaseYearId = value
        End Set
    End Property

    Public WriteOnly Property _DicDataBaseEndDate() As Dictionary(Of String, String)
        Set(ByVal value As Dictionary(Of String, String))
            mDicDataBaseEndDate = value
        End Set
    End Property
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 1
            mstrMembershipName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mblnShowBasicSalary = True
            mstrExchangeRate = ""
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mblnFirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            mstrEmployeeIds = String.Empty
            mintFromPeriodId = 0
            mstrFromPeriodName = String.Empty
            mintToPeriodId = 0
            mstrToPeriodName = String.Empty
            mintOtherEarningTranId = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mDicDataBaseYearId = Nothing
            mDicDataBaseEndDate = Nothing
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            mdtPeriodStart = xPeriodStart
            mdtPeriodEnd = xPeriodEnd
            'Sohail (03 Aug 2019) -- End

            objRpt = Generate_DetailReport(xUserUnkid, xCompanyUnkid, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal intUserUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim iECode As String = String.Empty
        Dim strDatabaseName As String = ""
        Dim StrI As String = ""
        Try

            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT  employeecode  " & _
                              ", MemshipNo " & _
                              ", empcontribution " & _
                              ", employercontribution " & _
                              ", Amount " & _
                              ", Remark " & _
                              ", PeriodName " & _
                              ", EmpId " & _
                              ", leaving_date " & _
                              ", PId " & _
                              ", EMembershiptranunkid " & _
                              ", CMembershiptranunkid " & _
                              ", EName " & _
                              ", end_date " & _
                              ", receiptno " & _
                              ", rcpt.receiptdate " & _
                              ", employer_membershipno " & _
                        "FROM    ( "
                'Sohail (05 Feb 2016) - [employer_membershipno]

                For i = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrI &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "SELECT  hremployee_master.employeecode AS employeecode " & _
                                  ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                                  ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS empcontribution " & _
                                  ", ISNULL(CAmount, 0) AS employercontribution " & _
                                  ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) + ISNULL(CAmount, 0) AS Amount  " & _
                                  ", '' AS Remark " & _
                                  ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                  ", hremployee_master.employeeunkid AS EmpId " & _
                                  ", ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) , '') AS leaving_date " & _
                                  ", cfcommon_period_tran.periodunkid AS PId " & _
                                  ", prpayrollprocess_tran.membershiptranunkid AS EMembershiptranunkid " & _
                                  ", c.membershiptranunkid AS CMembershiptranunkid " & _
                                  ", cfcommon_period_tran.end_date " & _
                                  ", ISNULL(hrmembership_master.employer_membershipno, '') AS employer_membershipno "
                    'Sohail (05 Feb 2016) - [employer_membershipno]

                    If mblnFirstNamethenSurname = True Then
                        StrI &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                    Else
                        StrI &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                    End If

                    StrI &= "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                      "/*AND ISNULL(cfcommon_period_tran.isactive , 0) = 1*/ " & _
                                    "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId  " & _
                                                      ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS CAmount " & _
                                                      ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                                                      ", prpayrollprocess_tran.membershiptranunkid " & _
                                                "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                        "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                         "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                          "/*AND ISNULL(cfcommon_period_tran.isactive, 0) = 1*/ "
                    'Sohail (18 Nov 2019) - Commented [AND ISNULL(cfcommon_period_tran.isactive , 0) = 1]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                        "AND hrmembership_master.membershipunkid = @MemId "
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid,  prtnaleave_tran.payperiodunkid,  prpayrollprocess_tran.membershiptranunkid "
                    'Sohail (11 Mar 2020) -- End

                    StrI &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                                        "AND prtnaleave_tran.payperiodunkid = C.iCPrdId "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND cfcommon_period_tran.isactive = 1 " & _
                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                    "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                    "AND hrmembership_master.membershipunkid = @MemId "
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid, hremployee_master.employeecode, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') " & _
                            ",ISNULL(hremployee_meminfo_tran.membershipno, ''), C.CAmount, ISNULL(cfcommon_period_tran.period_name, ''), ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), '') " & _
                            ", cfcommon_period_tran.periodunkid,cfcommon_period_tran.end_date,  prpayrollprocess_tran.membershiptranunkid,c.membershiptranunkid, ISNULL(hrmembership_master.employer_membershipno, '') "
                    'Sohail (11 Mar 2020) -- End

                Next

                StrQ &= StrI
                StrI = ""

                StrQ &= " ) AS A "

                StrQ &= "LEFT JOIN ( "

                For j = 0 To marrAllDatabaseName.Count - 1
                    strDatabaseName = marrAllDatabaseName(j)

                    If j > 0 Then
                        StrI &= " UNION "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "SELECT  prstatutorypayment_tran.periodunkid  " & _
                                                      ", prstatutorypayment_tran.employeeunkid " & _
                                                      ", prstatutorypayment_tran.membershiptranunkid " & _
                                                      ", prstatutorypayment_master.receiptno " & _
                                                      ", CONVERT(CHAR(8), ISNULL(prstatutorypayment_master.receiptdate, ''), 112) AS receiptdate " & _
                                                "FROM    " & strDatabaseName & "..prstatutorypayment_master " & _
                                                        "LEFT JOIN " & strDatabaseName & "..prstatutorypayment_tran ON prstatutorypayment_master.statutorypaymentmasterunkid = prstatutorypayment_tran.statutorypaymentmasterunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hremployee_master ON prstatutorypayment_tran.employeeunkid = hremployee_master.employeeunkid "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "                    WHERE   ISNULL(prstatutorypayment_master.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prstatutorypayment_tran.isvoid, 0) = 0 " & _
                                                        "AND prstatutorypayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND prstatutorypayment_tran.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                        "AND prstatutorypayment_master.membershipunkid = @MemId "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                Next

                StrQ &= StrI
                StrI = ""

                StrQ &= ") AS rcpt ON rcpt.periodunkid = A.PId " & _
                               "AND rcpt.employeeunkid = A.EmpId " & _
                               "AND rcpt.membershiptranunkid = A.EMembershiptranunkid "

                If mblnFirstNamethenSurname = True Then
                    'Sohail (16 Apr 2020) -- Start
                    'Issue : EPF Period wise report not coming in period order.
                    'StrQ &= " ORDER BY EName "
                    StrQ &= " ORDER BY EName, end_date "
                    'Sohail (16 Apr 2020) -- End
                Else
                    StrQ &= " ORDER BY EName, end_date "
                End If


            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT  employeecode  " & _
                              ", MemshipNo " & _
                              ", empcontribution " & _
                              ", employercontribution " & _
                              ", Amount " & _
                              ", Remark " & _
                              ", PeriodName " & _
                              ", EmpId " & _
                              ", leaving_date " & _
                              ", PId " & _
                              ", EMembershiptranunkid " & _
                              ", CMembershiptranunkid " & _
                              ", EName " & _
                              ", end_date " & _
                              ", receiptno " & _
                              ", rcpt.receiptdate " & _
                              ", employer_membershipno " & _
                        "FROM    ( "
                'Sohail (05 Feb 2016) - [employer_membershipno]

                For i = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrI &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "SELECT hremployee_master.employeecode AS employeecode " & _
                                  ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                                  ", SUM(( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS empcontribution " & _
                                  ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * ISNULL(CAmount, 0) AS employercontribution " & _
                                  ", SUM(( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) + ISNULL(CAmount, 0) ) AS Amount " & _
                                  ", '' AS Remark " & _
                                  ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                  ", hremployee_master.employeeunkid AS EmpId " & _
                                  ", ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) , '') AS leaving_date " & _
                                  ", cfcommon_period_tran.periodunkid AS PId " & _
                                  ", prpayrollprocess_tran.membershiptranunkid AS EMembershiptranunkid " & _
                                  ", c.membershiptranunkid AS CMembershiptranunkid " & _
                                  ", cfcommon_period_tran.end_date " & _
                                  ", ISNULL(hrmembership_master.employer_membershipno, '') AS employer_membershipno "
                    'Sohail (05 Feb 2016) - [employer_membershipno]

                    If mblnFirstNamethenSurname = True Then
                        StrI &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                    Else
                        StrI &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                    End If

                    StrI &= "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                      "/*AND ISNULL(cfcommon_period_tran.isactive, 0) = 1*/ " & _
                                    "JOIN " & strDatabaseName & "..prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                           "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                           "AND prpayment_tran.isvoid = 0 " & _
                                                           "AND cfcommon_period_tran.isactive = 1 " & _
                                                           "AND prpayment_tran.countryunkid = " & mintCountryId & " "
                    'Sohail (18 Nov 2019) - Commented [AND ISNULL(cfcommon_period_tran.isactive , 0) = 1]
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End


                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                    '                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                    '                                  ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                    '                                  ", prpayrollprocess_tran.membershiptranunkid " & _
                    '                            "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                    '                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                    '                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                    '                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                    '                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                    '                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    '                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                    '                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "
                    StrI &= "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                      ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS CAmount " & _
                                                      ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                                                      ", prpayrollprocess_tran.membershiptranunkid " & _
                                                "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                        "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                         "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                          "/*AND ISNULL(cfcommon_period_tran.isactive, 0) = 1*/ "
                    'Sohail (18 Nov 2019) - Commented [AND ISNULL(cfcommon_period_tran.isactive , 0) = 1]
                    'Sohail (12 Dec 2017) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                        "AND hrmembership_master.membershipunkid = @MemId "
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid,  prtnaleave_tran.payperiodunkid,  prpayrollprocess_tran.membershiptranunkid "
                    'Sohail (11 Mar 2020) -- End

                    StrI &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                                        "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
                                                        "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                                "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                                "AND hrmembership_master.membershipunkid = @MemId " & _
                                                                "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                                                                "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid, hremployee_master.employeecode, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') " & _
                            ",ISNULL(hremployee_meminfo_tran.membershipno, ''), C.CAmount, ISNULL(cfcommon_period_tran.period_name, ''), ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), '') " & _
                            ", cfcommon_period_tran.periodunkid,cfcommon_period_tran.end_date,  prpayrollprocess_tran.membershiptranunkid,c.membershiptranunkid, ISNULL(hrmembership_master.employer_membershipno, '') "
                    'Sohail (11 Mar 2020) -- End

                    StrI &= "      UNION ALL " & _
                            "SELECT  hremployee_master.employeecode AS employeecode " & _
                                  ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                                  ", SUM(" & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS empcontribution " & _
                                  ", " & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
                                  ", SUM(" & mdecConversionRate & " * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) + ISNULL(CAmount, 0) ) AS Amount " & _
                                  ", '' AS Remark " & _
                                  ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                  ", hremployee_master.employeeunkid AS EmpId " & _
                                  ", ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) , '') AS leaving_date " & _
                                  ", cfcommon_period_tran.periodunkid AS PId " & _
                                  ", prpayrollprocess_tran.membershiptranunkid AS EMembershiptranunkid " & _
                                  ", c.membershiptranunkid AS CMembershiptranunkid " & _
                                  ", cfcommon_period_tran.end_date " & _
                                  ", ISNULL(hrmembership_master.employer_membershipno, '') AS employer_membershipno "
                    'Sohail (05 Feb 2016) - [employer_membershipno]

                    If mblnFirstNamethenSurname = True Then
                        StrI &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                    Else
                        StrI &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                    End If

                    StrI &= "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                      "/*AND ISNULL(cfcommon_period_tran.isactive, 0) = 1*/ " & _
                                    "JOIN " & strDatabaseName & "..prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                           "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                           "AND prpayment_tran.isvoid = 0 " & _
                                                           "AND cfcommon_period_tran.isactive = 1 " & _
                                                           "AND prpayment_tran.countryunkid <> " & mintCountryId & " "
                    'Sohail (18 Nov 2019) - Commented [AND ISNULL(cfcommon_period_tran.isactive , 0) = 1]
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End


                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                    '                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                    '                                  ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                    '                                  ", prpayrollprocess_tran.membershiptranunkid " & _
                    '                            "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                    '                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                    '                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                    '                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                    '                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                    '                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    '                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                    '                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "
                    StrI &= "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                      ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS CAmount " & _
                                                      ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                                                      ", prpayrollprocess_tran.membershiptranunkid " & _
                                                "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                        "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                         "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                          "/*AND ISNULL(cfcommon_period_tran.isactive, 0) = 1*/ "
                    'Sohail (18 Nov 2019) - Commented [AND ISNULL(cfcommon_period_tran.isactive , 0) = 1]
                    'Sohail (12 Dec 2017) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                        "AND hrmembership_master.membershipunkid = @MemId "
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid,  prtnaleave_tran.payperiodunkid,  prpayrollprocess_tran.membershiptranunkid "
                    'Sohail (11 Mar 2020) -- End

                    StrI &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                                        "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
                            "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                    "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                    "AND hrmembership_master.membershipunkid = @MemId " & _
                                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                                    "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid, hremployee_master.employeecode, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') " & _
                            ",ISNULL(hremployee_meminfo_tran.membershipno, ''), C.CAmount, ISNULL(cfcommon_period_tran.period_name, ''), ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), '') " & _
                            ", cfcommon_period_tran.periodunkid,cfcommon_period_tran.end_date,  prpayrollprocess_tran.membershiptranunkid,c.membershiptranunkid, ISNULL(hrmembership_master.employer_membershipno, '') "
                    'Sohail (11 Mar 2020) -- End

                    StrI &= "UNION ALL " & _
                            "SELECT  hremployee_master.employeecode AS employeecode " & _
                                  ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                                  ", SUM(" & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS empcontribution " & _
                                  ", " & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
                                  ", SUM(" & mdecConversionRate & " * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) + ISNULL(CAmount, 0) ) AS Amount " & _
                                  ", '' AS Remark " & _
                                  ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                  ", hremployee_master.employeeunkid AS EmpId " & _
                                  ", ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) , '') AS leaving_date " & _
                                  ", cfcommon_period_tran.periodunkid AS PId " & _
                                  ", prpayrollprocess_tran.membershiptranunkid AS EMembershiptranunkid " & _
                                  ", c.membershiptranunkid AS CMembershiptranunkid " & _
                                  ", cfcommon_period_tran.end_date " & _
                                  ", ISNULL(hrmembership_master.employer_membershipno, '') AS employer_membershipno "
                    'Sohail (05 Feb 2016) - [employer_membershipno]

                    If mblnFirstNamethenSurname = True Then
                        StrI &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                    Else
                        StrI &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                    End If

                    StrI &= "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                      "AND cfcommon_period_tran.isactive = 1 " & _
                                    "LEFT JOIN " & strDatabaseName & "..prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                                "AND prpayment_tran.isvoid = 0 "

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End


                    'Sohail (12 Dec 2017) -- Start
                    'SUPPORT 1468 - Silverlands | Some employees not showing in NSSF report. Removing isdeleted filter from all statutory reports in 70.1.
                    'StrQ &= "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                    '                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                    '                                  ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                    '                                  ", prpayrollprocess_tran.membershiptranunkid " & _
                    '                            "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                    '                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                                    "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                    '                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                    '                                    "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                    '                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                    '                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    '                                    "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                    '                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "
                    StrI &= "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                      ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS CAmount " & _
                                                      ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
                                                      ", prpayrollprocess_tran.membershiptranunkid " & _
                                                "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                         "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                          "/*AND ISNULL(cfcommon_period_tran.isactive, 0) = 1*/ "
                    'Sohail (18 Nov 2019) - Commented [AND ISNULL(cfcommon_period_tran.isactive , 0) = 1]
                    'Sohail (12 Dec 2017) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                        "AND hrmembership_master.membershipunkid = @MemId "
                    'Sohail (18 Nov 2019) - [AND cfcommon_period_tran.isactive = 1]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid,  prtnaleave_tran.payperiodunkid,  prpayrollprocess_tran.membershiptranunkid "
                    'Sohail (11 Mar 2020) -- End

                    StrI &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                                        "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
                                            "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                    "AND hrmembership_master.membershipunkid = @MemId " & _
                                                    "AND prpayment_tran.paymenttranunkid IS NULL "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    StrI &= " GROUP BY hremployee_master.employeeunkid, hremployee_master.employeecode, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') " & _
                            ",ISNULL(hremployee_meminfo_tran.membershipno, ''), C.CAmount, ISNULL(cfcommon_period_tran.period_name, ''), ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), '') " & _
                            ", cfcommon_period_tran.periodunkid,cfcommon_period_tran.end_date,  prpayrollprocess_tran.membershiptranunkid,c.membershiptranunkid, ISNULL(hrmembership_master.employer_membershipno, '') "
                    'Sohail (11 Mar 2020) -- End

                Next

                StrQ &= StrI
                StrI = ""

                StrQ &= " ) AS A "

                StrQ &= "LEFT JOIN ( "

                For j = 0 To marrAllDatabaseName.Count - 1
                    strDatabaseName = marrAllDatabaseName(j)

                    If j > 0 Then
                        StrI &= " UNION "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "SELECT  prstatutorypayment_tran.periodunkid  " & _
                                                      ", prstatutorypayment_tran.employeeunkid " & _
                                                      ", prstatutorypayment_tran.membershiptranunkid " & _
                                                      ", prstatutorypayment_master.receiptno " & _
                                                      ", CONVERT(CHAR(8), ISNULL(prstatutorypayment_master.receiptdate, ''), 112) AS receiptdate " & _
                                                "FROM    " & strDatabaseName & "..prstatutorypayment_master " & _
                                                        "LEFT JOIN " & strDatabaseName & "..prstatutorypayment_tran ON prstatutorypayment_master.statutorypaymentmasterunkid = prstatutorypayment_tran.statutorypaymentmasterunkid " & _
                                                        "LEFT JOIN " & strDatabaseName & "..hremployee_master ON prstatutorypayment_tran.employeeunkid = hremployee_master.employeeunkid "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "                    WHERE   ISNULL(prstatutorypayment_master.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prstatutorypayment_tran.isvoid, 0) = 0 " & _
                                                        "AND prstatutorypayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND prstatutorypayment_tran.employeeunkid IN (" & mstrEmployeeIds & ") " & _
                                                        "AND prstatutorypayment_master.membershipunkid = @MemId "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                Next

                StrQ &= StrI
                StrI = ""

                StrQ &= ") AS rcpt ON rcpt.periodunkid = A.PId " & _
                               "AND rcpt.employeeunkid = A.EmpId " & _
                               "AND rcpt.membershiptranunkid = A.EMembershiptranunkid "

                If mblnFirstNamethenSurname = True Then
                    'Sohail (16 Apr 2020) -- Start
                    'Issue : EPF Period wise report not coming in period order.
                    'StrQ &= " ORDER BY EName "
                    StrQ &= " ORDER BY EName, end_date "
                    'Sohail (16 Apr 2020) -- End
                Else
                    StrQ &= " ORDER BY EName, end_date "
                End If

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrI = ""
            StrQ = ""

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                       "	 A.Empid " & _
                       "	, ISNULL(BasicSal, 0) AS BasicSal " & _
                       "	, ISNULL(GrossPay, 0) AS GrossPay  " & _
                       "    , A.PrdId " & _
                       "FROM " & _
                       "( "

                For i = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrI &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    StrI &= "	SELECT " & _
                           "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                           "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                           "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                                          "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                                    "JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                    "JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                           "		JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "	WHERE       ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                            "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                            "AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "

                    If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                        StrI &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                    Else
                        StrI &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "	GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid "

                Next

                StrQ &= StrI
                StrI = ""

                StrQ &= ") AS A " & _
                        "LEFT JOIN " & _
                        "( "

                For i = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrI &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    StrI &= "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay " & _
                        "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                        "	FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                        "LEFT  JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                        "LEFT  JOIN " & strDatabaseName & "..practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                    "AND prpayrollprocess_tran.activityunkid > 0 " & _
                        "		JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
                    'Sohail (11 Mar 2020) - [,cfcommon_period_tran.periodunkid AS PrdId]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "	WHERE   ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                        "		AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                        "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "		AND cfcommon_period_tran.isactive = 1 " & _
                        "		AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                        "       AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "
                    'Sohail (11 Mar 2020) - [AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ")]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (11 Mar 2020) -- Start
                    'SUPORT TWC ISSUE # 0004611 : epf report shows wrong gross amount. (Gross Pay coming as sum of all period for each period)
                    'StrI &= "	GROUP BY prpayrollprocess_tran.employeeunkid "
                    StrI &= "	GROUP BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.periodunkid "
                    'Sohail (11 Mar 2020) -- End

                Next

                StrQ &= StrI

                StrQ &= ") AS G ON G.Empid = A.Empid " & _
                        " AND G.PrdId = A.PrdId "
                'Sohail (11 Mar 2020) - [AND G.PrdId = A.PrdId]

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                                   "	 A.Empid " & _
                                   "	, ISNULL(BasicSal, 0) AS BasicSal " & _
                                   "	, ISNULL(GrossPay, 0) AS GrossPay  " & _
                                   "    ,A.PrdId " & _
                                   "FROM " & _
                                   "( "

                For i = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrI &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "	SELECT " & _
                                   "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                                   "		,( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                                   "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                                                  "FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                                            "   JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                            "   JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                            "   JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                            "	JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                            "    JOIN " & strDatabaseName & "..prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                            "    AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                               " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "    WHERE        ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                 "                                                  AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                             "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                 "                                                  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                                             "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "

                    If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                        StrI &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                    Else
                        StrI &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "GROUP BY  prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate,cfcommon_period_tran.periodunkid "

                    StrI &= "          UNION ALL " & _
                                 "          SELECT    " & _
                                 "                  prpayrollprocess_tran.employeeunkid AS Empid " & _
                                 "                  ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                                 "                  ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                 "                          FROM  " & strDatabaseName & "..prpayrollprocess_tran " & _
                                 "                          JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                 "                          JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                 "                          JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                 "                          JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                 "                          JOIN " & strDatabaseName & "..prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                 "                          AND " & strDatabaseName & "..prpayment_tran.countryunkid <> " & mintCountryId & " AND prpayment_tran.isvoid = 0"

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "    WHERE        ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                 "                                                  AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                             "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                 "                                                  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
                                             "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "

                    If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                        StrI &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                    Else
                        StrI &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "GROUP BY  prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid "

                    StrI &= "          UNION ALL " & _
                                 "           SELECT  " & _
                                 "                   prpayrollprocess_tran.employeeunkid AS Empid " & _
                                 "                ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36,  " & decDecimalPlaces & "))) AS BasicSal " & _
                                 "                ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                 "                          FROM  " & strDatabaseName & "..prpayrollprocess_tran " & _
                                 "                          JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                 "                          JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                 "                          JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                 "                          JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                 "                          LEFT JOIN " & strDatabaseName & "..prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                "                           AND prpayment_tran.isvoid = 0"

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "     WHERE             ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                 "                                                  AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                 "                                                  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
                                "AND prpayment_tran.paymenttranunkid IS NULL "

                    If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                        StrI &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                    Else
                        StrI &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "GROUP BY  prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid "

                Next

                StrQ &= StrI
                StrI = ""

                StrQ &= ") AS A " & _
                        "LEFT JOIN " & _
                        "( "

                For i = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrI &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "      	SELECT " & _
                                 "		    prpayrollprocess_tran.employeeunkid AS Empid " & _
                               "		    ,( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay " & _
                               "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                 "          	FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                                 "		        LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                "LEFT  JOIN " & strDatabaseName & "..practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                        "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                 "		        JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                 "		        JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                 "		        JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                 "             JOIN " & strDatabaseName & "..prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                 "             AND prpayment_tran.countryunkid = " & mintCountryId & " AND prpayment_tran.isvoid = 0"

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "	            WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                            "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                                 "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                 "		        AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                 "		        AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "		        AND cfcommon_period_tran.isactive = 1 " & _
                                 "		        AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
                                 "              AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "	            GROUP BY prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate,cfcommon_period_tran.periodunkid " & _
                                 "     UNION ALL  " & _
                                 "       SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                                 ",      " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay   " & _
                                 "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                 "       FROM     " & strDatabaseName & "..prpayrollprocess_tran " & _
                                 "       LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                        "LEFT  JOIN " & strDatabaseName & "..practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                    "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                 "       JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                 "       JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                 "       JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                 "       JOIN " & strDatabaseName & "..prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                 "       AND prpayment_tran.countryunkid <> " & mintCountryId & " AND prpayment_tran.isvoid = 0"


                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                               " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "	            WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                            "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                                 "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                 "		        AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                 "		        AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "		        AND cfcommon_period_tran.isactive = 1 " & _
                                 "		        AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
                                 "              AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "	            GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid " & _
                                   "     UNION ALL  " & _
                                 "       SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                                 ",      " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay   " & _
                                 "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                 "       FROM     " & strDatabaseName & "..prpayrollprocess_tran " & _
                                 "       LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                        "LEFT  JOIN " & strDatabaseName & "..practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                    "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                 "       JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                 "       JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                 "       JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                 "       LEFT JOIN " & strDatabaseName & "..prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                 "       AND prpayment_tran.isvoid = 0"

                    'Pinkal (22-APR-2015) -- Start
                    'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                    'Sohail (12 Dec 2017) -- Start
                    'Issue : StrQ is used instead of StrI in 70.1.
                    'StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                    StrI &= " AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                " AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " "
                    'Sohail (12 Dec 2017) -- End
                    'Pinkal (22-APR-2015) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrI &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrI &= mstrAnalysis_Join
                    StrI &= mstrAnalysis_Join.Replace(mdtPeriodEnd, mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    StrI &= "	            WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                            "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                                 "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                 "		        AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                 "		        AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "		        AND cfcommon_period_tran.isactive = 1 " & _
                                 "		        AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
                                 "               AND prpayment_tran.paymenttranunkid IS NULL "

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mstrUserAccessLevelFilterString <> "" Then
                    '    StrI &= mstrUserAccessLevelFilterString
                    'Else
                    '    If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrI &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrI &= " AND " & xUACFiltrQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrI &= "	            GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid "

                Next

                StrQ &= StrI

                StrQ &= " ) AS G ON G.Empid = A.Empid " & _
                        " AND G.PrdId = A.PrdId "
                'Sohail (11 Mar 2020) - [AND G.PrdId = A.PrdId]


            End If

            objDataOperation.ClearParameters()
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                If dtRow.Item("PeriodName").ToString.Trim.Length <= 0 Then Continue For
                Dim rpt_Row As DataRow
                If iECode <> dtRow.Item("employeecode").ToString.Trim Then
                    iCnt = 1
                    iECode = dtRow.Item("employeecode").ToString.Trim
                End If
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeecode") & Space(10) & dtRow.Item("EName")
                rpt_Row.Item("Column2") = dtRow.Item("MemshipNo")
                rpt_Row.Item("Column3") = dtRow.Item("PeriodName")
                Dim dtTemp() As DataRow = dsBasGross.Tables("DataTable").Select("Empid = '" & dtRow.Item("Empid") & "' AND PrdId = '" & dtRow.Item("PId") & "'")
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then
                        rpt_Row.Item("Column5") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                    Else
                        rpt_Row.Item("Column5") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
                    End If
                End If
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = iCnt.ToString
                If dtRow.Item("leaving_date").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Column10") = eZeeDate.convertDate(dtRow.Item("leaving_date").ToString).ToShortDateString
                End If
                rpt_Row.Item("Column15") = dtRow.Item("receiptno").ToString
                If dtRow.Item("receiptdate").ToString.Trim = "" Then
                    rpt_Row.Item("Column16") = ""
                Else
                    rpt_Row.Item("Column16") = eZeeDate.convertDate(dtRow.Item("receiptdate").ToString).ToShortDateString
                End If
                'Sohail (05 Feb 2016) -- Start
                'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
                rpt_Row.Item("Column17") = dtRow.Item("employer_membershipno")
                'Sohail (05 Feb 2016) -- End

                '--------------------- FILLING TOTAL ------------ START
                Dim iDecValue As Decimal = 0
                If mblnShowBasicSalary = True Then
                    If IsDBNull(dsBasGross.Tables("DataTable").Compute("SUM(BasicSal)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
                        iDecValue = dsBasGross.Tables("DataTable").Compute("SUM(BasicSal)", "Empid = '" & dtRow.Item("Empid") & "'")
                    End If
                    rpt_Row.Item("Column11") = Format(CDec(iDecValue), GUI.fmtCurrency)
                Else
                    If IsDBNull(dsBasGross.Tables("DataTable").Compute("SUM(GrossPay)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
                        iDecValue = dsBasGross.Tables("DataTable").Compute("SUM(GrossPay)", "Empid = '" & dtRow.Item("Empid") & "'")
                    End If
                    rpt_Row.Item("Column11") = Format(CDec(iDecValue), GUI.fmtCurrency)
                End If
                iDecValue = 0
                If IsDBNull(dsList.Tables("DataTable").Compute("SUM(empcontribution)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
                    iDecValue = dsList.Tables("DataTable").Compute("SUM(empcontribution)", "Empid = '" & dtRow.Item("Empid") & "'")
                End If
                rpt_Row.Item("Column12") = Format(CDec(iDecValue), GUI.fmtCurrency)

                iDecValue = 0
                If IsDBNull(dsList.Tables("DataTable").Compute("SUM(employercontribution)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
                    iDecValue = dsList.Tables("DataTable").Compute("SUM(employercontribution)", "Empid = '" & dtRow.Item("Empid") & "'")
                End If
                rpt_Row.Item("Column13") = Format(CDec(iDecValue), GUI.fmtCurrency)
                iDecValue = 0
                If IsDBNull(dsList.Tables("DataTable").Compute("SUM(Amount)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
                    iDecValue = dsList.Tables("DataTable").Compute("SUM(Amount)", "Empid = '" & dtRow.Item("Empid") & "'")
                End If
                rpt_Row.Item("Column14") = Format(CDec(iDecValue), GUI.fmtCurrency)
                '--------------------- FILLING TOTAL ------------ END
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                iCnt += 1
            Next

            objRpt = New ArutiReport.Designer.rptEPFPeriodWiseReport

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 1, "National Social Security Fund"))
            Call ReportFunction.TextChange(objRpt, "txtSubHeading", Language.getMessage(mstrModuleName, 2, "FORM NSSF/B.132"))
            Call ReportFunction.TextChange(objRpt, "lblReportDate", Language.getMessage(mstrModuleName, 3, "ReportDate:"))
            Call ReportFunction.TextChange(objRpt, "lblEmployee", Language.getMessage(mstrModuleName, 4, "Employee:"))
            Call ReportFunction.TextChange(objRpt, "lblMemNumber", Language.getMessage(mstrModuleName, 5, "Membership Number:"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "Name of the Employer:"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerNumber", Language.getMessage(mstrModuleName, 7, "Employer Number:"))
            Call ReportFunction.TextChange(objRpt, "lblDateOfLeaving", Language.getMessage(mstrModuleName, 8, "Date of Leaving:"))
            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 9, "YEAR"))
            Call ReportFunction.TextChange(objRpt, "lblContribution", Language.getMessage(mstrModuleName, 10, "Contribution"))
            Call ReportFunction.TextChange(objRpt, "lblTo", Language.getMessage(mstrModuleName, 11, "To"))
            'Sohail (05 Sep 2014) -- Start
            'Enhancement - Provide Signing Details on Statutory Payment screen.
            Call ReportFunction.TextChange(objRpt, "lblNameOfEmployer", Language.getMessage(mstrModuleName, 6, "Name of the Employer:"))
            Call ReportFunction.TextChange(objRpt, "lblDesignation", Language.getMessage(mstrModuleName, 21, "Designation:"))
            Call ReportFunction.TextChange(objRpt, "lblEmployersSign", Language.getMessage(mstrModuleName, 22, "Employer's Signature:"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerAddress", Language.getMessage(mstrModuleName, 23, "Address of Employer:"))
            Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 24, "Date:"))
            'Sohail (05 Sep 2014) -- End

            Call ReportFunction.TextChange(objRpt, "txtDate", ConfigParameter._Object._CurrentDateAndTime.Date)
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            'Sohail (05 Feb 2016) -- Start
            'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
            'Call ReportFunction.TextChange(objRpt, "txtEmployerNumber", Company._Object._Nssfno)
            'Sohail (05 Feb 2016) -- End
            Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)
            Call ReportFunction.TextChange(objRpt, "txtFromPeriod", mstrFromPeriodName)
            Call ReportFunction.TextChange(objRpt, "txtToPeriod", mstrToPeriodName)
            'Sohail (05 Sep 2014) -- Start
            'Enhancement - Provide Signing Details on Statutory Payment screen.
            Call ReportFunction.TextChange(objRpt, "txtNameOfEmployer", Company._Object._Name)
            Dim objCMaster As New clsMasterData
            Dim dsCountry As DataSet
            Dim StrAddress As String = Company._Object._Address1 & " " & Company._Object._Address2
            dsCountry = objCMaster.getCountryList("List", False, Company._Object._Countryunkid)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                StrAddress &= " " & dsCountry.Tables(0).Rows(0)("country_name")
            End If
            dsCountry.Dispose()
            objCMaster = Nothing

            Dim objState As New clsstate_master
            Dim objCity As New clscity_master
            Dim objZipCode As New clszipcode_master

            objState._Stateunkid = Company._Object._Stateunkid
            objCity._Cityunkid = Company._Object._Cityunkid
            objZipCode._Zipcodeunkid = Company._Object._Postalunkid
            StrAddress &= " " & objState._Name & " " & objCity._Name & " " & objZipCode._Zipcode_No
            objState = Nothing
            objCity = Nothing
            objZipCode = Nothing
            Call ReportFunction.TextChange(objRpt, "txtEmployerAddress", StrAddress)
            'Sohail (05 Sep 2014) -- End

            Call ReportFunction.TextChange(objRpt, "txtNo", Language.getMessage(mstrModuleName, 12, "No."))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 13, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtNumber", mstrMembershipName & " " & Language.getMessage(mstrModuleName, 14, "Number"))
            Call ReportFunction.TextChange(objRpt, "txtIncome", Language.getMessage(mstrModuleName, 15, "Income"))
            Call ReportFunction.TextChange(objRpt, "txtEmpContrib", Language.getMessage(mstrModuleName, 16, "Employee Contrib."))
            Call ReportFunction.TextChange(objRpt, "txtEmplrComtrib", Language.getMessage(mstrModuleName, 17, "Employer Contrib."))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 18, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtRectNo", Language.getMessage(mstrModuleName, 19, "ReceiptNo. "))
            Call ReportFunction.TextChange(objRpt, "txtRectDate", Language.getMessage(mstrModuleName, 20, "Receipt Date"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 18, "Total"))

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#Region " Report b4 closed year period given on 19 FEB 2015 "
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim dsBasGross As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim iECode As String = String.Empty
    '    Try

    '        objDataOperation = New clsDataOperation

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal

    '        If mintBaseCurrId = mintPaidCurrencyId Then

    '            StrQ = "SELECT  hremployee_master.employeecode AS employeecode " & _
    '                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
    '                          ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
    '                          ", ISNULL(CAmount, 0) AS employercontribution " & _
    '                          ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) AS Amount  " & _
    '                          ", '' AS Remark " & _
    '                          ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                          ", hremployee_master.employeeunkid AS EmpId " & _
    '                          ", ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) , '') AS leaving_date " & _
    '                          ", cfcommon_period_tran.periodunkid AS PId " & _
    '                          ", prpayrollprocess_tran.membershiptranunkid AS EMembershiptranunkid " & _
    '                          ", c.membershiptranunkid AS CMembershiptranunkid " & _
    '                          ", ISNULL(rcpt.receiptno, '') AS receiptno " & _
    '                          ", ISNULL(rcpt.receiptdate, '') AS receiptdate "

    '            If mblnFirstNamethenSurname = True Then
    '                StrQ &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
    '            Else
    '                StrQ &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
    '            End If

    '            StrQ &= "FROM    prpayrollprocess_tran " & _
    '                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                            "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                            "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
    '                            "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                             "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                            "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                            "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                              "AND ISNULL(cfcommon_period_tran.isactive , 0) = 1 " & _
    '                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId  " & _
    '                                              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
    '                                              ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
    '                                              ", prpayrollprocess_tran.membershiptranunkid " & _
    '                                        "FROM    prpayrollprocess_tran " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
    '                                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                                "AND hrmembership_master.membershipunkid = @MemId "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End


    '            StrQ &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
    '                                                "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
    '                            "LEFT JOIN ( SELECT  prstatutorypayment_tran.periodunkid  " & _
    '                                              ", prstatutorypayment_tran.employeeunkid " & _
    '                                              ", prstatutorypayment_tran.membershiptranunkid " & _
    '                                              ", prstatutorypayment_master.receiptno " & _
    '                                              ", CONVERT(CHAR(8), ISNULL(prstatutorypayment_master.receiptdate, ''), 112) AS receiptdate " & _
    '                                        "FROM    prstatutorypayment_master " & _
    '                                                "LEFT JOIN prstatutorypayment_tran ON prstatutorypayment_master.statutorypaymentmasterunkid = prstatutorypayment_tran.statutorypaymentmasterunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prstatutorypayment_tran.employeeunkid = hremployee_master.employeeunkid "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "                    WHERE   ISNULL(prstatutorypayment_master.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prstatutorypayment_tran.isvoid, 0) = 0 " & _
    '                                                "AND prstatutorypayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prstatutorypayment_tran.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                                "AND prstatutorypayment_master.membershipunkid = @MemId "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "                      ) AS rcpt ON rcpt.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                   "AND rcpt.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                   "AND rcpt.membershiptranunkid = prpayrollprocess_tran.membershiptranunkid "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                            "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                            "AND hrmembership_master.membershipunkid = @MemId "


    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            If mblnFirstNamethenSurname = True Then
    '                StrQ &= " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "
    '            Else
    '                StrQ &= " ORDER BY ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') "
    '            End If

    '            StrQ &= ", cfcommon_period_tran.end_date ASC  "

    '        ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

    '            StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
    '                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
    '                          ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
    '                          ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * ISNULL(CAmount, 0) AS employercontribution " & _
    '                          ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) ) AS Amount " & _
    '                          ", '' AS Remark " & _
    '                          ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                          ", hremployee_master.employeeunkid AS EmpId " & _
    '                          ", cfcommon_period_tran.periodunkid AS PId "

    '            If mblnFirstNamethenSurname = True Then
    '                StrQ &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
    '            Else
    '                StrQ &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
    '            End If

    '            StrQ &= "FROM    prpayrollprocess_tran " & _
    '                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                            "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                            "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                            "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                             "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                            "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                            "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                              "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
    '                            "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
    '                                                   "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
    '                                                   "AND prpayment_tran.isvoid = 0 " & _
    '                                                   "AND prpayment_tran.countryunkid = " & mintCountryId & " " & _
    '                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '                                              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
    '                                              ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
    '                                        "FROM    prpayrollprocess_tran " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                                "AND hrmembership_master.membershipunkid = @MemId "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End


    '            StrQ &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
    '                                                "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
    '                                                "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                        "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                                        "AND hrmembership_master.membershipunkid = @MemId " & _
    '                                                        "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                        "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "      UNION ALL " & _
    '                    "SELECT  hremployee_master.employeecode AS employeecode " & _
    '                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
    '                          ", " & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
    '                          ", " & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
    '                          ", " & mdecConversionRate & " * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) ) AS Amount " & _
    '                          ", '' AS Remark " & _
    '                          ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                          ", hremployee_master.employeeunkid AS EmpId " & _
    '                          ", cfcommon_period_tran.periodunkid AS PId "

    '            If mblnFirstNamethenSurname = True Then
    '                StrQ &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
    '            Else
    '                StrQ &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
    '            End If

    '            StrQ &= "FROM    prpayrollprocess_tran " & _
    '                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                            "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                            "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                            "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                             "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                            "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                            "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                              "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
    '                            "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
    '                                                   "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
    '                                                   "AND prpayment_tran.isvoid = 0 " & _
    '                                                   "AND prpayment_tran.countryunkid <> " & mintCountryId & " " & _
    '                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '                                              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
    '                                              ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
    '                                        "FROM    prpayrollprocess_tran " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                                "AND hrmembership_master.membershipunkid = @MemId "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End


    '            StrQ &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
    '                                                "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
    '                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                            "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                            "AND hrmembership_master.membershipunkid = @MemId " & _
    '                            "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
    '                            "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End


    '            StrQ &= "UNION ALL " & _
    '                    "SELECT  hremployee_master.employeecode AS employeecode " & _
    '                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
    '                          ", " & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
    '                          ", " & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
    '                          ", " & mdecConversionRate & " * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) ) AS Amount " & _
    '                          ", '' AS Remark " & _
    '                          ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                          ", hremployee_master.employeeunkid AS EmpId " & _
    '                          ", cfcommon_period_tran.periodunkid AS PId "

    '            If mblnFirstNamethenSurname = True Then
    '                StrQ &= ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
    '            Else
    '                StrQ &= ",   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
    '            End If

    '            StrQ &= "FROM    prpayrollprocess_tran " & _
    '                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                            "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                            "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                            "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                             "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                            "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                            "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                              "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
    '                            "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
    '                                                        "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
    '                                                        "AND prpayment_tran.isvoid = 0 " & _
    '                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '                                              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
    '                                              ", prtnaleave_tran.payperiodunkid AS iCPrdId " & _
    '                                        "FROM    prpayrollprocess_tran " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                                "AND hrmembership_master.membershipunkid = @MemId "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End


    '            StrQ &= "                      ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
    '                                                "AND prtnaleave_tran.payperiodunkid = C.iCPrdId " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                            "AND hremployee_master.employeeunkid IN (" & mstrEmployeeIds & ") " & _
    '                                            "AND hrmembership_master.membershipunkid = @MemId " & _
    '                                            "AND prpayment_tran.paymenttranunkid IS NULL "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            If mblnFirstNamethenSurname = True Then
    '                StrQ &= " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "
    '            Else
    '                StrQ &= " ORDER BY ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') "
    '            End If

    '            StrQ &= ", cfcommon_period_tran.end_date ASC  "

    '        End If

    '        objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If mintBaseCurrId = mintPaidCurrencyId Then

    '            StrQ = "SELECT " & _
    '                   "	 A.Empid " & _
    '                   "	, ISNULL(BasicSal, 0) AS BasicSal " & _
    '                   "	, ISNULL(GrossPay, 0) AS GrossPay  " & _
    '                   "    , A.PrdId " & _
    '                   "FROM " & _
    '                   "( " & _
    '                   "	SELECT " & _
    '                   "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                   "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                   "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                                                  "FROM      prpayrollprocess_tran " & _
    '                                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
    '            StrQ &= mstrAnalysis_Join
    '            StrQ &= "	WHERE       ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                                        "AND cfcommon_period_tran.isactive = 1 " & _
    '                                    "AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "

    '            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
    '            Else
    '                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '            End If

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid " & _
    '                ") AS A " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "LEFT  JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                            "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
    '                            "AND prpayrollprocess_tran.activityunkid > 0 " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
    '            StrQ &= mstrAnalysis_Join
    '            StrQ &= "	WHERE   ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
    '                        "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
    '                "		AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND cfcommon_period_tran.isactive = 1 " & _
    '                "		AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid " & _
    '                ") AS G ON G.Empid = A.Empid "
    '        ElseIf mintBaseCurrId <> mintPaidCurrencyId Then
    '            StrQ = "SELECT " & _
    '                               "	 A.Empid " & _
    '                               "	, ISNULL(BasicSal, 0) AS BasicSal " & _
    '                               "	, ISNULL(GrossPay, 0) AS GrossPay  " & _
    '                               "    ,A.PrdId " & _
    '                               "FROM " & _
    '                               "( " & _
    '                               "	SELECT " & _
    '                               "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                               "		,( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                               "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                                                              "FROM      prpayrollprocess_tran " & _
    '                                                                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                        "	JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                                                        "    JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                                                                        "    AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId
    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "    WHERE        ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                         "                                                  AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                     "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                         "                                                  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
    '                                     "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "

    '            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
    '            Else
    '                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '            End If

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate,cfcommon_period_tran.periodunkid "

    '            StrQ &= "          UNION ALL " & _
    '                         "          SELECT    " & _
    '                         "                  prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                         "                  ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                         "                  ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                         "                          FROM  prpayrollprocess_tran " & _
    '                         "                          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                         "                          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         "                          JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         "                          JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                         "                          JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                         "                          AND prpayment_tran.countryunkid <> " & mintCountryId & " AND prpayment_tran.isvoid = 0"

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "    WHERE        ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                         "                                                  AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                     "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                         "                                                  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") " & _
    '                                     "AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") "

    '            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
    '            Else
    '                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '            End If

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid "

    '            StrQ &= "          UNION ALL " & _
    '                         "           SELECT  " & _
    '                         "                   prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                         "                ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36,  " & decDecimalPlaces & "))) AS BasicSal " & _
    '                         "                ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                         "                          FROM  prpayrollprocess_tran " & _
    '                         "                          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                         "                          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         "                          JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         "                          JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                         "                          LEFT JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                        "                           AND prpayment_tran.isvoid = 0"

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "     WHERE             ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                         "                                                  AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                         "                                                  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
    '                        "AND prpayment_tran.paymenttranunkid IS NULL "

    '            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
    '            Else
    '                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '            End If

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid " & _
    '                         " ) AS A " & _
    '                         "  LEFT JOIN " & _
    '                         "  ( " & _
    '                         "      	SELECT " & _
    '                         "		    prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                       "		    ,( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay " & _
    '                       "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                         "          	FROM prpayrollprocess_tran " & _
    '                         "		        LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                        "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
    '                                                "AND prpayrollprocess_tran.activityunkid > 0 " & _
    '                         "		        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         "		        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         "		        JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                         "             JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                         "             AND prpayment_tran.countryunkid = " & mintCountryId & " AND prpayment_tran.isvoid = 0"
    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	            WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
    '                                    "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
    '                         "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '                         "		        AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                         "		        AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                         "		        AND cfcommon_period_tran.isactive = 1 " & _
    '                         "		        AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
    '                         "              AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "	            GROUP BY prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate,cfcommon_period_tran.periodunkid " & _
    '                         "     UNION ALL  " & _
    '                         "       SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                         ",      " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay   " & _
    '                         "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                         "       FROM     prpayrollprocess_tran " & _
    '                         "       LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                            "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
    '                                            "AND prpayrollprocess_tran.activityunkid > 0 " & _
    '                         "       JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         "       JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         "       JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                         "       JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                         "       AND prpayment_tran.countryunkid <> " & mintCountryId & " AND prpayment_tran.isvoid = 0"
    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	            WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
    '                                    "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
    '                         "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '                         "		        AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                         "		        AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                         "		        AND cfcommon_period_tran.isactive = 1 " & _
    '                         "		        AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
    '                         "              AND prpayment_tran.periodunkid IN (" & mstrPeriodIds & ") "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "	            GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid " & _
    '                           "     UNION ALL  " & _
    '                         "       SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                         ",      " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay   " & _
    '                         "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                         "       FROM     prpayrollprocess_tran " & _
    '                         "       LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
    '                                            "AND prpayrollprocess_tran.activityunkid > 0 " & _
    '                         "       JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         "       JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         "       JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                         "       LEFT JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                         "       AND prpayment_tran.isvoid = 0"
    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	            WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
    '                                    "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
    '                         "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '                         "		        AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                         "		        AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                         "		        AND cfcommon_period_tran.isactive = 1 " & _
    '                         "		        AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ") AND hremployee_master.employeeunkid  IN (" & mstrEmployeeIds & ") " & _
    '                         "               AND prpayment_tran.paymenttranunkid IS NULL "

    '            'Pinkal (15-Oct-2014) -- Start
    '            'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Pinkal (15-Oct-2014) -- End

    '            StrQ &= "	            GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid " & _
    '                        " ) AS G ON G.Empid = A.Empid "
    '        End If

    '        objDataOperation.ClearParameters()
    '        If mintOtherEarningTranId > 0 Then
    '            objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
    '        End If

    '        dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim iCnt As Integer = 0

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            If dtRow.Item("PeriodName").ToString.Trim.Length <= 0 Then Continue For
    '            Dim rpt_Row As DataRow
    '            If iECode <> dtRow.Item("employeecode").ToString.Trim Then
    '                iCnt = 1
    '                iECode = dtRow.Item("employeecode").ToString.Trim
    '            End If
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("employeecode") & Space(10) & dtRow.Item("EName")
    '            rpt_Row.Item("Column2") = dtRow.Item("MemshipNo")
    '            rpt_Row.Item("Column3") = dtRow.Item("PeriodName")
    '            Dim dtTemp() As DataRow = dsBasGross.Tables("DataTable").Select("Empid = '" & dtRow.Item("Empid") & "' AND PrdId = '" & dtRow.Item("PId") & "'")
    '            If dtTemp.Length > 0 Then
    '                If mblnShowBasicSalary = True Then
    '                    rpt_Row.Item("Column5") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column5") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
    '                End If
    '            End If
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = iCnt.ToString
    '            If dtRow.Item("leaving_date").ToString.Trim.Length > 0 Then
    '                rpt_Row.Item("Column10") = eZeeDate.convertDate(dtRow.Item("leaving_date").ToString).ToShortDateString
    '            End If
    '            rpt_Row.Item("Column15") = dtRow.Item("receiptno").ToString
    '            If dtRow.Item("receiptdate").ToString.Trim = "" Then
    '                rpt_Row.Item("Column16") = ""
    '            Else
    '                rpt_Row.Item("Column16") = eZeeDate.convertDate(dtRow.Item("receiptdate").ToString).ToShortDateString
    '            End If

    '            '--------------------- FILLING TOTAL ------------ START
    '            Dim iDecValue As Decimal = 0
    '            If mblnShowBasicSalary = True Then
    '                If IsDBNull(dsBasGross.Tables("DataTable").Compute("SUM(BasicSal)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
    '                    iDecValue = dsBasGross.Tables("DataTable").Compute("SUM(BasicSal)", "Empid = '" & dtRow.Item("Empid") & "'")
    '                End If
    '                rpt_Row.Item("Column11") = Format(CDec(iDecValue), GUI.fmtCurrency)
    '            Else
    '                If IsDBNull(dsBasGross.Tables("DataTable").Compute("SUM(GrossPay)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
    '                    iDecValue = dsBasGross.Tables("DataTable").Compute("SUM(GrossPay)", "Empid = '" & dtRow.Item("Empid") & "'")
    '                End If
    '                rpt_Row.Item("Column11") = Format(CDec(iDecValue), GUI.fmtCurrency)
    '            End If
    '            iDecValue = 0
    '            If IsDBNull(dsList.Tables("DataTable").Compute("SUM(empcontribution)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
    '                iDecValue = dsList.Tables("DataTable").Compute("SUM(empcontribution)", "Empid = '" & dtRow.Item("Empid") & "'")
    '            End If
    '            rpt_Row.Item("Column12") = Format(CDec(iDecValue), GUI.fmtCurrency)

    '            iDecValue = 0
    '            If IsDBNull(dsList.Tables("DataTable").Compute("SUM(employercontribution)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
    '                iDecValue = dsList.Tables("DataTable").Compute("SUM(employercontribution)", "Empid = '" & dtRow.Item("Empid") & "'")
    '            End If
    '            rpt_Row.Item("Column13") = Format(CDec(iDecValue), GUI.fmtCurrency)
    '            iDecValue = 0
    '            If IsDBNull(dsList.Tables("DataTable").Compute("SUM(Amount)", "Empid = '" & dtRow.Item("Empid") & "'")) = False Then
    '                iDecValue = dsList.Tables("DataTable").Compute("SUM(Amount)", "Empid = '" & dtRow.Item("Empid") & "'")
    '            End If
    '            rpt_Row.Item("Column14") = Format(CDec(iDecValue), GUI.fmtCurrency)
    '            '--------------------- FILLING TOTAL ------------ END
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            iCnt += 1
    '        Next

    '        objRpt = New ArutiReport.Designer.rptEPFPeriodWiseReport

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 1, "National Social Security Fund"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubHeading", Language.getMessage(mstrModuleName, 2, "FORM NSSF/B.132"))
    '        Call ReportFunction.TextChange(objRpt, "lblReportDate", Language.getMessage(mstrModuleName, 3, "ReportDate:"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployee", Language.getMessage(mstrModuleName, 4, "Employee:"))
    '        Call ReportFunction.TextChange(objRpt, "lblMemNumber", Language.getMessage(mstrModuleName, 5, "Membership Number:"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "Name of the Employer:"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployerNumber", Language.getMessage(mstrModuleName, 7, "Employer Number:"))
    '        Call ReportFunction.TextChange(objRpt, "lblDateOfLeaving", Language.getMessage(mstrModuleName, 8, "Date of Leaving:"))
    '        Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 9, "YEAR"))
    '        Call ReportFunction.TextChange(objRpt, "lblContribution", Language.getMessage(mstrModuleName, 10, "Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "lblTo", Language.getMessage(mstrModuleName, 11, "To"))
    '        'Sohail (05 Sep 2014) -- Start
    '        'Enhancement - Provide Signing Details on Statutory Payment screen.
    '        Call ReportFunction.TextChange(objRpt, "lblNameOfEmployer", Language.getMessage(mstrModuleName, 6, "Name of the Employer:"))
    '        Call ReportFunction.TextChange(objRpt, "lblDesignation", Language.getMessage(mstrModuleName, 21, "Designation:"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployersSign", Language.getMessage(mstrModuleName, 22, "Employer's Signature:"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployerAddress", Language.getMessage(mstrModuleName, 23, "Address of Employer:"))
    '        Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 24, "Date:"))
    '        'Sohail (05 Sep 2014) -- End

    '        Call ReportFunction.TextChange(objRpt, "txtDate", ConfigParameter._Object._CurrentDateAndTime.Date)
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerNumber", Company._Object._Nssfno)
    '        Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)
    '        Call ReportFunction.TextChange(objRpt, "txtFromPeriod", mstrFromPeriodName)
    '        Call ReportFunction.TextChange(objRpt, "txtToPeriod", mstrToPeriodName)
    '        'Sohail (05 Sep 2014) -- Start
    '        'Enhancement - Provide Signing Details on Statutory Payment screen.
    '        Call ReportFunction.TextChange(objRpt, "txtNameOfEmployer", Company._Object._Name)
    '        Dim objCMaster As New clsMasterData
    '        Dim dsCountry As DataSet
    '        Dim StrAddress As String = Company._Object._Address1 & " " & Company._Object._Address2
    '        dsCountry = objCMaster.getCountryList("List", False, Company._Object._Countryunkid)
    '        If dsCountry.Tables(0).Rows.Count > 0 Then
    '            StrAddress &= " " & dsCountry.Tables(0).Rows(0)("country_name")
    '        End If
    '        dsCountry.Dispose()
    '        objCMaster = Nothing

    '        Dim objState As New clsstate_master
    '        Dim objCity As New clscity_master
    '        Dim objZipCode As New clszipcode_master

    '        objState._Stateunkid = Company._Object._Stateunkid
    '        objCity._Cityunkid = Company._Object._Cityunkid
    '        objZipCode._Zipcodeunkid = Company._Object._Postalunkid
    '        StrAddress &= " " & objState._Name & " " & objCity._Name & " " & objZipCode._Zipcode_No
    '        objState = Nothing
    '        objCity = Nothing
    '        objZipCode = Nothing
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerAddress", StrAddress)
    '        'Sohail (05 Sep 2014) -- End

    '        Call ReportFunction.TextChange(objRpt, "txtNo", Language.getMessage(mstrModuleName, 12, "No."))
    '        Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 13, "Month"))
    '        Call ReportFunction.TextChange(objRpt, "txtNumber", mstrMembershipName & " " & Language.getMessage(mstrModuleName, 14, "Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtIncome", Language.getMessage(mstrModuleName, 15, "Income"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpContrib", Language.getMessage(mstrModuleName, 16, "Employee Contrib."))
    '        Call ReportFunction.TextChange(objRpt, "txtEmplrComtrib", Language.getMessage(mstrModuleName, 17, "Employer Contrib."))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 18, "Total"))
    '        Call ReportFunction.TextChange(objRpt, "txtRectNo", Language.getMessage(mstrModuleName, 19, "ReceiptNo. "))
    '        Call ReportFunction.TextChange(objRpt, "txtRectDate", Language.getMessage(mstrModuleName, 20, "Receipt Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 18, "Total"))

    '        Return objRpt
    '    Catch ex As Exception
    '        DisplayError.Show("-1", iECode & " - " & ex.Message, "Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "National Social Security Fund")
            Language.setMessage(mstrModuleName, 2, "FORM NSSF/B.132")
            Language.setMessage(mstrModuleName, 3, "ReportDate:")
            Language.setMessage(mstrModuleName, 4, "Employee:")
            Language.setMessage(mstrModuleName, 5, "Membership Number:")
            Language.setMessage(mstrModuleName, 6, "Name of the Employer:")
            Language.setMessage(mstrModuleName, 7, "Employer Number:")
            Language.setMessage(mstrModuleName, 8, "Date of Leaving:")
            Language.setMessage(mstrModuleName, 9, "YEAR")
            Language.setMessage(mstrModuleName, 10, "Contribution")
            Language.setMessage(mstrModuleName, 11, "To")
            Language.setMessage(mstrModuleName, 12, "No.")
            Language.setMessage(mstrModuleName, 13, "Month")
            Language.setMessage(mstrModuleName, 14, "Number")
            Language.setMessage(mstrModuleName, 15, "Income")
            Language.setMessage(mstrModuleName, 16, "Employee Contrib.")
            Language.setMessage(mstrModuleName, 17, "Employer Contrib.")
            Language.setMessage(mstrModuleName, 18, "Total")
            Language.setMessage(mstrModuleName, 19, "ReceiptNo.")
            Language.setMessage(mstrModuleName, 20, "Receipt Date")
            Language.setMessage(mstrModuleName, 21, "Designation:")
            Language.setMessage(mstrModuleName, 22, "Employer's Signature:")
            Language.setMessage(mstrModuleName, 23, "Address of Employer:")
            Language.setMessage(mstrModuleName, 24, "Date:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
