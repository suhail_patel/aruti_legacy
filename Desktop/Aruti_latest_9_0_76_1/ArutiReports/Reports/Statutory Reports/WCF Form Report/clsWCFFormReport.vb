'************************************************************************************************************************************
'Class Name :clsWCFFormReport.vb
'Purpose    :
'Date       :21 Jul 2015
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsWCFFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsWCFFormReport"
    Private mstrReportId As String = enArutiReport.WCF_REPORT
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "

    Private mintTranHeadId As Integer = 0
    Private mstrTranHeadName As String = String.Empty
    Private mintCurrencyID As Integer = 0
    Private mstrCurrencyName As String = ""
    Private mdec_ExRate As Decimal = 1
    Private mstrPeriodIds As String = String.Empty
    Private mstrComma_PeriodName As String = String.Empty
    Private mintOtherEarningTranId As Integer = 0
    Private mblnIngnoreZero As Boolean = False
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    'Sohail (02 Jun 2017) -- Start
    'Enhancement - 68.1 - Advance Filter on WCF Form Report.
    Private mstrAdvance_Filter As String = String.Empty
    'Sohail (02 Jun 2017) -- End
    'Sohail (18 Mar 2019) -- Start
    'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    'Sohail (18 Mar 2019) -- End
    'Hemant (27 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
    Private mintIdentityId As Integer = 0
    Private mstrIdentityName As String = String.Empty
    'Hemant (27 Aug 2019) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _TranHeadId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadName() As String
        Set(ByVal value As String)
            mstrTranHeadName = value
        End Set
    End Property

    Public WriteOnly Property _Period_Ids() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _Comma_PeriodName() As String
        Set(ByVal value As String)
            mstrComma_PeriodName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _IngnoreZero() As Boolean
        Set(ByVal value As Boolean)
            mblnIngnoreZero = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyID = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdec_ExRate = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Sohail (02 Jun 2017) -- Start
    'Enhancement - 68.1 - Advance Filter on WCF Form Report.
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Sohail (02 Jun 2017) -- End

    'Sohail (18 Mar 2019) -- Start
    'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property
    'Sohail (18 Mar 2019) -- End

    'Hemant (27 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
    Public WriteOnly Property _IdentityId() As Integer
        Set(ByVal value As Integer)
            mintIdentityId = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property
    'Hemant (27 Aug 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintTranHeadId = 0
            mstrTranHeadName = String.Empty
            mintCurrencyID = 0
            mstrCurrencyName = String.Empty
            mdec_ExRate = 1
            mintOtherEarningTranId = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            mstrAdvance_Filter = ""
            'Sohail (02 Jun 2017) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END



    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsBasGross As DataSet
        Dim dsContrib As DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            If mdec_ExRate <= 0 Then mdec_ExRate = 1

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Sohail (02 Jun 2017) -- End
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ = "SELECT  A.employeeunkid  " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername , '') + ' ' + hremployee_master.surname AS employeename  " & _
                          ", ISNULL(SUM(A.BasicSalary), 0) AS BasicSalary " & _
                          ", ISNULL(SUM(A.TaxableAmount), 0) AS TaxableAmount " & _
                    "FROM    ( SELECT    prpayrollprocess_tran.employeeunkid  " & _
                                      ", prpayrollprocess_tran.amount * " & mdec_ExRate & " AS BasicSalary " & _
                                      ", 0 AS TaxableAmount " & _
                              "FROM      prpayrollprocess_tran " & _
                                        "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (02 Jun 2017) -- End

            StrQ &= "          WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prtranhead_master.isvoid = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Sohail (02 Jun 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "          UNION ALL " & _
                              "SELECT    prpayrollprocess_tran.employeeunkid  " & _
                                      ", 0 AS BasicSalary " & _
                                      ", prpayrollprocess_tran.amount * " & mdec_ExRate & " AS TaxableAmount " & _
                              "FROM      prpayrollprocess_tran " & _
                                        "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (02 Jun 2017) -- End

            StrQ &= "          WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prtranhead_master.isvoid = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) " & _
                                        "AND prtranhead_master.istaxable = 1 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Sohail (02 Jun 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "        ) AS A " & _
                            "LEFT JOIN hremployee_master ON A.employeeunkid = hremployee_master.employeeunkid " & _
                    "GROUP BY A.employeeunkid  " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname "

            If mblnIngnoreZero = True Then
                StrQ &= " HAVING NOT( ISNULL(SUM(A.BasicSalary), 0) = 0 AND ISNULL(SUM(A.TaxableAmount), 0) = 0 ) "
            End If

            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT  ISNULL(SUM(prpayrollprocess_tran.amount * " & mdec_ExRate & "), 0) AS TotContrib " & _
                    "FROM    prpayrollprocess_tran " & _
                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (10 Oct 2017) -- Start
            'Enhancement - 70.1 - Advance Filter on WCF Form Report (issue 1332 - Aruti v69 and v70, Abood | Advance filter in WCF form report not working.).
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (10 Oct 2017) -- End

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtranhead_master.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid IN ( " & mstrPeriodIds & " ) " & _
                            "AND prpayrollprocess_tran.tranheadunkid = @CoContribHeadId "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (10 Oct 2017) -- Start
            'Enhancement - 70.1 - Advance Filter on WCF Form Report (issue 1332 - Aruti v69 and v70, Abood | Advance filter in WCF form report not working.).
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Sohail (10 Oct 2017) -- End

            objDataOperation.AddParameter("@CoContribHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)

            dsContrib = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim decContribTotal As Decimal = 0
            If dsContrib.Tables(0).Rows.Count > 0 Then
                decContribTotal = CDec(dsContrib.Tables(0).Rows(0).Item("TotContrib"))
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 1
            Dim decTotTaxable As Decimal = 0

            For Each dtRow As DataRow In dsBasGross.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = iCnt
                rpt_Row.Item("Column2") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column3") = dtRow.Item("employeecode")
                rpt_Row.Item("Column4") = dtRow.Item("employeename")
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("BasicSalary")), GUI.fmtCurrency)
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("TaxableAmount")), GUI.fmtCurrency)

                iCnt += 1
                decTotTaxable += CDec(dtRow.Item("TaxableAmount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptWCFFormReport
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtURT", Language.getMessage(mstrModuleName, 1, "UNITED REPUBLIC OF TANZANIA"))
            Call ReportFunction.TextChange(objRpt, "txtWCF", Language.getMessage(mstrModuleName, 2, "WORKERS COMPENSATION FUND (WCF)"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerContribution", Language.getMessage(mstrModuleName, 3, "EMPLOYER’S CONTRIBUTION FORM"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerParticular", Language.getMessage(mstrModuleName, 4, "Employer's particulars"))
            Call ReportFunction.TextChange(objRpt, "lblName", Language.getMessage(mstrModuleName, 5, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "lblWCFRegNo", Language.getMessage(mstrModuleName, 6, "WCF Reg. No. (If available):"))
            Call ReportFunction.TextChange(objRpt, "txtWCFRegNo", Company._Object._Registerdno)
            Call ReportFunction.TextChange(objRpt, "lblAddress", Language.getMessage(mstrModuleName, 7, "Address :"))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Company._Object._Address1 & " " & Company._Object._Address2 & " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No & " " & Company._Object._Country_Name)
            Call ReportFunction.TextChange(objRpt, "lblPhone", Language.getMessage(mstrModuleName, 8, "Phone :"))
            Call ReportFunction.TextChange(objRpt, "txtPhone", Company._Object._Phone1 & " " & Company._Object._Phone2)
            Call ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 9, "Email :"))
            Call ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
            Call ReportFunction.TextChange(objRpt, "txtRemittanceSummary", Language.getMessage(mstrModuleName, 10, "Remittance Summary"))
            Call ReportFunction.TextChange(objRpt, "lblAmount", Language.getMessage(mstrModuleName, 11, "Amount (USD/TZS) :"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Format(decContribTotal, GUI.fmtCurrency) & " (" & mstrCurrencyName & ")")
            Call ReportFunction.TextChange(objRpt, "txtPaymentDate", Language.getMessage(mstrModuleName, 12, "Payment Date :"))
            Call ReportFunction.TextChange(objRpt, "lblApplicableMonth", Language.getMessage(mstrModuleName, 13, "Applicable Month :"))
            Call ReportFunction.TextChange(objRpt, "txtApplicableMonth", mstrComma_PeriodName)
            Call ReportFunction.TextChange(objRpt, "txtBankName", Language.getMessage(mstrModuleName, 14, "Bank Name :"))
            Call ReportFunction.TextChange(objRpt, "txtRemittanceMethod", Language.getMessage(mstrModuleName, 15, "Remittance Method :"))
            Call ReportFunction.TextChange(objRpt, "txtCheque", Language.getMessage(mstrModuleName, 16, "(Electronic Transfer,Cheque etc)"))
            Call ReportFunction.TextChange(objRpt, "txtAuthorisingOfficer", Language.getMessage(mstrModuleName, 17, "Employer's Authorising Officer"))
            Call ReportFunction.TextChange(objRpt, "txtCertify", Language.getMessage(mstrModuleName, 18, "I hereby certify that to the best of my knowledge all particulars in this return are complete, true and correct."))
            Call ReportFunction.TextChange(objRpt, "txtOfficerName", Language.getMessage(mstrModuleName, 19, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtPosition", Language.getMessage(mstrModuleName, 20, "Position  :"))
            Call ReportFunction.TextChange(objRpt, "txtSignatureemployer", Language.getMessage(mstrModuleName, 21, "Signature of employer :"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 22, "Date :"))
            Call ReportFunction.TextChange(objRpt, "txtAttachList", Language.getMessage(mstrModuleName, 23, "(Attach list of amounts remitted for each employee. A sample list is available)"))
            Call ReportFunction.TextChange(objRpt, "txtWCP", Language.getMessage(mstrModuleName, 24, "WCP-1"))

            objRpt.Subreports("rptSubWCFFormReport").SetDataSource(rpt_Data)
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtAttachmentForm", Language.getMessage(mstrModuleName, 25, "ATTACHMENT TO FORM No. WCP - 1"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtURTsubReport", Language.getMessage(mstrModuleName, 1, "UNITED REPUBLIC OF TANZANIA"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtWCFsubReport", Language.getMessage(mstrModuleName, 2, "WORKERS COMPENSATION FUND (WCF)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtListAmtEmp", Language.getMessage(mstrModuleName, 26, "List of amounts contributed for each employee"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "lblEmployerName", Language.getMessage(mstrModuleName, 27, "Employer's Name :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtEmployerName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "lblWCFRegNoSubReport", Language.getMessage(mstrModuleName, 6, "WCF Reg. No. (If available):"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtWCFRegNoSubReport", Company._Object._Registerdno)
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "lblApplicableMonthSubReport", Language.getMessage(mstrModuleName, 13, "Applicable Month :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtApplicableMonthSubReport", mstrComma_PeriodName)
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtApplicableContribution", Language.getMessage(mstrModuleName, 28, "Applicable Contribution during 2015/16 :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtPrivateEntities", Language.getMessage(mstrModuleName, 29, "(1 % of gross pay for private entities)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtPublicEntities", Language.getMessage(mstrModuleName, 30, "(0.5 % of gross pay for public entities)"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtSNo", Language.getMessage(mstrModuleName, 31, "S/N"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtEmployeeID", Language.getMessage(mstrModuleName, 32, "Employee ID"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtEmployeeName", Language.getMessage(mstrModuleName, 33, "Employee Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtEmployeeBasicSalary", Language.getMessage(mstrModuleName, 34, "Employee Basic Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtEmployeeGrossSalary", Language.getMessage(mstrModuleName, 35, "Employee Gross Salary"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtAuthorisingOfficerSubReport", Language.getMessage(mstrModuleName, 17, "Employer's Authorising Officer"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtCertifysubreport", Language.getMessage(mstrModuleName, 18, "I hereby certify that to the best of my knowledge all particulars in this return are complete, true and correct."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtSignatureemployerSubReport", Language.getMessage(mstrModuleName, 21, "Signature of employer :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtOfficerNameSubReport", Language.getMessage(mstrModuleName, 19, "Name :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtPositionsubReport", Language.getMessage(mstrModuleName, 20, "Position  :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtDatesubReport", Language.getMessage(mstrModuleName, 22, "Date :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "lblTotal", Language.getMessage(mstrModuleName, 36, "Total"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtTotal", Format(decTotTaxable, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "lblContribTotal", Language.getMessage(mstrModuleName, 37, "Total Contributions Due"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubWCFFormReport"), "txtContribTotal", Format(decContribTotal, GUI.fmtCurrency))

            Return objRpt

        Catch ex As Exception
            'Sohail (18 Mar 2019) -- Start
            'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
            'Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            'Sohail (18 Mar 2019) -- End
            Return Nothing
        End Try
    End Function

    'Sohail (18 Mar 2019) -- Start
    'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
    Public Function Generate_EFT_WCF_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strExportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean _
                                           ) As Boolean


        Dim StrQ As String = ""
        Dim dsBasGross As DataSet
        Dim exForce As Exception = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            If mdec_ExRate <= 0 Then mdec_ExRate = 1

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            Dim objMaster As New clsMasterData
            Dim dsGender As DataSet = objMaster.getGenderList("List", False)
            Dim dicGender As Dictionary(Of Integer, String) = (From p In dsGender.Tables("List") Select New With {.Id = CInt(p.Item("id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.Id, Function(x) x.Name)
            objMaster = Nothing

            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                        ", hremployee_master.employeecode " & _
                        ", REPLACE(ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ''), '  ',' ') AS employeename  " & _
                        ", ISNULL(hremployee_master.firstname, '') AS firstname  " & _
                        ", ISNULL(hremployee_master.othername, '') AS othername  " & _
                        ", ISNULL(hremployee_master.surname, '') AS surname  " & _
                        ", ISNULL(CONVERT(VARCHAR(8),  birthdate, 112), '') AS birthdate " & _
                        ", ISNULL(hrjob_master.job_name, '') AS jobname " & _
                        ", hremployee_master.employmenttypeunkid " & _
                        ", ISNULL(cfcommon_master.name, '') AS employmenttype "

            StrQ &= ", CASE hremployee_master.gender "
            For Each pair In dicGender
                StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            StrQ &= " ELSE '' END AS gendername "

            StrQ &= "INTO  #TableEmp " & _
                    "FROM  hremployee_master " & _
                          "LEFT JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  "

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN hrjob_master ON J.jobunkid = hrjob_master.jobunkid "

            StrQ &= "WHERE 1 = 1 "

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "SELECT employeeunkid " & _
                        ", SUM(ISNULL(BasicSalary, 0)) AS BasicSalary " & _
                        ", SUM(ISNULL(TotalEarning, 0)) AS TotalEarning " & _
                   "INTO #TableHead " & _
                   "FROM " & _
                   "( " & _
                       "SELECT prpayrollprocess_tran.employeeunkid " & _
                            ", payperiodunkid " & _
                            ", prpayrollprocess_tran.amount * " & mdec_ExRate & " AS BasicSalary " & _
                            ", 0 AS TotalEarning " & _
                       "FROM  prpayrollprocess_tran " & _
                              "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                              "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                              "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                       "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            StrQ &= "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", payperiodunkid " & _
                             ", 0 AS BasicSalary " & _
                             ", prpayrollprocess_tran.amount * " & mdec_ExRate & " AS TotalEarning " & _
                        "FROM prpayrollprocess_tran " & _
                              "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                              "JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                              "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
                              "AND prtranhead_master.istaxable = 1 " & _
                   ") AS A " & _
                    "GROUP BY employeeunkid "

            If mblnIngnoreZero = True Then
                StrQ &= " HAVING NOT(  SUM(ISNULL(BasicSalary, 0)) = 0 AND SUM(ISNULL(TotalEarning, 0)) = 0 ) "
            End If

            StrQ &= "SELECT  DISTINCT #TableEmp.employeecode AS EmpCode " & _
                          ", #TableEmp.employeename AS EmpName  " & _
                          ", ISNULL(#TableEmp.firstname, '') AS firstname  " & _
                          ", ISNULL(#TableEmp.othername, '') AS othername  " & _
                          ", ISNULL(#TableEmp.surname, '') AS surname  " & _
                          ", ISNULL(#TableEmp.firstname, '') + ' ' + ISNULL(#TableEmp.othername, '') + ' ' + ISNULL(#TableEmp.surname, '') AS empployeename " & _
                          ", ISNULL(CONVERT(VARCHAR(8),  birthdate, 112), '') AS birthdate " & _
                          ", #TableEmp.jobname " & _
                          ", #TableEmp.gendername " & _
                          ", #TableEmp.employmenttype " & _
                          ", ISNULL(#TableHead.BasicSalary, 0 ) AS BasicSalary  " & _
                          ", ISNULL(#TableHead.TotalEarning, 0 ) AS TotalEarning  " & _
                          ", ISNULL(Id.Id_Num, '') AS Membershipno "
            'Hemant (27 Aug 2019) -- [", ISNULL(hremployee_meminfo_tran.membershipno, '') AS Membershipno " --> ", ISNULL(Id.Id_Num, '') AS Membershipno "]

            StrQ &= " FROM #TableHead " & _
                     "JOIN #TableEmp ON #TableEmp.employeeunkid = #TableHead.employeeunkid " & _
                     "LEFT JOIN prpayrollprocess_tran ON #TableHead.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                        "AND prpayrollprocess_tran.isvoid = 0 " & _
                        "AND prpayrollprocess_tran.tranheadunkid = @CoContribHeadId " & _
                     "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                        "AND prtnaleave_tran.isvoid = 0 " & _
                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                     "LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "AND hrmembership_master.isactive = 1 "
            'Hemant (27 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
            '       "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '                               " AND hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '                               "/* AND hremployee_meminfo_tran.isactive = 1*/ " & _
            '                               " AND hremployee_meminfo_tran.isdeleted = 0 "
            StrQ &= "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 employeeunkid AS IdEmp " & _
                    "			,ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_Num " & _
                    "		FROM hremployee_idinfo_tran " & _
                    "		JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                    "		WHERE cfcommon_master.masterunkid = '" & mintIdentityId & "' " & _
                    "	) AS Id ON Id.IdEmp = #TableEmp.employeeunkid "
            'Hemant (27 Aug 2019) -- End

            StrQ &= "WHERE  1 = 1 "

            objDataOperation.AddParameter("@CoContribHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadId)
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtExcelAdvance As New DataTable
            mdtExcelAdvance.Columns.Add("wcf_number", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("middlename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("lastname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("gender", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("dob", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("basicpay", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtExcelAdvance.Columns.Add("grosspay", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtExcelAdvance.Columns.Add("job_title", System.Type.GetType("System.String")).DefaultValue = ""
            mdtExcelAdvance.Columns.Add("employment_category", System.Type.GetType("System.String")).DefaultValue = ""

            For Each dtRow As DataRow In dsBasGross.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = mdtExcelAdvance.NewRow

                rpt_Row.Item("wcf_number") = dtRow.Item("Membershipno")
                rpt_Row.Item("firstname") = dtRow.Item("firstname")
                rpt_Row.Item("middlename") = dtRow.Item("othername")
                rpt_Row.Item("lastname") = dtRow.Item("surname")
                rpt_Row.Item("gender") = dtRow.Item("gendername")
                If dtRow.Item("birthdate").ToString.Trim <> "" Then
                    rpt_Row.Item("dob") = eZeeDate.convertDate(dtRow.Item("birthdate").ToString).ToString("yyyy-MM-dd")
                Else
                    rpt_Row.Item("dob") = dtRow.Item("birthdate")
                End If
                rpt_Row.Item("basicpay") = Format(CDec(dtRow.Item("BasicSalary")), mstrfmtCurrency)
                rpt_Row.Item("grosspay") = Format(CDec(dtRow.Item("TotalEarning")), mstrfmtCurrency)
                rpt_Row.Item("job_title") = dtRow.Item("jobname")
                rpt_Row.Item("employment_category") = dtRow.Item("employmenttype")

                mdtExcelAdvance.Rows.Add(rpt_Row)
            Next

            Dim intArrayColWidth(mdtExcelAdvance.Columns.Count - 1) As Integer
            For i As Integer = 0 To intArrayColWidth.Length - 1
                intArrayColWidth(i) = 130
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, xOpenReportAfterExport, mdtExcelAdvance, intArrayColWidth, False, True, False, , "EFT_WCF_(" & mstrCurrencyName & ")", , " ", , , False, Nothing, , , , False)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EFT_WCF_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (18 Mar 2019) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "UNITED REPUBLIC OF TANZANIA")
            Language.setMessage(mstrModuleName, 2, "WORKERS COMPENSATION FUND (WCF)")
            Language.setMessage(mstrModuleName, 3, "EMPLOYER’S CONTRIBUTION FORM")
            Language.setMessage(mstrModuleName, 4, "Employer's particulars")
            Language.setMessage(mstrModuleName, 5, "Name :")
            Language.setMessage(mstrModuleName, 6, "WCF Reg. No. (If available):")
            Language.setMessage(mstrModuleName, 7, "Address :")
            Language.setMessage(mstrModuleName, 8, "Phone :")
            Language.setMessage(mstrModuleName, 9, "Email :")
            Language.setMessage(mstrModuleName, 10, "Remittance Summary")
            Language.setMessage(mstrModuleName, 11, "Amount (USD/TZS) :")
            Language.setMessage(mstrModuleName, 12, "Payment Date :")
            Language.setMessage(mstrModuleName, 13, "Applicable Month :")
            Language.setMessage(mstrModuleName, 14, "Bank Name :")
            Language.setMessage(mstrModuleName, 15, "Remittance Method :")
            Language.setMessage(mstrModuleName, 16, "(Electronic Transfer,Cheque etc)")
            Language.setMessage(mstrModuleName, 17, "Employer's Authorising Officer")
            Language.setMessage(mstrModuleName, 18, "I hereby certify that to the best of my knowledge all particulars in this return are complete, true and correct.")
            Language.setMessage(mstrModuleName, 19, "Name :")
            Language.setMessage(mstrModuleName, 20, "Position  :")
            Language.setMessage(mstrModuleName, 21, "Signature of employer :")
            Language.setMessage(mstrModuleName, 22, "Date :")
            Language.setMessage(mstrModuleName, 23, "(Attach list of amounts remitted for each employee. A sample list is available)")
            Language.setMessage(mstrModuleName, 24, "WCP-1")
            Language.setMessage(mstrModuleName, 25, "ATTACHMENT TO FORM No. WCP - 1")
            Language.setMessage(mstrModuleName, 26, "List of amounts contributed for each employee")
            Language.setMessage(mstrModuleName, 27, "Employer's Name :")
            Language.setMessage(mstrModuleName, 28, "Applicable Contribution during 2015/16 :")
            Language.setMessage(mstrModuleName, 29, "(1 % of gross pay for private entities)")
            Language.setMessage(mstrModuleName, 30, "(0.5 % of gross pay for public entities)")
            Language.setMessage(mstrModuleName, 31, "S/N")
            Language.setMessage(mstrModuleName, 32, "Employee ID")
            Language.setMessage(mstrModuleName, 33, "Employee Name")
            Language.setMessage(mstrModuleName, 34, "Employee Basic Salary")
            Language.setMessage(mstrModuleName, 35, "Employee Gross Salary")
            Language.setMessage(mstrModuleName, 36, "Total")
            Language.setMessage(mstrModuleName, 37, "Total Contributions Due")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
