﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWCFFormReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.chkIngnoreZero = New System.Windows.Forms.CheckBox
        Me.objCheckAll = New System.Windows.Forms.CheckBox
        Me.lblPeriodList = New System.Windows.Forms.Label
        Me.lvPeriodList = New System.Windows.Forms.ListView
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.objColhStartDate = New System.Windows.Forms.ColumnHeader
        Me.objColhEndDate = New System.Windows.Forms.ColumnHeader
        Me.lblEmployerContribution = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlOtherEarning = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.lnkEFT_WCF_Export = New System.Windows.Forms.LinkLabel
        Me.cboIdentity = New System.Windows.Forms.ComboBox
        Me.lblIdentity = New System.Windows.Forms.Label
        Me.gbMandatoryInfo.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlOtherEarning.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 472)
        Me.NavPanel.Size = New System.Drawing.Size(703, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.cboIdentity)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIdentity)
        Me.gbMandatoryInfo.Controls.Add(Me.objlblExRate)
        Me.gbMandatoryInfo.Controls.Add(Me.dtpDate)
        Me.gbMandatoryInfo.Controls.Add(Me.lblAsOnDate)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.chkIngnoreZero)
        Me.gbMandatoryInfo.Controls.Add(Me.objCheckAll)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriodList)
        Me.gbMandatoryInfo.Controls.Add(Me.lvPeriodList)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmployerContribution)
        Me.gbMandatoryInfo.Controls.Add(Me.cboTranHead)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(9, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(437, 289)
        Me.gbMandatoryInfo.TabIndex = 0
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Info"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(208, 89)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(223, 16)
        Me.objlblExRate.TabIndex = 78
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(101, 90)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate.TabIndex = 2
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(8, 92)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(90, 16)
        Me.lblAsOnDate.TabIndex = 75
        Me.lblAsOnDate.Text = "Curr. As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(101, 63)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(104, 21)
        Me.cboCurrency.TabIndex = 1
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 62)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(90, 16)
        Me.lblCurrency.TabIndex = 72
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(339, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 70
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'chkIngnoreZero
        '
        Me.chkIngnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIngnoreZero.Location = New System.Drawing.Point(108, 266)
        Me.chkIngnoreZero.Name = "chkIngnoreZero"
        Me.chkIngnoreZero.Size = New System.Drawing.Size(125, 17)
        Me.chkIngnoreZero.TabIndex = 5
        Me.chkIngnoreZero.Text = "Ignore Zero"
        Me.chkIngnoreZero.UseVisualStyleBackColor = True
        '
        'objCheckAll
        '
        Me.objCheckAll.AutoSize = True
        Me.objCheckAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCheckAll.Location = New System.Drawing.Point(109, 145)
        Me.objCheckAll.Name = "objCheckAll"
        Me.objCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objCheckAll.TabIndex = 12
        Me.objCheckAll.UseVisualStyleBackColor = True
        '
        'lblPeriodList
        '
        Me.lblPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodList.Location = New System.Drawing.Point(5, 143)
        Me.lblPeriodList.Name = "lblPeriodList"
        Me.lblPeriodList.Size = New System.Drawing.Size(90, 16)
        Me.lblPeriodList.TabIndex = 11
        Me.lblPeriodList.Text = "Period List"
        Me.lblPeriodList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvPeriodList
        '
        Me.lvPeriodList.CheckBoxes = True
        Me.lvPeriodList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhPeriodName, Me.objColhStartDate, Me.objColhEndDate})
        Me.lvPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriodList.FullRowSelect = True
        Me.lvPeriodList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriodList.Location = New System.Drawing.Point(101, 141)
        Me.lvPeriodList.Name = "lvPeriodList"
        Me.lvPeriodList.Size = New System.Drawing.Size(324, 121)
        Me.lvPeriodList.TabIndex = 4
        Me.lvPeriodList.UseCompatibleStateImageBehavior = False
        Me.lvPeriodList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Period"
        Me.colhPeriodName.Width = 295
        '
        'objColhStartDate
        '
        Me.objColhStartDate.Tag = "objColhStartDate"
        Me.objColhStartDate.Text = "Start Date"
        Me.objColhStartDate.Width = 0
        '
        'objColhEndDate
        '
        Me.objColhEndDate.Tag = "objColhEndDate"
        Me.objColhEndDate.Text = "End Date"
        Me.objColhEndDate.Width = 0
        '
        'lblEmployerContribution
        '
        Me.lblEmployerContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerContribution.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployerContribution.Name = "lblEmployerContribution"
        Me.lblEmployerContribution.Size = New System.Drawing.Size(90, 16)
        Me.lblEmployerContribution.TabIndex = 8
        Me.lblEmployerContribution.Text = "Co. Contribution"
        Me.lblEmployerContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(101, 36)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(324, 21)
        Me.cboTranHead.TabIndex = 0
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlOtherEarning)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(9, 362)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(437, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 1
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOtherEarning
        '
        Me.pnlOtherEarning.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlOtherEarning.Controls.Add(Me.cboOtherEarning)
        Me.pnlOtherEarning.Controls.Add(Me.lblOtherEarning)
        Me.pnlOtherEarning.Location = New System.Drawing.Point(2, 26)
        Me.pnlOtherEarning.Name = "pnlOtherEarning"
        Me.pnlOtherEarning.Size = New System.Drawing.Size(431, 33)
        Me.pnlOtherEarning.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(287, 6)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 6)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(184, 21)
        Me.cboOtherEarning.TabIndex = 0
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEFT_WCF_Export
        '
        Me.lnkEFT_WCF_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_WCF_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_WCF_Export.Location = New System.Drawing.Point(110, 426)
        Me.lnkEFT_WCF_Export.Name = "lnkEFT_WCF_Export"
        Me.lnkEFT_WCF_Export.Size = New System.Drawing.Size(314, 17)
        Me.lnkEFT_WCF_Export.TabIndex = 71
        Me.lnkEFT_WCF_Export.TabStop = True
        Me.lnkEFT_WCF_Export.Text = "EFT WCF Export..."
        Me.lnkEFT_WCF_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIdentity
        '
        Me.cboIdentity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentity.DropDownWidth = 180
        Me.cboIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentity.FormattingEnabled = True
        Me.cboIdentity.Location = New System.Drawing.Point(101, 115)
        Me.cboIdentity.Name = "cboIdentity"
        Me.cboIdentity.Size = New System.Drawing.Size(118, 21)
        Me.cboIdentity.TabIndex = 105
        '
        'lblIdentity
        '
        Me.lblIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentity.Location = New System.Drawing.Point(8, 113)
        Me.lblIdentity.Name = "lblIdentity"
        Me.lblIdentity.Size = New System.Drawing.Size(90, 21)
        Me.lblIdentity.TabIndex = 104
        Me.lblIdentity.Text = "Identity"
        Me.lblIdentity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmWCFFormReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(703, 527)
        Me.Controls.Add(Me.lnkEFT_WCF_Export)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Name = "frmWCFFormReport"
        Me.Text = "frmEPFPeriodWiseReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.gbBasicSalaryOtherEarning, 0)
        Me.Controls.SetChildIndex(Me.lnkEFT_WCF_Export, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlOtherEarning.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents chkIngnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents objCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblPeriodList As System.Windows.Forms.Label
    Friend WithEvents lvPeriodList As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblEmployerContribution As System.Windows.Forms.Label
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlOtherEarning As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents lnkEFT_WCF_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents objColhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboIdentity As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdentity As System.Windows.Forms.Label
End Class
