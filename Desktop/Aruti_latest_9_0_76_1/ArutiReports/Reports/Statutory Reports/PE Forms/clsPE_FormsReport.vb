'************************************************************************************************************************************
'Class Name : clsPE_FormsReport.vb
'Purpose    :
'Date       :21/03/2017
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsPE_FormsReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPE_FormsReport"
    Private mstrReportId As String = enArutiReport.PE_Form_Report
    Private objDataOperation As clsDataOperation
    Private mdtFinal As DataTable = Nothing

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintHealthInsuranceHeadId As Integer = 0
    Private mstrHealthInsuranceHeadName As String = String.Empty
    Private mDicMemberships As Dictionary(Of Integer, String) = Nothing
    Private mDicAllocations As Dictionary(Of Integer, String) = Nothing
    Private mintIncrementReasonId As Integer = 0
    Private mstrIncrementReasonName As String = String.Empty
    Private mintPromotionReasonId As Integer = 0
    Private mstrPromotionReasonName As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnIncludeAccessFilter As Boolean = True
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintAllocationTypeId As Integer = 0
    Private mstrAllocationTypeName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _HealthInsuranceHeadId() As Integer
        Set(ByVal value As Integer)
            mintHealthInsuranceHeadId = value
        End Set
    End Property

    Public WriteOnly Property _HealthInsuranceHeadName() As String
        Set(ByVal value As String)
            mstrHealthInsuranceHeadName = value
        End Set
    End Property

    Public WriteOnly Property _DicMemberships() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mDicMemberships = value
        End Set
    End Property

    Public WriteOnly Property _DicAllocations() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mDicAllocations = value
        End Set
    End Property

    Public WriteOnly Property _IncrementReasonId() As Integer
        Set(ByVal value As Integer)
            mintIncrementReasonId = value
        End Set
    End Property

    Public WriteOnly Property _IncrementReasonName() As String
        Set(ByVal value As String)
            mstrIncrementReasonName = value
        End Set
    End Property

    Public WriteOnly Property _PromotionReasonId() As Integer
        Set(ByVal value As Integer)
            mintPromotionReasonId = value
        End Set
    End Property

    Public WriteOnly Property _PromotionReasonName() As String
        Set(ByVal value As String)
            mstrPromotionReasonName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintHealthInsuranceHeadId = 0
            mstrHealthInsuranceHeadName = String.Empty
            mDicMemberships = Nothing
            mDicAllocations = Nothing
            mintIncrementReasonId = 0
            mstrIncrementReasonName = String.Empty
            mintPromotionReasonId = 0
            mstrPromotionReasonName = String.Empty
            mstrAdvanceFilter = String.Empty
            mblnIncludeAccessFilter = True
            mblnFirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            mintAllocationTypeId = 0
            mstrAllocationTypeName = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub CreateTable(ByVal intReportTypeId As Integer)
        Try
            mdtFinal = New DataTable("Data")
            Dim dCol As DataColumn = Nothing
            Select Case intReportTypeId
                Case 2, 3, 4 '{FORM 8(b), FORM 8(c), FORM 8(d)}
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "allocationtype"
                        .Caption = mstrAllocationTypeName
                        .DataType = GetType(System.String)
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)
            End Select

            If intReportTypeId = 5 Then '{FORM 8(f)}

                dCol = New DataColumn
                With dCol
                    .ColumnName = "srno"
                    .Caption = Language.getMessage(mstrModuleName, 301, "S/N")
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ename"
                    .Caption = Language.getMessage(mstrModuleName, 302, "Employee Name")
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ecode"
                    .Caption = Language.getMessage(mstrModuleName, 303, "Check Number")
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ejob"
                    .Caption = Language.getMessage(mstrModuleName, 304, "Designation")
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "glevel"
                    .Caption = Language.getMessage(mstrModuleName, 305, "Salary Scale")
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "esalary"
                    .Caption = Language.getMessage(mstrModuleName, 306, "Basic Salary")
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ertdate"
                    .Caption = Language.getMessage(mstrModuleName, 307, "Date Of Retire")
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "allocationtype"
                    .Caption = mstrAllocationTypeName
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

            Else
                If intReportTypeId = 1 Or intReportTypeId = 2 Then
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "item"
                        .Caption = Language.getMessage(mstrModuleName, 100, "Item")
                        .DataType = GetType(System.String)
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)
                End If

                dCol = New DataColumn
                With dCol
                    .ColumnName = "tot_emp"
                    .Caption = Language.getMessage(mstrModuleName, 105, "No. Of Employee(s)")
                    .DataType = GetType(System.Int64)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "basic_sal"
                    .Caption = Language.getMessage(mstrModuleName, 101, "Basic Salary")
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ColI_" & mintIncrementReasonId.ToString
                    .Caption = mstrIncrementReasonName
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ColP_" & mintPromotionReasonId.ToString
                    .Caption = mstrPromotionReasonName
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "tot_sal"
                    If intReportTypeId = 1 Then
                        .Caption = Language.getMessage(mstrModuleName, 102, "Total Salary")
                    Else
                        .Caption = Language.getMessage(mstrModuleName, 103, "Total P.E")
                    End If
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)
                dCol.Expression = "basic_sal + ColI_" & mintIncrementReasonId.ToString & " + ColP_" & mintPromotionReasonId.ToString

                For Each iKey As Integer In mDicMemberships.Keys
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "ColM_" & iKey.ToString
                        .Caption = mDicMemberships(iKey)
                        .DataType = GetType(System.Decimal)
                        .DefaultValue = 0
                    End With
                    mdtFinal.Columns.Add(dCol)
                Next

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ColH_" & mintHealthInsuranceHeadId.ToString
                    .Caption = mstrHealthInsuranceHeadName
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "tot_deduction"
                    .Caption = Language.getMessage(mstrModuleName, 104, "Total Deductions")
                    .DataType = GetType(System.Decimal)
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)
                For Each iKey As Integer In mDicMemberships.Keys
                    dCol.Expression &= "+ColM_" & iKey.ToString
                Next
                dCol.Expression &= "+ColH_" & mintHealthInsuranceHeadId.ToString
                dCol.Expression = Mid(dCol.Expression, 2)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' THIS REPORT IS SIMILAR TO EMPLOYEE RETIREMENT REPORT, IF YOU MAKE CHANGES HERE PLEASE MAKE CHANGES ON {clsEmployeeRetirementReport.vb}
    ''' </summary>
    Private Sub Generate_PEForm8F(ByVal strDatabaseName As String, _
                                  ByVal intUserUnkid As Integer, _
                                  ByVal intYearUnkid As Integer, _
                                  ByVal intCompanyUnkid As Integer, _
                                  ByVal dtPeriodStart As Date, _
                                  ByVal dtPeriodEnd As Date, _
                                  ByVal strUserModeSetting As String, _
                                  ByVal blnOnlyApproved As Boolean, _
                                  ByVal strCurrencyFormat As String)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilter Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim strCheckedAllocations As String = String.Empty
            Dim StrAllocationFilter As String = String.Empty
            If mDicAllocations IsNot Nothing Then
                strCheckedAllocations = String.Join(",", mDicAllocations.Select(Function(x) x.Key.ToString).ToArray())
                Select Case mintAllocationTypeId
                    Case enAllocation.BRANCH
                        StrAllocationFilter = " AND nalloc.stationunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.DEPARTMENT_GROUP
                        StrAllocationFilter = " AND nalloc.deptgroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.DEPARTMENT
                        StrAllocationFilter = " AND nalloc.departmentunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.SECTION_GROUP
                        StrAllocationFilter = " AND nalloc.sectiongroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.SECTION
                        StrAllocationFilter = " AND nalloc.sectionunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.UNIT_GROUP
                        StrAllocationFilter = " AND nalloc.unitgroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.UNIT
                        StrAllocationFilter = " AND nalloc.unitunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.TEAM
                        StrAllocationFilter = " AND nalloc.teamunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.CLASS_GROUP
                        StrAllocationFilter = " AND nalloc.classgroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.CLASSES
                        StrAllocationFilter = " AND nalloc.classunkid IN (" & strCheckedAllocations & ") "
                End Select
            End If

            StrQ = "SELECT "

            If mblnFirstNamethenSurname = False Then
                StrQ &= " ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS eName "
            Else
                StrQ &= " ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS eName "
            End If

            StrQ &= "   ,hremployee_master.employeecode AS ecode " & _
                    "   ,ISNULL(JB.job_name,'') AS designation " & _
                    "   ,ISNULL(gl.name,'') as glevel " & _
                    "   ,esal.newscale as basicsalary " & _
                    "   ,CONVERT(NVARCHAR(8),RT.termination_to_date,112) as retire " & _
                    "   ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN ISNULL(nst.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN ISNULL(ndg.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN ISNULL(ndp.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN ISNULL(nsg.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN ISNULL(nsc.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN ISNULL(nug.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN ISNULL(nut.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN ISNULL(nte.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN ISNULL(ncg.name,'') " & _
                    "         WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN ISNULL(ncl.name,'') " & _
                    "    END AS egroup " & _
                    "   ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN nalloc.stationunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN nalloc.deptgroupunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN nalloc.departmentunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN nalloc.sectiongroupunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN nalloc.sectionunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN nalloc.unitgroupunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN nalloc.unitunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN nalloc.teamunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN nalloc.classgroupunkid " & _
                    "         WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN nalloc.classunkid " & _
                    "    END AS egroupid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If

            StrQ &= "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            prsalaryincrement_tran.employeeunkid " & _
                    "           ,prsalaryincrement_tran.gradelevelunkid " & _
                    "           ,prsalaryincrement_tran.newscale " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) As rno " & _
                    "       FROM prsalaryincrement_tran " & _
                    "       WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "    ) AS esal ON esal.employeeunkid = hremployee_master.employeeunkid AND esal.rno = 1 " & _
                    "    LEFT JOIN hrgradelevel_master as gl ON esal.gradelevelunkid = gl.gradelevelunkid " & _
                    "    JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "           ,date1 AS termination_to_date " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND datetypeunkid = '6' " & _
                    "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "    )RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "    )AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
                    "    JOIN hrjob_master JB ON Job.jobunkid = JB.jobunkid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            hremployee_transfer_tran.employeeunkid " & _
                    "           ,hremployee_transfer_tran.stationunkid " & _
                    "           ,hremployee_transfer_tran.deptgroupunkid " & _
                    "           ,hremployee_transfer_tran.departmentunkid " & _
                    "           ,hremployee_transfer_tran.sectiongroupunkid " & _
                    "           ,hremployee_transfer_tran.sectionunkid " & _
                    "           ,hremployee_transfer_tran.unitgroupunkid " & _
                    "           ,hremployee_transfer_tran.unitunkid " & _
                    "           ,hremployee_transfer_tran.teamunkid " & _
                    "           ,hremployee_transfer_tran.classgroupunkid " & _
                    "           ,hremployee_transfer_tran.classunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) as rno " & _
                    "       FROM hremployee_transfer_tran WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "           AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "    ) AS nalloc ON nalloc.employeeunkid = hremployee_master.employeeunkid AND nalloc.rno = 1 " & _
                    "    LEFT JOIN hrstation_master AS nst ON nalloc.stationunkid = nst.stationunkid " & _
                    "    LEFT JOIN hrdepartment_group_master AS ndg ON nalloc.deptgroupunkid = ndg.deptgroupunkid " & _
                    "    LEFT JOIN hrdepartment_master AS ndp ON nalloc.departmentunkid = ndp.departmentunkid " & _
                    "    LEFT JOIN hrsectiongroup_master AS nsg ON nalloc.sectiongroupunkid = nsg.sectiongroupunkid " & _
                    "    LEFT JOIN hrsection_master AS nsc ON nalloc.sectionunkid = nsc.sectionunkid " & _
                    "    LEFT JOIN hrunitgroup_master AS nug ON nalloc.unitgroupunkid = nug.unitgroupunkid " & _
                    "    LEFT JOIN hrunit_master AS nut ON nalloc.unitunkid = nut.unitunkid " & _
                    "    LEFT JOIN hrteam_master AS nte ON nalloc.teamunkid = nte.teamunkid " & _
                    "    LEFT JOIN hrclassgroup_master AS ncg ON nalloc.classgroupunkid = ncg.classgroupunkid " & _
                    "    LEFT JOIN hrclasses_master AS ncl ON nalloc.classunkid = ncl.classesunkid " & _
                    "WHERE CONVERT(CHAR(8),RT.termination_to_date,112) BETWEEN '" & eZeeDate.convertDate(dtPeriodStart).ToString & "' AND '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' "

            If StrAllocationFilter.Trim.Length > 0 Then
                StrQ &= StrAllocationFilter & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@AllocTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Call CreateTable(mintReportTypeId)
            Dim iCnt As Integer = 1
            For Each dtrow As DataRow In dsList.Tables(0).Rows
                Dim drow As DataRow = mdtFinal.NewRow

                drow.Item("srno") = iCnt.ToString
                drow.Item("ename") = dtrow.Item("eName")
                drow.Item("ecode") = dtrow.Item("ecode")
                drow.Item("ejob") = dtrow.Item("designation")
                drow.Item("glevel") = dtrow.Item("glevel")
                drow.Item("esalary") = Format(CDec(dtrow.Item("basicsalary")), strCurrencyFormat)
                drow.Item("ertdate") = eZeeDate.convertDate(dtrow.Item("retire").ToString).ToShortDateString
                drow.Item("allocationtype") = dtrow.Item("egroup")
                
                mdtFinal.Rows.Add(drow)
                iCnt += 1
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PEForm8F; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_PEForm8_AtoD(ByVal strDatabaseName As String, _
                                      ByVal intUserUnkid As Integer, _
                                      ByVal intYearUnkid As Integer, _
                                      ByVal intCompanyUnkid As Integer, _
                                      ByVal dtPeriodStart As Date, _
                                      ByVal dtPeriodEnd As Date, _
                                      ByVal strUserModeSetting As String, _
                                      ByVal blnOnlyApproved As Boolean, _
                                      ByVal strCurrencyFormat As String)

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsPayroll As New DataSet
        Dim dsSalary As New DataSet
        Dim dsContribution As New DataSet
        objDataOperation = New clsDataOperation
        Try
            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilter Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim strCheckedAllocations As String = String.Empty
            Dim StrAllocationFilter As String = String.Empty
            If mDicAllocations IsNot Nothing Then
                strCheckedAllocations = String.Join(",", mDicAllocations.Select(Function(x) x.Key.ToString).ToArray())
                Select Case mintAllocationTypeId
                    Case enAllocation.BRANCH
                        StrAllocationFilter = " AND nalloc.stationunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.DEPARTMENT_GROUP
                        StrAllocationFilter = " AND nalloc.deptgroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.DEPARTMENT
                        StrAllocationFilter = " AND nalloc.departmentunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.SECTION_GROUP
                        StrAllocationFilter = " AND nalloc.sectiongroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.SECTION
                        StrAllocationFilter = " AND nalloc.sectionunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.UNIT_GROUP
                        StrAllocationFilter = " AND nalloc.unitgroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.UNIT
                        StrAllocationFilter = " AND nalloc.unitunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.TEAM
                        StrAllocationFilter = " AND nalloc.teamunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.CLASS_GROUP
                        StrAllocationFilter = " AND nalloc.classgroupunkid IN (" & strCheckedAllocations & ") "
                    Case enAllocation.CLASSES
                        StrAllocationFilter = " AND nalloc.classunkid IN (" & strCheckedAllocations & ") "
                End Select
            End If

            StrQ = "SELECT " & _
                   "     A.employeeunkid AS empid " & _
                   "    ,A.basicsalary AS basic_salary " & _
                   "    ,A.fId " & _
                   "    ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN branch " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN dept_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN dept " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN sec_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN sec " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN unit_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN unit " & _
                   "          WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN team " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN cls_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN cls " & _
                   "      END AS egroup " & _
                   "    ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN stationunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN deptgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN departmentunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN sectiongroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN sectionunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN unitgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN unitunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN teamunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN classgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN classunkid " & _
                   "     END AS egroupid " & _
                   "    ,Id,GName " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT DISTINCT " & _
                   "         prpayrollprocess_tran.employeeunkid " & _
                   "        ,prtnaleave_tran.basicsalary " & _
                   "        ,1 AS fId " & _
                   "        ,nalloc.stationunkid " & _
                   "        ,nalloc.deptgroupunkid " & _
                   "        ,nalloc.departmentunkid " & _
                   "        ,nalloc.sectiongroupunkid " & _
                   "        ,nalloc.sectionunkid " & _
                   "        ,nalloc.unitgroupunkid " & _
                   "        ,nalloc.unitunkid " & _
                   "        ,nalloc.teamunkid " & _
                   "        ,nalloc.classgroupunkid " & _
                   "        ,nalloc.classunkid " & _
                   "        ,ISNULL(nst.name,'') AS branch " & _
                   "        ,ISNULL(ndg.name,'') AS dept_grp " & _
                   "        ,ISNULL(ndp.name,'') As dept " & _
                   "        ,ISNULL(nsg.name,'') AS sec_grp " & _
                   "        ,ISNULL(nsc.name,'') AS sec " & _
                   "        ,ISNULL(nug.name,'') AS unit_grp " & _
                   "        ,ISNULL(nut.name,'') AS unit " & _
                   "        ,ISNULL(nte.name,'') AS team " & _
                   "        ,ISNULL(ncg.name,'') AS cls_grp " & _
                   "        ,ISNULL(ncl.name,'') AS cls "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= "    FROM prpayrollprocess_tran " & _
                    "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "        JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If

            StrQ &= "        LEFT JOIN " & _
                    "        ( " & _
                    "            SELECT " & _
                    "                 hremployee_transfer_tran.employeeunkid " & _
                    "                ,hremployee_transfer_tran.stationunkid " & _
                    "                ,hremployee_transfer_tran.deptgroupunkid " & _
                    "                ,hremployee_transfer_tran.departmentunkid " & _
                    "                ,hremployee_transfer_tran.sectiongroupunkid " & _
                    "                ,hremployee_transfer_tran.sectionunkid " & _
                    "                ,hremployee_transfer_tran.unitgroupunkid " & _
                    "                ,hremployee_transfer_tran.unitunkid " & _
                    "                ,hremployee_transfer_tran.teamunkid " & _
                    "                ,hremployee_transfer_tran.classgroupunkid " & _
                    "                ,hremployee_transfer_tran.classunkid " & _
                    "                ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) as rno " & _
                    "            FROM hremployee_transfer_tran " & _
                    "            WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "        ) AS nalloc ON nalloc.employeeunkid = hremployee_master.employeeunkid AND nalloc.rno = 1 " & _
                    "        LEFT JOIN hrstation_master AS nst ON nalloc.stationunkid = nst.stationunkid " & _
                    "        LEFT JOIN hrdepartment_group_master AS ndg ON nalloc.deptgroupunkid = ndg.deptgroupunkid " & _
                    "        LEFT JOIN hrdepartment_master AS ndp ON nalloc.departmentunkid = ndp.departmentunkid " & _
                    "        LEFT JOIN hrsectiongroup_master AS nsg ON nalloc.sectiongroupunkid = nsg.sectiongroupunkid " & _
                    "        LEFT JOIN hrsection_master AS nsc ON nalloc.sectionunkid = nsc.sectionunkid " & _
                    "        LEFT JOIN hrunitgroup_master AS nug ON nalloc.unitgroupunkid = nug.unitgroupunkid " & _
                    "        LEFT JOIN hrunit_master AS nut ON nalloc.unitunkid = nut.unitunkid " & _
                    "        LEFT JOIN hrteam_master AS nte ON nalloc.teamunkid = nte.teamunkid " & _
                    "        LEFT JOIN hrclassgroup_master AS ncg ON nalloc.classgroupunkid = ncg.classgroupunkid " & _
                    "        LEFT JOIN hrclasses_master AS ncl ON nalloc.classunkid = ncl.classesunkid " & _
                    "    WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @payperiodunkid AND prtnaleave_tran.basicsalary > 0 "

            If StrAllocationFilter.Trim.Length > 0 Then
                StrQ &= StrAllocationFilter & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            StrQ &= "UNION " & _
                    "    SELECT DISTINCT " & _
                    "         prpayrollprocess_tran.employeeunkid " & _
                    "        ,prtnaleave_tran.basicsalary " & _
                    "        ,2 AS fId " & _
                    "        ,nalloc.stationunkid " & _
                    "        ,nalloc.deptgroupunkid " & _
                    "        ,nalloc.departmentunkid " & _
                    "        ,nalloc.sectiongroupunkid " & _
                    "        ,nalloc.sectionunkid " & _
                    "        ,nalloc.unitgroupunkid " & _
                    "        ,nalloc.unitunkid " & _
                    "        ,nalloc.teamunkid " & _
                    "        ,nalloc.classgroupunkid " & _
                    "        ,nalloc.classunkid " & _
                    "        ,ISNULL(nst.name,'') AS branch " & _
                    "        ,ISNULL(ndg.name,'') AS dept_grp " & _
                    "        ,ISNULL(ndp.name,'') As dept " & _
                    "        ,ISNULL(nsg.name,'') AS sec_grp " & _
                    "        ,ISNULL(nsc.name,'') AS sec " & _
                    "        ,ISNULL(nug.name,'') AS unit_grp " & _
                    "        ,ISNULL(nut.name,'') AS unit " & _
                    "        ,ISNULL(nte.name,'') AS team " & _
                    "        ,ISNULL(ncg.name,'') AS cls_grp " & _
                    "        ,ISNULL(ncl.name,'') AS cls "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= "    FROM prpayrollprocess_tran " & _
                    "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "        JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If
            StrQ &= "        LEFT JOIN " & _
                    "        ( " & _
                    "            SELECT " & _
                    "                 hremployee_transfer_tran.employeeunkid " & _
                    "                ,hremployee_transfer_tran.stationunkid " & _
                    "                ,hremployee_transfer_tran.deptgroupunkid " & _
                    "                ,hremployee_transfer_tran.departmentunkid " & _
                    "                ,hremployee_transfer_tran.sectiongroupunkid " & _
                    "                ,hremployee_transfer_tran.sectionunkid " & _
                    "                ,hremployee_transfer_tran.unitgroupunkid " & _
                    "                ,hremployee_transfer_tran.unitunkid " & _
                    "                ,hremployee_transfer_tran.teamunkid " & _
                    "                ,hremployee_transfer_tran.classgroupunkid " & _
                    "                ,hremployee_transfer_tran.classunkid " & _
                    "                ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) as rno " & _
                    "            FROM hremployee_transfer_tran " & _
                    "            WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "        ) AS nalloc ON nalloc.employeeunkid = hremployee_master.employeeunkid AND nalloc.rno = 1 " & _
                    "        LEFT JOIN hrstation_master AS nst ON nalloc.stationunkid = nst.stationunkid " & _
                    "        LEFT JOIN hrdepartment_group_master AS ndg ON nalloc.deptgroupunkid = ndg.deptgroupunkid " & _
                    "        LEFT JOIN hrdepartment_master AS ndp ON nalloc.departmentunkid = ndp.departmentunkid " & _
                    "        LEFT JOIN hrsectiongroup_master AS nsg ON nalloc.sectiongroupunkid = nsg.sectiongroupunkid " & _
                    "        LEFT JOIN hrsection_master AS nsc ON nalloc.sectionunkid = nsc.sectionunkid " & _
                    "        LEFT JOIN hrunitgroup_master AS nug ON nalloc.unitgroupunkid = nug.unitgroupunkid " & _
                    "        LEFT JOIN hrunit_master AS nut ON nalloc.unitunkid = nut.unitunkid " & _
                    "        LEFT JOIN hrteam_master AS nte ON nalloc.teamunkid = nte.teamunkid " & _
                    "        LEFT JOIN hrclassgroup_master AS ncg ON nalloc.classgroupunkid = ncg.classgroupunkid " & _
                    "        LEFT JOIN hrclasses_master AS ncl ON nalloc.classunkid = ncl.classesunkid " & _
                    "    WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @payperiodunkid AND prtnaleave_tran.basicsalary <= 0 "

            If StrAllocationFilter.Trim.Length > 0 Then
                StrQ &= StrAllocationFilter & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            StrQ &= ") AS A "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@AllocTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsPayroll = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT " & _
                   "     B.reason_id " & _
                   "    ,B.newscale AS scale " & _
                   "    ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN branch " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN dept_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN dept " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN sec_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN sec " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN unit_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN unit " & _
                   "          WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN team " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN cls_grp " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN cls " & _
                   "     END AS egroup " & _
                   "    ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN stationunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN deptgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN departmentunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN sectiongroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN sectionunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN unitgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN unitunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN teamunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN classgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN classunkid " & _
                   "     END AS egroupid " & _
                   "    ,Id, GName " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         prsalaryincrement_tran.reason_id " & _
                   "        ,prsalaryincrement_tran.newscale " & _
                   "        ,prsalaryincrement_tran.employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC,prsalaryincrement_tran.salaryincrementtranunkid DESC) as rno " & _
                   "        ,nalloc.stationunkid " & _
                   "        ,nalloc.deptgroupunkid " & _
                   "        ,nalloc.departmentunkid " & _
                   "        ,nalloc.sectiongroupunkid " & _
                   "        ,nalloc.sectionunkid " & _
                   "        ,nalloc.unitgroupunkid " & _
                   "        ,nalloc.unitunkid " & _
                   "        ,nalloc.teamunkid " & _
                   "        ,nalloc.classgroupunkid " & _
                   "        ,nalloc.classunkid " & _
                   "        ,ISNULL(nst.name,'') AS branch " & _
                   "        ,ISNULL(ndg.name,'') AS dept_grp " & _
                   "        ,ISNULL(ndp.name,'') As dept " & _
                   "        ,ISNULL(nsg.name,'') AS sec_grp " & _
                   "        ,ISNULL(nsc.name,'') AS sec " & _
                   "        ,ISNULL(nug.name,'') AS unit_grp " & _
                   "        ,ISNULL(nut.name,'') AS unit " & _
                   "        ,ISNULL(nte.name,'') AS team " & _
                   "        ,ISNULL(ncg.name,'') AS cls_grp " & _
                   "        ,ISNULL(ncl.name,'') AS cls "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= "    FROM prsalaryincrement_tran " & _
                    "        JOIN prpayrollprocess_tran ON prpayrollprocess_tran.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                    "        JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If

            StrQ &= "        LEFT JOIN " & _
                    "        ( " & _
                    "            SELECT " & _
                    "                 hremployee_transfer_tran.employeeunkid " & _
                    "                ,hremployee_transfer_tran.stationunkid " & _
                    "                ,hremployee_transfer_tran.deptgroupunkid " & _
                    "                ,hremployee_transfer_tran.departmentunkid " & _
                    "                ,hremployee_transfer_tran.sectiongroupunkid " & _
                    "                ,hremployee_transfer_tran.sectionunkid " & _
                    "                ,hremployee_transfer_tran.unitgroupunkid " & _
                    "                ,hremployee_transfer_tran.unitunkid " & _
                    "                ,hremployee_transfer_tran.teamunkid " & _
                    "                ,hremployee_transfer_tran.classgroupunkid " & _
                    "                ,hremployee_transfer_tran.classunkid " & _
                    "                ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) as rno " & _
                    "            FROM hremployee_transfer_tran " & _
                    "            WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "               AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "        ) AS nalloc ON nalloc.employeeunkid = hremployee_master.employeeunkid AND nalloc.rno = 1 " & _
                    "        LEFT JOIN hrstation_master AS nst ON nalloc.stationunkid = nst.stationunkid " & _
                    "        LEFT JOIN hrdepartment_group_master AS ndg ON nalloc.deptgroupunkid = ndg.deptgroupunkid " & _
                    "        LEFT JOIN hrdepartment_master AS ndp ON nalloc.departmentunkid = ndp.departmentunkid " & _
                    "        LEFT JOIN hrsectiongroup_master AS nsg ON nalloc.sectiongroupunkid = nsg.sectiongroupunkid " & _
                    "        LEFT JOIN hrsection_master AS nsc ON nalloc.sectionunkid = nsc.sectionunkid " & _
                    "        LEFT JOIN hrunitgroup_master AS nug ON nalloc.unitgroupunkid = nug.unitgroupunkid " & _
                    "        LEFT JOIN hrunit_master AS nut ON nalloc.unitunkid = nut.unitunkid " & _
                    "        LEFT JOIN hrteam_master AS nte ON nalloc.teamunkid = nte.teamunkid " & _
                    "        LEFT JOIN hrclassgroup_master AS ncg ON nalloc.classgroupunkid = ncg.classgroupunkid " & _
                    "        LEFT JOIN hrclasses_master AS ncl ON nalloc.classunkid = ncl.classesunkid " & _
                    "        JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "        JOIN cfcommon_master ON cfcommon_master.masterunkid = prsalaryincrement_tran.reason_id " & _
                    "        WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND prsalaryincrement_tran.periodunkid = @payperiodunkid " & _
                    "            AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "            AND prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @payperiodunkid AND prtnaleave_tran.basicsalary > 0 " & _
                    "            AND cfcommon_master.masterunkid IN (" & mintIncrementReasonId & " ," & mintPromotionReasonId & ") "

            If StrAllocationFilter.Trim.Length > 0 Then
                StrQ &= StrAllocationFilter & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            StrQ &= " ) AS B WHERE B.rno = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@AllocTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsSalary = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim strCsvHeadsSelected As String = String.Empty
            strCsvHeadsSelected = String.Join(",", mDicMemberships.Select(Function(x) x.Key.ToString).ToArray())
            strCsvHeadsSelected &= "," & mintHealthInsuranceHeadId.ToString

            StrQ = "SELECT " & _
                   "     prpayrollprocess_tran.tranheadunkid " & _
                   "    ,prpayrollprocess_tran.amount AS contribution " & _
                   "    ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN ISNULL(nst.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN ISNULL(ndg.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN ISNULL(ndp.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN ISNULL(nsg.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN ISNULL(nsc.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN ISNULL(nug.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN ISNULL(nut.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN ISNULL(nte.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN ISNULL(ncg.name,'') " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN ISNULL(ncl.name,'') " & _
                   "     END AS egroup " & _
                   "    ,CASE WHEN @AllocTypeId = " & enAllocation.BRANCH & " THEN nalloc.stationunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT_GROUP & " THEN nalloc.deptgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.DEPARTMENT & " THEN nalloc.departmentunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION_GROUP & " THEN nalloc.sectiongroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.SECTION & " THEN nalloc.sectionunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT_GROUP & " THEN nalloc.unitgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.UNIT & " THEN nalloc.unitunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.TEAM & " THEN nalloc.teamunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASS_GROUP & " THEN nalloc.classgroupunkid " & _
                   "          WHEN @AllocTypeId = " & enAllocation.CLASSES & " THEN nalloc.classunkid " & _
                   "     END AS egroupid " & _
                   "    ,CASE WHEN prpayrollprocess_tran.tranheadunkid = " & mintHealthInsuranceHeadId & " THEN 1 ELSE 0 END AS iHealth "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= " FROM prpayrollprocess_tran " & _
                    "    JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If

            StrQ &= "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             hremployee_transfer_tran.employeeunkid " & _
                    "            ,hremployee_transfer_tran.stationunkid " & _
                    "            ,hremployee_transfer_tran.deptgroupunkid " & _
                    "            ,hremployee_transfer_tran.departmentunkid " & _
                    "            ,hremployee_transfer_tran.sectiongroupunkid " & _
                    "            ,hremployee_transfer_tran.sectionunkid " & _
                    "            ,hremployee_transfer_tran.unitgroupunkid " & _
                    "            ,hremployee_transfer_tran.unitunkid " & _
                    "            ,hremployee_transfer_tran.teamunkid " & _
                    "            ,hremployee_transfer_tran.classgroupunkid " & _
                    "            ,hremployee_transfer_tran.classunkid " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) as rno " & _
                    "        FROM hremployee_transfer_tran " & _
                    "        WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "            AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    "    ) AS nalloc ON nalloc.employeeunkid = hremployee_master.employeeunkid AND nalloc.rno = 1 " & _
                    "    LEFT JOIN hrstation_master AS nst ON nalloc.stationunkid = nst.stationunkid " & _
                    "    LEFT JOIN hrdepartment_group_master AS ndg ON nalloc.deptgroupunkid = ndg.deptgroupunkid " & _
                    "    LEFT JOIN hrdepartment_master AS ndp ON nalloc.departmentunkid = ndp.departmentunkid " & _
                    "    LEFT JOIN hrsectiongroup_master AS nsg ON nalloc.sectiongroupunkid = nsg.sectiongroupunkid " & _
                    "    LEFT JOIN hrsection_master AS nsc ON nalloc.sectionunkid = nsc.sectionunkid " & _
                    "    LEFT JOIN hrunitgroup_master AS nug ON nalloc.unitgroupunkid = nug.unitgroupunkid " & _
                    "    LEFT JOIN hrunit_master AS nut ON nalloc.unitunkid = nut.unitunkid " & _
                    "    LEFT JOIN hrteam_master AS nte ON nalloc.teamunkid = nte.teamunkid " & _
                    "    LEFT JOIN hrclassgroup_master AS ncg ON nalloc.classgroupunkid = ncg.classgroupunkid " & _
                    "    LEFT JOIN hrclasses_master AS ncl ON nalloc.classunkid = ncl.classesunkid " & _
                    "    JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @payperiodunkid AND prtnaleave_tran.basicsalary > 0 " & _
                    "AND prpayrollprocess_tran.tranheadunkid IN (" & strCsvHeadsSelected & ") "

            If StrAllocationFilter.Trim.Length > 0 Then
                StrQ &= StrAllocationFilter & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@AllocTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsContribution = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Call CreateTable(mintReportTypeId)

            Select Case mintReportTypeId
                Case 1  '{Form8(a)}
                    Dim dtmp As DataRow() = Nothing
                    dtmp = dsPayroll.Tables(0).Select("fId = 1") ' WHERE BASIC SALARY IS > 0
                    Dim dRow As DataRow = mdtFinal.NewRow
                    dRow.Item("item") = Language.getMessage(mstrModuleName, 200, "Existing Employees on Payroll")

                    If dtmp.Length > 0 Then
                        dRow.Item("tot_emp") = dtmp.CopyToDataTable.Compute("COUNT(empid)", "")
                        dRow.Item("basic_sal") = Format(CDec(dtmp.CopyToDataTable.Compute("SUM(basic_salary)", "")), strCurrencyFormat)
                    End If

                    'S.SANDEEP [31-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                    If dsSalary.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintIncrementReasonId)) Then
                        dRow.Item("ColI_" & mintIncrementReasonId.ToString) = 0
                    Else
                        dRow.Item("ColI_" & mintIncrementReasonId.ToString) = Format(CDec(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintIncrementReasonId)), strCurrencyFormat)
                    End If
                    If IsDBNull(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintPromotionReasonId)) Then
                        dRow.Item("ColP_" & mintPromotionReasonId.ToString) = 0
                    Else
                        dRow.Item("ColP_" & mintPromotionReasonId.ToString) = Format(CDec(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintPromotionReasonId)), strCurrencyFormat)
                    End If
                    End If

                    If dsContribution.Tables(0).Rows.Count > 0 Then
                    For Each iKey As Integer In mDicMemberships.Keys

                            'S.SANDEEP [11-APR-2017] -- START
                            'ISSUE/ENHANCEMENT : SOME MEMBERSHIP(S) NOT HAVING VALUE FOR THE SELECTED PERIOD
                            'dRow.Item("ColM_" & iKey.ToString) = Format(CDec(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & iKey)), strCurrencyFormat)
                            If IsDBNull(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & iKey)) Then
                                dRow.Item("ColM_" & iKey.ToString) = 0
                            Else
                        dRow.Item("ColM_" & iKey.ToString) = Format(CDec(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & iKey)), strCurrencyFormat)
                            End If
                            'S.SANDEEP [11-APR-2017] -- END

                    Next

                        'S.SANDEEP [11-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : SOME MEMBERSHIP(S) NOT HAVING VALUE FOR THE SELECTED PERIOD
                        'dRow.Item("ColH_" & mintHealthInsuranceHeadId.ToString) = Format(CDec(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mintHealthInsuranceHeadId)), strCurrencyFormat)
                        If IsDBNull(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mintHealthInsuranceHeadId)) Then
                            dRow.Item("ColH_" & mintHealthInsuranceHeadId.ToString) = 0
                        Else
                    dRow.Item("ColH_" & mintHealthInsuranceHeadId.ToString) = Format(CDec(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mintHealthInsuranceHeadId)), strCurrencyFormat)
                    End If
                        'S.SANDEEP [11-APR-2017] -- END

                    End If
                    mdtFinal.Rows.Add(dRow)
                    'S.SANDEEP [31-MAR-2017] -- END

                    dtmp = dsPayroll.Tables(0).Select("fId = 2") ' WHERE BASIC SALARY IS <= 0
                    dRow = mdtFinal.NewRow
                    dRow.Item("item") = Language.getMessage(mstrModuleName, 201, "Existing Employees not in Payroll (Study leave and Contracts)")
                    If dtmp.Length > 0 Then
                        dRow.Item("tot_emp") = dtmp.CopyToDataTable.Compute("COUNT(empid)", "")
                    End If
                    mdtFinal.Rows.Add(dRow)

                    dRow = mdtFinal.NewRow
                    dRow.Item("item") = Language.getMessage(mstrModuleName, 202, "New Employees to be recruited")
                    mdtFinal.Rows.Add(dRow)

                Case 2, 3, 4 '{Form8(b), Form8(c), Form8(d) }
                    Dim dtmp As DataRow() = Nothing
                    For Each iKey As Integer In mDicAllocations.Keys

                        If mintReportTypeId = 2 Or mintReportTypeId = 3 Then
                            dtmp = dsPayroll.Tables(0).Select("fId = 1 AND egroupid = '" & iKey & "'") ' WHERE BASIC SALARY IS > 0
                            Dim dRow As DataRow = mdtFinal.NewRow

                            dRow.Item("allocationtype") = mDicAllocations(iKey)

                            If mintReportTypeId = 2 Then
                                dRow.Item("item") = Language.getMessage(mstrModuleName, 200, "Existing Employees on Payroll")
                            End If

                            If dtmp.Length > 0 Then
                                If IsDBNull(dtmp.CopyToDataTable.Compute("COUNT(empid)", "egroupid = '" & iKey & "'")) = False Then
                                    dRow.Item("tot_emp") = dtmp.CopyToDataTable.Compute("COUNT(empid)", "egroupid = '" & iKey & "'")
                                End If

                                If IsDBNull(dtmp.CopyToDataTable.Compute("SUM(basic_salary)", "egroupid = '" & iKey & "'")) = False Then
                                    dRow.Item("basic_sal") = Format(CDec(dtmp.CopyToDataTable.Compute("SUM(basic_salary)", "egroupid = '" & iKey & "'")), strCurrencyFormat)
                                End If
                            End If

                            If IsDBNull(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintIncrementReasonId & " AND egroupid = '" & iKey & "'")) Then
                                dRow.Item("ColI_" & mintIncrementReasonId.ToString) = 0
                            Else
                                dRow.Item("ColI_" & mintIncrementReasonId.ToString) = Format(CDec(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintIncrementReasonId & " AND egroupid = '" & iKey & "'")), strCurrencyFormat)
                            End If

                            If IsDBNull(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintPromotionReasonId & " AND egroupid = '" & iKey & "'")) Then
                                dRow.Item("ColP_" & mintPromotionReasonId.ToString) = 0
                            Else
                                dRow.Item("ColP_" & mintPromotionReasonId.ToString) = Format(CDec(dsSalary.Tables(0).Compute("SUM(scale)", "reason_id = " & mintPromotionReasonId & " AND egroupid = '" & iKey & "'")), strCurrencyFormat)
                            End If

                            For Each mKey As Integer In mDicMemberships.Keys
                                If IsDBNull(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mKey & " AND egroupid = '" & iKey & "'")) Then
                                    dRow.Item("ColM_" & mKey.ToString) = 0
                                Else
                                    dRow.Item("ColM_" & mKey.ToString) = Format(CDec(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mKey & " AND egroupid = '" & iKey & "'")), strCurrencyFormat)
                                End If
                            Next

                            If IsDBNull(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mintHealthInsuranceHeadId & " AND egroupid = '" & iKey & "'")) = False Then
                                dRow.Item("ColH_" & mintHealthInsuranceHeadId.ToString) = Format(CDec(dsContribution.Tables(0).Compute("SUM(contribution)", "tranheadunkid = " & mintHealthInsuranceHeadId & " AND egroupid = '" & iKey & "'")), strCurrencyFormat)
                            End If

                            mdtFinal.Rows.Add(dRow)
                        End If



                        If mintReportTypeId = 2 Or mintReportTypeId = 4 Then
                            dtmp = dsPayroll.Tables(0).Select("fId = 2 AND egroupid = '" & iKey & "'") ' WHERE BASIC SALARY IS <= 0
                            Dim dRow As DataRow = mdtFinal.NewRow
                            dRow.Item("allocationtype") = mDicAllocations(iKey)
                            If mintReportTypeId = 2 Then
                                dRow.Item("item") = Language.getMessage(mstrModuleName, 201, "Existing Employees not in Payroll (Study leave and Contracts)")
                            End If

                            If dtmp.Length > 0 Then
                                If IsDBNull(dtmp.CopyToDataTable.Compute("COUNT(empid)", "egroupid = '" & iKey & "'")) = False Then
                                    dRow.Item("tot_emp") = dtmp.CopyToDataTable.Compute("COUNT(empid)", "egroupid = '" & iKey & "'")
                                End If
                            End If

                            If mintReportTypeId = 4 And dtmp.Length > 0 Then
                                mdtFinal.Rows.Add(dRow)
                            ElseIf mintReportTypeId = 2 Then
                                mdtFinal.Rows.Add(dRow)
                            End If


                            If mintReportTypeId = 2 Then
                                dRow = mdtFinal.NewRow
                                dRow.Item("allocationtype") = mDicAllocations(iKey)
                                If mintReportTypeId = 2 Then
                                    dRow.Item("item") = Language.getMessage(mstrModuleName, 202, "New Employees to be recruited")
                                End If
                                mdtFinal.Rows.Add(dRow)
                            End If
                        End If


                    Next
            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PEForm8_AtoD; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Public Sub Export_PE_Forms(ByVal strDatabaseName As String, _
                               ByVal intUserUnkid As Integer, _
                               ByVal intYearUnkid As Integer, _
                               ByVal intCompanyUnkid As Integer, _
                               ByVal dtPeriodStart As Date, _
                               ByVal dtPeriodEnd As Date, _
                               ByVal strUserModeSetting As String, _
                               ByVal blnOnlyApproved As Boolean, _
                               ByVal xOpenReportAfterExport As Boolean, _
                               ByVal xExportPath As String, ByVal strCurrencyFormat As String)
        Try
            Select Case mintReportTypeId
                Case 1, 2, 3, 4 '{FORM 8(a), FORM 8(b), FORM 8(c), FORM 8(d)}
                    Call Generate_PEForm8_AtoD(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, strCurrencyFormat)
                Case 5  '{FROM 8(f)}
                    Call Generate_PEForm8F(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, strCurrencyFormat)
            End Select

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtFinal.Columns.Count - 1)
            For j As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(j) = 125
            Next

            'S.SANDEEP [31-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
            'Dim strReportName As String = mstrReportTypeName
            Dim strReportName As String = mstrReportTypeName.Split("-")(0).Trim
            Dim strFilterString As String = ""
            'S.SANDEEP [31-MAR-2017] -- END
            Dim strExtraReportTitle As String = ""
            Dim strarrGroupColumns As String() = Nothing
            Dim strGrpCols As String() = Nothing
            Select Case mintReportTypeId
                Case 1
                    strFilterString = Language.getMessage(mstrModuleName, 400, "Period :") & " " & mstrPeriodName
                    If mintEmployeeId > 0 Then
                        strFilterString &= ", " & Language.getMessage(mstrModuleName, 401, "Employee :") & " " & mstrEmployeeName
                    End If
                    strExtraReportTitle = mstrReportTypeName.Split("-")(1).Trim
                Case 2, 3, 4
                    strFilterString = Language.getMessage(mstrModuleName, 400, "Period :") & " " & mstrPeriodName
                    If mintEmployeeId > 0 Then
                        strFilterString &= ", " & Language.getMessage(mstrModuleName, 401, "Employee :") & " " & mstrEmployeeName
                    End If
                    strExtraReportTitle = mstrReportTypeName.Split("-")(1).Trim
                    If mintReportTypeId = 2 Then
                        strGrpCols = New String() {"allocationtype"}
                        strarrGroupColumns = strGrpCols
                    End If
                Case 5
                    strFilterString = Language.getMessage(mstrModuleName, 402, "From Date :") & " " & dtPeriodStart.ToShortDateString & " " & _
                                      Language.getMessage(mstrModuleName, 403, "To Date :") & " " & dtPeriodEnd.ToShortDateString

                    If mintEmployeeId > 0 Then
                        strFilterString &= ", " & Language.getMessage(mstrModuleName, 401, "Employee :") & " " & mstrEmployeeName
                    End If

                    strExtraReportTitle = mstrReportTypeName.Split("-")(1).Trim
            End Select

            'strReportName & "#10;"
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportPath, xOpenReportAfterExport, mdtFinal, intArrayColumnWidth, True, True, True, strarrGroupColumns, strReportName, strExtraReportTitle, strFilterString, Nothing, "", False, Nothing, Nothing, Nothing)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_PE_Forms; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Item")
            Language.setMessage(mstrModuleName, 101, "Basic Salary")
            Language.setMessage(mstrModuleName, 102, "Total Salary")
            Language.setMessage(mstrModuleName, 103, "Total P.E")
            Language.setMessage(mstrModuleName, 104, "Total Deductions")
            Language.setMessage(mstrModuleName, 105, "No. Of Employee(s)")
            Language.setMessage(mstrModuleName, 200, "Existing Employees on Payroll")
            Language.setMessage(mstrModuleName, 201, "Existing Employees not in Payroll (Study leave and Contracts)")
            Language.setMessage(mstrModuleName, 202, "New Employees to be recruited")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
