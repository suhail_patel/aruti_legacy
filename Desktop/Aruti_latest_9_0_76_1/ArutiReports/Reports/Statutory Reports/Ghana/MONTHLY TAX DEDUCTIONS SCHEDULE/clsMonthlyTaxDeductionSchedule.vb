'************************************************************************************************************************************
'Class Name : clsMonthlyTaxDeductionSchedule.vb
'Purpose    :
'Date       :03/06/2015
'Written By :Shani
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Shani
''' </summary>
Public Class clsMonthlyTaxDeductionSchedule
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMonthlyTaxDeductionSchedule"
    Private mstrReportId As String = enArutiReport.Monthly_Tax_Income_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = String.Empty

    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private marrDatabaseName As New ArrayList
    Private mstrCurrDatabaseName As String = FinancialYear._Object._DatabaseName

    Private mintPenstionSSFUnkID As Integer = 0
    Private mstrPenstionSSFName As String = String.Empty
    Private mintPenstionThirdTireUnkID As Integer = 0
    Private mstrPenstionThirdTireName As String = String.Empty

    Private mintAccomodationElementUnkID As Integer = 0
    Private mstrAccomodationElementName As String = String.Empty
    Private mintVehicleElementUnkID As Integer = 0
    Private mstrVehicleElementName As String = String.Empty


    Private mintTINNoMembershipUnkID As Integer = 0
    Private mstrTINNoMembershipName As String = String.Empty



    Private mintOtherEarningTranId As Integer = 0
    Private mstrOtherEarningTranName As String = String.Empty

    Private mblnIncludeInactiveEmp As Boolean = True

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mDicDataBaseYearId As Dictionary(Of String, Integer)
    Private mDicDataBaseEndDate As Dictionary(Of String, String)
    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (03 Aug 2019) -- Start
    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
    Private mdtEmpAsOnDate As Date
    'Sohail (03 Aug 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _CurrDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _PenstionSSFID() As Integer
        Set(ByVal value As Integer)
            mintPenstionSSFUnkID = value
        End Set
    End Property

    Public WriteOnly Property _PenstionSSFName() As String
        Set(ByVal value As String)
            mstrPenstionSSFName = value
        End Set
    End Property

    Public WriteOnly Property _AccomodationElementID() As Integer
        Set(ByVal value As Integer)
            mintAccomodationElementUnkID = value
        End Set
    End Property

    Public WriteOnly Property _AccomodationElementName() As String
        Set(ByVal value As String)
            mstrAccomodationElementName = value
        End Set
    End Property

    Public WriteOnly Property _PenstionThirdTireID() As Integer
        Set(ByVal value As Integer)
            mintPenstionThirdTireUnkID = value
        End Set
    End Property

    Public WriteOnly Property _PenstionThirdTireName() As String
        Set(ByVal value As String)
            mstrPenstionThirdTireName = value
        End Set
    End Property

    Public WriteOnly Property _TINNoMembershipId() As Integer
        Set(ByVal value As Integer)
            mintTINNoMembershipUnkID = value
        End Set
    End Property

    Public WriteOnly Property _TINNoMembershipName() As String
        Set(ByVal value As String)
            mstrTINNoMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _VehicleElementId() As Integer
        Set(ByVal value As Integer)
            mintVehicleElementUnkID = value
        End Set
    End Property

    Public WriteOnly Property _VehicleElementName() As String
        Set(ByVal value As String)
            mstrVehicleElementName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranName() As String
        Set(ByVal value As String)
            mstrOtherEarningTranName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property


    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public WriteOnly Property _DicDataBaseYearId() As Dictionary(Of String, Integer)
        Set(ByVal value As Dictionary(Of String, Integer))
            mDicDataBaseYearId = value
        End Set
    End Property

    Public WriteOnly Property _DicDataBaseEndDate() As Dictionary(Of String, String)
        Set(ByVal value As Dictionary(Of String, String))
            mDicDataBaseEndDate = value
        End Set
    End Property
    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (03 Aug 2019) -- Start
    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
    Public WriteOnly Property _EmpAsOnDate() As Date
        Set(ByVal value As Date)
            mdtEmpAsOnDate = value
        End Set
    End Property
    'Sohail (03 Aug 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = ""
            mstrPeriodIds = ""
            mstrPeriodNames = ""
            mintEmpId = 0
            mstrEmpName = ""
            mintPenstionSSFUnkID = 0
            mintPenstionThirdTireUnkID = 0
            mintAccomodationElementUnkID = 0
            mintVehicleElementUnkID = 0
            mintTINNoMembershipUnkID = 0
            mintOtherEarningTranId = 0


            mblnIncludeInactiveEmp = True


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""


            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            mDicDataBaseYearId = Nothing
            mDicDataBaseEndDate = Nothing
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@PenstionSSFID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPenstionSSFUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Penstion SSF :") & " " & mstrPenstionSSFName & " "

            objDataOperation.AddParameter("@penstionThirdTire", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPenstionThirdTireUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Penstion Third Tire :") & " " & mstrPenstionThirdTireName & " "

            objDataOperation.AddParameter("@AccomodationElement", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccomodationElementUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Accomodation Element :") & " " & mstrAccomodationElementName & " "

            objDataOperation.AddParameter("@VehicleElement ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVehicleElementUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Vehicle Element :") & " " & mstrVehicleElementName & " "

            objDataOperation.AddParameter("@TINNOid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTINNoMembershipUnkID)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "TIN No. :") & " " & mstrTINNoMembershipName & " "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Employee :") & " " & mstrEmpName & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Order By : ") & " " & Me.OrderByDisplay
            End If
            If mintViewIndex > 0 Then
                Me._FilterQuery &= " ORDER BY GName, TablePenstionSSF.EmpName "
            Else
                Me._FilterQuery &= " ORDER BY TablePenstionSSF.EmpName "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 8, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 9, "Employee Name")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Generate_DetailReport(ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean, _
                                          ByVal intBaseCurrencyId As Integer) As Boolean
        'Public Function Generate_DetailReport() As Boolean
        'S.SANDEEP [04 JUN 2015] -- END
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim dtPeriod As DataTable = Nothing
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            'S.SANDEEP [04 JUN 2015] -- END

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY TablePenstionSSF.EmpName) AS NVARCHAR(50)) AS SrNo " & _
                            ", TablePenstionSSF.EmpId " & _
                            ", TablePenstionSSF.EmpCode AS EmpCode " & _
                            ", TablePenstionSSF.EmpName " & _
                            ", TablePenstionSSF.JOB_Title AS Position " & _
                            ", TablePenstionSSF.MembershipUnkId AS MemId " & _
                            ", TablePenstionSSF.Membership_No AS MembershipNo " & _
                            ", TablePenstionSSF.Membership_Name AS MembershipName " & _
                            ", '' AS NonResident " & _
                            ", ISNULL(TableTINNO.Membership_No, '') AS TINNO " & _
                            ", SUM(ISNULL(BasicSal, 0)) AS BasicSal " & _
                            ", '' AS SecondaryEmployment " & _
                            ", SUM(ISNULL(TablePenstionSSF.Amount, 0)) AS SSF " & _
                            ", SUM(ISNULL(TablePenstionThirdTire.Amount, 0)) AS ThirdTier " & _
                            ", SUM(ISNULL(TableTaxble.Amount, 0) - ISNULL(BasicSal, 0)) AS CashAllowance " & _
                            ", 0 AS Income " & _
                            ", 0 AS FTaxBonus " & _
                            ", 0 AS ExcessBonus " & _
                            ", SUM(ISNULL(BasicSal, 0)) + SUM(ISNULL(TableTaxble.Amount, 0) - ISNULL(BasicSal, 0)) + 0 AS TotalCashEmolument " & _
                            ", SUM(ISNULL(TableHouseAllowance.Amount, 0)) AS AccomodationElement" & _
                            ", SUM(ISNULL(TableTranspAllowance.Amount, 0)) AS VehicleElement" & _
                            ", SUM(ISNULL(TableNonCash.Amount, 0)) AS NonCashBenefit " & _
                            ", SUM(ISNULL(TableTaxble.Amount, 0)) AS TOTAL_ASSESSABLE_INCOME " & _
                            ", SUM(ISNULL(TableTaxRelief.Amount, 0)) AS DeductibleRelief " & _
                            ", SUM(ISNULL(TablePenstionThirdTire.Amount, 0)) + SUM(ISNULL(TablePenstionThirdTire.Amount, 0)) + SUM(ISNULL(TableTaxRelief.Amount, 0)) AS TotalRelief " & _
                            ", SUM(ISNULL(TableTaxble.Amount, 0)) - (SUM(ISNULL(TablePenstionThirdTire.Amount, 0)) + SUM(ISNULL(TablePenstionThirdTire.Amount, 0)) + SUM(ISNULL(TableTaxRelief.Amount, 0))) AS ChargeableIncome " & _
                            ", SUM(ISNULL(TableDeduction.Amount, 0)) AS TaxDeductible " & _
                            ", 0 AS OTIncome " & _
                            ", 0 AS OTTax " & _
                            ", 0 + SUM(ISNULL(TableDeduction.Amount, 0)) + 0 AS TaxGRA " & _
                            ", 0 AS SeverancePaid " & _
                            ", '' AS Remarks " & _
                            ", 0 AS DBBNSSS " & _
                            ", SUM(ISNULL(TableGross.GrossPay, 0)) AS GrossPay " & _
                            ", TablePenstionSSF.Id " & _
                            ", TablePenstionSSF.GName " & _
                    "FROM  ( "
                '======================Penstion SSF (START) =============
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT               ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid " & _
                                ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                          ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '01-Jan-1900'), 112) AS end_date " & _
                          ", hremployee_master.employeeunkid AS Empid " & _
                          ", hremployee_master.employeecode AS EmpCode " & _
                          ", ISNULL(hrjob_master.job_name, '') AS JOB_Title " & _
                          ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)) AS Amount " & _
                          ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                          ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                          ", hrmembership_master.membershipname AS Membership_Name "

                    If mblnFirstNamethenSurname = True Then
                        StrInnerQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                    Else
                        StrInnerQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
                    End If

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrInnerQ &= "FROM  " & strDatabaseName & "..prpayrollprocess_tran " & _
                    '               "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '               "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                    '                               "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                               "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                    '               "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    '                               "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                    '               "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    '               "JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                    '               "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "


                    StrInnerQ &= "FROM  " & strDatabaseName & "..prpayrollprocess_tran " & _
                                   "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                   "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                   "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                   "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                   "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                   "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                   "JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                   "LEFT JOIN " & _
                                   "( " & _
                                   "    SELECT " & _
                                   "         jobunkid " & _
                                   "        ,jobgroupunkid " & _
                                   "        ,employeeunkid " & _
                                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                   "    FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & mDicDataBaseEndDate(strDatabaseName).ToString & "' " & _
                                   ") AS Jobs ON Jobs.employeeunkid = " & strDatabaseName & "..hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                   "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid "
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND hrmembership_master.membershipunkid =  @PenstionSSFID " & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= "        ) AS TablePenstionSSF  " & _
                            "LEFT JOIN ( "
                '============ Penstion SSF (END)================

                '============ Penstion Third Tire (START)=======
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid " & _
                                    ", hremployee_master.employeeunkid AS Empid " & _
                                    ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount " & _
                                    ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                    ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                    ", hrmembership_master.membershipname AS Membership_Name "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                        "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                        "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                        "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "



                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND hrmembership_master.membershipunkid  = @penstionThirdTire " & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "


                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= " ) AS TablePenstionThirdTire ON TablePenstionSSF.Periodid = TablePenstionThirdTire.Periodid " & _
                                   "AND TablePenstionSSF.Empid = TablePenstionThirdTire.Empid " & _
                            "LEFT JOIN ( "

                '============ Penstion Third Tire (END)=======

                '============ TINNO (START)=======

                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  hremployee_master.employeeunkid AS Empid  " & _
                                        ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                        ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                        ", hrmembership_master.membershipname AS Membership_Name "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= " FROM " & mstrCurrDatabaseName & "..hremployee_master " & _
                                   "JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted , 0) = 0 " & _
                                   "JOIN " & strDatabaseName & "..hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        'Sohail (07 Apr 2017) -- Start
                        'Issue - 65.1 - Incorrect syntax near keyword SELECT.
                        'StrQ &= StrInnerQ
                        StrInnerQ &= xUACQry
                        'Sohail (07 Apr 2017) -- End
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= " WHERE hrmembership_master.membershipunkid = @TINNOid "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND hremployee_master.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                Next

                StrQ &= StrInnerQ : StrInnerQ = ""

                StrQ &= " ) AS TableTINNO ON TablePenstionSSF.Empid = TableTINNO.Empid " & _
                                "LEFT JOIN ( "
                '============ TINNO (END)=======

                '============ Accomodation Element (START)=======
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid " & _
                                    ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                    ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36 , 2))) AS Amount "


                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "FROM  " & mstrCurrDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & mstrCurrDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & mstrCurrDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & mstrCurrDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid > 0 "


                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtranhead_master.tranheadunkid = @AccomodationElement " & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "


                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND hremployee_master.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= " GROUP BY "
                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= " prtnaleave_tran.payperiodunkid , prpayrollprocess_tran.employeeunkid"

                Next

                StrQ &= StrInnerQ : StrInnerQ = ""

                StrQ &= " ) AS TableHouseAllowance ON TablePenstionSSF.Periodid = TableHouseAllowance.Periodid " & _
                                "AND TablePenstionSSF.Empid = TableHouseAllowance.Empid " & _
                        "LEFT JOIN ( "
                '============ Accomodation Element (END)=======

                '============ Vehicle Element (START)=======
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT   ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid " & _
                                        ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                        ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36 , 2))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "FROM    " & mstrCurrDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrCurrDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrCurrDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "LEFT JOIN " & mstrCurrDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtranhead_master.tranheadunkid = @VehicleElement " & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND hremployee_master.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= " GROUP BY "
                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= " prtnaleave_tran.payperiodunkid , prpayrollprocess_tran.employeeunkid"
                Next

                StrQ &= StrInnerQ : StrInnerQ = ""

                StrQ &= " ) AS TableTranspAllowance ON TablePenstionSSF.Empid = TableTranspAllowance.Empid " & _
                                "LEFT JOIN ( "

                '============ Vehicle Element (END)=======

                '============enTranHeadType.EarningForEmployees(START)=========
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid " & _
                                        ",prpayrollprocess_tran.employeeunkid AS Empid " & _
                                        ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36 ," & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtranhead_master.isnoncashbenefit = 1 " & _
                                        "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "" & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                        "AND NOT (prtranhead_master.tranheadunkid = @VehicleElement OR prtranhead_master.tranheadunkid = @AccomodationElement ) "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= " GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= " prtnaleave_tran.payperiodunkid, prpayrollprocess_tran.employeeunkid "

                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= " ) AS TableNonCash ON TablePenstionSSF.Periodid = TableNonCash.Periodid AND TablePenstionSSF.Empid = TableNonCash.Empid " & _
                        " LEFT JOIN ( "
                '============ enTranHeadType.EarningForEmployees (END)=======

                '============enTranHeadType.EmployeesStatutoryDeductions(START)=========
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT   ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid " & _
                                        ", prpayrollprocess_tran.employeeunkid AS Empid  " & _
                                        ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36 ," & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & strDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & " " & _
                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= " GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= " prtnaleave_tran.payperiodunkid , prpayrollprocess_tran.employeeunkid "

                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= ") AS TableDeduction ON TablePenstionSSF.Periodid = TableDeduction.Periodid AND TablePenstionSSF.Empid = TableDeduction.Empid " & _
                        " LEFT JOIN ( "
                '============ enTranHeadType.EmployeesStatutoryDeductions (END)=======

                '============ TableTaxble (END)=======
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                    ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                    ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= " FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                    "JOIN " & mstrCurrDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                            "AND prtranhead_master.istaxable = 1 " & _
                                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= " GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= " prtnaleave_tran.payperiodunkid , prpayrollprocess_tran.employeeunkid "

                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= ") AS TableTaxble ON TablePenstionSSF.Periodid = TableTaxble.Periodid " & _
                                                              "AND TablePenstionSSF.Empid = TableTaxble.Empid " & _
                                    "LEFT JOIN ( "

                '============ TableTaxble (END)=======

                '============ TableTaxRelief (START)=======
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  ISNULL(prtnaleave_tran.payperiodunkid, 0) AS Periodid  " & _
                                    ", prpayrollprocess_tran.employeeunkid AS Empid " & _
                                    ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= " FROM " & strDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & mstrCurrDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prtranhead_master.istaxrelief = 1 " & _
                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= " GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= " prtnaleave_tran.payperiodunkid,prpayrollprocess_tran.employeeunkid "
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= ") AS TableTaxRelief ON TablePenstionSSF.Periodid = TableTaxRelief.Periodid " & _
                                                                 "AND TablePenstionSSF.Empid = TableTaxRelief.Empid " & _
                                    "LEFT JOIN ( "
                '============ TableTaxRelief (END)=======

                '============ Gross (START)==============

                StrQ &= "SELECT  Gross.Periodid  " & _
                            ", Gross.Empid " & _
                            ", SUM(Gross.Amount) AS GrossPay " & _
                        "FROM ( "
                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT prtnaleave_tran.payperiodunkid AS Periodid  " & _
                                        ", hremployee_master.employeeunkid AS Empid " & _
                                        ", CAST(ISNULL(prpayrollprocess_tran.amount , 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "
                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= " FROM      " & strDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & mstrCurrDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                        "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN " & strDatabaseName & "..practivity_master ON practivity_master.activityunkid = prpayrollprocess_tran.activityunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        'Sohail (07 Apr 2017) -- Start
                        'Issue - 65.1 - Incorrect syntax near keyword SELECT.
                        'StrQ &= xUACQry
                        StrInnerQ &= xUACQry
                        'Sohail (07 Apr 2017) -- End
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                            "AND ( prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ) "

                    If mintEmpId > 0 Then
                        StrInnerQ &= " AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [15 NOV 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END
                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= "                     ) AS Gross " & _
                                     "GROUP BY Gross.Periodid  " & _
                                           ", Gross.Empid " & _
                                   ") AS TableGross ON TablePenstionSSF.EmpId = TableGross.Empid " & _
                                                      "AND TablePenstionSSF.Periodid = TableGross.Periodid " & _
                         "LEFT JOIN ( "

                '============ Gross (END)==============

                '============ Salary Head (START)==============

                For i As Integer = 0 To marrDatabaseName.Count - 1
                    strDatabaseName = marrDatabaseName(i)

                    If i > 0 Then
                        StrInnerQ &= " UNION ALL "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    xUACFiltrQry = "" : xUACQry = ""
                    If mDicDataBaseEndDate.ContainsKey(strDatabaseName) = True AndAlso _
                       mDicDataBaseYearId.ContainsKey(strDatabaseName) = True Then
                        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(mDicDataBaseEndDate(strDatabaseName).ToString), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, mDicDataBaseYearId(strDatabaseName), strUserModeSetting)
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "SELECT  prtnaleave_tran.payperiodunkid AS Periodid  " & _
                                                  ", hremployee_master.employeeunkid AS Empid " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields
                    Else
                        StrInnerQ &= ", 0 AS Id, '' AS GName "
                    End If

                    StrInnerQ &= "                    FROM    " & strDatabaseName & "..prpayrollprocess_tran " & _
                                                    "JOIN " & mstrCurrDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN " & strDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

                    'Sohail (03 Aug 2019) -- Start
                    'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                    'StrInnerQ &= mstrAnalysis_Join
                    StrInnerQ &= mstrAnalysis_Join.Replace(eZeeDate.convertDate(mdtEmpAsOnDate), mDicDataBaseEndDate(strDatabaseName).ToString)
                    'Sohail (03 Aug 2019) -- End

                    'S.SANDEEP [15 NOV 2016] -- START
                    If xUACQry.Trim.Length > 0 Then
                        StrInnerQ &= xUACQry
                    End If
                    'S.SANDEEP [15 NOV 2016] -- END

                    StrInnerQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

                    If mintEmpId > 0 Then
                        StrInnerQ &= "                AND prpayrollprocess_tran.employeeunkid = @EmpId "
                    End If

                    If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                        StrInnerQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                    Else
                        StrInnerQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '        StrInnerQ &= UserAccessLevel._AccessLevelFilterString
                    '    End If
                    'End If

                    'S.SANDEEP [12 OCT 2016] -- START
                    'If xUACFiltrQry.Trim.Length > 0 Then
                    '    StrInnerQ &= " AND " & xUACFiltrQry
                    'End If
                    'S.SANDEEP [12 OCT 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    StrInnerQ &= "                    GROUP BY "

                    If mintViewIndex > 0 Then
                        StrInnerQ &= mstrAnalysis_Fields.Substring(2).Replace("AS Id", "").Replace("AS GName", "") & ","
                    End If

                    StrInnerQ &= "                            prtnaleave_tran.payperiodunkid  " & _
                                                      ", hremployee_master.employeeunkid "

                Next

                StrQ &= StrInnerQ
                StrInnerQ = ""

                StrQ &= ") AS BAS ON BAS.Empid = TablePenstionSSF.EmpId " & _
                                                      "AND TablePenstionSSF.Periodid = BAS.Periodid "
                '============ Salary Head (END)==============
                StrQ &= " GROUP BY TablePenstionSSF.EmpId  " & _
                                  ", TablePenstionSSF.EmpCode " & _
                                  ", TablePenstionSSF.EmpName " & _
                                  ", TablePenstionSSF.JOB_Title " & _
                                  ", ISNULL(TableTINNO.Membership_No, '') " & _
                                  ", TablePenstionSSF.MembershipUnkId " & _
                                  ", TablePenstionSSF.Membership_No " & _
                                  ", TablePenstionSSF.Membership_Name " & _
                                  ", TablePenstionSSF.Id " & _
                                  ", TablePenstionSSF.GName "
            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then  ' FOR DIFFERENT CURRENCY 

            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mdtTableExcel = dsList.Tables(0)
            Dim intColIdx As Integer = -1

            mdtTableExcel.Columns("SrNo").Caption = Language.getMessage(mstrModuleName, 10, "Ser. No")
            intColIdx += 1
            mdtTableExcel.Columns("SrNo").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("EmpName").Caption = Language.getMessage(mstrModuleName, 11, "Name Of Employee")
            intColIdx += 1
            mdtTableExcel.Columns("EmpName").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TINNO").Caption = Language.getMessage(mstrModuleName, 12, "TIN")
            intColIdx += 1
            mdtTableExcel.Columns("TINNO").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("Position").Caption = Language.getMessage(mstrModuleName, 13, "Position")
            intColIdx += 1
            mdtTableExcel.Columns("Position").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("NonResident").Caption = Language.getMessage(mstrModuleName, 14, "Tick(X) Non-Resident")
            intColIdx += 1
            mdtTableExcel.Columns("NonResident").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("BasicSal").Caption = Language.getMessage(mstrModuleName, 15, "Basic salary")
            intColIdx += 1
            mdtTableExcel.Columns("BasicSal").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("SecondaryEmployment").Caption = Language.getMessage(mstrModuleName, 16, "Tick(X)  Secondary Employment")
            intColIdx += 1
            mdtTableExcel.Columns("SecondaryEmployment").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("SSF").Caption = Language.getMessage(mstrModuleName, 17, "Social Security Fund")
            intColIdx += 1
            mdtTableExcel.Columns("SSF").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("ThirdTier").Caption = Language.getMessage(mstrModuleName, 18, "Third Tier")
            intColIdx += 1
            mdtTableExcel.Columns("ThirdTier").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("CashAllowance").Caption = Language.getMessage(mstrModuleName, 19, "Cash Allowances")
            intColIdx += 1
            mdtTableExcel.Columns("CashAllowance").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("Income").Caption = Language.getMessage(mstrModuleName, 20, "Bonus Income(up to 15% of Basic salary)")
            intColIdx += 1
            mdtTableExcel.Columns("Income").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("FTaxBonus").Caption = Language.getMessage(mstrModuleName, 21, "Final Tax on Bonus")
            intColIdx += 1
            mdtTableExcel.Columns("FTaxBonus").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("ExcessBonus").Caption = Language.getMessage(mstrModuleName, 22, "EXCESS BONUS")
            intColIdx += 1
            mdtTableExcel.Columns("ExcessBonus").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TotalCashEmolument").Caption = Language.getMessage(mstrModuleName, 23, "Total Cash emolument")
            intColIdx += 1
            mdtTableExcel.Columns("TotalCashEmolument").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("AccomodationElement").Caption = Language.getMessage(mstrModuleName, 24, "Accommodation Element")
            intColIdx += 1
            mdtTableExcel.Columns("AccomodationElement").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("VehicleElement").Caption = Language.getMessage(mstrModuleName, 25, "Vehicle element")
            intColIdx += 1
            mdtTableExcel.Columns("VehicleElement").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("NonCashBenefit").Caption = Language.getMessage(mstrModuleName, 26, "Non Cash Benefit")
            intColIdx += 1
            mdtTableExcel.Columns("NonCashBenefit").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TOTAL_ASSESSABLE_INCOME").Caption = Language.getMessage(mstrModuleName, 27, "Total Assessable Income")
            intColIdx += 1
            mdtTableExcel.Columns("TOTAL_ASSESSABLE_INCOME").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("DeductibleRelief").Caption = Language.getMessage(mstrModuleName, 28, "Deductible Reliefs")
            intColIdx += 1
            mdtTableExcel.Columns("DeductibleRelief").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TotalRelief").Caption = Language.getMessage(mstrModuleName, 29, "Total Reliefs")
            intColIdx += 1
            mdtTableExcel.Columns("TotalRelief").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("ChargeableIncome").Caption = Language.getMessage(mstrModuleName, 30, "Chargeable Income")
            intColIdx += 1
            mdtTableExcel.Columns("ChargeableIncome").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TaxDeductible").Caption = Language.getMessage(mstrModuleName, 31, "Tax Deductible")
            intColIdx += 1
            mdtTableExcel.Columns("TaxDeductible").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("OTIncome").Caption = Language.getMessage(mstrModuleName, 32, "Overtime Income")
            intColIdx += 1
            mdtTableExcel.Columns("OTIncome").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("OTTax").Caption = Language.getMessage(mstrModuleName, 33, "Overtime Tax")
            intColIdx += 1
            mdtTableExcel.Columns("OTTax").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("TaxGRA").Caption = Language.getMessage(mstrModuleName, 34, "Total Tax Payable to GRA")
            intColIdx += 1
            mdtTableExcel.Columns("TaxGRA").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("SeverancePaid").Caption = Language.getMessage(mstrModuleName, 35, "Severance pay paid")
            intColIdx += 1
            mdtTableExcel.Columns("SeverancePaid").SetOrdinal(intColIdx)

            mdtTableExcel.Columns("Remarks").Caption = Language.getMessage(mstrModuleName, 36, "Remarks")
            intColIdx += 1
            mdtTableExcel.Columns("Remarks").SetOrdinal(intColIdx)



            Dim strarrGroupColumns As String() = Nothing
            Dim strSubTotal As String = ""
            Dim strGTotal As String = Language.getMessage(mstrModuleName, 37, "Total :")
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                intColIdx += 1
                mdtTableExcel.Columns("GName").SetOrdinal(intColIdx)

                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
                strSubTotal = Language.getMessage(mstrModuleName, 38, "Sub Total :")
            End If

            For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
            Next

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 39, "EMPLOYER'S MONTHLY TAX DEDUCTIONS SCHEDULE (P. A. Y. E.)"), "s9wc")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "NAME OF EMPLOYER :"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Company._Object._Name, "s8bw")
            wcell.MergeAcross = 5
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 41, "EMPLOYER'S NEW TIN  :"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Company._Object._Tinno, "s8bw")
            wcell.MergeAcross = 5
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 42, "FOR THE MONTH OF  :"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(mstrPeriodNames, "s8bw")
            wcell.MergeAcross = 2
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 43, "EMPLOYER'S OLD TIN :"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            wcell.MergeAcross = 5
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)
            '--------------------

            row = New WorksheetRow()
            For i As Integer = 1 To mdtTableExcel.Columns.Count
                wcell = New WorksheetCell(i.ToString, "HeaderStyle")
                row.Cells.Add(wcell)
            Next
            If mintViewIndex > 0 Then
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            '--------------------

            row = New WorksheetRow()
            For i As Integer = 1 To mdtTableExcel.Columns.Count
                If i = 9 Then Continue For
                If i = 8 Then
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 44, "PENSIONS"), "HeaderStyle")
                    wcell.MergeAcross = 1
                    row.Cells.Add(wcell) 'PENSIONS
                Else
                    wcell = New WorksheetCell("", "HeaderStyle")
                    row.Cells.Add(wcell)
                End If
            Next

            If mintViewIndex > 0 Then
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 45, "DECLARATION:"), "s9wc")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 46, "I certify that I have deducted the correct amount of tax from the total remuneration of each member of staff employed by me/us for the YEAR of  :"), "s8bw")
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 47, "NAME OF DECLARANT:"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            wcell.MergeAcross = 3
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 48, "DESIGNATION:"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            wcell.MergeAcross = 3
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------
            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 49, "SIGNATURE:"), "s8bw")
            wcell.MergeAcross = 1
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            wcell.MergeAcross = 3
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 50, "DATE:"), "s8bw")
            row.Cells.Add(wcell)
            wcell = New WorksheetCell("", "s8bw")
            wcell.MergeAcross = 2
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            '--------------------

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            intArrayColumnWidth(0) = 125
            intArrayColumnWidth(1) = 200
            For i As Integer = 2 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Language.getMessage(mstrModuleName, 51, "DOMESTIC TAX REVENUE DIVISION"), , " ", Nothing, strGTotal, False, rowsArrayHeader, rowsArrayFooter, Nothing)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Penstion SSF :")
            Language.setMessage(mstrModuleName, 2, "Penstion Third Tire :")
            Language.setMessage(mstrModuleName, 3, "Accomodation Element :")
            Language.setMessage(mstrModuleName, 4, "Vehicle Element :")
            Language.setMessage(mstrModuleName, 5, "TIN No. :")
            Language.setMessage(mstrModuleName, 6, "Employee :")
            Language.setMessage(mstrModuleName, 7, "Order By :")
            Language.setMessage(mstrModuleName, 8, "Employee Code")
            Language.setMessage(mstrModuleName, 9, "Employee Name")
            Language.setMessage(mstrModuleName, 10, "Ser. No")
            Language.setMessage(mstrModuleName, 11, "Name Of Employee")
            Language.setMessage(mstrModuleName, 12, "TIN")
            Language.setMessage(mstrModuleName, 13, "Position")
            Language.setMessage(mstrModuleName, 14, "Tick(X) Non-Resident")
            Language.setMessage(mstrModuleName, 15, "Basic salary")
            Language.setMessage(mstrModuleName, 16, "Tick(X)  Secondary Employment")
            Language.setMessage(mstrModuleName, 17, "Social Security Fund")
            Language.setMessage(mstrModuleName, 18, "Third Tier")
            Language.setMessage(mstrModuleName, 19, "Cash Allowances")
            Language.setMessage(mstrModuleName, 20, "Bonus Income(up to 15% of Basic salary)")
            Language.setMessage(mstrModuleName, 21, "Final Tax on Bonus")
            Language.setMessage(mstrModuleName, 22, "EXCESS BONUS")
            Language.setMessage(mstrModuleName, 23, "Total Cash emolument")
            Language.setMessage(mstrModuleName, 24, "Accommodation Element")
            Language.setMessage(mstrModuleName, 25, "Vehicle element")
            Language.setMessage(mstrModuleName, 26, "Non Cash Benefit")
            Language.setMessage(mstrModuleName, 27, "Total Assessable Income")
            Language.setMessage(mstrModuleName, 28, "Deductible Reliefs")
            Language.setMessage(mstrModuleName, 29, "Total Reliefs")
            Language.setMessage(mstrModuleName, 30, "Chargeable Income")
            Language.setMessage(mstrModuleName, 31, "Tax Deductible")
            Language.setMessage(mstrModuleName, 32, "Overtime Income")
            Language.setMessage(mstrModuleName, 33, "Overtime Tax")
            Language.setMessage(mstrModuleName, 34, "Total Tax Payable to GRA")
            Language.setMessage(mstrModuleName, 35, "Severance pay paid")
            Language.setMessage(mstrModuleName, 36, "Remarks")
            Language.setMessage(mstrModuleName, 37, "Total :")
            Language.setMessage(mstrModuleName, 38, "Sub Total :")
            Language.setMessage(mstrModuleName, 39, "EMPLOYER'S MONTHLY TAX DEDUCTIONS SCHEDULE (P. A. Y. E.)")
            Language.setMessage(mstrModuleName, 40, "NAME OF EMPLOYER :")
            Language.setMessage(mstrModuleName, 41, "EMPLOYER'S NEW TIN  :")
            Language.setMessage(mstrModuleName, 42, "FOR THE MONTH OF  :")
            Language.setMessage(mstrModuleName, 43, "EMPLOYER'S OLD TIN :")
            Language.setMessage(mstrModuleName, 44, "PENSIONS")
            Language.setMessage(mstrModuleName, 45, "DECLARATION:")
            Language.setMessage(mstrModuleName, 46, "I certify that I have deducted the correct amount of tax from the total remuneration of each member of staff employed by me/us for the YEAR of  :")
            Language.setMessage(mstrModuleName, 47, "NAME OF DECLARANT:")
            Language.setMessage(mstrModuleName, 48, "DESIGNATION:")
            Language.setMessage(mstrModuleName, 49, "SIGNATURE:")
            Language.setMessage(mstrModuleName, 50, "DATE:")
            Language.setMessage(mstrModuleName, 51, "DOMESTIC TAX REVENUE DIVISION")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
