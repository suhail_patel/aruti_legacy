#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmMonthlyTaxDeductionSchedule
#Region "Variable"
    Private ReadOnly mstrModuleName As String = "frmMonthlyTaxDeductionSchedule"
    Private objTaxDeducationSchedule As clsMonthlyTaxDeductionSchedule
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mDicDataBaseYearId As Dictionary(Of String, Integer)
    Private mDicDataBaseEndDate As Dictionary(Of String, String)
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        PenstionSSF = 1
        PenstionThirdTire = 2
        AccomodationElement = 3
        VehicleElement = 4
        TINNo = 5
        Employee = 6
        Other_Earning = 7
    End Enum
#End Region

#Region " Constructor "

    Public Sub New()
        objTaxDeducationSchedule = New clsMonthlyTaxDeductionSchedule(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTaxDeducationSchedule.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmMonthlyTaxDeductionSchedule_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTaxDeducationSchedule = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyTaxDeductionSchedule_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMonthlyTaxDeductionSchedule_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.E Then
                Call btnExport.PerformClick()
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmMonthlyTaxDeductionSchedule_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMonthlyTaxDeductionSchedule_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()


            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyTaxDeductionSchedule_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objMember As New clsmembership_master
        Dim objTranHead As New clsTransactionHead
        Dim dsCombo As DataSet

        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsCombo = objMember.getListForCombo("Membership", True, , 1)
            With cboPensionsSSF
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            With cboPensionsThirdTier
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            dsCombo = objMember.getListForCombo("Membership", True, , 2)
            With cboTINNo
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Membership")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & "")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & "")
            'Sohail (21 Aug 2015) -- End
            With cboAccomodationElement
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                If dsCombo.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With

            With cboVehicleElement
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                If dsCombo.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("OtherEarning")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            objMember = Nothing
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboToPeriod.SelectedValue = mintFirstOpenPeriod
            cboEmployee.SelectedValue = 0
            cboPensionsSSF.SelectedValue = 0
            cboPensionsThirdTier.SelectedValue = 0
            cboAccomodationElement.SelectedValue = 0
            cboVehicleElement.SelectedValue = 0

            cboTINNo.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.AccomodationElement
                            cboAccomodationElement.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.VehicleElement
                            cboVehicleElement.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.PenstionSSF
                            cboPensionsSSF.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TINNo
                            cboTINNo.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.PenstionThirdTire
                            cboPensionsThirdTier.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee
                            cboEmployee.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Other_Earning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            Else
                                gbBasicSalaryOtherEarning.Checked = False
                            End If
                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objPeriod As New clscommom_period_Tran
        Dim strPeriodIDs As String = ""
        Dim strPeriodNAMEs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        Try
            objTaxDeducationSchedule.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To Period is mandatory information. Please select Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            ElseIf CInt(cboPensionsSSF.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Penstion SSF"), enMsgBoxStyle.Information)
                cboPensionsSSF.Focus()
                Return False
            End If

            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If

            Dim strDBIds As String = objPeriod.GetDISTINCTDatabaseNAMEs(enModuleReference.Payroll, eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date")), eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date")))
            arrDatabaseName.AddRange(strDBIds.Split(","))
            For Each dsRow As DataRow In CType(cboPeriod.DataSource, DataTable).Rows
                If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    strPeriodNAMEs &= ", " & dsRow.Item("name").ToString
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If strPeriodNAMEs.Trim <> "" Then strPeriodNAMEs = strPeriodNAMEs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Return False


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            mDicDataBaseYearId = New Dictionary(Of String, Integer)
            mDicDataBaseEndDate = New Dictionary(Of String, String)

            Dim objCompany As New clsCompany_Master
            Dim dsDB As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", 0)

            mDicDataBaseYearId = dsDB.Tables(0).AsEnumerable().ToDictionary(Of String, Integer)(Function(x) x.Field(Of String)("database_name"), Function(y) y.Field(Of Integer)("yearunkid"))
            mDicDataBaseEndDate = dsDB.Tables(0).AsEnumerable().ToDictionary(Of String, String)(Function(x) x.Field(Of String)("database_name"), Function(x) x.Field(Of String)("end_date"))

            objTaxDeducationSchedule._DicDataBaseEndDate = mDicDataBaseEndDate
            objTaxDeducationSchedule._DicDataBaseYearId = mDicDataBaseYearId

            objCompany = Nothing
            'S.SANDEEP [04 JUN 2015] -- END


            objTaxDeducationSchedule._PeriodIds = strPeriodIDs
            objTaxDeducationSchedule._PeriodNames = strPeriodNAMEs
            objTaxDeducationSchedule._Arr_DatabaseName = arrDatabaseName
            objTaxDeducationSchedule._CurrDatabaseName = FinancialYear._Object._DatabaseName

            If CInt(cboPensionsSSF.SelectedValue) > 0 Then
                objTaxDeducationSchedule._PenstionSSFID = cboPensionsSSF.SelectedValue
                objTaxDeducationSchedule._PenstionSSFName = cboPensionsSSF.Text
            End If

            If CInt(cboPensionsThirdTier.SelectedValue) > 0 Then
                objTaxDeducationSchedule._PenstionThirdTireID = cboPensionsThirdTier.SelectedValue
                objTaxDeducationSchedule._PenstionThirdTireName = cboPensionsThirdTier.Text
            End If

            If CInt(cboAccomodationElement.SelectedValue) > 0 Then
                objTaxDeducationSchedule._AccomodationElementID = cboAccomodationElement.SelectedValue
                objTaxDeducationSchedule._AccomodationElementName = cboAccomodationElement.Text
            End If

            If CInt(cboVehicleElement.SelectedValue) > 0 Then
                objTaxDeducationSchedule._VehicleElementId = CInt(cboVehicleElement.SelectedValue)
                objTaxDeducationSchedule._VehicleElementName = cboVehicleElement.Text
            End If

            If CInt(cboTINNo.SelectedValue) > 0 Then
                objTaxDeducationSchedule._TINNoMembershipId = cboTINNo.SelectedValue
                objTaxDeducationSchedule._TINNoMembershipName = cboTINNo.Text
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objTaxDeducationSchedule._EmpId = cboEmployee.SelectedValue
                objTaxDeducationSchedule._EmpName = cboEmployee.Text
            End If




            If gbBasicSalaryOtherEarning.Checked = True Then
                objTaxDeducationSchedule._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objTaxDeducationSchedule._OtherEarningTranId = 0
            End If

            objTaxDeducationSchedule._ViewByIds = mstrStringIds
            objTaxDeducationSchedule._ViewIndex = mintViewIdx
            objTaxDeducationSchedule._ViewByName = mstrStringName
            objTaxDeducationSchedule._Analysis_Fields = mstrAnalysis_Fields
            objTaxDeducationSchedule._Analysis_Join = mstrAnalysis_Join
            objTaxDeducationSchedule._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTaxDeducationSchedule._Report_GroupName = mstrReport_GroupName

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objTaxDeducationSchedule._EmpAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            'Sohail (03 Aug 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTaxDeducationSchedule.Generate_DetailReport()
            objTaxDeducationSchedule.Generate_DetailReport(User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                           True, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 7
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.PenstionSSF
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPensionsSSF.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)

                    Case enHeadTypeId.PenstionThirdTire
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPensionsThirdTier.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)

                    Case enHeadTypeId.AccomodationElement
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAccomodationElement.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)

                    Case enHeadTypeId.VehicleElement
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboVehicleElement.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)

                    Case enHeadTypeId.TINNo
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTINNo.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)


                    Case enHeadTypeId.Employee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployee.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)

                    Case enHeadTypeId.Other_Earning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherEarning.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMonthlyTaxDeductionSchedule.SetMessages()
            objfrm._Other_ModuleNames = "clsMonthlyTaxDeductionSchedule"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString), eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString))
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString), _
                                            eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub cboPensionsSSF_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboPensionsSSF.Validating, _
                                                                                                                           cboPensionsThirdTier.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If CInt(cbo.SelectedValue) > 0 Then
                If cbo.Name = "cboPensionsSSF" Then
                    If cbo.SelectedValue = cboPensionsThirdTier.SelectedValue Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, This Membership is already selected."))
                        cbo.SelectedValue = 0
                        e.Cancel = True
                    End If
                Else
                    If cbo.SelectedValue = cboPensionsSSF.SelectedValue Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, This Membership is already selected."))
                        cbo.SelectedValue = 0
                        e.Cancel = True
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPensionsSSF_Validating", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchAccomodationElement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAccomodationElement.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboAccomodationElement.DataSource
            frm.ValueMember = cboAccomodationElement.ValueMember
            frm.DisplayMember = cboAccomodationElement.DisplayMember
            'frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboAccomodationElement.SelectedValue = frm.SelectedValue
                cboAccomodationElement.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAccomodationElement_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchVehicleElement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVehicleElement.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboVehicleElement.DataSource
            frm.ValueMember = cboVehicleElement.ValueMember
            frm.DisplayMember = cboVehicleElement.DisplayMember
            'frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboVehicleElement.SelectedValue = frm.SelectedValue
                cboVehicleElement.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVehicleElement_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblVehicleElement.Text = Language._Object.getCaption(Me.lblVehicleElement.Name, Me.lblVehicleElement.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnOtherInformation.Text = Language._Object.getCaption(Me.lnOtherInformation.Name, Me.lnOtherInformation.Text)
			Me.lblTINNo.Text = Language._Object.getCaption(Me.lblTINNo.Name, Me.lblTINNo.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.lblAccomodationElement.Text = Language._Object.getCaption(Me.lblAccomodationElement.Name, Me.lblAccomodationElement.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblPensionsSSF.Text = Language._Object.getCaption(Me.lblPensionsSSF.Name, Me.lblPensionsSSF.Text)
			Me.lblPensionsThirdTier.Text = Language._Object.getCaption(Me.lblPensionsThirdTier.Name, Me.lblPensionsThirdTier.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period.")
			Language.setMessage(mstrModuleName, 2, "To Period is mandatory information. Please select Period.")
			Language.setMessage(mstrModuleName, 3, " To Period cannot be less than From Period")
			Language.setMessage(mstrModuleName, 4, "Please select Penstion SSF")
			Language.setMessage(mstrModuleName, 5, "Please select transaction head.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Sorry, This Membership is already selected.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

  
End Class
