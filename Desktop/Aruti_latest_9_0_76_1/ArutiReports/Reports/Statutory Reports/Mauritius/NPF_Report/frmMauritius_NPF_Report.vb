'************************************************************************************************************************************
'Class Name : frmMauritius_NPF_Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMauritius_NPF_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMauritius_PAYEReport"
    Private objNPF As clsMauritius_NPF_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing

#End Region

#Region " Contructor "

    Public Sub New()
        objNPF = New clsMauritius_NPF_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objNPF.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMembership As New clsmembership_master
        Dim dsCombos As New DataSet
        Try
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With
            'S.SANDEEP [23-OCT-2017] -- START
            'dsCombos = objMembership.getListForCombo("Membership", True, , 1)
            dsCombos = objMembership.getListForCombo("Membership", True, , 0)
            'S.SANDEEP [23-OCT-2017] -- END
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [23-OCT-2017] -- START
            dsCombos = objMembership.getListForCombo("contribution", True, , 1)
            With cboEmpContribution
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [23-OCT-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objperiod = Nothing : objTranHead = Nothing : objMembership = Nothing : dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            cboPeriod.SelectedValue = 0
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            cboMembership.SelectedValue = 0
            dsList = objUserDefRMode.GetList("List", enArutiReport.Mauritius_NPF_Report)
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))
                        Case 1  'MEMBERSHIP
                            If IsDBNull(dsRow.Item("transactionheadid")) = False Then
                                cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            End If
                            'S.SANDEEP [23-OCT-2017] -- START
                        Case 2  'CONTRIBUTION
                            If IsDBNull(dsRow.Item("transactionheadid")) = False Then
                                cboEmpContribution.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            End If
                            'S.SANDEEP [23-OCT-2017] -- END
                    End Select
                Next
            End If
            cboOtherEarning.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objNPF.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select period to continue, Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select membership to continue, Membership is mandatory information."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If
            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If
            'S.SANDEEP [23-OCT-2017] -- START
            If CInt(cboEmpContribution.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select employee contribution to continue, Employee contribution is mandatory information."), enMsgBoxStyle.Information)
                cboEmpContribution.Focus()
                Return False
            End If
            'S.SANDEEP [23-OCT-2017] -- END

            objNPF._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            objNPF._OtherEarningName = cboOtherEarning.Text
            objNPF._MembershipId = CInt(cboMembership.SelectedValue)
            objNPF._MembershipName = cboMembership.Text
            objNPF._PeriodId = CInt(cboPeriod.SelectedValue)
            objNPF._PeriodName = cboPeriod.Text
            objNPF._ViewByIds = mstrStringIds
            objNPF._ViewIndex = mintViewIdx
            objNPF._ViewByName = mstrStringName
            objNPF._Analysis_Fields = mstrAnalysis_Fields
            objNPF._Analysis_Join = mstrAnalysis_Join
            objNPF._Analysis_OrderBy = mstrAnalysis_OrderBy
            objNPF._Report_GroupName = mstrReport_GroupName
            'S.SANDEEP [23-OCT-2017] -- START
            objNPF._EmpContributionId = CInt(cboEmpContribution.SelectedValue)
            objNPF._EmpContributionName = cboEmpContribution.Text
            'S.SANDEEP [23-OCT-2017] -- END

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objNPF._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmMauritius_NPF_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objNPF = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMauritius_NPF_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMauritius_NPF_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.E Then
                Call btnExport.PerformClick()
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmMauritius_NPF_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMauritius_NPF_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.eZeeHeader.Title = objNPF._ReportName

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMauritius_NPF_Report_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Dim svDialog As New FolderBrowserDialog
            If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim stringFileName As String = "NPF_" & cboPeriod.Text & "_" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Format(Now, "hhmmss") & ".csv"
                Dim stringFilePath As String = svDialog.SelectedPath & "\" & stringFileName
                If objNPF.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._Base_CurrencyId, stringFilePath, GUI.fmtCurrency) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Problen in exporting report."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Report Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMauritius_NPF_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsMauritius_NPF_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPrd._Start_Date
                mdtPeriodEndDate = objPrd._End_Date
                objPrd = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSaveSelection_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveSelection.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Try
            '============ Save Membership ============
            objUserDefRMode._Reportunkid = enArutiReport.Mauritius_NPF_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1 'MEMBERSHIP
            objUserDefRMode._EarningTranHeadIds = CInt(cboMembership.SelectedValue).ToString()
            intUnkid = objUserDefRMode.isExist(enArutiReport.Mauritius_NPF_Report, 0, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If

            'S.SANDEEP [23-OCT-2017] -- START
            '============ Save Contribution ============
            objUserDefRMode._Reportunkid = enArutiReport.Mauritius_NPF_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 2 'Contribution
            objUserDefRMode._EarningTranHeadIds = CInt(cboEmpContribution.SelectedValue).ToString()
            intUnkid = objUserDefRMode.isExist(enArutiReport.Mauritius_NPF_Report, 0, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            'S.SANDEEP [23-OCT-2017] -- END

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Information saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSaveSelection_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
            Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select period to continue, Period is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please select membership to continue, Membership is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Please select transaction head.")
            Language.setMessage(mstrModuleName, 4, "Problen in exporting report.")
            Language.setMessage(mstrModuleName, 5, "Report Exported Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
