'************************************************************************************************************************************
'Class Name : clsP11Report.vb
'Purpose    :
'Date       :21/01/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System
Imports System.Text.RegularExpressions
Imports System.Text

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsP9AReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsP9AReport"
    Private mstrReportId As String = enArutiReport.P9A_Report

    Dim objDataOperation As clsDataOperation

    'Nilay (21-Oct-2016) -- Start
    'Enhancement : Kenya P9A statutory report on ESS for CCK
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    'Nilay (21-Oct-2016) -- End

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintQuartersValueHeadId As Integer = 0
    Private mstrQuartersValueHeadName As String = ""
    Private mintE1HeadId As Integer = 0
    Private mstrE1HeadName As String = ""
    Private mintE2HeadId As Integer = 0
    Private mstrE2HeadName As String = ""
    Private mintE3HeadId As Integer = 0
    Private mstrE3HeadName As String = ""
    Private mintAmountOfInterestHeadId As Integer = 0
    Private mstrAmountOfInterestHeadName As String = ""
    Private mintTaxChargedHeadId As Integer = 0
    Private mstrTaxChargedHeadName As String = ""
    Private mintPersonalReliefHeadId As Integer = 0
    Private mstrPersonalReliefHeadName As String = ""
    'Sohail (18 Jan 2017) -- Start
    'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
    Private mintInsuranceReliefHeadId As Integer = 0
    Private mstrInsuranceReliefHeadName As String = ""
    Private mdicYearDBName As New Dictionary(Of Integer, String)
    'Sohail (18 Jan 2017) -- End
    'Sohail (08 May 2020) -- Start
    'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
    Private mintTaxbleIncomeHeadId As Integer = 0
    Private mstrTaxbleIncomeHeadName As String = ""
    'Sohail (08 May 2020) -- End

    Private mblnIncludeInactiveEmp As Boolean = True

    'Sohail (19 Nov 2015) - Start
    Private mstrFirstPeriodYearName As String = ""
    'Sohail (19 Nov 2015) - End

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

    'Anjan [03 December 2015] -- Start
    'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request

    Private mintMembershipId As Integer = 0
    'Anjan [03 December 2015] -- End

    'Nilay (21-Oct-2016) -- Start
    'Enhancement : Kenya P9A statutory report on ESS for CCK
    Private mblnApplyUserAccessFilter As Boolean = True
    'Nilay (21-Oct-2016) -- End 

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadId() As Integer
        Set(ByVal value As Integer)
            mintQuartersValueHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrQuartersValueHeadName = value
        End Set
    End Property

    Public WriteOnly Property _E1HeadId() As Integer
        Set(ByVal value As Integer)
            mintE1HeadId = value
        End Set
    End Property

    Public WriteOnly Property _E1HeadName() As String
        Set(ByVal value As String)
            mstrE1HeadName = value
        End Set
    End Property

    Public WriteOnly Property _E2HeadId() As Integer
        Set(ByVal value As Integer)
            mintE2HeadId = value
        End Set
    End Property

    Public WriteOnly Property _E2HeadName() As String
        Set(ByVal value As String)
            mstrE2HeadName = value
        End Set
    End Property

    Public WriteOnly Property _E3HeadId() As Integer
        Set(ByVal value As Integer)
            mintE3HeadId = value
        End Set
    End Property

    Public WriteOnly Property _E3HeadName() As String
        Set(ByVal value As String)
            mstrE3HeadName = value
        End Set
    End Property

    Public WriteOnly Property _AmountOfInterestHeadId() As Integer
        Set(ByVal value As Integer)
            mintAmountOfInterestHeadId = value
        End Set
    End Property

    Public WriteOnly Property _AmountOfInterestHeadName() As String
        Set(ByVal value As String)
            mstrAmountOfInterestHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TaxChargedHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxChargedHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TaxChargedHeadName() As String
        Set(ByVal value As String)
            mstrTaxChargedHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PersonalReliefHeadId() As Integer
        Set(ByVal value As Integer)
            mintPersonalReliefHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PersonalReliefHeadName() As String
        Set(ByVal value As String)
            mstrPersonalReliefHeadName = value
        End Set
    End Property

    'Sohail (18 Jan 2017) -- Start
    'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
    Public WriteOnly Property _InsuranceReliefHeadId() As Integer
        Set(ByVal value As Integer)
            mintInsuranceReliefHeadId = value
        End Set
    End Property

    Public WriteOnly Property _InsuranceReliefHeadName() As String
        Set(ByVal value As String)
            mstrInsuranceReliefHeadName = value
        End Set
    End Property

    Public WriteOnly Property _dic_YearDBName() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicYearDBName = value
        End Set
    End Property
    'Sohail (18 Jan 2017) -- End

    'Sohail (08 May 2020) -- Start
    'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
    Public WriteOnly Property _TaxbleIncomeHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxbleIncomeHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TaxbleIncomeHeadName() As String
        Set(ByVal value As String)
            mstrTaxbleIncomeHeadName = value
        End Set
    End Property
    'Sohail (08 May 2020) -- End

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Sohail (19 Nov 2015) - Start
    Public WriteOnly Property _FirstPeriodYearName() As String
        Set(ByVal value As String)
            mstrFirstPeriodYearName = value
        End Set
    End Property
    'Sohail (19 Nov 2015) - End

    'Anjan [03 December 2015] -- Start
    'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property
    'Anjan [03 December 2015] -- End

    'Nilay (21-Oct-2016) -- Start
    'Enhancement : Kenya P9A statutory report on ESS for CCK
    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property
    'Nilay (21-Oct-2016) -- End

    'Sohail (20 Mar 2021) -- Start
    'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
    Private mstrBasicSalaryFormula As String = String.Empty
    Public WriteOnly Property _BasicSalaryFormula() As String
        Set(ByVal value As String)
            mstrBasicSalaryFormula = value
        End Set
    End Property

    Private mstrTaxChargedFormula As String = String.Empty
    Public WriteOnly Property _TaxChargedFormula() As String
        Set(ByVal value As String)
            mstrTaxChargedFormula = value
        End Set
    End Property
    'Sohail (20 Mar 2021) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mstrPeriodIds = ""
            mintQuartersValueHeadId = 0
            mstrQuartersValueHeadName = ""
            mintE1HeadId = 0
            mstrE1HeadName = ""
            mintE2HeadId = 0
            mstrE2HeadName = ""
            mintE3HeadId = 0
            mstrE3HeadName = ""
            mintAmountOfInterestHeadId = 0
            mstrAmountOfInterestHeadName = ""
            mintTaxChargedHeadId = 0
            mstrTaxChargedHeadName = ""
            mintPersonalReliefHeadId = 0
            mstrPersonalReliefHeadName = ""
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            mintInsuranceReliefHeadId = 0
            mstrInsuranceReliefHeadName = ""
            mdicYearDBName.Clear()
            'Sohail (18 Jan 2017) -- End
            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            mintTaxbleIncomeHeadId = 0
            mstrTaxbleIncomeHeadName = ""
            'Sohail (08 May 2020) -- End

            mblnIncludeInactiveEmp = True

            'Sohail (19 Nov 2015) - Start
            mstrFirstPeriodYearName = ""
            'Sohail (19 Nov 2015) - End

            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            mstrBasicSalaryFormula = ""
            mstrTaxChargedFormula = ""
            'Sohail (20 Mar 2021) -- End

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@quartersheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuartersValueHeadId)
            objDataOperation.AddParameter("@e1headid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintE1HeadId)
            objDataOperation.AddParameter("@e2headid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintE2HeadId)
            objDataOperation.AddParameter("@e3headid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintE3HeadId)
            objDataOperation.AddParameter("@amountofinterestheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAmountOfInterestHeadId)
            objDataOperation.AddParameter("@taxchargedheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxChargedHeadId)
            objDataOperation.AddParameter("@personalreliefheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPersonalReliefHeadId)
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            objDataOperation.AddParameter("@insurancereliefheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInsuranceReliefHeadId)
            'Sohail (18 Jan 2017) -- End
            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            objDataOperation.AddParameter("@taxableincomeheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxbleIncomeHeadId)
            'Sohail (08 May 2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        'Sohail (18 Jan 2017) -- Start
        'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        'Sohail (18 Jan 2017) -- End
        Try
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            objCompany._Companyunkid = xCompanyUnkid
            objConfig._Companyunkid = xCompanyUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, objCompany._Name, objCompany._Tinno, objConfig._CurrencyFormat)
            'Sohail (18 Jan 2017) -- End

            'Nilay (21-Oct-2016) -- Start
            'Enhancement : Kenya P9A statutory report on ESS for CCK
            Rpt = objRpt
            'Nilay (21-Oct-2016) -- End

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Sohail (18 Jan 2017) -- Start
    'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
    'Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
    '                                       ByVal intUserUnkid As Integer, _
    '                                       ByVal intYearUnkid As Integer, _
    '                                       ByVal intCompanyUnkid As Integer, _
    '                                       ByVal dtPeriodEnd As Date, _
    '                                       ByVal strUserModeSetting As String, _
    '                                       ByVal blnOnlyApproved As Boolean, _
    '                                       ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    'S.SANDEEP [04 JUN 2015] -- END
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim strBaseCurrencySign As String
    '    Dim decTotalPaye As Decimal = 0

    '    Try
    '        objDataOperation = New clsDataOperation

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
    '        'S.SANDEEP [04 JUN 2015] -- END
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        strBaseCurrencySign = objExchangeRate._Currency_Sign

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
    '        'Nilay (21-Oct-2016) -- Start
    '        'Enhancement : Kenya P9A statutory report on ESS for CCK
    '        'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
    '        If mblnApplyUserAccessFilter = True Then
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
    '        End If
    '        'Nilay (21-Oct-2016) -- End

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ = "SELECT  A.periodunkid " & _
    '                      ", cfcommon_period_tran.period_name " & _
    '                      ", A.employeeunkid " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
    '                      ", BasicSalary " & _
    '                      ", NonCashBenefit " & _
    '                      ", Quarters " & _
    '                      ", BasicSalary + NonCashBenefit + Quarters AS TotalGrossPay " & _
    '                      ", E1Head " & _
    '                      ", E2Head " & _
    '                      ", E3Head " & _
    '                      ", AmountOfInterestHead " & _
    '                      ", TaxChargedHead " & _
    '                      ", PersonalReliefHead " & _
    '                      ", ISNULL(C.membershipno, '') AS membershipno " & _
    '                "FROM    ( SELECT    ISNULL(Period.payperiodunkid, 0) AS periodunkid " & _
    '                                  ", ISNULL(Period.employeeunkid, 0) AS employeeunkid " & _
    '                                  ", SUM(ISNULL(Period.BasicSalary, 0)) AS BasicSalary " & _
    '                                  ", SUM(ISNULL(Period.NonCashBenefit, 0)) AS NonCashBenefit " & _
    '                                  ", SUM(ISNULL(Period.Quarters, 0)) AS Quarters " & _
    '                                  ", SUM(ISNULL(Period.E1Head, 0)) AS E1Head " & _
    '                                  ", SUM(ISNULL(Period.E2Head, 0)) AS E2Head " & _
    '                                  ", SUM(ISNULL(Period.E3Head, 0)) AS E3Head " & _
    '                                  ", SUM(ISNULL(Period.AmountOfInterestHead, 0)) AS AmountOfInterestHead " & _
    '                                  ", SUM(ISNULL(Period.TaxChargedHead, 0)) AS TaxChargedHead " & _
    '                                  ", SUM(ISNULL(Period.PersonalReliefHead, 0)) AS PersonalReliefHead " & _
    '                          "FROM      ( SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "
    '        'Anjan [03 December 2015] -- [Membership No in Employee's PIN textbox]

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.istaxable = 1 "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.isnoncashbenefit = 1 "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @quartersheadid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @e1headid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @e2headid "


    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @e3headid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @amountofinterestheadid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxChargedHead " & _
    '                                              ", 0 AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @taxchargedheadid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    payperiodunkid " & _
    '                                              ", prpayrollprocess_tran.employeeunkid " & _
    '                                              ", 0 AS BasicSalary " & _
    '                                              ", 0 AS NonCashBenefit " & _
    '                                              ", 0 AS Quarters " & _
    '                                              ", 0 AS E1Head " & _
    '                                              ", 0 AS E2Head " & _
    '                                              ", 0 AS E3Head " & _
    '                                              ", 0 AS AmountOfInterestHead " & _
    '                                              ", 0 AS TaxChargedHead " & _
    '                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS PersonalReliefHead " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.isvoid = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @personalreliefheadid "
    '        If mintEmpId > 0 Then
    '            StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "                    ) AS Period " & _
    '                          "GROUP BY  Period.payperiodunkid " & _
    '                                  ", Period.employeeunkid " & _
    '                        ") AS A " & _
    '                        "LEFT JOIN hremployee_master ON A.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN cfcommon_period_tran ON A.periodunkid = cfcommon_period_tran.periodunkid "

    '        'Anjan [03 December 2015] -- Start
    '        'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
    '        StrQ &= "       LEFT JOIN ( SELECT  DISTINCT hremployee_master.employeeunkid AS EmpId " & _
    '                                            ", hremployee_meminfo_tran.membershipno " & _
    '                                      "FROM    hremployee_meminfo_tran " & _
    '                                              "JOIN hremployee_master ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                              "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                   WHERE hrmembership_master.membershipunkid = @MemId " & _
    '                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 "

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        StrQ &= "           ) AS C ON C.EmpId = A.employeeunkid "
    '        'Anjan [03 December 2015] -- End

    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery

    '        StrQ &= " ORDER BY ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), cfcommon_period_tran.end_date "

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '        End If

    '        'Anjan [03 December 2015] -- Start
    '        'ENHANCEMENT :  Included Membership No in Employee's PIN textbox on Rutta's request
    '        objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
    '        'Anjan [03 December 2015] -- End

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim rpt_Row As DataRow = Nothing
    '        Dim intPrevEmpId As Integer = 0
    '        Dim intCurrEmpId As Integer = 0
    '        Dim decTotBasicSalary As Decimal = 0
    '        Dim decTotNonCashBenefit As Decimal = 0
    '        Dim decTotQuarters As Decimal = 0
    '        Dim decTotTotGrosPay As Decimal = 0
    '        Dim decTotE1 As Decimal = 0
    '        Dim decTotE2 As Decimal = 0
    '        Dim decTotE3 As Decimal = 0
    '        Dim decTotAmtOfInterest As Decimal = 0
    '        Dim decTotRetContribution As Decimal = 0
    '        Dim decTotChargeablePay As Decimal = 0
    '        Dim decTotTaxCharged As Decimal = 0
    '        Dim decTotPersonalRelief As Decimal = 0
    '        Dim decTotPayeTax As Decimal = 0

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            intCurrEmpId = CInt(dtRow.Item("employeeunkid"))

    '            If intPrevEmpId <> intCurrEmpId Then
    '                decTotBasicSalary = 0
    '                decTotNonCashBenefit = 0
    '                decTotQuarters = 0
    '                decTotTotGrosPay = 0
    '                decTotE1 = 0
    '                decTotE2 = 0
    '                decTotE3 = 0
    '                decTotAmtOfInterest = 0
    '                decTotRetContribution = 0
    '                decTotChargeablePay = 0
    '                decTotTaxCharged = 0
    '                decTotPersonalRelief = 0
    '                decTotPayeTax = 0
    '            End If

    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("period_name").ToString
    '            rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("BasicSalary")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("NonCashBenefit")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("Quarters")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("TotalGrossPay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("E1Head")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("E2Head")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("E3Head")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("AmountOfInterestHead")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("AmountOfInterestHead")) + Math.Min(CDec(dtRow.Item("E1Head")), IIf(CDec(dtRow.Item("E2Head")) < CDec(dtRow.Item("E3Head")), CDec(dtRow.Item("E2Head")), CDec(dtRow.Item("E3Head")))), GUI.fmtCurrency)
    '            rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("TotalGrossPay")) - CDec(rpt_Row.Item("Column10")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("TaxChargedHead")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column13") = Format(CDec(dtRow.Item("PersonalReliefHead")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column14") = Format(CDec(dtRow.Item("TaxChargedHead")) - CDec(dtRow.Item("PersonalReliefHead")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column15") = dtRow.Item("employeeunkid").ToString
    '            rpt_Row.Item("Column16") = "" 'dtRow.Item("employeecode").ToString
    '            rpt_Row.Item("Column17") = dtRow.Item("employeename").ToString
    '            'Anjan [03 December 2015] -- Start
    '            'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
    '            rpt_Row.Item("Column18") = dtRow.Item("membershipno").ToString
    '            'Anjan [03 December 2015] -- End

    '            decTotBasicSalary += CDec(rpt_Row.Item("Column2"))
    '            decTotNonCashBenefit += CDec(rpt_Row.Item("Column3"))
    '            decTotQuarters += CDec(rpt_Row.Item("Column4"))
    '            decTotTotGrosPay += CDec(rpt_Row.Item("Column5"))
    '            decTotE1 += CDec(rpt_Row.Item("Column6"))
    '            decTotE2 += CDec(rpt_Row.Item("Column7"))
    '            decTotE3 += CDec(rpt_Row.Item("Column8"))
    '            decTotAmtOfInterest += CDec(rpt_Row.Item("Column9"))
    '            decTotRetContribution += CDec(rpt_Row.Item("Column10"))
    '            decTotChargeablePay += CDec(rpt_Row.Item("Column11"))
    '            decTotTaxCharged += CDec(rpt_Row.Item("Column12"))
    '            decTotPersonalRelief += CDec(rpt_Row.Item("Column13"))
    '            decTotPayeTax += CDec(rpt_Row.Item("Column14"))

    '            rpt_Row.Item("Column21") = Format(decTotBasicSalary, GUI.fmtCurrency)
    '            rpt_Row.Item("Column22") = Format(decTotNonCashBenefit, GUI.fmtCurrency)
    '            rpt_Row.Item("Column23") = Format(decTotQuarters, GUI.fmtCurrency)
    '            rpt_Row.Item("Column24") = Format(decTotTotGrosPay, GUI.fmtCurrency)
    '            rpt_Row.Item("Column25") = Format(decTotE1, GUI.fmtCurrency)
    '            rpt_Row.Item("Column26") = Format(decTotE2, GUI.fmtCurrency)
    '            rpt_Row.Item("Column27") = Format(decTotE3, GUI.fmtCurrency)
    '            rpt_Row.Item("Column28") = Format(decTotAmtOfInterest, GUI.fmtCurrency)
    '            rpt_Row.Item("Column29") = Format(decTotRetContribution, GUI.fmtCurrency)
    '            rpt_Row.Item("Column30") = Format(decTotChargeablePay, GUI.fmtCurrency)
    '            rpt_Row.Item("Column31") = Format(decTotTaxCharged, GUI.fmtCurrency)
    '            rpt_Row.Item("Column32") = Format(decTotPersonalRelief, GUI.fmtCurrency)
    '            rpt_Row.Item("Column33") = Format(decTotPayeTax, GUI.fmtCurrency)

    '            intPrevEmpId = intCurrEmpId

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptKY_P9A

    '        objRpt.SetDataSource(rpt_Data)

    '        ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED"))
    '        ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 2, "KENYA REVENUE AUTHORITY"))
    '        ReportFunction.TextChange(objRpt, "lblAppendix2A", Language.getMessage(mstrModuleName, 3, "APPENDIX 2A"))
    '        ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT"))
    '        ReportFunction.TextChange(objRpt, "lblCaption4", Language.getMessage(mstrModuleName, 5, "TAX DEDUCTION CARD YEAR") & " " & mstrFirstPeriodYearName)
    '        ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "Employer's Name"))
    '        ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
    '        ReportFunction.TextChange(objRpt, "lblEmployeeFName", Language.getMessage(mstrModuleName, 7, "Employee's Main Names"))
    '        ReportFunction.TextChange(objRpt, "lblEmployeeOName", Language.getMessage(mstrModuleName, 8, "Employees's Other Names"))
    '        ReportFunction.TextChange(objRpt, "txtEmployeeOName", "")
    '        ReportFunction.TextChange(objRpt, "lblEmplrPIN", Language.getMessage(mstrModuleName, 9, "EMPLOYER'S PIN"))
    '        ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt, "lblEmplPIN", Language.getMessage(mstrModuleName, 10, "Employee's PIN"))
    '        'Anjan [03 December 2015] -- Start
    '        'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
    '        'ReportFunction.TextChange(objRpt, "txtEmplPIN", Language.getMessage(mstrModuleName, 11, ""))
    '        'Anjan [03 December 2015] -- End

    '        ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 12, "Month"))
    '        ReportFunction.TextChange(objRpt, "lblBasic", Language.getMessage(mstrModuleName, 13, "Basic Salary"))
    '        ReportFunction.TextChange(objRpt, "lblBenefit", Language.getMessage(mstrModuleName, 14, "Benefits of Non Cash"))
    '        ReportFunction.TextChange(objRpt, "lblQuaters", Language.getMessage(mstrModuleName, 15, "Value of Quarters"))
    '        ReportFunction.TextChange(objRpt, "lblGross", Language.getMessage(mstrModuleName, 16, "Total Gross Pay"))
    '        ReportFunction.TextChange(objRpt, "lblDCRS", Language.getMessage(mstrModuleName, 17, "Defined Contribution Retirement Scheme"))
    '        ReportFunction.TextChange(objRpt, "lblE1", Language.getMessage(mstrModuleName, 18, "E1 30% of A"))
    '        ReportFunction.TextChange(objRpt, "lblE2", Language.getMessage(mstrModuleName, 19, "E2 Actual"))
    '        ReportFunction.TextChange(objRpt, "lblE3", Language.getMessage(mstrModuleName, 20, "E3 Fixed"))
    '        ReportFunction.TextChange(objRpt, "lblOwnerIntr", Language.getMessage(mstrModuleName, 21, "Owener-Occupied Interest"))
    '        ReportFunction.TextChange(objRpt, "lblAmtIntr", Language.getMessage(mstrModuleName, 22, "Amt. Of Interest"))
    '        ReportFunction.TextChange(objRpt, "lblRCOIntr", Language.getMessage(mstrModuleName, 23, "Retire. Contrib. & Owner Occ. Interest"))
    '        ReportFunction.TextChange(objRpt, "lblGTotal", Language.getMessage(mstrModuleName, 24, "Lowest Of E added to F"))
    '        ReportFunction.TextChange(objRpt, "lblChargeble", Language.getMessage(mstrModuleName, 25, "Chargeable Pay"))
    '        ReportFunction.TextChange(objRpt, "lblTaxCharged", Language.getMessage(mstrModuleName, 26, "Tax Charged"))
    '        ReportFunction.TextChange(objRpt, "lblPR", Language.getMessage(mstrModuleName, 27, "Personal Relief"))
    '        ReportFunction.TextChange(objRpt, "lblIR", Language.getMessage(mstrModuleName, 28, "Insurance Relief"))
    '        ReportFunction.TextChange(objRpt, "lblKTotal", Language.getMessage(mstrModuleName, 29, "Total"))
    '        ReportFunction.TextChange(objRpt, "lblPayTax", Language.getMessage(mstrModuleName, 30, "PAYE TAX (J-K)"))
    '        ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 31, "Totals"))
    '        ReportFunction.TextChange(objRpt, "lblText1", Language.getMessage(mstrModuleName, 32, "To be completed by Employer at the end of year"))
    '        ReportFunction.TextChange(objRpt, "lblText2", Language.getMessage(mstrModuleName, 33, "Total Tax (COLL.)"))
    '        ReportFunction.TextChange(objRpt, "txtText2", strBaseCurrencySign)
    '        ReportFunction.TextChange(objRpt, "lblText3", Language.getMessage(mstrModuleName, 34, "TOTAL CHARGEABLE PAY (COL. H)"))
    '        ReportFunction.TextChange(objRpt, "txtText3", strBaseCurrencySign)
    '        ReportFunction.TextChange(objRpt, "lblText4", Language.getMessage(mstrModuleName, 35, "IMPORTANT"))
    '        ReportFunction.TextChange(objRpt, "lblText5", Language.getMessage(mstrModuleName, 36, "1. Use P9A"))
    '        ReportFunction.TextChange(objRpt, "lblText5_1", Language.getMessage(mstrModuleName, 37, "(a) For all liable employees and where director/employee received Benefits in addition to cash emoluments."))
    '        ReportFunction.TextChange(objRpt, "lblText5_2", Language.getMessage(mstrModuleName, 38, "(b) Where an employee is eligible to deduction on owner occupler Interest."))
    '        ReportFunction.TextChange(objRpt, "lblText6", Language.getMessage(mstrModuleName, 39, "2. (a) Deductible Interest in respect of any month must not exeed"))
    '        ReportFunction.TextChange(objRpt, "lblText6_1", Language.getMessage(mstrModuleName, 40, "(b) Attach"))
    '        ReportFunction.TextChange(objRpt, "lblText6_2", Language.getMessage(mstrModuleName, 41, "(i) Photostat copy of interest certificate and statement of account from the Financial Institution."))
    '        ReportFunction.TextChange(objRpt, "lblText6_3", Language.getMessage(mstrModuleName, 42, "(ii) The DECLARATION duly signed by the employee."))
    '        ReportFunction.TextChange(objRpt, "lblText7", Language.getMessage(mstrModuleName, 43, "NAMES OF FINANCIAL INSTITUTION ADVANCING MORTGAGE LOAN"))
    '        ReportFunction.TextChange(objRpt, "lblText8", Language.getMessage(mstrModuleName, 44, "HOUSING FINANCE COMPANY OF KENYA"))
    '        ReportFunction.TextChange(objRpt, "lblText9", Language.getMessage(mstrModuleName, 45, "L R NO.  OF OWNER OCCUPIED PROPERTY: 208/119 NAIROBI WEST"))
    '        ReportFunction.TextChange(objRpt, "lblText10", Language.getMessage(mstrModuleName, 46, "DATE OF OCCUPATION OF HOUSE: 15 DECEMBER 2000"))
    '        ReportFunction.TextChange(objRpt, "lblText11", Language.getMessage(mstrModuleName, 47, "(See back of this card for further information required by Department)."))

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
    Private Function Generate_DetailReport(ByVal strDatabaseName As String _
                                           , ByVal intUserUnkid As Integer _
                                           , ByVal intYearUnkid As Integer _
                                           , ByVal intCompanyUnkid As Integer _
                                           , ByVal dtPeriodEnd As Date _
                                           , ByVal strUserModeSetting As String _
                                           , ByVal blnOnlyApproved As Boolean _
                                           , ByVal intBaseCurrencyId As Integer _
                                           , ByVal strCompanyName As String _
                                           , ByVal strCompanyTinNo As String _
                                           , ByVal strFmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String
        Dim decTotalPaye As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            Dim xUACQry, xUACFiltrQry As String

            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            Dim objHead As New clsTransactionHead
            Dim objDHead As New Dictionary(Of Integer, String)
            Dim dsHead As DataSet = objHead.getComboList(strDatabaseName, "List", False, , , , True, , , , True, , , True)

            Dim strPattern As String = "([^#]*#[^#]*)#"
            Dim strReplacement As String = "$1)"

            If mstrBasicSalaryFormula.Trim <> "" Then

                Dim strResult As String = Regex.Replace(mstrBasicSalaryFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrTaxChargedFormula.Trim <> "" Then

                Dim strResult As String = Regex.Replace(mstrTaxChargedFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If
            'Sohail (20 Mar 2021) -- End

            For Each key In mdicYearDBName

                xUACQry = "" : xUACFiltrQry = ""

                If mblnApplyUserAccessFilter = True Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, key.Value.ToString, intUserUnkid, intCompanyUnkid, key.Key, strUserModeSetting)
                End If

                StrQ &= "SELECT hremployee_master.employeeunkid INTO #" & key.Value.ToString & " FROM hremployee_master "
                'Sohail (01 Feb 2019) - [employeeunkid] = [hremployee_master.employeeunkid]

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " WHERE 1 = 1 "

                If mintEmpId > 0 Then
                    StrQ &= "                  AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= " "
            Next

            StrQ &= "SELECT  A.periodunkid " & _
                          ", period_name " & _
                          ", A.employeeunkid " & _
                          ", employeename " & _
                          ", BasicSalary " & _
                          ", NonCashBenefit " & _
                          ", Quarters " & _
                          ", BasicSalary + NonCashBenefit + Quarters AS TotalGrossPay " & _
                          ", E1Head " & _
                          ", E2Head " & _
                          ", E3Head " & _
                          ", AmountOfInterestHead " & _
                          ", TaxChargedHead " & _
                          ", PersonalReliefHead " & _
                          ", InsuranceReliefHead " & _
                          ", TaxableIncomeHead " & _
                          ", membershipno "

            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            For Each itm In objDHead
                StrQ &= " , [" & itm.Key & "] "
            Next
            'Sohail (20 Mar 2021) -- End

            StrQ &= "FROM    ( "
            'Sohail (08 May 2020) - [TaxableIncomeHead]

            Dim i As Integer = -1
            Dim strCurrDatabaseName As String = ""
            Dim StrInnerQ As String = ""
            For Each key In mdicYearDBName
                strCurrDatabaseName = key.Value
                i += 1

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= " " & _
                             "SELECT    ISNULL(Period.payperiodunkid, 0) AS periodunkid " & _
                                      ", cfcommon_period_tran.period_name " & _
                                      ", cfcommon_period_tran.end_date " & _
                                      ", ISNULL(Period.employeeunkid, 0) AS employeeunkid " & _
                                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                      ", SUM(ISNULL(Period.BasicSalary, 0)) AS BasicSalary " & _
                                      ", SUM(ISNULL(Period.NonCashBenefit, 0)) AS NonCashBenefit " & _
                                      ", SUM(ISNULL(Period.Quarters, 0)) AS Quarters " & _
                                      ", SUM(ISNULL(Period.E1Head, 0)) AS E1Head " & _
                                      ", SUM(ISNULL(Period.E2Head, 0)) AS E2Head " & _
                                      ", SUM(ISNULL(Period.E3Head, 0)) AS E3Head " & _
                                      ", SUM(ISNULL(Period.AmountOfInterestHead, 0)) AS AmountOfInterestHead " & _
                                      ", SUM(ISNULL(Period.TaxChargedHead, 0)) AS TaxChargedHead " & _
                                      ", SUM(ISNULL(Period.PersonalReliefHead, 0)) AS PersonalReliefHead " & _
                                      ", SUM(ISNULL(Period.InsuranceReliefHead, 0)) AS InsuranceReliefHead " & _
                                      ", SUM(ISNULL(Period.TaxableIncomeHead, 0)) AS TaxableIncomeHead " & _
                                      ", ISNULL(C.membershipno, '') AS membershipno "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , SUM(ISNULL(Period.[" & itm.Key & "], 0)) AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      ( SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.istaxable = 1 "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.isnoncashbenefit = 1 "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @quartersheadid "


                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @e1headid "


                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @e2headid "


                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @e3headid "


                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @amountofinterestheadid "


                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxChargedHead " & _
                                                  ", 0 AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @taxchargedheadid "


                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS BasicSalary " & _
                                                  ", 0 AS NonCashBenefit " & _
                                                  ", 0 AS Quarters " & _
                                                  ", 0 AS E1Head " & _
                                                  ", 0 AS E2Head " & _
                                                  ", 0 AS E3Head " & _
                                                  ", 0 AS AmountOfInterestHead " & _
                                                  ", 0 AS TaxChargedHead " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS PersonalReliefHead " & _
                                                  ", 0 AS InsuranceReliefHead " & _
                                                  ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @personalreliefheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                              "SELECT    payperiodunkid " & _
                                                      ", prpayrollprocess_tran.employeeunkid " & _
                                                      ", 0 AS BasicSalary " & _
                                                      ", 0 AS NonCashBenefit " & _
                                                      ", 0 AS Quarters " & _
                                                      ", 0 AS E1Head " & _
                                                      ", 0 AS E2Head " & _
                                                      ", 0 AS E3Head " & _
                                                      ", 0 AS AmountOfInterestHead " & _
                                                      ", 0 AS TaxChargedHead " & _
                                                      ", 0 AS PersonalReliefHead " & _
                                                      ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS InsuranceReliefHead " & _
                                                      ", 0 AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 "
                'Sohail (08 May 2020) - [TaxableIncomeHead]

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND prtranhead_master.tranheadunkid = @insurancereliefheadid "

                'Sohail (08 May 2020) -- Start
                'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
                StrInnerQ &= "                      UNION ALL " & _
                                              "SELECT    payperiodunkid " & _
                                                      ", prpayrollprocess_tran.employeeunkid " & _
                                                      ", 0 AS BasicSalary " & _
                                                      ", 0 AS NonCashBenefit " & _
                                                      ", 0 AS Quarters " & _
                                                      ", 0 AS E1Head " & _
                                                      ", 0 AS E2Head " & _
                                                      ", 0 AS E3Head " & _
                                                      ", 0 AS AmountOfInterestHead " & _
                                                      ", 0 AS TaxChargedHead " & _
                                                      ", 0 AS PersonalReliefHead " & _
                                                      ", 0 AS InsuranceReliefHead " & _
                                                      ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxableIncomeHead "

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead
                    StrInnerQ &= " , 0 AS [" & itm.Key & "] "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "/*LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                        "AND prtranhead_master.tranheadunkid = @taxableincomeheadid "

                'Sohail (08 May 2020) -- End

                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                For Each itm In objDHead

                    StrInnerQ &= "            UNION ALL " & _
                                              "SELECT    payperiodunkid " & _
                                                      ", prpayrollprocess_tran.employeeunkid " & _
                                                      ", 0 AS BasicSalary " & _
                                                      ", 0 AS NonCashBenefit " & _
                                                      ", 0 AS Quarters " & _
                                                      ", 0 AS E1Head " & _
                                                      ", 0 AS E2Head " & _
                                                      ", 0 AS E3Head " & _
                                                      ", 0 AS AmountOfInterestHead " & _
                                                      ", 0 AS TaxChargedHead " & _
                                                      ", 0 AS PersonalReliefHead " & _
                                                      ", 0 AS InsuranceReliefHead " & _
                                                      ", 0 AS TaxableIncomeHead "

                    For Each itm1 In objDHead
                        If itm1.Key = itm.Key Then
                            StrInnerQ &= " , CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS [" & itm1.Key & "] "
                        Else
                            StrInnerQ &= " , 0 AS [" & itm1.Key & "] "
                        End If
                    Next

                    StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 "

                    StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                            "AND prtranhead_master.tranheadunkid = " & itm.Key & " "
                Next
                'Sohail (20 Mar 2021) -- End

                StrInnerQ &= "                    ) AS Period " & _
                                    "LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON Period.employeeunkid = hremployee_master.employeeunkid " & _
                                    "LEFT JOIN " & strCurrDatabaseName & "..cfcommon_period_tran ON Period.Payperiodunkid = cfcommon_period_tran.periodunkid "

                StrInnerQ &= "       LEFT JOIN ( SELECT  DISTINCT hremployee_master.employeeunkid AS EmpId " & _
                                                ", hremployee_meminfo_tran.membershipno " & _
                                                  "FROM    #" & strCurrDatabaseName & " " & _
                                                          "LEFT JOIN " & strCurrDatabaseName & "..hremployee_meminfo_tran ON #" & strCurrDatabaseName & ".employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                                          "JOIN " & strCurrDatabaseName & "..hremployee_master ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                          "LEFT JOIN " & strCurrDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "


                StrInnerQ &= "                   WHERE hrmembership_master.membershipunkid = @MemId " & _
                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 "


                StrInnerQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                  "GROUP BY  Period.payperiodunkid " & _
                                            ", cfcommon_period_tran.period_name " & _
                                          ", cfcommon_period_tran.end_date " & _
                                          ", Period.employeeunkid " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                                          ", ISNULL(C.membershipno, '') "


            Next

            Call FilterTitleAndFilterQuery()

            StrQ &= StrInnerQ
            StrQ &= ") AS A "

            StrQ &= " ORDER BY employeename, end_date "


            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value.ToString & " "
            Next

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim intPrevEmpId As Integer = 0
            Dim intCurrEmpId As Integer = 0
            Dim decTotBasicSalary As Decimal = 0
            Dim decTotNonCashBenefit As Decimal = 0
            Dim decTotQuarters As Decimal = 0
            Dim decTotTotGrosPay As Decimal = 0
            Dim decTotE1 As Decimal = 0
            Dim decTotE2 As Decimal = 0
            Dim decTotE3 As Decimal = 0
            Dim decTotAmtOfInterest As Decimal = 0
            Dim decTotRetContribution As Decimal = 0
            Dim decTotChargeablePay As Decimal = 0
            Dim decTotTaxCharged As Decimal = 0
            Dim decTotPersonalRelief As Decimal = 0
            Dim decTotInsuranceRelief As Decimal = 0 'Sohail (20 Jan 2017)
            Dim decTotPayeTax As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                intCurrEmpId = CInt(dtRow.Item("employeeunkid"))

                If intPrevEmpId <> intCurrEmpId Then
                    decTotBasicSalary = 0
                    decTotNonCashBenefit = 0
                    decTotQuarters = 0
                    decTotTotGrosPay = 0
                    decTotE1 = 0
                    decTotE2 = 0
                    decTotE3 = 0
                    decTotAmtOfInterest = 0
                    decTotRetContribution = 0
                    decTotChargeablePay = 0
                    decTotTaxCharged = 0
                    decTotPersonalRelief = 0
                    decTotInsuranceRelief = 0 'Sohail (20 Jan 2017)
                    decTotPayeTax = 0
                End If

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("period_name").ToString
                rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("BasicSalary")), strFmtCurrency)
                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                If mstrBasicSalaryFormula.Trim <> "" Then
                    Dim dt As New DataTable
                    Dim s As String = mstrBasicSalaryFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dtRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    rpt_Row.Item("Column2") = Format(CDec(ans), strFmtCurrency)
                End If
                'Sohail (20 Mar 2021) -- End
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("NonCashBenefit")), strFmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("Quarters")), strFmtCurrency)
                'Sohail (15 Apr 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                'rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("TotalGrossPay")), strFmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(Format(CDec(rpt_Row.Item("Column2")), strFmtCurrency)) + CDec(Format(CDec(rpt_Row.Item("Column3")), strFmtCurrency)) + CDec(Format(CDec(rpt_Row.Item("Column4")), strFmtCurrency)), strFmtCurrency)
                'Sohail (15Apr 2021) -- End
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("E1Head")), strFmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("E2Head")), strFmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("E3Head")), strFmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("AmountOfInterestHead")), strFmtCurrency)
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("AmountOfInterestHead")) + Math.Min(CDec(dtRow.Item("E1Head")), IIf(CDec(dtRow.Item("E2Head")) < CDec(dtRow.Item("E3Head")), CDec(dtRow.Item("E2Head")), CDec(dtRow.Item("E3Head")))), strFmtCurrency)
                'Sohail (08 May 2020) -- Start
                'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
                'rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("TotalGrossPay")) - CDec(rpt_Row.Item("Column10")), strFmtCurrency)
                rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("TaxableIncomeHead")), strFmtCurrency)
                'Sohail (08 May 2020) -- End
                rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("TaxChargedHead")), strFmtCurrency)
                'Sohail (20 Mar 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                If mstrTaxChargedFormula.Trim <> "" Then
                    Dim dt As New DataTable
                    Dim s As String = mstrTaxChargedFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dtRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    rpt_Row.Item("Column12") = Format(CDec(ans), strFmtCurrency)
                End If
                'Sohail (20 Mar 2021) -- End
                rpt_Row.Item("Column13") = Format(CDec(dtRow.Item("PersonalReliefHead")), strFmtCurrency)
                'Sohail (20 Jan 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
                'rpt_Row.Item("Column14") = Format(CDec(dtRow.Item("TaxChargedHead")) - CDec(dtRow.Item("PersonalReliefHead")), strFmtCurrency)
                'Sohail (21 Jan 2021) -- Start
                'Sheer Logic - P9A report Enhancement : OLD-267 - Column L should return zero if Columns (J-K) is negative.  This is meant to avoid negative values on this report.
                'rpt_Row.Item("Column14") = Format(CDec(dtRow.Item("TaxChargedHead")) - (CDec(dtRow.Item("PersonalReliefHead")) + CDec(dtRow.Item("InsuranceReliefHead"))), strFmtCurrency)
                'Sohail (03 Apr 2021) -- Start
                'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                'If CDec(dtRow.Item("TaxChargedHead")) - (CDec(dtRow.Item("PersonalReliefHead")) + CDec(dtRow.Item("InsuranceReliefHead"))) >= 0 Then
                '    rpt_Row.Item("Column14") = Format(CDec(dtRow.Item("TaxChargedHead")) - (CDec(dtRow.Item("PersonalReliefHead")) + CDec(dtRow.Item("InsuranceReliefHead"))), strFmtCurrency)
                'Else
                '    rpt_Row.Item("Column14") = Format(0, strFmtCurrency)
                'End If
                If CDec(rpt_Row.Item("Column12")) - (CDec(dtRow.Item("PersonalReliefHead")) + CDec(dtRow.Item("InsuranceReliefHead"))) >= 0 Then
                    rpt_Row.Item("Column14") = Format(CDec(rpt_Row.Item("Column12")) - (CDec(dtRow.Item("PersonalReliefHead")) + CDec(dtRow.Item("InsuranceReliefHead"))), strFmtCurrency)
                Else
                    rpt_Row.Item("Column14") = Format(0, strFmtCurrency)
                End If
                'Sohail (03 Apr 2021) -- End
                'Sohail (21 Jan 2021) -- End
                'Sohail (20 Jan 2017) -- End
                rpt_Row.Item("Column15") = dtRow.Item("employeeunkid").ToString
                rpt_Row.Item("Column16") = "" 'dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column17") = dtRow.Item("employeename").ToString
                'Anjan [03 December 2015] -- Start
                'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
                rpt_Row.Item("Column18") = dtRow.Item("membershipno").ToString
                'Anjan [03 December 2015] -- End
                'Sohail (18 Jan 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
                rpt_Row.Item("Column19") = Format(CDec(dtRow.Item("InsuranceReliefHead")), strFmtCurrency)
                'Sohail (18 Jan 2017) -- End

                decTotBasicSalary += CDec(rpt_Row.Item("Column2"))
                decTotNonCashBenefit += CDec(rpt_Row.Item("Column3"))
                decTotQuarters += CDec(rpt_Row.Item("Column4"))
                decTotTotGrosPay += CDec(rpt_Row.Item("Column5"))
                decTotE1 += CDec(rpt_Row.Item("Column6"))
                decTotE2 += CDec(rpt_Row.Item("Column7"))
                decTotE3 += CDec(rpt_Row.Item("Column8"))
                decTotAmtOfInterest += CDec(rpt_Row.Item("Column9"))
                decTotRetContribution += CDec(rpt_Row.Item("Column10"))
                decTotChargeablePay += CDec(rpt_Row.Item("Column11"))
                decTotTaxCharged += CDec(rpt_Row.Item("Column12"))
                decTotPersonalRelief += CDec(rpt_Row.Item("Column13"))
                decTotInsuranceRelief += CDec(rpt_Row.Item("Column19")) 'Sohail (20 Jan 2017)
                decTotPayeTax += CDec(rpt_Row.Item("Column14"))

                rpt_Row.Item("Column21") = Format(decTotBasicSalary, strFmtCurrency)
                rpt_Row.Item("Column22") = Format(decTotNonCashBenefit, strFmtCurrency)
                rpt_Row.Item("Column23") = Format(decTotQuarters, strFmtCurrency)
                rpt_Row.Item("Column24") = Format(decTotTotGrosPay, strFmtCurrency)
                rpt_Row.Item("Column25") = Format(decTotE1, strFmtCurrency)
                rpt_Row.Item("Column26") = Format(decTotE2, strFmtCurrency)
                rpt_Row.Item("Column27") = Format(decTotE3, strFmtCurrency)
                rpt_Row.Item("Column28") = Format(decTotAmtOfInterest, strFmtCurrency)
                rpt_Row.Item("Column29") = Format(decTotRetContribution, strFmtCurrency)
                rpt_Row.Item("Column30") = Format(decTotChargeablePay, strFmtCurrency)
                rpt_Row.Item("Column31") = Format(decTotTaxCharged, strFmtCurrency)
                rpt_Row.Item("Column32") = Format(decTotPersonalRelief, strFmtCurrency)
                rpt_Row.Item("Column34") = Format(decTotInsuranceRelief, strFmtCurrency) 'Sohail (20 Jan 2017)
                rpt_Row.Item("Column33") = Format(decTotPayeTax, strFmtCurrency)

                intPrevEmpId = intCurrEmpId

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptKY_P9A

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED"))
            ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 2, "KENYA REVENUE AUTHORITY"))
            ReportFunction.TextChange(objRpt, "lblAppendix2A", Language.getMessage(mstrModuleName, 3, "APPENDIX 2A"))
            ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblCaption4", Language.getMessage(mstrModuleName, 5, "TAX DEDUCTION CARD YEAR") & " " & mstrFirstPeriodYearName)
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "Employer's Name"))
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            'ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "txtEmployerName", strCompanyName)
            'Sohail (18 Jan 2017) -- End
            ReportFunction.TextChange(objRpt, "lblEmployeeFName", Language.getMessage(mstrModuleName, 7, "Employee's Main Names"))
            ReportFunction.TextChange(objRpt, "lblEmployeeOName", Language.getMessage(mstrModuleName, 8, "Employees's Other Names"))
            ReportFunction.TextChange(objRpt, "txtEmployeeOName", "")
            ReportFunction.TextChange(objRpt, "lblEmplrPIN", Language.getMessage(mstrModuleName, 9, "EMPLOYER'S PIN"))
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            'ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "txtEmplrPIN", strCompanyTinNo)
            'Sohail (18 Jan 2017) -- End
            ReportFunction.TextChange(objRpt, "lblEmplPIN", Language.getMessage(mstrModuleName, 10, "Employee's PIN"))
            'Anjan [03 December 2015] -- Start
            'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
            'ReportFunction.TextChange(objRpt, "txtEmplPIN", Language.getMessage(mstrModuleName, 11, ""))
            'Anjan [03 December 2015] -- End

            ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 12, "Month"))
            ReportFunction.TextChange(objRpt, "lblBasic", Language.getMessage(mstrModuleName, 13, "Basic Salary"))
            ReportFunction.TextChange(objRpt, "lblBenefit", Language.getMessage(mstrModuleName, 14, "Benefits of Non Cash"))
            ReportFunction.TextChange(objRpt, "lblQuaters", Language.getMessage(mstrModuleName, 15, "Value of Quarters"))
            ReportFunction.TextChange(objRpt, "lblGross", Language.getMessage(mstrModuleName, 16, "Total Gross Pay"))
            ReportFunction.TextChange(objRpt, "lblDCRS", Language.getMessage(mstrModuleName, 17, "Defined Contribution Retirement Scheme"))
            ReportFunction.TextChange(objRpt, "lblE1", Language.getMessage(mstrModuleName, 18, "E1 30% of A"))
            ReportFunction.TextChange(objRpt, "lblE2", Language.getMessage(mstrModuleName, 19, "E2 Actual"))
            ReportFunction.TextChange(objRpt, "lblE3", Language.getMessage(mstrModuleName, 20, "E3 Fixed"))
            ReportFunction.TextChange(objRpt, "lblOwnerIntr", Language.getMessage(mstrModuleName, 21, "Owener-Occupied Interest"))
            ReportFunction.TextChange(objRpt, "lblAmtIntr", Language.getMessage(mstrModuleName, 22, "Amt. Of Interest"))
            ReportFunction.TextChange(objRpt, "lblRCOIntr", Language.getMessage(mstrModuleName, 23, "Retire. Contrib. & Owner Occ. Interest"))
            ReportFunction.TextChange(objRpt, "lblGTotal", Language.getMessage(mstrModuleName, 24, "Lowest Of E added to F"))
            ReportFunction.TextChange(objRpt, "lblChargeble", Language.getMessage(mstrModuleName, 25, "Chargeable Pay"))
            ReportFunction.TextChange(objRpt, "lblTaxCharged", Language.getMessage(mstrModuleName, 26, "Tax Charged"))
            ReportFunction.TextChange(objRpt, "lblPR", Language.getMessage(mstrModuleName, 27, "Personal Relief"))
            ReportFunction.TextChange(objRpt, "lblIR", Language.getMessage(mstrModuleName, 28, "Insurance Relief"))
            ReportFunction.TextChange(objRpt, "lblKTotal", Language.getMessage(mstrModuleName, 29, "Total"))
            ReportFunction.TextChange(objRpt, "lblPayTax", Language.getMessage(mstrModuleName, 30, "PAYE TAX (J-K)"))
            ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 31, "Totals"))
            ReportFunction.TextChange(objRpt, "lblText1", Language.getMessage(mstrModuleName, 32, "To be completed by Employer at the end of year"))
            ReportFunction.TextChange(objRpt, "lblText2", Language.getMessage(mstrModuleName, 33, "Total Tax (COLL.)"))
            ReportFunction.TextChange(objRpt, "txtText2", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "lblText3", Language.getMessage(mstrModuleName, 34, "TOTAL CHARGEABLE PAY (COL. H)"))
            ReportFunction.TextChange(objRpt, "txtText3", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "lblText4", Language.getMessage(mstrModuleName, 35, "IMPORTANT"))
            ReportFunction.TextChange(objRpt, "lblText5", Language.getMessage(mstrModuleName, 36, "1. Use P9A"))
            ReportFunction.TextChange(objRpt, "lblText5_1", Language.getMessage(mstrModuleName, 37, "(a) For all liable employees and where director/employee received Benefits in addition to cash emoluments."))
            ReportFunction.TextChange(objRpt, "lblText5_2", Language.getMessage(mstrModuleName, 38, "(b) Where an employee is eligible to deduction on owner occupler Interest."))
            ReportFunction.TextChange(objRpt, "lblText6", Language.getMessage(mstrModuleName, 39, "2. (a) Deductible Interest in respect of any month must not exeed"))
            ReportFunction.TextChange(objRpt, "lblText6_1", Language.getMessage(mstrModuleName, 40, "(b) Attach"))
            ReportFunction.TextChange(objRpt, "lblText6_2", Language.getMessage(mstrModuleName, 41, "(i) Photostat copy of interest certificate and statement of account from the Financial Institution."))
            ReportFunction.TextChange(objRpt, "lblText6_3", Language.getMessage(mstrModuleName, 42, "(ii) The DECLARATION duly signed by the employee."))
            ReportFunction.TextChange(objRpt, "lblText7", Language.getMessage(mstrModuleName, 43, "NAMES OF FINANCIAL INSTITUTION ADVANCING MORTGAGE LOAN"))
            ReportFunction.TextChange(objRpt, "lblText8", Language.getMessage(mstrModuleName, 44, "HOUSING FINANCE COMPANY OF KENYA"))
            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-311 : Colum width & static data removal.
            'ReportFunction.TextChange(objRpt, "lblText9", Language.getMessage(mstrModuleName, 45, "L R NO.  OF OWNER OCCUPIED PROPERTY: 208/119 NAIROBI WEST"))
            'ReportFunction.TextChange(objRpt, "lblText10", Language.getMessage(mstrModuleName, 46, "DATE OF OCCUPATION OF HOUSE: 15 DECEMBER 2000"))
            ReportFunction.TextChange(objRpt, "lblText9", Language.getMessage(mstrModuleName, 48, "L R NO.  OF OWNER OCCUPIED PROPERTY:"))
            ReportFunction.TextChange(objRpt, "lblText10", Language.getMessage(mstrModuleName, 49, "DATE OF OCCUPATION OF HOUSE:"))
            'Sohail (20 Mar 2021) -- End
            ReportFunction.TextChange(objRpt, "lblText11", Language.getMessage(mstrModuleName, 47, "(See back of this card for further information required by Department)."))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (18 Jan 2017) -- End
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED")
            Language.setMessage(mstrModuleName, 2, "KENYA REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 3, "APPENDIX 2A")
            Language.setMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT")
            Language.setMessage(mstrModuleName, 5, "TAX DEDUCTION CARD YEAR")
            Language.setMessage(mstrModuleName, 6, "Employer's Name")
            Language.setMessage(mstrModuleName, 7, "Employee's Main Names")
            Language.setMessage(mstrModuleName, 8, "Employees's Other Names")
            Language.setMessage(mstrModuleName, 9, "EMPLOYER'S PIN")
            Language.setMessage(mstrModuleName, 10, "Employee's PIN")
            Language.setMessage(mstrModuleName, 12, "Month")
            Language.setMessage(mstrModuleName, 13, "Basic Salary")
            Language.setMessage(mstrModuleName, 14, "Benefits of Non Cash")
            Language.setMessage(mstrModuleName, 15, "Value of Quarters")
            Language.setMessage(mstrModuleName, 16, "Total Gross Pay")
            Language.setMessage(mstrModuleName, 17, "Defined Contribution Retirement Scheme")
            Language.setMessage(mstrModuleName, 18, "E1 30% of A")
            Language.setMessage(mstrModuleName, 19, "E2 Actual")
            Language.setMessage(mstrModuleName, 20, "E3 Fixed")
            Language.setMessage(mstrModuleName, 21, "Owener-Occupied Interest")
            Language.setMessage(mstrModuleName, 22, "Amt. Of Interest")
            Language.setMessage(mstrModuleName, 23, "Retire. Contrib. & Owner Occ. Interest")
            Language.setMessage(mstrModuleName, 24, "Lowest Of E added to F")
            Language.setMessage(mstrModuleName, 25, "Chargeable Pay")
            Language.setMessage(mstrModuleName, 26, "Tax Charged")
            Language.setMessage(mstrModuleName, 27, "Personal Relief")
            Language.setMessage(mstrModuleName, 28, "Insurance Relief")
            Language.setMessage(mstrModuleName, 29, "Total")
            Language.setMessage(mstrModuleName, 30, "PAYE TAX (J-K)")
            Language.setMessage(mstrModuleName, 31, "Totals")
            Language.setMessage(mstrModuleName, 32, "To be completed by Employer at the end of year")
            Language.setMessage(mstrModuleName, 33, "Total Tax (COLL.)")
            Language.setMessage(mstrModuleName, 34, "TOTAL CHARGEABLE PAY (COL. H)")
            Language.setMessage(mstrModuleName, 35, "IMPORTANT")
            Language.setMessage(mstrModuleName, 36, "1. Use P9A")
            Language.setMessage(mstrModuleName, 37, "(a) For all liable employees and where director/employee received Benefits in addition to cash emoluments.")
            Language.setMessage(mstrModuleName, 38, "(b) Where an employee is eligible to deduction on owner occupler Interest.")
            Language.setMessage(mstrModuleName, 39, "2. (a) Deductible Interest in respect of any month must not exeed")
            Language.setMessage(mstrModuleName, 40, "(b) Attach")
            Language.setMessage(mstrModuleName, 41, "(i) Photostat copy of interest certificate and statement of account from the Financial Institution.")
            Language.setMessage(mstrModuleName, 42, "(ii) The DECLARATION duly signed by the employee.")
            Language.setMessage(mstrModuleName, 43, "NAMES OF FINANCIAL INSTITUTION ADVANCING MORTGAGE LOAN")
            Language.setMessage(mstrModuleName, 44, "HOUSING FINANCE COMPANY OF KENYA")
            Language.setMessage(mstrModuleName, 47, "(See back of this card for further information required by Department).")
			Language.setMessage(mstrModuleName, 48, "L R NO.  OF OWNER OCCUPIED PROPERTY:")
			Language.setMessage(mstrModuleName, 49, "DATE OF OCCUPATION OF HOUSE:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
