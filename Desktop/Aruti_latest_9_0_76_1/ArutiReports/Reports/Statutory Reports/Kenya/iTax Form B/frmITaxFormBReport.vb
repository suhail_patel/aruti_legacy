'************************************************************************************************************************************
'Class Name : frmITaxFormBReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmITaxFormBReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmITaxFormBReport"
    Private objiTaxFormB As clsITaxFormBReport
    Private mstrPeriodsName As String = ""

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mdtPeriodEndDate As DateTime
    'Nilay (11 Apr 2017) -- Start
    'Enhancements: New statutory report iTax Form C for CCK
    Private menReport As enArutiReport
    Private objReport As New clsArutiReportClass
    'Nilay (11 Apr 2017) -- End

#End Region

#Region " Contructor "

    'Nilay (11 Apr 2017) -- Start
    'Enhancements: New statutory report iTax Form C for CCK
    'Public Sub New()
    Public Sub New(ByVal enReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        menReport = enReport
        'Nilay (11 Apr 2017) -- End 

        'Nilay (11 Apr 2017) -- Start
        'Enhancements: New statutory report iTax Form C for CCK
        'objiTaxFormB = New clsITaxFormBReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objiTaxFormB = New clsITaxFormBReport(menReport, intLangId, intCompanyId)
        'Nilay (11 Apr 2017) -- End

        objiTaxFormB.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        HouseAllowance = 1
        TransportAllowance = 2
        LeavePay = 3
        OverTimeAllowance = 4
        DirectorsFee = 5
        LumpSum = 6
        Membership = 7
        Other_Earning = 8
        'Sohail (11 Sep 2015) -- Start
        'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
        ActualContribution = 9
        MonthlyRelief = 10
        'Sohail (11 Sep 2015) -- End
        'Sohail (03 Apr 2017) -- Start
        'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
        Resident = 11
        NonResident = 12
        OwnedHouseValue = 13
        RentedHouseValue = 14
        OwnedHouse = 15
        RentedHouse = 16
        Column_S = 17
        Column_AB = 18
        DisabilityMembership = 19
        'Sohail (03 Apr 2017) -- End
        'Sohail (02 Feb 2018) -- Start
        'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
        Column_N = 20
        Column_AA = 21
        Column_AG = 22
        'Sohail (02 Feb 2018) -- End
        'Sohail (08 Mar 2018) -- Start
        'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
        Column_AI = 23
        'Sohail (08 Mar 2018) -- End
        'Hemant (17 Nov 2020) -- Start
        'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
        Ignore_Zero = 24
        'Hemant (17 Nov 2020) -- End
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim dsCombos As New DataSet
        'Nilay (17 Feb 2017) -- Start
        'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
        Dim objCommon As New clsCommon_Master
        Dim objPayPoint As New clspaypoint_master
        'Nilay (17 Feb 2017) -- End

        Dim objExcessSlab As New clsTranheadInexcessslabTran

        Try

            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (03 Apr 2017) -- Start
            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
            With cboDisabilityMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            'Sohail (03 Apr 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Housing", True, enTranHeadType.EarningForEmployees, , , , , "typeof_id NOT IN ( " & enTypeOf.Salary & ")")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Housing", True, enTranHeadType.EarningForEmployees, , , , , "typeof_id NOT IN ( " & enTypeOf.Salary & ")")
            'Sohail (21 Aug 2015) -- End
            With cboHouseAllow
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboTransAllow
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboLeavePay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            'Sohail (11 Sep 2015) -- Start
            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
            'With cboOverTimeAllow
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables(0).Copy
            '    .SelectedValue = 0
            'End With
            'Sohail (11 Sep 2015) -- End

            With cboDirectorFee
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboLumpSum
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With


            dsCombos = objExcessSlab.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With

            'Sohail (11 Sep 2015) -- Start
            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("ActualContribution", True)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "ActualContribution", True)
            'Sohail (21 Aug 2015) -- End
            With cboActualContribution
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("ActualContribution")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("MonthRelief", True, enTranHeadType.Informational)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "MonthRelief", True, enTranHeadType.Informational)
            'Sohail (21 Aug 2015) -- End
            With cboMonthlyRelief
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("MonthRelief")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("AllHeads", True)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "AllHeads", True)
            'Sohail (21 Aug 2015) -- End
            With cboOverTimeAllow
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads")
                .SelectedValue = 0
            End With

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            With cboEmployerOwnedHouseValue
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With

            With cboEmployerRentedHouseValue
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With

            With cboColumnS
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With

            With cboColumnAB
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With
            'Nilay (17 Feb 2017) -- End

            'Sohail (11 Sep 2015) -- End

            'Sohail (02 Feb 2018) -- Start
            'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
            With cboColumnN
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With

            With cboColumnAA
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With

            With cboColumnAG
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("AllHeads").Copy
                .SelectedValue = 0
            End With
            'Sohail (02 Feb 2018) -- End

            'Sohail (08 Mar 2018) -- Start
            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "PAYE", True, , , CInt(enTypeOf.Taxes))
            With cboColumnAI
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("PAYE")
                .SelectedValue = 0
            End With
            'Sohail (08 Mar 2018) -- End

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            With cboResident
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("PayType")
            End With

            With cboNonResident
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("PayType").Copy
            End With

            dsCombos = objPayPoint.getListForCombo("Paypoint", True)

            With cboEmployerOwnedHouse
                .ValueMember = "paypointunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Paypoint")
            End With

            With cboEmployerRentedHouse
                .ValueMember = "paypointunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Paypoint").Copy
            End With
            'Nilay (17 Feb 2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objEmp = Nothing
            objperiod = Nothing
            objTranHead = Nothing
            objCommon = Nothing 'Nilay (17 Feb 2017)
            objPayPoint = Nothing 'Nilay (17 Feb 2017)
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboHouseAllow.SelectedValue = 0
            cboTransAllow.SelectedValue = 0
            cboLeavePay.SelectedValue = 0
            cboOverTimeAllow.SelectedValue = 0
            cboDirectorFee.SelectedValue = 0
            cboLumpSum.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport
            'Sohail (03 Apr 2017) -- Start
            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
            cboDisabilityMembership.SelectedValue = 0
            'Sohail (03 Apr 2017) -- End

            Call CheckAll(lvPeriod, False)

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""

            'Hemant (17 Nov 2020) -- Start
            'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
            chkIgnoreZero.Checked = True
            'Hemant (17 Nov 2020) -- End

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.ITax_Form_B_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.HouseAllowance
                            cboHouseAllow.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.TransportAllowance
                            cboTransAllow.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.LeavePay
                            cboLeavePay.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OverTimeAllowance
                            cboOverTimeAllow.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.DirectorsFee
                            cboDirectorFee.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.LumpSum
                            cboLumpSum.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.Other_Earning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            Else
                                gbBasicSalaryOtherEarning.Checked = False
                            End If

                            'Sohail (11 Sep 2015) -- Start
                            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                        Case enHeadTypeId.ActualContribution
                            cboActualContribution.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.MonthlyRelief
                            cboMonthlyRelief.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (11 Sep 2015) -- End

                            'Sohail (03 Apr 2017) -- Start
                            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
                        Case enHeadTypeId.Resident
                            cboResident.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.NonResident
                            cboNonResident.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OwnedHouseValue
                            cboEmployerOwnedHouseValue.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.RentedHouseValue
                            cboEmployerRentedHouseValue.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OwnedHouse
                            cboEmployerOwnedHouse.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.RentedHouse
                            cboEmployerRentedHouse.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Column_S
                            cboColumnS.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Column_AB
                            cboColumnAB.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.DisabilityMembership
                            cboDisabilityMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (03 Apr 2017) -- End

                            'Sohail (02 Feb 2018) -- Start
                            'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
                        Case enHeadTypeId.Column_N
                            cboColumnN.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Column_AA
                            cboColumnAA.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Column_AG
                            cboColumnAG.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (02 Feb 2018) -- End

                            'Sohail (08 Mar 2018) -- Start
                            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
                        Case enHeadTypeId.Column_AI
                            cboColumnAI.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (08 Mar 2018) -- End

                            'Hemant (17 Nov 2020) -- Start
                            'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
                        Case enHeadTypeId.Ignore_Zero
                            chkIgnoreZero.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (17 Nov 2020) -- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter(Optional ByVal blnSaveSelection As Boolean = False) As Boolean
        Try
            objiTaxFormB.SetDefaultValue()

            mstrPeriodsName = String.Empty

            If blnSaveSelection = False Then
            If CInt(cboSlabEffectivePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Effective Period is mandatory information. Please select Effective Period."), enMsgBoxStyle.Information)
                cboSlabEffectivePeriod.Focus()
                Return False
            ElseIf lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check Periods to view report."), enMsgBoxStyle.Information)
                lvPeriod.Focus()
                Return False
                End If
            End If

            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If


            If CInt(cboMembership.SelectedValue) > 0 Then
                objiTaxFormB._MembershipId = cboMembership.SelectedValue
                objiTaxFormB._MembershipName = cboMembership.Text
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objiTaxFormB._EmployeeId = CInt(cboEmployee.SelectedValue)
                objiTaxFormB._EmployeeName = cboEmployee.Text
            End If

            If CInt(cboHouseAllow.SelectedValue) > 0 Then
                objiTaxFormB._ColumnFTranId = CInt(cboHouseAllow.SelectedValue)
            End If

            If CInt(cboTransAllow.SelectedValue) > 0 Then
                objiTaxFormB._ColumnGTranId = CInt(cboTransAllow.SelectedValue)
            End If

            If CInt(cboLeavePay.SelectedValue) > 0 Then
                objiTaxFormB._ColumnHTranId = CInt(cboLeavePay.SelectedValue)
            End If

            If CInt(cboOverTimeAllow.SelectedValue) > 0 Then
                objiTaxFormB._ColumnITranId = CInt(cboOverTimeAllow.SelectedValue)
            End If

            If CInt(cboDirectorFee.SelectedValue) > 0 Then
                objiTaxFormB._ColumnJTranId = CInt(cboDirectorFee.SelectedValue)
            End If

            If CInt(cboLumpSum.SelectedValue) > 0 Then
                objiTaxFormB._ColumnKTranId = CInt(cboLumpSum.SelectedValue)
            End If

            'Sohail (11 Sep 2015) -- Start
            'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
            If CInt(cboActualContribution.SelectedValue) > 0 Then
                objiTaxFormB._ColumnYTranId = CInt(cboActualContribution.SelectedValue)
            End If

            If CInt(cboMonthlyRelief.SelectedValue) > 0 Then
                objiTaxFormB._ColumnAFTranId = CInt(cboMonthlyRelief.SelectedValue)
            End If
            'Sohail (11 Sep 2015) -- End


            If gbBasicSalaryOtherEarning.Checked = True Then
                objiTaxFormB._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objiTaxFormB._OtherEarningTranId = 0
            End If

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            If CInt(cboResident.SelectedValue) > 0 Then
                objiTaxFormB._ResidentPayTypeID = CInt(cboResident.SelectedValue)
            End If

            If CInt(cboNonResident.SelectedValue) > 0 Then
                objiTaxFormB._NonResidentPayTypeID = CInt(cboNonResident.SelectedValue)
            End If

            If CInt(cboColumnS.SelectedValue) > 0 Then
                objiTaxFormB._ColumnSTranId = CInt(cboColumnS.SelectedValue)
            End If

            If CInt(cboEmployerOwnedHouseValue.SelectedValue) > 0 Then
                objiTaxFormB._EmployerOwnedHouseTranId = CInt(cboEmployerOwnedHouseValue.SelectedValue)
            End If

            If CInt(cboEmployerRentedHouseValue.SelectedValue) > 0 Then
                objiTaxFormB._EmployerRentedHouseTranId = CInt(cboEmployerRentedHouseValue.SelectedValue)
            End If

            If CInt(cboEmployerOwnedHouse.SelectedValue) > 0 Then
                objiTaxFormB._EmployerOwnedHousePayPointID = CInt(cboEmployerOwnedHouse.SelectedValue)
            End If

            If CInt(cboEmployerRentedHouse.SelectedValue) > 0 Then
                objiTaxFormB._EmployerRentedHousePayPointID = CInt(cboEmployerRentedHouse.SelectedValue)
            End If

            If CInt(cboColumnS.SelectedValue) > 0 Then
                objiTaxFormB._ColumnSTranId = CInt(cboColumnS.SelectedValue)
            End If

            If CInt(cboColumnAB.SelectedValue) > 0 Then
                objiTaxFormB._ColumnABTranId = CInt(cboColumnAB.SelectedValue)
            End If
            'Nilay (17 Feb 2017) -- End

            'Sohail (02 Feb 2018) -- Start
            'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
            If CInt(cboColumnN.SelectedValue) > 0 Then
                objiTaxFormB._ColumnNTranId = CInt(cboColumnN.SelectedValue)
            End If

            If CInt(cboColumnAA.SelectedValue) > 0 Then
                objiTaxFormB._ColumnAATranId = CInt(cboColumnAA.SelectedValue)
            End If

            If CInt(cboColumnAG.SelectedValue) > 0 Then
                objiTaxFormB._ColumnAGTranId = CInt(cboColumnAG.SelectedValue)
            End If
            'Sohail (02 Feb 2018) -- End

            'Sohail (08 Mar 2018) -- Start
            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
            If CInt(cboColumnAI.SelectedValue) > 0 Then
                objiTaxFormB._ColumnAITranId = CInt(cboColumnAI.SelectedValue)
            End If
            'Sohail (08 Mar 2018) -- End

            'Sohail (03 Apr 2017) -- Start
            'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
            objiTaxFormB._DisabilityMembershipId = cboDisabilityMembership.SelectedValue
            objiTaxFormB._DisabilityMembershipName = cboDisabilityMembership.Text
            'Sohail (03 Apr 2017) -- End

            objiTaxFormB._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            Dim mstrPeriodIds As String = ""
            Dim allId As List(Of String) = (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            Dim allName As List(Of String) = (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriodName.Index).Text)).ToList
            mstrPeriodIds = String.Join(",", allId.ToArray)
            mstrPeriodsName = String.Join(",", allName.ToArray)

            objiTaxFormB._PeriodIds = mstrPeriodIds
            objiTaxFormB._PeriodNames = mstrPeriodsName

            objiTaxFormB._ViewByIds = mstrStringIds
            objiTaxFormB._ViewIndex = mintViewIdx
            objiTaxFormB._ViewByName = mstrStringName
            objiTaxFormB._Analysis_Fields = mstrAnalysis_Fields
            objiTaxFormB._Analysis_Join = mstrAnalysis_Join
            objiTaxFormB._Analysis_OrderBy = mstrAnalysis_OrderBy
            objiTaxFormB._Report_GroupName = mstrReport_GroupName

            'Hemant (17 Nov 2020) -- Start
            'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
            objiTaxFormB._IgnoreZeroValue = chkIgnoreZero.CheckState
            objiTaxFormB._IncluderInactiveEmp = False
            'Hemant (17 Nov 2020) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub CheckAll(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmITaxFormBReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objiTaxFormB = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmITaxFormBReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmITaxFormBReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)


            Call OtherSettings()

            'Nilay (11 Apr 2017) -- Start
            'Enhancements: New statutory report iTax Form C for CCK
            Dim dsList As DataSet = objReport.getReportList(User._Object._Userunkid, Company._Object._Companyunkid, True)
            Dim row As DataRow() = dsList.Tables(0).Select("ReportId=" & menReport)
            If row.Length > 0 Then
                Me.eZeeHeader.Title = row(0).Item("ReportName").ToString
            End If
            If menReport = enArutiReport.ITax_Form_B_Report Then
                lblDisabilityMembership.Visible = False
                cboDisabilityMembership.Visible = False
            End If
            'Nilay (11 Apr 2017) -- End

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmITaxFormBReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            Dim strEndDate As String = (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriodName.Index).Tag.ToString)).Max

            'Hemant (07 Dec 2020) -- Start
            'Issue #AH-1841: Active Employee Missing on Itax Form B report
            Dim objPeriod As New clscommom_period_Tran
            Dim intPeriodId As Integer = (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).Min
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
            Dim dtStartDate As Date = objPeriod._Start_Date
            'Hemant (07 Dec 2020) -- End

            objiTaxFormB._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, strEndDate)
            'Sohail (03 Aug 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'If objiTaxFormB.Generate_PayrollReport(SaveDialog.FileName, _
            '                                       FinancialYear._Object._DatabaseName, _
            '                                       User._Object._Userunkid, _
            '                                       FinancialYear._Object._YearUnkid, _
            '                                       Company._Object._Companyunkid, _
            '                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                       ConfigParameter._Object._UserAccessModeSetting, _
            '                                       True, _
            '                                       ConfigParameter._Object._Base_CurrencyId) = True Then
            If objiTaxFormB.Generate_PayrollReport(SaveDialog.FileName, _
                                                   FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(strEndDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, _
                                                   ConfigParameter._Object._Base_CurrencyId, _
                                                   dtStartDate) = True Then
                'Hemant (07 Dec 2020) -- [dtStartDate]
                'Sohail (03 Aug 2019) -- End
                'If objiTaxFormB.Generate_PayrollReport(SaveDialog.FileName) = True Then
                'S.SANDEEP [04 JUN 2015] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsITaxFormBReport.SetMessages()
            objfrm._Other_ModuleNames = "clsITaxFormBReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter(True) = False Then Exit Try

            'Sohail (08 Mar 2018) -- Start
            'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
            'For intHeadType As Integer = 1 To 22
            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                'Sohail (08 Mar 2018) -- End
                'Sohail (02 Feb 2018) - [For intHeadType As Integer = 1 To 19]
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.ITax_Form_B_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.HouseAllowance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboHouseAllow.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TransportAllowance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTransAllow.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.LeavePay
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLeavePay.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OverTimeAllowance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOverTimeAllow.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.DirectorsFee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDirectorFee.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.LumpSum
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLumpSum.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Other_Earning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherEarning.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                        'Sohail (11 Sep 2015) -- Start
                        'Enhancement - Provided mapping for Y column (Actual Actribution) and column AF (Monthly Pesonal Relief) in iTax Form B Report for Kenya as per comment of Rutta in email Sub.:Aga Khan Foundation - Mandatory iTax Filing on 08-Sep-2015.
                    Case enHeadTypeId.ActualContribution
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboActualContribution.SelectedValue.ToString

                    Case enHeadTypeId.MonthlyRelief
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMonthlyRelief.SelectedValue.ToString
                        'Sohail (11 Sep 2015) -- End

                        'Sohail (03 Apr 2017) -- Start
                        'CCK Enhancement - 65.1 - Disability Membership mappings for column C on I Tax Form B Report.
                    Case enHeadTypeId.Resident
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboResident.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.NonResident
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNonResident.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OwnedHouseValue
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployerOwnedHouseValue.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.RentedHouseValue
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployerRentedHouseValue.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OwnedHouse
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployerOwnedHouse.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.RentedHouse
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployerRentedHouse.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Column_S
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboColumnS.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Column_AB
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboColumnAB.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.DisabilityMembership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDisabilityMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)
                        'Sohail (03 Apr 2017) -- End

                        'Sohail (02 Feb 2018) -- Start
                        'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
                    Case enHeadTypeId.Column_N
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboColumnN.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Column_AA
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboColumnAA.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Column_AG
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboColumnAG.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)
                        'Sohail (02 Feb 2018) -- End

                        'Sohail (08 Mar 2018) -- Start
                        'CCK Enhancement - Ref. # 177 : Provide mapping of this column AI and dropdown should have PAYE heads only.Report heading should be PAYE on I Tax Form B and C report in 70.1.
                    Case enHeadTypeId.Column_AI
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboColumnAI.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)
                        'Sohail (08 Mar 2018) -- End

                        'Hemant (17 Nov 2020) -- Start
                        'Enhancement # OLD-204 : 1) By default, Ignore Zero Values on Itax Form B report. Employees with zero values all through (or Unprocessed employees)  should NOT appear on report at all. 
                    Case enHeadTypeId.Ignore_Zero
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkIgnoreZero.Checked.ToString()

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_B_Report, 0, 0, intHeadType)
                        'Hemant (17 Nov 2020) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End



                Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
                dtTable.Rows.Clear()
            End If

            lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("periodunkid")
                lvItem.SubItems(colhPeriodName.Index).Tag = dtRow.Item("end_date").ToString
                lvPeriod.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvPeriod.GridLines = False

            If lvPeriod.Items.Count > 6 Then
                colhPeriodName.Width = 205 - 18
            Else
                colhPeriodName.Width = 205
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboColumnF_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboHouseAllow.Validating, cboTransAllow.Validating, _
                                                                                                                        cboLeavePay.Validating, cboOverTimeAllow.Validating, _
                                                                                                                        cboDirectorFee.Validating, cboLumpSum.Validating
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboMembership.Name AndAlso t.Name <> cboDisabilityMembership.Name AndAlso t.Name <> cboSlabEffectivePeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue)) 'Sohail (03 Apr 2017) - [cboDisabilityMembership]
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboColumnF_Validating", mstrModuleName)
        End Try
    End Sub

    'Nilay (17 Feb 2017) -- Start
    'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
    Private Sub cboResident_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboResident.SelectedIndexChanged
        Try
            If CInt(cboResident.SelectedValue) > 0 Then
                If CInt(cboResident.SelectedValue) = CInt(cboNonResident.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Resident and Non Resident cannot be the same. Please select different in either Resident or Non Resident."), enMsgBoxStyle.Information)
                    cboResident.SelectedValue = 0
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboResident_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNonResident_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNonResident.SelectedIndexChanged
        Try
            If CInt(cboNonResident.SelectedValue) > 0 Then
                If CInt(cboNonResident.SelectedValue) = CInt(cboResident.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Resident and Non Resident cannot be the same. Please select different in either Resident or Non Resident."), enMsgBoxStyle.Information)
                    cboNonResident.SelectedValue = 0
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNonResident_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployerOwnedHouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployerOwnedHouse.SelectedIndexChanged
        Try
            If CInt(cboEmployerOwnedHouse.SelectedValue) > 0 Then
                If CInt(cboEmployerOwnedHouse.SelectedValue) = CInt(cboEmployerRentedHouse.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Employer's owned house and rented house cannot be the same. Please select different in either Employer's owned house or rented house."), enMsgBoxStyle.Information)
                    cboEmployerOwnedHouse.SelectedValue = 0
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployerOwnedHouse_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployerRentedHouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployerRentedHouse.SelectedIndexChanged
        Try
            If CInt(cboEmployerRentedHouse.SelectedValue) > 0 Then
                If CInt(cboEmployerRentedHouse.SelectedValue) = CInt(cboEmployerOwnedHouse.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Employer's owned house and rented house cannot be the same. Please select different in either Employer's owned house or rented house."), enMsgBoxStyle.Information)
                    cboEmployerRentedHouse.SelectedValue = 0
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployerRentedHouse_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (17 Feb 2017) -- End

#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call CheckAll(lvPeriod, objchkAllPeriod.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Feb 2018) -- Start
    'CCK Enhancement - Ref. # 165 : Providing head mapping for Column N, AA and AG on I Tax Form B and C report in 70.1.
    Private Sub gbFilterCriteria_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles gbFilterCriteria.Scroll
        Try
            gbFilterCriteria.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbFilterCriteria_Scroll", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Feb 2018) -- End

#End Region




    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lblHouseAllow.Text = Language._Object.getCaption(Me.lblHouseAllow.Name, Me.lblHouseAllow.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblLumpSum.Text = Language._Object.getCaption(Me.lblLumpSum.Name, Me.lblLumpSum.Text)
			Me.lblDirectorFee.Text = Language._Object.getCaption(Me.lblDirectorFee.Name, Me.lblDirectorFee.Text)
			Me.lblOverTimeAllow.Text = Language._Object.getCaption(Me.lblOverTimeAllow.Name, Me.lblOverTimeAllow.Text)
			Me.lblLeavePay.Text = Language._Object.getCaption(Me.lblLeavePay.Name, Me.lblLeavePay.Text)
			Me.lblTransAllow.Text = Language._Object.getCaption(Me.lblTransAllow.Name, Me.lblTransAllow.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblActualContribution.Text = Language._Object.getCaption(Me.lblActualContribution.Name, Me.lblActualContribution.Text)
			Me.lblMonthlyRelief.Text = Language._Object.getCaption(Me.lblMonthlyRelief.Name, Me.lblMonthlyRelief.Text)
			Me.lblResident.Text = Language._Object.getCaption(Me.lblResident.Name, Me.lblResident.Text)
			Me.lblNonResident.Text = Language._Object.getCaption(Me.lblNonResident.Name, Me.lblNonResident.Text)
			Me.lblEmployerOwnedHouseValue.Text = Language._Object.getCaption(Me.lblEmployerOwnedHouseValue.Name, Me.lblEmployerOwnedHouseValue.Text)
			Me.lblEmployerRentedHouseValue.Text = Language._Object.getCaption(Me.lblEmployerRentedHouseValue.Name, Me.lblEmployerRentedHouseValue.Text)
			Me.lblEmployerRentedHouse.Text = Language._Object.getCaption(Me.lblEmployerRentedHouse.Name, Me.lblEmployerRentedHouse.Text)
			Me.lblEmployerOwnedHouse.Text = Language._Object.getCaption(Me.lblEmployerOwnedHouse.Name, Me.lblEmployerOwnedHouse.Text)
			Me.lblColumnAB.Text = Language._Object.getCaption(Me.lblColumnAB.Name, Me.lblColumnAB.Text)
			Me.lblColumnS.Text = Language._Object.getCaption(Me.lblColumnS.Name, Me.lblColumnS.Text)
			Me.lblDisabilityMembership.Text = Language._Object.getCaption(Me.lblDisabilityMembership.Name, Me.lblDisabilityMembership.Text)
			Me.lblColumnAG.Text = Language._Object.getCaption(Me.lblColumnAG.Name, Me.lblColumnAG.Text)
			Me.lblColumnN.Text = Language._Object.getCaption(Me.lblColumnN.Name, Me.lblColumnN.Text)
			Me.lblColumnAA.Text = Language._Object.getCaption(Me.lblColumnAA.Name, Me.lblColumnAA.Text)
			Me.lblColumnAI.Text = Language._Object.getCaption(Me.lblColumnAI.Name, Me.lblColumnAI.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Effective Period is mandatory information. Please select Effective Period.")
			Language.setMessage(mstrModuleName, 2, "Please check Periods to view report.")
			Language.setMessage(mstrModuleName, 3, "Please select transaction head.")
			Language.setMessage(mstrModuleName, 4, "Data Exported Successfuly.")
			Language.setMessage(mstrModuleName, 5, "Sorry, This transaction head is already mapped.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Resident and Non Resident cannot be the same. Please select different in either Resident or Non Resident.")
			Language.setMessage(mstrModuleName, 8, "Employer's owned house and rented house cannot be the same. Please select different in either Employer's owned house or rented house.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
