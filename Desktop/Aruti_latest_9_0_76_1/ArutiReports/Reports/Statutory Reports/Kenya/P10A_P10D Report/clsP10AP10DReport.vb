'************************************************************************************************************************************
'Class Name : clsP10DReport.vb
'Purpose    :
'Date       :07/12/2015
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsP10AP10DReport

    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsP10AP10DReport"
    Private mstrReportId As String = enArutiReport.P10A_P10D_Report

    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintHousingTranId As Integer = 0
    Private mstrStatutoryIds As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mstrLastPeriodYearName As String = String.Empty
    Private mintPeriodCount As Integer = -1
    Private mintPayeHeadId As Integer = 0
    Private mstrPayeHeadName As String = ""
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = ""
    Private mintOtherEarningTranId As Integer = 0
    Private mintFringeBenefitHeadId As Integer = 0
    Private mstrFringeBenefiHeadName As String = ""
    Private mintTaxOnHeadId As Integer = 0
    Private mstrTaxOnHeadName As String = ""

    Private mblnIncludeInactiveEmp As Boolean = True
    Private mdtSlabDate As Date

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Private mintReportTypeId As Integer = 0
    Private mdecTotBasic, mdecTotTaxOn, mdecTotAllowance, mdecTotGross, mdecTotFringeBenefit, mdecTotTaxable, mdecTotTaxDue As Decimal
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrReportTypeName As String = ""
    'Nilay (15-Dec-2015) -- End

    'Shani(22-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Private mstrBaseCurrencySign10A As String = ""
    'Shani(22-Dec-2015) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _StatutoryIds() As String
        Set(ByVal value As String)
            mstrStatutoryIds = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadId() As Integer
        Set(ByVal value As Integer)
            mintPayeHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrPayeHeadName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _FringeBenefitHeadId() As Integer
        Set(ByVal value As Integer)
            mintFringeBenefitHeadId = value
        End Set
    End Property

    Public WriteOnly Property _FringeBenefitHeadName() As String
        Set(ByVal value As String)
            mstrFringeBenefiHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TaxOnHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxOnHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TaxOnHeadName() As String
        Set(ByVal value As String)
            mstrTaxOnHeadName = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _LastPeriodYearName() As String
        Set(ByVal value As String)
            mstrLastPeriodYearName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtSlabDate = value
        End Set
    End Property

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Nilay (15-Dec-2015) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mintHousingTranId = 0
            mstrStatutoryIds = ""
            mstrPeriodIds = ""
            mintPayeHeadId = 0
            mstrPayeHeadName = ""
            mstrLastPeriodYearName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintOtherEarningTranId = 0
            mintFringeBenefitHeadId = 0
            mstrFringeBenefiHeadName = ""
            mintTaxOnHeadId = 0
            mstrTaxOnHeadName = ""
            mblnIncludeInactiveEmp = True
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            mstrReportTypeName = ""
            'Nilay (15-Dec-2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@payetranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayeHeadId)

            If mintMembershipId > 0 Then
                Me._FilterTitle = "Membership : " & mstrMembershipName

                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            End If

            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'If mintFringeBenefitHeadId > 0 Then
            '    objDataOperation.AddParameter("@fringebenefitheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFringeBenefitHeadId)
            'End If

            'If mintTaxOnHeadId > 0 Then
            '    objDataOperation.AddParameter("@taxonheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxOnHeadId)
            'End If
            objDataOperation.AddParameter("@fringebenefitheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFringeBenefitHeadId)
            objDataOperation.AddParameter("@taxonheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxOnHeadId)
            'Nilay (15-Dec-2015) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        '    Dim strReportExportFile As String = ""

        '    Try

        '        objRpt = Generate_DetailReport()

        '        If Not IsNothing(objRpt) Then
        '            Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '        End If
        '    Catch ex As Exception
        '        Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        '    End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xdatabasename As String, _
                                           ByVal xuserunkid As Integer, _
                                           ByVal xyearunkid As Integer, _
                                           ByVal xcompanyunkid As Integer, _
                                           ByVal xperiodstart As Date, _
                                           ByVal xperiodend As Date, _
                                           ByVal xusermodesetting As String, _
                                           ByVal xonlyapproved As Boolean, _
                                           ByVal xexportreportpath As String, _
                                           ByVal xopenreportafterexport As Boolean, _
                                           ByVal printreporttype As Integer, _
                                           Optional ByVal printaction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal exportaction As enExportAction = enExportAction.None, _
                                           Optional ByVal xbasecurrencyid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try
            menExportAction = exportaction
            If mintReportTypeId = 1 Then
                objRpt = Generate_DetailReport_P10A(xdatabasename, xuserunkid, xyearunkid, xcompanyunkid, _
                                               xperiodstart, xperiodend, xusermodesetting, xonlyapproved)

                If Not IsNothing(objRpt) Then

                    'Shani(22-Dec-2015) -- Start
                    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                    Dim intArrayColumnWidth As Integer() = {80, 175, 115, 115}
                    Dim rowsArrayHeader As New ArrayList
                    Dim rowsArrayFooter As New ArrayList
                    Dim row As ExcelWriter.WorksheetRow
                    Dim wcell As WorksheetCell

                    If mdtTableExcel IsNot Nothing Then
                        Dim objCompany As New clsCompany_Master
                        objCompany._Companyunkid = xcompanyunkid
                        If objCompany._Countryunkid = 112 Then 'Kenya
                            mstrReportTypeName = Me._ReportName

                            '-------------------Header Start---------------------------------
                            '--------------------Row1---------------

                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "KENYA REVENUE AUTHORITY"), "s12bw")
                            wcell.MergeAcross = 1
                            wcell.MergeDown = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row2---------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row3---------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 102, "Income Tax Department"), "s9bcw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row4---------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 101, "P.10A"), "s10bw")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row5--------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 103, "P.A.Y.E SUPPORTING LIST FOR END OF YEAR CERTIFICATE: YEAR ") & " " & mstrPeriodNames, "s10bw")
                            wcell.MergeAcross = 3
                            wcell.MergeDown = 1
                            row.Cells.Add(wcell)
                            '--------------------Row6--------------------------
                            rowsArrayHeader.Add(row)

                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row7--------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 104, "PIN"), "s9bcw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row8--------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Company._Object._Tinno, "s8cw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row9--------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 105, "EMPLOYER'S NAME"), "s9bw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Company._Object._Name, "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row9--------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '-------------------Header End---------------------------------

                            '-------------------Footer Start--------------------------------
                            '-------------------FRow1--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 108, "TOTAL EMOLUMENTS"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotGross, GUI.fmtCurrency), "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "n8")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow2--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 301, "TOTAL PAY TAX"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotTaxDue, GUI.fmtCurrency), "s8")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow3--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "TOTAL WCPS"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow4--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 113, "*TAX ON LUMPSUM/AUDIT/INTEREST/PENALTY"), "s8b")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotTaxOn, GUI.fmtCurrency), "s8")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow5--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)


                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 114, "*TOTAL TAX DEDUCTED/TOTAL C/F TO NEXT LIST"), "s8b")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow6--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 115, "*DELETE AS APPROPRIATE"), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow7--------------------------------

                            '-------------------FRow8--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 116, "NOTE TO EMPLOYER:"), "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 117, "ATTACH TWO COPIES OF THIS LIST TO END OF YEAR CERTIFICATE (P10)"), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow9--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 118, "INFORMATION REQUIRED FROM EMPLOYER AT END OF YEAR"), "s9bw")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow9--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 119, "(1)	Date employee connected if during year"), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow10--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 120, "Name and address of old employer"), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow11--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 121, "(2)    Date left if during year"), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow12--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 122, "Name and address of new employer"), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow13--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 123, "(3)    Where housing is provided, state monthly rent: "), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow13--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 124, "Charged ") & " " & mstrBaseCurrencySign10A, "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 125, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 126, " per month"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow14--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 127, "(4)    Where any of the pay relates to a period othen than this year, e.g. gratuity,"), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow15--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 128, "Give details of Amounts, Year and Tax."), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow16--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 129, "YEAR"), "s9b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 134, "Amount"), "s9b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow16--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 135, "Pound"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(mstrBaseCurrencySign10A, "s8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow17--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 130, "20"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 137, ""), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 141, ""), "s8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow18--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 131, "20"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 138, ""), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 142, ""), "s8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow19--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 132, "20"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 139, ""), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 143, ""), "s8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow20--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 133, "20"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 140, ""), "s9b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 144, ""), "s9b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow21--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 145, "FOR MONTHLY RATES OF BENEFITS REFER TO EMPLOYERS' GUIDE TO P.A.Y.E. SYSTEM - P7."), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow22--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 146, "CALCULATIONS OF TAX ON BENEFITS"), "s9bcw")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow23--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 147, "BENEFIT"), "s8BBc")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 148, "NO"), "s8BBc")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 149, "RATE"), "s8BBc")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 150, "NO. OF MONTHS"), "s8BBc")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 151, "TOTAL AMOUNT"), "s8BBc")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow24--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 152, "COOK/HSE. SERVANT"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 163, "") & " " & Language.getMessage(mstrModuleName, 171, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 179, "2250"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 189, "x") & "  " & Language.getMessage(mstrModuleName, 199, "12") & " " & Language.getMessage(mstrModuleName, 291, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 209, "27000"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow25--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 153, "GARDNER"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 164, "") & " " & Language.getMessage(mstrModuleName, 172, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 180, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 190, "x") & "  " & Language.getMessage(mstrModuleName, 200, "") & " " & Language.getMessage(mstrModuleName, 292, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 210, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow26--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 154, "AYAH"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 165, "") & " " & Language.getMessage(mstrModuleName, 173, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 181, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 191, "x") & "  " & Language.getMessage(mstrModuleName, 201, "") & " " & Language.getMessage(mstrModuleName, 293, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 211, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow27--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 155, "WATCHMAN(D)"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 166, "") & " " & Language.getMessage(mstrModuleName, 174, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 182, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 192, "x") & "  " & Language.getMessage(mstrModuleName, 202, "") & " " & Language.getMessage(mstrModuleName, 294, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 212, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow28--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 156, "WATCHMAN(N)"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 167, "") & " " & Language.getMessage(mstrModuleName, 175, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 183, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 193, "x") & "  " & Language.getMessage(mstrModuleName, 203, "") & " " & Language.getMessage(mstrModuleName, 295, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 213, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow29--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 157, "FURNITURE"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 168, "") & " " & Language.getMessage(mstrModuleName, 176, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 184, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 194, "x") & "  " & Language.getMessage(mstrModuleName, 204, "") & " " & Language.getMessage(mstrModuleName, 296, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 214, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow30--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 158, "WATER"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 169, "") & " " & Language.getMessage(mstrModuleName, 177, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 185, "500"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 195, "x") & "  " & Language.getMessage(mstrModuleName, 205, "12") & " " & Language.getMessage(mstrModuleName, 297, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 215, "6000"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow31--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 159, "TELEPHONE"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 170, "") & " " & Language.getMessage(mstrModuleName, 178, "x"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 186, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 196, "x") & "  " & Language.getMessage(mstrModuleName, 206, "") & " " & Language.getMessage(mstrModuleName, 298, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 216, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow32--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 160, "ELECTRICITY"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 187, "1500"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 197, "X") & "  " & Language.getMessage(mstrModuleName, 207, "12") & " " & Language.getMessage(mstrModuleName, 299, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 217, "18000"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow33--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 161, "SEC. SYST."), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 188, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 198, "X") & "  " & Language.getMessage(mstrModuleName, 208, "") & " " & Language.getMessage(mstrModuleName, 300, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 218, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow34--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 219, "Where actual cost is higher than given monthly rates of benefits then the actual cost is brought to charge in full."), "s8w")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow35--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 220, "LOW INTEREST RATE BELOW PRESCRIBED RATE OF INTEREST."), "s8bw")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow36--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 221, "EMPLOYERS LOAN"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 222, "=") & Language.getMessage(mstrModuleName, 223, "Kshs."), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 224, "") & Language.getMessage(mstrModuleName, 225, "@"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 226, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 227, "RATE"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow36--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 228, "RATE DIFFERENCE"), "s8w")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow37--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 229, "(PRESCRIBED RATE - EMPLOYERS RATE)="), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 230, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 231, "%"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow38--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 232, "MONTHLY BENEFIT (RATE DIFFERENCE x LOAN) ="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 233, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 234, "%") & Language.getMessage(mstrModuleName, 235, " X ") & Language.getMessage(mstrModuleName, 236, "Kshs. "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 237, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 238, "="), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow39--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 239, "12"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 240, "12"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow40--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 240, "MOTOR CARS"), "s8w")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow41--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 242, "Up to 15000 c.c."), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 243, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 244, "X  "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 245, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 246, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow42--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 249, "1501 c.c. - "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 250, "1750 c.c. - "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 251, "10750x12"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 252, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 253, "129000"), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow43--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 255, "1751 c.c. - "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 256, "2000 c.c. - "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 257, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 258, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 259, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow44--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 260, "2001 c.c. - "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 261, "3000 c.c. - "), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 262, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 263, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 264, ""), "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow45--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 265, "Over 3000 c.c."), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 266, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 267, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 268, ""), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow46--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 269, "Total Benefit in Year"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 270, ""), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 271, "="), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 272, "180000"), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow47--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 274, "If this amount doeas not agree with total of Col. B overleaf, attach explanation."), "s8w")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow48--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 275, "FOR PICK-UPS, PANEL VANS AND LAND-ROVERS REFER TO APPENDIX 5 OF EMPLOYER'S GUIDE."), "s8w")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow49--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 276, "CAR BENEFIT - The higher amount of the fixed monthly rate or the prescribed rate of benefits is to be brought to charge:-"), "s8w")
                            wcell.MergeAcross = 4
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow50--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 277, "PRESCRIBED RATE:- "), "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 278, "1996 - 1% per month of the initial cost of the vehicle."), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow51--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 279, "1997 - 1.5% per month of the initial cost of the vehicle."), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow52--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 280, "1998 - 2% per month of the initial cost of the vehicle."), "s8w")
                            wcell.MergeAcross = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow53--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 281, "EMPLOYERS CERTIFICATE OF PAY AND TAX"), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow54--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 282, "NAME"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 283, ""), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow55--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 284, "ADDRESS"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 285, ""), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow56--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 286, "SIGNATURE"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 287, ""), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow57--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 288, "DATE & STAMP"), "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 289, ""), "s8w")
                            wcell.MergeAcross = 3
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow58--------------------------------
                            row = New WorksheetRow()
                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 290, "NOTE: Employer's certificate to be signed by the person who prepares and submits to the PAYE End of Year Returns and copy of the P9A be issued to the employee in January."), "s8bw")
                            wcell.MergeAcross = 4
                            wcell.MergeDown = 2
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------Footer End--------------------------------
                        End If
                    End If
                    'Shani(22-Dec-2015) -- End
                    Call ReportExecute(objRpt, printaction, exportaction, xexportreportpath, xopenreportafterexport, _
                                       mdtTableExcel, intArrayColumnWidth, False, True, False, Nothing, mstrReportTypeName, "", "", Nothing, "", False, _
                                       rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, Nothing)


                End If

            ElseIf mintReportTypeId = 2 Then
                objRpt = Generate_DetailReport_P10D(xdatabasename, xuserunkid, xyearunkid, xcompanyunkid, _
                                                xperiodstart, xperiodend, xusermodesetting, xonlyapproved)

                If Not IsNothing(objRpt) Then
                    Dim intArrayColumnWidth As Integer() = {80, 175, 115, 115}
                    Dim rowsArrayHeader As New ArrayList
                    Dim rowsArrayFooter As New ArrayList
                    Dim row As ExcelWriter.WorksheetRow
                    Dim wcell As WorksheetCell

                    If mdtTableExcel IsNot Nothing Then
                        Dim objCompany As New clsCompany_Master
                        objCompany._Companyunkid = xcompanyunkid
                        If objCompany._Countryunkid = 112 Then 'Kenya
                            mstrReportTypeName = Me._ReportName
                            '--------------------Row1---------------
                            row = New WorksheetRow()

                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "KENYA REVENUE AUTHORITY"), "s12bw")
                            wcell.MergeAcross = 1
                            wcell.MergeDown = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row2---------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row3---------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "P.10D"), "s10bw")
                            'wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT"), "s9bcw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row4--------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            'wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "EMPLOYER’S QUARTERLY RETURN"), "s9bcw")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '--------------------Row5--------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "P.A.Y.E  RETURN FOR THE QUARTER ENDING") & " " & mstrPeriodNames & " " & "/" & " " & Language.getMessage(mstrModuleName, 8, "Year") & " " & mstrLastPeriodYearName, "s10bw")
                            wcell.MergeAcross = 3
                            wcell.MergeDown = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '------------------Row6---------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '------------------Row7-----------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            'wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "EMPLOYER'S PIN"), "s9bw")
                            'wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Company._Object._Tinno, "s8w")
                            'wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '------------------Row8--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "EMPLOYER'S NAME"), "s9bw")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Company._Object._Name, "s8w")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '------------------Row9-----------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayHeader.Add(row)
                            '-------------------Footer---------------------------------
                            '-------------------FRow1--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "BANK OF REMMITANCE"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "TOTAL PAYE TAX"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotTaxDue, GUI.fmtCurrency), "n8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow2--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "TOTAL WCPS"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "n8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow3--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "FRINGE BENEFIT TAX"), "s8b")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotFringeBenefit, GUI.fmtCurrency), "n8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow4--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "TAX ON LUMPSUMS/AUDIT/PENALTY/INTEREST"), "s8b")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotTaxOn, GUI.fmtCurrency), "n8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow5--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8")
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "TOTAL TAX PAID FOR THE QUARTER"), "s8b")
                            wcell.MergeAcross = 1
                            row.Cells.Add(wcell)

                            wcell = New WorksheetCell(Format(mdecTotTaxDue + mdecTotFringeBenefit + mdecTotTaxOn, GUI.fmtCurrency), "n8b")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)

                            '-------------------FRow6--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell("", "s8w")
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                            '-------------------FRow7--------------------------------
                            row = New WorksheetRow()

                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "NB:KRA SHALL NOT ACCEPT ANY  LIABILITY FOR LOSS OR DAMAGE INCURRED AS A RESULT OF ALTERATION OF THIS FORM"), "s8bw")
                            wcell.MergeAcross = 3
                            wcell.MergeDown = 1
                            row.Cells.Add(wcell)

                            rowsArrayFooter.Add(row)
                        End If
                    End If

                    Call ReportExecute(objRpt, printaction, exportaction, xexportreportpath, xopenreportafterexport, _
                                       mdtTableExcel, intArrayColumnWidth, False, True, False, Nothing, mstrReportTypeName, "", "", Nothing, "", False, _
                                       rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, Nothing)

                End If

            End If



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Nilay (15-Dec-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region


#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Private Function Generate_DetailReport_P10D(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            StrQ = "SELECT   basicpay AS basicpay " & _
                          ", taxon AS taxon " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", basicpay + taxon + ( allowancebenefit ) AS Grosspay " & _
                          ", ( basicpay + taxon + ( allowancebenefit ) ) - FringeBenefit AS taxable " & _
                          ", FringeBenefit AS FringeBenefit " & _
                          ", taxpayable AS taxpaid " & _
                          ", empid AS empid " & _
                          ", hremployee_master.employeecode AS empcode " & _
                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno "

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            Else
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            End If

            StrQ &= "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
                                      ", SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
                                      ", SUM(ISNULL(PAYE.taxon, 0)) AS taxon " & _
                                      ", SUM(ISNULL(PAYE.FringeBenefit, 0)) AS FringeBenefit " & _
                                      ", SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
                                      ", SUM(ISNULL(PAYE.paye, 0)) AS taxpaid " & _
                                      ", paye.empid AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @taxonheadid "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    "AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
            End If

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @fringebenefitheadid "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                              "HAVING    SUM(ISNULL(PAYE.taxpayable, 0)) <> 0 " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid "

            If mintMembershipId > 0 Then
                StrQ &= "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            End If

            StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                    "WHERE   1 = 1 " & _
                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 "


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            Else
                StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND taxon = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND FringeBenefit = 0 AND taxable = 0 AND taxpaid = 0")

            If dtTemp.Length > 0 Then
                For i As Integer = 0 To dtTemp.Length - 1
                    dsList.Tables(0).Rows.Remove(dtTemp(i))
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim intCnt As Integer = 1
            Dim intPrevEmp As Integer = 0

            mdecTotBasic = 0 : mdecTotTaxOn = 0 : mdecTotAllowance = 0 : mdecTotGross = 0 : mdecTotFringeBenefit = 0 : mdecTotTaxable = 0 : mdecTotTaxDue = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                If intPrevEmp = CInt(dtRow.Item("empid")) Then '*** To prevent Duplicate entries if employee has more than one Membership Assigned
                    intPrevEmp = CInt(dtRow.Item("empid"))
                    Continue For
                End If

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = intCnt
                rpt_Row.Item("Column2") = dtRow.Item("empname")
                rpt_Row.Item("Column3") = dtRow.Item("empcode")
                rpt_Row.Item("Column4") = dtRow.Item("membershipno")

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("basicpay")), GUI.fmtCurrency)
                rpt_Row.Item("Column82") = Format(CDec(dtRow.Item("taxon")), GUI.fmtCurrency)
                rpt_Row.Item("Column83") = Format(CDec(dtRow.Item("allowancebenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column84") = Format(CDec(dtRow.Item("Grosspay")), GUI.fmtCurrency)
                rpt_Row.Item("Column85") = Format(CDec(dtRow.Item("FringeBenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column86") = Format(CDec(dtRow.Item("taxable")), GUI.fmtCurrency)
                rpt_Row.Item("Column87") = Format(CDec(dtRow.Item("taxpaid")), GUI.fmtCurrency)

                mdecTotBasic = mdecTotBasic + CDec(dtRow.Item("basicpay"))
                mdecTotTaxOn = mdecTotTaxOn + CDec(dtRow.Item("taxon"))
                mdecTotAllowance = mdecTotAllowance + CDec(dtRow.Item("allowancebenefit"))
                mdecTotGross = mdecTotGross + CDec(dtRow.Item("Grosspay"))
                mdecTotFringeBenefit = mdecTotFringeBenefit + CDec(dtRow.Item("FringeBenefit"))
                mdecTotTaxable = mdecTotTaxable + CDec(dtRow.Item("taxable"))
                mdecTotTaxDue = mdecTotTaxDue + CDec(dtRow.Item("taxpaid"))

                intCnt += 1
                intPrevEmp = CInt(dtRow.Item("empid"))
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptKY_P10D

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 10, "KENYA REVENUE AUTHORITY"))
            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED"))
            ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 3, "P.10D"))
            ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblCaption4", Language.getMessage(mstrModuleName, 5, "EMPLOYER’S QUARTERLY RETURN"))
            ReportFunction.TextChange(objRpt, "lblQuarterEnding", Language.getMessage(mstrModuleName, 2, "P.A.Y.E  RETURN FOR THE QUARTER ENDING") & " " & mstrPeriodNames & " " & "/" & " " & Language.getMessage(mstrModuleName, 8, "Year") & " " & mstrLastPeriodYearName)
            ReportFunction.TextChange(objRpt, "lblEmployerPin", Language.getMessage(mstrModuleName, 7, "EMPLOYER'S PIN"))
            ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "EMPLOYER'S NAME"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)

            ReportFunction.TextChange(objRpt, "lblEmployeePIN", Language.getMessage(mstrModuleName, 14, "EMPLOYEE’S PIN"))
            ReportFunction.TextChange(objRpt, "lblEmployeeName", Language.getMessage(mstrModuleName, 15, "EMPLOYEE’S NAME"))
            ReportFunction.TextChange(objRpt, "lblTotalEmoluments", Language.getMessage(mstrModuleName, 16, "TOTAL EMOLUMENTS") & "  " & strBaseCurrencySign & ".")
            ReportFunction.TextChange(objRpt, "lblTotalPAYEDeducted", Language.getMessage(mstrModuleName, 18, "TOTAL PAYE DEDUCTED") & "  " & strBaseCurrencySign & ".")
            ReportFunction.TextChange(objRpt, "lblBankRemmitance", Language.getMessage(mstrModuleName, 19, "BANK OF REMMITANCE"))

            ReportFunction.TextChange(objRpt, "lblTotalPayeTax", Language.getMessage(mstrModuleName, 20, "TOTAL PAYE TAX"))
            ReportFunction.TextChange(objRpt, "txtTotalPayeTax", Format(mdecTotTaxDue, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblTotalWCPS", Language.getMessage(mstrModuleName, 21, "TOTAL WCPS"))
            ReportFunction.TextChange(objRpt, "txtTotalWCPS", "")

            ReportFunction.TextChange(objRpt, "lblFringeBenefitTax", Language.getMessage(mstrModuleName, 22, "FRINGE BENEFIT TAX"))
            ReportFunction.TextChange(objRpt, "txtFringeBenefitTax", Format(mdecTotFringeBenefit, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblTaxOn", Language.getMessage(mstrModuleName, 25, "TAX ON LUMPSUMS/AUDIT/PENALTY/INTEREST"))
            ReportFunction.TextChange(objRpt, "txtTaxOn", Format(mdecTotTaxOn, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblTotalTaxPaid", Language.getMessage(mstrModuleName, 26, "TOTAL TAX PAID FOR THE QUARTER"))
            ReportFunction.TextChange(objRpt, "txtTotalTaxPaid", Format(mdecTotTaxDue + mdecTotFringeBenefit + mdecTotTaxOn, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 23, "NB:KRA SHALL NOT ACCEPT ANY  LIABILITY FOR LOSS OR DAMAGE INCURRED AS A RESULT OF ALTERATION OF THIS FORM"))

            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables("ArutiTable")

                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 14, "EMPLOYEE’S PIN")
                intColIndex += 1
                mdtTableExcel.Columns("Column4").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 15, "EMPLOYEE’S NAME")
                intColIndex += 1
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 16, "TOTAL EMOLUMENTS") & "  " & strBaseCurrencySign & "."
                intColIndex += 1
                mdtTableExcel.Columns("Column84").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column87").Caption = Language.getMessage(mstrModuleName, 18, "TOTAL PAYE DEDUCTED") & "  " & strBaseCurrencySign & "."
                intColIndex += 1
                mdtTableExcel.Columns("Column87").SetOrdinal(intColIndex)

                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If




            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport_P10D; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim strBaseCurrencySign As String

    '    Try
    '        objDataOperation = New clsDataOperation

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        strBaseCurrencySign = objExchangeRate._Currency_Sign

    '        StrQ = "SELECT   basicpay AS basicpay " & _
    '                      ", taxon AS taxon " & _
    '                      ", allowancebenefit AS allowancebenefit " & _
    '                      ", basicpay + taxon + ( allowancebenefit ) AS Grosspay " & _
    '                      ", ( basicpay + taxon + ( allowancebenefit ) ) - FringeBenefit AS taxable " & _
    '                      ", FringeBenefit AS FringeBenefit " & _
    '                      ", taxpayable AS taxpaid " & _
    '                      ", empid AS empid " & _
    '                      ", hremployee_master.employeecode AS empcode " & _
    '                      ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno "

    '        If ConfigParameter._Object._FirstNamethenSurname = True Then
    '            StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
    '        Else
    '            StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
    '        End If


    '        StrQ &= "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '                                  ", SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
    '                                  ", SUM(ISNULL(PAYE.taxon, 0)) AS taxon " & _
    '                                  ", SUM(ISNULL(PAYE.FringeBenefit, 0)) AS FringeBenefit " & _
    '                                  ", SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
    '                                  ", SUM(ISNULL(PAYE.paye, 0)) AS taxpaid " & _
    '                                  ", paye.empid AS empid " & _
    '                          "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS taxon " & _
    '                                              ", 0 AS FringeBenefit " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

    '        If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '            StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
    '        Else
    '            StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '        End If

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxon " & _
    '                                              ", 0 AS FringeBenefit " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @taxonheadid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
    '                                              ", 0 AS taxon " & _
    '                                              ", 0 AS FringeBenefit " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '                                                "AND prtranhead_master.istaxable = 1 " & _
    '                                                "AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) "

    '        If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '            StrQ &= "AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
    '        Else
    '            StrQ &= "AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
    '        End If


    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If


    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS taxon " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS FringeBenefit " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
    '                                                "AND prtranhead_master.tranheadunkid = @fringebenefitheadid "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS taxon " & _
    '                                              ", 0 AS FringeBenefit " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        StrQ &= "                      ) AS PAYE " & _
    '                          "WHERE     1 = 1 " & _
    '                          "GROUP BY  empid " & _
    '                          "HAVING    SUM(ISNULL(PAYE.taxpayable, 0)) <> 0 " & _
    '                        ") AS A " & _
    '                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
    '                        "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid "

    '        If mintMembershipId > 0 Then
    '            StrQ &= "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
    '        End If

    '        StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "WHERE   1 = 1 " & _
    '                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 "


    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery

    '        If ConfigParameter._Object._FirstNamethenSurname = True Then
    '            StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
    '        Else
    '            StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
    '        End If

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND taxon = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND FringeBenefit = 0 AND taxable = 0 AND taxpaid = 0")

    '        If dtTemp.Length > 0 Then
    '            For i As Integer = 0 To dtTemp.Length - 1
    '                dsList.Tables(0).Rows.Remove(dtTemp(i))
    '            Next
    '            dsList.Tables(0).AcceptChanges()
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim rpt_Row As DataRow = Nothing
    '        Dim intCnt As Integer = 1
    '        Dim intPrevEmp As Integer = 0

    '        Dim decTotBasic, decTotTaxOn, decTotAllowance, decTotGross, decTotFringeBenefit, decTotTaxable, decTotTaxDue As Decimal
    '        decTotBasic = 0 : decTotTaxOn = 0 : decTotAllowance = 0 : decTotGross = 0 : decTotFringeBenefit = 0 : decTotTaxable = 0 : decTotTaxDue = 0

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            If intPrevEmp = CInt(dtRow.Item("empid")) Then '*** To prevent Duplicate entries if employee has more than one Membership Assigned
    '                intPrevEmp = CInt(dtRow.Item("empid"))
    '                Continue For
    '            End If


    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = intCnt
    '            rpt_Row.Item("Column2") = dtRow.Item("empname")
    '            rpt_Row.Item("Column3") = dtRow.Item("empcode")
    '            rpt_Row.Item("Column4") = dtRow.Item("membershipno")

    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("basicpay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("taxon")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("allowancebenefit")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Grosspay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("FringeBenefit")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("taxable")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("taxpaid")), GUI.fmtCurrency)

    '            decTotBasic = decTotBasic + CDec(dtRow.Item("basicpay"))
    '            decTotTaxOn = decTotTaxOn + CDec(dtRow.Item("taxon"))
    '            decTotAllowance = decTotAllowance + CDec(dtRow.Item("allowancebenefit"))
    '            decTotGross = decTotGross + CDec(dtRow.Item("Grosspay"))
    '            decTotFringeBenefit = decTotFringeBenefit + CDec(dtRow.Item("FringeBenefit"))
    '            decTotTaxable = decTotTaxable + CDec(dtRow.Item("taxable"))
    '            decTotTaxDue = decTotTaxDue + CDec(dtRow.Item("taxpaid"))


    '            intCnt += 1
    '            intPrevEmp = CInt(dtRow.Item("empid"))
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptKY_P10D

    '        objRpt.SetDataSource(rpt_Data)

    '        ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED"))
    '        ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 3, "P.10D"))
    '        ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT"))
    '        ReportFunction.TextChange(objRpt, "lblCaption4", Language.getMessage(mstrModuleName, 5, "EMPLOYER’S QUARTERLY RETURN"))
    '        ReportFunction.TextChange(objRpt, "lblQuarterEnding", Language.getMessage(mstrModuleName, 2, "P.A.Y.E  RETURN FOR THE QUARTER ENDING") & " " & mstrPeriodNames & " " & "/" & " " & Language.getMessage(mstrModuleName, 123, "Year" & " " & mstrLastPeriodYearName))
    '        ReportFunction.TextChange(objRpt, "lblEmployerPin", Language.getMessage(mstrModuleName, 7, "EMPLOYER'S PIN"))
    '        ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "NAME OF EMPLOYER "))
    '        ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)


    '        ReportFunction.TextChange(objRpt, "lblEmployeePIN", Language.getMessage(mstrModuleName, 14, "EMPLOYEE’S PIN"))
    '        ReportFunction.TextChange(objRpt, "lblEmployeeName", Language.getMessage(mstrModuleName, 15, "EMPLOYEE’S NAME"))
    '        ReportFunction.TextChange(objRpt, "lblTotalEmoluments", Language.getMessage(mstrModuleName, 16, "TOTAL EMOLUMENTS") & "  " & strBaseCurrencySign & ".")
    '        ReportFunction.TextChange(objRpt, "lblTotalPAYEDeducted", Language.getMessage(mstrModuleName, 18, "TOTAL PAYE DEDUCTED") & "  " & strBaseCurrencySign & ".")
    '        ReportFunction.TextChange(objRpt, "lblBankRemmitance", Language.getMessage(mstrModuleName, 19, "BANK OF REMMITANCE"))

    '        ReportFunction.TextChange(objRpt, "lblTotalPayeTax", Language.getMessage(mstrModuleName, 20, "TOTAL PAYE TAX"))
    '        ReportFunction.TextChange(objRpt, "txtTotalPayeTax", Format(decTotTaxDue, GUI.fmtCurrency))

    '        ReportFunction.TextChange(objRpt, "lblTotalWCPS", Language.getMessage(mstrModuleName, 21, "TOTAL WCPS"))
    '        ReportFunction.TextChange(objRpt, "txtTotalWCPS", "")

    '        ReportFunction.TextChange(objRpt, "lblFringeBenefitTax", Language.getMessage(mstrModuleName, 22, "FRINGE BENEFIT TAX"))
    '        ReportFunction.TextChange(objRpt, "txtFringeBenefitTax", Format(decTotFringeBenefit, GUI.fmtCurrency))

    '        ReportFunction.TextChange(objRpt, "lblTaxOn", Language.getMessage(mstrModuleName, 25, "TAX ON LUMPSUMS/AUDIT/PENALTY/INTEREST"))
    '        ReportFunction.TextChange(objRpt, "txtTaxOn", Format(decTotTaxOn, GUI.fmtCurrency))

    '        ReportFunction.TextChange(objRpt, "lblTotalTaxPaid", Language.getMessage(mstrModuleName, 25, "TOTAL TAX PAID FOR THE QUARTER"))
    '        ReportFunction.TextChange(objRpt, "txtTotalTaxPaid", Format(decTotTaxDue + decTotFringeBenefit + decTotTaxOn, GUI.fmtCurrency))

    '        ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 23, "NB:KRA SHALL NOT ACCEPT ANY  LIABILITY FOR LOSS OR DAMAGE INCURRED AS A RESULT OF ALTERATION OF THIS FORM"))


    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport_P10A(ByVal xDatabaseName As String, _
                                                ByVal xUserUnkid As Integer, _
                                                ByVal xYearUnkid As Integer, _
                                                ByVal xCompanyUnkid As Integer, _
                                                ByVal xPeriodStart As DateTime, _
                                                ByVal xPeriodEnd As DateTime, _
                                                ByVal xUserModeSetting As String, _
                                                ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            'Shani(22-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            mstrBaseCurrencySign10A = strBaseCurrencySign
            'Shani(22-Dec-2015) -- End

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            StrQ = "SELECT   basicpay AS basicpay " & _
                          ", taxon AS taxon " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", basicpay + taxon + ( allowancebenefit ) AS Grosspay " & _
                          ", ( basicpay + taxon + ( allowancebenefit ) ) - FringeBenefit AS taxable " & _
                          ", FringeBenefit AS FringeBenefit " & _
                          ", taxpayable AS taxpaid " & _
                          ", empid AS empid " & _
                          ", hremployee_master.employeecode AS empcode " & _
                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno "

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            Else
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            End If

            StrQ &= "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
                                      ", SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
                                      ", SUM(ISNULL(PAYE.taxon, 0)) AS taxon " & _
                                      ", SUM(ISNULL(PAYE.FringeBenefit, 0)) AS FringeBenefit " & _
                                      ", SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
                                      ", SUM(ISNULL(PAYE.paye, 0)) AS taxpaid " & _
                                      ", paye.empid AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @taxonheadid "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    "AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
            End If

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS FringeBenefit " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @fringebenefitheadid "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS taxon " & _
                                                  ", 0 AS FringeBenefit " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            StrQ &= "                      ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                              "HAVING    SUM(ISNULL(PAYE.taxpayable, 0)) <> 0 " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid "

            If mintMembershipId > 0 Then
                StrQ &= "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            End If

            StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                    "WHERE   1 = 1 " & _
                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 "


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            Else
                StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND taxon = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND FringeBenefit = 0 AND taxable = 0 AND taxpaid = 0")

            If dtTemp.Length > 0 Then
                For i As Integer = 0 To dtTemp.Length - 1
                    dsList.Tables(0).Rows.Remove(dtTemp(i))
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim intCnt As Integer = 1
            Dim intPrevEmp As Integer = 0

            mdecTotBasic = 0 : mdecTotTaxOn = 0 : mdecTotAllowance = 0 : mdecTotGross = 0 : mdecTotFringeBenefit = 0 : mdecTotTaxable = 0 : mdecTotTaxDue = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                If intPrevEmp = CInt(dtRow.Item("empid")) Then '*** To prevent Duplicate entries if employee has more than one Membership Assigned
                    intPrevEmp = CInt(dtRow.Item("empid"))
                    Continue For
                End If

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = intCnt
                rpt_Row.Item("Column2") = dtRow.Item("empname")
                rpt_Row.Item("Column3") = dtRow.Item("empcode")
                rpt_Row.Item("Column4") = dtRow.Item("membershipno")

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("basicpay")), GUI.fmtCurrency)
                rpt_Row.Item("Column82") = Format(CDec(dtRow.Item("taxon")), GUI.fmtCurrency)
                rpt_Row.Item("Column83") = Format(CDec(dtRow.Item("allowancebenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column84") = Format(CDec(dtRow.Item("Grosspay")), GUI.fmtCurrency)
                rpt_Row.Item("Column85") = Format(CDec(dtRow.Item("FringeBenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column86") = Format(CDec(dtRow.Item("taxable")), GUI.fmtCurrency)
                rpt_Row.Item("Column87") = Format(CDec(dtRow.Item("taxpaid")), GUI.fmtCurrency)

                mdecTotBasic = mdecTotBasic + CDec(dtRow.Item("basicpay"))
                mdecTotTaxOn = mdecTotTaxOn + CDec(dtRow.Item("taxon"))
                mdecTotAllowance = mdecTotAllowance + CDec(dtRow.Item("allowancebenefit"))
                mdecTotGross = mdecTotGross + CDec(dtRow.Item("Grosspay"))
                mdecTotFringeBenefit = mdecTotFringeBenefit + CDec(dtRow.Item("FringeBenefit"))
                mdecTotTaxable = mdecTotTaxable + CDec(dtRow.Item("taxable"))
                mdecTotTaxDue = mdecTotTaxDue + CDec(dtRow.Item("taxpaid"))

                intCnt += 1
                intPrevEmp = CInt(dtRow.Item("empid"))
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptKY_P10A

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 101, "P.10A"))
            ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 102, "Income Tax Department"))
            ReportFunction.TextChange(objRpt, "lblCertificateYear", Language.getMessage(mstrModuleName, 103, "P.A.Y.E SUPPORTING LIST FOR END OF YEAR CERTIFICATE: YEAR ") & " " & mstrPeriodNames)
            'ReportFunction.TextChange(objRpt, "txtYear", mstrPeriodNames)
            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblEmployerPin", Language.getMessage(mstrModuleName, 104, "PIN"))
            'ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 105, "EMPLOYER'S NAME"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)

            ReportFunction.TextChange(objRpt, "lblEmployeePIN", Language.getMessage(mstrModuleName, 106, "EMPLOYEE’S PIN"))
            ReportFunction.TextChange(objRpt, "lblEmployeeName", Language.getMessage(mstrModuleName, 107, "EMPLOYEE’S NAME"))
            ReportFunction.TextChange(objRpt, "lblTotalEmoluments", Language.getMessage(mstrModuleName, 108, "TOTAL EMOLUMENTS ") & "  " & strBaseCurrencySign & ".")
            ReportFunction.TextChange(objRpt, "lblTotalPAYEDeducted", Language.getMessage(mstrModuleName, 109, "PAYE DEDUCTED ") & "  " & strBaseCurrencySign & ".")

            ReportFunction.TextChange(objRpt, "lblEmoluments", Language.getMessage(mstrModuleName, 110, "TOTAL EMOLUMENTS"))
            ReportFunction.TextChange(objRpt, "txtTotalEmoluments", Format(mdecTotGross, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblTotalPayeTax", Language.getMessage(mstrModuleName, 111, "TOTAL PAYE TAX"))
            ReportFunction.TextChange(objRpt, "txtTotalPAYETax", Format(mdecTotTaxDue, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblTotalWCPS", Language.getMessage(mstrModuleName, 112, "TOTAL WCPS"))
            ReportFunction.TextChange(objRpt, "txtTotalWCPS", "")

            ReportFunction.TextChange(objRpt, "lblTaxOn", Language.getMessage(mstrModuleName, 113, "*TAX ON LUMPSUM/AUDIT/INTEREST/PENALTY"))
            ReportFunction.TextChange(objRpt, "txtTaxOn", Format(mdecTotTaxOn, GUI.fmtCurrency))

            ReportFunction.TextChange(objRpt, "lblTotalTaxDeducted", Language.getMessage(mstrModuleName, 114, "*TOTAL TAX DEDUCTED/TOTAL C/F TO NEXT LIST"))
            ReportFunction.TextChange(objRpt, "txtTotalTaxDeducted", "")

            ReportFunction.TextChange(objRpt, "lblDeleteNote", Language.getMessage(mstrModuleName, 115, "*DELETE AS APPROPRIATE"))
            ReportFunction.TextChange(objRpt, "lblNoteTo", Language.getMessage(mstrModuleName, 116, "NOTE TO EMPLOYER:"))
            ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 117, "ATTACH TWO COPIES OF THIS LIST TO END OF YEAR CERTIFICATE (P10)"))

            ReportFunction.TextChange(objRpt, "lblInfoReq", Language.getMessage(mstrModuleName, 118, "INFORMATION REQUIRED FROM EMPLOYER AT END OF YEAR"))
            ReportFunction.TextChange(objRpt, "lblPoint1Part1", Language.getMessage(mstrModuleName, 119, "(1)	Date employee connected if during year"))
            ReportFunction.TextChange(objRpt, "txtPoint1Part1", "")
            ReportFunction.TextChange(objRpt, "lblPoint1Part2", Language.getMessage(mstrModuleName, 120, "Name and address of old employer"))
            ReportFunction.TextChange(objRpt, "txtPoint1Part2", "")
            ReportFunction.TextChange(objRpt, "lblPoint2Part1", Language.getMessage(mstrModuleName, 121, "(2)    Date left if during year"))
            ReportFunction.TextChange(objRpt, "txtPoint2Part1", "")
            ReportFunction.TextChange(objRpt, "lblPoint2Part2", Language.getMessage(mstrModuleName, 122, "Name and address of new employer"))
            ReportFunction.TextChange(objRpt, "txtPoint2Part2", "")
            ReportFunction.TextChange(objRpt, "lblPoint3Part1", Language.getMessage(mstrModuleName, 123, "(3)    Where housing is provided, state monthly rent: "))
            ReportFunction.TextChange(objRpt, "lblPoint3Part2", Language.getMessage(mstrModuleName, 124, "Charged ") & " " & strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtPoint3Part2", Language.getMessage(mstrModuleName, 125, ""))
            ReportFunction.TextChange(objRpt, "lblPoint3Part3", Language.getMessage(mstrModuleName, 126, " per month"))
            ReportFunction.TextChange(objRpt, "lblPoint4Part1", Language.getMessage(mstrModuleName, 127, "(4)    Where any of the pay relates to a period other than this year, e.g. gratuity,"))
            ReportFunction.TextChange(objRpt, "lblPoint4Part2", Language.getMessage(mstrModuleName, 128, "Give details of Amounts, Year and Tax."))
            ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 129, "Year"))
            ReportFunction.TextChange(objRpt, "lblYear1", Language.getMessage(mstrModuleName, 130, "20"))
            ReportFunction.TextChange(objRpt, "lblYear2", Language.getMessage(mstrModuleName, 131, "20"))
            ReportFunction.TextChange(objRpt, "lblYear3", Language.getMessage(mstrModuleName, 132, "20"))
            ReportFunction.TextChange(objRpt, "lblYear4", Language.getMessage(mstrModuleName, 133, "20"))
            ReportFunction.TextChange(objRpt, "lblAmount", Language.getMessage(mstrModuleName, 134, "Amount"))
            ReportFunction.TextChange(objRpt, "lblCurrency1", Language.getMessage(mstrModuleName, 135, "Pound"))
            ReportFunction.TextChange(objRpt, "lblCurrency2", Language.getMessage(mstrModuleName, 136, strBaseCurrencySign))
            ReportFunction.TextChange(objRpt, "lblAmount1C1", Language.getMessage(mstrModuleName, 137, ""))
            ReportFunction.TextChange(objRpt, "lblAmount2C1", Language.getMessage(mstrModuleName, 138, ""))
            ReportFunction.TextChange(objRpt, "lblAmount3C1", Language.getMessage(mstrModuleName, 139, ""))
            ReportFunction.TextChange(objRpt, "lblAmount4C1", Language.getMessage(mstrModuleName, 140, ""))
            ReportFunction.TextChange(objRpt, "lblAmount1C2", Language.getMessage(mstrModuleName, 141, ""))
            ReportFunction.TextChange(objRpt, "lblAmount2C2", Language.getMessage(mstrModuleName, 142, ""))
            ReportFunction.TextChange(objRpt, "lblAmount3C2", Language.getMessage(mstrModuleName, 143, ""))
            ReportFunction.TextChange(objRpt, "lblAmount4C2", Language.getMessage(mstrModuleName, 144, ""))
            ReportFunction.TextChange(objRpt, "lblGuideNote", Language.getMessage(mstrModuleName, 145, "FOR MONTHLY RATES OF BENEFITS REFER TO EMPLOYERS' GUIDE TO P.A.Y.E. SYSTEM - P7."))

            ReportFunction.TextChange(objRpt, "lblCalofTaxBenefit", Language.getMessage(mstrModuleName, 146, "CALCULATIONS OF TAX ON BENEFITS"))
            ReportFunction.TextChange(objRpt, "lblBenefit", Language.getMessage(mstrModuleName, 147, "BENEFIT"))
            ReportFunction.TextChange(objRpt, "lblNo", Language.getMessage(mstrModuleName, 148, "NO."))
            ReportFunction.TextChange(objRpt, "lblRate", Language.getMessage(mstrModuleName, 149, "RATE"))
            ReportFunction.TextChange(objRpt, "lblNoOfMonths", Language.getMessage(mstrModuleName, 150, "NO. OF MONTHS"))
            ReportFunction.TextChange(objRpt, "lblTotalAmount", Language.getMessage(mstrModuleName, 151, "TOTAL AMOUNT"))

            ReportFunction.TextChange(objRpt, "lblServant", Language.getMessage(mstrModuleName, 152, "COOK/HSE. SERVANT"))
            ReportFunction.TextChange(objRpt, "lblGardner", Language.getMessage(mstrModuleName, 153, "GARDNER"))
            ReportFunction.TextChange(objRpt, "lblAyah", Language.getMessage(mstrModuleName, 154, "AYAH"))
            ReportFunction.TextChange(objRpt, "lblWatchmanD", Language.getMessage(mstrModuleName, 155, "WATCHMAN(D)"))
            ReportFunction.TextChange(objRpt, "lblWatchmanN", Language.getMessage(mstrModuleName, 156, "WATCHMAN(N)"))
            ReportFunction.TextChange(objRpt, "lblFurniture", Language.getMessage(mstrModuleName, 157, "FURNITURE"))
            ReportFunction.TextChange(objRpt, "lblWater", Language.getMessage(mstrModuleName, 158, "WATER"))
            ReportFunction.TextChange(objRpt, "lblTelephone", Language.getMessage(mstrModuleName, 159, "TELEPHONE"))
            ReportFunction.TextChange(objRpt, "lblElectricity", Language.getMessage(mstrModuleName, 160, "ELECTRICITY"))
            ReportFunction.TextChange(objRpt, "lblSecSyst", Language.getMessage(mstrModuleName, 161, "SEC. SYST."))

            ReportFunction.TextChange(objRpt, "lblTotalAmountCurr", Language.getMessage(mstrModuleName, 162, "Kshs."))

            ReportFunction.TextChange(objRpt, "txtServantNo", Language.getMessage(mstrModuleName, 163, ""))
            ReportFunction.TextChange(objRpt, "txtGardnerNo", Language.getMessage(mstrModuleName, 164, ""))
            ReportFunction.TextChange(objRpt, "txtAyahNo", Language.getMessage(mstrModuleName, 165, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanDNo", Language.getMessage(mstrModuleName, 166, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanNNo", Language.getMessage(mstrModuleName, 167, ""))
            ReportFunction.TextChange(objRpt, "txtFurnitureNo", Language.getMessage(mstrModuleName, 168, ""))
            ReportFunction.TextChange(objRpt, "txtWaterNo", Language.getMessage(mstrModuleName, 169, ""))
            ReportFunction.TextChange(objRpt, "txtTelephoneNo", Language.getMessage(mstrModuleName, 170, ""))

            ReportFunction.TextChange(objRpt, "lblServantX1", Language.getMessage(mstrModuleName, 171, "x"))
            ReportFunction.TextChange(objRpt, "lblGardnerX1", Language.getMessage(mstrModuleName, 172, "x"))
            ReportFunction.TextChange(objRpt, "lblAyahX1", Language.getMessage(mstrModuleName, 173, "x"))
            ReportFunction.TextChange(objRpt, "lblWatchmanDX1", Language.getMessage(mstrModuleName, 174, "x"))
            ReportFunction.TextChange(objRpt, "lblWatchmanNX1", Language.getMessage(mstrModuleName, 175, "x"))
            ReportFunction.TextChange(objRpt, "lblFurnitureX1", Language.getMessage(mstrModuleName, 176, "x"))
            ReportFunction.TextChange(objRpt, "lblWaterX1", Language.getMessage(mstrModuleName, 177, "x"))
            ReportFunction.TextChange(objRpt, "lblTelephoneX1", Language.getMessage(mstrModuleName, 178, "x"))

            ReportFunction.TextChange(objRpt, "txtServantRate", Language.getMessage(mstrModuleName, 179, "2250"))
            ReportFunction.TextChange(objRpt, "txtGardnerRate", Language.getMessage(mstrModuleName, 180, ""))
            ReportFunction.TextChange(objRpt, "txtAyahRate", Language.getMessage(mstrModuleName, 181, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanDRate", Language.getMessage(mstrModuleName, 182, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanNRate", Language.getMessage(mstrModuleName, 183, ""))
            ReportFunction.TextChange(objRpt, "txtFurnitureRate", Language.getMessage(mstrModuleName, 184, ""))
            ReportFunction.TextChange(objRpt, "txtWaterRate", Language.getMessage(mstrModuleName, 185, "500"))
            ReportFunction.TextChange(objRpt, "txtTelephoneRate", Language.getMessage(mstrModuleName, 186, ""))
            ReportFunction.TextChange(objRpt, "txtElectricityRate", Language.getMessage(mstrModuleName, 187, "1500"))
            ReportFunction.TextChange(objRpt, "txtSecSystRate", Language.getMessage(mstrModuleName, 188, ""))

            ReportFunction.TextChange(objRpt, "lblServantX2", Language.getMessage(mstrModuleName, 189, "x"))
            ReportFunction.TextChange(objRpt, "lblGardnerX2", Language.getMessage(mstrModuleName, 190, "x"))
            ReportFunction.TextChange(objRpt, "lblAyahX2", Language.getMessage(mstrModuleName, 191, "x"))
            ReportFunction.TextChange(objRpt, "lblWatchmanDX2", Language.getMessage(mstrModuleName, 192, "x"))
            ReportFunction.TextChange(objRpt, "lblWatchmanNX2", Language.getMessage(mstrModuleName, 193, "x"))
            ReportFunction.TextChange(objRpt, "lblFurnitureX2", Language.getMessage(mstrModuleName, 194, "x"))
            ReportFunction.TextChange(objRpt, "lblWaterX2", Language.getMessage(mstrModuleName, 195, "x"))
            ReportFunction.TextChange(objRpt, "lblTelephoneRateX2", Language.getMessage(mstrModuleName, 196, "x"))
            ReportFunction.TextChange(objRpt, "lblElectricityRateX2", Language.getMessage(mstrModuleName, 197, "x"))
            ReportFunction.TextChange(objRpt, "lblSecSystRateX2", Language.getMessage(mstrModuleName, 198, "x"))

            ReportFunction.TextChange(objRpt, "txtServantMonths", Language.getMessage(mstrModuleName, 199, "12"))
            ReportFunction.TextChange(objRpt, "txtGardnerMonths", Language.getMessage(mstrModuleName, 200, ""))
            ReportFunction.TextChange(objRpt, "txtAyahMonths", Language.getMessage(mstrModuleName, 201, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanDMonths", Language.getMessage(mstrModuleName, 202, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanNMonths", Language.getMessage(mstrModuleName, 203, ""))
            ReportFunction.TextChange(objRpt, "txtFurnitureMonths", Language.getMessage(mstrModuleName, 204, ""))
            ReportFunction.TextChange(objRpt, "txtWaterMonths", Language.getMessage(mstrModuleName, 205, "12"))
            ReportFunction.TextChange(objRpt, "txtTelephoneMonths", Language.getMessage(mstrModuleName, 206, ""))
            ReportFunction.TextChange(objRpt, "txtElectricityMonths", Language.getMessage(mstrModuleName, 207, "12"))
            ReportFunction.TextChange(objRpt, "txtSecSystMonths", Language.getMessage(mstrModuleName, 208, ""))

            ReportFunction.TextChange(objRpt, "lblCalTaxEq1", Language.getMessage(mstrModuleName, 291, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq2", Language.getMessage(mstrModuleName, 292, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq3", Language.getMessage(mstrModuleName, 293, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq4", Language.getMessage(mstrModuleName, 294, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq5", Language.getMessage(mstrModuleName, 295, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq6", Language.getMessage(mstrModuleName, 296, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq7", Language.getMessage(mstrModuleName, 297, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq8", Language.getMessage(mstrModuleName, 298, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq9", Language.getMessage(mstrModuleName, 299, "="))
            ReportFunction.TextChange(objRpt, "lblCalTaxEq10", Language.getMessage(mstrModuleName, 300, "="))

            ReportFunction.TextChange(objRpt, "txtServantTotal", Language.getMessage(mstrModuleName, 209, "27000"))
            ReportFunction.TextChange(objRpt, "txtGardnerTotal", Language.getMessage(mstrModuleName, 210, ""))
            ReportFunction.TextChange(objRpt, "txtAyahTotal", Language.getMessage(mstrModuleName, 211, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanDTotal", Language.getMessage(mstrModuleName, 212, ""))
            ReportFunction.TextChange(objRpt, "txtWatchmanNTotal", Language.getMessage(mstrModuleName, 213, ""))
            ReportFunction.TextChange(objRpt, "txtFurnitureTotal", Language.getMessage(mstrModuleName, 214, ""))
            ReportFunction.TextChange(objRpt, "txtWaterTotal", Language.getMessage(mstrModuleName, 215, "6000"))
            ReportFunction.TextChange(objRpt, "txtTelephoneTotal", Language.getMessage(mstrModuleName, 216, ""))
            ReportFunction.TextChange(objRpt, "txtElectricityTotal", Language.getMessage(mstrModuleName, 217, "18000"))
            ReportFunction.TextChange(objRpt, "txtSecSystTotal", Language.getMessage(mstrModuleName, 218, ""))

            ReportFunction.TextChange(objRpt, "lblMessage", Language.getMessage(mstrModuleName, 219, "Where actual cost is higher than given monthly rates of benefits then the actual cost is brought to charge in full."))
            ReportFunction.TextChange(objRpt, "lblPrescribeInterest", Language.getMessage(mstrModuleName, 220, "LOW INTEREST RATE BELOW PRESCRIBED RATE OF INTEREST."))
            ReportFunction.TextChange(objRpt, "lblEmployerLoan", Language.getMessage(mstrModuleName, 221, "EMPLOYERS LOAN"))
            ReportFunction.TextChange(objRpt, "lblEq", Language.getMessage(mstrModuleName, 222, "="))
            ReportFunction.TextChange(objRpt, "lblCurr", Language.getMessage(mstrModuleName, 223, "Kshs."))
            ReportFunction.TextChange(objRpt, "txtCurr", Language.getMessage(mstrModuleName, 224, ""))
            ReportFunction.TextChange(objRpt, "lblAt", Language.getMessage(mstrModuleName, 225, "@"))
            ReportFunction.TextChange(objRpt, "txtRate", Language.getMessage(mstrModuleName, 226, ""))
            ReportFunction.TextChange(objRpt, "lblAtRate", Language.getMessage(mstrModuleName, 227, "RATE"))
            ReportFunction.TextChange(objRpt, "lblRateDiff", Language.getMessage(mstrModuleName, 228, "RATE DIFFERENCE"))
            ReportFunction.TextChange(objRpt, "lblRateCal", Language.getMessage(mstrModuleName, 229, "(PRESCRIBED RATE - EMPLOYERS RATE)="))
            ReportFunction.TextChange(objRpt, "txtRateCal", Language.getMessage(mstrModuleName, 230, ""))
            ReportFunction.TextChange(objRpt, "lblPerc", Language.getMessage(mstrModuleName, 231, "%"))
            ReportFunction.TextChange(objRpt, "lblMonthlyBenefit", Language.getMessage(mstrModuleName, 232, "MONTHLY BENEFIT (RATE DIFFERENCE x LOAN) ="))
            ReportFunction.TextChange(objRpt, "txtMonthlyBenefit", Language.getMessage(mstrModuleName, 233, ""))
            ReportFunction.TextChange(objRpt, "lblPerc1", Language.getMessage(mstrModuleName, 234, "%"))
            ReportFunction.TextChange(objRpt, "lblMul", Language.getMessage(mstrModuleName, 235, " X "))
            ReportFunction.TextChange(objRpt, "lblCurr1", Language.getMessage(mstrModuleName, 236, "Kshs. "))
            ReportFunction.TextChange(objRpt, "lblEq1", Language.getMessage(mstrModuleName, 237, "= "))
            ReportFunction.TextChange(objRpt, "lblEq2", Language.getMessage(mstrModuleName, 238, "= "))
            ReportFunction.TextChange(objRpt, "lblDigit", Language.getMessage(mstrModuleName, 239, "12"))
            ReportFunction.TextChange(objRpt, "lblDigit1", Language.getMessage(mstrModuleName, 240, "12"))

            ReportFunction.TextChange(objRpt, "lblMotor", Language.getMessage(mstrModuleName, 241, "MOTOR CARS"))

            ReportFunction.TextChange(objRpt, "lblUpto11", Language.getMessage(mstrModuleName, 242, "Up to 15000 c.c."))
            ReportFunction.TextChange(objRpt, "txtUpto12", Language.getMessage(mstrModuleName, 243, ""))
            ReportFunction.TextChange(objRpt, "lblUpto13", Language.getMessage(mstrModuleName, 244, "X  "))
            ReportFunction.TextChange(objRpt, "lblEq14", Language.getMessage(mstrModuleName, 245, "="))
            ReportFunction.TextChange(objRpt, "txtUpto15", Language.getMessage(mstrModuleName, 246, ""))

            ReportFunction.TextChange(objRpt, "lblcc21", Language.getMessage(mstrModuleName, 249, "1501 c.c. - "))
            ReportFunction.TextChange(objRpt, "lblcc22", Language.getMessage(mstrModuleName, 250, "1750 c.c. - "))
            ReportFunction.TextChange(objRpt, "lblcc23", Language.getMessage(mstrModuleName, 251, "10750x12"))
            ReportFunction.TextChange(objRpt, "lblEq24", Language.getMessage(mstrModuleName, 252, "="))
            ReportFunction.TextChange(objRpt, "txtUpto25", Language.getMessage(mstrModuleName, 253, "129000"))

            ReportFunction.TextChange(objRpt, "lblcc31", Language.getMessage(mstrModuleName, 255, "1751 c.c. - "))
            ReportFunction.TextChange(objRpt, "lblcc32", Language.getMessage(mstrModuleName, 256, "2000 c.c. - "))
            ReportFunction.TextChange(objRpt, "txtUpto33", Language.getMessage(mstrModuleName, 257, ""))
            ReportFunction.TextChange(objRpt, "lblEq34", Language.getMessage(mstrModuleName, 258, "="))
            ReportFunction.TextChange(objRpt, "txtUpto35", Language.getMessage(mstrModuleName, 259, ""))

            ReportFunction.TextChange(objRpt, "lblcc41", Language.getMessage(mstrModuleName, 260, "2001 c.c. - "))
            ReportFunction.TextChange(objRpt, "lblcc42", Language.getMessage(mstrModuleName, 261, "3000 c.c. - "))
            ReportFunction.TextChange(objRpt, "txtUpto43", Language.getMessage(mstrModuleName, 262, ""))
            ReportFunction.TextChange(objRpt, "lblEq44", Language.getMessage(mstrModuleName, 263, "="))
            ReportFunction.TextChange(objRpt, "txtUpto45", Language.getMessage(mstrModuleName, 264, ""))

            ReportFunction.TextChange(objRpt, "lblcc51", Language.getMessage(mstrModuleName, 265, "Over 3000 c.c."))
            ReportFunction.TextChange(objRpt, "txtUpto52", Language.getMessage(mstrModuleName, 266, ""))
            ReportFunction.TextChange(objRpt, "lblEq53", Language.getMessage(mstrModuleName, 267, "="))
            ReportFunction.TextChange(objRpt, "txtUpto54", Language.getMessage(mstrModuleName, 268, ""))

            ReportFunction.TextChange(objRpt, "lblcc61", Language.getMessage(mstrModuleName, 269, "Total Benefit in Year"))
            ReportFunction.TextChange(objRpt, "txtUpto62", Language.getMessage(mstrModuleName, 270, ""))
            ReportFunction.TextChange(objRpt, "lblEq63", Language.getMessage(mstrModuleName, 271, "="))
            ReportFunction.TextChange(objRpt, "txtUpto64", Language.getMessage(mstrModuleName, 272, "180000"))

            ReportFunction.TextChange(objRpt, "lblMessage1", Language.getMessage(mstrModuleName, 274, "If this amount doeas not agree with total of Col. B overleaf, attach explanation."))
            ReportFunction.TextChange(objRpt, "lblMessage2", Language.getMessage(mstrModuleName, 275, "FOR PICK-UPS, PANEL VANS AND LAND-ROVERS REFER TO APPENDIX 5 OF EMPLOYER'S GUIDE."))
            ReportFunction.TextChange(objRpt, "lblMessage3", Language.getMessage(mstrModuleName, 276, "CAR BENEFIT - The higher amount of the fixed monthly rate or the prescribed rate of benefits is to be brought to charge:-"))

            ReportFunction.TextChange(objRpt, "lblPrescribedRate", Language.getMessage(mstrModuleName, 277, "PRESCRIBED RATE:- "))
            ReportFunction.TextChange(objRpt, "lblPRMsg1", Language.getMessage(mstrModuleName, 278, "1996 - 1% per month of the initial cost of the vehicle."))
            ReportFunction.TextChange(objRpt, "lblPRMsg2", Language.getMessage(mstrModuleName, 279, "1997 - 1.5% per month of the initial cost of the vehicle."))
            ReportFunction.TextChange(objRpt, "lblPRMsg3", Language.getMessage(mstrModuleName, 280, "1998 - 2% per month of the initial cost of the vehicle."))

            ReportFunction.TextChange(objRpt, "lblMessage4", Language.getMessage(mstrModuleName, 281, "EMPLOYERS CERTIFICATE OF PAY AND TAX"))

            ReportFunction.TextChange(objRpt, "lblName", Language.getMessage(mstrModuleName, 282, "NAME"))
            ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 283, ""))
            ReportFunction.TextChange(objRpt, "lblAddress", Language.getMessage(mstrModuleName, 284, "ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 285, ""))
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 286, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 287, ""))
            ReportFunction.TextChange(objRpt, "lblDateStamp", Language.getMessage(mstrModuleName, 288, "DATE & STAMP"))
            ReportFunction.TextChange(objRpt, "txtDateStamp", Language.getMessage(mstrModuleName, 289, ""))

            ReportFunction.TextChange(objRpt, "lblNote1", Language.getMessage(mstrModuleName, 290, "NOTE: Employer's certificate to be signed by the person who prepares and submits to the PAYE End of Year Returns and copy of the P9A be issued to the employee in January."))

            'Shani(22-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables("ArutiTable")

                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 14, "EMPLOYEE’S PIN")
                intColIndex += 1
                mdtTableExcel.Columns("Column4").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 15, "EMPLOYEE’S NAME")
                intColIndex += 1
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 16, "TOTAL EMOLUMENTS ") & "  " & strBaseCurrencySign & "."
                intColIndex += 1
                mdtTableExcel.Columns("Column84").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column87").Caption = Language.getMessage(mstrModuleName, 109, "PAYE DEDUCTED") & "  " & strBaseCurrencySign & "."
                intColIndex += 1
                mdtTableExcel.Columns("Column87").SetOrdinal(intColIndex)

                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If
            'Shani(22-Dec-2015) -- End


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport_P10A; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Nilay (15-Dec-2015) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED")
            Language.setMessage(mstrModuleName, 2, "P.A.Y.E  RETURN FOR THE QUARTER ENDING")
            Language.setMessage(mstrModuleName, 3, "P.10D")
            Language.setMessage(mstrModuleName, 4, "DOMESTIC TAXES DEPARTMENT")
            Language.setMessage(mstrModuleName, 5, "EMPLOYER’S QUARTERLY RETURN")
            Language.setMessage(mstrModuleName, 6, "NAME OF EMPLOYER")
            Language.setMessage(mstrModuleName, 7, "EMPLOYER'S PIN")
            Language.setMessage(mstrModuleName, 8, "Year")
            Language.setMessage(mstrModuleName, 14, "EMPLOYEE’S PIN")
            Language.setMessage(mstrModuleName, 15, "EMPLOYEE’S NAME")
            Language.setMessage(mstrModuleName, 16, "TOTAL EMOLUMENTS")
            Language.setMessage(mstrModuleName, 18, "TOTAL PAYE DEDUCTED")
            Language.setMessage(mstrModuleName, 19, "BANK OF REMMITANCE")
            Language.setMessage(mstrModuleName, 20, "TOTAL PAYE TAX")
            Language.setMessage(mstrModuleName, 21, "TOTAL WCPS")
            Language.setMessage(mstrModuleName, 22, "FRINGE BENEFIT TAX")
            Language.setMessage(mstrModuleName, 23, "NB:KRA SHALL NOT ACCEPT ANY  LIABILITY FOR LOSS OR DAMAGE INCURRED AS A RESULT OF ALTERATION OF THIS FORM")
            Language.setMessage(mstrModuleName, 25, "TAX ON LUMPSUMS/AUDIT/PENALTY/INTEREST")
            Language.setMessage(mstrModuleName, 26, "TOTAL TAX PAID FOR THE QUARTER")
            Language.setMessage(mstrModuleName, 101, "P.10A")
            Language.setMessage(mstrModuleName, 102, "Income Tax Department")
            Language.setMessage(mstrModuleName, 103, "P.A.Y.E SUPPORTING LIST FOR END OF YEAR CERTIFICATE: YEAR")
            Language.setMessage(mstrModuleName, 104, "PIN")
            Language.setMessage(mstrModuleName, 105, "EMPLOYER'S NAME")
            Language.setMessage(mstrModuleName, 106, "EMPLOYEE’S PIN")
            Language.setMessage(mstrModuleName, 107, "EMPLOYEE’S NAME")
            Language.setMessage(mstrModuleName, 108, "TOTAL EMOLUMENTS")
            Language.setMessage(mstrModuleName, 109, "PAYE DEDUCTED")
            Language.setMessage(mstrModuleName, 110, "TOTAL EMOLUMENTS")
            Language.setMessage(mstrModuleName, 111, "TOTAL PAYE TAX")
            Language.setMessage(mstrModuleName, 112, "TOTAL WCPS")
            Language.setMessage(mstrModuleName, 113, "*TAX ON LUMPSUM/AUDIT/INTEREST/PENALTY")
            Language.setMessage(mstrModuleName, 114, "*TOTAL TAX DEDUCTED/TOTAL C/F TO NEXT LIST")
            Language.setMessage(mstrModuleName, 115, "*DELETE AS APPROPRIATE")
            Language.setMessage(mstrModuleName, 116, "NOTE TO EMPLOYER:")
            Language.setMessage(mstrModuleName, 117, "ATTACH TWO COPIES OF THIS LIST TO END OF YEAR CERTIFICATE (P10)")
            Language.setMessage(mstrModuleName, 118, "INFORMATION REQUIRED FROM EMPLOYER AT END OF YEAR")
            Language.setMessage(mstrModuleName, 119, "(1)	Date employee connected if during year")
            Language.setMessage(mstrModuleName, 120, "Name and address of old employer")
            Language.setMessage(mstrModuleName, 121, "(2)    Date left if during year")
            Language.setMessage(mstrModuleName, 122, "Name and address of new employer")
            Language.setMessage(mstrModuleName, 123, "(3)    Where housing is provided, state monthly rent:")
            Language.setMessage(mstrModuleName, 124, "Charged")
            Language.setMessage(mstrModuleName, 125, "")
            Language.setMessage(mstrModuleName, 126, " per month")
            Language.setMessage(mstrModuleName, 127, "(4)    Where any of the pay relates to a period othen than this year, e.g. gratuity,")
            Language.setMessage(mstrModuleName, 128, "Give details of Amounts, Year and Tax.")
            Language.setMessage(mstrModuleName, 129, "Year")
            Language.setMessage(mstrModuleName, 130, "20")
            Language.setMessage(mstrModuleName, 131, "20")
            Language.setMessage(mstrModuleName, 132, "20")
            Language.setMessage(mstrModuleName, 133, "20")
            Language.setMessage(mstrModuleName, 134, "Amount")
            Language.setMessage(mstrModuleName, 135, "Pound")
            Language.setMessage(mstrModuleName, 136, "strBaseCurrencySign")
            Language.setMessage(mstrModuleName, 137, "")
            Language.setMessage(mstrModuleName, 138, "")
            Language.setMessage(mstrModuleName, 139, "")
            Language.setMessage(mstrModuleName, 140, "")
            Language.setMessage(mstrModuleName, 141, "")
            Language.setMessage(mstrModuleName, 142, "")
            Language.setMessage(mstrModuleName, 143, "")
            Language.setMessage(mstrModuleName, 144, "")
            Language.setMessage(mstrModuleName, 145, "FOR MONTHLY RATES OF BENEFITS REFER TO EMPLOYERS' GUIDE TO P.A.Y.E. SYSTEM - P7.")
            Language.setMessage(mstrModuleName, 146, "CALCULATIONS OF TAX ON BENEFITS")
            Language.setMessage(mstrModuleName, 147, "BENEFIT")
            Language.setMessage(mstrModuleName, 148, "NO.")
            Language.setMessage(mstrModuleName, 149, "RATE")
            Language.setMessage(mstrModuleName, 150, "NO. OF MONTHS")
            Language.setMessage(mstrModuleName, 151, "TOTAL AMOUNT")
            Language.setMessage(mstrModuleName, 152, "COOK/HSE. SERVANT")
            Language.setMessage(mstrModuleName, 153, "GARDNER")
            Language.setMessage(mstrModuleName, 154, "AYAH")
            Language.setMessage(mstrModuleName, 155, "WATCHMAN(D)")
            Language.setMessage(mstrModuleName, 156, "WATCHMAN(N)")
            Language.setMessage(mstrModuleName, 157, "FURNITURE")
            Language.setMessage(mstrModuleName, 158, "WATER")
            Language.setMessage(mstrModuleName, 159, "TELEPHONE")
            Language.setMessage(mstrModuleName, 160, "ELECTRICITY")
            Language.setMessage(mstrModuleName, 161, "SEC. SYST.")
            Language.setMessage(mstrModuleName, 162, "Kshs.")
            Language.setMessage(mstrModuleName, 163, "")
            Language.setMessage(mstrModuleName, 164, "")
            Language.setMessage(mstrModuleName, 165, "")
            Language.setMessage(mstrModuleName, 166, "")
            Language.setMessage(mstrModuleName, 167, "")
            Language.setMessage(mstrModuleName, 168, "")
            Language.setMessage(mstrModuleName, 169, "")
            Language.setMessage(mstrModuleName, 170, "")
            Language.setMessage(mstrModuleName, 171, "x")
            Language.setMessage(mstrModuleName, 172, "x")
            Language.setMessage(mstrModuleName, 173, "x")
            Language.setMessage(mstrModuleName, 174, "x")
            Language.setMessage(mstrModuleName, 175, "x")
            Language.setMessage(mstrModuleName, 176, "x")
            Language.setMessage(mstrModuleName, 177, "x")
            Language.setMessage(mstrModuleName, 178, "x")
            Language.setMessage(mstrModuleName, 179, "2250")
            Language.setMessage(mstrModuleName, 180, "")
            Language.setMessage(mstrModuleName, 181, "")
            Language.setMessage(mstrModuleName, 182, "")
            Language.setMessage(mstrModuleName, 183, "")
            Language.setMessage(mstrModuleName, 184, "")
            Language.setMessage(mstrModuleName, 185, "500")
            Language.setMessage(mstrModuleName, 186, "")
            Language.setMessage(mstrModuleName, 187, "1500")
            Language.setMessage(mstrModuleName, 188, "")
            Language.setMessage(mstrModuleName, 189, "x")
            Language.setMessage(mstrModuleName, 190, "x")
            Language.setMessage(mstrModuleName, 191, "x")
            Language.setMessage(mstrModuleName, 192, "x")
            Language.setMessage(mstrModuleName, 193, "x")
            Language.setMessage(mstrModuleName, 194, "x")
            Language.setMessage(mstrModuleName, 195, "x")
            Language.setMessage(mstrModuleName, 196, "")
            Language.setMessage(mstrModuleName, 197, "1500")
            Language.setMessage(mstrModuleName, 198, "")
            Language.setMessage(mstrModuleName, 199, "12")
            Language.setMessage(mstrModuleName, 200, "")
            Language.setMessage(mstrModuleName, 201, "")
            Language.setMessage(mstrModuleName, 202, "")
            Language.setMessage(mstrModuleName, 203, "")
            Language.setMessage(mstrModuleName, 204, "")
            Language.setMessage(mstrModuleName, 205, "12")
            Language.setMessage(mstrModuleName, 206, "")
            Language.setMessage(mstrModuleName, 207, "12")
            Language.setMessage(mstrModuleName, 208, "")
            Language.setMessage(mstrModuleName, 209, "27000")
            Language.setMessage(mstrModuleName, 210, "")
            Language.setMessage(mstrModuleName, 211, "")
            Language.setMessage(mstrModuleName, 212, "")
            Language.setMessage(mstrModuleName, 213, "")
            Language.setMessage(mstrModuleName, 214, "")
            Language.setMessage(mstrModuleName, 215, "6000")
            Language.setMessage(mstrModuleName, 216, "")
            Language.setMessage(mstrModuleName, 217, "18000")
            Language.setMessage(mstrModuleName, 218, "")
            Language.setMessage(mstrModuleName, 219, "Where actual cost is higher than given monthly rates of benefits then the actual cost is brought to charge in full.")
            Language.setMessage(mstrModuleName, 220, "LOW INTEREST RATE BELOW PRESCRIBED RATE OF INTEREST.")
            Language.setMessage(mstrModuleName, 221, "EMPLOYERS LOAN")
            Language.setMessage(mstrModuleName, 222, "=")
            Language.setMessage(mstrModuleName, 223, "Kshs.")
            Language.setMessage(mstrModuleName, 224, "")
            Language.setMessage(mstrModuleName, 225, "@")
            Language.setMessage(mstrModuleName, 226, "")
            Language.setMessage(mstrModuleName, 227, "RATE")
            Language.setMessage(mstrModuleName, 228, "RATE DIFFERENCE")
            Language.setMessage(mstrModuleName, 229, "(PRESCRIBED RATE - EMPLOYERS RATE)=")
            Language.setMessage(mstrModuleName, 230, "")
            Language.setMessage(mstrModuleName, 231, "%")
            Language.setMessage(mstrModuleName, 232, "MONTHLY BENEFIT (RATE DIFFERENCE x LOAN) =")
            Language.setMessage(mstrModuleName, 233, "")
            Language.setMessage(mstrModuleName, 234, "%")
            Language.setMessage(mstrModuleName, 235, " X")
            Language.setMessage(mstrModuleName, 236, "Kshs.")
            Language.setMessage(mstrModuleName, 237, "=")
            Language.setMessage(mstrModuleName, 238, "=")
            Language.setMessage(mstrModuleName, 239, "12")
            Language.setMessage(mstrModuleName, 240, "12")
            Language.setMessage(mstrModuleName, 241, "MOTOR CARS")
            Language.setMessage(mstrModuleName, 242, "Up to 15000 c.c.")
            Language.setMessage(mstrModuleName, 243, "")
            Language.setMessage(mstrModuleName, 244, "X")
            Language.setMessage(mstrModuleName, 245, "=")
            Language.setMessage(mstrModuleName, 246, "")
            Language.setMessage(mstrModuleName, 249, "1501 c.c. -")
            Language.setMessage(mstrModuleName, 250, "1750 c.c. -")
            Language.setMessage(mstrModuleName, 251, "10750x12")
            Language.setMessage(mstrModuleName, 252, "=")
            Language.setMessage(mstrModuleName, 253, "129000")
            Language.setMessage(mstrModuleName, 255, "1751 c.c. -")
            Language.setMessage(mstrModuleName, 256, "2000 c.c. -")
            Language.setMessage(mstrModuleName, 257, "")
            Language.setMessage(mstrModuleName, 258, "=")
            Language.setMessage(mstrModuleName, 259, "")
            Language.setMessage(mstrModuleName, 260, "2001 c.c. -")
            Language.setMessage(mstrModuleName, 261, "3000 c.c. -")
            Language.setMessage(mstrModuleName, 262, "")
            Language.setMessage(mstrModuleName, 263, "=")
            Language.setMessage(mstrModuleName, 264, "")
            Language.setMessage(mstrModuleName, 265, "Over 3000 c.c.")
            Language.setMessage(mstrModuleName, 266, "")
            Language.setMessage(mstrModuleName, 267, "=")
            Language.setMessage(mstrModuleName, 268, "")
            Language.setMessage(mstrModuleName, 269, "Total Benefit in Year")
            Language.setMessage(mstrModuleName, 270, "")
            Language.setMessage(mstrModuleName, 271, "=")
            Language.setMessage(mstrModuleName, 272, "180000")
            Language.setMessage(mstrModuleName, 274, "If this amount doeas not agree with total of Col. B overleaf, attach explanation.")
            Language.setMessage(mstrModuleName, 275, "FOR PICK-UPS, PANEL VANS AND LAND-ROVERS REFER TO APPENDIX 5 OF EMPLOYER'S GUIDE.")
            Language.setMessage(mstrModuleName, 276, "CAR BENEFIT - The higher amount of the fixed monthly rate or the prescribed rate of benefits is to be brought to charge:-")
            Language.setMessage(mstrModuleName, 277, "PRESCRIBED RATE:-")
            Language.setMessage(mstrModuleName, 278, "1996 - 1% per month of the initial cost of the vehicle.")
            Language.setMessage(mstrModuleName, 279, "1997 - 1.5% per month of the initial cost of the vehicle.")
            Language.setMessage(mstrModuleName, 280, "1998 - 2% per month of the initial cost of the vehicle.")
            Language.setMessage(mstrModuleName, 281, "EMPLOYERS CERTIFICATE OF PAY AND TAX")
            Language.setMessage(mstrModuleName, 282, "NAME")
            Language.setMessage(mstrModuleName, 283, "")
            Language.setMessage(mstrModuleName, 284, "ADDRESS")
            Language.setMessage(mstrModuleName, 285, "")
            Language.setMessage(mstrModuleName, 286, "SIGNATURE")
            Language.setMessage(mstrModuleName, 287, "")
            Language.setMessage(mstrModuleName, 288, "DATE & STAMP")
            Language.setMessage(mstrModuleName, 289, "")
            Language.setMessage(mstrModuleName, 290, "NOTE: Employer's certificate to be signed by the person who prepares and submits to the PAYE End of Year Returns and copy of the P9A be issued to the employee in January.")
            Language.setMessage(mstrModuleName, 291, "=")
            Language.setMessage(mstrModuleName, 292, "=")
            Language.setMessage(mstrModuleName, 293, "=")
            Language.setMessage(mstrModuleName, 294, "=")
            Language.setMessage(mstrModuleName, 295, "=")
            Language.setMessage(mstrModuleName, 296, "=")
            Language.setMessage(mstrModuleName, 297, "=")
            Language.setMessage(mstrModuleName, 298, "=")
            Language.setMessage(mstrModuleName, 299, "=")
            Language.setMessage(mstrModuleName, 300, "=")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
