#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmStatementAndPaymentTaxReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmStatementAndPaymentTaxReport"
    Private objStatementAndPaymentTaxReport As clsStatementAndPaymentTaxReport

    Private mstrPeriodsName As String = ""

    'S.SANDEEP [ 02 AUG 2012 ] -- START
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAnalysis_OrderBy_GroupName As String = "" 'Sohail (30 Nov 2013)

    Private menReport As enArutiReport
    Private objReport As New clsArutiReportClass
    Private mstrDeductionHeadIDs As String = ""

    Private mstrAdvanceFilter As String = ""


#End Region

#Region " Contructor "

    Public Sub New(ByVal enReport As enArutiReport)
        objStatementAndPaymentTaxReport = New clsStatementAndPaymentTaxReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        'objStatementAndPaymentTaxReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership_ID = 1
        Identity_ID = 2
        Primary_EmployeementType = 4
        Secondary_EmployeementType = 5
        DirectorFee_EmployeementType = 6
        TaxableAmountHead_ID = 7
        TaxPayableHead_ID = 8
        BasicPayHead_ID = 9
        DeductionHead_IDs = 10
        AllocationID = 11
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsCombos As New DataSet
        Try
            lvDeductionHead.Items.Clear()
            Dim lvItem As ListViewItem

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", False, , , , , , )


            For Each dtRow As DataRow In dsCombos.Tables("Head").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = CInt(dtRow.Item("tranheadunkid"))
                lvItem.SubItems.Add(dtRow.Item("name").ToString)

                If mstrDeductionHeadIDs.Trim <> "" Then
                    If mstrDeductionHeadIDs.Split(",").Contains(dtRow.Item("tranheadunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If
                lvDeductionHead.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvDeductionHead.GridLines = False

            If lvDeductionHead.Items.Count > 7 Then
                colhHeadName.Width = 209 - 18
            Else
                colhHeadName.Width = 209
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim dsCombos As New DataSet
        Dim objCommon As New clsCommon_Master
        Dim objTranHead As New clsTransactionHead
        Dim objperiod As New clscommom_period_Tran
        Dim objMasterData As New clsMasterData

        Try
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            Dim objMember As New clsmembership_master
            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
            End With


            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , , )
            With cboTaxableHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Head").Copy
                .SelectedValue = 0
            End With

            With cboPayable
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Head").Copy
                .SelectedValue = 0
            End With

            With cbobasicpay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Head").Copy
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "IdType")
            With cboIdType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("IdType")
            End With

            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With

            dsCombos = objMasterData.GetEAllocation_Notification("Allocation")
            Dim dr As DataRow = dsCombos.Tables("Allocation").NewRow
            dr.Item("Id") = 99999
            dr.Item("NAME") = Language.getMessage(mstrModuleName, 7, "Employeement Type")
            dsCombos.Tables("Allocation").Rows.Add(dr)
            With cboAllocation
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Allocation")
                .SelectedValue = 99999
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objTranHead = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboIdType.SelectedValue = 0
            cboPrimary.SelectedValue = 0
            cboSecondary.SelectedValue = 0
            cboDirectorFee.SelectedValue = 0
            cboTaxableHead.SelectedValue = 0
            cboPayable.SelectedValue = 0
            cbobasicpay.SelectedValue = 0
            Call CheckAll(lvDeductionHead, False)

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            Call objStatementAndPaymentTaxReport.SetDefaultValue()

            mstrDeductionHeadIDs = ""

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            If lvDeductionHead.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one deduction head to continue."), enMsgBoxStyle.Information)
                lvDeductionHead.Focus()
                Return False
            End If


            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            If CInt(cboAllocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Allocation is mandatory information. Please select Allocation to continue."), enMsgBoxStyle.Information)
                cboAllocation.Focus()
                Exit Function
            End If

            If CInt(cboMembership.SelectedValue) > 0 Then
                objStatementAndPaymentTaxReport._MembershipId = cboMembership.SelectedValue
                objStatementAndPaymentTaxReport._MembershipName = cboMembership.Text
            End If
            objStatementAndPaymentTaxReport._BasicPayHeadId = CInt(cbobasicpay.SelectedValue)

            Dim allId As List(Of String) = (From lv In lvDeductionHead.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            mstrDeductionHeadIDs = String.Join(",", allId.Select(Function(x) x.ToString()).ToArray())

            objStatementAndPaymentTaxReport._DeductionHeadId = mstrDeductionHeadIDs


            objStatementAndPaymentTaxReport._TaxableAmountHeadId = CInt(cboTaxableHead.SelectedValue)
            objStatementAndPaymentTaxReport._TaxPayableHeadId = CInt(cboPayable.SelectedValue)

            'objStatementAndPaymentTaxReport._ViewByIds = mstrStringIds
            'objStatementAndPaymentTaxReport._ViewIndex = mintViewIdx
            'objStatementAndPaymentTaxReport._ViewByName = mstrStringName
            'objStatementAndPaymentTaxReport._Analysis_Fields = mstrAnalysis_Fields
            'objStatementAndPaymentTaxReport._Analysis_Join = mstrAnalysis_Join
            'objStatementAndPaymentTaxReport._Report_GroupName = mstrReport_GroupName

            objStatementAndPaymentTaxReport._AllocationId = cboAllocation.SelectedValue

            objStatementAndPaymentTaxReport._AdvanceFilter = mstrAdvanceFilter

            If IsNothing(cboPrimary.SelectedValue) = False AndAlso cboPrimary.SelectedValue >= 0 Then
                objStatementAndPaymentTaxReport._PrimaryId = CInt(cboPrimary.SelectedValue)
            End If

            If IsNothing(cboSecondary.SelectedValue) = False AndAlso cboSecondary.SelectedValue >= 0 Then
                objStatementAndPaymentTaxReport._SecondaryId = CInt(cboSecondary.SelectedValue)
            End If

            If IsNothing(cboDirectorFee.SelectedValue) = False AndAlso cboDirectorFee.SelectedValue >= 0 Then
                objStatementAndPaymentTaxReport._DirectorsId = CInt(cboDirectorFee.SelectedValue)
            End If

            objStatementAndPaymentTaxReport._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objStatementAndPaymentTaxReport._IdentityId = cboIdType.SelectedValue
            objStatementAndPaymentTaxReport._IdentityName = cboIdType.Text
            objStatementAndPaymentTaxReport._IncludeInactiveEmp = chkInactiveEmp.Checked

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objStatementAndPaymentTaxReport._Period_Id = cboPeriod.SelectedValue
                objStatementAndPaymentTaxReport._Period_Names = cboPeriod.Text
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objStatementAndPaymentTaxReport._EmployeeId = cboEmployee.SelectedValue
                objStatementAndPaymentTaxReport._EmployeeName = cboEmployee.Text
            End If

            'S.SANDEEP |28-JUL-2021| -- START
            objStatementAndPaymentTaxReport._ShowEmployeeCode = chkShowEmployeeCode.Checked
            'S.SANDEEP |28-JUL-2021| -- END


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub CheckAll(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership_ID
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Identity_ID
                            cboIdType.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.AllocationID
                            cboAllocation.SelectedValue = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.Primary_EmployeementType
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                cboPrimary.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            End If

                        Case enHeadTypeId.Secondary_EmployeementType
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                cboSecondary.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            End If

                        Case enHeadTypeId.DirectorFee_EmployeementType
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                cboDirectorFee.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            End If

                        Case enHeadTypeId.TaxableAmountHead_ID
                            cboTaxableHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TaxPayableHead_ID
                            cboPayable.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.BasicPayHead_ID
                            cbobasicpay.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.DeductionHead_IDs
                            mstrDeductionHeadIDs = dsRow.Item("transactionheadid").ToString


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
#End Region

#Region " Forms "

    Private Sub frmStatementAndPaymentTaxReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objStatementAndPaymentTaxReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatementAndPaymentTaxReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatementAndPaymentTaxReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            'Call OtherSettings()
            Me._Title = objStatementAndPaymentTaxReport._ReportName
            Me._Message = objStatementAndPaymentTaxReport._ReportDesc
            Call FillCombo()
            Call ResetValue()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatementAndPaymentTaxReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatementAndPaymentTaxReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmStatementAndPaymentTaxReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatementAndPaymentTaxReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmStatementAndPaymentTaxReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub




#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            'objStatementAndPaymentTaxReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objStatementAndPaymentTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       objPeriod._Start_Date, _
                                       objPeriod._End_Date, _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            'objStatementAndPaymentTaxReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objStatementAndPaymentTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       objPeriod._Start_Date, _
                                       objPeriod._End_Date, _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStatementAndPaymentTaxReport.SetMessages()
            objfrm._Other_ModuleNames = "clsStatementAndPaymentTaxReport"

            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try

            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 13
                objUserDefRMode = New clsUserDef_ReportMode

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership_ID
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.Identity_ID
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIdType.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.Primary_EmployeementType
                        objUserDefRMode._Headtypeid = intHeadType

                        If IsNothing(cboPrimary.SelectedValue) = False Then
                            objUserDefRMode._EarningTranHeadIds = cboPrimary.SelectedValue.ToString
                        Else
                            objUserDefRMode._EarningTranHeadIds = -1
                        End If

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.Secondary_EmployeementType
                        objUserDefRMode._Headtypeid = intHeadType

                        If IsNothing(cboSecondary.SelectedValue) = False Then
                            objUserDefRMode._EarningTranHeadIds = cboSecondary.SelectedValue.ToString
                        Else
                            objUserDefRMode._EarningTranHeadIds = -1
                        End If

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.DirectorFee_EmployeementType
                        objUserDefRMode._Headtypeid = intHeadType

                        If IsNothing(cboDirectorFee.SelectedValue) = False Then
                            objUserDefRMode._EarningTranHeadIds = cboDirectorFee.SelectedValue.ToString
                        Else
                            objUserDefRMode._EarningTranHeadIds = -1
                        End If

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.TaxableAmountHead_ID
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTaxableHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.TaxPayableHead_ID
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPayable.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.BasicPayHead_ID
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cbobasicpay.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                    Case enHeadTypeId.DeductionHead_IDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrDeductionHeadIDs
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)


                    Case enHeadTypeId.AllocationID
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAllocation.SelectedValue
                        intUnkid = objUserDefRMode.isExist(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAllocation.LinkClicked
        'Dim frm As New frmViewAnalysis
        Dim frm As New frmAdvanceSearch

        Try
            'frm.displayDialog()
            'mstrStringIds = frm._ReportBy_Ids
            'mstrStringName = frm._ReportBy_Name
            'mintViewIdx = frm._ViewIndex

            'mstrAnalysis_Fields = frm._Analysis_Fields
            'mstrAnalysis_Join = frm._Analysis_Join
            'mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            'mstrReport_GroupName = frm._Report_GroupName
            'mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName


            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub cboColumnF_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboTaxableHead.Validating, cboPayable.Validating, cbobasicpay.Validating
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboMembership.Name AndAlso t.Name <> cboPeriod.Name AndAlso t.Name <> cboIdType.Name AndAlso t.Name <> cboPrimary.Name AndAlso t.Name <> cboSecondary.Name AndAlso t.Name <> cboDirectorFee.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboColumnF_Validating", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboEmployee.DataSource
                frm.ValueMember = cboEmployee.ValueMember
                frm.DisplayMember = cboEmployee.DisplayMember
                frm.CodeMember = "employeecode"
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboPeriod.DataSource
                frm.ValueMember = cboPeriod.ValueMember
                frm.DisplayMember = cboPeriod.DisplayMember
                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                    cboPeriod.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboMembership.DataSource
                frm.ValueMember = cboMembership.ValueMember
                frm.DisplayMember = cboMembership.DisplayMember
                If frm.DisplayDialog Then
                    cboMembership.SelectedValue = frm.SelectedValue
                    cboMembership.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchIdentity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchIdentity.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboIdType.DataSource
                frm.ValueMember = cboIdType.ValueMember
                frm.DisplayMember = cboIdType.DisplayMember
                If frm.DisplayDialog Then
                    cboIdType.SelectedValue = frm.SelectedValue
                    cboIdType.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchPrimary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPrimary.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboPrimary.DataSource
                frm.ValueMember = cboPrimary.ValueMember
                frm.DisplayMember = cboPrimary.DisplayMember
                If frm.DisplayDialog Then
                    cboPrimary.SelectedValue = frm.SelectedValue
                    cboPrimary.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchPrimary_Click", mstrModuleName)
            Finally
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchSecondary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSecondary.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboSecondary.DataSource
                frm.ValueMember = cboSecondary.ValueMember
                frm.DisplayMember = cboSecondary.DisplayMember
                If frm.DisplayDialog Then
                    cboSecondary.SelectedValue = frm.SelectedValue
                    cboSecondary.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSecondary_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchBasicPay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BasicPay.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cbobasicpay.DataSource
                frm.ValueMember = cbobasicpay.ValueMember
                frm.DisplayMember = cbobasicpay.DisplayMember
                If frm.DisplayDialog Then
                    cbobasicpay.SelectedValue = frm.SelectedValue
                    cbobasicpay.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchBasicPay_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBasicPay_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchPayable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPayable.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboPayable.DataSource
                frm.ValueMember = cboPayable.ValueMember
                frm.DisplayMember = cboPayable.DisplayMember
                If frm.DisplayDialog Then
                    cboPayable.SelectedValue = frm.SelectedValue
                    cboPayable.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchPayable_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchTaxable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTaxable.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboTaxableHead.DataSource
                frm.ValueMember = cboTaxableHead.ValueMember
                frm.DisplayMember = cboTaxableHead.DisplayMember
                If frm.DisplayDialog Then
                    cboTaxableHead.SelectedValue = frm.SelectedValue
                    cboTaxableHead.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchTaxable_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchDirector_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDirector.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboDirectorFee.DataSource
                frm.ValueMember = cboDirectorFee.ValueMember
                frm.DisplayMember = cboDirectorFee.DisplayMember
                If frm.DisplayDialog Then
                    cboDirectorFee.SelectedValue = frm.SelectedValue
                    cboDirectorFee.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchDirector_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    'Private Sub objbtnSearchDeduction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim frm As New frmCommonSearch
    '        Try
    '            frm.DataSource = cbodeductionhead.DataSource
    '            frm.ValueMember = cbodeductionhead.ValueMember
    '            frm.DisplayMember = cbodeductionhead.DisplayMember
    '            If frm.DisplayDialog Then
    '                cbodeductionhead.SelectedValue = frm.SelectedValue
    '                cbodeductionhead.Focus()
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objbtnSearchDeduction_Click", mstrModuleName)
    '        Finally
    '            frm = Nothing
    '        End Try
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try

    'End Sub


    Private Sub objchkAllDeductionHead_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllDeductionHead.CheckedChanged
        Try
            Call CheckAll(lvDeductionHead, objchkAllDeductionHead.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllDeductionHead_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDeductionHead.ItemChecked
        Try
            RemoveHandler objchkAllDeductionHead.CheckedChanged, AddressOf objchkAllDeductionHead_CheckedChanged
            If lvDeductionHead.CheckedItems.Count <= 0 Then
                objchkAllDeductionHead.CheckState = CheckState.Unchecked
            ElseIf lvDeductionHead.CheckedItems.Count < lvDeductionHead.Items.Count Then
                objchkAllDeductionHead.CheckState = CheckState.Indeterminate
            ElseIf lvDeductionHead.CheckedItems.Count = lvDeductionHead.Items.Count Then
                objchkAllDeductionHead.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllDeductionHead.CheckedChanged, AddressOf objchkAllDeductionHead_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim dsList As New DataSet
        Dim ValueColumn As String
        Dim TextColumn As String


        If cboAllocation.SelectedValue = enAllocation.BRANCH Then
            Dim objBranch As New clsStation
            dsList = objBranch.GetList("List", True)
            ValueColumn = "stationunkid"
            TextColumn = "name"
            objBranch = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.DEPARTMENT_GROUP Then
            Dim objDeptGrp As New clsDepartmentGroup
            dsList = objDeptGrp.GetList("List", True)
            ValueColumn = "deptgroupunkid"
            TextColumn = "name"
            objDeptGrp = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.DEPARTMENT Then
            Dim objDept As New clsDepartment
            dsList = objDept.GetList("List", True)
            ValueColumn = "departmentunkid"
            TextColumn = "name"
            objDept = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.SECTION_GROUP Then
            Dim objSectionGrp As New clsSectionGroup
            dsList = objSectionGrp.GetList("List", True)
            ValueColumn = "sectiongroupunkid"
            TextColumn = "name"
            objSectionGrp = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.SECTION Then
            Dim objSection As New clsSections
            dsList = objSection.GetList("List", True)
            ValueColumn = "sectionunkid"
            TextColumn = "name"
            objSection = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.UNIT_GROUP Then
            Dim objUnitGrp As New clsUnitGroup
            dsList = objUnitGrp.GetList("List", True)
            ValueColumn = "unitgroupunkid"
            TextColumn = "name"
            objUnitGrp = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.UNIT Then
            Dim objUnit As New clsUnits
            dsList = objUnit.GetList("List", True)
            ValueColumn = "unitunkid"
            TextColumn = "name"
            objUnit = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.TEAM Then
            Dim objTeam As New clsTeams
            dsList = objTeam.GetList("List", True)
            ValueColumn = "teamunkid"
            TextColumn = "name"
            objTeam = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.JOB_GROUP Then
            Dim objJobGrp As New clsJobGroup
            dsList = objJobGrp.GetList("List", True)
            ValueColumn = "jobgroupunkid"
            TextColumn = "name"
            objJobGrp = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.JOBS Then
            Dim objJob As New clsJobs
            dsList = objJob.GetList("List", True)
            ValueColumn = "jobunkid"
            TextColumn = "JobName"
            objJob = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.CLASS_GROUP Then
            Dim objClassGrp As New clsClassGroup
            dsList = objClassGrp.GetList("List", True)
            ValueColumn = "classgroupunkid"
            TextColumn = "JobName"
            objClassGrp = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.CLASSES Then
            Dim objClass As New clsClass
            dsList = objClass.GetList("List", True)
            ValueColumn = "classesunkid"
            TextColumn = "JobName"
            objClass = Nothing

        ElseIf cboAllocation.SelectedValue = enAllocation.COST_CENTER Then
            Dim objCost As New clscostcenter_master
            dsList = objCost.GetList("List", True)
            ValueColumn = "costcenterunkid"
            TextColumn = "costcentername"
            objCost = Nothing

        Else
            Dim objCommon As New clsCommon_Master
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            ValueColumn = "masterunkid"
            TextColumn = "name"
            objCommon = Nothing
        End If


        cboPrimary.DataSource = Nothing
        With cboPrimary
            .ValueMember = ValueColumn
            .DisplayMember = TextColumn
            .DataSource = dsList.Tables("List")
        End With

        cboSecondary.DataSource = Nothing

        With cboSecondary
            .ValueMember = ValueColumn
            .DisplayMember = TextColumn
            .DataSource = dsList.Tables("List").Copy
        End With

        cboDirectorFee.DataSource = Nothing
        With cboDirectorFee
            .ValueMember = ValueColumn
            .DisplayMember = TextColumn
            .DataSource = dsList.Tables("List").Copy
        End With

    End Sub

    'S.SANDEEP |28-JUL-2021| -- START
    Private Sub lnkExportDataxlsx_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkExportDataxlsx.LinkClicked
        Try
            If Not SetFilter() Then Exit Sub
            Dim ifile As String = "payetemplate.xlsm"
            Dim ipath As String = My.Computer.FileSystem.SpecialDirectories.Temp
            IO.File.WriteAllBytes(IO.Path.Combine(ipath, ifile), My.Resources.payetemplate)
            If IO.File.Exists(IO.Path.Combine(ipath, ifile)) Then
                Dim strExportPath As String = ""
                If System.IO.Directory.Exists(ConfigParameter._Object._ExportReportPath) = False Then
                    Dim dig As New System.Windows.Forms.FolderBrowserDialog
                    dig.Description = "Select Folder Where to export report."

                    If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                        strExportPath = dig.SelectedPath
                    Else
                        Exit Sub
                    End If
                Else
                    strExportPath = ConfigParameter._Object._ExportReportPath
                End If

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                objStatementAndPaymentTaxReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                                  User._Object._Userunkid, _
                                                                  FinancialYear._Object._YearUnkid, _
                                                                  Company._Object._Companyunkid, _
                                                                  objPeriod._Start_Date, _
                                                                  objPeriod._End_Date, _
                                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                                  True, strExportPath, _
                                                                  ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, _
                                                                  enExportAction.ExcelXLSM, ConfigParameter._Object._Base_CurrencyId)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkExportDataxlsx_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |28-JUL-2021| -- END

#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkSetAllocation.Text = Language._Object.getCaption(Me.lnkSetAllocation.Name, Me.lnkSetAllocation.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
			Me.lblidentity.Text = Language._Object.getCaption(Me.lblidentity.Name, Me.lblidentity.Text)
			Me.lblPrimary.Text = Language._Object.getCaption(Me.lblPrimary.Name, Me.lblPrimary.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.lblSecondary.Text = Language._Object.getCaption(Me.lblSecondary.Name, Me.lblSecondary.Text)
			Me.lblTransectionHead.Text = Language._Object.getCaption(Me.lblTransectionHead.Name, Me.lblTransectionHead.Text)
			Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblDeductionhead.Text = Language._Object.getCaption(Me.lblDeductionhead.Name, Me.lblDeductionhead.Text)
			Me.BasicPay.Text = Language._Object.getCaption(Me.BasicPay.Name, Me.BasicPay.Text)
			Me.lblbasicpayHead.Text = Language._Object.getCaption(Me.lblbasicpayHead.Name, Me.lblbasicpayHead.Text)
			Me.colhHeadName.Text = Language._Object.getCaption(CStr(Me.colhHeadName.Tag), Me.colhHeadName.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.EZeeGradientButton1.Text = Language._Object.getCaption(Me.EZeeGradientButton1.Name, Me.EZeeGradientButton1.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.chkInactiveEmp.Text = Language._Object.getCaption(Me.chkInactiveEmp.Name, Me.chkInactiveEmp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 3, "Please select atleast one deduction head to continue.")
			Language.setMessage(mstrModuleName, 5, "Sorry, This transaction head is already mapped.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Employeement Type")
			Language.setMessage(mstrModuleName, 7, "Allocation is mandatory information. Please select Allocation to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class
