Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports OfficeOpenXml

Public Class clsSkillsAndDevelopmentLevyMonthlyReturn
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsSkillsAndDevelopmentLevyMonthlyReturn"
    Private mstrReportId As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal menReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer)


        mstrReportId = menReport

        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintLocationId As Integer = -1
    Private mstrLocationName As String = String.Empty
    Private mintGrossPaymentsId As Integer = -1

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintSDLAmountId As Integer = -1
    Private mintSDLPayableId As Integer = -1

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0

    Private mintIdentityId As Integer = 0
    Private mstrIdentityName As String = String.Empty

    Private mintPeriodId As Integer = -1
    Private mstrPeriodCode As String = String.Empty
    Private mstrPeriodName As String = String.Empty

    Private mintOtherEarningTranId As Integer = 0
    Private mblnIgnoreZero As Boolean = False
    Private mblnShowBasicSalary As Boolean = True

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""


#End Region

#Region " Properties "
    Public WriteOnly Property _LocationId() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _LocationName() As String
        Set(ByVal value As String)
            mstrLocationName = value
        End Set
    End Property

    Public WriteOnly Property _SDLAmountId() As Integer
        Set(ByVal value As Integer)
            mintSDLAmountId = value
        End Set
    End Property

    Public WriteOnly Property _SDLPayableId() As Integer
        Set(ByVal value As Integer)
            mintSDLPayableId = value
        End Set
    End Property


    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode() As String
        Set(ByVal value As String)
            mstrPeriodCode = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub
    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = xCompanyUnkid

            'S.SANDEEP |28-JUL-2021| -- START
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, ExportAction)
            'S.SANDEEP |28-JUL-2021| -- END

            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = {130, 200, 100, 110, 110, 110, 120, 150}
                Dim rowsArrayHeader As New ArrayList

                'S.SANDEEP |28-JUL-2021| -- START
                'Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)
                If ExportAction <> enExportAction.ExcelXLSM Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)
                Else
                    Dim iMainFile = Me._ReportName
                    If iMainFile.Contains("/") = True Then
                        iMainFile = iMainFile.Replace("/", "_")
                    End If

                    If iMainFile.Contains("\") Then
                        iMainFile = iMainFile.Replace("\", "_")
                    End If

                    If iMainFile.Contains(":") Then
                        iMainFile = iMainFile.Replace(":", "_")
                    End If
                    Dim strExportFileName As String = ""
                    strExportFileName = iMainFile.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
                    xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsm"

                    Dim strCols As String() = New String() {"Column1", "Column12", "Column81", "Column82", "Column83", "Column84", "Column85", "Column86", "Column87"}

                    mdtTableExcel = mdtTableExcel.DefaultView.ToTable(False, strCols)
                    For cidx As Integer = 0 To strCols.Length - 1
                        mdtTableExcel.Columns(strCols.GetValue(cidx).ToString()).SetOrdinal(cidx)
                    Next

                    If mdtTableExcel.Rows.Count <= 0 Then mdtTableExcel.Rows.Add(mdtTableExcel.NewRow())

                    Dim ifile As String = "SDLtemplate.xlsm"
                    Dim ipath As String = My.Computer.FileSystem.SpecialDirectories.Temp
                    If IO.File.Exists(IO.Path.Combine(ipath, ifile)) Then
                        Dim fsrc As IO.FileInfo = New IO.FileInfo(IO.Path.Combine(ipath, ifile))
                        Using src As ExcelPackage = New ExcelPackage(fsrc)
                            For Each worksheet As ExcelWorksheet In src.Workbook.Worksheets
                                If worksheet.Name.ToUpper() = "SDL_DETAILS" Then
                                    If objCompany._Tinno.Trim.Length > 0 Then
                                        worksheet.Cells("B2").Value = objCompany._Tinno
                                    Else
                                        worksheet.Cells("B2").Value = 0
                                    End If

                                ElseIf worksheet.Name.ToUpper() = "SDL" Then
                                    worksheet.Cells("D7").LoadFromDataTable(mdtTableExcel, False)

                                    worksheet.Cells("I7").Formula = String.Format("Sum({0})", New ExcelAddress("G7:H7").Address)
                                    worksheet.Cells("K7").Formula = "=I7-J7"
                                    worksheet.Cells("I8").Formula = String.Format("Sum({0})", New ExcelAddress("G8:H8").Address)
                                    worksheet.Cells("K8").Formula = "=I8-J8"

                                    worksheet.Cells("F9").Formula = String.Format("Sum({0})", New ExcelAddress("F7:F8").Address)
                                    worksheet.Cells("G9").Formula = String.Format("Sum({0})", New ExcelAddress("G7:G8").Address)
                                    worksheet.Cells("H9").Formula = String.Format("Sum({0})", New ExcelAddress("H7:H8").Address)
                                    worksheet.Cells("I9").Formula = String.Format("Sum({0})", New ExcelAddress("I7:I8").Address)
                                    worksheet.Cells("J9").Formula = String.Format("Sum({0})", New ExcelAddress("J7:J8").Address)
                                    worksheet.Cells("K9").Formula = String.Format("Sum({0})", New ExcelAddress("K7:K8").Address)
                                    worksheet.Cells("L9").Formula = String.Format("Sum({0})", New ExcelAddress("L7:L8").Address)

                                    worksheet.Cells("F7:L7").Style.Numberformat.Format = GUI.fmtCurrency
                                    worksheet.Cells("F8:L8").Style.Numberformat.Format = GUI.fmtCurrency


                                    'worksheet.Cells("L7").Formula = "=IF(AND(K7>0, F9>=4), $K$7*$C$7, 0)"
                                    'worksheet.Cells("L8").Formula = "=IF(AND(K8>0, F9>=4), $K$8*$C$7, 0)"

                                End If
                                'worksheet.Protection.IsProtected = True
                                'worksheet.Protection.SetPassword("Tra2020@efiling")
                            Next
                            'src.Workbook.Protection.LockStructure = True
                            'src.Workbook.Protection.SetPassword("Tra2020@efiling")
                            Dim f As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                            src.SaveAs(f)
                        End Using
                    End If

                    If IO.File.Exists(xExportReportPath) Then
                        If xOpenReportAfterExport Then
                            Process.Start(xExportReportPath)
                        End If
                    End If
                    If IO.File.Exists(IO.Path.Combine(ipath, ifile)) Then
                        IO.File.Delete(IO.Path.Combine(ipath, ifile))
                    End If

                End If
                'S.SANDEEP |28-JUL-2021| -- END

                objCompany = Nothing

            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "generateReportNew", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal ExportAction As enExportAction) As CrystalDecisions.CrystalReports.Engine.ReportClass 'S.SANDEEP |28-JUL-2021| -- START {ByVal ExportAction As enExportAction} -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim strPermEmpTypeIds As String = String.Empty
        Dim strTempEmpTypeIds As String = String.Empty
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim objCommon As New clsCommon_Master
        Try
            objDataOperation = New clsDataOperation

            Dim dsEmplType As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, False, "EmplType")
            strPermEmpTypeIds = String.Join(",", (From p In dsEmplType.Tables(0).Select("name like '%PERM%' ") Select (p.Item("masterunkid").ToString)).ToArray())
            strTempEmpTypeIds = String.Join(",", (From p In dsEmplType.Tables(0).Select("name NOT like '%PERM%' ") Select (p.Item("masterunkid").ToString)).ToArray())

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            If mintBaseCurrId = mintPaidCurrencyId Then
                If strPermEmpTypeIds.Length > 0 Then
                StrQ &= "SELECT " & _
                           " @Permanent AS EmploymentType " & _
                           ",SUM(PAYEA.TotalEmployees) AS TotalEmployees " & _
                           ",SUM(PAYEA.basicsalary) AS TotalBasicSalary " & _
                           ",SUM(PAYEA.OtherAllowance) AS TotalOtherAllowance " & _
                           ",SUM(PAYEA.basicsalary) + SUM(PAYEA.OtherAllowance) AS TotalGrossPayments " & _
                           ",SUM(PAYEA.SDLAmount) AS TotalSDLAmount " & _
                           ",SUM(PAYEA.SDLPayable) AS TotalSDLPayable "
                'Basic Salary
                StrQ &= " FROM " & _
                       "( " & _
                       "	SELECT " & _
                       "         COUNT(*) AS TotalEmployees " & _
                       "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSalary " & _
                       "        ,0 AS OtherAllowance " & _
                       "        ,0 AS GrossPayments " & _
                       "        ,0 AS SDLAmount " & _
                       "        ,0 AS SDLPayable " & _
                                                      "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "	WHERE       prpayrollprocess_tran.isvoid = 0 " & _
                                                            "AND prtranhead_master.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") "



                    'S.SANDEEP |28-JUL-2021| -- START
                    'StrQ &= "AND prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & "  " & _
                    '" AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "

                    StrQ &= "AND prtranhead_master.tranheadunkid = @SDLPayabletranheadunkid  " & _
                " AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "
                    'S.SANDEEP |28-JUL-2021| -- END

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'Other Allowance
                StrQ &= "UNION ALL " & _
                        "SELECT " & _
                            "0 AS TotalEmployees " & _
                            ",0 AS BasicSalary " & _
                            ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS OtherAllowance " & _
                            ",0 AS GrossPayments " & _
                            ",0 AS SDLAmount " & _
                            ",0 AS SDLPayable " & _
                            "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                       "        LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid AND cmclaim_process_tran.isvoid = 0 " & _
                           "        LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid AND cmexpense_master.isactive = 1 " & _
                            "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                "  AND cmretire_process_tran.isvoid = 0 " & _
                                "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                "  AND crretireexpense.isactive = 1 "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") "

                    StrQ &= "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND prtranhead_master.istaxable = 1 AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) ) OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= " AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'SDL Amount
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLAmount " & _
                                                ",0 AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLAmounttranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                'SDL Amount
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                ",0 AS SDLAmount " & _
                                                ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLPayabletranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= ") AS PAYEA " & _
                " WHERE 1 = 1 "
                End If

                ' FOR TEMP EMPLOYEES
                If strTempEmpTypeIds.Length > 0 Then
                    If StrQ.Length > 0 Then
                        StrQ &= "UNION ALL "
                    End If

                    StrQ &= "SELECT " & _
                        " @Temporary AS EmploymentType " & _
                        ",SUM(PAYEA.TotalEmployees) AS TotalEmployees " & _
                        ",SUM(PAYEA.basicsalary) AS TotalBasicSalary " & _
                        ",SUM(PAYEA.OtherAllowance) AS TotalOtherAllowance " & _
                        ",SUM(PAYEA.basicsalary) + SUM(PAYEA.OtherAllowance) AS TotalGrossPayments " & _
                        ",SUM(PAYEA.SDLAmount) AS TotalSDLAmount " & _
                        ",SUM(PAYEA.SDLPayable) AS TotalSDLPayable "
                'Basic Salary
                StrQ &= " FROM " & _
                       "( " & _
                       "	SELECT " & _
                       "         COUNT(*) AS TotalEmployees " & _
                       "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSalary " & _
                       "        ,0 AS OtherAllowance " & _
                       "        ,0 AS GrossPayments " & _
                       "        ,0 AS SDLAmount " & _
                       "        ,0 AS SDLPayable " & _
                                                      "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "	WHERE       prpayrollprocess_tran.isvoid = 0 " & _
                                                            "AND prtranhead_master.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") "



                    'S.SANDEEP |28-JUL-2021| -- START
                    'StrQ &= "AND prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & "  " & _
                    '" AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "

                    StrQ &= "AND prtranhead_master.tranheadunkid = @SDLPayabletranheadunkid  " & _
                " AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "
                    'S.SANDEEP |28-JUL-2021| -- END
                    


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'Other Allowance
                StrQ &= "UNION ALL " & _
                        "SELECT " & _
                            "0 AS TotalEmployees " & _
                            ",0 AS BasicSalary " & _
                            ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS OtherAllowance " & _
                            ",0 AS GrossPayments " & _
                            ",0 AS SDLAmount " & _
                            ",0 AS SDLPayable " & _
                            "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                       "        LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid AND cmclaim_process_tran.isvoid = 0 " & _
                           "        LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid AND cmexpense_master.isactive = 1 " & _
                           "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                "  AND cmretire_process_tran.isvoid = 0 " & _
                                "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                "  AND crretireexpense.isactive = 1 "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") "

                    StrQ &= "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND prtranhead_master.istaxable = 1 AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) ) OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= " AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'SDL Amount
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLAmount " & _
                                                ",0 AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLAmounttranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                'SDL Amount
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                ",0 AS SDLAmount " & _
                                                ",SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLPayabletranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= ") AS PAYEA " & _
                " WHERE 1 = 1 "
                End If

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then
                If strPermEmpTypeIds.Length > 0 Then
                StrQ &= "SELECT " & _
                           " @Permanent AS EmploymentType " & _
                           ",SUM(PAYEA.TotalEmployees) AS TotalEmployees " & _
                           ",SUM(PAYEA.basicsalary) AS TotalBasicSalary " & _
                           ",SUM(PAYEA.OtherAllowance) AS TotalOtherAllowance " & _
                           ",SUM(PAYEA.basicsalary) + SUM(PAYEA.OtherAllowance) AS TotalGrossPayments " & _
                           ",SUM(PAYEA.SDLAmount) AS TotalSDLAmount " & _
                           ",SUM(PAYEA.SDLPayable) AS TotalSDLPayable "
                'Basic Salary
                StrQ &= " FROM " & _
                       "( " & _
                       "	SELECT " & _
                       "         COUNT(*) AS TotalEmployees " & _
                       "		," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSalary " & _
                       "        ,0 AS OtherAllowance " & _
                       "        ,0 AS GrossPayments " & _
                       "        ,0 AS SDLAmount " & _
                       "        ,0 AS SDLPayable " & _
                                                      "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "	WHERE       prpayrollprocess_tran.isvoid = 0 " & _
                                                            "AND prtranhead_master.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") "


                StrQ &= "AND prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & "  " & _
                " AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'Other Allowance
                StrQ &= "UNION ALL " & _
                        "SELECT " & _
                            "0 AS TotalEmployees " & _
                            ",0 AS BasicSalary " & _
                            "," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS OtherAllowance " & _
                            ",0 AS GrossPayments " & _
                            ",0 AS SDLAmount " & _
                            ",0 AS SDLPayable " & _
                            "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                       "        LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid AND cmclaim_process_tran.isvoid = 0 " & _
                           "        LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid AND cmexpense_master.isactive = 1 " & _
                           "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                "  AND cmretire_process_tran.isvoid = 0 " & _
                                "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                "  AND crretireexpense.isactive = 1 "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") "

                    StrQ &= "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND prtranhead_master.istaxable = 1 AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) ) OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= " AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'SDL Amount
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                "," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLAmount " & _
                                                ",0 AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLAmounttranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                'SDL Payable
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                ",0 AS SDLAmount " & _
                                                "," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLPayabletranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strPermEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= ") AS PAYEA " & _
                " WHERE 1 = 1 "
                End If

                ' FOR TEMP EMPLOYEES
                If strTempEmpTypeIds.Length > 0 Then
                    If StrQ.Length > 0 Then
                        StrQ &= "UNION ALL "
                    End If
                    StrQ &= "SELECT " & _
                        " @Temporary AS EmploymentType " & _
                        ",SUM(PAYEA.TotalEmployees) AS TotalEmployees " & _
                        ",SUM(PAYEA.basicsalary) AS TotalBasicSalary " & _
                        ",SUM(PAYEA.OtherAllowance) AS TotalOtherAllowance " & _
                        ",SUM(PAYEA.basicsalary) + SUM(PAYEA.OtherAllowance) AS TotalGrossPayments " & _
                        ",SUM(PAYEA.SDLAmount) AS TotalSDLAmount " & _
                        ",SUM(PAYEA.SDLPayable) AS TotalSDLPayable "
                'Basic Salary
                StrQ &= " FROM " & _
                       "( " & _
                       "	SELECT " & _
                       "         COUNT(*) AS TotalEmployees " & _
                       "		," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSalary " & _
                       "        ,0 AS OtherAllowance " & _
                       "        ,0 AS GrossPayments " & _
                       "        ,0 AS SDLAmount " & _
                       "        ,0 AS SDLPayable " & _
                                                      "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "	WHERE       prpayrollprocess_tran.isvoid = 0 " & _
                                                            "AND prtranhead_master.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") "


                StrQ &= "AND prtranhead_master.typeof_id = " & CInt(enTypeOf.Salary) & "  " & _
                " AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'Other Allowance
                StrQ &= "UNION ALL " & _
                        "SELECT " & _
                            "0 AS TotalEmployees " & _
                            ",0 AS BasicSalary " & _
                            "," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS OtherAllowance " & _
                            ",0 AS GrossPayments " & _
                            ",0 AS SDLAmount " & _
                            ",0 AS SDLPayable " & _
                            "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                       "        LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid AND cmclaim_process_tran.isvoid = 0 " & _
                           "        LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid AND cmexpense_master.isactive = 1 " & _
                           "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                "  AND cmretire_process_tran.isvoid = 0 " & _
                                "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                "  AND crretireexpense.isactive = 1 "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") "

                    StrQ &= "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND prtranhead_master.istaxable = 1 AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) ) OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                    'Sohail (09 Jun 2021) - [crretirementprocessunkid]

                StrQ &= " AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'SDL Amount
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                "," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLAmount " & _
                                                ",0 AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLAmounttranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                'SDL Payable
                StrQ &= "                   UNION ALL " & _
                                       "SELECT  " & _
                                               "0 AS TotalEmployees " & _
                                                ",0 AS BasicSalary " & _
                                                ",0 AS OtherAllowance " & _
                                                ",0 AS GrossPayments " & _
                                                ",0 AS SDLAmount " & _
                                                "," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS SDLPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @SDLPayabletranheadunkid " & _
                                                    "AND hremployee_master.employmenttypeunkid IN ( " & strTempEmpTypeIds & " ) "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= ") AS PAYEA " & _
                " WHERE 1 = 1 "
                End If

            End If


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@SDLAmounttranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSDLAmountId)
            objDataOperation.AddParameter("@SDLPayabletranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSDLPayableId)
            objDataOperation.AddParameter("@Permanent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "PERM"))
            objDataOperation.AddParameter("@Temporary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "TEMP"))


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0

            Dim StrTitle As String = ""
            Dim strLocation As String = ""

            Dim decColumn4Total As Integer = 0
            Dim decColumn9Total As Decimal = 0
            Dim decColumn2Total As Decimal = 0
            Dim decColumn13Total As Decimal = 0
            Dim decColumn14Total As Decimal = 0
            Dim decColumn3Total As Decimal = 0
            Dim decColumn10Total As Decimal = 0


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                If mblnIgnoreZero = True Then
                    If CDec(dtRow.Item("Amount")) = 0 Then Continue For
                End If

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                iCnt += 1
                rpt_Row("Column1") = iCnt.ToString
                rpt_Row("Column12") = dtRow.Item("EmploymentType")
                rpt_Row("Column4") = dtRow.Item("TotalEmployees")
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column81") = dtRow.Item("TotalEmployees")
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn4Total = decColumn4Total + CInt(dtRow.Item("TotalEmployees"))
                rpt_Row("Column9") = Format(CDec(dtRow.Item("TotalBasicSalary")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column82") = CDec(dtRow.Item("TotalBasicSalary"))
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalBasicSalary"))
                rpt_Row("Column2") = Format(CDec(dtRow.Item("TotalOtherAllowance")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column83") = CDec(dtRow.Item("TotalOtherAllowance"))
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn2Total = decColumn2Total + CDec(dtRow.Item("TotalOtherAllowance"))
                rpt_Row("Column13") = Format(CDec(dtRow.Item("TotalGrossPayments")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column84") = CDec(dtRow.Item("TotalGrossPayments"))
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn13Total = decColumn13Total + CDec(dtRow.Item("TotalGrossPayments"))
                rpt_Row("Column14") = Format(CDec("0.00"), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column85") = CDec("0.00")
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn14Total = decColumn14Total + CDec("0.00")
                rpt_Row("Column7") = mstrPeriodName
                rpt_Row("Column8") = FinancialYear._Object._FinancialYear_Name
                rpt_Row("Column33") = StrTitle
                rpt_Row("Column34") = ""

                rpt_Row("Column3") = Format(CDec(dtRow.Item("TotalSDLAmount")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column86") = CDec(dtRow.Item("TotalSDLAmount"))
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn3Total = decColumn3Total + CDec(dtRow.Item("TotalSDLAmount"))
                rpt_Row("Column10") = Format(CDec(dtRow.Item("TotalSDLPayable")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column87") = CDec(dtRow.Item("TotalSDLPayable"))
                'S.SANDEEP |28-JUL-2021| -- END
                decColumn10Total = decColumn10Total + CDec(dtRow.Item("TotalSDLPayable"))


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptSkillsAndDevelopmentLevyMonthlyReturn

            objRpt.SetDataSource(rpt_Data)

            'S.SANDEEP |28-JUL-2021| -- START
            If ExportAction = enExportAction.ExcelXLSM Then
                mdtTableExcel = rpt_Data.Tables("ArutiTable")
            End If
            'S.SANDEEP |28-JUL-2021| -- END


            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 1, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", mstrPeriodName)

            Dim objCMaster As New clsMasterData
            Dim dsCountry As New DataSet
            dsCountry = objCMaster.getCountryList("List", False, Company._Object._Countryunkid)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                strLocation &= dsCountry.Tables(0).Rows(0)("country_name")
            End If
            dsCountry.Dispose()
            objCMaster = Nothing

            Dim objState As New clsstate_master
            Dim objCity As New clscity_master
            Dim objZipCode As New clszipcode_master

            objState._Stateunkid = Company._Object._Stateunkid
            objCity._Cityunkid = Company._Object._Cityunkid
            objZipCode._Zipcodeunkid = Company._Object._Postalunkid

            strLocation = strLocation & " " & Company._Object._Address2
            objState = Nothing
            objCity = Nothing
            objZipCode = Nothing


            Call ReportFunction.TextChange(objRpt, "lblLocation", Language.getMessage(mstrModuleName, 2, "Location"))
            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 3, "Year"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", strLocation)
            Call ReportFunction.TextChange(objRpt, "txtHeader1", Language.getMessage(mstrModuleName, 4, "Skill and Development Levy Monthly Return for ") & " " & strLocation)
            Call ReportFunction.TextChange(objRpt, "txtHeader2", Language.getMessage(mstrModuleName, 5, "Note : This return is submitted under the provisions of Section 16 of the Vocational Education Training Act, Cap. 82. You are hereby required to submit the return and make payment within 7 days after the end of the month to which it relates."))

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 15, "S/No"))
            Call ReportFunction.TextChange(objRpt, "txtNatureOfEmployment", Language.getMessage(mstrModuleName, 6, "NATURE OF EMPLOYMENT"))
            Call ReportFunction.TextChange(objRpt, "txtNumberOfEmployees", Language.getMessage(mstrModuleName, 7, "NUMBER OF EMPLOYEES"))
            Call ReportFunction.TextChange(objRpt, "txtBasicSalary", Language.getMessage(mstrModuleName, 8, "BASIC SALARY"))

            Call ReportFunction.TextChange(objRpt, "txtOtherAllowances", Language.getMessage(mstrModuleName, 9, "OTHER ALLOWANCES"))
            Call ReportFunction.TextChange(objRpt, "txtGrossPayments", Language.getMessage(mstrModuleName, 10, "GROSS PAYMENTS"))
            Call ReportFunction.TextChange(objRpt, "txtExemption", Language.getMessage(mstrModuleName, 11, "EXEMPTION"))
            Call ReportFunction.TextChange(objRpt, "txtAmountSubjectToSDL", Language.getMessage(mstrModuleName, 12, "AMOUNT SUBJECT TO SDL"))

            Call ReportFunction.TextChange(objRpt, "txtSDLPayable", Language.getMessage(mstrModuleName, 16, "SDL PAYABLE"))

            Call ReportFunction.TextChange(objRpt, "txtPageTotal", Language.getMessage(mstrModuleName, 17, "TOTAL"))
            Call ReportFunction.TextChange(objRpt, "txtFormNo", Language.getMessage(mstrModuleName, 18, "FORM ITX 215.03.E"))


            Call ReportFunction.TextChange(objRpt, "txtTotal0", decColumn4Total)
            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn9Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn2Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn13Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn14Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decColumn3Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decColumn10Total, GUI.fmtCurrency))

            Return objRpt

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_DetailReport", mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    Private Sub Create_OnDetailReport()
        Try
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Create_OnDetailReport", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Month")
			Language.setMessage(mstrModuleName, 2, "Location")
			Language.setMessage(mstrModuleName, 3, "Year")
			Language.setMessage(mstrModuleName, 4, "Skill and Development Levy Monthly Return for")
			Language.setMessage(mstrModuleName, 5, "Note : This return is submitted under the provisions of Section 16 of the Vocational Education Training Act, Cap. 82. You are hereby required to submit the return and make payment within 7 days after the end of the month to which it relates.")
			Language.setMessage(mstrModuleName, 6, "NATURE OF EMPLOYMENT")
			Language.setMessage(mstrModuleName, 7, "NUMBER OF EMPLOYEES")
			Language.setMessage(mstrModuleName, 8, "BASIC SALARY")
			Language.setMessage(mstrModuleName, 9, "OTHER ALLOWANCES")
			Language.setMessage(mstrModuleName, 10, "GROSS PAYMENTS")
			Language.setMessage(mstrModuleName, 11, "EXEMPTION")
			Language.setMessage(mstrModuleName, 12, "AMOUNT SUBJECT TO SDL")
			Language.setMessage(mstrModuleName, 13, "PERM")
			Language.setMessage(mstrModuleName, 14, "TEMP")
			Language.setMessage(mstrModuleName, 15, "S/No")
			Language.setMessage(mstrModuleName, 16, "SDL PAYABLE")
			Language.setMessage(mstrModuleName, 17, "TOTAL")
			Language.setMessage(mstrModuleName, 18, "FORM ITX 215.03.E")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
