﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region


Public Class frmID22DECLARATIONReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmID22DECLARATIONReport"
    Private objID22DECLARATIONReport As clsID22DECLARATIONReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0

    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""

#End Region

#Region " Constructor "

    Public Sub New()
        objID22DECLARATIONReport = New clsID22DECLARATIONReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objID22DECLARATIONReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Public Enum enHeadTypeId
        Membership = 1
        Male = 2
        Female = 3
        SalairesBrutPresence = 4
        LogementDomesticite = 5
        Nourriture = 6
        IndemnitesImposables = 7
        SalairesBrutConges = 8
        TCS = 9
        IRPP = 10
        FNH = 11
        IndemnitesNonImposables = 12
        RS = 13
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objMember As New clsmembership_master
        Dim dsCombos As DataSet
        Dim mdtPeriod As DataTable
        Try
            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            mdtPeriod = New DataView(dsCombos.Tables("Period"), "periodunkid = 0 OR (start_date >= '" & eZeeDate.convertDate(CDate("01/Jan/" & Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-2)))) & "' AND end_date <= '" & eZeeDate.convertDate(CDate("31/Dec/" & Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-1)))) & "') ", "", DataViewRowState.CurrentRows).ToTable

            If mdtPeriod.Rows.Count > 1 Then
                If CInt(mdtPeriod.Rows(1).Item("periodunkid")) > 0 Then mintFirstPeriodId = CInt(mdtPeriod.Rows(1).Item("periodunkid"))

                mintFirstPeriodId = (From p In mdtPeriod Where (Year(eZeeDate.convertDate(p.Item("start_date").ToString)) = Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-1))) Select (CInt(p.Item("periodunkid")))).FirstOrDefault

                mintLastPeriodId = CInt(mdtPeriod.Rows(mdtPeriod.Rows.Count - 1).Item("periodunkid"))
            Else
                mintFirstPeriodId = 0
            End If
            If mintLastPeriodId <= 0 Then mintLastPeriodId = mintFirstPeriodId

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = mintLastPeriodId
            End With

            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
            objMember = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboMembership.SelectedValue = 0
            txtSalairesBrutPresenceFormula.Text = ""
            txtLogementDomesticiteFormula.Text = ""
            txtNourritureFormula.Text = ""
            txtIndemnitesImposablesFormula.Text = ""
            txtSalairesBrutCongesFormula.Text = ""
            txtTCSFormula.Text = ""
            txtIRPPFormula.Text = ""
            txtFNHFormula.Text = ""
            txtIndemnitesNonImposablesFormula.Text = ""
            txtRSFormula.Text = ""

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mstrAdvanceFilter = ""


            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.SalairesBrutPresence
                            txtSalairesBrutPresenceFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.LogementDomesticite
                            txtLogementDomesticiteFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.Nourriture
                            txtNourritureFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.IndemnitesImposables
                            txtIndemnitesImposablesFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.SalairesBrutConges
                            txtSalairesBrutCongesFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.TCS
                            txtTCSFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.IRPP
                            txtIRPPFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.FNH
                            txtFNHFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.IndemnitesNonImposables
                            txtIndemnitesNonImposablesFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.RS
                            txtRSFormula.Text = dsRow.Item("transactionheadid").ToString

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        Try
            objID22DECLARATIONReport.SetDefaultValue()

            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From period is mandatory information. Please select From period to continue."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Exit Function
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To period is mandatory information. Please select To period to continue."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            ElseIf CInt(cboFromPeriod.SelectedIndex) > CInt(cboToPeriod.SelectedIndex) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Function
            ElseIf txtSalairesBrutPresenceFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter Salaire Brut Inferieur Formula."), enMsgBoxStyle.Information)
                txtSalairesBrutPresenceFormula.Focus()
                Exit Function
            ElseIf txtLogementDomesticiteFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter Salaire Brut Superieur Formula."), enMsgBoxStyle.Information)
                txtLogementDomesticiteFormula.Focus()
                Exit Function
            ElseIf txtNourritureFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please enter Nourriture Formula."), enMsgBoxStyle.Information)
                txtNourritureFormula.Focus()
                Exit Function
            ElseIf txtIndemnitesImposablesFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter Indemnites Imposables Formula."), enMsgBoxStyle.Information)
                txtIndemnitesImposablesFormula.Focus()
                Exit Function
            ElseIf txtSalairesBrutCongesFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter Salaires Brut Conges Formula."), enMsgBoxStyle.Information)
                txtSalairesBrutCongesFormula.Focus()
                Exit Function
            ElseIf txtTCSFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please enter TCS Formula."), enMsgBoxStyle.Information)
                txtTCSFormula.Focus()
                Exit Function
            ElseIf txtIRPPFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please enter IRPP Formula."), enMsgBoxStyle.Information)
                txtIRPPFormula.Focus()
                Exit Function
            ElseIf txtFNHFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please enter FNH Formula."), enMsgBoxStyle.Information)
                txtFNHFormula.Focus()
                Exit Function
            ElseIf txtIndemnitesNonImposablesFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please enter Indemnites Non-Imposables Formula."), enMsgBoxStyle.Information)
                txtIndemnitesNonImposablesFormula.Focus()
                Exit Function
            ElseIf txtRSFormula.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please enter RS Formula."), enMsgBoxStyle.Information)
                txtRSFormula.Focus()
                Exit Function
            End If



            With objID22DECLARATIONReport
                ._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
                ._FromPeriodName = cboFromPeriod.Text

                ._ToPeriodId = CInt(cboToPeriod.SelectedValue)
                ._ToPeriodName = cboToPeriod.Text

                ._PeriodStart = eZeeDate.convertDate(CType(cboFromPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                ._PeriodEnd = eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
                ._FirstPeriodYearName = eZeeDate.convertDate(CType(cboFromPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString

                ._MembershipId = CInt(cboMembership.SelectedValue)
                ._MembershipName = cboMembership.Text

                ._SalairesBrutPresenceFormula = txtSalairesBrutPresenceFormula.Text.Trim
                ._LogementDomesticiteFormula = txtLogementDomesticiteFormula.Text.Trim
                ._NourritureFormula = txtNourritureFormula.Text.Trim
                ._IndemnitesImposablesFormula = txtIndemnitesImposablesFormula.Text.Trim
                ._SalairesBrutCongesFormula = txtSalairesBrutCongesFormula.Text.Trim
                ._TCSFormula = txtTCSFormula.Text.Trim
                ._IRPPFormula = txtIRPPFormula.Text.Trim
                ._FNHFormula = txtFNHFormula.Text.Trim
                ._IndemnitesNonImposablesFormula = txtIndemnitesNonImposablesFormula.Text.Trim
                ._RSFormula = txtRSFormula.Text.Trim

                Dim i As Integer = 0
                Dim strPreriodIds As String = cboFromPeriod.SelectedValue.ToString
                Dim mstrPeriodsName As String = cboFromPeriod.Text
                dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid")))
                If dsList.Tables("Database").Rows.Count > 0 Then
                    intPrevYearID = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))
                    If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                        mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                    End If
                End If

                For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
                    If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                        strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
                        mstrPeriodsName &= ", " & dsRow.Item("name").ToString
                        If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                            If dsList.Tables("Database").Rows.Count > 0 Then
                                If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                    mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                                End If
                            End If
                        End If
                        intPrevYearID = CInt(dsRow.Item("yearunkid"))
                    End If
                    i += 1
                Next
                ._dic_YearDBName = mdicYearDBName

                ._PeriodIDs = strPreriodIds
                ._PeriodNames = mstrPeriodsName


                ._ViewByIds = mstrStringIds
                ._ViewIndex = mintViewIdx
                ._ViewByName = mstrStringName
                ._Analysis_Fields = mstrAnalysis_Fields
                ._Analysis_Join = mstrAnalysis_Join
                ._Report_GroupName = mstrReport_GroupName

                If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                    mintPaidCurrencyId = mintBaseCurrId
                End If

                ._CountryId = CInt(cboCurrency.SelectedValue)
                ._BaseCurrencyId = mintBaseCurrId
                ._PaidCurrencyId = mintPaidCurrencyId
                ._CurrencyName = cboCurrency.Text.Trim

                If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                    ._ConversionRate = mdecPaidExRate / mdecBaseExRate
                End If

                ._ExchangeRate = LblCurrencyRate.Text


            End With




            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Private Sub frmRapportTrimestrielCNAMGS_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objID22DECLARATIONReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRapportTrimestrielCNAMGS_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRapportTrimestrielCNAMGS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            eZeeHeader.Title = objID22DECLARATIONReport._ReportName
            eZeeHeader.Message = objID22DECLARATIONReport._ReportDesc

            OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRapportTrimestrielCNAMGS_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objID22DECLARATIONReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objID22DECLARATIONReport._PeriodEnd))

            objID22DECLARATIONReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             objID22DECLARATIONReport._PeriodStart, _
                                             objID22DECLARATIONReport._PeriodEnd, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, FinancialYear._Object._Database_Start_Date, ConfigParameter._Object._Base_CurrencyId, GUI.fmtCurrency, "", ConfigParameter._Object._OpenAfterExport, False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Sub

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                   Case enHeadTypeId.SalairesBrutPresence
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtSalairesBrutPresenceFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.LogementDomesticite
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtLogementDomesticiteFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Nourriture
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtNourritureFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.IndemnitesImposables
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtIndemnitesImposablesFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.SalairesBrutConges
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtSalairesBrutCongesFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TCS
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtTCSFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.IRPP
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtIRPPFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.FNH
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtFNHFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.IndemnitesNonImposables
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtIndemnitesNonImposablesFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.RS
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtRSFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsID22DECLARATIONReport.SetMessages()
            objfrm._Other_ModuleNames = "clsID22DECLARATIONReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsSBF.Click, objbtnKeywordsLDF.Click, objbtnKeywordsNF.Click _
                                                                                                                                                           , objbtnKeywordsIIF.Click, objbtnKeywordsSPF.Click, objbtnKeywordsTCF.Click _
                                                                                                                                                           , objbtnKeywordsIRF.Click, objbtnKeywordsFNF.Click, objbtnKeywordsINF.Click _
                                                                                                                                                           , objbtnRSFormula.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 17, "Available Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#      (e.g. #010#)")

            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, eZee.Common.eZeeGradientButton).Name & "_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "


#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblLogementDomesticiteFormula.Text = Language._Object.getCaption(Me.lblLogementDomesticiteFormula.Name, Me.lblLogementDomesticiteFormula.Text)
			Me.lblSalairesBrutPresenceFormula.Text = Language._Object.getCaption(Me.lblSalairesBrutPresenceFormula.Name, Me.lblSalairesBrutPresenceFormula.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblNourritureFormula.Text = Language._Object.getCaption(Me.lblNourritureFormula.Name, Me.lblNourritureFormula.Text)
			Me.lblSalairesBrutCongesFormula.Text = Language._Object.getCaption(Me.lblSalairesBrutCongesFormula.Name, Me.lblSalairesBrutCongesFormula.Text)
			Me.lblIndemnitesImposablesFormula.Text = Language._Object.getCaption(Me.lblIndemnitesImposablesFormula.Name, Me.lblIndemnitesImposablesFormula.Text)
			Me.lblTCSFormula.Text = Language._Object.getCaption(Me.lblTCSFormula.Name, Me.lblTCSFormula.Text)
			Me.lblFNHFormula.Text = Language._Object.getCaption(Me.lblFNHFormula.Name, Me.lblFNHFormula.Text)
			Me.lblIRPPFormula.Text = Language._Object.getCaption(Me.lblIRPPFormula.Name, Me.lblIRPPFormula.Text)
			Me.lblIndemnitesNonImposablesFormula.Text = Language._Object.getCaption(Me.lblIndemnitesNonImposablesFormula.Name, Me.lblIndemnitesNonImposablesFormula.Text)
			Me.LblRSFormula.Text = Language._Object.getCaption(Me.LblRSFormula.Name, Me.LblRSFormula.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From period is mandatory information. Please select From period to continue.")
			Language.setMessage(mstrModuleName, 2, "To period is mandatory information. Please select To period to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
			Language.setMessage(mstrModuleName, 4, "Please enter Salaire Brut Inferieur Formula.")
			Language.setMessage(mstrModuleName, 5, "Please enter Salaire Brut Superieur Formula.")
			Language.setMessage(mstrModuleName, 6, "Please enter Nourriture Formula.")
			Language.setMessage(mstrModuleName, 7, "Please enter Indemnites Imposables Formula.")
			Language.setMessage(mstrModuleName, 8, "Please enter Salaires Brut Conges Formula.")
			Language.setMessage(mstrModuleName, 9, "Please enter TCS Formula.")
			Language.setMessage(mstrModuleName, 10, "Please enter IRPP Formula.")
			Language.setMessage(mstrModuleName, 11, "Please enter FNH Formula.")
			Language.setMessage(mstrModuleName, 12, "Please enter Indemnites Non-Imposables Formula.")
			Language.setMessage(mstrModuleName, 13, "Please enter RS Formula.")
			Language.setMessage(mstrModuleName, 14, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 16, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 17, "Available Keywords")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class