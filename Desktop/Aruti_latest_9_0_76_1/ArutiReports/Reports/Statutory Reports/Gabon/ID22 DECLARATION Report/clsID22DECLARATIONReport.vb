﻿'************************************************************************************************************************************
'Class Name : clsID22DECLARATIONReport.vb
'Purpose    :
'Date       :10/12/2021
'Written By :Pinkal 
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text
Imports System.Text.RegularExpressions

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsID22DECLARATIONReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsID22DECLARATIONReport"
    Private mstrReportId As String = enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report
    Private objDataOperation As clsDataOperation
    Private mdtFinal As DataTable = Nothing

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region


#Region " Private Variables "

    Private mstrPeriodIDs As String = String.Empty
    Private mstrPeriodNames As String = String.Empty
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mintFromPeriodId As Integer
    Private mstrFromPeriodName As String = String.Empty
    Private mintToPeriodId As Integer
    Private mstrToPeriodName As String = String.Empty
    Private mdicYearDBName As New Dictionary(Of Integer, String)
    Private mstrFirstPeriodYearName As String = ""
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintMaleId As Integer = 0
    Private mstrMaleName As String = String.Empty
    Private mintFemaleId As Integer = 0
    Private mstrFemaleName As String = String.Empty

    Private mstrSalairesBrutPresenceFormula As String = String.Empty
    Private mstrLogementDomesticiteFormula As String = String.Empty
    Private mstrNourritureFormula As String = String.Empty
    Private mstrIndemnitesImposablesFormula As String = String.Empty
    Private mstrSalairesBrutCongesFormula As String = String.Empty
    Private mstrTCSFormula As String = String.Empty
    Private mstrIRPPFormula As String = String.Empty
    Private mstrFNHFormula As String = String.Empty
    Private mstrIndemnitesNonImposablesFormula As String = String.Empty
    Private mstrRSFormula As String = String.Empty

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrCurrencyName As String = ""
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty


#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public Property _PeriodStart() As Date
        Get
            Return mdtPeriodStart
        End Get
        Set(ByVal value As Date)
            mdtPeriodStart = value
        End Set
    End Property

    Public Property _PeriodEnd() As Date
        Get
            Return mdtPeriodEnd
        End Get
        Set(ByVal value As Date)
            mdtPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _dic_YearDBName() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicYearDBName = value
        End Set
    End Property

    Public WriteOnly Property _FirstPeriodYearName() As String
        Set(ByVal value As String)
            mstrFirstPeriodYearName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _MaleId() As Integer
        Set(ByVal value As Integer)
            mintMaleId = value
        End Set
    End Property

    Public WriteOnly Property _MaleName() As String
        Set(ByVal value As String)
            mstrMaleName = value
        End Set
    End Property

    Public WriteOnly Property _FemaleId() As Integer
        Set(ByVal value As Integer)
            mintFemaleId = value
        End Set
    End Property

    Public WriteOnly Property _FemaleName() As String
        Set(ByVal value As String)
            mstrFemaleName = value
        End Set
    End Property

    Public WriteOnly Property _SalairesBrutPresenceFormula() As String
        Set(ByVal value As String)
            mstrSalairesBrutPresenceFormula = value
        End Set
    End Property

    Public WriteOnly Property _LogementDomesticiteFormula() As String
        Set(ByVal value As String)
            mstrLogementDomesticiteFormula = value
        End Set
    End Property

    Public WriteOnly Property _NourritureFormula() As String
        Set(ByVal value As String)
            mstrNourritureFormula = value
        End Set
    End Property

    Public WriteOnly Property _IndemnitesImposablesFormula() As String
        Set(ByVal value As String)
            mstrIndemnitesImposablesFormula = value
        End Set
    End Property

    Public WriteOnly Property _SalairesBrutCongesFormula() As String
        Set(ByVal value As String)
            mstrSalairesBrutCongesFormula = value
        End Set
    End Property

    Public WriteOnly Property _TCSFormula() As String
        Set(ByVal value As String)
            mstrTCSFormula = value
        End Set
    End Property

    Public WriteOnly Property _IRPPFormula() As String
        Set(ByVal value As String)
            mstrIRPPFormula = value
        End Set
    End Property

    Public WriteOnly Property _FNHFormula() As String
        Set(ByVal value As String)
            mstrFNHFormula = value
        End Set
    End Property

    Public WriteOnly Property _IndemnitesNonImposablesFormula() As String
        Set(ByVal value As String)
            mstrIndemnitesNonImposablesFormula = value
        End Set
    End Property

    Public WriteOnly Property _RSFormula() As String
        Set(ByVal value As String)
            mstrRSFormula = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property


    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property



#End Region

#Region " Public Function & Procedures "



    Public Sub SetDefaultValue()
        Try
            mstrPeriodIDs = String.Empty
            mintFromPeriodId = 0
            mstrFromPeriodName = String.Empty
            mintToPeriodId = 0
            mstrToPeriodName = String.Empty
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mstrPeriodIDs = String.Empty
            mintMaleId = 0
            mstrMaleName = String.Empty
            mintFemaleId = 0
            mstrFemaleName = String.Empty

            mstrSalairesBrutPresenceFormula = String.Empty
            mstrLogementDomesticiteFormula = String.Empty
            mstrNourritureFormula = String.Empty
            mstrIndemnitesImposablesFormula = String.Empty
            mstrSalairesBrutCongesFormula = String.Empty
            mstrTCSFormula = String.Empty
            mstrIRPPFormula = String.Empty
            mstrFNHFormula = String.Empty
            mstrIndemnitesNonImposablesFormula = String.Empty
            mstrRSFormula = String.Empty

            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            mstrCurrencyName = String.Empty

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String _
                                    , ByVal intUserUnkid As Integer _
                                    , ByVal intYearUnkid As Integer _
                                    , ByVal intCompanyUnkid As Integer _
                                    , ByVal dtPeriodStart As Date _
                                    , ByVal dtPeriodEnd As Date _
                                    , ByVal strUserModeSetting As String _
                                    , ByVal blnOnlyApproved As Boolean _
                                    , ByVal dtFinancialYearStart As Date _
                                    , ByVal intBaseCurrencyId As Integer _
                                    , ByVal strFmtCurrency As String _
                                    , ByVal strExportReportPath As String _
                                    , ByVal blnOpenAfterExport As Boolean _
                                    , ByVal blnExcelHTML As Boolean _
                                    )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objMembership As New clsmembership_master
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Dim dsList As New DataSet
        Dim strBaseCurrencySign As String
        Dim colIdx As Integer = 0

        objDataOperation = New clsDataOperation
        Try
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            objMembership._Membershipunkid = mintMembershipId
            objCompany._Companyunkid = intCompanyUnkid

            Dim objHead As New clsTransactionHead
            Dim objDHead As New Dictionary(Of Integer, String)
            Dim objRecapitulation As New Dictionary(Of Integer, String)

            Dim dsHead As DataSet = objHead.getComboList(strDatabaseName, "List", False, , , , True, , , , True, , , True)

            Dim strPattern As String = "([^#]*#[^#]*)#"
            Dim strReplacement As String = "$1)"

            If mstrSalairesBrutPresenceFormula.Trim <> "" AndAlso mstrSalairesBrutPresenceFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrSalairesBrutPresenceFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrLogementDomesticiteFormula.Trim <> "" AndAlso mstrLogementDomesticiteFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrLogementDomesticiteFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrNourritureFormula.Trim <> "" AndAlso mstrNourritureFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrNourritureFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrIndemnitesImposablesFormula.Trim <> "" AndAlso mstrIndemnitesImposablesFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrIndemnitesImposablesFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrSalairesBrutCongesFormula.Trim <> "" AndAlso mstrSalairesBrutCongesFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrSalairesBrutCongesFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrTCSFormula.Trim <> "" AndAlso mstrTCSFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrTCSFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrIRPPFormula.Trim <> "" AndAlso mstrIRPPFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrIRPPFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrFNHFormula.Trim <> "" AndAlso mstrFNHFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrFNHFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrIndemnitesNonImposablesFormula.Trim <> "" AndAlso mstrIndemnitesNonImposablesFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrIndemnitesNonImposablesFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrRSFormula.Trim <> "" AndAlso mstrRSFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrRSFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objRecapitulation.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objRecapitulation.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrFNHFormula.Trim <> "" AndAlso mstrFNHFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrFNHFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objRecapitulation.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objRecapitulation.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If


            Dim strCurrDatabaseName As String = ""
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String

            For Each key In mdicYearDBName
                strCurrDatabaseName = key.Value

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, key.Value.ToString)
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, key.Value.ToString, intUserUnkid, intCompanyUnkid, key.Key, strUserModeSetting)

                StrQ &= "SELECT hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", CONVERT(CHAR(8), hremployee_master.birthdate, 112) AS birthdate " & _
                         ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                         ", ISNULL(ETERM.empl_enddate, '') AS EocDate " & _
                         ", ISNULL(hremployee_master.gender, 0) AS genderunkid " & _
                         ", ISNULL(hremployee_master.maritalstatusunkid, 0) AS maritalstatusunkid " & _
                         ", ISNULL(marital.code,'') AS maritalstatuscode " & _
                         ", ISNULL(marital.name,'') AS maritalstatusname " & _
                         ", ISNULL(hrjob_master.job_code, '') AS JobCode " & _
                         ", ISNULL(hrunit_master.code, '') AS UnitCode " & _
                         ", ISNULL(hrteam_master.code, '') AS TeamCode " & _
                         ", ISNULL(hrgradelevel_master.name, '') AS GradeLevelName " & _
                         ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS membershipno " & _
                         ", ISNULL(cfcommon_master.name,'') AS MemshipCategory " & _
                         ", " & key.Key & " AS YearId "

                If mblnFirstNamethenSurname = False Then
                    StrQ &= ", ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') AS employeeame "
                Else
                    StrQ &= ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeeame "
                End If

                StrQ &= "INTO #" & strCurrDatabaseName & " " & _
                        "FROM " & strCurrDatabaseName & "..hremployee_master " & _
                        " LEFT JOIN " & strCurrDatabaseName & "..hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND hremployee_meminfo_tran.isdeleted = 0 AND hremployee_meminfo_tran.membershipunkid = @MemId " & _
                        " LEFT JOIN " & strCurrDatabaseName & "..hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                        " LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                            " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                        " LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master AS marital ON marital.masterunkid = hremployee_master.maritalstatusunkid " & _
                            " AND marital.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT TERM.TEEmpId " & _
                             ", TERM.empl_enddate " & _
                             ", TERM.termination_from_date " & _
                             ", TERM.TEfDt " & _
                             ", TERM.isexclude_payroll " & _
                             ", TERM.changereasonunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT TRM.employeeunkid AS TEEmpId " & _
                                 ", CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                                 ", CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                                 ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                 ", TRM.isexclude_payroll " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                 ", TRM.changereasonunkid " & _
                                "FROM " & strCurrDatabaseName & "..hremployee_dates_tran AS TRM " & _
                            "WHERE isvoid = 0 " & _
                                  "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                  "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @enddate " & _
                        ") AS TERM " & _
                        "WHERE TERM.Rno = 1 " & _
                    ") AS ETERM " & _
                        "ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                               "AND ETERM.TEfDt >= CONVERT(CHAR(8), appointeddate, 112) " & _
                        "LEFT JOIN " & _
                                "( " & _
                                "   SELECT " & _
                                "		 gradegroupunkid " & _
                                "		,gradeunkid " & _
                                "		,gradelevelunkid " & _
                                "		,employeeunkid AS GEmpId " & _
                                "	FROM " & _
                                "	( " & _
                                "		SELECT " & _
                                "			 gradegroupunkid " & _
                                "			,gradeunkid " & _
                                "			,gradelevelunkid " & _
                                "			,employeeunkid " & _
                                "			,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                "		FROM " & strCurrDatabaseName & "..prsalaryincrement_tran " & _
                                "		WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                "	) AS GRD WHERE GRD.Rno = 1 " & _
                                ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                        "LEFT JOIN " & _
                                "( " & _
                                "  SELECT " & _
                                    "        Trf.TrfEmpId " & _
                                    "       ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
                                    "       ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
                                    "       ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                                    "       ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                    "       ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
                                    "       ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
                                    "       ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
                                    "       ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
                                    "       ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
                                    "       ,ISNULL(Trf.classunkid,0) AS classunkid " & _
                                    "       ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                                    "       ,Trf.ETT_REASON " & _
                                    "   FROM " & _
                                    "   ( " & _
                                    "       SELECT " & _
                                    "            ETT.employeeunkid AS TrfEmpId " & _
                                    "           ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                                    "           ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                                    "           ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                                    "           ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                    "           ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                                    "           ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                                    "           ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                                    "           ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                                    "           ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                                    "           ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                                    "           ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                                    "           ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                                    "           ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
                                    "       FROM " & strCurrDatabaseName & "..hremployee_transfer_tran AS ETT " & _
                                    "           LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                    "           LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
                                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    "   ) AS Trf WHERE Trf.Rno = 1 " & _
                                    ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                            "LEFT JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "     Cat.CatEmpId " & _
                                    "    ,Cat.jobgroupunkid " & _
                                    "    ,Cat.jobunkid " & _
                                    "    ,Cat.CEfDt " & _
                                    "    ,Cat.RECAT_REASON " & _
                                    "   FROM " & _
                                    "   ( " & _
                                    "       SELECT " & _
                                    "            ECT.employeeunkid AS CatEmpId " & _
                                    "           ,ECT.jobgroupunkid " & _
                                    "           ,ECT.jobunkid " & _
                                    "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                                    "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                    "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                                    "       FROM " & strCurrDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                                    "           LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                    "           LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                    "   ) AS Cat WHERE Cat.Rno = 1 " & _
                                    ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                "LEFT JOIN " & strCurrDatabaseName & "..hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid " & _
                                "LEFT JOIN " & strCurrDatabaseName & "..hrunit_master ON hrunit_master.unitunkid = ETRF.unitunkid " & _
                                "LEFT JOIN " & strCurrDatabaseName & "..hrteam_master ON hrteam_master.teamunkid = ETRF.teamunkid " & _
                                "LEFT JOIN " & strCurrDatabaseName & "..hrgradelevel_master ON EGRD.gradelevelunkid = hrgradelevel_master.gradelevelunkid "

                StrQ &= mstrAnalysis_Join

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " WHERE 1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

            Next

            Dim i As Integer = -1

            StrQ &= "SELECT  * " & _
                    "INTO #TableEmp " & _
                    "FROM ( "

            StrQ &= "SELECT A.employeeunkid " & _
                            ", A.employeecode " & _
                            ", A.employeeame " & _
                            ", ISNULL(A.birthdate, '') AS birthdate " & _
                            ", A.appointeddate " & _
                            ", A.EocDate " & _
                            ", A.genderunkid " & _
                            ", A.maritalstatusunkid " & _
                            ", A.maritalstatuscode " & _
                            ", A.maritalstatusname " & _
                            ", A.JobCode " & _
                            ", A.UnitCode " & _
                            ", A.TeamCode " & _
                            ", A.GradeLevelName " & _
                            ", A.membershipno " & _
                            ", A.MemshipCategory " & _
                            ", DENSE_RANK() OVER(PARTITION BY A.employeeunkid ORDER BY A.YearId DESC) AS ROWNO " & _
                    "FROM ( "

            For Each key In mdicYearDBName
                strCurrDatabaseName = key.Value
                i += 1

                If i > 0 Then
                    StrQ &= " UNION ALL "
                End If

                StrQ &= "SELECT #" & strCurrDatabaseName & ".employeeunkid " & _
                            ", #" & strCurrDatabaseName & ".employeecode " & _
                            ", #" & strCurrDatabaseName & ".employeeame " & _
                            ", #" & strCurrDatabaseName & ".birthdate " & _
                            ", #" & strCurrDatabaseName & ".appointeddate " & _
                            ", #" & strCurrDatabaseName & ".EocDate " & _
                            ", #" & strCurrDatabaseName & ".genderunkid " & _
                            ", #" & strCurrDatabaseName & ".maritalstatusunkid " & _
                            ", #" & strCurrDatabaseName & ".maritalstatuscode " & _
                            ", #" & strCurrDatabaseName & ".maritalstatusname " & _
                            ", #" & strCurrDatabaseName & ".JobCode " & _
                            ", #" & strCurrDatabaseName & ".UnitCode " & _
                            ", #" & strCurrDatabaseName & ".TeamCode " & _
                            ", #" & strCurrDatabaseName & ".GradeLevelName " & _
                            ", #" & strCurrDatabaseName & ".membershipno " & _
                            ", #" & strCurrDatabaseName & ".MemshipCategory " & _
                            ", #" & strCurrDatabaseName & ".YearId " & _
                        "FROM #" & strCurrDatabaseName & " "

            Next

            StrQ &= " ) AS A " & _
                    " ) AS B " & _
                      "WHERE B.ROWNO = 1 "


            i = -1

            StrQ &= "SELECT #TableEmp.employeeunkid " & _
                         ", #TableEmp.employeecode " & _
                         ", #TableEmp.employeeame " & _
                         ", #TableEmp.birthdate " & _
                         ", #TableEmp.appointeddate " & _
                         ", #TableEmp.EocDate " & _
                         ", #TableEmp.genderunkid " & _
                         ", #TableEmp.maritalstatusunkid " & _
                         ", #TableEmp.maritalstatuscode " & _
                         ", #TableEmp.maritalstatusname " & _
                         ", #TableEmp.JobCode " & _
                         ", #TableEmp.UnitCode " & _
                         ", #TableEmp.GradeLevelName " & _
                         ", #TableEmp.TeamCode " & _
                         ", MAX(#TableEmp.MembershipNo) AS MembershipNo " & _
                         ", MAX(#TableEmp.MemshipCategory) AS MemshipCategory " & _
                         ", ROW_NUMBER() OVER(ORDER BY #TableEmp.employeecode) AS ROWNO "

            For Each itm In objDHead
                StrQ &= " , ISNULL(SUM([" & itm.Key & "]), 0) AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM " & _
                    "( "

            If objDHead.Count > 0 Then

                For Each key In mdicYearDBName
                    strCurrDatabaseName = key.Value

                    For Each itm In objDHead
                        i += 1

                        If i > 0 Then
                            StrQ &= " UNION ALL "
                        End If

                        StrQ &= "SELECT prpayrollprocess_tran.employeeunkid "

                        For Each itm1 In objDHead
                            If itm1.Key = itm.Key Then
                                StrQ &= " , SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS [" & itm1.Key & "] "
                            Else
                                StrQ &= " , 0 AS [" & itm1.Key & "] "
                            End If
                        Next

                        StrQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 "


                        StrQ &= "          WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = " & itm.Key & " " & _
                                                        "AND prpayrollprocess_tran.amount > 0 " & _
                                              "GROUP BY prpayrollprocess_tran.employeeunkid "
                    Next

                Next

            Else
                StrQ &= " SELECT 0 AS employeeunkid "
            End If

            StrQ &= ") AS A " & _
                  "RIGHT JOIN #TableEmp " & _
                                "ON A.employeeunkid = #TableEmp.employeeunkid " & _
                  "GROUP BY #TableEmp.employeeunkid " & _
                            ", #TableEmp.employeecode " & _
                            ", #TableEmp.employeeame " & _
                            ", #TableEmp.birthdate " & _
                            ", #TableEmp.appointeddate " & _
                             ", #TableEmp.EocDate " & _
                             ", #TableEmp.genderunkid " & _
                             ", #TableEmp.maritalstatusunkid " & _
                             ", #TableEmp.maritalstatuscode " & _
                             ", #TableEmp.maritalstatusname " & _
                             ", #TableEmp.JobCode " & _
                             ", #TableEmp.UnitCode " & _
                             ", #TableEmp.GradeLevelName " & _
                             ", #TableEmp.TeamCode "

            i = -1

            StrQ &= "SELECT payperiodunkid "

            For Each itm In objRecapitulation
                StrQ &= " , ISNULL(SUM([" & itm.Key & "]), 0) AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM " & _
                    "( "

            If objRecapitulation.Count > 0 Then

                For Each key In mdicYearDBName
                    strCurrDatabaseName = key.Value

                    For Each itm In objRecapitulation
                        i += 1

                        If i > 0 Then
                            StrQ &= " UNION ALL "
                        End If

                        StrQ &= "SELECT cfcommon_period_tran.end_date, prtnaleave_tran.payperiodunkid "

                        For Each itm1 In objRecapitulation
                            If itm1.Key = itm.Key Then
                                StrQ &= " , SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS [" & itm1.Key & "] "
                            Else
                                StrQ &= " , 0 AS [" & itm1.Key & "] "
                            End If
                        Next

                        StrQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

                        StrQ &= "          WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = " & itm.Key & " " & _
                                                        "AND prpayrollprocess_tran.amount > 0 " & _
                                              " GROUP BY cfcommon_period_tran.end_date, prtnaleave_tran.payperiodunkid "
                    Next

                Next

            Else
                StrQ &= " SELECT 0 AS payperiodunkid "
            End If

            StrQ &= ") AS A " & _
                         " GROUP BY end_date, payperiodunkid "

            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value.ToString & " "
            Next

            StrQ &= " DROP TABLE #TableEmp "


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEnd))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            With dsList.Tables(0)
                If .Columns.Contains("Age") = False Then
                    .Columns.Add("Age", Type.GetType("System.Int32")).DefaultValue = 0
                End If
                If .Columns.Contains("AppointDay") = False Then
                    .Columns.Add("AppointDay", Type.GetType("System.Int32")).DefaultValue = 0
                End If
                If .Columns.Contains("AppointMonth") = False Then
                    .Columns.Add("AppointMonth", Type.GetType("System.Int32")).DefaultValue = 0
                End If
                If .Columns.Contains("EOCDay") = False Then
                    .Columns.Add("EOCDay", Type.GetType("System.Int32")).DefaultValue = 0
                End If
                If .Columns.Contains("EOCMonth") = False Then
                    .Columns.Add("EOCMonth", Type.GetType("System.Int32")).DefaultValue = 0
                End If
                If .Columns.Contains("GenderID") = False Then
                    .Columns.Add("GenderID", Type.GetType("System.Int32")).DefaultValue = 0
                End If

                If .Columns.Contains("SalairesBrutPresence") = False Then
                    .Columns.Add("SalairesBrutPresence", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("LogementDomesticite") = False Then
                    .Columns.Add("LogementDomesticite", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("Nourrituree") = False Then
                    .Columns.Add("Nourrituree", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("IndemnitesImposables") = False Then
                    .Columns.Add("IndemnitesImposables", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("SalairesBrutConges") = False Then
                    .Columns.Add("SalairesBrutConges", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("TCS") = False Then
                    .Columns.Add("TCS", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("IRPP") = False Then
                    .Columns.Add("IRPP", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("FNH") = False Then
                    .Columns.Add("FNH", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("IndemnitesNonImposables") = False Then
                    .Columns.Add("IndemnitesNonImposables", Type.GetType("System.Decimal")).DefaultValue = 0
                End If

                If .Columns.Contains("TotalEarning") = False Then
                    .Columns.Add("TotalEarning", Type.GetType("System.Decimal")).DefaultValue = 0
                End If
                If .Columns.Contains("TotalDeduction") = False Then
                    .Columns.Add("TotalDeduction", Type.GetType("System.Decimal")).DefaultValue = 0
                End If

            End With

            With dsList.Tables(1)
                If .Columns.Contains("RS") = False Then
                    .Columns.Add("RS", Type.GetType("System.Decimal")).DefaultValue = 0
                    .Columns("RS").Caption = Language.getMessage(mstrModuleName, 39, "RS")
                End If
                If .Columns.Contains("FNH") = False Then
                    .Columns.Add("FNH", Type.GetType("System.Decimal")).DefaultValue = 0
                    .Columns("FNH").Caption = Language.getMessage(mstrModuleName, 40, "FNH")
                End If
            End With


            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dsRow.Item("AppointDay") = eZeeDate.convertDate(dsRow.Item("appointeddate").ToString).Day
                dsRow.Item("AppointMonth") = eZeeDate.convertDate(dsRow.Item("appointeddate").ToString).Month
                If dsRow.Item("birthdate").ToString.Trim <> "" Then
                    dsRow.Item("Age") = DateDiff(DateInterval.Year, eZeeDate.convertDate(dsRow.Item("birthdate").ToString), mdtPeriodEnd)
                    dsRow.Item("birthdate") = Format(eZeeDate.convertDate(dsRow.Item("birthdate").ToString), "dd-MM-yyyy")
                Else
                    dsRow.Item("Age") = 0
                End If
                dsRow.Item("appointeddate") = Format(eZeeDate.convertDate(dsRow.Item("appointeddate").ToString), "dd-MM-yyyy")
                If dsRow.Item("EocDate").ToString.Trim <> "" Then
                    dsRow.Item("EOCDay") = eZeeDate.convertDate(dsRow.Item("EocDate").ToString).Day
                    dsRow.Item("EOCMonth") = eZeeDate.convertDate(dsRow.Item("EocDate").ToString).Month
                    dsRow.Item("EocDate") = Format(eZeeDate.convertDate(dsRow.Item("EocDate").ToString), "dd-MM-yyyy")
                Else
                    dsRow.Item("EOCDay") = 0
                    dsRow.Item("EOCMonth") = 0
                End If
                If CInt(dsRow.Item("genderunkid")) = mintMaleId Then
                    dsRow.Item("GenderID") = 1
                ElseIf CInt(dsRow.Item("genderunkid")) = mintFemaleId Then
                    dsRow.Item("GenderID") = 2
                Else
                    dsRow.Item("GenderID") = 0
                End If

                Dim decSalairesBrutPresence As Decimal = 0
                If mstrSalairesBrutPresenceFormula.Trim <> "" AndAlso mstrSalairesBrutPresenceFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrSalairesBrutPresenceFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decSalairesBrutPresence = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrSalairesBrutPresenceFormula, decSalairesBrutPresence)
                End If

                Dim decLogementDomesticite As Decimal = 0
                If mstrLogementDomesticiteFormula.Trim <> "" AndAlso mstrLogementDomesticiteFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrLogementDomesticiteFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decLogementDomesticite = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrLogementDomesticiteFormula, decLogementDomesticite)
                End If

                Dim decNourrituree As Decimal = 0
                If mstrNourritureFormula.Trim <> "" AndAlso mstrNourritureFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrNourritureFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decNourrituree = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrNourritureFormula, decNourrituree)
                End If

                Dim decIndemnitesImposables As Decimal = 0
                If mstrIndemnitesImposablesFormula.Trim <> "" AndAlso mstrIndemnitesImposablesFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrIndemnitesImposablesFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decIndemnitesImposables = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrIndemnitesImposablesFormula, decIndemnitesImposables)
                End If

                Dim decSalairesBrutConges As Decimal = 0
                If mstrSalairesBrutCongesFormula.Trim <> "" AndAlso mstrSalairesBrutCongesFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrSalairesBrutCongesFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decSalairesBrutConges = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrSalairesBrutCongesFormula, decSalairesBrutConges)
                End If

                Dim decTCS As Decimal = 0
                If mstrTCSFormula.Trim <> "" AndAlso mstrTCSFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrTCSFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decTCS = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrTCSFormula, decTCS)
                End If

                Dim decIRPP As Decimal = 0
                If mstrIRPPFormula.Trim <> "" AndAlso mstrIRPPFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrIRPPFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decIRPP = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrIRPPFormula, decIRPP)
                End If

                Dim decFNH As Decimal = 0
                If mstrFNHFormula.Trim <> "" AndAlso mstrFNHFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrFNHFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decFNH = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrFNHFormula, decFNH)
                End If

                Dim decIndemnitesNonImposables As Decimal = 0
                If mstrIndemnitesNonImposablesFormula.Trim <> "" AndAlso mstrIndemnitesNonImposablesFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrIndemnitesNonImposablesFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        Dim strHeadid As String = itm.Key.ToString
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    decIndemnitesNonImposables = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                Else
                    Decimal.TryParse(mstrIndemnitesNonImposablesFormula, decIndemnitesNonImposables)
                End If


                dsRow.Item("SalairesBrutPresence") = Format(decSalairesBrutPresence, strFmtCurrency).Replace(",", "")
                dsRow.Item("LogementDomesticite") = Format(decLogementDomesticite, strFmtCurrency).Replace(",", "")
                dsRow.Item("Nourrituree") = Format(decNourrituree, strFmtCurrency).Replace(",", "")
                dsRow.Item("IndemnitesImposables") = Format(decIndemnitesImposables, strFmtCurrency).Replace(",", "")
                dsRow.Item("SalairesBrutConges") = Format(decSalairesBrutConges, strFmtCurrency).Replace(",", "")
                dsRow.Item("TCS") = Format(decTCS, strFmtCurrency).Replace(",", "")
                dsRow.Item("IRPP") = Format(decIRPP, strFmtCurrency).Replace(",", "")
                dsRow.Item("FNH") = Format(decFNH, strFmtCurrency).Replace(",", "")
                dsRow.Item("IndemnitesNonImposables") = Format(decIndemnitesNonImposables, strFmtCurrency).Replace(",", "")

                dsRow.Item("TotalEarning") = Format(CDec(Format(decSalairesBrutPresence, strFmtCurrency)) + CDec(Format(decLogementDomesticite, strFmtCurrency)) + CDec(Format(decNourrituree, strFmtCurrency)) + CDec(Format(decIndemnitesImposables, strFmtCurrency)) + CDec(Format(decSalairesBrutConges, strFmtCurrency)), strFmtCurrency).Replace(",", "")
                dsRow.Item("TotalDeduction") = Format(CDec(Format(decTCS, strFmtCurrency)) + CDec(Format(decTCS, strFmtCurrency)) + CDec(Format(decTCS, strFmtCurrency)), strFmtCurrency).Replace(",", "")
            Next
            dsList.Tables(0).AcceptChanges()


            If dsList.Tables.Count > 1 Then
                For Each dsRow As DataRow In dsList.Tables(1).Rows

                    Dim decRSFormula As Decimal = 0
                    If mstrRSFormula.Trim <> "" AndAlso mstrRSFormula.Contains("#") = True Then
                        Dim dt As New DataTable
                        Dim s As String = mstrRSFormula
                        Dim ans As Object = Nothing

                        For Each itm In objRecapitulation
                            Dim strHeadid As String = itm.Key.ToString
                            s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                        Next
                        Try
                            ans = dt.Compute(s.ToString.Replace(",", ""), "")
                        Catch ex As Exception

                        End Try
                        If ans Is Nothing Then
                            ans = 0
                        End If
                        decRSFormula = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                    Else
                        Decimal.TryParse(mstrRSFormula, decRSFormula)
                    End If

                    Dim decFNHFormula As Decimal = 0
                    If mstrFNHFormula.Trim <> "" AndAlso mstrFNHFormula.Contains("#") = True Then
                        Dim dt As New DataTable
                        Dim s As String = mstrFNHFormula
                        Dim ans As Object = Nothing

                        For Each itm In objRecapitulation
                            Dim strHeadid As String = itm.Key.ToString
                            s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                        Next
                        Try
                            ans = dt.Compute(s.ToString.Replace(",", ""), "")
                        Catch ex As Exception

                        End Try
                        If ans Is Nothing Then
                            ans = 0
                        End If
                        decFNHFormula = Format(CDec(ans), strFmtCurrency).Replace(",", "")

                    Else
                        Decimal.TryParse(mstrFNHFormula, decFNHFormula)
                    End If
                    dsRow.Item("RS") = Format(decRSFormula, strFmtCurrency).Replace(",", "")
                    dsRow.Item("FNH") = Format(decFNHFormula, strFmtCurrency).Replace(",", "")

                Next
                dsList.Tables(1).AcceptChanges()

            End If


            Dim mdtTableExcel As DataTable = Nothing
            Dim mdtRecapitulation As DataTable = Nothing
            Dim intColIndex As Integer = -1

            mdtTableExcel = New DataView(dsList.Tables(0), "TotalEarning <> 0 AND TotalDeduction <> 0", "", DataViewRowState.CurrentRows).ToTable

            If dsList.Tables.Count > 1 Then
                mdtRecapitulation = dsList.Tables(1).Copy
            End If


            If mdtTableExcel.Columns.Contains("No") = False Then
                Dim dc As New DataColumn("No")
                dc.DataType = Type.GetType("System.String")
                dc.DefaultValue = ""
                mdtTableExcel.Columns.Add(dc)
            End If


            intColIndex += 1
            mdtTableExcel.Columns("No").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("SalairesBrutPresence").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("LogementDomesticite").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("Nourrituree").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("IndemnitesImposables").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("SalairesBrutConges").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("TotalEarning").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("TCS").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("IRPP").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("FNH").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("TotalDeduction").SetOrdinal(intColIndex)

            intColIndex += 1
            mdtTableExcel.Columns("IndemnitesNonImposables").SetOrdinal(intColIndex)

            For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
            Next

            intColIndex = -1

            If mdtRecapitulation IsNot Nothing Then
                intColIndex += 1
                mdtRecapitulation.Columns("RS").SetOrdinal(intColIndex)

                intColIndex += 1
                mdtRecapitulation.Columns("FNH").SetOrdinal(intColIndex)
            End If


            For i = intColIndex + 1 To mdtRecapitulation.Columns.Count - 1
                mdtRecapitulation.Columns.RemoveAt(intColIndex + 1)
            Next


            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing
            Dim strBuilder As New StringBuilder
            Dim arrWorkSheetStyle As List(Of WorksheetStyle) = Nothing
            Dim strWorkSheetStyle As String = ""
            Dim intInitRows As Integer = 0

            If blnExcelHTML = False Then
                arrWorkSheetStyle = New List(Of WorksheetStyle)
                Dim s8cvt As New WorksheetStyle("s8cvt")
                With s8cvt
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Alignment.VerticalText = True
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s8cvt)

                Dim s8c_cc As New WorksheetStyle("s8c_cc")
                With s8c_cc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s8c_cc)

                Dim s8wc As New WorksheetStyle("s8wc")
                With s8wc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s8wc)

                Dim n8wc As New WorksheetStyle("n8wc")
                With n8wc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = strFmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8wc)

                Dim n8bwc As New WorksheetStyle("n8bwc")
                With n8bwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = strFmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bwc)

                Dim n8bcwc As New WorksheetStyle("n8bcwc")
                With n8bcwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = strFmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bcwc)

                Dim i8c As New WorksheetStyle("i8c")
                With i8c
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = False
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = "0"
                End With
                arrWorkSheetStyle.Add(i8c)

                Dim s10bc_lr As New WorksheetStyle("s10bc_lr")
                With s10bc_lr
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10bc_lr)

                Dim s10bc_l As New WorksheetStyle("s10bc_l")
                With s10bc_l
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10bc_l)

                Dim s10c_l As New WorksheetStyle("s10c_l")
                With s10c_l
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10c_l)

                Dim s10bc_r As New WorksheetStyle("s10bc_r")
                With s10bc_r
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10bc_r)

                Dim s10c As New WorksheetStyle("s10c")
                With s10c
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10c)

                Dim n10bc As New WorksheetStyle("n10bc")
                With n10bc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n10bc)

                Dim i10bc As New WorksheetStyle("i10bc")
                With i10bc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = "############0"
                End With
                arrWorkSheetStyle.Add(i10bc)

                Dim s10bcw As New WorksheetStyle("s10bcw")
                With s10bcw
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcw)

                Dim s10bcblue As New WorksheetStyle("s10bcblue")
                With s10bcblue
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Font.Color = "#5877FC"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcblue)

                Dim s10bred As New WorksheetStyle("s10bred")
                With s10bred
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.WrapText = True
                    .Font.Color = "#FF0000"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bred)



                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_lrtb")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_lr")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "DECLARATION DES TRAITEMENTS, SALAIRES, PENSIONS VERSES, ETC…(art. 90 du CGI)"), "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "MINISTERE DE L’ECONOMIE, DU COMMERCE,"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 4, "DE L’INDUSTRIE ET DU TOURISME"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Exercice :") & " " & mstrFirstPeriodYearName & " " & Language.getMessage(mstrModuleName, 7, "(au titre duquel les retenues ont été effectuées)"), "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "--------------"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "DIRECTION GENERALE DES IMPOTS"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "BORDEREAU DETAILLE DES SALAIRES VERSES AUX EMPLOYES"), "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "BP 37 / 45 – Libreville"), "s10c_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Tel : 79.53.76/77"), "s10c_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "NOM DE L’ENTREPRISE OU RAISON SOCIALE : ") & " " & objCompany._Name.ToUpper(), "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "NIF :") & " " & If(objCompany._Ppfno.ToUpper.Trim <> "", "| ", "") & String.Join("| ", (From p In objCompany._Ppfno.ToUpper.Trim.AsEnumerable Select (p.ToString)).ToArray()) & If(objCompany._Ppfno.ToUpper.Trim <> "", " |", ""), "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Feuillet n° I    I   I"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "La présente déclaration est à déposer avant le 30 avril simultanément aux déclarations n° ID01 ou ID14"), "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_ltb")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "EFFECTIFS"), "s8cvt")
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "n°"), "s8c_cc")
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "PRESENCE AU GABON"), "s8c_cc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Salaire brut(congés) #10; (5)"), "s8c_cc")
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Total #10; (1 à 5) #10; (6)"), "s8c_cc")
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "IMPÔTS RETENUS A LA SOURCE EN 20__"), "s8c_cc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 35, "Indemnités non imposables"), "s8c_cc")
                wcell.MergeDown = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 42, "RECAPITULATION DES RETENUES A LA SOURCE"), "s10bc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                row.Height = 35
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Salaires brut présence #10; (1)"), "s8c_cc")
                wcell.MergeDown = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 29, "AVANTAGES EN NATURE"), "s8c_cc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 31, "TCS #10; (7)"), "s8c_cc")
                wcell.MergeDown = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 32, "IRPP #10; (8)"), "s8c_cc")
                wcell.MergeDown = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 33, "FNH #10; (9)"), "s8c_cc")
                wcell.MergeDown = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "TOTAL #10; (7+8+9) #10; (10)"), "s8c_cc")
                wcell.MergeDown = 2
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 43, "Recette des Impôts de ………………………"), "s8c_cc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)

                row.Height = 35
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8c_cc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 36, "LOGEMENT Eau Electricité Domesticité #10; (2)"), "s8c_cc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Nourriture #10; (3)"), "s8c_cc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 38, "Indemnités imposables #10; (4)"), "s8c_cc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 44, "mois"), "s8c_cc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 45, "Montant des retenues #10; (12)"), "s8c_cc")
                wcell.MergeDown = 1
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 46, "Date et n° de quittance"), "s8c_cc")
                wcell.MergeDown = 1
                row.Cells.Add(wcell)

                row.Height = 35
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                rowsArrayHeader.Add(row)

                Dim xRowIdx As Integer = -1

                Dim intRowCount As Integer = mdtTableExcel.Rows.Count
                If intRowCount < (mdtRecapitulation.Rows.Count * 2) + 9 Then
                    intRowCount = (mdtRecapitulation.Rows.Count * 2) + 9
                End If

                For k As Integer = 0 To intRowCount - 1
                    xRowIdx += 1

                    row = New WorksheetRow()

                    If k < mdtTableExcel.Rows.Count Then

                        Dim dr As DataRow = mdtTableExcel.Rows(k)

                        wcell = New WorksheetCell()
                        wcell.StyleID = "i8c"
                        wcell.Data.Text = xRowIdx + 1
                        row.Cells.Add(wcell)

                        For Each col As DataColumn In mdtTableExcel.Columns
                            wcell = New WorksheetCell()
                            If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
                                wcell.StyleID = "n8wc"
                                wcell.Data.Type = DataType.Number
                            ElseIf col.DataType Is System.Type.GetType("System.Int32") OrElse col.DataType Is System.Type.GetType("System.Int64") Then
                                wcell.StyleID = "i8c"
                            Else
                                wcell.StyleID = "s8c_cc"
                            End If
                            wcell.Data.Text = dr(col.ColumnName)
                            row.Cells.Add(wcell)
                        Next
                        wcell = New WorksheetCell()
                        row.Cells.Add(wcell)
                    Else
                        If k = mdtTableExcel.Rows.Count Then
                            row.Height = 25
                            For Each col As DataColumn In mdtTableExcel.Columns
                                wcell = New WorksheetCell()
                                If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
                                    wcell.StyleID = "n8wc"
                                    wcell.Data.Type = DataType.Number
                                    wcell.Formula = "=SUM(R[-" & mdtTableExcel.Rows.Count & "]C:R[-1]C)"
                                ElseIf col.Ordinal = 0 Then
                                    wcell.StyleID = "s8b"
                                    wcell.MergeAcross = 1
                                    wcell.Data.Text = Language.getMessage(mstrModuleName, 41, "TOTAUX A REPORTER SUR LA FEUILLE SUIVANTE")
                                ElseIf col.Ordinal = 1 OrElse col.Ordinal = 2 Then
                                    Continue For
                                Else
                                    wcell.StyleID = "s8c_cc"
                                End If
                                row.Cells.Add(wcell)
                            Next
                            wcell = New WorksheetCell()
                            row.Cells.Add(wcell)
                        Else
                            wcell = New WorksheetCell()
                            row.Cells.Add(wcell)
                            For Each col As DataColumn In mdtTableExcel.Columns
                                wcell = New WorksheetCell()
                                row.Cells.Add(wcell)
                            Next
                            wcell = New WorksheetCell()
                            row.Cells.Add(wcell)
                        End If
                    End If


                    Dim xReRowIdx As Integer = -1
                    xReRowIdx = Math.Floor(xRowIdx / 2)
                    If mdtRecapitulation IsNot Nothing AndAlso mdtRecapitulation.Rows.Count > xReRowIdx Then
                        Dim dRow As DataRow = mdtRecapitulation.Rows(xReRowIdx)
                        Dim blnPrinted As Boolean = False
                        For Each Recol As DataColumn In mdtRecapitulation.Columns
                            wcell = New WorksheetCell()
                            wcell.StyleID = "s8c_cc"


                            If xRowIdx Mod 2 = 0 AndAlso blnPrinted = False Then
                                wcell.Data.Text = xReRowIdx + 1
                                wcell.MergeDown = 1
                                row.Cells.Add(wcell)

                                wcell = New WorksheetCell()
                                wcell.StyleID = "s8c_cc"
                                wcell.Data.Text = mdtRecapitulation.Columns("RS").Caption
                                row.Cells.Add(wcell)

                                wcell = New WorksheetCell()
                                If Recol.DataType Is System.Type.GetType("System.Decimal") OrElse Recol.DataType Is System.Type.GetType("System.Double") Then
                                    wcell.StyleID = "n8wc"
                                    wcell.Data.Type = DataType.Number
                                ElseIf Recol.DataType Is System.Type.GetType("System.Int32") OrElse Recol.DataType Is System.Type.GetType("System.Int64") Then
                                    wcell.StyleID = "i8c"
                                Else
                                    wcell.StyleID = "s8c_cc"
                                End If
                                wcell.Data.Text = dRow("RS").ToString()
                                row.Cells.Add(wcell)

                                blnPrinted = True
                            ElseIf xRowIdx Mod 2 = 1 AndAlso blnPrinted = False Then
                                wcell.Data.Text = ""
                                row.Cells.Add(wcell)

                                wcell = New WorksheetCell()
                                wcell.StyleID = "s8c_cc"
                                wcell.Data.Text = mdtRecapitulation.Columns("FNH").Caption
                                row.Cells.Add(wcell)

                                wcell = New WorksheetCell()
                                If Recol.DataType Is System.Type.GetType("System.Decimal") OrElse Recol.DataType Is System.Type.GetType("System.Double") Then
                                    wcell.StyleID = "n8wc"
                                    wcell.Data.Type = DataType.Number
                                ElseIf Recol.DataType Is System.Type.GetType("System.Int32") OrElse Recol.DataType Is System.Type.GetType("System.Int64") Then
                                    wcell.StyleID = "i8c"
                                Else
                                    wcell.StyleID = "s8c_cc"
                                End If
                                wcell.Data.Text = dRow("FNH").ToString()
                                row.Cells.Add(wcell)

                                blnPrinted = True
                            End If


                        Next

                        wcell = New WorksheetCell()
                        wcell.StyleID = "s8c_cc"
                        wcell.MergeDown = 1
                        row.Cells.Add(wcell)

                    End If

                    If mdtRecapitulation.Rows.Count * 2 = xRowIdx Then
                        wcell = New WorksheetCell()
                        wcell.StyleID = "s8c_cc"
                        wcell.MergeDown = 1
                        wcell.Data.Text = "Total"
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell()
                        wcell.StyleID = "s8c_cc"
                        wcell.MergeDown = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell()
                        wcell.StyleID = "n8bcwc"
                        wcell.Formula = "=SUM(R[-" & mdtRecapitulation.Rows.Count * 2 & "]C:R[-1]C)"
                        wcell.MergeDown = 1
                        wcell.Data.Type = DataType.Number
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell()
                        wcell.StyleID = "s8c_cc"
                        wcell.MergeDown = 1
                        row.Cells.Add(wcell)
                    End If

                    If (mdtRecapitulation.Rows.Count * 2) + 2 = xRowIdx Then
                        wcell = New WorksheetCell()
                        row.Cells.Add(wcell)
                    End If

                    If (mdtRecapitulation.Rows.Count * 2) + 3 = xRowIdx Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 47, "Le total des versements (col. 12) doit être A()"), "s10b")
                        wcell.MergeAcross = 3
                        row.Cells.Add(wcell)
                    ElseIf (mdtRecapitulation.Rows.Count * 2) + 4 = xRowIdx Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 48, "égal à celui des retenues (col.10). A défaut,"), "s8b")
                        wcell.MergeAcross = 3
                        row.Cells.Add(wcell)
                    ElseIf (mdtRecapitulation.Rows.Count * 2) + 5 = xRowIdx Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 49, "produire une note expplicative.,"), "s8b")
                        wcell.MergeAcross = 3
                        row.Cells.Add(wcell)
                    ElseIf (mdtRecapitulation.Rows.Count * 2) + 6 = xRowIdx Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 50, "A libreville"), "s10b")
                        wcell.MergeAcross = 3
                        row.Cells.Add(wcell)
                    ElseIf (mdtRecapitulation.Rows.Count * 2) + 7 = xRowIdx Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 51, "Cachet et signature de l'employeur"), "s8b")
                        wcell.MergeAcross = 3
                        row.Cells.Add(wcell)
                    ElseIf (mdtRecapitulation.Rows.Count * 2) + 8 = xRowIdx Then
                        wcell = New WorksheetCell("", "s8b")
                        wcell.MergeAcross = 3
                        row.Cells.Add(wcell)
                    End If

                    rowsArrayHeader.Add(row)

                Next

                If mdtTableExcel.Rows.Count > mdtRecapitulation.Rows.Count Then
                    row = New WorksheetRow()
                    row.Height = 25
                    For Each col As DataColumn In mdtTableExcel.Columns
                        wcell = New WorksheetCell()
                        If col.DataType Is System.Type.GetType("System.Decimal") OrElse col.DataType Is System.Type.GetType("System.Double") Then
                            col.ExtendedProperties.Add("style", "n8wc")
                            wcell.StyleID = "n8wc"
                            wcell.Data.Type = DataType.Number
                            wcell.Formula = "=SUM(R[-" & mdtTableExcel.Rows.Count & "]C:R[-1]C)"
                        ElseIf col.Ordinal = 0 Then
                            col.ExtendedProperties.Add("style", "i8c")
                            wcell.StyleID = "s8b"
                            wcell.MergeAcross = 1
                            wcell.Data.Text = Language.getMessage(mstrModuleName, 41, "TOTAUX A REPORTER SUR LA FEUILLE SUIVANTE")
                        ElseIf col.Ordinal = 1 OrElse col.Ordinal = 2 Then
                            Continue For
                        Else
                            wcell.StyleID = "s8c_cc"
                        End If
                        row.Cells.Add(wcell)
                    Next
                    rowsArrayFooter.Add(row)

                End If

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8wc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)



            End If


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                If ii = 0 Then
                    intArrayColumnWidth(ii) = 60
                ElseIf ii = 1 Then
                    intArrayColumnWidth(ii) = 120
                ElseIf ii > 1 Then
                    intArrayColumnWidth(ii) = 80
                End If
            Next

            Dim dtTable As DataTable = mdtTableExcel.Clone

            If blnExcelHTML = False Then
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, dtTable, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, marrWorksheetStyle:=arrWorkSheetStyle.ToArray, blnRegenerateROWNO:=True)
            Else
                Call ReportExecute(intCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, strExportReportPath, blnOpenAfterExport, dtTable, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, mstrWorksheetStyle:=strWorkSheetStyle, blnRegenerateROWNO:=True)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE")
			Language.setMessage(mstrModuleName, 2, "DECLARATION DES TRAITEMENTS, SALAIRES, PENSIONS VERSES, ETC…(art. 90 du CGI)")
			Language.setMessage(mstrModuleName, 3, "MINISTERE DE L’ECONOMIE, DU COMMERCE,")
			Language.setMessage(mstrModuleName, 4, "DE L’INDUSTRIE ET DU TOURISME")
			Language.setMessage(mstrModuleName, 5, "--------------")
			Language.setMessage(mstrModuleName, 6, "Exercice :")
			Language.setMessage(mstrModuleName, 7, "(au titre duquel les retenues ont été effectuées)")
			Language.setMessage(mstrModuleName, 8, "DIRECTION GENERALE DES IMPOTS")
			Language.setMessage(mstrModuleName, 9, "BORDEREAU DETAILLE DES SALAIRES VERSES AUX EMPLOYES")
			Language.setMessage(mstrModuleName, 10, "BP 37 / 45 – Libreville")
			Language.setMessage(mstrModuleName, 11, "Tel : 79.53.76/77")
			Language.setMessage(mstrModuleName, 12, "NOM DE L’ENTREPRISE OU RAISON SOCIALE :")
			Language.setMessage(mstrModuleName, 13, "NIF :")
			Language.setMessage(mstrModuleName, 14, "Feuillet n° I    I   I")
			Language.setMessage(mstrModuleName, 15, "La présente déclaration est à déposer avant le 30 avril simultanément aux déclarations n° ID01 ou ID14")
			Language.setMessage(mstrModuleName, 16, "EFFECTIFS")
			Language.setMessage(mstrModuleName, 17, "n°")
			Language.setMessage(mstrModuleName, 22, "PRESENCE AU GABON")
			Language.setMessage(mstrModuleName, 23, "Salaire brut(congés) #10; (5)")
			Language.setMessage(mstrModuleName, 24, "Total #10; (1 à 5) #10; (6)")
			Language.setMessage(mstrModuleName, 25, "IMPÔTS RETENUS A LA SOURCE EN 20__")
			Language.setMessage(mstrModuleName, 28, "Salaires brut présence #10; (1)")
			Language.setMessage(mstrModuleName, 29, "AVANTAGES EN NATURE")
			Language.setMessage(mstrModuleName, 31, "TCS #10; (7)")
			Language.setMessage(mstrModuleName, 32, "IRPP #10; (8)")
			Language.setMessage(mstrModuleName, 33, "FNH #10; (9)")
			Language.setMessage(mstrModuleName, 34, "TOTAL #10; (7+8+9) #10; (10)")
			Language.setMessage(mstrModuleName, 35, "Indemnités non imposables")
			Language.setMessage(mstrModuleName, 36, "LOGEMENT Eau Electricité Domesticité #10; (2)")
			Language.setMessage(mstrModuleName, 37, "Nourriture #10; (3)")
			Language.setMessage(mstrModuleName, 38, "Indemnités imposables #10; (4)")
			Language.setMessage(mstrModuleName, 39, "RS")
			Language.setMessage(mstrModuleName, 40, "FNH")
			Language.setMessage(mstrModuleName, 41, "TOTAUX A REPORTER SUR LA FEUILLE SUIVANTE")
			Language.setMessage(mstrModuleName, 42, "RECAPITULATION DES RETENUES A LA SOURCE")
			Language.setMessage(mstrModuleName, 43, "Recette des Impôts de ………………………")
			Language.setMessage(mstrModuleName, 44, "mois")
			Language.setMessage(mstrModuleName, 45, "Montant des retenues #10; (12)")
			Language.setMessage(mstrModuleName, 46, "Date et n° de quittance")
			Language.setMessage(mstrModuleName, 47, "Le total des versements (col. 12) doit être A()")
			Language.setMessage(mstrModuleName, 48, "égal à celui des retenues (col.10). A défaut,")
			Language.setMessage(mstrModuleName, 49, "produire une note expplicative.,")
			Language.setMessage(mstrModuleName, 50, "A libreville")
			Language.setMessage(mstrModuleName, 51, "Cachet et signature de l'employeur")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
