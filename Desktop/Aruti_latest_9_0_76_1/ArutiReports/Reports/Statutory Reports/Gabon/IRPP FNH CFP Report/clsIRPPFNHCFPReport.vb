﻿'************************************************************************************************************************************
'Class Name : clsIRPPFNHCFPReport.vb
'Purpose    :
'Date       :01/02/2021
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsIRPPFNHCFPReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsIRPPFNHCFPReport"
    Private mstrReportId As String = enArutiReport.IRPP_FNH_CFP_Report

    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

#End Region

#Region " Properties "

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Private mstrPeriodIds As String = ""
    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Private mstrPeriodNames As String = String.Empty
    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Private mstrLastPeriodYearName As String = String.Empty
    Public WriteOnly Property _LastPeriodYearName() As String
        Set(ByVal value As String)
            mstrLastPeriodYearName = value
        End Set
    End Property

    Private mintPersonalTaxHeadId As Integer = 0
    Public WriteOnly Property _PersonalTaxHeadId() As Integer
        Set(ByVal value As Integer)
            mintPersonalTaxHeadId = value
        End Set
    End Property

    Private mstrPersonalTaxHeadName As String = String.Empty
    Public WriteOnly Property _PersonalTaxHeadName() As String
        Set(ByVal value As String)
            mstrPersonalTaxHeadName = value
        End Set
    End Property

    Private mintAdditionalTaxHeadId As Integer = 0
    Public WriteOnly Property _AdditionalTaxHeadId() As Integer
        Set(ByVal value As Integer)
            mintAdditionalTaxHeadId = value
        End Set
    End Property

    Private mstrAdditionalTaxHeadName As String = String.Empty
    Public WriteOnly Property _AdditionalTaxHeadName() As String
        Set(ByVal value As String)
            mstrAdditionalTaxHeadName = value
        End Set
    End Property

    Private mintHousingFundLevyHeadId As Integer = 0
    Public WriteOnly Property _HousingFundLevyHeadId() As Integer
        Set(ByVal value As Integer)
            mintHousingFundLevyHeadId = value
        End Set
    End Property

    Private mstrHousingFundLevyHeadName As String = String.Empty
    Public WriteOnly Property _HousingFundLevyHeadName() As String
        Set(ByVal value As String)
            mstrHousingFundLevyHeadName = value
        End Set
    End Property




    Private mblnIncludeInactiveEmp As Boolean = True
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Private mblnApplyUserAccessFilter As Boolean = True
    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    'For Analysis By
    Private mintViewIndex As Integer = -1
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Private mstrViewByIds As String = String.Empty
    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Private mstrViewByName As String = String.Empty
    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Private mstrAnalysis_Fields As String = ""
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Private mstrAnalysis_Join As String = ""
    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Private mstrAnalysis_OrderBy As String = ""
    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Private mstrReport_GroupName As String = ""
    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mstrPeriodIds = ""
            mstrPeriodNames = ""
            mstrLastPeriodYearName = ""
            mintPersonalTaxHeadId = 0
            mstrPersonalTaxHeadName = ""
            mintAdditionalTaxHeadId = 0
            mstrAdditionalTaxHeadName = ""
            mintHousingFundLevyHeadId = 0
            mstrHousingFundLevyHeadName = ""

            mblnIncludeInactiveEmp = True
            mblnApplyUserAccessFilter = True

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@personaltaxheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPersonalTaxHeadId)
            objDataOperation.AddParameter("@additionaltaxheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdditionalTaxHeadId)
            objDataOperation.AddParameter("@housingfundlevyheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHousingFundLevyHeadId)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objCompany._Companyunkid = xCompanyUnkid
            objConfig._Companyunkid = xCompanyUnkid
            Dim strCityCode As String = ""
            If objCompany._Cityunkid > 0 Then
                Dim objCity As New clscity_master
                objCity._Cityunkid = objCompany._Cityunkid
                strCityCode = objCity._Code
                objCity = Nothing
            End If
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, objCompany._Name, objCompany._Registerdno, objCompany._Tinno, (objCompany._Address1 & " " & objCompany._Address2).Trim, objCompany._District, strCityCode, objCompany._City_Name, objCompany._Email, objCompany._Website, objCompany._Phone1, objCompany._Fax, objConfig._CurrencyFormat)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = Nothing
                Dim strarrGroupColumns As String() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell
                Dim mintTotColumns As Integer = 18

                If mdtTableExcel IsNot Nothing Then
                    Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

                    If mintViewIndex > 0 Then
                        Dim strGrpCols As String() = {"column12", "column6"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    Else
                        Dim strGrpCols As String() = {"column6"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    End If

                    ReDim intArrayColumnWidth(mintTotColumns - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        If i >= 3 AndAlso i <= 10 Then
                            intArrayColumnWidth(i) = 20
                        ElseIf i >= 12 AndAlso i <= 16 Then
                            intArrayColumnWidth(i) = 30
                        Else
                            intArrayColumnWidth(i) = 80
                        End If
                    Next

                    '*******   REPORT HEADER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE"), "s8bctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 12

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "ID 10"), "s8bctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "MINISTERE DE L’ECONOMIE, DU COMMERCE,"), "s8bctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 14
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "DE L’INDUSTRIE ET DU TOURISME"), "s8bctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 8

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Date de réception ou cachet du service"), "s8c")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 5
                    wcell.MergeDown = 4
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "_ _ _ _ _ _ _ _ _ _"), "s8bctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "DIRECTION GENERALE DES IMPOTS"), "s8bctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "BP 37 / 45 - LIBREVILLE"), "s8ctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "TEL : 01.79.53.76 OU 01.79.53.77"), "s8ctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "DECLARATION"), "s9bcw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "(A établir en deux exemplaires)"), "s8ctw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "RETENUES A LA SOURCE SUR LES SALAIRES"), "s9bcw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Exercice :"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell(mstrLastPeriodYearName, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "mois :"), "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(mstrPeriodNames, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "N° du contribuable : "), "s8bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    For i = 0 To objCompany._Tinno.Length - 1
                        If objCompany._Tinno.Substring(i, 1).Trim = "" Then
                            wcell = New WorksheetCell(objCompany._Tinno.Substring(i, 1), "s8cw")
                        Else
                            wcell = New WorksheetCell(objCompany._Tinno.Substring(i, 1), "s8bc")
                        End If
                        row.Cells.Add(wcell)
                    Next
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "(Numéro d'Identification Fiscale (NIF))"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 7
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "1 - Identification du contribuable"), "s9b")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 14

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Cadre réservé à l'administration"), "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, ""), "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell("", "s8bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 11

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    wcell.MergeDown = 7
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Raison sociale :"), "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(objCompany._District, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 11

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Sigle:"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(objCompany._Name, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 11

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Boîte postale :"), "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell((objCompany._Address1 & " " & objCompany._Address2).Trim, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4
                    wcell.MergeDown = 1

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Ville :"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 3

                    wcell = New WorksheetCell(objCompany._City_Name, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 29, "Télécopie :"), "s8w")
                    wcell.Index = 8
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 3

                    wcell = New WorksheetCell(objCompany._Fax, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Téléphone :"), "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(objCompany._Phone1, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 30, "Site Internet :"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 3

                    wcell = New WorksheetCell(objCompany._Website, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Adresse e-mail :"), "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(objCompany._Email, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 31, "Code de résidence :"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 3

                    wcell = New WorksheetCell(strCityCode, "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 32, "(Résidence d'affectation de l'impôt)"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 6
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 14
                    wcell.MergeDown = 2

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 33, "(Confirmation du code de résidence ou inscription du code correct)"), "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    wcell.MergeDown = 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "2 – Détermination des retenues sur salaires à payer"), "s9b")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 36, "Désignation de l’impôt"), "s8bc")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 13

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Montant des retenues"), "s8bc")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 35, "Code rubrique"), "s8bc")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    Dim decTotal As Decimal = 0
                    For Each dtRow As DataRow In mdtTableExcel.Rows
                        row = New WorksheetRow()

                        If dtRow.Item("Column4").ToString = "1" Then
                            wcell = New WorksheetCell(dtRow.Item("Column2").ToString, "s8bc")
                        Else
                            wcell = New WorksheetCell(dtRow.Item("Column2").ToString, "s8c")
                        End If
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = 13

                        wcell = New WorksheetCell(dtRow.Item("Column3").ToString, "s8br")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = 2

                        wcell = New WorksheetCell(dtRow.Item("Column1").ToString, "s8c")
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)
                        '----------------------

                        decTotal += CDec(dtRow.Item("Column3"))
                    Next

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 38, "Montant global dû :"), "s8bc")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 13

                    wcell = New WorksheetCell(Format(decTotal, objConfig._CurrencyFormat), "s8br")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 39, "3 - Règlement de l'impôt"), "s9b")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "Mode de versement:"), "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 41, "- espèces :"), "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 6

                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell("", "s8_r")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 42, "- chèque   :"), "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 6

                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 43, "n° de chèque :"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell("", "s8_r")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 44, "Banque :"), "s8b_l")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8_r")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 45, "virement "), "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 6

                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 46, "date :"), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell("", "s8_r")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 4
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 47, "IBAN "), "s8_r")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_l")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 48, "BIC "), "s8_r")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 2
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_t")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 49, "n° de quittance :"), "s8c")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 5

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 51, "Fait à"), "s8_ltb")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell(objCompany._City_Name, "s8_tb")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8_tb")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1

                    wcell = New WorksheetCell("", "s8_rtb")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 1
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 50, "Cachet de la Recette"), "s8c")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 5

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 53, "Signature et cachet du contribuable"), "s8c")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 8
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 2
                    wcell.MergeDown = 4

                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 5

                    wcell = New WorksheetCell("", "s8")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = 8
                    wcell.MergeDown = 4
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8_t")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 54, "Rappel :"), "s8bw")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 55, "Le dépôt de la déclaration doit obligatoirement s'accompagner du paiement de l'impôt dû."), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 3
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 52, "Ne pas globaliser les versements. Etablir une déclaration par résidence fiscale (commune ou département)."), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 3
                    rowsArrayHeader.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 56, "Date limite de dépôt (art. 95, 96 et 206 du CGI) : le 15 de chaque suivant le paiement des salaires."), "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 3
                    rowsArrayHeader.Add(row)
                    '----------------------

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mintTotColumns - 1
                    rowsArrayFooter.Add(row)
                    '----------------------
                End If

                Dim dtTable As DataTable = mdtTableExcel
                If mdtTableExcel IsNot Nothing Then
                    dtTable = New DataView(mdtTableExcel, " 1 = 2 ", "", DataViewRowState.CurrentRows).ToTable
                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, dtTable, intArrayColumnWidth, False, False, False, strarrGroupColumns, "", "", "", Nothing, "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String _
                                           , ByVal intUserUnkid As Integer _
                                           , ByVal intYearUnkid As Integer _
                                           , ByVal intCompanyUnkid As Integer _
                                           , ByVal dtPeriodEnd As Date _
                                           , ByVal strUserModeSetting As String _
                                           , ByVal blnOnlyApproved As Boolean _
                                           , ByVal intBaseCurrencyId As Integer _
                                           , ByVal strCompanyName As String _
                                           , ByVal strCompanyRegNo As String _
                                           , ByVal strCompanyTinNo As String _
                                           , ByVal strCompanyAddress As String _
                                           , ByVal strCompanyDistrict As String _
                                           , ByVal strCompanyCityCode As String _
                                           , ByVal strCompanyCityName As String _
                                           , ByVal strCompanyEmail As String _
                                           , ByVal strCompanyWebsite As String _
                                           , ByVal strCompanyTelephone As String _
                                           , ByVal strCompanyFax As String _
                                           , ByVal strFmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String
        Dim decTotalPaye As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            Dim xUACQry, xUACFiltrQry As String


            xUACQry = "" : xUACFiltrQry = ""

            If mblnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            End If

            StrQ &= "SELECT hremployee_master.employeeunkid INTO #TableEmp FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= " "


            StrQ &= ""

            Dim i As Integer = -1
            Dim strCurrDatabaseName As String = strDatabaseName
            Dim StrInnerQ As String = ""



            StrInnerQ &= "SELECT    payperiodunkid " & _
                                              ", " & frmIRPPFNHCFPReport.enHeadTypeId.PersonalTax & " AS Id " & _
                                              ", @PersonalTax AS Name " & _
                                              ", SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS Amount " & _
                                              ", 0 AS isbold " & _
                                     "FROM      #TableEmp " & _
                                               "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                               "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                "AND prtranhead_master.tranheadunkid = @personaltaxheadid " & _
                                    "GROUP BY  prtnaleave_tran.payperiodunkid "

            StrInnerQ &= "                      UNION ALL " & _
                                      "SELECT    payperiodunkid " & _
                                              ", " & frmIRPPFNHCFPReport.enHeadTypeId.AdditionalTax & " AS Id " & _
                                              ", @AdditionalTax AS Name " & _
                                              ", SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS Amount " & _
                                              ", 0 AS isbold " & _
                                          "FROM      #TableEmp " & _
                                                    "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                    "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                "AND prtranhead_master.tranheadunkid = @additionaltaxheadid " & _
                                    "GROUP BY  prtnaleave_tran.payperiodunkid "

            StrInnerQ &= "                      UNION ALL " & _
                                      "SELECT    payperiodunkid " & _
                                              ", " & frmIRPPFNHCFPReport.enHeadTypeId.HousingFundLevy & " AS Id " & _
                                              ", @HousingFundLevy AS Name " & _
                                              ", SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS Amount " & _
                                              ", 0 AS isbold " & _
                                          "FROM      #TableEmp " & _
                                                    "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                    "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            '

            StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                "AND prtranhead_master.tranheadunkid = @housingfundlevyheadid " & _
                                    "GROUP BY  prtnaleave_tran.payperiodunkid "



            Call FilterTitleAndFilterQuery()

            StrQ &= StrInnerQ

            StrQ &= " DROP TABLE #TableEmp "

            objDataOperation.AddParameter("@PersonalTax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 57, "Impôt sur le Revenu des Personnes Physiques :"))
            objDataOperation.AddParameter("@AdditionalTax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 58, "Taxe complémentaire sur les Traitements et Salaires :"))
            objDataOperation.AddParameter("@HousingFundLevy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 59, "Prélèvement pour le Fonds National de l’Habitat :"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing

            Dim decTotal As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = ""
                rpt_Row.Item("Column2") = dtRow.Item("Name").ToString
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("Amount")), strFmtCurrency)

                rpt_Row.Item("Column4") = dtRow.Item("isbold").ToString

                For i = 0 To strCompanyTinNo.Length - 1
                    rpt_Row.Item("Column5" & i + 1) = strCompanyTinNo.Substring(i, 1)
                Next

                decTotal += CDec(Format(CDec(dtRow.Item("Amount")), strFmtCurrency))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptIRPPFNHCFPReport

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblRepubliqueGabonaise", Language.getMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE"))
            ReportFunction.TextChange(objRpt, "lblMinisteredeleconomiedela", Language.getMessage(mstrModuleName, 2, "MINISTERE DE L’ECONOMIE, DU COMMERCE,"))
            ReportFunction.TextChange(objRpt, "lblProspectiveetdelaprogrammation", Language.getMessage(mstrModuleName, 3, "DE L’INDUSTRIE ET DU TOURISME"))
            ReportFunction.TextChange(objRpt, "lblDash", Language.getMessage(mstrModuleName, 6, "_ _ _ _ _ _ _ _ _ _"))
            ReportFunction.TextChange(objRpt, "lblDirectiongeneraledesimpots", Language.getMessage(mstrModuleName, 7, "DIRECTION GENERALE DES IMPOTS"))
            ReportFunction.TextChange(objRpt, "lblAddress1", Language.getMessage(mstrModuleName, 8, "BP 37 / 45 - LIBREVILLE"))
            ReportFunction.TextChange(objRpt, "lblAddress2", Language.getMessage(mstrModuleName, 9, "TEL : 01.79.53.76 OU 01.79.53.77"))

            ReportFunction.TextChange(objRpt, "lblID", Language.getMessage(mstrModuleName, 10, "ID 10"))
            ReportFunction.TextChange(objRpt, "lblDatedereception", Language.getMessage(mstrModuleName, 11, "Date de réception ou cachet du service"))

            ReportFunction.TextChange(objRpt, "lblDeclaration", Language.getMessage(mstrModuleName, 13, "DECLARATION"))
            ReportFunction.TextChange(objRpt, "lblEtablirendeuxexemplaires", Language.getMessage(mstrModuleName, 14, "(A établir en deux exemplaires)"))
            ReportFunction.TextChange(objRpt, "lblRetenues", Language.getMessage(mstrModuleName, 15, "RETENUES A LA SOURCE SUR LES SALAIRES"))
            ReportFunction.TextChange(objRpt, "lblExercice", Language.getMessage(mstrModuleName, 16, "Exercice :"))
            ReportFunction.TextChange(objRpt, "txtYear", mstrLastPeriodYearName)
            ReportFunction.TextChange(objRpt, "lblMois", Language.getMessage(mstrModuleName, 17, "mois :"))
            ReportFunction.TextChange(objRpt, "txtPeriodName", mstrPeriodNames)

            ReportFunction.TextChange(objRpt, "lblTaxPayerNo", Language.getMessage(mstrModuleName, 18, "N° du contribuable : "))
            ReportFunction.TextChange(objRpt, "lblTINNo", Language.getMessage(mstrModuleName, 19, "(Numéro d'Identification Fiscale (NIF))"))
            ReportFunction.TextChange(objRpt, "txtTinNo", "")

            ReportFunction.TextChange(objRpt, "lblIdentification", Language.getMessage(mstrModuleName, 20, "1 - Identification du contribuable"))
            ReportFunction.TextChange(objRpt, "lblAdministration", Language.getMessage(mstrModuleName, 21, "Cadre réservé à l'administration"))
            ReportFunction.TextChange(objRpt, "lblRaisonSociale", Language.getMessage(mstrModuleName, 23, "Raison sociale :"))
            ReportFunction.TextChange(objRpt, "txtCompanyDistrict", strCompanyDistrict)
            ReportFunction.TextChange(objRpt, "lblBoîtepostale", Language.getMessage(mstrModuleName, 24, "Boîte postale :"))
            ReportFunction.TextChange(objRpt, "txtCompanyAddress", strCompanyAddress)
            ReportFunction.TextChange(objRpt, "lblTelephone", Language.getMessage(mstrModuleName, 25, "Téléphone :"))
            ReportFunction.TextChange(objRpt, "txtCompanyTelephone", strCompanyTelephone)
            ReportFunction.TextChange(objRpt, "lblEmailAddress", Language.getMessage(mstrModuleName, 26, "Adresse e-mail :"))
            ReportFunction.TextChange(objRpt, "txtCompanyEmail", strCompanyEmail)
            ReportFunction.TextChange(objRpt, "lblSigle", Language.getMessage(mstrModuleName, 27, "Sigle:"))
            ReportFunction.TextChange(objRpt, "txtCompanyName2", strCompanyName)
            ReportFunction.TextChange(objRpt, "lblVille", Language.getMessage(mstrModuleName, 28, "Ville :"))
            ReportFunction.TextChange(objRpt, "txtCompanyCity", strCompanyCityName)
            ReportFunction.TextChange(objRpt, "lvlTelecopie", Language.getMessage(mstrModuleName, 29, "Télécopie :"))
            ReportFunction.TextChange(objRpt, "txtCompanyFax", strCompanyFax)
            ReportFunction.TextChange(objRpt, "lblSiteInternet", Language.getMessage(mstrModuleName, 30, "Site Internet :"))
            ReportFunction.TextChange(objRpt, "txtCompanyWebsite", strCompanyWebsite)
            ReportFunction.TextChange(objRpt, "lblCompanyCityCode", Language.getMessage(mstrModuleName, 31, "Code de résidence :"))
            ReportFunction.TextChange(objRpt, "txtCompanyCityCode", strCompanyCityCode)
            ReportFunction.TextChange(objRpt, "lblResidence", Language.getMessage(mstrModuleName, 32, "(Résidence d'affectation de l'impôt)"))
            ReportFunction.TextChange(objRpt, "lblConfirmation", Language.getMessage(mstrModuleName, 33, "(Confirmation du code de résidence ou inscription du code correct)"))

            ReportFunction.TextChange(objRpt, "lblIdentification2", Language.getMessage(mstrModuleName, 34, "2 – Détermination des retenues sur salaires à payer"))
            ReportFunction.TextChange(objRpt, "lblCoderubrique", Language.getMessage(mstrModuleName, 35, "Code rubrique"))
            ReportFunction.TextChange(objRpt, "lblDesignation", Language.getMessage(mstrModuleName, 36, "Désignation de l’impôt"))
            ReportFunction.TextChange(objRpt, "lblMontant", Language.getMessage(mstrModuleName, 37, "Montant des retenues"))

            ReportFunction.TextChange(objRpt, "lblMontantGlobal", Language.getMessage(mstrModuleName, 38, "Montant global dû :"))
            ReportFunction.TextChange(objRpt, "txtMontantGlobal", Format(decTotal, strFmtCurrency))

            ReportFunction.TextChange(objRpt, "lblReglement", Language.getMessage(mstrModuleName, 39, "3 - Règlement de l'impôt"))
            ReportFunction.TextChange(objRpt, "lblModedeVersement", Language.getMessage(mstrModuleName, 40, "Mode de versement:"))
            ReportFunction.TextChange(objRpt, "lblEspeces", Language.getMessage(mstrModuleName, 41, "- espèces :"))
            ReportFunction.TextChange(objRpt, "lblCheque", Language.getMessage(mstrModuleName, 42, "- chèque   :"))
            ReportFunction.TextChange(objRpt, "lblNdeCheque", Language.getMessage(mstrModuleName, 43, "n° de chèque :"))
            ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 44, "Banque :"))
            ReportFunction.TextChange(objRpt, "lblTransfer", Language.getMessage(mstrModuleName, 45, "virement "))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 46, "date :"))
            ReportFunction.TextChange(objRpt, "lblIBAN", Language.getMessage(mstrModuleName, 47, "IBAN "))
            ReportFunction.TextChange(objRpt, "lblBIC", Language.getMessage(mstrModuleName, 48, "BIC "))

            ReportFunction.TextChange(objRpt, "lblReceiptNumber", Language.getMessage(mstrModuleName, 49, "n° de quittance :"))
            ReportFunction.TextChange(objRpt, "lblStamp", Language.getMessage(mstrModuleName, 50, "Cachet de la Recette"))
            ReportFunction.TextChange(objRpt, "lblFait", Language.getMessage(mstrModuleName, 51, "Fait à"))
            ReportFunction.TextChange(objRpt, "txtCity", strCompanyCityName)
            ReportFunction.TextChange(objRpt, "lblFooter3", Language.getMessage(mstrModuleName, 52, "Ne pas globaliser les versements. Etablir une déclaration par résidence fiscale (commune ou département)."))
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 53, "Signature et cachet du contribuable"))
            ReportFunction.TextChange(objRpt, "lblRecall", Language.getMessage(mstrModuleName, 54, "Rappel :"))
            ReportFunction.TextChange(objRpt, "lblFooter1", Language.getMessage(mstrModuleName, 55, "Le dépôt de la déclaration doit obligatoirement s'accompagner du paiement de l'impôt dû."))
            ReportFunction.TextChange(objRpt, "lblFooter2", Language.getMessage(mstrModuleName, 56, "Date limite de dépôt (art. 95, 96 et 206 du CGI) : le 15 de chaque suivant le paiement des salaires."))

            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = New DataView(rpt_Data.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

                Dim mintColumn As Integer = 0
                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                mdtTableExcel.Columns(mintColumn).Caption = ""
                mintColumn += 1

                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mdtTableExcel.Columns(mintColumn).Caption = ""
                mintColumn += 1

                mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                mdtTableExcel.Columns(mintColumn).Caption = ""
                mintColumn += 1

                mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                mdtTableExcel.Columns(mintColumn).Caption = ""
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column12").SetOrdinal(mintColumn)
                    mdtTableExcel.Columns(mintColumn).Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Replace(":", "").Trim, mstrReport_GroupName)
                    mintColumn += 1
                End If

                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (18 Jan 2017) -- End
#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE")
			Language.setMessage(mstrModuleName, 2, "MINISTERE DE L’ECONOMIE, DU COMMERCE,")
			Language.setMessage(mstrModuleName, 3, "DE L’INDUSTRIE ET DU TOURISME")
			Language.setMessage(mstrModuleName, 6, "_ _ _ _ _ _ _ _ _ _")
			Language.setMessage(mstrModuleName, 7, "DIRECTION GENERALE DES IMPOTS")
			Language.setMessage(mstrModuleName, 8, "BP 37 / 45 - LIBREVILLE")
			Language.setMessage(mstrModuleName, 9, "TEL : 01.79.53.76 OU 01.79.53.77")
			Language.setMessage(mstrModuleName, 10, "ID 10")
			Language.setMessage(mstrModuleName, 11, "Date de réception ou cachet du service")
			Language.setMessage(mstrModuleName, 13, "DECLARATION")
			Language.setMessage(mstrModuleName, 14, "(A établir en deux exemplaires)")
			Language.setMessage(mstrModuleName, 15, "RETENUES A LA SOURCE SUR LES SALAIRES")
			Language.setMessage(mstrModuleName, 16, "Exercice :")
			Language.setMessage(mstrModuleName, 17, "mois :")
			Language.setMessage(mstrModuleName, 18, "N° du contribuable :")
			Language.setMessage(mstrModuleName, 19, "(Numéro d'Identification Fiscale (NIF))")
			Language.setMessage(mstrModuleName, 20, "1 - Identification du contribuable")
			Language.setMessage(mstrModuleName, 21, "Cadre réservé à l'administration")
			Language.setMessage(mstrModuleName, 22, "")
			Language.setMessage(mstrModuleName, 23, "Raison sociale :")
			Language.setMessage(mstrModuleName, 24, "Boîte postale :")
			Language.setMessage(mstrModuleName, 25, "Téléphone :")
			Language.setMessage(mstrModuleName, 26, "Adresse e-mail :")
			Language.setMessage(mstrModuleName, 27, "Sigle:")
			Language.setMessage(mstrModuleName, 28, "Ville :")
			Language.setMessage(mstrModuleName, 29, "Télécopie :")
			Language.setMessage(mstrModuleName, 30, "Site Internet :")
			Language.setMessage(mstrModuleName, 31, "Code de résidence :")
			Language.setMessage(mstrModuleName, 32, "(Résidence d'affectation de l'impôt)")
			Language.setMessage(mstrModuleName, 33, "(Confirmation du code de résidence ou inscription du code correct)")
			Language.setMessage(mstrModuleName, 34, "2 – Détermination des retenues sur salaires à payer")
			Language.setMessage(mstrModuleName, 35, "Code rubrique")
			Language.setMessage(mstrModuleName, 36, "Désignation de l’impôt")
			Language.setMessage(mstrModuleName, 37, "Montant des retenues")
			Language.setMessage(mstrModuleName, 38, "Montant global dû :")
			Language.setMessage(mstrModuleName, 39, "3 - Règlement de l'impôt")
			Language.setMessage(mstrModuleName, 40, "Mode de versement:")
			Language.setMessage(mstrModuleName, 41, "- espèces :")
			Language.setMessage(mstrModuleName, 42, "- chèque   :")
			Language.setMessage(mstrModuleName, 43, "n° de chèque :")
			Language.setMessage(mstrModuleName, 44, "Banque :")
			Language.setMessage(mstrModuleName, 45, "virement")
			Language.setMessage(mstrModuleName, 46, "date :")
			Language.setMessage(mstrModuleName, 47, "IBAN")
			Language.setMessage(mstrModuleName, 48, "BIC")
			Language.setMessage(mstrModuleName, 49, "n° de quittance :")
			Language.setMessage(mstrModuleName, 50, "Cachet de la Recette")
			Language.setMessage(mstrModuleName, 51, "Fait à")
			Language.setMessage(mstrModuleName, 52, "Ne pas globaliser les versements. Etablir une déclaration par résidence fiscale (commune ou département).")
			Language.setMessage(mstrModuleName, 53, "Signature et cachet du contribuable")
			Language.setMessage(mstrModuleName, 54, "Rappel :")
			Language.setMessage(mstrModuleName, 55, "Le dépôt de la déclaration doit obligatoirement s'accompagner du paiement de l'impôt dû.")
			Language.setMessage(mstrModuleName, 56, "Date limite de dépôt (art. 95, 96 et 206 du CGI) : le 15 de chaque suivant le paiement des salaires.")
			Language.setMessage(mstrModuleName, 57, "Impôt sur le Revenu des Personnes Physiques :")
			Language.setMessage(mstrModuleName, 58, "Taxe complémentaire sur les Traitements et Salaires :")
			Language.setMessage(mstrModuleName, 59, "Prélèvement pour le Fonds National de l’Habitat :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
