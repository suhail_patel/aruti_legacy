﻿'************************************************************************************************************************************
'Class Name : frmIRPPFNHCFPReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmIRPPFNHCFPReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmIRPPFNHCFPReport"
    Private objIRPPFNHCFPReport As clsIRPPFNHCFPReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        objIRPPFNHCFPReport = New clsIRPPFNHCFPReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objIRPPFNHCFPReport.SetDefaultValue()
        InitializeComponent()
        _Show_ExcelExtra_Menu = True
    End Sub

#End Region

#Region " Private Enum "
    Public Enum enHeadTypeId
        PersonalTax = 1
        AdditionalTax = 2
        HousingFundLevy = 3
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData

        Dim objMember As New clsmembership_master


        Dim dsCombos As DataSet

        Try


            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)


            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , )
            With cboPersonalTax
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            With cboAdditionalTax
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboHousingFundLevy
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstPeriodId
            cboPersonalTax.SelectedValue = 0
            cboAdditionalTax.SelectedValue = 0
            cboHousingFundLevy.SelectedValue = 0


            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.IRPP_FNH_CFP_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.PersonalTax
                            cboPersonalTax.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.AdditionalTax
                            cboAdditionalTax.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.HousingFundLevy
                            cboHousingFundLevy.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        Try
            objIRPPFNHCFPReport.SetDefaultValue()


            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False

            ElseIf CInt(cboPersonalTax.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select head for Personal Tax."), enMsgBoxStyle.Information)
                cboPersonalTax.Focus()
                Return False
            ElseIf CInt(cboAdditionalTax.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select head for Additional Tax."), enMsgBoxStyle.Information)
                cboAdditionalTax.Focus()
                Return False
            ElseIf CInt(cboHousingFundLevy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select head for Housing Fund Levy."), enMsgBoxStyle.Information)
                cboHousingFundLevy.Focus()
                Return False
            End If



            Dim i As Integer = 0

            Dim strPreriodIds As String = cboPeriod.SelectedValue.ToString
            Dim mstrPeriodsName As String = cboPeriod.Text

            objIRPPFNHCFPReport._PeriodIds = strPreriodIds
            objIRPPFNHCFPReport._PeriodNames = mstrPeriodsName

            objIRPPFNHCFPReport._LastPeriodYearName = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString

            objIRPPFNHCFPReport._PersonalTaxHeadId = CInt(cboPersonalTax.SelectedValue)
            objIRPPFNHCFPReport._PersonalTaxHeadName = cboPersonalTax.Text

            objIRPPFNHCFPReport._AdditionalTaxHeadId = CInt(cboAdditionalTax.SelectedValue)
            objIRPPFNHCFPReport._AdditionalTaxHeadName = cboAdditionalTax.Text

            objIRPPFNHCFPReport._HousingFundLevyHeadId = CInt(cboHousingFundLevy.SelectedValue)
            objIRPPFNHCFPReport._HousingFundLevyHeadName = cboHousingFundLevy.Text

            objIRPPFNHCFPReport._ViewByIds = mstrStringIds
            objIRPPFNHCFPReport._ViewIndex = mintViewIdx
            objIRPPFNHCFPReport._ViewByName = mstrStringName
            objIRPPFNHCFPReport._Analysis_Fields = mstrAnalysis_Fields
            objIRPPFNHCFPReport._Analysis_Join = mstrAnalysis_Join
            objIRPPFNHCFPReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objIRPPFNHCFPReport._Report_GroupName = mstrReport_GroupName


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmIRPPFNHCFPReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objIRPPFNHCFPReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIRPPFNHCFPReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIRPPFNHCFPReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call frmIRPPFNHCFPReport_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmIRPPFNHCFPReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIRPPFNHCFPReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objIRPPFNHCFPReport._ReportName
            Me._Message = objIRPPFNHCFPReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIRPPFNHCFPReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIRPPFNHCFPReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsIRPPFNHCFPReport.SetMessages()
            objfrm._Other_ModuleNames = "clsIRPPFNHCFPReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmIRPPFNHCFPReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub frmIRPPFNHCFPReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objIRPPFNHCFPReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objIRPPFNHCFPReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmIRPPFNHCFPReport_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmIRPPFNHCFPReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objIRPPFNHCFPReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objIRPPFNHCFPReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmIRPPFNHCFPReport_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.IRPP_FNH_CFP_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.PersonalTax
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPersonalTax.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.IRPP_FNH_CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.AdditionalTax
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAdditionalTax.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.IRPP_FNH_CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.HousingFundLevy
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboHousingFundLevy.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.IRPP_FNH_CFP_Report, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub frmIRPPFNHCFPReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIRPPFNHCFPReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox' Events "

    Private Sub cboBasicSalary_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPersonalTax.KeyPress, _
                                                                                                                            cboAdditionalTax.KeyPress, _
                                                                                                                            cboHousingFundLevy.KeyPress

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, ComboBox).Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboBasicSalary_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPersonalTax.SelectedIndexChanged, _
    '                                                                                                                    cboAdditionalTax.SelectedIndexChanged, _
    '                                                                                                                    cboHousingFundLevy.SelectedIndexChanged, _

    '    Try

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub cboBasicSalary_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboPersonalTax.Validating, _
                                                                                                                        cboAdditionalTax.Validating, _
                                                                                                                        cboHousingFundLevy.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, ComboBox).Name & "_Validating", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Link Button Events "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblHousingFundLevy.Text = Language._Object.getCaption(Me.lblHousingFundLevy.Name, Me.lblHousingFundLevy.Text)
			Me.lblAdditionalTax.Text = Language._Object.getCaption(Me.lblAdditionalTax.Name, Me.lblAdditionalTax.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblPersonalTax.Text = Language._Object.getCaption(Me.lblPersonalTax.Name, Me.lblPersonalTax.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period.")
			Language.setMessage(mstrModuleName, 2, "Please select head for Personal Tax.")
			Language.setMessage(mstrModuleName, 3, "Please select head for Additional Tax.")
			Language.setMessage(mstrModuleName, 4, "Please select head for Housing Fund Levy.")
			Language.setMessage(mstrModuleName, 8, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 9, "Sorry, This transaction head is already mapped.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class