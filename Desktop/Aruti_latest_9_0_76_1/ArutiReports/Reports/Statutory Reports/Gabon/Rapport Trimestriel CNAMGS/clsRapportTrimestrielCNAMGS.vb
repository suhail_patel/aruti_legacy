﻿'************************************************************************************************************************************
'Class Name : clsRapportTrimestrielCNAMGS.vb
'Purpose    :
'Date       :27/10/2021
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text
Imports System.Text.RegularExpressions

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsRapportTrimestrielCNAMGS
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsRapportTrimestrielCNAMGS"
    Private mstrReportId As String = enArutiReport.Rapport_Trimestriel_CNAMGS
    Private objDataOperation As clsDataOperation
    Private mdtFinal As DataTable = Nothing

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Enum "
    Public Enum enYearQuarter
        T1 = 1
        T2 = 2
        T3 = 3
        T4 = 4
    End Enum
#End Region

#Region " Private Variables "

    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintYearQuarterId As Integer = -1
    Private mstrYearQuarterName As String = String.Empty
    Private mstrPeriodIDs As String = String.Empty
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mintPeriodId1 As Integer
    Private mstrPeriodCode1 As String = String.Empty
    Private mstrPeriodName1 As String = String.Empty
    Private mintPeriodId2 As Integer
    Private mstrPeriodCode2 As String = String.Empty
    Private mstrPeriodName2 As String = String.Empty
    Private mintPeriodId3 As Integer
    Private mstrPeriodCode3 As String = String.Empty
    Private mstrPeriodCustomCode3 As String = String.Empty
    Private mstrPeriodName3 As String = String.Empty
    Private mintDaysWorkedId1 As Integer = -1
    Private mstrDaysWorkedName1 As String = String.Empty
    Private mintDaysWorkedId2 As Integer = -1
    Private mstrDaysWorkedName2 As String = String.Empty
    Private mintDaysWorkedId3 As Integer = -1
    Private mstrDaysWorkedName3 As String = String.Empty

    'Pinkal (25-Nov-2021)-- Start
    'Gabon Statutory Report Enhancements. 
    Private mintAssietteId As Integer = -1
    Private mstrAssiettesoumis As String = String.Empty
    'Pinkal (25-Nov-2021)-- End

    Private mdecEmployeurRate As Decimal = 0
    Private mdecTravailleurRate As Decimal = 0
    Private mstrPlafondFormula As String = String.Empty
    Private mblnShowLogo As Boolean = True

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrCurrencyName As String = ""
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty


#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _YearQuarterId() As Integer
        Set(ByVal value As Integer)
            mintYearQuarterId = value
        End Set
    End Property

    Public WriteOnly Property _YearQuarterName() As String
        Set(ByVal value As String)
            mstrYearQuarterName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public Property _PeriodStart() As Date
        Get
            Return mdtPeriodStart
        End Get
        Set(ByVal value As Date)
            mdtPeriodStart = value
        End Set
    End Property

    Public Property _PeriodEnd() As Date
        Get
            Return mdtPeriodEnd
        End Get
        Set(ByVal value As Date)
            mdtPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId1() As Integer
        Set(ByVal value As Integer)
            mintPeriodId1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode1() As String
        Set(ByVal value As String)
            mstrPeriodCode1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName1() As String
        Set(ByVal value As String)
            mstrPeriodName1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId2() As Integer
        Set(ByVal value As Integer)
            mintPeriodId2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode2() As String
        Set(ByVal value As String)
            mstrPeriodCode2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName2() As String
        Set(ByVal value As String)
            mstrPeriodName2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId3() As Integer
        Set(ByVal value As Integer)
            mintPeriodId3 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode3() As String
        Set(ByVal value As String)
            mstrPeriodCode3 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCustomCode3() As String
        Set(ByVal value As String)
            mstrPeriodCustomCode3 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName3() As String
        Set(ByVal value As String)
            mstrPeriodName3 = value
        End Set
    End Property

    Public WriteOnly Property _DaysWorkedId1() As Integer
        Set(ByVal value As Integer)
            mintDaysWorkedId1 = value
        End Set
    End Property

    Public WriteOnly Property _DaysWorkedName1() As String
        Set(ByVal value As String)
            mstrDaysWorkedName1 = value
        End Set
    End Property

    Public WriteOnly Property _DaysWorkedId2() As Integer
        Set(ByVal value As Integer)
            mintDaysWorkedId2 = value
        End Set
    End Property

    Public WriteOnly Property _DaysWorkedName2() As String
        Set(ByVal value As String)
            mstrDaysWorkedName2 = value
        End Set
    End Property

    Public WriteOnly Property _DaysWorkedId3() As Integer
        Set(ByVal value As Integer)
            mintDaysWorkedId3 = value
        End Set
    End Property

    Public WriteOnly Property _DaysWorkedName3() As String
        Set(ByVal value As String)
            mstrDaysWorkedName3 = value
        End Set
    End Property

    'Pinkal (25-Nov-2021)-- Start
    'Gabon Statutory Report Enhancements. 
    Public WriteOnly Property _AssietteId() As Integer
        Set(ByVal value As Integer)
            mintAssietteId = value
        End Set
    End Property

    Public WriteOnly Property _Assiettesoumis() As String
        Set(ByVal value As String)
            mstrAssiettesoumis = value
        End Set
    End Property
    'Pinkal (25-Nov-2021)-- End

    Public WriteOnly Property _EmployeurRate() As Decimal
        Set(ByVal value As Decimal)
            mdecEmployeurRate = value
        End Set
    End Property

    Public WriteOnly Property _TravailleurRate() As Decimal
        Set(ByVal value As Decimal)
            mdecTravailleurRate = value
        End Set
    End Property

    Public WriteOnly Property _PlafondFormula() As String
        Set(ByVal value As String)
            mstrPlafondFormula = value
        End Set
    End Property

    Public WriteOnly Property _ShowLogo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLogo = value
        End Set
    End Property


    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property


    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property



#End Region

#Region " Public Function & Procedures "

    Public Function getComboListForYearQuarter(ByVal blnAddSelect As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect = True Then
                StrQ = "SELECT 0 AS Id ,@Select AS NAME UNION "
            End If

            StrQ &= "SELECT " & enYearQuarter.T1 & " AS Id, @T1 " & _
                   "UNION SELECT " & enYearQuarter.T2 & " AS Id, @T2 " & _
                   "UNION SELECT " & enYearQuarter.T3 & " AS Id, @T3 " & _
                   "UNION SELECT " & enYearQuarter.T4 & " AS Id, @T4 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select Quarter"))
            objDataOperation.AddParameter("@T1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "T1"))
            objDataOperation.AddParameter("@T2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "T2"))
            objDataOperation.AddParameter("@T3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "T3"))
            objDataOperation.AddParameter("@T4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "T4"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboListForYearQuarter; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintYearQuarterId = 0
            mstrYearQuarterName = String.Empty
            mstrPeriodIDs = String.Empty
            mintPeriodId1 = 0
            mstrPeriodCode1 = String.Empty
            mstrPeriodName1 = String.Empty
            mintPeriodId2 = 0
            mstrPeriodCode2 = String.Empty
            mstrPeriodName2 = String.Empty
            mintPeriodId3 = 0
            mstrPeriodCode3 = String.Empty
            mstrPeriodCustomCode3 = String.Empty
            mstrPeriodName3 = String.Empty
            mintDaysWorkedId1 = 0
            mstrDaysWorkedName1 = String.Empty
            mintDaysWorkedId2 = 0
            mstrDaysWorkedName2 = String.Empty
            mintDaysWorkedId3 = 0
            mstrDaysWorkedName3 = String.Empty
            mdecEmployeurRate = 0
            mdecTravailleurRate = 0
            mstrPlafondFormula = String.Empty
            mblnShowLogo = True

            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            mintAssietteId = 0
            mstrAssiettesoumis = ""
            'Pinkal (25-Nov-2021)-- End

            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            mstrCurrencyName = String.Empty

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String _
                                    , ByVal intUserUnkid As Integer _
                                    , ByVal intYearUnkid As Integer _
                                    , ByVal intCompanyUnkid As Integer _
                                    , ByVal dtPeriodStart As Date _
                                    , ByVal dtPeriodEnd As Date _
                                    , ByVal strUserModeSetting As String _
                                    , ByVal blnOnlyApproved As Boolean _
                                    , ByVal dtFinancialYearStart As Date _
                                    , ByVal intBaseCurrencyId As Integer _
                                    , ByVal strFmtCurrency As String _
                                    , ByVal strExportReportPath As String _
                                    , ByVal blnOpenAfterExport As Boolean _
                                    , ByVal blnExcelHTML As Boolean _
                                    )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objMembership As New clsmembership_master
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Dim dsList As New DataSet
        Dim strBaseCurrencySign As String
        Dim colIdx As Integer = 0

        objDataOperation = New clsDataOperation
        Try
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            objMembership._Membershipunkid = mintMembershipId
            objCompany._Companyunkid = intCompanyUnkid
            'objConfig._Companyunkid = xCompanyUnkid

            Dim xUACQry, xUACFiltrQry, xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

            'If mblnApplyUserAccessFilter = True Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'End If
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False)

            Dim objHead As New clsTransactionHead
            Dim objDHead As New Dictionary(Of Integer, String)
            Dim dsHead As DataSet = objHead.getComboList(strDatabaseName, "List", False, , , , True, , , , True, , , True)

            Dim strPattern As String = "([^#]*#[^#]*)#"
            Dim strReplacement As String = "$1)"

            If mstrPlafondFormula.Trim <> "" AndAlso mstrPlafondFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrPlafondFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            StrQ = "SELECT hremployee_master.employeeunkid " & _
                         ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                         ", hremployee_master.employeecode " & _
                         ", ISNULL(ETERM.empl_enddate, '') AS EocDate "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') AS employeeame "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeeame "
            End If

            StrQ &= "INTO #TableEmp " & _
                    "FROM hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT TERM.TEEmpId " & _
                             ", TERM.empl_enddate " & _
                             ", TERM.termination_from_date " & _
                             ", TERM.TEfDt " & _
                             ", TERM.isexclude_payroll " & _
                             ", TERM.changereasonunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT TRM.employeeunkid AS TEEmpId " & _
                                 ", CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                                 ", CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                                 ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                 ", TRM.isexclude_payroll " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                 ", TRM.changereasonunkid " & _
                            "FROM hremployee_dates_tran AS TRM " & _
                            "WHERE isvoid = 0 " & _
                                  "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                  "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @enddate " & _
                        ") AS TERM " & _
                        "WHERE TERM.Rno = 1 " & _
                    ") AS ETERM " & _
                        "ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                           "AND ETERM.TEfDt >= CONVERT(CHAR(8), appointeddate, 112) "


            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If


            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If


            StrQ &= "SELECT A.employeeunkid " & _
                         ", #TableEmp.employeecode " & _
                         ", #TableEmp.employeeame " & _
                         ", #TableEmp.appointeddate " & _
                         ", #TableEmp.EocDate " & _
                         ", SUM(A.amountTwo) AS amountTwo " & _
                         ", SUM(A.amountThree) AS amountThree " & _
                         ", SUM(A.daysworkedOne) AS daysworkedOne " & _
                         ", SUM(A.daysworkedTwo) AS daysworkedTwo " & _
                         ", SUM(A.daysworkedThree) AS daysworkedThree " & _
                         ", SUM(A.empcontribution) AS empcontribution " & _
                         ", MAX(A.MembershipNo) AS MembershipNo " & _
                         ", MAX(A.MemshipCategory) AS MemshipCategory " & _
                         ", ROW_NUMBER() OVER(ORDER BY #TableEmp.employeeame) AS ROWNO "

            For Each itm In objDHead
                StrQ &= " , SUM([" & itm.Key & "]) AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS amountTwo " & _
                             ", 0 AS amountThree " & _
                             ", 0 AS daysworkedOne " & _
                             ", 0 AS daysworkedTwo " & _
                             ", 0 AS daysworkedThree " & _
                             ", 0 AS empcontribution " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS MemshipCategory "

            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "JOIN prtranhead_master " & _
                                "ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtranhead_master.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId2 & ") "

            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            '"AND prtranhead_master.istaxable = 1 " & _
            StrQ &= " AND prtranhead_master.tranheadunkid = " & mintAssietteId
            'Pinkal (25-Nov-2021)-- End

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid " & _
                        "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", 0 AS amountTwo " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS amountThree " & _
                             ", 0 AS daysworkedOne " & _
                             ", 0 AS daysworkedTwo " & _
                             ", 0 AS daysworkedThree " & _
                             ", 0 AS empcontribution " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS MemshipCategory "

            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "JOIN prtranhead_master " & _
                                "ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtranhead_master.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId3 & ") "

            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            '"AND prtranhead_master.istaxable = 1 " & _
            StrQ &= " AND prtranhead_master.tranheadunkid =  " & mintAssietteId
            'Pinkal (25-Nov-2021)-- End

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid " & _
                        "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", 0 AS amountTwo " & _
                             ", 0 AS amountThree " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS daysworkedOne " & _
                             ", 0 AS daysworkedTwo " & _
                             ", 0 AS daysworkedThree " & _
                             ", 0 AS empcontribution " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS MemshipCategory "

            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId1 & ") " & _
                              "AND prpayrollprocess_tran.tranheadunkid = " & mintDaysWorkedId1 & " " & _
                        "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid " & _
                        "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", 0 AS amountTwo " & _
                             ", 0 AS amountThree " & _
                             ", 0 AS daysworkedOne " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS daysworkedTwo " & _
                             ", 0 AS daysworkedThree " & _
                             ", 0 AS empcontribution " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS MemshipCategory "

            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId2 & ") " & _
                              "AND prpayrollprocess_tran.tranheadunkid = " & mintDaysWorkedId2 & " " & _
                        "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid " & _
                        "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", 0 AS amountTwo " & _
                             ", 0 AS amountThree " & _
                             ", 0 AS daysworkedOne " & _
                             ", 0 AS daysworkedTwo " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS daysworkedThree " & _
                             ", 0 AS empcontribution " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS MemshipCategory "

            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId3 & ") " & _
                              "AND prpayrollprocess_tran.tranheadunkid = " & mintDaysWorkedId3 & " " & _
                        "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid " & _
                        "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", 0 AS amountTwo " & _
                             ", 0 AS amountThree " & _
                             ", 0 AS daysworkedOne " & _
                             ", 0 AS daysworkedTwo " & _
                             ", 0 AS daysworkedThree " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                             ", ''  AS MembershipNo " & _
                             ", '' AS MemshipCategory "

            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            '", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MembershipNo " & _
            '", ISNULL(cfcommon_master.name, '') AS MemshipCategory "
            'Pinkal (25-Nov-2021)-- End


            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM #TableEmp " & _
                            "LEFT JOIN prpayrollprocess_tran " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "LEFT JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN cfcommon_period_tran " & _
                                "ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                   "AND cfcommon_period_tran.isactive = 1 "

            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            '"LEFT JOIN hremployee_meminfo_tran " & _
            '    "ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
            '       "AND hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
            '       "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '"LEFT JOIN hrmembership_master " & _
            '    "ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '       "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
            '"LEFT JOIN cfcommon_master " & _
            '    "ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
            '       "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _

            StrQ &= " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            StrQ &= " WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                         " AND prtranhead_master.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId1 & ") " & _
                         " AND prtranhead_master.tranheadunkid =  " & mintAssietteId

            '" AND hrmembership_master.membershipunkid = @MemId "




            StrQ &= " UNION ALL " & _
                         " SELECT #TableEmp.employeeunkid " & _
                         ", " & mintPeriodId1 & "payperiodunkid " & _
                         ", 0 AS amountTwo " & _
                         ", 0 AS amountThree " & _
                         ", 0 AS daysworkedOne " & _
                         ", 0 AS daysworkedTwo " & _
                         ", 0 AS daysworkedThree " & _
                         ", 0 AS empcontribution " & _
                          ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS membershipno " & _
                          ", ISNULL(cfcommon_master.name,'') AS MemshipCategory "

            For Each itm In objDHead
                StrQ &= " , 0 AS [" & itm.Key & "] "
            Next


            StrQ &= " FROM #TableEmp " & _
                         " LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                         " LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                         " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                         " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                         " WHERE hrmembership_master.membershipunkid = @MemId "

            'Pinkal (25-Nov-2021)-- End



            For Each itm In objDHead

                StrQ &= "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", prtnaleave_tran.payperiodunkid " & _
                             ", 0 AS amountTwo " & _
                             ", 0 AS amountThree " & _
                             ", 0 AS daysworkedOne " & _
                             ", 0 AS daysworkedTwo " & _
                             ", 0 AS daysworkedThree " & _
                             ", 0 AS empcontribution " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS MemshipCategory "

                For Each itm1 In objDHead
                    If itm1.Key = itm.Key Then
                        StrQ &= " , SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS [" & itm1.Key & "] "
                    Else
                        StrQ &= " , 0 AS [" & itm1.Key & "] "
                    End If
                Next

                StrQ &= "FROM      #TableEmp " & _
                                    "LEFT JOIN prpayrollprocess_tran ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrQ &= "          WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                            "AND prtranhead_master.tranheadunkid = " & itm.Key & " " & _
                                  "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                                            ", prtnaleave_tran.payperiodunkid "

            Next

            StrQ &= ") AS A " & _
                    "JOIN #TableEmp " & _
                                "ON A.employeeunkid = #TableEmp.employeeunkid " & _
                    "GROUP BY A.employeeunkid " & _
                            ", #TableEmp.employeecode " & _
                            ", #TableEmp.employeeame " & _
                            ", #TableEmp.appointeddate " & _
                            ", #TableEmp.EocDate "



            StrQ &= " DROP TABLE #TableEmp "


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEnd))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dsRow.Item("appointeddate") = Format(eZeeDate.convertDate(dsRow.Item("appointeddate").ToString), "dd-MM-yyyy")
                If dsRow.Item("EocDate").ToString.Trim <> "" Then
                    dsRow.Item("EocDate") = Format(eZeeDate.convertDate(dsRow.Item("EocDate").ToString), "dd-MM-yyyy")
                End If
            Next
            dsList.Tables(0).AcceptChanges()

            Dim decPlafond As Decimal = 0
            If mstrPlafondFormula.Trim <> "" AndAlso mstrPlafondFormula.Contains("#") = True Then
                Dim dt As New DataTable
                Dim s As String = mstrPlafondFormula
                Dim ans As Object = Nothing

                For Each itm In objDHead
                    Dim strHeadid As String = itm.Key.ToString
                    s = s.Replace("#" & itm.Value & "#", Format((From p In dsList.Tables(0) Select CDec((p.Item(strHeadid)))).Sum, strFmtCurrency))
                Next
                Try
                    ans = dt.Compute(s.ToString.Replace(",", ""), "")
                Catch ex As Exception

                End Try
                If ans Is Nothing Then
                    ans = 0
                End If
                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'decPlafond = Format(CDec(ans), strFmtCurrency)
                decPlafond = Format(CDec(ans), strFmtCurrency).Replace(",", "")
                'Pinkal (25-Nov-2021)-- End

            Else
                Decimal.TryParse(mstrPlafondFormula, decPlafond)
            End If

            Dim mdtTableExcel As DataTable
            Dim intColIndex As Integer = -1
            'If menExportAction = enExportAction.ExcelExtra Then
            mdtTableExcel = dsList.Tables(0)

            intColIndex += 1
            mdtTableExcel.Columns("ROWNO").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 6, "N°")

            intColIndex += 1
            mdtTableExcel.Columns("MembershipNo").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 7, "Matricule")

            intColIndex += 1
            mdtTableExcel.Columns("employeeame").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 9, "NOM ET PRENOM")

            intColIndex += 1
            mdtTableExcel.Columns("appointeddate").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 10, "EMBAUCHE")

            intColIndex += 1
            mdtTableExcel.Columns("EocDate").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 11, "CESSATION")

            intColIndex += 1
            mdtTableExcel.Columns("empcontribution").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 12, "Assiette soumis à cotisation")
            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            If blnExcelHTML = False Then
                mdtTableExcel.Columns("empcontribution").ExtendedProperties.Add("style", "n8wc")
            Else
                mdtTableExcel.Columns("empcontribution").ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
            End If
            'Pinkal (25-Nov-2021)-- End


            intColIndex += 1
            mdtTableExcel.Columns("daysworkedOne").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 13, "Nbre Hrs/Jrs")

            intColIndex += 1
            mdtTableExcel.Columns("amountTwo").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 14, "Assiette soumis à cotisation")
            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            If blnExcelHTML = False Then
                mdtTableExcel.Columns("amountTwo").ExtendedProperties.Add("style", "n8wc")
            Else
                mdtTableExcel.Columns("amountTwo").ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
            End If
            'Pinkal (25-Nov-2021)-- End

            intColIndex += 1
            mdtTableExcel.Columns("daysworkedTwo").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 15, "Nbre Hrs/Jrs")

            intColIndex += 1
            mdtTableExcel.Columns("amountThree").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 16, "Assiette soumis à cotisation")
            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            If blnExcelHTML = False Then
                mdtTableExcel.Columns("amountThree").ExtendedProperties.Add("style", "n8wc")
            Else
                mdtTableExcel.Columns("amountThree").ExtendedProperties.Add("style", "mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34))
            End If
            'Pinkal (25-Nov-2021)-- End

            intColIndex += 1
            mdtTableExcel.Columns("daysworkedThree").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 17, "Nbre Hrs/Jrs")

            For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
            Next


            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing
            Dim strBuilder As New StringBuilder
            Dim arrWorkSheetStyle As List(Of WorksheetStyle) = Nothing
            Dim strWorkSheetStyle As String = ""
            Dim intInitRows As Integer = 0

            If blnExcelHTML = False Then
                arrWorkSheetStyle = New List(Of WorksheetStyle)
                Dim s10bcw As New WorksheetStyle("s10bcw")
                With s10bcw
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcw)

                Dim s10bcblue As New WorksheetStyle("s10bcblue")
                With s10bcblue
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Font.Color = "#5877FC"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcblue)

                Dim s10bred As New WorksheetStyle("s10bred")
                With s10bred
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.WrapText = True
                    .Font.Color = "#FF0000"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bred)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                Dim n8wc As New WorksheetStyle("n8wc")
                With n8wc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8wc)

                Dim n8bwc As New WorksheetStyle("n8bwc")
                With n8bwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bwc)

                Dim n8bcwc As New WorksheetStyle("n8bcwc")
                With n8bcwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bcwc)
                'Pinkal (25-Nov-2021)-- End



                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "DECLARATION TRIMESTRIELLE DE SALAIRES"), "s10bc")
                wcell.MergeAcross = 3
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Période :") & " " & mstrYearQuarterName & "/" & Year(dtFinancialYearStart) & " : " & Language.getMessage(mstrModuleName, 20, "DTS011439") & "/" & Format(dtFinancialYearStart, "yy"), "s8bc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Matricule employeur CNAMGS"), "s10bcw")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(objMembership._EmployerMemNo, "s8ctw")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Nom ou Raison Sociale"), "s10bcw")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "CACHET ET SIGNATURE"), "s8bc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(objCompany._Name, "s8ctw")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                wcell.MergeAcross = 3
                wcell.MergeDown = 4
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                colIdx = 0
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 24, "Taux de cotisation"), "s8bc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell()

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell.StyleID = "n8b"
                wcell.StyleID = "n8bwc"
                'Pinkal (25-Nov-2021)-- End

                wcell.Data.Type = DataType.Number
                Dim str As String = "=SUM(R" & (rowsArrayHeader.Count + 2) & "C" & colIdx + 1 & ":R" & (rowsArrayHeader.Count + 3) & "C" & colIdx + 1 & ")"
                wcell.Formula = str
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "B.P:"), "s10bcw")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(objCompany._Address2, "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "VILLE : ") & objCompany._State_Name, "s10bcw")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Employeur"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell(mdecEmployeurRate, DataType.Number, "n8")
                wcell = New WorksheetCell(mdecEmployeurRate, DataType.Number, "n8wc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Travailleur"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell(mdecTravailleurRate, DataType.Number, "n8")
                wcell = New WorksheetCell(mdecTravailleurRate, DataType.Number, "n8wc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 29, "TEL:"), "s10bcw")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(objCompany._Phone1, "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 30, "FAX : ") & objCompany._Fax, "s10bcw")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell("", "n8")
                wcell = New WorksheetCell("", "n8wc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 31, "Email : ") & objCompany._Email, "s8w")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 32, "Plafond mensuel CNAMGS"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell(decPlafond, DataType.Number, "n8")
                wcell = New WorksheetCell(decPlafond, DataType.Number, "n8wc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 33, "Cotisations nettes dues CNAMGS"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 34, "DATE DE RECEPTION"), "s8bc")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell("", "n8bc")
                wcell = New WorksheetCell("", "n8bcwc")
                'Pinkal (25-Nov-2021)-- End
                wcell.Data.Type = DataType.Number
                wcell.Formula = "=R" & rowsArrayHeader.Count + 5 & "C10"
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                wcell.MergeAcross = 3
                wcell.MergeDown = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 35, "Cotisations payées à la CNAMGS"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                colIdx = 0
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 36, "Recap."), "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Effectif"), "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(mdtTableExcel.Rows.Count.ToString, "i8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 38, "MASSE SALARIALE SOUMISE A COTISATION : "), "s8bc")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell("", "n8b")
                wcell = New WorksheetCell("", "n8bwc")
                'Pinkal (25-Nov-2021)-- End

                wcell.Formula = "=R" & rowsArrayHeader.Count + 4 & "C6+R" & rowsArrayHeader.Count + 4 & "C8+R" & rowsArrayHeader.Count + 4 & "C10"
                wcell.Data.Type = DataType.Number
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 39, "COTISATIONS SOCIALES:"), "s8b")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell()

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell.StyleID = "n8b"
                wcell.StyleID = "n8bwc"
                'Pinkal (25-Nov-2021)-- End

                wcell.Data.Type = DataType.Number
                wcell.Formula = "=SUM(R" & (rowsArrayHeader.Count + 2) & "C" & colIdx + 1 & ":R" & (rowsArrayHeader.Count + 3) & "C" & colIdx + 1 & ")"
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                colIdx = 0
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell("", "s8b")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell("", "n8")
                wcell = New WorksheetCell("", "n8wc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "Part patronale"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell()

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell.StyleID = "n8"
                wcell.StyleID = "n8wc"
                'Pinkal (25-Nov-2021)-- End
                wcell.Data.Type = DataType.Number
                wcell.Formula = "=R" & rowsArrayHeader.Count & "C7*R" & rowsArrayHeader.Count - 10 & "C6"
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                colIdx = 0
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell("", "s8b")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell("", "n8")
                wcell = New WorksheetCell("", "n8wc")
                'Pinkal (25-Nov-2021)-- End

                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 41, "Part salariale"), "s8w")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                colIdx += wcell.MergeAcross + 1
                wcell = New WorksheetCell()

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell.StyleID = "n8"
                wcell.StyleID = "n8wc"
                'Pinkal (25-Nov-2021)-- End
                wcell.Data.Type = DataType.Number
                wcell.Formula = "=R" & rowsArrayHeader.Count - 1 & "C7*R" & rowsArrayHeader.Count - 10 & "C6"
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 42, " TOTAL A REPORTER PAGE SUIVANTE"), "s8b")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell(Format((From p In mdtTableExcel Select (CDec(p.Item("empcontribution")))).Sum, "00.00"), DataType.Number, "n8b")
                wcell = New WorksheetCell(Format((From p In mdtTableExcel Select (CDec(p.Item("empcontribution")))).Sum, "00.00"), DataType.Number, "n8bwc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell(Format((From p In mdtTableExcel Select (CDec(p.Item("amountTwo")))).Sum, "00.00"), DataType.Number, "n8b")
                wcell = New WorksheetCell(Format((From p In mdtTableExcel Select (CDec(p.Item("amountTwo")))).Sum, "00.00"), DataType.Number, "n8bwc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s8")
                row.Cells.Add(wcell)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'wcell = New WorksheetCell(Format((From p In mdtTableExcel Select (CDec(p.Item("amountThree")))).Sum, "00.00"), DataType.Number, "n8b")
                wcell = New WorksheetCell(Format((From p In mdtTableExcel Select (CDec(p.Item("amountThree")))).Sum, "00.00"), DataType.Number, "n8bwc")
                'Pinkal (25-Nov-2021)-- End
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8b")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCode1, "s10bcblue")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCode2, "s10bcblue")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCode3, "s10bcblue")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 43, "Rappel"), "s10bcw")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 44, "Date limite de retour de la DTS à la CNAMGS :"), "s10bred")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(mstrPeriodCustomCode3, "s10bcblue")
                wcell.MergeAcross = 1
                wcell.MergeDown = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 45, "Date limite de paiement des cotisations dues à la CNAMGS :"), "s10bred")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 46, "Au-delà de la date ci-dessus, une pénalité est appliquée conformément à la loi :"), "s8_lrtb")
                wcell.MergeAcross = 7
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 47, "- 25% pour non dépôt de la DTS calculé sur le montant de la DTS du dernier trimestre déclaré ;"), "s8_lr")
                wcell.MergeAcross = 7
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 48, "- 2% pour non paiement des cotisations par mois de retard cumulable au prorata temporis ;"), "s8_lrb")
                wcell.MergeAcross = 7
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 49, "NB: Veuillez mettre les effectifs à jour avec les mouvements justifiés entrants et sortants du personnel"), "s8b")
                wcell.MergeAcross = 6
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 50, "Date"), "s8b")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 51, "Mois 1"), "s8b")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 52, "Mois 2"), "s8b")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 53, "Mois 3"), "s8b")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                For Each col As DataColumn In mdtTableExcel.Columns
                    wcell = New WorksheetCell(col.Caption, "s8")
                    row.Cells.Add(wcell)
                Next
                rowsArrayHeader.Add(row)

            Else
                strWorkSheetStyle &= ".s10bcw {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; } "
                strWorkSheetStyle &= ".s10bcblue {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; color:#5877FC} "
                strWorkSheetStyle &= ".s10bred {font-size:12px; font-weight:bold; font-family:Tahoma; text-align:center; color:#FF0000} "
                intInitRows = 3

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                strWorkSheetStyle &= ".n8wc {font-size:10px; font-family:Tahoma; mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34) & ";} "
                strWorkSheetStyle &= ".n8bwc {font-size:10px; font-weight:bold; font-family:Tahoma; mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34) & ";} "
                strWorkSheetStyle &= ".n8bcwc {font-size:10px; font-weight:bold; font-family:Tahoma; text-align:center; mso-number-format:" & Chr(34) & GUI.fmtCurrency.Replace(",", "") & Chr(34) & ";} "
                'Pinkal (25-Nov-2021)-- End


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 

                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                If IO.Directory.Exists(objAppSett._ApplicationPath & "Data\Images") = False Then
                    IO.Directory.CreateDirectory(objAppSett._ApplicationPath & "Data\Images")
                End If
                objAppSett = Nothing
                'strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'If objCompany._Stamp IsNot Nothing Then
                '    objCompany._Stamp.Save(strImPath.Replace("Logo.jpg", "Stamp.jpg"))
                'End If
                'strBuilder.Append(" <img SRC='" & strImPath.Replace("Logo.jpg", "Stamp.jpg") & "' ALT='' HEIGHT = '100' WIDTH ='200' />" & vbCrLf)
                'strBuilder.Append(" </TD> " & vbCrLf)
                'For j As Integer = 0 To 6
                '    strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                'Next
                'strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'If objCompany._Image IsNot Nothing Then
                '    objCompany._Image.Save(strImPath)
                'End If
                'strBuilder.Append(" <img SRC='" & strImPath & "' ALT='' HEIGHT = '100' WIDTH ='100' />" & vbCrLf)
                'strBuilder.Append(" </TD> " & vbCrLf)


                Dim img As Image = My.Resources.CNAMGS
                'Dim ms As New IO.MemoryStream
                'img.Save(ms, img.RawFormat)
                img.Save(strImPath.Replace("Logo.jpg", "CNAMGS.jpg"))
                'Dim ImgByte As Byte() = ms.ToArray()
                'Dim mstrBase64Img = Convert.ToBase64String(ImgByte)

                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'strBuilder.Append(" <img SRC='data:image/png;base64," & mstrBase64Img & "' ALT='' HEIGHT = '100' WIDTH ='200' />" & vbCrLf)
                strBuilder.Append(" <img SRC='" & strImPath.Replace("Logo.jpg", "CNAMGS.jpg") & "' ALT='' HEIGHT = '100' WIDTH ='200' />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)

                For j As Integer = 0 To 6
                    strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                Next

                'mstrBase64Img = ""
                'ms = Nothing
                'img = Nothing
                'ImgByte = Nothing

                img = My.Resources.RepublicGabo
                'ms = New IO.MemoryStream
                'img.Save(ms, img.RawFormat)
                img.Save(strImPath.Replace("Logo.jpg", "RepublicGabo.jpg"))
                'ImgByte = ms.ToArray()
                'mstrBase64Img = Convert.ToBase64String(ImgByte)

                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%' style='border-width: 0px;'> " & vbCrLf)
                'strBuilder.Append(" <img SRC='data:image/png;base64," & mstrBase64Img & "' ALT='' HEIGHT = '100' WIDTH ='250' />" & vbCrLf)
                strBuilder.Append(" <img SRC='" & strImPath.Replace("Logo.jpg", "RepublicGabo.jpg") & "' ALT='' HEIGHT = '100' WIDTH ='250' />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)

                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                For j As Integer = 0 To 6
                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)
                Next

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' rowspan='2' class='s10bc'>" & Language.getMessage(mstrModuleName, 18, "DECLARATION TRIMESTRIELLE DE SALAIRES") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s8bc'>" & Language.getMessage(mstrModuleName, 19, "Période :") & " " & mstrYearQuarterName & "/" & Year(dtFinancialYearStart) & " : " & Language.getMessage(mstrModuleName, 20, "DTS011439") & "/" & Format(dtFinancialYearStart, "yy") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s10bcw'>" & Language.getMessage(mstrModuleName, 21, "Matricule employeur CNAMGS") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8ctw'>" & objMembership._EmployerMemNo & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s10bcw'>" & Language.getMessage(mstrModuleName, 22, "Nom ou Raison Sociale") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s8bc'>" & Language.getMessage(mstrModuleName, 23, "CACHET ET SIGNATURE") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8ctw'>" & objCompany._Name & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' rowspan='5' class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                colIdx = 0
                strBuilder.Append("<TD colspan='3' class='s8w'>&nbsp;</TD>" & vbCrLf)
                colIdx += 3
                strBuilder.Append("<TD colspan='2' class='s8bc'>" & Language.getMessage(mstrModuleName, 24, "Taux de cotisation") & "</TD>" & vbCrLf)
                colIdx += 2
                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8b'>=SUM(" & ConvertToExcelColumnLetter(colIdx + 1) & (rowsArrayHeader.Count + intInitRows + 2).ToString & ":" & ConvertToExcelColumnLetter(colIdx + 1) & (rowsArrayHeader.Count + intInitRows + 3).ToString & ")</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bwc'>=SUM(" & ConvertToExcelColumnLetter(colIdx + 1) & (rowsArrayHeader.Count + intInitRows + 2).ToString & ":" & ConvertToExcelColumnLetter(colIdx + 1) & (rowsArrayHeader.Count + intInitRows + 3).ToString & ")</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s10bcw'>" & Language.getMessage(mstrModuleName, 25, "B.P:") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>" & objCompany._Address2 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s10bcw'>" & Language.getMessage(mstrModuleName, 26, "VILLE : ") & objCompany._State_Name & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>" & Language.getMessage(mstrModuleName, 27, "Employeur") & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8'>" & mdecEmployeurRate & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8wc'>" & mdecEmployeurRate & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End


                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>" & Language.getMessage(mstrModuleName, 28, "Travailleur") & "</TD>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8'>" & mdecTravailleurRate & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8wc'>" & mdecTravailleurRate & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s10bcw'>" & Language.getMessage(mstrModuleName, 29, "TEL:") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>" & objCompany._Phone1 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s10bcw'>" & Language.getMessage(mstrModuleName, 30, "FAX : ") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8w'>" & Language.getMessage(mstrModuleName, 31, "Email : ") & objCompany._Email & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>" & Language.getMessage(mstrModuleName, 32, "Plafond mensuel CNAMGS") & objCompany._Email & "</TD>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8'>" & decPlafond & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8wc'>" & decPlafond & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End


                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8w'>" & Language.getMessage(mstrModuleName, 33, "Cotisations nettes dues CNAMGS") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s8bc'>" & Language.getMessage(mstrModuleName, 34, "DATE DE RECEPTION") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD colspan='2' class='n8bc'>=J" & rowsArrayHeader.Count + intInitRows + 5 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='n8bcwc'>=J" & rowsArrayHeader.Count + intInitRows + 5 & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End
                strBuilder.Append("<TD colspan='4' class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' rowspan='3' class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2'class='s8w'>" & Language.getMessage(mstrModuleName, 35, "Cotisations payées à la CNAMGS") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                colIdx = 0
                strBuilder.Append("<TD class='s8b'>" & Language.getMessage(mstrModuleName, 36, "Recap.") & "</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD class='s8b'>" & Language.getMessage(mstrModuleName, 37, "Effectif") & "</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD class='i8b'>" & mdtTableExcel.Rows.Count.ToString & "</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD colspan='3' class='s8bc'>" & Language.getMessage(mstrModuleName, 38, "MASSE SALARIALE SOUMISE A COTISATION : ") & "</TD>" & vbCrLf)
                colIdx += 3

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8b'>=F" & rowsArrayHeader.Count + intInitRows + 4 & "+H" & rowsArrayHeader.Count + intInitRows + 4 & "+J" & rowsArrayHeader.Count + intInitRows + 4 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bwc'>=F" & rowsArrayHeader.Count + intInitRows + 4 & "+H" & rowsArrayHeader.Count + intInitRows + 4 & "+J" & rowsArrayHeader.Count + intInitRows + 4 & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                colIdx += 1
                strBuilder.Append("<TD colspan='2' class='s8b'>" & Language.getMessage(mstrModuleName, 39, "COTISATIONS SOCIALES:") & "</TD>" & vbCrLf)
                colIdx += 2

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8b'>=" & ConvertToExcelColumnLetter(colIdx + 1) & rowsArrayHeader.Count + intInitRows + 2 & "+" & ConvertToExcelColumnLetter(colIdx + 1) & rowsArrayHeader.Count + intInitRows + 3 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bwc'>=" & ConvertToExcelColumnLetter(colIdx + 1) & rowsArrayHeader.Count + intInitRows + 2 & "+" & ConvertToExcelColumnLetter(colIdx + 1) & rowsArrayHeader.Count + intInitRows + 3 & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End
                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                colIdx = 0
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD colspan='3' class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 3
                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD colspan='2' class='s8'>" & Language.getMessage(mstrModuleName, 40, "Part patronale") & "</TD>" & vbCrLf)
                colIdx += 2

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8'>=G" & rowsArrayHeader.Count + intInitRows & "*F" & rowsArrayHeader.Count + intInitRows - 10 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8wc'>=G" & rowsArrayHeader.Count + intInitRows & "*F" & rowsArrayHeader.Count + intInitRows - 10 & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                colIdx = 0
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD colspan='3' class='s8b'>&nbsp;</TD>" & vbCrLf)
                colIdx += 3
                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                colIdx += 1
                strBuilder.Append("<TD colspan='2' class='s8'>" & Language.getMessage(mstrModuleName, 41, "Part salariale") & "</TD>" & vbCrLf)
                colIdx += 2

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8'>=G" & rowsArrayHeader.Count - 1 + intInitRows & "*F" & rowsArrayHeader.Count + intInitRows - 10 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8wc'>=G" & rowsArrayHeader.Count - 1 + intInitRows & "*F" & rowsArrayHeader.Count + intInitRows - 10 & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='3' class='s8b'>" & Language.getMessage(mstrModuleName, 42, " TOTAL A REPORTER PAGE SUIVANTE") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8b'>" & Format((From p In mdtTableExcel Select (CDec(p.Item("empcontribution")))).Sum, "00.00") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bwc'>" & Format((From p In mdtTableExcel Select (CDec(p.Item("empcontribution")))).Sum, "00.00") & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8b'>" & Format((From p In mdtTableExcel Select (CDec(p.Item("amountTwo")))).Sum, "00.00") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bwc'>" & Format((From p In mdtTableExcel Select (CDec(p.Item("amountTwo")))).Sum, "00.00") & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End

                strBuilder.Append("<TD class='s8'>&nbsp;</TD>" & vbCrLf)

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                'strBuilder.Append("<TD class='n8b'>" & Format((From p In mdtTableExcel Select (CDec(p.Item("amountThree")))).Sum, "00.00") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD class='n8bwc'>" & Format((From p In mdtTableExcel Select (CDec(p.Item("amountThree")))).Sum, "00.00") & "</TD>" & vbCrLf)
                'Pinkal (25-Nov-2021)-- End


                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8b'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s10bcblue'>" & mstrPeriodCode1 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s10bcblue'>" & mstrPeriodCode2 & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s10bcblue'>" & mstrPeriodCode3 & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s10bcw'>" & Language.getMessage(mstrModuleName, 43, "Rappel") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s10bred'>" & Language.getMessage(mstrModuleName, 44, "Date limite de retour de la DTS à la CNAMGS :") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' rowspan='2' class='s10bred'>" & mstrPeriodCustomCode3 & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='4' class='s10bred'>" & Language.getMessage(mstrModuleName, 45, "Date limite de paiement des cotisations dues à la CNAMGS :") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='8' class='s8_lrtb'>" & Language.getMessage(mstrModuleName, 46, "Au-delà de la date ci-dessus, une pénalité est appliquée conformément à la loi :") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='8' class='s8_lr'>" & Language.getMessage(mstrModuleName, 47, "- 25% pour non dépôt de la DTS calculé sur le montant de la DTS du dernier trimestre déclaré ;") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='8' class='s8_lrb'>" & Language.getMessage(mstrModuleName, 48, "- 2% pour non paiement des cotisations par mois de retard cumulable au prorata temporis ;") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD colspan='7' class='s8b'>" & Language.getMessage(mstrModuleName, 49, "NB: Veuillez mettre les effectifs à jour avec les mouvements justifiés entrants et sortants du personnel") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD class='s8w'>&nbsp;</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8b'>" & Language.getMessage(mstrModuleName, 50, "Date") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8b'>" & Language.getMessage(mstrModuleName, 51, "Mois 1") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8b'>" & Language.getMessage(mstrModuleName, 52, "Mois 2") & "</TD>" & vbCrLf)
                strBuilder.Append("<TD colspan='2' class='s8b'>" & Language.getMessage(mstrModuleName, 53, "Mois 3") & "</TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

                strBuilder.Append("<TR>" & vbCrLf)
                For Each col As DataColumn In mdtTableExcel.Columns
                    strBuilder.Append("<TD class='s8'>" & col.Caption & "</TD>" & vbCrLf)
                Next
                strBuilder.Append("</TR>" & vbCrLf)
                If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)
                End If


                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
                For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                    If ii = 0 Then
                        intArrayColumnWidth(ii) = 50
                    ElseIf ii = 2 Then
                        intArrayColumnWidth(ii) = 140
                    Else
                        intArrayColumnWidth(ii) = 100
                    End If
                Next

                If blnExcelHTML = False Then
                    Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, marrWorksheetStyle:=arrWorkSheetStyle.ToArray)
                Else
                    Call ReportExecute(intCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, strExportReportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, mstrWorksheetStyle:=strWorkSheetStyle)
                End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select Quarter")
			Language.setMessage(mstrModuleName, 2, "T1")
			Language.setMessage(mstrModuleName, 3, "T2")
			Language.setMessage(mstrModuleName, 4, "T3")
			Language.setMessage(mstrModuleName, 5, "T4")
			Language.setMessage(mstrModuleName, 6, "N°")
			Language.setMessage(mstrModuleName, 7, "Matricule")
			Language.setMessage(mstrModuleName, 9, "NOM ET PRENOM")
			Language.setMessage(mstrModuleName, 10, "EMBAUCHE")
			Language.setMessage(mstrModuleName, 11, "CESSATION")
			Language.setMessage(mstrModuleName, 12, "Assiette soumis à cotisation")
			Language.setMessage(mstrModuleName, 13, "Nbre Hrs/Jrs")
			Language.setMessage(mstrModuleName, 14, "Assiette soumis à cotisation")
			Language.setMessage(mstrModuleName, 15, "Nbre Hrs/Jrs")
			Language.setMessage(mstrModuleName, 16, "Assiette soumis à cotisation")
			Language.setMessage(mstrModuleName, 17, "Nbre Hrs/Jrs")
			Language.setMessage(mstrModuleName, 18, "DECLARATION TRIMESTRIELLE DE SALAIRES")
			Language.setMessage(mstrModuleName, 19, "Période :")
			Language.setMessage(mstrModuleName, 20, "DTS011439")
			Language.setMessage(mstrModuleName, 21, "Matricule employeur CNAMGS")
			Language.setMessage(mstrModuleName, 22, "Nom ou Raison Sociale")
			Language.setMessage(mstrModuleName, 23, "CACHET ET SIGNATURE")
			Language.setMessage(mstrModuleName, 24, "Taux de cotisation")
			Language.setMessage(mstrModuleName, 25, "B.P:")
			Language.setMessage(mstrModuleName, 26, "VILLE :")
			Language.setMessage(mstrModuleName, 27, "Employeur")
			Language.setMessage(mstrModuleName, 28, "Travailleur")
			Language.setMessage(mstrModuleName, 29, "TEL:")
			Language.setMessage(mstrModuleName, 30, "FAX :")
			Language.setMessage(mstrModuleName, 31, "Email :")
			Language.setMessage(mstrModuleName, 32, "Plafond mensuel CNAMGS")
			Language.setMessage(mstrModuleName, 33, "Cotisations nettes dues CNAMGS")
			Language.setMessage(mstrModuleName, 34, "DATE DE RECEPTION")
			Language.setMessage(mstrModuleName, 35, "Cotisations payées à la CNAMGS")
			Language.setMessage(mstrModuleName, 36, "Recap.")
			Language.setMessage(mstrModuleName, 37, "Effectif")
			Language.setMessage(mstrModuleName, 38, "MASSE SALARIALE SOUMISE A COTISATION :")
			Language.setMessage(mstrModuleName, 39, "COTISATIONS SOCIALES:")
			Language.setMessage(mstrModuleName, 40, "Part patronale")
			Language.setMessage(mstrModuleName, 41, "Part salariale")
			Language.setMessage(mstrModuleName, 42, " TOTAL A REPORTER PAGE SUIVANTE")
			Language.setMessage(mstrModuleName, 43, "Rappel")
			Language.setMessage(mstrModuleName, 44, "Date limite de retour de la DTS à la CNAMGS :")
			Language.setMessage(mstrModuleName, 45, "Date limite de paiement des cotisations dues à la CNAMGS :")
			Language.setMessage(mstrModuleName, 46, "Au-delà de la date ci-dessus, une pénalité est appliquée conformément à la loi :")
			Language.setMessage(mstrModuleName, 47, "- 25% pour non dépôt de la DTS calculé sur le montant de la DTS du dernier trimestre déclaré ;")
			Language.setMessage(mstrModuleName, 48, "- 2% pour non paiement des cotisations par mois de retard cumulable au prorata temporis ;")
			Language.setMessage(mstrModuleName, 49, "NB: Veuillez mettre les effectifs à jour avec les mouvements justifiés entrants et sortants du personnel")
			Language.setMessage(mstrModuleName, 50, "Date")
			Language.setMessage(mstrModuleName, 51, "Mois 1")
			Language.setMessage(mstrModuleName, 52, "Mois 2")
			Language.setMessage(mstrModuleName, 53, "Mois 3")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
