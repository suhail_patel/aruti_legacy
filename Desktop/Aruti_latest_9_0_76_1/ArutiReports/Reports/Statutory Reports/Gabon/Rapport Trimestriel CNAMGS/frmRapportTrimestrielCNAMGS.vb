﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmRapportTrimestrielCNAMGS

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmRapportTrimestrielCNAMGS"
    Private objRTCNAMGS As clsRapportTrimestrielCNAMGS

    Private mstrReport_GroupName As String = ""
    'Private mintFirstOpenPeriod As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty
    'Private strSavedMembership As String = String.Empty
    'Private strSavedAllocations As String = String.Empty
    'Private mintSaveAllocTypeId As Integer = enAllocation.CLASS_GROUP
    'Private mdtPeriodStartDate As Date = Nothing
    'Private mdtPeriodEndDate As Date = Nothing

    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""


#End Region

#Region " Constructor "

    Public Sub New()
        objRTCNAMGS = New clsRapportTrimestrielCNAMGS(User._Object._Languageunkid, Company._Object._Companyunkid)
        objRTCNAMGS.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Public Enum enHeadTypeId
        Membership = 1
        YearQuarter = 2
        DaysWorked_1 = 3
        DaysWorked_2 = 4
        DaysWorked_3 = 5
        EmployeurRate = 6
        TravailleurRate = 7
        PlafondFormula = 8
        ShowLogo = 9
        'Pinkal (25-Nov-2021)-- Start
        'Gabon Statutory Report Enhancements. 
        Asscotisation = 10
        'Pinkal (25-Nov-2021)-- End
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim objHead As New clsTransactionHead
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objRTCNAMGS.getComboListForYearQuarter(True)
            With cboYearQuarter
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            'dsCombos = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.Informational)
            dsCombos = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True)

            With cboDaysWorked1
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboDaysWorked2
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboDaysWorked3
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboAssiette
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            'Pinkal (25-Nov-2021) -- End

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objHead = Nothing
        End Try
    End Sub

    Public Sub FillList()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            dsList = objPeriod.GetList("List", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, 0)
            Dim startdate As Date = FinancialYear._Object._Database_Start_Date.AddMonths((CInt(cboYearQuarter.SelectedValue) - 1) * 3)
            Dim enddate As Date = FinancialYear._Object._Database_Start_Date.AddMonths(((CInt(cboYearQuarter.SelectedValue)) * 3)).AddDays(-1)

            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "start_date >= '" & eZeeDate.convertDate(startdate) & "' AND end_date <= '" & eZeeDate.convertDate(enddate) & "' ", "end_date", DataViewRowState.CurrentRows).ToTable

            objcolhPeriodunkid.DataPropertyName = "periodunkid"
            ColhPeriodCode.DataPropertyName = "period_code"
            objcolhPeriodCustomCode.DataPropertyName = "sunjv_periodcode"
            colhPeriodName.DataPropertyName = "period_name"
            objcolhPeriodStart.DataPropertyName = "start_date"
            objcolhPeriodEnd.DataPropertyName = "end_date"

            With dgvPeriod
                .AutoGenerateColumns = False

                .DataSource = dtTable

            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboMembership.SelectedValue = 0
            cboYearQuarter.SelectedValue = 0
            cboDaysWorked1.SelectedValue = 0
            cboDaysWorked2.SelectedValue = 0
            cboDaysWorked3.SelectedValue = 0
            'Pinkal (25-Nov-2021)-- Start
            'Gabon Statutory Report Enhancements. 
            cboAssiette.SelectedValue = 0
            'Pinkal (25-Nov-2021)-- End

            txtEmployeurRate.Decimal = 0
            txtTravailleurRate.Decimal = 0
            txtPlafondFormula.Text = ""
            chkShowLogo.Checked = True

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mstrAdvanceFilter = ""


            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Rapport_Trimestriel_CNAMGS)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.YearQuarter
                            cboYearQuarter.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.DaysWorked_1
                            cboDaysWorked1.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.DaysWorked_2
                            cboDaysWorked2.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.DaysWorked_3
                            cboDaysWorked3.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.EmployeurRate
                            txtEmployeurRate.Decimal = CDec(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TravailleurRate
                            txtTravailleurRate.Decimal = CDec(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.PlafondFormula
                            txtPlafondFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.ShowLogo
                            chkShowLogo.Checked = CBool(dsRow.Item("transactionheadid"))

                            'Pinkal (25-Nov-2021)-- Start
                            'Gabon Statutory Report Enhancements. 
                        Case enHeadTypeId.Asscotisation
                            cboAssiette.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Pinkal (25-Nov-2021)-- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objRTCNAMGS.SetDefaultValue()

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Function
            ElseIf CInt(cboYearQuarter.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Year Quarter is mandatory information. Please select Year Quarter to continue."), enMsgBoxStyle.Information)
                cboYearQuarter.Focus()
                Exit Function
            ElseIf CInt(cboDaysWorked1.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Days worked for 1st period is mandatory information. Please select Days worked to continue."), enMsgBoxStyle.Information)
                cboDaysWorked1.Focus()
                Exit Function
            ElseIf CInt(cboDaysWorked2.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Days worked for 2nd period is mandatory information. Please select Days worked to continue."), enMsgBoxStyle.Information)
                cboDaysWorked2.Focus()
                Exit Function
            ElseIf CInt(cboDaysWorked3.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Days worked for 3rd period is mandatory information. Please select Days worked to continue."), enMsgBoxStyle.Information)
                cboDaysWorked3.Focus()
                Exit Function

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
            ElseIf CInt(cboAssiette.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Assiette soumis à cotisation is mandatory information. Please select Assiette soumis à cotisation to continue."), enMsgBoxStyle.Information)
                cboAssiette.Focus()
                Exit Function
                'Pinkal (25-Nov-2021)-- End

            ElseIf txtEmployeurRate.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please enter Employeur Rate."), enMsgBoxStyle.Information)
                txtEmployeurRate.Focus()
                Exit Function
            ElseIf txtTravailleurRate.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter Travailleur Rate."), enMsgBoxStyle.Information)
                txtTravailleurRate.Focus()
                Exit Function
            End If

            If dgvPeriod.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, There is no period in selected year quarter."), enMsgBoxStyle.Information)
                cboYearQuarter.Focus()
                Exit Function
            End If

            With objRTCNAMGS
                ._MembershipId = CInt(cboMembership.SelectedValue)
                ._MembershipName = cboMembership.Text

                ._YearQuarterId = CInt(cboYearQuarter.SelectedValue)
                ._YearQuarterName = cboYearQuarter.Text

                ._DaysWorkedId1 = CInt(cboDaysWorked1.SelectedValue)
                ._DaysWorkedName1 = cboDaysWorked1.Text

                ._DaysWorkedId2 = CInt(cboDaysWorked2.SelectedValue)
                ._DaysWorkedName2 = cboDaysWorked2.Text

                ._DaysWorkedId3 = CInt(cboDaysWorked3.SelectedValue)
                ._DaysWorkedName3 = cboDaysWorked3.Text

                'Pinkal (25-Nov-2021)-- Start
                'Gabon Statutory Report Enhancements. 
                ._AssietteId = CInt(cboAssiette.SelectedValue)
                ._Assiettesoumis = cboAssiette.Text
                'Pinkal (25-Nov-2021)-- End

                ._EmployeurRate = txtEmployeurRate.Decimal
                ._TravailleurRate = txtTravailleurRate.Decimal

                ._PlafondFormula = txtPlafondFormula.Text.Trim

                ._PeriodIDs = String.Join(",", (From p In dgvPeriod.Rows.Cast(Of DataGridViewRow)() Select (p.Cells(objcolhPeriodunkid.Index).Value.ToString)).ToArray)
                ._PeriodStart = eZeeDate.convertDate((From p In dgvPeriod.Rows.Cast(Of DataGridViewRow)() Select (p.Cells(objcolhPeriodStart.Index).Value.ToString)).First.ToString)
                ._PeriodEnd = eZeeDate.convertDate((From p In dgvPeriod.Rows.Cast(Of DataGridViewRow)() Select (p.Cells(objcolhPeriodEnd.Index).Value.ToString)).Last.ToString)

                ._PeriodId1 = dgvPeriod.Rows(0).Cells(objcolhPeriodunkid.Index).Value.ToString
                ._PeriodCode1 = dgvPeriod.Rows(0).Cells(ColhPeriodCode.Index).Value.ToString
                ._PeriodName1 = dgvPeriod.Rows(0).Cells(colhPeriodName.Index).Value.ToString

                If dgvPeriod.RowCount >= 2 Then
                    ._PeriodId2 = dgvPeriod.Rows(1).Cells(objcolhPeriodunkid.Index).Value.ToString
                    ._PeriodCode2 = dgvPeriod.Rows(1).Cells(ColhPeriodCode.Index).Value.ToString
                    ._PeriodName2 = dgvPeriod.Rows(1).Cells(colhPeriodName.Index).Value.ToString
                End If

                If dgvPeriod.RowCount >= 3 Then
                    ._PeriodId3 = dgvPeriod.Rows(2).Cells(objcolhPeriodunkid.Index).Value.ToString
                    ._PeriodCode3 = dgvPeriod.Rows(2).Cells(ColhPeriodCode.Index).Value.ToString
                    ._PeriodCustomCode3 = dgvPeriod.Rows(2).Cells(objcolhPeriodCustomCode.Index).Value.ToString
                    ._PeriodName3 = dgvPeriod.Rows(2).Cells(colhPeriodName.Index).Value.ToString
                End If

                ._ShowLogo = chkShowLogo.Checked

                ._ViewByIds = mstrStringIds
                ._ViewIndex = mintViewIdx
                ._ViewByName = mstrStringName
                ._Analysis_Fields = mstrAnalysis_Fields
                ._Analysis_Join = mstrAnalysis_Join
                ._Report_GroupName = mstrReport_GroupName

                If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                    mintPaidCurrencyId = mintBaseCurrId
                End If

                ._CountryId = CInt(cboCurrency.SelectedValue)
                ._BaseCurrencyId = mintBaseCurrId
                ._PaidCurrencyId = mintPaidCurrencyId
                ._CurrencyName = cboCurrency.Text.Trim

                If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                    ._ConversionRate = mdecPaidExRate / mdecBaseExRate
                End If

                ._ExchangeRate = LblCurrencyRate.Text


                ._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname

            End With




            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function



#End Region

#Region " Form's Event(s) "

    Private Sub frmRapportTrimestrielCNAMGS_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRTCNAMGS = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRapportTrimestrielCNAMGS_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRapportTrimestrielCNAMGS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            eZeeHeader.Title = objRTCNAMGS._ReportName
            eZeeHeader.Message = objRTCNAMGS._ReportDesc

            OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRapportTrimestrielCNAMGS_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objRTCNAMGS._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objRTCNAMGS._PeriodEnd))

            objRTCNAMGS.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             objRTCNAMGS._PeriodStart, _
                                             objRTCNAMGS._PeriodEnd, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, FinancialYear._Object._Database_Start_Date, ConfigParameter._Object._Base_CurrencyId, GUI.fmtCurrency, "", ConfigParameter._Object._OpenAfterExport, chkShowLogo.Checked)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Sub

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Rapport_Trimestriel_CNAMGS
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.YearQuarter
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboYearQuarter.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.DaysWorked_1
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDaysWorked1.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.DaysWorked_2
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDaysWorked2.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.DaysWorked_3
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDaysWorked3.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.EmployeurRate
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtEmployeurRate.Decimal.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.TravailleurRate
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtTravailleurRate.Decimal.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.PlafondFormula
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtPlafondFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                    Case enHeadTypeId.ShowLogo
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkShowLogo.Checked

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)

                        'Pinkal (25-Nov-2021)-- Start
                        'Gabon Statutory Report Enhancements. 
                    Case enHeadTypeId.Asscotisation
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAssiette.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Rapport_Trimestriel_CNAMGS, 0, 0, intHeadType)
                        'Pinkal (25-Nov-2021)-- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsRapportTrimestrielCNAMGS.SetMessages()
            objfrm._Other_ModuleNames = "clsRapportTrimestrielCNAMGS"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsPF.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 10, "Available Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#      (e.g. #010#)")

            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, eZee.Common.eZeeGradientButton).Name & "_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Combobox Event(s) "

    Private Sub cboYearQuarter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYearQuarter.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYearQuarter_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "



#End Region

#Region " Texbox Event(s) "

    Private Sub txtEmployeurRate_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmployeurRate.Leave
        Try
            txtEmployeurRate.Text = Format(txtEmployeurRate.Decimal, "###,###,#00.00####")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEmployeurRate_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtTravailleurRate_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTravailleurRate.Leave
        Try
            txtTravailleurRate.Text = Format(txtTravailleurRate.Decimal, "###,###,#00.00####")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtTravailleurRate_Leave", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub



#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()

            Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.chkShowLogo.Text = Language._Object.getCaption(Me.chkShowLogo.Name, Me.chkShowLogo.Text)
			Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)
			Me.lblYearQuarter.Text = Language._Object.getCaption(Me.lblYearQuarter.Name, Me.lblYearQuarter.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblDaysWorked1.Text = Language._Object.getCaption(Me.lblDaysWorked1.Name, Me.lblDaysWorked1.Text)
			Me.lblDaysWorked3.Text = Language._Object.getCaption(Me.lblDaysWorked3.Name, Me.lblDaysWorked3.Text)
			Me.lblDaysWorked2.Text = Language._Object.getCaption(Me.lblDaysWorked2.Name, Me.lblDaysWorked2.Text)
			Me.lblEmployeurRate.Text = Language._Object.getCaption(Me.lblEmployeurRate.Name, Me.lblEmployeurRate.Text)
			Me.lblTravailleurRate.Text = Language._Object.getCaption(Me.lblTravailleurRate.Name, Me.lblTravailleurRate.Text)
			Me.lblPlafondFormula.Text = Language._Object.getCaption(Me.lblPlafondFormula.Name, Me.lblPlafondFormula.Text)
			Me.ColhPeriodCode.HeaderText = Language._Object.getCaption(Me.ColhPeriodCode.Name, Me.ColhPeriodCode.HeaderText)
			Me.colhPeriodName.HeaderText = Language._Object.getCaption(Me.colhPeriodName.Name, Me.colhPeriodName.HeaderText)
			Me.LblAssiette.Text = Language._Object.getCaption(Me.LblAssiette.Name, Me.LblAssiette.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Year Quarter is mandatory information. Please select Year Quarter to continue.")
			Language.setMessage(mstrModuleName, 3, "Days worked for 1st period is mandatory information. Please select Days worked to continue.")
			Language.setMessage(mstrModuleName, 4, "Days worked for 2nd period is mandatory information. Please select Days worked to continue.")
			Language.setMessage(mstrModuleName, 5, "Days worked for 3rd period is mandatory information. Please select Days worked to continue.")
			Language.setMessage(mstrModuleName, 6, "Please enter Employeur Rate.")
			Language.setMessage(mstrModuleName, 7, "Please enter Travailleur Rate.")
			Language.setMessage(mstrModuleName, 8, "Sorry, There is no period in selected year quarter.")
			Language.setMessage(mstrModuleName, 9, "Selection Saved Successfully")
            Language.setMessage(mstrModuleName, 10, "Available Keywords")
			Language.setMessage(mstrModuleName, 11, "Assiette soumis à cotisation is mandatory information. Please select Assiette soumis à cotisation to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class