﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRapportTrimestrielCNAMGS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRapportTrimestrielCNAMGS))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnKeywordsPF = New eZee.Common.eZeeGradientButton
        Me.lblPlafondFormula = New System.Windows.Forms.Label
        Me.txtPlafondFormula = New System.Windows.Forms.TextBox
        Me.txtTravailleurRate = New eZee.TextBox.NumericTextBox
        Me.lblTravailleurRate = New System.Windows.Forms.Label
        Me.txtEmployeurRate = New eZee.TextBox.NumericTextBox
        Me.lblEmployeurRate = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvPeriod = New System.Windows.Forms.DataGridView
        Me.objcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColhPeriodCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPeriodName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodCustomCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodEnd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblDaysWorked3 = New System.Windows.Forms.Label
        Me.cboDaysWorked3 = New System.Windows.Forms.ComboBox
        Me.lblDaysWorked2 = New System.Windows.Forms.Label
        Me.cboDaysWorked2 = New System.Windows.Forms.ComboBox
        Me.lblDaysWorked1 = New System.Windows.Forms.Label
        Me.cboDaysWorked1 = New System.Windows.Forms.ComboBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkShowLogo = New System.Windows.Forms.CheckBox
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.lblYearQuarter = New System.Windows.Forms.Label
        Me.cboYearQuarter = New System.Windows.Forms.ComboBox
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.cboAssiette = New System.Windows.Forms.ComboBox
        Me.LblAssiette = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(807, 60)
        Me.eZeeHeader.TabIndex = 69
        Me.eZeeHeader.Title = ""
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(411, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        Me.objbtnAdvanceFilter.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(514, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(610, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(706, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 478)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(807, 55)
        Me.EZeeFooter1.TabIndex = 70
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.LblAssiette)
        Me.gbMandatoryInfo.Controls.Add(Me.cboAssiette)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsPF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPlafondFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtPlafondFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtTravailleurRate)
        Me.gbMandatoryInfo.Controls.Add(Me.lblTravailleurRate)
        Me.gbMandatoryInfo.Controls.Add(Me.txtEmployeurRate)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmployeurRate)
        Me.gbMandatoryInfo.Controls.Add(Me.Panel1)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDaysWorked3)
        Me.gbMandatoryInfo.Controls.Add(Me.cboDaysWorked3)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDaysWorked2)
        Me.gbMandatoryInfo.Controls.Add(Me.cboDaysWorked2)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDaysWorked1)
        Me.gbMandatoryInfo.Controls.Add(Me.cboDaysWorked1)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowLogo)
        Me.gbMandatoryInfo.Controls.Add(Me.chkIgnoreZero)
        Me.gbMandatoryInfo.Controls.Add(Me.lblYearQuarter)
        Me.gbMandatoryInfo.Controls.Add(Me.cboYearQuarter)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(496, 357)
        Me.gbMandatoryInfo.TabIndex = 71
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnKeywordsPF
        '
        Me.objbtnKeywordsPF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsPF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsPF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsPF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsPF.BorderSelected = False
        Me.objbtnKeywordsPF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsPF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsPF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsPF.Location = New System.Drawing.Point(409, 252)
        Me.objbtnKeywordsPF.Name = "objbtnKeywordsPF"
        Me.objbtnKeywordsPF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsPF.TabIndex = 166
        '
        'lblPlafondFormula
        '
        Me.lblPlafondFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlafondFormula.Location = New System.Drawing.Point(9, 248)
        Me.lblPlafondFormula.Name = "lblPlafondFormula"
        Me.lblPlafondFormula.Size = New System.Drawing.Size(108, 34)
        Me.lblPlafondFormula.TabIndex = 165
        Me.lblPlafondFormula.Text = "Plafond mensuel CNAMGS Formula"
        Me.lblPlafondFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPlafondFormula
        '
        Me.txtPlafondFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlafondFormula.Location = New System.Drawing.Point(123, 252)
        Me.txtPlafondFormula.Name = "txtPlafondFormula"
        Me.txtPlafondFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtPlafondFormula.TabIndex = 164
        '
        'txtTravailleurRate
        '
        Me.txtTravailleurRate.AllowNegative = True
        Me.txtTravailleurRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTravailleurRate.DigitsInGroup = 0
        Me.txtTravailleurRate.Flags = 0
        Me.txtTravailleurRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTravailleurRate.Location = New System.Drawing.Point(123, 225)
        Me.txtTravailleurRate.MaxDecimalPlaces = 4
        Me.txtTravailleurRate.MaxWholeDigits = 9
        Me.txtTravailleurRate.Name = "txtTravailleurRate"
        Me.txtTravailleurRate.Prefix = ""
        Me.txtTravailleurRate.RangeMax = 1.7976931348623157E+308
        Me.txtTravailleurRate.RangeMin = -1.7976931348623157E+308
        Me.txtTravailleurRate.Size = New System.Drawing.Size(111, 21)
        Me.txtTravailleurRate.TabIndex = 122
        Me.txtTravailleurRate.Text = "0"
        Me.txtTravailleurRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTravailleurRate
        '
        Me.lblTravailleurRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTravailleurRate.Location = New System.Drawing.Point(9, 228)
        Me.lblTravailleurRate.Name = "lblTravailleurRate"
        Me.lblTravailleurRate.Size = New System.Drawing.Size(108, 15)
        Me.lblTravailleurRate.TabIndex = 123
        Me.lblTravailleurRate.Text = "Travailleur Rate (%)"
        Me.lblTravailleurRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeurRate
        '
        Me.txtEmployeurRate.AllowNegative = True
        Me.txtEmployeurRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEmployeurRate.DigitsInGroup = 0
        Me.txtEmployeurRate.Flags = 0
        Me.txtEmployeurRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeurRate.Location = New System.Drawing.Point(123, 198)
        Me.txtEmployeurRate.MaxDecimalPlaces = 4
        Me.txtEmployeurRate.MaxWholeDigits = 9
        Me.txtEmployeurRate.Name = "txtEmployeurRate"
        Me.txtEmployeurRate.Prefix = ""
        Me.txtEmployeurRate.RangeMax = 1.7976931348623157E+308
        Me.txtEmployeurRate.RangeMin = -1.7976931348623157E+308
        Me.txtEmployeurRate.Size = New System.Drawing.Size(111, 21)
        Me.txtEmployeurRate.TabIndex = 120
        Me.txtEmployeurRate.Text = "0"
        Me.txtEmployeurRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEmployeurRate
        '
        Me.lblEmployeurRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeurRate.Location = New System.Drawing.Point(9, 201)
        Me.lblEmployeurRate.Name = "lblEmployeurRate"
        Me.lblEmployeurRate.Size = New System.Drawing.Size(108, 15)
        Me.lblEmployeurRate.TabIndex = 121
        Me.lblEmployeurRate.Text = "Employeur Rate (%)"
        Me.lblEmployeurRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvPeriod)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(263, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(221, 158)
        Me.Panel1.TabIndex = 72
        '
        'dgvPeriod
        '
        Me.dgvPeriod.AllowUserToAddRows = False
        Me.dgvPeriod.AllowUserToDeleteRows = False
        Me.dgvPeriod.AllowUserToResizeRows = False
        Me.dgvPeriod.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPeriod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhPeriodunkid, Me.ColhPeriodCode, Me.colhPeriodName, Me.objcolhPeriodCustomCode, Me.objcolhPeriodStart, Me.objcolhPeriodEnd})
        Me.dgvPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPeriod.Location = New System.Drawing.Point(0, 0)
        Me.dgvPeriod.Name = "dgvPeriod"
        Me.dgvPeriod.ReadOnly = True
        Me.dgvPeriod.RowHeadersVisible = False
        Me.dgvPeriod.RowHeadersWidth = 5
        Me.dgvPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPeriod.Size = New System.Drawing.Size(221, 158)
        Me.dgvPeriod.TabIndex = 287
        '
        'objcolhPeriodunkid
        '
        Me.objcolhPeriodunkid.HeaderText = "periodunkid"
        Me.objcolhPeriodunkid.Name = "objcolhPeriodunkid"
        Me.objcolhPeriodunkid.ReadOnly = True
        Me.objcolhPeriodunkid.Visible = False
        '
        'ColhPeriodCode
        '
        Me.ColhPeriodCode.HeaderText = "Code"
        Me.ColhPeriodCode.Name = "ColhPeriodCode"
        Me.ColhPeriodCode.ReadOnly = True
        Me.ColhPeriodCode.Width = 50
        '
        'colhPeriodName
        '
        Me.colhPeriodName.HeaderText = "Period Name"
        Me.colhPeriodName.Name = "colhPeriodName"
        Me.colhPeriodName.ReadOnly = True
        Me.colhPeriodName.Width = 160
        '
        'objcolhPeriodCustomCode
        '
        Me.objcolhPeriodCustomCode.HeaderText = "Custom Code"
        Me.objcolhPeriodCustomCode.Name = "objcolhPeriodCustomCode"
        Me.objcolhPeriodCustomCode.ReadOnly = True
        Me.objcolhPeriodCustomCode.Visible = False
        '
        'objcolhPeriodStart
        '
        Me.objcolhPeriodStart.HeaderText = "Period Start"
        Me.objcolhPeriodStart.Name = "objcolhPeriodStart"
        Me.objcolhPeriodStart.ReadOnly = True
        Me.objcolhPeriodStart.Visible = False
        '
        'objcolhPeriodEnd
        '
        Me.objcolhPeriodEnd.HeaderText = "Period End"
        Me.objcolhPeriodEnd.Name = "objcolhPeriodEnd"
        Me.objcolhPeriodEnd.ReadOnly = True
        Me.objcolhPeriodEnd.Visible = False
        '
        'lblDaysWorked3
        '
        Me.lblDaysWorked3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysWorked3.Location = New System.Drawing.Point(9, 143)
        Me.lblDaysWorked3.Name = "lblDaysWorked3"
        Me.lblDaysWorked3.Size = New System.Drawing.Size(108, 17)
        Me.lblDaysWorked3.TabIndex = 118
        Me.lblDaysWorked3.Text = "Days worked (3)"
        Me.lblDaysWorked3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDaysWorked3
        '
        Me.cboDaysWorked3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDaysWorked3.DropDownWidth = 230
        Me.cboDaysWorked3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDaysWorked3.FormattingEnabled = True
        Me.cboDaysWorked3.Location = New System.Drawing.Point(123, 141)
        Me.cboDaysWorked3.Name = "cboDaysWorked3"
        Me.cboDaysWorked3.Size = New System.Drawing.Size(126, 21)
        Me.cboDaysWorked3.TabIndex = 117
        '
        'lblDaysWorked2
        '
        Me.lblDaysWorked2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysWorked2.Location = New System.Drawing.Point(9, 116)
        Me.lblDaysWorked2.Name = "lblDaysWorked2"
        Me.lblDaysWorked2.Size = New System.Drawing.Size(108, 17)
        Me.lblDaysWorked2.TabIndex = 116
        Me.lblDaysWorked2.Text = "Days worked (2)"
        Me.lblDaysWorked2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDaysWorked2
        '
        Me.cboDaysWorked2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDaysWorked2.DropDownWidth = 230
        Me.cboDaysWorked2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDaysWorked2.FormattingEnabled = True
        Me.cboDaysWorked2.Location = New System.Drawing.Point(123, 114)
        Me.cboDaysWorked2.Name = "cboDaysWorked2"
        Me.cboDaysWorked2.Size = New System.Drawing.Size(126, 21)
        Me.cboDaysWorked2.TabIndex = 115
        '
        'lblDaysWorked1
        '
        Me.lblDaysWorked1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysWorked1.Location = New System.Drawing.Point(9, 89)
        Me.lblDaysWorked1.Name = "lblDaysWorked1"
        Me.lblDaysWorked1.Size = New System.Drawing.Size(108, 17)
        Me.lblDaysWorked1.TabIndex = 114
        Me.lblDaysWorked1.Text = "Days worked (1)"
        Me.lblDaysWorked1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDaysWorked1
        '
        Me.cboDaysWorked1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDaysWorked1.DropDownWidth = 230
        Me.cboDaysWorked1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDaysWorked1.FormattingEnabled = True
        Me.cboDaysWorked1.Location = New System.Drawing.Point(123, 87)
        Me.cboDaysWorked1.Name = "cboDaysWorked1"
        Me.cboDaysWorked1.Size = New System.Drawing.Size(126, 21)
        Me.cboDaysWorked1.TabIndex = 113
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(123, 302)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 111
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'chkShowLogo
        '
        Me.chkShowLogo.Checked = True
        Me.chkShowLogo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowLogo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLogo.Location = New System.Drawing.Point(123, 279)
        Me.chkShowLogo.Name = "chkShowLogo"
        Me.chkShowLogo.Size = New System.Drawing.Size(280, 17)
        Me.chkShowLogo.TabIndex = 92
        Me.chkShowLogo.Text = "Show Logo"
        Me.chkShowLogo.UseVisualStyleBackColor = True
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(304, 302)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(126, 17)
        Me.chkIgnoreZero.TabIndex = 90
        Me.chkIgnoreZero.Text = "Ignore Zero"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        Me.chkIgnoreZero.Visible = False
        '
        'lblYearQuarter
        '
        Me.lblYearQuarter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYearQuarter.Location = New System.Drawing.Point(9, 62)
        Me.lblYearQuarter.Name = "lblYearQuarter"
        Me.lblYearQuarter.Size = New System.Drawing.Size(108, 17)
        Me.lblYearQuarter.TabIndex = 86
        Me.lblYearQuarter.Text = "Year Quarter"
        Me.lblYearQuarter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYearQuarter
        '
        Me.cboYearQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYearQuarter.DropDownWidth = 230
        Me.cboYearQuarter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYearQuarter.FormattingEnabled = True
        Me.cboYearQuarter.Location = New System.Drawing.Point(123, 60)
        Me.cboYearQuarter.Name = "cboYearQuarter"
        Me.cboYearQuarter.Size = New System.Drawing.Size(126, 21)
        Me.cboYearQuarter.TabIndex = 85
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(255, 332)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(188, 15)
        Me.LblCurrencyRate.TabIndex = 79
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(255, 310)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(94, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblCurrency.Visible = False
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(367, 308)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 76
        Me.cboCurrency.Visible = False
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(357, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 35)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(108, 15)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(123, 33)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(126, 21)
        Me.cboMembership.TabIndex = 65
        '
        'cboAssiette
        '
        Me.cboAssiette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssiette.DropDownWidth = 230
        Me.cboAssiette.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssiette.FormattingEnabled = True
        Me.cboAssiette.Location = New System.Drawing.Point(123, 168)
        Me.cboAssiette.Name = "cboAssiette"
        Me.cboAssiette.Size = New System.Drawing.Size(126, 21)
        Me.cboAssiette.TabIndex = 168
        '
        'LblAssiette
        '
        Me.LblAssiette.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAssiette.Location = New System.Drawing.Point(9, 168)
        Me.LblAssiette.Name = "LblAssiette"
        Me.LblAssiette.Size = New System.Drawing.Size(108, 29)
        Me.LblAssiette.TabIndex = 169
        Me.LblAssiette.Text = "Assiette soumis à cotisation"
        Me.LblAssiette.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmRapportTrimestrielCNAMGS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 533)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRapportTrimestrielCNAMGS"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Rapport Trimestriel CNAMGS"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents chkShowLogo As System.Windows.Forms.CheckBox
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents lblYearQuarter As System.Windows.Forms.Label
    Friend WithEvents cboYearQuarter As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblDaysWorked1 As System.Windows.Forms.Label
    Friend WithEvents cboDaysWorked1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblDaysWorked3 As System.Windows.Forms.Label
    Friend WithEvents cboDaysWorked3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblDaysWorked2 As System.Windows.Forms.Label
    Friend WithEvents cboDaysWorked2 As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPeriod As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Public WithEvents txtEmployeurRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEmployeurRate As System.Windows.Forms.Label
    Public WithEvents txtTravailleurRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTravailleurRate As System.Windows.Forms.Label
    Friend WithEvents objbtnKeywordsPF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPlafondFormula As System.Windows.Forms.Label
    Friend WithEvents txtPlafondFormula As System.Windows.Forms.TextBox
    Friend WithEvents objcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColhPeriodCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPeriodName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodCustomCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodEnd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblAssiette As System.Windows.Forms.Label
    Friend WithEvents cboAssiette As System.Windows.Forms.ComboBox
End Class
