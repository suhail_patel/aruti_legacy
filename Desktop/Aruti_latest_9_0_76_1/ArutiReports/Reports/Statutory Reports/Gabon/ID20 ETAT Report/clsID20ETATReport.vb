﻿'************************************************************************************************************************************
'Class Name : clsID20ETATReport.vb
'Purpose    :
'Date       :03/12/2021
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text
Imports System.Text.RegularExpressions

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsID20ETATReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsID20ETATReport"
    Private mstrReportId As String = enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report
    Private objDataOperation As clsDataOperation
    Private mdtFinal As DataTable = Nothing

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Enum "
    
#End Region

#Region " Private Variables "

    Private mstrPeriodIDs As String = String.Empty
    Private mstrPeriodNames As String = String.Empty
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mintFromPeriodId As Integer
    Private mstrFromPeriodName As String = String.Empty
    Private mintToPeriodId As Integer
    Private mstrToPeriodName As String = String.Empty
    Private mdicYearDBName As New Dictionary(Of Integer, String)
    Private mstrFirstPeriodYearName As String = ""

    Private mstrSalaireBrutInferieurFormula As String = String.Empty
    Private mstrSalaireBrutSuperieurFormula As String = String.Empty

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrCurrencyName As String = ""

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty


#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodIDs() As String
        Set(ByVal value As String)
            mstrPeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public Property _PeriodStart() As Date
        Get
            Return mdtPeriodStart
        End Get
        Set(ByVal value As Date)
            mdtPeriodStart = value
        End Set
    End Property

    Public Property _PeriodEnd() As Date
        Get
            Return mdtPeriodEnd
        End Get
        Set(ByVal value As Date)
            mdtPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _dic_YearDBName() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicYearDBName = value
        End Set
    End Property

    Public WriteOnly Property _FirstPeriodYearName() As String
        Set(ByVal value As String)
            mstrFirstPeriodYearName = value
        End Set
    End Property

    Public WriteOnly Property _SalaireBrutInferieurFormula() As String
        Set(ByVal value As String)
            mstrSalaireBrutInferieurFormula = value
        End Set
    End Property

    Public WriteOnly Property _SalaireBrutSuperieurFormula() As String
        Set(ByVal value As String)
            mstrSalaireBrutSuperieurFormula = value
        End Set
    End Property

    


    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property



    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property



#End Region

#Region " Public Function & Procedures "


    Public Sub SetDefaultValue()
        Try
            
            mstrPeriodIDs = String.Empty
            mintFromPeriodId = 0
            mstrFromPeriodName = String.Empty
            mintToPeriodId = 0
            mstrToPeriodName = String.Empty
            mstrSalaireBrutInferieurFormula = String.Empty
            mstrSalaireBrutSuperieurFormula = String.Empty

            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            mstrCurrencyName = String.Empty
            mstrFirstPeriodYearName = ""

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String _
                                    , ByVal intUserUnkid As Integer _
                                    , ByVal intYearUnkid As Integer _
                                    , ByVal intCompanyUnkid As Integer _
                                    , ByVal dtPeriodStart As Date _
                                    , ByVal dtPeriodEnd As Date _
                                    , ByVal strUserModeSetting As String _
                                    , ByVal blnOnlyApproved As Boolean _
                                    , ByVal dtFinancialYearStart As Date _
                                    , ByVal intBaseCurrencyId As Integer _
                                    , ByVal strFmtCurrency As String _
                                    , ByVal strExportReportPath As String _
                                    , ByVal blnOpenAfterExport As Boolean _
                                    , ByVal blnExcelHTML As Boolean _
                                    )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objMembership As New clsmembership_master
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Dim dsList As New DataSet
        Dim strBaseCurrencySign As String
        Dim colIdx As Integer = 0

        objDataOperation = New clsDataOperation
        Try
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            objCompany._Companyunkid = intCompanyUnkid

            Dim objHead As New clsTransactionHead
            Dim objDHead As New Dictionary(Of Integer, String)
            Dim dsHead As DataSet = objHead.getComboList(strDatabaseName, "List", False, , , , True, , , , True, , , True)

            Dim strPattern As String = "([^#]*#[^#]*)#"
            Dim strReplacement As String = "$1)"

            If mstrSalaireBrutInferieurFormula.Trim <> "" AndAlso mstrSalaireBrutInferieurFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrSalaireBrutInferieurFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If

            If mstrSalaireBrutSuperieurFormula.Trim <> "" AndAlso mstrSalaireBrutSuperieurFormula.Contains("#") = True Then

                Dim strResult As String = Regex.Replace(mstrSalaireBrutSuperieurFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )

                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
            End If


            Dim strCurrDatabaseName As String = ""
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String

            For Each key In mdicYearDBName
                strCurrDatabaseName = key.Value

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , False, key.Value.ToString)
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, key.Value.ToString, intUserUnkid, intCompanyUnkid, key.Key, strUserModeSetting)

                StrQ &= "SELECT hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", " & key.Key & " AS YearId "

                StrQ &= "INTO #" & strCurrDatabaseName & " " & _
                        "FROM " & strCurrDatabaseName & "..hremployee_master " & _
                        "LEFT JOIN " & _
                        "( " & _
                            "SELECT TERM.TEEmpId " & _
                                 ", TERM.empl_enddate " & _
                                 ", TERM.termination_from_date " & _
                                 ", TERM.TEfDt " & _
                                 ", TERM.isexclude_payroll " & _
                                 ", TERM.changereasonunkid " & _
                            "FROM " & _
                            "( " & _
                                "SELECT TRM.employeeunkid AS TEEmpId " & _
                                     ", CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                                     ", CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                                     ", CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                                     ", TRM.isexclude_payroll " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                     ", TRM.changereasonunkid " & _
                                "FROM " & strCurrDatabaseName & "..hremployee_dates_tran AS TRM " & _
                                "WHERE isvoid = 0 " & _
                                      "AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                                      "AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= @enddate " & _
                            ") AS TERM " & _
                            "WHERE TERM.Rno = 1 " & _
                        ") AS ETERM " & _
                            "ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                               "AND ETERM.TEfDt >= CONVERT(CHAR(8), appointeddate, 112) "

                StrQ &= mstrAnalysis_Join

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " WHERE 1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


            Next


            Dim i As Integer = -1

            StrQ &= "SELECT  * " & _
                    "INTO #TableEmp " & _
                    "FROM ( "

            StrQ &= "SELECT    A.employeeunkid " & _
                            ", A.employeecode " & _
                            ", DENSE_RANK() OVER(PARTITION BY A.employeeunkid ORDER BY A.YearId DESC) AS ROWNO " & _
                    "FROM ( "

            For Each key In mdicYearDBName
                strCurrDatabaseName = key.Value
                i += 1

                If i > 0 Then
                    StrQ &= " UNION ALL "
                End If

                StrQ &= "SELECT #" & strCurrDatabaseName & ".employeeunkid " & _
                            ", #" & strCurrDatabaseName & ".employeecode " & _
                            ", #" & strCurrDatabaseName & ".YearId " & _
                        "FROM #" & strCurrDatabaseName & " "

            Next

            StrQ &= " ) AS A " & _
                    " ) AS B " & _
                      "WHERE B.ROWNO = 1 "
            

            i = -1
            

            StrQ &= "SELECT #TableEmp.employeeunkid " & _
                         ", #TableEmp.employeecode "

            For Each itm In objDHead
                StrQ &= " , ISNULL(SUM([" & itm.Key & "]), 0) AS [" & itm.Key & "] "
            Next

            StrQ &= "FROM " & _
                    "( "
                        
            If objDHead.Count > 0 Then

            For Each key In mdicYearDBName
                    strCurrDatabaseName = key.Value

                For Each itm In objDHead
                    i += 1

                    If i > 0 Then
                        StrQ &= " UNION ALL "
                    End If

                    StrQ &= "SELECT prpayrollprocess_tran.employeeunkid "

                    For Each itm1 In objDHead
                        If itm1.Key = itm.Key Then
                            StrQ &= " , SUM(ISNULL(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS [" & itm1.Key & "] "
                        Else
                            StrQ &= " , 0 AS [" & itm1.Key & "] "
                        End If
                    Next

                    StrQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                            "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 "

                    StrQ &= "          WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIDs & ") " & _
                                                "AND prtranhead_master.tranheadunkid = " & itm.Key & " " & _
                                                "AND prpayrollprocess_tran.amount > 0 " & _
                                      "GROUP BY prpayrollprocess_tran.employeeunkid "
                    Next

                Next

            Else
                StrQ &= " SELECT 0 AS employeeunkid "
            End If

            StrQ &= ") AS A " & _
                    "RIGHT JOIN #TableEmp " & _
                                "ON A.employeeunkid = #TableEmp.employeeunkid " & _
                    "GROUP BY #TableEmp.employeeunkid " & _
                            ", #TableEmp.employeecode "



            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value.ToString & " "
            Next

            StrQ &= " DROP TABLE #TableEmp "


            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEnd))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Columns.Contains("BrutInferieur") = False Then
                dsList.Tables(0).Columns.Add("BrutInferieur", Type.GetType("System.Decimal")).DefaultValue = 0
            End If
            If dsList.Tables(0).Columns.Contains("BrutSuperieur") = False Then
                dsList.Tables(0).Columns.Add("BrutSuperieur", Type.GetType("System.Decimal")).DefaultValue = 0
            End If

            Dim decCount1 As Decimal = 0
            Dim decCount2 As Decimal = 0

            Dim decAmount1 As Decimal = 0
            Dim decAmount2 As Decimal = 0

            For Each dsRow As DataRow In dsList.Tables(0).Rows

                If mstrSalaireBrutInferieurFormula.Trim <> "" AndAlso mstrSalaireBrutInferieurFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrSalaireBrutInferieurFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow.Item("BrutInferieur") = Format(CDec(ans), strFmtCurrency)
                Else
                    Decimal.TryParse(mstrSalaireBrutInferieurFormula, decAmount1)
                    dsRow.Item("BrutInferieur") = Format(0, strFmtCurrency)
                End If

                If mstrSalaireBrutSuperieurFormula.Trim <> "" AndAlso mstrSalaireBrutSuperieurFormula.Contains("#") = True Then
                    Dim dt As New DataTable
                    Dim s As String = mstrSalaireBrutSuperieurFormula
                    Dim ans As Object = Nothing

                    For Each itm In objDHead
                        s = s.Replace("#" & itm.Value & "#", Format(CDec(dsRow.Item(itm.Key.ToString)), strFmtCurrency))
                    Next
                    Try
                        ans = dt.Compute(s.ToString.Replace(",", ""), "")
                    Catch ex As Exception

                    End Try
                    If ans Is Nothing Then
                        ans = 0
                    End If
                    dsRow.Item("BrutSuperieur") = Format(CDec(ans), strFmtCurrency)
                Else
                    Decimal.TryParse(mstrSalaireBrutSuperieurFormula, decAmount2)
                    dsRow.Item("BrutSuperieur") = Format(0, strFmtCurrency)
                End If

            Next
            dsList.Tables(0).AcceptChanges()

            

            Dim mdtTableExcel As DataTable
            Dim intColIndex As Integer = -1
            mdtTableExcel = dsList.Tables(0)
            Dim mdecBrutLimit As Decimal = 1000000

            If mstrSalaireBrutInferieurFormula.Trim <> "" AndAlso mstrSalaireBrutInferieurFormula.Contains("#") = True Then
                decCount1 = (From p In mdtTableExcel Where (CDec(p.Item("BrutInferieur")) <> 0 AndAlso CDec(p.Item("BrutInferieur")) <= mdecBrutLimit) Select (CInt(p.Item("employeeunkid")))).Distinct().Count
                decAmount1 = (From p In mdtTableExcel Where (CDec(p.Item("BrutInferieur")) <= mdecBrutLimit) Select (CDec(p.Item("BrutInferieur")))).Sum()
            Else
                decCount1 = (From p In mdtTableExcel Select (CInt(p.Item("employeeunkid")))).Distinct().Count
            End If


            If mstrSalaireBrutSuperieurFormula.Trim <> "" AndAlso mstrSalaireBrutSuperieurFormula.Contains("#") = True Then
                decCount2 = (From p In mdtTableExcel Where (CDec(p.Item("BrutSuperieur")) <> 0 AndAlso CDec(p.Item("BrutSuperieur")) > mdecBrutLimit) Select (CInt(p.Item("employeeunkid")))).Distinct().Count
                decAmount2 = (From p In mdtTableExcel Where (CDec(p.Item("BrutSuperieur")) > mdecBrutLimit) Select (CDec(p.Item("BrutSuperieur")))).Sum()
            Else
                decCount2 = (From p In mdtTableExcel Select (CInt(p.Item("employeeunkid")))).Distinct().Count
            End If

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing
            Dim strBuilder As New StringBuilder
            Dim arrWorkSheetStyle As List(Of WorksheetStyle) = Nothing
            Dim strWorkSheetStyle As String = ""
            Dim intInitRows As Integer = 0

            If blnExcelHTML = False Then
                arrWorkSheetStyle = New List(Of WorksheetStyle)
                Dim s10bc_lr As New WorksheetStyle("s10bc_lr")
                With s10bc_lr
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10bc_lr)

                Dim s10bc_l As New WorksheetStyle("s10bc_l")
                With s10bc_l
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10bc_l)

                Dim s10c_l As New WorksheetStyle("s10c_l")
                With s10c_l
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10c_l)

                Dim s10bc_r As New WorksheetStyle("s10bc_r")
                With s10bc_r
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Center
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10bc_r)

                Dim s10c As New WorksheetStyle("s10c")
                With s10c
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Alignment.WrapText = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .NumberFormat = "General"
                End With
                arrWorkSheetStyle.Add(s10c)

                Dim n10bc As New WorksheetStyle("n10bc")
                With n10bc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n10bc)

                Dim i10bc As New WorksheetStyle("i10bc")
                With i10bc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = "############0"
                End With
                arrWorkSheetStyle.Add(i10bc)

                Dim s10bcw As New WorksheetStyle("s10bcw")
                With s10bcw
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcw)

                Dim s10bcblue As New WorksheetStyle("s10bcblue")
                With s10bcblue
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.WrapText = True
                    .Font.Color = "#5877FC"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bcblue)

                Dim s10bred As New WorksheetStyle("s10bred")
                With s10bred
                    .Font.FontName = "Tahoma"
                    .Font.Size = 10
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Left
                    .Alignment.WrapText = True
                    .Font.Color = "#FF0000"
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                End With
                arrWorkSheetStyle.Add(s10bred)

                Dim n8wc As New WorksheetStyle("n8wc")
                With n8wc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Color = "#000000"
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8wc)

                Dim n8bwc As New WorksheetStyle("n8bwc")
                With n8bwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Right
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bwc)

                Dim n8bcwc As New WorksheetStyle("n8bcwc")
                With n8bcwc
                    .Font.FontName = "Tahoma"
                    .Font.Size = 8
                    .Font.Bold = True
                    .Alignment.Horizontal = StyleHorizontalAlignment.Center
                    .Alignment.Vertical = StyleVerticalAlignment.Top
                    .Alignment.WrapText = True
                    .Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1)
                    .Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1)
                    .NumberFormat = GUI.fmtCurrency.Replace(",", "")
                End With
                arrWorkSheetStyle.Add(n8bcwc)



                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_lrtb")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_lr")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE"), "s10bc_l")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "MINISTERE DE L’ECONOMIE, DE L'EMPLOIE ET"), "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "ETAT DE LA MASSE SALARIALE"), "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 4, "DU DEVELOPPEMENT  DURABLE"), "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "......................"), "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Exercice :") & " " & mstrFirstPeriodYearName & " " & Language.getMessage(mstrModuleName, 7, "(au titre duquel les salaires ont été versés)"), "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Direction Générale des Impôts"), "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "BP 37 / 45 – Libreville"), "s10c_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Tel : 79.53.76/77"), "s10c_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "La présente déclaration est à déposer avant le 28 Fevrier"), "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "RAISON SOCIALE DE L’ENTREPRISE :") & " " & objCompany._Name.ToUpper, "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell("", "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bc_l")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "NIF :") & " " & If(objCompany._Ppfno.Trim <> "", "| ", "") & String.Join("| ", (From p In objCompany._Ppfno.Trim.AsEnumerable Select (p.ToString.ToUpper)).ToArray()) & If(objCompany._Ppfno.Trim <> "", " |", ""), "s10bc_r")
                wcell.MergeAcross = 2
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_lr")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_lr")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8_ltb")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Rubrique"), "s10bc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Nombre de salariés"), "s10bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Total des rémunérations payées en"), "s10bc")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Employés percevant un salaire brut inférieur à 1 000 000 de francs CFA par mois"), "s10c")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decCount1.ToString, DataType.Number, "i10bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decAmount1.ToString, DataType.Number, "n10bc")
                row.Cells.Add(wcell)
                row.AutoFitHeight = True
                row.Height = 30
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Employés percevant un salaire brut supérieur à 1 000 000 de francs CFA par mois"), "s10c")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decCount2.ToString, DataType.Number, "i10bc")
                row.Cells.Add(wcell)
                wcell = New WorksheetCell(decAmount2.ToString, DataType.Number, "n10bc")
                row.Cells.Add(wcell)
                row.AutoFitHeight = True
                row.Height = 30
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "TOTAL"), "s10bc")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
                wcell = New WorksheetCell()
                wcell.Data.Type = DataType.Number
                wcell.StyleID = "i10bc"
                wcell.Formula = "=SUM(R" & (rowsArrayHeader.Count - 1) & "C3:R" & (rowsArrayHeader.Count) & "C3)"
                row.Cells.Add(wcell)
                wcell = New WorksheetCell()
                wcell.Data.Type = DataType.Number
                wcell.StyleID = "n10bc"
                wcell.Formula = "=SUM(R" & (rowsArrayHeader.Count - 1) & "C4:R" & (rowsArrayHeader.Count) & "C4)"
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "NB : Prendre les salaires bruts versés après déduction des retenues pour retraite et Sécurité sociale."), "s10")
                wcell.MergeAcross = 3
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)

            Else
            
            End If


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                If ii = 0 Then
                    intArrayColumnWidth(ii) = 200
                ElseIf ii = 2 Then
                    intArrayColumnWidth(ii) = 150
                ElseIf ii = 3 Then
                    intArrayColumnWidth(ii) = 180
                Else
                    intArrayColumnWidth(ii) = 120
                End If
            Next

            Dim dtTable As DataTable = mdtTableExcel.Clone
            If blnExcelHTML = False Then
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, dtTable, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, marrWorksheetStyle:=arrWorkSheetStyle.ToArray)
            Else
                Call ReportExecute(intCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelHTML, strExportReportPath, blnOpenAfterExport, dtTable, intArrayColumnWidth, False, False, False, Nothing, , , , , , , rowsArrayHeader, rowsArrayFooter, mstrWorksheetStyle:=strWorkSheetStyle)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "REPUBLIQUE GABONAISE")
			Language.setMessage(mstrModuleName, 2, "MINISTERE DE L’ECONOMIE, DE L'EMPLOIE ET")
			Language.setMessage(mstrModuleName, 3, "ETAT DE LA MASSE SALARIALE")
			Language.setMessage(mstrModuleName, 4, "DU DEVELOPPEMENT  DURABLE")
			Language.setMessage(mstrModuleName, 5, "......................")
			Language.setMessage(mstrModuleName, 6, "Exercice :")
			Language.setMessage(mstrModuleName, 7, "(au titre duquel les salaires ont été versés)")
			Language.setMessage(mstrModuleName, 8, "Direction Générale des Impôts")
			Language.setMessage(mstrModuleName, 9, "BP 37 / 45 – Libreville")
			Language.setMessage(mstrModuleName, 10, "Tel : 79.53.76/77")
			Language.setMessage(mstrModuleName, 11, "La présente déclaration est à déposer avant le 28 Fevrier")
			Language.setMessage(mstrModuleName, 12, "RAISON SOCIALE DE L’ENTREPRISE :")
			Language.setMessage(mstrModuleName, 13, "NIF :")
			Language.setMessage(mstrModuleName, 14, "Rubrique")
			Language.setMessage(mstrModuleName, 15, "Nombre de salariés")
			Language.setMessage(mstrModuleName, 16, "Total des rémunérations payées en")
			Language.setMessage(mstrModuleName, 17, "Employés percevant un salaire brut inférieur à 1 000 000 de francs CFA par mois")
			Language.setMessage(mstrModuleName, 18, "Employés percevant un salaire brut supérieur à 1 000 000 de francs CFA par mois")
			Language.setMessage(mstrModuleName, 19, "TOTAL")
			Language.setMessage(mstrModuleName, 20, "NB : Prendre les salaires bruts versés après déduction des retenues pour retraite et Sécurité sociale.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
