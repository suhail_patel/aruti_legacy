﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCFPReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCFPReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRate = New eZee.TextBox.NumericTextBox
        Me.lblRate = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.cboBaseCFP = New System.Windows.Forms.ComboBox
        Me.lblBaseCFP = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboOtherBenefit = New System.Windows.Forms.ComboBox
        Me.lblOtherBenefit = New System.Windows.Forms.Label
        Me.cboCNSSContribution = New System.Windows.Forms.ComboBox
        Me.lblCNSSContribution = New System.Windows.Forms.Label
        Me.cboCashBenefit = New System.Windows.Forms.ComboBox
        Me.lblCashBenefit = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblBasicSalary = New System.Windows.Forms.Label
        Me.cboBasicSalary = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 435)
        Me.NavPanel.Size = New System.Drawing.Size(554, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtRate)
        Me.gbFilterCriteria.Controls.Add(Me.lblRate)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.cboBaseCFP)
        Me.gbFilterCriteria.Controls.Add(Me.lblBaseCFP)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.cboOtherBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.lblOtherBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.cboCNSSContribution)
        Me.gbFilterCriteria.Controls.Add(Me.lblCNSSContribution)
        Me.gbFilterCriteria.Controls.Add(Me.cboCashBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.lblCashBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblBasicSalary)
        Me.gbFilterCriteria.Controls.Add(Me.cboBasicSalary)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(377, 298)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRate
        '
        Me.txtRate.AllowNegative = True
        Me.txtRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtRate.DigitsInGroup = 0
        Me.txtRate.Flags = 0
        Me.txtRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRate.Location = New System.Drawing.Point(177, 195)
        Me.txtRate.MaxDecimalPlaces = 4
        Me.txtRate.MaxWholeDigits = 9
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Prefix = ""
        Me.txtRate.RangeMax = 1.7976931348623157E+308
        Me.txtRate.RangeMin = -1.7976931348623157E+308
        Me.txtRate.Size = New System.Drawing.Size(111, 21)
        Me.txtRate.TabIndex = 6
        Me.txtRate.Text = "0"
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRate
        '
        Me.lblRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRate.Location = New System.Drawing.Point(8, 197)
        Me.lblRate.Name = "lblRate"
        Me.lblRate.Size = New System.Drawing.Size(163, 15)
        Me.lblRate.TabIndex = 78
        Me.lblRate.Text = "Rate (%)"
        Me.lblRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(269, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 72
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'cboBaseCFP
        '
        Me.cboBaseCFP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBaseCFP.FormattingEnabled = True
        Me.cboBaseCFP.Location = New System.Drawing.Point(177, 168)
        Me.cboBaseCFP.Name = "cboBaseCFP"
        Me.cboBaseCFP.Size = New System.Drawing.Size(167, 21)
        Me.cboBaseCFP.TabIndex = 5
        '
        'lblBaseCFP
        '
        Me.lblBaseCFP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBaseCFP.Location = New System.Drawing.Point(8, 170)
        Me.lblBaseCFP.Name = "lblBaseCFP"
        Me.lblBaseCFP.Size = New System.Drawing.Size(163, 15)
        Me.lblBaseCFP.TabIndex = 33
        Me.lblBaseCFP.Text = "Base CFP"
        Me.lblBaseCFP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(177, 222)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 7
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboOtherBenefit
        '
        Me.cboOtherBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherBenefit.FormattingEnabled = True
        Me.cboOtherBenefit.Location = New System.Drawing.Point(177, 141)
        Me.cboOtherBenefit.Name = "cboOtherBenefit"
        Me.cboOtherBenefit.Size = New System.Drawing.Size(167, 21)
        Me.cboOtherBenefit.TabIndex = 4
        '
        'lblOtherBenefit
        '
        Me.lblOtherBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherBenefit.Location = New System.Drawing.Point(8, 143)
        Me.lblOtherBenefit.Name = "lblOtherBenefit"
        Me.lblOtherBenefit.Size = New System.Drawing.Size(163, 15)
        Me.lblOtherBenefit.TabIndex = 22
        Me.lblOtherBenefit.Text = "Other Benefit"
        Me.lblOtherBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCNSSContribution
        '
        Me.cboCNSSContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCNSSContribution.FormattingEnabled = True
        Me.cboCNSSContribution.Location = New System.Drawing.Point(177, 114)
        Me.cboCNSSContribution.Name = "cboCNSSContribution"
        Me.cboCNSSContribution.Size = New System.Drawing.Size(167, 21)
        Me.cboCNSSContribution.TabIndex = 3
        '
        'lblCNSSContribution
        '
        Me.lblCNSSContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCNSSContribution.Location = New System.Drawing.Point(8, 116)
        Me.lblCNSSContribution.Name = "lblCNSSContribution"
        Me.lblCNSSContribution.Size = New System.Drawing.Size(163, 15)
        Me.lblCNSSContribution.TabIndex = 16
        Me.lblCNSSContribution.Text = "CNSS Contribution"
        Me.lblCNSSContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCashBenefit
        '
        Me.cboCashBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCashBenefit.FormattingEnabled = True
        Me.cboCashBenefit.Location = New System.Drawing.Point(177, 87)
        Me.cboCashBenefit.Name = "cboCashBenefit"
        Me.cboCashBenefit.Size = New System.Drawing.Size(167, 21)
        Me.cboCashBenefit.TabIndex = 2
        '
        'lblCashBenefit
        '
        Me.lblCashBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashBenefit.Location = New System.Drawing.Point(8, 89)
        Me.lblCashBenefit.Name = "lblCashBenefit"
        Me.lblCashBenefit.Size = New System.Drawing.Size(163, 15)
        Me.lblCashBenefit.TabIndex = 10
        Me.lblCashBenefit.Text = "Cash Benefit"
        Me.lblCashBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(177, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(163, 15)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBasicSalary
        '
        Me.lblBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicSalary.Location = New System.Drawing.Point(8, 62)
        Me.lblBasicSalary.Name = "lblBasicSalary"
        Me.lblBasicSalary.Size = New System.Drawing.Size(163, 15)
        Me.lblBasicSalary.TabIndex = 4
        Me.lblBasicSalary.Text = "Basic Salary"
        Me.lblBasicSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBasicSalary
        '
        Me.cboBasicSalary.DropDownWidth = 180
        Me.cboBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBasicSalary.FormattingEnabled = True
        Me.cboBasicSalary.Location = New System.Drawing.Point(177, 60)
        Me.cboBasicSalary.Name = "cboBasicSalary"
        Me.cboBasicSalary.Size = New System.Drawing.Size(167, 21)
        Me.cboBasicSalary.TabIndex = 1
        '
        'frmCFPReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(554, 490)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmCFPReport"
        Me.Text = "frmCFPReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents cboBaseCFP As System.Windows.Forms.ComboBox
    Friend WithEvents lblBaseCFP As System.Windows.Forms.Label
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboOtherBenefit As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherBenefit As System.Windows.Forms.Label
    Friend WithEvents cboCNSSContribution As System.Windows.Forms.ComboBox
    Friend WithEvents lblCNSSContribution As System.Windows.Forms.Label
    Friend WithEvents cboCashBenefit As System.Windows.Forms.ComboBox
    Friend WithEvents lblCashBenefit As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblBasicSalary As System.Windows.Forms.Label
    Friend WithEvents cboBasicSalary As System.Windows.Forms.ComboBox
    Friend WithEvents lblRate As System.Windows.Forms.Label
    Public WithEvents txtRate As eZee.TextBox.NumericTextBox
End Class
