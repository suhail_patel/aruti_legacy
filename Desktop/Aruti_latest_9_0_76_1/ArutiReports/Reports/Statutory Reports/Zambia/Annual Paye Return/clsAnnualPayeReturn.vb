'************************************************************************************************************************************
'Class Name : clsAnnualPayeReturn.vb
'Purpose    :
'Date        :12/07/2013
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsAnnualPayeReturn
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAnnualPayeReturn"
    Private mstrReportId As String = enArutiReport.Za_AnnualPayeReturn  '15
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private minSlabTranId As Integer = 0
    Private mstrPeriodIds As String = String.Empty
    Private mintPermanantEmpTypeId As Integer = -1
    Private mstrPermanantEmpType As String = ""
    Private mintTemparoryEmpTypeId As Integer = -1
    Private mstrTemparoryEmpType As String = ""
    Private mstrEndPeriodName As String = String.Empty
    Private mdtEndPeriodDate As DateTime
    Private mintPeriodCount As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _SlabTranId() As Integer
        Set(ByVal value As Integer)
            minSlabTranId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _EndPeriodName() As String
        Set(ByVal value As String)
            mstrEndPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EndPeriodDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndPeriodDate = value
        End Set
    End Property

    Public WriteOnly Property _PermanantEmpTypeID() As Integer
        Set(ByVal value As Integer)
            mintPermanantEmpTypeId = value
        End Set
    End Property

    Public WriteOnly Property _PermanantEmpType() As String
        Set(ByVal value As String)
            mstrPermanantEmpType = value
        End Set
    End Property

    Public WriteOnly Property _TemparoryEmpTypeID() As Integer
        Set(ByVal value As Integer)
            mintTemparoryEmpTypeId = value
        End Set
    End Property

    Public WriteOnly Property _TemparoryEmpType() As String
        Set(ByVal value As String)
            mstrTemparoryEmpType = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCount() As Integer
        Set(ByVal value As Integer)
            mintPeriodCount = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            minSlabTranId = 0
            mintPeriodCount = 0
            mdtEndPeriodDate = Nothing
            mintPermanantEmpTypeId = 0
            mstrPermanantEmpType = ""
            mintTemparoryEmpTypeId = 0
            mstrTemparoryEmpType = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsSlab As New DataSet
        Dim dsPermList As New DataSet
        Dim dsTempList As New DataSet
        Dim dsPeriodicData As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Slab As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            If mdtEndPeriodDate <> Nothing Then
                dtPeriodEnd = mdtEndPeriodDate
            End If
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            StrQ = "SELECT  SUM(Taxable) AS Taxable " & _
                       ", SUM(TaxAmt) AS TaxAmt " & _
                       ", SUM(Taxable) - SUM(TaxAmt) AS PaidAmt " & _
                       ", Id , GName " & _
                       " FROM " & _
                       "( " & _
                       "    SELECT    SUM(CAST(amount AS DECIMAL(36, 2))) AS Taxable , 0 AS TaxAmt "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM  vwPayroll " & _
                         " LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid AND GroupID =" & enViewPayroll_Group.Earning

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= " WHERE istaxable = 1 AND payperiodunkid IN (" & mstrPeriodIds & ")  "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Sohail (14 Sep 2017) -- Start
            'Issue - 69.1 - startIndex cannot be larger than length of string.
            'StrQ &= " GROUP BY " & mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "").Substring(1).Trim
            If mintViewIndex > 0 Then
                StrQ &= " GROUP BY " & mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "").Substring(1).Trim
            End If
            'Sohail (14 Sep 2017) -- End


            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= " UNION ALL " & _
                         " SELECT    0 AS Taxable , SUM(CAST(amount AS DECIMAL(36, 2))) TaxAmt "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM vwPayroll " & _
                         " LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
                         " LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= " WHERE  prtranhead_master.tranheadunkid = " & minSlabTranId & _
                         " AND payperiodunkid IN (" & mstrPeriodIds & ") "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Sohail (14 Sep 2017) -- Start
            'Issue - 69.1 - startIndex cannot be larger than length of string.
            'StrQ &= " GROUP BY " & mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "").Substring(1).Trim
            If mintViewIndex > 0 Then
                StrQ &= " GROUP BY " & mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "").Substring(1).Trim
            End If
            'Sohail (14 Sep 2017) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= " ) AS B    "




            StrQ &= " GROUP BY Id,GName"


            dsPeriodicData = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Row As DataRow = Nothing
            For Each dRow As DataRow In dsPeriodicData.Tables(0).Rows
                rpt_Row = rpt_Data.Tables(0).NewRow
                rpt_Row.Item("Column1") = Format(CDec(dRow("Taxable")), GUI.fmtCurrency)
                rpt_Row.Item("Column2") = Format(CDec(dRow("TaxAmt")), GUI.fmtCurrency)

                'Pinkal (09-Sep-2013) -- Start
                'Enhancement : TRA Changes   'AS PER THE OLAIS COMMENTS FROM ZRA(ZAMBIA REVENUE AUTHORITY) THAT DUE TAX AND PAID TAX AMOUNT SHOULD BE THE SAME

                'rpt_Row.Item("Column3") = Format(CDec(dRow("PaidAmt")), GUI.fmtCurrency)
                'rpt_Row.Item("Column4") = Format(CDec(dRow("TaxAmt")) - CDec(dRow("PaidAmt")), GUI.fmtCurrency)

                rpt_Row.Item("Column3") = Format(CDec(dRow("TaxAmt")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(rpt_Row.Item("Column2")) - CDec(rpt_Row.Item("Column3")), GUI.fmtCurrency)

                'Pinkal (09-Sep-2013) -- End

                rpt_Data.Tables(0).Rows.Add(rpt_Row)
            Next

            StrQ = "SELECT  CASE WHEN SrNo < Cnt " & _
                      "AND SrNo = 1 " & _
                      "THEN CONVERT(NVARCHAR(50), CASE WHEN inexcessof = 0 THEN @UpTo ELSE CONVERT(NVARCHAR(50), inexcessof) END) " & _
                      "WHEN SrNo < Cnt THEN CONVERT(NVARCHAR(50), inexcessof + 1) " & _
                      "WHEN SrNo = Cnt THEN @Above " & _
                      "END AS SLAB1 " & _
                      ", CASE WHEN SrNo = Cnt THEN CONVERT(NVARCHAR(50), inexcessof ) ELSE CONVERT(NVARCHAR(50), amountupto) END AS SLAB2 " & _
                      ", SrNo AS RangeNo " & _
                      ", 0 as 'PermCount' " & _
                      ", 0.00 as 'PermTotalIncome' " & _
                      ", 0 as 'TempCount' " & _
                      ", 0.00 as 'TempTotalIncome' " & _
                     "FROM  ( SELECT    ROW_NUMBER() OVER ( ORDER BY inexcessof ASC ) AS SrNo " & _
                                  ", CONVERT(DECIMAL(30, 2), inexcessof) AS inexcessof " & _
                                  ", CONVERT(DECIMAL(30, 2), amountupto) AS amountupto " & _
                                  ", ( SELECT    COUNT(prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid) " & _
                                            "FROM      prtranhead_inexcessslab_tran " & _
                                            "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                            "WHERE  tranheadunkid = @TranHeadId " & _
                                            "AND isvoid = 0 " & _
                                            "AND cfcommon_period_tran.end_date = ( SELECT " & _
                                                              "MAX(end_date) " & _
                                                              "FROM " & _
                                                              "prtranhead_inexcessslab_tran " & _
                                                              "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                              "WHERE " & _
                                                              "ISNULL(prtranhead_inexcessslab_tran.isvoid, " & _
                                                              "0) = 0 " & _
                                                              "AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
                                                              "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                                                              ") " & _
                                        ") AS Cnt " & _
                                "FROM  prtranhead_inexcessslab_tran " & _
                                "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                "WHERE     tranheadunkid = @TranHeadId " & _
                                "AND isvoid = 0 " & _
                                "AND cfcommon_period_tran.end_date = ( SELECT " & _
                                                              "MAX(end_date) " & _
                                                          "FROM " & _
                                                              "prtranhead_inexcessslab_tran " & _
                                                              "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                          "WHERE " & _
                                                              "ISNULL(prtranhead_inexcessslab_tran.isvoid, " & _
                                                              "0) = 0 " & _
                                                              "AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
                                                              "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                                                        ") " & _
                                ") AS SLB "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, minSlabTranId)
            objDataOperation.AddParameter("@UpTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Up To"))
            objDataOperation.AddParameter("@Above", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Above"))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndPeriodDate))

            dsSlab = objDataOperation.ExecQuery(StrQ, "Slab")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If mintPermanantEmpTypeId > 0 Then

                StrQ = "SELECT  prpayrollprocess_tran.employeeunkid " & _
                            ", ISNULL(SUM(amount), 0.00) AS Amount " & _
                            " FROM prpayrollprocess_tran " & _
                            " JOIN prtnaleave_tran ON prpayrollprocess_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            " AND prtnaleave_tran.isvoid = 0 " & _
                            " JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                            " AND prtranhead_master.istaxable = 1 " & _
                            " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                            " AND prtranhead_master.isvoid = 0 " & _
                            " JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid  AND hremployee_master.employmenttypeunkid = " & mintPermanantEmpTypeId

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & " ) AND prpayrollprocess_tran.tranheadunkid > 0 AND prpayrollprocess_tran.isvoid = 0 "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= " GROUP BY prpayrollprocess_tran.employeeunkid "

                dsPermList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If mintTemparoryEmpTypeId > 0 Then

                StrQ = "SELECT  prpayrollprocess_tran.employeeunkid " & _
                           ", ISNULL(SUM(amount), 0.00) AS Amount " & _
                           " FROM prpayrollprocess_tran " & _
                           " JOIN prtnaleave_tran ON prpayrollprocess_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                           " AND prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                           " AND prtnaleave_tran.isvoid = 0 " & _
                           " JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                           " AND prtranhead_master.istaxable = 1 " & _
                           " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                           " AND prtranhead_master.isvoid = 0 " & _
                           " JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid  AND hremployee_master.employmenttypeunkid = " & mintTemparoryEmpTypeId

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & " ) AND prpayrollprocess_tran.tranheadunkid > 0 AND prpayrollprocess_tran.isvoid = 0 "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= " GROUP BY prpayrollprocess_tran.employeeunkid "

                dsTempList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If


            rpt_Row = Nothing


            rpt_Slab = New ArutiReport.Designer.dsArutiReport
            For i As Integer = 0 To dsSlab.Tables(0).Rows.Count - 1
                rpt_Row = rpt_Slab.Tables(0).NewRow
                Dim mdcAmount As Decimal = 0
                Dim drPermRow() As DataRow = Nothing
                Dim drTempRow() As DataRow = Nothing

                Dim mdcPermTotalIncome As Decimal = 0
                Dim mdcTempTotalIncome As Decimal = 0

                If i = 0 Then
                    mdcAmount = 0

                    If mintPermanantEmpTypeId > 0 Then
                        drPermRow = dsPermList.Tables(0).Select("Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2")))
                        mdcPermTotalIncome = IIf(dsPermList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))) Is DBNull.Value, 0, dsPermList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))))
                    End If

                    If mintTemparoryEmpTypeId > 0 Then
                        drTempRow = dsTempList.Tables(0).Select("Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2")))
                        mdcTempTotalIncome = IIf(dsTempList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))) Is DBNull.Value, 0, dsTempList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))))
                    End If

                ElseIf i = dsSlab.Tables(0).Rows.Count - 1 Then

                    mdcAmount = CDec(dsSlab.Tables(0).Rows(i - 1)("SLAB2"))

                    If mintPermanantEmpTypeId > 0 Then
                        drPermRow = dsPermList.Tables(0).Select("Amount >= " & mdcAmount)
                        mdcPermTotalIncome = IIf(dsPermList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount) Is DBNull.Value, 0, dsPermList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount))
                    End If

                    If mintTemparoryEmpTypeId > 0 Then
                        drTempRow = dsTempList.Tables(0).Select("Amount >= " & mdcAmount)
                        mdcTempTotalIncome = IIf(dsTempList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount) Is DBNull.Value, 0, dsTempList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount))
                    End If


                Else
                    mdcAmount = CDec(dsSlab.Tables(0).Rows(i)("SLAB1"))

                    If mintPermanantEmpTypeId > 0 Then
                        drPermRow = dsPermList.Tables(0).Select("Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2")))
                        mdcPermTotalIncome = IIf(dsPermList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))) Is DBNull.Value, 0, dsPermList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))))
                    End If

                    If mintTemparoryEmpTypeId > 0 Then
                        drTempRow = dsTempList.Tables(0).Select("Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2")))
                        mdcTempTotalIncome = IIf(dsTempList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))) Is DBNull.Value, 0, dsTempList.Tables(0).Compute("SUM(Amount)", "Amount >= " & mdcAmount & " AND Amount <= " & CDec(dsSlab.Tables(0).Rows(i)("SLAB2"))))
                    End If

                End If


                If mintPermanantEmpTypeId > 0 AndAlso drPermRow.Length > 0 Then
                    dsSlab.Tables(0).Rows(i)("PermCount") = drPermRow.Length
                    dsSlab.Tables(0).Rows(i)("PermTotalIncome") = mdcPermTotalIncome
                End If

                If mintTemparoryEmpTypeId > 0 AndAlso drTempRow.Length > 0 Then
                    dsSlab.Tables(0).Rows(i)("TempCount") = drTempRow.Length
                    dsSlab.Tables(0).Rows(i)("TempTotalIncome") = mdcTempTotalIncome
                End If

                If i = 0 Then
                    rpt_Row.Item("Column1") = Language.getMessage(mstrModuleName, 1, "Up To")

                ElseIf i = dsSlab.Tables(0).Rows.Count - 1 Then
                    rpt_Row.Item("Column1") = Language.getMessage(mstrModuleName, 2, "Above")
                Else
                    rpt_Row.Item("Column1") = Format(CDec(dsSlab.Tables(0).Rows(i)("SLAB1")), GUI.fmtCurrency)
                End If

                rpt_Row.Item("Column2") = Format(CDec(dsSlab.Tables(0).Rows(i)("SLAB2")), GUI.fmtCurrency)
                rpt_Row.Item("Column3") = CInt(dsSlab.Tables(0).Rows(i)("PermCount"))
                rpt_Row.Item("Column4") = Format(CDec(dsSlab.Tables(0).Rows(i)("PermTotalIncome")), GUI.fmtCurrency)
                rpt_Row.Item("Column5") = CInt(dsSlab.Tables(0).Rows(i)("TempCount"))
                rpt_Row.Item("Column6") = Format(CDec(dsSlab.Tables(0).Rows(i)("TempTotalIncome")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = CInt(dsSlab.Tables(0).Compute("SUM(PermCount)", "1=1"))
                rpt_Row.Item("Column8") = Format(CDec(dsSlab.Tables(0).Compute("SUM(PermTotalIncome)", "1=1")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = CInt(dsSlab.Tables(0).Compute("SUM(TempCount)", "1=1"))
                rpt_Row.Item("Column10") = Format(CDec(dsSlab.Tables(0).Compute("SUM(TempTotalIncome)", "1=1")), GUI.fmtCurrency)
                rpt_Slab.Tables(0).Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptAnnualPayeReturn

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptSubEmpByIncome").SetDataSource(rpt_Slab)

            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "txtEmployerAddress", Company._Object._Address1 & " " & Company._Object._Address2)
            ReportFunction.TextChange(objRpt, "txtCompanyTPIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "txtCompanyAcNo", Company._Object._Company_Reg_No)

            ReportFunction.TextChange(objRpt, "txtCompanyHeading", Language.getMessage(mstrModuleName, 84, "ZAMBIA REVENUE AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtDirectTaxes", Language.getMessage(mstrModuleName, 3, "DIRECT TAXES DIVISION"))
            ReportFunction.TextChange(objRpt, "txtEmpAnnualPayeReturn", Language.getMessage(mstrModuleName, 4, "EMPLOYER’S ANNUAL PAYE RETURN"))
            ReportFunction.TextChange(objRpt, "txtChargeYear", Language.getMessage(mstrModuleName, 5, "CHARGE YEAR ENDED 31st DECEMBER....."))
            ReportFunction.TextChange(objRpt, "txtDateStamp", Language.getMessage(mstrModuleName, 6, "DATE STAMP"))
            'ReportFunction.TextChange(objRpt, "txtEmployerName", Language.getMessage(mstrModuleName, 7, "EMPLOYER’S NAME AND ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtTPIN", Language.getMessage(mstrModuleName, 8, "TPIN"))
            ReportFunction.TextChange(objRpt, "txtPart1", Language.getMessage(mstrModuleName, 9, "PART I"))
            ReportFunction.TextChange(objRpt, "txtReconciliationPaye", Language.getMessage(mstrModuleName, 10, "1. RECONCILIATION OF PAYE AS PER DETAILS IN PART II"))
            ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 11, "AMOUNT"))
            ReportFunction.TextChange(objRpt, "txtTotalchargeableWages", Language.getMessage(mstrModuleName, 12, "a. Total chargeable emoluments for the year, including salaries, wages, fees, commissions, bonuses, overtime, gratuity etc."))
            ReportFunction.TextChange(objRpt, "txtTotaltaxdue", Language.getMessage(mstrModuleName, 13, "b. Total tax due for the year (The total net tax deducted from the  emoluments)."))
            ReportFunction.TextChange(objRpt, "txtTotaltaxduringYear", Language.getMessage(mstrModuleName, 14, "c. Total tax already paid during the year (excluding penalties and interest)"))
            ReportFunction.TextChange(objRpt, "txtBalanceofTaxPayable", Language.getMessage(mstrModuleName, 15, "d. Balance of Tax Payable (b) - (c) ..........................................................................................."))
            ReportFunction.TextChange(objRpt, "txtBalanceOfTax", Language.getMessage(mstrModuleName, 16, "2. BALANCE OF TAX"))
            ReportFunction.TextChange(objRpt, "txtPenalties", Language.getMessage(mstrModuleName, 17, "Penalties of 5% of the tax due and interest at the ruling Bank of Zambia discount rate, will be charged on any amount which remains unpaid after the due date (14th April)."))
            ReportFunction.TextChange(objRpt, "txtSubmissionofReturn", Language.getMessage(mstrModuleName, 18, "3. SUBMISSION OF THE RETURN"))
            ReportFunction.TextChange(objRpt, "txtReturnindividuals", Language.getMessage(mstrModuleName, 19, "The Return must be submitted not later than 1st June……………….. Late submission of the return will attract penalties of 340 penalty units (K61, 200) per month or part thereof, for limited companies, and 170 penalty units (K30, 600) per month or part thereof for individuals."))
            ReportFunction.TextChange(objRpt, "txtGuidanceNotes", Language.getMessage(mstrModuleName, 20, "GUIDANCE NOTES ON COMPLETION OF PART II."))
            ReportFunction.TextChange(objRpt, "txtColumn1", Language.getMessage(mstrModuleName, 21, "Column1: Enter the NRC/TPIN of each employee."))
            ReportFunction.TextChange(objRpt, "txtColumn2", Language.getMessage(mstrModuleName, 22, "Column 2: Enter the full name of every employee who was or is still in your employment as at 31st March……."))
            ReportFunction.TextChange(objRpt, "txtColumn3", Language.getMessage(mstrModuleName, 23, "Column 3: Enter the total chargeable emoluments (as shown by column 2 of the Tax Deduction Card) for the year or the period during which the employee concerned was employed by you."))
            ReportFunction.TextChange(objRpt, "txtColumn4", Language.getMessage(mstrModuleName, 24, "Column 4: Enter the total tax credit for the year or period."))
            ReportFunction.TextChange(objRpt, "txtColumn5", Language.getMessage(mstrModuleName, 25, "Column 5: Enter details for every employee from whose emoluments tax was deducted by you during the year. Do not include any tax in respect of previous employment. Where no tax was deducted enter Nil."))
            ReportFunction.TextChange(objRpt, "txtColumn6", Language.getMessage(mstrModuleName, 26, "Column 6: Enter the net amount refunded by you where you have refunded to any employee more tax than you have deducted from him/her."))
            ReportFunction.TextChange(objRpt, "txtPart3", Language.getMessage(mstrModuleName, 27, "PART III"))
            ReportFunction.TextChange(objRpt, "txtMaineconomicactivity", Language.getMessage(mstrModuleName, 28, "1. Main economic activity"))
            ReportFunction.TextChange(objRpt, "txtForoffice", Language.getMessage(mstrModuleName, 29, "For official use"))
            ReportFunction.TextChange(objRpt, "txtMaineconomicFinalProduct", Language.getMessage(mstrModuleName, 30, "The main economic activity is the final product or service that the firm/establishment renders. Please be very specific, e.g. copper production, manufacturing of cement, Dairy farming, mixed farming, accounting and management consultancy, retailing of mixed groceries, sale of used cars, etc. If space permits, we can add provision for two secondary economic activities for those firms that have more than one economic activity"))
            ReportFunction.TextChange(objRpt, "txttypeoffirm", Language.getMessage(mstrModuleName, 31, "2. Type of firm/establishment:"))
            ReportFunction.TextChange(objRpt, "txtCentralGovernment", Language.getMessage(mstrModuleName, 32, "Central Government"))
            ReportFunction.TextChange(objRpt, "txtPvtcompany", Language.getMessage(mstrModuleName, 33, "Private Limited Company"))
            ReportFunction.TextChange(objRpt, "txtCooperative", Language.getMessage(mstrModuleName, 34, "Cooperative"))
            ReportFunction.TextChange(objRpt, "txtLocalGovernment", Language.getMessage(mstrModuleName, 35, "Local Government"))
            ReportFunction.TextChange(objRpt, "txtOtherBussiness", Language.getMessage(mstrModuleName, 36, "Other Private business (e.g. informal sector)"))
            ReportFunction.TextChange(objRpt, "txtChurch", Language.getMessage(mstrModuleName, 37, "Church/Charity/Non-profit Making Organization (NGO)"))
            ReportFunction.TextChange(objRpt, "txtParastatal", Language.getMessage(mstrModuleName, 38, "Parastatal"))
            ReportFunction.TextChange(objRpt, "txtIndividual", Language.getMessage(mstrModuleName, 39, "Individual"))
            ReportFunction.TextChange(objRpt, "txtPublicLimitedCompany", Language.getMessage(mstrModuleName, 40, "Public Limited Company"))
            ReportFunction.TextChange(objRpt, "txtOtherSpecify", Language.getMessage(mstrModuleName, 41, "Other (specify)"))


            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListofEmployees", Language.getMessage(mstrModuleName, 42, "LIST OF EMPLOYEES AS PER DEDUCTION CARDS"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListColumn1", Language.getMessage(mstrModuleName, 43, "1."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListColumn2", Language.getMessage(mstrModuleName, 44, "2."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListColumn3", Language.getMessage(mstrModuleName, 45, "3."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListColumn4", Language.getMessage(mstrModuleName, 46, "4."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListColumn5", Language.getMessage(mstrModuleName, 47, "5."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListColumn6", Language.getMessage(mstrModuleName, 48, "6."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtRegistration", Language.getMessage(mstrModuleName, 49, "National Registration Number (TPIN)"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtNameofEmp", Language.getMessage(mstrModuleName, 50, "Name of Employee"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtYearChargeable", Language.getMessage(mstrModuleName, 51, "Chargeable emoluments for the Year"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtTotaltaxcredit", Language.getMessage(mstrModuleName, 52, "Total tax credit for the year or period"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtTDCYEar", Language.getMessage(mstrModuleName, 53, "Tax deducted as shown on the TDC for the Year"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtTaxRefunded", Language.getMessage(mstrModuleName, 54, "Tax Refunded"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtFromContinution", Language.getMessage(mstrModuleName, 55, "*From Continuation Sheets…. …. …. …. …… …. … ... ........ ...."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtListEmployees", Language.getMessage(mstrModuleName, 56, "Totals…. …. …. …. …… …. …. … … … … …… ...... ......."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubDeductionCards"), "txtContniuationsheets", Language.getMessage(mstrModuleName, 57, "* WHERE THE LIST OF EMPLOYEES EXTENDS BEYOND THE NUMBER OF ROWS PROVIDED ABOVE, PLEASE USE CONTINUAT ION SHEETS AND INSERT THE RESPECTIVE TOTALS ON THESPACE PROVIDED ABOVE."))

            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtDistributionEmp", Language.getMessage(mstrModuleName, 58, "3. Distribution of Employees by Income Bracket (see example over leaf)"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEpByIncomeGrossMonth", Language.getMessage(mstrModuleName, 59, "Gross Income per Month"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEmpByIncomeNoofPermanent", Language.getMessage(mstrModuleName, 60, "Number of Permanent Employees in the bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEmpbyIncomeTotalIncome", Language.getMessage(mstrModuleName, 61, "Total Income within bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEmpByIncomeTemporaryEmp", Language.getMessage(mstrModuleName, 62, "Number of Temporary Employees in the bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEmpbyIncomeTotalIncome1", Language.getMessage(mstrModuleName, 61, "Total Income within bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEmpByIncomeTotal", Language.getMessage(mstrModuleName, 64, "Total"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubEmpByIncome"), "txtEmpByIncomeAllTaxableIncome", Language.getMessage(mstrModuleName, 65, "Note: Gross income must include ALL TAXABLE INCOME including allowances such as housing allowance, transport allowance,overtime pay, etc."))


            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpExample", Language.getMessage(mstrModuleName, 66, "Example"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmployees", Language.getMessage(mstrModuleName, 67, "If the company employs eight people at the following wage rates:"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtEmployeeSalMonth", Language.getMessage(mstrModuleName, 68, "2 employees at K250, 000 per month") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 69, "2 employee at K800, 000 per month") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 70, "2 employee at K900, 000 per month") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 71, "1 employee at K1, 200,000 per month (inc. housing allowance of K300,000)") & vbCrLf & _
                                                    Language.getMessage(mstrModuleName, 72, "1 employee at K2, 200,000 per month (inc. housing allowance of K500,000))"))

            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtTemporaryEmployee", Language.getMessage(mstrModuleName, 73, "In addition, the firms employs 3 temporary employees at 300,000 per month"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyForm", Language.getMessage(mstrModuleName, 74, "The form should be completed as follows:"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompnayEmpGrossInCome", Language.getMessage(mstrModuleName, 59, "Gross Income per Month"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpNoofPermanent", Language.getMessage(mstrModuleName, 60, "Number of Permanent Employees in the bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpTotalInCome", Language.getMessage(mstrModuleName, 61, "Total Income within bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpTemporaryEmp", Language.getMessage(mstrModuleName, 62, "Number of Temporary Employees in the bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpTotalInCome1", Language.getMessage(mstrModuleName, 61, "Total Income within bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpDeclaration", Language.getMessage(mstrModuleName, 75, "4. DECLARATION"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpDeclarationReturn", Language.getMessage(mstrModuleName, 76, "I declare that the details in this Return are true and correct."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpName", Language.getMessage(mstrModuleName, 77, "Name : ..........................................................................................................."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpCapacity", Language.getMessage(mstrModuleName, 78, "Capacity : ......................................................................................................"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpSignature", Language.getMessage(mstrModuleName, 79, "Signature : ....................................................................................................."))
            ReportFunction.TextChange(objRpt.Subreports("rptSubTotalCompanyEmp"), "txtCompanyEmpDate", Language.getMessage(mstrModuleName, 80, "Date : ............................................................................................................."))
            ReportFunction.TextChange(objRpt, "txtCFP18", Language.getMessage(mstrModuleName, 81, "CF P18"))
            ReportFunction.TextChange(objRpt, "txtCFP181", Language.getMessage(mstrModuleName, 81, "CF P18"))
            ReportFunction.TextChange(objRpt, "txtCFP182", Language.getMessage(mstrModuleName, 82, "CF/P18"))
            ReportFunction.TextChange(objRpt, "txtVersion", Language.getMessage(mstrModuleName, 83, "Version 0.2 04/03/03"))
            ReportFunction.TextChange(objRpt, "txtVersion1", Language.getMessage(mstrModuleName, 83, "Version 0.2 04/03/03"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Up To")
            Language.setMessage(mstrModuleName, 2, "Above")
            Language.setMessage(mstrModuleName, 3, "DIRECT TAXES DIVISION")
            Language.setMessage(mstrModuleName, 4, "EMPLOYER’S ANNUAL PAYE RETURN")
            Language.setMessage(mstrModuleName, 5, "CHARGE YEAR ENDED 31st DECEMBER.....")
            Language.setMessage(mstrModuleName, 6, "DATE STAMP")
            Language.setMessage(mstrModuleName, 8, "TPIN")
            Language.setMessage(mstrModuleName, 9, "PART I")
            Language.setMessage(mstrModuleName, 10, "1. RECONCILIATION OF PAYE AS PER DETAILS IN PART II")
            Language.setMessage(mstrModuleName, 11, "AMOUNT")
            Language.setMessage(mstrModuleName, 12, "a. Total chargeable emoluments for the year, including salaries, wages, fees, commissions, bonuses, overtime, gratuity etc.")
            Language.setMessage(mstrModuleName, 13, "b. Total tax due for the year (The total net tax deducted from the  emoluments).")
            Language.setMessage(mstrModuleName, 14, "c. Total tax already paid during the year (excluding penalties and interest)")
            Language.setMessage(mstrModuleName, 15, "d. Balance of Tax Payable (b) - (c) ...........................................................................................")
            Language.setMessage(mstrModuleName, 16, "2. BALANCE OF TAX")
            Language.setMessage(mstrModuleName, 17, "Penalties of 5% of the tax due and interest at the ruling Bank of Zambia discount rate, will be charged on any amount which remains unpaid after the due date (14th April).")
            Language.setMessage(mstrModuleName, 18, "3. SUBMISSION OF THE RETURN")
            Language.setMessage(mstrModuleName, 19, "The Return must be submitted not later than 1st June……………….. Late submission of the return will attract penalties of 340 penalty units (K61, 200) per month or part thereof, for limited companies, and 170 penalty units (K30, 600) per month or part thereof for individuals.")
            Language.setMessage(mstrModuleName, 20, "GUIDANCE NOTES ON COMPLETION OF PART II.")
            Language.setMessage(mstrModuleName, 21, "Column1: Enter the NRC/TPIN of each employee.")
            Language.setMessage(mstrModuleName, 22, "Column 2: Enter the full name of every employee who was or is still in your employment as at 31st March…….")
            Language.setMessage(mstrModuleName, 23, "Column 3: Enter the total chargeable emoluments (as shown by column 2 of the Tax Deduction Card) for the year or the period during which the employee concerned was employed by you.")
            Language.setMessage(mstrModuleName, 24, "Column 4: Enter the total tax credit for the year or period.")
            Language.setMessage(mstrModuleName, 25, "Column 5: Enter details for every employee from whose emoluments tax was deducted by you during the year. Do not include any tax in respect of previous employment. Where no tax was deducted enter Nil.")
            Language.setMessage(mstrModuleName, 26, "Column 6: Enter the net amount refunded by you where you have refunded to any employee more tax than you have deducted from him/her.")
            Language.setMessage(mstrModuleName, 27, "PART III")
            Language.setMessage(mstrModuleName, 28, "1. Main economic activity")
            Language.setMessage(mstrModuleName, 29, "For official use")
            Language.setMessage(mstrModuleName, 30, "The main economic activity is the final product or service that the firm/establishment renders. Please be very specific, e.g. copper production, manufacturing of cement, Dairy farming, mixed farming, accounting and management consultancy, retailing of mixed groceries, sale of used cars, etc. If space permits, we can add provision for two secondary economic activities for those firms that have more than one economic activity")
            Language.setMessage(mstrModuleName, 31, "2. Type of firm/establishment:")
            Language.setMessage(mstrModuleName, 32, "Central Government")
            Language.setMessage(mstrModuleName, 33, "Private Limited Company")
            Language.setMessage(mstrModuleName, 34, "Cooperative")
            Language.setMessage(mstrModuleName, 35, "Local Government")
            Language.setMessage(mstrModuleName, 36, "Other Private business (e.g. informal sector)")
            Language.setMessage(mstrModuleName, 37, "Church/Charity/Non-profit Making Organization (NGO)")
            Language.setMessage(mstrModuleName, 38, "Parastatal")
            Language.setMessage(mstrModuleName, 39, "Individual")
            Language.setMessage(mstrModuleName, 40, "Public Limited Company")
            Language.setMessage(mstrModuleName, 41, "Other (specify)")
            Language.setMessage(mstrModuleName, 42, "LIST OF EMPLOYEES AS PER DEDUCTION CARDS")
            Language.setMessage(mstrModuleName, 43, "1.")
            Language.setMessage(mstrModuleName, 44, "2.")
            Language.setMessage(mstrModuleName, 45, "3.")
            Language.setMessage(mstrModuleName, 46, "4.")
            Language.setMessage(mstrModuleName, 47, "5.")
            Language.setMessage(mstrModuleName, 48, "6.")
            Language.setMessage(mstrModuleName, 49, "National Registration Number (TPIN)")
            Language.setMessage(mstrModuleName, 50, "Name of Employee")
            Language.setMessage(mstrModuleName, 51, "Chargeable emoluments for the Year")
            Language.setMessage(mstrModuleName, 52, "Total tax credit for the year or period")
            Language.setMessage(mstrModuleName, 53, "Tax deducted as shown on the TDC for the Year")
            Language.setMessage(mstrModuleName, 54, "Tax Refunded")
            Language.setMessage(mstrModuleName, 55, "*From Continuation Sheets…. …. …. …. …… …. … ... ........ ....")
            Language.setMessage(mstrModuleName, 56, "Totals…. …. …. …. …… …. …. … … … … …… ...... .......")
            Language.setMessage(mstrModuleName, 57, "* WHERE THE LIST OF EMPLOYEES EXTENDS BEYOND THE NUMBER OF ROWS PROVIDED ABOVE, PLEASE USE CONTINUAT ION SHEETS AND INSERT THE RESPECTIVE TOTALS ON THESPACE PROVIDED ABOVE.")
            Language.setMessage(mstrModuleName, 58, "3. Distribution of Employees by Income Bracket (see example over leaf)")
            Language.setMessage(mstrModuleName, 59, "Gross Income per Month")
            Language.setMessage(mstrModuleName, 60, "Number of Permanent Employees in the bracket")
            Language.setMessage(mstrModuleName, 61, "Total Income within bracket")
            Language.setMessage(mstrModuleName, 62, "Number of Temporary Employees in the bracket")
            Language.setMessage(mstrModuleName, 64, "Total")
            Language.setMessage(mstrModuleName, 65, "Note: Gross income must include ALL TAXABLE INCOME including allowances such as housing allowance, transport allowance,overtime pay, etc.")
            Language.setMessage(mstrModuleName, 66, "Example")
            Language.setMessage(mstrModuleName, 67, "If the company employs eight people at the following wage rates:")
            Language.setMessage(mstrModuleName, 68, "2 employees at K250, 000 per month")
            Language.setMessage(mstrModuleName, 69, "2 employee at K800, 000 per month")
            Language.setMessage(mstrModuleName, 70, "2 employee at K900, 000 per month")
            Language.setMessage(mstrModuleName, 71, "1 employee at K1, 200,000 per month (inc. housing allowance of K300,000)")
            Language.setMessage(mstrModuleName, 72, "1 employee at K2, 200,000 per month (inc. housing allowance of K500,000))")
            Language.setMessage(mstrModuleName, 73, "In addition, the firms employs 3 temporary employees at 300,000 per month")
            Language.setMessage(mstrModuleName, 74, "The form should be completed as follows:")
            Language.setMessage(mstrModuleName, 75, "4. DECLARATION")
            Language.setMessage(mstrModuleName, 76, "I declare that the details in this Return are true and correct.")
            Language.setMessage(mstrModuleName, 77, "Name : ...........................................................................................................")
            Language.setMessage(mstrModuleName, 78, "Capacity : ......................................................................................................")
            Language.setMessage(mstrModuleName, 79, "Signature : .....................................................................................................")
            Language.setMessage(mstrModuleName, 80, "Date : .............................................................................................................")
            Language.setMessage(mstrModuleName, 81, "CF P18")
            Language.setMessage(mstrModuleName, 82, "CF/P18")
            Language.setMessage(mstrModuleName, 83, "Version 0.2 04/03/03")
            Language.setMessage(mstrModuleName, 84, "ZAMBIA REVENUE AUTHORITY")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
