'************************************************************************************************************************************
'Class Name   : clsZa_Monthly_Return_AMD.vb
'Purpose      :
'Date         :15/07/2013
'Written By   :Sandeep J. Sharma
'Modified     :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsZa_Monthly_Return_AMD
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsZa_Monthly_Return_AMD"
    Private mstrReportId As String = enArutiReport.Za_Monthly_AMD_Report    '121

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintEmpHeadTypeId As Integer = -1
    Private mintEmprHeadTypeId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintIdentityId As Integer = 0
    Private mstrIdentityName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrPeriodEndDate As String = String.Empty
    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private mstrPeriodCode As String = String.Empty
    Private mblnIsExportToCSV As Boolean = False
    'Sohail (14 May 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmpHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmpHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmprHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmprHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IdentityId() As Integer
        Set(ByVal value As Integer)
            mintIdentityId = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As String
        Set(ByVal value As String)
            mstrPeriodEndDate = value
        End Set
    End Property

    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Public WriteOnly Property _PeriodCode() As String
        Set(ByVal value As String)
            mstrPeriodCode = value
        End Set
    End Property

    Public WriteOnly Property _IsExportToCSV() As Boolean
        Set(ByVal value As Boolean)
            mblnIsExportToCSV = value
        End Set
    End Property
    'Sohail (14 May 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintEmpHeadTypeId = -1
            mintEmprHeadTypeId = -1
            mintPeriodId = -1
            mstrPeriodName = String.Empty
            mintIdentityId = 0
            mstrIdentityName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrPeriodEndDate = String.Empty
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            mstrPeriodCode = ""
            mblnIsExportToCSV = False
            'Sohail (14 May 2019) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, xExportReportPath)
            'Sohail (14 May 2019) -- End

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strExportPath As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (14 May 2019) - [strExportPath]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim SQry As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            Dim objDataOperation As New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""


            'StrQ = "SELECT TOP 1 database_name,periodunkid " & _
            '       "FROM cfcommon_period_tran " & _
            '       "    JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
            '       "WHERE CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) < '" & mstrPeriodEndDate & "' " & _
            '       "    AND modulerefid = '" & enModuleReference.Payroll & "' AND cfcommon_period_tran.isactive = 1 ORDER BY periodunkid DESC "

            StrQ = "SELECT TOP 1 database_name,periodunkid,cfcommon_period_tran.end_date,hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                   "FROM cfcommon_period_tran " & _
                   "    JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                   "WHERE CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) < '" & mstrPeriodEndDate & "' " & _
                   "    AND modulerefid = '" & enModuleReference.Payroll & "' AND cfcommon_period_tran.isactive = 1 ORDER BY periodunkid DESC "
            'S.SANDEEP [04 JUN 2015] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dsList.Tables(0).Rows(0)("end_date"), blnOnlyApproved, dsList.Tables("List").Rows(0).Item("database_name"), intUserUnkid, intCompanyUnkid, dsList.Tables("List").Rows(0).Item("yearunkid"), strUserModeSetting)
                'S.SANDEEP [04 JUN 2015] -- END

                SQry = "LEFT JOIN " & _
                       "( " & _
                       "	SELECT " & _
                       "	 Gross.Empid " & _
                       "	,SUM(Gross.Amount) AS EGross " & _
                       "	FROM " & _
                       "	( " & _
                       "		SELECT " & _
                       "			 " & dsList.Tables("List").Rows(0).Item("database_name") & "..hremployee_master.employeeunkid AS Empid " & _
                       "			,CAST(ISNULL(" & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                       "		FROM " & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran " & _
                       "			JOIN " & dsList.Tables("List").Rows(0).Item("database_name") & "..hremployee_master ON " & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.employeeunkid = " & dsList.Tables("List").Rows(0).Item("database_name") & "..hremployee_master.employeeunkid "

                SQry &= mstrAnalysis_Join.Replace("hremployee_master", dsList.Tables("List").Rows(0).Item("database_name") & "..hremployee_master")

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (24 May 2019) -- Start
                'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. - Claim Request Earning was not included in Total Earning
                'SQry &= "			JOIN " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtranhead_master ON " & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.tranheadunkid = " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtranhead_master.tranheadunkid " & _
                '       "			JOIN " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran ON " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran.tnaleavetranunkid = " & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.tnaleavetranunkid " & _
                '       "		WHERE " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtranhead_master.trnheadtype_id = '" & enTranHeadType.EarningForEmployees & "' " & _
                '       "			AND ISNULL(" & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(" & dsList.Tables("List").Rows(0).Item("database_name") & "..prtranhead_master.isvoid, 0) = 0 " & _
                '       "			AND ISNULL(" & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran.isvoid, 0) = 0 AND " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran.payperiodunkid = '" & dsList.Tables("List").Rows(0).Item("periodunkid") & "' "
                SQry &= "			JOIN " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran ON " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran.tnaleavetranunkid = " & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.tnaleavetranunkid " & _
                      "		WHERE " & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.add_deduct = '" & enTranHeadType.EarningForEmployees & "' " & _
                      "			AND ISNULL(" & dsList.Tables("List").Rows(0).Item("database_name") & "..prpayrollprocess_tran.isvoid, 0) = 0 " & _
                       "			AND ISNULL(" & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran.isvoid, 0) = 0 AND " & dsList.Tables("List").Rows(0).Item("database_name") & "..prtnaleave_tran.payperiodunkid = '" & dsList.Tables("List").Rows(0).Item("periodunkid") & "' "
                'Sohail (24 May 2019) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    SQry &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", dsList.Tables("List").Rows(0).Item("database_name") & "..hremployee_master")
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END




                SQry &= "	) AS Gross " & _
                       "	GROUP BY Gross.Empid " & _
                       ") AS EGross ON EGross.Empid = hremployee_master.employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            xUACQry = "" : xUACFiltrQry = ""
            If mstrPeriodEndDate.Trim.Length > 0 Then
                dtPeriodEnd = eZeeDate.convertDate(mstrPeriodEndDate)
            End If
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ = "SELECT " & _
                   "    ISNULL(hremployee_meminfo_tran.membershipno, '') AS MEM_NO " & _
                   "   ,ISNULL(hrmembership_master.employer_membershipno, '') AS EMPLOYER_MEM_NO " & _
                   "   ,ISNULL(Id_Num,'') AS ID_NUM " & _
                   "   ,ISNULL(hremployee_master.surname, '') SURNAME " & _
                   "   ,ISNULL(hremployee_master.firstname, '') AS FIRSTNAME " & _
                   "   ,ISNULL(hremployee_master.othername, '') AS OTHERNAME " & _
                   "   ,ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS DOB "
            'Sohail (14 May 2019) - [EMPLOYER_MEM_NO, OTHERNAME]
            If SQry.Trim.Length > 0 Then
                StrQ &= "	,ISNULL(EGross.EGross,0) AS E_GROSS "
            Else
                StrQ &= "	,0 AS E_GROSS "
            End If
            StrQ &= "	,ISNULL(NGross.GrossPay,0) AS N_GROSS " & _
                    "	,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS EMP_SHARE " & _
                    "	,ISNULL(CAmount, 0) AS EMPR_SHARE " & _
                    "	,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) AS TOTAL " & _
                    "	,ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS DOJ " & _
                    "	,ISNULL(CONVERT(CHAR(8),termination_from_date,112),'') AS DLE " & _
                    "	,hremployee_master.employeeunkid AS EmpId " & _
                    "	,hremployee_master.employeecode AS employeecode " & _
                    "FROM prpayrollprocess_tran " & _
                    "	JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                    "	LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                    "	JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "	LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid	AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 employeeunkid AS IdEmp " & _
                    "			,ISNULL(cfcommon_master.name,'')  AS Id_Name " & _
                    "			,ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_Num " & _
                    "		FROM hremployee_idinfo_tran " & _
                    "		JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                    "		WHERE cfcommon_master.masterunkid = '" & mintIdentityId & "' " & _
                    "	) AS Id ON Id.IdEmp = hremployee_master.employeeunkid " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 hremployee_master.employeeunkid AS EmpId " & _
                    "			,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")), 0) AS CAmount " & _
                    "		FROM prpayrollprocess_tran " & _
                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "			JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                    "			LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                    "			JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 0 " & _
                    "		WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "			AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.membershipunkid = @MemId " & _
                    "			AND hrmembership_master.cotranheadunkid = @CHeadType AND hrmembership_master.emptranheadunkid = @EHeadType "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "		     Gross.Empid " & _
                    "		    ,SUM(Gross.Amount) AS GrossPay " & _
                    "		FROM " & _
                    "		( " & _
                    "			SELECT " & _
                    "				 hremployee_master.employeeunkid AS Empid " & _
                    "				,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                    "			FROM prpayrollprocess_tran " & _
                    "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (24 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. - Claim Request Earning was not included in Total Earning
            'StrQ &= "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '        "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '        "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
            '        "				AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '        "				AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId "
            StrQ &= "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "			WHERE prpayrollprocess_tran.add_deduct = 1 " & _
                    "				AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "				AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId "
            'Sohail (24 May 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END
            StrQ &= "		) AS Gross " & _
                    "		GROUP BY Gross.Empid " & _
                    "	) AS NGross ON NGross.Empid = hremployee_master.employeeunkid "
            StrQ &= SQry
            StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                    " AND hrmembership_master.membershipunkid = @MemId AND hrmembership_master.emptranheadunkid = @EHeadType "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "ORDER BY employeecode "


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@EHeadType", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpHeadTypeId)
            objDataOperation.AddParameter("@CHeadType", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmprHeadTypeId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("MEM_NO")
                rpt_Row.Item("Column2") = dtRow.Item("ID_NUM")
                rpt_Row.Item("Column3") = dtRow.Item("SURNAME")
                rpt_Row.Item("Column4") = dtRow.Item("FIRSTNAME")
                'Sohail (14 May 2019) -- Start
                'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
                rpt_Row.Item("Column15") = dtRow.Item("OTHERNAME")
                rpt_Row.Item("Column16") = dtRow.Item("EMPLOYER_MEM_NO")
                rpt_Row.Item("Column17") = Year(eZeeDate.convertDate(mstrPeriodEndDate.ToString)).ToString
                'rpt_Row.Item("Column18") = mstrPeriodCode
                rpt_Row.Item("Column18") = Month(eZeeDate.convertDate(mstrPeriodEndDate.ToString)).ToString
                'Sohail (14 May 2019) -- End
                If dtRow.Item("DOB").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("DOB").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column5") = ""
                End If
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("E_GROSS")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("N_GROSS")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("EMP_SHARE")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("EMPR_SHARE")), GUI.fmtCurrency)
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("TOTAL")), GUI.fmtCurrency)
                rpt_Row.Item("Column11") = eZeeDate.convertDate(dtRow.Item("DOJ").ToString).ToShortDateString
                If dtRow.Item("DLE").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Column12") = eZeeDate.convertDate(dtRow.Item("DLE").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column12") = ""
                End If

                If (CDate(eZeeDate.convertDate(dtRow.Item("DOJ").ToString)).Month = CDate(eZeeDate.convertDate(mstrPeriodEndDate)).Month) AndAlso _
                   (CDate(eZeeDate.convertDate(dtRow.Item("DOJ").ToString)).Year = CDate(eZeeDate.convertDate(mstrPeriodEndDate)).Year) Then
                    rpt_Row.Item("Column13") = "I"
                ElseIf dtRow.Item("DLE").ToString.Trim.Length > 0 Then
                    If (CDate(eZeeDate.convertDate(dtRow.Item("DLE").ToString)).Month = CDate(eZeeDate.convertDate(mstrPeriodEndDate)).Month) AndAlso _
                       (CDate(eZeeDate.convertDate(dtRow.Item("DLE").ToString)).Year = CDate(eZeeDate.convertDate(mstrPeriodEndDate)).Year) Then
                        rpt_Row.Item("Column13") = "D"
                    End If
                ElseIf dtRow.Item("E_GROSS") <> dtRow.Item("N_GROSS") Then
                    rpt_Row.Item("Column13") = "R"
                Else
                    rpt_Row.Item("Column13") = ""
                End If

                rpt_Row.Item("Column81") = dtRow.Item("E_GROSS")
                rpt_Row.Item("Column82") = dtRow.Item("N_GROSS")
                rpt_Row.Item("Column83") = dtRow.Item("EMP_SHARE")
                rpt_Row.Item("Column84") = dtRow.Item("EMPR_SHARE")
                rpt_Row.Item("Column85") = dtRow.Item("TOTAL")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            If mblnIsExportToCSV = True Then
                Dim mdtTableExcel As DataTable = rpt_Data.Tables(0)
                Dim intColIdx As Integer = -1

                intColIdx += 1
                mdtTableExcel.Columns("Column16").Caption = Language.getMessage(mstrModuleName, 63, "Employer #")
                mdtTableExcel.Columns("Column16").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column17").Caption = Language.getMessage(mstrModuleName, 64, "Year")
                mdtTableExcel.Columns("Column17").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column18").Caption = Language.getMessage(mstrModuleName, 65, "Month")
                mdtTableExcel.Columns("Column18").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 66, "MemNo.")
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 67, "Id No. (NRC)")
                mdtTableExcel.Columns("Column2").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 68, "Surname")
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 69, "Firstname")
                mdtTableExcel.Columns("Column4").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column15").Caption = Language.getMessage(mstrModuleName, 70, "Middlename")
                mdtTableExcel.Columns("Column15").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 71, "DOB")
                mdtTableExcel.Columns("Column5").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 72, "TotalEarnings")
                mdtTableExcel.Columns("Column7").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column8").Caption = Language.getMessage(mstrModuleName, 73, "EmployeeCont.")
                mdtTableExcel.Columns("Column8").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 74, "EmployerCont.")
                mdtTableExcel.Columns("Column9").SetOrdinal(intColIdx)

                For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
                Next

                Dim strSB As System.Text.StringBuilder = DataTableToCSV(mdtTableExcel, CChar(","), False)

                If SaveTextFile(strExportPath, strSB) = False Then
                    Return Nothing
                    Exit Function
                Else
                    Return Nothing
                    Exit Function
                End If

            End If
            'Sohail (14 May 2019) -- End

            objRpt = New ArutiReport.Designer.rptZam_MonthlyReturn_AMD

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtTitle1", Language.getMessage(mstrModuleName, 1, "NATIONAL PENSION SCHEME AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtFrmNo", Language.getMessage(mstrModuleName, 2, "FORM NO."))
            ReportFunction.TextChange(objRpt, "txtNPS90", Language.getMessage(mstrModuleName, 3, "NPS900-"))
            ReportFunction.TextChange(objRpt, "txtTitle2", Language.getMessage(mstrModuleName, 4, "MONTHLY RETURN / AMENDMENT FORM"))
            ReportFunction.TextChange(objRpt, "txtPartA", Language.getMessage(mstrModuleName, 5, "PART A. EMPLOYER DETAILS"))

            ReportFunction.TextChange(objRpt, "lblEmplrAccNo", Language.getMessage(mstrModuleName, 6, "EMPLOYER ACCOUNT NO."))
            ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 7, "EMPLOYER NAME"))
            ReportFunction.TextChange(objRpt, "lblContribution", Language.getMessage(mstrModuleName, 8, "CONTRIBUTION PERIOD"))
            ReportFunction.TextChange(objRpt, "lblReturnFormat", Language.getMessage(mstrModuleName, 9, "RETURN FORMAT"))
            ReportFunction.TextChange(objRpt, "lblHC", Language.getMessage(mstrModuleName, 10, "HC - HARD COPY FORMAT"))
            ReportFunction.TextChange(objRpt, "lblDK", Language.getMessage(mstrModuleName, 11, "DK - DISKETTE FORMAT"))
            ReportFunction.TextChange(objRpt, "lblTickApplicable", Language.getMessage(mstrModuleName, 12, "(Tick where applicable)"))
            ReportFunction.TextChange(objRpt, "lblAMD", Language.getMessage(mstrModuleName, 13, "AMENDMENT TOTAL (K)"))
            ReportFunction.TextChange(objRpt, "lblAMD_Value", Language.getMessage(mstrModuleName, 14, "(Value of Amendments on form)"))
            ReportFunction.TextChange(objRpt, "lblFinYear", Language.getMessage(mstrModuleName, 15, "FINANCIAL YEAR"))
            ReportFunction.TextChange(objRpt, "lblTotSheet", Language.getMessage(mstrModuleName, 16, "NO. OF SHEETS"))
            ReportFunction.TextChange(objRpt, "lblTotalContribution", Language.getMessage(mstrModuleName, 17, "TOTAL CONTRIBUTION (K)"))
            ReportFunction.TextChange(objRpt, "lblValueWhole", Language.getMessage(mstrModuleName, 18, "(Value of whole return)"))
            ReportFunction.TextChange(objRpt, "lblTotEmp", Language.getMessage(mstrModuleName, 19, "NO. OF EMPLOYEES"))
            ReportFunction.TextChange(objRpt, "lblOfficeUse", Language.getMessage(mstrModuleName, 20, "FOR OFFICIAL USE ONLY"))

            ReportFunction.TextChange(objRpt, "lblProcessCenter", Language.getMessage(mstrModuleName, 21, "PROCESSING CENTRE"))
            ReportFunction.TextChange(objRpt, "lblDateReceived", Language.getMessage(mstrModuleName, 22, "DATE RECEIVED"))
            ReportFunction.TextChange(objRpt, "lblRemark", Language.getMessage(mstrModuleName, 23, "REMARKS"))
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 24, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "lblPartB", Language.getMessage(mstrModuleName, 25, "PART B. EMPLOYEE DETAILS"))
            ReportFunction.TextChange(objRpt, "lblNote1", Language.getMessage(mstrModuleName, 26, "(To be completed monthly where an employee is joining, leaving, and / or when there are changes in gross wages)"))
            ReportFunction.TextChange(objRpt, "lblNote2", Language.getMessage(mstrModuleName, 27, "NOTE"))

            ReportFunction.TextChange(objRpt, "txtFrmNo", Language.getMessage(mstrModuleName, 2, "FORM NO."))
            ReportFunction.TextChange(objRpt, "txtNPS90", Language.getMessage(mstrModuleName, 3, "NPS900-"))

            ReportFunction.TextChange(objRpt, "lblPoint1", Language.getMessage(mstrModuleName, 28, "1. For new employees, write 'I' in column 13."))
            ReportFunction.TextChange(objRpt, "lblPoint2", Language.getMessage(mstrModuleName, 29, "2. For cessation of employement, write 'D' in column 13."))
            ReportFunction.TextChange(objRpt, "lblPoint3", Language.getMessage(mstrModuleName, 30, "3. For wage changes, write 'R' in column 13."))

            ReportFunction.TextChange(objRpt, "lblCol1", Language.getMessage(mstrModuleName, 31, "Col. 1"))
            ReportFunction.TextChange(objRpt, "lblCol2", Language.getMessage(mstrModuleName, 32, "Col. 2"))
            ReportFunction.TextChange(objRpt, "lblCol3", Language.getMessage(mstrModuleName, 33, "Col. 3"))
            ReportFunction.TextChange(objRpt, "lblCol4", Language.getMessage(mstrModuleName, 34, "Col. 4"))
            ReportFunction.TextChange(objRpt, "lblCol5", Language.getMessage(mstrModuleName, 35, "Col. 5"))
            ReportFunction.TextChange(objRpt, "lblCol6", Language.getMessage(mstrModuleName, 36, "Col. 6"))
            ReportFunction.TextChange(objRpt, "lblCol7", Language.getMessage(mstrModuleName, 37, "Col. 7"))
            ReportFunction.TextChange(objRpt, "lblCol8", Language.getMessage(mstrModuleName, 38, "Col. 8"))
            ReportFunction.TextChange(objRpt, "lblCol9", Language.getMessage(mstrModuleName, 39, "Col. 9"))
            ReportFunction.TextChange(objRpt, "lblCol10", Language.getMessage(mstrModuleName, 40, "Col. 10"))
            ReportFunction.TextChange(objRpt, "lblCol11", Language.getMessage(mstrModuleName, 41, "Col. 11"))
            ReportFunction.TextChange(objRpt, "lblCol12", Language.getMessage(mstrModuleName, 42, "Col. 12"))
            ReportFunction.TextChange(objRpt, "lblCol13", Language.getMessage(mstrModuleName, 43, "Col. 13"))

            ReportFunction.TextChange(objRpt, "lblSSN", Language.getMessage(mstrModuleName, 44, "SOCIAL SECURITY NUMBER"))
            ReportFunction.TextChange(objRpt, "lblNRCNo", Language.getMessage(mstrModuleName, 45, "NRC NUMBER"))
            ReportFunction.TextChange(objRpt, "lblSurname", Language.getMessage(mstrModuleName, 46, "SURNAME"))
            ReportFunction.TextChange(objRpt, "lblFirstName", Language.getMessage(mstrModuleName, 47, "FIRST NAME (In Full)"))
            ReportFunction.TextChange(objRpt, "lblDOB", Language.getMessage(mstrModuleName, 48, "DATE OF BIRTH"))
            ReportFunction.TextChange(objRpt, "lblOldGross", Language.getMessage(mstrModuleName, 49, "GROSS WAGES (Existing)"))
            ReportFunction.TextChange(objRpt, "lblNewGross", Language.getMessage(mstrModuleName, 50, "GROSS WAGES (New)"))
            ReportFunction.TextChange(objRpt, "lblEmplShare", Language.getMessage(mstrModuleName, 51, "EMPLOYEE'S SHARE"))
            ReportFunction.TextChange(objRpt, "lblEmplrShare", Language.getMessage(mstrModuleName, 52, "EMPLOYER'S SHARE"))
            ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 53, "TOTAL (Col8+Col9)"))
            ReportFunction.TextChange(objRpt, "lblDateJoined", Language.getMessage(mstrModuleName, 54, "DATE JOINED"))
            ReportFunction.TextChange(objRpt, "lblLeavDate", Language.getMessage(mstrModuleName, 55, "DATE LEFT EMPLOYMENT"))
            ReportFunction.TextChange(objRpt, "lblAction", Language.getMessage(mstrModuleName, 56, "ACT. IND."))

            ReportFunction.TextChange(objRpt, "lblGTot", Language.getMessage(mstrModuleName, 57, "TOTALS"))

            ReportFunction.TextChange(objRpt, "lblDeclaration", Language.getMessage(mstrModuleName, 58, "DECLARATION"))
            ReportFunction.TextChange(objRpt, "lblDeclareText", Language.getMessage(mstrModuleName, 59, "I certify that the amount of wages paid and the amounts of contributions payable by me under the National Pension Scheme Act as set out on the return are correct"))
            ReportFunction.TextChange(objRpt, "lblFinalSign", Language.getMessage(mstrModuleName, 60, "Signature"))
            ReportFunction.TextChange(objRpt, "lblCapacity", Language.getMessage(mstrModuleName, 61, "Capacity"))
            ReportFunction.TextChange(objRpt, "lblOfficialStamp", Language.getMessage(mstrModuleName, 62, "OFFICIAL STAMP"))

            ReportFunction.TextChange(objRpt, "txtEmplrAccNo", Company._Object._Company_Reg_No)
            ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)
            For iVal As Integer = 0 To mstrPeriodEndDate - 2
                Select Case iVal
                    Case 0
                        ReportFunction.TextChange(objRpt, "txtDay1", mstrPeriodEndDate.Chars(iVal))
                    Case 1
                        ReportFunction.TextChange(objRpt, "txtDay2", mstrPeriodEndDate.Chars(iVal))
                    Case 2
                        ReportFunction.TextChange(objRpt, "txtYr1", mstrPeriodEndDate.Chars(iVal))
                    Case 3
                        ReportFunction.TextChange(objRpt, "txtYr2", mstrPeriodEndDate.Chars(iVal))
                    Case 4
                        ReportFunction.TextChange(objRpt, "txtMon1", mstrPeriodEndDate.Chars(iVal))
                    Case 5
                        ReportFunction.TextChange(objRpt, "txtMon2", mstrPeriodEndDate.Chars(iVal))
                End Select
            Next
            ReportFunction.TextChange(objRpt, "txtFinYear", FinancialYear._Object._FinancialYear_Name)
            ReportFunction.TextChange(objRpt, "txtTotEmp", rpt_Data.Tables(0).Rows.Count)

            If rpt_Data.Tables(0).Rows.Count > 0 Then
                ReportFunction.TextChange(objRpt, "txtTotCol6", Format(CDec(rpt_Data.Tables(0).Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt, "txtTotCol7", Format(CDec(rpt_Data.Tables(0).Compute("SUM(Column82)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt, "txtTotCol8", Format(CDec(rpt_Data.Tables(0).Compute("SUM(Column83)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt, "txtTotCol9", Format(CDec(rpt_Data.Tables(0).Compute("SUM(Column84)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt, "txtTotCol10", Format(CDec(rpt_Data.Tables(0).Compute("SUM(Column85)", "")), GUI.fmtCurrency))
            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb.ToString.Trim)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
    'Sohail (14 May 2019) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "NATIONAL PENSION SCHEME AUTHORITY")
            Language.setMessage(mstrModuleName, 2, "FORM NO.")
            Language.setMessage(mstrModuleName, 3, "NPS900-")
            Language.setMessage(mstrModuleName, 4, "MONTHLY RETURN / AMENDMENT FORM")
            Language.setMessage(mstrModuleName, 5, "PART A. EMPLOYER DETAILS")
            Language.setMessage(mstrModuleName, 6, "EMPLOYER ACCOUNT NO.")
            Language.setMessage(mstrModuleName, 7, "EMPLOYER NAME")
            Language.setMessage(mstrModuleName, 8, "CONTRIBUTION PERIOD")
            Language.setMessage(mstrModuleName, 9, "RETURN FORMAT")
            Language.setMessage(mstrModuleName, 10, "HC - HARD COPY FORMAT")
            Language.setMessage(mstrModuleName, 11, "DK - DISKETTE FORMAT")
            Language.setMessage(mstrModuleName, 12, "(Tick where applicable)")
            Language.setMessage(mstrModuleName, 13, "AMENDMENT TOTAL (K)")
            Language.setMessage(mstrModuleName, 14, "(Value of Amendments on form)")
            Language.setMessage(mstrModuleName, 15, "FINANCIAL YEAR")
            Language.setMessage(mstrModuleName, 16, "NO. OF SHEETS")
            Language.setMessage(mstrModuleName, 17, "TOTAL CONTRIBUTION (K)")
            Language.setMessage(mstrModuleName, 18, "(Value of whole return)")
            Language.setMessage(mstrModuleName, 19, "NO. OF EMPLOYEES")
            Language.setMessage(mstrModuleName, 20, "FOR OFFICIAL USE ONLY")
            Language.setMessage(mstrModuleName, 21, "PROCESSING CENTRE")
            Language.setMessage(mstrModuleName, 22, "DATE RECEIVED")
            Language.setMessage(mstrModuleName, 23, "REMARKS")
            Language.setMessage(mstrModuleName, 24, "SIGNATURE")
            Language.setMessage(mstrModuleName, 25, "PART B. EMPLOYEE DETAILS")
            Language.setMessage(mstrModuleName, 26, "(To be completed monthly where an employee is joining, leaving, and / or when there are changes in gross wages)")
            Language.setMessage(mstrModuleName, 27, "NOTE")
            Language.setMessage(mstrModuleName, 28, "1. For new employees, write 'I' in column 13.")
            Language.setMessage(mstrModuleName, 29, "2. For cessation of employement, write 'D' in column 13.")
            Language.setMessage(mstrModuleName, 30, "3. For wage changes, write 'R' in column 13.")
            Language.setMessage(mstrModuleName, 31, "Col. 1")
            Language.setMessage(mstrModuleName, 32, "Col. 2")
            Language.setMessage(mstrModuleName, 33, "Col. 3")
            Language.setMessage(mstrModuleName, 34, "Col. 4")
            Language.setMessage(mstrModuleName, 35, "Col. 5")
            Language.setMessage(mstrModuleName, 36, "Col. 6")
            Language.setMessage(mstrModuleName, 37, "Col. 7")
            Language.setMessage(mstrModuleName, 38, "Col. 8")
            Language.setMessage(mstrModuleName, 39, "Col. 9")
            Language.setMessage(mstrModuleName, 40, "Col. 10")
            Language.setMessage(mstrModuleName, 41, "Col. 11")
            Language.setMessage(mstrModuleName, 42, "Col. 12")
            Language.setMessage(mstrModuleName, 43, "Col. 13")
            Language.setMessage(mstrModuleName, 44, "SOCIAL SECURITY NUMBER")
            Language.setMessage(mstrModuleName, 45, "NRC NUMBER")
            Language.setMessage(mstrModuleName, 46, "SURNAME")
            Language.setMessage(mstrModuleName, 47, "FIRST NAME (In Full)")
            Language.setMessage(mstrModuleName, 48, "DATE OF BIRTH")
            Language.setMessage(mstrModuleName, 49, "GROSS WAGES (Existing)")
            Language.setMessage(mstrModuleName, 50, "GROSS WAGES (New)")
            Language.setMessage(mstrModuleName, 51, "EMPLOYEE'S SHARE")
            Language.setMessage(mstrModuleName, 52, "EMPLOYER'S SHARE")
            Language.setMessage(mstrModuleName, 53, "TOTAL (Col8+Col9)")
            Language.setMessage(mstrModuleName, 54, "DATE JOINED")
            Language.setMessage(mstrModuleName, 55, "DATE LEFT EMPLOYMENT")
            Language.setMessage(mstrModuleName, 56, "ACT. IND.")
            Language.setMessage(mstrModuleName, 57, "TOTALS")
            Language.setMessage(mstrModuleName, 58, "DECLARATION")
            Language.setMessage(mstrModuleName, 59, "I certify that the amount of wages paid and the amounts of contributions payable by me under the National Pension Scheme Act as set out on the return are correct")
            Language.setMessage(mstrModuleName, 60, "Signature")
            Language.setMessage(mstrModuleName, 61, "Capacity")
            Language.setMessage(mstrModuleName, 62, "OFFICIAL STAMP")
            Language.setMessage(mstrModuleName, 63, "Employer #")
            Language.setMessage(mstrModuleName, 64, "Year")
            Language.setMessage(mstrModuleName, 65, "Month")
            Language.setMessage(mstrModuleName, 66, "MemNo.")
            Language.setMessage(mstrModuleName, 67, "Id No. (NRC)")
            Language.setMessage(mstrModuleName, 68, "Surname")
            Language.setMessage(mstrModuleName, 69, "Firstname")
            Language.setMessage(mstrModuleName, 70, "Middlename")
            Language.setMessage(mstrModuleName, 71, "DOB")
            Language.setMessage(mstrModuleName, 72, "TotalEarnings")
            Language.setMessage(mstrModuleName, 73, "EmployeeCont.")
            Language.setMessage(mstrModuleName, 74, "EmployerCont.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
