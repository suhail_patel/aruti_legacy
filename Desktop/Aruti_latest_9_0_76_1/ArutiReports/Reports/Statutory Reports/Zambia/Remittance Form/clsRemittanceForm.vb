'************************************************************************************************************************************
'Class Name : clsRemittanceForm.vb
'Purpose    :
'Date        :15/07/2013
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsRemittanceForm
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsRemittanceForm"
    Private mstrReportId As String = enArutiReport.Za_RemittanceForm
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 0
    Private mstrPeriodIds As String = String.Empty
    Private mstrEndPeriodName As String = String.Empty
    Private mdtEndPeriodDate As DateTime
    Private mintPeriodCount As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintEmpHeadTypeId As Integer = -1
    Private mintEmprHeadTypeId As Integer = -1

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewBy() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _EmpHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmpHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmprHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmprHeadTypeId = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mintPeriodCount = 0
            mdtEndPeriodDate = Nothing
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsPeriodicData As New DataSet
        Dim exForce As Exception = Nothing
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ = "SELECT " & _
                   "	 ISNULL(financialyear_name,'') AS [Year] " & _
                   "	,ISNULL(period_name,'') AS [Month] " & _
                   "	,ISNULL(EAmount,0)+ISNULL(CAmount,0) AS [TSHARE] " & _
                   "FROM cfcommon_period_tran " & _
                   "JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "		 prtnaleave_tran.payperiodunkid AS EPID " & _
                   "		,SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")),0)) AS EAmount " & _
                   "	FROM prpayrollprocess_tran " & _
                   "		JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                   "		LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                   "		JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                   "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                   "		AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") AND hrmembership_master.membershipunkid = '" & mintMembershipId & "' " & _
                   "		AND hrmembership_master.emptranheadunkid = '" & mintEmpHeadTypeId & "' " & _
                   "	GROUP BY prtnaleave_tran.payperiodunkid " & _
                   ") AS E_SHARE ON E_SHARE.EPID = cfcommon_period_tran.periodunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "		 prtnaleave_tran.payperiodunkid AS CPID " & _
                   "		,SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")),0)) AS CAmount " & _
                   "	FROM prpayrollprocess_tran " & _
                   "		JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END
            StrQ &= "		JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                   "		LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                   "		JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                   "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                   "		AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") AND hrmembership_master.membershipunkid = '" & mintMembershipId & "' " & _
                   "		AND hrmembership_master.cotranheadunkid = '" & mintEmprHeadTypeId & "' AND hrmembership_master.emptranheadunkid = '" & mintEmpHeadTypeId & "' "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	GROUP BY prtnaleave_tran.payperiodunkid " & _
                   ") AS C_SHARE ON C_SHARE.CPID = cfcommon_period_tran.periodunkid " & _
                   "WHERE periodunkid IN (" & mstrPeriodIds & ") AND cfcommon_period_tran.isactive = 1 "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("Year")
                rpt_Row.Item("Column2") = dtRow.Item("Month")
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("TSHARE")), GUI.fmtCurrency)

                rpt_Row.Item("Column81") = dtRow.Item("TSHARE")


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptRemittanceForm

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptSubCollectionPeriods").SetDataSource(rpt_Data)
            ReportFunction.TextChange(objRpt, "txtFormNo", Language.getMessage(mstrModuleName, 1, "FORM NO. NPS31"))
            ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 2, "NATIONAL PENSION SCHEME AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtRemittanceForm", Language.getMessage(mstrModuleName, 3, "REMITTANCE FORM"))
            ReportFunction.TextChange(objRpt, "txtEmpDetails", Language.getMessage(mstrModuleName, 4, "PART A. EMPLOYER DETAILS"))
            ReportFunction.TextChange(objRpt, "txtEmployerACNo", Language.getMessage(mstrModuleName, 5, "EMPLOYER ACCOUNT NUMBER"))
            ReportFunction.TextChange(objRpt, "txtEmployerNameAdd", Language.getMessage(mstrModuleName, 6, "EMPLOYER NAME AND ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtPOBox", Language.getMessage(mstrModuleName, 7, "P.O.BOX"))
            ReportFunction.TextChange(objRpt, "txtPlotNumber", Language.getMessage(mstrModuleName, 8, "PLOT NUMBER"))
            ReportFunction.TextChange(objRpt, "txtArea", Language.getMessage(mstrModuleName, 9, "AREA"))
            ReportFunction.TextChange(objRpt, "txtStreetNo", Language.getMessage(mstrModuleName, 10, "STREET NUMBER"))
            ReportFunction.TextChange(objRpt, "txtTelePhoneNo", Language.getMessage(mstrModuleName, 11, "TELEPHONE / CELLPHONE NO."))
            ReportFunction.TextChange(objRpt, "txtFAXNo", Language.getMessage(mstrModuleName, 12, "FAX NUMBER"))
            ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 13, "EMAIL"))
            ReportFunction.TextChange(objRpt, "txtMethodofPayment", Language.getMessage(mstrModuleName, 14, "METHOD OF PAYMENT"))
            ReportFunction.TextChange(objRpt, "txtCash", Language.getMessage(mstrModuleName, 15, "CASH"))
            ReportFunction.TextChange(objRpt, "txtCheque", Language.getMessage(mstrModuleName, 16, "CHEQUE"))
            ReportFunction.TextChange(objRpt, "txtMoneyOrder", Language.getMessage(mstrModuleName, 17, "MONEY ORDER"))
            ReportFunction.TextChange(objRpt, "txtPostalOrder", Language.getMessage(mstrModuleName, 18, "POSTAL ORDER"))
            ReportFunction.TextChange(objRpt, "txtChequeOrder", Language.getMessage(mstrModuleName, 19, "CHEQUE/MONEY ORDER/POSTAL ORDER NUMBER"))
            ReportFunction.TextChange(objRpt, "txtDelete", Language.getMessage(mstrModuleName, 20, "(Delete what is not applicable)"))
            ReportFunction.TextChange(objRpt, "txtBankPost", Language.getMessage(mstrModuleName, 21, "BANK/POST OFFICE NAME / BRANCH/TOWN"))
            ReportFunction.TextChange(objRpt, "txtPaymentterms", Language.getMessage(mstrModuleName, 22, "(Please indicate the makeup of your payment in terms of rebased and unrebased kwacha in the table below)"))
            ReportFunction.TextChange(objRpt, "txtReBasedCurrency", Language.getMessage(mstrModuleName, 23, "REBASED CURRENCY (ZMW)"))
            ReportFunction.TextChange(objRpt, "txtUnrebasedCurrency", Language.getMessage(mstrModuleName, 24, "UNREBASED CURRENCY (ZMK)"))
            ReportFunction.TextChange(objRpt, "txtTotalRebasedCurrency", Language.getMessage(mstrModuleName, 25, "TOTAL AMOUNT IN REBASED CURRENCY (ZMW)"))
            ReportFunction.TextChange(objRpt, "txtPartD", Language.getMessage(mstrModuleName, 26, "PART D. DECLARATION"))
            ReportFunction.TextChange(objRpt, "txtPartDPreviousRemittance", Language.getMessage(mstrModuleName, 27, "I certify that the amount paid is correct and that there have been NO changes in gross wages and number of employees in relation to prevoius remittance"))
            ReportFunction.TextChange(objRpt, "txtPartDamendmentReturn", Language.getMessage(mstrModuleName, 28, "I certify that the amount paid is correct in acordance to changes in gross wages / number of employees as shown by accompaning amendment return"))
            ReportFunction.TextChange(objRpt, "txtTickApplicable", Language.getMessage(mstrModuleName, 29, "(please tick as applicable)"))
            ReportFunction.TextChange(objRpt, "txttickapplicable1", Language.getMessage(mstrModuleName, 30, "(Tick which is applicable)"))
            ReportFunction.TextChange(objRpt, "txtCapacity", Language.getMessage(mstrModuleName, 31, "CAPACITY"))
            ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 32, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 33, "DATE"))
            ReportFunction.TextChange(objRpt, "txtPartE", Language.getMessage(mstrModuleName, 34, "PART E. FOR OFFICIAL USE ONLY"))
            ReportFunction.TextChange(objRpt, "txtPartESignature", Language.getMessage(mstrModuleName, 32, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "txtPartEDate", Language.getMessage(mstrModuleName, 33, "DATE"))
            ReportFunction.TextChange(objRpt, "txtProcessedBy", Language.getMessage(mstrModuleName, 35, "PROCESSED BY"))
            ReportFunction.TextChange(objRpt, "txtPartESignature1", Language.getMessage(mstrModuleName, 32, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "txtPartEDate1", Language.getMessage(mstrModuleName, 33, "DATE"))
            ReportFunction.TextChange(objRpt, "txtProcessingCenter", Language.getMessage(mstrModuleName, 36, "PROCESSING CENTRE"))
            ReportFunction.TextChange(objRpt, "txtManualReceiptNo", Language.getMessage(mstrModuleName, 37, "MANUAL RECEIPT NO. (if any)"))
            ReportFunction.TextChange(objRpt, "txtReceiptNo", Language.getMessage(mstrModuleName, 38, "RECEIPT NUMBER"))
            ReportFunction.TextChange(objRpt, "txtDatePrinted", Language.getMessage(mstrModuleName, 39, "DATE PRINTED"))
            ReportFunction.TextChange(objRpt, "txtReturnVerificationCheckList", Language.getMessage(mstrModuleName, 40, "RETURN VERIFICATION CHECKLIST"))
            ReportFunction.TextChange(objRpt, "txttickApplicable2", Language.getMessage(mstrModuleName, 29, "(please tick as applicable)"))
            ReportFunction.TextChange(objRpt, "txtValidAcNo", Language.getMessage(mstrModuleName, 41, "01. IS ACCOUNT NUMBER VALID"))
            ReportFunction.TextChange(objRpt, "txtNoofEmployees", Language.getMessage(mstrModuleName, 42, "02. NUMBER OF EMPLOYEES:"))
            ReportFunction.TextChange(objRpt, "txtLessthan10", Language.getMessage(mstrModuleName, 43, "LESS THAN 10"))
            ReportFunction.TextChange(objRpt, "txtMoreThan10", Language.getMessage(mstrModuleName, 44, "MORE THAN 10"))
            ReportFunction.TextChange(objRpt, "txtReturnType", Language.getMessage(mstrModuleName, 45, "03. RETURN TYPE"))
            ReportFunction.TextChange(objRpt, "txtSoftCopy", Language.getMessage(mstrModuleName, 46, "SOFT COPY"))
            ReportFunction.TextChange(objRpt, "txtHardCopy", Language.getMessage(mstrModuleName, 47, "HARD COPY"))
            ReportFunction.TextChange(objRpt, "txtReturnFormat", Language.getMessage(mstrModuleName, 48, "04. IS RETURN IN CORRECT FORMAT AND GRIDLINES INSERTED"))
            ReportFunction.TextChange(objRpt, "txtFullEmpNames", Language.getMessage(mstrModuleName, 49, "05. FULL EMPLOYEE NAMES"))
            ReportFunction.TextChange(objRpt, "txtAllNRCNo", Language.getMessage(mstrModuleName, 50, "06. ALL NRC NUMBERS FOR EMPLOYEES INSERTED"))
            ReportFunction.TextChange(objRpt, "txtReturnChequeAmt", Language.getMessage(mstrModuleName, 51, "07. RETURN TOTAL AGREES WITH CHEQUE AMOUNT"))
            ReportFunction.TextChange(objRpt, "txtPenaltyPayment", Language.getMessage(mstrModuleName, 52, "08. HAS PENALTY PAYMENT (IF ANY) BEEN ALLOCATED TO CORRECT PERIODS"))
            ReportFunction.TextChange(objRpt, "txtChequeValid", Language.getMessage(mstrModuleName, 53, "09. IS CHEQUE VALID (PROPERLY DATED)"))
            ReportFunction.TextChange(objRpt, "txtAmtInWords", Language.getMessage(mstrModuleName, 54, "10. DOES AMOUNT IN WORDS AND FIGURES AGREE ON CHEQUE"))
            ReportFunction.TextChange(objRpt, "txtIsChequeSigned", Language.getMessage(mstrModuleName, 55, "11. IS CHEQUE SIGNED"))
            ReportFunction.TextChange(objRpt, "txtVerifiedBy", Language.getMessage(mstrModuleName, 56, "VERIFIED BY:"))
            ReportFunction.TextChange(objRpt, "txtReturnDateStamp", Language.getMessage(mstrModuleName, 57, "OFFICIAL STAMP"))
            ReportFunction.TextChange(objRpt, "txtOfficialStamp", Language.getMessage(mstrModuleName, 57, "OFFICIAL STAMP"))


            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtPARTB", Language.getMessage(mstrModuleName, 58, "PART B. COLLECTION PERIODS"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtTableRebasedCurrency", Language.getMessage(mstrModuleName, 59, "(Figures in the table below should be in the rebased currency only)"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtPartBYear", Language.getMessage(mstrModuleName, 60, "YEAR"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtPartBMonth", Language.getMessage(mstrModuleName, 61, "MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtAmountYear", Language.getMessage(mstrModuleName, 62, "AMOUNT PAID FOR YEAR/MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtPenaltyPaid", Language.getMessage(mstrModuleName, 63, "PENALTY PAID"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtNetAmtPaid", Language.getMessage(mstrModuleName, 64, "NET AMOUNT PAID"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtPartBTotal", Language.getMessage(mstrModuleName, 65, "TOTAL"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubCollectionPeriods"), "txtPartBTotalAmount", Language.getMessage(mstrModuleName, 66, "(Should be equal to Total Amount in Part A.)"))

            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPARTC", Language.getMessage(mstrModuleName, 67, "PART C. AGREEMENT DETAILS"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtTableAgreement", Language.getMessage(mstrModuleName, 68, "(to be completed where Employer is paying for an existing Agreement with NAPSA)"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPartCYear", Language.getMessage(mstrModuleName, 60, "YEAR"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPartCMonth", Language.getMessage(mstrModuleName, 61, "MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPartCAmountDue", Language.getMessage(mstrModuleName, 69, "AMOUNT DUE"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPartCAmountPaid", Language.getMessage(mstrModuleName, 70, "AMOUNT PAID"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPartCTotal", Language.getMessage(mstrModuleName, 65, "TOTAL"))
            ReportFunction.TextChange(objRpt.Subreports("rptSubAgreementDetails"), "txtPartCTotalAmount", Language.getMessage(mstrModuleName, 66, "(Should be equal to Total Amount in Part A.)"))

            ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(CDec(rpt_Data.Tables(0).Compute("SUM(Column81)", "")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtAccNo_Value", Company._Object._Reg1_Value)
            ReportFunction.TextChange(objRpt, "txtEmplrName_Value", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "txtEmplrAddr_Value", Company._Object._Address1 & " " & Company._Object._Address2)
            ReportFunction.TextChange(objRpt, "txtPBox_Value", "")
            ReportFunction.TextChange(objRpt, "txtPlot_Value", "")
            ReportFunction.TextChange(objRpt, "txtArea_Value", "")
            ReportFunction.TextChange(objRpt, "txtStreetNo_Value", "")
            ReportFunction.TextChange(objRpt, "txtTelehone_value", Company._Object._Phone1)
            ReportFunction.TextChange(objRpt, "txtFax_Value", Company._Object._Fax)
            ReportFunction.TextChange(objRpt, "txtEmail_Value", Company._Object._Email)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "FORM NO. NPS31")
            Language.setMessage(mstrModuleName, 2, "NATIONAL PENSION SCHEME AUTHORITY")
            Language.setMessage(mstrModuleName, 3, "REMITTANCE FORM")
            Language.setMessage(mstrModuleName, 4, "PART A. EMPLOYER DETAILS")
            Language.setMessage(mstrModuleName, 5, "EMPLOYER ACCOUNT NUMBER")
            Language.setMessage(mstrModuleName, 6, "EMPLOYER NAME AND ADDRESS")
            Language.setMessage(mstrModuleName, 7, "P.O.BOX")
            Language.setMessage(mstrModuleName, 8, "PLOT NUMBER")
            Language.setMessage(mstrModuleName, 9, "AREA")
            Language.setMessage(mstrModuleName, 10, "STREET NUMBER")
            Language.setMessage(mstrModuleName, 11, "TELEPHONE / CELLPHONE NO.")
            Language.setMessage(mstrModuleName, 12, "FAX NUMBER")
            Language.setMessage(mstrModuleName, 13, "EMAIL")
            Language.setMessage(mstrModuleName, 14, "METHOD OF PAYMENT")
            Language.setMessage(mstrModuleName, 15, "CASH")
            Language.setMessage(mstrModuleName, 16, "CHEQUE")
            Language.setMessage(mstrModuleName, 17, "MONEY ORDER")
            Language.setMessage(mstrModuleName, 18, "POSTAL ORDER")
            Language.setMessage(mstrModuleName, 19, "CHEQUE/MONEY ORDER/POSTAL ORDER NUMBER")
            Language.setMessage(mstrModuleName, 20, "(Delete what is not applicable)")
            Language.setMessage(mstrModuleName, 21, "BANK/POST OFFICE NAME / BRANCH/TOWN")
            Language.setMessage(mstrModuleName, 22, "(Please indicate the makeup of your payment in terms of rebased and unrebased kwacha in the table below)")
            Language.setMessage(mstrModuleName, 23, "REBASED CURRENCY (ZMW)")
            Language.setMessage(mstrModuleName, 24, "UNREBASED CURRENCY (ZMK)")
            Language.setMessage(mstrModuleName, 25, "TOTAL AMOUNT IN REBASED CURRENCY (ZMW)")
            Language.setMessage(mstrModuleName, 26, "PART D. DECLARATION")
            Language.setMessage(mstrModuleName, 27, "I certify that the amount paid is correct and that there have been NO changes in gross wages and number of employees in relation to prevoius remittance")
            Language.setMessage(mstrModuleName, 28, "I certify that the amount paid is correct in acordance to changes in gross wages / number of employees as shown by accompaning amendment return")
            Language.setMessage(mstrModuleName, 29, "(please tick as applicable)")
            Language.setMessage(mstrModuleName, 30, "(Tick which is applicable)")
            Language.setMessage(mstrModuleName, 31, "CAPACITY")
            Language.setMessage(mstrModuleName, 32, "SIGNATURE")
            Language.setMessage(mstrModuleName, 33, "DATE")
            Language.setMessage(mstrModuleName, 34, "PART E. FOR OFFICIAL USE ONLY")
            Language.setMessage(mstrModuleName, 35, "PROCESSED BY")
            Language.setMessage(mstrModuleName, 36, "PROCESSING CENTRE")
            Language.setMessage(mstrModuleName, 37, "MANUAL RECEIPT NO. (if any)")
            Language.setMessage(mstrModuleName, 38, "RECEIPT NUMBER")
            Language.setMessage(mstrModuleName, 39, "DATE PRINTED")
            Language.setMessage(mstrModuleName, 40, "RETURN VERIFICATION CHECKLIST")
            Language.setMessage(mstrModuleName, 41, "01. IS ACCOUNT NUMBER VALID")
            Language.setMessage(mstrModuleName, 42, "02. NUMBER OF EMPLOYEES:")
            Language.setMessage(mstrModuleName, 43, "LESS THAN 10")
            Language.setMessage(mstrModuleName, 44, "MORE THAN 10")
            Language.setMessage(mstrModuleName, 45, "03. RETURN TYPE")
            Language.setMessage(mstrModuleName, 46, "SOFT COPY")
            Language.setMessage(mstrModuleName, 47, "HARD COPY")
            Language.setMessage(mstrModuleName, 48, "04. IS RETURN IN CORRECT FORMAT AND GRIDLINES INSERTED")
            Language.setMessage(mstrModuleName, 49, "05. FULL EMPLOYEE NAMES")
            Language.setMessage(mstrModuleName, 50, "06. ALL NRC NUMBERS FOR EMPLOYEES INSERTED")
            Language.setMessage(mstrModuleName, 51, "07. RETURN TOTAL AGREES WITH CHEQUE AMOUNT")
            Language.setMessage(mstrModuleName, 52, "08. HAS PENALTY PAYMENT (IF ANY) BEEN ALLOCATED TO CORRECT PERIODS")
            Language.setMessage(mstrModuleName, 53, "09. IS CHEQUE VALID (PROPERLY DATED)")
            Language.setMessage(mstrModuleName, 54, "10. DOES AMOUNT IN WORDS AND FIGURES AGREE ON CHEQUE")
            Language.setMessage(mstrModuleName, 55, "11. IS CHEQUE SIGNED")
            Language.setMessage(mstrModuleName, 56, "VERIFIED BY:")
            Language.setMessage(mstrModuleName, 57, "OFFICIAL STAMP")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
