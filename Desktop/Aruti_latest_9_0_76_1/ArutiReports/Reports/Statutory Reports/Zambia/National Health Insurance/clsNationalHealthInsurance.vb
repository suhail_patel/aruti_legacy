﻿'************************************************************************************************************************************
'Class Name   : clsNationalHealthInsurance.vb
'Purpose      :
'Date         :30/06/2021
'Written By   :Sohail
'Modified     :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Text

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsNationalHealthInsurance
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsNationalHealthInsurance"
    Private mstrReportId As String = enArutiReport.National_Health_Insurance_Report
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrBasicSalaryFormula As String = String.Empty

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _BasicSalaryFormula() As String
        Set(ByVal value As String)
            mstrBasicSalaryFormula = value
        End Set
    End Property


    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = -1
            mstrPeriodName = String.Empty
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mstrBasicSalaryFormula = String.Empty

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                     ByVal intUserUnkid As Integer, _
                                     ByVal intYearUnkid As Integer, _
                                     ByVal intCompanyUnkid As Integer, _
                                     ByVal dtPeriodStart As Date, _
                                     ByVal dtPeriodEnd As Date, _
                                     ByVal strUserModeSetting As String, _
                                     ByVal blnOnlyApproved As Boolean, _
                                     ByVal intBaseCurrencyId As Integer, _
                                     ByVal strFmtCurrency As String, _
                                     ByVal xExportReportPath As String, _
                                     ByVal xOpenReportAfterExport As Boolean _
                                     )

        Dim StrQ As String = ""
        Dim StrEmpQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            StrEmpQ = "SELECT   hremployee_master.employeeunkid " & _
                      ", hremployee_master.employeecode AS employeecode " & _
                      ", ISNULL(hremployee_master.surname, '') SURNAME " & _
                      ", ISNULL(hremployee_master.firstname, '') AS FIRSTNAME " & _
                      ", ISNULL(hremployee_master.othername, '') AS OTHERNAME " & _
                      ", hremployee_master.birthdate "

            StrEmpQ &= "INTO #TableEmp "
            StrEmpQ &= "FROM hremployee_master "

            StrEmpQ &= mstrAnalysis_Join

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrEmpQ &= xDateJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrEmpQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrEmpQ &= xAdvanceJoinQry
            'End If

            StrEmpQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrEmpQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrEmpQ &= " AND " & xUACFiltrQry
            End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrEmpQ &= xDateFilterQry
            '    End If
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrEmpQ &= " AND " & mstrAdvance_Filter
            'End If

            StrQ = StrEmpQ

            StrQ &= "SELECT " & _
                      "  ISNULL(E.MEM_NO, '') AS MEM_NO " & _
                      ", ISNULL(E.EMPLOYER_MEM_NO, '') AS EMPLOYER_MEM_NO " & _
                      ", ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_Num " & _
                      ", ISNULL(#TableEmp.surname, '') SURNAME " & _
                      ", ISNULL(#TableEmp.firstname, '') AS FIRSTNAME " & _
                      ", ISNULL(#TableEmp.othername, '') AS OTHERNAME " & _
                      ", CAST(ISNULL(#TableEmp.birthdate, '') AS NVARCHAR(MAX)) AS DOB " & _
                      ", ISNULL(E.EAmount, 0) AS EMP_SHARE " & _
                      ", ISNULL(CAmount, 0) AS EMPR_SHARE " & _
                      ", #TableEmp.employeeunkid AS EmpId " & _
                      ", #TableEmp.employeecode AS employeecode " & _
                      ", NGross.BasicSalary " & _
                      ", " & Year(dtPeriodStart) & " AS Year " & _
                      ", " & Month(dtPeriodStart) & " AS Month " & _
                      ", '' AS Col1 " & _
                  "FROM #TableEmp "

            StrQ &= "LEFT JOIN hremployee_idinfo_tran ON #TableEmp.employeeunkid = hremployee_idinfo_tran.employeeunkid AND hremployee_idinfo_tran.isdefault = 1 " & _
                   "JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "		 #TableEmp.employeeunkid AS EmpId " & _
                   "        , ISNULL(hremployee_meminfo_tran.membershipno, '') AS MEM_NO " & _
                   "        , ISNULL(hrmembership_master.employer_membershipno, '') AS EMPLOYER_MEM_NO " & _
                   "		,SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")), 0)) AS EAmount " & _
                   "	FROM prpayrollprocess_tran " & _
                   "		JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                   "			JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                   "			LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                   "			JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 0 " & _
                   "		WHERE prpayrollprocess_tran.isvoid = 0 " & _
                   "			AND prtnaleave_tran.isvoid = 0 " & _
                   "			AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.membershipunkid = @MemId " & _
                   "       GROUP BY #TableEmp.employeeunkid, ISNULL(hremployee_meminfo_tran.membershipno, ''), ISNULL(hrmembership_master.employer_membershipno, '') " & _
                   ") AS E ON E.EmpId = #TableEmp.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "		 #TableEmp.employeeunkid AS EmpId " & _
                   "		,SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")), 0)) AS CAmount " & _
                   "	FROM prpayrollprocess_tran " & _
                   "		JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                   "			JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                   "			LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                   "			JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 0 " & _
                   "		WHERE prpayrollprocess_tran.isvoid = 0 " & _
                   "			AND prtnaleave_tran.isvoid = 0 " & _
                   "			AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.membershipunkid = @MemId " & _
                   "       GROUP BY #TableEmp.employeeunkid " & _
                   ") AS C ON C.EmpId = #TableEmp.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "	     Basic.Empid " & _
                   "	    ,SUM(Basic.Amount) AS BasicSalary " & _
                   "	FROM " & _
                   "	( " & _
                   "		SELECT " & _
                   "			 prpayrollprocess_tran.employeeunkid AS Empid " & _
                   "			,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                   "		FROM prpayrollprocess_tran " & _
                   "			JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid "
            StrQ &= "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "           JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "		WHERE prtranhead_master.typeof_id = " & enTypeOf.Salary & " " & _
                    "			AND prpayrollprocess_tran.isvoid = 0 " & _
                    "			AND prtranhead_master.isvoid = 0 " & _
                    "			AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId "
            StrQ &= "		) AS Basic " & _
                    "		GROUP BY Basic.Empid " & _
                    "	) AS NGross ON NGross.Empid = #TableEmp.employeeunkid "

            StrQ &= " WHERE 1 = 1 "

            StrQ &= "ORDER BY #TableEmp.employeecode "

            StrQ &= "DROP TABLE #TableEmp "

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objHead As New clsTransactionHead
            Dim objDHead As New Dictionary(Of Integer, String)
            Dim dsHead As DataSet = objHead.getComboList(strDatabaseName, "List", False, , , , True, , , , True, , , True)

            Dim strPattern As String = "([^#]*#[^#]*)#"
            Dim strReplacement As String = "$1)"

            Dim dsPayroll As DataSet = Nothing
            If mstrBasicSalaryFormula.Trim <> "" Then

                Dim strResult As String = Regex.Replace(mstrBasicSalaryFormula, strPattern, strReplacement) 'Replace 2nd ocurance of # with )
                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)

                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString

                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next

                StrQ = StrEmpQ
                StrQ &= "SELECT  Period.payperiodunkid " & _
                          ", Period.employeeunkid "

                For Each itm In objDHead
                    StrQ &= " , SUM(ISNULL(Period.[" & itm.Key & "], 0)) AS [" & itm.Key & "] "
                Next

                StrQ &= "FROM      ( "

                Dim i As Integer = -1
                For Each itm In objDHead
                    i += 1

                    If i > 0 Then
                        StrQ &= " UNION ALL "
                    End If

                    StrQ &= "SELECT    payperiodunkid " & _
                                    ", prpayrollprocess_tran.employeeunkid "

                    For Each itm1 In objDHead
                        If itm1.Key = itm.Key Then
                            StrQ &= " , CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS [" & itm1.Key & "] "
                        Else
                            StrQ &= " , 0 AS [" & itm1.Key & "] "
                        End If
                    Next

                    StrQ &= "FROM      prpayrollprocess_tran " & _
                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                    "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                              "AND prpayrollprocess_tran.tranheadunkid > 0 "

                    StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                            "AND prtranhead_master.tranheadunkid = " & itm.Key & " "

                Next

                StrQ &= "                    ) AS Period " & _
                        "GROUP BY  Period.payperiodunkid " & _
                                ", Period.employeeunkid "

                StrQ &= "DROP TABLE #TableEmp "

                dsPayroll = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows

                If mstrBasicSalaryFormula.Trim <> "" Then
                    Dim dr() As DataRow = dsPayroll.Tables(0).Select("employeeunkid = " & CInt(dsRow.Item("empid")) & " ")
                    If dr.Length > 0 Then

                        Dim dt As New DataTable
                        Dim s As String = mstrBasicSalaryFormula
                        Dim ans As Object = Nothing

                        For Each itm In objDHead
                            s = s.Replace("#" & itm.Value & "#", Format(CDec(dr(0).Item(itm.Key.ToString)), strFmtCurrency))
                        Next
                        Try
                            ans = dt.Compute(s.ToString.Replace(",", ""), "")
                        Catch ex As Exception

                        End Try
                        If ans Is Nothing Then
                            ans = 0
                        End If

                        dsRow.Item("BasicSalary") = Format(CDec(ans), strFmtCurrency)
                    End If
                End If

                If CDate(dsRow.Item("DOB")) <> CDate("01/Jan/1900") Then
                    dsRow.Item("DOB") = Format(CDate(dsRow.Item("DOB")), "dd-MM-yy")
                Else
                    dsRow.Item("DOB") = ""
                End If
                dsRow.Item("Col1") = Language.getMessage(mstrModuleName, 1, "I")

            Next
            dsList.Tables(0).AcceptChanges()

            Dim intColIndex As Integer = -1
            Dim mdtTableExcel As DataTable = dsList.Tables(0).Copy

            intColIndex += 1
            mdtTableExcel.Columns("EMPLOYER_MEM_NO").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 2, "Employer Number")

            intColIndex += 1
            mdtTableExcel.Columns("Year").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 3, "Year")

            intColIndex += 1
            mdtTableExcel.Columns("Month").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 4, "Month")

            intColIndex += 1
            mdtTableExcel.Columns("MEM_NO").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 5, "Member Number")

            intColIndex += 1
            mdtTableExcel.Columns("Id_Num").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 6, "NRC")

            intColIndex += 1
            mdtTableExcel.Columns("FIRSTNAME").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 7, "First Name")

            intColIndex += 1
            mdtTableExcel.Columns("SURNAME").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 8, "Last Name")

            intColIndex += 1
            mdtTableExcel.Columns("OTHERNAME").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 9, "Other Name")

            intColIndex += 1
            mdtTableExcel.Columns("DOB").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 10, "DOB")

            intColIndex += 1
            mdtTableExcel.Columns("BasicSalary").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 11, "Basic Salary")

            intColIndex += 1
            mdtTableExcel.Columns("EMP_SHARE").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 12, "Member Share")

            intColIndex += 1
            mdtTableExcel.Columns("EMPR_SHARE").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 13, "Employer Share")

            intColIndex += 1
            mdtTableExcel.Columns("Col1").SetOrdinal(intColIndex)
            mdtTableExcel.Columns(intColIndex).Caption = Language.getMessage(mstrModuleName, 14, "")

            For i As Integer = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
            Next

            Using package As New OfficeOpenXml.ExcelPackage()
                Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                Dim xTable As DataTable = mdtTableExcel.Copy


                worksheet.Cells("A1").LoadFromDataTable(xTable, True)
                worksheet.Cells("A1:XFD").AutoFilter = False
                worksheet.Cells(1, 1, 1, xTable.Rows.Count).Style.Font.Bold = True
                worksheet.Cells(2, 10, (xTable.Rows.Count - 1) + 2, 12).Style.Numberformat.Format = GUI.fmtCurrency
                worksheet.Cells.AutoFitColumns(0)


                Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                package.SaveAs(fi)

            End Using


            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try

    End Function
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "I")
			Language.setMessage(mstrModuleName, 2, "Employer Number")
			Language.setMessage(mstrModuleName, 3, "Year")
			Language.setMessage(mstrModuleName, 4, "Month")
			Language.setMessage(mstrModuleName, 5, "Member Number")
			Language.setMessage(mstrModuleName, 6, "NRC")
			Language.setMessage(mstrModuleName, 7, "First Name")
			Language.setMessage(mstrModuleName, 8, "Last Name")
			Language.setMessage(mstrModuleName, 9, "Other Name")
			Language.setMessage(mstrModuleName, 10, "DOB")
			Language.setMessage(mstrModuleName, 11, "Basic Salary")
			Language.setMessage(mstrModuleName, 12, "Member Share")
			Language.setMessage(mstrModuleName, 13, "Employer Share")
			Language.setMessage(mstrModuleName, 14, "")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
