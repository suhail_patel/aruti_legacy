'************************************************************************************************************************************
'Class Name : clsLAPFContribution.vb
'Purpose    :
'Date       :02/04/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class
''' Developer: Sohail
''' </summary>
Public Class clsLAPFContribution
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLAPFContribution"
    Private mstrReportId As String = enArutiReport.LAPFContribution
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintEmpId As Integer = -1
    Private mstrEmpName As String = String.Empty
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintEmpTranHeadId As Integer = -1
    Private mintEmpTypeOfId As Integer = -1
    Private mintEmprTranHeadId As Integer = -1
    Private mintEmprTypeOfId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mstrGrossPayFrom As String = String.Empty
    Private mstrGrossPayTo As String = String.Empty
    Private mstrEmpContribFrom As String = String.Empty
    Private mstrEmpContribTo As String = String.Empty
    Private mstrEmplrContribFrom As String = String.Empty
    Private mstrEmplrContribTo As String = String.Empty
    Private mintEmplrNumTypeId As Integer = -1
    Private mstrEmplrNumTypeName As String = String.Empty
    Private mstrEmplrNumber As String = String.Empty
    Private mstrMembershipNo As String = String.Empty
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 28 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnShowBasicSalary As Boolean = False
    'S.SANDEEP [ 28 FEB 2013 ] -- END
    Private mintOtherEarningTranId As Integer = 0 'Sohail (28 Aug 2013)

    'Sohail (16 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    'Sohail (16 Mar 2013) -- End

    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0

    'Pinkal (02-May-2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmpTranHeadId() As Integer
        Set(ByVal value As Integer)
            mintEmpTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _EmpTypeOfId() As Integer
        Set(ByVal value As Integer)
            mintEmpTypeOfId = value
        End Set
    End Property

    Public WriteOnly Property _EmprTranHeadId() As Integer
        Set(ByVal value As Integer)
            mintEmprTranHeadId = value
        End Set
    End Property

    Public WriteOnly Property _EmprTypeOfId() As Integer
        Set(ByVal value As Integer)
            mintEmprTypeOfId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayFrom() As String
        Set(ByVal value As String)
            mstrGrossPayFrom = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayTo() As String
        Set(ByVal value As String)
            mstrGrossPayTo = value
        End Set
    End Property

    Public WriteOnly Property _EmpContribFrom() As String
        Set(ByVal value As String)
            mstrEmpContribFrom = value
        End Set
    End Property

    Public WriteOnly Property _EmpContribTo() As String
        Set(ByVal value As String)
            mstrEmpContribTo = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContribFrom() As String
        Set(ByVal value As String)
            mstrEmplrContribFrom = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContribTo() As String
        Set(ByVal value As String)
            mstrEmplrContribTo = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmplrNumTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumTypeName() As String
        Set(ByVal value As String)
            mstrEmplrNumTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumber() As String
        Set(ByVal value As String)
            mstrEmplrNumber = value
        End Set
    End Property

    Public WriteOnly Property _MembershipNo() As String
        Set(ByVal value As String)
            mstrMembershipNo = value
        End Set
    End Property


    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property
    'Sohail (28 Aug 2013) -- End

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'S.SANDEEP [ 28 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _ShowBasicSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBasicSalary = value
        End Set
    End Property
    'S.SANDEEP [ 28 FEB 2013 ] -- END

    'Sohail (16 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 Mar 2013) -- End

    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    'Pinkal (02-May-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmpId = 0
            mstrEmpName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintEmpTranHeadId = 0
            mintEmpTypeOfId = 0
            mintEmprTranHeadId = 0
            mintEmprTypeOfId = 0
            mintPeriodId = 0
            mstrPeriodName = ""
            mstrGrossPayFrom = ""
            mstrGrossPayTo = ""
            mstrEmpContribFrom = ""
            mstrEmpContribTo = ""
            mstrEmplrContribFrom = ""
            mstrEmplrContribTo = ""
            mintEmplrNumTypeId = 0
            mstrEmplrNumTypeName = ""
            mstrEmplrNumber = ""
            mstrMembershipNo = ""
            mintReportId = -1
            mstrReportTypeName = ""
            mintOtherEarningTranId = 0 'Sohail (28 Aug 2013)
            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnShowBasicSalary = False
            'S.SANDEEP [ 28 FEB 2013 ] -- END

            'Sohail (16 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Sohail (16 Mar 2013) -- End


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            'Pinkal (02-May-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@EHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpTranHeadId)
            objDataOperation.AddParameter("@CHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmprTranHeadId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If
            'Sohail (28 Aug 2013) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Order By : ") & " " & Me.OrderByDisplay
                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
                'Sohail (16 Mar 2013) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection


    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes  AS PER ANJAN SIR GUIDANCE [REASON : Sr.No is not effecting records in sorting]

            'iColumn_DetailReport.Add(New IColumn("srno", Language.getMessage(mstrModuleName, 1, "Sr. No.")))
            'iColumn_DetailReport.Add(New IColumn("membershipname", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            'iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 3, "Member Number")))

            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("membershipname", Language.getMessage(mstrModuleName, 3, "Member Number")))


            'Pinkal (02-May-2013) -- End

            'Pinkal (02-Oct-2012) -- Start
            'Enhancement : TRA Changes
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 43, "Employee Code")))
            'Pinkal (02-Oct-2012) -- End



            iColumn_DetailReport.Add(New IColumn("Emp_ContributionRate", Language.getMessage(mstrModuleName, 4, "Employee Rate")))
            iColumn_DetailReport.Add(New IColumn("empcontribution", Language.getMessage(mstrModuleName, 5, "Employee Amount")))
            iColumn_DetailReport.Add(New IColumn("Employer_ContributionRate", Language.getMessage(mstrModuleName, 6, "Employer's Rate")))
            iColumn_DetailReport.Add(New IColumn("employercontribution", Language.getMessage(mstrModuleName, 7, "Employer Amount")))
            iColumn_DetailReport.Add(New IColumn("TotalContrib", Language.getMessage(mstrModuleName, 8, "Total Contribution")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES (QUERY WAS TAKING LONG TIME OT EXECUTE SO, OPTIMIZED QUERY HAS BEEN PUT.)
            'StrQ = "SELECT  ROW_NUMBER() OVER ( ORDER BY Emp_Contribution.Empid ) AS srno " & _
            '              ", Emp_Contribution.MemNo AS membershipname " & _
            '              ", Emp_Contribution.empname AS employeename " & _
            '              ", Emp_Contribution.EmpCode AS employeecode " & _
            '              ", Emp_Contribution.Empid AS empid " & _
            '              ", Emp_Contribution.MembershipUnkId AS membershipid " & _
            '              ", ISNULL(pay.BasicSal, 0) AS pay " & _
            '              ", CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 " & _
            '                          "OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 " & _
            '                     "ELSE ( ISNULL(Emp_Contribution.Amount, 0) / ISNULL(pay.BasicSal, " & _
            '                                                                      "0) ) * 100 " & _
            '                "END AS Emp_ContributionRate " & _
            '              ", ISNULL(Emp_Contribution.Amount, 0) AS empcontribution " & _
            '              ", CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0 " & _
            '                          "OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 " & _
            '                     "ELSE ( ISNULL(Employer_Contribution.Amount, 0) " & _
            '                            "/ ISNULL(pay.BasicSal, 0) ) * 100 " & _
            '                "END AS Employer_ContributionRate " & _
            '              ", ISNULL(Employer_Contribution.amount, 0) AS employercontribution " & _
            '              ", ( ISNULL(Emp_Contribution.Amount, 0) " & _
            '                  "+ ISNULL(Employer_Contribution.amount, 0) ) AS TotalContrib " & _
            '        "FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
            '                          ", cfcommon_period_tran.period_name AS PeriodName " & _
            '                          ", hremployee_master.employeeunkid AS Empid " & _
            '                          ", hremployee_master.employeecode AS EmpCode " & _
            '                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS empname " & _
            '                          ", prtranhead_master.trnheadname AS EmpHead " & _
            '                          ", prtranhead_master.tranheadunkid AS EmpHeadId " & _
            '                          ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
            '                          ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
            '                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemNo " & _
            '                  "FROM      prpayrollprocess_tran " & _
            '                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershiptranunkid = prpayrollprocess_tran.membershiptranunkid " & _
            '                                                                 "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                                                                 "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                            "LEFT JOIN hrmembership_master ON prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
            '                  "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                            "AND cfcommon_period_tran.isactive = 1 " & _
            '                            "AND cfcommon_period_tran.periodunkid IN ( " & mintPeriodId & " ) " & _
            '                            "AND prtranhead_master.tranheadunkid = @EHeadId " & _
            '                ") AS Emp_Contribution " & _
            '                "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
            '                                  ", cfcommon_period_tran.period_name AS PeriodName " & _
            '                                  ", prpayrollprocess_tran.employeeunkid AS Empid " & _
            '                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
            '                                  ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
            '                            "FROM    prpayrollprocess_tran " & _
            '                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                                    "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '                            "WHERE   prtranhead_master.trnheadtype_id = 1 " & _
            '                                    "AND prtranhead_master.typeof_id = 1 " & _
            '                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                    "AND cfcommon_period_tran.isactive = 1 " & _
            '                                    "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
            '                            "GROUP BY cfcommon_period_tran.periodunkid " & _
            '                                  ", cfcommon_period_tran.period_name " & _
            '                                  ", prpayrollprocess_tran.employeeunkid " & _
            '                                  ", cfcommon_period_tran.end_date " & _
            '                          ") pay ON pay.Empid = Emp_Contribution.Empid " & _
            '                "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
            '                                  ", prtranhead_master.trnheadname AS EmployerHead " & _
            '                                  ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
            '                                  ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
            '                                  ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
            '                                  ", prpayrollprocess_tran.payrollprocesstranunkid " & _
            '                            "FROM    prpayrollprocess_tran " & _
            '                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                                    "LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershiptranunkid = prpayrollprocess_tran.membershiptranunkid " & _
            '                                                                      "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                                                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                                    "LEFT JOIN hrmembership_master ON prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
            '                            "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
            '                                    "AND prtranhead_master.tranheadunkid = @CHeadId " & _
            '                                    "AND hrmembership_master.emptranheadunkid = @EHeadId " & _
            '                                    "AND hrmembership_master.cotranheadunkid = @CHeadId " & _
            '                          ") AS Employer_Contribution ON Emp_Contribution.Empid = Employer_Contribution.Empid " & _
            '                                                        "AND Emp_Contribution.MembershipUnkId = Employer_Contribution.MembershipId "


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                       "     ROW_NUMBER() OVER ( ORDER BY hremployee_master.employeeunkid) AS srno " & _
                       "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                       "	,hremployee_master.employeecode AS employeecode " & _
                       "	,ISNULL(hremployee_meminfo_tran.membershipno,'') AS membershipname " & _
                       "	,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                       "	,ISNULL(CAmount,0) AS employercontribution " & _
                       "	,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+ISNULL(CAmount,0) AS TotalContrib " & _
                       "	,ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                       "	,hremployee_master.employeeunkid AS EmpId " & _
                       "	,0 AS Emp_ContributionRate " & _
                       "	,0 AS Employer_ContributionRate " & _
                       "	,0 AS Wages "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (16 Mar 2013) -- End

                StrQ &= "FROM      prpayrollprocess_tran " & _
                                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "	JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                       "	LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                       "	JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "	LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                       "	LEFT JOIN " & _
                       "	( " & _
                       "		SELECT " & _
                       "			 hremployee_master.employeeunkid AS EmpId " & _
                       "			,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (16 Mar 2013) -- End

                StrQ &= "		FROM prpayrollprocess_tran " & _
                       "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "			JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                       "			LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                       "			JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (16 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "		WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                       "			AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                       "			AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                       "			AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                       "			AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	    ) AS C ON C.EmpId = hremployee_master.employeeunkid "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (16 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                       "	AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                       "	AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then


                StrQ = " SELECT   ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS employeename " & _
                           " , hremployee_master.employeecode AS employeecode " & _
                           " , ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipname " & _
                           " , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)), 0) AS empcontribution " & _
                           " , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * ISNULL(CAmount, 0) AS employercontribution " & _
                           " , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)), 0) + ISNULL(CAmount, 0)) AS TotalContrib " & _
                           " , ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                           " , hremployee_master.employeeunkid AS EmpId " & _
                           ", 0 AS Emp_ContributionRate " & _
                           ", 0 AS Employer_ContributionRate " & _
                           " , 0 AS Wages "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM  prpayrollprocess_tran " & _
                             "  JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "	JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             "	LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                             "	JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "	LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                             "  JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                             "  AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId


                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " LEFT JOIN (  " & _
                             "              SELECT  hremployee_master.employeeunkid AS EmpId " & _
                             "              , ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36,2)), 0) AS CAmount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "              FROM prpayrollprocess_tran " & _
                             "              JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "              JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             "              AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                             "              LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             "              AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                             "              JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "              LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "              AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "		WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                             "			AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                             "			AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             "			AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                             "			AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "	        ) AS C ON C.EmpId = hremployee_master.employeeunkid "


                StrQ &= "     WHERE(ISNULL(prpayrollprocess_tran.isvoid, 0) = 0) AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "      AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                             "      AND prpayment_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL      " & _
                             " SELECT  ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                             " , hremployee_master.employeecode AS employeecode " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipname " & _
                             "," & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)), 0) AS empcontribution " & _
                             "," & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
                             "," & mdecConversionRate & " * (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)), 0) + ISNULL(CAmount, 0)) AS TotalContrib " & _
                             ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", 0 AS Emp_ContributionRate " & _
                             ", 0 AS Employer_ContributionRate " & _
                             ", 0 AS Wages"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM  prpayrollprocess_tran " & _
                             "  JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "	JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             "	LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                             "	JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "	LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                             "  JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                             "  AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " LEFT JOIN (  " & _
                             "              SELECT  hremployee_master.employeeunkid AS EmpId " & _
                             "              , ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36,2)), 0) AS CAmount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "              FROM prpayrollprocess_tran " & _
                             "              JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "              JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             "              AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                             "              LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             "              AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                             "              JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "              LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "              AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "		WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                             "			AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                             "			AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             "			AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                             "			AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	        ) AS C ON C.EmpId = hremployee_master.employeeunkid "

                StrQ &= "     WHERE(ISNULL(prpayrollprocess_tran.isvoid, 0) = 0) AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "      AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                             "      AND prpayment_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL      " & _
                            " SELECT  ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                            " , hremployee_master.employeecode AS employeecode " & _
                            ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipname " & _
                            "," & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)), 0) AS empcontribution " & _
                            "," & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
                            "," & mdecConversionRate & " * (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 2)), 0) + ISNULL(CAmount, 0)) AS TotalContrib " & _
                            ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                            ", hremployee_master.employeeunkid AS EmpId " & _
                            ", 0 AS Emp_ContributionRate " & _
                            ", 0 AS Employer_ContributionRate " & _
                            ", 0 AS Wages"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM  prpayrollprocess_tran " & _
                             "  JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "	JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             "	LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                             "	JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "	LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                             "  LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                             "  AND prpayment_tran.isvoid = 0 "


                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " LEFT JOIN (  " & _
                             "              SELECT  hremployee_master.employeeunkid AS EmpId " & _
                             "              , ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36,2)), 0) AS CAmount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "              FROM prpayrollprocess_tran " & _
                             "              JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "              JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             "              AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                             "              LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             "              AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                             "              JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "              LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "              AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "		WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                             "			AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                             "			AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             "			AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                             "			AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	        ) AS C ON C.EmpId = hremployee_master.employeeunkid "

                StrQ &= "     WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "      AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                             "      AND prpayment_tran.paymenttranunkid IS NULL "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

            End If


            'Pinkal (02-May-2013) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtBlank() As DataRow = dsList.Tables(0).Select("Emp_ContributionRate = 0 AND empcontribution = 0 AND Employer_ContributionRate = 0 AND employercontribution = 0 AND TotalContrib = 0")

            If dtBlank.Length > 0 Then
                For i As Integer = 0 To dtBlank.Length - 1
                    dsList.Tables(0).Rows.Remove(dtBlank(i))
                Next
                dsList.Tables(0).AcceptChanges()
            End If



            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes


            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                        "	 A.Empid " & _
                        "	,BasicSal " & _
                        "	,GrossPay " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (16 Mar 2013) -- End

                StrQ &= "FROM    prpayrollprocess_tran " & _
                                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (16 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "	WHERE prtranhead_master.typeof_id = 1 " & _
                '                                "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                                "AND cfcommon_period_tran.isactive = 1 " & _
                '    "		AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '    "	GROUP BY prpayrollprocess_tran.employeeunkid "
                StrQ &= "	WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                    "AND cfcommon_period_tran.periodunkid = @PeriodId "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid "
                'Sohail (28 Aug 2013) -- End

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If
                'Sohail (16 Mar 2013) -- End

                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (16 Mar 2013) -- End

                StrQ &= "FROM    prpayrollprocess_tran " & _
                                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (16 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	WHERE prtranhead_master.trnheadtype_id = 1 " & _
                        "		AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "		AND cfcommon_period_tran.isactive = 1 " & _
                        "		AND cfcommon_period_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	    GROUP BY prpayrollprocess_tran.employeeunkid "

                'Sohail (16 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If
                'Sohail (16 Mar 2013) -- End

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                     "	 A.Empid " & _
                     "	,BasicSal " & _
                     "	,GrossPay " & _
                     "FROM " & _
                     "( " & _
                     "	SELECT " & _
                     "	    	 prpayrollprocess_tran.employeeunkid AS Empid " & _
                                               ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *   SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM  prpayrollprocess_tran " & _
                             "          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "          JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "          JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "          JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                             "          AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "	        WHERE prtranhead_master.typeof_id = 1 " & _
                '             "		        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '             "              AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             "              AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             "		        AND cfcommon_period_tran.isactive = 1 " & _
                '             "		        AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '             "              AND prpayment_tran.periodunkid =  @PeriodId " & _
                '             "	            GROUP BY prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "
                StrQ &= "	        WHERE       ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                             "              AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                             "              AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "		        AND cfcommon_period_tran.isactive = 1 " & _
                             "		        AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                "AND prpayment_tran.periodunkid =  @PeriodId "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "
                'Sohail (28 Aug 2013) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "       UNION ALL " & _
                            "              SELECT    prpayrollprocess_tran.employeeunkid AS Empid " & _
                            "               ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36,2))) AS BasicSal"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM  prpayrollprocess_tran " & _
                            "          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "          JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "          JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "          JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                            "          AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "	        WHERE prtranhead_master.typeof_id = 1 " & _
                '             "		     AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '             "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             "		     AND cfcommon_period_tran.isactive = 1 " & _
                '             "		     AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '             "           AND prpayment_tran.periodunkid =  @PeriodId " & _
                '             "	         GROUP BY prpayrollprocess_tran.employeeunkid "
                StrQ &= "	        WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                             "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                             "           AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                         "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "		     AND cfcommon_period_tran.isactive = 1 " & _
                             "		     AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                         "AND prpayment_tran.periodunkid =  @PeriodId "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid "
                'Sohail (28 Aug 2013) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "       UNION ALL " & _
                             "              SELECT    prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "               ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36,2))) AS BasicSal"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM  prpayrollprocess_tran " & _
                             "          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "          JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "          JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "          LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                             "          AND prpayment_tran.isvoid = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "	        WHERE prtranhead_master.typeof_id = 1 " & _
                '             "		    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                '             "          AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             "          AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             "		    AND cfcommon_period_tran.isactive = 1 " & _
                '             "		    AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '              "         AND prpayment_tran.paymenttranunkid IS NULL " & _
                '             "	        GROUP BY prpayrollprocess_tran.employeeunkid "
                StrQ &= "	        WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                             "          AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                             "          AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "		    AND cfcommon_period_tran.isactive = 1 " & _
                             "		    AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                              "AND prpayment_tran.paymenttranunkid IS NULL "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid "
                'Sohail (28 Aug 2013) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= " ) AS A " & _
                             "  JOIN " & _
                             "  ( " & _
                             "     SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "     , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36,2))) AS GrossPay  "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "      FROM  prpayrollprocess_tran " & _
                             "      JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "      JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "      JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "      JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "      JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                             "      AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "     WHERE  prtranhead_master.trnheadtype_id =  1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "      AND ISNULL(prtranhead_master.isvoid, 0) = 0 And ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "      AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId  " & _
                             "      AND prpayment_tran.periodunkid = @PeriodId  "


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "      GROUP BY prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= " UNION ALL " & _
                            "       SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                            ",      " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, 2))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "        FROM  prpayrollprocess_tran " & _
                             "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "        JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "        JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                             "      AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             " AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             " AND prpayment_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= " UNION ALL " & _
                             "       SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",      " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, 2))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "        FROM  prpayrollprocess_tran " & _
                            "        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "        JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "        LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                            "      AND prpayment_tran.isvoid = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             " AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             " AND prpayment_tran.paymenttranunkid IS NULL "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            End If

            StrQ &= ") AS G ON G.Empid = A.Empid "

            'Pinkal (02-May-2013) -- End


            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 28 FEB 2013 ] -- END



            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim decColumn4Total, decColumn6Total, decColumn8Total, decColumn9Total As Decimal
            decColumn4Total = 0 : decColumn6Total = 0 : decColumn8Total = 0 : decColumn9Total = 0
            Dim iCnt As Integer = 1



            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

            '    Dim rpt_Row As DataRow
            '    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

            '    rpt_Row.Item("Column1") = iCnt
            '    rpt_Row.Item("Column2") = dtRow.Item("membershipname")
            '    rpt_Row.Item("Column3") = dtRow.Item("employeecode")
            '    rpt_Row.Item("Column4") = dtRow.Item("employeename")
            '    rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("pay")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
            '    rpt_Row.Item("Column8") = ""
            '    rpt_Row.Item("Column9") = ""

            '    rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("TotalContrib")), GUI.fmtCurrency)


            '    decColumn4Total = decColumn4Total + CDec(dtRow.Item("pay"))
            '    decColumn6Total = decColumn6Total + CDec(dtRow.Item("employercontribution"))
            '    decColumn8Total = decColumn8Total + CDec(dtRow.Item("empcontribution"))
            '    decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalContrib"))

            '    iCnt += 1
            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            'Next
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = iCnt
                rpt_Row.Item("Column2") = dtRow.Item("membershipname")
                rpt_Row.Item("Column3") = dtRow.Item("employeecode")
                rpt_Row.Item("Column4") = dtRow.Item("employeename")
                Dim dtTemp() As DataRow = dsBasGross.Tables("DataTable").Select("Empid = '" & dtRow.Item("Empid") & "'")
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then
                        rpt_Row.Item("Column5") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                    Else
                        rpt_Row.Item("Column5") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
                    End If
                End If
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = ""
                rpt_Row.Item("Column9") = ""
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("TotalContrib")), GUI.fmtCurrency)
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("BasicSal"))
                    Else
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("GrossPay"))
                    End If
                End If
                decColumn6Total = decColumn6Total + CDec(dtRow.Item("employercontribution"))
                decColumn8Total = decColumn8Total + CDec(dtRow.Item("empcontribution"))
                decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalContrib"))

                iCnt += 1
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next
            'S.SANDEEP [ 28 FEB 2013 ] -- END



            objRpt = New ArutiReport.Designer.rptLAPFReport


            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtFormLAPF10", Language.getMessage(mstrModuleName, 10, "Form LAPF 10"))
            Call ReportFunction.TextChange(objRpt, "txtCaption1", Language.getMessage(mstrModuleName, 11, "THE UNITED REPUBLIC OF TANZANIA"))
            Call ReportFunction.TextChange(objRpt, "txtCaption2", Language.getMessage(mstrModuleName, 12, "THE LOCAL AUTHORITIES PENSIONS FUND"))
            Call ReportFunction.TextChange(objRpt, "txtFormCatption", Language.getMessage(mstrModuleName, 13, "MEMBER�S CONTRIBUTION FORM"))

            Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 14, "Contributing Employer�s Name:"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)

            Call ReportFunction.TextChange(objRpt, "lblEmployerAddress", Language.getMessage(mstrModuleName, 15, "Address:"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerAddress1", Company._Object._Address1 & " " & Company._Object._Address2)
            Call ReportFunction.TextChange(objRpt, "txtEmployerAddress2", Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Country_Name & " " & Company._Object._Post_Code_No)


            Call ReportFunction.TextChange(objRpt, "lblPageNo", Language.getMessage(mstrModuleName, 16, "Page No:"))
            Call ReportFunction.TextChange(objRpt, "txtPageNo", "")

            Call ReportFunction.TextChange(objRpt, "lblChequeNo", Language.getMessage(mstrModuleName, 17, "Cheque No.:"))
            Call ReportFunction.TextChange(objRpt, "txtChequeNo", "")

            Call ReportFunction.TextChange(objRpt, "lblEmployerCodeNo", Language.getMessage(mstrModuleName, 18, "Contributing Employer�s Code No.:"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerCodeNo", Company._Object._Reg1_Value)

            Call ReportFunction.TextChange(objRpt, "lblChequeDate", Language.getMessage(mstrModuleName, 19, "Date of Cheque:"))
            Call ReportFunction.TextChange(objRpt, "txtChequeDate", "")

            Call ReportFunction.TextChange(objRpt, "lblDepartmentCode", Language.getMessage(mstrModuleName, 20, "Department Code No.:"))
            Call ReportFunction.TextChange(objRpt, "txtDepartmentCode", "")

            Call ReportFunction.TextChange(objRpt, "lblAmount", Language.getMessage(mstrModuleName, 21, "Amount:"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Format(decColumn9Total, GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "lblContributionMonth", Language.getMessage(mstrModuleName, 22, "Month of Contribution:"))
            Call ReportFunction.TextChange(objRpt, "txtContributionMonth", mstrPeriodName)

            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 23, "Year:"))
            Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)

            Call ReportFunction.TextChange(objRpt, "lblReceipt", Language.getMessage(mstrModuleName, 24, "Receipt No.:"))
            Call ReportFunction.TextChange(objRpt, "txtReceipt", "")

            Call ReportFunction.TextChange(objRpt, "lblDistrictCode", Language.getMessage(mstrModuleName, 25, "Zonal/Regional/District Code No.:"))
            Call ReportFunction.TextChange(objRpt, "txtDistrictCode", Company._Object._District)

            Call ReportFunction.TextChange(objRpt, "lblDateOfReceipt", Language.getMessage(mstrModuleName, 26, "Date of Receipt:"))
            Call ReportFunction.TextChange(objRpt, "txtDateOfReceipt", "")

            Call ReportFunction.TextChange(objRpt, "lblIssuedBy", Language.getMessage(mstrModuleName, 27, "Issued by:"))
            Call ReportFunction.TextChange(objRpt, "txtIssuedBy", "")

            Call ReportFunction.TextChange(objRpt, "lblSrNo", Language.getMessage(mstrModuleName, 28, "No."))
            Call ReportFunction.TextChange(objRpt, "lblMembershipNo", Language.getMessage(mstrModuleName, 29, "Membership No."))
            Call ReportFunction.TextChange(objRpt, "lblCheckNo", Language.getMessage(mstrModuleName, 30, "Check No."))

            Call ReportFunction.TextChange(objRpt, "lblMembername", Language.getMessage(mstrModuleName, 31, "Member Name"))

            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call ReportFunction.TextChange(objRpt, "lblBasicSalary", Language.getMessage(mstrModuleName, 32, "Basic Salary"))
            If mblnShowBasicSalary = True Then
                Call ReportFunction.TextChange(objRpt, "lblBasicSalary", Language.getMessage(mstrModuleName, 32, "Basic Salary"))
            Else
                Call ReportFunction.TextChange(objRpt, "lblBasicSalary", Language.getMessage(mstrModuleName, 44, "Gross Pay"))
            End If
            'S.SANDEEP [ 28 FEB 2013 ] -- END


            Call ReportFunction.TextChange(objRpt, "lblStatutoryContribution", Language.getMessage(mstrModuleName, 33, "Statutory Contribution"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerContribution", Language.getMessage(mstrModuleName, 34, "Employer's Contribution (15%)"))
            Call ReportFunction.TextChange(objRpt, "lblEmployeesContribution", Language.getMessage(mstrModuleName, 35, "Employees Contribution (5%)"))

            Call ReportFunction.TextChange(objRpt, "lblSupplimentarOtherContribution", Language.getMessage(mstrModuleName, 36, "Supplementary / Other Contribution Contributions"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerContributionOther", Language.getMessage(mstrModuleName, 37, "Employer's Contribution (..%)"))
            Call ReportFunction.TextChange(objRpt, "lblEmployeesContributionOther", Language.getMessage(mstrModuleName, 38, "Employees Contribution (..%)"))

            Call ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 39, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 40, "Grand Total"))

            Call ReportFunction.TextChange(objRpt, "lblEmployersSignature", Language.getMessage(mstrModuleName, 41, "Contributing Employer�s Signature:"))
            Call ReportFunction.TextChange(objRpt, "txtEmployersSignature", "")

            Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 42, "Date:"))

            Call ReportFunction.TextChange(objRpt, "lblNote", "")
            Call ReportFunction.TextChange(objRpt, "lblNote1", "")
            Call ReportFunction.TextChange(objRpt, "lblNote2", "")
            Call ReportFunction.TextChange(objRpt, "lblNote3", "")


            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn6Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn8Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn9Total, GUI.fmtCurrency))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#Region " Generate_DetailReport before generate Statutory reports from using membershiptranunkid from prpayrollprocess_tran table on (27 Nov 2012) "
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'Sohail (17 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        'Sohail (17 Aug 2012) -- End


    '        StrQ = "SELECT  ROW_NUMBER() OVER ( ORDER BY ed.Empid ) AS srno " & _
    '                      ", ed.MemNo AS membershipname " & _
    '                      ", ed.empname AS employeename " & _
    '                      ", ed.EmpCode AS employeecode " & _
    '                      ", ed.Empid AS empid " & _
    '                      ", ed.MembershipUnkId AS membershipid " & _
    '                      ", ISNULL(pay.BasicSal, 0) AS pay " & _
    '                      ", CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 " & _
    '                                  "OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 " & _
    '                             "ELSE ( ISNULL(Emp_Contribution.Amount, 0) / ISNULL(pay.BasicSal, " & _
    '                                                                              "0) ) * 100 " & _
    '                        "END AS Emp_ContributionRate " & _
    '                      ", ISNULL(Emp_Contribution.Amount, 0) AS empcontribution " & _
    '                      ", CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0 " & _
    '                                  "OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 " & _
    '                             "ELSE ( ISNULL(Employer_Contribution.Amount, 0) " & _
    '                                    "/ ISNULL(pay.BasicSal, 0) ) * 100 " & _
    '                        "END AS Employer_ContributionRate " & _
    '                      ", ISNULL(Employer_Contribution.amount, 0) AS employercontribution " & _
    '                      ", ( ISNULL(Emp_Contribution.Amount, 0) " & _
    '                          "+ ISNULL(Employer_Contribution.amount, 0) ) AS TotalContrib " & _
    '                      ", ISNULL(pay.end_date, '19000101') AS end_date " & _
    '                      ", ed.EmpId " & _
    '                      ", ed.EDPeriodUnkId " & _
    '                "FROM    ( SELECT    prearningdeduction_master.tranheadunkid " & _
    '                                  ", hremployee_master.employeeunkid AS Empid " & _
    '                                  ", hremployee_master.employeecode AS EmpCode " & _
    '                                  ", hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '                                  ", ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _
    '                                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS empname " & _
    '                                  ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemNo " & _
    '                                  ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                          "FROM      prearningdeduction_master " & _
    '                                    "LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                                    "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                         "AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '                                                                         "AND ISNULL(hremployee_meminfo_tran.isactive, " & _
    '                                                                              "0) = 1 " & _
    '                          "WHERE     1 = 1 " & _
    '                                    "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
    '                                    "AND hrmembership_master.isactive = 1 " & _
    '                                    "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '                        ") AS ed " & _
    '                        "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                                          ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                                          ", prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                                          ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                            "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
    '                                    "WHERE   prtranhead_master.trnheadtype_id = 1 " & _
    '                                            "AND prtranhead_master.typeof_id = 1 " & _
    '                                            "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND cfcommon_period_tran.isactive = 1 " & _
    '                                            "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '                                    "GROUP BY cfcommon_period_tran.periodunkid " & _
    '                                          ", cfcommon_period_tran.period_name " & _
    '                                          ", prpayrollprocess_tran.employeeunkid " & _
    '                                          ", cfcommon_period_tran.end_date " & _
    '                                  ") pay ON pay.Empid = ed.Empid " & _
    '                        "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                                          ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                                          ", hremployee_master.employeeunkid AS Empid " & _
    '                                          ", prtranhead_master.trnheadname AS EmpHead " & _
    '                                          ", prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '                                          ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                          ", hrmembership_master.membershipunkid AS MembershipId " & _
    '                                          ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                                            "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                                                                              "AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                                            "LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                              "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                              "AND ISNULL(hremployee_meminfo_tran.isactive, " & _
    '                                                                              "0) = 1 " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND cfcommon_period_tran.isactive = 1 " & _
    '                                            "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
    '                                            "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '                                            "AND prtranhead_master.tranheadunkid = @EHeadId " & _
    '                                  ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid " & _
    '                                                           "AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '                                                           "AND ed.EDPeriodUnkId = Emp_Contribution.EDPeriodUnkId " & _
    '                        "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
    '                                          ", prtranhead_master.trnheadname AS EmployerHead " & _
    '                                          ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                                          ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                          ", hrmembership_master.membershipunkid AS MembershipId " & _
    '                                          ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                                          ", prpayrollprocess_tran.payrollprocesstranunkid " & _
    '                                    "FROM    prpayrollprocess_tran " & _
    '                                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                            "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                                                                              "AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                                            "LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                                            "LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                                                                              "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                              "AND ISNULL(hremployee_meminfo_tran.isactive, " & _
    '                                                                              "0) = 1 " & _
    '                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                            "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
    '                                            "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '                                            "AND prtranhead_master.tranheadunkid = @CHeadId " & _
    '                                            "AND hrmembership_master.emptranheadunkid = @EHeadId " & _
    '                                            "AND hrmembership_master.cotranheadunkid = @CHeadId " & _
    '                                  ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid " & _
    '                                                                "AND ed.MembershipUnkId = Employer_Contribution.MembershipId " & _
    '                                                                "AND ed.EDPeriodUnkId = Employer_Contribution.EDPeriodUnkId "



    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtBlank() As DataRow = dsList.Tables(0).Select("Emp_ContributionRate = 0 AND empcontribution = 0 AND Employer_ContributionRate = 0 AND employercontribution = 0 AND TotalContrib = 0")

    '        If dtBlank.Length > 0 Then
    '            For i As Integer = 0 To dtBlank.Length - 1
    '                dsList.Tables(0).Rows.Remove(dtBlank(i))
    '            Next
    '            dsList.Tables(0).AcceptChanges()
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim decColumn4Total, decColumn6Total, decColumn8Total, decColumn9Total As Decimal
    '        decColumn4Total = 0 : decColumn6Total = 0 : decColumn8Total = 0 : decColumn9Total = 0
    '        Dim iCnt As Integer = 1



    '        Dim objED As clsEarningDeduction
    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            objED = New clsEarningDeduction
    '            If objED.GetCurrentSlabEDPeriodUnkID(CInt(dtRow.Item("EmpId")), eZeeDate.convertDate(dtRow.Item("end_date").ToString)) <> CInt(dtRow.Item("EDPeriodUnkId")) Then
    '                Continue For
    '            End If

    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = iCnt
    '            rpt_Row.Item("Column2") = dtRow.Item("membershipname")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column4") = dtRow.Item("employeename")
    '            rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("pay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
    '            'rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("Emp_ContributionRate")), GUI.fmtCurrency)
    '            'rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Employer_ContributionRate")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = ""
    '            rpt_Row.Item("Column9") = ""

    '            rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("TotalContrib")), GUI.fmtCurrency)


    '            decColumn4Total = decColumn4Total + CDec(dtRow.Item("pay"))
    '            decColumn6Total = decColumn6Total + CDec(dtRow.Item("employercontribution"))
    '            decColumn8Total = decColumn8Total + CDec(dtRow.Item("empcontribution"))
    '            decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalContrib"))

    '            iCnt += 1
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptLAPFReport


    '        objRpt.SetDataSource(rpt_Data)


    '        Call ReportFunction.TextChange(objRpt, "txtFormLAPF10", Language.getMessage(mstrModuleName, 10, "Form LAPF 10"))
    '        Call ReportFunction.TextChange(objRpt, "txtCaption1", Language.getMessage(mstrModuleName, 11, "THE UNITED REPUBLIC OF TANZANIA"))
    '        Call ReportFunction.TextChange(objRpt, "txtCaption2", Language.getMessage(mstrModuleName, 12, "THE LOCAL AUTHORITIES PENSIONS FUND"))
    '        Call ReportFunction.TextChange(objRpt, "txtFormCatption", Language.getMessage(mstrModuleName, 13, "MEMBER�S CONTRIBUTION FORM"))

    '        Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 14, "Contributing Employer�s Name:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)

    '        Call ReportFunction.TextChange(objRpt, "lblEmployerAddress", Language.getMessage(mstrModuleName, 15, "Address:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerAddress1", Company._Object._Address1 & " " & Company._Object._Address2)
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerAddress2", Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Country_Name & " " & Company._Object._Post_Code_No)


    '        Call ReportFunction.TextChange(objRpt, "lblPageNo", Language.getMessage(mstrModuleName, 16, "Page No:"))
    '        Call ReportFunction.TextChange(objRpt, "txtPageNo", "")

    '        Call ReportFunction.TextChange(objRpt, "lblChequeNo", Language.getMessage(mstrModuleName, 17, "Cheque No.:"))
    '        Call ReportFunction.TextChange(objRpt, "txtChequeNo", "")

    '        Call ReportFunction.TextChange(objRpt, "lblEmployerCodeNo", Language.getMessage(mstrModuleName, 18, "Contributing Employer�s Code No.:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerCodeNo", Company._Object._Reg1_Value)

    '        Call ReportFunction.TextChange(objRpt, "lblChequeDate", Language.getMessage(mstrModuleName, 19, "Date of Cheque:"))
    '        Call ReportFunction.TextChange(objRpt, "txtChequeDate", "")

    '        Call ReportFunction.TextChange(objRpt, "lblDepartmentCode", Language.getMessage(mstrModuleName, 20, "Department Code No.:"))
    '        Call ReportFunction.TextChange(objRpt, "txtDepartmentCode", "")

    '        Call ReportFunction.TextChange(objRpt, "lblAmount", Language.getMessage(mstrModuleName, 21, "Amount:"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Format(decColumn9Total, GUI.fmtCurrency))

    '        Call ReportFunction.TextChange(objRpt, "lblContributionMonth", Language.getMessage(mstrModuleName, 22, "Month of Contribution:"))
    '        Call ReportFunction.TextChange(objRpt, "txtContributionMonth", mstrPeriodName)

    '        Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 23, "Year:"))
    '        Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)

    '        Call ReportFunction.TextChange(objRpt, "lblReceipt", Language.getMessage(mstrModuleName, 24, "Receipt No.:"))
    '        Call ReportFunction.TextChange(objRpt, "txtReceipt", "")

    '        Call ReportFunction.TextChange(objRpt, "lblDistrictCode", Language.getMessage(mstrModuleName, 25, "Zonal/Regional/District Code No.:"))
    '        Call ReportFunction.TextChange(objRpt, "txtDistrictCode", Company._Object._District)

    '        Call ReportFunction.TextChange(objRpt, "lblDateOfReceipt", Language.getMessage(mstrModuleName, 26, "Date of Receipt:"))
    '        Call ReportFunction.TextChange(objRpt, "txtDateOfReceipt", "")

    '        Call ReportFunction.TextChange(objRpt, "lblIssuedBy", Language.getMessage(mstrModuleName, 27, "Issued by:"))
    '        Call ReportFunction.TextChange(objRpt, "txtIssuedBy", "")

    '        Call ReportFunction.TextChange(objRpt, "lblSrNo", Language.getMessage(mstrModuleName, 28, "No."))
    '        Call ReportFunction.TextChange(objRpt, "lblMembershipNo", Language.getMessage(mstrModuleName, 29, "Membership No."))
    '        Call ReportFunction.TextChange(objRpt, "lblCheckNo", Language.getMessage(mstrModuleName, 30, "Check No."))

    '        Call ReportFunction.TextChange(objRpt, "lblMembername", Language.getMessage(mstrModuleName, 31, "Member Name"))
    '        Call ReportFunction.TextChange(objRpt, "lblBasicSalary", Language.getMessage(mstrModuleName, 32, "Basic Salary"))

    '        Call ReportFunction.TextChange(objRpt, "lblStatutoryContribution", Language.getMessage(mstrModuleName, 33, "Statutory Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployerContribution", Language.getMessage(mstrModuleName, 34, "Employer's Contribution (15%)"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployeesContribution", Language.getMessage(mstrModuleName, 35, "Employees Contribution (5%)"))

    '        Call ReportFunction.TextChange(objRpt, "lblSupplimentarOtherContribution", Language.getMessage(mstrModuleName, 36, "Supplementary / Other Contribution Contributions"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployerContributionOther", Language.getMessage(mstrModuleName, 37, "Employer's Contribution (..%)"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployeesContributionOther", Language.getMessage(mstrModuleName, 38, "Employees Contribution (..%)"))

    '        Call ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 39, "Total"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 40, "Grand Total"))

    '        Call ReportFunction.TextChange(objRpt, "lblEmployersSignature", Language.getMessage(mstrModuleName, 41, "Contributing Employer�s Signature:"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployersSignature", "")

    '        Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 42, "Date:"))

    '        Call ReportFunction.TextChange(objRpt, "lblNote", "")
    '        Call ReportFunction.TextChange(objRpt, "lblNote1", "")
    '        Call ReportFunction.TextChange(objRpt, "lblNote2", "")
    '        Call ReportFunction.TextChange(objRpt, "lblNote3", "")


    '        Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn6Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn8Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn9Total, GUI.fmtCurrency))

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sr. No.")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Member Number")
            Language.setMessage(mstrModuleName, 4, "Employee Rate")
            Language.setMessage(mstrModuleName, 5, "Employee Amount")
            Language.setMessage(mstrModuleName, 6, "Employer's Rate")
            Language.setMessage(mstrModuleName, 7, "Employer Amount")
            Language.setMessage(mstrModuleName, 8, "Total Contribution")
            Language.setMessage(mstrModuleName, 9, "Order By :")
            Language.setMessage(mstrModuleName, 10, "Form LAPF 10")
            Language.setMessage(mstrModuleName, 11, "THE UNITED REPUBLIC OF TANZANIA")
            Language.setMessage(mstrModuleName, 12, "THE LOCAL AUTHORITIES PENSIONS FUND")
            Language.setMessage(mstrModuleName, 13, "MEMBER�S CONTRIBUTION FORM")
            Language.setMessage(mstrModuleName, 14, "Contributing Employer�s Name:")
            Language.setMessage(mstrModuleName, 15, "Address:")
            Language.setMessage(mstrModuleName, 16, "Page No:")
            Language.setMessage(mstrModuleName, 17, "Cheque No.:")
            Language.setMessage(mstrModuleName, 18, "Contributing Employer�s Code No.:")
            Language.setMessage(mstrModuleName, 19, "Date of Cheque:")
            Language.setMessage(mstrModuleName, 20, "Department Code No.:")
            Language.setMessage(mstrModuleName, 21, "Amount:")
            Language.setMessage(mstrModuleName, 22, "Month of Contribution:")
            Language.setMessage(mstrModuleName, 23, "Year:")
            Language.setMessage(mstrModuleName, 24, "Receipt No.:")
            Language.setMessage(mstrModuleName, 25, "Zonal/Regional/District Code No.:")
            Language.setMessage(mstrModuleName, 26, "Date of Receipt:")
            Language.setMessage(mstrModuleName, 27, "Issued by:")
            Language.setMessage(mstrModuleName, 28, "No.")
            Language.setMessage(mstrModuleName, 29, "Membership No.")
            Language.setMessage(mstrModuleName, 30, "Check No.")
            Language.setMessage(mstrModuleName, 31, "Member Name")
            Language.setMessage(mstrModuleName, 32, "Basic Salary")
            Language.setMessage(mstrModuleName, 33, "Statutory Contribution")
            Language.setMessage(mstrModuleName, 34, "Employer's Contribution (15%)")
            Language.setMessage(mstrModuleName, 35, "Employees Contribution (5%)")
            Language.setMessage(mstrModuleName, 36, "Supplementary / Other Contribution Contributions")
            Language.setMessage(mstrModuleName, 37, "Employer's Contribution (..%)")
            Language.setMessage(mstrModuleName, 38, "Employees Contribution (..%)")
            Language.setMessage(mstrModuleName, 39, "Total")
            Language.setMessage(mstrModuleName, 40, "Grand Total")
            Language.setMessage(mstrModuleName, 41, "Contributing Employer�s Signature:")
            Language.setMessage(mstrModuleName, 42, "Date:")
            Language.setMessage(mstrModuleName, 43, "Employee Code")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
