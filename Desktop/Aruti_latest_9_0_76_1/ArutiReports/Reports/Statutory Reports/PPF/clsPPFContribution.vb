'************************************************************************************************************************************
'Class Name : clsPPFContribution.vb
'Purpose    :
'Date       :08/12/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Anjan
''' </summary>
Public Class clsPPFContribution
    Inherits IReportData
    ' Private Shared ReadOnly mstrModuleName As String = "clsPPFContribution"

    Private Shared mstrModuleName As String = "" 'Anjan (20 Mar 2012)
    Private mstrReportId As String = "" 'Anjan (20 Mar 2012)
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal menReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'Anjan (20 Mar 2012)

        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        mstrReportId = menReport
        Select Case menReport
            Case enArutiReport.PPFContribution
                mstrModuleName = "clsPPFContribution"
            Case enArutiReport.PSPFContribution
                mstrModuleName = "clsPSPFContribution"
            Case enArutiReport.GEPFContribution
                mstrModuleName = "clsGEPFContribution"
        End Select
        'Anjan (20 Mar 2012)-End 

        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintEmpId As Integer = -1
    Private mstrEmpName As String = String.Empty
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintEmpHeadTypeId As Integer = -1
    Private mintEmpTypeOfId As Integer = -1
    Private mintEmprHeadTypeId As Integer = -1
    Private mintEmprTypeOfId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mstrGrossPayFrom As String = String.Empty
    Private mstrGrossPayTo As String = String.Empty
    Private mstrEmpContribFrom As String = String.Empty
    Private mstrEmpContribTo As String = String.Empty
    Private mstrEmplrContribFrom As String = String.Empty
    Private mstrEmplrContribTo As String = String.Empty
    Private mintEmplrNumTypeId As Integer = -1
    Private mstrEmplrNumTypeName As String = String.Empty
    Private mstrEmplrNumber As String = String.Empty
    Private mstrMembershipNo As String = String.Empty
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintOtherEarningTranId As Integer = 0 'Sohail (28 Aug 2013)

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 28 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnShowBasicSalary As Boolean = False
    'S.SANDEEP [ 28 FEB 2013 ] -- END

    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Sohail (14 Mar 2013) -- End

    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0

    'Pinkal (02-May-2013) -- End

    'Sohail (20 Dec 2017) -- Start
    'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
    Private mstrAddress As String
    Private mstrState As String
    Private mstrPhoneNumber As String
    Private mstrCurrency As String

    'Sohail (20 Dec 2017) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmpHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmpHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmpTypeOfId() As Integer
        Set(ByVal value As Integer)
            mintEmpTypeOfId = value
        End Set
    End Property

    Public WriteOnly Property _EmprHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmprHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmprTypeOfId() As Integer
        Set(ByVal value As Integer)
            mintEmprTypeOfId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayFrom() As String
        Set(ByVal value As String)
            mstrGrossPayFrom = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayTo() As String
        Set(ByVal value As String)
            mstrGrossPayTo = value
        End Set
    End Property

    Public WriteOnly Property _EmpContribFrom() As String
        Set(ByVal value As String)
            mstrEmpContribFrom = value
        End Set
    End Property

    Public WriteOnly Property _EmpContribTo() As String
        Set(ByVal value As String)
            mstrEmpContribTo = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContribFrom() As String
        Set(ByVal value As String)
            mstrEmplrContribFrom = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContribTo() As String
        Set(ByVal value As String)
            mstrEmplrContribTo = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmplrNumTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumTypeName() As String
        Set(ByVal value As String)
            mstrEmplrNumTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumber() As String
        Set(ByVal value As String)
            mstrEmplrNumber = value
        End Set
    End Property

    Public WriteOnly Property _MembershipNo() As String
        Set(ByVal value As String)
            mstrMembershipNo = value
        End Set
    End Property


    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property
    'Sohail (28 Aug 2013) -- End

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 28 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _ShowBasicSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBasicSalary = value
        End Set
    End Property
    'S.SANDEEP [ 28 FEB 2013 ] -- END

    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (14 Mar 2013) -- End


    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    'Pinkal (02-May-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmpId = 0
            mstrEmpName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintEmpHeadTypeId = 0
            mintEmpTypeOfId = 0
            mintEmprHeadTypeId = 0
            mintEmprTypeOfId = 0
            mintPeriodId = 0
            mstrPeriodName = ""
            mstrGrossPayFrom = ""
            mstrGrossPayTo = ""
            mstrEmpContribFrom = ""
            mstrEmpContribTo = ""
            mstrEmplrContribFrom = ""
            mstrEmplrContribTo = ""
            mintEmplrNumTypeId = 0
            mstrEmplrNumTypeName = ""
            mstrEmplrNumber = ""
            mstrMembershipNo = ""
            mintReportId = -1
            mstrReportTypeName = ""
            mintOtherEarningTranId = 0 'Sohail (28 Aug 2013)

            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnShowBasicSalary = False
            'S.SANDEEP [ 28 FEB 2013 ] -- END

            'Sohail (14 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Sohail (14 Mar 2013) -- End

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            'Pinkal (02-May-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@EHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpHeadTypeId)
            'objDataOperation.AddParameter("@ETypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpTypeOfId)
            objDataOperation.AddParameter("@CHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmprHeadTypeId)
            'objDataOperation.AddParameter("@CTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmprTypeOfId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If
            'Sohail (28 Aug 2013) -- End

            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Membership :") & " " & mstrMembershipName & " "

            'If mintEmpId > 0 Then
            '    objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            '    Me._FilterQuery &= "AND EmpId = @EmpId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Employee :") & " " & mstrEmpName & " "
            'End If

            'If mstrGrossPayFrom.Trim <> "" AndAlso mstrGrossPayTo.Trim <> "" Then
            '    If Convert.ToDecimal(mstrGrossPayFrom) >= 0 AndAlso Convert.ToDecimal(mstrGrossPayTo) >= 0 Then
            '        objDataOperation.AddParameter("@GrossPayFrom", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrGrossPayFrom))
            '        objDataOperation.AddParameter("@GrossPayTo", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrGrossPayTo))
            '        Me._FilterQuery &= "AND GrossPay BETWEEN @GrossPayFrom AND @GrossPayTo "
            '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Gross Pay From : ") & " " & mstrGrossPayFrom & " " & _
            '                                    Language.getMessage(mstrModuleName, 11, "to") & " " & mstrGrossPayTo & " "
            '    End If
            'End If

            'If mstrEmpContribFrom.Trim <> "" AndAlso mstrEmpContribTo.Trim <> "" Then
            '    If Convert.ToDecimal(mstrEmpContribFrom) >= 0 AndAlso Convert.ToDecimal(mstrEmpContribTo) >= 0 Then
            '        objDataOperation.AddParameter("@EmpContribFrom", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmpContribFrom))
            '        objDataOperation.AddParameter("@EmpContribTo", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmpContribTo))
            '        Me._FilterQuery &= "AND EmpContrib BETWEEN @EmpContribFrom AND @EmpContribTo "
            '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Employee Contribution From : ") & " " & mstrEmpContribFrom & " " & _
            '                                    Language.getMessage(mstrModuleName, 11, "to") & " " & mstrEmpContribTo & " "
            '    End If
            'End If

            'If mstrEmplrContribFrom.Trim <> "" AndAlso mstrEmplrContribTo.Trim <> "" Then
            '    If Convert.ToDecimal(mstrEmplrContribFrom) >= 0 AndAlso Convert.ToDecimal(mstrEmplrContribTo) >= 0 Then
            '        objDataOperation.AddParameter("@EmplrContribFrom", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmplrContribFrom))
            '        objDataOperation.AddParameter("@EmplrContribTo", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmplrContribTo))
            '        Me._FilterQuery &= "AND EmprContrib BETWEEN @EmplrContribFrom AND @EmplrContribTo "
            '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Employer Contribution From : ") & " " & mstrEmpContribFrom & " " & _
            '                                    Language.getMessage(mstrModuleName, 11, "to") & " " & mstrEmpContribTo & " "
            '    End If
            'End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Order By : ") & " " & Me.OrderByDisplay
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Sohail (14 Mar 2013) -- Start
        '    'TRA - ENHANCEMENT
        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing
        '    'Sohail (14 Mar 2013) -- End

        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            'Sohail (20 Dec 2017) -- Start
            'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = xCompanyUnkid
            mstrAddress = objCompany._Address1 & " " & objCompany._Address2

            mstrAddress &= " " & objCompany._City_Name & " " & objCompany._Post_Code_No & " " & objCompany._Country_Name
            mstrState = objCompany._State_Name
            mstrPhoneNumber = objCompany._Phone1

            'Sohail (20 Dec 2017) -- End

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Public Function GetEmployer_No(Optional ByVal strList As String = "List") As DataSet
    '    Dim StrN As String = String.Empty
    '    Dim dsData As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation
    '        StrN &= "SELECT 0 As Id, @Select As Name " & _
    '                     "UNION SELECT 1 AS Id, @NSSF As Name " & _
    '                     "UNION SELECT 2 AS Id, @PPF As Name " & _
    '                     "ORDER BY Id "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Select"))
    '        objDataOperation.AddParameter("@NSSF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "NSSF No."))
    '        objDataOperation.AddParameter("@PPF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "PPF No."))

    '        dsData = objDataOperation.ExecQuery(StrN, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsData

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployer_No; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Public Function GetReportType(Optional ByVal strList As String = "List") As DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim dsReport As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT 1 As Id, @Periodic As Name " & _
    '                   "UNION SELECT 2 As Id, @Monthly As Name "

    '        objDataOperation.AddParameter("@Periodic", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Period Wise"))
    '        objDataOperation.AddParameter("@Monthly", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Month Wise"))

    '        dsReport = objDataOperation.ExecQuery(StrQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsReport

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetReportType; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection


    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes  AS PER ANJAN SIR GUIDANCE [REASON : Sr.No is not effecting records in sorting]
            'iColumn_DetailReport.Add(New IColumn("srno", Language.getMessage(mstrModuleName, 1, "Sr. No.")))

            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 3, "Employee Name")))

            'Pinkal (02-May-2013) -- End

            iColumn_DetailReport.Add(New IColumn("membershipname", Language.getMessage(mstrModuleName, 2, "Member Number")))

            'Pinkal (02-Oct-2012) -- Start
            'Enhancement : TRA Changes
            iColumn_DetailReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 13, "Employee Code")))
            'Pinkal (02-Oct-2012) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'ISSUE: COLUMNS REMOVED FROM DISPLAY BUT NOT FROM THE ORDER BY QUERY

            'iColumn_DetailReport.Add(New IColumn("Emp_ContributionRate", Language.getMessage(mstrModuleName, 4, "Employee Rate")))
            'iColumn_DetailReport.Add(New IColumn("Employer_ContributionRate", Language.getMessage(mstrModuleName, 6, "Employer's Rate")))
            'S.SANDEEP [04 JUN 2015] -- END

            iColumn_DetailReport.Add(New IColumn("empcontribution", Language.getMessage(mstrModuleName, 5, "Employee Amount")))
            iColumn_DetailReport.Add(New IColumn("employercontribution", Language.getMessage(mstrModuleName, 7, "Employer Amount")))
            iColumn_DetailReport.Add(New IColumn("TotalContrib", Language.getMessage(mstrModuleName, 27, "Total Contribution")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (20 Dec 2017) -- Start
            'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            mstrCurrency = objExchangeRate._Currency_Sign
            'Sohail (20 Dec 2017) -- End



            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT  ROW_NUMBER() OVER ( ORDER BY Emp_Contribution.Empid ) AS srno " & _
            '      ", Emp_Contribution.MemNo AS membershipname " & _
            '      ", Emp_Contribution.empname AS employeename " & _
            '      ", Emp_Contribution.Empid AS empid " & _
            '      ", Emp_Contribution.EmpCode AS EmpCode " & _
            '      ", Emp_Contribution.MembershipUnkId AS membershipid " & _
            '         ",ISNULL(pay.BasicSal,0) AS pay " & _
            '         ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
            '         ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
            '         ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
            '         ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
            '         ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
            '"FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
            '                  ", cfcommon_period_tran.period_name AS PeriodName " & _
            '        ",hremployee_master.employeeunkid AS Empid " & _
            '        ",hremployee_master.employeecode AS EmpCode " & _
            '"		,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS empname " & _
            '                  ", prtranhead_master.trnheadname AS EmpHead " & _
            '                  ", prtranhead_master.tranheadunkid AS EmpHeadId " & _
            '                  ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
            '                  ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
            '        ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
            '                  ", ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _
            '"FROM prpayrollprocess_tran " & _
            '                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '   "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '   "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '   "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                    "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
            '                                                         "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                                                         "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                    "LEFT JOIN hrmembership_master ON prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
            '          "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
            '   "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
            '   "AND cfcommon_period_tran.isactive = 1 " & _
            '   "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
            '                    "AND prtranhead_master.tranheadunkid = @EHeadId " & _
            '        ") AS Emp_Contribution " & _
            '        "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
            '       ",cfcommon_period_tran.period_name AS PeriodName " & _
            '                          ", prpayrollprocess_tran.employeeunkid AS Empid " & _
            '                          ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
            '                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
            '"FROM prpayrollprocess_tran " & _
            '   "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '   "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '   "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
            '                            "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '                    "WHERE   prtranhead_master.trnheadtype_id = 1 " & _
            '                            "AND prtranhead_master.typeof_id = 1 " & _
            '                            "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
            '   "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
            '"		AND cfcommon_period_tran.isactive = 1 " & _
            '   "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
            '                    "GROUP BY cfcommon_period_tran.periodunkid " & _
            '                          ", cfcommon_period_tran.period_name " & _
            '                          ", prpayrollprocess_tran.employeeunkid " & _
            '                          ", cfcommon_period_tran.end_date " & _
            '                  ") pay ON pay.Empid = Emp_Contribution.Empid " & _
            '        "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
            '           ",prtranhead_master.trnheadname AS EmployerHead " & _
            '           ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
            '           ",CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
            '"		,hrmembership_master.membershipunkid AS MembershipId " & _
            '           ",prpayrollprocess_tran.payrollprocesstranunkid " & _
            '    "FROM prpayrollprocess_tran " & _
            '       "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '       "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '       "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                            "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
            '                                                              "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                                                              "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                            "LEFT JOIN hrmembership_master ON prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
            '  "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
            '   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
            '"		AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
            '   "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
            '                            "AND prtranhead_master.tranheadunkid = @CHeadId " & _
            '                            "AND hrmembership_master.emptranheadunkid = @EHeadId " & _
            '                            "AND hrmembership_master.cotranheadunkid = @CHeadId " & _
            '                  ") AS Employer_Contribution ON Emp_Contribution.Empid = Employer_Contribution.Empid " & _
            '                                                "AND Emp_Contribution.MembershipUnkId = Employer_Contribution.MembershipId "



            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If mintBaseCurrId = mintPaidCurrencyId Then


                StrQ = "SELECT " & _
                       "     ROW_NUMBER() OVER ( ORDER BY hremployee_master.employeeunkid) AS srno " & _
                       "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                                ",hremployee_master.employeecode AS EmpCode " & _
                       "	,ISNULL(hremployee_meminfo_tran.membershipno,'') AS membershipname " & _
                       "	,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                       "	,ISNULL(CAmount,0) AS employercontribution " & _
                       "	,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+ISNULL(CAmount,0) AS TotalContrib " & _
                       "	,ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                       "	,hremployee_master.employeeunkid AS EmpId "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (14 Mar 2013) -- End

                StrQ &= "FROM prpayrollprocess_tran " & _
                                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "	JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                       "	LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                       "	JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "	LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                       "	LEFT JOIN " & _
                       "	( " & _
                       "		SELECT " & _
                       "			 hremployee_master.employeeunkid AS EmpId " & _
                       "			,ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (14 Mar 2013) -- End

                StrQ &= "FROM prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "			JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                       "			LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                       "			JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (14 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
                        "		AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
                       "			AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                       "			AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                                                    "AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	) AS C ON C.EmpId = hremployee_master.employeeunkid "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (14 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                       "	AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                       "	AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                       "	AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = " SELECT  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS employeename " & _
                           ", hremployee_master.employeecode AS EmpCode " & _
                           ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipname " & _
                           ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                           ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * ISNULL(CAmount, 0) AS employercontribution " & _
                           ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) ) AS TotalContrib " & _
                           ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                           ", hremployee_master.employeeunkid AS EmpId "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM  prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             " LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             " AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                             " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             " LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                             " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= " LEFT JOIN ( " & _
                             "                      SELECT  hremployee_master.employeeunkid AS EmpId " & _
                             "                      , ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")), 0) AS CAmount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "                  FROM prpayrollprocess_tran " & _
                              "                 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                              "                 JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                              "                 AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                              "                 LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                              "                 AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                              "                 JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                              "                 LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                              "                 AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "                  WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "                  AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             "                  AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                             "                  AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS C ON C.EmpId = hremployee_master.employeeunkid "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             " AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                             " AND prpayment_tran.periodunkid = @PeriodId  "


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL " & _
                             " SELECT  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                             ", hremployee_master.employeecode AS EmpCode " & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipname " & _
                             ", " & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", " & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
                             ", " & mdecConversionRate & " * (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0)) AS TotalContrib " & _
                             ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             " LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             " AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                             " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             " LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                             " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                             " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= "  LEFT JOIN ( " & _
                             "                    SELECT  hremployee_master.employeeunkid AS EmpId " & _
                             "                    , ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")), 0) AS CAmount  "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "                      FROM  prpayrollprocess_tran " & _
                             "                      JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "                      JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             "                      AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             "                      LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             "                      AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                             "                      JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "                      LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "                      AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                      WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "                      AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             "                      AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                             "                      AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS C ON C.EmpId = hremployee_master.employeeunkid "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                             " AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                             " AND prpayment_tran.periodunkid = @PeriodId  "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL " & _
                             " SELECT  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                             ", hremployee_master.employeecode AS EmpCode " & _
                             " , ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipname " & _
                             ", " & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", " & mdecConversionRate & " * ISNULL(CAmount, 0) AS employercontribution " & _
                             ", " & mdecConversionRate & " * ( ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) ) AS TotalContrib " & _
                             ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM  prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             " AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                             " LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             " AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                             " JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             " LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                             " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
                             " LEFT JOIN ( " & _
                             "                     SELECT  hremployee_master.employeeunkid AS EmpId " & _
                             "                     ,  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount   "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "                   FROM prpayrollprocess_tran " & _
                             "                   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "                   JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             "                   AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                             "                   LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                             "                   AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                             "                   JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                             "                   LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "                   AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                  WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "                  AND prtnaleave_tran.payperiodunkid =  @PeriodId " & _
                             "                  AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                             "                  AND hrmembership_master.emptranheadunkid = @EHeadId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= ") AS C ON C.EmpId = hremployee_master.employeeunkid"

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                          " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                          " AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                          " AND prpayment_tran.paymenttranunkid IS NULL "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtBlank() As DataRow = dsList.Tables(0).Select("empcontribution = 0 AND employercontribution = 0 AND TotalContrib = 0")

            If dtBlank.Length > 0 Then
                For i As Integer = 0 To dtBlank.Length - 1
                    dsList.Tables(0).Rows.Remove(dtBlank(i))
                Next
                dsList.Tables(0).AcceptChanges()
            End If



            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                        "	 A.Empid " & _
                        "	,BasicSal " & _
                        "	,GrossPay " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (14 Mar 2013) -- End

                StrQ &= "	FROM prpayrollprocess_tran " & _
                        "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (14 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "	WHERE prtranhead_master.typeof_id = 1 " & _
                '                    "		AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                    "		AND cfcommon_period_tran.isactive = 1 " & _
                '                    "		AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '                    "	GROUP BY prpayrollprocess_tran.employeeunkid "
                StrQ &= "WHERE    ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                    "		AND cfcommon_period_tran.isactive = 1 " & _
                                 "AND cfcommon_period_tran.periodunkid = @PeriodId "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid "
                'Sohail (28 Aug 2013) -- End

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If
                'Sohail (14 Mar 2013) -- End

                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'Sohail (14 Mar 2013) -- End

                StrQ &= "	FROM prpayrollprocess_tran " & _
                        "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                StrQ &= mstrAnalysis_Join
                'Sohail (14 Mar 2013) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	WHERE prtranhead_master.trnheadtype_id = 1 " & _
                        "		AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                        "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "		AND cfcommon_period_tran.isactive = 1 " & _
                        "		AND cfcommon_period_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid "

                'Sohail (14 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If
                'Sohail (14 Mar 2013) -- End

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                     "	 A.Empid " & _
                     "	,BasicSal " & _
                     "	,GrossPay " & _
                     "FROM " & _
                     "( " & _
                     "	SELECT " & _
                     "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                     "		,( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   FROM prpayrollprocess_tran " & _
                             "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
                             "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "   JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid AND prpayment_tran.isvoid = 0" & _
                             "   AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.countryunkid = " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "  WHERE prtranhead_master.typeof_id = 1 And ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '             "  AND prpayment_tran.periodunkid = @PeriodId " & _
                '             "  GROUP BY  prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "
                StrQ &= "  WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "
                'Sohail (28 Aug 2013) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "  UNION ALL " & _
                            "  SELECT prpayrollprocess_tran.employeeunkid AS Empid " & _
                            ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   FROM  prpayrollprocess_tran " & _
                             "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "   JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid   AND prpayment_tran.isvoid = 0 " & _
                             "   AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.countryunkid <> " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "    WHERE  prtranhead_master.typeof_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '             "    AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             "    AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '             "    AND prpayment_tran.periodunkid = @PeriodId " & _
                '             "    GROUP BY  prpayrollprocess_tran.employeeunkid "
                StrQ &= "    WHERE    ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "    AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                     "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "    AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                     "AND prpayment_tran.periodunkid = @PeriodId "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid "
                'Sohail (28 Aug 2013) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "     UNION ALL " & _
                             "     SELECT prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "     ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS BasicSal"


                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "    FROM prpayrollprocess_tran " & _
                             "    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "    JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "    LEFT JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                             "    AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "  WHERE prtranhead_master.typeof_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                '             " AND prpayment_tran.paymenttranunkid IS NULL " & _
                '             " GROUP BY  prpayrollprocess_tran.employeeunkid"
                StrQ &= "  WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                    "AND prpayment_tran.paymenttranunkid IS NULL "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid"
                'Sohail (28 Aug 2013) -- End

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= ") AS A " & _
                             "JOIN " & _
                             "  ( " & _
                             "           SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "          ,( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM prpayrollprocess_tran " & _
                            "           JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "           JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "           JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "           JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                            "           AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prpayment_tran.countryunkid = " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "          WHERE  prtranhead_master.trnheadtype_id =1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "          AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "          AND prpayment_tran.periodunkid= @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "          GROUP BY prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "          UNION ALL " & _
                             "          SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",         " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "             FROM prpayrollprocess_tran " & _
                             "             JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "             JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                             "             AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prpayment_tran.countryunkid <> " & mintCountryId

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "            WHERE prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "            AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "            AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "            AND prpayment_tran.periodunkid= @PeriodId  "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "            GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "          UNION ALL " & _
                             "          SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",         " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM prpayrollprocess_tran " & _
                             "          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "           JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "           LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                            "           AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "           WHERE prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "           AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "           AND prpayment_tran.paymenttranunkid IS NULL  "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "           GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            End If

            'Pinkal (02-May-2013) -- End

            StrQ &= ") AS G ON G.Empid = A.Empid "





            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 28 FEB 2013 ] -- END

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim decColumn4Total, decColumn6Total, decColumn8Total, decColumn9Total As Decimal
            decColumn4Total = 0 : decColumn6Total = 0 : decColumn8Total = 0 : decColumn9Total = 0
            Dim iCnt As Integer = 1



            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = iCnt
                rpt_Row.Item("Column2") = dtRow.Item("membershipname")
                rpt_Row.Item("Column3") = dtRow.Item("employeename")
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
                'Sohail (01 Nov 2017) -- Start
                'Issue - 70.1 - SUPPORT 0001439 - SUMATRA | nssf and pspf reports discrepancy with the payroll report.
                'rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("TotalContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("empcontribution")) + CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
                'Sohail (01 Nov 2017) -- End
                rpt_Row.Item("Column10") = dtRow.Item("EmpCode")
                Dim dtTemp() As DataRow = dsBasGross.Tables("DataTable").Select("Empid = '" & dtRow.Item("Empid") & "'")
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then

                        rpt_Row.Item("Column4") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)

                        If CDec(dtRow.Item("empcontribution")) = 0 Or CDec(dtTemp(0).Item("BasicSal")) = 0 Then
                            rpt_Row.Item("Column5") = CDbl(0)
                        Else
                            rpt_Row.Item("Column5") = CDbl((CDec(dtRow.Item("empcontribution")) / CDec(dtTemp(0).Item("BasicSal"))) * 100).ToString("##0.#0")
                        End If

                        If CDec(dtRow.Item("employercontribution")) = 0 Or CDec(dtTemp(0).Item("BasicSal")) = 0 Then
                            rpt_Row.Item("Column7") = CDbl(0)
                        Else
                            rpt_Row.Item("Column7") = CDbl((CDec(dtRow.Item("employercontribution")) / CDec(dtTemp(0).Item("BasicSal"))) * 100).ToString("##0.#0")
                        End If
                    Else

                        rpt_Row.Item("Column4") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)

                        If CDec(dtRow.Item("empcontribution")) = 0 Or CDec(dtTemp(0).Item("GrossPay")) = 0 Then
                            rpt_Row.Item("Column5") = CDbl(0)
                        Else
                            rpt_Row.Item("Column5") = CDbl((CDec(dtRow.Item("empcontribution")) / CDec(dtTemp(0).Item("GrossPay"))) * 100)
                        End If

                        If CDec(dtRow.Item("employercontribution")) = 0 Or CDec(dtTemp(0).Item("GrossPay")) = 0 Then
                            rpt_Row.Item("Column7") = CDbl(0)
                        Else
                            rpt_Row.Item("Column7") = CDbl((CDec(dtRow.Item("employercontribution")) / CDec(dtTemp(0).Item("GrossPay"))) * 100)
                        End If
                    End If
                End If
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("BasicSal"))
                    Else
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("GrossPay"))
                    End If
                End If
                'Sohail (01 Nov 2017) -- Start
                'Issue - 70.1 - SUPPORT 0001439 - SUMATRA | nssf and pspf reports discrepancy with the payroll report.
                'decColumn6Total = decColumn6Total + CDec(dtRow.Item("empcontribution"))
                'decColumn8Total = decColumn8Total + CDec(dtRow.Item("employercontribution"))
                'decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalContrib"))
                decColumn6Total = decColumn6Total + CDec(Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency))
                decColumn8Total = decColumn8Total + CDec(Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency))
                decColumn9Total = decColumn9Total + CDec(Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)) + CDec(Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency))
                'Sohail (01 Nov 2017) -- End

                iCnt += 1
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            Select Case mstrReportId
                Case enArutiReport.PPFContribution
                    objRpt = New ArutiReport.Designer.rptPPF_contribution
                Case enArutiReport.PSPFContribution
                    objRpt = New ArutiReport.Designer.rptPSPF_contribution
                Case enArutiReport.GEPFContribution
                    objRpt = New ArutiReport.Designer.rptGEPF_contribution
            End Select

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtAppendix", Language.getMessage(mstrModuleName, 9, "APPENDIX 5 "))

            Select Case mstrReportId
                Case enArutiReport.PPFContribution
                    Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 10, "PPF/CONT/01"))
                Case enArutiReport.PSPFContribution
                    'Sohail (20 Dec 2017) -- Start
                    'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
                    'Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 11, "PSPF/CONT/01"))
                    Call ReportFunction.TextChange(objRpt, "txtPPFCont", "")
                    Call ReportFunction.TextChange(objRpt, "txtAppendix", Language.getMessage(mstrModuleName, 29, "Annexture  4 "))
                    'Sohail (20 Dec 2017) -- End

                Case enArutiReport.GEPFContribution
                    Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 12, "GEPF/CONT/01"))
            End Select


            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 13, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "lblContribution", Language.getMessage(mstrModuleName, 14, "CONTRIBUTION FOR THE MONTH OF"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", "")
            Call ReportFunction.TextChange(objRpt, "lblEmployer", Language.getMessage(mstrModuleName, 15, "Name of Employer"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)

            Select Case mstrReportId
                Case enArutiReport.PPFContribution
                    objRpt.SetParameterValue("parPPFAddress", Language.getMessage(mstrModuleName, 16, "Director General PPF") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 17, "P.O Box. 72473") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 18, "Dar es Salaam, Tanzania") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 19, "Tel.: 2113919/22. 2110642") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 20, "Fax.: 2117772"))
                Case enArutiReport.PSPFContribution
                    'Sohail (20 Dec 2017) -- Start
                    'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
                    'objRpt.SetParameterValue("parPPFAddress", "")
                    objRpt.SetParameterValue("parPPFAddress", Language.getMessage(mstrModuleName, 30, "P.O BOX 4843") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 31, "GOLDEN JUBILEE TOWERS ") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 32, "OHIO/KIBO STREET") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 33, "TEL:+255 22 2120912/52, 2127375/76") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 34, "Email: pspf@pspf-tz.org") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 35, "Website:www.pspf-tz.org") & _
                                                    vbCrLf & Language.getMessage(mstrModuleName, 36, "DAR ES SALAAM"))

                    Call ReportFunction.TextChange(objRpt, "txtContribRemitForm", Language.getMessage(mstrModuleName, 37, "CONTRIBUTIONS REMITTANCE FORM"))
                    Call ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 38, "NAME OF THE CONTRIBUTING EMPLOYER : "))
                    Call ReportFunction.TextChange(objRpt, "txtNameinfull", Language.getMessage(mstrModuleName, 39, "Name "))
                    Call ReportFunction.TextChange(objRpt, "txtmemberhispname", Language.getMessage(mstrModuleName, 40, "Check Number (Membership Number)"))
                    Call ReportFunction.TextChange(objRpt, "txtVotecode", Language.getMessage(mstrModuleName, 41, "Votecode (Employer Number)"))
                    Call ReportFunction.TextChange(objRpt, "lblAddress", Language.getMessage(mstrModuleName, 42, "ADDRESS : "))
                    Call ReportFunction.TextChange(objRpt, "lblLocation", Language.getMessage(mstrModuleName, 43, "LOCATION : "))
                    Call ReportFunction.TextChange(objRpt, "lblPhoneNumber", Language.getMessage(mstrModuleName, 44, "PHONE NUMBER : "))
                    Call ReportFunction.TextChange(objRpt, "lblChequeNumber", Language.getMessage(mstrModuleName, 45, "CHEQUE NUMBER : "))
                    Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 46, "DATE : "))
                    Call ReportFunction.TextChange(objRpt, "lblAmount", Language.getMessage(mstrModuleName, 47, "AMOUNT : "))
                    Call ReportFunction.TextChange(objRpt, "lblNameAndSignature", Language.getMessage(mstrModuleName, 48, "NAME AND SIGNATURE : "))
                    Call ReportFunction.TextChange(objRpt, "txtCheckNumber", "")
                    Call ReportFunction.TextChange(objRpt, "txtDate", "")
                    Call ReportFunction.TextChange(objRpt, "txtCurrency", "")

                    'Sohail (20 Dec 2017) -- End
                Case enArutiReport.GEPFContribution
                    objRpt.SetParameterValue("parPPFAddress", "")
            End Select



            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
            'Sohail (20 Dec 2017) -- Start
            'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            If mstrReportId <> enArutiReport.PSPFContribution Then
                'Sohail (20 Dec 2017) -- End
                Call ReportFunction.TextChange(objRpt, "txtNameinfull", Language.getMessage(mstrModuleName, 21, "Name in Full"))
                Call ReportFunction.TextChange(objRpt, "txtmemberhispname", Language.getMessage(mstrModuleName, 2, "Member Number"))
                'Sohail (20 Dec 2017) -- Start
                'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            End If
            'Sohail (20 Dec 2017) -- End
            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call ReportFunction.TextChange(objRpt, "txtMonthlySalary", Language.getMessage(mstrModuleName, 22, "Monthly Salary"))
            If mblnShowBasicSalary = True Then
                Call ReportFunction.TextChange(objRpt, "txtMonthlySalary", Language.getMessage(mstrModuleName, 22, "Monthly Salary"))
            Else
                Call ReportFunction.TextChange(objRpt, "txtMonthlySalary", Language.getMessage(mstrModuleName, 56, "Gross Pay"))
            End If
            'S.SANDEEP [ 28 FEB 2013 ] -- END

            'Sohail (20 Dec 2017) -- Start
            'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            If mstrReportId <> enArutiReport.PSPFContribution Then
                'Sohail (20 Dec 2017) -- End

                Call ReportFunction.TextChange(objRpt, "txtMemberdetails", Language.getMessage(mstrModuleName, 23, "Members Contribution"))

                Call ReportFunction.TextChange(objRpt, "txtMemberRate", Language.getMessage(mstrModuleName, 24, "Rate"))
                Call ReportFunction.TextChange(objRpt, "txtMemberAmt", Language.getMessage(mstrModuleName, 25, "Amount"))

                Call ReportFunction.TextChange(objRpt, "txtEmployersdetails", Language.getMessage(mstrModuleName, 26, "Employer's Contribution"))
                Call ReportFunction.TextChange(objRpt, "txtEmployerRate", Language.getMessage(mstrModuleName, 24, "Rate"))
                Call ReportFunction.TextChange(objRpt, "txtEmployerAmt", Language.getMessage(mstrModuleName, 25, "Amount"))
                Call ReportFunction.TextChange(objRpt, "txtTotalContribution", Language.getMessage(mstrModuleName, 27, "Total Contribution"))


                'Sohail (20 Dec 2017) -- Start
                'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            End If
            Call ReportFunction.TextChange(objRpt, "txtMemberAmt", Language.getMessage(mstrModuleName, 57, "Member Amount(5% or 10%)"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerAmt", Language.getMessage(mstrModuleName, 58, "Employer Amount(15% or 10%)"))
            Call ReportFunction.TextChange(objRpt, "txtTotalContribution", Language.getMessage(mstrModuleName, 59, "Total Contibutions (20%)"))
            'Sohail (20 Dec 2017) -- End


            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 28, "TOTAL"))

            Call ReportFunction.TextChange(objRpt, "txtPeriod", mstrPeriodName)

            'Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 31, "Printed Date :"))
            'Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 32, "Printed By :"))
            'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 33, "Page :"))

            'Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            'Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            'Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn6Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn8Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn9Total, GUI.fmtCurrency))
            'Sohail (20 Dec 2017) -- Start
            'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
            If mstrReportId = enArutiReport.PSPFContribution Then
                Call ReportFunction.TextChange(objRpt, "txtAddress", mstrAddress)
                Call ReportFunction.TextChange(objRpt, "txtLocation", mstrState)
                Call ReportFunction.TextChange(objRpt, "txtPhoneNumber", mstrPhoneNumber)
                Call ReportFunction.TextChange(objRpt, "txtCurrency", mstrCurrency)
                Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn9Total, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtNameAndSignature", Me._UserName)
            End If
            'Sohail (20 Dec 2017) -- End


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function GenerateEFT_PSPF_DetailReport(ByVal strDatabaseName As String, _
                                                  ByVal intUserUnkid As Integer, _
                                                  ByVal intYearUnkid As Integer, _
                                                  ByVal intCompanyUnkid As Integer, _
                                                  ByVal dtPeriodEnd As Date, _
                                                  ByVal strUserModeSetting As String, _
                                                  ByVal blnOnlyApproved As Boolean, _
                                                  ByVal strExportReportPath As String, _
                                                  ByVal blnOpenAfterExport As Boolean, _
                                                  ByVal intBaseCurrencyId As Integer) As Boolean
        'Public Function GenerateEFT_PSPF_DetailReport() As Boolean
        'S.SANDEEP [04 JUN 2015] -- END

        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport

        Dim objBank As New clspayrollgroup_master
        Dim blnFlag As Boolean = False
        Dim intPrevId As Integer = 0
        'Dim decSubTotEmp As Decimal = 0
        'Dim decSubTotCo As Decimal = 0
        'Dim decSubTotBasic As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            strQ = "SELECT  PeriodName AS PeriodName " & _
                          ", EmpId " & _
                          ", EmpName AS EmpName " & _
                          ", end_date " & _
                          ", MembershipNo AS MembershipNo " & _
                          ", MembershipName AS MembershipName " & _
                          ", EmpContrib AS EmpContrib " & _
                          ", EmprContrib AS EmprContrib " & _
                          ", Total AS Total " & _
                          ", PaymentDate " & _
                          ", BasicSal AS BasicSal " & _
                          ", EmpCode AS EmpCode " & _
                          ", Id " & _
                          ", GName " & _
                    "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
                                      ", TableEmp_Contribution.PeriodName " & _
                                      ", TableEmp_Contribution.end_date " & _
                                      ", TableEmp_Contribution.EmpId " & _
                                      ", TableEmp_Contribution.EmpName " & _
                                      ", TableEmp_Contribution.Membership_Name AS MembershipName " & _
                                      ", TableEmp_Contribution.Membership_No AS MembershipNo " & _
                                      ", TableEmp_Contribution.MembershipUnkId AS MemId " & _
                                      ", ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
                                      ", TableEmployer_Contribution.EmployerHead " & _
                                      ", ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
                                      ", ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
                                      ", ISNULL(Payment.paymentdate, '19000101') AS PaymentDate " & _
                                      ", ISNULL(BasicSal, 0) AS BasicSal " & _
                                      ", TableEmp_Contribution.EmpCode AS EmpCode " & _
                                      ", TableEmp_Contribution.StDate AS StDate " & _
                                      ", TableEmp_Contribution.Id " & _
                                      ", TableEmp_Contribution.GName " & _
                              "FROM      ( SELECT    ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                                                  ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                                  ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, " & _
                                                                            "'01-Jan-1900'), 112) AS end_date " & _
                                                  ", hremployee_master.employeeunkid AS Empid " & _
                                                  ", hremployee_master.employeecode AS EmpCode " & _
                                                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                                                  ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                                  ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                                  ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                                  ", hrmembership_master.membershipname AS Membership_Name " & _
                                                  ", cfcommon_period_tran.start_date AS StDate "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", 0 AS Id, '' AS GName "
            End If

            strQ &= "                       FROM      prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                  "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                  "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                    "JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join
            End If
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            strQ &= "                       WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND cfcommon_period_tran.isactive = 1 " & _
                                                    "AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                                                    "AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId "

            'If mstrEmpCode.Trim <> "" Then
            '    strQ &= " AND hremployee_master.employeecode LIKE @EmpCode "
            'End If

            'If mintEmployeeunkid > 0 Then
            '    strQ &= " AND hremployee_master.employeeunkid = @Employeeunkid "
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            strQ &= "                   ) AS TableEmp_Contribution " & _
                                        "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
                                                          ", prtranhead_master.trnheadname AS EmployerHead " & _
                                                          ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                                                          ", ISNULL(cfcommon_period_tran.periodunkid, " & _
                                                                   "0) AS Periodid " & _
                                                          ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                                          ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                                          ", hrmembership_master.membershipunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", 0 AS Id, '' AS GName "
            End If

            strQ &= "                               FROM    prpayrollprocess_tran " & _
                                                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                            "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                  "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                  "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                                                  "0) = 0 " & _
                                                            "JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            strQ &= "                               WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                                            "AND hrmembership_master.cotranheadunkid = @CHeadId " & _
                                                            "AND hrmembership_master.emptranheadunkid = @EHeadId " & _
                                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId "

            'If mstrEmpCode.Trim <> "" Then
            '    strQ &= " AND hremployee_master.employeecode LIKE @EmpCode "
            'End If

            'If mintEmployeeunkid > 0 Then
            '    strQ &= " AND hremployee_master.employeeunkid = @Employeeunkid "
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            strQ &= "                               ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
                                                                                  "AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
                                                                                  "AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
                                        "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                                                          ", cfcommon_period_tran.period_name AS PeriodName " & _
                                                          ", hremployee_master.employeeunkid AS Empid " & _
                                                          ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, " & _
                                                                            "0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", 0 AS Id, '' AS GName "
            End If

            strQ &= "                               FROM    prpayrollprocess_tran " & _
                                                            "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ &= "                               WHERE   prtranhead_master.trnheadtype_id = 1 " & _
            '                                                "AND prtranhead_master.typeof_id = 1 " & _
            '                                                "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
            '                                                           "0) = 0 " & _
            '                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                                "AND cfcommon_period_tran.isactive = 1 " & _
            '                                                "AND cfcommon_period_tran.periodunkid = @PeriodId "
            strQ &= "                               WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                           "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                                            "AND cfcommon_period_tran.periodunkid = @PeriodId "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                strQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                strQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If
            'Sohail (28 Aug 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'If mstrEmpCode.Trim <> "" Then
            '    strQ &= " AND hremployee_master.employeecode LIKE @EmpCode "
            'End If

            'If mintEmployeeunkid > 0 Then
            '    strQ &= " AND hremployee_master.employeeunkid = @Employeeunkid "
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If

            strQ &= "                               GROUP BY cfcommon_period_tran.periodunkid " & _
                                                          ", cfcommon_period_tran.period_name " & _
                                                          ", hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If
            strQ &= "                               ) AS BAS ON BAS.Empid = TableEmp_Contribution.EmpId " & _
                                                              "AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
                                        " LEFT JOIN ( SELECT  prpayment_tran.employeeunkid AS EmpId " & _
                                                          ", prpayment_tran.periodunkid AS Periodid " & _
                                                          ", CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) AS paymentdate "

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", 0 AS Id, '' AS GName "
            End If


            strQ &= "                               FROM    prpayment_tran " & _
                                                                        "LEFT JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                                        "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                                                        "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                        "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                                                        "JOIN hrmsConfiguration..cfbankacctype_master ON hrmsConfiguration..cfbankacctype_master.accounttypeunkid = premployee_bank_tran.accounttypeunkid " & _
                                                                        "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                                                        "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                                                                        "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid "


            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            strQ &= "                               WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                            "AND prempsalary_tran.isvoid = 0 " & _
                                                            "AND premployee_bank_tran.isvoid = 0 " & _
                                                            "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
                                                            "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
                                                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                            "AND prpayment_tran.periodunkid = @PeriodId "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If

            If mblnIncludeInactiveEmp = False Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'S.SANDEEP [04 JUN 2015] -- END

            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'If mstrEmpCode.Trim <> "" Then
            '    strQ &= " AND hremployee_master.employeecode LIKE @EmpCode "
            'End If

            'If mintEmployeeunkid > 0 Then
            '    strQ &= " AND hremployee_master.employeeunkid = @Employeeunkid "
            'End If



            'If mintReportModeId = 0 Then 'DRAFT
            '    strQ &= " AND ISNULL(prpayment_tran.isauthorized, 0 ) = @isauthorized "
            'ElseIf mintReportModeId = 1 Then 'AUTHORIZED
            '    strQ &= " AND ISNULL(prpayment_tran.isauthorized, 0 ) = @isauthorized "
            'End If

            'If mintCompanyBranchId > 0 Then
            '    strQ &= " AND prpayment_tran.branchunkid = @branchunkid "
            'End If

            'If mstrCompanyBankAccountNo.Trim <> "" Then
            '    strQ &= " AND prpayment_tran.accountno = @accountno "
            'End If

            'If mintBankId > 0 Then
            '    strQ &= " AND hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = @BankId "
            'End If

            'If mintBranchId > 0 Then
            '    strQ &= " AND hrmsConfiguration..cfbankbranch_master.branchunkid = @BranchName "
            'End If

            'If mdblAmount > 0 And mdblAmountTo > 0 Then
            '    strQ &= " AND ISNULL(prempsalary_tran.expaidamt,0) BETWEEN  @Amount AND @AmountTo "
            'End If

            'If mintCurrencyId > 0 Then
            '    strQ &= " AND prpayment_tran.countryunkid = @Currencyunkid "
            'End If



            strQ &= "                           ) AS Payment ON BAS.EmpId = Payment.EmpId " & _
                                                                  "AND BAS.Periodid = Payment.Periodid " & _
                            ") AS Contribution " & _
                    "WHERE   1 = 1 " & _
                            "AND Periodid = @PeriodId " & _
                            "AND EmpContrib <> 0 " & _
                            "AND EmprContrib <> 0 "

            Call FilterTitleAndFilterQuery()

            'strQ &= Me._FilterQuery
            If mintViewIndex > 0 Then
                'strQ &= " ORDER BY GName, " & OrderByQuery
                strQ &= " ORDER BY GName "
                'Else
                '    strQ &= " ORDER BY " & OrderByQuery
            End If


            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim intRowCount As Integer = dsList.Tables("DataTable").Rows.Count

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            'mdecEFTPspf_Emp_Contrib = 0
            'mdecEFTPspf_Co_Contrib = 0
            'mdecEFTPspf_BasicSal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column2") = dtRow.Item("EmpCode")
                rpt_Row.Item("Column3") = dtRow.Item("EmpName")
                rpt_Row.Item("Column4") = dtRow.Item("PeriodName")
                rpt_Row.Item("Column5") = dtRow.Item("MembershipNo")
                rpt_Row.Item("Column6") = ConfigParameter._Object._EFT_Code
                rpt_Row.Item("Column7") = eZeeDate.convertDate(dtRow.Item("paymentdate").ToString).ToShortDateString
                rpt_Row.Item("Column81") = CDec(Format(dtRow.Item("EmpContrib"), GUI.fmtCurrency))
                rpt_Row.Item("Column82") = CDec(Format(dtRow.Item("EmprContrib"), GUI.fmtCurrency))
                rpt_Row.Item("Column83") = CDec(Format(dtRow.Item("BasicSal"), GUI.fmtCurrency))

                rpt_Row.Item("Column9") = dtRow.Item("Id")
                rpt_Row.Item("Column10") = dtRow.Item("GName")

                'If mintViewIndex > 0 Then
                '    If intPrevId <> dtRow.Item("Id") Then
                '        decSubTotEmp = CDec(Format(dtRow.Item("EmpContrib"), GUI.fmtCurrency))
                '        decSubTotCo = CDec(Format(dtRow.Item("EmprContrib"), GUI.fmtCurrency))
                '        decSubTotBasic = CDec(Format(dtRow.Item("BasicSal"), GUI.fmtCurrency))
                '    Else
                '        decSubTotEmp += CDec(Format(dtRow.Item("EmpContrib"), GUI.fmtCurrency))
                '        decSubTotCo = +CDec(Format(dtRow.Item("EmprContrib"), GUI.fmtCurrency))
                '        decSubTotBasic += CDec(Format(dtRow.Item("BasicSal"), GUI.fmtCurrency))
                '    End If
                'End If
                'rpt_Row.Item("Column11") = Format(decSubTotEmp, GUI.fmtCurrency)
                'rpt_Row.Item("Column12") = Format(decSubTotCo, GUI.fmtCurrency)
                'rpt_Row.Item("Column13") = Format(decSubTotBasic, GUI.fmtCurrency)

                'mdecEFTPspf_Emp_Contrib += CDec(Format(dtRow.Item("EmpContrib"), GUI.fmtCurrency))
                'mdecEFTPspf_Co_Contrib += CDec(Format(dtRow.Item("EmprContrib"), GUI.fmtCurrency))
                'mdecEFTPspf_BasicSal += CDec(Format(dtRow.Item("BasicSal"), GUI.fmtCurrency))

                intPrevId = dtRow.Item("Id")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next



            'objRpt = New ArutiReport.Designer.rptEFTPSPF


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'ReportFunction.Logo_Display(objRpt, _
            '                            ConfigParameter._Object._IsDisplayLogo, _
            '                            ConfigParameter._Object._ShowLogoRightSide, _
            '                            "arutiLogo1", _
            '                            "arutiLogo2", _
            '                            arrImageRow, _
            '                            "txtCompanyName", _
            '                            "txtReportName", _
            '                            "", _
            '                            ConfigParameter._Object._GetLeftMargin, _
            '                            ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 37, "Prepared By :"))
            'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", "")
            'Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 38, "Checked By :"))

            'If ConfigParameter._Object._IsShowApprovedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 39, "Approved By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            'End If

            'If ConfigParameter._Object._IsShowReceivedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 40, "Received By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            'End If

            'objRpt.SetDataSource(rpt_Data)

            'Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 49, "Staff Name"))
            'Call ReportFunction.TextChange(objRpt, "txtMembershipNo", Language.getMessage(mstrModuleName, 81, "Check No."))
            'Call ReportFunction.TextChange(objRpt, "txtVoteCode", Language.getMessage(mstrModuleName, 82, "votecode"))
            'Call ReportFunction.TextChange(objRpt, "txtMemberAmt", Language.getMessage(mstrModuleName, 83, "Member Amount"))
            'Call ReportFunction.TextChange(objRpt, "txtEmployerAmt", Language.getMessage(mstrModuleName, 84, "Employer Amount"))
            'Call ReportFunction.TextChange(objRpt, "txtPaymentDate", Language.getMessage(mstrModuleName, 85, "PayDate"))
            'Call ReportFunction.TextChange(objRpt, "txtBasicPay", Language.getMessage(mstrModuleName, 86, "Basic Pay"))

            'Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 70, "Printed Date :"))
            'Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 71, "Printed By :"))

            'Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            'Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            'Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)

            'If mintReportModeId = 0 Then
            '    Call ReportFunction.TextChange(objRpt, "txtReportMode", "DRAFT") 'mstrReportModeName
            'ElseIf mintReportModeId = 1 Then
            '    Call ReportFunction.TextChange(objRpt, "txtReportMode", "AUTHORIZED") 'mstrReportModeName
            'End If
            'objRpt.ReportDefinition.ReportObjects("txtReportMode").Left = objRpt.ReportDefinition.ReportObjects("txtCompanyName").Left
            'objRpt.ReportDefinition.ReportObjects("txtReportMode").Width = objRpt.ReportDefinition.ReportObjects("txtCompanyName").Width


            'If mintViewIndex > 0 Then
            '    Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            '    Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 46, "Group Total :"))
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GHAnalysis", False)
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GFAnalysis", True)
            'Else
            '    Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            '    Call ReportFunction.TextChange(objRpt, "txtGroupTotal", "")
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GHAnalysis", True)
            '    Call ReportFunction.EnableSuppressSection(objRpt, "GFAnalysis", True)
            'End If

            Dim intColIndex As Integer = -1
            mdtTableExcel = rpt_Data.Tables(0)


            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 49, "Staff Name")
            intColIndex += 1
            mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 50, "Check No.")
            intColIndex += 1
            mdtTableExcel.Columns("Column5").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 51, "votecode")
            intColIndex += 1
            mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 52, "Member Amount")
            intColIndex += 1
            mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 53, "Employer Amount")
            intColIndex += 1
            mdtTableExcel.Columns("Column82").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 54, "PayDate")
            intColIndex += 1
            mdtTableExcel.Columns("Column7").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 55, "Basic Pay")
            intColIndex += 1
            mdtTableExcel.Columns("Column83").SetOrdinal(intColIndex)

            'If mintViewIndex > 0 Then
            '    mdtTableExcel.Columns("Column10").Caption = mstrReport_GroupName
            '    intColIndex += 1
            '    mdtTableExcel.Columns("Column10").SetOrdinal(intColIndex)
            'End If

            For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
            Next

            Dim intArrayColWidth() As Integer = {200, 100, 130, 150, 150, 110, 150}

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtTableExcel, intArrayColWidth, False, , False, , , , " ", , , , , , , , False)
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateEFT_PSPF_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            objBank = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function
    'Sohail (14 Mar 2013) -- End

#Region " Generate_DetailReport before generate Statutory reports from using membershiptranunkid from prpayrollprocess_tran table on (27 Nov 2012) "
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'Sohail (17 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        'Sohail (17 Aug 2012) -- End


    '        'S.SANDEEP [ 15 SEP 2011 ] -- START
    '        'ISSUE : BASIC SAL CONTRIBUTED FOR VARIOUS COSTCENTER
    '        'StrQ = "SELECT " & _
    '        '                "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '        '                 ",ed.MemNo AS membershipname " & _
    '        '                 ",ed.empname AS employeename " & _
    '        '                 ",ed.Empid AS empid " & _
    '        '                 ",ed.MembershipUnkId AS membershipid " & _
    '        '                 ",ISNULL(pay.BasicSal,0) AS pay " & _
    '        '                 ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '        '                 ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '        '                 ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '        '                 ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '        '                 ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '        '    "FROM " & _
    '        '        "( " & _
    '        '          "SELECT " & _
    '        '                "prearningdeduction_master.tranheadunkid " & _
    '        '                ",hremployee_master.employeeunkid AS Empid " & _
    '        '                ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '                ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '        '        "		,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS empname " & _
    '        '                ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '        '                "FROM prearningdeduction_master " & _
    '        '        "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '        '           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "			AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "   WHERE 1 = 1  AND ISNULL(prearningdeduction_master.isvoid,0)= 0 " & _
    '        '        "		AND hrmembership_master.isactive = 1 " & _
    '        '            "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '        '        ") AS ed " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '               "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '        '               ",ISNULL(prpayrollprocess_tran.amount ,0) AS BasicSal " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '        '        "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '           "AND prtranhead_master.typeof_id = 1 " & _
    '        '           "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '        ") pay ON pay.Empid = ed.Empid " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",hremployee_master.employeeunkid AS Empid " & _
    '        '               ",prtranhead_master.trnheadname AS EmpHead " & _
    '        '               ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '        '               ",prpayrollprocess_tran.amount AS Amount " & _
    '        '        "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                          "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '        "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '        '        "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '        "		AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '        '        ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                   "hremployee_master.employeeunkid AS Empid " & _
    '        '                   ",prtranhead_master.trnheadname AS EmployerHead " & _
    '        '                   ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '                   ",prpayrollprocess_tran.amount AS Amount " & _
    '        '        "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '                   ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '        '            "FROM prpayrollprocess_tran " & _
    '        '               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '               "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '               "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                         "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '        "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '        '        "		LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '          "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '        "		AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '        "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '        '        ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "



    '        'S.SANDEEP [ 18 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES (DUPLICATE NAMES OF EMPLOYEE DUE TO SLAB INTRODUCTION IN EARNING & DEDUCTION)
    '        'StrQ = "SELECT " & _
    '        '               "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '        '                ",ed.MemNo AS membershipname " & _
    '        '                ",ed.empname AS employeename " & _
    '        '                ",ed.Empid AS empid " & _
    '        '                ",ed.MembershipUnkId AS membershipid " & _
    '        '                ",ISNULL(pay.BasicSal,0) AS pay " & _
    '        '                ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '        '                ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '        '                ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '        '                ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '        '                ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '        '   "FROM " & _
    '        '       "( " & _
    '        '         "SELECT " & _
    '        '               "prearningdeduction_master.tranheadunkid " & _
    '        '               ",hremployee_master.employeeunkid AS Empid " & _
    '        '               ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '               ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '        '       "		,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS empname " & _
    '        '               ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '        '               "FROM prearningdeduction_master " & _
    '        '       "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '        '          "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '       "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '       "			AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '       "   WHERE 1 = 1  AND ISNULL(prearningdeduction_master.isvoid,0)= 0 " & _
    '        '       "		AND hrmembership_master.isactive = 1 " & _
    '        '           "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '        '       ") AS ed " & _
    '        '   "LEFT JOIN " & _
    '        '       "( " & _
    '        '           "SELECT " & _
    '        '              "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '              ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '              ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '        '      "       ,SUM(ISNULL(prpayrollprocess_tran.amount ,0)) AS BasicSal " & _
    '        '       "FROM prpayrollprocess_tran " & _
    '        '          "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '          "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '          "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '          "JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '        '       "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '          "AND prtranhead_master.typeof_id = 1 " & _
    '        '          "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '          "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '          "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '          "AND cfcommon_period_tran.isactive = 1 " & _
    '        '          "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '      "   Group By cfcommon_period_tran.periodunkid,cfcommon_period_tran.period_name,prpayrollprocess_tran.employeeunkid " & _
    '        '       ") pay ON pay.Empid = ed.Empid " & _
    '        '   "LEFT JOIN " & _
    '        '       "( " & _
    '        '           "SELECT " & _
    '        '               "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '              ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '              ",hremployee_master.employeeunkid AS Empid " & _
    '        '              ",prtranhead_master.trnheadname AS EmpHead " & _
    '        '              ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '        '              ",prpayrollprocess_tran.amount AS Amount " & _
    '        '       "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '       "FROM prpayrollprocess_tran " & _
    '        '          "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '          "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '          "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '          "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '          "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                         "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '       "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '        '       "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '       "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '       "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '          "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '          "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '       "		AND cfcommon_period_tran.isactive = 1 " & _
    '        '       "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '          "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '          "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '        '       ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '        '   "LEFT JOIN " & _
    '        '       "( " & _
    '        '           "SELECT " & _
    '        '                  "hremployee_master.employeeunkid AS Empid " & _
    '        '                  ",prtranhead_master.trnheadname AS EmployerHead " & _
    '        '                  ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '                  ",prpayrollprocess_tran.amount AS Amount " & _
    '        '       "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '                  ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '        '           "FROM prpayrollprocess_tran " & _
    '        '              "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '              "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '              "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '              "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                        "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '       "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '        '       "		LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '       "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '         "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '          "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '       "		AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '       "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '          "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '        '          "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '        '       ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "

    '        StrQ = "SELECT " & _
    '                        "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '                         ",ed.MemNo AS membershipname " & _
    '                         ",ed.empname AS employeename " & _
    '                         ",ed.Empid AS empid " & _
    '                         ",ed.EmpCode AS EmpCode " & _
    '                         ",ed.MembershipUnkId AS membershipid " & _
    '                         ",ISNULL(pay.BasicSal,0) AS pay " & _
    '                         ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '                         ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '                         ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '                         ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '                         ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '                    ",ISNULL(pay.end_date, '19000101') AS end_date " & _
    '                    ",ed.EmpId " & _
    '                    ",ed.EDPeriodUnkId " & _
    '            "FROM " & _
    '                "( " & _
    '                  "SELECT " & _
    '                        "prearningdeduction_master.tranheadunkid " & _
    '                        ",hremployee_master.employeeunkid AS Empid " & _
    '                        ",hremployee_master.employeecode AS EmpCode " & _
    '                        ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '                        ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '                "		,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS empname " & _
    '                        ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '                    ",prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                        "FROM prearningdeduction_master " & _
    '                "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                   "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "			AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                "   WHERE 1 = 1  AND ISNULL(prearningdeduction_master.isvoid,0)= 0 " & _
    '                "		AND hrmembership_master.isactive = 1 " & _
    '                    "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '                ") AS ed " & _
    '            "LEFT JOIN " & _
    '                "( " & _
    '                    "SELECT " & _
    '                       "cfcommon_period_tran.periodunkid AS Periodid " & _
    '                       ",cfcommon_period_tran.period_name AS PeriodName " & _
    '                       ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '               "       ,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                    ",CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) AS end_date " & _
    '                "FROM prpayrollprocess_tran " & _
    '                   "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                   "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                   "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                   "AND prtranhead_master.typeof_id = 1 " & _
    '                   "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '                   "AND cfcommon_period_tran.isactive = 1 " & _
    '                   "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '                    "   Group By cfcommon_period_tran.periodunkid,cfcommon_period_tran.period_name,prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.end_date " & _
    '                ") pay ON pay.Empid = ed.Empid " & _
    '            "LEFT JOIN " & _
    '                "( " & _
    '                    "SELECT " & _
    '                        "cfcommon_period_tran.periodunkid AS Periodid " & _
    '                       ",cfcommon_period_tran.period_name AS PeriodName " & _
    '                       ",hremployee_master.employeeunkid AS Empid " & _
    '                       ",prtranhead_master.trnheadname AS EmpHead " & _
    '                       ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '                       ",CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '                    ",prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                "FROM prpayrollprocess_tran " & _
    '                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                   "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                   "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                   "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                                  "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '                "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '                "		AND cfcommon_period_tran.isactive = 1 " & _
    '                "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '                   "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '                   "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '                ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId AND ed.EDPeriodUnkId = Emp_Contribution.EDPeriodUnkId " & _
    '            "LEFT JOIN " & _
    '                "( " & _
    '                    "SELECT " & _
    '                           "hremployee_master.employeeunkid AS Empid " & _
    '                           ",prtranhead_master.trnheadname AS EmployerHead " & _
    '                           ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                           ",CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '                    ",prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                           ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '                    "FROM prpayrollprocess_tran " & _
    '                       "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                       "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                       "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                       "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                                 "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '                "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                "		LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                  "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '                "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '                   "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '                   "AND prtranhead_master.tranheadunkid = @CHeadId  " 'Sohail (13 Apr 2012) - [EmpCode]

    '        'Sohail (06 Mar 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        StrQ &= " AND hrmembership_master.emptranheadunkid = @EHeadId " & _
    '                " AND hrmembership_master.cotranheadunkid = @CHeadId "
    '        'Sohail (06 Mar 2012) -- End

    '        StrQ &= ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId AND ed.EDPeriodUnkId = Employer_Contribution.EDPeriodUnkId "
    '        'S.SANDEEP [ 18 FEB 2012 ] -- END



    '        'S.SANDEEP [ 15 SEP 2011 ] -- END 

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtBlank() As DataRow = dsList.Tables(0).Select("Emp_ContributionRate = 0 AND empcontribution = 0 AND Employer_ContributionRate = 0 AND employercontribution = 0 AND TotalContrib = 0")

    '        If dtBlank.Length > 0 Then
    '            For i As Integer = 0 To dtBlank.Length - 1
    '                dsList.Tables(0).Rows.Remove(dtBlank(i))
    '            Next
    '            dsList.Tables(0).AcceptChanges()
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn4Total, decColumn6Total, decColumn8Total, decColumn9Total As Decimal
    '        decColumn4Total = 0 : decColumn6Total = 0 : decColumn8Total = 0 : decColumn9Total = 0
    '        Dim iCnt As Integer = 1
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 



    '        'S.SANDEEP [ 18 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Dim objED As clsEarningDeduction
    '        'S.SANDEEP [ 18 FEB 2012 ] -- END
    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            objED = New clsEarningDeduction
    '            If objED.GetCurrentSlabEDPeriodUnkID(CInt(dtRow.Item("EmpId")), eZeeDate.convertDate(dtRow.Item("end_date").ToString)) <> CInt(dtRow.Item("EDPeriodUnkId")) Then
    '                Continue For
    '            End If

    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = iCnt
    '            rpt_Row.Item("Column2") = dtRow.Item("membershipname")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeename")
    '            rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("pay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("Emp_ContributionRate")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("Employer_ContributionRate")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("TotalContrib")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column10") = dtRow.Item("EmpCode") 'Sohail (13 Apr 2012) -- Start


    '            'S.SANDEEP [ 05 JULY 2011 ] -- START
    '            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '            decColumn4Total = decColumn4Total + CDec(dtRow.Item("pay"))
    '            decColumn6Total = decColumn6Total + CDec(dtRow.Item("empcontribution"))
    '            decColumn8Total = decColumn8Total + CDec(dtRow.Item("employercontribution"))
    '            decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalContrib"))
    '            'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '            iCnt += 1
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next


    '        'Anjan (20 Mar 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '        Select Case mstrReportId
    '            Case enArutiReport.PPFContribution
    '                objRpt = New ArutiReport.Designer.rptPPF_contribution
    '            Case enArutiReport.PSPFContribution
    '                objRpt = New ArutiReport.Designer.rptPSPF_contribution
    '            Case enArutiReport.GEPFContribution
    '                objRpt = New ArutiReport.Designer.rptGEPF_contribution
    '        End Select
    '        'Anjan (20 Mar 2012)-End 

    '        objRpt.SetDataSource(rpt_Data)


    '        Call ReportFunction.TextChange(objRpt, "txtAppendix", Language.getMessage(mstrModuleName, 9, "APPENDIX 5 "))

    '        'Anjan (20 Mar 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

    '        Select Case mstrReportId
    '            Case enArutiReport.PPFContribution
    '                Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 10, "PPF/CONT/01"))
    '            Case enArutiReport.PSPFContribution
    '                Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 11, "PSPF/CONT/01"))
    '            Case enArutiReport.GEPFContribution
    '                Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 12, "GEPF/CONT/01"))
    '        End Select
    '        'Anjan (20 Mar 2012)-End


    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 13, "Employee Code")) 'Sohail (13 Apr 2012)
    '        Call ReportFunction.TextChange(objRpt, "lblContribution", Language.getMessage(mstrModuleName, 14, "CONTRIBUTION FOR THE MONTH OF"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", "")
    '        Call ReportFunction.TextChange(objRpt, "lblEmployer", Language.getMessage(mstrModuleName, 15, "Name of Employer"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)


    '        'Anjan (20 Mar 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '        Select Case mstrReportId
    '            Case enArutiReport.PPFContribution
    '                objRpt.SetParameterValue("parPPFAddress", Language.getMessage(mstrModuleName, 16, "Director General PPF") & _
    '                                                vbCrLf & Language.getMessage(mstrModuleName, 17, "P.O Box. 72473") & _
    '                                                vbCrLf & Language.getMessage(mstrModuleName, 18, "Dar es Salaam, Tanzania") & _
    '                                                vbCrLf & Language.getMessage(mstrModuleName, 19, "Tel.: 2113919/22. 2110642") & _
    '                                                vbCrLf & Language.getMessage(mstrModuleName, 20, "Fax.: 2117772"))
    '            Case enArutiReport.PSPFContribution
    '                objRpt.SetParameterValue("parPPFAddress", "")
    '            Case enArutiReport.GEPFContribution
    '                objRpt.SetParameterValue("parPPFAddress", "")
    '        End Select
    '        'Anjan (20 Mar 2012)-End 



    '        Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
    '        Call ReportFunction.TextChange(objRpt, "txtmemberhispname", Language.getMessage(mstrModuleName, 2, "Member Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtNameinfull", Language.getMessage(mstrModuleName, 21, "Name in Full"))

    '        Call ReportFunction.TextChange(objRpt, "txtMonthlySalary", Language.getMessage(mstrModuleName, 22, "Monthly Salary"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberdetails", Language.getMessage(mstrModuleName, 23, "Members Contribution"))

    '        Call ReportFunction.TextChange(objRpt, "txtMemberRate", Language.getMessage(mstrModuleName, 24, "Rate"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberAmt", Language.getMessage(mstrModuleName, 25, "Amount"))

    '        Call ReportFunction.TextChange(objRpt, "txtEmployersdetails", Language.getMessage(mstrModuleName, 26, "Employer's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerRate", Language.getMessage(mstrModuleName, 24, "Rate"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerAmt", Language.getMessage(mstrModuleName, 25, "Amount"))

    '        Call ReportFunction.TextChange(objRpt, "txtTotalContribution", Language.getMessage(mstrModuleName, 27, "Total Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 28, "TOTAL"))

    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", mstrPeriodName)

    '        'Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 31, "Printed Date :"))
    '        'Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 32, "Printed By :"))
    '        'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 33, "Page :"))

    '        'Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        'Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
    '        'Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol41")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol61")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol81")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol91")
    '        Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn6Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn8Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn9Total, GUI.fmtCurrency))
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'Sandeep [ 09 MARCH 2011 ] -- Start
    '        'Issue : Formula To Calulate The Contribution Changed
    '        'StrQ = "SELECT " & _
    '        '                "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '        '                 ",ed.membershipname AS membershipname " & _
    '        '                 ",ed.empname AS employeename " & _
    '        '                 ",ed.Empid AS empid " & _
    '        '                 ",ed.MembershipUnkId AS membershipid " & _
    '        '                 ",ISNULL(pay.BasicSal,0) AS pay " & _
    '        '                 ",CASE WHEN ISNULL(Emp_Contribution.Amount,0) = 0 THEN 0 ELSE " & _
    '        '                      "(ISNULL(Emp_Contribution.Amount,0) /(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0))*100) END AS Emp_ContributionRate " & _
    '        '                 ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '        '                 ",CASE WHEN ISNULL(Employer_Contribution.Amount,0) = 0 THEN 0 ELSE " & _
    '        '                      "(ISNULL(Employer_Contribution.Amount,0) /(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0))*100) END AS Employer_ContributionRate " & _
    '        '                 ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '        '                 ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '        '    "FROM " & _
    '        '        "( " & _
    '        '          "SELECT " & _
    '        '                "prearningdeduction_master.tranheadunkid " & _
    '        '                ",hremployee_master.employeeunkid AS Empid " & _
    '        '                ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '                ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '        '                ",hremployee_master.firstname + '' + hremployee_master.othername + ''+ hremployee_master.surname AS empname " & _
    '        '                "FROM prearningdeduction_master " & _
    '        '           "LEFT JOIN hrmembership_master ON prearningdeduction_master.vendorunkid = hrmembership_master.membershipunkid " & _
    '        '           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "WHERE hremployee_master.isactive = 1 " & _
    '        '            "AND hrmembership_master.isactive = 1 " & _
    '        '            "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '        '        ") AS ed " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '               "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '        '               ",ISNULL(prpayrollprocess_tran.amount ,0) AS BasicSal " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '           "AND prtranhead_master.typeof_id = 1 " & _
    '        '           "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '    ") pay ON pay.Empid = ed.Empid " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",hremployee_master.employeeunkid AS Empid " & _
    '        '               ",prtranhead_master.trnheadname AS EmpHead " & _
    '        '               ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '        '               ",prpayrollprocess_tran.amount AS Amount " & _
    '        '               ",prearningdeduction_master.vendorunkid AS MembershipId " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                          "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '           "LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '        '                          "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND hremployee_master.isactive = 1 " & _
    '        '           "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '        '        ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                   "hremployee_master.employeeunkid AS Empid " & _
    '        '                   ",prtranhead_master.trnheadname AS EmployerHead " & _
    '        '                   ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '                   ",prpayrollprocess_tran.amount AS Amount " & _
    '        '                   ",prearningdeduction_master.vendorunkid AS MembershipId " & _
    '        '                   ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '        '            "FROM prpayrollprocess_tran " & _
    '        '               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '               "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '               "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                         "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '               "LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '        '                         "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '          "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND hremployee_master.isactive = 1 " & _
    '        '           "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '        '        ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "


    '        'Sandeep [ 16 MAY 2011 ] -- Start
    '        'ISSUE : DATA DUPLICATION DUE TO INACTIVE MEMBERSHIP
    '        '    StrQ = "SELECT " & _
    '        '            "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '        '             ",ed.MemNo AS membershipname " & _
    '        '             ",ed.empname AS employeename " & _
    '        '             ",ed.Empid AS empid " & _
    '        '             ",ed.MembershipUnkId AS membershipid " & _
    '        '             ",ISNULL(pay.BasicSal,0) AS pay " & _
    '        '             ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '        '             ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '        '             ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '        '             ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '        '             ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '        '"FROM " & _
    '        '    "( " & _
    '        '      "SELECT " & _
    '        '            "prearningdeduction_master.tranheadunkid " & _
    '        '            ",hremployee_master.employeeunkid AS Empid " & _
    '        '            ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '            ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '        '            ",hremployee_master.firstname + '' + hremployee_master.othername + ''+ hremployee_master.surname AS empname " & _
    '        '            ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '        '            "FROM prearningdeduction_master " & _
    '        '       "LEFT JOIN hrmembership_master ON prearningdeduction_master.vendorunkid = hrmembership_master.membershipunkid " & _
    '        '       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '       "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '        '                " AND prearningdeduction_master.vendorunkid = hremployee_meminfo_tran.membershipunkid " & _
    '        '    "WHERE hremployee_master.isactive = 1 " & _
    '        '        "AND hrmembership_master.isactive = 1 " & _
    '        '        "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '        '    ") AS ed " & _
    '        '"LEFT JOIN " & _
    '        '    "( " & _
    '        '        "SELECT " & _
    '        '           "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '           ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '           ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '        '           ",ISNULL(prpayrollprocess_tran.amount ,0) AS BasicSal " & _
    '        '    "FROM prpayrollprocess_tran " & _
    '        '       "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '       "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '    "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '       "AND prtranhead_master.typeof_id = 1 " & _
    '        '       "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '       "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '       "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '       "AND cfcommon_period_tran.isactive = 1 " & _
    '        '       "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '") pay ON pay.Empid = ed.Empid " & _
    '        '"LEFT JOIN " & _
    '        '    "( " & _
    '        '        "SELECT " & _
    '        '            "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '           ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '           ",hremployee_master.employeeunkid AS Empid " & _
    '        '           ",prtranhead_master.trnheadname AS EmpHead " & _
    '        '           ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '        '           ",prpayrollprocess_tran.amount AS Amount " & _
    '        '           ",prearningdeduction_master.vendorunkid AS MembershipId " & _
    '        '    "FROM prpayrollprocess_tran " & _
    '        '       "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '       "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '       "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                      "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '       "LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '        '                      "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '    "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '       "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '       "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '       "AND cfcommon_period_tran.isactive = 1 " & _
    '        '       "AND hremployee_master.isactive = 1 " & _
    '        '       "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '       "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '       "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '        '    ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '        '"LEFT JOIN " & _
    '        '    "( " & _
    '        '        "SELECT " & _
    '        '               "hremployee_master.employeeunkid AS Empid " & _
    '        '               ",prtranhead_master.trnheadname AS EmployerHead " & _
    '        '               ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '               ",prpayrollprocess_tran.amount AS Amount " & _
    '        '               ",prearningdeduction_master.vendorunkid AS MembershipId " & _
    '        '               ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                     "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '           "LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '        '                     "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '      "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '       "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '       "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '       "AND hremployee_master.isactive = 1 " & _
    '        '       "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '       "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '        '       "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '        '    ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "


    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : MEMBERSHIP MAPPED WITH TRANSHEAD
    '        'StrQ = "SELECT " & _
    '        '                "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '        '                 ",ed.MemNo AS membershipname " & _
    '        '                 ",ed.empname AS employeename " & _
    '        '                 ",ed.Empid AS empid " & _
    '        '                 ",ed.MembershipUnkId AS membershipid " & _
    '        '                 ",ISNULL(pay.BasicSal,0) AS pay " & _
    '        '                 ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '        '                 ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '        '                 ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '        '                 ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '        '                 ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '        '    "FROM " & _
    '        '        "( " & _
    '        '          "SELECT " & _
    '        '                "prearningdeduction_master.tranheadunkid " & _
    '        '                ",hremployee_master.employeeunkid AS Empid " & _
    '        '                ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '                ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '        '                ",hremployee_master.firstname + '' + hremployee_master.othername + ''+ hremployee_master.surname AS empname " & _
    '        '                ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '        '                "FROM prearningdeduction_master " & _
    '        '           "LEFT JOIN hrmembership_master ON prearningdeduction_master.vendorunkid = hrmembership_master.membershipunkid " & _
    '        '           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '           "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '        '            " AND prearningdeduction_master.vendorunkid = hremployee_meminfo_tran.membershipunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "WHERE hremployee_master.isactive = 1 " & _
    '        '            "AND hrmembership_master.isactive = 1 " & _
    '        '            "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '        '        ") AS ed " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '               "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '        '               ",ISNULL(prpayrollprocess_tran.amount ,0) AS BasicSal " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '           "AND prtranhead_master.typeof_id = 1 " & _
    '        '           "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '    ") pay ON pay.Empid = ed.Empid " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",hremployee_master.employeeunkid AS Empid " & _
    '        '               ",prtranhead_master.trnheadname AS EmpHead " & _
    '        '               ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '        '               ",prpayrollprocess_tran.amount AS Amount " & _
    '        '               ",prearningdeduction_master.vendorunkid AS MembershipId " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                          "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '           "LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '        '                  "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND hremployee_master.isactive = 1 " & _
    '        '           "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '        '        ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                   "hremployee_master.employeeunkid AS Empid " & _
    '        '                   ",prtranhead_master.trnheadname AS EmployerHead " & _
    '        '                   ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '                   ",prpayrollprocess_tran.amount AS Amount " & _
    '        '                   ",prearningdeduction_master.vendorunkid AS MembershipId " & _
    '        '                   ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '        '            "FROM prpayrollprocess_tran " & _
    '        '               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '               "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '               "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                         "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '               "LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid " & _
    '        '                 "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '          "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND hremployee_master.isactive = 1 " & _
    '        '           "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '        '        ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "


    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'StrQ = "SELECT " & _
    '        '                "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '        '                 ",ed.MemNo AS membershipname " & _
    '        '                 ",ed.empname AS employeename " & _
    '        '                 ",ed.Empid AS empid " & _
    '        '                 ",ed.MembershipUnkId AS membershipid " & _
    '        '                 ",ISNULL(pay.BasicSal,0) AS pay " & _
    '        '                 ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '        '                 ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '        '                 ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '        '                 ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '        '                 ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '        '    "FROM " & _
    '        '        "( " & _
    '        '          "SELECT " & _
    '        '                "prearningdeduction_master.tranheadunkid " & _
    '        '                ",hremployee_master.employeeunkid AS Empid " & _
    '        '                ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '                ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '        '        "		,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS empname " & _
    '        '                ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '        '                "FROM prearningdeduction_master " & _
    '        '        "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '        '           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "			AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "WHERE hremployee_master.isactive = 1 " & _
    '        '            "AND hrmembership_master.isactive = 1 " & _
    '        '            "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '        '        ") AS ed " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '               "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '        '               ",ISNULL(prpayrollprocess_tran.amount ,0) AS BasicSal " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '           "AND prtranhead_master.typeof_id = 1 " & _
    '        '           "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '    ") pay ON pay.Empid = ed.Empid " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                "cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '               ",cfcommon_period_tran.period_name AS PeriodName " & _
    '        '               ",hremployee_master.employeeunkid AS Empid " & _
    '        '               ",prtranhead_master.trnheadname AS EmpHead " & _
    '        '               ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '        '               ",prpayrollprocess_tran.amount AS Amount " & _
    '        '        "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "FROM prpayrollprocess_tran " & _
    '        '           "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '           "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '           "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '           "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '           "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                          "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '        "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '        '        "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.isactive = 1 " & _
    '        '           "AND hremployee_master.isactive = 1 " & _
    '        '           "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '        '        ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '        '    "LEFT JOIN " & _
    '        '        "( " & _
    '        '            "SELECT " & _
    '        '                   "hremployee_master.employeeunkid AS Empid " & _
    '        '                   ",prtranhead_master.trnheadname AS EmployerHead " & _
    '        '                   ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '                   ",prpayrollprocess_tran.amount AS Amount " & _
    '        '        "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '                   ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '        '            "FROM prpayrollprocess_tran " & _
    '        '               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '               "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '               "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '                         "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '        '        "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '        '        "		LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '          "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '        '           "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '        '           "AND hremployee_master.isactive = 1 " & _
    '        '           "AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '        '           "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '        '           "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '        '        ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "

    '        StrQ = "SELECT " & _
    '                        "ROW_NUMBER() OVER(ORDER BY ed.Empid) AS srno " & _
    '                         ",ed.MemNo AS membershipname " & _
    '                         ",ed.empname AS employeename " & _
    '                         ",ed.Empid AS empid " & _
    '                         ",ed.MembershipUnkId AS membershipid " & _
    '                         ",ISNULL(pay.BasicSal,0) AS pay " & _
    '                         ",CASE WHEN ISNULL(Emp_Contribution.Amount, 0) = 0 OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Emp_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100 END AS Emp_ContributionRate " & _
    '                         ",ISNULL(Emp_Contribution.Amount,0) AS empcontribution " & _
    '                         ",CASE WHEN ISNULL(Employer_Contribution.Amount, 0) = 0  OR ISNULL(pay.BasicSal, 0) = 0 THEN 0 ELSE (ISNULL(Employer_Contribution.Amount, 0)/ISNULL(pay.BasicSal, 0))*100  END AS Employer_ContributionRate " & _
    '                         ",ISNULL(Employer_Contribution.amount ,0)AS employercontribution " & _
    '                         ",(ISNULL(Emp_Contribution.Amount,0) +ISNULL(Employer_Contribution.amount ,0)) AS TotalContrib " & _
    '            "FROM " & _
    '                "( " & _
    '                  "SELECT " & _
    '                        "prearningdeduction_master.tranheadunkid " & _
    '                        ",hremployee_master.employeeunkid AS Empid " & _
    '                        ",hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '                        ",ISNULL(hrmembership_master.membershipname,'') AS membershipname " & _
    '                "		,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS empname " & _
    '                        ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemNo " & _
    '                        "FROM prearningdeduction_master " & _
    '                "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                   "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "			AND hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                "   WHERE 1 = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND hrmembership_master.isactive = 1 " & _
    '                    "AND prearningdeduction_master.tranheadunkid = @EHeadId " & _
    '                ") AS ed " & _
    '            "LEFT JOIN " & _
    '                "( " & _
    '                    "SELECT " & _
    '                       "cfcommon_period_tran.periodunkid AS Periodid " & _
    '                       ",cfcommon_period_tran.period_name AS PeriodName " & _
    '                       ",prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                       ",ISNULL(prpayrollprocess_tran.amount ,0) AS BasicSal " & _
    '                "FROM prpayrollprocess_tran " & _
    '                   "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                   "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                   "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                   "AND prtranhead_master.typeof_id = 1 " & _
    '                   "AND ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '                   "AND cfcommon_period_tran.isactive = 1 " & _
    '                   "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= ") pay ON pay.Empid = ed.Empid " & _
    '            "LEFT JOIN " & _
    '                "( " & _
    '                    "SELECT " & _
    '                        "cfcommon_period_tran.periodunkid AS Periodid " & _
    '                       ",cfcommon_period_tran.period_name AS PeriodName " & _
    '                       ",hremployee_master.employeeunkid AS Empid " & _
    '                       ",prtranhead_master.trnheadname AS EmpHead " & _
    '                       ",prtranhead_master.tranheadunkid AS EmpHeadId " & _
    '                       ",prpayrollprocess_tran.amount AS Amount " & _
    '                "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '                "FROM prpayrollprocess_tran " & _
    '                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                   "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                   "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                   "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                                  "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '                "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.emptranheadunkid " & _
    '                "		LEFT JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 " & _
    '                "		AND cfcommon_period_tran.isactive = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '                   "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ") " & _
    '                   "AND prtranhead_master.tranheadunkid = @EHeadId  " & _
    '                ") AS Emp_Contribution ON ed.Empid = Emp_Contribution.Empid AND ed.MembershipUnkId = Emp_Contribution.MembershipId " & _
    '            "LEFT JOIN " & _
    '                "( " & _
    '                    "SELECT " & _
    '                           "hremployee_master.employeeunkid AS Empid " & _
    '                           ",prtranhead_master.trnheadname AS EmployerHead " & _
    '                           ",prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                           ",prpayrollprocess_tran.amount AS Amount " & _
    '                "		,hrmembership_master.membershipunkid AS MembershipId " & _
    '                           ",prpayrollprocess_tran.payrollprocesstranunkid " & _
    '                    "FROM prpayrollprocess_tran " & _
    '                       "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                       "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                       "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                       "LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                                 "AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid " & _
    '                "		LEFT JOIN hrmembership_master ON prearningdeduction_master.tranheadunkid = hrmembership_master.cotranheadunkid " & _
    '                "		LEFT JOIN hremployee_Meminfo_tran ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "			AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                  "WHERE  ISNULL(prpayrollprocess_tran.isvoid ,0) = 0 " & _
    '                   "AND ISNULL(prtranhead_master.isvoid ,0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid ,0) = 0 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND ISNULL(prearningdeduction_master.isvoid ,0) = 0 " & _
    '                   "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
    '                   "AND prtranhead_master.tranheadunkid = @CHeadId  " & _
    '                ") AS Employer_Contribution ON ed.Empid = Employer_Contribution.Empid AND ed.MembershipUnkId = Employer_Contribution.MembershipId "

    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 


    '        'S.SANDEEP [ 08 June 2011 ] -- END

    '        'Sandeep [ 16 MAY 2011 ] -- End 


    '        'Sandeep [ 09 MARCH 2011 ] -- End 

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn4Total, decColumn6Total, decColumn8Total, decColumn9Total As Decimal
    '        decColumn4Total = 0 : decColumn6Total = 0 : decColumn8Total = 0 : decColumn9Total = 0
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("srno")
    '            rpt_Row.Item("Column2") = dtRow.Item("membershipname")
    '            rpt_Row.Item("Column3") = dtRow.Item("employeename")
    '            rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("pay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("Emp_ContributionRate")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("Employer_ContributionRate")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("TotalContrib")), GUI.fmtCurrency)


    '            'S.SANDEEP [ 05 JULY 2011 ] -- START
    '            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '            decColumn4Total = decColumn4Total + CDec(dtRow.Item("pay"))
    '            decColumn6Total = decColumn6Total + CDec(dtRow.Item("empcontribution"))
    '            decColumn8Total = decColumn8Total + CDec(dtRow.Item("employercontribution"))
    '            decColumn9Total = decColumn9Total + CDec(dtRow.Item("TotalContrib"))
    '            'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptPPF_contribution


    '        objRpt.SetDataSource(rpt_Data)


    '        Call ReportFunction.TextChange(objRpt, "txtAppendix", Language.getMessage(mstrModuleName, 15, "APPENDIX 5 "))
    '        Call ReportFunction.TextChange(objRpt, "txtPPFCont", Language.getMessage(mstrModuleName, 16, "PPF/CONT/01"))
    '        Call ReportFunction.TextChange(objRpt, "lblContribution", Language.getMessage(mstrModuleName, 117, "CONTRIBUTION FOR THE MONTH OF"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", "")
    '        Call ReportFunction.TextChange(objRpt, "lblEmployer", Language.getMessage(mstrModuleName, 18, "Name of Employer"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)

    '        objRpt.SetParameterValue("parPPFAddress", Language.getMessage(mstrModuleName, 19, "Director General PPF") & _
    '                                        vbCrLf & Language.getMessage(mstrModuleName, 20, "P.O Box. 72473") & _
    '                                        vbCrLf & Language.getMessage(mstrModuleName, 21, "Dar es Salaam, Tanzania") & _
    '                                        vbCrLf & Language.getMessage(mstrModuleName, 22, "Tel.: 2113919/22. 2110642") & _
    '                                        vbCrLf & Language.getMessage(mstrModuleName, 23, "Fax.: 2117772"))

    '        Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
    '        Call ReportFunction.TextChange(objRpt, "txtmemberhispname", Language.getMessage(mstrModuleName, 3, "Member Number"))
    '        Call ReportFunction.TextChange(objRpt, "txtNameinfull", Language.getMessage(mstrModuleName, 24, "Name in Full"))

    '        Call ReportFunction.TextChange(objRpt, "txtMonthlySalary", Language.getMessage(mstrModuleName, 25, "Monthly Salary"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberdetails", Language.getMessage(mstrModuleName, 26, "Members Contribution"))

    '        Call ReportFunction.TextChange(objRpt, "txtMemberRate", Language.getMessage(mstrModuleName, 27, "Rate"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberAmt", Language.getMessage(mstrModuleName, 28, "Amount"))

    '        Call ReportFunction.TextChange(objRpt, "txtEmployersdetails", Language.getMessage(mstrModuleName, 30, "Employer's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerRate", Language.getMessage(mstrModuleName, 27, "Rate"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerAmt", Language.getMessage(mstrModuleName, 28, "Amount"))

    '        Call ReportFunction.TextChange(objRpt, "txtTotalContribution", Language.getMessage(mstrModuleName, 29, "Total Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 30, "TOTAL"))

    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", mstrPeriodName)

    '        'Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 31, "Printed Date :"))
    '        'Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 32, "Printed By :"))
    '        'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 33, "Page :"))

    '        'Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        'Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        'Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
    '        'Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol41")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol61")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol81")
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol91")
    '        Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn6Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn8Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn9Total, GUI.fmtCurrency))
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sr. No.")
            Language.setMessage(mstrModuleName, 2, "Member Number")
            Language.setMessage(mstrModuleName, 3, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Employee Amount")
            Language.setMessage(mstrModuleName, 7, "Employer Amount")
            Language.setMessage(mstrModuleName, 8, "Order By :")
            Language.setMessage(mstrModuleName, 9, "APPENDIX 5")
            Language.setMessage(mstrModuleName, 10, "PPF/CONT/01")
            Language.setMessage(mstrModuleName, 12, "GEPF/CONT/01")
            Language.setMessage(mstrModuleName, 13, "Employee Code")
            Language.setMessage(mstrModuleName, 14, "CONTRIBUTION FOR THE MONTH OF")
            Language.setMessage(mstrModuleName, 15, "Name of Employer")
            Language.setMessage(mstrModuleName, 16, "Director General PPF")
            Language.setMessage(mstrModuleName, 17, "P.O Box. 72473")
            Language.setMessage(mstrModuleName, 18, "Dar es Salaam, Tanzania")
            Language.setMessage(mstrModuleName, 19, "Tel.: 2113919/22. 2110642")
            Language.setMessage(mstrModuleName, 20, "Fax.: 2117772")
            Language.setMessage(mstrModuleName, 21, "Name in Full")
            Language.setMessage(mstrModuleName, 22, "Monthly Salary")
            Language.setMessage(mstrModuleName, 23, "Members Contribution")
            Language.setMessage(mstrModuleName, 24, "Rate")
            Language.setMessage(mstrModuleName, 25, "Amount")
            Language.setMessage(mstrModuleName, 26, "Employer's Contribution")
            Language.setMessage(mstrModuleName, 27, "Total Contribution")
            Language.setMessage(mstrModuleName, 28, "TOTAL")
            Language.setMessage(mstrModuleName, 29, "Annexture  4")
            Language.setMessage(mstrModuleName, 30, "P.O BOX 4843")
            Language.setMessage(mstrModuleName, 31, "GOLDEN JUBILEE TOWERS")
            Language.setMessage(mstrModuleName, 32, "OHIO/KIBO STREET")
            Language.setMessage(mstrModuleName, 33, "TEL:+255 22 2120912/52, 2127375/76")
            Language.setMessage(mstrModuleName, 34, "Email: pspf@pspf-tz.org")
            Language.setMessage(mstrModuleName, 35, "Website:www.pspf-tz.org")
            Language.setMessage(mstrModuleName, 36, "DAR ES SALAAM")
            Language.setMessage(mstrModuleName, 37, "CONTRIBUTIONS REMITTANCE FORM")
            Language.setMessage(mstrModuleName, 38, "NAME OF THE CONTRIBUTING EMPLOYER :")
            Language.setMessage(mstrModuleName, 39, "Name")
            Language.setMessage(mstrModuleName, 40, "Check number (membership number)")
            Language.setMessage(mstrModuleName, 41, "Votecode (employer number)")
            Language.setMessage(mstrModuleName, 42, "ADDRESS :")
            Language.setMessage(mstrModuleName, 43, "LOCATION :")
            Language.setMessage(mstrModuleName, 44, "PHONE NUMBER :")
            Language.setMessage(mstrModuleName, 45, "CHEQUE NUMBER :")
            Language.setMessage(mstrModuleName, 46, "DATE :")
            Language.setMessage(mstrModuleName, 47, "AMOUNT :")
            Language.setMessage(mstrModuleName, 48, "NAME AND SIGNATURE :")
            Language.setMessage(mstrModuleName, 49, "Staff Name")
            Language.setMessage(mstrModuleName, 50, "Check No.")
            Language.setMessage(mstrModuleName, 51, "votecode")
            Language.setMessage(mstrModuleName, 52, "Member Amount")
            Language.setMessage(mstrModuleName, 53, "Employer Amount")
            Language.setMessage(mstrModuleName, 54, "PayDate")
            Language.setMessage(mstrModuleName, 55, "Basic Pay")
            Language.setMessage(mstrModuleName, 56, "Gross Pay")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
