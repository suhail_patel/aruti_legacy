﻿'Class Name : clsTrainingCalendarReport.vb
'Purpose    :
'Date       : 18-May-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsTrainingCalendarReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingCalendarReport"
    Private mstrReportId As String = enArutiReport.Training_Calendar_Report
    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintCalendarUnkid As Integer = 0
    Private mstrCalendarName As String = ""
    Private mintTrainingUnkid As Integer = 0
    Private mstrTrainingName As String = ""
    Private mdtDateFrom As Date = Nothing
    Private mdtDateTo As Date = Nothing

#End Region

#Region " Properties "

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _DateFrom() As Date
        Set(ByVal value As Date)
            mdtDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _DateTo() As Date
        Set(ByVal value As Date)
            mdtDateTo = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkid = 0
            mstrCalendarName = ""
            mintTrainingUnkid = 0
            mstrTrainingName = ""
            mdtDateFrom = Nothing
            mdtDateTo = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Calendar :") & " " & mstrCalendarName & " "
            End If

            If mintTrainingUnkid > 0 Then
                objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkid)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.trainingcourseunkid = @trainingcourseunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Training :") & " " & mstrTrainingName & " "
            End If

            If mdtDateFrom <> Nothing Then
                objDataOperation.AddParameter("@DateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDateFrom))
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.startdate >= @DateFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Date From :") & " " & mdtDateFrom.Date & " " 
            End If

            If mdtDateTo <> Nothing Then
                objDataOperation.AddParameter("@DateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDateTo))
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.enddate <= @DateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Date To") & " " & mdtDateTo.Date & " "
            End If

            Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "startdate"
            OrderByQuery = "trdepartmentaltrainingneed_master.startdate"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub
#End Region

#Region " Report Generation "
    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("SrNo", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Sr No")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TrainingName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Training Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TargetGroup", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Target Group")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            Dim dsAllocation As DataSet = (New clsMasterData).GetTrainingTargetedGroup("List", "", False, False)
            Dim dsStatus As DataSet = (New clsDepartmentaltrainingneed_master).getStatusComboList("List", True)


            StrQ &= "SELECT trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                     ", trdepartmentaltrainingneed_master.departmentunkid " & _
                     ", trdepartmentaltrainingneed_master.periodunkid " & _
                     ", ISNULL(trtraining_calendar_master.calendar_name, '') AS periodname " & _
                     ", trdepartmentaltrainingneed_master.competenceunkid " & _
                     ", trdepartmentaltrainingneed_master.trainingcategoryunkid " & _
                     ", ISNULL(trtrainingcategory_master.categoryname, '') AS trainingcategoryname " & _
                     ", trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                     ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                     ", trdepartmentaltrainingneed_master.learningmethodunkid " & _
                     ", ISNULL(trtrainingitemsinfo_master.info_name, '') AS learningmethodname " & _
                     ", trdepartmentaltrainingneed_master.targetedgroupunkid " & _
                     ", trdepartmentaltrainingneed_master.noofstaff " & _
                     ", trdepartmentaltrainingneed_master.startdate AS startdate " & _
                     ", trdepartmentaltrainingneed_master.enddate AS enddate " & _
                     ", trdepartmentaltrainingneed_master.trainingpriority " & _
                     ", ISNULL(trtrainingpriority_master.trpriority_name, '') AS trainingpriorityname " & _
                     ", trdepartmentaltrainingneed_master.trainingproviderunkid " & _
                     ", ISNULL(hrinstitute_master.institute_name, '') AS trainingprovidername " & _
                     ", trdepartmentaltrainingneed_master.trainingvenueunkid " & _
                     ", ISNULL(trtrainingvenue_master.venuename, '') AS trainingvenuename " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.other_competence, '') AS other_competence " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.other_trainingcourse, '') AS other_trainingcourse " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0) AS statusunkid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.issubmitforapproval, 0) AS issubmitforapproval " & _
                     ", trdepartmentaltrainingneed_master.remark " & _
                     ", trdepartmentaltrainingneed_master.iscertirequired " & _
                     ", trdepartmentaltrainingneed_master.totalcost " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.isactive, 1) AS isactive " & _
                     "/*, ISNULL(hrdepartment_master.name, '') as Department*/ " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.allocationid, 0) AS allocationid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.moduleid, 0) AS moduleid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.moduletranunkid, 0) AS moduletranunkid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.istrainingcostoptional, 0) AS istrainingcostoptional " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.insertformid, 0) AS insertformid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.refno, '') AS refno " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.approved_totalcost, trdepartmentaltrainingneed_master.totalcost) AS approved_totalcost " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0) AS request_statusunkid " & _
                     ", CASE ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) "

            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(CInt(dsRow.Item("Id"))) <= 0 Then
                    StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN @employeenames "
                Else
                    StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
                End If

            Next
            StrQ &= " END AS targetedgroupname " & _
                    ", CASE ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0) "

            For Each dsRow As DataRow In dsStatus.Tables(0).Rows
                StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
            Next
            StrQ &= " END AS statusname "

            StrQ &= ", 0 AS allocationtranunkid " & _
                        ", '' AS allocationtrancode " & _
                        ", '' AS allocationtranname "

            StrQ &= ", 0 AS depttrainingneedresourcestranunkid " & _
                    ", '' AS trainingresourcename " & _
                    ", 0 AS depttrainingneedfinancingsourcestranunkid " & _
                    ", '' AS financingsourcename " & _
                    ", 0 AS depttrainingneedtrainingcoordinatortranunkid " & _
                    ", '' AS coordinatorname " & _
                    ", 0 As depttrainingneedcostitemtranunkid " & _
                    ", '' AS info_name " & _
                       ", CAST(0 AS DECIMAL(36,6)) AS amount "

            StrQ &= "FROM trdepartmentaltrainingneed_master " & _
                    "LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                    "/*LEFT JOIN pdpgoals_master ON pdpgoals_master.pdpgoalsmstunkid = trdepartmentaltrainingneed_master.competenceunkid " & _
                            "AND pdpgoals_master.isvoid = 0 " & _
                    "LEFT JOIN pdpitemdatatran ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
                            "AND pdpitemdatatran.isvoid = 0*/ " & _
                    "LEFT JOIN cfcommon_master AS tcompetence ON tcompetence.masterunkid = trdepartmentaltrainingneed_master.competenceunkid " & _
                           "AND tcompetence.mastertype = " & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & " " & _
                           "AND tcompetence.isactive = 1 " & _
                    "LEFT JOIN trtrainingcategory_master ON trtrainingcategory_master.categoryunkid = trainingcategoryunkid " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                           "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "AND tcourse.isactive = 1 " & _
                    "LEFT JOIN trtrainingitemsinfo_master ON trtrainingitemsinfo_master.infounkid = trdepartmentaltrainingneed_master.learningmethodunkid " & _
                            "AND trtrainingitemsinfo_master.isactive = 1 " & _
                    "LEFT JOIN trtrainingpriority_master ON trtrainingpriority_master.trpriorityunkid = trdepartmentaltrainingneed_master.trainingpriority " & _
                    "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = trdepartmentaltrainingneed_master.trainingproviderunkid " & _
                            "AND hrinstitute_master.isactive = 1 " & _
                    "LEFT JOIN trtrainingvenue_master ON trtrainingvenue_master.venueunkid = trdepartmentaltrainingneed_master.trainingvenueunkid " & _
                            "AND trtrainingvenue_master.isactive = 1 " & _
                    "/*LEFT JOIN hrdepartment_master  ON hrdepartment_master.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid*/ "


            StrQ &= "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                      "AND trtraining_calendar_master.isactive = 1 " & _
                      "AND trtrainingcategory_master.isactive = 1 " & _
                      "AND trtrainingpriority_master.isactive = 1 " & _
                      "AND trdepartmentaltrainingneed_master.statusunkid = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            objDataOperation.AddParameter("@employeenames", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Employee Names"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strMasterUnkIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("departmentaltrainingneedunkid").ToString)).ToArray)
            If strMasterUnkIDs.Trim <> "" Then
                Dim objTAlloc As New clsDepttrainingneed_allocation_Tran
                objTAlloc._DataOp = objDataOperation
                Dim dsTAlloc As DataSet = objTAlloc.GetList("Alloc", -1, " AND trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                Dim objTEmp As New clsDepttrainingneed_employee_Tran
                objTEmp._DataOp = objDataOperation
                Dim dsTEmp As DataSet = objTEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Alloc", False, "", -1, " AND trdepttrainingneed_employee_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    Dim drT() As DataRow = dsTAlloc.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                    If drT.Length > 0 Then
                        Dim dt As DataTable = drT.CopyToDataTable
                        If dt.Rows.Count > 0 Then
                            Dim strAllocationTranName As String = String.Empty
                            For Each drAllocRow As DataRow In dt.Rows
                                strAllocationTranName &= "," & drAllocRow.Item("AllocationName").ToString
                            Next
                            If strAllocationTranName.Length > 0 Then
                                strAllocationTranName = strAllocationTranName.Substring(1)
                            End If
                            dsRow.Item("allocationtranname") = strAllocationTranName
                        End If

                    Else

                        drT = dsTEmp.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count > 0 Then
                                Dim strAllocationTranName As String = String.Empty
                                For Each drAllocRow As DataRow In dt.Rows
                                    strAllocationTranName &= "," & drAllocRow.Item("employeename").ToString
                                Next
                                If strAllocationTranName.Length > 0 Then
                                    strAllocationTranName = strAllocationTranName.Substring(1)
                                End If
                                dsRow.Item("allocationtranname") = strAllocationTranName
                            End If
                        End If
                    End If
                Next
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 1
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("SrNo") = intCount
                rpt_Row.Item("TrainingName") = drRow.Item("trainingcoursename")
                rpt_Row.Item("TargetGroup") = drRow.Item("allocationtranname")
                rpt_Row.Item("StartDate") = CDate(drRow.Item("startdate")).ToShortDateString
                rpt_Row.Item("EndDate") = CDate(drRow.Item("enddate")).ToShortDateString

                dtFinalTable.Rows.Add(rpt_Row)

                intCount = intCount + 1
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = {100, 100, 200, 100, 100}
            'ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            'For i As Integer = 0 To intArrayColumnWidth.Length - 1
            '    intArrayColumnWidth(i) = 125
            'Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Training Name")
			Language.setMessage(mstrModuleName, 2, "Target Group")
			Language.setMessage(mstrModuleName, 3, "Start Date")
			Language.setMessage(mstrModuleName, 4, "End Date")
			Language.setMessage(mstrModuleName, 5, "Sr No")
			Language.setMessage(mstrModuleName, 6, "Training :")
			Language.setMessage(mstrModuleName, 7, "Employee Names")
			Language.setMessage(mstrModuleName, 11, "Date To")
			Language.setMessage(mstrModuleName, 12, "Date From :")
			Language.setMessage(mstrModuleName, 15, "Prepared By :")
			Language.setMessage(mstrModuleName, 16, "Checked By :")
			Language.setMessage(mstrModuleName, 17, "Approved By")
			Language.setMessage(mstrModuleName, 18, "Received By :")
			Language.setMessage(mstrModuleName, 19, "Calendar :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
