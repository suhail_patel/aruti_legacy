﻿'Class Name : clsTrainingBudgetReportNew.vb
'Purpose    :
'Date       : 22-Nov-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsTrainingBudgetReportNew
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingBudgetReportNew"
    Private mstrReportId As String = enArutiReport.Training_Budget_Spreadsheet  '279
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintCalendarUnkId As Integer = 0
    Private mstrCalendarName As String = String.Empty
    Private mintTrainingUnkId As Integer = 0
    Private mstrTrainingName As String = String.Empty
    Private mblnIsIncludeZeroCost As Boolean = True
    Private mintTrainingNeedAllocationID As Integer = -1
    Private mstrTrainingNeedAllocationName As String = String.Empty

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#End Region

#Region " Properties "

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkId = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _IsIncludeZeroCost() As Boolean
        Set(ByVal value As Boolean)
            mblnIsIncludeZeroCost = value
        End Set
    End Property

    Public WriteOnly Property _TrainingNeedAllocationID() As Integer
        Set(ByVal value As Integer)
            mintTrainingNeedAllocationID = value
        End Set
    End Property

    Public WriteOnly Property _TrainingNeedAllocationName() As String
        Set(ByVal value As String)
            mstrTrainingNeedAllocationName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

#End Region

#Region "Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkId = 0
            mstrCalendarName = String.Empty
            mintTrainingUnkId = 0
            mstrTrainingName = String.Empty
            mintTrainingNeedAllocationID = 0
            mstrTrainingNeedAllocationName = String.Empty
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid
            objConfig._Companyunkid = xCompanyUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DisplayChart(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, True)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "generateReport", mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub FilterTitleAndFilterQuery()

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkId > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkId)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Calendar : ") & " " & mstrCalendarName & " "
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Allocation : ") & " " & mstrTrainingNeedAllocationName & " "

            If mintTrainingUnkId > 0 Then
                objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkId)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.trainingcourseunkid = @trainingcourseunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Training : ") & " " & mstrTrainingName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsCombo As DataSet = Nothing
        Dim dsList As New DataSet

        Dim objTItem As New clstrainingitemsInfo_master
        Try
            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("allocationtranname", System.Type.GetType("System.String"))
            dtCol.Caption = mstrTrainingNeedAllocationName
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("RefNo", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Ref No.")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Training", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Training")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TrainingNeedCategory", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Training Need Category")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Priority", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Priority")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Duration", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Duration")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TargetedStaff", System.Type.GetType("System.Int64"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Targeted Staff")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dsCombo = objTItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            If dsCombo.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsCombo.Tables(0).Rows
                    dtCol = New DataColumn("ColumnCost" & dtRow.Item("InfounkId"))
                    dtCol.DataType = System.Type.GetType("System.Decimal")
                    dtCol.Caption = dtRow.Item("Info_Name")
                    dtCol.DefaultValue = 0
                    dtFinalTable.Columns.Add(dtCol)
                Next
            End If

            dtCol = New DataColumn("TotalApprovedBudget", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Total Approved Budget")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dsList = GetQueryData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("allocationtranname") = drRow.Item("allocationtranname")
                rpt_Row.Item("RefNo") = drRow.Item("RefNo")
                rpt_Row.Item("Training") = drRow.Item("trainingcoursename")
                rpt_Row.Item("TrainingNeedCategory") = drRow.Item("trainingcategoryname")
                rpt_Row.Item("StartDate") = CDate(drRow.Item("startdate")).ToShortDateString
                rpt_Row.Item("EndDate") = CDate(drRow.Item("enddate")).ToShortDateString
                rpt_Row.Item("Priority") = drRow.Item("priority")
                rpt_Row.Item("Duration") = drRow.Item("Duration")
                rpt_Row.Item("TargetedStaff") = drRow.Item("noofstaff")

                For Each dr As DataRow In dsCombo.Tables(0).Rows
                    rpt_Row.Item("ColumnCost" & dr("infounkid")) = Format(drRow.Item("ColumnCost" & dr("infounkid")), GUI.fmtCurrency)
                Next

                rpt_Row.Item("TotalApprovedBudget") = Format(drRow.Item("TotalApprovedBudget"), GUI.fmtCurrency)

                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            intArrayColumnWidth(0) = 200
            For i As Integer = 1 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 100
            Next
            'Dim intArrayColumnWidth As Integer() = {200, 80, 125, 125, 80, 80, 100, 125, 100}
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader, rowsArrayFooter)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
            objTItem = Nothing
        End Try

    End Sub

    Public Function Generate_DisplayChart(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim dtCol As DataColumn
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            dsList = GetQueryData(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)

            Dim rpt_Row As DataRow = Nothing

            For Each dRow As DataRow In dsList.Tables(0).Rows
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dRow.Item("trainingcoursename")
                rpt_Row.Item("Column81") = dRow.Item("totalapprovedbudget")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingCostAnalysis

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription" _
                                        )

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 14, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 15, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 16, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 17, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 18, "Training Cost Analysis"))
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DisplayChart; Module Name: " & mstrModuleName)
        End Try

    End Function

    Public Function GetQueryData(ByVal xDatabaseName As String, _
                                 ByVal xUserUnkid As Integer, _
                                 ByVal xYearUnkid As Integer, _
                                 ByVal xCompanyUnkid As Integer, _
                                 ByVal xPeriodStart As Date, _
                                 ByVal xPeriodEnd As Date, _
                                 ByVal xUserModeSetting As String, _
                                 ByVal xOnlyApproved As Boolean, _
                                 ByVal xIncludeIn_ActiveEmployee As Boolean) As DataSet
        Dim dsList As New DataSet
        Dim dsCostList As New DataSet
        Dim dsCombo As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objTItem As New clstrainingitemsInfo_master
        Dim dtCol As DataColumn
        Try

            objDataOperation = New clsDataOperation

            StrQ &= " Select " & _
                    " departmentaltrainingneedunkid " & _
                    ", '' AS allocationtranname " & _
                    ", ISNULL(refno, '') AS refno " & _
                    ", ISNULL(trdepartmentaltrainingneed_master.trainingcourseunkid,0) AS trainingcourseunkid " & _
                    ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                    ", ISNULL(trtrainingcategory_master.categoryname, '') AS trainingcategoryname " & _
                    ", trdepartmentaltrainingneed_master.startdate AS startdate " & _
                    ", trdepartmentaltrainingneed_master.enddate AS enddate " & _
                    ", ISNULL(trtrainingpriority_master.trpriority_name,'') AS priority " & _
                    ", datediff(DAY,startdate,enddate) + 1 AS trainingdays" & _
                    ", '' AS duration " & _
                    ", ISNULL(trdepartmentaltrainingneed_master.noofstaff,0) AS noofstaff " & _
                    ", ISNULL(trdepartmentaltrainingneed_master.approved_totalcost,0) AS totalapprovedbudget "


            StrQ &= "FROM trdepartmentaltrainingneed_master " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                           "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "AND tcourse.isactive = 1 " & _
                    "LEFT JOIN trtrainingcategory_master  ON trtrainingcategory_master.categoryunkid = trdepartmentaltrainingneed_master.trainingcategoryunkid " & _
                           "AND trtrainingcategory_master.isactive = 1 " & _
                    "LEFT JOIN trtrainingpriority_master  ON trtrainingpriority_master.trpriorityunkid = trdepartmentaltrainingneed_master.trainingpriority " & _
                           "AND trtrainingpriority_master.isactive = 1 " & _
                    "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                    "AND trdepartmentaltrainingneed_master.statusunkid = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If mblnIsIncludeZeroCost = False Then
                StrQ &= " AND trdepartmentaltrainingneed_master.approved_totalcost > 0 "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            dsCombo = objTItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            If dsCombo.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsCombo.Tables(0).Rows
                    dtCol = New DataColumn("ColumnCost" & dtRow.Item("InfounkId"))
                    dtCol.DataType = System.Type.GetType("System.Decimal")
                    dtCol.Caption = dtRow.Item("Info_Name")
                    dtCol.DefaultValue = 0
                    dsList.Tables(0).Columns.Add(dtCol)
                Next
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT " & _
              "  trdepttrainingneed_costitem_tran.depttrainingneedcostitemtranunkid " & _
              ", trdepttrainingneed_costitem_tran.departmentaltrainingneedunkid " & _
              ", trdepttrainingneed_costitem_tran.costitemunkid " & _
              ", ISNULL(trtrainingitemsinfo_master.info_code, '') AS info_code " & _
              ", ISNULL(trtrainingitemsinfo_master.info_name, '') AS info_name " & _
              ", trdepttrainingneed_costitem_tran.amount " & _
              ", ISNULL(trdepttrainingneed_costitem_tran.approved_amount, 0) AS approved_amount " & _
             "FROM trdepttrainingneed_costitem_tran " & _
                "LEFT JOIN trtrainingitemsinfo_master ON trtrainingitemsinfo_master.infounkid = trdepttrainingneed_costitem_tran.costitemunkid " & _
             " WHERE trdepttrainingneed_costitem_tran.isvoid = 0 "

            dsCostList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim strMasterUnkIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("departmentaltrainingneedunkid").ToString)).ToArray)
            If strMasterUnkIDs.Trim <> "" Then
                Dim objTAlloc As New clsDepttrainingneed_allocation_Tran
                objTAlloc._DataOp = objDataOperation
                Dim dsTAlloc As DataSet = objTAlloc.GetList("Alloc", -1, " AND trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                Dim objTEmp As New clsDepttrainingneed_employee_Tran
                objTEmp._DataOp = objDataOperation
                Dim dsTEmp As DataSet = objTEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Alloc", False, "", -1, " AND trdepttrainingneed_employee_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    Dim drT() As DataRow = dsTAlloc.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                    If drT.Length > 0 Then
                        Dim dt As DataTable = drT.CopyToDataTable
                        If dt.Rows.Count > 0 Then
                            Dim strAllocationTranName As String = String.Empty
                            For Each drAllocRow As DataRow In dt.Rows
                                strAllocationTranName &= "," & drAllocRow.Item("AllocationName").ToString
                            Next
                            If strAllocationTranName.Length > 0 Then
                                strAllocationTranName = strAllocationTranName.Substring(1)
                            End If
                            dsRow.Item("allocationtranname") = strAllocationTranName
                        End If

                    Else

                        drT = dsTEmp.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count > 0 Then
                                Dim strAllocationTranName As String = String.Empty
                                For Each drAllocRow As DataRow In dt.Rows
                                    strAllocationTranName &= "," & drAllocRow.Item("employeename").ToString
                                Next
                                If strAllocationTranName.Length > 0 Then
                                    strAllocationTranName = strAllocationTranName.Substring(1)
                                End If
                                dsRow.Item("allocationtranname") = strAllocationTranName
                            End If
                        End If
                    End If

                    Dim intYears As Integer = Math.Floor(CInt(dsRow("trainingdays")) / 365)
                    Dim intMonths As Integer = Math.Floor((CInt(dsRow("trainingdays")) Mod 365) / 30)
                    Dim intDays As Integer = Math.Floor(((CInt(dsRow("trainingdays")) Mod 365) Mod 30))
                    dsRow("duration") = IIf(intYears > 0, intYears.ToString() & " Year(s) ", "").ToString & IIf(intMonths > 0, intMonths.ToString & " Month(s) ", "").ToString & IIf(intDays > 0, intDays.ToString & " Day(s)", "").ToString

                    Dim drCost() As DataRow = dsCostList.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                    If drCost.Length > 0 Then
                        For Each dr As DataRow In drCost
                            dsRow("ColumnCost" & dr("costitemunkid")) = dr("amount")
                        Next
                    End If
                Next
            End If
            dsList.Tables(0).AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQueryData; Module Name: " & mstrModuleName)
        Finally
            objTItem = Nothing
        End Try
        Return dsList
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Ref No.")
			Language.setMessage(mstrModuleName, 2, "Training")
			Language.setMessage(mstrModuleName, 3, "Training Need Category")
			Language.setMessage(mstrModuleName, 4, "Start Date")
			Language.setMessage(mstrModuleName, 5, "End Date")
			Language.setMessage(mstrModuleName, 6, "Priority")
			Language.setMessage(mstrModuleName, 7, "Duration")
			Language.setMessage(mstrModuleName, 8, "Targeted Staff")
			Language.setMessage(mstrModuleName, 9, "Total Approved Budget")
			Language.setMessage(mstrModuleName, 10, "Calendar :")
			Language.setMessage(mstrModuleName, 11, "Allocation :")
			Language.setMessage(mstrModuleName, 12, "Training :")
			Language.setMessage(mstrModuleName, 13, "Order By :")
			Language.setMessage(mstrModuleName, 14, "Prepared By :")
			Language.setMessage(mstrModuleName, 15, "Checked By :")
			Language.setMessage(mstrModuleName, 16, "Approved By :")
			Language.setMessage(mstrModuleName, 17, "Received By :")
			Language.setMessage(mstrModuleName, 18, "Training Cost Analysis")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
