﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmExecutedTrainingsReport
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExecutedTrainingsReport"
    Private objExecutedTrainingsReport As clsExecutedTrainingsReport

#End Region

#Region " Constructor "
    Public Sub New()
        objExecutedTrainingsReport = New clsExecutedTrainingsReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objExecutedTrainingsReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmExecutedTrainingsReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objExecutedTrainingsReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExecutedTrainingsReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExecutedTrainingsReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExecutedTrainingsReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExecutedTrainingsReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExecutedTrainingsReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExecutedTrainingsReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExecutedTrainingsReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExecutedTrainingsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsExecutedTrainingsReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master

        Try
            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCalendar = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            dtpDateFrom.Checked = False
            dtpDateTo.Checked = False
            cboTrainingName.SelectedIndex = 0

            objExecutedTrainingsReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Traininig Calendar."), enMsgBoxStyle.Information)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            objExecutedTrainingsReport.SetDefaultValue()

            objExecutedTrainingsReport._CalendarUnkid = cboTrainingCalendar.SelectedValue
            objExecutedTrainingsReport._CalendarName = cboTrainingCalendar.Text
            objExecutedTrainingsReport._TrainingUnkid = cboTrainingName.SelectedValue
            objExecutedTrainingsReport._TrainingName = cboTrainingName.Text
            If dtpDateFrom.Checked = True Then
                objExecutedTrainingsReport._DateFrom = dtpDateFrom.Value.Date
            End If
            If dtpDateTo.Checked = True Then
                objExecutedTrainingsReport._DateTo = dtpDateTo.Value.Date
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objExecutedTrainingsReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, False, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTrainingName.DataSource
            frm.ValueMember = cboTrainingName.ValueMember
            frm.DisplayMember = cboTrainingName.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTrainingName.SelectedValue = frm.SelectedValue
                cboTrainingName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Link Buttons Events"
    'Hemant (19 Oct 2021) -- Start
    'ENHANCEMENT : OLD-491 - Finca UG - Give link to Display chart separately.
    Private Sub lnkDisplayChart_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDisplayChart.LinkClicked

        Try
            If SetFilter() = False Then Exit Sub

            objExecutedTrainingsReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.Preview, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkDisplayChart_Click", mstrModuleName)
        Finally

        End Try
    End Sub
    'Hemant (19 Oct 2021) -- End
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblDateTo.Text = Language._Object.getCaption(Me.lblDateTo.Name, Me.lblDateTo.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.Name, Me.lblDateFrom.Text)
			Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.Name, Me.lblTrainingName.Text)
			Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.Name, Me.lblTrainingCalendar.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lnkDisplayChart.Text = Language._Object.getCaption(Me.lnkDisplayChart.Name, Me.lnkDisplayChart.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Traininig Calendar.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class