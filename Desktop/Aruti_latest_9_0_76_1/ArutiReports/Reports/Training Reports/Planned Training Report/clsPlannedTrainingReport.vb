﻿'************************************************************************************************************************************
'Class Name : clsPlannedTrainingReport.vb
'Purpose    :
'Date       : 21/05/2021
'Written By :Pinkal 
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsPlannedTrainingReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPlannedTrainingReport"
    Private mstrReportId As String = enArutiReport.Planned_Training_Report
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""

    Private mstrOrderByQuery As String = ""

#End Region

#Region " Properties "

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintStatusId = 0
            mstrStatusName = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""

        Try

            If mintPeriodId > 0 Then
                Me._FilterQuery &= "AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Period: ") & " " & mstrPeriodName & " "
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= "AND trdepartmentaltrainingneed_master.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Status: ") & " " & mstrStatusName & " "
            Else
                Me._FilterQuery &= "AND trdepartmentaltrainingneed_master.statusunkid IN (" & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & ", " & clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved & ") "
            End If

            If mdtStartDate <> Nothing Then
                Me._FilterQuery &= " AND @StartDate BETWEEN CONVERT(char(8),trdepartmentaltrainingneed_master.startdate,112) AND CONVERT(char(8),trdepartmentaltrainingneed_master.enddate,112) "
                objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Start Date: ") & " " & mdtStartDate.ToShortDateString() & " "
            End If

            If mdtEndDate <> Nothing Then
                Me._FilterQuery &= " AND @EndDate BETWEEN CONVERT(char(8),trdepartmentaltrainingneed_master.startdate,112) AND CONVERT(char(8),trdepartmentaltrainingneed_master.enddate,112) "
                objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "End Date: ") & " " & mdtEndDate.ToShortDateString() & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, " Order By : ") & " " & Me.OrderByDisplay

                mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END", Language.getMessage(mstrModuleName, 1, "Training Name")))
            iColumn_DetailReport.Add(New IColumn("trdepartmentaltrainingneed_master.startdate ", Language.getMessage(mstrModuleName, 2, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("trdepartmentaltrainingneed_master.enddate ", Language.getMessage(mstrModuleName, 3, "End Date")))
            iColumn_DetailReport.Add(New IColumn("trdepartmentaltrainingneed_master.statusunkid", Language.getMessage(mstrModuleName, 15, "Status")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As Date _
                                     , ByVal xPeriodEnd As Date _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try


            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            User._Object._Userunkid = xUserUnkid

            Dim dsStatus As DataSet = (New clsDepartmentaltrainingneed_master).getStatusComboList("List", False)

            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()


            StrQ = " SELECT DISTINCT " & _
                      " trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                      ", trdepartmentaltrainingneed_master.periodunkid " & _
                      ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') " & _
                      "  ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                      " , trdepartmentaltrainingneed_master.startdate " & _
                      " , trdepartmentaltrainingneed_master.enddate " & _
                      " , trdepartmentaltrainingneed_master.statusunkid " & _
                      " , CASE trdepartmentaltrainingneed_master.statusunkid "

            For Each dsRow As DataRow In dsStatus.Tables(0).Rows
                StrQ &= " WHEN " & CInt(dsRow.Item("Id")) & " THEN '" & dsRow.Item("Name").ToString & "' "
            Next
            StrQ &= " ELSE '' END AS statusname "

            StrQ &= " FROM trdepartmentaltrainingneed_master " & _
                      " LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                      " AND tcourse.isactive = 1 " & _
                      " WHERE trdepartmentaltrainingneed_master.isvoid = 0 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("trainingcoursename")
                rpt_Row.Item("Column2") = CDate(dtRow.Item("startdate")).ToShortDateString()
                rpt_Row.Item("Column3") = CDate(dtRow.Item("enddate")).ToShortDateString()
                rpt_Row.Item("Column4") = dtRow.Item("statusname")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next


            objRpt = New ArutiReport.Designer.rptPlannedTrainingReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                       ConfigParameter._Object._GetLeftMargin, _
                                       ConfigParameter._Object._GetRightMargin)




            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 8, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 9, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 10, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 11, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtTrainingName", Language.getMessage(mstrModuleName, 12, "Training Name"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 2, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 3, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 15, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 16, "Sr. No."))
            Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 14, "Total Count :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function



#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Training Name")
			Language.setMessage(mstrModuleName, 2, "Start Date")
			Language.setMessage(mstrModuleName, 3, "End Date")
			Language.setMessage(mstrModuleName, 4, "Period:")
			Language.setMessage(mstrModuleName, 5, "Status:")
			Language.setMessage(mstrModuleName, 6, "Start Date:")
			Language.setMessage(mstrModuleName, 7, "End Date:")
			Language.setMessage(mstrModuleName, 8, "Prepared By :")
			Language.setMessage(mstrModuleName, 9, "Checked By :")
			Language.setMessage(mstrModuleName, 10, "Approved By :")
			Language.setMessage(mstrModuleName, 11, "Received By :")
			Language.setMessage(mstrModuleName, 12, "Training Name")
			Language.setMessage(mstrModuleName, 13, " Order By :")
			Language.setMessage(mstrModuleName, 14, "Total Count :")
			Language.setMessage(mstrModuleName, 15, "Status")
			Language.setMessage(mstrModuleName, 16, "Sr. No.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
