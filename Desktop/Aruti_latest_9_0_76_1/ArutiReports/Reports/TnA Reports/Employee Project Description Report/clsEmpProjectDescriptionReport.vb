'************************************************************************************************************************************
'Class Name : clsEmpProjectDescriptionReport.vb
'Purpose    :
'Date       : 26/06/2020
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsEmpProjectDescriptionReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpProjectDescriptionReport"
    Private mstrReportId As String = enArutiReport.Employee_Project_Description_Report  '231
    Dim objDataOperation As clsDataOperation

#Region "Enum"
    Public Enum EnColumn
        Show_Donor = 1
        Show_Project = 2
        Show_Status = 3
        Show_HTML = 4
    End Enum
#End Region

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintPeriodID As Integer = 0
    Private mstrPeriodName As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployee As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatus As String = ""
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mdtEmployee As DataTable = Nothing
    Private mstrOrderByQuery As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

    Private mblnShowDonor As Boolean = True
    Private mblnShowProject As Boolean = True
    Private mblnShowStatus As Boolean = True
    Private mblnViewHTMLReport As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployee = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _dtEmployee() As DataTable
        Set(ByVal value As DataTable)
            mdtEmployee = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public Property _ShowDonort() As Boolean
        Get
            Return mblnShowDonor
        End Get
        Set(ByVal value As Boolean)
            mblnShowDonor = value
        End Set
    End Property

    Public Property _ShowProject() As Boolean
        Get
            Return mblnShowProject
        End Get
        Set(ByVal value As Boolean)
            mblnShowProject = value
        End Set
    End Property

    Public Property _ShowStatus() As Boolean
        Get
            Return mblnShowStatus
        End Get
        Set(ByVal value As Boolean)
            mblnShowStatus = value
        End Set
    End Property

    Public Property _ViewReportInHTML() As Boolean
        Get
            Return mblnViewHTMLReport
        End Get
        Set(ByVal value As Boolean)
            mblnViewHTMLReport = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodID = 0
            mstrPeriodName = ""
            mintEmployeeId = 0
            mstrEmployee = ""
            mintStatusId = 0
            mstrStatus = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mdtEmployee = Nothing
            mstrReport_GroupName = ""
            mblnShowDonor = True
            mblnShowProject = True
            mblnShowStatus = True
            mblnViewHTMLReport = False
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mintPeriodID > 0 Then
                Me._FilterQuery &= " AND ltbemployee_timesheet.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND ltbemployee_timesheet.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= " AND ltbemployee_timesheet.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
            End If

            'If Me.OrderByQuery <> "" Then
            '    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
            'End If

            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, ByVal blnIsFromEmailAttachment As Boolean)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid
            User._Object._Userunkid = xUserUnkid

            objDataOperation = New clsDataOperation



            If mdtEmployee Is Nothing OrElse mdtEmployee.Rows.Count <= 0 Then Exit Sub

            If System.IO.Directory.Exists(mstrExportReportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    mstrExportReportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            For count As Integer = 0 To mdtEmployee.Rows.Count - 1

                objDataOperation.ClearParameters()
                mintEmployeeId = CInt(mdtEmployee.Rows(count)("employeeunkid"))
                Dim mstrEmployeeCode As String = mdtEmployee.Rows(count)("employeecode").ToString()
                mstrEmployee = mdtEmployee.Rows(count)("employeename").ToString()

                StrQ = " SELECT " & _
                        " ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                        ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                        ",Convert(Char(8),ltbemployee_timesheet.activitydate,112) AS activitydate " & _
                        ", ISNULL(bgfundsource_master.fundname,'') AS fundname " & _
                        ", ISNULL(bgfundprojectcode_master.fundprojectcode,'') AS fundprojectcode " & _
                        ", ISNULL(bgfundprojectcode_master.fundprojectname,'') AS fundprojectname " & _
                        ", ISNULL(bgfundactivity_tran.activity_code,'') AS activity_code " & _
                        ", ISNULL(bgfundactivity_tran.activity_name,'') AS activity_name " & _
                        ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),CAST(ltbemployee_timesheet.activity_hrs AS INT) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( CAST(ltbemployee_timesheet.activity_hrs AS DECIMAL) % 60)), 2), '00:00') AS activity_hrs " & _
                        ",ltbemployee_timesheet.description " & _
                        ",ltbemployee_timesheet.statusunkid " & _
                        ",CASE WHEN ltbemployee_timesheet.statusunkid = 1 then @Approve " & _
                        "          WHEN ltbemployee_timesheet.statusunkid = 2 then @Pending " & _
                        "          WHEN ltbemployee_timesheet.statusunkid = 3 then @Reject " & _
                        "          WHEN ltbemployee_timesheet.statusunkid = 6 then @Cancel END as Status " & _
                        " FROM ltbemployee_timesheet " & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = ltbemployee_timesheet.employeeunkid " & _
                        " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = ltbemployee_timesheet.fundsourceunkid " & _
                        " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid =  ltbemployee_timesheet.projectcodeunkid " & _
                        " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = ltbemployee_timesheet.activityunkid "

                StrQ &= " WHERE ltbemployee_timesheet.isvoid = 0 "




                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= mstrOrderByQuery

                objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
                objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

                Dim strarrGroupColumns As String() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell
                Dim strBuilder As New StringBuilder

                Dim mdtTableExcel As DataTable = dsList.Tables(0).Copy()

                If mdtTableExcel.Columns.Contains("EmployeeCode") Then
                    mdtTableExcel.Columns.Remove("EmployeeCode")
                End If

                If mdtTableExcel.Columns.Contains("Employee") Then
                    mdtTableExcel.Columns.Remove("Employee")
                End If

                If mdtTableExcel.Columns.Contains("statusunkid") Then
                    mdtTableExcel.Columns.Remove("statusunkid")
                End If

                For Each drRow In mdtTableExcel.Rows
                    drRow("activitydate") = eZeeDate.convertDate(drRow("activitydate").ToString()).ToShortDateString()
                Next


                Dim objEmp As New clsEmployee_Master
                Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & _
                                                            clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Job

                Dim dsEmpList As DataSet = objEmp.GetListForDynamicField(StrCheck_Fields, xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate.Date, mdtEndDate.Date, _
                                                                                                       xUserModeSetting, True, True, "List", mintEmployeeId, False, "", False, False, False, True, Nothing, IIf(xUserUnkid > 0, True, False))

                objEmp = Nothing

                If mblnViewHTMLReport = False Then

                    row = New WorksheetRow()

                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Name:") & " " & mstrEmployee & _
                                                                           Space(30) & Language.getMessage(mstrModuleName, 2, "Employee No:") & " " & mstrEmployeeCode & _
                                                                           Space(30) & Language.getMessage(mstrModuleName, 3, "Position / Title:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("Job").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("Job").ToString) & _
                                                                           Space(30) & Language.getMessage(mstrModuleName, 4, "Month:") & " " & mstrPeriodName & _
                                                                           Space(30) & Language.getMessage(mstrModuleName, 5, "Location:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("Station").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("Station").ToString), "s10bw")


                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayHeader.Add(row)


                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)


                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)


                Else

                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD colspan=" & mdtTableExcel.Columns.Count - 1 & " style='font-size:13px;border-width: 0px;'><B>" & Language.getMessage(mstrModuleName, 1, "Name:") & " " & mstrEmployee & _
                                                                                                                                                                                                     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 2, "Employee No:") & " " & mstrEmployeeCode & _
                                                                                                                                                                                                     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 3, "Position / Title:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("Job").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("Job").ToString) & _
                                                                                                                                                                                                     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 4, "Month:") & " " & mstrPeriodName & _
                                                                                                                                                                                                     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 5, "Location:") & " " & IIf(dsEmpList.Tables(0).Rows(0)("Station").ToString.Length <= 0, "", dsEmpList.Tables(0).Rows(0)("Station").ToString) & _
                                                                                                                                                                                            "</B></TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)

                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)

                    strBuilder.Append("<TR>" & vbCrLf)
                    strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)


                    If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)

                End If

                '--------------------

                mdtTableExcel.Columns("activitydate").Caption = Language.getMessage(mstrModuleName, 6, "Activity Date")

                If mblnShowDonor = False Then
                    If mdtTableExcel.Columns.Contains("fundname") Then
                        mdtTableExcel.Columns.Remove("fundname")
                    End If
                Else
                    mdtTableExcel.Columns("fundname").Caption = Language.getMessage(mstrModuleName, 7, "Donor/Grant")
                End If

                If mblnShowProject = False Then
                    If mdtTableExcel.Columns.Contains("fundprojectcode") Then
                        mdtTableExcel.Columns.Remove("fundprojectcode")
                    End If
                    If mdtTableExcel.Columns.Contains("fundprojectname") Then
                        mdtTableExcel.Columns.Remove("fundprojectname")
                    End If
                Else
                    mdtTableExcel.Columns("fundprojectcode").Caption = Language.getMessage(mstrModuleName, 8, "Project Code")
                    mdtTableExcel.Columns("fundprojectname").Caption = Language.getMessage(mstrModuleName, 9, "Project Name")
                End If

                If mblnShowStatus = False Then
                    If mdtTableExcel.Columns.Contains("Status") Then
                        mdtTableExcel.Columns.Remove("Status")
                    End If
                Else
                    mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 14, "Status")
                End If

                mdtTableExcel.Columns("activity_code").Caption = Language.getMessage(mstrModuleName, 10, "Activity Code")
                mdtTableExcel.Columns("activity_name").Caption = Language.getMessage(mstrModuleName, 11, "Activity Name")
                mdtTableExcel.Columns("activity_hrs").Caption = Language.getMessage(mstrModuleName, 12, "Activity Hrs")
                mdtTableExcel.Columns("description").Caption = Language.getMessage(mstrModuleName, 13, "Description")

                'SET EXCEL CELL WIDTH  
                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    If i = 0 Then
                        intArrayColumnWidth(i) = 75
                    ElseIf i >= 1 AndAlso i < intArrayColumnWidth.Length - 3 Then
                        intArrayColumnWidth(i) = 150
                    ElseIf i = intArrayColumnWidth.Length - 3 Then
                        intArrayColumnWidth(i) = 75
                    ElseIf i = intArrayColumnWidth.Length - 2 Then
                        intArrayColumnWidth(i) = 150
                    ElseIf i = intArrayColumnWidth.Length - 1 Then
                        intArrayColumnWidth(i) = 75
                    End If
                Next
                'SET EXCEL CELL WIDTH


                If mstrEmployeeCode.Contains("/") Then
                    mstrEmployeeCode = mstrEmployeeCode.Replace("/", "-")
                ElseIf mstrEmployeeCode.Contains("\") Then
                    mstrEmployeeCode = mstrEmployeeCode.Replace("\", "-")
                End If

                Dim mblnPromptSavedMsg As Boolean = False
                If count = mdtEmployee.Rows.Count - 1 Then mblnPromptSavedMsg = True

                If mblnViewHTMLReport Then
                    Call ReportExecute(xCompanyUnkid, enArutiReport.Employee_Project_Description_Report, Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, False, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, True, mstrEmployeeCode & "_" & mstrEmployee & "_" & mstrPeriodName, blnIsFromEmailAttachment, True, mblnPromptSavedMsg)
                Else
                    Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, False, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, mstrEmployeeCode & "_" & mstrEmployee & "_" & mstrPeriodName, True, mblnPromptSavedMsg)
                End If
            Next

            mstrExportReportPath = ConfigParameter._Object._ExportReportPath

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Name:")
            Language.setMessage(mstrModuleName, 2, "Employee No:")
            Language.setMessage(mstrModuleName, 3, "Position / Title:")
            Language.setMessage(mstrModuleName, 4, "Month:")
            Language.setMessage(mstrModuleName, 5, "Location:")
            Language.setMessage(mstrModuleName, 6, "Activity Date")
            Language.setMessage(mstrModuleName, 7, "Donor/Grant")
            Language.setMessage(mstrModuleName, 8, "Project Code")
            Language.setMessage(mstrModuleName, 9, "Project Name")
            Language.setMessage(mstrModuleName, 10, "Activity Code")
            Language.setMessage(mstrModuleName, 11, "Activity Name")
            Language.setMessage(mstrModuleName, 12, "Activity Hrs")
            Language.setMessage(mstrModuleName, 13, "Description")
            Language.setMessage(mstrModuleName, 14, "Status")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


