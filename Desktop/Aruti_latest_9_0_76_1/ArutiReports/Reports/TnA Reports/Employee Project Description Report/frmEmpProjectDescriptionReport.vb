Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmEmpProjectDescriptionReport


#Region "Private Variables"

    Private mstrModuleName As String = "frmEmpProjectDescriptionReport"
    Dim objEmpProjectDescription As clsEmpProjectDescriptionReport
    Private mstrReport_GroupName As String = ""
    Private dvEmployee As DataView
    Dim mstrAdvanceFilter As String = ""
#End Region

#Region "Constructor"

    Public Sub New()
        objEmpProjectDescription = New clsEmpProjectDescriptionReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpProjectDescription.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date.Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            Dim objStatus As New clsMasterData
            dsList = objStatus.getLeaveStatusList("Status", ConfigParameter._Object._ApplicableLeaveStatus, False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid in (0,1,2,3)", "", DataViewRowState.CurrentRows).ToTable()
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            objStatus = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub FillEmployee(ByVal mdtStartDate As Date, ByVal mdtEndDate As Date)
        Try
            Dim objEmp As New clsEmployee_Master

            Dim dsList As DataSet = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                             mdtStartDate, mdtEndDate, _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter)

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            dvEmployee = dsList.Tables(0).DefaultView

            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "EmpCodeName"
            dgEmployee.DataSource = dvEmployee

            objEmp = Nothing

            Call objbtnReset.ShowResult(CStr(dgEmployee.RowCount))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub


    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Project_Description_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CType(dsRow.Item("headtypeid"), clsEmpProjectDescriptionReport.EnColumn)

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_Donor
                            chkShowDonor.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_Project
                            chkShowProject.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_Status
                            chkShowStatus.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case clsEmpProjectDescriptionReport.EnColumn.Show_HTML
                            chkShowReportInHtml.Checked = CBool(dsRow.Item("transactionheadid"))

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            FillCombo()
            cboPeriod.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            mstrAdvanceFilter = ""
            txtSearchEmp.Text = ""
            objchkSelectAll.Checked = False
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                cboPeriod.Focus()
                Return False
            End If

            objEmpProjectDescription.SetDefaultValue()

            objEmpProjectDescription._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpProjectDescription._PeriodName = cboPeriod.Text
            objEmpProjectDescription._StatusId = CInt(cboStatus.SelectedValue)
            objEmpProjectDescription._StatusName = cboStatus.Text

            If dvEmployee.Table().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True).Count > 0 Then
                objEmpProjectDescription._dtEmployee = dvEmployee.Table().AsEnumerable().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True).ToList().CopyToDataTable()
            Else
                objEmpProjectDescription._dtEmployee = dvEmployee.Table().Copy()
            End If


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objEmpProjectDescription._StartDate = objPeriod._TnA_StartDate.Date
            objEmpProjectDescription._EndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing

            objEmpProjectDescription._ShowDonort = chkShowDonor.Checked
            objEmpProjectDescription._ShowProject = chkShowProject.Checked
            objEmpProjectDescription._ShowStatus = chkShowStatus.Checked
            objEmpProjectDescription._ViewReportInHTML = chkShowReportInHtml.Checked

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            Dim mintCheckedCount As Integer = dvEmployee.Table().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).Count()
            If mintCheckedCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCheckedCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCheckedCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If
            dvEmployee.Table.AcceptChanges()
            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Form's Events"

    Private Sub frmEmpProjectDescriptionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpProjectDescription = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpProjectDescriptionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpProjectDescriptionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objEmpProjectDescription._ReportName
            eZeeHeader.Message = objEmpProjectDescription._ReportDesc
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpProjectDescriptionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpProjectDescriptionReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpProjectDescriptionReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpProjectDescriptionReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpProjectDescriptionReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpProjectDescriptionReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpProjectDescriptionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                cboPeriod.Focus()
                Exit Sub
            End If


            If SetFilter() = False Then Exit Sub
            Me.Cursor = Cursors.WaitCursor()
            objEmpProjectDescription.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdvanceFilter = ""
            objchkSelectAll.Checked = False
            txtSearchEmp.Text = ""
            cboPeriod_SelectedValueChanged(cboPeriod, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboPeriod
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboStatus
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchStatus_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSetting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            objUserDefRMode._Reportunkid = enArutiReport.Employee_Project_Description_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            For i As Integer = 1 To 4

                Select Case CType(i, clsEmpProjectDescriptionReport.EnColumn)

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_Donor
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_Donor
                        objUserDefRMode._EarningTranHeadIds = chkShowDonor.Checked

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_Project
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_Project
                        objUserDefRMode._EarningTranHeadIds = chkShowProject.Checked

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_Status
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_Status
                        objUserDefRMode._EarningTranHeadIds = chkShowStatus.Checked

                    Case clsEmpProjectDescriptionReport.EnColumn.Show_HTML
                        objUserDefRMode._Headtypeid = clsEmpProjectDescriptionReport.EnColumn.Show_HTML
                        objUserDefRMode._EarningTranHeadIds = chkShowReportInHtml.Checked

                End Select

                intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Project_Description_Report, 0, 0, objUserDefRMode._Headtypeid)
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    mblnFlag = objUserDefRMode.Insert()
                Else
                    mblnFlag = objUserDefRMode.Update()
                End If

            Next

            If mblnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Selection Saved Successfully."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(objUserDefRMode._Message)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSetting_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "EmpCodeName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
            cboPeriod_SelectedValueChanged(cboPeriod, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboPeriod_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedValueChanged
        Try
            Dim mdtStartDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
            Dim mdtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPeriod._TnA_StartDate.Date
                mdtEndDate = objPeriod._TnA_EndDate.Date
                objPeriod = Nothing
            End If
            FillEmployee(mdtStartDate, mdtEndDate)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "WebMessge"
    Private Sub WebMessage()
        Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeDetails.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeDetails.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSettings.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSettings.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.chkShowReportInHtml.Text = Language._Object.getCaption(Me.chkShowReportInHtml.Name, Me.chkShowReportInHtml.Text)
			Me.btnSaveSettings.Text = Language._Object.getCaption(Me.btnSaveSettings.Name, Me.btnSaveSettings.Text)
			Me.LblStatus.Text = Language._Object.getCaption(Me.LblStatus.Name, Me.LblStatus.Text)
			Me.gbEmployeeDetails.Text = Language._Object.getCaption(Me.gbEmployeeDetails.Name, Me.gbEmployeeDetails.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.chkShowStatus.Text = Language._Object.getCaption(Me.chkShowStatus.Name, Me.chkShowStatus.Text)
			Me.chkShowProject.Text = Language._Object.getCaption(Me.chkShowProject.Name, Me.chkShowProject.Text)
			Me.chkShowDonor.Text = Language._Object.getCaption(Me.chkShowDonor.Name, Me.chkShowDonor.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select period.")
			Language.setMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
			Language.setMessage(mstrModuleName, 3, "Selection Saved Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
