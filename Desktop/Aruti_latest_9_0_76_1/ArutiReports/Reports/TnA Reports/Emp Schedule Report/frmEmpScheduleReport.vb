'************************************************************************************************************************************
'Class Name : frmEmpScheduleReport.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpScheduleReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpScheduleReport"
    Private objSchedule As clsEmpScheduleReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        'Pinkal (21-AUG-2014) -- Start
        'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT. 
        '_Show_AdvanceFilter = True
        'Pinkal (21-AUG-2014) -- End
        objSchedule = New clsEmpScheduleReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objSchedule.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            Dim objDepartment As New clsDepartment
            dsList = objDepartment.getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objDepartment = Nothing


            Dim objSection As New clsSections
            dsList = objSection.getComboList("List", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objSection = Nothing

            'Pinkal (30-Oct-2017) -- End


            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objMaster.getYearList("List")
            With cboYear
                .DisplayMember = dsList.Tables("List").Columns(0).ColumnName
                .DataSource = dsList.Tables("List")
                .SelectedIndex = cboYear.FindStringExact(CStr(Now.Date.Year))
            End With


            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            If ConfigParameter._Object._PolicyManagementTNA Then
            dsList = objPolicy.getListForCombo("List", True)
            With cboPolicy
                .ValueMember = "policyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            Else
                cboPolicy.Enabled = False
            End If
            'Pinkal (30-Oct-2017) -- End

            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                '.Items.Add(Language.getMessage(mstrModuleName, 1, "Shift Schedule "))
                '.Items.Add(Language.getMessage(mstrModuleName, 2, "Policy Schedule"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Shift Schedule Period Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Shift Schedule Date Wise"))
                If ConfigParameter._Object._PolicyManagementTNA Then
                    .Items.Add(Language.getMessage(mstrModuleName, 6, "Policy Schedule Period Wise"))
                    .Items.Add(Language.getMessage(mstrModuleName, 7, "Policy Schedule Date Wise"))
                End If
                'Pinkal (30-Oct-2017) -- End
                .SelectedIndex = 0
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing : objShift = Nothing
            objPolicy = Nothing : objMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboPolicy.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            cboShift.SelectedValue = 0
            cboYear.SelectedIndex = cboYear.FindStringExact(CStr(Now.Date.Year))
            mstrAdvanceFilter = ""
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""

            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            cboDepartment.SelectedIndex = 0
            cboSection.SelectedIndex = 0
            'Pinkal (30-Oct-2017) -- End

            'Pinkal (08-Oct-2015) -- Start
            'Enhancement - WORKING ON DAY OFF OPTION FOR POWERSOFT.
            chkShowDayOFF.Checked = False
            'Pinkal (08-Oct-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSchedule.SetDefaultValue()


            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            If CInt(cboReportType.SelectedIndex) = 0 OrElse CInt(cboReportType.SelectedIndex) = 2 Then  'SHIFT & POLICY PERIOD WISE
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Return False
                End If
            ElseIf CInt(cboReportType.SelectedIndex) = 1 OrElse CInt(cboReportType.SelectedIndex) = 3 Then   'SHIFT & POLICY DATE WISE

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003221 {PAPAYE}.
                'If CInt(cboDepartment.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Department is mandatory information. Please select department to continue."), enMsgBoxStyle.Information)
                '    cboDepartment.Focus()
                '    Return False
                'ElseIf CInt(cboSection.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Section is mandatory information. Please select section to continue."), enMsgBoxStyle.Information)
                '    cboSection.Focus()
                '    Return False
                'End If
                Dim objCfGroup As New clsGroup_Master(True)
                If objCfGroup._Groupname.ToUpper <> "PAPAYE FAST FOODS LTD" Then
                If CInt(cboDepartment.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Department is mandatory information. Please select department to continue."), enMsgBoxStyle.Information)
                    cboDepartment.Focus()
                Return False
                ElseIf CInt(cboSection.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Section is mandatory information. Please select section to continue."), enMsgBoxStyle.Information)
                    cboSection.Focus()
                    Return False
                End If
            End If
                objSchedule._CompanyGroupName = objCfGroup._Groupname.ToUpper
                'S.SANDEEP |29-MAR-2019| -- END
            End If
            'Pinkal (30-Oct-2017) -- End

            objSchedule._Advance_Filter = mstrAdvanceFilter
            objSchedule._ViewByIds = mstrStringIds
            objSchedule._ViewIndex = mintViewIdx
            objSchedule._ViewByName = mstrStringName
            objSchedule._Analysis_Fields = mstrAnalysis_Fields
            objSchedule._Analysis_Join = mstrAnalysis_Join
            objSchedule._Report_GroupName = mstrReport_GroupName
            objSchedule._EmployeeId = cboEmployee.SelectedValue
            objSchedule._EmployeeName = cboEmployee.Text
            objSchedule._IncludeInactive = chkInactiveemp.Checked
            objSchedule._PeriodId = cboPeriod.SelectedValue
            objSchedule._PeriodName = cboPeriod.Text
            objSchedule._PolicyId = cboPolicy.SelectedValue
            objSchedule._PolicyName = cboPolicy.Name
            objSchedule._ReportTypeId = cboReportType.SelectedIndex
            objSchedule._ReportTypeName = cboReportType.Text
            objSchedule._ShiftId = cboShift.SelectedValue
            objSchedule._ShiftName = cboShift.Text
            objSchedule._YearName = cboYear.Text

            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            If CInt(cboReportType.SelectedIndex) = 1 OrElse CInt(cboReportType.SelectedIndex) = 3 Then
                objSchedule._FromDate = dtpFromDate.Value.Date
                objSchedule._ToDate = dtpToDate.Value.Date
            End If
            objSchedule._DepartmentId = CInt(cboDepartment.SelectedValue)
            objSchedule._Department = cboDepartment.Text
            objSchedule._SectionId = CInt(cboSection.SelectedValue)
            objSchedule._Section = cboSection.Text
            'Pinkal (30-Oct-2017) -- End


            'Pinkal (08-Oct-2015) -- Start
            'Enhancement - WORKING ON DAY OFF OPTION FOR POWERSOFT.
            objSchedule._ShowDayOFF = chkShowDayOFF.Checked
            'Pinkal (08-Oct-2015) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (30-Oct-2017) -- Start
    'Enhancement - Ref id 15 Working on Employee Scheduled Report.

    Private Sub EnableDisablePeriod(ByVal blnEnable As Boolean)
        Try
            Dim mdtDate As DateTime = ConfigParameter._Object._CurrentDateAndTime.Date
            cboPeriod.SelectedIndex = 0 : cboPeriod.Enabled = blnEnable
            dtpFromDate.Value = mdtDate : dtpToDate.Value = mdtDate
            dtpFromDate.Enabled = Not blnEnable : dtpToDate.Enabled = Not blnEnable
            cboDepartment.Enabled = Not blnEnable : objbtnSearchDepartment.Enabled = Not blnEnable : cboDepartment.SelectedIndex = 0
            cboSection.Enabled = Not blnEnable : objbtnSearchSection.Enabled = Not blnEnable : cboSection.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisablePeriod", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (30-Oct-2017) -- End


#End Region

#Region " Forms "

    Private Sub frmEmpScheduleReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSchedule = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpScheduleReport_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmpScheduleReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Pinkal (21-AUG-2014) -- Start
            'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT. 
            'Me._Title = objSchedule._ReportName
            'Me._Message = objSchedule._ReportDesc
            eZeeHeader.Title = objSchedule._ReportName
            eZeeHeader.Message = objSchedule._ReportDesc
            'Pinkal (21-AUG-2014) -- End
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpScheduleReport_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (21-AUG-2014) -- Start
    'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT. 

    'Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    '    Try
    '        If e.Control Then
    '            If e.KeyCode = Windows.Forms.Keys.E Then
    '                Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (21-AUG-2014) -- End

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    'Pinkal (21-AUG-2014) -- Start
    'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT. 


    'Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs)
    '    Try
    '        If Not SetFilter() Then Exit Sub
    '        objSchedule.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs)
    '    Try
    '        If Not SetFilter() Then Exit Sub
    '        objSchedule.generateReport(cboReportType.SelectedIndex, enPrintAction.None, e.Type)
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Call ResetValue()
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.Close()
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmAdvanceSearch
    '    Try
    '        frm._Hr_EmployeeTable_Alias = "hremployee_master"
    '        frm.ShowDialog()
    '        mstrAdvanceFilter = frm._GetFilterString
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objSchedule.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDurationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpScheduleReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (21-AUG-2014) -- End

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

  
    'Pinkal (30-Oct-2017) -- Start
    'Enhancement - Ref id 15 Working on Employee Scheduled Report.

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click, objbtnSearchPolicy.Click _
                                                                                                                                                                   , objbtnSearchDepartment.Click, objbtnSearchSection.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            Dim cboCombo As ComboBox = Nothing

            If CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchDepartment.Name.ToUpper Then
                cboCombo = cboDepartment
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchSection.Name.ToUpper Then
                cboCombo = cboSection
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchShift.Name.ToUpper Then
                cboCombo = cboShift
            ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper() = objbtnSearchPolicy.Name.ToUpper Then
                cboCombo = cboPolicy
            End If

            dtList = CType(cboCombo.DataSource, DataTable)
            With cboCombo
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (30-Oct-2017) -- End


#End Region

#Region " LinkButton Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try

            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.

            Select Case cboReportType.SelectedIndex
                'Case 0  'SHIFT SCHEDULE
                '    cboPolicy.Enabled = False : cboPolicy.SelectedValue = 0
                '    cboShift.Enabled = True : cboShift.SelectedValue = 0
                'Case 1  'POLICY SCHEDULE
                '    cboPolicy.Enabled = True : cboPolicy.SelectedValue = 0
                '    cboShift.Enabled = False : cboShift.SelectedValue = 0


                Case 0  'SHIFT SCHEDULE PERIOD WISE
                    cboPolicy.Enabled = False : cboPolicy.SelectedValue = 0
                    cboShift.Enabled = True : cboShift.SelectedValue = 0
                    objbtnSearchShift.Enabled = True : objbtnSearchPolicy.Enabled = False
                    EnableDisablePeriod(True)

                Case 1  'SHIFT SCHEDULE DATE WISE
                    cboPolicy.Enabled = False : cboPolicy.SelectedValue = 0
                    cboShift.Enabled = True : cboShift.SelectedValue = 0
                    objbtnSearchShift.Enabled = True : objbtnSearchPolicy.Enabled = False
                    EnableDisablePeriod(False)

                Case 2  'POLICY SCHEDULE PERIOD WISE
                    cboPolicy.Enabled = True : cboPolicy.SelectedValue = 0
                    cboShift.Enabled = False : cboShift.SelectedValue = 0
                    objbtnSearchShift.Enabled = False : objbtnSearchPolicy.Enabled = True
                    EnableDisablePeriod(True)

                Case 3  'POLICY SCHEDULE DATE WISE
                    cboPolicy.Enabled = True : cboPolicy.SelectedValue = 0
                    cboShift.Enabled = False : cboShift.SelectedValue = 0
                    objbtnSearchShift.Enabled = False : objbtnSearchPolicy.Enabled = True
                    EnableDisablePeriod(False)

            End Select


            'Pinkal (30-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Pinkal (30-Oct-2017) -- Start
    'Enhancement - Ref id 15 Working on Employee Scheduled Report.

#Region "Datepicker Events"

    Private Sub dtpFromDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.ValueChanged
        Try
            dtpToDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpToDate.MinDate = dtpFromDate.Value.Date
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpFromDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (30-Oct-2017) -- End









	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblPolicy.Text = Language._Object.getCaption(Me.lblPolicy.Name, Me.lblPolicy.Text)
			Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblRType.Text = Language._Object.getCaption(Me.lblRType.Name, Me.lblRType.Text)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
            Me.LblSection.Text = Language._Object.getCaption(Me.LblSection.Name, Me.LblSection.Text)
            Me.LblDepartment.Text = Language._Object.getCaption(Me.LblDepartment.Name, Me.LblDepartment.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Sorry, Period is mandatory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 4, "Shift Schedule Period Wise")
            Language.setMessage(mstrModuleName, 5, "Shift Schedule Date Wise")
            Language.setMessage(mstrModuleName, 6, "Policy Schedule Period Wise")
            Language.setMessage(mstrModuleName, 7, "Policy Schedule Date Wise")
            Language.setMessage(mstrModuleName, 8, "Sorry, Department is mandatory information. Please select department to continue.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Section is mandatory information. Please select section to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

  
   
   
 
 
End Class
