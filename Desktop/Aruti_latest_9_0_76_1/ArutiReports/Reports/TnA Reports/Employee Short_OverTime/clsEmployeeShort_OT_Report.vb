'************************************************************************************************************************************
'Class Name : clsEmployeeTimesheetReport.vb
'Purpose    :
'Date       :24/01/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsEmployeeShort_OT_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeShort_OT_Report"
    Private mstrReportId As String = enArutiReport.EmployeeShortTime_OverTime
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintFromhrs As Integer = 0
    Private mintTohrs As Integer = 0
    Private mstrOrderByQuery As String = ""
    Private mblnIsActive As Boolean = True
    Private mintShiftunkid As Integer = 0
    Private mstrShiftName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty


    'Pinkal (30-Sep-2015) -- Start
    'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
    Private mintCountryID As Integer = 0
    Private mintPeriodID As Integer = 0
    Private mstrPeriodName As String = ""
    Private mdtPYPeriodStartDate As Date = Nothing
    Private mdtPYPeriodEndDate As Date = Nothing
    Private mblnEachEmployeeOnPage As Boolean = True
    'Pinkal (30-Sep-2015) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public Property _Fromhrs() As Integer
        Get
            Return mintFromhrs
        End Get
        Set(ByVal value As Integer)
            mintFromhrs = value
        End Set
    End Property

    Public Property _Tohrs() As Integer
        Get
            Return mintTohrs
        End Get
        Set(ByVal value As Integer)
            mintTohrs = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive()
        Set(ByVal value)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _Shiftunkid() As Integer
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property


    'Pinkal (30-Sep-2015) -- Start
    'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.

    Public WriteOnly Property _CountryID() As Integer
        Set(ByVal value As Integer)
            mintCountryID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodID() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PYPeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPYPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PYPeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPYPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ShowEachEmployeeOnNewPage() As Boolean
        Set(ByVal value As Boolean)
            mblnEachEmployeeOnPage = value
        End Set
    End Property

    'Pinkal (30-Sep-2015) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmpId = 0
            mintFromhrs = 0
            mintTohrs = 0
            mstrEmpName = ""
            mstrOrderByQuery = ""
            mblnIsActive = True
            mintShiftunkid = 0
            mstrShiftName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAdvance_Filter = ""

            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            mintCountryID = 0
            mblnEachEmployeeOnPage = True
            mdtPYPeriodStartDate = Nothing
            mdtPYPeriodEndDate = Nothing
            'Pinkal (30-Sep-2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, " From Date: ") & " " & mdtFromDate.ToShortDateString & " "

            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " To Date: ") & " " & mdtToDate.ToShortDateString & " "

            If mintFromhrs > 0 Then

                If mintReportTypeId = 1 Then  ' FOR SHORT HOURS
                    Me._FilterQuery &= " AND tnalogin_summary.total_short_hrs >= @Fromhrs "
                ElseIf mintReportTypeId = 2 Then  ' FOR OVER TIME 
                    Me._FilterQuery &= " AND tnalogin_summary.total_overtime >= @Fromhrs "
                End If
                objDataOperation.AddParameter("@Fromhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromhrs)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " From Hrs: ") & " " & CalculateTime(mintFromhrs) & " "
            End If

            If mintTohrs > 0 Then

                If mintReportTypeId = 1 Then   ' FOR SHORT HOURS
                    Me._FilterQuery &= " AND tnalogin_summary.total_short_hrs <= @ToHrs "
                ElseIf mintReportTypeId = 2 Then   ' FOR OVER TIME 
                    Me._FilterQuery &= " AND tnalogin_summary.total_overtime <= @ToHrs "
                End If

                objDataOperation.AddParameter("@ToHrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTohrs)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " To Hrs: ") & " " & CalculateTime(mintTohrs) & " "
            End If

            If mintEmpId > 0 Then
                Me._FilterQuery &= " AND tnalogin_summary.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, " Employee : ") & " " & mstrEmpName & " "
            End If
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintShiftunkid > 0 Then
                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)
                Me._FilterQuery &= " AND tnashift_master.shiftunkid = @ShiftId"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, " Shift : ") & " " & mstrShiftName & " "
            End If


            'Pinkal (30-Sep-2015) -- Start
            'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.
            If mintCountryID = 162 Then ' Oman
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, " Period : ") & " " & mstrPeriodName & " "
            End If
            'Pinkal (30-Sep-2015) -- End

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCountryID <> 162 Then 'Omann 
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Else
                objRpt = Generate_Oman_ShortHourDetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("login_date", Language.getMessage(mstrModuleName, 2, "Date")))
            iColumn_DetailReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 1, "Employee")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()


            StrQ = " SELECT tnalogin_summary.employeeunkid,employeecode,ISNULL(firstname,'') + ' ' + ISNULL(surname,'') as employee," & _
                       " CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) as login_date,'' holiday,  " & _
                       "  CASE WHEN total_hrs = total_overtime THEN '00:00' ELSE RIGHT('00' + CONVERT(VARCHAR(max),total_hrs/3600),2) + ':' +  RIGHT('00' + CONVERT(VARCHAR(2),(total_hrs%3600)/60),2) END AS workhrs,  logintran.breakhr,"

            If mintReportTypeId = 1 Then '  FOR SHORT HOUR

                StrQ &= " RIGHT('00' + CONVERT(VARCHAR(max),total_short_hrs/3600) ,2)+ ':' +  " & _
                             " RIGHT('00' + CONVERT(VARCHAR(2),(total_short_hrs%3600)/60),2)  AS shorthrs, "


                'Pinkal (09-Jan-2013) -- Start
                'Enhancement : TRA Changes

                'StrQFilter = " AND  t.total_overtime = 0 "
                StrQFilter = " AND  t.total_overtime = 0 AND total_short_hrs > 0 "

                'Pinkal (09-Jan-2013) -- End



            ElseIf mintReportTypeId = 2 Then  'FOR OVER TIME

                StrQ &= " RIGHT('00' + CONVERT(VARCHAR(max),total_overtime/3600),2) + ':' +  " & _
                             " RIGHT('00' + CONVERT(VARCHAR(2),(total_overtime%3600)/60),2)  AS overtime, "


                'Pinkal (09-Jan-2013) -- Start
                'Enhancement : TRA Changes

                'StrQFilter = " AND  t.total_short_hrs = 0 "
                StrQFilter = " AND  t.total_short_hrs = 0 AND total_overtime > 0 "

                'Pinkal (09-Jan-2013) -- End



            End If


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            StrQ &= " RIGHT('00' + CONVERT(VARCHAR(max),total_nighthrs/3600),2) + ':' +   RIGHT('00' + CONVERT(VARCHAR(2),(total_nighthrs%3600)/60),2)  AS Nighthrs , " & _
                         " CONVERT(VARCHAR(max),(SELECT SUM(t.total_hrs) /3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & " )) + ':'  " & _
                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_hrs) %3600)/ 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & "  )),2)  AS TotWorkhrs, " & _
                         "  CONVERT(VARCHAR(max),(SELECT SUM(t.total_short_hrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' +  " & _
                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_short_hrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotShorthrs,   " & _
                         " CONVERT(VARCHAR(max),(SELECT SUM(t.total_overtime)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' +   " & _
                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_overtime)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotExtrahrs, " & _
                         " CONVERT(VARCHAR(max),(SELECT SUM(t.total_nighthrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & " )) + ':' +   " & _
                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_nighthrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & " )),2)  AS TotNighthrs "


            ' " CONVERT(VARCHAR(max),(SELECT (SUM(t.total_hrs) - SUM(t.total_overtime))/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & " )) + ':'  " & _
            ' " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT ((SUM(t.total_hrs) - SUM(t.total_overtime)) %3600/ 60) FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & "  )),2)  AS TotWorkhrs, " & _

            'Pinkal (09-Jan-2013) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM tnalogin_summary " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                         " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid " & _
                         " JOIN ( " & _
                                        " SELECT employeeunkid,logindate,RIGHT('00' + CONVERT(VARCHAR(max),SUM(breakhr)/3600),2) + ':' +     " & _
                                        " RIGHT('00' + CONVERT(VARCHAR(2),SUM(breakhr%3600)/60),2) as breakhr " & _
                                        " FROM tnalogin_tran WHERE CONVERT(VARCHAR(8),tnalogin_tran.logindate,112) BETWEEN @FromDate AND @ToDate " & _
                                        " GROUP BY logindate,employeeunkid ) as logintran " & _
                       " ON logintran.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) = CONVERT(VARCHAR(8),logintran.logindate,112) "



            'Pinkal (11-Jul-2016) -- Start
            'Enhancement - Changed in Query 
            '" JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
            'Pinkal (11-Jul-2016) -- End


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Shani(24-Aug-2015) -- End

            StrQ &= " WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) BETWEEN @FromDate AND @ToDate "


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            If mintReportTypeId = 1 Then  '  FOR SHORT HOUR
                'StrQ &= " AND  total_overtime = 0 "
                StrQ &= " AND  total_overtime = 0  AND total_short_hrs > 0"
            ElseIf mintReportTypeId = 2 Then 'FOR OVER TIME
                'StrQ &= " AND  total_short_hrs = 0 "
                StrQ &= " AND  total_short_hrs = 0  AND total_overtime  > 0"
            End If

            'Pinkal (09-Jan-2013) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (27-Feb-2013) -- Start
            ''Enhancement : TRA Changes
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''Pinkal (27-Feb-2013) -- End

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim mStrHoliday As String = GetHolidayName(CInt(dtRow.Item("employeeunkid")), dtRow.Item("login_date").ToString)

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employee")
                rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString
                rpt_Row.Item("Column5") = dtRow.Item("breakhr")
                rpt_Row.Item("Column6") = dtRow.Item("workhrs")

                If mintReportTypeId = 1 Then 'FOR SHORT HOUR
                    rpt_Row.Item("Column7") = dtRow.Item("shorthrs")
                ElseIf mintReportTypeId = 2 Then 'FOR OVER TIME
                    rpt_Row.Item("Column7") = dtRow.Item("overtime")
                End If

                rpt_Row.Item("Column8") = mStrHoliday
                rpt_Row.Item("Column9") = dtRow.Item("TotWorkhrs")


                If mintReportTypeId = 1 Then  '  FOR SHORT HOUR
                    rpt_Row.Item("Column10") = dtRow.Item("TotShorthrs")
                ElseIf mintReportTypeId = 2 Then 'FOR OVER TIME
                    rpt_Row.Item("Column10") = dtRow.Item("TotExtrahrs")
                End If

                rpt_Row.Item("Column11") = dtRow.Item("Nighthrs")
                rpt_Row.Item("Column12") = dtRow.Item("TotNighthrs")
                rpt_Row.Item("Column13") = dtRow.Item("GName")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeShort_Overtime

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If



            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 19, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 20, "Code : "))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 2, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtBreakhrs", Language.getMessage(mstrModuleName, 3, "Break Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtWorkinghrs", Language.getMessage(mstrModuleName, 4, "Working Hrs"))

            If mintReportTypeId = 1 Then  '  FOR SHORT HOUR
                Call ReportFunction.TextChange(objRpt, "txthrs", Language.getMessage(mstrModuleName, 5, "Short Hrs"))
            ElseIf mintReportTypeId = 2 Then 'FOR OVER TIME
                Call ReportFunction.TextChange(objRpt, "txthrs", Language.getMessage(mstrModuleName, 8, "Over Time"))
            End If

            Call ReportFunction.TextChange(objRpt, "txtNighthrs", Language.getMessage(mstrModuleName, 25, "Night Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtholiday", Language.getMessage(mstrModuleName, 6, "Holiday"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))
            Call ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 7, "Total :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (30-Sep-2015) -- Start
    'Enhancement - WORKING ON VOLTAMP NEW SHORT HOUR REPORT.

    Private Function Generate_Oman_ShortHourDetailReport(ByVal strDatabaseName As String _
                                                                                      , ByVal intUserUnkid As Integer _
                                                                                      , ByVal intYearUnkid As Integer _
                                                                                      , ByVal intCompanyUnkid As Integer _
                                                                                      , ByVal dtPeriodStart As Date _
                                                                                      , ByVal dtPeriodEnd As Date _
                                                                                      , ByVal strUserModeSetting As String _
                                                                                      , ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.ClearParameters()


            StrQ = " SELECT tnalogin_summary.employeeunkid,employeecode " & _
                       ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') as employee" & _
                       ", CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) as login_date " & _
                       ", checkintime " & _
                       ", checkouttime " & _
                       ", logintran.breaksec " & _
                       ", logintran.breakhr " & _
                       ", ISNULL(tnashift_master.shiftcode, '') AS shiftcode " & _
                       ", ISNULL(tnashift_master.shiftname, '') AS shiftname " & _
                       ", tnapolicy_tran.ltcoming_grace " & _
                       ", RIGHT('00' + CONVERT(VARCHAR(max),ISNULL(tnapolicy_tran.ltcoming_grace,0)/3600) ,2)+ ':' +   RIGHT('00' + CONVERT(VARCHAR(2),(ISNULL(tnapolicy_tran.ltcoming_grace,0)%3600)/60),2) AS ltcoming_gracehrs " & _
                       ", tnalogin_summary.total_hrs As total_hrsec " & _
                       ", CASE WHEN total_hrs = total_overtime THEN '00:00' ELSE RIGHT('00' + CONVERT(VARCHAR(max),total_hrs/3600),2) + ':' +  RIGHT('00' + CONVERT(VARCHAR(2),(total_hrs%3600)/60),2) END AS workhrs "



            If mintReportTypeId = 1 Then '  FOR SHORT HOUR
                StrQ &= ",total_short_hrs, RIGHT('00' + CONVERT(VARCHAR(max),total_short_hrs/3600) ,2)+ ':' +   RIGHT('00' + CONVERT(VARCHAR(2),(total_short_hrs%3600)/60),2)  AS shorthrs, "
                StrQFilter = " AND  t.total_overtime = 0 AND total_short_hrs > 0 "
            End If

            StrQ &= " CONVERT(VARCHAR(max),(SELECT SUM(t.total_hrs) /3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & " )) + ':'  " & _
                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_hrs) %3600)/ 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate " & StrQFilter & "  )),2)  AS TotWorkhrs " & _
                         ",  CONVERT(VARCHAR(max),(SELECT SUM(t.total_short_hrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' +  " & _
                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_short_hrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotShorthrs   " & _
                         ", 0.00 as basic " & _
                         ", 0.00 as amount " & _
                         ", 0.00 as allowance " & _
                         ", 0.00 as Total_allowance "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            'Pinkal (27-Dec-2021)-- Start
            'Voltamp Report is not generating for one employee[Employee Code :1156] 
            '" JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_master.shiftunkid " & _
            'Pinkal (27-Dec-2021) -- End


            StrQ &= " FROM tnalogin_summary " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                         " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid " & _
                         " JOIN tnapolicy_master ON tnapolicy_master.policyunkid = tnalogin_summary.policyunkid " & _
                         " JOIN tnapolicy_tran ON tnapolicy_tran.policyunkid = tnapolicy_master.policyunkid AND tnapolicy_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                         " JOIN ( " & _
                                        " SELECT employeeunkid,logindate,SUM(tnalogin_tran.breakhr) AS breaksec, RIGHT('00' + CONVERT(VARCHAR(max),SUM(breakhr)/3600),2) + ':' +     " & _
                                        " RIGHT('00' + CONVERT(VARCHAR(2),SUM(breakhr%3600)/60),2) as breakhr " & _
                                        ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') " & _
                                        "            WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') <>  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') THEN   " & _
                                        "            ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') + ' *'   ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') END checkintime " & _
                                        ", CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') " & _
                                        "            WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  <> 	ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') THEN " & _
                                        "            ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') + ' *'   ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') END checkouttime " & _
                                        " FROM tnalogin_tran WHERE isvoid = 0 AND CONVERT(VARCHAR(8),tnalogin_tran.logindate,112) BETWEEN @FromDate AND @ToDate " & _
                                        " GROUP BY logindate,employeeunkid ) as logintran " & _
                       " ON logintran.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) = CONVERT(VARCHAR(8),logintran.logindate,112) "

            'Pinkal (13-Aug-2019) -- 'Defect [0004083:- VOLTAMP] -  Issues with the Short Hours Report. [Include isvoid = 0]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) BETWEEN @FromDate AND @ToDate "


            If mintReportTypeId = 1 Then  '  FOR SHORT HOUR
                StrQ &= " AND  total_overtime = 0  AND total_short_hrs > 0"
            End If


            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim mStrHoliday As String = GetHolidayName(CInt(dtRow.Item("employeeunkid")), dtRow.Item("login_date").ToString)

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                Dim objED As New clsEarningDeduction

                '================================START TO GET BASIC PAY OF EACH EMPLOYEE FOR THE END DATE OF PAYROLL PERIOD=============================

                'Pinkal (22-Feb-2021) -- Start
                'Bug On Voltamp  -   Working Oman Short Hour Detail Report as ED Method changed since 2019 due to that it is giving error.


                'Dim dsBasicPay As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, mdtFromDate, mdtToDate _
                '                                                                  , strUserModeSetting, blnOnlyApproved, False, "List", True, dtRow("employeeunkid").ToString(), "", 0, 3, 0 _
                '                                                                  , "prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others _
                '                                                                  , mdtPYPeriodEndDate.Date, -1, Nothing)

                Dim dsBasicPay As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, mdtFromDate, mdtToDate _
                                                                                  , strUserModeSetting, blnOnlyApproved, False, "List", True, dtRow("employeeunkid").ToString(), "", 0, 3, 0 _
                                                                                 , "", mdtPYPeriodEndDate.Date, -1, Nothing, False, "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others)

                'Pinkal (22-Feb-2021) -- End


                If dsBasicPay IsNot Nothing AndAlso dsBasicPay.Tables(0).Rows.Count > 0 Then
                    dtRow.Item("basic") = CDec(dsBasicPay.Tables(0).Rows(0)("amount"))
                End If
                dsBasicPay.Clear()
                dsBasicPay = Nothing

                '================================END TO GET BASIC PAY OF EACH EMPLOYEE FOR THE END DATE OF PAYROLL PERIOD=================================

                '================================START TO GET ALLOWANCES OF EACH EMPLOYEE FOR THE END DATE OF PAYROLL PERIOD=============================


                'Pinkal (22-Feb-2021) -- Start
                'Bug On Voltamp  -   Working Oman Short Hour Detail Report as ED Method changed since 2019 due to that it is giving error.
                'Dim dsOtherAllowance As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, mdtFromDate, mdtToDate _
                '                                                                  , strUserModeSetting, blnOnlyApproved, False, "List", True, dtRow("employeeunkid").ToString(), "", 0, 3, 0 _
                '                                                                  , "prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Informational & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others _
                '                                                                  , mdtPYPeriodEndDate.Date, -1, Nothing)
                Dim dsOtherAllowance As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, mdtFromDate, mdtToDate _
                                                                                  , strUserModeSetting, blnOnlyApproved, False, "List", True, dtRow("employeeunkid").ToString(), "", 0, 3, 0 _
                                                                                  , "", mdtPYPeriodEndDate.Date, -1, Nothing, False, "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Informational & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others)
                'Pinkal (22-Feb-2021) -- End


                If dsOtherAllowance IsNot Nothing AndAlso dsOtherAllowance.Tables(0).Rows.Count > 0 Then
                    dtRow.Item("allowance") = CDec(dsOtherAllowance.Tables(0).Compute("SUM(amount)", "1=1"))
                End If
                dsOtherAllowance.Clear()
                dsOtherAllowance = Nothing

                objED = Nothing
                '================================END TO GET ALLOWANCES OF EACH EMPLOYEE FOR THE END DATE OF PAYROLL PERIOD=============================


                '================================START TO GET TOTAL ALLOWANCE  OF EACH EMPLOYEE =============================
                dtRow.Item("Total_allowance") = CDec(dtRow.Item("basic")) + CDec(dtRow.Item("allowance"))
                '================================START TO GET TOTAL ALLOWANCE OF EACH EMPLOYEE =============================

                '================================START TO GET AMOUNT  OF EACH EMPLOYEE =============================
                dtRow.Item("amount") = CDec(dtRow.Item("total_short_hrs") / 3600) * CDec(CDec(dtRow.Item("Total_allowance")) / DateDiff(DateInterval.Day, mdtPYPeriodStartDate.Date, mdtPYPeriodEndDate.Date.AddDays(1))) / 9
                '================================START TO GET AMOUNT OF EACH EMPLOYEE =============================


                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employee")
                rpt_Row.Item("Column4") = WeekdayName(Weekday(eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString), True, FirstDayOfWeek.Sunday) & "  " & eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString
                rpt_Row.Item("Column5") = dtRow.Item("checkintime").ToString
                rpt_Row.Item("Column6") = dtRow.Item("checkouttime").ToString
                rpt_Row.Item("Column7") = dtRow.Item("breakhr")
                rpt_Row.Item("Column8") = dtRow.Item("ltcoming_gracehrs")
                rpt_Row.Item("Column9") = dtRow.Item("workhrs")
                rpt_Row.Item("Column10") = dtRow.Item("shorthrs")
                rpt_Row.Item("Column11") = Format(dtRow.Item("amount"), GUI.fmtCurrency)
                rpt_Row.Item("Column12") = Format(dtRow.Item("basic"), GUI.fmtCurrency)
                rpt_Row.Item("Column13") = Format(dtRow.Item("allowance"), GUI.fmtCurrency)
                rpt_Row.Item("Column14") = dtRow.Item("TotShorthrs")
                rpt_Row.Item("Column15") = dtRow.Item("GName")
                rpt_Row.Item("Column26") = dtRow.Item("shiftcode")

                '---------------------------------------------------------------START EMPLOYEE WISE TOTAL------------------------------------------------------------------------
                Dim mstrEmpCode As String = dtRow("employeecode").ToString

                rpt_Row.Item("Column16") = CalculateTime((From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode Select CInt(p.Item("breaksec"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column17") = CalculateTime((From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode Select CInt(p.Item("ltcoming_grace"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column18") = CalculateTime((From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode Select CInt(p.Item("total_hrsec"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column19") = CalculateTime((From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode Select CInt(p.Item("total_short_hrs"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column20") = Format((From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode Select CDec(p.Item("amount"))).DefaultIfEmpty.Sum(), GUI.fmtCurrency)

                '---------------------------------------------------------------END EMPLOYEE WISE TOTAL------------------------------------------------------------------------


                '---------------------------------------------------------------START TOTAL------------------------------------------------------------------------
                rpt_Row.Item("Column21") = CalculateTime((From p In dsList.Tables("DataTable") Select CInt(p.Item("breaksec"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column22") = CalculateTime((From p In dsList.Tables("DataTable") Select CInt(p.Item("ltcoming_grace"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column23") = CalculateTime((From p In dsList.Tables("DataTable") Select CInt(p.Item("total_hrsec"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column24") = CalculateTime((From p In dsList.Tables("DataTable") Select CInt(p.Item("total_short_hrs"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                rpt_Row.Item("Column25") = Format((From p In dsList.Tables("DataTable") Select CDec(p.Item("amount"))).DefaultIfEmpty.Sum(), GUI.fmtCurrency)
                '---------------------------------------------------------------END  TOTAL------------------------------------------------------------------------


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptOman_Employeeshorthrs

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column15}) <> {ArutiTable.Column15} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 19, "Code : "))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 17, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 2, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 26, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 27, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtBreakhrs", Language.getMessage(mstrModuleName, 3, "Break Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtWorkinghrs", Language.getMessage(mstrModuleName, 4, "Working Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtLTcomingGrace", Language.getMessage(mstrModuleName, 28, "Late Coming Grace"))
            Call ReportFunction.TextChange(objRpt, "txtshortHRs", Language.getMessage(mstrModuleName, 5, "Short Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtamount", Language.getMessage(mstrModuleName, 29, "Amount"))
            Call ReportFunction.TextChange(objRpt, "txtBasic", Language.getMessage(mstrModuleName, 30, "Basic :"))
            Call ReportFunction.TextChange(objRpt, "txtTotallowance", Language.getMessage(mstrModuleName, 31, "Other Allowance :"))
            Call ReportFunction.TextChange(objRpt, "txtshift", Language.getMessage(mstrModuleName, 34, "Shift"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 7, "Total :"))
            Call ReportFunction.TextChange(objRpt, "txtTotal1", Language.getMessage(mstrModuleName, 7, "Total :"))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Oman_ShortHourDetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (30-Sep-2015) -- End


    Public Function GetHolidayName(ByVal employeeunkid As Integer, ByVal login_date As String) As String

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try

            Dim objDataOperation As New clsDataOperation

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            objDataOperation.AddParameter("@login_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, login_date)

            StrQ = "SELECT CASE WHEN ISNULL(b.leavename,'')<>'' THEN isnull(b.leavename,'') ELSE isnull(a.holidayname,'') END AS holiday  FROM " & _
                    "(SELECT holidayname,employeeunkid,holidaydate FROM lvemployee_holiday " & _
                    "JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                    "WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),holidaydate,112) = @login_date) as A " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "SELECT leavename,employeeunkid,leavedate FROM dbo.lvleaveIssue_tran " & _
                    "JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                    "JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                    "WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),leavedate,112) = @login_date " & _
                    ") AS B ON b.employeeunkid = a.employeeunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("holiday").ToString
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHolidayName; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function CalculateTime(ByVal intSecond As Integer) As Decimal
        Dim calctime As Decimal = 0
        Dim calcMinute As Decimal = 0
        Dim calMinute As Decimal = 0
        Try
            calcMinute = intSecond / 60
            calMinute = calcMinute Mod 60
            Dim calHour As Decimal = calcMinute / 60
            Return CDec(CDec(Int(calHour) + (calMinute / 100)))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateTime" & mstrModuleName)
        End Try
        Return calctime
    End Function




#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee")
            Language.setMessage(mstrModuleName, 2, "Date")
            Language.setMessage(mstrModuleName, 3, "Break Hrs")
            Language.setMessage(mstrModuleName, 4, "Working Hrs")
            Language.setMessage(mstrModuleName, 5, "Short Hrs")
            Language.setMessage(mstrModuleName, 6, "Holiday")
            Language.setMessage(mstrModuleName, 7, "Total :")
            Language.setMessage(mstrModuleName, 8, "Over Time")
            Language.setMessage(mstrModuleName, 10, "Printed By :")
            Language.setMessage(mstrModuleName, 11, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Page :")
            Language.setMessage(mstrModuleName, 13, " From Date:")
            Language.setMessage(mstrModuleName, 14, " To Date:")
            Language.setMessage(mstrModuleName, 15, " From Hrs:")
            Language.setMessage(mstrModuleName, 16, " To Hrs:")
            Language.setMessage(mstrModuleName, 17, " Employee :")
            Language.setMessage(mstrModuleName, 18, " Order By :")
            Language.setMessage(mstrModuleName, 19, "Employee :")
            Language.setMessage(mstrModuleName, 20, "Code :")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Checked By :")
            Language.setMessage(mstrModuleName, 23, "Approved By :")
            Language.setMessage(mstrModuleName, 24, "Received By :")
            Language.setMessage(mstrModuleName, 25, "Night Hrs")
            Language.setMessage(mstrModuleName, 26, "In Time")
            Language.setMessage(mstrModuleName, 27, "Out Time")
            Language.setMessage(mstrModuleName, 28, "Late Coming Grace")
            Language.setMessage(mstrModuleName, 29, "Amount")
            Language.setMessage(mstrModuleName, 30, "Basic :")
            Language.setMessage(mstrModuleName, 31, "Other Allowance :")
            Language.setMessage(mstrModuleName, 32, " Period :")
            Language.setMessage(mstrModuleName, 33, " Shift :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
