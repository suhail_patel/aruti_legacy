﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeShort_OT_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowEachEmpOnNewPage = New System.Windows.Forms.CheckBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.LblShift = New System.Windows.Forms.Label
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.pnlFromToDate = New System.Windows.Forms.Panel
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.lblToDate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.txtFromhour = New eZee.TextBox.NumericTextBox
        Me.txtTohour = New eZee.TextBox.NumericTextBox
        Me.lblToHour = New System.Windows.Forms.Label
        Me.lblFromHrs = New System.Windows.Forms.Label
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.pnlTnAPeriod = New System.Windows.Forms.Panel
        Me.LblDuration = New System.Windows.Forms.Label
        Me.objTnAPeriod = New System.Windows.Forms.Label
        Me.objBtnSearchTnAPeriod = New eZee.Common.eZeeGradientButton
        Me.LblTnAPeriod = New System.Windows.Forms.Label
        Me.cboTnAPeriod = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlFromToDate.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.pnlTnAPeriod.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEachEmpOnNewPage)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.LblShift)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchShift)
        Me.gbFilterCriteria.Controls.Add(Me.cboShift)
        Me.gbFilterCriteria.Controls.Add(Me.pnlFromToDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.txtFromhour)
        Me.gbFilterCriteria.Controls.Add(Me.txtTohour)
        Me.gbFilterCriteria.Controls.Add(Me.lblToHour)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromHrs)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(390, 254)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowEachEmpOnNewPage
        '
        Me.chkShowEachEmpOnNewPage.Checked = True
        Me.chkShowEachEmpOnNewPage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowEachEmpOnNewPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEachEmpOnNewPage.Location = New System.Drawing.Point(96, 230)
        Me.chkShowEachEmpOnNewPage.Name = "chkShowEachEmpOnNewPage"
        Me.chkShowEachEmpOnNewPage.Size = New System.Drawing.Size(281, 18)
        Me.chkShowEachEmpOnNewPage.TabIndex = 214
        Me.chkShowEachEmpOnNewPage.Text = "Show Each Employee On New Page"
        Me.chkShowEachEmpOnNewPage.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(293, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 23
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblShift
        '
        Me.LblShift.BackColor = System.Drawing.Color.Transparent
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(8, 124)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(80, 13)
        Me.LblShift.TabIndex = 208
        Me.LblShift.Text = "Shift"
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(356, 119)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 210
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 120
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(96, 120)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(254, 21)
        Me.cboShift.TabIndex = 209
        '
        'pnlFromToDate
        '
        Me.pnlFromToDate.Controls.Add(Me.dtpToDate)
        Me.pnlFromToDate.Controls.Add(Me.dtpFromDate)
        Me.pnlFromToDate.Controls.Add(Me.lblFromDate)
        Me.pnlFromToDate.Controls.Add(Me.lblToDate)
        Me.pnlFromToDate.Location = New System.Drawing.Point(4, 64)
        Me.pnlFromToDate.Name = "pnlFromToDate"
        Me.pnlFromToDate.Size = New System.Drawing.Size(377, 29)
        Me.pnlFromToDate.TabIndex = 18
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(269, 5)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpToDate.TabIndex = 176
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(92, 5)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpFromDate.TabIndex = 152
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(5, 9)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(80, 13)
        Me.lblFromDate.TabIndex = 151
        Me.lblFromDate.Text = "From Date"
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(201, 9)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(62, 13)
        Me.lblToDate.TabIndex = 175
        Me.lblToDate.Text = "To Date"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(356, 147)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 206
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(96, 206)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(254, 18)
        Me.chkInactiveemp.TabIndex = 203
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'txtFromhour
        '
        Me.txtFromhour.AllowNegative = True
        Me.txtFromhour.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFromhour.DigitsInGroup = 0
        Me.txtFromhour.Flags = 0
        Me.txtFromhour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFromhour.Location = New System.Drawing.Point(96, 177)
        Me.txtFromhour.MaxDecimalPlaces = 4
        Me.txtFromhour.MaxWholeDigits = 9
        Me.txtFromhour.Name = "txtFromhour"
        Me.txtFromhour.Prefix = ""
        Me.txtFromhour.RangeMax = 1.7976931348623157E+308
        Me.txtFromhour.RangeMin = -1.7976931348623157E+308
        Me.txtFromhour.Size = New System.Drawing.Size(89, 21)
        Me.txtFromhour.TabIndex = 185
        Me.txtFromhour.Text = "0"
        '
        'txtTohour
        '
        Me.txtTohour.AllowNegative = True
        Me.txtTohour.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTohour.DigitsInGroup = 0
        Me.txtTohour.Flags = 0
        Me.txtTohour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTohour.Location = New System.Drawing.Point(268, 177)
        Me.txtTohour.MaxDecimalPlaces = 4
        Me.txtTohour.MaxWholeDigits = 9
        Me.txtTohour.Name = "txtTohour"
        Me.txtTohour.Prefix = ""
        Me.txtTohour.RangeMax = 1.7976931348623157E+308
        Me.txtTohour.RangeMin = -1.7976931348623157E+308
        Me.txtTohour.Size = New System.Drawing.Size(82, 21)
        Me.txtTohour.TabIndex = 186
        Me.txtTohour.Text = "0"
        '
        'lblToHour
        '
        Me.lblToHour.BackColor = System.Drawing.Color.Transparent
        Me.lblToHour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToHour.Location = New System.Drawing.Point(199, 181)
        Me.lblToHour.Name = "lblToHour"
        Me.lblToHour.Size = New System.Drawing.Size(60, 13)
        Me.lblToHour.TabIndex = 182
        Me.lblToHour.Text = "To Hour"
        '
        'lblFromHrs
        '
        Me.lblFromHrs.BackColor = System.Drawing.Color.Transparent
        Me.lblFromHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromHrs.Location = New System.Drawing.Point(8, 181)
        Me.lblFromHrs.Name = "lblFromHrs"
        Me.lblFromHrs.Size = New System.Drawing.Size(80, 13)
        Me.lblFromHrs.TabIndex = 180
        Me.lblFromHrs.Text = "From Hour"
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(80, 13)
        Me.lblReportType.TabIndex = 178
        Me.lblReportType.Text = "Report Type"
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 120
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(96, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(281, 21)
        Me.cboReportType.TabIndex = 179
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(13, 55)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(361, 9)
        Me.EZeeLine1.TabIndex = 16
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 152)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(80, 13)
        Me.lblEmpName.TabIndex = 167
        Me.lblEmpName.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(96, 148)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(255, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(9, 326)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(390, 63)
        Me.gbSortBy.TabIndex = 17
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(336, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(254, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'pnlTnAPeriod
        '
        Me.pnlTnAPeriod.Controls.Add(Me.LblDuration)
        Me.pnlTnAPeriod.Controls.Add(Me.objTnAPeriod)
        Me.pnlTnAPeriod.Controls.Add(Me.objBtnSearchTnAPeriod)
        Me.pnlTnAPeriod.Controls.Add(Me.LblTnAPeriod)
        Me.pnlTnAPeriod.Controls.Add(Me.cboTnAPeriod)
        Me.pnlTnAPeriod.Location = New System.Drawing.Point(16, 130)
        Me.pnlTnAPeriod.Name = "pnlTnAPeriod"
        Me.pnlTnAPeriod.Size = New System.Drawing.Size(377, 53)
        Me.pnlTnAPeriod.TabIndex = 18
        '
        'LblDuration
        '
        Me.LblDuration.BackColor = System.Drawing.Color.Transparent
        Me.LblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDuration.Location = New System.Drawing.Point(5, 31)
        Me.LblDuration.Name = "LblDuration"
        Me.LblDuration.Size = New System.Drawing.Size(80, 17)
        Me.LblDuration.TabIndex = 214
        Me.LblDuration.Text = "Duration"
        '
        'objTnAPeriod
        '
        Me.objTnAPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objTnAPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTnAPeriod.Location = New System.Drawing.Point(94, 31)
        Me.objTnAPeriod.Name = "objTnAPeriod"
        Me.objTnAPeriod.Size = New System.Drawing.Size(276, 17)
        Me.objTnAPeriod.TabIndex = 212
        Me.objTnAPeriod.Text = "#TnAValue"
        '
        'objBtnSearchTnAPeriod
        '
        Me.objBtnSearchTnAPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objBtnSearchTnAPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnSearchTnAPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnSearchTnAPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnSearchTnAPeriod.BorderSelected = False
        Me.objBtnSearchTnAPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnSearchTnAPeriod.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objBtnSearchTnAPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnSearchTnAPeriod.Location = New System.Drawing.Point(353, 4)
        Me.objBtnSearchTnAPeriod.Name = "objBtnSearchTnAPeriod"
        Me.objBtnSearchTnAPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objBtnSearchTnAPeriod.TabIndex = 213
        '
        'LblTnAPeriod
        '
        Me.LblTnAPeriod.BackColor = System.Drawing.Color.Transparent
        Me.LblTnAPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTnAPeriod.Location = New System.Drawing.Point(5, 6)
        Me.LblTnAPeriod.Name = "LblTnAPeriod"
        Me.LblTnAPeriod.Size = New System.Drawing.Size(80, 17)
        Me.LblTnAPeriod.TabIndex = 212
        Me.LblTnAPeriod.Text = "Period"
        '
        'cboTnAPeriod
        '
        Me.cboTnAPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTnAPeriod.DropDownWidth = 120
        Me.cboTnAPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTnAPeriod.FormattingEnabled = True
        Me.cboTnAPeriod.Location = New System.Drawing.Point(92, 4)
        Me.cboTnAPeriod.Name = "cboTnAPeriod"
        Me.cboTnAPeriod.Size = New System.Drawing.Size(254, 21)
        Me.cboTnAPeriod.TabIndex = 212
        '
        'frmEmployeeShort_OT_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.pnlTnAPeriod)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeShort_OT_Report"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Short Hours Report"
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.pnlTnAPeriod, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.pnlFromToDate.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.pnlTnAPeriod.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblEmpName As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents lblFromDate As System.Windows.Forms.Label
    Public WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Public WithEvents lblToDate As System.Windows.Forms.Label
    Public WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Private WithEvents lblToHour As System.Windows.Forms.Label
    Private WithEvents lblFromHrs As System.Windows.Forms.Label
    Friend WithEvents txtTohour As eZee.TextBox.NumericTextBox
    Friend WithEvents txtFromhour As eZee.TextBox.NumericTextBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Private WithEvents LblShift As System.Windows.Forms.Label
    Public WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents pnlFromToDate As System.Windows.Forms.Panel
    Friend WithEvents pnlTnAPeriod As System.Windows.Forms.Panel
    Friend WithEvents objBtnSearchTnAPeriod As eZee.Common.eZeeGradientButton
    Private WithEvents LblTnAPeriod As System.Windows.Forms.Label
    Public WithEvents cboTnAPeriod As System.Windows.Forms.ComboBox
    Private WithEvents objTnAPeriod As System.Windows.Forms.Label
    Private WithEvents LblDuration As System.Windows.Forms.Label
    Friend WithEvents chkShowEachEmpOnNewPage As System.Windows.Forms.CheckBox
End Class
