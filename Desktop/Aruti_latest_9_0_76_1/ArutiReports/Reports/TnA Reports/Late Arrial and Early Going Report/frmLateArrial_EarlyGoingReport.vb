Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmLateArrial_EarlyGoingReport

#Region "Private Variables"

    Private mstrModuleName As String = "frmLateArrial_EarlyGoingReport"
    Dim objLateArrialReport As clsLateArrialReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objLateArrialReport = New clsLateArrialReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLateArrialReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Late Arrival Report "))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Early Departure Report"))
            cboReportType.SelectedIndex = 0


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            dtpStartdate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpStartdate.Checked = False
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Checked = False
            cboEmployee.SelectedIndex = 0
            chkIgnoreBlankRows.Checked = True
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            objLateArrialReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objLateArrialReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLateArrialReport.SetDefaultValue()
            objLateArrialReport._YearId = FinancialYear._Object._YearUnkid

            'Pinkal (17-Nov-2018) -- Start
            'Bug - PAPAYE#3102 | Unable to print report employee late arrival/early departure

            If dtpStartdate.Checked Then
                objLateArrialReport._FromDate = dtpStartdate.Value.Date
            Else
                objLateArrialReport._FromDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            If dtpToDate.Checked Then
                objLateArrialReport._ToDate = dtpToDate.Value.Date
            Else
                objLateArrialReport._ToDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            'Pinkal (17-Nov-2018) -- End

            objLateArrialReport._EmployeeID = CInt(cboEmployee.SelectedValue)
            objLateArrialReport._EmployeeName = cboEmployee.Text
            objLateArrialReport._Advance_Filter = mstrAdvanceFilter
            objLateArrialReport._ViewByIds = mstrViewByIds
            objLateArrialReport._ViewIndex = mintViewIndex
            objLateArrialReport._ViewByName = mstrViewByName
            objLateArrialReport._Analysis_Fields = mstrAnalysis_Fields
            objLateArrialReport._Analysis_Join = mstrAnalysis_Join
            objLateArrialReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objLateArrialReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objLateArrialReport._Report_GroupName = mstrReport_GroupName
            objLateArrialReport._ReportId = CInt(cboReportType.SelectedIndex)
            objLateArrialReport._ReportTypeName = cboReportType.Text
            objLateArrialReport._IgnoreBlankRows = chkIgnoreBlankRows.Checked
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003555 {PAPAYE}.
            objLateArrialReport._ShowShiftName = chkShowShiftName.Checked
            'S.SANDEEP |29-MAR-2019| -- END


            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            objLateArrialReport._DoNotConsiderLeave = chkDonotconsiderLeave.Checked
            'Pinkal (08-Aug-2019) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmIssued_LvFormDetail_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLateArrialReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIssued_LvFormDetail_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmIssued_LvFormDetail_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objLateArrialReport._ReportName
            eZeeHeader.Message = objLateArrialReport._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIssued_LvFormDetail_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIssued_LvFormDetail_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIssued_LvFormDetail_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIssued_LvFormDetail_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIssued_LvFormDetail_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLateArrialReport.SetMessages()
            objfrm._Other_ModuleNames = "clsLateArrialReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objLateArrialReport.Generate_LateArrival_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                             , Company._Object._Companyunkid, dtpToDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                                             , True, ConfigParameter._Object._IsIncludeInactiveEmp)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003555 {PAPAYE}.
#Region "Link Event"

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Late_Arrival_Report
            objUserDefRMode._Reporttypeid = cboReportType.SelectedIndex
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = cboReportType.SelectedIndex


            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            Dim intUnkid As Integer = -1
            'If chkShowShiftName.Checked = True Then
            '    objUserDefRMode._EarningTranHeadIds = 1
            '    intUnkid = objUserDefRMode.isExist(enArutiReport.Late_Arrival_Report, cboReportType.SelectedIndex, 0, cboReportType.SelectedIndex)
            'Else
            '    objUserDefRMode._EarningTranHeadIds = 0
            '    intUnkid = objUserDefRMode.isExist(enArutiReport.Late_Arrival_Report, cboReportType.SelectedIndex, 0, cboReportType.SelectedIndex)
            'End If

            objUserDefRMode._EarningTranHeadIds = chkShowShiftName.Checked & "|" & chkDonotconsiderLeave.Checked
                intUnkid = objUserDefRMode.isExist(enArutiReport.Late_Arrival_Report, cboReportType.SelectedIndex, 0, cboReportType.SelectedIndex)

            'Pinkal (08-Aug-2019) -- End


            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Setting saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003555 {PAPAYE}.
            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet
            dsList = objUserDefRMode.GetList("List", enArutiReport.Late_Arrival_Report, 0, cboReportType.SelectedIndex)

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            'If dsList.Tables("List").Rows.Count > 0 Then
            '    Select Case CInt(dsList.Tables("List").Rows(0).Item("transactionheadid"))
            '        Case 0
            '            chkShowShiftName.Checked = False
            '        Case 1
            '            chkShowShiftName.Checked = True
            '    End Select
            'Else
            '    chkShowShiftName.Checked = False
            'End If

            'S.SANDEEP |29-MAR-2019| -- END

            If dsList.Tables("List").Rows.Count > 0 Then
                Dim ar() As String = dsList.Tables("List").Rows(0).Item("transactionheadid").ToString().Split("|")
                If ar.Length > 0 Then
                    chkShowShiftName.Checked = CBool(ar(0).ToString())
                    If ar.Length > 1 Then
                        chkDonotconsiderLeave.Checked = CBool(ar(1).ToString())
                    Else
                        chkDonotconsiderLeave.Checked = False
                    End If
            Else
                chkShowShiftName.Checked = False
                    chkDonotconsiderLeave.Checked = False
                End If
            End If

            If CInt(cboReportType.SelectedIndex) = 0 Then
                chkDonotconsiderLeave.Text = Language.getMessage(mstrModuleName, 3, "Do not count") & " " & Language.getMessage(mstrModuleName, 4, "Late Arrival") & " " & Language.getMessage(mstrModuleName, 5, "When Employee is on Leave")
            ElseIf CInt(cboReportType.SelectedIndex) = 1 Then
                chkDonotconsiderLeave.Text = Language.getMessage(mstrModuleName, 3, "Do not count") & " " & Language.getMessage(mstrModuleName, 5, "Early Departure") & " " & Language.getMessage(mstrModuleName, 5, "When Employee is on Leave")
            End If
            'Pinkal (08-Aug-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP |29-MAR-2019| -- END

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objLateArrialReport.setOrderBy(CInt(cboReportType.SelectedIndex))
            txtOrderBy.Text = objLateArrialReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
			Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
			Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Late Arrival Report")
			Language.setMessage(mstrModuleName, 2, "Early Departure Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
