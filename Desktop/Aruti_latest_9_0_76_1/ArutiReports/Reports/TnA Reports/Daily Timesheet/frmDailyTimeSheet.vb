Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

Public Class frmDailyTimeSheet
    Private mstrModuleName As String = "frmDailyTimeSheet"
    Private objDailyTimeSheet As clsDailyTimeSheet

#Region "Constructor"
    Public Sub New()
        objDailyTimeSheet = New clsDailyTimeSheet(User._Object._Languageunkid,Company._Object._Companyunkid)
        objDailyTimeSheet.SetDefaultValue()
        'Pinkal (26-Nov-2018) -- Start
        'Enhancement - Working on Papaye Manual Attendance Register Report.
        _Show_AdvanceFilter = True
        'Pinkal (26-Nov-2018) -- End

        InitializeComponent()
    End Sub
#End Region

#Region "Private Variables"

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (26-Nov-2018) -- Start
    'Enhancement - Working on Papaye Manual Attendance Register Report.
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (26-Nov-2018) -- End

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet


            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            'Dim objShift As New clsNewshift_master
            'dsList = objShift.getListForCombo("List", True)
            'With cboShift
            '    .ValueMember = "shiftunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboShiftType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP |29-MAR-2019| -- END
  
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Daily Timesheet"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Daily Timesheet On Duty"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Daily Timesheet Off Duty"))

            'Pinkal (26-Nov-2018) -- Start
            'Enhancement - Working on Papaye Manual Attendance Register Report.
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 5, "Manual Attendance Register"))
            'Pinkal (26-Nov-2018) -- End

            cboReportType.SelectedIndex = 0

            Dim objMData As New clsMasterData
            dsList = objMData.GetEAllocation_Notification("List")
            Dim drow As DataRow = dsList.Tables("List").NewRow
            drow.Item("Id") = 0
            drow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Select Allocation for Report")
            dsList.Tables("List").Rows.InsertAt(drow, 0)
            With cboReportColumn
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtDate.Value = DateTime.Now
            cboEmployee.SelectedValue = 0
            cboShift.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            txtWorkedHourFrom.Text = ""
            txtWorkedHourTo.Text = ""
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            'objDailyTimeSheet.setDefaultOrderBy(0)
            'txtOrderBy.Text = objDailyTimeSheet.OrderByDisplay
            cboShiftType.SelectedValue = 0
            'S.SANDEEP |29-MAR-2019| -- END


            'Pinkal (16-Apr-2019) -- Start
            'Defect - Solved Defect for Papaaye Manaul Attendance Register Report for Advance Filter Issue.
            mstrAdvanceFilter = ""
            'Pinkal (16-Apr-2019) -- End

            chkInactiveemp.Checked = False
            cboReportType.SelectedIndex = 0
            cboReportColumn.SelectedValue = ConfigParameter._Object._SelectedAllocationForDailyTimeSheetReport_Voltamp
            chkShowEmployeeStatus.Checked = ConfigParameter._Object._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objDailyTimeSheet.SetDefaultValue()

            objDailyTimeSheet._Date = dtDate.Value

            objDailyTimeSheet._EmpId = cboEmployee.SelectedValue
            objDailyTimeSheet._EmpName = cboEmployee.Text

            objDailyTimeSheet._WorkedHourFrom = txtWorkedHourFrom.Text
            objDailyTimeSheet._WorkedHourTo = txtWorkedHourTo.Text
            objDailyTimeSheet._IsActive = chkInactiveemp.Checked
            objDailyTimeSheet._Shiftunkid = cboShift.SelectedValue
            objDailyTimeSheet._ShiftName = cboShift.Text
            objDailyTimeSheet._ViewByIds = mstrStringIds
            objDailyTimeSheet._ViewIndex = mintViewIdx
            objDailyTimeSheet._ViewByName = mstrStringName
            objDailyTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objDailyTimeSheet._Analysis_Join = mstrAnalysis_Join
            objDailyTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objDailyTimeSheet._Report_GroupName = mstrReport_GroupName
            objDailyTimeSheet._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objDailyTimeSheet._ReportTypeName = cboReportType.Text
            objDailyTimeSheet._SelectedAllocationId = cboReportColumn.SelectedValue
            objDailyTimeSheet._AllocationName = cboReportColumn.Text
            objDailyTimeSheet._ShowEmployeeStatus = chkShowEmployeeStatus.Checked
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            objDailyTimeSheet._ShiftTypeId = CInt(cboShiftType.SelectedValue)
            objDailyTimeSheet._ShiftTypeName = cboShiftType.Text
            'S.SANDEEP |29-MAR-2019| -- END


            'Pinkal (16-Apr-2019) -- Start
            'Defect - Solved Defect for Papaaye Manaul Attendance Register Report for Advance Filter Issue.
            objDailyTimeSheet._AdvanceFilter = mstrAdvanceFilter
            'Pinkal (16-Apr-2019) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Form's Events"

    Private Sub frmDailyTimeSheet_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDailyTimeSheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmDailyTimeSheet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()
            Me._Title = objDailyTimeSheet._ReportName
            Me._Message = objDailyTimeSheet._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDailyTimeSheet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmDailyTimeSheet_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDailyTimeSheet_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmDailyTimeSheet_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub


            objDailyTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                dtDate.Value.Date, _
                                                dtDate.Value.Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmDailyTimeSheet_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objDailyTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDailyTimeSheet_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_Reset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmDailyTimeSheet_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmDailyTimeSheet_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDailyTimeSheet.SetMessages()
            objfrm._Other_ModuleNames = "clsDailyTimeSheet"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmDailyTimeSheet_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboShift.DataSource
            frm.ValueMember = cboShift.ValueMember
            frm.DisplayMember = cboShift.DisplayMember
            If frm.DisplayDialog Then
                cboShift.SelectedValue = frm.SelectedValue
                cboShift.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (26-Nov-2018) -- Start
    'Enhancement - Working on Papaye Manual Attendance Register Report.
    Private Sub frmDailyTimeSheet_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (26-Nov-2018) -- End



#End Region

#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            'objDailyTimeSheet.setOrderBy(0)
            objDailyTimeSheet.setOrderBy(cboReportType.SelectedIndex)
            'S.SANDEEP |29-MAR-2019| -- END
            txtOrderBy.Text = objDailyTimeSheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) = 0 Then
                lnkSetAnalysis.Visible = True
                gbSortBy.Visible = True
                cboShift.Enabled = True
                gbShowColumns.Visible = True
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003630 {PAPAYE}.
                objDailyTimeSheet.setDefaultOrderBy(cboReportType.SelectedIndex)
                txtOrderBy.Text = objDailyTimeSheet.OrderByDisplay
                'S.SANDEEP |29-MAR-2019| -- END

            ElseIf CInt(cboReportType.SelectedIndex) = 1 Then
                lnkSetAnalysis.Visible = True
                gbSortBy.Visible = True
                gbShowColumns.Visible = False
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003630 {PAPAYE}.
                objDailyTimeSheet.setDefaultOrderBy(0)
                txtOrderBy.Text = objDailyTimeSheet.OrderByDisplay
                'S.SANDEEP |29-MAR-2019| -- END

            ElseIf CInt(cboReportType.SelectedIndex) = 2 Then
                gbSortBy.Visible = False
                lnkSetAnalysis.Visible = True
                gbShowColumns.Visible = False

                'Pinkal (26-Nov-2018) -- Start
                'Enhancement - Working on Papaye Manual Attendance Register Report.
            ElseIf CInt(cboReportType.SelectedIndex) = 3 Then
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003630 {PAPAYE}.
                'gbSortBy.Visible = False
                gbSortBy.Visible = True
                objDailyTimeSheet.setDefaultOrderBy(cboReportType.SelectedIndex)
                txtOrderBy.Text = objDailyTimeSheet.OrderByDisplay
                'S.SANDEEP |29-MAR-2019| -- END
                lnkSetAnalysis.Visible = True
                gbShowColumns.Visible = False
                'Pinkal (26-Nov-2018) -- End

            End If

            'Pinkal (26-Nov-2018) -- Start
            'Enhancement - Working on Papaye Manual Attendance Register Report.
            If CInt(cboReportType.SelectedIndex) = 3 Then
                _Show_AdvanceFilter = True
            Else
                _Show_AdvanceFilter = False
            End If
            'Pinkal (26-Nov-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003630 {PAPAYE}.
    Private Sub cboShiftType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboShiftType.SelectedIndexChanged
        Try
            Dim objShift As New clsNewshift_master : Dim dsList As New DataSet
            dsList = objShift.getListForCombo("List", True, CInt(cboShiftType.SelectedValue))
            Dim dtTable As DataTable = Nothing
            If CInt(cboShiftType.SelectedValue) <= 0 Then
                dtTable = dsList.Tables(0).Select("shiftunkid = 0").CopyToDataTable()
            Else
                dtTable = dsList.Tables(0)
            End If
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboShiftType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |29-MAR-2019| -- END

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lnkSaveChanges_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveChanges.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            objConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp = cboReportColumn.SelectedValue
            objConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp = chkShowEmployeeStatus.Checked
            objConfig.updateParam()
            objConfig.Refresh()
            ConfigParameter._Object.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSaveChanges_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.gbShowColumns.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShowColumns.ForeColor = GUI._eZeeContainerHeaderForeColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblWorkedHourTo.Text = Language._Object.getCaption(Me.lblWorkedHourTo.Name, Me.lblWorkedHourTo.Text)
			Me.lblWorkedHourFrom.Text = Language._Object.getCaption(Me.lblWorkedHourFrom.Name, Me.lblWorkedHourFrom.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
            Me.gbShowColumns.Text = Language._Object.getCaption(Me.gbShowColumns.Name, Me.gbShowColumns.Text)
            Me.chkShowEmployeeStatus.Text = Language._Object.getCaption(Me.chkShowEmployeeStatus.Name, Me.chkShowEmployeeStatus.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.lnkSaveChanges.Text = Language._Object.getCaption(Me.lnkSaveChanges.Name, Me.lnkSaveChanges.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Daily Timesheet")
			Language.setMessage(mstrModuleName, 2, "Daily Timesheet On Duty")
			Language.setMessage(mstrModuleName, 3, "Daily Timesheet Off Duty")
                        Language.setMessage(mstrModuleName, 4, "Select Allocation for Report")
			Language.setMessage(mstrModuleName, 5, "Manual Attendance Register")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class
