'************************************************************************************************************************************
'Class Name : clsEmpBudgetTimesheetSummaryReport.vb
'Purpose    :
'Date       : 25/04/2017
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsEmpBudgetTimesheetSummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpBudgetTimesheetSummaryReport"
    Private mstrReportId As String = enArutiReport.Employee_Timesheet_Project_Summary_Report  '192
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeID As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrEmployeeCode As String = ""
    Private mstrTransactionHeadIDs As String = ""
    Private mintProjectCodeId As Integer = 0
    Private mstrProjectCode As String = ""
    Private mintPeriodID As Integer = 0
    Private mstrPeriodName As String = ""
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnViewHTMLReport As Boolean = False
    Private dsEmployee As DataSet = Nothing
    Private mdctProjectWiseTotal As Dictionary(Of Integer, Decimal)
    Private mintViewIndex As Integer = 0
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrOrderByQuery As String = ""


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private mblnShowEmpIdentifyNo As Boolean = False
    Private mblnShowLeaveTypeHours As Boolean = False
    Private mdctLeaveTypeWiseTotal As Dictionary(Of Integer, Decimal)
    Private mblnShowHolidayHours As Boolean = False
    Private mdctHolidayTotal As Dictionary(Of Integer, Decimal)
    Private mblnShowExtraHours As Boolean = False
    Private mdctExtraTotal As Dictionary(Of Integer, Decimal)
    'Pinkal (28-Jul-2018) -- End

    'Pinkal (13-Aug-2018) -- Start
    'Enhancement - Changes For PACT [Ref #249,252]
    Private mblnIgnoreZeroLeaveTypeHrs As Boolean = False
    'Pinkal (13-Aug-2018) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeID = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _TransactionHeadIDs() As String
        Set(ByVal value As String)
            mstrTransactionHeadIDs = value
        End Set
    End Property

    Public WriteOnly Property _ProjectCodeId() As Integer
        Set(ByVal value As Integer)
            mintProjectCodeId = value
        End Set
    End Property

    Public WriteOnly Property _ProjectCode() As String
        Set(ByVal value As String)
            mstrProjectCode = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public Property _ViewReportInHTML() As Boolean
        Get
            Return mblnViewHTMLReport
        End Get
        Set(ByVal value As Boolean)
            mblnViewHTMLReport = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Public WriteOnly Property _ShowEmpIdentifyNo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpIdentifyNo = value
        End Set
    End Property

    Public WriteOnly Property _ShowLeaveTypeHours() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLeaveTypeHours = value
        End Set
    End Property

    Public WriteOnly Property _ShowHolidayHours() As Boolean
        Set(ByVal value As Boolean)
            mblnShowHolidayHours = value
        End Set
    End Property

    Public WriteOnly Property _ShowExtraHours() As Boolean
        Set(ByVal value As Boolean)
            mblnShowExtraHours = value
        End Set
    End Property
    'Pinkal (28-Jul-2018) -- End

    'Pinkal (13-Aug-2018) -- Start
    'Enhancement - Changes For PACT [Ref #249,252]

    Public WriteOnly Property _IgnoreZeroLeaveTypeHrs() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZeroLeaveTypeHrs = value
        End Set
    End Property

    'Pinkal (13-Aug-2018) -- End



#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrEmployeeCode = ""
            mintProjectCodeId = 0
            mstrProjectCode = ""
            mintPeriodID = 0
            mstrPeriodName = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mstrOrderByQuery = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrAnalysis_OrderBy = ""
            mblnViewHTMLReport = False

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            mblnShowEmpIdentifyNo = False
            mblnShowLeaveTypeHours = False
            mblnShowHolidayHours = False
            mblnShowExtraHours = False
            'Pinkal (28-Jul-2018) -- End

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            mblnIgnoreZeroLeaveTypeHrs = False
            'Pinkal (13-Aug-2018) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Period: ") & " " & mstrPeriodName & " "


            If mintViewIndex > 0 Then
                mstrOrderByQuery &= " ORDER BY " & mstrAnalysis_OrderBy_GName & ",employeename"
            Else
                mstrOrderByQuery &= " ORDER BY employeename "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_EmployeeSummaryReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            'Dim objEmployee As New clsEmployee_Master
            'dsEmployee = objEmployee.GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate.Date _
            '                                                                                         , mdtEndDate.Date, xUserModeSetting, True, False, "List", False, mintEmployeeId, , , , , , , , , , , , , , mstrFilter)

            'If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Columns.Contains("isapproved") Then
            '    dsEmployee.Tables(0).Columns.Remove("isapproved")
            'End If

            'If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Columns.Contains("EmpCodeName") Then
            '    dsEmployee.Tables(0).Columns.Remove("EmpCodeName")
            'End If
            'objEmployee = Nothing


            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, mdtStartDate.Date, mdtEndDate.Date, True, False, xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndDate.Date, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            StrQ = "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode "

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            If mblnShowEmpIdentifyNo Then
                StrQ &= ", ISNULL(hremployee_idinfo_tran.identity_no,'') AS identity_no"
            End If
            'Pinkal (28-Jul-2018) -- End

            StrQ &= ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master " & _
                        " LEFT JOIN " & _
                        " ( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         jobgroupunkid " & _
                        "        ,jobunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid AS costcenterunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 gradegroupunkid " & _
                        "		,gradeunkid " & _
                        "		,gradelevelunkid " & _
                        "		,employeeunkid " & _
                        "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                        "	FROM prsalaryincrement_tran " & _
                        "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        hremployee_shift_tran.employeeunkid " & _
                        "       ,hremployee_shift_tran.shiftunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_shift_tran " & _
                        "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                        "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            If mblnShowEmpIdentifyNo Then
                StrQ &= " LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid  AND hremployee_idinfo_tran.isdefault = 1 "
            End If
            'Pinkal (28-Jul-2018) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

            'If mblnShowEmpIdentifyNo Then
            '    StrQ &= " AND hremployee_idinfo_tran.isdefault = 1"
            'End If

            'Pinkal (28-Jul-2018) -- End

            'Pinkal (28-Aug-2018) -- Start
            'Bug - Solved Problem For PACT WHEN BUDGET TIMESHEET IS APPLIED ON LEAVE DAYS.
            If xDataFilterQry.Trim.Length > 0 Then
                StrQ &= xDataFilterQry
            End If
            'Pinkal (28-Aug-2018) -- End


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsEmployee = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsTranHead As DataSet
            If mstrTransactionHeadIDs.Trim.Length > 0 Then
                Dim objTranhead As New clsTransactionHead
                dsTranHead = objTranhead.getComboList(xDatabaseName, "List", False, , , , , , "tranheadunkid in (" & mstrTransactionHeadIDs & ")")
                objTranhead = Nothing

                If dsEmployee IsNot Nothing AndAlso dsList IsNot Nothing AndAlso dsTranHead.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In dsTranHead.Tables(0).Rows
                        Dim dcColumn As New DataColumn("tranhead_" & dr("name").ToString())
                        dcColumn.Caption = dr("name").ToString()
                        dcColumn.ExtendedProperties.Add("tranheadunkid", CInt(dr("tranheadunkid")))
                        dcColumn.DataType = GetType(System.String)
                        dcColumn.DefaultValue = ""
                        dsEmployee.Tables(0).Columns.Add(dcColumn)
                    Next
                End If

                Dim mdtTranHead As DataTable = GetEmpTransactionHeadsAmount()

                If mdtTranHead IsNot Nothing Then
                    For Each dr As DataRow In dsEmployee.Tables(0).Rows
                        For i As Integer = 0 To dsEmployee.Tables(0).Columns.Count - 1
                            'Pinkal (28-Jul-2018) -- Start
                            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                            If dsEmployee.Tables(0).Columns(i).ColumnName = "Id" OrElse dsEmployee.Tables(0).Columns(i).ColumnName = "GName" OrElse dsEmployee.Tables(0).Columns(i).ColumnName = "employeeunkid" OrElse dsEmployee.Tables(0).Columns(i).ColumnName = "employeecode" OrElse dsEmployee.Tables(0).Columns(i).ColumnName = "employeename" OrElse dsEmployee.Tables(0).Columns(i).ColumnName = "identity_no" Then Continue For
                            'Pinkal (28-Jul-2018) -- End

                            Dim drtranhead = From drtrand In mdtTranHead Where drtrand("employeeunkid") = CInt(dr("employeeunkid")) And drtrand("tranheadunkid") = CInt(dsEmployee.Tables(0).Columns(i).ExtendedProperties("tranheadunkid"))
                            If drtranhead.Count > 0 Then
                                dr(dsEmployee.Tables(0).Columns(i)) = Format(CDec(drtranhead(0)("amount")), GUI.fmtCurrency)
                            Else
                                dr(dsEmployee.Tables(0).Columns(i)) = Format(0, GUI.fmtCurrency)
                            End If
                        Next
                    Next
                End If

            End If


            Dim drRow As DataRow = dsEmployee.Tables(0).NewRow()
            If dsEmployee.Tables(0).Rows.Count > 0 Then
                drRow("Id") = dsEmployee.Tables(0).Rows(dsEmployee.Tables(0).Rows.Count - 1)("Id")
                drRow("GName") = dsEmployee.Tables(0).Rows(dsEmployee.Tables(0).Rows.Count - 1)("GName")
            Else
                drRow("Id") = 0
                drRow("GName") = ""
            End If
            drRow("employeeunkid") = -999
            drRow("employeecode") = ""
            drRow("employeename") = Language.getMessage(mstrModuleName, 1, "Total")
            dsEmployee.Tables(0).Rows.Add(drRow)


            Dim mintColumnIndex As Integer = dsEmployee.Tables(0).Columns.Count

            Dim objProject As New clsFundProjectCode
            dsList = objProject.GetComboList("Project", False, 0)
            Dim dtProject As DataTable = Nothing
            If mintProjectCodeId > 0 Then
                dtProject = New DataView(dsList.Tables(0), "fundprojectcodeunkid = " & mintProjectCodeId, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtProject = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            objProject = Nothing


            If dsEmployee IsNot Nothing AndAlso dtProject IsNot Nothing AndAlso dtProject.Rows.Count > 0 Then
                mdctProjectWiseTotal = New Dictionary(Of Integer, Decimal)
                For Each dr As DataRow In dtProject.Rows
                    Dim dcColumn As New DataColumn("fundprojectcode_" & dr("fundprojectcode").ToString())
                    dcColumn.Caption = dr("fundprojectcode").ToString()
                    dcColumn.ExtendedProperties.Add("fundprojectcodeunkid", CInt(dr("fundprojectcodeunkid")))
                    dcColumn.DataType = GetType(System.String)
                    dcColumn.DefaultValue = ""
                    dsEmployee.Tables(0).Columns.Add(dcColumn)
                    If mdctProjectWiseTotal.ContainsKey(CInt(dr("fundprojectcodeunkid"))) = False Then
                        mdctProjectWiseTotal.Add(CInt(dr("fundprojectcodeunkid")), 0)
                    End If
                Next
            End If


            If mintReportTypeID = 0 Then 'By Hourly

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                If mblnShowLeaveTypeHours Then
                    Dim dsLeaveType As DataSet
                    Dim objLeaveType As New clsleavetype_master
                    dsLeaveType = objLeaveType.getListForCombo("List", False, -1, "")
                    objLeaveType = Nothing

                    If dsEmployee IsNot Nothing AndAlso dsList IsNot Nothing AndAlso dsLeaveType.Tables(0).Rows.Count > 0 Then
                        mdctLeaveTypeWiseTotal = New Dictionary(Of Integer, Decimal)
                        For Each dr As DataRow In dsLeaveType.Tables(0).Rows
                            Dim dcColumn As New DataColumn("leavetype_" & dr("name").ToString())
                            dcColumn.Caption = dr("name").ToString() & " " & Language.getMessage(mstrModuleName, 13, "Hours")
                            dcColumn.ExtendedProperties.Add("leavetypeunkid", CInt(dr("leavetypeunkid")))
                            dcColumn.DataType = GetType(System.String)
                            dcColumn.DefaultValue = ""
                            dsEmployee.Tables(0).Columns.Add(dcColumn)
                            If mdctLeaveTypeWiseTotal.ContainsKey(CInt(dr("leavetypeunkid"))) = False Then
                                mdctLeaveTypeWiseTotal.Add(CInt(dr("leavetypeunkid")), 0)
                            End If
                        Next
                    End If
                End If

                If mblnShowHolidayHours Then
                    mdctHolidayTotal = New Dictionary(Of Integer, Decimal)
                    Dim dcColumn As New DataColumn("Holidays_Hrs")
                    dcColumn.Caption = Language.getMessage(mstrModuleName, 14, " Holidays Hours")
                    dcColumn.ExtendedProperties.Add("HolidayID", -9999)
                    dcColumn.DataType = GetType(System.String)
                    dcColumn.DefaultValue = ""
                    dsEmployee.Tables(0).Columns.Add(dcColumn)
                    If mdctHolidayTotal.ContainsKey(-9999) = False Then
                        mdctHolidayTotal.Add(-9999, 0)
                    End If
                End If

                If mblnShowExtraHours Then
                    mdctExtraTotal = New Dictionary(Of Integer, Decimal)
                    Dim dcColumn As New DataColumn("Extra_Hrs")
                    dcColumn.Caption = Language.getMessage(mstrModuleName, 15, " Extra Hours")
                    dcColumn.ExtendedProperties.Add("ExtraHrID", -10000)
                    dcColumn.DataType = GetType(System.String)
                    dcColumn.DefaultValue = ""
                    dsEmployee.Tables(0).Columns.Add(dcColumn)
                    If mdctExtraTotal.ContainsKey(-10000) = False Then
                        mdctExtraTotal.Add(-10000, 0)
                    End If
                End If

                'Pinkal (28-Jul-2018) -- End

                Generate_ByHourSummaryReport(mintColumnIndex)

            ElseIf mintReportTypeID = 1 Then 'By Percentage
                Generate_ByPercentageSummaryReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate, mdtEndDate, xUserModeSetting, True, mintColumnIndex)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell
            Dim strBuilder As New StringBuilder
            Dim mdtTableExcel As DataTable = dsEmployee.Tables(0)


            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("Id") Then
                mdtTableExcel.Columns.Remove("Id")
            End If


            mdtTableExcel.Columns("GName").SetOrdinal(mdtTableExcel.Columns.Count - 1)

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            If mintReportTypeID = 0 AndAlso mblnIgnoreZeroLeaveTypeHrs AndAlso mblnShowLeaveTypeHours Then 'By Hourly

                Dim Firstindex As Integer = (From p In mdtTableExcel.Columns.Cast(Of DataColumn)() Where (p.ExtendedProperties.ContainsKey("leavetypeunkid") = True) Select (p.Ordinal)).FirstOrDefault
                Dim Lastindex As Integer = (From p In mdtTableExcel.Columns.Cast(Of DataColumn)() Where (p.ExtendedProperties.ContainsKey("leavetypeunkid") = True) Select (p.Ordinal)).LastOrDefault
                Dim mdctColumn As New Dictionary(Of Integer, String)
                For i As Integer = Firstindex To Lastindex
                    Dim dr() As DataRow = mdtTableExcel.Select("[" & mdtTableExcel.Columns(i).ColumnName & "] = '00:00' OR [" & mdtTableExcel.Columns(i).ColumnName & "] = '' ")
                    If dr.Length = mdtTableExcel.Rows.Count Then
                        If mdctColumn.ContainsKey(i) = False Then
                            mdctColumn.Add(i, mdtTableExcel.Columns(i).ColumnName)
                        End If
                    End If
                Next


                For Each kvp As KeyValuePair(Of Integer, String) In mdctColumn
                    If mdtTableExcel.Columns.Contains(mdctColumn(kvp.Key)) Then
                        mdtTableExcel.Columns.Remove(mdctColumn(kvp.Key))
                    End If
                Next
                mdtTableExcel.AcceptChanges()
            End If
            'Pinkal (13-Aug-2018) -- End



            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 90

                ElseIf i = 1 Then
                    intArrayColumnWidth(i) = 150

                ElseIf i > 1 AndAlso i < intArrayColumnWidth.Length - 1 Then
                    intArrayColumnWidth(i) = 100

                ElseIf i <= intArrayColumnWidth.Length - 1 AndAlso mintViewIndex <= 0 Then
                    intArrayColumnWidth(i) = 150

                ElseIf i = intArrayColumnWidth.Length - 1 AndAlso mintViewIndex > 0 Then
                    intArrayColumnWidth(i) = 0

                End If
            Next
            'SET EXCEL CELL WIDTH

            If mblnViewHTMLReport = False Then

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayHeader.Add(row)


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)


                If mdtTableExcel IsNot Nothing AndAlso mdtTableExcel.Rows.Count > 0 Then

                    row = New WorksheetRow()
                    wcell = Nothing
                    Dim mblnTranFlag As Boolean = False
                    Dim mblnProjectFlag As Boolean = False

                    For Each col As DataColumn In mdtTableExcel.Columns
                        If col.ColumnName.StartsWith("tranhead_") = True Then
                            If mblnTranFlag Then Continue For
                            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Transaction Heads(Earning and Deductions Amounts)"), "HeaderStyle")
                            wcell.MergeAcross = dsTranHead.Tables(0).Rows.Count - 1
                            row.Cells.Add(wcell)
                            mblnTranFlag = True
                        ElseIf col.ColumnName.StartsWith("fundprojectcode_") = True Then
                            If mblnProjectFlag Then Continue For
                            If mintReportTypeID = 0 Then
                                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Hours Worked per Project Code in a Month"), "HeaderStyle")
                            ElseIf mintReportTypeID = 1 Then
                                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Project Code Allocation And Time in Percentages Spent on Each Project Code"), "HeaderStyle")
                            End If
                            wcell.MergeAcross = dtProject.Rows.Count - 1
                            row.Cells.Add(wcell)
                            mblnProjectFlag = True
                        ElseIf col.Ordinal <= mdtTableExcel.Columns.Count - 1 Then
                            wcell = New WorksheetCell("", "HeaderStyle")
                            row.Cells.Add(wcell)
                        End If
                    Next
                    rowsArrayHeader.Add(row)

                End If

            Else

                If mdtTableExcel IsNot Nothing AndAlso mdtTableExcel.Rows.Count > 0 Then

                    strBuilder.Append("<TR>" & vbCrLf)

                    Dim mblnTranFlag As Boolean = False
                    Dim mblnProjectFlag As Boolean = False

                    Dim xColumnIndex As Integer = IIf(mintViewIndex <= 0, mdtTableExcel.Columns.Count - 1, mdtTableExcel.Columns.Count - 2)

                    For i As Integer = 0 To xColumnIndex

                        If mdtTableExcel.Columns(i).ColumnName.StartsWith("tranhead_") = True Then
                            If mblnTranFlag Then Continue For
                            strBuilder.Append("<TD bgcolor='Blue' align='center' Colspan = " & dsTranHead.Tables(0).Rows.Count & " style='background-color:blue; color:white; font-weight:bold; font-size:12px;'>" & Language.getMessage(mstrModuleName, 6, "Transaction Heads(Earning and Deductions Amounts)") & "</B> </TD>" & vbCrLf)
                            mblnTranFlag = True
                        ElseIf mdtTableExcel.Columns(i).ColumnName.StartsWith("fundprojectcode_") = True Then
                            If mblnProjectFlag Then Continue For
                            If mintReportTypeID = 0 Then
                                strBuilder.Append("<TD bgcolor='Blue' align='center' Colspan = " & dtProject.Rows.Count & " style='background-color:blue; color:white; font-weight:bold; font-size:12px;'>" & Language.getMessage(mstrModuleName, 7, "Hours Worked per Project Code in a Month") & "</B> </TD>" & vbCrLf)
                            ElseIf mintReportTypeID = 1 Then
                                strBuilder.Append("<TD bgcolor='Blue' align='center' Colspan = " & dtProject.Rows.Count & " style='background-color:blue; color:white; font-weight:bold; font-size:12px;'>" & Language.getMessage(mstrModuleName, 8, "Project Code Allocation And Time in Percentages Spent on Each Project Code") & "</B> </TD>" & vbCrLf)
                            End If
                            mblnProjectFlag = True
                        ElseIf mdtTableExcel.Columns(i).Ordinal <= mdtTableExcel.Columns.Count - 1 Then
                            strBuilder.Append("<TD bgcolor='Blue' Colspan =1 style='width:" & intArrayColumnWidth(i) & "px;border-width:1px'> &nbsp; </TD>" & vbCrLf)
                        End If

                    Next

                    strBuilder.Append("</TR>" & vbCrLf)

                    If strBuilder.Length > 0 Then rowsArrayHeader.Add(strBuilder.ToString)
                    strBuilder.Remove(0, strBuilder.Length)

                End If



                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)

                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD style='border-width: 0px;'> &nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)


                strBuilder.Append("<TR>" & vbCrLf)
                strBuilder.Append("<TD style='border-width: 0px;'>&nbsp; </TD>" & vbCrLf)
                strBuilder.Append("</TR>" & vbCrLf)


                If strBuilder.Length > 0 Then rowsArrayFooter.Add(strBuilder.ToString)
                strBuilder.Remove(0, strBuilder.Length)

            End If

            '--------------------


            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 9, "Employee Code")
            mdtTableExcel.Columns("employeename").Caption = Language.getMessage(mstrModuleName, 10, "Employee Name")

            If mintReportTypeID = 0 Then
                mdtTableExcel.Columns("Total Monthly Hours").Caption = Language.getMessage(mstrModuleName, 11, "Total Monthly Hours")
            End If

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            If mblnShowEmpIdentifyNo Then
                mdtTableExcel.Columns("identity_no").Caption = Language.getMessage(mstrModuleName, 12, "Identify Number")
                mdtTableExcel.Columns("identity_no").ExtendedProperties.Add("style", "s8b")
            End If
            'Pinkal (28-Jul-2018) -- End

            mdtTableExcel.Columns("employeecode").ExtendedProperties.Add("style", "s8b")
            mdtTableExcel.Columns("employeename").ExtendedProperties.Add("style", "s8b")

            If mblnViewHTMLReport Then
                Call ReportExecute(xCompanyUnkid, enArutiReport.Employee_Timesheet_Project_Summary_Report, Nothing, enPrintAction.None, enExportAction.ExcelHTML, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " - " & mstrReportTypeName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, True, "", False, False)
            Else
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " - " & mstrReportTypeName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False, "", False)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmployeeSummaryReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_ByHourSummaryReport(ByVal xColumnIndex As Integer)
        Dim StrQ As String = ""
        Dim dsList As DataSet = Nothing

        Try

            'StrQ = " SELECT " & _
            '        "  ISNULL(tsLevel.tslevelunkid, 0) AS tslevelunkid " & _
            '        ", ISNULL(tsLevel.levelname, '') AS [Level] " & _
            '        ", ISNULL(STUFF((SELECT DISTINCT ',' +  CAST( CASE WHEN tsapprover_master.isexternalapprover = 0 THEN  " & _
            '        "                                                                            ISNULL(hremployee_master.employeecode, '') + ' - ' +  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')  " & _
            '        "                                                              ELSE " & _
            '        "                                                              ISNULL(usr.username,'') END   AS NVARCHAR(max)) " & _
            '        "                          FROM tsemptimesheet_approval " & _
            '        "                          LEFT JOIN tsapprover_master ON tsemptimesheet_approval.tsapproverunkid = tsapprover_master.tsapproverunkid " & _
            '        "                          LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
            '        "                          LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tsapprover_master.employeeapproverunkid " & _
            '        "                          LEFT JOIN hrapprover_usermapping ON tsapprover_master.tsapproverunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Timesheet_Approver & _
            '        "                          LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
            '        "                          WHERE periodunkid = @periodunkid AND tsemptimesheet_approval.employeeunkid = @employeeunkid " & _
            '        "                          AND tsemptimesheet_approval.isvoid = 0 AND tsemptimesheet_approval.statusunkid = 1 " & _
            '        "                          AND tsemptimesheet_approval.iscancel = 0 AND tsapproverlevel_master.tslevelunkid = tsLevel.tslevelunkid   FOR XML PATH('')),1,1,''),'') AS Approver " & _
            '        "  FROM tsapproverlevel_master tsLevel "

            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            'Dim dsApprover As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            'End If


            If dsEmployee IsNot Nothing Then

                If dsEmployee.Tables(0).Columns.Contains("Total Monthly Hours") = False Then
                    Dim dcColumn As New DataColumn("Total Monthly Hours")
                    dcColumn.DataType = GetType(System.String)
                    dcColumn.DefaultValue = ""
                    dsEmployee.Tables(0).Columns.Add(dcColumn)
                End If

                Dim mintEmpTotalHours As Integer = 0
                Dim minTotalHours As Integer = 0


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                Dim objBudgetTimesheet As New clsBudgetEmp_timesheet
                'Pinkal (28-Jul-2018) -- End

                For i As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1
                    mintEmpTotalHours = 0
                    For j As Integer = xColumnIndex To dsEmployee.Tables(0).Columns.Count - 1

                        If dsEmployee.Tables(0).Columns(j).ColumnName = "Total Monthly Hours" Then
                            If mintEmpTotalHours > 0 Then dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mintEmpTotalHours).ToString("#00.00").Replace(".", ":")
                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, minTotalHours).ToString("#00.00").Replace(".", ":")
                            End If

                            'Pinkal (28-Jul-2018) -- Start
                            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                        ElseIf dsEmployee.Tables(0).Columns(j).ColumnName.StartsWith("fundprojectcode_") Then

                            Dim mintSec As Integer = GetEmpProjectWiseTotalActivityHrs(CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid")))
                            If mintSec > 0 Then dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mintSec).ToString("#00.00").Replace(".", ":")
                            mintEmpTotalHours += mintSec

                            If mdctProjectWiseTotal.ContainsKey(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid"))) Then
                                mdctProjectWiseTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid"))) += mintSec
                            End If

                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mdctProjectWiseTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid")))).ToString("#00.00").Replace(".", ":")
                            End If

                        ElseIf dsEmployee.Tables(0).Columns(j).ColumnName.StartsWith("leavetype_") Then

                            Dim mintLeaveTypeSec As Integer = GetEmpLeaveHolidayHours(CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), mdtStartDate.Date, mdtEndDate.Date, True, CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("leavetypeunkid")), False)
                            If mintLeaveTypeSec > 0 Then dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mintLeaveTypeSec).ToString("#00.00").Replace(".", ":")
                            mintEmpTotalHours += mintLeaveTypeSec

                            If mdctLeaveTypeWiseTotal.ContainsKey(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("leavetypeunkid"))) Then
                                mdctLeaveTypeWiseTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("leavetypeunkid"))) += mintLeaveTypeSec
                            End If

                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mdctLeaveTypeWiseTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("leavetypeunkid")))).ToString("#00.00").Replace(".", ":")
                            End If

                        ElseIf dsEmployee.Tables(0).Columns(j).ColumnName.StartsWith("Holidays_Hrs") Then

                            Dim mintHolidaySec As Integer = GetEmpLeaveHolidayHours(CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), mdtStartDate.Date, mdtEndDate.Date, False, CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("HolidayID")), True)
                            If mintHolidaySec > 0 Then dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mintHolidaySec).ToString("#00.00").Replace(".", ":")
                            mintEmpTotalHours += mintHolidaySec

                            If mdctHolidayTotal.ContainsKey(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("HolidayID"))) Then
                                mdctHolidayTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("HolidayID"))) += mintHolidaySec
                            End If

                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mdctHolidayTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("HolidayID")))).ToString("#00.00").Replace(".", ":")
                            End If

                        ElseIf dsEmployee.Tables(0).Columns(j).ColumnName.StartsWith("Extra_Hrs") Then
                            Dim mintExtraHrsSec As Integer = 0
                            Dim dtExtraHours As DataTable = objBudgetTimesheet.GetEmployeeExtraHours(CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), mdtStartDate.Date, mdtEndDate.Date)
                            If dtExtraHours IsNot Nothing AndAlso dtExtraHours.Rows.Count > 0 Then
                                mintExtraHrsSec = CInt(dtExtraHours.Compute("SUM(Extrahrs)", "1=1"))
                            End If

                            If mintExtraHrsSec > 0 Then dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mintExtraHrsSec).ToString("#00.00").Replace(".", ":")
                            mintEmpTotalHours += mintExtraHrsSec

                            If mdctExtraTotal.ContainsKey(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("ExtraHrID"))) Then
                                mdctExtraTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("ExtraHrID"))) += mintExtraHrsSec
                            End If

                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = CalculateTime(True, mdctExtraTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("ExtraHrID")))).ToString("#00.00").Replace(".", ":")
                            End If

                            'Pinkal (28-Jul-2018) -- End

                        End If
                    Next
                    minTotalHours += mintEmpTotalHours
                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ByHourSummaryReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_ByPercentageSummaryReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, ByVal xColumnIndex As Integer)
        Try

            If dsEmployee IsNot Nothing Then

                If dsEmployee.Tables(0).Columns.Contains("Total") = False Then
                    Dim dcColumn As New DataColumn("Total")
                    dcColumn.DataType = GetType(System.String)
                    dcColumn.DefaultValue = ""
                    dsEmployee.Tables(0).Columns.Add(dcColumn)
                End If

                Dim objEmpTimesheet As New clsBudgetEmp_timesheet
                Dim dtEmpTotalApprovedHrs As DataTable = objEmpTimesheet.GetEmpApprovedTimesheetHrs(mdtStartDate, mdtEndDate)
                objEmpTimesheet = Nothing

                Dim objEmpShift As New clsEmployee_Shift_Tran
                Dim mdecTotalColPercentage As Decimal = 0

                For i As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1

                    Dim mdecTotalRowPercentage As Decimal = 0
                    Dim mintTotalWorkingDays As Integer = 0
                    Dim minTotalWorkinghrs As Integer = 0
                    Dim mintTotalHolidayDays As Integer = 0
                    Dim minTotalHolidayWorkinghrs As Integer = 0

                    objEmpShift.GetWorkingDaysAndHours(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), mdtStartDate, mdtEndDate, mintTotalWorkingDays, minTotalWorkinghrs)
                    objEmpShift.GetPHDaysAndHours(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), mdtStartDate, mdtEndDate, mintTotalHolidayDays, minTotalHolidayWorkinghrs)

                    mintTotalWorkingDays = mintTotalWorkingDays - mintTotalHolidayDays
                    minTotalWorkinghrs = minTotalWorkinghrs - minTotalHolidayWorkinghrs

                    For j As Integer = xColumnIndex To dsEmployee.Tables(0).Columns.Count - 1

                        If dsEmployee.Tables(0).Columns(j).ColumnName = "Total" Then

                            If mdecTotalRowPercentage > 0 Then dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = mdecTotalRowPercentage.ToString("#00.00")

                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = mdecTotalColPercentage.ToString("#00.00")
                            End If

                        Else

                            Dim drApprovedHrs = From drRow In dtEmpTotalApprovedHrs Where CInt(drRow("employeeunkid")) = CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid"))

                            If drApprovedHrs.Count > 0 Then
                                If CInt(drApprovedHrs(0)("ApprovedHrsinSec")) > minTotalWorkinghrs Then minTotalWorkinghrs = CInt(drApprovedHrs(0)("ApprovedHrsinSec"))
                            End If

                            Dim mintSec As Integer = GetEmpProjectWiseTotalActivityHrs(CInt(dsEmployee.Tables(0).Rows(i)("employeeunkid")), CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid")))
                            If mintSec > 0 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = Math.Round(CDec(mintSec * 100 / minTotalWorkinghrs), 2).ToString("#00.00")
                            End If

                            mdecTotalRowPercentage += Math.Round(CDec(mintSec * 100 / minTotalWorkinghrs), 2)

                            If mdctProjectWiseTotal.ContainsKey(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid"))) Then
                                mdctProjectWiseTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid"))) += Math.Round(CDec(mintSec * 100 / minTotalWorkinghrs), 2)
                            End If

                            If i = dsEmployee.Tables(0).Rows.Count - 1 Then
                                dsEmployee.Tables(0).Rows(i)(dsEmployee.Tables(0).Columns(j).ColumnName) = Math.Round(mdctProjectWiseTotal(CInt(dsEmployee.Tables(0).Columns(j).ExtendedProperties("fundprojectcodeunkid"))), 2).ToString("#00.00")
                            End If

                        End If

                    Next

                    mdecTotalColPercentage += mdecTotalRowPercentage

                Next


                dtEmpTotalApprovedHrs.Clear()
                dtEmpTotalApprovedHrs = Nothing
                objEmpShift = Nothing

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ByPercentageSummaryReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function GetEmpTransactionHeadsAmount() As DataTable
        Dim mdtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try

            strQ = " SELECT " & _
                      " prpayrollprocess_tran.employeeunkid  " & _
                      ", prpayrollprocess_tran.tranheadunkid " & _
                      ", ISNULL(amount, 0.00) AS amount " & _
                      " FROM prpayrollprocess_tran " & _
                      " LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                      " WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                      " AND prtnaleave_tran.payperiodunkid = @PeriodID  " & _
                      " AND prpayrollprocess_tran.tranheadunkid in (" & mstrTransactionHeadIDs & ")"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@PeriodID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpTransactionHeadsAmount; Module Name: " & mstrModuleName)
        End Try
        Return mdtTable
    End Function

    Private Function GetEmpProjectWiseTotalActivityHrs(ByVal xEmployeeID As Integer, ByVal xProjectID As Integer) As Integer
        Dim mintTotalSeconds As Integer = 0
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try

            strQ = "SELECT " & _
                     "       ltbemployee_timesheet.projectcodeunkid " & _
                     ",      SUM(ISNULL(approvedactivity_hrs, 0)) * 60 AS ApprovedHrsInSec " & _
                     " FROM ltbemployee_timesheet " & _
                     " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid = ltbemployee_timesheet.projectcodeunkid " & _
                     " WHERE ltbemployee_timesheet.isvoid = 0 AND statusunkid = 1 AND issubmit_approval = 1 " & _
                     " AND ltbemployee_timesheet.employeeunkid = @EmployeeId AND ltbemployee_timesheet.projectcodeunkid = @projectcodeunkid " & _
                     " AND CONVERT(CHAR(8), ltbemployee_timesheet.activitydate, 112) BETWEEN @Startdate AND @EndDate " & _
                     " GROUP BY  ltbemployee_timesheet.employeeunkid ,ltbemployee_timesheet.projectcodeunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID)
            objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xProjectID)
            objDataOperation.AddParameter("@Startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate.Date).ToString())
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate.Date).ToString())
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintTotalSeconds = CInt(dsList.Tables(0).Rows(0)("ApprovedHrsInSec"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpProjectWiseTotalActivityHrs; Module Name: " & mstrModuleName)
        End Try
        Return mintTotalSeconds
    End Function


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private Function GetEmpLeaveHolidayHours(ByVal xEmployeeID As Integer, ByVal xStartDate As DateTime, ByVal xEndDate As DateTime, ByVal xIncludeLeave As Boolean, ByVal xLeaveTypeID As Integer, ByVal xIncludeHoliday As Boolean) As Integer
        Dim mintTotalHours As Integer = 0
        Try
            Dim dtTable As DataTable = Nothing
            If xIncludeLeave Then
                Dim objLeaveIssue As New clsleaveissue_master
                dtTable = objLeaveIssue.GetEmployeeIssueLeaveDays(xEmployeeID, xLeaveTypeID, mdtStartDate.Date, mdtEndDate.Date)
                objLeaveIssue = Nothing
            ElseIf xIncludeHoliday Then
                Dim objEmpHoliday As New clsemployee_holiday
                dtTable = objEmpHoliday.GetEmployeeHolidayList(xEmployeeID, xStartDate, xEndDate)
                objEmpHoliday = Nothing
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                Dim objShiftTran As New clsshift_tran
                Dim xShiftID As Integer = 0
                Dim xShiftHrs As Integer = 0
                Dim objEmpTimesheet As New clsBudgetEmp_timesheet
                For Each drRow As DataRow In dtTable.Rows
                    If xIncludeHoliday Then
                        Dim dsHolidayData As DataSet = objEmpTimesheet.GetEmployeeTimesheetList(mintPeriodID, -1, xEmployeeID, -1, eZeeDate.convertDate(drRow.Item("Date").ToString).Date, "", False)
                        If dsHolidayData IsNot Nothing AndAlso dsHolidayData.Tables(0).Rows.Count > 0 Then
                            For i As Integer = 0 To dsHolidayData.Tables(0).Rows.Count - 1
                                mintTotalHours += (CInt(dsHolidayData.Tables(0).Rows(i)("approvedactivity_hrs")) * 60)
                            Next
                            Continue For
                        End If
                    End If
                    xShiftID = objEmpShiftTran.GetEmployee_Current_ShiftId(eZeeDate.convertDate(drRow("Date").ToString()).Date, xEmployeeID)
                    objShiftTran.GetShiftTran(xShiftID, Nothing)
                    Dim drShiftHrs() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(eZeeDate.convertDate(drRow.Item("Date").ToString).Date), False, FirstDayOfWeek.Sunday).ToString()))
                    If drShiftHrs.Length > 0 Then
                        mintTotalHours += CInt(drShiftHrs(0)("workinghrsinsec"))
                    End If
                Next
                objShiftTran = Nothing
                objEmpShiftTran = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpLeaveHolidayHours; Module Name: " & mstrModuleName)
        End Try
        Return mintTotalHours
    End Function
    'Pinkal (28-Jul-2018) -- End


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Total")
            Language.setMessage(mstrModuleName, 2, "Employee:")
            Language.setMessage(mstrModuleName, 3, "Period:")
            Language.setMessage(mstrModuleName, 6, "Transaction Heads(Earning and Deductions Amounts)")
            Language.setMessage(mstrModuleName, 7, "Hours Worked per Project Code in a Month")
            Language.setMessage(mstrModuleName, 8, "Project Code Allocation And Time in Percentages Spent on Each Project Code")
            Language.setMessage(mstrModuleName, 9, "Employee Code")
            Language.setMessage(mstrModuleName, 10, "Employee Name")
            Language.setMessage(mstrModuleName, 11, "Total Monthly Hours")
            Language.setMessage(mstrModuleName, 12, "Identify Number")
            Language.setMessage(mstrModuleName, 13, "Hours")
            Language.setMessage(mstrModuleName, 14, " Holidays Hours")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
