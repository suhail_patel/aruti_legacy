'************************************************************************************************************************************
'Class Name : clsEmployeeTimesheetReport.vb
'Purpose    :
'Date       :24/01/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsEmployeeTimesheetReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeTimesheetReport"
    Private mstrReportId As String = enArutiReport.EmployeeTimeSheet
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mintShiftunkid As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mblnIsActive As Boolean = True
    Private mstrShiftName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrEmployeeCode As String = ""
    Private mintDepartmentId As Integer = 0
    Private mstrDepartment As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnEachEmployeeOnPage As Boolean = False
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIsPolicyManagementTNA As Boolean = False
    Private mintAttendId As Integer = 0
    Private mstrAttCode As String = ""
    Private mblnShowTiming24HRS As Boolean = False
    Private mblnShowHoliday As Boolean = False
    Private mblnShowLeaveType As Boolean = False
    Private mblnShowBreakTime As Boolean = False
    Private mblnShowShift As Boolean = False
    Private mintYearID As Integer = -1
    Private mblnShowBaseHrs As Boolean = False
    Private mblnShowTotalAbsentDays As Boolean = False
    Private mdtDbStartDate As Date = Nothing
    Private mdtDbEndDate As Date = Nothing
    Private mblnIsPreviousDB As Boolean = False
    Private mblnIncludeAccessFilterQry As Boolean = True

    'Pinkal (08-Aug-2019) -- Start
    'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
    Private mblnIncludeOFFHrsinTotalBaseHrs As Boolean = True
    'Pinkal (08-Aug-2019) -- End

    'Pinkal (13-Sep-2021)-- Start
    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
    Private mblnShowAbsentAfterEmpTerminated As Boolean = True
    Private mblnShowLateComingEaryGoingAsShortHrs As Boolean = False
    Private mblnShowShortHrsOnBaseHrs As Boolean = False
    Private mblnShowActualTimingAfterRoundOff As Boolean = False
    Private mblnDisplayShortHrsWhenEmpAbsent As Boolean = False
    'Pinkal (13-Sep-2021) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public Property _Shiftunkid() As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _EmpCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _Department() As String
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _ShowEachEmployeeOnNewPage() As Boolean
        Set(ByVal value As Boolean)
            mblnEachEmployeeOnPage = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _IsPolicyManagementTNA() As Boolean
        Set(ByVal value As Boolean)
            mblnIsPolicyManagementTNA = value
        End Set
    End Property

    Public WriteOnly Property _AttenId() As Integer
        Set(ByVal value As Integer)
            mintAttendId = value
        End Set
    End Property

    Public WriteOnly Property _AttCode() As String
        Set(ByVal value As String)
            mstrAttCode = value
        End Set
    End Property

    Public WriteOnly Property _ShowTiming24Hrs() As Boolean
        Set(ByVal value As Boolean)
            mblnShowTiming24HRS = value
        End Set
    End Property

    Public WriteOnly Property _ShowHoliday() As Boolean
        Set(ByVal value As Boolean)
            mblnShowHoliday = value
        End Set
    End Property

    Public WriteOnly Property _ShowLeaveType() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLeaveType = value
        End Set
    End Property

    Public WriteOnly Property _ShowBreakTime() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBreakTime = value
        End Set
    End Property

    Public WriteOnly Property _ShowShift() As Boolean
        Set(ByVal value As Boolean)
            mblnShowShift = value
        End Set
    End Property

    Public WriteOnly Property _ShowBaseHrs() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBaseHrs = value
        End Set
    End Property

    Public WriteOnly Property _ShowTotalAbsentDays() As Boolean
        Set(ByVal value As Boolean)
            mblnShowTotalAbsentDays = value
        End Set
    End Property

    Public WriteOnly Property _DBstartDate() As Date
        Set(ByVal value As Date)
            mdtDbStartDate = value
        End Set
    End Property

    Public WriteOnly Property _DBEndDate() As Date
        Set(ByVal value As Date)
            mdtDbEndDate = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearID = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    'Pinkal (08-Aug-2019) -- Start
    'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.

    Public WriteOnly Property _IncludeOFFHrsinTotalBaseHrs() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeOFFHrsinTotalBaseHrs = value
        End Set
    End Property

    'Pinkal (08-Aug-2019) -- End

    'Pinkal (13-Sep-2021)-- Start
    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
    Public WriteOnly Property _ShowAbsentAfterEmpTerminated() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAbsentAfterEmpTerminated = value
        End Set
    End Property

    Public WriteOnly Property _ShowLateComingEaryGoingAsShortHrs() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLateComingEaryGoingAsShortHrs = value
        End Set
    End Property

    Public WriteOnly Property _ShowShortHrsOnBaseHrs() As Boolean
        Set(ByVal value As Boolean)
            mblnShowShortHrsOnBaseHrs = value
        End Set
    End Property

    Public WriteOnly Property _ShowActualTimingAfterRoundOff() As Boolean
        Set(ByVal value As Boolean)
            mblnShowActualTimingAfterRoundOff = value
        End Set
    End Property

    Public WriteOnly Property _DisplayShortHrsWhenEmpAbsent() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplayShortHrsWhenEmpAbsent = value
        End Set
    End Property
    'Pinkal (13-Sep-2021) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmpId = 0
            mintShiftunkid = 0
            mstrEmpName = ""
            mstrOrderByQuery = ""
            mblnIsActive = True
            mstrShiftName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrEmployeeCode = ""
            mintDepartmentId = 0
            mstrDepartment = ""
            mstrAdvanceFilter = ""
            mblnEachEmployeeOnPage = True
            mintAttendId = 0
            mstrAttCode = ""
            mblnShowTiming24HRS = False
            mblnShowHoliday = False
            mblnShowLeaveType = False
            mblnShowBreakTime = False
            mblnShowShift = False
            mblnShowBaseHrs = False
            mblnShowTotalAbsentDays = False
            mblnIncludeAccessFilterQry = True

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            mblnIncludeOFFHrsinTotalBaseHrs = True
            'Pinkal (08-Aug-2019) -- End

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            mblnShowAbsentAfterEmpTerminated = True
            mblnShowLateComingEaryGoingAsShortHrs = False
            mblnShowShortHrsOnBaseHrs = False
            mblnShowActualTimingAfterRoundOff = False
            mblnDisplayShortHrsWhenEmpAbsent = False
            'Pinkal (13-Sep-2021)-- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, " From Date: ") & " " & mdtFromDate.ToShortDateString & " "

            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, " To Date: ") & " " & mdtToDate.ToShortDateString & " "

            'Pinkal (10-Aug-2016) -- Start
            'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
            objDataOperation.AddParameter("@ABSENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "AB"))
            'Pinkal (10-Aug-2016) -- End

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= " AND tnalogin_summary.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 53, "Code : ") & " " & mstrEmployeeCode & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Employee : ") & " " & mstrEmpName & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 54, "Department : ") & " " & mstrDepartment & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mintShiftunkid > 0 Then
                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)
                Me._FilterQuery &= " AND tnashift_master.shiftunkid = @ShiftId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Shift : ") & " " & mstrShiftName & " "
            End If
            If mintAttendId <> 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 65, "AttCode : ") & " " & mstrAttCode & " "
            End If

            If Me.OrderByQuery <> "" Then
                'Pinkal (09-Jun-2015) -- Start
                'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                If mblnIsPolicyManagementTNA Then
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Else

                    'Pinkal (03-AUG-2015) -- Start
                    'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
                    If mblnIsPreviousDB = False Then

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'mstrOrderByQuery &= "  ORDER BY employeecode," & Me.OrderByQuery

                        'Pinkal (25-Jan-2018) -- Start
                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                        'mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery


                        'Pinkal (23-Apr-2020) -- Start
                        'Bug - 0004665: THE RESIDENCE ZANZIBAR : Not able to export Employee timesheet report when you sort by Employee code
                        If Me.OrderByQuery.ToString.Contains("employeecode") = False Then
                            mstrOrderByQuery &= "  ORDER BY  employeecode," & Me.OrderByQuery
                        Else
                            mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                        End If
                        'Pinkal (23-Apr-2020) -- End

                        'Pinkal (25-Jan-2018) -- end

                        'Shani(24-Aug-2015) -- End
                    End If
                    'Pinkal (03-AUG-2015) -- End
                End If
                'Pinkal (09-Jun-2015) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    If mblnIsPolicyManagementTNA <> ConfigParameter._Object._PolicyManagementTNA Then
        '        mblnIsPolicyManagementTNA = ConfigParameter._Object._PolicyManagementTNA
        '    End If

        '    If mblnIsPolicyManagementTNA Then
        '        objRpt = Generate_ShiftPolicy_DetailReport()
        '    Else
        '        objRpt = Generate_DetailReport()
        '    End If

        '    Rpt = objRpt
        '    '
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            If mblnIsPolicyManagementTNA Then
                objRpt = Generate_ShiftPolicy_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Else
                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If Company._Object._Name.ToString().ToUpper() = "NMB BANK PLC" Then
                    objRpt = Generate_NMBDetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                Else
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If
                'Pinkal (27-Aug-2020) -- End
            End If

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            iColumn_DetailReport.Add(New IColumn("login_date", Language.getMessage(mstrModuleName, 2, "Date")))

            If mblnIsPolicyManagementTNA = False Then
                iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 56, "Employee Code")))
                iColumn_DetailReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 1, "Employee")))
            End If

            iColumn_DetailReport.Add(New IColumn("checkintime", Language.getMessage(mstrModuleName, 3, "In Time")))
            iColumn_DetailReport.Add(New IColumn("checkouttime", Language.getMessage(mstrModuleName, 4, "Out Time")))
            iColumn_DetailReport.Add(New IColumn("breakhr", Language.getMessage(mstrModuleName, 5, "Break")))
            iColumn_DetailReport.Add(New IColumn("workhour", Language.getMessage(mstrModuleName, 6, "Working Hrs")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Sub As ArutiReport.Designer.dsArutiReport = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "No"))
            objDataOperation.AddParameter("@Holidays", SqlDbType.Int, eZeeDataType.INT_SIZE, GetHolidayandWeekend(mdtFromDate.Date, mdtToDate.Date, Company._Object._YearUnkid, mintEmpId, mintShiftunkid))



            'Pinkal (03-AUG-2015) -- Start
            'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.

            If mdtFromDate.Date < mdtDbStartDate.Date Then
                Dim mintCompanyID As Integer = 0
                Dim exForce As Exception = Nothing

                StrQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & mintYearID & "  "
                Dim dsCompany As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    mintCompanyID = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
                Else
                    'Pinkal (17-Dec-2015) -- Start
                    'Enhancement - Bug Solved when User Select From Date which was not fall in company Year.
                    StrQ = ""
                    'Pinkal (17-Dec-2015) -- End
                End If

                If mintCompanyID > 0 Then
                    Dim mstrPreviousDBName As String = ""
                    Dim mdtFYStartDate As Date = Nothing
                    Dim mdtFYEndDate As Date = Nothing

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompanyID & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
                    StrQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompanyID & " AND isclosed = 1 AND (start_date BETWEEN @stdate  AND @enddate or end_date BETWEEN @stdate  AND @enddate)  "
                    'Shani(24-Aug-2015) -- End
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromDate))

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate))
                    'Shani(24-Aug-2015) -- End

                    Dim dsDBName As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Pinkal (17-Dec-2015) -- Start
                    'Enhancement - Bug Solved when User Select From Date which was not fall in company Year.
                    If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count <= 0 Then
                        StrQ = ""
                    End If
                    'Pinkal (17-Dec-2015) -- End

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    For Each dRow As DataRow In dsDBName.Tables(0).Rows

                        Dim xprvDateJoinQry, xprvDateFilterQry, xprvUACQry, xprvUACFiltrQry, xprvAdvanceJoinQry As String

                        mstrPreviousDBName = dRow("database_name").ToString()
                        mdtFYStartDate = CDate(dRow("start_date"))
                        mdtFYEndDate = CDate(dRow("end_date"))
                        mblnIsPreviousDB = True

                        xprvDateJoinQry = xDateJoinQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvDateFilterQry = xDateFilterQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvUACQry = xUACQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvUACFiltrQry = xUACFiltrQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvAdvanceJoinQry = xAdvanceJoinQry.Replace(strDatabaseName, mstrPreviousDBName)

                        StrQ = " SELECT employeeunkid, employeecode,employee,isactive,shiftunkid,shiftname,Department " & _
                                  ",login_date,checkintime,checkouttime,breakhr,shifthour " & _
                                  ",CASE WHEN workhour = extrahrs THEN '00:00' ELSE workhour END AS workhour " & _
                                  ",shorthrs,extrahrs , nighthrs ,0 as TotReqHrs, TotWorkhrs " & _
                                  " ,TotShorthrs,TotExtrahrs,  TotNighthrs ,TotAbsent,holiday , TotBreakHrs, Id AS Id , GName AS GName " & _
                                  ", CASE WHEN isunpaidleave  = 1 THEN @ABSENT  ELSE '' END AttenCode "

                        'Pinkal (13-Sep-2021)-- Start
                        'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                        If mblnShowLateComingEaryGoingAsShortHrs Then
                            StrQ &= ",ltcomeearlygoing,Totalltcomeearlygoing "
                        End If
                        'Pinkal (13-Sep-2021)-- End

                        StrQ &= " FROM " & _
                                    " ( " & _
                                             " SELECT  tnalogin_summary.employeeunkid , " & _
                                             "ISNULL(hremployee_master.employeecode,'') AS employeecode, " & _
                                             "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                                             "+ ISNULL(hremployee_master.surname, '') AS employee , " & _
                                             " hremployee_master.isactive , "

                        If xprvUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " hremployee_master.jobunkid "
                        End If

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        'Pinkal (10-Aug-2016) -- Start
                        'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
                        StrQ &= ", tnalogin_summary.isunpaidleave "
                        'Pinkal (10-Aug-2016) -- End

                        StrQ &= ",ISNULL(tnashift_master.shiftunkid,0) AS shiftunkid , " & _
                                    " ISNULL(tnashift_master.shiftname,'') AS shiftname , " & _
                                    " ISNULL(dept.name,'') AS department, " & _
                                    " CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) AS login_date, " & _
                                    " ISNULL(logintran.checkintime,'') AS checkintime  , " & _
                                    " ISNULL(logintran.checkouttime,'') AS checkouttime , " & _
                                    " ISNULL(logintran.breakhr,'') AS breakhr , " & _
                                    " CASE WHEN  tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 THEN ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnashift_tran.workinghrs) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (SUM(tnashift_tran.workinghrs) % 3600) / 60), 2), '00:00') ELSE '00:00' END 'shifthour'," & _
                                    " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_hrs)/ 3600), 2) + ':' " & _
                                    " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.total_hrs)% 3600 ) / 60), 2), '00:00') AS workhour , " & _
                                    " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_short_hrs)/ 3600), 2) + ':' " & _
                                    " + RIGHT('00'+ CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_short_hrs)% 3600 ) / 60), 2), '00:00') AS shorthrs , " & _
                                    " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_overtime)/ 3600), 2) + ':' " & _
                                    " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.total_overtime)% 3600 ) / 60),2), '00:00') AS extrahrs, " & _
                                    " ISNULL(RIGHT('00'+ CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_nighthrs) / 3600), 2) + ':' " & _
                                    "+ RIGHT('00'+ CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_nighthrs) % 3600 ) / 60),2), '00:00') AS nighthrs , " & _
                                    " CONVERT(VARCHAR(max),(SELECT SUM(t.total_hrs)/3600 FROM  " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                                    " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_hrs)%3600/ 60)  FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS Totworkhrs, " & _
                                    " CONVERT(VARCHAR(max),(SELECT SUM(t.total_short_hrs)/3600 FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                                    " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_short_hrs)%3600/ 60)  FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotShorthrs, " & _
                                    " CONVERT(VARCHAR(max),(SELECT SUM(t.total_overtime)/3600 FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                                    " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_overtime)%3600/ 60)  FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotExtrahrs, " & _
                                    " CONVERT(VARCHAR(max),(SELECT SUM(t.total_nighthrs)/3600 FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                                    " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_nighthrs)%3600/ 60)  FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotNighthrs, " & _
                                    " CASE WHEN tnalogin_summary.isunpaidleave > 0  THEN 1 ELSE 0 END TotAbsent, '' AS holiday   "
                        StrQ &= ", (SELECT " & _
                                     " ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) % 3600 / 60), 2), '00:00')  " & _
                                     " FROM " & mstrPreviousDBName & "..tnalogin_Tran " & _
                                     " WHERE CONVERT(VARCHAR(8), logindate, 112) BETWEEN @FromDate AND @ToDate And tnalogin_tran.employeeunkid = tnalogin_summary.employeeunkid AND isvoid = 0 ) AS TotBreakHrs "

                        'Pinkal (13-Sep-2021)-- Start
                        'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                        If mblnShowLateComingEaryGoingAsShortHrs Then
                            StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.ltcoming_grace +  tnalogin_summary.elrgoing_grace) / 3600), 2) + ':' " & _
                                         " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.ltcoming_grace  + tnalogin_summary.elrgoing_grace)% 3600 ) / 60), 2), '00:00') AS ltcomeearlygoing  " & _
                                         ", CONVERT(VARCHAR(max),(SELECT SUM(t.ltcoming_grace +  t.elrgoing_grace) /3600 FROM  " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                                         " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.ltcoming_grace + t.elrgoing_grace)%3600/ 60)  FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS Totalltcomeearlygoing "
                        End If
                        'Pinkal (13-Sep-2021) -- End


                        StrQ &= " FROM  " & mstrPreviousDBName & "..tnalogin_summary " & _
                                     " JOIN " & mstrPreviousDBName & "..hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid "



                        'Pinkal (29-Nov-2017) -- Start
                        'Bug - WINDSOR GOLF - issue # 0001665: TnA attendance report displaying wrong department of employee.
                        StrQ &= " LEFT JOIN " & _
                                     " ( " & _
                                     " SELECT " & _
                                     "    departmentunkid " & _
                                     "   ,employeeunkid " & _
                                     "   ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                     " FROM " & mstrPreviousDBName & "..hremployee_transfer_tran " & _
                                     " WHERE isvoid = 0 	AND CONVERT(CHAR(8), effectivedate, 112) <= @ToDate) AS A " & _
                                     " ON a.employeeunkid = tnalogin_summary.employeeunkid AND A.rno = 1 " & _
                                     " LEFT JOIN " & mstrPreviousDBName & "..hrdepartment_master dept on dept.departmentunkid = A.departmentunkid "
                        'Pinkal (29-Nov-2017) -- End



                        StrQ &= "    JOIN " & mstrPreviousDBName & "..tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                                "     JOIN " & mstrPreviousDBName & "..tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                                "     LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid, " & _
                                "                   ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') checkintime , " & _
                                "                   ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '') checkouttime , " & _
                                "                   ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' " & _
                                "                   + RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr)% 3600 / 60), 2), '00:00') AS breakhr " & _
                                "                 FROM " & mstrPreviousDBName & "..tnalogin_Tran " & _
                                "                 WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid " & _
                                "               ) AS logintran " & _
                                "                       ON logintran.employeeunkid = tnalogin_summary.employeeunkid " & _
                                "                       AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  "

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Join
                        End If
                        If xprvDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xprvDateJoinQry
                        End If

                        If xprvUACQry.Trim.Length > 0 Then
                            StrQ &= xprvUACQry
                        End If

                        If xprvAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xprvAdvanceJoinQry
                        End If

                        StrQ &= " WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate  "

                        If xprvUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xprvUACFiltrQry & " "
                        End If

                        If mblnIsActive = False Then
                            If xprvDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xprvDateFilterQry & " "
                            End If
                        End If


                        If mstrAdvanceFilter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvanceFilter
                        End If

                        Call FilterTitleAndFilterQuery()

                        StrQ &= Me._FilterQuery

                        StrQ &= "GROUP BY ISNULL(hremployee_master.employeecode,''), " & _
                                    "ISNULL(hremployee_master.firstname, '') , " & _
                                    "ISNULL(hremployee_master.surname, '') , " & _
                                    "hremployee_master.isactive , " & _
                                    "ISNULL(tnashift_master.shiftname,'') ," & _
                                    "ISNULL(dept.name,''), " & _
                                    "tnalogin_summary.employeeunkid , " & _
                                    "CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) , " & _
                                    "tnalogin_summary.login_date, " & _
                                    "hremployee_master.firstname , " & _
                                    "hremployee_master.surname, " & _
                                    "tnashift_master.shiftunkid," & _
                                    "tnashift_tran.dayid, " & _
                                    "tnashift_tran.workinghrs, " & _
                                    "logintran.checkintime, " & _
                                    "logintran.checkouttime, " & _
                                    "logintran.breakhr "

                        If xprvUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= ",hremployee_master.jobunkid "
                        End If

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                        End If

                        StrQ &= ",tnalogin_summary.isunpaidleave " & _
                                  " ) AS ETS  WHERE 1 = 1 "

                        mblnIsPreviousDB = False

                        StrQ &= " UNION "
                    Next


                    'Shani(24-Aug-2015) -- End

                Else
                    'Pinkal (17-Dec-2015) -- Start
                    'Enhancement - Bug Solved when User Select From Date which was not fall in company Year.
                    StrQ = ""
                    'Pinkal (17-Dec-2015) -- End
                End If  ' If mintCompanyID > 0 Then
            End If
            'Pinkal (03-AUG-2015) -- End




            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

            'StrQ = " SELECT employeeunkid, employeecode,employee,isactive,shiftunkid,shiftname " & _
            '          ",login_date,checkintime,checkouttime,breakhr,shifthour " & _
            '          ",CASE WHEN workhour = extrahrs THEN '00:00' ELSE workhour END AS workhour " & _
            '          ",shorthrs,extrahrs , nighthrs ,0 as TotReqHrs " & _
            '          " , CONVERT(varchar(MAX),(CONVERT(INT,SUBSTRING(Totworkhrs,0,CHARINDEX(':',Totworkhrs))) * 3600   + CONVERT(INT,RIGHT(Totworkhrs,2)) * 60 - " & _
            '          " CONVERT(INT,SUBSTRING(TotExtrahrs,0,CHARINDEX(':',TotExtrahrs))) * 3600   + CONVERT(INT,RIGHT(TotExtrahrs,2)) * 60) / 3600 )  " & _
            '          " + ':' +  " & _
            '          " RIGHT('00' + CONVERT(VARCHAR(MAX),(CONVERT(INT,SUBSTRING(Totworkhrs,0,CHARINDEX(':',Totworkhrs))) * 3600   + CONVERT(INT,RIGHT(Totworkhrs,2)) * 60 - " & _
            '          " CONVERT(INT,SUBSTRING(TotExtrahrs,0,CHARINDEX(':',TotExtrahrs))) * 3600   + CONVERT(INT,RIGHT(TotExtrahrs,2))) %3600)/60,2) TotWorkhrs " & _
            '          " ,TotShorthrs,TotExtrahrs,  TotNighthrs ,TotAbsent,holiday "


            objDataOperation.ClearParameters()

            StrQ &= " SELECT employeeunkid, employeecode,employee,isactive,shiftunkid,shiftname,Department " & _
                     ",login_date,checkintime,checkouttime,breakhr,shifthour " & _
                     ",CASE WHEN workhour = extrahrs THEN '00:00' ELSE workhour END AS workhour " & _
                     ",shorthrs,extrahrs , nighthrs ,0 as TotReqHrs, TotWorkhrs " & _
                     " ,TotShorthrs,TotExtrahrs,  TotNighthrs ,TotAbsent,holiday , TotBreakHrs, Id AS Id , GName AS GName " & _
                     ", CASE WHEN isunpaidleave  = 1 THEN @ABSENT  ELSE '' END AttenCode "

            'Pinkal (09-Jun-2015) -- End

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If mblnShowLateComingEaryGoingAsShortHrs Then
                StrQ &= ",ltcomeearlygoing,Totalltcomeearlygoing "
            End If
            'Pinkal (13-Sep-2021)-- End


            StrQ &= " FROM " & _
                        " ( " & _
                                 " SELECT  tnalogin_summary.employeeunkid , " & _
                                 "ISNULL(hremployee_master.employeecode,'') AS employeecode, " & _
                                "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                                "+ ISNULL(hremployee_master.surname, '') AS employee , " & _
                                " hremployee_master.isactive "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= " hremployee_master.jobunkid "
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= ", hremployee_master.jobunkid "
            End If
            'Shani(24-Aug-2015) -- End

            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Pinkal (09-Jun-2015) -- End


            'Pinkal (10-Aug-2016) -- Start
            'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
            StrQ &= ", tnalogin_summary.isunpaidleave "
            'Pinkal (10-Aug-2016) -- End


            StrQ &= ",ISNULL(tnashift_master.shiftunkid,0) AS shiftunkid , " & _
                        " ISNULL(tnashift_master.shiftname,'') AS shiftname , " & _
                        " ISNULL(dept.name,'') AS department, " & _
                        " CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) AS login_date, " & _
                        " ISNULL(logintran.checkintime,'') AS checkintime  , " & _
                        " ISNULL(logintran.checkouttime,'') AS checkouttime , " & _
                        " ISNULL(logintran.breakhr,'') AS breakhr , " & _
                        " CASE WHEN  (tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1) AND tnalogin_summary.isweekend = 0 THEN ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnashift_tran.workinghrs) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (SUM(tnashift_tran.workinghrs) % 3600) / 60), 2), '00:00') ELSE '00:00' END 'shifthour'," & _
                        " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_hrs)/ 3600), 2) + ':' " & _
                        " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.total_hrs)% 3600 ) / 60), 2), '00:00') AS workhour , " & _
                        " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_short_hrs)/ 3600), 2) + ':' " & _
                        " + RIGHT('00'+ CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_short_hrs)% 3600 ) / 60), 2), '00:00') AS shorthrs , " & _
                        " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_overtime)/ 3600), 2) + ':' " & _
                        " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.total_overtime)% 3600 ) / 60),2), '00:00') AS extrahrs, " & _
                        " ISNULL(RIGHT('00'+ CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_nighthrs) / 3600), 2) + ':' " & _
                        "+ RIGHT('00'+ CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_nighthrs) % 3600 ) / 60),2), '00:00') AS nighthrs , " & _
                        " CONVERT(VARCHAR(max),(SELECT SUM(t.total_hrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                        " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_hrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS Totworkhrs, " & _
                        " CONVERT(VARCHAR(max),(SELECT SUM(t.total_short_hrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                        " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_short_hrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotShorthrs, " & _
                        " CONVERT(VARCHAR(max),(SELECT SUM(t.total_overtime)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                        " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_overtime)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotExtrahrs, " & _
                        " CONVERT(VARCHAR(max),(SELECT SUM(t.total_nighthrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                        " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_nighthrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS TotNighthrs, " & _
                        " CASE WHEN tnalogin_summary.isunpaidleave > 0  THEN 1 ELSE 0 END TotAbsent, '' AS holiday   "

            StrQ &= ", (SELECT " & _
                         " ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) % 3600 / 60), 2), '00:00') FROM tnalogin_Tran " & _
                         " WHERE CONVERT(VARCHAR(8), logindate, 112) BETWEEN @FromDate AND @ToDate And tnalogin_tran.employeeunkid = tnalogin_summary.employeeunkid AND isvoid = 0 ) AS TotBreakHrs "

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If mblnShowLateComingEaryGoingAsShortHrs Then
                StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.ltcoming_grace + tnalogin_summary.elrgoing_grace) / 3600), 2) + ':' " & _
                             " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.ltcoming_grace + tnalogin_summary.elrgoing_grace)% 3600 ) / 60), 2), '00:00') AS ltcomeearlygoing  " & _
                             ", CONVERT(VARCHAR(max),(SELECT SUM(t.ltcoming_grace + t.elrgoing_grace) /3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                             " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.ltcoming_grace +  t.elrgoing_grace)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS Totalltcomeearlygoing "
            End If
            'Pinkal (13-Sep-2021) -- End


            StrQ &= "FROM  tnalogin_summary " & _
                         "JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid "


            'Pinkal (29-Nov-2017) -- Start
            'Bug - WINDSOR GOLF - issue # 0001665: TnA attendance report displaying wrong department of employee.
            StrQ &= " LEFT JOIN " & _
                         " ( " & _
                         " SELECT " & _
                         "    departmentunkid " & _
                         "   ,employeeunkid " & _
                         "   ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         " FROM hremployee_transfer_tran " & _
                         " WHERE isvoid = 0 	AND CONVERT(CHAR(8), effectivedate, 112) <= @ToDate) AS A " & _
                         " ON a.employeeunkid = tnalogin_summary.employeeunkid AND A.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept on dept.departmentunkid = A.departmentunkid "
            'Pinkal (29-Nov-2017) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (09-Jun-2015) -- Start
            ''Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If
            ''Pinkal (09-Jun-2015) -- End

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If

            'Shani(24-Aug-2015) -- End

            StrQ &= "JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
            '                   "LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid, " & _
            '                                   "ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') checkintime , " & _
            '                                          "ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '') checkouttime , " & _
            '                                          "ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' " & _
            '                                                         "+ RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr)% 3600 / 60), 2), '00:00') AS breakhr " & _
            '                                    "FROM tnalogin_Tran " & _
            '                                    "WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid " & _
            '                    ") AS logintran " & _
            '                                  "ON logintran.employeeunkid = tnalogin_summary.employeeunkid " & _
            '                                       "AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  " & _
            '                        "WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate  "

            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvanceFilter & " "
            'End If

            StrQ &= "JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                               "LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid, " & _
                                               "ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') checkintime , " & _
                                                      "ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '') checkouttime , " & _
                                                      "ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' " & _
                                                                     "+ RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr)% 3600 / 60), 2), '00:00') AS breakhr " & _
                                                "FROM tnalogin_Tran " & _
                                                "WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid " & _
                                ") AS logintran " & _
                                              "ON logintran.employeeunkid = tnalogin_summary.employeeunkid " & _
                                                   "AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= "WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate  "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "GROUP BY ISNULL(hremployee_master.employeecode,''), " & _
                        "ISNULL(hremployee_master.firstname, '') , " & _
                        "ISNULL(hremployee_master.surname, '') , " & _
                        "hremployee_master.isactive , " & _
                        "ISNULL(tnashift_master.shiftname,'') ," & _
                        "ISNULL(dept.name,''), " & _
                        "tnalogin_summary.employeeunkid , " & _
                        "CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) , " & _
                        "tnalogin_summary.login_date, " & _
                        "hremployee_master.firstname , " & _
                        "hremployee_master.surname, " & _
                        "tnashift_master.shiftunkid," & _
                        "tnashift_tran.dayid, " & _
                        "tnashift_tran.workinghrs, " & _
                        "tnalogin_summary.isweekend , " & _
                        "logintran.checkintime, " & _
                        "logintran.checkouttime, " & _
                        "logintran.breakhr "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= "hremployee_master.jobunkid "
            'End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= ",hremployee_master.jobunkid "
            End If

            'Shani(24-Aug-2015) -- End

            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
            If mintViewIndex > 0 Then

                'Pinkal (06-Apr-2017) -- Start
                'Enhancement - Solved Error When Analysis by is using.
                'StrQ &= "," & mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                'Pinkal (06-Apr-2017) -- End
            End If
            'Pinkal (09-Jun-2015) -- End


            StrQ &= ",tnalogin_summary.isunpaidleave " & _
                      " ) AS ETS  WHERE 1 = 1 "

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim TotHolidays As Integer = 0
            Dim TotMispunch As Integer = 0
            Dim intEmployeID As Integer = 0
            Dim minTotalBaseHours As Integer = 0

            'Pinkal (10-Aug-2016) -- Start
            'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
            Dim objShitTran As New clsshift_tran
            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            Dim objEmpDates As New clsemployee_dates_tran
            'Pinkal (10-Aug-2016) -- End

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim mStrHoliday As String = ""
                Dim mStrLeaveType As String = ""

                'Pinkal (16-May-2019) -- Start
                'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type
                Dim mStrLeaveTypeCode As String = ""
                'Pinkal (16-May-2019) -- End

                Dim arstr() As String = Nothing


                If mblnShowHoliday OrElse mblnShowLeaveType Then
                    arstr = GetHolidayName(CInt(dtRow.Item("employeeunkid")), dtRow.Item("login_date").ToString).ToString().Split(",")
                    mStrHoliday = arstr(0).ToString()

                    If mStrHoliday.ToString.Length > 0 Then
                        If CInt(dtRow.Item("TotAbsent")) > 0 Then
                            dtRow.Item("TotAbsent") = CInt(dtRow.Item("TotAbsent")) - 1
                        End If
                        TotHolidays += 1
                    End If

                    mStrLeaveType = arstr(1).ToString()

                    'Pinkal (16-May-2019) -- Start
                    'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type
                    mStrLeaveTypeCode = arstr(2).ToString()
                    'Pinkal (16-May-2019) -- End


                    If mStrLeaveType.ToString().Length > 0 Then
                        If CInt(dtRow.Item("TotAbsent")) > 0 Then
                            dtRow.Item("TotAbsent") = CInt(dtRow.Item("TotAbsent")) - 1
                        End If
                    End If

                End If

                objShitTran.GetShiftTran(CInt(dtRow.Item("shiftunkid")))
                Dim drWeekend() As DataRow = objShitTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(eZeeDate.convertDate(dtRow.Item("login_date").ToString).Date), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 1")

                If drWeekend.Length > 0 Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")
                End If


                Dim mblnDayOff As Boolean = objEmpDayOff.isExist(eZeeDate.convertDate(dtRow.Item("login_date").ToString).Date, CInt(dtRow.Item("employeeunkid")), -1, Nothing)
                If mblnDayOff Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 38, "OF")
                End If

                If arstr IsNot Nothing Then mStrHoliday = arstr(0).ToString()

                If mStrHoliday.ToString.Length > 0 Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL")
                End If


                'Pinkal (16-May-2019) -- Start
                'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type
                If arstr IsNot Nothing AndAlso arstr.Length > 0 Then
                    mStrLeaveType = arstr(1).ToString()
                    mStrLeaveTypeCode = arstr(2).ToString()
                End If
                'Pinkal (16-May-2019) -- End


                If mStrLeaveType.ToString().Length > 0 Then
                    'Pinkal (16-May-2019) -- Start
                    'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type
                    'dtRow.Item("AttenCode") = mStrLeaveType
                    dtRow.Item("AttenCode") = mStrLeaveTypeCode
                    'Pinkal (16-May-2019) -- End
                End If


                Dim dsDates As DataSet = objEmpDates.Get_Current_Dates(eZeeDate.convertDate(dtRow.Item("login_date").ToString), enEmp_Dates_Transaction.DT_SUSPENSION, CInt(dtRow.Item("employeeunkid")))
                If dsDates IsNot Nothing AndAlso dsDates.Tables(0).Rows.Count > 0 Then
                    'Pinkal (10-Mar-2021) -- Start
                    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                    If CInt(dtRow.Item("login_date").ToString) >= CInt(dsDates.Tables(0).Rows(0)("ddate1").ToString()) AndAlso CInt(dtRow.Item("login_date").ToString()) <= CInt(dsDates.Tables(0).Rows(0)("ddate2").ToString()) Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 70, "Sup")
                End If
                    'Pinkal (10-Mar-2021) -- End
                End If

                'Pinkal (10-Aug-2016) -- End


                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employee")


                rpt_Row.Item("Column4") = WeekdayName(Weekday(eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString), True, FirstDayOfWeek.Sunday)
                Dim mstrFormart As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString()
                If System.Text.RegularExpressions.Regex.Matches(mstrFormart, "M", System.Text.RegularExpressions.RegexOptions.IgnoreCase).Count <= 1 Then
                    mstrFormart = mstrFormart.Replace("M", "MM")
                End If
                If System.Text.RegularExpressions.Regex.Matches(mstrFormart, "d", System.Text.RegularExpressions.RegexOptions.IgnoreCase).Count <= 1 Then
                    mstrFormart = mstrFormart.Replace("d", "dd")
                End If
                rpt_Row.Item("Column30") = eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToString(mstrFormart)

                If intEmployeID <> CInt(dtRow.Item("employeeunkid")) Then
                    intEmployeID = CInt(dtRow.Item("employeeunkid"))
                    TotMispunch = 0

                    'Pinkal (08-Aug-2019) -- Start
                    'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                    minTotalBaseHours = 0
                    'Pinkal (08-Aug-2019) -- End

                End If

                If dtRow.Item("checkintime").ToString <> "" Then
                    If mblnShowTiming24HRS Then
                        rpt_Row.Item("Column5") = CDate(dtRow.Item("checkintime")).ToString("HH:mm")
                    Else
                        rpt_Row.Item("Column5") = CDate(dtRow.Item("checkintime")).ToShortTimeString
                    End If
                ElseIf dtRow.Item("checkouttime").ToString <> "" Then
                    rpt_Row.Item("Column5") = "***"
                End If

                If dtRow.Item("checkouttime").ToString <> "" Then
                    If mblnShowTiming24HRS Then
                        rpt_Row.Item("Column6") = CDate(dtRow.Item("checkouttime")).ToString("HH:mm")
                    Else
                        rpt_Row.Item("Column6") = CDate(dtRow.Item("checkouttime")).ToShortTimeString
                    End If

                ElseIf dtRow.Item("checkintime").ToString <> "" Then
                    rpt_Row.Item("Column6") = "***"
                    If CInt(dtRow.Item("TotAbsent")) > 0 Then
                        dtRow.Item("TotAbsent") = CInt(dtRow.Item("TotAbsent")) - 1
                    End If
                    TotMispunch += 1
                End If

                rpt_Row.Item("Column7") = dtRow.Item("breakhr")
                rpt_Row.Item("Column8") = dtRow.Item("workhour")

                'Pinkal (13-Sep-2021)-- Start
                'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                If mblnShowLateComingEaryGoingAsShortHrs Then
                    rpt_Row.Item("Column9") = dtRow.Item("ltcomeearlygoing")
                Else
                rpt_Row.Item("Column9") = dtRow.Item("shorthrs")
                End If
                'Pinkal (13-Sep-2021) -- End


                rpt_Row.Item("Column10") = dtRow.Item("extrahrs")

                'Pinkal (08-Aug-2019) -- Start
                'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                'rpt_Row.Item("Column12") = CDbl(GetTotalRequirehrs(mdtFromDate.Date, mdtToDate.Date, CInt(dtRow.Item("shiftunkid")))).ToString("#0.00")
                minTotalBaseHours += GetEmployeeShiftHours(CInt(dtRow.Item("employeeunkid")), eZeeDate.convertDate(dtRow.Item("login_date")).Date, CInt(dtRow.Item("shiftunkid")))
                'Pinkal (08-Aug-2019) -- End


                rpt_Row.Item("Column13") = dtRow.Item("Totworkhrs")

                'Pinkal (13-Sep-2021)-- Start
                'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                If mblnShowLateComingEaryGoingAsShortHrs Then
                    rpt_Row.Item("Column14") = dtRow.Item("Totalltcomeearlygoing")
                Else
                rpt_Row.Item("Column14") = dtRow.Item("TotShorthrs")
                End If
                'Pinkal (13-Sep-2021) -- End


                rpt_Row.Item("Column15") = dtRow.Item("TotExtrahrs")
                rpt_Row.Item("Column17") = dtRow.Item("TotAbsent")
                rpt_Row.Item("Column18") = mStrHoliday
                rpt_Row.Item("Column19") = dtRow.Item("shiftname")
                rpt_Row.Item("Column20") = dtRow.Item("shifthour")
                rpt_Row.Item("Column21") = dtRow.Item("nighthrs")
                rpt_Row.Item("Column22") = dtRow.Item("TotNighthrs")


                'Pinkal (08-Aug-2019) -- Start
                'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                If mblnIncludeOFFHrsinTotalBaseHrs = False AndAlso dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 38, "OF") Then
                    rpt_Row.Item("Column20") = ""
                    minTotalBaseHours = minTotalBaseHours - GetEmployeeShiftHours(CInt(dtRow.Item("employeeunkid")), eZeeDate.convertDate(dtRow.Item("login_date")).Date, CInt(dtRow.Item("shiftunkid")))
                End If
                rpt_Row.Item("Column12") = CalculateTime(True, minTotalBaseHours).ToString("#0.00")
                'Pinkal (08-Aug-2019) -- End



                If mblnShowLeaveType Then
                    rpt_Row.Item("Column23") = mStrLeaveType
                End If

                If mblnShowHoliday Then
                    rpt_Row.Item("Column24") = TotHolidays
                End If

                rpt_Row.Item("Column25") = dtRow.Item("TotBreakhrs")
                rpt_Row.Item("Column26") = dtRow.Item("Id")
                rpt_Row.Item("Column27") = dtRow.Item("GName")
                rpt_Row.Item("Column28") = dtRow.Item("Department")
                rpt_Row.Item("Column29") = TotMispunch

                'Pinkal (13-Jan-2017) -- Start
                'Enhancement -Solved Bug for TRZ when DayOFF not displyed (Residance Zanzibar) .
                If dtRow.Item("AttenCode").ToString().Length <= 0 AndAlso (dtRow.Item("checkintime").ToString().Trim.Length <= 0 OrElse dtRow.Item("checkouttime").ToString().Trim.Length <= 0) Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 39, "AB")
                    rpt_Row.Item("Column31") = Language.getMessage(mstrModuleName, 39, "AB")
                End If
                'Pinkal (13-Jan-2017) -- End


                If dtRow.Item("AttenCode").ToString().Length <= 0 Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 41, "PR")
                    rpt_Row.Item("Column31") = Language.getMessage(mstrModuleName, 41, "PR")
                Else
                    rpt_Row.Item("Column31") = dtRow.Item("AttenCode")
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            If mintAttendId <> 0 Then
                Dim mdtTable As DataTable = New DataView(rpt_Data.Tables("ArutiTable"), "Column31='" & mstrAttCode & "'", "", DataViewRowState.CurrentRows).ToTable
                rpt_Data.Tables.Remove("ArutiTable")
                rpt_Data.Tables.Add(mdtTable)
            End If

            If mblnShowLeaveType Then

                StrQ = " SELECT ISNULL(lvleavetype_master.leavename,'') LeaveName,ISNULL(SUM(lvleaveIssue_tran.dayfraction),0) AS Leaveday " & _
                           " FROM hremployee_master " & _
                           " JOIN lvleaveIssue_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                           " JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                           " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND (leavedate >= @FromDate AND leavedate <= @ToDate) " & _
                           " WHERE hremployee_master.employeeunkid = @EmpId GROUP BY lvleavetype_master.leavename"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId.ToString)
                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                dsList = objDataOperation.ExecQuery(StrQ, "LeaveInfo")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                rpt_Sub = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("LeaveInfo").Rows
                    Dim rpt_Row1 As DataRow
                    rpt_Row1 = rpt_Sub.Tables("ArutiTable").NewRow

                    rpt_Row1.Item("Column1") = dtRow.Item("LeaveName")
                    rpt_Row1.Item("Column2") = dtRow.Item("Leaveday")

                    rpt_Sub.Tables("ArutiTable").Rows.Add(rpt_Row1)
                Next

            End If

            'END FOR SUBREPORT


            objRpt = New ArutiReport.Designer.rptEmployeeTimeSheet

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 30, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 31, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 32, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 33, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            If mblnShowLeaveType Then
                objRpt.Subreports("rptEmpLeaveType").SetDataSource(rpt_Sub)
            End If
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 24, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 2, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 3, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 4, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtWorkinghrs", Language.getMessage(mstrModuleName, 6, "Working Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtShorthrs", Language.getMessage(mstrModuleName, 9, "Short Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtExtrahrs", Language.getMessage(mstrModuleName, 10, "Extra Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtNighthrs", Language.getMessage(mstrModuleName, 34, "Night Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 27, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtAttCode", Language.getMessage(mstrModuleName, 49, "AttCode"))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If

            If mblnShowLeaveType Then
                Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 36, "Leave Type"))
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", False)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtLeaveType", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column231", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            If mblnShowHoliday Then
                Call ReportFunction.TextChange(objRpt, "txtholiday", Language.getMessage(mstrModuleName, 12, "Holiday"))
                Call ReportFunction.TextChange(objRpt, "lblTotHoliday", Language.getMessage(mstrModuleName, 37, "Total Holidays"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtholiday", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column181", True)
                Call ReportFunction.EnableSuppress(objRpt, "lblTotHoliday", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column241", True)
            End If



            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection2", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column27}) <> {ArutiTable.Column27} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection2", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            Call ReportFunction.TextChange(objRpt, "txtMispunch", "*** : " & Language.getMessage(mstrModuleName, 66, "Mispunch"))

            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 54, "Department :"))


            If mblnShowBreakTime = True Then
                Call ReportFunction.TextChange(objRpt, "txtBreakhrs", Language.getMessage(mstrModuleName, 5, "Break"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtBreakhrs", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column251", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column71", True)
            End If

            If mblnShowShift = True Then
                Call ReportFunction.TextChange(objRpt, "txtShift", Language.getMessage(mstrModuleName, 28, "Shift :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtShift", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column191", True)
            End If

            Call ReportFunction.TextChange(objRpt, "lblTotMispunch", Language.getMessage(mstrModuleName, 68, "Total Mispunch Days"))

            'Pinkal (09-Jun-2015) -- End

            If mblnShowBaseHrs = True Then
                Call ReportFunction.TextChange(objRpt, "txtShifthour", Language.getMessage(mstrModuleName, 44, "Base Hrs"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtShifthour", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column201", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column112", True)
            End If

            If mblnShowTotalAbsentDays = True Then
                Call ReportFunction.TextChange(objRpt, "lblTotAbsent", Language.getMessage(mstrModuleName, 18, "Total Absent days"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblTotAbsent", True)
                Call ReportFunction.EnableSuppress(objRpt, "frmColTot171", True)
            End If

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 19, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 20, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 21, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Generate_ShiftPolicy_DetailReport(ByVal strDatabaseName As String, _
                                                       ByVal intUserUnkid As Integer, _
                                                       ByVal intYearUnkid As Integer, _
                                                       ByVal intCompanyUnkid As Integer, _
                                                       ByVal dtPeriodStart As Date, _
                                                       ByVal dtPeriodEnd As Date, _
                                                       ByVal strUserModeSetting As String, _
                                                       ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@Holidays", SqlDbType.Int, eZeeDataType.INT_SIZE, GetHolidayandWeekend(mdtFromDate.Date, mdtToDate.Date, Company._Object._YearUnkid, mintEmpId, mintShiftunkid))


            StrQ = " SELECT ROW_NUMBER() OVER (ORDER BY login_date) AS SrNo " & _
                       " , employeeunkid " & _
                       ", employeecode " & _
                       ", employee " & _
                       ", shiftunkid " & _
                       ", shiftcode " & _
                       ", shiftname " & _
                       ", department " & _
                       ", login_date " & _
                       ", LTRIM(checkintime) AS  checkintime " & _
                       ", LTRIM(checkouttime) AS checkouttime " & _
                       ", breakhr " & _
                       ", Totalbreakhr " & _
                       ", teahr " & _
                       ", Totalteahr " & _
                       ", basehrs " & _
                       ", workhour " & _
                       ", TotalHrs " & _
                       ", ot1 " & _
                       ", TotalOt1 " & _
                       ", ot2 " & _
                       ", TotalOt2 " & _
                       ", ot3 " & _
                       ", TotalOt3 " & _
                       ", ot4 " & _
                       ", TotalOt4 " & _
                       ", CASE WHEN isunpaidleave  = 1 THEN @ABSENT  ELSE '' END AttenCode " & _
                       ", CASE WHEN isunpaidleave  = 1 THEN -2 ELSE 0 END AS AttenId " & _
                       ", Id AS Id " & _
                       ", GName AS GName " & _
                       ", ISNULL(ETS.EditCount,0) AS EditCount " & _
                       ", workinghrs " & _
                       ", basehrsinsec "

            'Pinkal (01-Mar-2022) -- Bug :  Issue Resolved for Biocorn to display total short hrs.[", basehrsinsec "]

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.

            If mblnShowAbsentAfterEmpTerminated = False Then
                StrQ &= ", EOC , LEAVING , RETIRE , REHIRE "
            End If

            If mblnShowShortHrsOnBaseHrs AndAlso mblnShowLateComingEaryGoingAsShortHrs Then
                StrQ &= ", ltcomeearlygoing ,Totalltcomeearlygoing "
            End If
            'Pinkal (13-Sep-2021)-- End


            StrQ &= " FROM    " & _
                       "( SELECT    tnalogin_summary.employeeunkid " & _
                       " , ISNULL(hremployee_master.employeecode, '' ) AS employeecode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  + ' ' + ISNULL(hremployee_master.surname, '') AS employee "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= ", hremployee_master.jobunkid "
            End If


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= ", ISNULL(tnashift_master.shiftunkid, 0) AS shiftunkid " & _
                         ", ISNULL(tnashift_master.shiftcode, '') AS shiftcode " & _
                         ", ISNULL(tnashift_master.shiftname, '') AS shiftname " & _
                         ", ISNULL(Dm.name, '') AS department " & _
                         ", CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) AS login_date " & _
                         ", ISNULL(CASE WHEN atlogintran.checkintime IS NULL THEN logintran.checkintime ELSE logintran.checkintime + ' ~' END , '00:00') AS checkintime " & _
                         ", ISNULL(CASE WHEN atlogintran.checkouttime IS NULL THEN logintran.checkouttime ELSE logintran.checkouttime + ' ~' END , '00:00') AS checkouttime " & _
                         ", ISNULL(logintran.breakhr, '00:00') AS breakhr " & _
                         ", ISNULL(SUM(logintran.Totalbreakhr),0) AS Totalbreakhr  " & _
                         " , ISNULL(logintran.teahr, '00:00') AS teahr " & _
                         ", ISNULL(SUM(logintran.Totalteahr), 0) AS Totalteahr  " & _
                         ", tnashift_tran.workinghrs "


            'Pinkal (01-Mar-2022) -- Start
            'Bug :  Issue Resolved for Biocorn to display total short hrs.
            'If mblnIsPolicyManagementTNA Then
            '    StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), (tnashift_tran.workinghrs - tnapolicy_tran.breaktime) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( (tnashift_tran.workinghrs - tnapolicy_tran.breaktime) % 3600 ) / 60), 2), '00:00') AS basehrs "
            'Else
            '    StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), tnashift_tran.workinghrs / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( tnashift_tran.workinghrs % 3600 ) / 60), 2), '00:00') AS basehrs "
            'End If
            If mblnIsPolicyManagementTNA Then
                StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), (tnashift_tran.workinghrs - tnapolicy_tran.breaktime) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( (tnashift_tran.workinghrs - tnapolicy_tran.breaktime) % 3600 ) / 60), 2), '00:00') AS basehrs " & _
                             ", ISNULL(tnashift_tran.workinghrs - tnapolicy_tran.breaktime, 0) AS basehrsinsec "
            Else
                StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), tnashift_tran.workinghrs / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( tnashift_tran.workinghrs % 3600 ) / 60), 2), '00:00') AS basehrs " & _
                             ", ISNULL(tnashift_tran.workinghrs, 0) AS basehrsinsec "
            End If
            'Pinkal (01-Mar-2022) -- End


            StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.total_hrs) / 3600), 2) + ':' + RIGHT('00'  + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_hrs) % 3600 ) / 60), 2), '00:00') AS workhour " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.total_overtime) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_overtime) % 3600 ) / 60), 2) AS ot1 " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ot2) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ot2) % 3600 ) / 60), 2) AS ot2 " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ot3) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ot3) % 3600 ) / 60), 2) AS ot3 " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ot4)  / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ot4) % 3600 ) / 60),2) AS ot4 " & _
                         ", tnalogin_summary.isunpaidleave " & _
                         ", SUM(tnalogin_summary.total_hrs) AS TotalHrs " & _
                         ", SUM(tnalogin_summary.total_overtime) AS TotalOt1" & _
                         ", SUM(tnalogin_summary.ot2) AS TotalOt2" & _
                         ", SUM(tnalogin_summary.ot3) AS TotalOt3" & _
                         ", SUM(tnalogin_summary.ot4) AS TotalOt4" & _
                         ", atlogintran.EditCount "

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.

            If mblnShowAbsentAfterEmpTerminated = False Then
                StrQ &= ",TRM.EOC,TRM.LEAVING,RET.RETIRE	,HIRE.REHIRE "
            End If

            If mblnShowLateComingEaryGoingAsShortHrs Then
                StrQ &= ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ltcoming_grace +  tnalogin_summary.elrgoing_grace) / 3600), 2) + ':' + RIGHT('00'  + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ltcoming_grace +  tnalogin_summary.elrgoing_grace) % 3600 ) / 60), 2), '00:00') AS ltcomeearlygoing " & _
                             ", SUM(tnalogin_summary.ltcoming_grace +  tnalogin_summary.elrgoing_grace) AS Totalltcomeearlygoing "
            End If
            'Pinkal (13-Sep-2021)-- End

            StrQ &= " FROM tnalogin_summary " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid "

            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran "


            StrQ &= "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @ToDate "

            StrQ &= "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master Dm ON Alloc.departmentunkid = Dm.departmentunkid " & _
                         " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                         " JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                         " LEFT JOIN ( " & _
                    "           SELECT  " & _
                    "               tnalogin_tran.logindate " & _
                         "               ,employeeunkid,holdunkid "



            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If mblnShowActualTimingAfterRoundOff Then

                StrQ &= ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') " & _
                           "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') <>  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') THEN  " & _
                           "            ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') + ' *'  " & _
                           "  ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') END checkintime  " & _
                           ", CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  " & _
                           "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  <> 	ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') THEN " & _
                           "           ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') + ' *'" & _
                           "  ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') END checkouttime  "


            Else
                StrQ &= ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') " & _
                                                      "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') <>  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') THEN  " & _
                                                      "            ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') + ' *'  " & _
                                                      "  ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') END checkintime  " & _
                                                      ", CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  " & _
                                                      "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  <> 	ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') THEN " & _
                                                      "           ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') + ' *'" & _
                             "  ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') END checkouttime  "
            End If

            StrQ &= ", ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' + RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr)% 3600 / 60), 2), '00:00') AS breakhr " & _
                                                      ", SUM(tnalogin_tran.breakhr) AS Totalbreakhr " & _
                                                      ", ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.teahr) / 3600), 2) + ':' + RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.teahr)% 3600 / 60), 2), '00:00') AS teahr " & _
                                                      ", SUM(tnalogin_tran.teahr) AS Totalteahr " & _
                                                      " FROM tnalogin_Tran " & _
                                                      " WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid,holdunkid " & _
                    "               ) AS logintran ON logintran.employeeunkid = tnalogin_summary.employeeunkid " & _
                         "  AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  "

            'Pinkal (13-Sep-2021)-- End

            If mblnIsPolicyManagementTNA Then
                StrQ &= "JOIN tnapolicy_master ON tnapolicy_master.policyunkid = tnalogin_summary.policyunkid AND tnapolicy_master.isvoid = 0 " & _
                             "JOIN tnapolicy_tran ON tnapolicy_tran.policyunkid = tnapolicy_master.policyunkid AND tnapolicy_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 "
            End If

            StrQ &= " LEFT JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "          attnalogin_tran.logindate " & _
                         "         ,employeeunkid " & _
                         "         ,1 AS EditCount " & _
                         "         ,RIGHT(CONVERT(VARCHAR(20), MIN(attnalogin_Tran.checkintime), 100), 8) AS checkintime " & _
                         "         ,RIGHT(CONVERT(VARCHAR(20), MAX(attnalogin_Tran.checkouttime), 100), 8) AS checkouttime " & _
                         "         FROM attnalogin_Tran " & _
                         "         WHERE CONVERT(VARCHAR(8), logindate, 112) BETWEEN @FromDate AND @ToDate " & _
                         "         AND sourcetype  = 5 AND audittype = 2 " & _
                         "         GROUP BY logindate ,employeeunkid " & _
                         "  ) AS atlogintran " & _
                         " ON atlogintran.employeeunkid = tnalogin_summary.employeeunkid " & _
                         " AND CONVERT(VARCHAR(8), atlogintran.logindate, 112) = CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) AND tnalogin_summary.total_hrs > 0 "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate  "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "  GROUP BY  CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) " & _
                         "  , ISNULL(hremployee_master.employeecode, '')" & _
                         "  , ISNULL(hremployee_master.firstname, '') " & _
                         "  , ISNULL(hremployee_master.othername, '') " & _
                         "  , ISNULL(hremployee_master.surname, '') " & _
                         "  , ISNULL(tnashift_master.shiftcode, '') " & _
                         "  , ISNULL(tnashift_master.shiftname, '') " & _
                         "  , tnalogin_summary.employeeunkid " & _
                         "  , DM.departmentunkid " & _
                         "  , DM.name " & _
                         "  , hremployee_master.firstname " & _
                         "  , hremployee_master.surname " & _
                         "  , tnashift_master.shiftunkid " & _
                         "  , tnashift_tran.workinghrs " & _
                         "  , logintran.checkintime " & _
                         "  , logintran.checkouttime " & _
                         "  , logintran.breakhr "

            If mblnIsPolicyManagementTNA Then

                StrQ &= ",tnapolicy_tran.breaktime " & _
                             ",logintran.teahr "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= ",hremployee_master.jobunkid "
            End If

            StrQ &= " ,atlogintran.checkintime " & _
                         " ,atlogintran.checkouttime " & _
                         " ,atlogintran.EditCount "

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If mblnShowAbsentAfterEmpTerminated = False Then
                StrQ &= ",TRM.EOC,TRM.LEAVING,RET.RETIRE	,HIRE.REHIRE "
            End If
            'Pinkal (13-Sep-2021)-- End

            StrQ &= ",tnalogin_summary.isunpaidleave " & _
                      " ) AS ETS  WHERE 1 = 1 "


            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mintTotWorkhrs As Integer = 0
            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            Dim objShitTran As New clsshift_tran
            Dim minTotBreakhrs As Integer = 0
            Dim minTotTeahrs As Integer = 0
            Dim mintOT1 As Integer = 0
            Dim mintOT2 As Integer = 0
            Dim mintOT3 As Integer = 0
            Dim mintOT4 As Integer = 0
            Dim count As Integer = 0

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            Dim minTotalShorthrs As Integer = 0
            'Pinkal (13-Sep-2021)-- End


            'Pinkal (12-Jan-2022)-- Start
            'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
            Dim mstrGroupName As String = ""
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroupName = objGroup._Groupname
            objGroup = Nothing
            'Pinkal (12-Jan-2022) -- End


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                'Pinkal (13-Sep-2021)-- Start
                'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                If mblnShowAbsentAfterEmpTerminated = False Then
                    Dim objEmpDates As New clsemployee_dates_tran
                    Dim dsEmpTermination As DataSet = objEmpDates.Get_Current_Dates(eZeeDate.convertDate(dtRow.Item("login_date").ToString).Date, enEmp_Dates_Transaction.DT_TERMINATION, CInt(dtRow.Item("employeeunkid")), Nothing, "")
                    If dsEmpTermination IsNot Nothing AndAlso dsEmpTermination.Tables(0).Rows.Count > 0 Then
                        'Pinkal (01-Feb-2022) -- Start
                        'Enhancement NMB  - Language Change Issue for All Modules.	
                        'If eZeeDate.convertDate(CDate(dsEmpTermination.Tables(0).Rows(0)("date1")).Date) < dtRow.Item("login_date").ToString() Then Continue For
                        'If eZeeDate.convertDate(CDate(dsEmpTermination.Tables(0).Rows(0)("date2")).Date) < dtRow.Item("login_date").ToString() Then Continue For
                        If IsDBNull(dsEmpTermination.Tables(0).Rows(0)("date1")) = False AndAlso dsEmpTermination.Tables(0).Rows(0)("date1") <> Nothing AndAlso eZeeDate.convertDate(CDate(dsEmpTermination.Tables(0).Rows(0)("date1")).Date) < dtRow.Item("login_date").ToString() Then Continue For
                        If IsDBNull(dsEmpTermination.Tables(0).Rows(0)("date2")) = False AndAlso dsEmpTermination.Tables(0).Rows(0)("date2") <> Nothing AndAlso eZeeDate.convertDate(CDate(dsEmpTermination.Tables(0).Rows(0)("date2")).Date) < dtRow.Item("login_date").ToString() Then Continue For
                        'Pinkal (01-Feb-2022) -- End

                    End If
                    dsEmpTermination.Clear()
                    dsEmpTermination = Nothing

                    Dim dsEmpRetirememt As DataSet = objEmpDates.Get_Current_Dates(eZeeDate.convertDate(dtRow.Item("login_date").ToString).Date, enEmp_Dates_Transaction.DT_RETIREMENT, CInt(dtRow.Item("employeeunkid")), Nothing, "")
                    If dsEmpRetirememt IsNot Nothing AndAlso dsEmpRetirememt.Tables(0).Rows.Count > 0 Then
                        If eZeeDate.convertDate(CDate(dsEmpRetirememt.Tables(0).Rows(0)("date1")).Date) < dtRow.Item("login_date").ToString() Then Continue For
                    End If
                    dsEmpRetirememt.Clear()
                    dsEmpRetirememt = Nothing

                    objEmpDates = Nothing
                End If
                'Pinkal (13-Sep-2021)-- End


                Dim blnWeekend As Boolean = False

                objShitTran.GetShiftTran(CInt(dtRow.Item("shiftunkid")))
                Dim drWeekend() As DataRow = objShitTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(eZeeDate.convertDate(dtRow.Item("login_date").ToString).Date), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 1")

                If drWeekend.Length > 0 Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")
                    blnWeekend = True
                End If


                Dim mblnDayOff As Boolean = objEmpDayOff.isExist(eZeeDate.convertDate(dtRow.Item("login_date").ToString).Date, CInt(dtRow.Item("employeeunkid")), -1, Nothing)
                If mblnDayOff Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 38, "OF")
                End If

                Dim arstr() As String = GetHolidayName(CInt(dtRow.Item("employeeunkid")), dtRow.Item("login_date").ToString).ToString().Split(",")
                Dim mStrHoliday As String = arstr(0).ToString()
                If mStrHoliday.ToString.Length > 0 Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL")
                End If

                'Pinkal (16-May-2019) -- Start
                'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type
                'Dim mStrLeaveType As String = arstr(1).ToString()
                'If mStrLeaveType.ToString().Length > 0 Then
                'dtRow.Item("AttenCode") = mStrLeaveType
                'End If
                Dim mStrLeaveTypeCode As String = arstr(2).ToString()
                If mStrLeaveTypeCode.ToString().Length > 0 Then
                    dtRow.Item("AttenCode") = mStrLeaveTypeCode
                End If
                'Pinkal (16-May-2019) -- End


                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                count += 1

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employee")
                rpt_Row.Item("Column4") = dtRow.Item("shiftcode")
                rpt_Row.Item("Column5") = WeekdayName(Weekday(eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString), True, FirstDayOfWeek.Sunday) & "  " & eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString

                If dtRow.Item("AttenCode").ToString().Length <= 0 Then
                    dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 41, "PR")
                    rpt_Row.Item("Column6") = Language.getMessage(mstrModuleName, 41, "PR")
                Else
                    rpt_Row.Item("Column6") = dtRow.Item("AttenCode")
                End If


                If dtRow.Item("checkintime").ToString <> "" Then
                    If (dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL") OrElse dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")) AndAlso dtRow.Item("checkintime").ToString() = "00:00" Then
                        rpt_Row.Item("Column7") = ""
                    ElseIf rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 41, "PR") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 40, "HL") AndAlso _
                            rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 55, "WK") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 38, "OF") Then

                        'Pinkal (12-Jan-2022)-- Start
                        'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                        If mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then
                            If mStrLeaveTypeCode <> "" AndAlso dtRow.Item("checkintime").ToString <> "" AndAlso dtRow.Item("checkintime").ToString <> "00:00" Then
                                rpt_Row.Item("Column7") = (dtRow.Item("checkintime"))
                            Else
                                rpt_Row.Item("Column7") = ""
                            End If
                        Else
                        rpt_Row.Item("Column7") = ""
                        End If
                        'Pinkal (12-Jan-2022) -- End
                    Else
                        rpt_Row.Item("Column7") = (dtRow.Item("checkintime"))
                    End If
                End If

                If dtRow.Item("checkouttime").ToString <> "" Then

                    If (dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL") OrElse dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")) AndAlso dtRow.Item("checkouttime").ToString() = "00:00" Then
                        rpt_Row.Item("Column8") = ""
                    ElseIf rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 41, "PR") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 40, "HL") AndAlso _
                            rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 55, "WK") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 38, "OF") Then

                        'Pinkal (12-Jan-2022)-- Start
                        'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                        If mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then
                            If mStrLeaveTypeCode <> "" AndAlso dtRow.Item("checkouttime").ToString <> "" AndAlso dtRow.Item("checkouttime").ToString <> "00:00" Then
                                rpt_Row.Item("Column8") = (dtRow.Item("checkouttime"))
                            Else
                        rpt_Row.Item("Column8") = ""
                            End If
                        Else
                            rpt_Row.Item("Column8") = ""
                        End If
                        'Pinkal (12-Jan-2022) -- End
                    Else
                        rpt_Row.Item("Column8") = (dtRow.Item("checkouttime"))
                    End If
                End If

                If (dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL") OrElse dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")) AndAlso dtRow.Item("breakhr").ToString() = "00:00" Then
                    rpt_Row.Item("Column9") = ""
                ElseIf rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 41, "PR") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 40, "HL") AndAlso _
                            rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 55, "WK") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 38, "OF") Then

                    'Pinkal (12-Jan-2022)-- Start
                    'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                    If mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then
                        If mStrLeaveTypeCode <> "" AndAlso dtRow.Item("breakhr").ToString <> "" AndAlso dtRow.Item("breakhr").ToString <> "00:00" Then
                            rpt_Row.Item("Column9") = dtRow.Item("breakhr")
                        Else
                    rpt_Row.Item("Column9") = ""
                        End If
                    Else
                        rpt_Row.Item("Column9") = ""
                    End If
                    'Pinkal (12-Jan-2022) -- End
                Else
                    rpt_Row.Item("Column9") = dtRow.Item("breakhr")
                End If

                If (dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL") OrElse dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")) AndAlso dtRow.Item("workhour").ToString() = "00:00" Then
                    rpt_Row.Item("Column10") = ""
                ElseIf rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 41, "PR") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 40, "HL") AndAlso _
                            rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 55, "WK") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 38, "OF") Then

                    'Pinkal (12-Jan-2022)-- Start
                    'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                    If mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then
                        If mStrLeaveTypeCode <> "" AndAlso dtRow.Item("workhour").ToString <> "" AndAlso dtRow.Item("workhour").ToString <> "00:00" Then
                            rpt_Row.Item("Column10") = dtRow.Item("workhour")
                        Else
                            rpt_Row.Item("Column10") = ""
                        End If
                    Else
                    rpt_Row.Item("Column10") = ""
                    End If
                    'Pinkal (12-Jan-2022) -- End
                Else
                    rpt_Row.Item("Column10") = dtRow.Item("workhour")
                End If

                If (dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 40, "HL") OrElse dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 55, "WK")) AndAlso dtRow.Item("workhour").ToString() = "00:00" Then
                    rpt_Row.Item("Column11") = ""
                ElseIf rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 41, "PR") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 40, "HL") AndAlso _
                             rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 55, "WK") AndAlso rpt_Row.Item("Column6").ToString() <> Language.getMessage(mstrModuleName, 38, "OF") Then

                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnDisplayShortHrsWhenEmpAbsent AndAlso dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 39, "AB") Then
                        rpt_Row.Item("Column11") = dtRow.Item("basehrs")
                        'Pinkal (01-Mar-2022) -- Start
                        'Bug :  Issue Resolved for Biocorn to display total short hrs.
                        'dtRow.Item("Totalltcomeearlygoing") = CInt(dtRow.Item("workinghrs"))
                        dtRow.Item("Totalltcomeearlygoing") = CInt(dtRow.Item("basehrsinsec"))
                        'Pinkal (01-Mar-2022) -- End
                    Else
                        'Pinkal (12-Jan-2022)-- Start
                        'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                        If mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then
                            If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                                If dtRow.Item("ltcomeearlygoing").ToString().Trim() <> "00:00" AndAlso dtRow.Item("ltcomeearlygoing").ToString().Trim() <> "" Then
                                    rpt_Row.Item("Column11") = dtRow.Item("ltcomeearlygoing")
                                Else
                                    rpt_Row.Item("Column11") = ""
                                End If
                            Else
                    rpt_Row.Item("Column11") = ""
                    End If
                        Else
                            rpt_Row.Item("Column11") = ""
                        End If
                        'Pinkal (12-Jan-2022) -- End
                    End If
                    'Pinkal (13-Sep-2021)-- End
                Else
                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                        rpt_Row.Item("Column11") = dtRow.Item("ltcomeearlygoing")
                Else
                    rpt_Row.Item("Column11") = dtRow.Item("basehrs")
                End If
                    'Pinkal (13-Sep-2021)-- End
                End If

                If dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 39, "AB") AndAlso dtRow.Item("checkintime").ToString <> "" AndAlso dtRow.Item("checkintime").ToString <> "00:00" Then
                    rpt_Row.Item("Column7") = dtRow.Item("checkintime")
                ElseIf dtRow.Item("AttenCode") = Language.getMessage(mstrModuleName, 39, "AB") AndAlso dtRow.Item("checkouttime").ToString <> "" AndAlso dtRow.Item("checkouttime").ToString <> "00:00" Then
                    rpt_Row.Item("Column8") = dtRow.Item("checkouttime")
                End If

                If dtRow.Item("ot1").ToString().Trim <> "00:00" Then
                    rpt_Row.Item("Column12") = dtRow.Item("ot1")
                Else
                    rpt_Row.Item("Column12") = ""
                End If

                If dtRow.Item("ot2").ToString().Trim <> "00:00" Then
                    rpt_Row.Item("Column13") = dtRow.Item("ot2")
                Else
                    rpt_Row.Item("Column13") = ""
                End If

                If dtRow.Item("ot3").ToString().Trim <> "00:00" Then
                    rpt_Row.Item("Column14") = dtRow.Item("ot3")
                Else
                    rpt_Row.Item("Column14") = ""
                End If

                If dtRow.Item("ot4").ToString().Trim <> "00:00" Then
                    rpt_Row.Item("Column15") = dtRow.Item("ot4")
                Else
                    rpt_Row.Item("Column15") = ""
                End If

                rpt_Row.Item("Column16") = dtRow.Item("department")
                rpt_Row.Item("Column35") = dtRow.Item("Id")
                rpt_Row.Item("Column36") = dtRow.Item("GName")

                If dtRow.Item("teahr").ToString().Trim <> "00:00" Then
                    rpt_Row.Item("Column23") = dtRow.Item("teahr")
                Else
                    rpt_Row.Item("Column23") = ""
                End If

                '---------------------------------------------------------------START EMPLOYEE WISE TOTAL------------------------------------------------------------------------


                Dim mstrEmpCode As String = dtRow("employeecode").ToString
                If mintAttendId <> 0 Then
                    rpt_Row.Item("Column29") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalbreakhr"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    rpt_Row.Item("Column30") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("TotalHrs"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    rpt_Row.Item("Column31") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot1"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    rpt_Row.Item("Column32") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot2"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    rpt_Row.Item("Column33") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot3"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    rpt_Row.Item("Column34") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot4"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    rpt_Row.Item("Column37") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalteahr"))).DefaultIfEmpty.Sum()).ToString("#00.00")

                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                        rpt_Row.Item("Column41") = CalculateTime(True, (From p In dsList.Tables("DataTable") Where p.Item("employeecode") = mstrEmpCode AndAlso p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalltcomeearlygoing"))).DefaultIfEmpty.Sum()).ToString("#00.00")
                    End If
                    'Pinkal (13-Sep-2021)-- End
                Else
                    rpt_Row.Item("Column29") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalbreakhr)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    rpt_Row.Item("Column30") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(TotalHrs)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    rpt_Row.Item("Column31") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot1)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    rpt_Row.Item("Column32") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot2)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    rpt_Row.Item("Column33") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot3)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    rpt_Row.Item("Column34") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot4)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    rpt_Row.Item("Column37") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalteahr)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                        rpt_Row.Item("Column41") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalltcomeearlygoing)", "employeecode = '" & dtRow("employeecode") & "'"))).ToString("#00.00")
                    End If
                    'Pinkal (13-Sep-2021)-- End
                End If


                'Pinkal (17-Nov-2017) -- Start
                'Enhancement -  Ref No: 131 Display Edited Data in Employee Timesheet Report only .
                rpt_Row.Item("Column39") = CInt(dsList.Tables("DataTable").Compute("SUM(EditCount)", "employeecode = '" & dtRow("employeecode") & "'")).ToString()
                rpt_Row.Item("Column40") = dtRow.Item("EditCount").ToString()
                'Pinkal (17-Nov-2017) -- End


                '----------------------------------------------------------------END EMPLOYEE WISE TOTAL-----------------------------------------------------------------------

                ''---------------------------------------------------------------START SHIFT WISE TOTAL------------------------------------------------------------------------

                'rpt_Row.Item("Column23") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalbreakhr)", "shiftunkid = " & CInt(dtRow("shiftunkid"))))).ToString("#00.00")
                'rpt_Row.Item("Column24") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(TotalHrs)", "shiftunkid = " & CInt(dtRow("shiftunkid"))))).ToString("#00.00")
                'rpt_Row.Item("Column25") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot1)", "shiftunkid = " & CInt(dtRow("shiftunkid"))))).ToString("#00.00")
                'rpt_Row.Item("Column26") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot2)", "shiftunkid = " & CInt(dtRow("shiftunkid"))))).ToString("#00.00")
                'rpt_Row.Item("Column27") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot3)", "shiftunkid = " & CInt(dtRow("shiftunkid"))))).ToString("#00.00")
                'rpt_Row.Item("Column28") = CalculateTime(True, CInt(dsList.Tables("DataTable").Compute("SUM(Totalot4)", "shiftunkid = " & CInt(dtRow("shiftunkid"))))).ToString("#00.00")

                ''----------------------------------------------------------------END SHIFT WISE TOTAL-----------------------------------------------------------------------


                '---------------------------------------------------------------START TOTAL------------------------------------------------------------------------

                If mintAttendId <> 0 Then
                    minTotBreakhrs = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalbreakhr"))).DefaultIfEmpty.Sum()
                    mintTotWorkhrs = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("TotalHrs"))).DefaultIfEmpty.Sum()
                    mintOT1 = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot1"))).DefaultIfEmpty.Sum()
                    mintOT2 = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot2"))).DefaultIfEmpty.Sum()
                    mintOT3 = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot3"))).DefaultIfEmpty.Sum()
                    mintOT4 = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalot4"))).DefaultIfEmpty.Sum()
                    minTotTeahrs = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalteahr"))).DefaultIfEmpty.Sum()
                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                        minTotalShorthrs = (From p In dsList.Tables("DataTable") Where p.Item("Attencode") = mstrAttCode Select CInt(p.Item("Totalltcomeearlygoing"))).DefaultIfEmpty.Sum()
                    End If
                    'Pinkal (13-Sep-2021)-- End
                Else
                    minTotBreakhrs = CInt(dsList.Tables("DataTable").Compute("SUM(Totalbreakhr)", "1=1"))
                    mintTotWorkhrs = CInt(dsList.Tables("DataTable").Compute("SUM(TotalHrs)", "1=1"))
                    mintOT1 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot1)", "1=1"))
                    mintOT2 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot2)", "1=1"))
                    mintOT3 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot3)", "1=1"))
                    mintOT4 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot4)", "1=1"))
                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                        minTotalShorthrs = CInt(dsList.Tables("DataTable").Compute("SUM(Totalltcomeearlygoing)", "1=1"))
                    End If
                    'Pinkal (13-Sep-2021)-- End
                End If

                minTotTeahrs = CInt(dsList.Tables("DataTable").Compute("SUM(Totalteahr)", "1=1"))
                rpt_Row.Item("Column17") = CalculateTime(True, minTotBreakhrs).ToString("#00.00")
                rpt_Row.Item("Column18") = CalculateTime(True, mintTotWorkhrs).ToString("#00.00")
                rpt_Row.Item("Column19") = CalculateTime(True, mintOT1).ToString("#00.00")
                rpt_Row.Item("Column20") = CalculateTime(True, mintOT2).ToString("#00.00")
                rpt_Row.Item("Column21") = CalculateTime(True, mintOT3).ToString("#00.00")
                rpt_Row.Item("Column22") = CalculateTime(True, mintOT4).ToString("#00.00")
                rpt_Row.Item("Column38") = CalculateTime(True, minTotTeahrs).ToString("#00.00")

                'Pinkal (13-Sep-2021)-- Start
                'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                If mblnShowShortHrsOnBaseHrs And mblnShowLateComingEaryGoingAsShortHrs Then
                    rpt_Row.Item("Column42") = CalculateTime(True, minTotalShorthrs).ToString("#00.00")
                End If
                'Pinkal (13-Sep-2021)-- End

                '---------------------------------------------------------------END TOTAL------------------------------------------------------------------------


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)


            Next

            If mintAttendId <> 0 Then
                Dim mdtTable As DataTable = New DataView(rpt_Data.Tables("ArutiTable"), "Column6='" & mstrAttCode & "'", "", DataViewRowState.CurrentRows).ToTable
                rpt_Data.Tables.Remove("ArutiTable")
                rpt_Data.Tables.Add(mdtTable)
            End If

            objRpt = New ArutiReport.Designer.rptEmpPolicyTimeSheet

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 30, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 31, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 32, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 33, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection2", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column36}) <> {ArutiTable.Column36} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection2", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 2, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 3, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 4, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtBreakhrs", Language.getMessage(mstrModuleName, 5, "Break"))
            Call ReportFunction.TextChange(objRpt, "txtWorkinghrs", Language.getMessage(mstrModuleName, 43, "Total Hrs"))

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If mblnShowShortHrsOnBaseHrs Then
                Call ReportFunction.TextChange(objRpt, "txtBaseHrs", Language.getMessage(mstrModuleName, 9, "Short Hrs"))
            Else
            Call ReportFunction.TextChange(objRpt, "txtBaseHrs", Language.getMessage(mstrModuleName, 44, "Base Hrs"))
            End If

            If mblnShowLateComingEaryGoingAsShortHrs = False Then
                Call ReportFunction.EnableSuppress(objRpt, "Column411", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column421", True)
            End If
            'Pinkal (13-Sep-2021)-- End


            Call ReportFunction.TextChange(objRpt, "txtOT1", Language.getMessage(mstrModuleName, 45, "OT1"))
            Call ReportFunction.TextChange(objRpt, "txtOT2", Language.getMessage(mstrModuleName, 46, "OT2"))
            Call ReportFunction.TextChange(objRpt, "txtOT3", Language.getMessage(mstrModuleName, 47, "OT3"))
            Call ReportFunction.TextChange(objRpt, "txtOT4", Language.getMessage(mstrModuleName, 48, "OT4"))
            Call ReportFunction.TextChange(objRpt, "txtAttCode", Language.getMessage(mstrModuleName, 49, "AttCode"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 50, "Sr"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 51, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtShift", Language.getMessage(mstrModuleName, 52, "Shift"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 19, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 20, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 21, "Page :"))

            'Pinkal (28-Jan-2014) -- Start
            'Enhancement : Oman Changes
            Call ReportFunction.TextChange(objRpt, "txtTeahrs", Language.getMessage(mstrModuleName, 58, "Tea"))
            'Pinkal (28-Jan-2014) -- End

            'S.SANDEEP [ 26 NOV 2013 ] -- START
            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 53, "Code : "))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 24, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 63, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If
            'S.SANDEEP [ 26 NOV 2013 ] -- END


            'Pinkal (17-Nov-2017) -- Start
            'Enhancement -  Ref No: 131 Display Edited Data in Employee Timesheet Report only .
            Call ReportFunction.TextChange(objRpt, "txtEditedCount", Language.getMessage(mstrModuleName, 71, "~ (Edited Count) : "))
            'Pinkal (17-Nov-2017) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ShiftPolicy_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetHolidayandWeekend(ByVal startdate As DateTime, ByVal enddate As DateTime, ByVal yearunkid As Integer, ByVal employeeunkid As Integer, ByVal shiftunkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "DECLARE @weekdays AS VARCHAR(500) " & _
                          "DECLARE @QRY AS NVARCHAR(MAX) " & _
                      "DECLARE @DayName AS NVARCHAR(100)" & _
                      "DECLARE @intDay AS INTEGER " & _
                      "DECLARE @weekenddayid AS INTEGER " & _
                      " SET @weekenddayid = -1 " & _
                      " SET @DayName = '' " & _
                      " SET @intDay = 0 " & _
                      " WHILE @intDay <= 6 " & _
                      "  BEGIN " & _
                      "         SELECT @weekenddayid = dayid FROM tnashift_tran WHERE isweekend = 1 AND shiftunkid = @shiftunkid " & _
                      "         IF @weekenddayid <> @intDay  " & _
                      "         BEGIN " & _
                      "             SET @DayName = @DayName + DATENAME(DW,DATEADD(DD, 1 - DATEPART(DW, CONVERT(VARCHAR(10), GETDATE(), 111) ), CONVERT(VARCHAR(10), GETDATE()+ @intDay, 111) )) + ',' " & _
                      "         End " & _
                      "  SET @intDay = @intDay+1 " & _
                      " End " & _
                      " SET @weekdays = '''' + REPLACE(SUBSTRING(@DayName,0,LEN(@DayName)), ',', ''',''') + '''' " & _
                          "SELECT    @QRY = N'DECLARE @myTable TABLE " & _
                                            "( " & _
                                               "PeriodDate DateTime " & _
                                           ",   PeriodDay nvarchar(20) " & _
                                            ") " & _
                                             "declare @StartDate datetime " & _
                                             "declare @Days int " & _
                                             "declare @CurrentDay int " & _
                                             "set @StartDate = ''' + @start_date +''' " & _
                                             "set @Days = DATEDIFF(dd, DATEADD(dd, 0, ''' + @start_date + '''),'''+ @enddate + ''') " & _
                                             "set @CurrentDay = 0 " & _
                                             "while @CurrentDay < @Days " & _
                                               "begin " & _
                                                    "insert @myTable (PeriodDate,PeriodDay) values (dateadd(dd, @CurrentDay, @StartDate),DATEname(dw,dateadd(dd, @CurrentDay, @StartDate))) " & _
                                                    "set @CurrentDay = @CurrentDay + 1 " & _
                                               "End " & _
                                             "select COUNT(*) as days from @myTable WHERE PeriodDay NOT IN (' + @weekdays + ') " & _
                                            "Union " & _
                        "SELECT count(*)  as days FROM tnalogin_summary " & _
                        "JOIN lvholiday_master ON convert(char(8),holidaydate,112) = convert(char(8),login_date,112) " & _
                        "WHERE employeeunkid =  ''' + CAST (@employeeunkid AS NVARCHAR(MAX)) " & _
                                    "+ '''   And tnalogin_summary.ispaidleave = 0 And tnalogin_summary.isunpaidleave = 0 " & _
                        "AND convert(char(8),login_date,112) between ''' + @start_date + ''' and  ''' + @enddate + '''' " & _
                        "EXECUTE sp_executesql @QRY "

            objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(startdate))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(enddate))
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, yearunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, shiftunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then Return dsList.Tables("List").Compute("sum(days)", "1=1")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHolidayandWeekend; Module Name: " & mstrModuleName)
        End Try
        Return 0
    End Function

    Public Function GetHolidayName(ByVal employeeunkid As Integer, ByVal login_date As String) As String

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try

            Dim objDataOperation As New clsDataOperation

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            objDataOperation.AddParameter("@login_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, login_date)


            StrQ = " SELECT   ISNULL((SELECT  holidayname  " & _
                       " FROM lvemployee_holiday " & _
                    "JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                       " WHERE employeeunkid = @employeeunkid  AND CONVERT(CHAR(8), holidaydate, 112) = @login_date),'') AS holiday "


            'Pinkal (16-May-2019) -- Start
            'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type

            'If mblnIsPolicyManagementTNA Then  ''Pinkal (01-Feb-2014) -- Start  ConfigParameter._Object._PolicyManagementTNA
            '    StrQ &= ", ISNULL(( SELECT  leavetypecode  FROM  lvleaveIssue_tran " & _
            '             " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
            '             " JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
            '             " WHERE ISNULL(lvleaveIssue_tran.isvoid, 0) = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), leavedate, 112) = @login_date ),'') AS leavename "
            'Else
            '    StrQ &= ", ISNULL(( SELECT  leavename  FROM  lvleaveIssue_tran " & _
            '                 " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
            '                 " JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
            '                 " WHERE ISNULL(lvleaveIssue_tran.isvoid, 0) = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), leavedate, 112) = @login_date ),'') AS leavename "
            'End If

            StrQ &= ", ISNULL(( SELECT  leavetypecode  FROM  lvleaveIssue_tran " & _
                     " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                     " JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                     " WHERE ISNULL(lvleaveIssue_tran.isvoid, 0) = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), leavedate, 112) = @login_date ),'') AS leavetypecode " & _
                     ", ISNULL(( SELECT  leavename  FROM  lvleaveIssue_tran " & _
                     " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                     " JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                     " WHERE ISNULL(lvleaveIssue_tran.isvoid, 0) = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), leavedate, 112) = @login_date ),'') AS leavename "

            'Pinkal (16-May-2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                'Pinkal (16-May-2019) -- Start
                'Bug[0003822] - Employee Timesheet not displaying data if you filter data by using AttCode filter criteria i.e by Using Leave type
                'Return dsList.Tables(0).Rows(0)("holiday").ToString & "," & dsList.Tables(0).Rows(0)("leavename").ToString
                Return dsList.Tables(0).Rows(0)("holiday").ToString & "," & dsList.Tables(0).Rows(0)("leavename").ToString() & "," & dsList.Tables(0).Rows(0)("leavetypecode").ToString()
                'Pinkal (16-May-2019) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHolidayName; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetTotalRequirehrs(ByVal dtstartdate As Date, ByVal dtEnddate As Date, ByVal intshiftId As Integer) As String
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtstartdate)
            objDataOperation.AddParameter("@eddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtEnddate)
            objDataOperation.AddParameter("@shiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, intshiftId)

            StrQ = " DECLARE @intDay AS INT " & _
                      " DECLARE @startdate AS DATETIME " & _
                      " DECLARE @enddate AS DATETIME " & _
                      " DECLARE @weekdayId AS INT " & _
                      " DECLARE @weekendId AS INT " & _
                      " DECLARE @totalhrs AS INT " & _
                      " DECLARE @workinghrs AS INT " & _
                      " SET @totalhrs = 0 " & _
                      " SET @intDay = 0 " & _
                      " SET @weekdayId = -1 " & _
                      " SET @weekendId = -1 " & _
                      " SET @startdate =@stdate " & _
                      " SET @enddate =@eddate " & _
                      " WHILE @intDay <= (SELECT DateDiff(d, @startdate, @enddate)) " & _
                      " BEGIN " & _
                              " SELECT @weekdayId = datepart (dw,DATEADD(d,@intday,@startdate)) -1 FROM tnashift_tran WHERE isweekend = 0 AND shiftunkid = @shiftId " & _
                              " SELECT @weekendId = dayid FROM tnashift_tran WHERE isweekend = 1 AND shiftunkid = @shiftId " & _
                              " IF @weekdayId <> @weekendId  " & _
                              " BEGIN " & _
                              "          SET @workinghrs = 0 " & _
                                      " SELECT @workinghrs = workinghrs FROM tnashift_tran WHERE isweekend = 0 AND shiftunkid = @shiftId  AND dayid = @weekdayId " & _
                                      " SET @totalhrs = @totalhrs + @workinghrs " & _
                              " END " & _
                              " SET @intDay =  @intDay +  1 " & _
                     " END " & _
                    "SELECT ISNULL(@totalhrs,0) AS 'TotalReqhrs'  "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim calcMinute As Integer = CInt(dsList.Tables(0).Rows(0)("TotalReqhrs")) / 60
                Dim calMinute As Integer = calcMinute Mod 60
                Dim calHour As Double = calcMinute / 60
                Return CDec(CDec(Int(calHour) + (calMinute / 100))).ToString()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalRequirehrs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetFilterForAttCode() As DataTable
        Dim dtAttendCode As DataTable = Nothing
        Dim strQ As String = ""
        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT 0 AS Id, @Select AS leavetypecode,@Select1 AS leavename  " & _
                      " UNION ALL SELECT -1 As Id, @OFF AS leavetypecode,@OFFDays +  ' [' + @OFF + ']  ' AS leavename " & _
                      " UNION ALL SELECT -2 As Id, @AB AS leavetypecode, @ABDays + '  [' +@AB + ']  '   AS leavename" & _
                      " UNION ALL SELECT -3 As Id,@HL AS leavetypecode, @HLDays  + '  [' + @HL + ']  ' AS leavename" & _
                      " UNION ALL SELECT -4 As Id,@PR AS leavetypecode, @PRDays + '  [' + @PR + ']  ' AS leavename" & _
                      " UNION ALL SELECT -5 As Id,@WK AS leavetypecode, @Weekend  + '  [' + @WK + ']  ' AS leavename "

            'Pinkal (10-Aug-2016) -- Start
            'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
            strQ &= " UNION ALL SELECT -6 As Id,@Sup AS leavetypecode, @Suspended  + '  [' + @Sup + ']  ' AS leavename "
            'Pinkal (10-Aug-2016) -- End

            strQ &= " UNION ALL SELECT leavetypeunkid AS Id ,ISNULL(leavetypecode,'') AS leavetypecode, leavename + '  [' + ISNULL(leavetypecode,'') + ']  ' AS leavename  FROM lvleavetype_master WHERE isactive = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 59, "Select"))
            objDataOperation.AddParameter("@Select1", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 59, "Select"))
            objDataOperation.AddParameter("@OFFDays", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 60, "Day Off "))
            objDataOperation.AddParameter("@OFF", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "OF"))
            objDataOperation.AddParameter("@ABDays", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 61, "Absent Day"))
            objDataOperation.AddParameter("@AB", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "AB"))
            objDataOperation.AddParameter("@HLDays", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Holiday"))
            objDataOperation.AddParameter("@HL", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "HL"))
            objDataOperation.AddParameter("@PRDays", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 62, "Present Day"))
            objDataOperation.AddParameter("@PR", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 41, "PR"))
            objDataOperation.AddParameter("@Weekend", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 64, "Weekend"))
            objDataOperation.AddParameter("@WK", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "WK"))

            'Pinkal (10-Aug-2016) -- Start
            'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
            objDataOperation.AddParameter("@Suspended", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 69, "Suspended"))
            objDataOperation.AddParameter("@Sup", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 70, "Sup"))
            'Pinkal (10-Aug-2016) -- End

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dtAttendCode = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFilterForAttCode; Module Name: " & mstrModuleName)
        End Try
        Return dtAttendCode
    End Function

    Public Sub Send_ETimeSheet(ByVal strDatabaseName As String, _
                               ByVal intUserUnkid As Integer, _
                               ByVal intYearUnkid As Integer, _
                               ByVal intCompanyUnkid As Integer, _
                               ByVal dtPeriodStart As Date, _
                               ByVal dtPeriodEnd As Date, _
                               ByVal strUserModeSetting As String, _
                               ByVal blnOnlyApproved As Boolean, _
                               ByVal StrFileName As String, _
                               ByVal StrFilePath As String)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            Me.OrderByQuery = ""
            If mblnIsPolicyManagementTNA Then
                objRpt = Generate_ShiftPolicy_DetailReport(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved)
            Else
                objRpt = Generate_DetailReport(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved)
            End If

            If Not IsNothing(objRpt) Then
                Dim oStream As New IO.MemoryStream
                oStream = objRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
                Dim fs As New System.IO.FileStream(StrFilePath & "\" & StrFileName & ".pdf", IO.FileMode.Create, IO.FileAccess.Write)
                Dim data As Byte() = oStream.ToArray()
                fs.Write(data, 0, data.Length)
                fs.Close()
                fs.Dispose()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_ETimeSheet; Module Name: " & mstrModuleName)
        Finally
            objRpt.Dispose()
            GC.Collect()
        End Try
    End Sub


    'Pinkal (08-Aug-2019) -- Start
    'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
    Public Function GetEmployeeShiftHours(ByVal xEmployeId As Integer, ByVal xDate As Date, ByVal xShiftId As Integer) As Integer
        Dim xShiftHours As Integer = 0
        Try
            Dim objShitTran As New clsshift_tran
            objShitTran.GetShiftTran(xShiftId)
            Dim drWeekend() As DataRow = objShitTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(xDate.Date), False, FirstDayOfWeek.Sunday).ToString()))
            If drWeekend.Length > 0 Then
                xShiftHours = CInt(drWeekend(0)("workinghrsinsec"))
            End If
            objShitTran = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeOffDaysShiftHours; Module Name: " & mstrModuleName)
        End Try
        Return xShiftHours
    End Function
    'Pinkal (08-Aug-2019) -- End



    'Pinkal (27-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

    Private Function Generate_NMBDetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Dim rpt_Sub As ArutiReport.Designer.dsArutiReport = Nothing
        Try
            objDataOperation = New clsDataOperation


            'Dim objAbsent As New clsemployee_absent
            'objAbsent._StartDate = mdtFromDate.Date
            'objAbsent._EndDate = mdtToDate.Date
            'Dim dsPeriodData As DataSet = objAbsent.GeneratePeriodDate(objDataOperation)
            'objAbsent = Nothing


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "No"))
            objDataOperation.AddParameter("@Holidays", SqlDbType.Int, eZeeDataType.INT_SIZE, GetHolidayandWeekend(mdtFromDate.Date, mdtToDate.Date, Company._Object._YearUnkid, mintEmpId, mintShiftunkid))


            If mdtFromDate.Date < mdtDbStartDate.Date Then
                Dim mintCompanyID As Integer = 0
                Dim exForce As Exception = Nothing

                StrQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & mintYearID & "  "
                Dim dsCompany As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    mintCompanyID = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
                Else
                    StrQ = ""
                End If

                If mintCompanyID > 0 Then
                    Dim mstrPreviousDBName As String = ""
                    Dim mdtFYStartDate As Date = Nothing
                    Dim mdtFYEndDate As Date = Nothing

                    StrQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompanyID & " AND isclosed = 1 AND (start_date BETWEEN @stdate  AND @enddate or end_date BETWEEN @stdate  AND @enddate)  "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromDate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate))

                    Dim dsDBName As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count <= 0 Then
                        StrQ = ""
                    End If

                    For Each dRow As DataRow In dsDBName.Tables(0).Rows

                        Dim xprvDateJoinQry, xprvDateFilterQry, xprvUACQry, xprvUACFiltrQry, xprvAdvanceJoinQry As String

                        mstrPreviousDBName = dRow("database_name").ToString()
                        mdtFYStartDate = CDate(dRow("start_date"))
                        mdtFYEndDate = CDate(dRow("end_date"))
                        mblnIsPreviousDB = True

                        xprvDateJoinQry = xDateJoinQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvDateFilterQry = xDateFilterQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvUACQry = xUACQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvUACFiltrQry = xUACFiltrQry.Replace(strDatabaseName, mstrPreviousDBName)
                        xprvAdvanceJoinQry = xAdvanceJoinQry.Replace(strDatabaseName, mstrPreviousDBName)

                        StrQ = " SELECT employeeunkid, employeecode,employee,isactive,Department " & _
                                  ",login_date,checkintime,checkouttime, " & _
                                  " workhour " & _
                                  ",TotWorkhrs " & _
                                  ",TotAbsent " & _
                                  ", Id AS Id , GName AS GName " & _
                                  ", CASE WHEN isunpaidleave  = 1 THEN @ABSENT  ELSE '' END AttenCode " & _
                                  " FROM " & _
                                    " ( " & _
                                             " SELECT  tnalogin_summary.employeeunkid , " & _
                                             "ISNULL(hremployee_master.employeecode,'') AS employeecode, " & _
                                             "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                                             "+ ISNULL(hremployee_master.surname, '') AS employee , " & _
                                             " hremployee_master.isactive "

                        If xprvUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= ", hremployee_master.jobunkid "
                        End If

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= ", tnalogin_summary.isunpaidleave, " & _
                                    " ISNULL(dept.name,'') AS department, " & _
                                    " CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) AS login_date, " & _
                                    " ISNULL(logintran.checkintime,'') AS checkintime  , " & _
                                    " ISNULL(logintran.checkouttime,'') AS checkouttime , " & _
                                    " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_hrs)/ 3600), 2) + ':' " & _
                                    " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.total_hrs)% 3600 ) / 60), 2), '00:00') AS workhour , " & _
                                    " CONVERT(VARCHAR(max),(SELECT SUM(t.total_hrs)/3600 FROM  " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                                    " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_hrs)%3600/ 60)  FROM " & mstrPreviousDBName & "..tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS Totworkhrs , " & _
                                     " CASE WHEN tnalogin_summary.isunpaidleave > 0  THEN 1 ELSE 0 END TotAbsent " & _
                                     " FROM  " & mstrPreviousDBName & "..tnalogin_summary   " & _
                                     " LEFT JOIN " & mstrPreviousDBName & "..hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                                     " LEFT JOIN " & _
                                     " ( " & _
                                     " SELECT " & _
                                     "    departmentunkid " & _
                                     "   ,employeeunkid " & _
                                     "   ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                     " FROM " & mstrPreviousDBName & "..hremployee_transfer_tran " & _
                                     " WHERE isvoid = 0 	AND CONVERT(CHAR(8), effectivedate, 112) <= @ToDate) AS A " & _
                                     " ON a.employeeunkid = tnalogin_summary.employeeunkid AND A.rno = 1 " & _
                                     " LEFT JOIN " & mstrPreviousDBName & "..hrdepartment_master dept on dept.departmentunkid = A.departmentunkid " & _
                                     " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid, " & _
                                     "                                    ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') checkintime , " & _
                                     "                                    ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '') checkouttime , " & _
                                     "                                    ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' " & _
                                     "                                  + RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr)% 3600 / 60), 2), '00:00') AS breakhr " & _
                                     "                      FROM " & mstrPreviousDBName & "..tnalogin_Tran " & _
                                     "                      WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid " & _
                                     "                   ) AS logintran  ON logintran.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  "

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Join
                        End If
                        If xprvDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xprvDateJoinQry
                        End If

                        If xprvUACQry.Trim.Length > 0 Then
                            StrQ &= xprvUACQry
                        End If

                        If xprvAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xprvAdvanceJoinQry
                        End If

                        StrQ &= " WHERE CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate "

                        If xprvUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xprvUACFiltrQry & " "
                        End If

                        If mblnIsActive = False Then
                            If xprvDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xprvDateFilterQry & " "
                            End If
                        End If


                        If mstrAdvanceFilter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvanceFilter
                        End If

                        Call FilterTitleAndFilterQuery()

                        StrQ &= Me._FilterQuery

                        StrQ &= "GROUP BY ISNULL(hremployee_master.employeecode,''), " & _
                                    "ISNULL(hremployee_master.firstname, '') , " & _
                                    "ISNULL(hremployee_master.surname, '') , " & _
                                    "hremployee_master.isactive , " & _
                                    "ISNULL(dept.name,''), " & _
                                    "tnalogin_summary.employeeunkid , " & _
                                    "CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) , " & _
                                    "tnalogin_summary.login_date, " & _
                                    "logintran.checkintime, " & _
                                    "logintran.checkouttime"

                        If xprvUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= ",hremployee_master.jobunkid "
                        End If

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                        End If

                        StrQ &= ",tnalogin_summary.isunpaidleave " & _
                                  " ) AS ETS  WHERE 1 = 1 "

                        mblnIsPreviousDB = False

                        StrQ &= " UNION "
                    Next

                Else
                    StrQ = ""
                End If  ' If mintCompanyID > 0 Then
            End If

            objDataOperation.ClearParameters()

            StrQ &= " SELECT employeeunkid, employeecode,employee,isactive,Department " & _
                     ",login_date,checkintime,checkouttime " & _
                     ",workhour " & _
                     ", TotWorkhrs " & _
                     " ,TotAbsent , Id AS Id , GName AS GName " & _
                     ", CASE WHEN isunpaidleave  = 1 THEN @ABSENT  ELSE '' END AttenCode "

            StrQ &= " FROM " & _
                        " ( " & _
                                 " SELECT  tnalogin_summary.employeeunkid , " & _
                                 "ISNULL(hremployee_master.employeecode,'') AS employeecode, " & _
                                "ISNULL(hremployee_master.firstname, '') + ' ' " & _
                                "+ ISNULL(hremployee_master.surname, '') AS employee , " & _
                                " hremployee_master.isactive "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= ", hremployee_master.jobunkid "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= ", tnalogin_summary.isunpaidleave, " & _
                        " ISNULL(dept.name,'') AS department, " & _
                        " CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) AS login_date, " & _
                        " ISNULL(logintran.checkintime,'') AS checkintime  , " & _
                        " ISNULL(logintran.checkouttime,'') AS checkouttime , " & _
                        " ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_summary.total_hrs)/ 3600), 2) + ':' " & _
                        " + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(tnalogin_summary.total_hrs)% 3600 ) / 60), 2), '00:00') AS workhour , " & _
                        " CONVERT(VARCHAR(max),(SELECT SUM(t.total_hrs)/3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )) + ':' + " & _
                        " + RIGHT('00' + CONVERT(VARCHAR(2),(SELECT (SUM(t.total_hrs)%3600/ 60)  FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),t.login_date,112) BETWEEN @FromDate and @ToDate )),2)  AS Totworkhrs, " & _
                        " CASE WHEN tnalogin_summary.isunpaidleave > 0  THEN 1 ELSE 0 END TotAbsent " & _
                        " FROM tnalogin_summary    " & _
                        " LEFT JOIN hremployee_master  ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                         " LEFT JOIN " & _
                         " ( " & _
                         " SELECT " & _
                         "    departmentunkid " & _
                         "   ,employeeunkid " & _
                         "   ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         " FROM hremployee_transfer_tran " & _
                         " WHERE isvoid = 0 	AND CONVERT(CHAR(8), effectivedate, 112) <= @ToDate) AS A " & _
                         " ON a.employeeunkid = tnalogin_summary.employeeunkid AND A.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept on dept.departmentunkid = A.departmentunkid " & _
                         " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid, " & _
                         "                                ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') checkintime , " & _
                         "                                ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '') checkouttime  " & _
                         "                  FROM tnalogin_Tran " & _
                         "                  WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid " & _
                         "                 ) AS logintran ON logintran.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= " WHERE  CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate  "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY ISNULL(hremployee_master.employeecode,''), " & _
                        " ISNULL(hremployee_master.firstname, '') , " & _
                        " ISNULL(hremployee_master.surname, '') , " & _
                        " hremployee_master.isactive , " & _
                        " ISNULL(dept.name,''), " & _
                        " tnalogin_summary.employeeunkid , " & _
                        " CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) , " & _
                        " tnalogin_summary.login_date, " & _
                        " tnalogin_summary.isweekend, " & _
                        " logintran.checkintime, " & _
                        " logintran.checkouttime "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= ",hremployee_master.jobunkid "
            End If


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If


            StrQ &= ",tnalogin_summary.isunpaidleave " & _
                      " ) AS ETS  "

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim TotHolidays As Integer = 0
            Dim TotMispunch As Integer = 0
            Dim intEmployeID As Integer = 0
            Dim minTotalBaseHours As Integer = 0

            Dim objShitTran As New clsshift_tran


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employeeunkid")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("employee")


                rpt_Row.Item("Column4") = WeekdayName(Weekday(eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToShortDateString), True, FirstDayOfWeek.Sunday)
                Dim mstrFormart As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString()
                If System.Text.RegularExpressions.Regex.Matches(mstrFormart, "M", System.Text.RegularExpressions.RegexOptions.IgnoreCase).Count <= 1 Then
                    mstrFormart = mstrFormart.Replace("M", "MM")
                End If
                If System.Text.RegularExpressions.Regex.Matches(mstrFormart, "d", System.Text.RegularExpressions.RegexOptions.IgnoreCase).Count <= 1 Then
                    mstrFormart = mstrFormart.Replace("d", "dd")
                End If

                rpt_Row.Item("Column30") = eZeeDate.convertDate(dtRow.Item("login_date").ToString).ToString(mstrFormart)

                If intEmployeID <> CInt(dtRow.Item("employeeunkid")) Then
                    intEmployeID = CInt(dtRow.Item("employeeunkid"))
                    TotMispunch = 0
                End If

                If dtRow.Item("checkintime").ToString <> "" Then
                    If mblnShowTiming24HRS Then
                        rpt_Row.Item("Column5") = CDate(dtRow.Item("checkintime")).ToString("HH:mm")
                    Else
                        rpt_Row.Item("Column5") = CDate(dtRow.Item("checkintime")).ToShortTimeString
                    End If
                ElseIf dtRow.Item("checkouttime").ToString <> "" Then
                    rpt_Row.Item("Column5") = "***"
                End If

                If dtRow.Item("checkouttime").ToString <> "" Then
                    If mblnShowTiming24HRS Then
                        rpt_Row.Item("Column6") = CDate(dtRow.Item("checkouttime")).ToString("HH:mm")
                    Else
                        rpt_Row.Item("Column6") = CDate(dtRow.Item("checkouttime")).ToShortTimeString
                    End If

                ElseIf dtRow.Item("checkintime").ToString <> "" Then
                    rpt_Row.Item("Column6") = "***"
                    TotMispunch += 1
                End If

                rpt_Row.Item("Column8") = dtRow.Item("workhour")
                rpt_Row.Item("Column13") = dtRow.Item("Totworkhrs")
                rpt_Row.Item("Column17") = dtRow.Item("TotAbsent")
                rpt_Row.Item("Column26") = dtRow.Item("Id")
                rpt_Row.Item("Column27") = dtRow.Item("GName")
                rpt_Row.Item("Column28") = dtRow.Item("Department")
                rpt_Row.Item("Column29") = TotMispunch

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptNMBEmployeeTimeSheet

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 30, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 31, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 32, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 33, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 24, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 2, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 3, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 4, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtWorkinghrs", Language.getMessage(mstrModuleName, 6, "Working Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 27, "Code :"))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection2", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column27}) <> {ArutiTable.Column27} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection2", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            Call ReportFunction.TextChange(objRpt, "txtMispunch", "*** : " & Language.getMessage(mstrModuleName, 66, "Mispunch"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 54, "Department :"))
            Call ReportFunction.TextChange(objRpt, "lblTotMispunch", Language.getMessage(mstrModuleName, 68, "Total Mispunch Days"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 19, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 20, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Return objRpt
        Catch ex As Exception
            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.
            'DisplayError.Show(-1, ex.Message, "Generate_DetailReport", mstrModuleName)
            DisplayError.Show(-1, ex.Message, "Generate_NMBDetailReport", mstrModuleName)
            'Pinkal (08-Feb-2022) -- End
            Return Nothing
        End Try
    End Function

    'Pinkal (27-Aug-2020) -- End



#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee")
            Language.setMessage(mstrModuleName, 2, "Date")
            Language.setMessage(mstrModuleName, 3, "In Time")
            Language.setMessage(mstrModuleName, 4, "Out Time")
            Language.setMessage(mstrModuleName, 5, "Break")
            Language.setMessage(mstrModuleName, 6, "Working Hrs")
            Language.setMessage(mstrModuleName, 7, "Yes")
            Language.setMessage(mstrModuleName, 8, "No")
            Language.setMessage(mstrModuleName, 9, "Short Hrs")
            Language.setMessage(mstrModuleName, 10, "Extra Hrs")
            Language.setMessage(mstrModuleName, 12, "Holiday")
            Language.setMessage(mstrModuleName, 18, "Total Absent days")
            Language.setMessage(mstrModuleName, 19, "Printed By :")
            Language.setMessage(mstrModuleName, 20, "Printed Date :")
            Language.setMessage(mstrModuleName, 21, "Page :")
            Language.setMessage(mstrModuleName, 22, " From Date:")
            Language.setMessage(mstrModuleName, 23, " To Date:")
            Language.setMessage(mstrModuleName, 24, "Employee :")
            Language.setMessage(mstrModuleName, 25, " Order By :")
            Language.setMessage(mstrModuleName, 27, "Code :")
            Language.setMessage(mstrModuleName, 28, "Shift :")
            Language.setMessage(mstrModuleName, 30, "Prepared By :")
            Language.setMessage(mstrModuleName, 31, "Checked By :")
            Language.setMessage(mstrModuleName, 32, "Approved By :")
            Language.setMessage(mstrModuleName, 33, "Received By :")
            Language.setMessage(mstrModuleName, 34, "Night Hrs")
            Language.setMessage(mstrModuleName, 36, "Leave Type")
            Language.setMessage(mstrModuleName, 37, "Total Holidays")
            Language.setMessage(mstrModuleName, 38, "OF")
            Language.setMessage(mstrModuleName, 39, "AB")
            Language.setMessage(mstrModuleName, 40, "HL")
            Language.setMessage(mstrModuleName, 41, "PR")
            Language.setMessage(mstrModuleName, 43, "Total Hrs")
            Language.setMessage(mstrModuleName, 44, "Base Hrs")
            Language.setMessage(mstrModuleName, 45, "OT1")
            Language.setMessage(mstrModuleName, 46, "OT2")
            Language.setMessage(mstrModuleName, 47, "OT3")
            Language.setMessage(mstrModuleName, 48, "OT4")
            Language.setMessage(mstrModuleName, 49, "AttCode")
            Language.setMessage(mstrModuleName, 50, "Sr")
            Language.setMessage(mstrModuleName, 51, "Total")
            Language.setMessage(mstrModuleName, 52, "Shift")
            Language.setMessage(mstrModuleName, 53, "Code :")
            Language.setMessage(mstrModuleName, 54, "Department :")
            Language.setMessage(mstrModuleName, 55, "WK")
            Language.setMessage(mstrModuleName, 56, "Employee Code")
            Language.setMessage(mstrModuleName, 58, "Tea")
            Language.setMessage(mstrModuleName, 59, "Select")
            Language.setMessage(mstrModuleName, 60, "Day Off")
            Language.setMessage(mstrModuleName, 61, "Absent Day")
            Language.setMessage(mstrModuleName, 62, "Present Day")
            Language.setMessage(mstrModuleName, 63, "Sub Total :")
            Language.setMessage(mstrModuleName, 64, "Weekend")
            Language.setMessage(mstrModuleName, 65, "AttCode :")
            Language.setMessage(mstrModuleName, 66, "Mispunch")
            Language.setMessage(mstrModuleName, 68, "Total Mispunch Days")
            Language.setMessage(mstrModuleName, 69, "Suspended")
            Language.setMessage(mstrModuleName, 70, "Sup")
			Language.setMessage(mstrModuleName, 71, "~ (Edited Count) :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
