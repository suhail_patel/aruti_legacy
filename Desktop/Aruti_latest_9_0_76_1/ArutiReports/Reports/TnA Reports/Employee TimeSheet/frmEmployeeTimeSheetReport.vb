Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 1

Public Class frmEmployeeTimeSheetReport
    Private mstrModuleName As String = "frmEmployeeTimeSheetReport"
    Private objEmployeeTimeSheet As clsEmployeeTimesheetReport

#Region "Constructor"
    Public Sub New()
        objEmployeeTimeSheet = New clsEmployeeTimesheetReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmployeeTimeSheet.SetDefaultValue()
        InitializeComponent()

        _Show_AdvanceFilter = True
        chkShowHoliday.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        chkShowLeaveType.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        chkShowTimingsIn24Hrs.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        chkShift.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        chkBreaktime.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        chkShowBaseHrs.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        chkShowTotalAbsentDays.Visible = Not ConfigParameter._Object._PolicyManagementTNA

        'Pinkal (08-Aug-2019) -- Start
        'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
        chkIncludeOFFHrsInBaseHrs.Visible = Not ConfigParameter._Object._PolicyManagementTNA
        'Pinkal (08-Aug-2019) -- End

    End Sub
#End Region

#Region "Private Variables"

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            Dim dtAttCode As DataTable = objEmployeeTimeSheet.GetFilterForAttCode()
            With cboAttCode
                .ValueMember = "Id"
                .DisplayMember = "leavename"
                .DataSource = dtAttCode
                .SelectedValue = 0
            End With


            If ConfigParameter._Object._EmpTimesheetSetting.Trim.Length > 0 Then
                Dim ar() As String = ConfigParameter._Object._EmpTimesheetSetting.ToString.Trim.Split("|")
                If ar.Length > 0 Then
                    chkShowTimingsIn24Hrs.Checked = CBool(ar(0).ToString())
                    If ConfigParameter._Object._PolicyManagementTNA = False Then
                        chkShowHoliday.Checked = CBool(ar(1).ToString())
                        chkShowLeaveType.Checked = CBool(ar(2).ToString())
                        If ar.Length > 4 Then
                            chkBreaktime.Checked = CBool(ar(4).ToString())
                            chkShift.Checked = CBool(ar(5).ToString())

                            If ar.Length - 1 < 6 OrElse ar(6).ToString().Trim.Length <= 0 Then
                                chkShowBaseHrs.Checked = False
                            Else
                                chkShowBaseHrs.Checked = CBool(ar(6).ToString())
                            End If
                            If ar.Length - 1 < 7 OrElse ar(7).ToString.Trim.Length <= 0 Then
                                chkShowTotalAbsentDays.Checked = False
                            Else
                                chkShowTotalAbsentDays.Checked = CBool(ar(7).ToString())
                            End If

                            'Pinkal (08-Aug-2019) -- Start
                            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                            If ar.Length - 1 < 8 OrElse ar(8).ToString.Trim.Length <= 0 Then
                                chkIncludeOFFHrsInBaseHrs.Checked = True
                            Else
                                chkIncludeOFFHrsInBaseHrs.Checked = CBool(ar(8).ToString())
                            End If
                            'Pinkal (08-Aug-2019) -- End
                        End If
                            'Pinkal (13-Sep-2021)-- Start
                            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    Else
                        If ar.Length > 4 Then
                            If ar.Length - 1 < 9 OrElse ar(9).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If ArtLic._Object.HotelName.ToUpper() <> "COLAS LTD" Then
                                    chkShowLateComingInShortHrs.Checked = False
                                Else
                                chkShowLateComingInShortHrs.Checked = True
                                End If
                                'Pinkal (09-Dec-2021) -- End
                            Else
                                chkShowLateComingInShortHrs.Checked = CBool(ar(9).ToString())
                            End If

                            If ar.Length - 1 < 10 OrElse ar(10).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If ArtLic._Object.HotelName.ToUpper() <> "COLAS LTD" Then
                                    chkShowActualTimingAfterRoundOff.Checked = False
                                Else
                                chkShowActualTimingAfterRoundOff.Checked = True
                                End If
                                'Pinkal (09-Dec-2021) -- End
                            Else
                                chkShowActualTimingAfterRoundOff.Checked = CBool(ar(10).ToString())
                            End If

                            If ar.Length - 1 < 11 OrElse ar(11).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If ArtLic._Object.HotelName.ToUpper() <> "COLAS LTD" Then
                                    chkShowShortHrsOnBaseHrs.Checked = False
                                Else
                                chkShowShortHrsOnBaseHrs.Checked = True
                                End If
                                'Pinkal (09-Dec-2021) -- End
                            Else
                                chkShowShortHrsOnBaseHrs.Checked = CBool(ar(11).ToString())
                            End If

                            If ar.Length - 1 < 12 OrElse ar(12).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If ArtLic._Object.HotelName.ToUpper() <> "COLAS LTD" Then
                                    chkDisplayShortHrsWhenEmpAbsent.Checked = False
                                Else
                                chkDisplayShortHrsWhenEmpAbsent.Checked = True
                                End If
                                'Pinkal (09-Dec-2021) -- End
                            Else
                                chkDisplayShortHrsWhenEmpAbsent.Checked = CBool(ar(12).ToString())
                            End If

                            If ar.Length - 1 < 13 OrElse ar(13).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If ArtLic._Object.HotelName.ToUpper() <> "COLAS LTD" Then
                                    chkShowAbsentAfterEmpTerminated.Checked = False
                                Else
                                chkShowAbsentAfterEmpTerminated.Checked = True
                                End If
                                'Pinkal (09-Dec-2021) -- End
                            Else
                                chkShowAbsentAfterEmpTerminated.Checked = CBool(ar(13).ToString())
                            End If
                        End If
                        'Pinkal (13-Sep-2021)-- End
                    End If
                    chkShowEachEmpOnNewPage.Checked = CBool(ar(3).ToString())
                End If
            End If
            'Pinkal (09-Jun-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboEmployee.SelectedValue = 0
            cboShift.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            objEmployeeTimeSheet.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmployeeTimeSheet.OrderByDisplay
            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""
            cboAttCode.SelectedIndex = 0

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If ArtLic._Object.HotelName.ToUpper() <> "COLAS LTD" Then
                chkShowAbsentAfterEmpTerminated.Visible = False
                chkShowLateComingInShortHrs.Visible = False
                chkShowShortHrsOnBaseHrs.Visible = False
                chkShowActualTimingAfterRoundOff.Visible = False
                chkDisplayShortHrsWhenEmpAbsent.Visible = False
            Else
                chkShowAbsentAfterEmpTerminated.Visible = True
                chkShowLateComingInShortHrs.Visible = True
                chkShowShortHrsOnBaseHrs.Visible = True
                chkShowActualTimingAfterRoundOff.Visible = True
                chkDisplayShortHrsWhenEmpAbsent.Visible = True
            End If
            'Pinkal (13-Sep-2021)-- End


            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If Company._Object._Name.ToUpper() = "NMB BANK PLC" Then
                chkShift.Visible = False
                chkShowTotalAbsentDays.Visible = False
                chkIncludeOFFHrsInBaseHrs.Visible = False
                chkShowHoliday.Visible = False
                chkBreaktime.Visible = False
                chkShowLeaveType.Visible = False
                chkShowBaseHrs.Visible = False

                'Pinkal (13-Sep-2021)-- Start
                'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                chkShowAbsentAfterEmpTerminated.Checked = False
                chkShowLateComingInShortHrs.Visible = False
                chkShowShortHrsOnBaseHrs.Visible = False
                chkShowActualTimingAfterRoundOff.Visible = False
                chkDisplayShortHrsWhenEmpAbsent.Visible = False
                'Pinkal (13-Sep-2021) -- End
            End If
            'Pinkal (27-Aug-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmployeeTimeSheet.SetDefaultValue()


            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

            'If ConfigParameter._Object._PolicyManagementTNA = False AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee. Employee is compulsory information."), enMsgBoxStyle.Information)
            '    cboEmployee.Select()
            '    Return False
            'End If

            'Pinkal (09-Jun-2015) -- End

            objEmployeeTimeSheet._EmpId = cboEmployee.SelectedValue
            objEmployeeTimeSheet._EmpName = cboEmployee.Text

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                objEmployeeTimeSheet._Shiftunkid = objEmp._Shiftunkid
                objEmployeeTimeSheet._EmpCode = objEmp._Employeecode
                objEmployeeTimeSheet._DepartmentId = objEmp._Departmentunkid
                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmp._Departmentunkid
                objEmployeeTimeSheet._Department = objDept._Name
            End If

            objEmployeeTimeSheet._FromDate = dtpFromDate.Value.Date
            objEmployeeTimeSheet._ToDate = dtpToDate.Value.Date
            objEmployeeTimeSheet._IsActive = chkInactiveemp.Checked
            objEmployeeTimeSheet._Shiftunkid = CInt(cboShift.SelectedValue)
            objEmployeeTimeSheet._ShiftName = cboShift.Text
            objEmployeeTimeSheet._ViewByIds = mstrStringIds
            objEmployeeTimeSheet._ViewIndex = mintViewIdx
            objEmployeeTimeSheet._ViewByName = mstrStringName
            objEmployeeTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objEmployeeTimeSheet._Analysis_Join = mstrAnalysis_Join
            objEmployeeTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmployeeTimeSheet._Report_GroupName = mstrReport_GroupName
            objEmployeeTimeSheet._AdvanceFilter = mstrAdvanceFilter
            objEmployeeTimeSheet._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            objEmployeeTimeSheet._AttenId = CInt(cboAttCode.SelectedValue)
            Dim dtRow As DataRowView = CType(cboAttCode.SelectedItem, DataRowView)
            objEmployeeTimeSheet._AttCode = dtRow.Row("leavetypecode").ToString
            objEmployeeTimeSheet._ShowHoliday = chkShowHoliday.Checked
            objEmployeeTimeSheet._ShowLeaveType = chkShowLeaveType.Checked
            objEmployeeTimeSheet._ShowTiming24Hrs = chkShowTimingsIn24Hrs.Checked
            objEmployeeTimeSheet._ShowBreakTime = chkBreaktime.Checked
            objEmployeeTimeSheet._ShowShift = chkShift.Checked
            objEmployeeTimeSheet._ShowBaseHrs = chkShowBaseHrs.Checked
            objEmployeeTimeSheet._ShowTotalAbsentDays = chkShowTotalAbsentDays.Checked
            objEmployeeTimeSheet._DBstartDate = FinancialYear._Object._Database_Start_Date.Date
            objEmployeeTimeSheet._DBEndDate = FinancialYear._Object._Database_End_Date.Date
            objEmployeeTimeSheet._YearId = FinancialYear._Object._YearUnkid
            objEmployeeTimeSheet._IsPolicyManagementTNA = ConfigParameter._Object._PolicyManagementTNA

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            objEmployeeTimeSheet._IncludeOFFHrsinTotalBaseHrs = chkIncludeOFFHrsInBaseHrs.Checked
            'Pinkal (08-Aug-2019) -- End

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            objEmployeeTimeSheet._ShowAbsentAfterEmpTerminated = chkShowAbsentAfterEmpTerminated.Checked
            objEmployeeTimeSheet._ShowLateComingEaryGoingAsShortHrs = chkShowLateComingInShortHrs.Checked
            objEmployeeTimeSheet._ShowShortHrsOnBaseHrs = chkShowShortHrsOnBaseHrs.Checked
            objEmployeeTimeSheet._ShowActualTimingAfterRoundOff = chkShowActualTimingAfterRoundOff.Checked
            objEmployeeTimeSheet._DisplayShortHrsWhenEmpAbsent = chkDisplayShortHrsWhenEmpAbsent.Checked
            'Pinkal (13-Sep-2021)-- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmDailyTimeSheet_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeTimeSheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyTimeSheet_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmployeeTimeSheetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objEmployeeTimeSheet._ReportName
            Me._Message = objEmployeeTimeSheet._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeTimeSheetReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmEmployeeTimeSheetReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            objEmployeeTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, e.Type, Aruti.Data.enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmployeeTimeSheetReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objEmployeeTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTimeSheetReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmployeeTimeSheetReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeTimesheetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeTimesheetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeTimeSheetReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboShift.DataSource
            frm.ValueMember = cboShift.ValueMember
            frm.DisplayMember = cboShift.DisplayMember
            If frm.DisplayDialog Then
                cboShift.SelectedValue = frm.SelectedValue
                cboShift.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAttCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAttCode.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboAttCode.DataSource
            frm.CodeMember = "leavetypecode"
            frm.ValueMember = cboAttCode.ValueMember
            frm.DisplayMember = cboAttCode.DisplayMember
            If frm.DisplayDialog Then
                cboAttCode.SelectedValue = frm.SelectedValue
                cboAttCode.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAttCode_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmployeeTimeSheet.setOrderBy(0)
            txtOrderBy.Text = objEmployeeTimeSheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked 'S.SANDEEP [ 26 NOV 2013 ] -- START {Handler Added} -- END
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            'objConfig._EmpTimesheetSetting = chkShowTimingsIn24Hrs.Checked.ToString() & "|" & chkShowHoliday.Checked.ToString() & "|" & chkShowLeaveType.Checked.ToString() & "|" & _
            '                                                    chkShowEachEmpOnNewPage.Checked.ToString() & "|" & chkBreaktime.Checked.ToString() & "|" & chkShift.Checked.ToString() & "|" & _
            '                                                    chkShowBaseHrs.Checked.ToString() & "|" & chkShowTotalAbsentDays.Checked.ToString()


            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            'objConfig._EmpTimesheetSetting = chkShowTimingsIn24Hrs.Checked.ToString() & "|" & chkShowHoliday.Checked.ToString() & "|" & chkShowLeaveType.Checked.ToString() & "|" & _
            '                                                  chkShowEachEmpOnNewPage.Checked.ToString() & "|" & chkBreaktime.Checked.ToString() & "|" & chkShift.Checked.ToString() & "|" & _
            '                                                chkShowBaseHrs.Checked.ToString() & "|" & chkShowTotalAbsentDays.Checked.ToString() & "|" & chkIncludeOFFHrsInBaseHrs.Checked.ToString()

            objConfig._EmpTimesheetSetting = chkShowTimingsIn24Hrs.Checked.ToString() & "|" & chkShowHoliday.Checked.ToString() & "|" & chkShowLeaveType.Checked.ToString() & "|" & _
                                                                chkShowEachEmpOnNewPage.Checked.ToString() & "|" & chkBreaktime.Checked.ToString() & "|" & chkShift.Checked.ToString() & "|" & _
                                                                chkShowBaseHrs.Checked.ToString() & "|" & chkShowTotalAbsentDays.Checked.ToString() & "|" & chkIncludeOFFHrsInBaseHrs.Checked.ToString() & "|" & _
                                                                chkShowLateComingInShortHrs.Checked.ToString() & "|" & chkShowActualTimingAfterRoundOff.Checked.ToString() & "|" & _
                                                                chkShowShortHrsOnBaseHrs.Checked.ToString() & "|" & chkDisplayShortHrsWhenEmpAbsent.Checked.ToString() & "|" & _
                                                                chkShowAbsentAfterEmpTerminated.Checked.ToString()

            'Pinkal (13-Sep-2021) -- End

          

            'Pinkal (08-Aug-2019) -- End

            If objConfig.updateParam() = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Filters saved successfully."), enMsgBoxStyle.Information)
            End If
            objConfig = Nothing
            ConfigParameter._Object.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        End Try
    End Sub


#End Region

    'Pinkal (13-Sep-2021)-- Start
    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.

#Region "CheckBox Event"

    Private Sub chkShowShortHrsOnBaseHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowShortHrsOnBaseHrs.CheckedChanged
        Try
            chkShowLateComingInShortHrs.Enabled = False
            chkShowLateComingInShortHrs.Checked = chkShowShortHrsOnBaseHrs.Checked
            chkDisplayShortHrsWhenEmpAbsent.Enabled = chkShowShortHrsOnBaseHrs.Checked
            If chkShowShortHrsOnBaseHrs.Checked = False Then
                chkDisplayShortHrsWhenEmpAbsent.Checked = chkShowShortHrsOnBaseHrs.Checked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowShortHrsOnBaseHrs_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (13-Sep-2021)-- End


  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblAttCode.Text = Language._Object.getCaption(Me.LblAttCode.Name, Me.LblAttCode.Text)
			Me.chkShowEachEmpOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachEmpOnNewPage.Name, Me.chkShowEachEmpOnNewPage.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkShowHoliday.Text = Language._Object.getCaption(Me.chkShowHoliday.Name, Me.chkShowHoliday.Text)
			Me.chkShowLeaveType.Text = Language._Object.getCaption(Me.chkShowLeaveType.Name, Me.chkShowLeaveType.Text)
			Me.chkShowTimingsIn24Hrs.Text = Language._Object.getCaption(Me.chkShowTimingsIn24Hrs.Name, Me.chkShowTimingsIn24Hrs.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
			Me.chkShift.Text = Language._Object.getCaption(Me.chkShift.Name, Me.chkShift.Text)
			Me.chkBreaktime.Text = Language._Object.getCaption(Me.chkBreaktime.Name, Me.chkBreaktime.Text)
			Me.chkShowBaseHrs.Text = Language._Object.getCaption(Me.chkShowBaseHrs.Name, Me.chkShowBaseHrs.Text)
			Me.chkShowTotalAbsentDays.Text = Language._Object.getCaption(Me.chkShowTotalAbsentDays.Name, Me.chkShowTotalAbsentDays.Text)
            Me.chkIncludeOFFHrsInBaseHrs.Text = Language._Object.getCaption(Me.chkIncludeOFFHrsInBaseHrs.Name, Me.chkIncludeOFFHrsInBaseHrs.Text)
			Me.chkShowLateComingInShortHrs.Text = Language._Object.getCaption(Me.chkShowLateComingInShortHrs.Name, Me.chkShowLateComingInShortHrs.Text)
			Me.chkShowShortHrsOnBaseHrs.Text = Language._Object.getCaption(Me.chkShowShortHrsOnBaseHrs.Name, Me.chkShowShortHrsOnBaseHrs.Text)
			Me.chkDisplayShortHrsWhenEmpAbsent.Text = Language._Object.getCaption(Me.chkDisplayShortHrsWhenEmpAbsent.Name, Me.chkDisplayShortHrsWhenEmpAbsent.Text)
			Me.chkShowActualTimingAfterRoundOff.Text = Language._Object.getCaption(Me.chkShowActualTimingAfterRoundOff.Name, Me.chkShowActualTimingAfterRoundOff.Text)
			Me.chkShowAbsentAfterEmpTerminated.Text = Language._Object.getCaption(Me.chkShowAbsentAfterEmpTerminated.Name, Me.chkShowAbsentAfterEmpTerminated.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Filters saved successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
