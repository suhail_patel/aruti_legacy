'************************************************************************************************************************************
'Class Name : clsAttendanceSummaryReport.vb
'Purpose    :
'Date       :26-Oct-2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsAttendanceSummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAttendanceSummaryReport"
    Private mstrReportId As String = enArutiReport.Attendance_Summary_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeave1 As Integer = -1
    Private mintLeave2 As Integer = -1
    Private mintLeave3 As Integer = -1
    Private mintLeave4 As Integer = -1
    Private mstrLeave1 As String = ""
    Private mstrLeave2 As String = ""
    Private mstrLeave3 As String = ""
    Private mstrLeave4 As String = ""


    'Pinkal (1-Jul-2014) -- Start
    'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    Private mintLeave5 As Integer = -1
    Private mstrLeave5 As String = ""
    'Pinkal (1-Jul-2014) -- End


    'Pinkal (25-Feb-2015) -- Start
    'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
    Private mblnShowTotalHrs As Boolean = False
    'Pinkal (25-Feb-2015) -- End



    Private mstrSpecialLeaveTypeIds As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    'Private mblnIsActive As Boolean = True
    Private mstrOrderByQuery As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId1() As Integer
        Set(ByVal value As Integer)
            mintLeave1 = value
        End Set
    End Property

    Public WriteOnly Property _Leave1() As String
        Set(ByVal value As String)
            mstrLeave1 = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId2() As Integer
        Set(ByVal value As Integer)
            mintLeave2 = value
        End Set
    End Property

    Public WriteOnly Property _Leave2() As String
        Set(ByVal value As String)
            mstrLeave2 = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId3() As Integer
        Set(ByVal value As Integer)
            mintLeave3 = value
        End Set
    End Property

    Public WriteOnly Property _Leave3() As String
        Set(ByVal value As String)
            mstrLeave3 = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId4() As Integer
        Set(ByVal value As Integer)
            mintLeave4 = value
        End Set
    End Property

    Public WriteOnly Property _Leave4() As String
        Set(ByVal value As String)
            mstrLeave4 = value
        End Set
    End Property

    Public WriteOnly Property _SpecialLeaveTypeIds() As String
        Set(ByVal value As String)
            mstrSpecialLeaveTypeIds = value
        End Set
    End Property



    'Pinkal (1-Jul-2014) -- Start
    'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

    Public WriteOnly Property _LeaveId5() As Integer
        Set(ByVal value As Integer)
            mintLeave5 = value
        End Set
    End Property

    Public WriteOnly Property _Leave5() As String
        Set(ByVal value As String)
            mstrLeave5 = value
        End Set
    End Property


    'Pinkal (1-Jul-2014) -- End


    'Pinkal (25-Feb-2015) -- Start
    'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].

    Public WriteOnly Property _ShowTotalHrs() As Boolean
        Set(ByVal value As Boolean)
            mblnShowTotalHrs = value
        End Set
    End Property

    'Pinkal (25-Feb-2015) -- End


    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrOrderByQuery = ""
            mintLeave1 = -1
            mintLeave2 = -1
            mintLeave3 = -1
            mintLeave4 = -1
            mstrLeave1 = ""
            mstrLeave2 = ""
            mstrLeave3 = ""
            mstrLeave4 = ""


            'Pinkal (1-Jul-2014) -- Start
            'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            mintLeave5 = -1
            mstrLeave5 = ""
            'Pinkal (1-Jul-2014) -- End


            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
            mblnShowTotalHrs = False
            'Pinkal (25-Feb-2015) -- End


            mstrSpecialLeaveTypeIds = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mblnIncludeInactiveEmp = False
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            'mblnIsActive = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate1))
            objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate2))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Start Date : ") & " " & mdtDate1 & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "End Date : ") & " " & mdtDate2 & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND tnalogin_summary.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, " Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GetLeaveCount(ByVal mstrLeaveTypeID As String, ByVal intEmployeeId As Integer) As Decimal
        Dim mdcTotalCount As Decimal = 0
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            StrQ &= " SELECT ISNULL(SUM(lvleaveIssue_tran.dayfraction),0.00) AS TotalDays " & _
                         " FROM lvleaveIssue_tran " & _
                         " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                         " AND lvleaveIssue_master.isvoid = 0 AND lvleaveIssue_master.employeeunkid = @EmployeeID " & _
                         " JOIN lvleavetype_master ON lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                         " AND lvleaveIssue_master.leavetypeunkid IN (" & mstrLeaveTypeID & " ) " & _
                         " WHERE lvleaveIssue_tran.isvoid = 0 " & _
                         " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) >= @FromDate " & _
                         " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) <= @ToDate "

            '" GROUP BY lvleavetype_master.leavetypecode , lvleavetype_master.leavename"

            objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate1))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate2))
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdcTotalCount = CDec(dsList.Tables(0).Rows(0)("TotalDays"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLeaveCount; Module Name: " & mstrModuleName)
        End Try
        Return mdcTotalCount
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EmployeeCode", Language.getMessage(mstrModuleName, 2, "Emp.Code")))
            iColumn_DetailReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 3, "Employee")))
            iColumn_DetailReport.Add(New IColumn("Present", Language.getMessage(mstrModuleName, 5, "Present")))
            iColumn_DetailReport.Add(New IColumn("Absent", Language.getMessage(mstrModuleName, 6, "Absent")))
            iColumn_DetailReport.Add(New IColumn("Total", Language.getMessage(mstrModuleName, 8, "Total")))
            iColumn_DetailReport.Add(New IColumn("OT1", Language.getMessage(mstrModuleName, 9, "OT1")))
            iColumn_DetailReport.Add(New IColumn("OT2", Language.getMessage(mstrModuleName, 10, "OT2")))
            iColumn_DetailReport.Add(New IColumn("OT3", Language.getMessage(mstrModuleName, 11, "OT3")))
            iColumn_DetailReport.Add(New IColumn("OT4", Language.getMessage(mstrModuleName, 12, "OT4")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass

    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'ROW_NUMBER() OVER(ORDER BY tnalogin_summary.employeeunkid)


    '        'Pinkal (01-Jul-2014) -- Start
    '        'Enhancement : Oman Changes AS PER MR.PRABHAKAR COMMENT IF EMPLOYEE IS JOINNED IN BETWEEN MONTH THEN BEFORE JOINNED DON'T CONSIDERED ALL DAYS AS ABSENT AND NEVER MATCH EMPLOYEE TIMESHEET REPORT AND THIS REPORT.


    '        StrQ = " SELECT tnalogin_summary.employeeunkid " & _
    '                  " , ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
    '                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
    '                  ", CONVERT(VARCHAR,hremployee_master.appointeddate,103) + ' - ' + CONVERT(VARCHAR,hremployee_master.termination_to_date,103)  AS 'DOJ/DOR' " & _
    '                  ",  CONVERT(CHAR(8),hremployee_master.appointeddate,112) appointeddate " & _
    '                  ",  CONVERT(CHAR(8),hremployee_master.termination_from_date,112) termination_from_date " & _
    '                  ",  CONVERT(CHAR(8),hremployee_master.termination_to_date,112) termination_to_date " & _
    '                  ",  CONVERT(CHAR(8),hremployee_master.empl_enddate,112) empl_enddate " & _
    '                  ", ( SELECT COUNT(*)  FROM  tnalogin_summary t WHERE t.total_hrs > 0 AND t.employeeunkid = tnalogin_summary.employeeunkid AND t.ispaidleave = 0 AND t.login_date >= @fromdate AND t.login_date <= @todate) AS 'Present' " & _
    '                  ", ( SELECT ISNULL(SUM(t.lvdayfraction),0.00)  FROM  tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND t.isunpaidleave = 1 AND t.login_date >= @fromdate AND t.login_date <= @todate) AS 'Absent' " & _
    '                  ", 0 AS Leave1 " & _
    '                  ", 0 AS Leave2 " & _
    '                  ", 0 AS Leave3 " & _
    '                  ", 0 AS Leave4 " & _
    '                  ", 0 AS Leave5 " & _
    '                  ", 0 AS SpecialLeave " & _
    '                  ", 0 AS Total " & _
    '                  ", SUM(ISNULL(total_overtime, 0)) AS OT1 " & _
    '                  ", SUM(ISNULL(ot2, 0)) AS OT2 " & _
    '                  ", SUM(ISNULL(ot3, 0)) AS OT3 " & _
    '                  ", SUM(ISNULL(ot4, 0)) AS OT4 "



    '        'Pinkal (25-Feb-2015) -- Start
    '        'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].

    '        If mblnShowTotalHrs Then
    '            StrQ &= ", SUM(ISNULL(total_hrs,0)) AS TotalWorkedHrs "
    '        End If

    '        'Pinkal (25-Feb-2015) -- End


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= " FROM tnalogin_summary " & _
    '                     " JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If

    '        StrQ &= " WHERE CONVERT(CHAR(8),login_date,112) >= @fromdate AND CONVERT(CHAR(8),login_date,112) <= @todate "

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= " GROUP BY ISNULL(hremployee_master.employeecode, '') " & _
    '                 ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
    '                 ", hremployee_master.appointeddate " & _
    '                 ", hremployee_master.termination_to_date " & _
    '                 ", hremployee_master.termination_from_date " & _
    '                 ", hremployee_master.empl_enddate " & _
    '                 ", tnalogin_summary.employeeunkid "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
    '        End If

    '        If mintViewIndex > 0 Then
    '            StrQ &= " ORDER BY " & mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "").Substring(1) & "," & Me.OrderByQuery
    '        Else
    '            StrQ &= mstrOrderByQuery
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim mintGroupID As Integer = 0
    '        Dim mdecLeave1 As Decimal = 0
    '        Dim mdecLeave2 As Decimal = 0
    '        Dim mdecLeave3 As Decimal = 0
    '        Dim mdecLeave4 As Decimal = 0
    '        Dim mdecLeave5 As Decimal = 0


    '        'Pinkal (1-Jul-2014) -- Start
    '        'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    '        Dim mdecLeave6 As Decimal = 0
    '        Dim mblnIsPaid As Boolean = False
    '        Dim objLeave As New clsleavetype_master
    '        'Pinkal (1-Jul-2014) -- End


    '        Dim mdtPeriod1 As Date = mdtDate1.Date
    '        Dim mdtPeriod2 As Date = mdtDate2.Date


    '        For Each dtRow As DataRow In dsList.Tables("Datatable").Rows
    'Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '            rpt_Row.Item("Column1") = dtRow.Item("Id")
    '            rpt_Row.Item("Column2") = dtRow.Item("GName")
    '            rpt_Row.Item("Column4") = dtRow.Item("EmployeeCode")
    '            rpt_Row.Item("Column5") = dtRow.Item("Employee")


    ''Pinkal (25-Feb-2015) -- Start
    ''Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
    ''rpt_Row.Item("Column6") = dtRow.Item("DOJ/DOR")

    '            If mblnShowTotalHrs Then
    '                rpt_Row.Item("Column6") = CalculateTime(True, CInt(dtRow.Item("TotalWorkedHrs"))).ToString("#00.00")
    '            End If
    ''Pinkal (25-Feb-2015) -- End



    ''Pinkal (24-Jan-2014) -- Start
    ''Enhancement : Oman Changes  'AS PER MR.PRABHAKAR COMMENT IF EMPLOYEE IS JOINNED IN BETWEEN MONTH THEN BEFORE JOINNED ALL DAYS CONSIDERED AS ABSENT AND NEVER MATCH EMPLOYEE TIMESHEET REPORT AND THIS REPORT.

    ''Dim mdecAbesentDays As Decimal = 0
    ''If Not IsDBNull(dtRow.Item("appointeddate").ToString()) Then
    ''    If dtRow.Item("appointeddate").ToString > eZeeDate.convertDate(mdtDate1.Date) Then
    ''        mdecAbesentDays = DateDiff(DateInterval.Day, mdtDate1.Date, eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).Date)
    ''    End If
    ''End If

    ''rpt_Row.Item("Column8") = CDec(CDec(dtRow.Item("Absent")) + mdecAbesentDays).ToString("#0.00")

    ''Pinkal (24-Jan-2014) -- End


    '            mdtDate1 = mdtPeriod1.Date
    '            mdtDate2 = mdtPeriod2.Date


    ''Pinkal (01-Jul-2014) -- Start
    ''Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    ''Dim mdecAbesentDays As Decimal = 0
    'Dim mdecunPaidDays As Decimal = 0
    'Dim mdecPaidDays As Decimal = 0
    ''Pinkal (01-Jul-2014) -- End


    '            If Not IsDBNull(dtRow.Item("appointeddate")) Then
    '                If dtRow.Item("appointeddate").ToString > eZeeDate.convertDate(mdtDate1.Date) Then
    '                    mdtDate1 = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString()).Date
    '                    rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
    '                End If
    '            End If

    '            If Not IsDBNull(dtRow.Item("termination_from_date")) Then
    '                If dtRow.Item("termination_from_date").ToString < eZeeDate.convertDate(mdtDate2.Date) Then
    '                    mdtDate2 = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString()).Date
    '                    rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
    '                End If
    '            End If

    '            If Not IsDBNull(dtRow.Item("termination_to_date")) Then
    '                If dtRow.Item("termination_to_date").ToString < eZeeDate.convertDate(mdtDate2.Date) Then
    '                    mdtDate2 = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString()).Date
    '                    rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
    '                End If
    '            End If

    '            If Not IsDBNull(dtRow.Item("empl_enddate")) Then
    '                If dtRow.Item("empl_enddate").ToString > eZeeDate.convertDate(mdtDate2.Date) Then
    '                    mdtDate2 = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString()).Date
    '                    rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
    '                End If
    '            End If

    ''rpt_Row.Item("Column8") = CDec(CDec(dtRow.Item("Absent")) + mdecAbesentDays).ToString("#0.00")

    ''Pinkal (01-Jul-2014) -- End

    '            If mintLeave1 > 0 Then
    '                rpt_Row.Item("Column8") = GetLeaveCount(mintLeave1.ToString(), CInt(dtRow.Item("employeeunkid")))
    '            Else
    '                rpt_Row.Item("Column8") = 0.0
    '            End If

    '            objLeave._Leavetypeunkid = mintLeave1
    '            If objLeave._IsPaid Then
    '                mdecPaidDays += CDec(rpt_Row.Item("Column8"))
    '            End If

    '            If mintLeave2 > 0 Then
    '                rpt_Row.Item("Column9") = GetLeaveCount(mintLeave2.ToString(), CInt(dtRow.Item("employeeunkid")))
    '            Else
    '                rpt_Row.Item("Column9") = 0.0
    '            End If

    '            objLeave._Leavetypeunkid = mintLeave2
    '            If objLeave._IsPaid Then
    '                mdecPaidDays += CDec(rpt_Row.Item("Column9"))
    '            Else
    '                rpt_Row.Item("Column8") = CDec(CDec(rpt_Row.Item("Column8")) + CDec(dtRow.Item("Absent")) - CDec(rpt_Row.Item("Column8")) - CDec(rpt_Row.Item("Column9"))).ToString("#0.00")
    '            End If

    '            If mintLeave3 > 0 Then
    '                rpt_Row.Item("Column10") = GetLeaveCount(mintLeave3.ToString(), CInt(dtRow.Item("employeeunkid")))
    '            Else
    '                rpt_Row.Item("Column10") = 0.0
    '            End If

    '            objLeave._Leavetypeunkid = mintLeave3
    '            If objLeave._IsPaid Then
    '                mdecPaidDays += CDec(rpt_Row.Item("Column10"))
    '            End If

    '            If mintLeave4 > 0 Then
    '                rpt_Row.Item("Column11") = GetLeaveCount(mintLeave4.ToString(), CInt(dtRow.Item("employeeunkid")))
    '            Else
    '                rpt_Row.Item("Column11") = 0.0
    '            End If

    '            objLeave._Leavetypeunkid = mintLeave4
    '            If objLeave._IsPaid Then
    '                mdecPaidDays += CDec(rpt_Row.Item("Column11"))
    '            End If

    '            If mintLeave5 > 0 Then
    '                rpt_Row.Item("Column12") = GetLeaveCount(mintLeave5.ToString(), CInt(dtRow.Item("employeeunkid")))
    '            Else
    '                rpt_Row.Item("Column12") = 0.0
    '            End If

    '            objLeave._Leavetypeunkid = mintLeave5
    '            If objLeave._IsPaid Then
    '                mdecPaidDays += CDec(rpt_Row.Item("Column12"))
    '            End If

    '            If mstrSpecialLeaveTypeIds.Trim.Length > 0 Then
    '                rpt_Row.Item("Column13") = GetLeaveCount(mstrSpecialLeaveTypeIds, CInt(dtRow.Item("employeeunkid")))
    '            Else
    '                rpt_Row.Item("Column13") = 0.0
    '            End If

    '            mdecPaidDays += CDec(rpt_Row.Item("Column13"))


    ''Pinkal (01-Jul-2014) -- Start
    ''Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    ''rpt_Row.Item("Column7") = DateDiff(DateInterval.Day, mdtDate1, mdtDate2.AddDays(1)) - (CDec(rpt_Row.Item("Column8")) + CDec(rpt_Row.Item("Column9")) + CDec(rpt_Row.Item("Column10")) + CDec(rpt_Row.Item("Column11")) + CDec(rpt_Row.Item("Column12")) + CDec(rpt_Row.Item("Column13")))
    '            rpt_Row.Item("Column7") = CDec(DateDiff(DateInterval.Day, mdtDate1, mdtDate2.AddDays(1)) - (mdecPaidDays + CDec(dtRow.Item("Absent")))).ToString("#0.00")
    ''Pinkal (01-Jul-2014) -- End

    '            rpt_Row.Item("Column14") = CDec(DateDiff(DateInterval.Day, mdtDate1, mdtDate2.AddDays(1))).ToString("#00.00")


    ''rpt_Row.Item("Column14") = CInt(dtRow.Item("Present")) + CDec(rpt_Row.Item("Column9")) + CDec(rpt_Row.Item("Column10")) + CDec(rpt_Row.Item("Column11")) + CDec(rpt_Row.Item("Column12")) + CDec(rpt_Row.Item("Column13"))


    '            rpt_Row.Item("Column15") = CalculateTime(True, CInt(dtRow.Item("OT1"))).ToString("#00.00")
    '            rpt_Row.Item("Column16") = CalculateTime(True, CInt(dtRow.Item("OT2"))).ToString("#00.00")
    '            rpt_Row.Item("Column17") = CalculateTime(True, CInt(dtRow.Item("OT3"))).ToString("#00.00")
    '            rpt_Row.Item("Column18") = CalculateTime(True, CInt(dtRow.Item("OT4"))).ToString("#00.00")



    ''Pinkal (27-Dec-2013) -- Start
    ''Enhancement : Oman Changes



    ''Pinkal (01-Jul-2014) -- Start
    ''Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    '            rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column8"))
    '            rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column9"))
    '            rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column10"))
    '            rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column11"))
    '            rpt_Row.Item("Column85") = CDec(rpt_Row.Item("Column12"))
    '            rpt_Row.Item("Column86") = CDec(rpt_Row.Item("Column13"))
    ''Pinkal (01-Jul-2014) -- End

    '            rpt_Row.Item("Column19") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT1)", "1=1")).ToString("#00.00")
    '            rpt_Row.Item("Column20") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT2)", "1=1")).ToString("#00.00")
    '            rpt_Row.Item("Column21") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT3)", "1=1")).ToString("#00.00")
    '            rpt_Row.Item("Column22") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT4)", "1=1")).ToString("#00.00")

    '            If mintViewIndex > 0 Then
    '                If mintGroupID <> CInt(dtRow.Item("Id")) Then

    ''Pinkal (01-Jul-2014) -- Start
    ''Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    '                    mdecLeave1 = rpt_Row.Item("Column8")
    '                    mdecLeave2 = rpt_Row.Item("Column9")
    '                    mdecLeave3 = rpt_Row.Item("Column10")
    '                    mdecLeave4 = rpt_Row.Item("Column11")
    '                    mdecLeave5 = rpt_Row.Item("Column12")
    '                    mdecLeave6 = rpt_Row.Item("Column13")
    ''Pinkal (01-Jul-2014) -- End
    '                    mintGroupID = CInt(dtRow.Item("Id"))
    '                Else
    ''Pinkal (01-Jul-2014) -- Start
    ''Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    '                    mdecLeave1 = mdecLeave1 + CDec(rpt_Row.Item("Column8"))
    '                    mdecLeave2 = mdecLeave2 + CDec(rpt_Row.Item("Column9"))
    '                    mdecLeave3 = mdecLeave3 + CDec(rpt_Row.Item("Column10"))
    '                    mdecLeave4 = mdecLeave4 + CDec(rpt_Row.Item("Column11"))
    '                    mdecLeave5 = mdecLeave5 + CDec(rpt_Row.Item("Column12"))
    '                    mdecLeave6 = mdecLeave6 + CDec(rpt_Row.Item("Column13"))
    ''Pinkal (01-Jul-2014) -- End
    '                End If

    '                rpt_Row.Item("Column23") = mdecLeave1
    '                rpt_Row.Item("Column24") = mdecLeave2
    '                rpt_Row.Item("Column25") = mdecLeave3
    '                rpt_Row.Item("Column26") = mdecLeave4
    '                rpt_Row.Item("Column27") = mdecLeave5

    ''Pinkal (01-Jul-2014) -- Start
    ''Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    '                rpt_Row.Item("Column32") = mdecLeave6
    ''Pinkal (01-Jul-2014) -- End


    '                rpt_Row.Item("Column28") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT1)", "Id = " & mintGroupID)).ToString("#00.00")
    '                rpt_Row.Item("Column29") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT2)", "Id = " & mintGroupID)).ToString("#00.00")
    '                rpt_Row.Item("Column30") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT3)", "Id = " & mintGroupID)).ToString("#00.00")
    '                rpt_Row.Item("Column31") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT4)", "Id = " & mintGroupID)).ToString("#00.00")

    '            End If


    ''Pinkal (24-Jan-2014) -- Start
    ''Enhancement : Oman Changes

    '            If rpt_Row.Item("Column8").ToString() = "0.00" Then
    '                rpt_Row.Item("Column8") = ""
    '            End If

    '            If rpt_Row.Item("Column9").ToString() = "0.00" Then
    '                rpt_Row.Item("Column9") = ""
    '            End If

    '            If rpt_Row.Item("Column10").ToString() = "0.00" Then
    '                rpt_Row.Item("Column10") = ""
    '            End If

    '            If rpt_Row.Item("Column11").ToString() = "0.00" Then
    '                rpt_Row.Item("Column11") = ""
    '            End If

    '            If rpt_Row.Item("Column12").ToString() = "0.00" Then
    '                rpt_Row.Item("Column12") = ""
    '            End If

    '            If rpt_Row.Item("Column13").ToString() = "0.00" Then
    '                rpt_Row.Item("Column13") = ""
    '            End If

    '            If rpt_Row.Item("Column15").ToString() = "00.00" Then
    '                rpt_Row.Item("Column15") = ""
    '            End If

    '            If rpt_Row.Item("Column16").ToString() = "00.00" Then
    '                rpt_Row.Item("Column16") = ""
    '            End If

    '            If rpt_Row.Item("Column17").ToString() = "00.00" Then
    '                rpt_Row.Item("Column17") = ""
    '            End If

    '            If rpt_Row.Item("Column18").ToString() = "00.00" Then
    '                rpt_Row.Item("Column18") = ""
    '            End If

    ''Pinkal (24-Jan-2014) -- End

    ''Pinkal (27-Dec-2013) -- End


    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '        Next


    '        objRpt = New ArutiReport.Designer.rptAttendanceSummary

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 18, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 19, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 20, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 21, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr.No"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 2, "Emp.Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 3, "Employee"))

    '        'Pinkal (25-Feb-2015) -- Start
    '        'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
    '        'Call ReportFunction.TextChange(objRpt, "txtDOJDOR", Language.getMessage(mstrModuleName, 4, "DOJ/DOR"))
    '        'Pinkal (25-Feb-2015) -- End
    '        Call ReportFunction.TextChange(objRpt, "txtPresent", Language.getMessage(mstrModuleName, 5, "Present"))


    '        'Pinkal (01-Jul-2014) -- Start
    '        'Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

    '        'Call ReportFunction.TextChange(objRpt, "txtAbsent", Language.getMessage(mstrModuleName, 6, "Absent"))

    '        'If mintLeave1 > 0 Then
    '        '    Call ReportFunction.TextChange(objRpt, "txtLeave1", mstrLeave1)
    '        'Else
    '        '    Call ReportFunction.EnableSuppress(objRpt, "txtLeave1", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column91", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column231", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "SumofColumn811", True)
    '        'End If

    '        'If mintLeave2 > 0 Then
    '        '    Call ReportFunction.TextChange(objRpt, "txtLeave2", mstrLeave2)
    '        'Else
    '        '    Call ReportFunction.EnableSuppress(objRpt, "txtLeave2", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column101", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column241", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "SumofColumn821", True)
    '        'End If

    '        'If mintLeave3 > 0 Then
    '        '    Call ReportFunction.TextChange(objRpt, "txtLeave3", mstrLeave3)
    '        'Else
    '        '    Call ReportFunction.EnableSuppress(objRpt, "txtLeave3", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column111", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column251", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "SumofColumn831", True)
    '        'End If

    '        'If mintLeave4 > 0 Then
    '        '    Call ReportFunction.TextChange(objRpt, "txtLeave4", mstrLeave4)
    '        'Else
    '        '    Call ReportFunction.EnableSuppress(objRpt, "txtLeave4", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column121", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "Column261", True)
    '        '    Call ReportFunction.EnableSuppress(objRpt, "SumofColumn841", True)
    '        'End If

    '        'Pinkal (01-Jul-2014) -- End


    '        'Pinkal (01-Jul-2014) -- Start
    '        'Enhancement - Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

    '        If mintLeave1 > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtLeave1", mstrLeave1)
    '        Else
    '            Call ReportFunction.EnableSuppress(objRpt, "txtLeave1", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column231", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "SumofColumn811", True)
    '        End If

    '        If mintLeave2 > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtLeave2", mstrLeave2)
    '        Else
    '            Call ReportFunction.EnableSuppress(objRpt, "txtLeave2", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column91", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column241", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "SumofColumn821", True)
    '        End If

    '        If mintLeave3 > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtLeave3", mstrLeave3)
    '        Else
    '            Call ReportFunction.EnableSuppress(objRpt, "txtLeave3", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column101", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column251", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "SumofColumn831", True)
    '        End If

    '        If mintLeave4 > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtLeave4", mstrLeave4)
    '        Else
    '            Call ReportFunction.EnableSuppress(objRpt, "txtLeave4", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column111", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column261", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "SumofColumn841", True)
    '        End If

    '        If mintLeave5 > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtLeave5", mstrLeave5)
    '        Else
    '            Call ReportFunction.EnableSuppress(objRpt, "txtLeave5", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column121", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column271", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "SumofColumn851", True)
    '        End If

    '        If mstrSpecialLeaveTypeIds.Trim.Length > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtspLeave", Language.getMessage(mstrModuleName, 7, "Special Leave"))
    '        Else
    '            Call ReportFunction.EnableSuppress(objRpt, "txtspLeave", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column131", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column321", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "SumofColumn861", True)
    '        End If

    '        'Pinkal (01-Jul-2014) -- End



    '        'Pinkal (25-Feb-2015) -- Start
    '        'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
    '        If mblnShowTotalHrs = False Then
    '            Call ReportFunction.EnableSuppress(objRpt, "txtTotWorkedHrs", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column61", True)
    '            objRpt.ReportDefinition.ReportObjects("txtTotal").Width = objRpt.ReportDefinition.ReportObjects("txtTotal").Width + objRpt.ReportDefinition.ReportObjects("txtTotWorkedHrs").Width
    '            objRpt.ReportDefinition.ReportObjects("Column141").Width = objRpt.ReportDefinition.ReportObjects("Column141").Width + objRpt.ReportDefinition.ReportObjects("Column61").Width
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtTotWorkedHrs", Language.getMessage(mstrModuleName, 25, "Total Worked Hrs"))
    '        End If
    '        'Pinkal (25-Feb-2015) -- End


    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 8, "Total"))
    '        Call ReportFunction.TextChange(objRpt, "txtOT1", Language.getMessage(mstrModuleName, 9, "OT1"))
    '        Call ReportFunction.TextChange(objRpt, "txtOT2", Language.getMessage(mstrModuleName, 10, "OT2"))
    '        Call ReportFunction.TextChange(objRpt, "txtOT3", Language.getMessage(mstrModuleName, 11, "OT3"))
    '        Call ReportFunction.TextChange(objRpt, "txtOT4", Language.getMessage(mstrModuleName, 12, "OT4"))


    '        'Pinkal (27-Dec-2013) -- Start
    '        'Enhancement : Oman Changes
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 23, "Sub Total : "))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 24, "Total : "))
    '        'Pinkal (27-Dec-2013) -- End



    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT tnalogin_summary.employeeunkid " & _
                      " , ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      ", CONVERT(VARCHAR, hremployee_master.appointeddate, 103) + ' - ' + CONVERT(VARCHAR, RT.date1, 103) AS 'DOJ/DOR' " & _
                      ",  CONVERT(CHAR(8),hremployee_master.appointeddate,112) appointeddate " & _
                      ", CONVERT(CHAR(8), EocDt.date2, 112) termination_from_date " & _
                      ", CONVERT(CHAR(8), RT.date1, 112) termination_to_date " & _
                      ", CONVERT(CHAR(8), EocDt.date1, 112) empl_enddate " & _
                      ", ( SELECT COUNT(*)  FROM  tnalogin_summary t WHERE t.total_hrs > 0 AND t.employeeunkid = tnalogin_summary.employeeunkid AND t.ispaidleave = 0 AND t.login_date >= @fromdate AND t.login_date <= @todate) AS 'Present' " & _
                      ", ( SELECT ISNULL(SUM(t.lvdayfraction),0.00)  FROM  tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND t.isunpaidleave = 1 AND t.login_date >= @fromdate AND t.login_date <= @todate) AS 'Absent' " & _
                      ", 0 AS Leave1 " & _
                      ", 0 AS Leave2 " & _
                      ", 0 AS Leave3 " & _
                      ", 0 AS Leave4 " & _
                      ", 0 AS Leave5 " & _
                      ", 0 AS SpecialLeave " & _
                      ", 0 AS Total " & _
                      ", SUM(ISNULL(total_overtime, 0)) AS OT1 " & _
                      ", SUM(ISNULL(ot2, 0)) AS OT2 " & _
                      ", SUM(ISNULL(ot3, 0)) AS OT3 " & _
                      ", SUM(ISNULL(ot4, 0)) AS OT4 "

            If mblnShowTotalHrs Then
                StrQ &= ", SUM(ISNULL(total_hrs,0)) AS TotalWorkedHrs "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM tnalogin_summary " & _
                    "   JOIN hremployee_master ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               date1 " & _
                    "               ,date2 " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_dates_tran " & _
                    "           WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               date1 " & _
                    "               ,date2 " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_dates_tran " & _
                    "           WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       )AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 "

            'Shani(30-Jan-2016)
            'Old (1141) : - WHERE isvoid = 0 AND datetypeunkid = '4' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            'New (1141) : - WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            'Olf (1151) : - WHERE isvoid = 0 AND datetypeunkid = '6' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            'New (1151) : - WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE CONVERT(CHAR(8),login_date,112) >= @fromdate AND CONVERT(CHAR(8),login_date,112) <= @todate "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY ISNULL(hremployee_master.employeecode, '') " & _
                     ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                     ", hremployee_master.appointeddate " & _
                    "       ,EocDt.date1 " & _
                    "       ,EocDt.date2 " & _
                    "       ,RT.date1 " & _
                     ", tnalogin_summary.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
            End If

            If mintViewIndex > 0 Then
                StrQ &= " ORDER BY " & mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "").Substring(1) & "," & Me.OrderByQuery
            Else
                StrQ &= mstrOrderByQuery
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mintGroupID As Integer = 0
            Dim mdecLeave1 As Decimal = 0
            Dim mdecLeave2 As Decimal = 0
            Dim mdecLeave3 As Decimal = 0
            Dim mdecLeave4 As Decimal = 0
            Dim mdecLeave5 As Decimal = 0


            'Pinkal (1-Jul-2014) -- Start
            'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            Dim mdecLeave6 As Decimal = 0
            Dim mblnIsPaid As Boolean = False
            Dim objLeave As New clsleavetype_master
            'Pinkal (1-Jul-2014) -- End


            Dim mdtPeriod1 As Date = mdtDate1.Date
            Dim mdtPeriod2 As Date = mdtDate2.Date


            For Each dtRow As DataRow In dsList.Tables("Datatable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("Id")
                rpt_Row.Item("Column2") = dtRow.Item("GName")
                rpt_Row.Item("Column4") = dtRow.Item("EmployeeCode")
                rpt_Row.Item("Column5") = dtRow.Item("Employee")

                If mblnShowTotalHrs Then
                    rpt_Row.Item("Column6") = CalculateTime(True, CInt(dtRow.Item("TotalWorkedHrs"))).ToString("#00.00")
                End If
                mdtDate1 = mdtPeriod1.Date
                mdtDate2 = mdtPeriod2.Date

                Dim mdecunPaidDays As Decimal = 0
                Dim mdecPaidDays As Decimal = 0

                If Not IsDBNull(dtRow.Item("appointeddate")) Then
                    If dtRow.Item("appointeddate").ToString > eZeeDate.convertDate(mdtDate1.Date) Then
                        mdtDate1 = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString()).Date
                        rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
                    End If
                End If

                If Not IsDBNull(dtRow.Item("termination_from_date")) Then
                    If dtRow.Item("termination_from_date").ToString < eZeeDate.convertDate(mdtDate2.Date) Then
                        mdtDate2 = eZeeDate.convertDate(dtRow.Item("termination_from_date").ToString()).Date
                        rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
                    End If
                End If

                If Not IsDBNull(dtRow.Item("termination_to_date")) Then
                    If dtRow.Item("termination_to_date").ToString < eZeeDate.convertDate(mdtDate2.Date) Then
                        mdtDate2 = eZeeDate.convertDate(dtRow.Item("termination_to_date").ToString()).Date
                        rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
                    End If
                End If

                If Not IsDBNull(dtRow.Item("empl_enddate")) Then
                    'Pinkal (24-Apr-2018) -- Start
                    'Bug :(Support - 0002214) :  Absents are not coming in the attendance summery sheet
                    'If dtRow.Item("empl_enddate").ToString() > eZeeDate.convertDate(mdtDate2.Date) Then
                    If dtRow.Item("empl_enddate").ToString() < eZeeDate.convertDate(mdtDate2.Date) Then
                        'Pinkal (24-Apr-2018) -- End
                        mdtDate2 = eZeeDate.convertDate(dtRow.Item("empl_enddate").ToString()).Date
                        rpt_Row.Item("Column5") = rpt_Row.Item("Column5") & Space(3) & "*"
                    End If
                End If

                If mintLeave1 > 0 Then
                    rpt_Row.Item("Column8") = GetLeaveCount(mintLeave1.ToString(), CInt(dtRow.Item("employeeunkid")))
                Else
                    rpt_Row.Item("Column8") = 0.0
                End If

                objLeave._Leavetypeunkid = mintLeave1
                If objLeave._IsPaid Then
                    mdecPaidDays += CDec(rpt_Row.Item("Column8"))
                End If

                If mintLeave2 > 0 Then
                    rpt_Row.Item("Column9") = GetLeaveCount(mintLeave2.ToString(), CInt(dtRow.Item("employeeunkid")))
                Else
                    rpt_Row.Item("Column9") = 0.0
                End If

                objLeave._Leavetypeunkid = mintLeave2
                If objLeave._IsPaid Then
                    mdecPaidDays += CDec(rpt_Row.Item("Column9"))
                Else
                    rpt_Row.Item("Column8") = CDec(CDec(rpt_Row.Item("Column8")) + CDec(dtRow.Item("Absent")) - CDec(rpt_Row.Item("Column8")) - CDec(rpt_Row.Item("Column9"))).ToString("#0.00")
                End If

                If mintLeave3 > 0 Then
                    rpt_Row.Item("Column10") = GetLeaveCount(mintLeave3.ToString(), CInt(dtRow.Item("employeeunkid")))
                Else
                    rpt_Row.Item("Column10") = 0.0
                End If

                objLeave._Leavetypeunkid = mintLeave3
                If objLeave._IsPaid Then
                    mdecPaidDays += CDec(rpt_Row.Item("Column10"))
                End If

                If mintLeave4 > 0 Then
                    rpt_Row.Item("Column11") = GetLeaveCount(mintLeave4.ToString(), CInt(dtRow.Item("employeeunkid")))
                Else
                    rpt_Row.Item("Column11") = 0.0
                End If

                objLeave._Leavetypeunkid = mintLeave4
                If objLeave._IsPaid Then
                    mdecPaidDays += CDec(rpt_Row.Item("Column11"))
                End If

                If mintLeave5 > 0 Then
                    rpt_Row.Item("Column12") = GetLeaveCount(mintLeave5.ToString(), CInt(dtRow.Item("employeeunkid")))
                Else
                    rpt_Row.Item("Column12") = 0.0
                End If

                objLeave._Leavetypeunkid = mintLeave5
                If objLeave._IsPaid Then
                    mdecPaidDays += CDec(rpt_Row.Item("Column12"))
                End If

                If mstrSpecialLeaveTypeIds.Trim.Length > 0 Then
                    rpt_Row.Item("Column13") = GetLeaveCount(mstrSpecialLeaveTypeIds, CInt(dtRow.Item("employeeunkid")))
                Else
                    rpt_Row.Item("Column13") = 0.0
                End If

                mdecPaidDays += CDec(rpt_Row.Item("Column13"))

                rpt_Row.Item("Column7") = CDec(DateDiff(DateInterval.Day, mdtDate1, mdtDate2.AddDays(1)) - (mdecPaidDays + CDec(dtRow.Item("Absent")))).ToString("#0.00")
                rpt_Row.Item("Column14") = CDec(DateDiff(DateInterval.Day, mdtDate1, mdtDate2.AddDays(1))).ToString("#00.00")
                rpt_Row.Item("Column15") = CalculateTime(True, CInt(dtRow.Item("OT1"))).ToString("#00.00")
                rpt_Row.Item("Column16") = CalculateTime(True, CInt(dtRow.Item("OT2"))).ToString("#00.00")
                rpt_Row.Item("Column17") = CalculateTime(True, CInt(dtRow.Item("OT3"))).ToString("#00.00")
                rpt_Row.Item("Column18") = CalculateTime(True, CInt(dtRow.Item("OT4"))).ToString("#00.00")
                rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column8"))
                rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column9"))
                rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column10"))
                rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column11"))
                rpt_Row.Item("Column85") = CDec(rpt_Row.Item("Column12"))
                rpt_Row.Item("Column86") = CDec(rpt_Row.Item("Column13"))
                rpt_Row.Item("Column19") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT1)", "1=1")).ToString("#00.00")
                rpt_Row.Item("Column20") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT2)", "1=1")).ToString("#00.00")
                rpt_Row.Item("Column21") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT3)", "1=1")).ToString("#00.00")
                rpt_Row.Item("Column22") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT4)", "1=1")).ToString("#00.00")

                If mintViewIndex > 0 Then
                    If mintGroupID <> CInt(dtRow.Item("Id")) Then
                        mdecLeave1 = rpt_Row.Item("Column8")
                        mdecLeave2 = rpt_Row.Item("Column9")
                        mdecLeave3 = rpt_Row.Item("Column10")
                        mdecLeave4 = rpt_Row.Item("Column11")
                        mdecLeave5 = rpt_Row.Item("Column12")
                        mdecLeave6 = rpt_Row.Item("Column13")
                        mintGroupID = CInt(dtRow.Item("Id"))
                    Else
                        mdecLeave1 = mdecLeave1 + CDec(rpt_Row.Item("Column8"))
                        mdecLeave2 = mdecLeave2 + CDec(rpt_Row.Item("Column9"))
                        mdecLeave3 = mdecLeave3 + CDec(rpt_Row.Item("Column10"))
                        mdecLeave4 = mdecLeave4 + CDec(rpt_Row.Item("Column11"))
                        mdecLeave5 = mdecLeave5 + CDec(rpt_Row.Item("Column12"))
                        mdecLeave6 = mdecLeave6 + CDec(rpt_Row.Item("Column13"))
                    End If

                    rpt_Row.Item("Column23") = mdecLeave1
                    rpt_Row.Item("Column24") = mdecLeave2
                    rpt_Row.Item("Column25") = mdecLeave3
                    rpt_Row.Item("Column26") = mdecLeave4
                    rpt_Row.Item("Column27") = mdecLeave5
                    rpt_Row.Item("Column32") = mdecLeave6
                    rpt_Row.Item("Column28") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT1)", "Id = " & mintGroupID)).ToString("#00.00")
                    rpt_Row.Item("Column29") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT2)", "Id = " & mintGroupID)).ToString("#00.00")
                    rpt_Row.Item("Column30") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT3)", "Id = " & mintGroupID)).ToString("#00.00")
                    rpt_Row.Item("Column31") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(OT4)", "Id = " & mintGroupID)).ToString("#00.00")
                End If

                If rpt_Row.Item("Column8").ToString() = "0.00" Then
                    rpt_Row.Item("Column8") = ""
                End If

                If rpt_Row.Item("Column9").ToString() = "0.00" Then
                    rpt_Row.Item("Column9") = ""
                End If

                If rpt_Row.Item("Column10").ToString() = "0.00" Then
                    rpt_Row.Item("Column10") = ""
                End If

                If rpt_Row.Item("Column11").ToString() = "0.00" Then
                    rpt_Row.Item("Column11") = ""
                End If

                If rpt_Row.Item("Column12").ToString() = "0.00" Then
                    rpt_Row.Item("Column12") = ""
                End If

                If rpt_Row.Item("Column13").ToString() = "0.00" Then
                    rpt_Row.Item("Column13") = ""
                End If

                If rpt_Row.Item("Column15").ToString() = "00.00" Then
                    rpt_Row.Item("Column15") = ""
                End If

                If rpt_Row.Item("Column16").ToString() = "00.00" Then
                    rpt_Row.Item("Column16") = ""
                End If

                If rpt_Row.Item("Column17").ToString() = "00.00" Then
                    rpt_Row.Item("Column17") = ""
                End If

                If rpt_Row.Item("Column18").ToString() = "00.00" Then
                    rpt_Row.Item("Column18") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next


            objRpt = New ArutiReport.Designer.rptAttendanceSummary

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 18, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 19, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 20, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 21, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 2, "Emp.Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 3, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtPresent", Language.getMessage(mstrModuleName, 5, "Present"))

            If mintLeave1 > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtLeave1", mstrLeave1)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtLeave1", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column231", True)
                Call ReportFunction.EnableSuppress(objRpt, "SumofColumn811", True)
            End If

            If mintLeave2 > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtLeave2", mstrLeave2)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtLeave2", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column91", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column241", True)
                Call ReportFunction.EnableSuppress(objRpt, "SumofColumn821", True)
            End If

            If mintLeave3 > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtLeave3", mstrLeave3)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtLeave3", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column101", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column251", True)
                Call ReportFunction.EnableSuppress(objRpt, "SumofColumn831", True)
            End If

            If mintLeave4 > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtLeave4", mstrLeave4)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtLeave4", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column111", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column261", True)
                Call ReportFunction.EnableSuppress(objRpt, "SumofColumn841", True)
            End If

            If mintLeave5 > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtLeave5", mstrLeave5)
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtLeave5", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column121", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column271", True)
                Call ReportFunction.EnableSuppress(objRpt, "SumofColumn851", True)
            End If

            If mstrSpecialLeaveTypeIds.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtspLeave", Language.getMessage(mstrModuleName, 7, "Special Leave"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtspLeave", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column131", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column321", True)
                Call ReportFunction.EnableSuppress(objRpt, "SumofColumn861", True)
            End If

            If mblnShowTotalHrs = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtTotWorkedHrs", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column61", True)
                objRpt.ReportDefinition.ReportObjects("txtTotal").Width = objRpt.ReportDefinition.ReportObjects("txtTotal").Width + objRpt.ReportDefinition.ReportObjects("txtTotWorkedHrs").Width
                objRpt.ReportDefinition.ReportObjects("Column141").Width = objRpt.ReportDefinition.ReportObjects("Column141").Width + objRpt.ReportDefinition.ReportObjects("Column61").Width
            Else
                Call ReportFunction.TextChange(objRpt, "txtTotWorkedHrs", Language.getMessage(mstrModuleName, 25, "Total Worked Hrs"))
            End If

            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 8, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtOT1", Language.getMessage(mstrModuleName, 9, "OT1"))
            Call ReportFunction.TextChange(objRpt, "txtOT2", Language.getMessage(mstrModuleName, 10, "OT2"))
            Call ReportFunction.TextChange(objRpt, "txtOT3", Language.getMessage(mstrModuleName, 11, "OT3"))
            Call ReportFunction.TextChange(objRpt, "txtOT4", Language.getMessage(mstrModuleName, 12, "OT4"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 23, "Sub Total : "))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 24, "Total : "))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Shani(24-Aug-2015) -- End
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sr.No")
            Language.setMessage(mstrModuleName, 2, "Emp.Code")
            Language.setMessage(mstrModuleName, 3, "Employee")
            Language.setMessage(mstrModuleName, 4, "DOJ/DOR")
            Language.setMessage(mstrModuleName, 5, "Present")
            Language.setMessage(mstrModuleName, 6, "Absent")
            Language.setMessage(mstrModuleName, 7, "Special Leave")
            Language.setMessage(mstrModuleName, 8, "Total")
            Language.setMessage(mstrModuleName, 9, "OT1")
            Language.setMessage(mstrModuleName, 10, "OT2")
            Language.setMessage(mstrModuleName, 11, "OT3")
            Language.setMessage(mstrModuleName, 12, "OT4")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")
            Language.setMessage(mstrModuleName, 15, "Start Date :")
            Language.setMessage(mstrModuleName, 16, "End Date :")
            Language.setMessage(mstrModuleName, 17, " Employee :")
            Language.setMessage(mstrModuleName, 18, "Prepared By :")
            Language.setMessage(mstrModuleName, 19, "Checked By :")
            Language.setMessage(mstrModuleName, 20, "Approved By :")
            Language.setMessage(mstrModuleName, 21, "Received By :")
            Language.setMessage(mstrModuleName, 22, " Order By :")
            Language.setMessage(mstrModuleName, 23, "Sub Total :")
            Language.setMessage(mstrModuleName, 24, "Total :")
            Language.setMessage(mstrModuleName, 25, "Total Worked Hrs")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
