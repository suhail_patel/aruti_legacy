﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
#End Region

Public Class clsPDPDevelopmentAreasReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPDPDevelopmentAreasReport"
    Private mstrReportId As String = enArutiReport.PDP_Development_Areas_Report
    Private objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mintPDPGoalViewType As Integer = 0
    Private mblnAllowToViewOtherTraining As Boolean = False

    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PDPGoalViewType() As Integer
        Set(ByVal value As Integer)
            mintPDPGoalViewType = value
        End Set
    End Property

    Public WriteOnly Property _AllowToViewOtherTraining() As Boolean
        Set(ByVal value As Boolean)
            mblnAllowToViewOtherTraining = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mblnIncludeInactiveEmp = False
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mblnAllowToViewOtherTraining = False
            mintPDPGoalViewType = 0

            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.ClearParameters()

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND pdpgoals_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintPDPGoalViewType = CInt(enPDPViewType.Selection) Then
                Me._FilterQuery &= " AND pdpgoals_master.itemunkid > 0 "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Employee :") & " " & mstrEmployeeName & " "
            ElseIf mintPDPGoalViewType = CInt(enPDPViewType.FreeText) Then
                Me._FilterQuery &= " AND pdpgoals_master.itemunkid <= 0 "
            End If


            If mblnAllowToViewOtherTraining = False Then
                Me._FilterQuery &= " AND ISNULL(pdpgoals_trainingneed_tran.isfromcompetency,0) = 1 "
            End If



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function GetStatusData(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True _
                                     ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Dim objtlcycle_master As New clstlstages_master

        Try
            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If




            'strQ &= "IF OBJECT_ID('tempdb..#parentscore') IS NOT NULL DROP TABLE #parentscore " & _
            '         "CREATE TABLE #parentscore ( parentgoalunkid int, AvgScore Decimal(10,2) ); " & _
            '        "INSERT INTO #parentscore " & _
            '             "SELECT " & _
            '                  "A.parentgoalunkid " & _
            '                  ",SUM(A.pct_updated) / COUNT(A.pct_updated) AS AvgScore " & _
            '             "FROM (SELECT " & _
            '                       "pdpgoals_master.parentgoalunkid " & _
            '                     ",ISNULL(pdpgoals_update_tran.pct_updated,0) as pct_updated " & _
            '                     ",ROW_NUMBER() OVER (PARTITION BY pdpgoals_update_tran.pdpgoalsmstunkid ORDER BY pdpgoals_update_tran.updatedatetime DESC) AS xNo " & _
            '                  "FROM pdpgoals_master " & _
            '                  "LEFT JOIN pdpgoals_update_tran " & _
            '                       "ON pdpgoals_master.pdpgoalsmstunkid = pdpgoals_update_tran.pdpgoalsmstunkid AND pdpgoals_update_tran.isvoid = 0 " & _
            '                  "WHERE parentgoalunkid > 0 " & _
            '                  "AND pdpgoals_master.isvoid = 0 " & _
            '                  " ) AS A " & _
            '             "WHERE A.xNo = 1 " & _
            '             "GROUP BY A.parentgoalunkid " & _
            '        "IF OBJECT_ID('tempdb..#parentchild') IS NOT NULL DROP TABLE #parentchild " & _
            '         "CREATE TABLE #parentchild ( parentgoalunkid int, childs int ); " & _
            '        "INSERT INTO #parentchild " & _
            '             "SELECT " & _
            '                  "A.parentGoal " & _
            '                ",COUNT(A.childGoal) AS childs " & _
            '             "FROM (SELECT " & _
            '                       "pdpgoals_master.pdpgoalsmstunkid AS childGoal " & _
            '                     ",pdpgoals_master.parentgoalunkid AS parentGoal " & _
            '                  "FROM pdpgoals_master " & _
            '                  "WHERE parentgoalunkid > 0 " & _
            '                  "AND pdpgoals_master.isvoid = 0) AS A " & _
            '             "GROUP BY A.parentGoal " & _
            '                       ",A.childGoal "

            strQ &= "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                              "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 AND iscompetencyselection = 1 "


            strQ &= "SELECT " & _
                                   "pdpgoals_master.pdpgoalsmstunkid " & _
                                 ",pdpgoals_master.actionplancategoryunkid " & _
                                 ",ISNULL(pdpaction_plan_category.category,'') as actionplancategory " & _
                                 ",pdpgoals_master.employeeunkid " & _
                                 ",CASE " & _
                                        "WHEN pdpgoals_master.itemunkid > 0 THEN CASE " & _
                                                  "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                                                  "ELSE pdpitemdatatran.fieldvalue " & _
                                             "END " & _
                                        "ELSE pdpgoals_master.goal_name " & _
                                   "END AS goal_name " & _
                                 ",pdpgoals_master.goal_description " & _
                                 ",ISNULL(pdpgoals_master.statusunkid,0) AS statusunkid " & _
                                 ",CONVERT(CHAR(8), pdpgoals_master.duedate, 112) AS DueDate " & _
                                 ",pdpgoals_master.itemunkid " & _
                                 ",pdpgoals_master.parentgoalunkid " & _
                                 ",pdpgoals_mentors.employeeunkid AS mentoremployeeunkid " & _
                                 ",ISNULL(pdpgoals_master.pdpgoalsmstunkid, 0) AS Secondary_Sort_Order " & _
                                 ",CASE " & _
                                        "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                                        "ELSE -1 " & _
                                   "END as competencyVal " & _
                                 ",ISNULL(pdpitemdatatran.iscompetencyselection, 0) AS iscompetencyselection " & _
                                ",  hremployee_master.employeecode AS employeecode " & _
                                ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                                ",  ISNULL(employee_department.name, '') AS EmployeeDepartment " & _
                                ",  ISNULL(employee_section_grp.name, '') AS EmployeeSectionGrp " & _
                                ",  ISNULL(employee_class_grp.name, '') AS EmployeeClassGrp " & _
                                ",  ISNULL(employee_class.name, '') AS EmployeeClass " & _
                                ",  ISNULL(employee_grade_grp.name, '') AS EmployeeGraderGrp " & _
                                ",  ISNULL(employee_branch.name, '') AS EmployeeBranch " & _
                                ",  ISNULL(employee_job.job_name, '') AS EmployeeJob " & _
                                ",  ISNULL(cfcommon_master.name,'') AS TrainingName " & _
                                ",  ISNULL(pdpgoals_trainingneed_tran.isrecommended,0) AS isrecommended " & _
                                ",  CASE WHEN pdpgoals_master.itemunkid > 0 THEN @Selection ELSE @FreeText END GoalType " & _
                              "FROM pdpgoals_master " & _
                              "LEFT JOIN pdpformmaster " & _
                                   "ON pdpformmaster.pdpformunkid = pdpgoals_master.pdpformunkid " & _
                                   "AND pdpformmaster.isvoid = 0 " & _
                              "LEFT JOIN pdpaction_plan_category " & _
                                   "ON pdpgoals_master.actionplancategoryunkid = pdpaction_plan_category.actionplancategoryunkid " & _
                                   "AND pdpaction_plan_category.isactive = 1 " & _
                              "LEFT JOIN hremployee_master " & _
                                   "ON pdpformmaster.employeeunkid = hremployee_master.employeeunkid " & _
                              "LEFT JOIN pdpitemdatatran " & _
                                   "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
                                   "AND pdpitemdatatran.isvoid = 0 " & _
                              "LEFT JOIN #competencyList " & _
                                   "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                              "LEFT JOIN pdpgoals_mentors " & _
                                   "ON pdpgoals_mentors.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_mentors.isvoid = 0 " & _
                              "LEFT JOIN pdpgoals_trainingneed_tran " & _
                                   "ON pdpgoals_trainingneed_tran.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_trainingneed_tran.isvoid = 0 " & _
                              "LEFT JOIN cfcommon_master " & _
                                    "ON cfcommon_master.masterunkid = pdpgoals_trainingneed_tran.trainingcourseunkid " & _
                                    "and mastertype = " & CInt(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER) & " "
                              
                             


            strQ &= "LEFT JOIN (SELECT " & _
                   "stationunkid " & _
                   ",deptgroupunkid " & _
                   ",departmentunkid " & _
                   ",sectiongroupunkid " & _
                   ",sectionunkid " & _
                   ",unitgroupunkid " & _
                   ",unitunkid " & _
                   ",teamunkid " & _
                   ",classgroupunkid " & _
                   ",classunkid " & _
                   ",employeeunkid " & _
                   ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                "FROM hremployee_transfer_tran " & _
                "WHERE isvoid = 0 " & _
                "AND CONVERT(CHAR(8), effectivedate, 112) <='" & eZeeDate.convertDate(xPeriodEnd) & "') AS Emp_transfer " & _
                "ON Emp_transfer.employeeunkid = hremployee_master.employeeunkid " & _
                     "AND Emp_transfer.Rno = 1 " & _
           "LEFT JOIN (SELECT " & _
                     "jobgroupunkid " & _
                   ",jobunkid " & _
                   ",employeeunkid " & _
                   ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                "FROM hremployee_categorization_tran " & _
                "WHERE isvoid = 0 " & _
                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "') AS Emp_categorization " & _
                "ON Emp_categorization.employeeunkid = hremployee_master.employeeunkid " & _
                     "AND Emp_categorization.Rno = 1 " & _
           "LEFT JOIN (SELECT " & _
                     "gradegroupunkid " & _
                   ",gradeunkid " & _
                   ",gradelevelunkid " & _
                   ",employeeunkid " & _
                   ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                "FROM prsalaryincrement_tran " & _
                "WHERE isvoid = 0 " & _
                "AND isapproved = 1 " & _
                "AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "') AS Emp_Grade " & _
                "ON Emp_Grade.employeeunkid = hremployee_master.employeeunkid " & _
                     "AND Emp_Grade.Rno = 1 "

            '********************** DATA FOR DATES CONDITION ************************' --- START

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'strQ &= mstrAnalysis_Join

            strQ &= "LEFT JOIN hrdepartment_master as employee_department " & _
                         "ON Emp_transfer.departmentunkid = employee_department.departmentunkid " & _
                              "AND employee_department.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrjob_master as employee_job " & _
                         "ON Emp_categorization.jobunkid = employee_job.jobunkid " & _
                              "AND employee_job.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrsectiongroup_master AS  employee_section_grp " & _
                         "ON Emp_transfer.sectiongroupunkid = employee_section_grp.sectiongroupunkid " & _
                              "AND employee_section_grp.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrclassgroup_master as  employee_class_grp " & _
                         "ON Emp_transfer.classgroupunkid = employee_class_grp.classgroupunkid " & _
                              "AND employee_class_grp.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrclasses_master as employee_class " & _
                         "ON Emp_transfer.classunkid = employee_class.classesunkid " & _
                              "AND employee_class.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrgradegroup_master as employee_grade_grp " & _
                         "ON Emp_Grade.gradegroupunkid = employee_grade_grp.gradegroupunkid " & _
                              "AND employee_grade_grp.isactive = 1 " & _
                    " " & _
                    "LEFT JOIN hrstation_master AS employee_branch " & _
                         "ON Emp_transfer.stationunkid = employee_branch.stationunkid " & _
                              "AND employee_branch.isactive = 1 "

            strQ &= "WHERE pdpgoals_master.isvoid = 0 and isrecommended = 1 and ISNULL(pdpformmaster.status,-1) = " & CInt(clspdpform_master.enPDPFormStatus.Lock) & " "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            Call FilterTitleAndFilterQuery()
            strQ &= Me._FilterQuery

            strQ &= " ORDER by pdpgoals_master.employeeunkid "


            objDataOperation.AddParameter("@Selection", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1126, "Selected From List"))
            objDataOperation.AddParameter("@FreeText", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1127, "Free Text"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOtherStagesDetailList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )


        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty


        Try

            objDataOperation = New clsDataOperation
            dsList = GetStatusData(xDatabaseName, xUserUnkid, _
                                          xYearUnkid, xCompanyUnkid, _
                                          xPeriodStart, xPeriodEnd, _
                                          xUserModeSetting, _
                                          xOnlyApproved, mblnIncludeInactiveEmp, _
                                          "DevelopmentGoalList")


            dtFinalTable = New DataTable()

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Name", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Company", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage("clsMasterData", 430, "Branch")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Function", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage("clsMasterData", 428, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage("clsMasterData", 427, "Section Group")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Zone", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage("clsMasterData", 420, "Class Group")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Branch", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage("clsMasterData", 419, "Classes")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Job", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Job")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("GradeLevel", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage("clsMasterData", 1063, "Grade Group")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Areaofdevelopment", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Area Of Development")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("RecommendedTraining", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Recommended Training")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("GoalType", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "Goal Type")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)


            Dim oldEmployeeId As Integer = -1
            Dim rpt_Row As DataRow = Nothing


            For Each drRow As DataRow In dsList.Tables("DevelopmentGoalList").Rows
                rpt_Row = dtFinalTable.NewRow

                'If oldEmployeeId <> CInt(drRow("employeeunkid")) Then
                oldEmployeeId = CInt(drRow("employeeunkid"))

                rpt_Row.Item("EmployeeCode") = drRow.Item("employeecode")
                rpt_Row.Item("Name") = drRow.Item("employeename")
                rpt_Row.Item("Company") = drRow.Item("EmployeeBranch")
                rpt_Row.Item("Function") = drRow.Item("EmployeeDepartment")
                rpt_Row.Item("Department") = drRow.Item("EmployeeSectionGrp")
                rpt_Row.Item("Zone") = drRow.Item("EmployeeClassGrp")
                rpt_Row.Item("Branch") = drRow.Item("EmployeeClass")
                rpt_Row.Item("Job") = drRow.Item("EmployeeJob")
                rpt_Row.Item("GradeLevel") = drRow.Item("EmployeeGraderGrp")

                'End If

                rpt_Row.Item("Areaofdevelopment") = drRow.Item("goal_name")
                rpt_Row.Item("RecommendedTraining") = drRow.Item("TrainingName")
                rpt_Row.Item("GoalType") = drRow.Item("GoalType")

                dtFinalTable.Rows.Add(rpt_Row)

            Next


            dtFinalTable.AcceptChanges()
            mdtTableExcel = dtFinalTable

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", False, rowsArrayHeader)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally


        End Try
    End Sub


#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Code")
			Language.setMessage(mstrModuleName, 2, "Employee Name")
			Language.setMessage(mstrModuleName, 8, "Job")
			Language.setMessage(mstrModuleName, 10, "Area Of Development")
			Language.setMessage(mstrModuleName, 11, "Recommended Training")
			Language.setMessage(mstrModuleName, 15, "Prepared By :")
			Language.setMessage(mstrModuleName, 16, "Checked By :")
			Language.setMessage(mstrModuleName, 17, "Approved By")
			Language.setMessage(mstrModuleName, 18, "Received By :")
			Language.setMessage(mstrModuleName, 19, "Employee :")
			Language.setMessage("clsMasterData", 419, "Classes")
			Language.setMessage("clsMasterData", 420, "Class Group")
			Language.setMessage("clsMasterData", 427, "Section Group")
			Language.setMessage("clsMasterData", 428, "Department")
			Language.setMessage("clsMasterData", 430, "Branch")
			Language.setMessage("clsMasterData", 1063, "Grade Group")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
