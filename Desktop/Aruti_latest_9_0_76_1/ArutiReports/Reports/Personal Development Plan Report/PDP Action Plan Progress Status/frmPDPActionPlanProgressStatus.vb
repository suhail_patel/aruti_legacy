﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmPDPActionPlanProgressStatus
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPDPActionPlanProgressStatus"
    Private objPDPActionPlanProgressStatus As clsPDPActionPlanProgressStatus

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Dim strEmployeeId As String = ""
#End Region

#Region " Constructor "
    Public Sub New()
        objPDPActionPlanProgressStatus = New clsPDPActionPlanProgressStatus(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPDPActionPlanProgressStatus.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmPDPActionPlanProgressStatus_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPDPActionPlanProgressStatus = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPActionPlanProgressStatus_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPActionPlanProgressStatus_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPActionPlanProgressStatus_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPActionPlanProgressStatus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPActionPlanProgressStatus_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPActionPlanProgressStatus_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPActionPlanProgressStatus_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTalentExitsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTalentExitsReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objpdpaction_plan_category As New clspdpaction_plan_category
        Dim ObjEmp As New clsEmployee_Master

        Try
            dsCombo = objpdpaction_plan_category.GetCategoryList("ActionplanList", True)
            With cboActionPlanCategory
                .ValueMember = "actionplancategoryunkid"
                .DisplayMember = "category"
                .DataSource = dsCombo.Tables("ActionplanList")
                .SelectedValue = 0
            End With

            dsCombo = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            chkIncludeInactiveEmp.Checked = False
            cboActionPlanCategory.SelectedIndex = 0
            If cboEmployee.DataSource IsNot Nothing AndAlso cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objPDPActionPlanProgressStatus.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objPDPActionPlanProgressStatus.SetDefaultValue()

            objPDPActionPlanProgressStatus._Categoryid = cboActionPlanCategory.SelectedValue
            objPDPActionPlanProgressStatus._CategoryName = cboActionPlanCategory.Text
            objPDPActionPlanProgressStatus._EmployeeUnkid = cboEmployee.SelectedValue
            objPDPActionPlanProgressStatus._EmployeeName = cboEmployee.Text
            objPDPActionPlanProgressStatus._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objPDPActionPlanProgressStatus._ViewByIds = mstrStringIds
            objPDPActionPlanProgressStatus._ViewIndex = mintViewIdx
            objPDPActionPlanProgressStatus._ViewByName = mstrStringName
            objPDPActionPlanProgressStatus._Analysis_Fields = mstrAnalysis_Fields
            objPDPActionPlanProgressStatus._Analysis_Join = mstrAnalysis_Join
            objPDPActionPlanProgressStatus._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPDPActionPlanProgressStatus._Report_GroupName = mstrReport_GroupName
            objPDPActionPlanProgressStatus._Advance_Filter = mstrAdvanceFilter
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objPDPActionPlanProgressStatus.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchActionPlanCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchActionPlanCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboActionPlanCategory.DataSource
            frm.ValueMember = cboActionPlanCategory.ValueMember
            frm.DisplayMember = cboActionPlanCategory.DisplayMember
            If frm.DisplayDialog Then
                cboActionPlanCategory.SelectedValue = frm.SelectedValue
                cboActionPlanCategory.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchStatus_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Events"

#End Region

#Region " Other Control's Events "

    'Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
    '    Dim frm As New frmViewAnalysis
    '    Try
    '        frm.displayDialog()
    '        mstrStringIds = frm._ReportBy_Ids
    '        mstrStringName = frm._ReportBy_Name
    '        mintViewIdx = frm._ViewIndex
    '        mstrAnalysis_Fields = frm._Analysis_Fields
    '        mstrAnalysis_Join = frm._Analysis_Join
    '        mstrAnalysis_OrderBy = frm._Analysis_OrderBy
    '        mstrReport_GroupName = frm._Report_GroupName
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
    '    Finally
    '        frm = Nothing
    '    End Try
    'End Sub


    Private Sub lnkAdvanceFilter_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.lblActionPlanCategory.Text = Language._Object.getCaption(Me.lblActionPlanCategory.Name, Me.lblActionPlanCategory.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class