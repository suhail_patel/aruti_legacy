'************************************************************************************************************************************
'Class Name :clsEmployeeExpenseReport.vb
'Purpose    :
'Date       : 04-Jun-2019
'Written By : Pinkal Jariwala.
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

Public Class clsEmployeeExpenseReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeExpenseReport"
    Private mstrReportId As String = enArutiReport.Employee_Expense_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintExpenseCategoryId As Integer = 0
    Private mstrExpenseCategory As String = ""
    Private mintExpenseId As Integer = 0
    Private mstrExpense As String = ""
    Private mintCurrencyId As Integer = 0
    Private mstrCurrencySign As String = ""
    Private mintBaseCurrencyId As Integer = 0
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mstrOrderByQuery As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private menExportAction As enExportAction
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategoryId() As Integer
        Set(ByVal value As Integer)
            mintExpenseCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategory() As String
        Set(ByVal value As String)
            mstrExpenseCategory = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseId() As Integer
        Set(ByVal value As Integer)
            mintExpenseId = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _CurrencySign() As String
        Set(ByVal value As String)
            mstrCurrencySign = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public Property _PaymentApprovalwithLeaveApproval()
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintExpenseCategoryId = 0
            mstrExpenseCategory = ""
            mintExpenseId = 0
            mstrExpense = ""
            mintBaseCurrencyId = 0
            mintCurrencyId = 0
            mstrCurrencySign = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mstrOrderByQuery = ""
            mblnIncludeInactiveEmp = False
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
            mblnPaymentApprovalwithLeaveApproval = False
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetDefaultValue", mstrModuleName)
        End Try

    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "From Date : ") & " " & mdtFromDate.ToShortDateString & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "To Date : ") & " " & mdtToDate.ToShortDateString & " "

            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromDate).ToString())
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate).ToString())

            If mintExpenseCategoryId > 0 Then
                objDataOperation.AddParameter("@ExpenseCatID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @ExpenseCatID "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Expense Category : ") & " " & mstrExpenseCategory & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Employee : ") & " " & mstrEmployeeName & " "
            End If


            'Pinkal (27-Jun-2019) -- Start
            'Enhancement - Change in Employee Expense Report As Per NMB Requirement.
            If mintExpenseId > 0 Then
                objDataOperation.AddParameter("@ExpenseID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseId)
                '    Me._FilterQuery &= " AND .expenseunkid = @ExpenseID "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Expense : ") & " " & mstrExpense & " "
            End If
            'Pinkal (27-Jun-2019) -- End

            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@currencyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                Me._FilterQuery &= " AND request.countryunkid = @currencyId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Currency : ") & " " & mstrCurrencySign & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Order By :") & " " & Me.OrderByDisplay & " "

                'Pinkal (13-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If mintViewIndex > 0 Then
                '    Me._FilterQuery &= "ORDER BY GName," & Me.OrderByQuery
                'Else
                '    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                'End If

                If mintViewIndex > 0 Then
                    mstrOrderByQuery &= "ORDER BY GName," & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= "ORDER BY " & Me.OrderByQuery
                End If
                'Pinkal (13-Jun-2019) -- End

            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setDefaultOrderBy", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setOrderBy", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("Employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("Employee", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("SectionGrp", Language.getMessage(mstrModuleName, 10, "Section Group")))
            iColumn_DetailReport.Add(New IColumn("Class", Language.getMessage(mstrModuleName, 3, "Class")))

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Create_OnDetailReport", mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                             ByVal intUserUnkid As Integer, _
                                             ByVal intYearUnkid As Integer, _
                                             ByVal strFinancialYear_Name As String, _
                                             ByVal intCompanyUnkid As Integer, _
                                             ByVal dtPeriodStart As Date, _
                                             ByVal dtPeriodEnd As Date, _
                                             ByVal strUserModeSetting As String, _
                                             ByVal blnOnlyApproved As Boolean, _
                                             ByVal strExportPath As String, _
                                             ByVal blnOpenAfterExport As Boolean, _
                                             Optional ByVal ExportAction As enExportAction = enExportAction.None _
                                        )
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        menExportAction = ExportAction

        Dim mdtTableExcel As New DataTable
        Dim dtCol As DataColumn

        dtCol = New DataColumn("Sr.No", System.Type.GetType("System.Int32"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 13, "Sr.No")
        dtCol.DefaultValue = 0
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("GrpName", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 15, "Group Name")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("EmpCode", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Employee Code")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("Employee", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Name")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("SectionGroup", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Section Group")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("Class", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Class")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("Domicile", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Domicile")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("AppliedAmt", System.Type.GetType("System.Decimal"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Applied Amount")
        dtCol.DefaultValue = 0
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("ApprovedAmt", System.Type.GetType("System.Decimal"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Approved Amount")
        dtCol.DefaultValue = 0
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("AccNo", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Account Number")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        dtCol = New DataColumn("Year", System.Type.GetType("System.String"))
        dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Year")
        dtCol.DefaultValue = ""
        mdtTableExcel.Columns.Add(dtCol)

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            'Pinkal (27-Jun-2019) -- Start
            'Enhancement - Change in Employee Expense Report As Per NMB Requirement.

            'StrQ = " SELECT " & _
            '          " hremployee_master.employeeunkid " & _
            '          ", cmclaim_request_master.crmasterunkid " & _
            '          ", cmclaim_request_master.expensetypeid " & _
            '          ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode "

            'If mblnFirstNamethenSurname = True Then
            '    StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
            'Else
            '    StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
            'End If

            'StrQ &= ", ISNULL(SM.name, '') AS SectionGrp " & _
            '              ", ISNULL(CM.name,'') AS Class " & _
            '              ", ISNULL(hremployee_master.domicile_address1, '') + ' ' + ISNULL(hremployee_master.domicile_address2, '')  AS Domicile " & _
            '              ", 0 AS AppliedAmt " & _
            '              ", 0 AS ApprovedAmt " & _
            '              ", '' AS AccNo " & _
            '              ", '" & strFinancialYear_Name & "' as Year " & _
            '              ", ISNULL(cmclaim_request_master.p2prequisitionid,'') AS p2prequisitionid "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "  FROM cmclaim_request_master " & _
            '             "  JOIN cmclaim_request_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
            '             "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
            '             "  LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_request_tran.expenseunkid " & _
            '             "  LEFT JOIN  " & _
            '             "  ( " & _
            '             "      SELECT " & _
            '             "      sectiongroupunkid " & _
            '             "    , employeeunkid " & _
            '             "    , classunkid " & _
            '             "    , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '             "      FROM hremployee_transfer_tran " & _
            '             "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtToDate) & "' " & _
            '             "  ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid  AND Alloc.rno = 1 " & _
            '             " LEFT JOIN hrsectiongroup_master AS SM ON Alloc.sectiongroupunkid = SM.sectiongroupunkid " & _
            '             " LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid "


            StrQ = " SELECT " & _
                      " hremployee_master.employeeunkid " & _
                      ", cmclaim_request_master.crmasterunkid " & _
                      ", cmclaim_request_master.expensetypeid " & _
                      ", cmclaim_request_master.transactiondate " & _
                      ", cmclaim_request_master.claimrequestno " & _
                      ", Approval.approvaldate " & _
                      ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
            End If

            StrQ &= ", ISNULL(SM.name, '') AS SectionGrp " & _
                          ", ISNULL(CM.name,'') AS Class " & _
                          ", ISNULL(hremployee_master.domicile_address1, '') + ' ' + ISNULL(hremployee_master.domicile_address2, '')  AS Domicile " & _
                          ", ISNULL(Request.AppliedAmt,0.00) AS AppliedAmt " & _
                          ", ISNULL(Approval.ApprovedAmt,0.00) AS ApprovedAmt   " & _
                          ", '' AS AccNo " & _
                          ", '" & strFinancialYear_Name & "' as Year " & _
                          ", ISNULL(cmclaim_request_master.p2prequisitionid,'') AS p2prequisitionid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "  FROM cmclaim_request_master " & _
                         "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                         "  LEFT JOIN  " & _
                         "  ( " & _
                         "      SELECT " & _
                         "      sectiongroupunkid " & _
                         "    , employeeunkid " & _
                         "    , classunkid " & _
                         "    , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "      FROM hremployee_transfer_tran " & _
                         "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtToDate) & "' " & _
                         "  ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid  AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrsectiongroup_master AS SM ON Alloc.sectiongroupunkid = SM.sectiongroupunkid " & _
                         " LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                         " LEFT JOIN " & _
                         " ( " & _
                         "      SELECT " & _
                         "              crmasterunkid " & _
                         ",             countryunkid " & _
                         ",             SUM(amount) AS AppliedAmt "

            If mintExpenseId > 0 Then StrQ &= ", expenseunkid "

            StrQ &= "      FROM cmclaim_request_tran " & _
                         "      WHERE isvoid = 0 "

            If mintExpenseId > 0 Then StrQ &= " AND expenseunkid = @ExpenseID "

            StrQ &= " GROUP BY crmasterunkid,countryunkid "

            If mintExpenseId > 0 Then StrQ &= ",expenseunkid"

            StrQ &= " ) AS Request 	ON Request.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                         " LEFT JOIN " & _
                         " ( " & _
                         "      SELECT " & _
                         "          crmasterunkid " & _
                         ",         approvaldate " & _
                         ",         SUM(amount) AS ApprovedAmt " & _
                         ",         ROW_NUMBER() OVER (PARTITION BY crmasterunkid ORDER BY approvaldate DESC) AS rno "

            If mintExpenseId > 0 Then StrQ &= ", expenseunkid "

            StrQ &= "      FROM cmclaim_approval_tran " & _
                         "      WHERE isvoid = 0 And approvaldate Is Not NULL And statusunkid = 1 "

            If mintExpenseId > 0 Then StrQ &= " AND expenseunkid = @ExpenseID "

            StrQ &= "      GROUP BY crmasterunkid,approvaldate "

            If mintExpenseId > 0 Then StrQ &= ", expenseunkid "

            StrQ &= " ) AS Approval ON Approval.crmasterunkid = cmclaim_request_master.crmasterunkid AND Approval.rno = 1 "

            'Pinkal (27-Jun-2019) -- End

            'Pinkal (13-Jun-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", cmclaim_request_tran.expenseunkid " & _]

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  cmclaim_request_master.isvoid = 0 " & _
                         " AND CONVERT(CHAR(8),Approval.approvaldate,112) >= @FromDate AND CONVERT(CHAR(8),Approval.approvaldate,112) <= @ToDate " & _
                         " AND cmclaim_request_master.statusunkid = 1"

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (27-Jun-2019) -- Start
            'Enhancement - Change in Employee Expense Report As Per NMB Requirement.

            'StrQ &= " GROUP BY hremployee_master.employeeunkid " & _
            '             " ,cmclaim_request_master.crmasterunkid " & _
            '             " ,cmclaim_request_master.expensetypeid " & _
            '             ",ISNULL(hremployee_master.employeecode, '')  " & _
            '             ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  " & _
            '             ",ISNULL(SM.name, '')  " & _
            '             ",ISNULL(CM.name, '') " & _
            '             ",ISNULL(hremployee_master.domicile_address1, '') + ' ' + ISNULL(hremployee_master.domicile_address2, '') " & _
            '             ",ISNULL(cmclaim_request_master.p2prequisitionid, '')  "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            'End If

            'Pinkal (27-Jun-2019) -- End

            'Pinkal (13-Jun-2019) -- End


            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim dt_Row As DataRow = Nothing
            Dim iCnt As Integer = 1 : Dim StrGrpName As String = String.Empty
            Dim dtTable As DataTable
            If mstrReport_GroupName.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables("DataTable"), "", "GName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("DataTable")
            End If

            Dim objEmpBank As New clsEmployeeBanks
            Dim objClaimRequestTran As New clsclaim_request_tran
            Dim objClaimRequest As New clsclaim_request_master
            objClaimRequest._IsPaymentApprovalwithLeaveApproval = mblnPaymentApprovalwithLeaveApproval

            For Each dFRow As DataRow In dsList.Tables(0).Rows
                dt_Row = mdtTableExcel.NewRow
                dt_Row.Item("Sr.No") = iCnt.ToString
                dt_Row.Item("EmpCode") = dFRow.Item("EmployeeCode")
                dt_Row.Item("Employee") = dFRow.Item("Employee")
                dt_Row.Item("SectionGroup") = dFRow.Item("SectionGrp")
                dt_Row.Item("Class") = dFRow.Item("Class")
                dt_Row.Item("Domicile") = dFRow.Item("Domicile")

                'Pinkal (27-Jun-2019) -- Start
                'Enhancement - Change in Employee Expense Report As Per NMB Requirement.

                'dt_Row.Item("AppliedAmt") = Format(objClaimRequestTran.GetAppliedExpenseAmount(CInt(dFRow.Item("crmasterunkid"))), GUI.fmtCurrency)

                'If dFRow.Item("p2prequisitionid").ToString().Length > 0 Then
                '    dt_Row.Item("ApprovedAmt") = Format(objClaimRequestTran.GetAppliedExpenseAmount(CInt(dFRow.Item("crmasterunkid"))), GUI.fmtCurrency)
                'Else
                '    dt_Row.Item("ApprovedAmt") = Format(objClaimRequest.GetFinalApproverApprovedAmount(CInt(dFRow.Item("employeeunkid")), CInt(dFRow.Item("expensetypeid")), CInt(dFRow.Item("crmasterunkid"))), GUI.fmtCurrency)
                'End If

                dt_Row.Item("AppliedAmt") = Format(CDec(dFRow.Item("AppliedAmt")), GUI.fmtCurrency)
                dt_Row.Item("ApprovedAmt") = Format(CDec(dFRow.Item("ApprovedAmt")), GUI.fmtCurrency)

                'Pinkal (27-Jun-2019) -- End

                Dim dsEmpBank As DataSet = objEmpBank.GetEmployeeBanks(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, mdtFromDate.Date, mdtToDate.Date, strUserModeSetting, True, False, CInt(dFRow.Item("employeeunkid")), True, "")
                If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                    dt_Row.Item("AccNo") = dsEmpBank.Tables(0).Rows(0)("accountno").ToString()
                Else
                    dt_Row.Item("AccNo") = ""
                End If

                dt_Row.Item("Year") = dFRow.Item("Year")
                dt_Row.Item("GrpName") = dFRow.Item("GName")
                mdtTableExcel.Rows.Add(dt_Row)
                iCnt += 1
            Next
            objClaimRequestTran = Nothing
            objClaimRequest = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayFooter As New ArrayList
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strGTotal As String = Language.getMessage(mstrModuleName, 6, "Grand Total :")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 7, "Sub Total :")
            Dim strarrGroupColumns As String() = Nothing
            If mintViewIndex > 0 Then
                Dim strGrpCols As String() = {"GrpName"}
                strarrGroupColumns = strGrpCols
            End If
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GrpName").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
            Else
                mdtTableExcel.Columns.Remove("GrpName")
            End If

            If mdtTableExcel IsNot Nothing Then

                If mintExpenseCategoryId > 0 Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Expense Category :") & " " & mstrExpenseCategory, "s10bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayHeader.Add(row)
                End If

                If mintExpenseId > 0 Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Expense :") & " " & mstrExpense, "s10bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayHeader.Add(row)
                End If

                If mintEmployeeId > 0 Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Employee :") & " " & mstrEmployeeName, "s10bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayHeader.Add(row)
                End If

                If mintCurrencyId > 0 Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Currency :") & " " & mstrCurrencySign, "s10bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayHeader.Add(row)
                End If

                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    intArrayColumnWidth(i) = 125
                Next

                '*******   REPORT FOOTER   ******
                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s8w")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayFooter.Add(row)
                '----------------------

                row = New WorksheetRow()
                If ConfigParameter._Object._IsShowPreparedBy = True Then
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Prepared By :"), "s8bw")
                    wcell.MergeAcross = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)

                End If

                If ConfigParameter._Object._IsShowApprovedBy = True Then
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Approved By :"), "s8bw")
                    wcell.MergeAcross = 1
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)

                End If


                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Chief Financial Officer :"), "s8bw")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                rowsArrayFooter.Add(row)


                'Pinkal (07-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrExpense & " " & mdtFromDate.Date.ToString("dd MMM") & " - " & mdtToDate.Date.ToString("dd MMM") & " " & mdtToDate.Year, "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, IIf(mstrExpense.ToString().Length < 0, mstrExpense, Me._ReportName) & " " & mdtFromDate.Date.ToString("dd MMM") & " - " & mdtToDate.Date.ToString("dd MMM") & " " & mdtToDate.Year, "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)
                'Pinkal (07-Jun-2019) -- End


            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Generate_DetailReport", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Code")
			Language.setMessage(mstrModuleName, 2, "Employee Name")
			Language.setMessage(mstrModuleName, 3, "Class")
			Language.setMessage(mstrModuleName, 4, "Domicile")
			Language.setMessage(mstrModuleName, 5, "Applied Amount")
			Language.setMessage(mstrModuleName, 6, "Grand Total :")
			Language.setMessage(mstrModuleName, 7, "Sub Total :")
			Language.setMessage(mstrModuleName, 8, "Approved Amount")
			Language.setMessage(mstrModuleName, 9, "Account Number")
			Language.setMessage(mstrModuleName, 10, "Section Group")
			Language.setMessage(mstrModuleName, 11, "Year")
			Language.setMessage(mstrModuleName, 12, "Chief Financial Officer :")
			Language.setMessage(mstrModuleName, 13, "Sr.No")
			Language.setMessage(mstrModuleName, 14, "Approved By :")
			Language.setMessage(mstrModuleName, 15, "Group Name")
			Language.setMessage(mstrModuleName, 16, "From Date :")
			Language.setMessage(mstrModuleName, 17, "To Date :")
			Language.setMessage(mstrModuleName, 18, "Expense Category :")
			Language.setMessage(mstrModuleName, 19, "Prepared By :")
			Language.setMessage(mstrModuleName, 20, "Expense :")
			Language.setMessage(mstrModuleName, 21, "Currency :")
			Language.setMessage(mstrModuleName, 22, "Order By :")
			Language.setMessage(mstrModuleName, 23, "Employee :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
