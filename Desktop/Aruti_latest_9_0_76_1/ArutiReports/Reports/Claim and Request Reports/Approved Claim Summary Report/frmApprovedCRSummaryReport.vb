'************************************************************************************************************************************
'Class Name : frmApprovedCRSummaryReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApprovedCRSummaryReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmApprovedCRSummaryReport"
    Private ObjApprovedCRSummary As clsApprovedCRSummaryReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintCurrencyId As Integer = 0
#End Region

#Region " Contructor "

    Public Sub New()
        ObjApprovedCRSummary = New clsApprovedCRSummaryReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        ObjApprovedCRSummary.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try

            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
            End With

            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)

            With cboExpenseCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
            End With

            dsCombos = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombos = objExchange.getComboList("List", True, False)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables(0).Copy()
                .SelectedIndex = 0
            End With

            Dim drCurrencySign = dsCombos.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("isbasecurrency") = 1).Select(Function(x) x.Field(Of Integer)("countryunkid"))

            If drCurrencySign.Count > 0 Then
                cboCurrency.SelectedValue = CInt(drCurrencySign(0))
                mintCurrencyId = CInt(drCurrencySign(0))
            End If

            objExchange = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboExpenseCategory.SelectedValue = 0
            dtpApprovedFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpApprovedToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpApprovedFromDate.Checked = False
            dtpApprovedToDate.Checked = False
            cboEmployee.SelectedValue = 0
            cboUOM.SelectedValue = 0
            cboExpense.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            cboCurrency.SelectedValue = mintCurrencyId
            ObjApprovedCRSummary.setDefaultOrderBy(0)
            txtOrderBy.Text = ObjApprovedCRSummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            ObjApprovedCRSummary.SetDefaultValue()

            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.please select Expense Category."), enMsgBoxStyle.Information)
                cboExpenseCategory.Select()
                Return False

            ElseIf CInt(cboUOM.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "UOM is compulsory information.please select UOM."), enMsgBoxStyle.Information)
                cboUOM.Select()
                Return False

            End If

            ObjApprovedCRSummary._ApprovedFromDate = IIf(dtpApprovedFromDate.Checked = True, dtpApprovedFromDate.Value.Date, Nothing)
            ObjApprovedCRSummary._ApprovedToDate = IIf(dtpApprovedToDate.Checked = True, dtpApprovedToDate.Value.Date, Nothing)
            ObjApprovedCRSummary._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            ObjApprovedCRSummary._ExpCateName = cboExpenseCategory.Text.ToString()
            ObjApprovedCRSummary._EmpUnkId = CInt(cboEmployee.SelectedValue)
            ObjApprovedCRSummary._EmpName = cboEmployee.Text.ToString
            ObjApprovedCRSummary._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            ObjApprovedCRSummary._UserUnkid = User._Object._Userunkid
            ObjApprovedCRSummary._CompanyUnkId = Company._Object._Companyunkid
            ObjApprovedCRSummary._UOMId = CInt(cboUOM.SelectedValue)
            ObjApprovedCRSummary._UOM = cboUOM.Text
            ObjApprovedCRSummary._ExpenseID = CInt(cboExpense.SelectedValue)
            ObjApprovedCRSummary._Expense = cboExpense.Text
            ObjApprovedCRSummary._ViewByIds = mstrStringIds
            ObjApprovedCRSummary._ViewIndex = mintViewIdx
            ObjApprovedCRSummary._ViewByName = mstrStringName
            ObjApprovedCRSummary._Analysis_Fields = mstrAnalysis_Fields
            ObjApprovedCRSummary._Analysis_Join = mstrAnalysis_Join
            ObjApprovedCRSummary._Analysis_OrderBy = mstrAnalysis_OrderBy
            ObjApprovedCRSummary._Report_GroupName = mstrReport_GroupName
            ObjApprovedCRSummary._CurrencyId = CInt(cboCurrency.SelectedValue)

            If CInt(cboUOM.SelectedValue) = enExpUoM.UOM_AMOUNT Then
                If CInt(cboCurrency.SelectedValue) <= 0 Then
                    ObjApprovedCRSummary._Currency = ""
                Else
                    ObjApprovedCRSummary._Currency = cboCurrency.Text
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmApprovedCRSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            ObjApprovedCRSummary = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmApprovedCRSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprovedCRSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            Me._Title = ObjApprovedCRSummary._ReportName
            Me._Message = ObjApprovedCRSummary._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprovedCRSummaryReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprovedCRSummaryReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsImprestBalanceReport.SetMessages()
            objfrm._Other_ModuleNames = "clsClaimRequestsummaryReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmCRSummaryReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            ObjApprovedCRSummary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            ObjApprovedCRSummary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpenseCategory.DataSource
            frm.ValueMember = cboExpenseCategory.ValueMember
            frm.DisplayMember = cboExpenseCategory.DisplayMember
            If frm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = frm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            ObjApprovedCRSummary.setOrderBy(0)
            txtOrderBy.Text = ObjApprovedCRSummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpense.DataSource
            frm.ValueMember = cboExpense.ValueMember
            frm.DisplayMember = cboExpense.DisplayMember
            If frm.DisplayDialog Then
                cboExpense.SelectedValue = frm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objExpense = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUOM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUOM.SelectedIndexChanged
        Try
            If CInt(cboUOM.SelectedValue) = enExpUoM.UOM_QTY Then
                cboCurrency.SelectedValue = 0
                cboCurrency.Enabled = False
            ElseIf CInt(cboUOM.SelectedValue) = enExpUoM.UOM_AMOUNT Then
                cboCurrency.SelectedValue = mintCurrencyId
                cboCurrency.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUOM_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblApprovedFromDate.Text = Language._Object.getCaption(Me.lblApprovedFromDate.Name, Me.lblApprovedFromDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
			Me.lblApprovedToDate.Text = Language._Object.getCaption(Me.lblApprovedToDate.Name, Me.lblApprovedToDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.LblUOM.Text = Language._Object.getCaption(Me.LblUOM.Name, Me.LblUOM.Text)
			Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Category is compulsory information.please select Expense Category.")
			Language.setMessage(mstrModuleName, 2, "UOM is compulsory information.please select UOM.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
