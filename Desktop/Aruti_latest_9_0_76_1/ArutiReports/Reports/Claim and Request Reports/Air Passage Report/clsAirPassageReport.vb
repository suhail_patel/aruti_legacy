'************************************************************************************************************************************
'Class Name :clsAirPassageReport.vb
'Purpose    :
'Date       : 11-Apr-2014
'Written By : Pinkal Jariwala.
'Modified   :
'************************************************************************************************************************************


Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala.
''' </summary>
''' 
'''

Public Class clsAirPassageReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAirPassageReport"
    Private mstrReportId As String = enArutiReport.Air_Passage_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mdtAsonDate As DateTime = Nothing
    Private mintExpenseCategoryId As Integer = 0
    Private mstrExpenseCategory As String = 0
    Private mintEmployeeId As Integer = 0
    Private mstrEmployee As String = ""
    Private mintSectorId As Integer = 0
    Private mstrSector As String = ""
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private mintYearId As Integer = 0
    Private mintLeaveBalanceSetting As Integer = 0
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

#End Region

#Region " Properties "

    Public WriteOnly Property _AsonDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsonDate = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategoryId() As Integer
        Set(ByVal value As Integer)
            mintExpenseCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategory() As String
        Set(ByVal value As String)
            mstrExpenseCategory = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _Employee() As String
        Set(ByVal value As String)
            mstrEmployee = value
        End Set
    End Property

    Public WriteOnly Property _SectorId() As Integer
        Set(ByVal value As Integer)
            mintSectorId = value
        End Set
    End Property

    Public WriteOnly Property _Sector() As String
        Set(ByVal value As String)
            mstrSector = value
        End Set
    End Property

    Public WriteOnly Property _blnPaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtAsonDate = ConfigParameter._Object._CurrentDateAndTime.Date
            mintExpenseCategoryId = 0
            mstrExpenseCategory = ""
            mintEmployeeId = 0
            mstrEmployee = ""
            mintSectorId = 0
            mstrSector = ""
            mintLeaveBalanceSetting = 0
            mintYearId = 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " As On Date : ") & " " & mdtAsonDate.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Employee :") & " " & mstrEmployee & " "
            End If

            If mintSectorId > 0 Then
                objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectorId)
                Me._FilterQuery &= " AND cmclaim_approval_tran.secrouteunkid = @secrouteunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Sector :") & " " & mstrSector & " "
            End If

            If mintExpenseCategoryId > 0 Then
                objDataOperation.AddParameter("@expensecategoryId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @expensecategoryId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Expense Category :") & " " & mstrExpenseCategory & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            Dim objMstData As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intPeriodId As Integer = objMstData.getCurrentPeriodID(enModuleReference.Payroll, mdtAsonDate.Date, 0, mintYearId, False)
            Dim intPeriodId As Integer = objMstData.getCurrentPeriodID(enModuleReference.Payroll, mdtAsonDate.Date, mintYearId, 0, False)
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objPeriod As New clscommom_period_Tran
            Dim objClaimMst As New clsclaim_request_master
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = intPeriodId
            objPeriod._Periodunkid(strDatabaseName) = intPeriodId
            'Sohail (21 Aug 2015) -- End


            'StrQ = "SELECT DISTINCT  cmclaim_request_master.employeeunkid, ISNULL(hremployee_master.employeecode, '') AS employeecode" & _
            '           ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername,'')  + ' ' + ISNULL(hremployee_master.surname,'') employee " & _
            '           ", -1 AS crapproverunkid " & _
            '           ", -1 AS approveremployeeunkid " & _
            '           ", -1 AS crlevelunkid " & _
            '           ", -1 AS crpriority " & _
            '           ", ISNULL(cmclaim_request_master.statusunkid,2) AS statusunkid " & _
            '           "  FROM cmclaim_request_master " & _
            '           " JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
            '           " WHERE  cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 "


            StrQ = "SELECT cmexpbalance_tran.employeeunkid, ISNULL(hremployee_master.employeecode, '') AS employeecode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ", ISNULL(cmexpbalance_tran.accrue_amount,0.00) AS accrue_amount " & _
            '             ", ISNULL(cmexpbalance_tran.daily_amount,0.00) AS daily_amount " & _
            '             ", cmexpbalance_tran.startdate " & _
            '             ", cmexpbalance_tran.enddate " & _
            '             ", ISNULL(cfcommon_master.masterunkid, 0) AS sectorId " & _
            '             ", ISNULL(cfcommon_master.name, '') AS sector " & _
            '             ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing " & _
            '             ", cmclaim_request_master.statusunkid " & _
            '             " FROM cmexpbalance_tran " & _
            '             " JOIN hremployee_master ON cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '             " JOIN cmclaim_request_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
            '             " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmexpbalance_tran.expenseunkid AND isaccrue = 1  " & _
            '             " JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmexpbalance_tran.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmclaim_approval_tran.isvoid = 0 " & _
            '             " LEFT JOIN cmexpense_costing_tran ON cmclaim_approval_tran.secrouteunkid = cmexpense_costing_tran.secrouteunkid AND cmexpense_costing_tran.isvoid = 0 " & _
            '             " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmexpense_costing_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
            '             " WHERE  1 =  1 "
            StrQ &= ", ISNULL(cmexpbalance_tran.accrue_amount,0.00) AS accrue_amount " & _
                         ", ISNULL(cmexpbalance_tran.daily_amount,0.00) AS daily_amount " & _
                         ", cmexpbalance_tran.startdate " & _
                         ", cmexpbalance_tran.enddate " & _
                         ", ISNULL(cfcommon_master.masterunkid, 0) AS sectorId " & _
                         ", ISNULL(cfcommon_master.name, '') AS sector " & _
                         ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing " & _
                         ", cmclaim_request_master.statusunkid " & _
                         " FROM cmexpbalance_tran " & _
                         " JOIN hremployee_master ON cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                         " JOIN cmclaim_request_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
                         " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmexpbalance_tran.expenseunkid AND isaccrue = 1  " & _
                         " JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmexpbalance_tran.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                         " LEFT JOIN cmexpense_costing_tran ON cmclaim_approval_tran.secrouteunkid = cmexpense_costing_tran.secrouteunkid AND cmexpense_costing_tran.isvoid = 0 " & _
                         " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmexpense_costing_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  1 =  1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            'If mblnIncludeInactiveEmp = False Then
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " GROUP BY cmexpbalance_tran.employeeunkid,ISNULL(hremployee_master.employeecode, '') "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "
            End If

            StrQ &= ", ISNULL(cfcommon_master.name, '') " & _
                        ", ISNULL(cmexpense_costing_tran.amount,0.00) " & _
                        ", cmclaim_request_master.statusunkid " & _
                        ", ISNULL(cfcommon_master.masterunkid, 0) " & _
                        ", ISNULL(cmexpbalance_tran.accrue_amount,0.00) " & _
                        ", ISNULL(cmexpbalance_tran.daily_amount,0.00) " & _
                        ", cmexpbalance_tran.startdate " & _
                        ", cmexpbalance_tran.enddate "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mdcBeginValue As Decimal = 0
            Dim mdcEndValue As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dtRow.Item("employee")
                rpt_Rows.Item("Column3") = Math.Round(CDec(dtRow.Item("accrue_amount")), 2)   'No of tickets per Year
                rpt_Rows.Item("Column4") = dtRow.Item("sector")  'Sector
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("costing")), GUI.fmtCurrency)   'Amount

                Dim mstrDate As String = objClaimMst.GetSectorLastAvailedDate(CInt(dtRow.Item("employeeunkid")), mintExpenseCategoryId, CInt(dtRow.Item("sectorId")), mblnPaymentApprovalwithLeaveApproval).ToString()

                If mstrDate.Trim.Length > 0 Then
                    rpt_Rows.Item("Column6") = eZeeDate.convertDate(mstrDate).ToShortDateString  ' Date Last Availed
                Else
                    rpt_Rows.Item("Column6") = ""   ' Date Last Availed
                End If

                Dim intDiff As Integer = DateDiff(DateInterval.Day, CDate(dtRow.Item("startdate")), objPeriod._Start_Date.Date.AddDays(1))
                rpt_Rows.Item("Column7") = Math.Round(CDec(CDec(dtRow.Item("daily_amount")) * intDiff), 4)   'Balance as on Begining of Period

                Dim intMonthdays As Integer = DateTime.DaysInMonth(mdtAsonDate.Year, mdtAsonDate.Month)
                rpt_Rows.Item("Column8") = Math.Round(CDec(CDec(dtRow.Item("daily_amount")) * intMonthdays), 4) ' Accrue for the period
                rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("costing")) * CDec(rpt_Rows.Item("Column8")), GUI.fmtCurrency)  'Value Accrue for the period 

                rpt_Rows.Item("Column10") = objClaimMst.GetFinalApproverApprovedQtyFromSector(CInt(dtRow.Item("employeeunkid")), mintExpenseCategoryId, CInt(dtRow.Item("sectorId")), mblnPaymentApprovalwithLeaveApproval)   'Ticket used for period
                rpt_Rows.Item("Column11") = CDec(rpt_Rows.Item("Column7")) + CDec(rpt_Rows.Item("Column8")) - CDec(rpt_Rows.Item("Column10"))  'Qty Balance as on End of the Period
                rpt_Rows.Item("Column12") = Format(CDec(rpt_Rows.Item("Column11")) * CDec(rpt_Rows.Item("Column5")), GUI.fmtCurrency)

                mdcBeginValue += CDec(rpt_Rows.Item("Column9"))
                mdcEndValue += CDec(rpt_Rows.Item("Column12"))

                rpt_Rows.Item("Column13") = Format(mdcBeginValue, GUI.fmtCurrency)
                rpt_Rows.Item("Column14") = Format(mdcEndValue, GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            objRpt = New ArutiReport.Designer.rptAirPassageReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            'Dim arrImageRow As DataRow = Nothing
            'arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'Dim objImg(0) As Object

            'If Company._Object._Image IsNot Nothing Then
            '    objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            'Else
            '    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
            '    objImg(0) = eZeeDataType.image2Data(imgBlank)
            'End If

            'arrImageRow.Item("arutiLogo") = objImg(0)

            'rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 5, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 6, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtServiceYear", Language.getMessage(mstrModuleName, 7, "Tickets Per Service Yr"))
            Call ReportFunction.TextChange(objRpt, "txtSector", Language.getMessage(mstrModuleName, 8, "Sector"))
            Call ReportFunction.TextChange(objRpt, "txtCostperticket", Language.getMessage(mstrModuleName, 9, "Cost Per ticket"))
            Call ReportFunction.TextChange(objRpt, "txtLastAvailed", Language.getMessage(mstrModuleName, 10, "Date Last Availed"))
            Call ReportFunction.TextChange(objRpt, "txtbalonBeginPeriod", Language.getMessage(mstrModuleName, 11, "Balance as on beginning of Period"))
            Call ReportFunction.TextChange(objRpt, "txtbeginQty", Language.getMessage(mstrModuleName, 12, "Qty"))
            Call ReportFunction.TextChange(objRpt, "txtbeginValue", Language.getMessage(mstrModuleName, 13, "Value"))
            Call ReportFunction.TextChange(objRpt, "txtticketused", Language.getMessage(mstrModuleName, 14, "Tickets Used for the Period"))
            Call ReportFunction.TextChange(objRpt, "txtEndQty", Language.getMessage(mstrModuleName, 12, "Qty"))
            Call ReportFunction.TextChange(objRpt, "txtEndValue", Language.getMessage(mstrModuleName, 13, "Value"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 15, "Total : "))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 16, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Code")
            Language.setMessage(mstrModuleName, 6, "Employee")
            Language.setMessage(mstrModuleName, 7, "Tickets Per Service Yr")
            Language.setMessage(mstrModuleName, 8, "Sector")
            Language.setMessage(mstrModuleName, 9, "Cost Per ticket")
            Language.setMessage(mstrModuleName, 10, "Date Last Availed")
            Language.setMessage(mstrModuleName, 11, "Balance as on beginning of Period")
            Language.setMessage(mstrModuleName, 12, "Qty")
            Language.setMessage(mstrModuleName, 13, "Value")
            Language.setMessage(mstrModuleName, 14, "Tickets Used for the Period")
            Language.setMessage(mstrModuleName, 15, "Total :")
            Language.setMessage(mstrModuleName, 16, "Printed By :")
            Language.setMessage(mstrModuleName, 17, "Printed Date :")
            Language.setMessage(mstrModuleName, 18, " As On Date :")
            Language.setMessage(mstrModuleName, 19, "Employee :")
            Language.setMessage(mstrModuleName, 20, "Sector :")
            Language.setMessage(mstrModuleName, 21, "Expense Category :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
