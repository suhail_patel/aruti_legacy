
Imports eZeeCommonLib
Imports Aruti.Data
Imports System

Public Class clsEmployeeClaimForm
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeClaimForm"
    Private mstrReportId As String = enArutiReport.Employee_Claim_Form
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintExpenseCategoryID As Integer = 0
    Private mstrExpenseCategory As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintClaimFormId As Integer = 0
    Private mstrClaimForm As String = ""
    Private mstrSpecialAllowanceIds As String = ""
    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintYearId As Integer = 0
    Private mdtFinStartDate As DateTime = Nothing
    Private mdtFinEndDate As DateTime = Nothing

    'Pinkal (05-May-2020) -- Start
    'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
    ' Private mblnPaymentApprovalwithLeaveApproval As Boolean = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    'Pinkal (05-May-2020) -- End

    Private mintLeaveBalanceSetting As Integer = 0
    Private mintLeaveAccrueTenureSetting As Integer = 0
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    Private mintPeriodID As Integer = 0
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private mdtClaimDate As Date = Nothing
    Private mblnShowEmpScale As Boolean = False


    'Pinkal (19-Jul-2021)-- Start
    'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
    Private mblnIsPrinted As Boolean = False
    Private mdtPrintedDateTime As DateTime = Nothing
    Private mintPrintUserId As Integer = 0
    Private mstrPrintedIp As String = ""
    Private mstrPrintedHost As String = ""
    Private mblnIsPrintFromWeb As Boolean = False
    'Pinkal (19-Jul-2021)-- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategoryId() As Integer
        Set(ByVal value As Integer)
            mintExpenseCategoryID = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseCategory() As String
        Set(ByVal value As String)
            mstrExpenseCategory = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ClaimFormId() As Integer
        Set(ByVal value As Integer)
            mintClaimFormId = value
        End Set
    End Property

    Public WriteOnly Property _SpecialAllowanceIds() As String
        Set(ByVal value As String)
            mstrSpecialAllowanceIds = value
        End Set
    End Property

    Public WriteOnly Property _ClaimFormName() As String
        Set(ByVal value As String)
            mstrClaimForm = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _Fin_StartDate() As Date
        Set(ByVal value As Date)
            mdtFinStartDate = value
        End Set
    End Property

    Public WriteOnly Property _Fin_Enddate() As Date
        Set(ByVal value As Date)
            mdtFinEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    Public WriteOnly Property _PeiordID() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property


    'Pinkal (19-Jul-2021)-- Start
    'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.

    Public Property _IsPrinted() As Boolean
        Get
            Return mblnIsPrinted
        End Get
        Set(ByVal value As Boolean)
            mblnIsPrinted = value
        End Set
    End Property

    Public Property _PrintedDateTime() As DateTime
        Get
            Return mdtPrintedDateTime
        End Get
        Set(ByVal value As DateTime)
            mdtPrintedDateTime = value
        End Set
    End Property

    Public Property _PrintUserId() As Integer
        Get
            Return mintPrintUserId
        End Get
        Set(ByVal value As Integer)
            mintPrintUserId = value
        End Set
    End Property

    Public Property _PrintedIp() As String
        Get
            Return mstrPrintedIp
        End Get
        Set(ByVal value As String)
            mstrPrintedIp = value
        End Set
    End Property

    Public Property _PrintedHost() As String
        Get
            Return mstrPrintedHost
        End Get
        Set(ByVal value As String)
            mstrPrintedHost = value
        End Set
    End Property

    Public Property _IsPrintFromWeb() As Boolean
        Get
            Return mblnIsPrintFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsPrintFromWeb = value
        End Set
    End Property

    'Pinkal (19-Jul-2021)-- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintExpenseCategoryID = 0
            mstrExpenseCategory = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintClaimFormId = 0
            mstrClaimForm = ""
            mstrSpecialAllowanceIds = ""
            mstrOrderByQuery = ""
            mintYearId = 0
            mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year
            mdtFinEndDate = Nothing
            mdtFinEndDate = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintClaimFormId > 0 Then
                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
                Me._FilterQuery &= " AND cmclaim_request_master.crmasterunkid  = @crmasterunkid "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid  = @EmployeeId "
            End If

            If mintExpenseCategoryID > 0 Then
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
                Me._FilterQuery &= " AND  cmclaim_request_master.expensetypeid = @expensetypeid "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Try


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                               , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                               , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                               , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                               , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                               , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid

            Dim mintCountryId As Integer = Company._Object._Countryunkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            mintUserUnkid = xUserUnkid
            User._Object._Userunkid = mintUserUnkid


            'Pinkal (05-May-2020) -- Start
            'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
            If Company._Object._Name.ToString().ToUpper() = "ABOOD GROUP OF COMPANIES." OrElse Company._Object._Name.ToString().ToUpper() = "GOOD NEIGHBORS TANZANIA" Then
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)
                'Pinkal (30-May-2020) -- Start
                'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            ElseIf Company._Object._Name.ToString().ToUpper() = "KADCO" Then
                objRpt = Generate_FuelApplicationForm(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)
                'Pinkal (30-May-2020) -- End
            Else
                objRpt = GenerateEmployee_ClaimForm(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, True, xUserModeSetting)
            End If
            'Pinkal (05-May-2020) -- End

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function GetEmployeeSalaryDetails() As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " SELECT " & _
                       "	0 AS 'Salary'" & _
                       "   ,prpayrollprocess_tran.employeeunkid" & _
                       "   ,payperiodunkid" & _
                       "   ,ISNULL(amount, 0.00) AS 'Amount'" & _
                       " FROM prpayrollprocess_tran" & _
                       " LEFT JOIN prtnaleave_tran	ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid" & _
                       " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid" & _
                       " WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtranhead_master.isvoid = 0" & _
                       " AND prpayrollprocess_tran.employeeunkid = @employeeunkid AND prtranhead_master.typeof_id = " & enTypeOf.Salary & _
                       " AND prtnaleave_tran.payperiodunkid = @periodunkid" & _
                       " UNION" & _
                       " SELECT" & _
                       "	1 AS 'Allowances' " & _
                       "   ,prpayrollprocess_tran.employeeunkid " & _
                       "   ,payperiodunkid " & _
                       "   ,ISNULL(SUM(amount), 0.00) AS 'Amount' " & _
                       " FROM prpayrollprocess_tran " & _
                       " LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                       " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                       " WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 AND prtranhead_master.isvoid = 0 " & _
                       " AND prpayrollprocess_tran.employeeunkid = @employeeunkid  AND prpayrollprocess_tran.tranheadunkid IN (" & mstrSpecialAllowanceIds & ") " & _
                       " AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                       " GROUP BY prpayrollprocess_tran.employeeunkid,payperiodunkid "

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodID)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeSalaryDetails; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass


        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim mintLeaveTypeId As Integer = 0
        Dim mintLeaveFormId As Integer = 0
        Dim mstrLeaveType As String = ""

        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_LeaveDetails As ArutiReport.Designer.dsArutiReport
        Dim rpt_PreviousDetails As ArutiReport.Designer.dsArutiReport
        Dim rpt_ExpenseDetails As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimApprovers As ArutiReport.Designer.dsArutiReport


        Try
            objDataOperation = New clsDataOperation


            StrQ = " SELECT  cmclaim_request_master.employeeunkid " & _
                       ",cmclaim_request_master.crmasterunkid " & _
                       ",hremployee_master.employeecode " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                       ",hrjob_master.job_name AS jobtitle " & _
                       ",hrgrade_master.name AS grade " & _
                       ",hrdepartment_master.name AS department " & _
                       " ,hremployee_master.appointeddate " & _
                       ",hremployee_master.confirmation_date " & _
                       ",cmclaim_request_master.claimrequestno " & _
                       ",cmclaim_request_master.transactiondate " & _
                       ",cmclaim_request_master.remark As ClaimRemark "


            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then

                StrQ &= ",ISNULL(lvleaveform.formunkid,0) As formunkid " & _
                             ",ISNULL(lvleaveform.formno,'') AS formno " & _
                             ",ISNULL(lvleaveform.leavetypeunkid,0) AS leavetypeunkid " & _
                             ",ISNULL(lvleavetype_master.leavename,'') AS leavename " & _
                             ",lvleaveform.applydate " & _
                             ",lvleaveform.startdate " & _
                             ",lvleaveform.returndate " & _
                             ",lvleaveform.approve_stdate " & _
                             ",lvleaveform.approve_eddate " & _
                             ",lvleaveform.addressonleave " & _
                             ",lvleaveform.remark AS LeaveRemark "
            End If

            StrQ &= " FROM cmclaim_request_master "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then

                StrQ &= " LEFT JOIN lvleaveform on lvleaveform.formunkid = cmclaim_request_master.referenceunkid " & _
                             " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid "
            End If

            StrQ &= " JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                         " JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                        " JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "        gradeunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "    FROM prsalaryincrement_tran " & _
                        "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                        ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                        " JOIN " & _
                                " ( " & _
                                "    SELECT " & _
                                "         departmentunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                                " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                       " JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         date1 " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_dates_tran " & _
                       "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                       " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                       " WHERE cmclaim_request_master.isvoid = 0 "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " AND lvleaveform.isvoid = 0 "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_LeaveDetails = New ArutiReport.Designer.dsArutiReport
            rpt_PreviousDetails = New ArutiReport.Designer.dsArutiReport
            rpt_ExpenseDetails = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimApprovers = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("claimrequestno")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("department")
                If Not IsDBNull(dtRow.Item("appointeddate")) Then
                    rpt_Rows.Item("Column6") = CDate(dtRow.Item("appointeddate")).ToShortDateString
                Else
                    rpt_Rows.Item("Column6") = ""
                End If

                If mblnShowEmpScale AndAlso mstrSpecialAllowanceIds.Trim.Length > 0 Then
                    Dim dtTable As DataTable = GetEmployeeSalaryDetails()

                    If dtTable.Rows.Count > 0 Then
                        rpt_Rows.Item("Column7") = Format(CDec(dtTable.Rows(0)("amount")), GUI.fmtCurrency)
                        rpt_Rows.Item("Column8") = Format(CDec(dtTable.Rows(1)("amount")), GUI.fmtCurrency)

                        If IsDBNull(dtTable.Compute("SUM(amount)", "1=1")) = False Then
                            rpt_Rows.Item("Column9") = Format(CDec(dtTable.Compute("SUM(amount)", "1=1")), GUI.fmtCurrency)
                        Else
                            rpt_Rows.Item("Column9") = Format(0, GUI.fmtCurrency)
                        End If
                    Else
                        rpt_Rows.Item("Column7") = Format(0, GUI.fmtCurrency)
                        rpt_Rows.Item("Column8") = Format(0, GUI.fmtCurrency)
                        rpt_Rows.Item("Column9") = Format(0, GUI.fmtCurrency)
                    End If
                Else
                    rpt_Rows.Item("Column7") = Format(0, GUI.fmtCurrency)
                    rpt_Rows.Item("Column8") = Format(0, GUI.fmtCurrency)
                    rpt_Rows.Item("Column9") = Format(0, GUI.fmtCurrency)
                End If

                If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                    mintLeaveFormId = CInt(dtRow.Item("formunkid"))
                    mintLeaveTypeId = dtRow.Item("leavetypeunkid")
                    mstrLeaveType = dtRow.Item("leavename").ToString()
                End If
                mdtClaimDate = CDate(dtRow.Item("transactiondate")).Date
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            '/* START PREVIOUS CLAIM APPLICATION DETAILS

            StrQ = " SELECT " & _
                       "  ISNULL(cmclaim_request_master.crmasterunkid, 0) AS crmasterunkid " & _
                       ",  ISNULL(cmclaim_request_master.employeeunkid, 0) AS employeeunkid " & _
                       ",  ISNULL(cmclaim_request_tran.expenseunkid, 0) AS expenseunkid " & _
                       ",  ISNULL(cmexpense_master.name, '') AS Expense " & _
                       ",  cmclaim_request_master.transactiondate " & _
                       ",  ISNULL( cmclaim_request_tran.amount,0.00) As RequestedAmt " & _
                       " FROM cmclaim_request_master " & _
                       " LEFT JOIN cmclaim_request_tran ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid AND cmclaim_request_tran.isvoid = 0 " & _
                       " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid " & _
                       " WHERE CONVERT(CHAR(8), cmclaim_request_master.transactiondate, 112) >= @startdate AND CONVERT(CHAR(8), cmclaim_request_master.transactiondate, 112) <= @enddate " & _
                       " AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.employeeunkid = @employeeunkid " & _
                       " AND cmclaim_request_master.expensetypeid = @expensetypeid " & _
                       " AND cmclaim_request_master.crmasterunkid <> @crmasterunkid " & _
                       " ORDER by ISNULL(cmclaim_request_master.employeeunkid, 0),ISNULL(cmexpense_master.name, '')"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate.Date).ToString())
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate.Date).ToString())
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim mintExpId As Integer = 0
            Dim mdecRequestedAmt As Decimal = 0
            Dim mdecApprovedAmt As Decimal = 0

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Dim objClaimMst As New clsclaim_request_master
                For Each dtRow As DataRow In dsList.Tables(0).Rows

                    If mintExpId <> CInt(dtRow("expenseunkid")) Then
                        mdecApprovedAmt = 0
                        mdecRequestedAmt = 0
                        mintExpId = CInt(dtRow("expenseunkid"))
                    End If

                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_PreviousDetails.Tables("ArutiTable").NewRow()
                    rpt_Row.Item("Column1") = dtRow("Expense").ToString()

                    If IsDBNull(dtRow("transactiondate")) = False Then
                        rpt_Row.Item("Column2") = CDate(dtRow("transactiondate")).ToShortDateString()
                    Else
                        rpt_Row.Item("Column2") = ""
                    End If

                    rpt_Row.Item("Column3") = Format(CDec(dtRow("RequestedAmt").ToString()), GUI.fmtCurrency)
                    mdecRequestedAmt += CDec(dtRow("RequestedAmt").ToString())
                    rpt_Row.Item("Column6") = Format(mdecRequestedAmt, GUI.fmtCurrency)

                    Dim dsApprovedAmt As DataSet = objClaimMst.GetFinalApproverApprovedDetails(mblnPaymentApprovalwithLeaveApproval, mintEmployeeId, mintExpenseCategoryID _
                                                                                                                                         , CInt(dtRow("crmasterunkid")), CInt(dtRow("expenseunkid")))

                    If dsApprovedAmt IsNot Nothing AndAlso dsApprovedAmt.Tables(0).Rows.Count > 0 Then
                        rpt_Row.Item("Column4") = Format(CDec(dsApprovedAmt.Tables(0).Rows(0)("Amount")), GUI.fmtCurrency)
                        mdecApprovedAmt += CDec(dsApprovedAmt.Tables(0).Rows(0)("Amount"))
                        rpt_Row.Item("Column7") = Format(mdecApprovedAmt, GUI.fmtCurrency)
                    End If

                    rpt_Row.Item("Column5") = CInt(dtRow("expenseunkid"))
                    rpt_PreviousDetails.Tables("ArutiTable").Rows.Add(rpt_Row)

                Next
                objClaimMst = Nothing
            End If

            '/* END PREVIOUS CLAIM APPLICATION DETAILS



            '/* START CLAIM APPLICATION DETAILS

            StrQ = " SELECT cmclaim_request_tran.expenseunkid,ISNULL(cmexpense_master.name,'') AS Expense " & _
                       ", ISNULL(cmclaim_request_tran.amount,0.00) AS Amount, ISNULL(cmclaim_request_tran.expense_remark,'') AS expense_remark " & _
                       ", ISNULL(cmexpense_master.leavetypeunkid,0) AS leavetypeunkid " & _
                       ", ISNULL(lvleavetype_master.leavename,'') AS leavename " & _
                       " FROM cmclaim_request_tran " & _
                       " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid " & _
                       " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = cmexpense_master.leavetypeunkid " & _
                       " WHERE cmclaim_request_tran.isvoid = 0 AND cmclaim_request_tran.crmasterunkid = @crmasterunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim mstrExpenseName As String = ""
            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Dim objClaimMst As New clsclaim_request_master
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_ExpenseDetails.Tables("ArutiTable").NewRow()
                    rpt_Row.Item("Column1") = mdtClaimDate.ToShortDateString()
                    rpt_Row.Item("Column2") = dtRow("Expense").ToString()
                    rpt_Row.Item("Column3") = Format(CDec(dtRow("Amount").ToString()), GUI.fmtCurrency)

                    Dim dsApprovedAmt As DataSet = objClaimMst.GetFinalApproverApprovedDetails(mblnPaymentApprovalwithLeaveApproval, mintEmployeeId, mintExpenseCategoryID _
                                                                                                                                         , mintClaimFormId, CInt(dtRow("expenseunkid")))

                    If dsApprovedAmt IsNot Nothing AndAlso dsApprovedAmt.Tables(0).Rows.Count > 0 Then
                        rpt_Row.Item("Column4") = Format(CDec(dsApprovedAmt.Tables(0).Rows(0)("Amount")), GUI.fmtCurrency)
                    End If

                    rpt_Row.Item("Column5") = dtRow("expense_remark").ToString()

                    If mintLeaveTypeId <= 0 Then
                        mintLeaveTypeId = CInt(dtRow("leavetypeunkid"))
                        mstrLeaveType = dtRow("leavename").ToString()
                    End If

                    rpt_ExpenseDetails.Tables("ArutiTable").Rows.Add(rpt_Row)

                    If dsList.Tables(0).Rows.Count = 1 Then
                        mstrExpenseName = dtRow("Expense").ToString()
                    End If

                Next
                objClaimMst = Nothing
            End If


            '/* END CLAIM APPLICATION DETAILS




            '/* START LEAVE BALANCE DETAILS

            If mintExpenseCategoryID = enExpenseType.EXP_LEAVE AndAlso mintLeaveTypeId > 0 Then

                Dim objBalance As New clsleavebalance_tran
                Dim dsBalanceList As DataSet = Nothing
                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    dsBalanceList = objBalance.GetLeaveBalanceInfo(mdtPeriodEndDate.Date, mintLeaveTypeId.ToString(), mintEmployeeId.ToString(), mintLeaveAccrueTenureSetting _
                                                                                           , mintLeaveAccrueDaysAfterEachMonth, mdtFinStartDate.Date, mdtFinEndDate.Date, False, False, mintLeaveBalanceSetting, 0, xYearUnkid)
                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    objBalance._DBStartdate = mdtFinStartDate.Date
                    objBalance._DBEnddate = mdtFinEndDate.Date
                    dsBalanceList = objBalance.GetLeaveBalanceInfo(mdtEmployeeAsonDate.Date, mintLeaveTypeId.ToString(), mintEmployeeId.ToString(), mintLeaveAccrueTenureSetting _
                                                                                        , mintLeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True, mintLeaveBalanceSetting, 0, xYearUnkid)
                End If


                If dsBalanceList IsNot Nothing And dsBalanceList.Tables(0).Rows.Count > 0 Then
                    Dim objLeaveForm As New clsleaveform
                    For Each dtRow As DataRow In dsBalanceList.Tables(0).Rows
                        Dim rpt_Row As DataRow
                        rpt_Row = rpt_LeaveDetails.Tables("ArutiTable").NewRow()
                        rpt_Row.Item("Column1") = mstrLeaveType.ToString()
                        rpt_Row.Item("Column2") = CDec(dsBalanceList.Tables(0).Rows(0)("LeaveBF")).ToString("#0.00")
                        rpt_Row.Item("Column3") = CDec(dsBalanceList.Tables(0).Rows(0)("Accrue_amount")).ToString("#0.00")
                        rpt_Row.Item("Column4") = CDec(CDec(dsBalanceList.Tables(0).Rows(0)("Issue_amount")) + CDec(dsBalanceList.Tables(0).Rows(0)("LeaveEncashment"))).ToString("#0.00")
                        rpt_Row.Item("Column5") = CDec(dsBalanceList.Tables(0).Rows(0)("LeaveAdjustment")).ToString("#0.00")
                        rpt_Row.Item("Column6") = CDec(CDec(dsBalanceList.Tables(0).Rows(0)("Accrue_amount")) + CDec(dsBalanceList.Tables(0).Rows(0)("LeaveAdjustment")) + CDec(dsBalanceList.Tables(0).Rows(0)("LeaveBF")) - (CDec(dsBalanceList.Tables(0).Rows(0)("Issue_amount")) + CDec(dsBalanceList.Tables(0).Rows(0)("LeaveEncashment")))).ToString("#0.00")

                        Dim dList As DataSet = objLeaveForm.GetEmployeeLastIssuedLeave(mintLeaveFormId, mintEmployeeId, mintLeaveTypeId)
                        If dList IsNot Nothing And dList.Tables(0).Rows.Count > 0 Then
                            rpt_Row.Item("Column7") = CDate(dList.Tables(0).Rows(0)("startdate")).ToShortDateString & "  " & Language.getMessage(mstrModuleName, 40, "To") & "  " & CDate(dList.Tables(0).Rows(0)("enddate")).ToShortDateString
                        End If
                        rpt_LeaveDetails.Tables("ArutiTable").Rows.Add(rpt_Row)
                    Next
                    objLeaveForm = Nothing
                End If

            End If


            '/* END LEAVE BALANCE DETAILS


            '/* START CLAIM APPROVER DETAILS

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                       "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                       "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                       "  FROM cmclaim_approval_tran "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= "    JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid "
            Else
                StrQ &= "    JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                             "     AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmexpapprover_master.expensetypeid = @expensetypeid   " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid "
            End If

            StrQ &= "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                         "    WHERE cmclaim_approval_tran.isvoid = 0  AND cmclaim_approval_tran.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " AND lvleaveapprover_master.isexternalapprover = 1 "
            Else
                StrQ &= " AND cmexpapprover_master.isexternalapprover = 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = " SELECT cmclaim_request_master.crmasterunkid  "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= ",   ISNULL(lvleaveapprover_master.approverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelname, '') AS LevelName " & _
                                       ",   ISNULL(lvapproverlevel_master.priority, 0) AS priority "
            Else
                StrInnerQry &= ",   ISNULL(cmexpapprover_master.crapproverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelname, '') AS LevelName " & _
                                       ",   ISNULL(cmapproverlevel_master.crpriority, 0) AS priority "
            End If

            'Pinkal (04-Jan-2019) -- Start
            'Bug - [0003308] Error when exporting claims form for one employee.
            '" ,ISNULL(hremployee_master.employeeunkid, 0) AS Employeeunkid " & _
            'Pinkal (04-Jan-2019) -- End

            StrInnerQry &= " ,#APPVR_NAME#  AS Approver " & _
                                  " ,cmclaim_approval_tran.approvaldate As ApprovalDate " & _
                                  " ,cmclaim_approval_tran.statusunkid " & _
                                  " ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
                                  "  END as Status " & _
                                  " ,ISNULL(cmclaim_approval_tran.expense_remark,'') As expense_remark " & _
                                  " FROM cmclaim_request_master " & _
                                  " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid AND lvleaveapprover_master.isvoid = 0 " & _
                                       " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
            Else
                StrInnerQry &= " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                      " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid "
            End If

            StrInnerQry &= " #COMM_JOIN# "

            StrConditionQry = " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.expensetypeid = @expensetypeid AND cmclaim_request_master.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrConditionQry &= "  AND lvleaveapprover_master.isexternalapprover = #ExApprId# "
            Else
                StrConditionQry &= "  AND cmexpapprover_master.isexternalapprover = #ExApprId#  "
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid ")
            Else
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid ")
            End If

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " ORDER BY lvapproverlevel_master.priority "
            Else
                StrQ &= " ORDER BY cmapproverlevel_master.crpriority "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid ")
                Else
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                                   " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If


                    End If

                Next
            End If

            Dim mintApproverID As Integer = 0
            Dim xCount As Integer = -1
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow

                If mintApproverID <> CInt(dtRow("approverunkid")) Then
                    rpt_Row = rpt_ClaimApprovers.Tables("ArutiTable").NewRow()
                    rpt_Row.Item("Column1") = dtRow.Item("LevelName").ToString()
                    rpt_Row.Item("Column2") = dtRow.Item("Approver").ToString()
                    If Not IsDBNull(dtRow.Item("ApprovalDate")) Then
                        rpt_Row.Item("Column3") = CDate(dtRow.Item("ApprovalDate")).ToShortDateString()
                    Else
                        rpt_Row.Item("Column3") = ""
                    End If
                    rpt_Row.Item("Column4") = dtRow.Item("Status").ToString()
                    rpt_Row.Item("Column5") = dtRow.Item("expense_remark").ToString()
                    mintApproverID = CInt(dtRow("approverunkid"))
                    rpt_ClaimApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
                    xCount += 1
                Else
                    If dtRow.Item("expense_remark").ToString().Trim.Length > 0 Then
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column5") &= IIf(rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column5").ToString().Trim().Length > 0, ",", "") & dtRow.Item("expense_remark").ToString()
                    End If
                End If

            Next

            '/* END CLAIM APPROVER DETAILS

            objRpt = New ArutiReport.Designer.rptClaimRequestForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)



            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            'Call ReportFunction.EnableSuppress(objRpt, "txtFilterDescription", True)

            Call ReportFunction.TextChange(objRpt, "txtApplicationNo", Language.getMessage(mstrModuleName, 1, "APPLICATION NUMBER"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 3, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 4, "Emp Code :"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 5, "Job Title :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 6, "Dept :"))
            Call ReportFunction.TextChange(objRpt, "txtAppDate", Language.getMessage(mstrModuleName, 7, "Appt. Date :"))

            If mblnShowEmpScale Then
                Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 8, "Basic Salary :"))
                Call ReportFunction.TextChange(objRpt, "txtSpecialAllowance", Language.getMessage(mstrModuleName, 9, "Special Allowance :"))
                Call ReportFunction.TextChange(objRpt, "txtTotalSalAllowance", Language.getMessage(mstrModuleName, 10, "Total (Basic Salary + Special Allowance) :"))
                ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection3", False)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection3", True)
            End If


            If mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                objRpt.Subreports("rptLeaveRecords").SetDataSource(rpt_LeaveDetails)
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtLeaveRecordTitle", Language.getMessage(mstrModuleName, 11, "LEAVE RECORDS"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtLeaveType", Language.getMessage(mstrModuleName, 12, "Leave Type :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtLeavebf", Language.getMessage(mstrModuleName, 13, "Leave B/F :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtAccruedDate", Language.getMessage(mstrModuleName, 14, "Accrued ToDate :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtTotalIssuedDate", Language.getMessage(mstrModuleName, 15, "Total Issued ToDate :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtTotalAdjustment", Language.getMessage(mstrModuleName, 16, "Total Adjustment :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtLeaveBalanceAsOnDate", Language.getMessage(mstrModuleName, 17, "Leave Balance As on Date :"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptLeaveRecords"), "txtLastLeaveDates", Language.getMessage(mstrModuleName, 18, "Last Leave Dates :"))
            Else
                ReportFunction.EnableSuppressSection(objRpt.Subreports("rptLeaveRecords"), "ReportHeaderSection2", True)
            End If

            objRpt.Subreports("rptPreviousAdvanceRecords").SetDataSource(rpt_PreviousDetails)
            Call ReportFunction.TextChange(objRpt.Subreports("rptPreviousAdvanceRecords"), "txtPreviousRecords", Language.getMessage(mstrModuleName, 19, "PREVIOUS RECORDS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPreviousAdvanceRecords"), "txtClaimName", Language.getMessage(mstrModuleName, 20, "CLAIM NAME"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPreviousAdvanceRecords"), "txtApplicationDate", Language.getMessage(mstrModuleName, 21, "DATE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPreviousAdvanceRecords"), "txtRequestedAmount", Language.getMessage(mstrModuleName, 22, "REQUESTED AMOUNT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPreviousAdvanceRecords"), "txtApprovedAmount", Language.getMessage(mstrModuleName, 23, "APPROVED AMOUNT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPreviousAdvanceRecords"), "txtPreviousTotal", Language.getMessage(mstrModuleName, 24, "TOTAL AMOUNT"))



            objRpt.Subreports("rptAdvanceApplicationDetails").SetDataSource(rpt_ExpenseDetails)
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimDetails", Language.getMessage(mstrModuleName, 25, "APPLICATION DETAILS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtExpenseDate", Language.getMessage(mstrModuleName, 21, "DATE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtExpenseName", Language.getMessage(mstrModuleName, 20, "CLAIM NAME"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtRequestedAmount", Language.getMessage(mstrModuleName, 22, "REQUESTED AMOUNT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtApprovedAmt", Language.getMessage(mstrModuleName, 23, "APPROVED AMOUNT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimRemark", Language.getMessage(mstrModuleName, 26, "CLAIM REMARK"))



            objRpt.Subreports("rptAdvanceApprovals").SetDataSource(rpt_ClaimApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtAdvanceApproversDetails", Language.getMessage(mstrModuleName, 27, "APPROVALS DETAILS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtLevelName", Language.getMessage(mstrModuleName, 28, "LEVEL NAME"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApproverName", Language.getMessage(mstrModuleName, 29, "APPROVER NAME"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovalDate", Language.getMessage(mstrModuleName, 30, "APPROVAL DATE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovalStatus", Language.getMessage(mstrModuleName, 31, "APPROVAL STATUS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtRemarks", Language.getMessage(mstrModuleName, 32, "REMARKS/COMMENTS"))

            If mstrExpenseName.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtApplicationFor", Language.getMessage(mstrModuleName, 33, "APPLICATION FOR") & " " & mstrExpenseName)
            Else
                Call ReportFunction.TextChange(objRpt, "txtApplicationFor", Language.getMessage(mstrModuleName, 34, "Employee ") & " " & mstrExpenseCategory & " " & Language.getMessage(mstrModuleName, 35, "Claims"))
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 36, "Prepared By :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblPreparedBy", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtPreparedBy", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 37, "Received By :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblReceivedBy", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtReceivedBy", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = False AndAlso ConfigParameter._Object._IsShowReceivedBy = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (05-May-2020) -- Start
    'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
    Private Function GenerateEmployee_ClaimForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimDetail As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimApprovers As ArutiReport.Designer.dsArutiReport
        Dim mdtRequestDate As DateTime = Nothing
        Dim mstrClaimRemark As String = ""
        Try

            objDataOperation = New clsDataOperation


            StrQ = " SELECT  cmclaim_request_master.employeeunkid " & _
                      ",cmclaim_request_master.crmasterunkid " & _
                      ",employeecode " & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                      ",cmclaim_request_master.claimrequestno " & _
                      ",cmclaim_request_master.transactiondate " & _
                      ",cmclaim_request_master.claim_remark " & _
                      ",hrjob_master.job_name AS jobtitle " & _
                      ",hrgrade_master.name AS grade " & _
                      ",hrdepartment_master.name AS department " & _
                      ",hrsectiongroup_master.name AS sectiongrp " & _
                      ",hrclassgroup_master.name AS classgrp " & _
                      ",hrclasses_master.name AS class " & _
                      " ,hremployee_master.appointeddate " & _
                      ",hremployee_master.confirmation_date " & _
                      " FROM cmclaim_request_master " & _
                      " JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                      " JOIN " & _
                        " ( " & _
                        "         SELECT " & _
                        "         jobunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                        " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                       " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                       " JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "        gradeunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                       "    FROM prsalaryincrement_tran " & _
                       "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                       ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                       " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                       " JOIN " & _
                               " ( " & _
                               "    SELECT " & _
                               "         departmentunkid " & _
                               "        ,sectiongroupunkid " & _
                               "        ,classgroupunkid " & _
                               "        ,classunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                               "    FROM hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                               " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                      " LEFT JOIN hrsectiongroup_master ON  hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                      " LEFT JOIN hrclassgroup_master ON  hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid " & _
                      " LEFT JOIN hrclasses_master ON  hrclasses_master.classesunkid = Alloc.classunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         date1 " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_dates_tran " & _
                      "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                      " WHERE cmclaim_request_master.isvoid = 0 "


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimDetail = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimApprovers = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow()

                rpt_Rows.Item("Column1") = dtRow.Item("claimrequestno")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("department")
                rpt_Rows.Item("Column10") = dtRow.Item("sectiongrp")
                rpt_Rows.Item("Column11") = dtRow.Item("classgrp")
                rpt_Rows.Item("Column12") = dtRow.Item("class")
                rpt_Rows.Item("Column13") = dtRow.Item("grade")

                mdtRequestDate = CDate(dtRow.Item("transactiondate"))
                mstrClaimRemark = dtRow.Item("claim_remark").ToString()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            '/* START CLAIM APPLICATION DETAILS

            Dim dsClaimDetails As DataSet = Nothing

            StrQ = "SELECT cmclaim_request_tran.crtranunkid " & _
                      ",cmclaim_request_tran.crmasterunkid " & _
                      ",cmclaim_request_tran.expenseunkid " & _
                      ",cmclaim_request_tran.secrouteunkid " & _
                      ",ISNULL(cmexpense_master.name,'') AS Expense " & _
                      ",ISNULL(cfcommon_master.name,'') AS Sector " & _
                      ",ISNULL(cmclaim_request_tran.quantity,0.00) AS quantity " & _
                      ",ISNULL(cmclaim_request_tran.unitprice,0.00) AS unitprice " & _
                      ",ISNULL(cmclaim_request_tran.amount,0.00) AS amount " & _
                      ",ISNULL(cmclaim_request_tran.expense_remark,'') AS Expense_Remark " & _
                      ",cfexchange_rate.currency_sign As currency_sign " & _
                      " FROM cmclaim_request_tran " & _
                      " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_request_tran.expenseunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_request_tran.secrouteunkid and cfcommon_master.mastertype= @mastertype " & _
                      " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_request_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_request_tran.countryunkid " & _
                      " WHERE cmclaim_request_tran.isvoid = 0 and cmclaim_request_tran.crmasterunkid = @crmasterunkid "


            'Pinkal (08-Apr-2021)--   'Enhancement  -  Working on Employee Claim Form Report.[",cfexchange_rate.currency_sign As currency_sign " & _" LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_request_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_request_tran.countryunkid " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, clsCommon_Master.enCommonMaster.SECTOR_ROUTE)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            dsClaimDetails = objDataOperation.ExecQuery(StrQ, "Detail")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsClaimDetails IsNot Nothing And dsClaimDetails.Tables(0).Rows.Count > 0 Then

                For Each dtRow As DataRow In dsClaimDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_ClaimDetail.Tables("ArutiTable").NewRow()

                    rpt_Row.Item("Column1") = mdtRequestDate.ToShortDateString()
                    rpt_Row.Item("Column2") = mstrClaimRemark.Trim()
                    rpt_Row.Item("Column3") = dtRow.Item("Expense")
                    rpt_Row.Item("Column4") = dtRow.Item("Sector")
                    rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("quantity")), GUI.fmtCurrency)
                    rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("unitprice")), GUI.fmtCurrency)

                    'Pinkal (08-Apr-2021)-- Start
                    'Enhancement  -  Working on Employee Claim Form Report.
                    'rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    rpt_Row.Item("Column7") = dtRow.Item("currency_sign").ToString() & "  " & Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                    'Pinkal (08-Apr-2021) -- End


                    rpt_Row.Item("Column8") = dtRow.Item("Expense_Remark").ToString()

                    'Pinkal (08-Apr-2021)-- Start
                    'Enhancement  -  Working on Employee Claim Form Report.
                    'rpt_Row.Item("Column9") = Format(CDec(IIf(IsDBNull(dsClaimDetails.Tables(0).Compute("SUM(amount)", "crmasterunkid = " & mintClaimFormId)), 0, dsClaimDetails.Tables(0).Compute("SUM(amount)", "crmasterunkid = " & mintClaimFormId))), GUI.fmtCurrency)
                    rpt_Row.Item("Column9") = dtRow.Item("currency_sign").ToString() & "  " & Format(CDec(IIf(IsDBNull(dsClaimDetails.Tables(0).Compute("SUM(amount)", "crmasterunkid = " & mintClaimFormId)), 0, dsClaimDetails.Tables(0).Compute("SUM(amount)", "crmasterunkid = " & mintClaimFormId))), GUI.fmtCurrency)
                    'Pinkal (08-Apr-2021) -- End


                    rpt_Row.Item("Column10") = mstrExpenseCategory
                    rpt_ClaimDetail.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

            End If

            '/* END CLAIM APPLICATION DETAILS

            '/* START CLAIM APPROVER DETAILS

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                       "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                       "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                       "  FROM cmclaim_approval_tran "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= "    JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid "
            Else
                StrQ &= "    JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                             "     AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmexpapprover_master.expensetypeid = @expensetypeid   " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid "
            End If

            StrQ &= "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                         "    WHERE cmclaim_approval_tran.isvoid = 0  AND cmclaim_approval_tran.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " AND lvleaveapprover_master.isexternalapprover = 1 "
            Else
                StrQ &= " AND cmexpapprover_master.isexternalapprover = 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = " SELECT cmclaim_request_master.crmasterunkid  "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= ",   ISNULL(lvleaveapprover_master.approverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelname, '') AS LevelName " & _
                                       ",   ISNULL(lvapproverlevel_master.priority, 0) AS priority "
            Else
                StrInnerQry &= ",   ISNULL(cmexpapprover_master.crapproverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelname, '') AS LevelName " & _
                                       ",   ISNULL(cmapproverlevel_master.crpriority, 0) AS priority "
            End If

            StrInnerQry &= " ,#APPVR_NAME#  AS Approver " & _
                                  " ,cmclaim_approval_tran.approvaldate As ApprovalDate " & _
                                  " ,cmclaim_approval_tran.statusunkid " & _
                                  " ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
                                  "  END as Status " & _
                                  " ,ISNULL(cmclaim_approval_tran.amount,0.00) As amount " & _
                                  " ,ISNULL(cmclaim_approval_tran.expense_remark,'') As expense_remark " & _
                                  ",cfexchange_rate.currency_sign As currency_sign " & _
                                  " FROM cmclaim_request_master " & _
                                  " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                                  " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "


            'Pinkal (08-Apr-2021)-- Enhancement  -  Working on Employee Claim Form Report.[",cfexchange_rate.currency_sign As currency_sign " & _ LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "]

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid AND lvleaveapprover_master.isvoid = 0 " & _
                                       " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
            Else
                StrInnerQry &= " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                      " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid "
            End If

            StrInnerQry &= " #COMM_JOIN# "

            StrConditionQry = " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.expensetypeid = @expensetypeid AND cmclaim_request_master.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrConditionQry &= "  AND lvleaveapprover_master.isexternalapprover = #ExApprId# "
            Else
                StrConditionQry &= "  AND cmexpapprover_master.isexternalapprover = #ExApprId#  "
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid ")
            Else
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid ")
            End If

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " ORDER BY lvapproverlevel_master.priority "
            Else
                StrQ &= " ORDER BY cmapproverlevel_master.crpriority "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid ")
                Else
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                                   " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If


                    End If

                Next
            End If

            Dim mintApproverID As Integer = 0
            Dim xCount As Integer = -1
            Dim mdecAmount As Decimal = 0
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow

                If mintApproverID <> CInt(dtRow("approverunkid")) Then
                    rpt_Row = rpt_ClaimApprovers.Tables("ArutiTable").NewRow()
                    rpt_Row.Item("Column1") = dtRow.Item("LevelName").ToString()
                    rpt_Row.Item("Column2") = dtRow.Item("Approver").ToString()
                    If Not IsDBNull(dtRow.Item("ApprovalDate")) Then
                        rpt_Row.Item("Column3") = CDate(dtRow.Item("ApprovalDate")).ToShortDateString()
                    Else
                        rpt_Row.Item("Column3") = ""
                    End If
                    mdecAmount = CDec(dtRow.Item("amount"))

                    'Pinkal (08-Apr-2021)-- Start
                    'Enhancement  -  Working on Employee Claim Form Report.
                    'rpt_Row.Item("Column4") =  Format(mdecAmount, GUI.fmtCurrency)
                    rpt_Row.Item("Column4") = dtRow.Item("currency_sign").ToString() & "  " & Format(mdecAmount, GUI.fmtCurrency)
                    'Pinkal (08-Apr-2021) -- End


                    rpt_Row.Item("Column5") = dtRow.Item("Status").ToString()
                    rpt_Row.Item("Column6") = dtRow.Item("expense_remark").ToString()
                    mintApproverID = CInt(dtRow("approverunkid"))
                    rpt_ClaimApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
                    xCount += 1
                Else
                    If CDec(dtRow.Item("amount")) > 0 Then
                        mdecAmount += CDec(dtRow.Item("amount"))

                        'Pinkal (08-Apr-2021)-- Start
                        'Enhancement  -  Working on Employee Claim Form Report.
                        'rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column4") = Format(mdecAmount, GUI.fmtCurrency)
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column4") = dtRow.Item("currency_sign").ToString() & "  " & Format(mdecAmount, GUI.fmtCurrency)
                        'Pinkal (08-Apr-2021) -- End


                    End If

                    If dtRow.Item("expense_remark").ToString().Trim.Length > 0 Then
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column6") &= IIf(rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column5").ToString().Trim().Length > 0, ",", "") & dtRow.Item("expense_remark").ToString()
                    End If
                End If

            Next

            '/* END CLAIM APPROVER DETAILS


            objRpt = New ArutiReport.Designer.rptEmployeeClaimForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)



            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            'Call ReportFunction.EnableSuppress(objRpt, "txtFilterDescription", True)

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            Call ReportFunction.TextChange(objRpt, "txtApplicationNo", info1.ToTitleCase(Language.getMessage(mstrModuleName, 1, "APPLICATION NUMBER").ToLower()) & " : ")
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 3, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 4, "Emp Code :"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 5, "Job Title :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 6, "Dept :"))
            Call ReportFunction.TextChange(objRpt, "txtSectionGroup", Language.getMessage(mstrModuleName, 41, "Section Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClassGroup", Language.getMessage(mstrModuleName, 42, "Class Group :"))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 43, "Class :"))
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 44, "Grade :"))


            objRpt.Subreports("rptAdvanceApplicationDetails").SetDataSource(rpt_ClaimDetail)
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimApplicationDetails", Language.getMessage(mstrModuleName, 45, "CLAIM APPLICATION DETAILS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtRequestDate", Language.getMessage(mstrModuleName, 46, "REQUEST DATE :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimDescription", Language.getMessage(mstrModuleName, 47, "CLAIM DESCRIPTION"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtSectorRoute", Language.getMessage(mstrModuleName, 48, "SECTOR/ROUTE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtQuantity", Language.getMessage(mstrModuleName, 49, "QUANTITY"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtUnitPrice", Language.getMessage(mstrModuleName, 50, "UNIT PRICE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtRequestedAmt", Language.getMessage(mstrModuleName, 51, "REQUESTED AMT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtExpenseRemark", Language.getMessage(mstrModuleName, 60, "EXPENSE REMARK"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtClaimRemark", Language.getMessage(mstrModuleName, 26, "CLAIM REMARK") & " :")
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtGrandTotal", Language.getMessage(mstrModuleName, 58, "GRAND TOTAL"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApplicationDetails"), "txtExpenseCategory", Language.getMessage(mstrModuleName, 61, "EXPENSE CATEGORY :"))



            objRpt.Subreports("rptAdvanceApprovals").SetDataSource(rpt_ClaimApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovalDetails", Language.getMessage(mstrModuleName, 52, "APPROVAL DETAILS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApproverLevel", Language.getMessage(mstrModuleName, 53, "APPROVAL LEVEL"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprover", Language.getMessage(mstrModuleName, 54, "APPROVER"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovedAmount", Language.getMessage(mstrModuleName, 55, "APPROVED AMOUNT"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApprovalDate", Language.getMessage(mstrModuleName, 30, "APPROVAL DATE"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtStatus", Language.getMessage(mstrModuleName, 56, "STATUS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtApproverRemarks", Language.getMessage(mstrModuleName, 57, "APPROVER REMARKS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptAdvanceApprovals"), "txtTotalApprovedAmount", Language.getMessage(mstrModuleName, 59, "TOTAL APPROVED AMOUNT"))


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 36, "Prepared By :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblPreparedBy", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtPreparedBy", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 37, "Received By :"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "lblReceivedBy", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtReceivedBy", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = False AndAlso ConfigParameter._Object._IsShowReceivedBy = False Then
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Pinkal (05-May-2020) -- End


    'Pinkal (30-May-2020) -- Start
    'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.

    Private Function Generate_FuelApplicationForm(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimDetail As ArutiReport.Designer.dsArutiReport
        Dim rpt_ClaimApprovers As ArutiReport.Designer.dsArutiReport
        Dim mdtRequestDate As DateTime = Nothing
        Dim mstrApplicationStatus As String = ""
        Dim mstrClaimRemark As String = ""

        'Pinkal (19-Jul-2021)-- Start
        'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
        Dim mintStatusId As Integer = 2
        Dim mblnIsPrinted As Boolean = False
        'Pinkal (19-Jul-2021)-- End

        Try

            objDataOperation = New clsDataOperation


            StrQ = " SELECT  cmclaim_request_master.employeeunkid " & _
                      ",cmclaim_request_master.crmasterunkid " & _
                      ",employeecode " & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employeename " & _
                      ",cmclaim_request_master.claimrequestno " & _
                      ",cmclaim_request_master.transactiondate " & _
                      ",cmclaim_request_master.claim_remark " & _
                      ",hrjob_master.job_name AS jobtitle " & _
                      ",hrgrade_master.name AS grade " & _
                      ",hrdepartment_master.name AS department " & _
                      " ,CASE WHEN cmclaim_request_master.statusunkid = 1 then @Approve " & _
                      "            WHEN cmclaim_request_master.statusunkid = 2 then @Pending " & _
                      "            WHEN cmclaim_request_master.statusunkid = 3 then @Reject " & _
                      "            WHEN cmclaim_request_master.statusunkid = 6 then @Cancel " & _
                      "  END as Status " & _
                      ", ISNULL(App.ApprovedAmt,0.00) AS ApprovedAmt " & _
                      ", cmclaim_request_master.statusunkid " & _
                      ", cmclaim_request_master.isprinted " & _
                      " FROM cmclaim_request_master " & _
                      " JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                      " JOIN " & _
                      " ( " & _
                      "         SELECT " & _
                      "         jobunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                      " JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "        gradeunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "    FROM prsalaryincrement_tran " & _
                      "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                      " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                      " JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         departmentunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "'" & _
                      " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                      " LEFT JOIN ( " & _
                      "             SELECT " & _
                      "                     crmasterunkid " & _
                      "                     ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                      "             FROM cmclaim_approval_tran " & _
                      "             WHERE isvoid = 0 And statusunkid = 1 AND " & _
                      "             crapproverunkid IN ( " & _
                      "                                             SELECT crapproverunkid FROM  " & _
                      "                                             ( " & _
                      "                                                 SELECT " & _
                      "                                                         ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                      "                                                        ,cm.crmasterunkid " & _
                      "                                                        ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                      "                                                 FROM cmclaim_approval_tran cm " & _
                      "                                                 JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                      "                                                 JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                      "                                                 WHERE cm.isvoid = 0 AND cm.crmasterunkid = @crmasterunkid " & _
                      "                                             ) as cnt where cnt.rno= 1 " & _
                      "                                   ) " & _
                      "	GROUP BY crmasterunkid" & _
                      " ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                      " WHERE cmclaim_request_master.isvoid = 0 "


            'Pinkal (19-Jul-2021)-- Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.[cmclaim_request_master.statusunkid, cmclaim_request_master.isprinted]

            'Pinkal (19-Nov-2020) -- Enhancement KADCO Employee Claim Form Report [ ", ISNULL(App.ApprovedAmt,0.00) AS ApprovedAmt " & _]

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimDetail = New ArutiReport.Designer.dsArutiReport
            rpt_ClaimApprovers = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow()

                rpt_Rows.Item("Column1") = dtRow.Item("claimrequestno")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column4") = dtRow.Item("jobtitle")
                rpt_Rows.Item("Column5") = dtRow.Item("department")

                'Pinkal (19-Nov-2020) -- START
                'Enhancement KADCO Employee Claim Form Report 
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("ApprovedAmt")), GUI.fmtCurrency)
                'Pinkal (19-Nov-2020) -- End


                mdtRequestDate = CDate(dtRow.Item("transactiondate"))
                mstrClaimRemark = dtRow.Item("claim_remark").ToString()
                mstrApplicationStatus = dtRow.Item("Status").ToString()

                'Pinkal (19-Jul-2021)-- Start
                'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
                mintStatusId = CInt(dtRow.Item("statusunkid").ToString())
                mblnIsPrinted = CBool(dtRow.Item("isprinted").ToString())
                'Pinkal (19-Jul-2021)-- End
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            '/* START CLAIM APPLICATION DETAILS

            Dim dsClaimDetails As DataSet = Nothing
            Dim mstrVehicleRegNo As String = ""


            StrQ = "SELECT cmclaim_request_tran.crtranunkid " & _
                      ",cmclaim_request_tran.crmasterunkid " & _
                      ",cmclaim_request_tran.expenseunkid " & _
                      ",cmclaim_request_tran.secrouteunkid " & _
                      ",ISNULL(cmexpense_master.name,'') AS Expense " & _
                      ",ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
                      ",CASE WHEN ISNULL(cmexpense_master.isaccrue,0) = 0 THEN 0.00 ELSE ISNULL(cmexpbalance_tran.accrue_amount,0.00) END AnnualBalance " & _
                      ",ISNULL(cmclaim_request_tran.amount,0.00) AS Requestedamt " & _
                      ",ISNULL(cmexpbalance_tran.remaining_bal,0.00) AS Remaining_bal " & _
                      ",ISNULL(App.ApprovedAmt,0.00) AS ApprovedAmt " & _
                      ",ISNULL(cmclaim_request_tran.expense_remark,'') AS VehicleregNo " & _
                      " FROM cmclaim_request_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                      " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_request_tran.expenseunkid " & _
                      " JOIN cmexpbalance_tran ON cmexpense_master.expenseunkid  = cmexpbalance_tran.expenseunkid AND cmclaim_request_master.employeeunkid = cmexpbalance_tran.employeeunkid AND cmexpbalance_tran.isvoid = 0 " & _
                      " LEFT JOIN ( " & _
                      "             SELECT " & _
                      "                     crmasterunkid " & _
                      "                     ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                      "             FROM cmclaim_approval_tran " & _
                      "             WHERE isvoid = 0 And statusunkid = 1 AND " & _
                      "             crapproverunkid IN ( " & _
                      "                                             SELECT crapproverunkid FROM  " & _
                      "                                             ( " & _
                      "                                                 SELECT " & _
                      "                                                         ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                      "                                                        ,cm.crmasterunkid " & _
                      "                                                        ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                      "                                                 FROM cmclaim_approval_tran cm " & _
                      "                                                 JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                      "                                                 JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                      "                                                 WHERE cm.isvoid = 0 AND cm.crmasterunkid = @crmasterunkid " & _
                      "                                             ) as cnt where cnt.rno= 1 " & _
                      "                                   ) " & _
                      "	GROUP BY crmasterunkid" & _
                      " ) AS App ON App.crmasterunkid = cmclaim_request_tran.crmasterunkid " & _
                      " WHERE cmclaim_request_tran.isvoid = 0 AND cmclaim_request_tran.crmasterunkid = @crmasterunkid "

            'Pinkal (19-Nov-2020) -- Enhancement KADCO Employee Claim Form Report [AND cm.crmasterunkid = @crmasterunkid]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            dsClaimDetails = objDataOperation.ExecQuery(StrQ, "Detail")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsClaimDetails IsNot Nothing And dsClaimDetails.Tables(0).Rows.Count > 0 Then
                Dim objClaim As New clsclaim_request_master
                For Each dtRow As DataRow In dsClaimDetails.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_ClaimDetail.Tables("ArutiTable").NewRow()

                    rpt_Row.Item("Column1") = mdtRequestDate.ToShortDateString()
                    rpt_Row.Item("Column2") = dtRow.Item("VehicleregNo").ToString()

                    If CBool(dtRow.Item("isaccrue")) Then
                        rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("AnnualBalance")), GUI.fmtCurrency) & " LTS"
                        rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("Remaining_bal")), GUI.fmtCurrency) & " LTS"
                    Else
                        rpt_Row.Item("Column3") = "NA"
                        rpt_Row.Item("Column5") = "NA"
                    End If

                    rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("Requestedamt")), GUI.fmtCurrency) & " LTS"
                    rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("ApprovedAmt")), GUI.fmtCurrency) & " LTS"
                    rpt_Row.Item("Column7") = mstrApplicationStatus
                    rpt_Row.Item("Column8") = mstrClaimRemark
                    rpt_Row.Item("Column9") = Format(CDec(objClaim.GetApprovedAmountAsOnDate(mblnPaymentApprovalwithLeaveApproval, mdtRequestDate.Date, mintEmployeeId, mintExpenseCategoryID, CInt(dtRow.Item("expenseunkid")))), GUI.fmtCurrency) & " LTS"
                    mstrVehicleRegNo = dtRow.Item("VehicleregNo").ToString()
                    rpt_ClaimDetail.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

            End If

            '/* END CLAIM APPLICATION DETAILS

            '/* START CLAIM APPROVER DETAILS

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                       "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                       "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                       "  FROM cmclaim_approval_tran "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= "    JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid "
            Else
                StrQ &= "    JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                             "     AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmexpapprover_master.expensetypeid = @expensetypeid   " & _
                             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid "
            End If

            StrQ &= "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                         "    WHERE cmclaim_approval_tran.isvoid = 0  AND cmclaim_approval_tran.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " AND lvleaveapprover_master.isexternalapprover = 1 "
            Else
                StrQ &= " AND cmexpapprover_master.isexternalapprover = 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)
            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrInnerQry = " SELECT cmclaim_request_master.crmasterunkid  "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= ",   ISNULL(lvleaveapprover_master.approverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(lvapproverlevel_master.levelname, '') AS LevelName " & _
                                       ",   ISNULL(lvapproverlevel_master.priority, 0) AS priority "
            Else
                StrInnerQry &= ",   ISNULL(cmexpapprover_master.crapproverunkid, 0) AS approverunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelunkid, 0) AS levelunkid " & _
                                       ",   ISNULL(cmapproverlevel_master.crlevelname, '') AS LevelName " & _
                                       ",   ISNULL(cmapproverlevel_master.crpriority, 0) AS priority "
            End If

            StrInnerQry &= " ,#APPVR_NAME#  AS Approver " & _
                                  " ,cmclaim_approval_tran.approvaldate As ApprovalDate " & _
                                  " ,cmclaim_approval_tran.statusunkid " & _
                                  " ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject " & _
                                  "            WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
                                  "  END as Status " & _
                                  " ,ISNULL(cmclaim_approval_tran.amount,0.00) As amount " & _
                                  " ,ISNULL(cmclaim_approval_tran.expense_remark,'') As expense_remark " & _
                                  " ,ISNULL(cmclaim_request_tran.expense_remark,'') As Reqexp_remark " & _
                                  " FROM cmclaim_request_master " & _
                                  " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                  " LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_tran.isvoid = 0 "

            'Pinkal (27-Jun-2020) -- 'Enhancement NMB -   Working on Employee Signature Report.[LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_tran.isvoid = 0]

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrInnerQry &= " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid AND lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid AND lvleaveapprover_master.isvoid = 0 " & _
                                       " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
            Else
                StrInnerQry &= " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                      " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid "
            End If

            StrInnerQry &= " #COMM_JOIN# "

            StrConditionQry = " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.expensetypeid = @expensetypeid AND cmclaim_request_master.crmasterunkid = @crmasterunkid "

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrConditionQry &= "  AND lvleaveapprover_master.isexternalapprover = #ExApprId# "
            Else
                StrConditionQry &= "  AND cmexpapprover_master.isexternalapprover = #ExApprId#  "
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lvleaveapprover_master.leaveapproverunkid = hremployee_master.employeeunkid ")
            Else
                StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid ")
            End If

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")

            If mblnPaymentApprovalwithLeaveApproval AndAlso mintExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                StrQ &= " ORDER BY lvapproverlevel_master.priority "
            Else
                StrQ &= " ORDER BY cmapproverlevel_master.crpriority "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseCategoryID)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimFormId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid ")
                Else
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                                   " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
Recalculate:
                Dim mintPrioriry As Integer = 0
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If mintPrioriry <> CInt(dsList.Tables(0).Rows(i)("priority")) Then
                        mintPrioriry = CInt(dsList.Tables(0).Rows(i)("priority"))
                        Dim drRow() As DataRow = dsList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                        If drRow.Length > 0 Then
                            Dim drPending() As DataRow = dsList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                            If drPending.Length > 0 Then
                                For Each dRow As DataRow In drPending
                                    dsList.Tables(0).Rows.Remove(dRow)
                                    GoTo Recalculate
                                Next
                                dsList.AcceptChanges()
                            End If
                        End If


                    End If

                Next
            End If

            Dim mintApproverID As Integer = 0
            Dim xCount As Integer = -1
            Dim mdecAmount As Decimal = 0
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow

                If mintApproverID <> CInt(dtRow("approverunkid")) Then
                    rpt_Row = rpt_ClaimApprovers.Tables("ArutiTable").NewRow()
                    rpt_Row.Item("Column1") = dtRow.Item("LevelName").ToString()
                    rpt_Row.Item("Column2") = dtRow.Item("Approver").ToString()
                    If Not IsDBNull(dtRow.Item("ApprovalDate")) Then
                        rpt_Row.Item("Column3") = CDate(dtRow.Item("ApprovalDate")).ToShortDateString()
                    Else
                        rpt_Row.Item("Column3") = ""
                    End If
                    mdecAmount = CDec(dtRow.Item("amount"))
                    rpt_Row.Item("Column4") = Format(mdecAmount, GUI.fmtCurrency)
                    rpt_Row.Item("Column5") = dtRow.Item("Status").ToString()

                    'Pinkal (27-Jun-2020) -- Start
                    'Enhancement NMB -   Working on Employee Signature Report.
                    If dtRow.Item("Reqexp_remark").ToString().Trim <> dtRow.Item("expense_remark").ToString().Trim Then
                        rpt_Row.Item("Column6") = dtRow.Item("expense_remark").ToString()
                    Else
                        rpt_Row.Item("Column6") = ""
                    End If
                    'Pinkal (27-Jun-2020) -- End


                    mintApproverID = CInt(dtRow("approverunkid"))
                    rpt_ClaimApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
                    xCount += 1
                Else
                    If CDec(dtRow.Item("amount")) > 0 Then
                        mdecAmount += CDec(dtRow.Item("amount"))
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column4") = Format(mdecAmount, GUI.fmtCurrency)
                    End If

                    'Pinkal (27-Jun-2020) -- Start
                    'Enhancement NMB -   Working on Employee Signature Report.
                    If dtRow.Item("Reqexp_remark").ToString().Trim <> dtRow.Item("expense_remark").ToString().Trim.Length > 0 Then
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column6") &= IIf(rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column5").ToString().Trim().Length > 0, ",", "") & dtRow.Item("expense_remark").ToString()
                    Else
                        rpt_ClaimApprovers.Tables("ArutiTable").Rows(xCount)("Column6") &= ""
                    End If
                    'Pinkal (27-Jun-2020) -- End
                End If

            Next

            '/* END CLAIM APPROVER DETAILS


            objRpt = New ArutiReport.Designer.rptFuelApplicationForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)



            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            Call ReportFunction.TextChange(objRpt, "txtApplicationNo", info1.ToTitleCase(Language.getMessage(mstrModuleName, 1, "APPLICATION NUMBER").ToLower()) & " : ")
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", info1.ToTitleCase(Language.getMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS").ToLower()))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 3, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 4, "Emp Code :"))
            Call ReportFunction.TextChange(objRpt, "txtDesignation", Language.getMessage(mstrModuleName, 5, "Job Title :"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 6, "Dept :"))


            objRpt.Subreports("rptFuelApplicationDetails").SetDataSource(rpt_ClaimDetail)
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtFuelApplicationDetails", Language.getMessage(mstrModuleName, 62, "Fuel Application Details"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtApplicationDate", Language.getMessage(mstrModuleName, 63, "Application Date :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtVehicleRegNo", Language.getMessage(mstrModuleName, 64, "Vehicle Reg. No :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtAnnualBalPetrol", Language.getMessage(mstrModuleName, 65, "Annual fuel balance of gasoline/petrol :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtRequestedAmtFuel", Language.getMessage(mstrModuleName, 66, "Amount of fuel requested :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtRemainingFuelBalance", Language.getMessage(mstrModuleName, 67, "Remaining fuel balance :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtApprovedAmount", Language.getMessage(mstrModuleName, 68, "Approved amount :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtApplicationStatus", Language.getMessage(mstrModuleName, 69, "Application Status :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtPurpose", Language.getMessage(mstrModuleName, 70, "Purpose :"))

            'Pinkal (19-Jul-2021)-- Start
            'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
            'Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtCumulativeBal", info1.ToTitleCase(Language.getMessage(mstrModuleName, 85, "Cumulative Fuel Balance :").ToLower()))
            Call ReportFunction.TextChange(objRpt.Subreports("rptFuelApplicationDetails"), "txtCumulativeBal", info1.ToTitleCase(Language.getMessage(mstrModuleName, 86, "Cumulative Fuel Balance :").ToLower()))
            'Pinkal (19-Jul-2021)-- End

            objRpt.Subreports("rptApprovals").SetDataSource(rpt_ClaimApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApprovalDetails", info1.ToTitleCase(Language.getMessage(mstrModuleName, 52, "APPROVAL DETAILS").ToLower()))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApproverLevel", Language.getMessage(mstrModuleName, 71, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApprover", Language.getMessage(mstrModuleName, 72, "Approver Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApprovalDate", info1.ToTitleCase(Language.getMessage(mstrModuleName, 30, "APPROVAL DATE").ToLower()))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApprovedAmount", Language.getMessage(mstrModuleName, 73, "Approved Fuel Amt."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtStatus", info1.ToTitleCase(Language.getMessage(mstrModuleName, 56, "STATUS").ToLower()))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApprovals"), "txtApproverRemarks", info1.ToTitleCase(Language.getMessage(mstrModuleName, 57, "APPROVER REMARKS").ToLower()))


            Call ReportFunction.TextChange(objRpt, "txtToFuelSupplier", Language.getMessage(mstrModuleName, 74, "To : Fuel Supplier"))
            Call ReportFunction.TextChange(objRpt, "txtFormNo", Language.getMessage(mstrModuleName, 85, "Form No:"))
            Call ReportFunction.TextChange(objRpt, "txtPleaseSupply", Language.getMessage(mstrModuleName, 75, "Please Supply"))
            Call ReportFunction.TextChange(objRpt, "txtLTSof", Language.getMessage(mstrModuleName, 76, "LTS of"))
            Call ReportFunction.TextChange(objRpt, "txtgasoline", Language.getMessage(mstrModuleName, 77, "gasoline / petrol"))
            Call ReportFunction.TextChange(objRpt, "txtVehicleRegNo", Language.getMessage(mstrModuleName, 78, "to vehicle with Reg.No"))
            Call ReportFunction.TextChange(objRpt, "txtFooterVehicleRegNo", mstrVehicleRegNo)
            Call ReportFunction.TextChange(objRpt, "txtEquipmentNo", Language.getMessage(mstrModuleName, 79, "Equipment No"))
            Call ReportFunction.TextChange(objRpt, "txtEquipmentOthers", Language.getMessage(mstrModuleName, 80, "Others"))
            Call ReportFunction.TextChange(objRpt, "txtForKadco", Language.getMessage(mstrModuleName, 81, "For KADCO"))
            Call ReportFunction.TextChange(objRpt, "txtkadcoDate", Language.getMessage(mstrModuleName, 82, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtDeliveryNoteNo", Language.getMessage(mstrModuleName, 83, "Delivery Note No"))
            Call ReportFunction.TextChange(objRpt, "txtDevliveryDate", Language.getMessage(mstrModuleName, 82, "Date"))



            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 84, "Fuel/Lubricant Application Form"))


            'Pinkal (19-Jul-2021)-- Start
            'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
            If mintStatusId = 1 AndAlso mblnIsPrinted = False Then  'Approved
                Dim objclaim As New clsclaim_request_master
                If objclaim.UpdatePrintInfoForFuelApplicationForm(mintClaimFormId, True, mdtPrintedDateTime, mintPrintUserId, mstrPrintedIp, mstrPrintedHost, mblnIsPrintFromWeb) = False Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If
                objclaim = Nothing
                Call ReportFunction.TextChange(objRpt, "txtCopyName", Language.getMessage(mstrModuleName, 87, "Original Copy"))
                Call ReportFunction.TextChange(objRpt, "txtCopyName1", Language.getMessage(mstrModuleName, 87, "Original Copy"))
            ElseIf mintStatusId = 1 AndAlso mblnIsPrinted = True Then  'Approved
                Call ReportFunction.TextChange(objRpt, "txtCopyName", Language.getMessage(mstrModuleName, 88, "Reprinted Copy"))
                Call ReportFunction.TextChange(objRpt, "txtCopyName1", Language.getMessage(mstrModuleName, 88, "Reprinted Copy"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtCopyName", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtCopyName1", True)
            End If
            'Pinkal (19-Jul-2021)-- End


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_FuelApplicationForm; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Pinkal (30-May-2020) -- End



#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "APPLICATION NUMBER")
            Language.setMessage(mstrModuleName, 2, "EMPLOYEE PARTICULARS")
            Language.setMessage(mstrModuleName, 3, "Name :")
            Language.setMessage(mstrModuleName, 4, "Emp Code :")
            Language.setMessage(mstrModuleName, 5, "Job Title :")
            Language.setMessage(mstrModuleName, 6, "Dept :")
            Language.setMessage(mstrModuleName, 7, "Appt. Date :")
            Language.setMessage(mstrModuleName, 8, "Basic Salary :")
            Language.setMessage(mstrModuleName, 9, "Special Allowance :")
            Language.setMessage(mstrModuleName, 10, "Total (Basic Salary + Special Allowance) :")
            Language.setMessage(mstrModuleName, 11, "LEAVE RECORDS")
            Language.setMessage(mstrModuleName, 12, "Leave Type :")
            Language.setMessage(mstrModuleName, 13, "Leave B/F :")
            Language.setMessage(mstrModuleName, 14, "Accrued ToDate :")
            Language.setMessage(mstrModuleName, 15, "Total Issued ToDate :")
            Language.setMessage(mstrModuleName, 16, "Total Adjustment :")
            Language.setMessage(mstrModuleName, 17, "Leave Balance As on Date :")
            Language.setMessage(mstrModuleName, 18, "Last Leave Dates :")
            Language.setMessage(mstrModuleName, 19, "PREVIOUS RECORDS")
            Language.setMessage(mstrModuleName, 20, "CLAIM NAME")
            Language.setMessage(mstrModuleName, 21, "DATE")
            Language.setMessage(mstrModuleName, 22, "REQUESTED AMOUNT")
            Language.setMessage(mstrModuleName, 23, "APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 24, "TOTAL AMOUNT")
            Language.setMessage(mstrModuleName, 25, "APPLICATION DETAILS")
            Language.setMessage(mstrModuleName, 26, "CLAIM REMARK")
            Language.setMessage(mstrModuleName, 27, "APPROVALS DETAILS")
            Language.setMessage(mstrModuleName, 28, "LEVEL NAME")
            Language.setMessage(mstrModuleName, 29, "APPROVER NAME")
            Language.setMessage(mstrModuleName, 30, "APPROVAL DATE")
            Language.setMessage(mstrModuleName, 31, "APPROVAL STATUS")
            Language.setMessage(mstrModuleName, 32, "REMARKS/COMMENTS")
            Language.setMessage(mstrModuleName, 33, "APPLICATION FOR")
            Language.setMessage(mstrModuleName, 34, "Employee")
            Language.setMessage(mstrModuleName, 35, "Claims")
            Language.setMessage(mstrModuleName, 36, "Prepared By :")
            Language.setMessage(mstrModuleName, 37, "Received By :")
            Language.setMessage(mstrModuleName, 38, "Printed By :")
            Language.setMessage(mstrModuleName, 39, "Printed Date :")
            Language.setMessage(mstrModuleName, 40, "To")
            Language.setMessage(mstrModuleName, 41, "Section Group :")
            Language.setMessage(mstrModuleName, 42, "Class Group :")
            Language.setMessage(mstrModuleName, 43, "Class :")
            Language.setMessage(mstrModuleName, 44, "Grade :")
            Language.setMessage(mstrModuleName, 45, "CLAIM APPLICATION DETAILS")
            Language.setMessage(mstrModuleName, 46, "REQUEST DATE :")
            Language.setMessage(mstrModuleName, 47, "CLAIM DESCRIPTION")
            Language.setMessage(mstrModuleName, 48, "SECTOR/ROUTE")
            Language.setMessage(mstrModuleName, 49, "QUANTITY")
            Language.setMessage(mstrModuleName, 50, "UNIT PRICE")
            Language.setMessage(mstrModuleName, 51, "REQUESTED AMT")
            Language.setMessage(mstrModuleName, 52, "APPROVAL DETAILS")
            Language.setMessage(mstrModuleName, 53, "APPROVAL LEVEL")
            Language.setMessage(mstrModuleName, 54, "APPROVER")
            Language.setMessage(mstrModuleName, 55, "APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 56, "STATUS")
            Language.setMessage(mstrModuleName, 57, "APPROVER REMARKS")
            Language.setMessage(mstrModuleName, 58, "GRAND TOTAL")
            Language.setMessage(mstrModuleName, 59, "TOTAL APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 60, "EXPENSE REMARK")
            Language.setMessage(mstrModuleName, 61, "EXPENSE CATEGORY :")
            Language.setMessage(mstrModuleName, 62, "Fuel Application Details")
            Language.setMessage(mstrModuleName, 63, "Application Date :")
            Language.setMessage(mstrModuleName, 64, "Vehicle Reg. No :")
            Language.setMessage(mstrModuleName, 65, "Annual fuel balance of gasoline/petrol :")
            Language.setMessage(mstrModuleName, 66, "Amount of fuel requested :")
            Language.setMessage(mstrModuleName, 67, "Remaining fuel balance :")
            Language.setMessage(mstrModuleName, 68, "Approved amount :")
            Language.setMessage(mstrModuleName, 69, "Application Status :")
            Language.setMessage(mstrModuleName, 70, "Purpose :")
            Language.setMessage(mstrModuleName, 71, "Level Name")
            Language.setMessage(mstrModuleName, 72, "Approver Name")
            Language.setMessage(mstrModuleName, 73, "Approved Fuel Amt.")
            Language.setMessage(mstrModuleName, 74, "To : Fuel Supplier")
            Language.setMessage(mstrModuleName, 75, "Please Supply")
            Language.setMessage(mstrModuleName, 76, "LTS of")
            Language.setMessage(mstrModuleName, 77, "gasoline / petrol")
            Language.setMessage(mstrModuleName, 78, "to vehicle with Reg.No")
            Language.setMessage(mstrModuleName, 79, "Equipment No")
            Language.setMessage(mstrModuleName, 80, "Others")
            Language.setMessage(mstrModuleName, 81, "For KADCO")
            Language.setMessage(mstrModuleName, 82, "Date")
            Language.setMessage(mstrModuleName, 83, "Delivery Note No")
            Language.setMessage(mstrModuleName, 84, "Fuel/Lubricant Application Form")
			Language.setMessage(mstrModuleName, 85, "Form No:")
			Language.setMessage(mstrModuleName, 86, "Cumulative Fuel Balance :")
			Language.setMessage(mstrModuleName, 87, "Original Copy")
			Language.setMessage(mstrModuleName, 88, "Reprinted Copy")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
