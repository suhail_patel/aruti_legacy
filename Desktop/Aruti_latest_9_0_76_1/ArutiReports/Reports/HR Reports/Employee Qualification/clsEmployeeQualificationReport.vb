'************************************************************************************************************************************
'Class Name : clsEmployeeSkills.vb
'Purpose    :
'Date       :10/9/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeQualificationReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeQualificationReport"
    Private mstrReportId As String = CStr(enArutiReport.Employee_Qualification_Report)  '21
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
        Create_WithoutEducationOnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintQualificationGrpId As Integer = -1
    Private mstrQualificationGrpName As String = String.Empty
    Private mintQualificationId As Integer = -1
    Private mstrQualificationName As String = String.Empty
    Private mintInstituteId As Integer = -1
    Private mstrInstituteName As String = String.Empty
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    Private mintDepartmentId As Integer = -1
    Private mstrDepartmentName As String = String.Empty
    Private mintJobId As Integer = -1
    Private mstrJobName As String = String.Empty

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sandeep [ 12 MARCH 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsAcitve As Boolean = True
    'Pinkal (24-Jun-2011) -- End


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    'Pinkal (22-Mar-2012) -- End


    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End 

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End


    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _QualificationGrpId() As Integer
        Set(ByVal value As Integer)
            mintQualificationGrpId = value
        End Set
    End Property

    Public WriteOnly Property _QualificationGrpName() As String
        Set(ByVal value As String)
            mstrQualificationGrpName = value
        End Set
    End Property

    Public WriteOnly Property _QualificationId() As Integer
        Set(ByVal value As Integer)
            mintQualificationId = value
        End Set
    End Property

    Public WriteOnly Property _QualificationName() As String
        Set(ByVal value As String)
            mstrQualificationName = value
        End Set
    End Property

    Public WriteOnly Property _InstituteId() As Integer
        Set(ByVal value As Integer)
            mintInstituteId = value
        End Set
    End Property

    Public WriteOnly Property _InstituteName() As String
        Set(ByVal value As String)
            mstrInstituteName = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _BranchName() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentName() As String
        Set(ByVal value As String)
            mstrDepartmentName = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _JobName() As String
        Set(ByVal value As String)
            mstrJobName = value
        End Set
    End Property


    'Sandeep [ 12 MARCH 2011 ] -- Start
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property
    'Sandeep [ 12 MARCH 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAcitve = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End



    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Pinkal (22-Mar-2012) -- End

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Anjan (17 May 2012)-End 

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintQualificationGrpId = 0
            mstrQualificationGrpName = ""
            mintQualificationId = 0
            mstrQualificationName = ""
            mintInstituteId = 0
            mstrInstituteName = ""
            mintBranchId = 0
            mstrBranchName = ""
            mintDepartmentId = 0
            mstrDepartmentName = ""
            mintJobId = 0
            mstrJobName = ""
            'Sandeep [ 12 MARCH 2011 ] -- Start
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            'Sandeep [ 12 MARCH 2011 ] -- End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsAcitve = True
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            mintReportTypeId = 0
            mstrReportTypeName = ""
            'Pinkal (22-Mar-2012) -- End


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Anjan (17 May 2012)-End 

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            'Sandeep [ 12 MARCH 2011 ] -- Start
            'If mintEmployeeId > 0 Then
            '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            '    Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            'End If

            'If mintQualificationGrpId > 0 Then
            '    objDataOperation.AddParameter("@QualificationGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGrpId)
            '    Me._FilterQuery &= " AND cfcommon_master.masterunkid = @QualificationGrpId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Qualification Group :") & "" & mstrQualificationGrpName & " "
            'End If

            'If mintQualificationId > 0 Then
            '    objDataOperation.AddParameter("@QualificationId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationId)
            '    Me._FilterQuery &= " AND hrqualification_master.qualificationunkid = @QualificationId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Qualification :") & " " & mstrQualificationName & " "
            'End If

            'If mintInstituteId > 0 Then
            '    objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
            '    Me._FilterQuery &= " AND hrinstitute_master.instituteunkid = @InstituteId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Place Of Award :") & " " & mstrInstituteName & " "
            'End If

            'If mintBranchId > 0 Then
            '    objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            '    Me._FilterQuery &= " AND hrstation_master.stationunkid = @BranchId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "") & " " & mstrBranchName & " "
            'End If

            'If mintDepartmentId > 0 Then
            '    objDataOperation.AddParameter("@DepartmentId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
            '    Me._FilterQuery &= " AND hrdepartment_master.departmentunkid = @DepartmentId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "") & " " & mstrDepartmentName & " "
            'End If

            'If mintJobId > 0 Then
            '    objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            '    Me._FilterQuery &= " AND hrjob_master.jobunkid = @JobId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "") & " " & mstrJobName & " "
            'End If


            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsAcitve = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                If mintReportTypeId = 0 Then
                    Me._FilterQuery &= " AND EmpId = @EmployeeId "
                ElseIf mintReportTypeId = 1 Then
                    Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintReportTypeId = 0 Then

                If mintQualificationGrpId > 0 Then
                    objDataOperation.AddParameter("@QualificationGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGrpId)
                    Me._FilterQuery &= " AND QGrpId = @QualificationGrpId "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Qualification Group :") & "" & mstrQualificationGrpName & " "
                End If

                If mintQualificationId > 0 Then
                    objDataOperation.AddParameter("@QualificationId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationId)
                    Me._FilterQuery &= " AND QuaId = @QualificationId "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Qualification :") & " " & mstrQualificationName & " "
                End If

                If mintInstituteId > 0 Then
                    objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
                    Me._FilterQuery &= " AND InstId= @InstituteId "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Place Of Award :") & " " & mstrInstituteName & " "
                End If

            End If

            'If mintViewIndex <> enAnalysisReport.Job Then

            '    If mstrViewByName.Length > 0 Then
            '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Analysis By :") & " " & mstrViewByName & " "
            '    End If

            'End If

            'Sandeep [ 12 MARCH 2011 ] -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid


        '    'Pinkal (22-Mar-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintReportTypeId = 0 Then      'WITH EDUCATION DETAIL
        '        objRpt = Generate_DetailReport()

        '    ElseIf mintReportTypeId = 1 Then   'WITHOUT EDUCATION DETAIL
        '        objRpt = Generate_WithoutEducationDetailReport()
        '    End If

        '    'Pinkal (22-Mar-2012) -- End
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If mintReportTypeId = 0 Then      'WITH EDUCATION DETAIL
                objRpt = Generate_DetailReport(xDatabaseName, _
                                               xUserUnkid, _
                                               xYearUnkid, _
                                               xCompanyUnkid, _
                                               xPeriodStart, _
                                               xPeriodEnd, _
                                               xUserModeSetting, _
                                               xOnlyApproved)

            ElseIf mintReportTypeId = 1 Then   'WITHOUT EDUCATION DETAIL
                objRpt = Generate_WithoutEducationDetailReport(xDatabaseName, _
                                                               xUserUnkid, _
                                                               xYearUnkid, _
                                                               xCompanyUnkid, _
                                                               xPeriodStart, _
                                                               xPeriodEnd, _
                                                               xUserModeSetting, _
                                                               xOnlyApproved)
            End If

            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes

            If mintReportTypeId = 0 Then
                OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            ElseIf mintReportTypeId = 1 Then
                OrderByDisplay = iColumn_WithoutEducationDetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_WithoutEducationDetailReport.ColumnItem(0).Name
            End If

            'Pinkal (22-Mar-2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes

            If mintReportTypeId = 0 Then
                Call OrderByExecute(iColumn_DetailReport)
            ElseIf mintReportTypeId = 1 Then
                Call OrderByExecute(iColumn_WithoutEducationDetailReport)
            End If

            'Pinkal (22-Mar-2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            iColumn_DetailReport.Add(New IColumn("ISNULL(EndDate,'')", Language.getMessage(mstrModuleName, 38, "End Date")))
            'Pinkal (25-APR-2012) -- End

            iColumn_DetailReport.Add(New IColumn("ISNULL(EmpName,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(QualificationGrp,'')", Language.getMessage(mstrModuleName, 7, "Qualification Group")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(Qualification,'')", Language.getMessage(mstrModuleName, 9, "Qualification")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(POA,'')", Language.getMessage(mstrModuleName, 10, "Place Of Award")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(Date,'')", Language.getMessage(mstrModuleName, 11, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("qlevel", Language.getMessage(mstrModuleName, 37, "Qualification Group Level")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Dim iColumn_WithoutEducationDetailReport As New IColumnCollection

    Public Property Field_OnWithoutEducationDetailReport() As IColumnCollection
        Get
            Return iColumn_WithoutEducationDetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_WithoutEducationDetailReport = value
        End Set
    End Property

    Private Sub Create_WithoutEducationOnDetailReport()
        Try
            iColumn_WithoutEducationDetailReport.Clear()

            iColumn_WithoutEducationDetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 28, "Employee Code")))
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_WithoutEducationDetailReport.Add(New IColumn(" ISNULL(firstname,'') + ' ' + ISNULL(surname,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_WithoutEducationDetailReport.Add(New IColumn(" ISNULL(surname,'') + ' ' + ISNULL(firstname,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            Else
                iColumn_WithoutEducationDetailReport.Add(New IColumn(" ISNULL(firstname,'') + ' ' + ISNULL(surname,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END


            iColumn_WithoutEducationDetailReport.Add(New IColumn("job_name", Language.getMessage(mstrModuleName, 13, "Job Title")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_WithoutEducationOnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_WithoutEducationDetailReport(ByVal strDatabaseName As String, _
                                                           ByVal intUserUnkid As Integer, _
                                                           ByVal intYearUnkid As Integer, _
                                                           ByVal intCompanyUnkid As Integer, _
                                                           ByVal dtPeriodStart As Date, _
                                                           ByVal dtPeriodEnd As Date, _
                                                           ByVal strUserModeSetting As String, _
                                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        'Private Function Generate_WithoutEducationDetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

            'StrQ = "SELECT "
            'Select Case mintViewIndex

            '    Case enAnalysisReport.Branch
            '        StrQ &= "  name AS GName, "

            '    Case enAnalysisReport.Department
            '        StrQ &= "  name AS GName, "

            '    Case enAnalysisReport.Section
            '        StrQ &= "  name AS GName, "

            '    Case enAnalysisReport.Unit
            '        StrQ &= "  name AS GName, "

            '    Case enAnalysisReport.Job
            '        StrQ &= " hrjob_master.job_name AS GName, "

            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " costcentername AS GName, "

            '    Case enAnalysisReport.SectionGroup
            '        StrQ &= " name AS GName,"

            '    Case enAnalysisReport.UnitGroup
            '        StrQ &= " name AS GName, "

            '    Case enAnalysisReport.Team
            '        StrQ &= " name AS GName, "

            '    Case Else
            '        StrQ &= " '' AS GName, "
            'End Select


            'Anjan (17 May 2012)-End 




            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'StrQ &= "SELECT  ISNULL(employeeunkid,'') as  employeeunkid " & _
            '        ",ISNULL(employeecode,'') as  employeecode "

            ''S.SANDEEP [ 26 MAR 2014 ] -- START
            ''ENHANCEMENT : Requested by Rutta
            ''",ISNULL(firstname,'') + ' ' + ISNULL(surname,'') as  EmpName " & _
            'If mblnFirstNamethenSurname = False Then
            '    StrQ &= ",ISNULL(surname,'') + ' ' + ISNULL(firstname,'') as  EmpName "
            'Else
            '    StrQ &= ",ISNULL(firstname,'') + ' ' + ISNULL(surname,'') as  EmpName "
            'End If

            'StrQ &= ",ISNULL(jm.job_code,'') as job_code " & _
            '             ",ISNULL(jm.job_level,'') as job_level " & _
            '             ",ISNULL(jm.job_name,'') as job_name  "

            ''Anjan (17 May 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            ''Anjan (17 May 2012)-End 


            'StrQ &= " FROM hremployee_master " & _
            '             " LEFT JOIN hrjob_master jm ON hremployee_master.jobunkid = jm.jobunkid "


            ''Anjan (17 May 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  "

            ''    Case enAnalysisReport.Department
            ''        StrQ &= " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "

            ''    Case enAnalysisReport.Section
            ''        StrQ &= " JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid "

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid "

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid "

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid "

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid "

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid "

            ''End Select

            'StrQ &= mstrAnalysis_Join

            ''Anjan (17 May 2012)-End 


            'StrQ &= " WHERE employeeunkid NOT IN (SELECT employeeunkid FROM hremp_qualification_tran WHERE isvoid = 0) "

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If mblnIsAcitve = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= "	AND ISNULL(hremployee_master.isactive,0) = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES

            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END

            ''S.SANDEEP [ 12 MAY 2012 ] -- END



            ''Anjan (17 May 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " AND hrstation_master.stationunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Department
            ''        StrQ &= " AND hrdepartment_master.departmentunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Section
            ''        StrQ &= "AND hrsection_master.sectionunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " AND hrunit_master.unitunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " AND hrjob_master.jobunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " AND prcostcenter_master.costcenterunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " AND hrsectiongroup_master.sectiongroupunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " AND hrunitgroup_master.unitgroupunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " AND hrteam_master.teamunkid in (" & mstrViewByIds & ")"

            ''End Select


            StrQ &= "SELECT  ISNULL(hremployee_master.employeeunkid,'') as  employeeunkid " & _
                    ",ISNULL(hremployee_master.employeecode,'') as  employeecode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') as  EmpName "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') as  EmpName "
            End If

            StrQ &= ",ISNULL(jm.job_code,'') as job_code " & _
                    ",ISNULL(jm.job_level,'') as job_level " & _
                    ",ISNULL(jm.job_name,'') as job_name  "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.rno = 1 " & _
                    "JOIN hrjob_master AS jm ON J.jobunkid = jm.jobunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hremp_qualification_tran WHERE isvoid = 0) "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsAcitve = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'S.SANDEEP [04 JUN 2015] -- END



            'Anjan (17 May 2012)-End 




            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column3") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column4") = dtRow.Item("job_name")

                If dtRow.Item("GName").ToString() <> "" Then
                    Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column5") = drSubTotal.Length
                End If

                rpt_Rows.Item("Column6") = dsList.Tables("DataTable").Rows.Count

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeWithNoEducation


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 29, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 30, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 31, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 32, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If


            If mintViewIndex = enAnalysisReport.Job Then
                ReportFunction.EnableSuppress(objRpt, "Column41", True)
                ReportFunction.EnableSuppress(objRpt, "txtJobTitle", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 33, "Emp No."))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 12, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 13, "Job Title"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 16, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 17, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 18, "Grand Total :"))


            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'Select Case mintViewIndex

            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 19, "Branch :"))

            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 20, "Department :"))

            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 21, "Section :"))

            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 22, "Unit :"))

            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 23, "Job :"))

            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 24, "Cost Center :"))

            '    Case enAnalysisReport.SectionGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 25, "Section Group :"))

            '    Case enAnalysisReport.UnitGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 26, "Unit Group :"))

            '    Case enAnalysisReport.Team
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 27, "Team :"))

            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")

            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Anjan (17 May 2012)-End 




            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_WithoutEducationDetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (22-Mar-2012) -- End


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        'Sandeep [ 12 MARCH 2011 ] -- Start
        Dim blnFlag As Boolean = False
        'Sandeep [ 12 MARCH 2011 ] -- End 
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sandeep [ 12 MARCH 2011 ] -- Start
            'StrQ = "SELECT " & _
            '            "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '            "	,ISNULL(cfcommon_master.name,'') AS QualificationGrp " & _
            '            "	,ISNULL(hrqualification_master.qualificationname,'') AS Qualification " & _
            '            "	,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '            "	,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '            "FROM hremp_qualification_tran " & _
            '            "	JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '            "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '            "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
            '            "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '            "	JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '            "	JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
            '            "	JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '            "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 " & _
            '            "	AND ISNULL(hremployee_master.isactive,0) = 1 "
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'StrQ = "SELECT "
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= " stationunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Department
            '        StrQ &= " departmentunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Section
            '        StrQ &= "  sectionunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Unit
            '        StrQ &= "  unitunkid , name AS GName "
            '        blnFlag = True
            '    Case enAnalysisReport.Job
            '        StrQ &= "  jobunkid , job_name AS GName "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " costcenterunkid  , costcentername AS GName "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        StrQ &= " ISNULL(EmpName,'') AS EmpName " & _
            '                  ", ISNULL(QualificationGrp,'') AS QualificationGrp " & _
            '                  ", ISNULL(Qualification,'') AS Qualification " & _
            '                  ", ISNULL(POA,'') AS POA " & _
            '                  ", ISNULL(Date,'') AS Date " & _
            '                  ", ISNULL(QDesc,'') AS QDesc " & _
            '                  ",'' AS GName " & _
            '                  ", qlevel " & _
            '                  ", ISNULL(EndDate,'')  AS EndDate "
            '        blnFlag = False
            'End Select
            'If blnFlag = True Then
            '    StrQ &= ", ISNULL(EmpName,'') AS EmpName " & _
            '                  ", ISNULL(QualificationGrp,'') AS QualificationGrp " & _
            '                  ", ISNULL(Qualification,'') AS Qualification " & _
            '                  ", ISNULL(POA,'') AS POA " & _
            '                  ", ISNULL(Date,'') AS Date " & _
            '                  ", ISNULL(QDesc,'') AS QDesc " & _
            '                  ", ISNULL(EndDate,'')  AS EndDate "
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " FROM hrstation_master "
            '        Case enAnalysisReport.Department
            '            StrQ &= " FROM hrdepartment_master "
            '        Case enAnalysisReport.Section
            '            StrQ &= " FROM hrsection_master "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " FROM hrunit_master "
            '        Case enAnalysisReport.Job
            '            StrQ &= " FROM hrjob_master "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " FROM prcostcenter_master "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            '    'StrQ &= " LEFT JOIN " & _
            '    '             " ( " & _
            '    '                " SELECT " & _
            '    '                "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '    '                "	,ISNULL(cfcommon_master.name,'') AS QualificationGrp " & _
            '    '                "	,ISNULL(hrqualification_master.qualificationname,'') AS Qualification " & _
            '    '                "	,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '    '                "	,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '    '                "	,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '    '                "   ,hremployee_master.employeeunkid AS EmpId " & _
            '    '                "   ,cfcommon_master.masterunkid AS QGrpId " & _
            '    '                "   ,hrqualification_master.qualificationunkid AS QuaId  " & _
            '    '                "   ,hrinstitute_master.instituteunkid AS InstId " & _
            '    '                "  , ISNULL(cfcommon_master.qlevel,0) as qlevel " & _
            '    '                "  ,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') AS EndDate "
            'StrQ &= " LEFT JOIN " & _
            '             " ( " & _
            '                " SELECT " & _
            '                "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '                   "	,CASE WHEN ISNULL(cfcommon_master.name,'') <>'' THEN ISNULL(cfcommon_master.name,'') ELSE ISNULL(other_qualificationgrp,'') END AS QualificationGrp " & _
            '                   "	,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrqualification_master.qualificationname,'') ELSE ISNULL(other_qualification,'') END AS Qualification " & _
            '                "	,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '                "	,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '                "	,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '                "   ,hremployee_master.employeeunkid AS EmpId " & _
            '                   "   ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
            '                   "   ,ISNULL(hrqualification_master.qualificationunkid,0) AS QuaId  " & _
            '                   "   ,ISNULL(hrinstitute_master.instituteunkid,0) AS InstId " & _
            '                "  , ISNULL(cfcommon_master.qlevel,0) as qlevel " & _
            '                "  ,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') AS EndDate "
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= ", hremployee_master.stationunkid AS BranchId  "
            '        Case enAnalysisReport.Department
            '            StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            '        Case enAnalysisReport.Section
            '            StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            '        Case enAnalysisReport.Unit
            '            StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            '        Case enAnalysisReport.Job
            '            StrQ &= ", hremployee_master.jobunkid AS JobId  "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= ", hremployee_master.costcenterunkid As CCId "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            '    'Pinkal (24-Jun-2011) -- Start
            '    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            '    'StrQ &= "FROM hremp_qualification_tran " & _
            '    '                "	JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    '                "	JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '    '                "	JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
            '    '                "	JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '    '            "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 " & _
            '    '            "	AND ISNULL(hremployee_master.isactive,0) = 1 "
            'StrQ &= "FROM hremp_qualification_tran " & _
            '                "	JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                    "	LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '                    "	LEFT JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
            '                    "	LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '                "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 "
            '    If mblnIsAcitve = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= "	AND ISNULL(hremployee_master.isactive,0) = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    'Pinkal (24-Jun-2011) -- End
            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " ) AS EQS ON hrstation_master.stationunkid = EQS.BranchId " & _
            '                          " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Department
            '            StrQ &= " ) AS EQS ON hrdepartment_master.departmentunkid = EQS.DeptId " & _
            '                          " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Section
            '            StrQ &= " ) AS EQS ON hrsection_master.sectionunkid = EQS.SecId " & _
            '                          " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " ) AS EQS ON hrunit_master.unitunkid = EQS.UnitId " & _
            '                          " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Job
            '            StrQ &= " ) AS EQS ON hrjob_master.jobunkid = EQS.JobId " & _
            '                          " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " ) AS EQS ON prcostcenter_master.costcenterunkid = EQS.CCId " & _
            '                          " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            'Else
            '    'StrQ &= " FROM " & _
            '    '             " ( " & _
            '    '                    " SELECT " & _
            '    '        "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '    '        "	,ISNULL(cfcommon_master.name,'') AS QualificationGrp " & _
            '    '        "	,ISNULL(hrqualification_master.qualificationname,'') AS Qualification " & _
            '    '        "	,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '    '        "	,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '    '        "	,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '    '                    "   ,hremployee_master.employeeunkid AS EmpId " & _
            '    '                    "   ,cfcommon_master.masterunkid AS QGrpId " & _
            '    '                    "   ,hrqualification_master.qualificationunkid AS QuaId  " & _
            '    '                    "   ,hrinstitute_master.instituteunkid AS InstId " & _
            '    '        "FROM hremp_qualification_tran " & _
            '    '        "	JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    '        "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '    '        "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
            '    '        "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '    '        "	JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '    '        "	JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
            '    '        "	JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '    '        "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 " & _
            '    '                    "	AND ISNULL(hremployee_master.isactive,0) = 1 ) AS EQS WHERE 1 = 1 "
            '    'Anjan (17 Apr 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    'StrQ &= " FROM " & _
            '    '             " ( " & _
            '    '                    " SELECT " & _
            '    '        "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '    '        "	,ISNULL(cfcommon_master.name,'') AS QualificationGrp " & _
            '    '        "	,ISNULL(hrqualification_master.qualificationname,'') AS Qualification " & _
            '    '        "	,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '    '        "	,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '    '        "	,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '    '                    "   ,hremployee_master.employeeunkid AS EmpId " & _
            '    '                    "   ,cfcommon_master.masterunkid AS QGrpId " & _
            '    '                    "   ,hrqualification_master.qualificationunkid AS QuaId  " & _
            '    '                    "   ,hrinstitute_master.instituteunkid AS InstId " & _
            '    '                    "  , ISNULL(cfcommon_master.qlevel,0) as qlevel " & _
            '    '                    ",ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') AS EndDate " & _
            '    '        "FROM hremp_qualification_tran " & _
            '    '        "	JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    '        "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '    '        "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
            '    '        "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '    '        "	JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '    '        "	JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
            '    '        "	JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '    '      "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 "
            'StrQ &= " FROM " & _
            '             " ( " & _
            '                    " SELECT " & _
            '        "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '           "	,CASE WHEN ISNULL(cfcommon_master.name,'') <>'' THEN ISNULL(cfcommon_master.name,'') ELSE ISNULL(other_qualificationgrp,'') END AS QualificationGrp " & _
            '           "	,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrqualification_master.qualificationname,'') ELSE ISNULL(other_qualification,'') END AS Qualification " & _
            '        "	,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '        "	,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '        "	,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '                    "   ,hremployee_master.employeeunkid AS EmpId " & _
            '                       "   ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
            '                       "   ,ISNULL(hrqualification_master.qualificationunkid,0) AS QuaId  " & _
            '                       "   ,ISNULL(hrinstitute_master.instituteunkid,0) AS InstId " & _
            '                    "  , ISNULL(cfcommon_master.qlevel,0) as qlevel " & _
            '                    ",ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') AS EndDate " & _
            '        "FROM hremp_qualification_tran " & _
            '        "	JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
            '        "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '           "	LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '           "	LEFT JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
            '           "	LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '      "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 "
            '    'Anjan (17 Apr 2012)-End 
            '    If mblnIsAcitve = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= "	AND ISNULL(hremployee_master.isactive,0) = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    StrQ &= "  ) AS EQS WHERE 1 = 1 "
            'End If
            ''Sandeep [ 12 MARCH 2011 ] -- End 

            StrQ &= "SELECT " & _
                    " ISNULL(EmpName,'') AS EmpName " & _
                    ",ISNULL(QualificationGrp,'') AS QualificationGrp " & _
                    ",ISNULL(Qualification,'') AS Qualification " & _
                    ",ISNULL(POA,'') AS POA " & _
                    ",ISNULL(Date,'') AS Date " & _
                    ",ISNULL(QDesc,'') AS QDesc " & _
                    ",GName AS GName " & _
                    ",Id AS Id " & _
                    ",qlevel " & _
                    ",ISNULL(EndDate,'')  AS EndDate " & _
                    ",ISNULL(employeecode,'') AS employeecode " & _
                    " FROM " & _
                    "( " & _
                    "   SELECT "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '"	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            If mblnFirstNamethenSurname = False Then
                StrQ &= "	 ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If

            'S.SANDEEP |24-JAN-2019| -- START
            StrQ &= "   ,ISNULL(hremployee_master.employeecode,'') as employeecode "
            'S.SANDEEP |24-JAN-2019| -- END


            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            'StrQ &= "  ,CASE WHEN ISNULL(cfcommon_master.name,'') <>'' THEN ISNULL(cfcommon_master.name,'') ELSE ISNULL(other_qualificationgrp,'') END AS QualificationGrp " & _
            '                    "  ,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrqualification_master.qualificationname,'') ELSE ISNULL(other_qualification,'') END AS Qualification " & _
            '                    "  ,ISNULL(hrinstitute_master.institute_name,'') AS POA " & _
            '                    "  ,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
            '                    "  ,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
            '                    "  ,hremployee_master.employeeunkid AS EmpId " & _
            '                    "  ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
            '                    "  ,ISNULL(hrqualification_master.qualificationunkid,0) AS QuaId  " & _
            '                    "  ,ISNULL(hrinstitute_master.instituteunkid,0) AS InstId " & _
            '                    "  ,ISNULL(cfcommon_master.qlevel,0) as qlevel " & _
            '                    "  ,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') AS EndDate "


            StrQ &= "  ,CASE WHEN ISNULL(cfcommon_master.name,'') <>'' THEN ISNULL(cfcommon_master.name,'') ELSE ISNULL(other_qualificationgrp,'') END AS QualificationGrp " & _
                    "  ,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrqualification_master.qualificationname,'') ELSE ISNULL(other_qualification,'') END AS Qualification " & _
                    "  ,CASE WHEN ISNULL(hrqualification_master.qualificationname,'') <>'' THEN ISNULL(hrinstitute_master.institute_name,'') ELSE ISNULL(other_institute,'') END AS POA " & _
                    "  ,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') AS Date " & _
                    "  ,ISNULL(hremp_qualification_tran.remark,'') AS QDesc " & _
                    "  ,hremployee_master.employeeunkid AS EmpId " & _
                    "  ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
                    "  ,ISNULL(hrqualification_master.qualificationunkid,0) AS QuaId  " & _
                    "  ,ISNULL(hrinstitute_master.instituteunkid,0) AS InstId " & _
                    "  ,ISNULL(cfcommon_master.qlevel,0) as qlevel " & _
                    "  ,ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') AS EndDate "
            'S.SANDEEP [23 JUL 2016] -- END

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremp_qualification_tran " & _
                    "   JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            
            StrQ &= "   LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "   LEFT JOIN cfcommon_master ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
                    "   LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital=0 "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 "

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If mblnIsAcitve = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsAcitve = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= " ) AS A WHERE 1 = 1 "
            'S.SANDEEP [ 16 MAY 2012 ] -- END


            'S.SANDEEP |24-JAN-2019| -- START
            '---> "   ,ISNULL(employeecode,'') as employeecode " -- ADDED
            'S.SANDEEP |24-JAN-2019| -- END

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                'S.SANDEEP [ 23 SEP 2011 ] -- START
                'ENHANCEMENT : CODE OPTIMIZATION
                If dtRow.Item("EmpName").ToString.Trim.Length <= 0 Then Continue For
                'S.SANDEEP [ 23 SEP 2011 ] -- END

                'S.SANDEEP |24-JAN-2019| -- START
                'rpt_Row.Item("Column1") = dtRow.Item("EmpName")
                rpt_Row.Item("Column1") = dtRow.Item("employeecode") & " - " & dtRow.Item("EmpName")
                'S.SANDEEP |24-JAN-2019| -- END
                rpt_Row.Item("Column2") = dtRow.Item("QualificationGrp")
                rpt_Row.Item("Column3") = dtRow.Item("Qualification")
                rpt_Row.Item("Column4") = dtRow.Item("POA")
                If dtRow.Item("Date").ToString.Trim <> "" Then
                    rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column5") = ""
                End If
                rpt_Row.Item("Column6") = dtRow.Item("QDesc")

                'Sandeep [ 12 MARCH 2011 ] -- Start
                rpt_Row.Item("Column7") = dtRow.Item("GName")
                'Sandeep [ 12 MARCH 2011 ] -- End 
                If dtRow.Item("EndDate").ToString.Trim <> "" Then
                    rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("EndDate").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column8") = ""
                End If


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeQualification

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 29, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 30, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 31, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 32, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 

            objRpt.SetDataSource(rpt_Data)

            objRpt.Subreports("rptQualifCount").SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt.Subreports("rptQualifCount"), "txtQualifCount", Language.getMessage(mstrModuleName, 36, "Qualification Count"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualifCount"), "txtQualifGroup", Language.getMessage(mstrModuleName, 7, "Qualification Group"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualifCount"), "txtNo", Language.getMessage(mstrModuleName, 34, "No."))



            ReportFunction.TextChange(objRpt, "txtNo", Language.getMessage(mstrModuleName, 34, "No."))
            ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 6, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtQGrp", Language.getMessage(mstrModuleName, 7, "Qualification Group"))
            ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 9, "Qualification"))
            ReportFunction.TextChange(objRpt, "txtPlace", Language.getMessage(mstrModuleName, 10, "Place Of Award"))
            ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 11, "Start Date"))
            ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 35, "Remark"))
            ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 39, "Award Date"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 16, "Page :"))


            'Sandeep [ 12 MARCH 2011 ] -- Start

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 19, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 20, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 21, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 22, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 23, "Job :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 24, "Cost Center :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            'Sandeep [ 12 MARCH 2011 ] -- End 

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            'Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            'Pinkal (22-Mar-2012) -- End


            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Qualification Group :")
            Language.setMessage(mstrModuleName, 3, "Qualification :")
            Language.setMessage(mstrModuleName, 4, "Place Of Award :")
            Language.setMessage(mstrModuleName, 5, "Order By :")
            Language.setMessage(mstrModuleName, 6, "Employee Name")
            Language.setMessage(mstrModuleName, 7, "Qualification Group")
            Language.setMessage(mstrModuleName, 9, "Qualification")
            Language.setMessage(mstrModuleName, 10, "Place Of Award")
            Language.setMessage(mstrModuleName, 11, "Start Date")
            Language.setMessage(mstrModuleName, 12, "Employee Name")
            Language.setMessage(mstrModuleName, 13, "Job Title")
            Language.setMessage(mstrModuleName, 14, "Printed By :")
            Language.setMessage(mstrModuleName, 15, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, "Page :")
            Language.setMessage(mstrModuleName, 17, "Sub Total :")
            Language.setMessage(mstrModuleName, 18, "Grand Total :")
            Language.setMessage(mstrModuleName, 28, "Employee Code")
            Language.setMessage(mstrModuleName, 29, "Prepared By :")
            Language.setMessage(mstrModuleName, 30, "Checked By :")
            Language.setMessage(mstrModuleName, 31, "Approved By :")
            Language.setMessage(mstrModuleName, 32, "Received By :")
            Language.setMessage(mstrModuleName, 33, "Emp No.")
            Language.setMessage(mstrModuleName, 34, "No.")
            Language.setMessage(mstrModuleName, 35, "Remark")
            Language.setMessage(mstrModuleName, 36, "Qualification Count")
            Language.setMessage(mstrModuleName, 37, "Qualification Group Level")
            Language.setMessage(mstrModuleName, 38, "End Date")
            Language.setMessage(mstrModuleName, 39, "Award Date")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
