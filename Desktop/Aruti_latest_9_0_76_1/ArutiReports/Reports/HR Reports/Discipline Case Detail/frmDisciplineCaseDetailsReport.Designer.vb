﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineCaseDetailsReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeInactiveEmployee = New System.Windows.Forms.CheckBox
        Me.chkDisplayAllocationBasedOnChargeDate = New System.Windows.Forms.CheckBox
        Me.cboOffenceCategory = New System.Windows.Forms.ComboBox
        Me.lblOffenceCategory = New System.Windows.Forms.Label
        Me.lblDateFrom = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchOffence = New eZee.Common.eZeeGradientButton
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 433)
        Me.NavPanel.Size = New System.Drawing.Size(713, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeInactiveEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.chkDisplayAllocationBasedOnChargeDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboOffenceCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblOffenceCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchOffence)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(460, 192)
        Me.gbFilterCriteria.TabIndex = 20
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeInactiveEmployee
        '
        Me.chkIncludeInactiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmployee.Location = New System.Drawing.Point(124, 166)
        Me.chkIncludeInactiveEmployee.Name = "chkIncludeInactiveEmployee"
        Me.chkIncludeInactiveEmployee.Size = New System.Drawing.Size(292, 17)
        Me.chkIncludeInactiveEmployee.TabIndex = 165
        Me.chkIncludeInactiveEmployee.Text = "Include Inactive Employee"
        Me.chkIncludeInactiveEmployee.UseVisualStyleBackColor = True
        '
        'chkDisplayAllocationBasedOnChargeDate
        '
        Me.chkDisplayAllocationBasedOnChargeDate.Checked = True
        Me.chkDisplayAllocationBasedOnChargeDate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDisplayAllocationBasedOnChargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayAllocationBasedOnChargeDate.Location = New System.Drawing.Point(124, 143)
        Me.chkDisplayAllocationBasedOnChargeDate.Name = "chkDisplayAllocationBasedOnChargeDate"
        Me.chkDisplayAllocationBasedOnChargeDate.Size = New System.Drawing.Size(292, 17)
        Me.chkDisplayAllocationBasedOnChargeDate.TabIndex = 145
        Me.chkDisplayAllocationBasedOnChargeDate.Text = "Display Employee Allocation Based on Charge Date"
        Me.chkDisplayAllocationBasedOnChargeDate.UseVisualStyleBackColor = True
        '
        'cboOffenceCategory
        '
        Me.cboOffenceCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOffenceCategory.DropDownWidth = 300
        Me.cboOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOffenceCategory.FormattingEnabled = True
        Me.cboOffenceCategory.Location = New System.Drawing.Point(124, 61)
        Me.cboOffenceCategory.Name = "cboOffenceCategory"
        Me.cboOffenceCategory.Size = New System.Drawing.Size(292, 21)
        Me.cboOffenceCategory.TabIndex = 139
        '
        'lblOffenceCategory
        '
        Me.lblOffenceCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffenceCategory.Location = New System.Drawing.Point(8, 63)
        Me.lblOffenceCategory.Name = "lblOffenceCategory"
        Me.lblOffenceCategory.Size = New System.Drawing.Size(110, 17)
        Me.lblOffenceCategory.TabIndex = 138
        Me.lblOffenceCategory.Text = "Offence Category"
        Me.lblOffenceCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateFrom
        '
        Me.lblDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(8, 36)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(110, 17)
        Me.lblDateFrom.TabIndex = 88
        Me.lblDateFrom.Text = "Charge Date"
        Me.lblDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(254, 37)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(37, 15)
        Me.lblTo.TabIndex = 87
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(297, 34)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpToDate.TabIndex = 86
        '
        'objbtnSearchOffence
        '
        Me.objbtnSearchOffence.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOffence.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOffence.BorderSelected = False
        Me.objbtnSearchOffence.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOffence.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOffence.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOffence.Location = New System.Drawing.Point(422, 88)
        Me.objbtnSearchOffence.Name = "objbtnSearchOffence"
        Me.objbtnSearchOffence.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOffence.TabIndex = 143
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(124, 34)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpFromDate.TabIndex = 86
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(363, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 20
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(8, 90)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(110, 17)
        Me.lblDisciplineType.TabIndex = 141
        Me.lblDisciplineType.Text = "Offence Description"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(422, 115)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(422, 61)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 140
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 117)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(110, 17)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.DropDownWidth = 1000
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(124, 88)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(292, 21)
        Me.cboDisciplineType.TabIndex = 142
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(124, 115)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(292, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'frmDisciplineCaseDetailsReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(713, 488)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmDisciplineCaseDetailsReport"
        Me.Text = "frmDisciplineCaseDetailsReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboOffenceCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblOffenceCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchOffence As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents chkDisplayAllocationBasedOnChargeDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeInactiveEmployee As System.Windows.Forms.CheckBox
End Class
