'************************************************************************************************************************************
'Class Name : frmAssessmentListReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentListReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentListReport"
    Private objAssessmentList As clsAssessmentListReport

#End Region

#Region " Contructor "

    Public Sub New()
        objAssessmentList = New clsAssessmentListReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAssessmentList.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjAssessGrp As New clsassess_group_master
        Dim ObjAssessItem As New clsassess_item_master
        Dim ObjResult As New clsresult_master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim ObjBranch As New clsStation
        Dim ObjJob As New clsJobs
        Dim ObjAssessor As New clsAssessor
        Dim dsCombos As New DataSet
        Try

            'Pinkal (24-Jun-2011) -- Start

            ' dsCombos = ObjEmp.GetEmployeeList("List", True, True)
            dsCombos = ObjEmp.GetEmployeeList("List", True, False)

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjAssessGrp.getListForCombo("List", True)
            With cboAssessedGroup
                .ValueMember = "assessgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjAssessItem.getListForCombo("List", True)
            With cboAssessedItem
                .ValueMember = "assessitemunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjResult.getComboList("List", True)
            With cboResult
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjJob.getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjAssessor.GetList("List", True)
            With cboAssessor
                .ValueMember = "assessormasterunkid"
                .DisplayMember = "assessorname"
                .DataSource = dsCombos.Tables("List")
            End With

            'Sandeep [ 10 FEB 2011 ] -- Start
            dsCombos = objAssessmentList.Get_ReportType("RType")
            With cboReportType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("RType")
            End With
            'Sandeep [ 10 FEB 2011 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjEmp = Nothing
            ObjAssessGrp = Nothing
            ObjAssessItem = Nothing
            ObjResult = Nothing
            ObjPeriod = Nothing
            ObjBranch = Nothing
            ObjJob = Nothing
            ObjAssessor = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboAssessedGroup.SelectedValue = 0
            cboAssessedItem.SelectedValue = 0
            cboAssessor.SelectedValue = 0
            cboBranch.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboResult.SelectedValue = 0
            objAssessmentList.setDefaultOrderBy(0)
            txtOrderBy.Text = objAssessmentList.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAssessmentList.SetDefaultValue()

            objAssessmentList._AssessGrpId = cboAssessedGroup.SelectedValue
            objAssessmentList._AssessGrpName = cboAssessedGroup.Text

            objAssessmentList._AssessItemId = cboAssessedItem.SelectedValue
            objAssessmentList._AssessItemName = cboAssessedItem.Text

            objAssessmentList._AssessorId = cboAssessor.SelectedValue
            objAssessmentList._AssessorName = cboAssessor.Text

            objAssessmentList._BranchId = cboBranch.SelectedValue
            objAssessmentList._BranchName = cboBranch.Text

            objAssessmentList._EmployeeId = cboEmployee.SelectedValue
            objAssessmentList._EmployeeName = cboEmployee.Text

            objAssessmentList._JobId = cboJob.SelectedValue
            objAssessmentList._JobName = cboJob.Text

            objAssessmentList._PeriodId = cboPeriod.SelectedValue
            objAssessmentList._PeriodName = cboPeriod.Text

            objAssessmentList._ResultId = cboResult.SelectedValue
            objAssessmentList._ResultName = cboResult.Text

            'Sandeep [ 10 FEB 2011 ] -- Start
            objAssessmentList._ReportTypeId = cboReportType.SelectedValue
            objAssessmentList._ReportTypeName = cboReportType.Text
            'Sandeep [ 10 FEB 2011 ] -- End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objAssessmentList._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            objAssessmentList._IncludeUncommited = chkUncommited.Checked
            'S.SANDEEP [ 22 JULY 2011 ] -- END 


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmAssessmentListReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAssessmentList = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeSkillsReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentListReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objAssessmentList._ReportName
            Me._Message = objAssessmentList._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentListReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'Sandeep [ 10 FEB 2011 ] -- Start
            'objAssessmentList.generateReport(0, e.Type, enExportAction.None)
            objAssessmentList.generateReport(cboReportType.SelectedValue, e.Type, enExportAction.None)
            'Sandeep [ 10 FEB 2011 ] -- End 
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'Sandeep [ 10 FEB 2011 ] -- Start
            'objAssessmentList.generateReport(0, enPrintAction.None, e.Type)
            objAssessmentList.generateReport(cboReportType.SelectedValue, enPrintAction.None, e.Type)
            'Sandeep [ 10 FEB 2011 ] -- End 
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objAssessmentList.setOrderBy(0)
            txtOrderBy.Text = objAssessmentList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub cboAssessedGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessedGroup.SelectedIndexChanged
        Try
            If cboAssessedGroup.SelectedValue > 0 Then
                Dim ObjAssessItem As New clsassess_item_master
                Dim dsCombos As New DataSet
                dsCombos = ObjAssessItem.getListForCombo("List", True, CInt(cboAssessedGroup.SelectedValue))
                With cboAssessedItem
                    .ValueMember = "assessitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessedGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblAssessItem.Text = Language._Object.getCaption(Me.lblAssessItem.Name, Me.lblAssessItem.Text)
			Me.lblAssessGroup.Text = Language._Object.getCaption(Me.lblAssessGroup.Name, Me.lblAssessGroup.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
			Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkUncommited.Text = Language._Object.getCaption(Me.chkUncommited.Name, Me.chkUncommited.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
