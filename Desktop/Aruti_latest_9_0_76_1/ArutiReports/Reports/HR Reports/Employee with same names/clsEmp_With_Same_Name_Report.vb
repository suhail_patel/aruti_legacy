#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsEmp_With_Same_Name_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmp_With_Same_Name_Report"
    Private mstrReportId As String = enArutiReport.Employee_WithSameName    '98
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mblnIsWithFirstName As Boolean = False
    Private mblnIsWithSurName As Boolean = False
    Private mblnIsWithOtherName As Boolean = False
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrFirstNameCaption As String = String.Empty
    Private mstrSurNameCaption As String = String.Empty
    Private mstrOtherNameCaption As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _IsWithFirstName() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWithFirstName = value
        End Set
    End Property

    Public WriteOnly Property _IsWithSurName() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWithSurName = value
        End Set
    End Property

    Public WriteOnly Property _IsWithOtherName() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWithOtherName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNameCaption() As String
        Set(ByVal value As String)
            mstrFirstNameCaption = value
        End Set
    End Property

    Public WriteOnly Property _SurNameCaption() As String
        Set(ByVal value As String)
            mstrSurNameCaption = value
        End Set
    End Property

    Public WriteOnly Property _OtherNameCaption() As String
        Set(ByVal value As String)
            mstrOtherNameCaption = value
        End Set
    End Property

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mblnIsWithFirstName = False
            mblnIsWithSurName = False
            mblnIsWithOtherName = False
            mblnIncludeInactiveEmp = False
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            Dim StrMessage As String = String.Empty

            If mblnIsWithFirstName = True Then
                If StrMessage.Trim.Length <= 0 Then
                    StrMessage = Language.getMessage(mstrModuleName, 1, "Similarity Based On : ") & " " & mstrFirstNameCaption & " "
                Else
                    StrMessage &= " " & mstrFirstNameCaption & " "
                End If
            End If

            If mblnIsWithSurName = True Then
                If StrMessage.Trim.Length <= 0 Then
                    StrMessage = Language.getMessage(mstrModuleName, 1, "Similarity Based On : ") & " " & mstrSurNameCaption & " "
                Else
                    StrMessage &= " , " & mstrSurNameCaption & " "
                End If
            End If

            If mblnIsWithOtherName = True Then
                If StrMessage.Trim.Length <= 0 Then
                    StrMessage = Language.getMessage(mstrModuleName, 1, "Similarity Based On : ") & " " & mstrOtherNameCaption & " "
                Else
                    StrMessage &= " , " & mstrOtherNameCaption & " "
                End If
            End If

            Me._FilterTitle &= StrMessage

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')", Language.getMessage(mstrModuleName, 3, "Firstname")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Surname")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(othername,'')", Language.getMessage(mstrModuleName, 7, "Othername")))
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 8, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("appointeddate", Language.getMessage(mstrModuleName, 5, "Appointed Date")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(jm.job_name,'')", Language.getMessage(mstrModuleName, 6, "Job Title")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim StrQuery As String = String.Empty
        Dim StrJoin As String = String.Empty
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "	 employeecode AS ECode " & _
                   "	,ISNULL(firstname,'') AS FName " & _
                   "	,ISNULL(surname,'') AS SName " & _
                   "	,ISNULL(othername,'') AS OName " & _
                   "	,CONVERT(CHAR(8),appointeddate,112) AS ADate " & _
                   "    ,ISNULL(jm.job_name,'') AS Job_Title " & _
                   "    ,ISNULL(dm.name,'') AS Department " & _
                   "    ,appointeddate AS appointeddate "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " JOIN " & _
                    "( " & _
                    "   SELECT "
            If mblnIsWithFirstName = True Then
                If StrQuery.Trim.Length <= 0 Then
                    StrQuery &= " LTRIM(RTRIM(firstname)) fName "
                End If
            End If

            If mblnIsWithSurName = True Then
                If StrQuery.Trim.Length <= 0 Then
                    StrQuery &= " LTRIM(RTRIM(surname)) sName "
                Else
                    StrQuery &= " ,LTRIM(RTRIM(surname)) sName "
                End If
            End If

            If mblnIsWithOtherName = True Then
                If StrQuery.Trim.Length <= 0 Then
                    StrQuery &= " LTRIM(RTRIM(othername)) oName "
                Else
                    StrQuery &= " ,LTRIM(RTRIM(othername)) oName "
                End If
            End If


            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= StrQuery & _
            '        ", COUNT(employeecode) AS cnt " & _
            '        " FROM hremployee_master GROUP BY "

            StrQ &= StrQuery & _
                    ", COUNT(employeecode) AS cnt " & _
                    " FROM hremployee_master WHERE 1 =1 "


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'Shani(24-Aug-2015) -- End

            StrQ &= " GROUP BY "
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            StrQuery = ""

            If mblnIsWithFirstName = True Then
                If StrQuery.Trim.Length <= 0 Then
                    StrQuery &= " LTRIM(RTRIM(firstname)) "
                End If
            End If

            If mblnIsWithSurName = True Then
                If StrQuery.Trim.Length <= 0 Then
                    StrQuery &= " LTRIM(RTRIM(surname)) "
                Else
                    StrQuery &= " ,LTRIM(RTRIM(surname)) "
                End If
            End If

            If mblnIsWithOtherName = True Then
                If StrQuery.Trim.Length <= 0 Then
                    StrQuery &= " LTRIM(RTRIM(othername)) "
                Else
                    StrQuery &= " ,LTRIM(RTRIM(othername)) "
                End If
            End If
            StrQ &= StrQuery & _
                    "HAVING COUNT(employeecode) > 1 " & _
            ") AS B ON "

            If mblnIsWithFirstName = True Then
                If StrJoin.Trim.Length <= 0 Then
                    StrJoin &= " B.fName = LTRIM(RTRIM(hremployee_master.firstname)) "
                End If
            End If

            If mblnIsWithSurName = True Then
                If StrJoin.Trim.Length <= 0 Then
                    StrJoin &= " B.sName = LTRIM(RTRIM(hremployee_master.surname)) "
                Else
                    StrJoin &= " AND B.sName = LTRIM(RTRIM(hremployee_master.surname)) "
                End If
            End If

            If mblnIsWithOtherName = True Then
                If StrJoin.Trim.Length <= 0 Then
                    StrJoin &= " B.oName = LTRIM(RTRIM(hremployee_master.othername)) "
                Else
                    StrJoin &= " AND B.oName = LTRIM(RTRIM(hremployee_master.othername)) "
                End If
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= StrJoin & _
            '        " LEFT JOIN hrjob_master jm ON hremployee_master.jobunkid = jm.jobunkid " & _
            '        " LEFT JOIN hrdepartment_master dm ON hremployee_master.departmentunkid = dm.departmentunkid "


            'StrQ &= mstrAnalysis_Join

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If


            StrQ &= StrJoin

            StrQ &= "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    " LEFT JOIN hrjob_master jm ON Jobs.jobunkid = jm.jobunkid " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    " LEFT JOIN hrdepartment_master dm ON Alloc.departmentunkid = dm.departmentunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= " WHERE 1=1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("ECode")
                rpt_Rows.Item("Column3") = dtRow.Item("FName")
                rpt_Rows.Item("Column4") = dtRow.Item("SName")
                rpt_Rows.Item("Column5") = dtRow.Item("OName")
                rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("ADate").ToString).ToShortDateString
                rpt_Rows.Item("Column7") = dtRow.Item("Job_Title")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmp_With_Same_Name_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 9, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 10, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 11, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 12, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 8, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 3, "Firstname"))
            Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 4, "Surname"))
            Call ReportFunction.TextChange(objRpt, "txtOtherName", Language.getMessage(mstrModuleName, 7, "Othername"))
            Call ReportFunction.TextChange(objRpt, "txtAppDate", Language.getMessage(mstrModuleName, 5, "Appointed Date"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 6, "Job Title"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 13, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 14, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Similarity Based On :")
            Language.setMessage(mstrModuleName, 2, " Order By :")
            Language.setMessage(mstrModuleName, 3, "Firstname")
            Language.setMessage(mstrModuleName, 4, "Surname")
            Language.setMessage(mstrModuleName, 5, "Appointed Date")
            Language.setMessage(mstrModuleName, 6, "Job Title")
            Language.setMessage(mstrModuleName, 7, "Othername")
            Language.setMessage(mstrModuleName, 8, "Employee Code")
            Language.setMessage(mstrModuleName, 9, "Prepared By :")
            Language.setMessage(mstrModuleName, 10, "Checked By :")
            Language.setMessage(mstrModuleName, 11, "Approved By :")
            Language.setMessage(mstrModuleName, 12, "Received By :")
            Language.setMessage(mstrModuleName, 13, "Sub Total :")
            Language.setMessage(mstrModuleName, 14, "Grand Total :")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
