'************************************************************************************************************************************
'Class Name : frmAssessmentSoreRatingReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

'S.SANDEEP |11-SEP-2019| -- START
'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}

Public Class frmAssessmentSoreRatingReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentSoreRatingReport"
    Private objScoreRating As clsAssessSoreRatingReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        objScoreRating = New clsAssessSoreRatingReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objScoreRating.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Try

            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 13, "Based On Single Period"))
                .Items.Add(Language.getMessage(mstrModuleName, 14, "Based On Multiple Period"))
                .SelectedIndex = 0
            End With

            With cboSubReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Based on Employee"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Based on Allocation"))
                If Company._Object._Code = "FZ" Or Company._Object._Code = "FZT" Or Company._Object._Code = "FZM" Then
                    .Items.Add(Language.getMessage(mstrModuleName, 12, "Based on Competencies"))
                End If
                .SelectedIndex = 0
            End With

            Dim objMData As New clsMasterData
            dsCombos = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With

            dsCombos = objMaster.GetCondition(True, True, False, False, False)
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            Dim objCScoreMaster As New clsComputeScore_master
            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            End With
            objCScoreMaster = Nothing

            dsCombos = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            With cboFiancialYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub FillPeriod(ByVal intYearId As Integer)
        Try
            If intYearId > 0 Then
                Dim dsList As New DataSet
                Dim ObjPeriod As New clscommom_period_Tran
                dsList = ObjPeriod.getListForCombo(enModuleReference.Assessment, intYearId, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False, 0, False)
                ObjPeriod = Nothing
                Dim xCol As New DataColumn
                With xCol
                    .DataType = GetType(System.Boolean)
                    .ColumnName = "icheck"
                    .DefaultValue = False
                End With
                dgvPeriod.AutoGenerateColumns = False
                dgcolhPeriod.DataPropertyName = "name"
                objdgcolhCheck.DataPropertyName = "icheck"
                objdgcolhPId.DataPropertyName = "periodunkid"
                dgvPeriod.DataSource = dsList.Tables(0)
            Else
                dgvPeriod.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            'S.SANDEEP [ 02 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            radWA_All.Checked = True
            'S.SANDEEP [ 28 APRIL 2012 ] -- END

            'S.SANDEEP [ 10 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objScoreRating.setDefaultOrderBy(1)
            txtOrderBy.Text = objScoreRating.OrderByDisplay
            'S.SANDEEP [ 10 SEPT 2013 ] -- END

            'S.SANDEEP [ 28 FEB 2014 ] -- START
            chkOption.Checked = False
            Call chkOption_CheckedChanged(New Object, New EventArgs)
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 28 FEB 2014 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objScoreRating.SetDefaultValue()

            If cboSubReportType.SelectedIndex = 0 AndAlso cboReportType.SelectedIndex = 0 Then
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            End If

            If cboSubReportType.SelectedIndex = 0 Then
                If cboEmployee.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select employee to continue."), enMsgBoxStyle.Information)
                    cboEmployee.Focus()
                    Return False
                End If
            End If
            objScoreRating._EmployeeId = cboEmployee.SelectedValue
            objScoreRating._EmployeeName = cboEmployee.Text

            If cboSubReportType.SelectedIndex > 0 Then
                If lvAllocation.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select atleast one allocation to export report."), enMsgBoxStyle.Information)
                    lvAllocation.Focus()
                    Return False
                End If
                If chkOption.Checked = True Then
                    If CInt(cboCondition.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, condition is mandatory information."), enMsgBoxStyle.Information)
                        cboCondition.Focus()
                        Return False
                    End If
                End If
                If lvDisplayCol.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select atleast one column to display in report."), enMsgBoxStyle.Information)
                    lvDisplayCol.Focus()
                    Return False
                End If
            End If

            If cboReportType.SelectedIndex = 1 Then
                If dgvPeriod.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells(objdgcolhCheck.Index).Value = True).Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please check atleast one period from the list in order to generate report."), enMsgBoxStyle.Information)
                    Return False
                End If
                objScoreRating._DicPeriodChecked = dgvPeriod.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells(objdgcolhCheck.Index).Value = True).ToDictionary(Function(x) CInt(x.Cells(objdgcolhPId.Index).Value), Function(y) y.Cells(dgcolhPeriod.Index).Value.ToString())
            End If

            objScoreRating._ReportType_Id = cboSubReportType.SelectedIndex
            objScoreRating._ReportType_Name = cboSubReportType.Text

            If cboSubReportType.SelectedIndex = 1 Or _
               cboSubReportType.SelectedIndex = 2 Then
                If chkConsiderEmployeeTermination.Checked = True Then
                objScoreRating._StartDate = mdtStartDate
                objScoreRating._EndDate = mdtEndDate
                Else
                    objScoreRating._StartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString).Date
                    objScoreRating._EndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString).Date
                End If

                Dim StrStringIds As String = ""
                If lvAllocation.CheckedItems.Count > 0 Then
                    For Each lvItem As ListViewItem In lvAllocation.CheckedItems
                        StrStringIds &= "," & lvItem.Tag.ToString
                    Next
                    If StrStringIds.Length > 0 Then StrStringIds = Mid(StrStringIds, 2)
                End If
                objScoreRating._Allocation = cboAllocations.Text

                If StrStringIds.Length > 0 Then
                    Select Case cboAllocations.SelectedValue
                        Case enAllocation.BRANCH
                            StrStringIds = "Alloc.stationunkid IN(" & StrStringIds & ") "
                        Case enAllocation.DEPARTMENT_GROUP
                            StrStringIds = "Alloc.deptgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.DEPARTMENT
                            StrStringIds = "Alloc.departmentunkid IN(" & StrStringIds & ") "
                        Case enAllocation.SECTION_GROUP
                            StrStringIds = "Alloc.sectiongroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.SECTION
                            StrStringIds = "Alloc.sectionunkid IN(" & StrStringIds & ") "
                        Case enAllocation.UNIT_GROUP
                            StrStringIds = "Alloc.unitgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.UNIT
                            StrStringIds = "Alloc.unitunkid IN(" & StrStringIds & ") "
                        Case enAllocation.TEAM
                            StrStringIds = "Alloc.teamunkid IN(" & StrStringIds & ") "
                        Case enAllocation.JOB_GROUP
                            StrStringIds = "Jobs.jobgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.JOBS
                            StrStringIds = "Jobs.jobunkid IN(" & StrStringIds & ") "
                        Case enAllocation.CLASS_GROUP
                            StrStringIds = "Alloc.classgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.CLASSES
                            StrStringIds = "Alloc.classunkid IN(" & StrStringIds & ") "
                    End Select
                End If
                objScoreRating._AllocationIds = StrStringIds
            End If

            objScoreRating._PeriodId = cboPeriod.SelectedValue
            objScoreRating._PeriodName = cboPeriod.Text
            objScoreRating._OnProbationSelected = chkOption.Checked
            objScoreRating._Caption = chkOption.Text
            objScoreRating._Condition = cboCondition.Text
            If dtpAppDate.Enabled = True Then
                objScoreRating._AppointmentDate = dtpAppDate.Value
            End If
            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            For Each lCheck As ListViewItem In lvDisplayCol.CheckedItems
                iCols &= lCheck.SubItems(objcolhSelectCol.Index).Text & vbCrLf
                iJoins &= lCheck.SubItems(objcolhJoin.Index).Text & vbCrLf
                iDisplay &= lCheck.SubItems(objcolhDisplay.Index).Text & vbCrLf
            Next
            objScoreRating._SelectedCols = iCols
            objScoreRating._SelectedJoin = iJoins
            objScoreRating._DisplayCols = iDisplay
            objScoreRating._ScoringOptionId = ConfigParameter._Object._ScoringOptionId
            objScoreRating._AdvanceFilter = mstrAdvanceFilter
            objScoreRating._SelfAssignCompetencies = ConfigParameter._Object._Self_Assign_Competencies
            objScoreRating._ShowScaleOnReport = chkShowEmpSalary.Checked
            objScoreRating._IsCalibrationSettingActive = ConfigParameter._Object._IsCalibrationSettingActive
            objScoreRating._DisplayScoreName = cboScoreOption.Text
            objScoreRating._DisplayScoreType = cboScoreOption.SelectedValue

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count >= 5 Then
                colhAllocations.Width = 250 - 20
                objchkAll.Location = New Point(299, 62)
            Else
                colhAllocations.Width = 250
                objchkAll.Location = New Point(319, 62)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 8, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 9, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                'dRow = dsList.Tables(0).NewRow
                'dRow.Item("Id") = 903
                'dRow.Item("Name") = Language.getMessage(mstrModuleName, 6, "Cost Center")
                'dsList.Tables(0).Rows.Add(dRow)

                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("Name").ToString
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            lvItem.SubItems.Add(",ISNULL(estm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrstation_master AS estm ON estm.stationunkid = Alloc.stationunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.DEPARTMENT_GROUP
                            lvItem.SubItems.Add(",ISNULL(edgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrdepartment_group_master AS edgm ON edgm.deptgroupunkid = Alloc.deptgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.DEPARTMENT
                            lvItem.SubItems.Add(",ISNULL(edpt.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrdepartment_master AS edpt ON edpt.departmentunkid = Alloc.departmentunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.SECTION_GROUP
                            lvItem.SubItems.Add(",ISNULL(esgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrsectiongroup_master AS esgm ON esgm.sectiongroupunkid = Alloc.sectiongroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.SECTION
                            lvItem.SubItems.Add(",ISNULL(esec.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrsection_master AS esec ON esec.sectionunkid = Alloc.sectionunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.UNIT_GROUP
                            lvItem.SubItems.Add(",ISNULL(eugm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrunitgroup_master AS eugm ON eugm.unitgroupunkid = Alloc.unitgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.UNIT
                            lvItem.SubItems.Add(",ISNULL(eutm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrunit_master AS eutm ON eutm.unitunkid = Alloc.unitunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.TEAM
                            lvItem.SubItems.Add(",ISNULL(etem.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrteam_master AS etem ON etem.teamunkid = Alloc.teamunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.JOB_GROUP
                            lvItem.SubItems.Add(",ISNULL(ejgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrjobgroup_master AS ejgm ON ejgm.jobgroupunkid = Jobs.jobgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.JOBS
                            lvItem.SubItems.Add(",ISNULL(ejbm.job_name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrjob_master AS ejbm ON ejbm.jobunkid = Jobs.jobunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.CLASS_GROUP
                            lvItem.SubItems.Add(",ISNULL(ecgm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrclassgroup_master AS ecgm ON ecgm.classgroupunkid = Alloc.classgroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.CLASSES
                            lvItem.SubItems.Add(",ISNULL(ecls.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrclasses_master AS ecls ON ecls.classesunkid = Alloc.classunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 900
                            lvItem.SubItems.Add(",ISNULL(eggm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("JOIN hrgradegroup_master eggm ON grds.gradegroupunkid = eggm.gradegroupunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 901
                            lvItem.SubItems.Add(",ISNULL(egdm.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrgrade_master egdm ON grds.gradeunkid = egdm.gradeunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case 902
                            lvItem.SubItems.Add(",ISNULL(egdl.name,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN hrgradelevel_master egdl ON grds.gradelevelunkid = egdl.gradelevelunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                        Case enAllocation.COST_CENTER
                            lvItem.SubItems.Add(",ISNULL(ecct.costcentername,'') AS [" & lvItem.Text & "]")
                            lvItem.SubItems.Add("LEFT JOIN prcostcenter_master AS ecct ON ecct.costcenterunkid = CC.costcenterunkid")
                            lvItem.SubItems.Add(",[" & lvItem.Text & "]")
                    End Select

                    lvDisplayCol.Items.Add(lvItem)
                Next

                If lvDisplayCol.Items.Count > 12 Then
                    colhDisplayName.Width = 245 - 18
                Else
                    colhDisplayName.Width = 245
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmAssessmentSoreRatingReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objScoreRating = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAssessmentSoreRatingReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentSoreRatingReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.EZeeHeader1.Title = objScoreRating._ReportName
            Me.EZeeHeader1.Message = objScoreRating._ReportDesc

            Call FillCombo()

            Call ResetValue()

            If ConfigParameter._Object._IsCalibrationSettingActive Then
                plnScoreType.Visible = True
            Else
                plnScoreType.Visible = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentSoreRatingReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Select Case cboSubReportType.SelectedIndex
                Case 0  'EMPLOYEE BASED 
                    Dim strFileName As String = String.Empty
                    If dgvPeriod.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells(objdgcolhCheck.Index).Value = True).Count <= 0 Then
                        objScoreRating.Export_Assessment_Report(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, User._Object._Userunkid, Company._Object._Companyunkid, strFileName)
                    Else
                        objScoreRating.Export_Assessment_Report_MultiPeriod(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, User._Object._Userunkid, Company._Object._Companyunkid, strFileName)
                    End If
                Case 1  'ALLOCATION BASED
                    If dgvPeriod.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells(objdgcolhCheck.Index).Value = True).Count <= 0 Then
                    objScoreRating.Report_Allocation_Based_New(FinancialYear._Object._DatabaseName, _
                                                               User._Object._Userunkid, _
                                                               FinancialYear._Object._YearUnkid, _
                                                               Company._Object._Companyunkid, _
                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                   ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._IsCalibrationSettingActive)
                    Else
                        objScoreRating.Report_Allocation_Based_MultiPeriod(FinancialYear._Object._DatabaseName, _
                                                                           User._Object._Userunkid, _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           Company._Object._Companyunkid, _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._IsCalibrationSettingActive)
                    End If
                Case 2
                    objScoreRating.Report_Based_On_Competencies(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessSoreRatingReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessSoreRatingReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END


    'S.SANDEEP [ 28 FEB 2014 ] -- START
    Private Sub btnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 28 FEB 2014 ] -- END

#End Region

#Region " Controls Events "

    Private Sub cboSubReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSubReportType.SelectedIndexChanged
        Try
            Select Case cboSubReportType.SelectedIndex
                Case 0
                    pnlAllocation.Enabled = False
                    txtSearch.Text = ""
                    objchkAll.Checked = False
                    cboEmployee.Enabled = True : objbtnSearchEmployee.Enabled = True
                    txtOrderBy.Enabled = False : objbtnSort.Enabled = False
                    pnlCondition.Enabled = False
                    lvDisplayCol.Items.Clear()
                    btnAdvanceFilter.Visible = False
                    mstrAdvanceFilter = ""
                    chkConsiderEmployeeTermination.Enabled = False
                    chkConsiderEmployeeTermination.Checked = False
                    chkShowEmpSalary.Enabled = False
                    chkShowEmpSalary.Checked = False
                    If cboReportType.SelectedIndex = 0 Then objsp1.Enabled = False
                    If cboReportType.SelectedIndex = 0 Then cboPeriod.Enabled = True

                Case 1, 2
                    cboEmployee.SelectedValue = 0
                    cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                    pnlAllocation.Enabled = True
                    txtOrderBy.Enabled = True : objbtnSort.Enabled = True
                    pnlCondition.Enabled = True
                    Call Fill_Column_List()
                    btnAdvanceFilter.Visible = True
                    chkConsiderEmployeeTermination.Enabled = True
                    chkConsiderEmployeeTermination.Checked = False
                    chkShowEmpSalary.Enabled = True
                    chkShowEmpSalary.Checked = False
                    If cboReportType.SelectedIndex = 1 Then
                        cboPeriod.SelectedValue = 0
                        cboPeriod.Enabled = False
                    End If
                    If cboSubReportType.SelectedIndex <> 1 Then
                        cboReportType.SelectedIndex = 0
                        objsp1.Enabled = False
                    Else
                        If cboReportType.SelectedIndex > 0 Then
                            objsp1.Enabled = True
                        Else
                            objsp1.Enabled = False
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSubReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex <= 0 Then
                cboFiancialYear.SelectedValue = 0
                objsp1.Enabled = False
                If cboSubReportType.Items.Count > 0 Then cboSubReportType.SelectedIndex = 0
                If cboReportType.SelectedIndex = 0 Then cboPeriod.Enabled = True
            Else
                objsp1.Enabled = True
                cboPeriod.SelectedValue = 0
                cboPeriod.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
                Dim ObjEmp As New clsEmployee_Master
                Dim dsCombos As New DataSet

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = ObjEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , mdtStartDate, mdtEndDate)
                'Else
                '    dsCombos = ObjEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                'End If

                'S.SANDEEP [27 Jan 2016] -- START
                'dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                '                                  User._Object._Userunkid, _
                '                                  FinancialYear._Object._YearUnkid, _
                '                                  Company._Object._Companyunkid, _
                '                                  mdtStartDate, _
                '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                  mdtEndDate, _
                '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

                dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  mdtStartDate, mdtEndDate, _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

                'Shani (26-Sep-2016) -Replace- [ConfigParameter._Object._IncludeCustomItemInPlanning] --> [ConfigParameter._Object._IsIncludeInactiveEmp]


                'S.SANDEEP [27 Jan 2016] -- END

                'S.SANDEEP [04 JUN 2015] -- END

                Dim dTable As DataTable
                If ConfigParameter._Object._ExcludeProbatedEmpOnPerformance = True Then
                    Dim sId As String = objScoreRating.Get_Probated_EmpIds(mdtEndDate)
                    If sId.Trim.Length > 0 Then
                        dTable = New DataView(dsCombos.Tables(0), "employeeunkid NOT IN(" & sId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dTable = New DataView(dsCombos.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                    End If
                Else
                    dTable = New DataView(dsCombos.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If

                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dTable
                End With
                ObjEmp = Nothing : dsCombos.Dispose()
            Else
                Dim ObjEmp As New clsEmployee_Master
                Dim dsCombos As New DataSet

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = ObjEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
                'Else
                '    dsCombos = ObjEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                'End If

                dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
                'S.SANDEEP [04 JUN 2015] -- END

                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsCombos.Tables("List")
                End With

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objScoreRating.setOrderBy(1)
            txtOrderBy.Text = objScoreRating.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub chkOption_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOption.CheckedChanged
        Try
            cboCondition.Enabled = chkOption.Checked
            dtpAppDate.Enabled = chkOption.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkOption_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkdisplay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkdisplay.CheckedChanged
        Try
            For Each lvitem As ListViewItem In lvDisplayCol.Items
                lvitem.Checked = objchkdisplay.Checked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkdisplay_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFiancialYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFiancialYear.SelectedIndexChanged
        Try
            Call FillPeriod(cboFiancialYear.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFiancialYear_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkUncommited.Text = Language._Object.getCaption(Me.chkUncommited.Name, Me.chkUncommited.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.radWA_AssessReviewer.Text = Language._Object.getCaption(Me.radWA_AssessReviewer.Name, Me.radWA_AssessReviewer.Text)
            Me.radWA_Reviewer.Text = Language._Object.getCaption(Me.radWA_Reviewer.Name, Me.radWA_Reviewer.Text)
            Me.radWA_Assessor.Text = Language._Object.getCaption(Me.radWA_Assessor.Name, Me.radWA_Assessor.Text)
            Me.radWA_All.Text = Language._Object.getCaption(Me.radWA_All.Name, Me.radWA_All.Text)
            Me.lblRType.Text = Language._Object.getCaption(Me.lblRType.Name, Me.lblRType.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.chkOption.Text = Language._Object.getCaption(Me.chkOption.Name, Me.chkOption.Text)
            Me.lblOverallCondition.Text = Language._Object.getCaption(Me.lblOverallCondition.Name, Me.lblOverallCondition.Text)
            Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
            Me.colhDisplayName.Text = Language._Object.getCaption(CStr(Me.colhDisplayName.Tag), Me.colhDisplayName.Text)
            Me.chkConsiderEmployeeTermination.Text = Language._Object.getCaption(Me.chkConsiderEmployeeTermination.Name, Me.chkConsiderEmployeeTermination.Text)
            Me.chkShowEmpSalary.Text = Language._Object.getCaption(Me.chkShowEmpSalary.Name, Me.chkShowEmpSalary.Text)
            Me.lblDisplayScore.Text = Language._Object.getCaption(Me.lblDisplayScore.Name, Me.lblDisplayScore.Text)
            Me.lblSubReportType.Text = Language._Object.getCaption(Me.lblSubReportType.Name, Me.lblSubReportType.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
            Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is mandatory information. Please select employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 3, "Based on Employee")
            Language.setMessage(mstrModuleName, 4, "Based on Allocation")
            Language.setMessage(mstrModuleName, 5, "Grade Level")
            Language.setMessage(mstrModuleName, 7, "Sorry, condition is mandatory information.")
            Language.setMessage(mstrModuleName, 8, "Grade Group")
            Language.setMessage(mstrModuleName, 9, "Grade")
            Language.setMessage(mstrModuleName, 10, "Please select atleast one allocation to export report.")
            Language.setMessage(mstrModuleName, 11, "Please select atleast one column to display in report.")
            Language.setMessage(mstrModuleName, 12, "Based on Competencies")
            Language.setMessage(mstrModuleName, 13, "Based On Single Period")
            Language.setMessage(mstrModuleName, 14, "Based On Multiple Period")
            Language.setMessage(mstrModuleName, 15, "Please check atleast one period from the list in order to generate report.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
'Public Class frmAssessmentSoreRatingReport

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmAssessmentSoreRatingReport"
'    Private objScoreRating As clsAssessSoreRatingReport
'    'S.SANDEEP [ 14 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mdtStartDate As DateTime = Nothing
'    Private mdtEndDate As DateTime = Nothing
'    'S.SANDEEP [ 14 AUG 2013 ] -- END

'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    Private mstrAdvanceFilter As String = String.Empty
'    'S.SANDEEP [ 28 FEB 2014 ] -- END

'#End Region

'#Region " Contructor "

'    Public Sub New()
'        objScoreRating = New clsAssessSoreRatingReport(User._Object._Languageunkid,Company._Object._Companyunkid)
'        objScoreRating.SetDefaultValue()
'        InitializeComponent()
'    End Sub

'#End Region

'#Region " Private Function "

'    Private Sub FillCombo()
'        Dim ObjEmp As New clsEmployee_Master
'        Dim ObjPeriod As New clscommom_period_Tran
'        Dim dsCombos As New DataSet
'        Try

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'            ''S.SANDEEP [ 15 MAY 2012 ] -- START
'            ''ENHANCEMENT : TRA CHANGES
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    dsCombos = ObjEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
'            'Else
'            '    dsCombos = ObjEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'            'End If
'            ''S.SANDEEP [ 15 MAY 2012 ] -- END

'            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                            User._Object._Userunkid, _
'                                            FinancialYear._Object._YearUnkid, _
'                                            Company._Object._Companyunkid, _
'                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                            ConfigParameter._Object._UserAccessModeSetting, _
'                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsCombos.Tables("List")
'            End With

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True)
'            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
'            'Sohail (21 Aug 2015) -- End
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("List")
'            End With

'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            With cboReportType
'                .Items.Clear()
'                .Items.Add(Language.getMessage(mstrModuleName, 3, "Based on Employee"))
'                .Items.Add(Language.getMessage(mstrModuleName, 4, "Based on Allocation"))
'                'S.SANDEEP [16 SEP 2016] -- START
'                'ENHANCEMENT : NEW REPORT TYPE ONLY FOR FINCA
'                If Company._Object._Code = "FZ" Or Company._Object._Code = "FZT" Or Company._Object._Code = "FZM" Then
'                    .Items.Add(Language.getMessage(mstrModuleName, 12, "Based on Competencies"))
'                End If
'                'S.SANDEEP [16 SEP 2016] -- END
'                .SelectedIndex = 0
'            End With

'            Dim objMData As New clsMasterData
'            dsCombos = objMData.GetEAllocation_Notification("List")
'            Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ")", "", DataViewRowState.CurrentRows).ToTable
'            With cboAllocations
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dtTable
'                .SelectedValue = 1
'            End With
'            'S.SANDEEP [ 14 AUG 2013 ] -- END

'            'S.SANDEEP [ 28 FEB 2014 ] -- START
'            Dim objMaster As New clsMasterData
'            'Nilay (10-Nov-2016) -- Start
'            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
'            'dsCombos = objMaster.GetCondition(True)
'            dsCombos = objMaster.GetCondition(True, True, False, False, False)
'            'Nilay (10-Nov-2016) -- End
'            With cboCondition
'                .ValueMember = "id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombos.Tables(0)
'                .SelectedValue = 0
'            End With
'            objMaster = Nothing
'            'S.SANDEEP [ 28 FEB 2014 ] -- END

'            'S.SANDEEP |27-MAY-2019| -- START
'            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
'            Dim objCScoreMaster As New clsComputeScore_master
'            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
'            With cboScoreOption
'                .ValueMember = "Id"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("List")
'                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
'            End With
'            objCScoreMaster = Nothing
'            'S.SANDEEP |27-MAY-2019| -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
'        End Try
'    End Sub

'    Private Sub ResetValue()
'        Try
'            cboEmployee.SelectedValue = 0
'            cboPeriod.SelectedValue = 0
'            'S.SANDEEP [ 02 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            radWA_All.Checked = True
'            'S.SANDEEP [ 28 APRIL 2012 ] -- END

'            'S.SANDEEP [ 10 SEPT 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objScoreRating.setDefaultOrderBy(1)
'            txtOrderBy.Text = objScoreRating.OrderByDisplay
'            'S.SANDEEP [ 10 SEPT 2013 ] -- END

'            'S.SANDEEP [ 28 FEB 2014 ] -- START
'            chkOption.Checked = False
'            Call chkOption_CheckedChanged(New Object, New EventArgs)
'            mstrAdvanceFilter = ""
'            'S.SANDEEP [ 28 FEB 2014 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Function SetFilter() As Boolean
'        Try
'            objScoreRating.SetDefaultValue()

'            If cboPeriod.SelectedValue <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
'                cboPeriod.Focus()
'                Return False
'            End If

'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If cboReportType.SelectedIndex = 0 Then
'                If cboEmployee.SelectedValue <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select employee to continue."), enMsgBoxStyle.Information)
'                    cboEmployee.Focus()
'                    Return False
'                End If
'            End If
'            objScoreRating._EmployeeId = cboEmployee.SelectedValue
'            objScoreRating._EmployeeName = cboEmployee.Text
'            'S.SANDEEP [ 14 AUG 2013 ] -- END

'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES

'            'S.SANDEEP [ 10 SEPT 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If lvAllocation.CheckedItems.Count <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one allocation to export report."), enMsgBoxStyle.Information)
'            '    lvAllocation.Focus()
'            '    Return False
'            'End If
'            If cboReportType.SelectedIndex > 0 Then
'                If lvAllocation.CheckedItems.Count <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select atleast one allocation to export report."), enMsgBoxStyle.Information)
'                    lvAllocation.Focus()
'                    Return False
'                End If
'                'S.SANDEEP [ 28 FEB 2014 ] -- START
'                If chkOption.Checked = True Then
'                    If CInt(cboCondition.SelectedValue) <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, condition is mandatory information."), enMsgBoxStyle.Information)
'                        cboCondition.Focus()
'                        Return False
'                    End If
'                End If
'                If lvDisplayCol.CheckedItems.Count <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select atleast one column to display in report."), enMsgBoxStyle.Information)
'                    lvDisplayCol.Focus()
'                    Return False
'                End If
'                'S.SANDEEP [ 28 FEB 2014 ] -- END
'            End If
'            'S.SANDEEP [ 10 SEPT 2013 ] -- END


'            objScoreRating._ReportType_Id = cboReportType.SelectedIndex
'            objScoreRating._ReportType_Name = cboReportType.Text

'            'S.SANDEEP [16 SEP 2016] -- START
'            'If cboReportType.SelectedIndex = 1 Then
'            If cboReportType.SelectedIndex = 1 Or _
'               cboReportType.SelectedIndex = 2 Then
'                'S.SANDEEP [16 SEP 2016] -- END

'                'S.SANDEEP [16 DEC 2016] -- START
'                'objScoreRating._StartDate = mdtStartDate
'                'objScoreRating._EndDate = mdtEndDate
'                If chkConsiderEmployeeTermination.Checked = True Then
'                    objScoreRating._StartDate = mdtStartDate
'                    objScoreRating._EndDate = mdtEndDate
'                Else
'                    objScoreRating._StartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString).Date
'                    objScoreRating._EndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString).Date
'                End If

'                'S.SANDEEP [16 DEC 2016] -- END


'                Dim StrStringIds As String = ""
'                If lvAllocation.CheckedItems.Count > 0 Then
'                    For Each lvItem As ListViewItem In lvAllocation.CheckedItems
'                        StrStringIds &= "," & lvItem.Tag.ToString
'                    Next
'                    If StrStringIds.Length > 0 Then StrStringIds = Mid(StrStringIds, 2)
'                End If
'                objScoreRating._Allocation = cboAllocations.Text

'                If StrStringIds.Length > 0 Then
'                    Select Case cboAllocations.SelectedValue
'                        Case enAllocation.BRANCH
'                            StrStringIds = "hremployee_master.stationunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.DEPARTMENT_GROUP
'                            StrStringIds = "hremployee_master.deptgroupunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.DEPARTMENT
'                            StrStringIds = "hremployee_master.departmentunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.SECTION_GROUP
'                            StrStringIds = "hremployee_master.sectiongroupunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.SECTION
'                            StrStringIds = "hremployee_master.sectionunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.UNIT_GROUP
'                            StrStringIds = "hremployee_master.unitgroupunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.UNIT
'                            StrStringIds = "hremployee_master.unitunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.TEAM
'                            StrStringIds = "hremployee_master.teamunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.JOB_GROUP
'                            StrStringIds = "hremployee_master.jobgroupunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.JOBS
'                            StrStringIds = "hremployee_master.jobunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.CLASS_GROUP
'                            StrStringIds = "hremployee_master.classgroupunkid IN(" & StrStringIds & ") "
'                        Case enAllocation.CLASSES
'                            StrStringIds = "hremployee_master.classesunkid IN(" & StrStringIds & ") "
'                    End Select
'                End If
'                objScoreRating._AllocationIds = StrStringIds
'            End If
'            'S.SANDEEP [ 14 AUG 2013 ] -- END


'            objScoreRating._PeriodId = cboPeriod.SelectedValue
'            objScoreRating._PeriodName = cboPeriod.Text

'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If radWA_All.Checked = True Then
'            '    objScoreRating._WeightedAvgBy = clsAssessSoreRatingReport.enWeightedAverage.WEIGHTEDAVG_ALL
'            'ElseIf radWA_Assessor.Checked = True Then
'            '    objScoreRating._WeightedAvgBy = clsAssessSoreRatingReport.enWeightedAverage.WEIGHTEDAVG_ASSESSOR
'            'ElseIf radWA_Reviewer.Checked = True Then
'            '    objScoreRating._WeightedAvgBy = clsAssessSoreRatingReport.enWeightedAverage.WEIGHTEDAVG_REVIEWER
'            'ElseIf radWA_AssessReviewer.Checked = True Then
'            '    objScoreRating._WeightedAvgBy = clsAssessSoreRatingReport.enWeightedAverage.WEIGHTEDAVG_ASSESSOR_REVIEWER
'            'End If
'            'S.SANDEEP [ 14 AUG 2013 ] -- END


'            'S.SANDEEP [ 28 FEB 2014 ] -- START
'            objScoreRating._OnProbationSelected = chkOption.Checked
'            objScoreRating._Caption = chkOption.Text
'            objScoreRating._Condition = cboCondition.Text
'            objScoreRating._AppointmentDate = dtpAppDate.Value
'            Dim iCols, iJoins, iDisplay As String
'            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
'            For Each lCheck As ListViewItem In lvDisplayCol.CheckedItems
'                iCols &= lCheck.SubItems(objcolhSelectCol.Index).Text & vbCrLf
'                iJoins &= lCheck.SubItems(objcolhJoin.Index).Text & vbCrLf
'                iDisplay &= lCheck.SubItems(objcolhDisplay.Index).Text & vbCrLf
'            Next
'            objScoreRating._SelectedCols = iCols
'            objScoreRating._SelectedJoin = iJoins
'            objScoreRating._DisplayCols = iDisplay
'            'S.SANDEEP [ 28 FEB 2014 ] -- END


'            'S.SANDEEP [21 AUG 2015] -- START
'            objScoreRating._ScoringOptionId = ConfigParameter._Object._ScoringOptionId
'            objScoreRating._AdvanceFilter = mstrAdvanceFilter
'            'S.SANDEEP [21 AUG 2015] -- END


'            'S.SANDEEP [08 Jan 2016] -- START
'            objScoreRating._SelfAssignCompetencies = ConfigParameter._Object._Self_Assign_Competencies
'            'S.SANDEEP [08 Jan 2016] -- END

'            'S.SANDEEP [17 DEC 2016] -- START
'            'ENHANCEMENT : SALARY INCLUSION
'            objScoreRating._ShowScaleOnReport = chkShowEmpSalary.Checked
'            'S.SANDEEP [17 DEC 2016] -- END

'            'S.SANDEEP |27-MAY-2019| -- START
'            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
'            objScoreRating._IsCalibrationSettingActive = ConfigParameter._Object._IsCalibrationSettingActive
'            objScoreRating._DisplayScoreName = cboScoreOption.Text
'            objScoreRating._DisplayScoreType = cboScoreOption.SelectedValue
'            'S.SANDEEP |27-MAY-2019| -- END

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    'S.SANDEEP [ 14 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub Fill_Data()
'        Dim dList As New DataSet
'        Try
'            Select Case cboAllocations.SelectedValue
'                Case enAllocation.BRANCH
'                    Dim objBranch As New clsStation
'                    dList = objBranch.GetList("List")
'                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
'                Case enAllocation.DEPARTMENT_GROUP
'                    Dim objDeptGrp As New clsDepartmentGroup
'                    dList = objDeptGrp.GetList("List")
'                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
'                Case enAllocation.DEPARTMENT
'                    Dim objDept As New clsDepartment
'                    dList = objDept.GetList("List")
'                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
'                Case enAllocation.SECTION_GROUP
'                    Dim objSecGrp As New clsSectionGroup
'                    dList = objSecGrp.GetList("List")
'                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
'                Case enAllocation.SECTION
'                    Dim objSec As New clsSections
'                    dList = objSec.GetList("List")
'                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
'                Case enAllocation.UNIT_GROUP
'                    Dim objUnitGrp As New clsUnitGroup
'                    dList = objUnitGrp.GetList("List")
'                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
'                Case enAllocation.UNIT
'                    Dim objUnit As New clsUnits
'                    dList = objUnit.GetList("List")
'                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
'                Case enAllocation.TEAM
'                    Dim objTeam As New clsTeams
'                    dList = objTeam.GetList("List")
'                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
'                Case enAllocation.JOB_GROUP
'                    Dim objJobGrp As New clsJobGroup
'                    dList = objJobGrp.GetList("List")
'                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
'                Case enAllocation.JOBS
'                    Dim objJob As New clsJobs
'                    dList = objJob.GetList("List")
'                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
'                Case enAllocation.CLASS_GROUP
'                    Dim objClsGrp As New clsClassGroup
'                    dList = objClsGrp.GetList("List")
'                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
'                Case enAllocation.CLASSES
'                    Dim objCls As New clsClass
'                    dList = objCls.GetList("List")
'                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
'            End Select
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
'        Try
'            lvAllocation.Items.Clear()
'            For Each dtRow As DataRow In dTable.Rows
'                Dim lvItem As New ListViewItem

'                lvItem.Text = dtRow.Item(StrDisColName).ToString
'                lvItem.Tag = dtRow.Item(StrIdColName)

'                lvAllocation.Items.Add(lvItem)
'            Next
'            If lvAllocation.Items.Count >= 5 Then
'                colhAllocations.Width = 250 - 20
'                objchkAll.Location = New Point(299, 62)
'            Else
'                colhAllocations.Width = 250
'                objchkAll.Location = New Point(319, 62)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Do_Operation(ByVal blnOpt As Boolean)
'        Try
'            For Each LItem As ListViewItem In lvAllocation.Items
'                LItem.Checked = blnOpt
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 14 AUG 2013 ] -- END

'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    Private Sub Fill_Column_List()
'        Dim objMData As New clsMasterData
'        Dim dsList As New DataSet
'        Try
'            dsList = objMData.GetEAllocation_Notification("List")
'            If dsList.Tables(0).Rows.Count > 0 Then
'                Dim dRow As DataRow = dsList.Tables(0).NewRow
'                dRow.Item("Id") = 900
'                dRow.Item("Name") = Language.getMessage(mstrModuleName, 8, "Grade Group")
'                dsList.Tables(0).Rows.Add(dRow)

'                dRow = dsList.Tables(0).NewRow
'                dRow.Item("Id") = 901
'                dRow.Item("Name") = Language.getMessage(mstrModuleName, 9, "Grade")
'                dsList.Tables(0).Rows.Add(dRow)

'                dRow = dsList.Tables(0).NewRow
'                dRow.Item("Id") = 902
'                dRow.Item("Name") = Language.getMessage(mstrModuleName, 5, "Grade Level")
'                dsList.Tables(0).Rows.Add(dRow)

'                'dRow = dsList.Tables(0).NewRow
'                'dRow.Item("Id") = 903
'                'dRow.Item("Name") = Language.getMessage(mstrModuleName, 6, "Cost Center")
'                'dsList.Tables(0).Rows.Add(dRow)

'                Dim lvItem As ListViewItem
'                For Each dtRow As DataRow In dsList.Tables(0).Rows
'                    lvItem = New ListViewItem

'                    lvItem.Text = dtRow.Item("Name").ToString
'                    Select Case CInt(dtRow.Item("Id"))
'                        Case enAllocation.BRANCH
'                            lvItem.SubItems.Add(",ISNULL(hrstation_master.name,'') AS [Branch]")
'                            lvItem.SubItems.Add("LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid")
'                            lvItem.SubItems.Add(",[Branch]")
'                        Case enAllocation.DEPARTMENT_GROUP
'                            lvItem.SubItems.Add(",ISNULL(hrdepartment_group_master.name,'') AS [Department Group]")
'                            lvItem.SubItems.Add("LEFT JOIN hrdepartment_group_master ON hremployee_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid")
'                            lvItem.SubItems.Add(",[Department Group]")
'                        Case enAllocation.DEPARTMENT
'                            lvItem.SubItems.Add(",ISNULL(hrdepartment_master.name,'') AS [Department]")
'                            lvItem.SubItems.Add("JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid")
'                            lvItem.SubItems.Add(",[Department]")
'                        Case enAllocation.SECTION_GROUP
'                            lvItem.SubItems.Add(",ISNULL(hrsectiongroup_master.name,'') AS [Section Group]")
'                            lvItem.SubItems.Add("LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid")
'                            lvItem.SubItems.Add(",[Section Group]")
'                        Case enAllocation.SECTION
'                            lvItem.SubItems.Add(",ISNULL(hrsection_master.name,'') AS [Section]")
'                            lvItem.SubItems.Add("LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid")
'                            lvItem.SubItems.Add(",[Section]")
'                        Case enAllocation.UNIT_GROUP
'                            lvItem.SubItems.Add(",ISNULL(hrunitgroup_master.name,'') AS [Unit Group]")
'                            lvItem.SubItems.Add("LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid")
'                            lvItem.SubItems.Add(",[Unit Group]")
'                        Case enAllocation.UNIT
'                            lvItem.SubItems.Add(",ISNULL(hrunit_master.name,'') AS [Unit]")
'                            lvItem.SubItems.Add("LEFT JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid")
'                            lvItem.SubItems.Add(",[Unit]")
'                        Case enAllocation.TEAM
'                            lvItem.SubItems.Add(",ISNULL(hrteam_master.name,'') AS [Team]")
'                            lvItem.SubItems.Add("LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid")
'                            lvItem.SubItems.Add(",[Team]")
'                        Case enAllocation.JOB_GROUP
'                            lvItem.SubItems.Add(",ISNULL(hrjobgroup_master.name,'') AS [Job Group]")
'                            lvItem.SubItems.Add("LEFT JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid")
'                            lvItem.SubItems.Add(",[Job Group]")
'                        Case enAllocation.JOBS
'                            lvItem.SubItems.Add(",ISNULL(hrjob_master.job_name,'') AS [Job]")
'                            lvItem.SubItems.Add("JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid")
'                            lvItem.SubItems.Add(",[Job]")
'                        Case enAllocation.CLASS_GROUP
'                            lvItem.SubItems.Add(",ISNULL(hrclassgroup_master.name,'') AS [Class Group]")
'                            lvItem.SubItems.Add("LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid")
'                            lvItem.SubItems.Add(",[Class Group]")
'                        Case enAllocation.CLASSES
'                            lvItem.SubItems.Add(",ISNULL(hrclasses_master.name,'') AS [Class]")
'                            lvItem.SubItems.Add("LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid")
'                            lvItem.SubItems.Add(",[Class]")
'                        Case 900
'                            lvItem.SubItems.Add(",ISNULL(hrgradegroup_master.name,'') AS [Grade Group]")
'                            lvItem.SubItems.Add("JOIN hrgradegroup_master ON hremployee_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid")
'                            lvItem.SubItems.Add(",[Grade Group]")
'                        Case 901
'                            lvItem.SubItems.Add(",ISNULL(hrgrade_master.name,'') AS [Grade]")
'                            lvItem.SubItems.Add("JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid")
'                            lvItem.SubItems.Add(",[Grade]")
'                        Case 902
'                            lvItem.SubItems.Add(",ISNULL(hrgradelevel_master.name,'') AS [Grade Level]")
'                            lvItem.SubItems.Add("JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid")
'                            lvItem.SubItems.Add(",[Grade Level]")
'                        Case enAllocation.COST_CENTER
'                            lvItem.SubItems.Add(",ISNULL(prcostcenter_master.costcentername,'') AS [Cost Center]")
'                            lvItem.SubItems.Add("LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid")
'                            lvItem.SubItems.Add(",[Cost Center]")
'                    End Select

'                    lvDisplayCol.Items.Add(lvItem)
'                Next

'                If lvDisplayCol.Items.Count > 12 Then
'                    colhDisplayName.Width = 245 - 18
'                Else
'                    colhDisplayName.Width = 245
'                End If

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'            objMData = Nothing : dsList = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 28 FEB 2014 ] -- END

'#End Region

'#Region " Forms "

'    Private Sub frmAssessmentSoreRatingReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Try
'            objScoreRating = Nothing
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmAssessmentSoreRatingReport_FormClosed", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmAssessmentSoreRatingReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Language.setLanguage(Me.Name)

'            'S.SANDEEP [ 03 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call OtherSettings()
'            'S.SANDEEP [ 03 SEP 2012 ] -- END


'            Me.EZeeHeader1.Title = objScoreRating._ReportName
'            Me.EZeeHeader1.Message = objScoreRating._ReportDesc

'            Call FillCombo()

'            Call ResetValue()

'            'S.SANDEEP |27-MAY-2019| -- START
'            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
'            If ConfigParameter._Object._IsCalibrationSettingActive Then
'                pnlAllocation.Location = New Point(3, 137)
'                pnlAllocation.Size = New Size(343, 182)
'                lvAllocation.Size = New Size(255, 122)
'            Else
'                pnlAllocation.Location = New Point(3, 111)
'                pnlAllocation.Size = New Size(343, 204)
'                lvAllocation.Size = New Size(255, 144)
'            End If
'            'S.SANDEEP |27-MAY-2019| -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmAssessmentSoreRatingReport_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.Control Then
'                If e.KeyCode = Windows.Forms.Keys.E Then
'                    Call btnExport_Click(sender, e)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        Try
'            Select Case e.KeyChar
'                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
'                    Windows.Forms.SendKeys.Send("{TAB}")
'                    e.Handled = True
'                    Exit Select
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Buttons "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
'        Try
'            If SetFilter() = False Then Exit Sub
'            Select Case cboReportType.SelectedIndex
'                Case 0  'EMPLOYEE BASED 

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'objScoreRating.Export_Assessment_Report()

'                    'S.SANDEEP [13-JUL-2017] -- START
'                    'ISSUE/ENHANCEMENT : 
'                    Dim strFileName As String = String.Empty
'                    'S.SANDEEP [13-JUL-2017] -- END
'                    objScoreRating.Export_Assessment_Report(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, User._Object._Userunkid, Company._Object._Companyunkid, strFileName) 'S.SANDEEP [13-JUL-2017] -- START -- END




'                    'S.SANDEEP [04 JUN 2015] -- END

'                Case 1  'ALLOCATION BASED
'                    'S.SANDEEP [21 AUG 2015] -- START
'                    'objScoreRating.Report_Allocation_Based()

'                    'S.SANDEEP |27-MAY-2019| -- START
'                    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
'                    'objScoreRating.Report_Allocation_Based_New(FinancialYear._Object._DatabaseName, _
'                    '                                           User._Object._Userunkid, _
'                    '                                           FinancialYear._Object._YearUnkid, _
'                    '                                           Company._Object._Companyunkid, _
'                    '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                    '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                    '                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) 'S.SANDEEP [13-JUL-2017] -- START -- END

'                    objScoreRating.Report_Allocation_Based_New(FinancialYear._Object._DatabaseName, _
'                                                               User._Object._Userunkid, _
'                                                               FinancialYear._Object._YearUnkid, _
'                                                               Company._Object._Companyunkid, _
'                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, ConfigParameter._Object._IsCalibrationSettingActive) 'S.SANDEEP [13-JUL-2017] -- START -- END
'                    'S.SANDEEP |27-MAY-2019| -- END

'                    'S.SANDEEP [21 AUG 2015] -- END

'                    'S.SANDEEP [16 SEP 2016] -- START
'                Case 2
'                    objScoreRating.Report_Based_On_Competencies(FinancialYear._Object._DatabaseName, _
'                                                                User._Object._Userunkid, _
'                                                                FinancialYear._Object._YearUnkid, _
'                                                                Company._Object._Companyunkid, _
'                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) 'S.SANDEEP [13-JUL-2017] -- START -- END
'                    'S.SANDEEP [16 SEP 2016] -- END
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
'        Try
'            Call ResetValue()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim frm As New frmCommonSearch
'        Try
'            frm.DataSource = cboEmployee.DataSource
'            frm.ValueMember = cboEmployee.ValueMember
'            frm.DisplayMember = cboEmployee.DisplayMember
'            frm.CodeMember = "employeecode"
'            If frm.DisplayDialog Then
'                cboEmployee.SelectedValue = frm.SelectedValue
'                cboEmployee.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    'S.SANDEEP [ 03 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsAssessSoreRatingReport.SetMessages()
'            objfrm._Other_ModuleNames = "clsAssessSoreRatingReport"
'            objfrm.displayDialog(Me)

'            Call Language.setLanguage(Me.Name)
'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 03 SEP 2012 ] -- END


'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    Private Sub btnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
'        Dim frm As New frmAdvanceSearch
'        Try
'            frm._Hr_EmployeeTable_Alias = "hremployee_master"
'            frm.ShowDialog()
'            mstrAdvanceFilter = frm._GetFilterString
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 28 FEB 2014 ] -- END

'#End Region

'    'S.SANDEEP [ 14 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'#Region " Controls Events "

'    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
'        Try
'            Select Case cboReportType.SelectedIndex
'                Case 0
'                    pnlAllocation.Enabled = False
'                    txtSearch.Text = ""
'                    objchkAll.Checked = False
'                    cboEmployee.Enabled = True : objbtnSearchEmployee.Enabled = True
'                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES

'                    'S.SANDEEP [ 28 FEB 2014 ] -- START
'                    'gbSortBy.Enabled = False
'                    txtOrderBy.Enabled = False : objbtnSort.Enabled = False
'                    pnlCondition.Enabled = False
'                    lvDisplayCol.Items.Clear()
'                    btnAdvanceFilter.Visible = False
'                    mstrAdvanceFilter = ""
'                    'S.SANDEEP [ 28 FEB 2014 ] -- END

'                    'S.SANDEEP [16 DEC 2016] -- START
'                    chkConsiderEmployeeTermination.Enabled = False
'                    chkConsiderEmployeeTermination.Checked = False
'                    'S.SANDEEP [16 DEC 2016] -- END

'                    'S.SANDEEP [17 DEC 2016] -- START
'                    'ENHANCEMENT : SALARY INCLUSION
'                    chkShowEmpSalary.Enabled = False
'                    chkShowEmpSalary.Checked = False
'                    'S.SANDEEP [17 DEC 2016] -- END

'                    'S.SANDEEP [ 10 SEPT 2013 ] -- END
'                    'S.SANDEEP [16 SEP 2016] -- START
'                    'Case 1
'                Case 1, 2
'                    'S.SANDEEP [16 SEP 2016] -- END
'                    cboEmployee.SelectedValue = 0
'                    cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
'                    pnlAllocation.Enabled = True
'                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES

'                    'S.SANDEEP [ 28 FEB 2014 ] -- START
'                    'gbSortBy.Enabled = True
'                    txtOrderBy.Enabled = True : objbtnSort.Enabled = True
'                    pnlCondition.Enabled = True
'                    Call Fill_Column_List()
'                    btnAdvanceFilter.Visible = True
'                    'S.SANDEEP [ 28 FEB 2014 ] -- END

'                    'S.SANDEEP [16 DEC 2016] -- START
'                    chkConsiderEmployeeTermination.Enabled = True
'                    chkConsiderEmployeeTermination.Checked = False
'                    'S.SANDEEP [16 DEC 2016] -- END

'                    'S.SANDEEP [17 DEC 2016] -- START
'                    'ENHANCEMENT : SALARY INCLUSION
'                    chkShowEmpSalary.Enabled = True
'                    chkShowEmpSalary.Checked = False
'                    'S.SANDEEP [17 DEC 2016] -- END

'                    'S.SANDEEP [ 10 SEPT 2013 ] -- END
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
'        Try
'            colhAllocations.Text = cboAllocations.Text
'            Call Fill_Data()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
'        Try
'            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
'            Call Do_Operation(CBool(objchkAll.CheckState))
'            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
'        Try
'            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
'            If lvAllocation.CheckedItems.Count <= 0 Then
'                objchkAll.CheckState = CheckState.Unchecked
'            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
'                objchkAll.CheckState = CheckState.Indeterminate
'            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
'                objchkAll.CheckState = CheckState.Checked
'            End If
'            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
'        Try
'            If lvAllocation.Items.Count <= 0 Then Exit Sub
'            lvAllocation.SelectedIndices.Clear()
'            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
'            If lvFoundItem IsNot Nothing Then
'                lvAllocation.TopItem = lvFoundItem
'                lvFoundItem.Selected = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
'        Try
'            If CInt(cboPeriod.SelectedValue) > 0 Then
'                Dim objPrd As New clscommom_period_Tran
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
'                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
'                'Sohail (21 Aug 2015) -- End
'                mdtStartDate = objPrd._Start_Date
'                mdtEndDate = objPrd._End_Date
'                objPrd = Nothing
'                Dim ObjEmp As New clsEmployee_Master
'                Dim dsCombos As New DataSet

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                '    dsCombos = ObjEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , mdtStartDate, mdtEndDate)
'                'Else
'                '    dsCombos = ObjEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'                'End If

'                'S.SANDEEP [27 Jan 2016] -- START
'                'dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                '                                  User._Object._Userunkid, _
'                '                                  FinancialYear._Object._YearUnkid, _
'                '                                  Company._Object._Companyunkid, _
'                '                                  mdtStartDate, _
'                '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                '                                  mdtEndDate, _
'                '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

'                dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                                  User._Object._Userunkid, _
'                                                  FinancialYear._Object._YearUnkid, _
'                                                  Company._Object._Companyunkid, _
'                                                  mdtStartDate, mdtEndDate, _
'                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
'                                                  ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

'                'Shani (26-Sep-2016) -Replace- [ConfigParameter._Object._IncludeCustomItemInPlanning] --> [ConfigParameter._Object._IsIncludeInactiveEmp]


'                'S.SANDEEP [27 Jan 2016] -- END

'                'S.SANDEEP [04 JUN 2015] -- END

'                Dim dTable As DataTable
'                If ConfigParameter._Object._ExcludeProbatedEmpOnPerformance = True Then
'                    Dim sId As String = objScoreRating.Get_Probated_EmpIds(mdtEndDate)
'                    If sId.Trim.Length > 0 Then
'                        dTable = New DataView(dsCombos.Tables(0), "employeeunkid NOT IN(" & sId & ")", "", DataViewRowState.CurrentRows).ToTable
'                    Else
'                        dTable = New DataView(dsCombos.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
'                    End If
'                Else
'                    dTable = New DataView(dsCombos.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
'                End If

'                With cboEmployee
'                    .ValueMember = "employeeunkid"
'                    .DisplayMember = "employeename"
'                    .DataSource = dTable
'                End With
'                ObjEmp = Nothing : dsCombos.Dispose()
'            Else
'                Dim ObjEmp As New clsEmployee_Master
'                Dim dsCombos As New DataSet

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                '    dsCombos = ObjEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
'                'Else
'                '    dsCombos = ObjEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'                'End If

'                dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                            User._Object._Userunkid, _
'                                            FinancialYear._Object._YearUnkid, _
'                                            Company._Object._Companyunkid, _
'                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                            ConfigParameter._Object._UserAccessModeSetting, _
'                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
'                'S.SANDEEP [04 JUN 2015] -- END

'                With cboEmployee
'                    .ValueMember = "employeeunkid"
'                    .DisplayMember = "employeename"
'                    .DataSource = dsCombos.Tables("List")
'                End With

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [ 10 SEPT 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
'        Try
'            objScoreRating.setOrderBy(1)
'            txtOrderBy.Text = objScoreRating.OrderByDisplay
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 10 SEPT 2013 ] -- END

'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    Private Sub chkOption_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOption.CheckedChanged
'        Try
'            cboCondition.Enabled = chkOption.Checked
'            dtpAppDate.Enabled = chkOption.Checked
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkOption_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objchkdisplay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkdisplay.CheckedChanged
'        Try
'            For Each lvitem As ListViewItem In lvDisplayCol.Items
'                lvitem.Checked = objchkdisplay.Checked
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkdisplay_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 28 FEB 2014 ] -- END

'#End Region
'    'S.SANDEEP [ 14 AUG 2013 ] -- END


'End Class

'S.SANDEEP |11-SEP-2019| -- END
