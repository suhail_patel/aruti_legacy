'************************************************************************************************************************************
'Class Name : clsAssessmentListReport.vb
'Purpose    :
'Date       : 06/02/2012
'Written By : Sandeep J. Sharma
'Modified   : Shani K. Sheladiya 
'Mod. date  : 27-APR-2016
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsAssessSoreRatingReport
    Inherits IReportData

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsAssessSoreRatingReport"
    Private mstrReportId As String = enArutiReport.EmployeeAssessSoreRatingReport
    Private mdtFinalTable As DataTable = Nothing
    Private dsColumns As DataSet = Nothing
    Private StrFinalPath As String = String.Empty
    Private mdecFinalScore As Decimal = 0
    'S.SANDEEP [17 DEC 2016] -- START
    'ENHANCEMENT : SALARY INCLUSION
    Private mblnShowScaleOnReport As Boolean = False
    'S.SANDEEP [17 DEC 2016] -- END
#End Region

#Region " Properties Value "
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrAllocation As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    Private mintReportType_Id As Integer = -1
    Private mstrReportType_Name As String = String.Empty
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mblnOnProbationSelected As Boolean = False
    Private mstrCondition As String = String.Empty
    Private mdtAppointmentDate As Date = Nothing
    Private mstrSelectedCols As String = String.Empty
    Private mstrSelectedJoin As String = String.Empty
    Private mstrDisplayCols As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrCaption As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintScoringOptionId As Integer = 0
    Private blnIsSelfAssignCompetencies As Boolean = False

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Private mblnIsCalibrationSettingActive As Boolean = False
    Private mintDisplayScoreType As Integer = 0
    Private mstrDisplayScoreName As String = String.Empty
    'S.SANDEEP |27-MAY-2019| -- END

    'S.SANDEEP |11-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}
    Private mdicPeriodChecked As Dictionary(Of Integer, String)
    'S.SANDEEP |11-SEP-2019| -- END

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Allocation() As String
        Set(ByVal value As String)
            mstrAllocation = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _ReportType_Id() As Integer
        Set(ByVal value As Integer)
            mintReportType_Id = value
        End Set
    End Property

    Public WriteOnly Property _ReportType_Name() As String
        Set(ByVal value As String)
            mstrReportType_Name = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _OnProbationSelected() As Boolean
        Set(ByVal value As Boolean)
            mblnOnProbationSelected = value
        End Set
    End Property

    Public WriteOnly Property _Condition() As String
        Set(ByVal value As String)
            mstrCondition = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentDate() As Date
        Set(ByVal value As Date)
            mdtAppointmentDate = value
        End Set
    End Property

    Public WriteOnly Property _SelectedCols() As String
        Set(ByVal value As String)
            mstrSelectedCols = value
        End Set
    End Property

    Public WriteOnly Property _SelectedJoin() As String
        Set(ByVal value As String)
            mstrSelectedJoin = value
        End Set
    End Property

    Public WriteOnly Property _DisplayCols() As String
        Set(ByVal value As String)
            mstrDisplayCols = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _Caption() As String
        Set(ByVal value As String)
            mstrCaption = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ScoringOptionId() As Integer
        Set(ByVal value As Integer)
            mintScoringOptionId = value
        End Set
    End Property

    Public WriteOnly Property _SelfAssignCompetencies() As Boolean
        Set(ByVal value As Boolean)
            blnIsSelfAssignCompetencies = value
        End Set
    End Property

    'S.SANDEEP [17 DEC 2016] -- START
    'ENHANCEMENT : SALARY INCLUSION
    Public WriteOnly Property _ShowScaleOnReport() As Boolean
        Set(ByVal value As Boolean)
            mblnShowScaleOnReport = value
        End Set
    End Property
    'S.SANDEEP [17 DEC 2016] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public WriteOnly Property _IsCalibrationSettingActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCalibrationSettingActive = value
        End Set
    End Property
    Public WriteOnly Property _DisplayScoreType() As Integer
        Set(ByVal value As Integer)
            mintDisplayScoreType = value
        End Set
    End Property
    Public WriteOnly Property _DisplayScoreName() As String
        Set(ByVal value As String)
            mstrDisplayScoreName = value
        End Set
    End Property
    'S.SANDEEP |27-MAY-2019| -- END

    'S.SANDEEP |11-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}
    Public WriteOnly Property _DicPeriodChecked() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicPeriodChecked = value
        End Set
    End Property
    'S.SANDEEP |11-SEP-2019| -- END
#End Region

#Region " Public Function(s) And Procedures "
    Dim iColumn_DetailReport As New IColumnCollection

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '')", Language.getMessage(mstrModuleName, 21, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '')", Language.getMessage(mstrModuleName, 21, "Employee Name")))
            End If
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 20, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("Profile", Language.getMessage(mstrModuleName, 24, "Profile")))
            iColumn_DetailReport.Add(New IColumn("appointeddate", Language.getMessage(mstrModuleName, 25, "Appointed Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub SetDefaultValue()
        Try
            'mintEmployeeId = 0
            'mstrEmployeeName = String.Empty
            'mintPeriodId = 0
            'mstrPeriodName = String.Empty
            'mstrAllocation = String.Empty
            'mstrAllocationIds = String.Empty
            'mintReportType_Id = -1
            'mstrReportType_Name = String.Empty
            'mblnOnProbationSelected = False
            'mstrCondition = String.Empty
            'mdtAppointmentDate = Nothing
            'mstrSelectedCols = String.Empty
            'mstrSelectedJoin = String.Empty
            'mstrDisplayCols = String.Empty
            'mintScoringOptionId = 0
            'blnIsSelfAssignCompetencies = False

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            mblnIsCalibrationSettingActive = False
            mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            mstrDisplayScoreName = ""
            'S.SANDEEP |27-MAY-2019| -- END

            'S.SANDEEP |11-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}
            mdicPeriodChecked = New Dictionary(Of Integer, String)
            'S.SANDEEP |11-SEP-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function Get_Probated_EmpIds(ByVal dtEndDate As DateTime) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim strIds As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            StrQ = "SELECT ISNULL( STUFF((SELECT DISTINCT ',' + CAST(employeeunkid AS NVARCHAR(50)) FROM athremployee_master " & _
                   "WHERE CONVERT(CHAR(8),probation_from_date,112)<= @end_date AND CONVERT(CHAR(8),probation_to_date,112)>= @end_date " & _
                   "FOR XML PATH('')),1,1,''),'') AS Ids "
            objData.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtEndDate))

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strIds = dsList.Tables(0).Rows(0).Item("Ids").ToString.Trim
            End If

            Return strIds
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Probated_Emp; Module Name: " & mstrModuleName)
            Return strIds
        Finally
        End Try
    End Function


    'S.SANDEEP [13-JUL-2017] -- START
    'ISSUE/ENHANCEMENT : 
    Public Sub Export_Assessment_Report(ByVal dtEmployeeAsOnDate As DateTime, ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean, ByVal intUserId As Integer, ByVal intCompanyId As Integer, ByRef mstrFileName As String)
        'Public Sub Export_Assessment_Report(ByVal dtEmployeeAsOnDate As DateTime)
        'S.SANDEEP [13-JUL-2017] -- END
        Try

            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyId <= 0 Then
                intCompanyId = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyId
            ConfigParameter._Object._Companyunkid = intCompanyId

            If intUserId <= 0 Then
                intUserId = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserId
            'S.SANDEEP [13-JUL-2017] -- END

            Call CreateTable()

            Dim dTRow As DataRow = Nothing

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'Dim dtComputataion As DataTable = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
            Dim dtComputataion As DataTable = Nothing
            If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                dtComputataion = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, False)
            ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                dtComputataion = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, mblnIsCalibrationSettingActive)
            End If
            'S.SANDEEP |27-MAY-2019| -- END


            dTRow = mdtFinalTable.NewRow
            'Shani (24-May-2016) -- Start
            'dTRow = Get_BSC_Evalution(dtEmployeeAsOnDate)       'BSC
            dTRow = Get_BSC_Evalution(dtEmployeeAsOnDate, dtComputataion)       'BSC
            'Shani (24-May-2016) -- End
            mdtFinalTable.Rows.Add(dTRow)

            dTRow = mdtFinalTable.NewRow
            'Shani (24-May-2016) -- Start
            'dTRow = Get_GEvaluation(dtEmployeeAsOnDate)       'GENERAL EVALUATION 
            dTRow = Get_GEvaluation(dtEmployeeAsOnDate, dtComputataion)       'GENERAL EVALUATION 
            'Shani (24-May-2016) -- End
            mdtFinalTable.Rows.Add(dTRow)

            'Shani (24-May-2016) -- Start
            'Call Set_Total(dtEmployeeAsOnDate)                'SETTING COLUMN TOTAL
            Call Set_Total(dtEmployeeAsOnDate, dtComputataion)                'SETTING COLUMN TOTAL
            'Shani (24-May-2016) -- End




            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
            'End If

            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & mintEmployeeId.ToString(), strExportPath, mdtFinalTable) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                mstrFileName = Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & mintEmployeeId.ToString()
                Call ReportFunction.Open_ExportedFile(blnOpenAfterExport, StrFinalPath)
            End If
            'S.SANDEEP [13-JUL-2017] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Assessment_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CreateTable()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation

        Try
            mdtFinalTable = New DataTable("Data")

            mdtFinalTable.Columns.Add("referencename", System.Type.GetType("System.String")).Caption = ""

            '-------------------Employee Start-------------------
            StrQ = "    SELECT " & _
                   "     AId " & _
                   "    ,Appriser " & _
                   "    ,Mode " & _
                   "    ,EmpId " & _
                   "    ,analysisunkid " & _
                   "    ,visibletypeid " & _
                   "    ,isfound " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        'S_' + CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) AS AId "
            'S.SANDEEP [27 DEC 2016] -- START {analysisunkid,visibletypeid} -- END


            If mblnFirstNamethenSurname = False Then
                StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' ] ' AS Appriser  "
            Else
                StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser "
            End If
            StrQ &= "       ,1 AS Mode " & _
                   "        ,hremployee_master.employeeunkid AS EmpId " & _
                   "        ,1 AS analysisunkid " & _
                   "        ,1 AS visibletypeid " & _
                   "        ,1 AS isfound " & _
                   "    FROM hremployee_master " & _
                   "    WHERE hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END
            '-------------------Employee END-------------------

            '-------------------Assessor Start-------------------
            StrQ &= "UNION ALL " & _
                   "    SELECT " & _
                   "         'A_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
                    "        ,@Appriser + CHAR(13) + ' [ ' + #ASS_Name# +' ] ' AS Appriser " & _
                   "        ,2 AS Mode " & _
                   "        ,hrassessor_master.assessormasterunkid " & _
                   "        ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
                   "        ,hrassessor_tran.visibletypeid " & _
                   "        ,CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN hrevaluation_analysis_master.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
                   "              WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN hrevaluation_analysis_master.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
                   "         END AS isfound " & _
                   "    FROM hrassessor_tran " & _
                   "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "        LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
                   "                AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
                   "        #ASS_JOIN# " & _
                   "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid,isfound} -- END
            '-------------------Assessor END-------------------

            '-------------------Reviewer Start-------------------
            StrQ &= "UNION ALL " & _
                   "    SELECT " & _
                   "         'R_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
                   "        ,@Reviewer + CHAR(13) + ' [ ' + #REV_NAME# +' ] ' AS Appriser " & _
                   "        ,4 AS Mode " & _
                   "        ,hrassessor_master.assessormasterunkid " & _
                   "        ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
                   "        ,hrassessor_tran.visibletypeid " & _
                   "        ,CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN hrevaluation_analysis_master.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
                   "              WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN hrevaluation_analysis_master.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
                   "         END AS isfound " & _
                   "    FROM hrassessor_tran " & _
                   "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "        LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
                   "                AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
                   "        #REV_JOIN# " & _
                   "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid,isfound} -- END

            '-------------------Reviewer Start-------------------
            StrQ &= ") AS A WHERE 1 = 1 "

            Dim strFinalQ As String = StrQ
            Dim dsTemp As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , mintEmployeeId, , clsAssessor.enAssessorType.ASSESSOR)

            If dsTemp.Tables(0).Rows.Count > 0 Then
                If dsTemp.Tables(0).Rows(0)("companyunkid") <= 0 AndAlso dsTemp.Tables(0).Rows(0)("DBName").ToString.Trim.Length <= 0 Then
                    strFinalQ = strFinalQ.Replace("#ASS_Name#", "ISNULL(cfuser_master.username,'') ")
                    strFinalQ = strFinalQ.Replace("#ASS_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid ")
                Else
                    strFinalQ = strFinalQ.Replace("#ASS_Name#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
                    strFinalQ = strFinalQ.Replace("#ASS_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
                                                                " JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                End If
                strFinalQ = strFinalQ.Replace("#DBName#", dsTemp.Tables(0).Rows(0)("DBName") & "..")
            Else
                strFinalQ = strFinalQ.Replace("#ASS_Name#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid")
            End If


            dsTemp = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , mintEmployeeId, , clsAssessor.enAssessorType.REVIEWER)
            If dsTemp.Tables(0).Rows.Count > 0 Then
                If dsTemp.Tables(0).Rows(0)("companyunkid") <= 0 AndAlso dsTemp.Tables(0).Rows(0)("DBName").ToString.Trim.Length <= 0 Then
                    strFinalQ = strFinalQ.Replace("#REV_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strFinalQ = strFinalQ.Replace("#REV_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid ")
                Else
                    strFinalQ = strFinalQ.Replace("#REV_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
                    strFinalQ = strFinalQ.Replace("#REV_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
                                                                " JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                End If
                strFinalQ = strFinalQ.Replace("#DBName#", dsTemp.Tables(0).Rows(0)("DBName") & "..")
            Else
                strFinalQ = strFinalQ.Replace("#REV_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
                strFinalQ = strFinalQ.Replace("#REV_JOIN#", "JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid")
            End If

            objDataOperation.AddParameter("@Self", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Self"))
            objDataOperation.AddParameter("@Appriser", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor"))
            objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Reviewer"))
            objDataOperation.AddParameter("@ExAssessor", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor"))

            dsColumns = objDataOperation.ExecQuery(strFinalQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Dim dCol As DataColumn

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'If dsColumns.Tables("List").Rows.Count > 0 Then

            '    For Each dRow As DataRow In dsColumns.Tables("List").Rows
            '        dCol = New DataColumn
            '        dCol.ColumnName = "Column" & dRow.Item("AId").ToString
            '        dCol.Caption = dRow.Item("Appriser").ToString
            '        dCol.DataType = System.Type.GetType("System.Decimal")
            '    Next
            'End If
            Dim mDicMode As New Dictionary(Of Integer, Integer)
            Dim dtSumm As DataTable = dsColumns.Tables("List").DefaultView.ToTable(True, "Mode")
            mDicMode = dtSumm.AsEnumerable().ToDictionary(Of Integer, Integer)(Function(row) row.Field(Of Integer)("Mode"), Function(row) row.Field(Of Integer)("Mode"))
            Dim dtTable As DataTable = dsColumns.Tables("List").Clone
            If mDicMode.Keys.Count > 0 Then
                Dim xtemp() As DataRow = Nothing
                For Each xKey As Integer In mDicMode.Keys
                    xtemp = Nothing
                    xtemp = dsColumns.Tables("List").Select("Mode = '" & xKey & "' AND isfound = 1")
                    If xtemp.Length <= 0 Then
                        xtemp = dsColumns.Tables("List").Select("Mode = '" & xKey & "' AND isfound = 0 AND visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "'")
                    End If
                    For i As Integer = 0 To xtemp.Length - 1
                        dtTable.ImportRow(xtemp(i))
                    Next
                Next
            End If
            If dtTable.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTable.Rows
                    dCol = New DataColumn
                    dCol.ColumnName = "Column" & dRow.Item("AId").ToString
                    dCol.Caption = dRow.Item("Appriser").ToString
                    dCol.DataType = System.Type.GetType("System.Decimal")

                    mdtFinalTable.Columns.Add(dCol)
                Next
            End If
            dsColumns.Tables.Remove("List")
            dsColumns.Tables.Add(dtTable)
            'S.SANDEEP [27 DEC 2016] -- END


            'mdtFinalTable.Columns.Add("total", System.Type.GetType("System.Decimal")).Caption = "Total"
            'mdtFinalTable.Columns.Add("total_percent", System.Type.GetType("System.Decimal")).Caption = "Total %"

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Shani (24-May-2016) -- Start
    'Private Function Get_BSC_Evalution(ByVal dtEmployeeAsOnDate As Date) As DataRow
    Private Function Get_BSC_Evalution(ByVal dtEmployeeAsOnDate As Date, ByVal dtComputionScore As DataTable) As DataRow
        'Shani (24-May-2016) -- End

        Dim dBRow As DataRow = mdtFinalTable.NewRow
        'Dim dsTList As New DataSet
        Dim xEvaluation As New clsevaluation_analysis_master
        Dim decResult As Decimal = 0
        ''Shani (24-May-2016) -- Start
        'Dim dtComputionScore As DataTable = Nothing
        ''Shani (24-May-2016) -- End
        Try

            ''Shani (24-May-2016) -- Start
            'dtComputionScore = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
            ''Shani (24-May-2016) -- End

            dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC"
            For Each drow As DataRow In dsColumns.Tables("List").Rows
                If IsDBNull(drow("Mode")) = False AndAlso drow("Mode") > 0 Then
                    'Shani (24-May-2016) -- Start
                    'Select Case CInt(drow("Mode"))
                    '    Case 1
                    '        decResult = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, _
                    '                                      mintScoringOptionId, _
                    '                                      enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
                    '                                      mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
                    '        dBRow.Item("Column" & "S_" & drow("EmpId")) = decResult
                    '    Case 2
                    '        decResult = xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, _
                    '                                      mintScoringOptionId, _
                    '                                      enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
                    '                                      mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
                    '        dBRow.Item("Column" & "A_" & drow("EmpId")) = decResult
                    '    Case 4
                    '        decResult = xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, _
                    '                                       mintScoringOptionId, _
                    '                                       enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
                    '                                       mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
                    '        dBRow.Item("Column" & "R_" & drow("EmpId")) = decResult
                    'End Select

                    Select Case CInt(drow("Mode"))
                        Case 1
                            decResult = 0
                            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(X) X.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
                                                                                           And X.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                           .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                                If xScore.Count > 0 Then
                                    decResult = xScore(0)
                                End If
                            End If
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dBRow.Item("Column" & "S_" & drow("EmpId")) = Format(decResult, "#####0.#0")
                            dBRow.Item("Column" & "S_" & drow("EmpId")) = Format(decResult, "#########################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                        Case 2
                            decResult = 0
                            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(X) X.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE _
                                                                                           And X.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                           .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                                If xScore.Count > 0 Then
                                    decResult = xScore(0)
                                End If
                            End If
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dBRow.Item("Column" & "A_" & drow("EmpId")) = Format(decResult, "#####0.#0")
                            dBRow.Item("Column" & "A_" & drow("EmpId")) = Format(decResult, "#########################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                        Case 4
                            decResult = 0
                            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(X) X.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
                                                                                           And X.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                                           .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                                If xScore.Count > 0 Then
                                    decResult = xScore(0)
                                End If
                            End If
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dBRow.Item("Column" & "R_" & drow("EmpId")) = Format(decResult, "#####0.#0")
                            dBRow.Item("Column" & "R_" & drow("EmpId")) = Format(decResult, "#########################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                    End Select
                    'Shani (24-May-2016) -- End
                End If
            Next

            Return dBRow
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_BSC_Row; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            xEvaluation = Nothing
        End Try
    End Function


    'Shani (24-May-2016) -- Start
    'Private Function Get_GEvaluation(ByVal dtEmployeeAsOnDate As DateTime) As DataRow
    Private Function Get_GEvaluation(ByVal dtEmployeeAsOnDate As DateTime, ByVal dtComputionScore As DataTable) As DataRow
        'Shani (24-May-2016) -- End
        Dim dGRow As DataRow = mdtFinalTable.NewRow
        Dim dsTList As New DataSet
        Dim decResult As Decimal = 0
        Dim xEvaluation As New clsevaluation_analysis_master
        ''Shani (24-May-2016) -- Start
        'Dim dtComputionScore As DataTable = Nothing
        ''Shani (24-May-2016) -- End
        Try

            ''Shani (24-May-2016) -- Start
            'dtComputionScore = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
            ''Shani (24-May-2016) -- End

            dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT"

            For Each drow As DataRow In dsColumns.Tables("List").Rows
                If IsDBNull(drow("Mode")) = False AndAlso drow("Mode") > 0 Then
                    'Shani (24-May-2016) -- Start
                    'Select Case CInt(drow("Mode"))
                    '    Case 1
                    '        decResult = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, _
                    '                                      mintScoringOptionId, _
                    '                                      enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
                    '                                      mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
                    '        dGRow.Item("Column" & "S_" & drow("EmpId")) = decResult
                    '    Case 2
                    '        decResult = xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, _
                    '                                      mintScoringOptionId, _
                    '                                      enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
                    '                                      mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
                    '        dGRow.Item("Column" & "A_" & drow("EmpId")) = decResult
                    '    Case 4
                    '        decResult = xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, _
                    '                                       mintScoringOptionId, _
                    '                                       enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
                    '                                       mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
                    '        dGRow.Item("Column" & "R_" & drow("EmpId")) = decResult
                    'End Select
                    Select Case CInt(drow("Mode"))
                        Case 1
                            decResult = 0
                            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(X) X.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
                                                                                           And X.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                           .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                                If xScore.Count > 0 Then
                                    decResult = xScore(0)
                                End If
                            End If
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dGRow.Item("Column" & "S_" & drow("EmpId")) = Format(decResult, "#####0.#0")
                            dGRow.Item("Column" & "S_" & drow("EmpId")) = Format(decResult, "#########################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                        Case 2
                            decResult = 0
                            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(X) X.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE _
                                                                                           And X.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                           .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                                If xScore.Count > 0 Then
                                    decResult = xScore(0)
                                End If
                            End If
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dGRow.Item("Column" & "A_" & drow("EmpId")) = Format(decResult, "#####0.#0")
                            dGRow.Item("Column" & "A_" & drow("EmpId")) = Format(decResult, "#########################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                        Case 4
                            decResult = 0
                            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(X) X.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE _
                                                                                           And X.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                                           .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                                If xScore.Count > 0 Then
                                    decResult = xScore(0)
                                End If
                            End If
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'dGRow.Item("Column" & "R_" & drow("EmpId")) = Format(decResult, "#####0.#0")
                            dGRow.Item("Column" & "R_" & drow("EmpId")) = Format(decResult, "#########################0.#0")
                            'S.SANDEEP |21-AUG-2019| -- END
                    End Select
                    'Shani (24-May-2016) -- End
                End If
            Next

            Return dGRow
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_GEvaluation; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            xEvaluation = Nothing
        End Try
    End Function


    'Shani (24-May-2016) -- Start
    'Private Sub Set_Total(ByVal dtEmployeeAsOnDate As Date)
    Private Sub Set_Total(ByVal dtEmployeeAsOnDate As Date, ByVal dtComputionScore As DataTable)
        'Shani (24-May-2016) -- End


        Dim xEvaluation As New clsevaluation_analysis_master
        Try
            mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
            mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)

            mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("referencename") = "FORMULA FOR TOTAL OVERALL SCORES"


            'Shani (24-May-2016) -- Start
            'For Each drow As DataRow In dsColumns.Tables("List").Rows
            '    If IsDBNull(drow("Mode")) = False AndAlso drow("Mode") > 0 Then
            '        Select Case CInt(drow("Mode"))
            '            Case 1
            '                mdecFinalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            '                mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)

            '                'Case 2
            '                '    mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            '                '    mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)

            '                'Case 4
            '                '    mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            '                '    mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            '        End Select
            '    End If
            'Next

            'mdecFinalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            'mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)

            'If mdecFinalScore <= 0 Then
            '    mdecFinalScore = xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            '    mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            'End If

            'If mdecFinalScore <= 0 Then
            '    mdecFinalScore = xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            '    mdecFinalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies)
            'End If
            mdecFinalScore = 0
            If dtComputionScore IsNot Nothing AndAlso dtComputionScore.Rows.Count > 0 Then
                Dim xScore = dtComputionScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = mintEmployeeId).Select(Function(x) x.Field(Of Decimal)("finaloverallscore")).ToList
                If xScore.Count > 0 Then
                    mdecFinalScore = xScore(0)
                End If
            End If
            'Shani (24-May-2016) -- End

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("Column" & "S_" & mintEmployeeId) = Format(mdecFinalScore, "#####0.#0")
            mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("Column" & "S_" & mintEmployeeId) = Format(mdecFinalScore, "#########################0.#0")
            'S.SANDEEP |21-AUG-2019| -- END
            mdtFinalTable.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Total; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim StrValue As String = ""
        Try
            Select Case ConfigParameter._Object._PerformanceComputation
                Case 1  'WEIGHTED AVG OF
                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrValue = Language.getMessage(mstrModuleName, 8, "Weighted Average By Employee,Assessor,Reviewer.")
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrValue = Language.getMessage(mstrModuleName, 9, "Weighted Average By Employee,Assessor.")
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrValue = Language.getMessage(mstrModuleName, 10, "Weighted Average By Employee,Reviewer.")
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrValue = Language.getMessage(mstrModuleName, 11, "Weighted Average By Assessor,Reviewer.")
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrValue = Language.getMessage(mstrModuleName, 12, "Weighted Average By Employee.")
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrValue = Language.getMessage(mstrModuleName, 13, "Weighted Average By Assessor.")
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrValue = Language.getMessage(mstrModuleName, 14, "Weighted Average By Reviewer.")
                    End If
                Case 2  'WEIGHT SCORE OF
                    If ConfigParameter._Object._WScrComputType = 1 Then
                        StrValue = Language.getMessage(mstrModuleName, 15, "Weighted Score By Employee.")
                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
                        StrValue = Language.getMessage(mstrModuleName, 16, "Weighted Score By Assessor.")
                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
                        StrValue = Language.getMessage(mstrModuleName, 17, "Weighted Score By Reviewer.")
                    End If
            End Select

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            If mblnIsCalibrationSettingActive Then
                StrValue = Language.getMessage(mstrModuleName, 26, "Display Score Type") & " : " & mstrDisplayScoreName
            End If
            'S.SANDEEP |27-MAY-2019| -- END

            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 5, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='5%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='40%' colspan= " & objDataReader.Columns.Count - 1 & " align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='40%' colspan= " & objDataReader.Columns.Count - 1 & " align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & "FORMULAR AND A GUIDE TO OVERALL SCORES RATING" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & "Employee : " & mstrEmployeeName & ", Period : " & mstrPeriodName & ", " & StrValue & "</B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strBuilder.Append("<TD BORDER=1 WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If k = 1 Then
                        If objDataReader.Rows(i)("referencename").ToString.Trim.Contains("FORMULA FOR TOTAL OVERALL SCORES") Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN=" & objDataReader.Columns.Count - 1 & "><FONT SIZE=2><B>" & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                            Exit For
                        End If
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If

                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD colspan = " & objDataReader.Columns.Count & " BORDER = 0 ALIGN='LEFT'><FONT SIZE=2><B>" & "OVERALL PERFORMANCE RATING (Tick on relevant box to show overall quality of the Appraisee performance)" & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)



            Dim objRating As New clsAppraisal_Rating
            Dim dsRating = New DataSet : Dim dtTable As DataTable
            dsRating = objRating.getComboList("List", False)
            dtTable = New DataView(dsRating.Tables(0), "", "scrt DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dsRating.Dispose() : objRating = Nothing
            strBuilder.Append(" <TR> " & vbCrLf)
            For Each dr In dtTable.Rows
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dr("name") & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            Dim blnAboveMaxRating As Boolean = False
            Dim drow() As DataRow = dtTable.Select("scrf <= " & mdecFinalScore & " AND scrt >= " & mdecFinalScore & " ")
            For Each dr In dtTable.Rows
                If drow.Length > 0 Then
                    If dr Is drow(0) Then
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X</B></FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
                    End If
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
                End If

            Next
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)



            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>SCORE<B></FONT></B></TD>" & vbCrLf)
            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>LEVEL OF PERFORMANCE<B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            For Each dr In dtTable.Rows
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr("scrf").ToString & " - " & dr("scrt").ToString & "</FONT></TD>" & vbCrLf)
                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr("name") & "</FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    'S.SANDEEP [19-JUL-2017] -- START
    'ISSUE/ENHANCEMENT : REPORT TAKING LONG TIME
    'Public Sub Report_Allocation_Based_New(ByVal strDatabaseName As String, _
    '                                           ByVal intUserUnkid As Integer, _
    '                                           ByVal intYearUnkid As Integer, _
    '                                           ByVal intCompanyUnkid As Integer, _
    '                                           ByVal dtPeriodStart As Date, _
    '                                           ByVal dtPeriodEnd As Date, _
    '                                           ByVal strUserModeSetting As String, _
    '                                           ByVal blnOnlyApproved As Boolean, _
    '                                           ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean) 'S.SANDEEP [13-JUL-2017] -- START {strExportPath,blnOpenAfterExport} -- END


    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dsList As New DataSet
    '    Dim dtRating As DataTable
    '    Dim exForce As Exception
    '    Try
    '        'S.SANDEEP [13-JUL-2017] -- START
    '        'ISSUE/ENHANCEMENT : 
    '        If intCompanyUnkid <= 0 Then
    '            intCompanyUnkid = Company._Object._Companyunkid
    '        End If

    '        Company._Object._Companyunkid = intCompanyUnkid
    '        ConfigParameter._Object._Companyunkid = intCompanyUnkid

    '        If intUserUnkid <= 0 Then
    '            intUserUnkid = User._Object._Userunkid
    '        End If

    '        User._Object._Userunkid = intUserUnkid
    '        'S.SANDEEP [13-JUL-2017] -- END

    '        dtPeriodStart = mdtStartDate : dtPeriodEnd = mdtEndDate
    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

    '        StrQ = "SELECT " & _
    '                "	 employeecode AS [Employee Code] "
    '        If mblnFirstNamethenSurname = False Then
    '            StrQ &= ", ISNULL(surname, '') + ' ' " & _
    '                        "+ ISNULL(firstname, '') + ' ' " & _
    '                        "+ ISNULL(othername, '') AS [Employee Name] "
    '        Else
    '            StrQ &= ", ISNULL(firstname, '') + ' ' " & _
    '                        "+ ISNULL(othername, '') + ' ' " & _
    '                        "+ ISNULL(surname, '') AS [Employee Name] "
    '        End If
    '        'S.SANDEEP [17 DEC 2016] -- START
    '        'ENHANCEMENT : SALARY INCLUSION
    '        If mblnShowScaleOnReport Then
    '            StrQ &= "   ,ISNULL(SAL.Scale,0) AS Scale "
    '        End If
    '        'S.SANDEEP [17 DEC 2016] -- END
    '        StrQ &= mstrSelectedCols & " " & _
    '                "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2)) AS Score " & _
    '                "   ,'' AS [Profile] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2)) AS [Self Assessment] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2)) AS [Assessor(s) Assessment] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2)) AS [Reviewer Assessment] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2)) AS [Self BSC] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2))AS [Assessor(s) BSC] " & _
    '                "   ,CAST(0 AS DECIMAL(30,2)) AS [Reviewer BSC] " & _
    '                "	,hremployee_master.employeeunkid AS EId " & _
    '                "	,'" & mintPeriodId & "' AS PId " & _
    '                "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
    '                "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
    '                "   ,appointeddate AS aDate " & _
    '                "FROM hremployee_master " & _
    '                mstrSelectedJoin & " "
    '        'S.SANDEEP [17 DEC 2016] -- START
    '        'ENHANCEMENT : SALARY INCLUSION
    '        If mblnShowScaleOnReport Then
    '            StrQ &= "LEFT JOIN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        newscale AS Scale " & _
    '                    "       ,employeeunkid AS EmpId " & _
    '                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
    '                    "   FROM prsalaryincrement_tran " & _
    '                    "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
    '                    "   AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                    ") AS SAL ON SAL.EmpId = hremployee_master.employeeunkid AND SAL.rno = 1 "
    '        End If
    '        'S.SANDEEP [17 DEC 2016] -- END
    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 hrassessor_master.assessormasterunkid AS AsrMId " & _
    '                "		,hrassessor_tran.employeeunkid AS AEId " & _
    '                "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
    '                "	FROM hrassessor_tran " & _
    '                "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '                "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
    '                "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
    '                "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                "	WHERE hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
    '                ") AS ASR ON ASR.AEId = hremployee_master.employeeunkid AND ASR.analysisunkid > 0 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 hrassessor_master.assessormasterunkid AS RevMId " & _
    '                "		,hrassessor_tran.employeeunkid AS REId " & _
    '                "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
    '                "	FROM hrassessor_tran " & _
    '                "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '                "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.reviewerunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
    '                "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
    '                "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                "	WHERE hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
    '                ") AS REV ON REV.REId = hremployee_master.employeeunkid AND REV.analysisunkid > 0 "


    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= "WHERE isapproved = 1 "

    '        If mstrAllocationIds.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAllocationIds
    '        End If

    '        If mstrAdvanceFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvanceFilter
    '        End If

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry & " "
    '        End If

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= xDateFilterQry & " "
    '        End If

    '        If mblnOnProbationSelected = True Then
    '            StrQ &= " AND hremployee_master.employeeunkid " & _
    '                    "NOT IN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "	    Probation.employeeunkid " & _
    '                    "   FROM " & _
    '                    "   ( " & _
    '                    "	    SELECT " & _
    '                    "		     employeeunkid " & _
    '                    "		    ,CONVERT(CHAR(8),date1,112) AS PFD " & _
    '                    "		    ,CONVERT(CHAR(8),date2,112) AS PTD " & _
    '                    "		    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                    "	    FROM hremployee_dates_tran " & _
    '                    "	    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
    '                    "	    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
    '                    "   ) AS Probation WHERE Probation.Rno = 1 " & _
    '                    "   AND Probation.PFD <= '" & eZeeDate.convertDate(mdtEndDate) & "' AND Probation.PTD >= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
    '                    ")"
    '        End If

    '        If Me.OrderByQuery <> "" Then
    '            StrQ &= "ORDER BY " & Me.OrderByQuery
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        Dim objRating As New clsAppraisal_Rating
    '        dtRating = objRating._DataTable
    '        objRating = Nothing


    '        'Shani (24-May-2016) -- Start
    '        'Dim objCmpuMst As New clsassess_computation_master
    '        'Dim mblnIsSelfIncluded As Boolean = False
    '        'Dim mblnIsAssrIncluded As Boolean = False
    '        'Dim mblnIsRevrIncluded As Boolean = False
    '        'Dim xFormulaTable As DataTable = Nothing

    '        'xFormulaTable = objCmpuMst.Get_Computation_TranVariables(mintPeriodId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE)
    '        'If xFormulaTable.Rows.Count > 0 Then
    '        '    Dim xMatchValue As String = String.Empty
    '        '    For Each eValue As enAssess_Computation_Formulas In [Enum].GetValues(GetType(enAssess_Computation_Formulas))
    '        '        For Each xRow As DataRow In xFormulaTable.Rows
    '        '            xMatchValue = "1000" & eValue
    '        '            Select Case eValue
    '        '                Case enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, enAssess_Computation_Formulas.EMP_OVERALL_SCORE
    '        '                    If xRow.Item("computation_typeid") = xMatchValue Then
    '        '                        mblnIsSelfIncluded = True
    '        '                    End If
    '        '                Case enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, enAssess_Computation_Formulas.ASR_OVERALL_SCORE
    '        '                    If xRow.Item("computation_typeid") = xMatchValue Then
    '        '                        mblnIsAssrIncluded = True
    '        '                    End If
    '        '                Case enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, enAssess_Computation_Formulas.REV_OVERALL_SCORE
    '        '                    If xRow.Item("computation_typeid") = xMatchValue Then
    '        '                        mblnIsRevrIncluded = True
    '        '                    End If
    '        '            End Select
    '        '        Next
    '        '    Next
    '        'End If
    '        'objCmpuMst = Nothing

    '        Dim strEmpids = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("EId").ToString).ToArray)
    '        Dim dtComputationScore As DataTable = (New clsComputeScore_master).GetComputeScore(strEmpids, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
    '        'Shani (24-May-2016) -- End

    '        For Each dtRow As DataRow In dsList.Tables("List").Rows

    '            'Shani (24-May-2016) -- Start
    '            'Call SetScoreValue(dtRow, mblnIsSelfIncluded, mblnIsAssrIncluded, mblnIsRevrIncluded, mdtEndDate)
    '            Dim xFormulaScore As Decimal = 0

    '            xFormulaScore = 0
    '            Dim dRow As DataRow = dtRow
    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dRow("EId")) _
    '                                                                             And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
    '                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Self Assessment") = Format(xFormulaScore, "#####0.#0")

    '            xFormulaScore = 0

    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dRow("EId")) _
    '                                                                             And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE _
    '                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Assessor(s) Assessment") = Format(xFormulaScore, "#####0.#0")

    '            xFormulaScore = 0
    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim dxRow As DataRow = dtRow
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dxRow("EId")) _
    '                                                                             And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE _
    '                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Reviewer Assessment") = Format(xFormulaScore, "#####0.#0")

    '            xFormulaScore = 0
    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim dxRow As DataRow = dtRow
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dxRow("EId")) _
    '                                                                             And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
    '                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Self BSC") = Format(xFormulaScore, "#####0.#0")

    '            xFormulaScore = 0
    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim dxRow As DataRow = dtRow
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dxRow("EId")) _
    '                                                                             And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE _
    '                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Assessor(s) BSC") = Format(xFormulaScore, "#####0.#0")

    '            xFormulaScore = 0
    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim dxRow As DataRow = dtRow
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dxRow("EId")) _
    '                                                                             And x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
    '                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Reviewer BSC") = Format(xFormulaScore, "#####0.#0")

    '            xFormulaScore = 0
    '            If dtComputationScore IsNot Nothing AndAlso dtComputationScore.Rows.Count > 0 Then
    '                Dim dxRow As DataRow = dtRow
    '                Dim xScore = dtComputationScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dxRow("EId"))) _
    '                                                             .Select(Function(x) x.Field(Of Decimal)("finaloverallscore")).ToList
    '                If xScore.Count > 0 Then
    '                    xFormulaScore = xScore(0)
    '                End If
    '            End If
    '            dtRow.Item("Score") = CDec(Format(CDec(xFormulaScore), "#####0.#0"))
    '            'Shani (24-May-2016) -- End




    '            If dtRating.Rows.Count > 0 Then
    '                If dtRow.Item("Score").ToString.Trim.Length > 0 Then
    '                    Dim dtmp() As DataRow = dtRating.Select(CDec(dtRow.Item("Score")) & " >= score_from AND " & CDec(dtRow.Item("Score")) & " <= score_to ")
    '                    If dtmp.Length > 0 Then
    '                        dtRow.Item("Profile") = dtmp(0).Item("grade_award")
    '                    End If
    '                End If
    '            End If

    '            If mblnOnProbationSelected = True Then
    '                If dtRow.Item("Score").ToString.Trim.Length > 0 AndAlso CDec(dtRow.Item("Score")) <= 0 Then
    '                    Dim mstrFilterCondition As String = "[Date Hired] " & mstrCondition & eZeeDate.convertDate(mdtAppointmentDate)
    '                    If dsList.Tables(0).Select(mstrFilterCondition & " AND [Employee Code] = '" & dtRow("Employee Code") & "'").Length > 0 Then
    '                        dtRow.Item("Profile") = Language.getMessage(mstrModuleName, 19, "On Probation")
    '                    End If
    '                End If
    '            End If

    '            dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
    '        Next

    '        dsList.Tables("List").Columns.Remove("aDate")
    '        dsList.Tables("List").Columns.Remove("EId")
    '        dsList.Tables("List").Columns.Remove("PId")
    '        dsList.Tables("List").Columns.Remove("AsrMstId")
    '        dsList.Tables("List").Columns.Remove("RevMstId")

    '        If ConfigParameter._Object._IsCompanyNeedReviewer = False Then
    '            dsList.Tables("List").Columns.Remove("Reviewer Assessment")
    '            dsList.Tables("List").Columns.Remove("Reviewer BSC")
    '        End If

    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            Select Case i
    '                Case 0
    '                    intArrayColumnWidth(i) = 80
    '                Case 1, 2, 3, 4, 7
    '                    intArrayColumnWidth(i) = 125
    '                Case Else
    '                    intArrayColumnWidth(i) = 50
    '            End Select
    '        Next

    '        Dim iExFliter As String = String.Empty
    '        Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")
    '        iExFliter = mstrCaption & " " & mstrCondition & " " & mdtAppointmentDate.Date.ToShortDateString

    '        'S.SANDEEP [13-JUL-2017] -- START
    '        'ISSUE/ENHANCEMENT : 
    '        'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing)
    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing)
    '        'S.SANDEEP [13-JUL-2017] -- END

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Report_Allocation_Based; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Public Sub Report_Allocation_Based_New(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal strExportPath As String, _
                                           ByVal blnOpenAfterExport As Boolean, _
                                           ByVal blnIsCalibrationSettingActive As Boolean) 'S.SANDEEP |27-MAY-2019| -- START {blnIsCalibrationSettingActive} -- END

        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim dtRating As DataTable
        Dim exForce As Exception
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyUnkid
            mDecRoundingFactor = objConfig._PAScoringRoudingFactor
            objConfig = Nothing
            'S.SANDEEP |24-DEC-2019| -- END

            dtPeriodStart = mdtStartDate : dtPeriodEnd = mdtEndDate
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            'S.SANDEEP |11-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}


            ''S.SANDEEP |27-MAY-2019| -- START
            ''ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
            '    StrQ = "DECLARE @Setting AS BIT ; SET @Setting = 0 "
            'ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
            '    StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " "
            'End If
            ''S.SANDEEP |27-MAY-2019| -- END

            'StrQ &= "SELECT " & _
            '        "	 employeecode AS [Employee Code] "
            'If mblnFirstNamethenSurname = False Then
            '    StrQ &= ", ISNULL(surname, '') + ' ' " & _
            '                "+ ISNULL(firstname, '') + ' ' " & _
            '                "+ ISNULL(othername, '') AS [Employee Name] "
            'Else
            '    StrQ &= ", ISNULL(firstname, '') + ' ' " & _
            '                "+ ISNULL(othername, '') + ' ' " & _
            '                "+ ISNULL(surname, '') AS [Employee Name] "
            'End If
            'If mblnShowScaleOnReport Then
            '    StrQ &= "   ,ISNULL(SAL.Scale,0) AS Scale "
            'End If
            ''S.SANDEEP |27-MAY-2019| -- START
            ''ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]

            ''S.SANDEEP |21-AUG-2019| -- START
            ''ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            ''StrQ &= mstrSelectedCols & " " & _
            ''        "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
            ''        "   ,CAST(ISNULL(CFS.finaloverallscore, 0) AS DECIMAL(30, 2)) AS Score " & _
            ''        "   ,CASE WHEN CAST(ISNULL(CFS.finaloverallscore, 0) AS DECIMAL(30, 2)) <= 0 THEN '' ELSE ISNULL(CFS.PF, '') END AS [Profile] " & _
            ''        "   ,CAST(ISNULL(scr.SA,0) AS DECIMAL(30,2)) AS [Self Assessment] " & _
            ''        "   ,CAST(ISNULL(scr.AA,0) AS DECIMAL(30,2)) AS [Assessor(s) Assessment] " & _
            ''        "   ,CAST(ISNULL(scr.RA,0) AS DECIMAL(30,2)) AS [Reviewer Assessment] " & _
            ''        "   ,CAST(ISNULL(scr.SB,0) AS DECIMAL(30,2)) AS [Self BSC] " & _
            ''        "   ,CAST(ISNULL(scr.AB,0) AS DECIMAL(30,2))AS [Assessor(s) BSC] " & _
            ''        "   ,CAST(ISNULL(scr.RB,0) AS DECIMAL(30,2)) AS [Reviewer BSC] " & _
            ''        "	,hremployee_master.employeeunkid AS EId " & _
            ''        "	,'" & mintPeriodId & "' AS PId " & _
            ''        "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
            ''        "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
            ''        "   ,appointeddate AS aDate " & _
            ''        "FROM hremployee_master " & _
            ''        mstrSelectedJoin & " " & _
            ''        "LEFT JOIN " & _
            ''        "( " & _
            ''        "   SELECT DISTINCT " & _
            ''        "        CSM.employeeunkid " & _
            ''        "       ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            ''        "             WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            ''        "             WHEN @Setting = 0 THEN CSM.finaloverallscore END AS finaloverallscore " & _
            ''        "       ,ISNULL((SELECT TOP 1 grade_award " & _
            ''        "                FROM hrapps_ratings " & _
            ''        "                WHERE isvoid = 0 " & _
            ''        "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            ''        "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            ''        "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) >= score_from " & _
            ''        "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            ''        "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            ''        "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) <= score_to " & _
            ''        "        ), '') AS PF " & _
            ''        "   FROM hrassess_compute_score_master AS CSM " & _
            ''        "   WHERE CSM.isvoid = 0 " & _
            ''        "   AND CSM.periodunkid = @periodunkid " & _
            ''        ") AS CFS ON CFS.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrSelectedCols & " " & _
            '        "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
            '        "   ,CAST(ISNULL(CFS.finaloverallscore, 0) AS DECIMAL(36, 2)) AS Score " & _
            '        "   ,CASE WHEN CAST(ISNULL(CFS.finaloverallscore, 0) AS DECIMAL(36, 2)) <= 0 THEN '' ELSE ISNULL(CFS.PF, '') END AS [Profile] " & _
            '        "   ,CAST(ISNULL(scr.SA,0) AS DECIMAL(36,2)) AS [Self Assessment] " & _
            '        "   ,CAST(ISNULL(scr.AA,0) AS DECIMAL(36,2)) AS [Assessor(s) Assessment] " & _
            '        "   ,CAST(ISNULL(scr.RA,0) AS DECIMAL(36,2)) AS [Reviewer Assessment] " & _
            '        "   ,CAST(ISNULL(scr.SB,0) AS DECIMAL(36,2)) AS [Self BSC] " & _
            '        "   ,CAST(ISNULL(scr.AB,0) AS DECIMAL(36,2)) AS [Assessor(s) BSC] " & _
            '        "   ,CAST(ISNULL(scr.RB,0) AS DECIMAL(36,2)) AS [Reviewer BSC] " & _
            '        "	,hremployee_master.employeeunkid AS EId " & _
            '        "	,'" & mintPeriodId & "' AS PId " & _
            '        "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
            '        "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
            '        "   ,appointeddate AS aDate " & _
            '        "FROM hremployee_master " & _
            '        mstrSelectedJoin & " " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT DISTINCT " & _
            '        "        CSM.employeeunkid " & _
            '        "       ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            '        "             WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            '        "             WHEN @Setting = 0 THEN CSM.finaloverallscore END AS finaloverallscore " & _
            '        "       ,ISNULL((SELECT TOP 1 grade_award " & _
            '        "                FROM hrapps_ratings " & _
            '        "                WHERE isvoid = 0 " & _
            '        "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            '        "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            '        "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) >= score_from " & _
            '        "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
            '        "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
            '        "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) <= score_to " & _
            '        "        ), '') AS PF " & _
            '        "   FROM hrassess_compute_score_master AS CSM " & _
            '        "   WHERE CSM.isvoid = 0 " & _
            '        "   AND CSM.periodunkid = @periodunkid " & _
            '        ") AS CFS ON CFS.employeeunkid = hremployee_master.employeeunkid "
            ''S.SANDEEP |21-AUG-2019| -- END


            ''StrQ &= mstrSelectedCols & " " & _
            ''        "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
            ''        "   ,CAST(ISNULL(scr.FS,0) AS DECIMAL(30, 2)) AS Score " & _
            ''        "   ,CASE WHEN ISNULL(scr.SA,0) <= 0 AND ISNULL(scr.AA,0) <= 0 AND ISNULL(scr.RA,0) <= 0 AND ISNULL(scr.SB,0) <= 0 AND ISNULL(scr.AB,0) <= 0 AND ISNULL(scr.RB,0) <= 0 THEN '' ELSE ISNULL(scr.PF,'') END AS [Profile] " & _
            ''        "   ,CAST(ISNULL(scr.SA,0) AS DECIMAL(30,2)) AS [Self Assessment] " & _
            ''        "   ,CAST(ISNULL(scr.AA,0) AS DECIMAL(30,2)) AS [Assessor(s) Assessment] " & _
            ''        "   ,CAST(ISNULL(scr.RA,0) AS DECIMAL(30,2)) AS [Reviewer Assessment] " & _
            ''        "   ,CAST(ISNULL(scr.SB,0) AS DECIMAL(30,2)) AS [Self BSC] " & _
            ''        "   ,CAST(ISNULL(scr.AB,0) AS DECIMAL(30,2)) AS [Assessor(s) BSC] " & _
            ''        "   ,CAST(ISNULL(scr.RB,0) AS DECIMAL(30,2)) AS [Reviewer BSC] " & _
            ''        "	,hremployee_master.employeeunkid AS EId " & _
            ''        "	,'" & mintPeriodId & "' AS PId " & _
            ''        "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
            ''        "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
            ''        "   ,appointeddate AS aDate " & _
            ''        "FROM hremployee_master " & _
            ''        mstrSelectedJoin & " "
            ''StrQ &= "LEFT JOIN " & _
            ''          "( " & _
            ''               "SELECT " & _
            ''                    " hremployee_master.employeeunkid " & _
            ''                    ",ISNULL(SA.[Self Assessment],0) AS SA " & _
            ''                    ",ISNULL(AA.[Assessor(s) Assessment],0) AS AA " & _
            ''                    ",ISNULL(RA.[Reviewer Assessment],0) AS RA " & _
            ''                    ",ISNULL(SB.[Self BSC],0) AS SB " & _
            ''                    ",ISNULL(AB.[Assessor(s) BSC],0) AS AB " & _
            ''                    ",ISNULL(RB.[Reviewer BSC],0) AS RB " & _
            ''                    ",ISNULL(SA.finaloverallscore,0) AS FS " & _
            ''                    ",ISNULL((SELECT TOP 1 grade_award FROM hrapps_ratings WHERE isvoid = 0 AND ISNULL(SA.finaloverallscore,0) >= score_from AND ISNULL(SA.finaloverallscore,0) <= score_to),'') AS PF " & _
            ''               "FROM hremployee_master " & _
            ''               "LEFT JOIN " & _
            ''               "( " & _
            ''                    "SELECT " & _
            ''                         "hrassess_compute_score_master.employeeunkid " & _
            ''                         ",hrassess_compute_score_master.finaloverallscore " & _
            ''                         ",hrassess_compute_score_tran.competency_value " & _
            ''                         ",CASE WHEN formula_typeid = 4 AND assessmodeid = 1 THEN formula_value END AS [Self Assessment] " & _
            ''                    "FROM hrassess_compute_score_tran " & _
            ''                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            ''                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            ''                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            ''                    "AND assessmodeid = 1 AND formula_typeid = 4 " & _
            ''               ") AS SA ON SA.employeeunkid = hremployee_master.employeeunkid " & _
            ''               "LEFT JOIN " & _
            ''               "( " & _
            ''                    "SELECT " & _
            ''                          "hrassess_compute_score_master.employeeunkid " & _
            ''                         ",hrassess_compute_score_master.finaloverallscore " & _
            ''                         ",hrassess_compute_score_tran.competency_value " & _
            ''                         ",CASE WHEN formula_typeid = 5 AND assessmodeid = 2 THEN formula_value END AS [Assessor(s) Assessment] " & _
            ''                    "FROM hrassess_compute_score_tran " & _
            ''                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            ''                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            ''                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            ''                    "AND assessmodeid = 2 AND formula_typeid = 5 " & _
            ''               ") AS AA ON AA.employeeunkid = hremployee_master.employeeunkid " & _
            ''               "LEFT JOIN " & _
            ''               "( " & _
            ''                    "SELECT " & _
            ''                          "hrassess_compute_score_master.employeeunkid " & _
            ''                         ",hrassess_compute_score_master.finaloverallscore " & _
            ''                         ",hrassess_compute_score_tran.competency_value " & _
            ''                         ",CASE WHEN formula_typeid = 6 AND assessmodeid = 3 THEN formula_value END AS [Reviewer Assessment] " & _
            ''                    "FROM hrassess_compute_score_tran " & _
            ''                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            ''                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            ''                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            ''                    "AND assessmodeid = 3 AND formula_typeid = 6 " & _
            ''               ") AS RA ON RA.employeeunkid = hremployee_master.employeeunkid " & _
            ''               "LEFT JOIN " & _
            ''               "( " & _
            ''                    "SELECT " & _
            ''                          "hrassess_compute_score_master.employeeunkid " & _
            ''                         ",hrassess_compute_score_master.finaloverallscore " & _
            ''                         ",hrassess_compute_score_tran.competency_value " & _
            ''                         ",CASE WHEN formula_typeid = 1 AND assessmodeid = 1 THEN formula_value END AS [Self BSC] " & _
            ''                    "FROM hrassess_compute_score_tran " & _
            ''                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            ''                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            ''                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            ''                    "AND assessmodeid = 1 AND formula_typeid = 1 " & _
            ''               ") AS SB ON SB.employeeunkid = hremployee_master.employeeunkid " & _
            ''               "LEFT JOIN " & _
            ''               "( " & _
            ''                    "SELECT " & _
            ''                          "hrassess_compute_score_master.employeeunkid " & _
            ''                         ",hrassess_compute_score_master.finaloverallscore " & _
            ''                         ",hrassess_compute_score_tran.competency_value " & _
            ''                         ",CASE WHEN formula_typeid = 2 AND assessmodeid = 2 THEN formula_value END AS [Assessor(s) BSC] " & _
            ''                    "FROM hrassess_compute_score_tran " & _
            ''                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            ''                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            ''                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            ''                    "AND assessmodeid = 2 AND formula_typeid = 2 " & _
            ''               ") AS AB ON AB.employeeunkid = hremployee_master.employeeunkid " & _
            ''               "LEFT JOIN " & _
            ''               "( " & _
            ''                    "SELECT " & _
            ''                          "hrassess_compute_score_master.employeeunkid " & _
            ''                         ",hrassess_compute_score_master.finaloverallscore " & _
            ''                         ",hrassess_compute_score_tran.competency_value " & _
            ''                         ",CASE WHEN formula_typeid = 3 AND assessmodeid = 3 THEN formula_value END AS [Reviewer BSC] " & _
            ''                    "FROM hrassess_compute_score_tran " & _
            ''                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            ''                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            ''                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            ''                    "AND assessmodeid = 3 AND formula_typeid = 3 " & _
            ''               ") AS RB ON RB.employeeunkid = hremployee_master.employeeunkid " & _
            ''          ") AS scr ON scr.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= "LEFT JOIN " & _
            '          "( " & _
            '               "SELECT " & _
            '                    " hremployee_master.employeeunkid " & _
            '                    ",ISNULL(SA.[Self Assessment],0) AS SA " & _
            '                    ",ISNULL(AA.[Assessor(s) Assessment],0) AS AA " & _
            '                    ",ISNULL(RA.[Reviewer Assessment],0) AS RA " & _
            '                    ",ISNULL(SB.[Self BSC],0) AS SB " & _
            '                    ",ISNULL(AB.[Assessor(s) BSC],0) AS AB " & _
            '                    ",ISNULL(RB.[Reviewer BSC],0) AS RB " & _
            '                    ",ISNULL(SA.finaloverallscore,0) AS FS " & _
            '                    ",ISNULL((SELECT TOP 1 grade_award FROM hrapps_ratings WHERE isvoid = 0 AND ISNULL(SA.finaloverallscore,0) >= score_from AND ISNULL(SA.finaloverallscore,0) <= score_to),'') AS PF " & _
            '               "FROM hremployee_master " & _
            '               "LEFT JOIN " & _
            '               "( " & _
            '                    "SELECT " & _
            '                         "hrassess_compute_score_master.employeeunkid " & _
            '                         ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
            '                         "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
            '                         "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
            '                         " END AS finaloverallscore " & _
            '                         ",hrassess_compute_score_tran.competency_value " & _
            '                         ",CASE WHEN formula_typeid = 4 AND assessmodeid = 1 THEN formula_value END AS [Self Assessment] " & _
            '                    "FROM hrassess_compute_score_tran " & _
            '                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            '                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            '                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                    "AND assessmodeid = 1 AND formula_typeid = 4 " & _
            '               ") AS SA ON SA.employeeunkid = hremployee_master.employeeunkid " & _
            '               "LEFT JOIN " & _
            '               "( " & _
            '                    "SELECT " & _
            '                          "hrassess_compute_score_master.employeeunkid " & _
            '                         ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
            '                         "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
            '                         "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
            '                         " END AS finaloverallscore " & _
            '                         ",hrassess_compute_score_tran.competency_value " & _
            '                         ",CASE WHEN formula_typeid = 5 AND assessmodeid = 2 THEN formula_value END AS [Assessor(s) Assessment] " & _
            '                    "FROM hrassess_compute_score_tran " & _
            '                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            '                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            '                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                    "AND assessmodeid = 2 AND formula_typeid = 5 " & _
            '               ") AS AA ON AA.employeeunkid = hremployee_master.employeeunkid " & _
            '               "LEFT JOIN " & _
            '               "( " & _
            '                    "SELECT " & _
            '                          "hrassess_compute_score_master.employeeunkid " & _
            '                         ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
            '                         "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
            '                         "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
            '                         " END AS finaloverallscore " & _
            '                         ",hrassess_compute_score_tran.competency_value " & _
            '                         ",CASE WHEN formula_typeid = 6 AND assessmodeid = 3 THEN formula_value END AS [Reviewer Assessment] " & _
            '                    "FROM hrassess_compute_score_tran " & _
            '                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            '                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            '                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                    "AND assessmodeid = 3 AND formula_typeid = 6 " & _
            '               ") AS RA ON RA.employeeunkid = hremployee_master.employeeunkid " & _
            '               "LEFT JOIN " & _
            '               "( " & _
            '                    "SELECT " & _
            '                          "hrassess_compute_score_master.employeeunkid " & _
            '                         ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
            '                         "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
            '                         "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
            '                         " END AS finaloverallscore " & _
            '                         ",hrassess_compute_score_tran.competency_value " & _
            '                         ",CASE WHEN formula_typeid = 1 AND assessmodeid = 1 THEN formula_value END AS [Self BSC] " & _
            '                    "FROM hrassess_compute_score_tran " & _
            '                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            '                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            '                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                    "AND assessmodeid = 1 AND formula_typeid = 1 " & _
            '               ") AS SB ON SB.employeeunkid = hremployee_master.employeeunkid " & _
            '               "LEFT JOIN " & _
            '               "( " & _
            '                    "SELECT " & _
            '                          "hrassess_compute_score_master.employeeunkid " & _
            '                         ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
            '                         "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
            '                         "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
            '                         " END AS finaloverallscore " & _
            '                         ",hrassess_compute_score_tran.competency_value " & _
            '                         ",CASE WHEN formula_typeid = 2 AND assessmodeid = 2 THEN formula_value END AS [Assessor(s) BSC] " & _
            '                    "FROM hrassess_compute_score_tran " & _
            '                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            '                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            '                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                    "AND assessmodeid = 2 AND formula_typeid = 2 " & _
            '               ") AS AB ON AB.employeeunkid = hremployee_master.employeeunkid " & _
            '               "LEFT JOIN " & _
            '               "( " & _
            '                    "SELECT " & _
            '                          "hrassess_compute_score_master.employeeunkid " & _
            '                         ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
            '                         "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
            '                         "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
            '                         " END AS finaloverallscore " & _
            '                         ",hrassess_compute_score_tran.competency_value " & _
            '                         ",CASE WHEN formula_typeid = 3 AND assessmodeid = 3 THEN formula_value END AS [Reviewer BSC] " & _
            '                    "FROM hrassess_compute_score_tran " & _
            '                         "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
            '                         "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
            '                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
            '                    "AND assessmodeid = 3 AND formula_typeid = 3 " & _
            '               ") AS RB ON RB.employeeunkid = hremployee_master.employeeunkid " & _
            '          ") AS scr ON scr.employeeunkid = hremployee_master.employeeunkid "
            ''S.SANDEEP |27-MAY-2019| -- END

            'If mblnShowScaleOnReport Then
            '    StrQ &= "LEFT JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        newscale AS Scale " & _
            '            "       ,employeeunkid AS EmpId " & _
            '            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
            '            "   FROM prsalaryincrement_tran " & _
            '            "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
            '            "   AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '            ") AS SAL ON SAL.EmpId = hremployee_master.employeeunkid AND SAL.rno = 1 "
            'End If
            ''S.SANDEEP [17 DEC 2016] -- END
            'StrQ &= "LEFT JOIN " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 hrassessor_master.assessormasterunkid AS AsrMId " & _
            '        "		,hrassessor_tran.employeeunkid AS AEId " & _
            '        "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
            '        "	FROM hrassessor_tran " & _
            '        "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '        "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
            '        "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
            '        "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	WHERE hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '        ") AS ASR ON ASR.AEId = hremployee_master.employeeunkid AND ASR.analysisunkid > 0 " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 hrassessor_master.assessormasterunkid AS RevMId " & _
            '        "		,hrassessor_tran.employeeunkid AS REId " & _
            '        "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
            '        "	FROM hrassessor_tran " & _
            '        "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '        "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.reviewerunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
            '        "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
            '        "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	WHERE hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '        ") AS REV ON REV.REId = hremployee_master.employeeunkid AND REV.analysisunkid > 0 "


            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            'StrQ &= "WHERE isapproved = 1 "

            'If mstrAllocationIds.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAllocationIds
            'End If

            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvanceFilter
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry & " "
            'End If

            'If xDateFilterQry.Trim.Length > 0 Then
            '    StrQ &= xDateFilterQry & " "
            'End If

            'If mblnOnProbationSelected = True Then
            '    StrQ &= " AND hremployee_master.employeeunkid " & _
            '            "NOT IN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "	    Probation.employeeunkid " & _
            '            "   FROM " & _
            '            "   ( " & _
            '            "	    SELECT " & _
            '            "		     employeeunkid " & _
            '            "		    ,CONVERT(CHAR(8),date1,112) AS PFD " & _
            '            "		    ,CONVERT(CHAR(8),date2,112) AS PTD " & _
            '            "		    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
            '            "	    FROM hremployee_dates_tran " & _
            '            "	    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
            '            "	    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
            '            "   ) AS Probation WHERE Probation.Rno = 1 " & _
            '            "   AND Probation.PFD <= '" & eZeeDate.convertDate(mdtEndDate) & "' AND Probation.PTD >= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
            '            ")"
            'End If

            'If Me.OrderByQuery <> "" Then
            '    StrQ &= "ORDER BY " & Me.OrderByQuery
            'End If

            StrQ = GetCommonQueryString(mintPeriodId, "", dtPeriodEnd, xDateJoinQry, xUACQry, xAdvanceJoinQry, xUACFiltrQry, xDateFilterQry)
            'S.SANDEEP |11-SEP-2019| -- END

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim objRating As New clsAppraisal_Rating
            dtRating = objRating._DataTable
            objRating = Nothing

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, mDecRoundingFactor, dtRating))
            'S.SANDEEP |24-DEC-2019| -- END

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If mblnOnProbationSelected = True Then
                    If dtRow.Item("Score").ToString.Trim.Length > 0 AndAlso CDec(dtRow.Item("Score")) <= 0 Then
                        If mdtAppointmentDate <> Nothing Then
                            Dim mstrFilterCondition As String = "[Date Hired] " & mstrCondition & eZeeDate.convertDate(mdtAppointmentDate)
                            If dsList.Tables(0).Select(mstrFilterCondition & " AND [Employee Code] = '" & dtRow("Employee Code") & "'").Length > 0 Then
                                dtRow.Item("Profile") = Language.getMessage(mstrModuleName, 19, "On Probation")
                            End If
                        End If
                    End If
                End If
                dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
            Next

            dsList.Tables("List").Columns.Remove("aDate")
            dsList.Tables("List").Columns.Remove("EId")
            dsList.Tables("List").Columns.Remove("PId")
            dsList.Tables("List").Columns.Remove("AsrMstId")
            dsList.Tables("List").Columns.Remove("RevMstId")
            'S.SANDEEP |11-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}
            dsList.Tables("List").Columns.Remove("PName")
            'S.SANDEEP |11-SEP-2019| -- END

            If ConfigParameter._Object._IsCompanyNeedReviewer = False Then
                dsList.Tables("List").Columns.Remove("Reviewer Assessment")
                dsList.Tables("List").Columns.Remove("Reviewer BSC")
            End If

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 80
                    Case 1, 2, 3, 4, 7
                        intArrayColumnWidth(i) = 125
                    Case Else
                        intArrayColumnWidth(i) = 50
                End Select
            Next

            Dim iExFliter As String = String.Empty
            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")
            If mdtAppointmentDate <> Nothing Then iExFliter = mstrCaption & " " & mstrCondition & " " & mdtAppointmentDate.Date.ToShortDateString
            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            iExFliter &= ", " & Language.getMessage(mstrModuleName, 26, "Display Score Type") & " : " & mstrDisplayScoreName
            'S.SANDEEP |27-MAY-2019| -- END

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Report_Allocation_Based; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19-JUL-2017] -- END

    'S.SANDEEP |24-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
    Private Function PerformingRounding(ByVal x As DataRow, ByVal dblFactor As Decimal, ByVal dtRatings As DataTable) As Boolean
        Try
            If IsDBNull(x("Score")) = False Then
                x("Score") = Rounding.BRound(CDec(x("Score")), dblFactor)
            Else
                x("Score") = 0
            End If
            If IsDBNull(x("Self Assessment")) = False Then
                x("Self Assessment") = Rounding.BRound(CDec(x("Self Assessment")), dblFactor)
            Else
                x("Self Assessment") = 0
            End If
            If IsDBNull(x("Assessor(s) Assessment")) = False Then
                x("Assessor(s) Assessment") = Rounding.BRound(CDec(x("Assessor(s) Assessment")), dblFactor)
            Else
                x("Assessor(s) Assessment") = 0
            End If
            If IsDBNull(x("Reviewer Assessment")) = False Then
                x("Reviewer Assessment") = Rounding.BRound(CDec(x("Reviewer Assessment")), dblFactor)
            Else
                x("Reviewer Assessment") = 0
            End If
            If IsDBNull(x("Self BSC")) = False Then
                x("Self BSC") = Rounding.BRound(CDec(x("Self BSC")), dblFactor)
            Else
                x("Self BSC") = 0
            End If
            If IsDBNull(x("Assessor(s) BSC")) = False Then
                x("Assessor(s) BSC") = Rounding.BRound(CDec(x("Assessor(s) BSC")), dblFactor)
            Else
                x("Assessor(s) BSC") = 0
            End If
            If IsDBNull(x("Reviewer BSC")) = False Then
                x("Reviewer BSC") = Rounding.BRound(CDec(x("Reviewer BSC")), dblFactor)
            Else
                x("Reviewer BSC") = 0
            End If
            Dim dtmp() As DataRow = Nothing
            dtmp = dtRatings.Select(x("Score") & " >= score_from AND " & x("Score") & "<= score_to")
            If dtmp.Length > 0 Then
                x("Profile") = dtmp(0)("grade_award")
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformingRounding; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |24-DEC-2019| -- END

    Private Sub SetScoreValue(ByVal dRow As DataRow, ByVal mblnIsSelfIncluded As Boolean, ByVal mblnIsAssrIncluded As Boolean, ByVal mblnIsRevrIncluded As Boolean, ByVal dtEmployeeAsOnDate As DateTime, ByVal blnUsedAgreedScore As Boolean)
        'Shani (23-Nov-2016) -- [blnUsedAgreedScore]

        'Private Sub SetScoreValue(ByVal dRow As DataRow, ByVal mblnIsSelfIncluded As Boolean, ByVal mblnIsAssrIncluded As Boolean, ByVal mblnIsRevrIncluded As Boolean)
        'S.SANDEEP [04 JUN 2015] -- END

        Try
            Dim xEvaluation As New clsevaluation_analysis_master
            If dRow IsNot Nothing Then


                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'dRow.Item("Self Assessment") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '                                                                     False, _
                '                                                                     mintScoringOptionId, _
                '                                                                     enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Assessor(s) Assessment") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     False, _
                '                                                                     mintScoringOptionId, _
                '                                                                     enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Reviewer Assessment") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                     False, _
                '                                                                     mintScoringOptionId, _
                '                                                                     enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                ''S.SANDEEP [27 Jan 2016] -- START
                ''dRow.Item("Self BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                ''                                                                     False, _
                ''                                                                     mintScoringOptionId, _
                ''                                                                     enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
                ''                                                                     dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                ''dRow.Item("Assessor(s) BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                ''                                                                     False, _
                ''                                                                     mintScoringOptionId, _
                ''                                                                     enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
                ''                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                ''dRow.Item("Reviewer BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                ''                                                                     False, _
                ''                                                                     mintScoringOptionId, _
                ''                                                                     enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
                ''                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END


                'dRow.Item("Self BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '                                                              True, _
                '                                                                     mintScoringOptionId, _
                '                                                                     enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Assessor(s) BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     True, _
                '                                                                     mintScoringOptionId, _
                '                                                                     enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Reviewer BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                     True, _
                '                                                                     mintScoringOptionId, _
                '                                                                     enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                ''S.SANDEEP [27 Jan 2016] -- END

                'Dim xOverallScore As Decimal = 0
                'If mblnIsSelfIncluded Then
                '    xOverallScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                'End If

                'If mblnIsAssrIncluded Then
                '    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                'End If

                'If mblnIsRevrIncluded Then
                '    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                'End If

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("Self Assessment") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=False, _
                '                                                                     xScoreOptId:=mintScoringOptionId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=dRow.Item("EId"), _
                '                                                                     xPeriodId:=dRow.Item("PId"), _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Assessor(s) Assessment") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                            IsBalanceScoreCard:=False, _
                '                                                                            xScoreOptId:=mintScoringOptionId, _
                '                                                                            xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                '                                                                            xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                '                                                                            xEmployeeId:=dRow.Item("EId"), _
                '                                                                            xPeriodId:=dRow.Item("PId"), _
                '                                                                            xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                            xAssessorReviewerId:=dRow.Item("AsrMstId"), _
                '                                                                            xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Reviewer Assessment") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                         IsBalanceScoreCard:=False, _
                '                                                                         xScoreOptId:=mintScoringOptionId, _
                '                                                                         xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                '                                                                         xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                '                                                                         xEmployeeId:=dRow.Item("EId"), _
                '                                                                         xPeriodId:=dRow.Item("PId"), _
                '                                                                         xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                         xAssessorReviewerId:=dRow.Item("RevMstId"), _
                '                                                                         xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Self BSC") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                              IsBalanceScoreCard:=True, _
                '                                                              xScoreOptId:=mintScoringOptionId, _
                '                                                              xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                '                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                '                                                              xEmployeeId:=dRow.Item("EId"), _
                '                                                              xPeriodId:=dRow.Item("PId"), _
                '                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Assessor(s) BSC") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=True, _
                '                                                                     xScoreOptId:=mintScoringOptionId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=dRow.Item("EId"), _
                '                                                                     xPeriodId:=dRow.Item("PId"), _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=dRow.Item("AsrMstId"), _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                'dRow.Item("Reviewer BSC") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                  IsBalanceScoreCard:=True, _
                '                                                                  xScoreOptId:=mintScoringOptionId, _
                '                                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                '                                                                  xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                '                                                                  xEmployeeId:=dRow.Item("EId"), _
                '                                                                  xPeriodId:=dRow.Item("PId"), _
                '                                                                  xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                  xAssessorReviewerId:=dRow.Item("RevMstId"), _
                '                                                                  xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                dRow.Item("Self Assessment") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                                                     IsBalanceScoreCard:=False, _
                                                                                     xScoreOptId:=mintScoringOptionId, _
                                                                                     xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                                                                                     xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                                                     xEmployeeId:=dRow.Item("EId"), _
                                                                                     xPeriodId:=dRow.Item("PId"), _
                                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "###########################0.#0")

                dRow.Item("Assessor(s) Assessment") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                                                            IsBalanceScoreCard:=False, _
                                                                                            xScoreOptId:=mintScoringOptionId, _
                                                                                            xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                                                                                            xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                                                            xEmployeeId:=dRow.Item("EId"), _
                                                                                            xPeriodId:=dRow.Item("PId"), _
                                                                                            xUsedAgreedScore:=blnUsedAgreedScore, _
                                                                                            xAssessorReviewerId:=dRow.Item("AsrMstId"), _
                                                                                            xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "###########################0.#0")

                dRow.Item("Reviewer Assessment") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                                                         IsBalanceScoreCard:=False, _
                                                                                         xScoreOptId:=mintScoringOptionId, _
                                                                                         xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                                                                                         xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                                                         xEmployeeId:=dRow.Item("EId"), _
                                                                                         xPeriodId:=dRow.Item("PId"), _
                                                                                         xUsedAgreedScore:=blnUsedAgreedScore, _
                                                                                         xAssessorReviewerId:=dRow.Item("RevMstId"), _
                                                                                         xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "###########################0.#0")

                dRow.Item("Self BSC") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                                              IsBalanceScoreCard:=True, _
                                                                              xScoreOptId:=mintScoringOptionId, _
                                                                              xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                                              xEmployeeId:=dRow.Item("EId"), _
                                                                              xPeriodId:=dRow.Item("PId"), _
                                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "###########################0.#0")

                dRow.Item("Assessor(s) BSC") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                                                     IsBalanceScoreCard:=True, _
                                                                                     xScoreOptId:=mintScoringOptionId, _
                                                                                     xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                                                                                     xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                                                     xEmployeeId:=dRow.Item("EId"), _
                                                                                     xPeriodId:=dRow.Item("PId"), _
                                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                                                                                     xAssessorReviewerId:=dRow.Item("AsrMstId"), _
                                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "###########################0.#0")

                dRow.Item("Reviewer BSC") = Format(CDec(xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                                                  IsBalanceScoreCard:=True, _
                                                                                  xScoreOptId:=mintScoringOptionId, _
                                                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                                                                                  xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                                                  xEmployeeId:=dRow.Item("EId"), _
                                                                                  xPeriodId:=dRow.Item("PId"), _
                                                                                  xUsedAgreedScore:=blnUsedAgreedScore, _
                                                                                  xAssessorReviewerId:=dRow.Item("RevMstId"), _
                                                                                  xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)), "###########################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END

                Dim xOverallScore As Decimal = 0
                If mblnIsSelfIncluded Then
                    xOverallScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                              IsBalanceScoreCard:=True, _
                                                              xScoreOptId:=mintScoringOptionId, _
                                                              xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                              xEmployeeId:=dRow.Item("EId"), _
                                                              xPeriodId:=dRow.Item("PId"), _
                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                    xOverallScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                              IsBalanceScoreCard:=False, _
                                                              xScoreOptId:=mintScoringOptionId, _
                                                              xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                              xEmployeeId:=dRow.Item("EId"), _
                                                              xPeriodId:=dRow.Item("PId"), _
                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                End If

                If mblnIsAssrIncluded Then
                    xOverallScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                              IsBalanceScoreCard:=True, _
                                                              xScoreOptId:=mintScoringOptionId, _
                                                              xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                              xEmployeeId:=dRow.Item("EId"), _
                                                              xPeriodId:=dRow.Item("PId"), _
                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                              xAssessorReviewerId:=dRow.Item("AsrMstId"), _
                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                    xOverallScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                              IsBalanceScoreCard:=False, _
                                                              xScoreOptId:=mintScoringOptionId, _
                                                              xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                              xEmployeeId:=dRow.Item("EId"), _
                                                              xPeriodId:=dRow.Item("PId"), _
                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                              xAssessorReviewerId:=dRow.Item("AsrMstId"), _
                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                End If

                If mblnIsRevrIncluded Then
                    xOverallScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                              IsBalanceScoreCard:=True, _
                                                              xScoreOptId:=mintScoringOptionId, _
                                                              xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                              xEmployeeId:=dRow.Item("EId"), _
                                                              xPeriodId:=dRow.Item("PId"), _
                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                              xAssessorReviewerId:=dRow.Item("RevMstId"), _
                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                    xOverallScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                              IsBalanceScoreCard:=False, _
                                                              xScoreOptId:=mintScoringOptionId, _
                                                              xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                                                              xEmployeeAsOnDate:=dtEmployeeAsOnDate, _
                                                              xEmployeeId:=dRow.Item("EId"), _
                                                              xPeriodId:=dRow.Item("PId"), _
                                                              xUsedAgreedScore:=blnUsedAgreedScore, _
                                                              xAssessorReviewerId:=dRow.Item("RevMstId"), _
                                                              xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                End If

                'Shani (23-Nov-2016) -- End

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("Score") = CDec(Format(CDec(xOverallScore), "#####0.#0"))
                dRow.Item("Score") = CDec(Format(CDec(xOverallScore), "#########################0.#0"))
                'S.SANDEEP |21-AUG-2019| -- END



                dRow.AcceptChanges()

            End If
            xEvaluation = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetScoreValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [16 SEP 2016] -- START
    Public Sub Report_Based_On_Competencies(ByVal strDatabaseName As String, _
                                            ByVal intUserUnkid As Integer, _
                                            ByVal intYearUnkid As Integer, _
                                            ByVal intCompanyUnkid As Integer, _
                                            ByVal dtPeriodStart As Date, _
                                            ByVal dtPeriodEnd As Date, _
                                            ByVal strUserModeSetting As String, _
                                            ByVal blnOnlyApproved As Boolean, _
                                            ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean) 'S.SANDEEP [13-JUL-2017] -- START {strExportPath,blnOpenAfterExport} -- END
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim dsCategory As New DataSet
        Dim exForce As Exception
        Try
            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid
            'S.SANDEEP [13-JUL-2017] -- END

            dtPeriodStart = mdtStartDate : dtPeriodEnd = mdtEndDate
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT " & _
                   " employeecode AS [Employee Code] "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS [Employee Name] "
            Else
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS [Employee Name] "
            End If
            StrQ &= mstrSelectedCols & " " & _
                    " ,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
                    " ,hremployee_master.employeeunkid " & _
                    "FROM hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        pt.gradegroupunkid " & _
                    "       ,pt.gradeunkid " & _
                    "       ,pt.gradelevelunkid " & _
                    "       ,pt.employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY pt.employeeunkid ORDER BY pt.incrementdate DESC, pt.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran pt " & _
                    "   WHERE pt.isvoid = 0 AND pt.isapproved = 1 AND CONVERT(CHAR(8),pt.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS grds ON hremployee_master.employeeunkid = grds.employeeunkid AND grds.rno = 1 " & _
                     mstrSelectedJoin & " "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE isapproved = 1 "

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAllocationIds
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mblnOnProbationSelected = True Then
                StrQ &= " AND hremployee_master.employeeunkid " & _
                        "NOT IN " & _
                        "( " & _
                        "   SELECT " & _
                        "	    Probation.employeeunkid " & _
                        "   FROM " & _
                        "   ( " & _
                        "	    SELECT " & _
                        "		     employeeunkid " & _
                        "		    ,CONVERT(CHAR(8),date1,112) AS PFD " & _
                        "		    ,CONVERT(CHAR(8),date2,112) AS PTD " & _
                        "		    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "	    FROM hremployee_dates_tran " & _
                        "	    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
                        "	    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        "   ) AS Probation WHERE Probation.Rno = 1 " & _
                        "   AND Probation.PFD <= '" & eZeeDate.convertDate(mdtEndDate) & "' AND Probation.PTD >= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ")"
            End If

            If Me.OrderByQuery <> "" Then
                StrQ &= "ORDER BY " & Me.OrderByQuery
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim intTotalCompetency As Integer = 0

            StrQ = "SELECT " & _
                   "     cfcommon_master.name " & _
                   "    ,'Col_'+ CAST(cfcommon_master.masterunkid AS NVARCHAR(MAX)) AS cols " & _
                   "FROM hrassess_competencies_master " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = @mastertype " & _
                   "WHERE hrassess_competencies_master.periodunkid = @periodunkid AND cfcommon_master.isactive = 1 AND hrassess_competencies_master.isactive = 1 " & _
                   "ORDER BY cfcommon_master.masterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsCategory = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intTotalCompetency = dsCategory.Tables(0).Rows.Count
                For Each dr As DataRow In dsCategory.Tables(0).Rows
                    dsList.Tables(0).Columns.Add(dr.Item("cols"), GetType(Decimal)).Caption = dr.Item("name")
                Next
                dsList.Tables(0).Columns.Add("Average Score", GetType(Decimal)).DefaultValue = 0
                dsList.Tables(0).Columns.Add("Assessor's Comments", GetType(String)).DefaultValue = ""
            End If

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ = "SELECT " & _
            '       "     cfcommon_master.name " & _
            '       "    ,hrevaluation_analysis_master.assessedemployeeunkid " & _
            '       "    ,SUM(hrcompetency_analysis_tran.result) AS result " & _
            '       "    ,'Col_'+ CAST(cfcommon_master.masterunkid AS NVARCHAR(MAX)) AS cols " & _
            '       "    ,hrcompetency_analysis_tran.remark " & _
            '       "FROM hrcompetency_analysis_tran " & _
            '       "    JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '       "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = @mastertype " & _
            '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
            '       "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = @periodunkid " & _
            '       "AND hrevaluation_analysis_master.assessmodeid = @assessmodeid AND hrevaluation_analysis_master.isvoid = 0 " & _
            '       "GROUP BY cfcommon_master.name ,hrevaluation_analysis_master.assessedemployeeunkid,cfcommon_master.masterunkid,hrcompetency_analysis_tran.remark " & _
            '       "ORDER BY hrevaluation_analysis_master.assessedemployeeunkid "

            StrQ = "SELECT " & _
                   "     cfcommon_master.name " & _
                   "    ,hrevaluation_analysis_master.assessedemployeeunkid " & _
                   "    ,CAST(SUM(hrcompetency_analysis_tran.result) AS DECIMAL(36,2)) AS result " & _
                   "    ,'Col_'+ CAST(cfcommon_master.masterunkid AS NVARCHAR(MAX)) AS cols " & _
                   "    ,hrcompetency_analysis_tran.remark " & _
                   "FROM hrcompetency_analysis_tran " & _
                   "    JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = @mastertype " & _
                   "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = @periodunkid " & _
                   "AND hrevaluation_analysis_master.assessmodeid = @assessmodeid AND hrevaluation_analysis_master.isvoid = 0 " & _
                   "GROUP BY cfcommon_master.name ,hrevaluation_analysis_master.assessedemployeeunkid,cfcommon_master.masterunkid,hrcompetency_analysis_tran.remark " & _
                   "ORDER BY hrevaluation_analysis_master.assessedemployeeunkid "
            'S.SANDEEP |21-AUG-2019| -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES))
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enAssessmentMode.APPRAISER_ASSESSMENT))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsCategory = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
                Dim filterrow() As DataRow = dsCategory.Tables(0).Select("assessedemployeeunkid = '" & dtRow.Item("employeeunkid").ToString & "'")
                Dim dblAvgScore As Double = 0 : Dim StrComments As String = "" : Dim intEmpId As Integer = dtRow.Item("employeeunkid")
                If filterrow.Length > 0 Then
                    If intTotalCompetency > 0 Then
                        dblAvgScore = CDec(dsCategory.Tables(0).Compute("SUM(result)", "assessedemployeeunkid = '" & dtRow.Item("employeeunkid").ToString & "'")) / intTotalCompetency
                    End If
                    StrComments = String.Join("#10;", dsCategory.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("assessedemployeeunkid") = intEmpId).Select(Function(x) x.Field(Of String)("remark")).ToArray())
                End If
                For index As Integer = 0 To filterrow.Length - 1
                    'S.SANDEEP [13-JUL-2017] -- START
                    'ISSUE/ENHANCEMENT :
                    'dtRow.Item(filterrow(index)("cols")) = filterrow(index)("result")
                    If dsList.Tables(0).Columns.Contains(filterrow(index)("cols").ToString) Then
                        dtRow.Item(filterrow(index)("cols")) = filterrow(index)("result")
                    End If
                    'S.SANDEEP [13-JUL-2017] -- END
                Next
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dtRow.Item("Average Score") = CDbl(dblAvgScore).ToString("###.#0")
                dtRow.Item("Average Score") = CDbl(dblAvgScore).ToString("########################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END

                If StrComments.StartsWith("#10;") = False Then
                    dtRow.Item("Assessor's Comments") = StrComments
                End If
            Next

            dsList.Tables("List").Columns.Remove("employeeunkid")

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 80
                    Case Else
                        intArrayColumnWidth(i) = 100
                End Select
            Next

            Dim iExFliter As String = String.Empty
            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")
            iExFliter = mstrCaption & " " & mstrCondition & " " & mdtAppointmentDate.Date.ToShortDateString

            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing)
            'S.SANDEEP [13-JUL-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:Report_Based_On_Competencies ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [16 SEP 2016] -- END


    'S.SANDEEP |11-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-884|Ref#0003943}
    Public Sub Report_Allocation_Based_MultiPeriod(ByVal strDatabaseName As String, _
                                                   ByVal intUserUnkid As Integer, _
                                                   ByVal intYearUnkid As Integer, _
                                                   ByVal intCompanyUnkid As Integer, _
                                                   ByVal dtPeriodStart As Date, _
                                                   ByVal dtPeriodEnd As Date, _
                                                   ByVal strUserModeSetting As String, _
                                                   ByVal blnOnlyApproved As Boolean, _
                                                   ByVal strExportPath As String, _
                                                   ByVal blnOpenAfterExport As Boolean, _
                                                   ByVal blnIsCalibrationSettingActive As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim dtRating As DataTable
        Dim exForce As Exception
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyUnkid
            mDecRoundingFactor = objConfig._PAScoringRoudingFactor
            objConfig = Nothing
            'S.SANDEEP |24-DEC-2019| -- END


            dtPeriodStart = mdtStartDate : dtPeriodEnd = mdtEndDate
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim dsFinalList As New DataSet

            For Each intPeriodId As Integer In mdicPeriodChecked.Keys
                objDataOperation.ClearParameters()
                StrQ = GetCommonQueryString(intPeriodId, mdicPeriodChecked(intPeriodId), dtPeriodEnd, xDateJoinQry, xUACQry, xAdvanceJoinQry, xUACFiltrQry, xDateFilterQry)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim objRating As New clsAppraisal_Rating
                dtRating = objRating._DataTable
                objRating = Nothing

                'S.SANDEEP |24-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, mDecRoundingFactor, dtRating))
                'S.SANDEEP |24-DEC-2019| -- END

                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    If mblnOnProbationSelected = True Then
                        If dtRow.Item("Score").ToString.Trim.Length > 0 AndAlso CDec(dtRow.Item("Score")) <= 0 Then
                            If mdtAppointmentDate <> Nothing Then
                                Dim mstrFilterCondition As String = "[Date Hired] " & mstrCondition & eZeeDate.convertDate(mdtAppointmentDate)
                                If dsList.Tables(0).Select(mstrFilterCondition & " AND [Employee Code] = '" & dtRow("Employee Code") & "'").Length > 0 Then
                                    dtRow.Item("Profile") = Language.getMessage(mstrModuleName, 19, "On Probation")
                                End If
                            End If
                        End If
                    End If
                    dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
                Next

                If dsFinalList.Tables.Count > 0 Then
                    dsFinalList.Tables(0).Merge(dsList.Tables(0), True)
                Else
                    dsFinalList = dsList.Copy
                End If

            Next

            dsFinalList.Tables("List").Columns.Remove("aDate")
            dsFinalList.Tables("List").Columns.Remove("EId")
            dsFinalList.Tables("List").Columns.Remove("PId")
            dsFinalList.Tables("List").Columns.Remove("AsrMstId")
            dsFinalList.Tables("List").Columns.Remove("RevMstId")

            dsFinalList.Tables("List").Columns("PName").Caption = Language.getMessage(mstrModuleName, 27, "Period")

            If ConfigParameter._Object._IsCompanyNeedReviewer = False Then
                dsFinalList.Tables("List").Columns.Remove("Reviewer Assessment")
                dsFinalList.Tables("List").Columns.Remove("Reviewer BSC")
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim strGrpCols As String() = {"PName"}
            strarrGroupColumns = strGrpCols

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsFinalList.Tables("List").Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 80
                    Case 1, 2, 3, 4, 7
                        intArrayColumnWidth(i) = 125
                    Case Else
                        intArrayColumnWidth(i) = 50
                End Select
            Next

            Dim iExFliter As String = String.Empty
            Dim iExTitle As String = ""
            If mdtAppointmentDate <> Nothing Then iExFliter = mstrCaption & " " & mstrCondition & " " & mdtAppointmentDate.Date.ToShortDateString
            iExFliter &= ", " & Language.getMessage(mstrModuleName, 26, "Display Score Type") & " : " & mstrDisplayScoreName

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsFinalList.Tables("List"), intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " " & mstrReportType_Name, iExTitle, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Report_Allocation_Based_MultiPeriod; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function GetCommonQueryString(ByVal iPeriodId As Integer, _
                                          ByVal iPeriodName As String, _
                                          ByVal dtPeriodEnd As DateTime, _
                                          ByVal xDateJoinQry As String, _
                                          ByVal xUACQry As String, _
                                          ByVal xAdvanceJoinQry As String, _
                                          ByVal xUACFiltrQry As String, _
                                          ByVal xDateFilterQry As String) As String
        Dim StrQ As String = String.Empty
        Try
            If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = 0 "
            ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " "
            End If
            StrQ &= "SELECT DISTINCT " & _
                    "	 employeecode AS [Employee Code] "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS [Employee Name] "
            Else
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS [Employee Name] "
            End If
            If mblnShowScaleOnReport Then
                StrQ &= "   ,ISNULL(SAL.Scale,0) AS Scale "
            End If
            StrQ &= mstrSelectedCols & " " & _
                    "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
                    "   ,CAST(ISNULL(CFS.finaloverallscore, 0) AS DECIMAL(36, 2)) AS Score " & _
                    "   ,CASE WHEN CAST(ISNULL(CFS.finaloverallscore, 0) AS DECIMAL(36, 2)) <= 0 THEN '' ELSE ISNULL(CFS.PF, '') END AS [Profile] " & _
                    "   ,CAST(ISNULL(scr.SA,0) AS DECIMAL(36,2)) AS [Self Assessment] " & _
                    "   ,CAST(ISNULL(scr.AA,0) AS DECIMAL(36,2)) AS [Assessor(s) Assessment] " & _
                    "   ,CAST(ISNULL(scr.RA,0) AS DECIMAL(36,2)) AS [Reviewer Assessment] " & _
                    "   ,CAST(ISNULL(scr.SB,0) AS DECIMAL(36,2)) AS [Self BSC] " & _
                    "   ,CAST(ISNULL(scr.AB,0) AS DECIMAL(36,2)) AS [Assessor(s) BSC] " & _
                    "   ,CAST(ISNULL(scr.RB,0) AS DECIMAL(36,2)) AS [Reviewer BSC] " & _
                    "	,hremployee_master.employeeunkid AS EId " & _
                    "	,'" & iPeriodId & "' AS PId " & _
                    "   ,'" & iPeriodName & "' AS PName " & _
                    "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
                    "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
                    "   ,appointeddate AS aDate " & _
                    "FROM hremployee_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        pt.gradegroupunkid " & _
                    "       ,pt.gradeunkid " & _
                    "       ,pt.gradelevelunkid " & _
                    "       ,pt.employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY pt.employeeunkid ORDER BY pt.incrementdate DESC, pt.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran pt " & _
                    "   WHERE pt.isvoid = 0 AND pt.isapproved = 1 AND CONVERT(CHAR(8),pt.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS grds ON hremployee_master.employeeunkid = grds.employeeunkid AND grds.rno = 1 " & _
                    mstrSelectedJoin & " " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT DISTINCT " & _
                    "        CSM.employeeunkid " & _
                    "       ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                    "             WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                    "             WHEN @Setting = 0 THEN CSM.finaloverallscore END AS finaloverallscore " & _
                    "       ,ISNULL((SELECT TOP 1 grade_award " & _
                    "                FROM hrapps_ratings " & _
                    "                WHERE isvoid = 0 " & _
                    "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                    "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                    "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) >= score_from " & _
                    "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                    "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                    "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) <= score_to " & _
                    "        ), '') AS PF " & _
                    "   FROM hrassess_compute_score_master AS CSM " & _
                    "   WHERE CSM.isvoid = 0 " & _
                    "   AND CSM.periodunkid = @periodunkid " & _
                    ") AS CFS ON CFS.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                      "( " & _
                           "SELECT " & _
                                " hremployee_master.employeeunkid " & _
                                ",ISNULL(SA.[Self Assessment],0) AS SA " & _
                                ",ISNULL(AA.[Assessor(s) Assessment],0) AS AA " & _
                                ",ISNULL(RA.[Reviewer Assessment],0) AS RA " & _
                                ",ISNULL(SB.[Self BSC],0) AS SB " & _
                                ",ISNULL(AB.[Assessor(s) BSC],0) AS AB " & _
                                ",ISNULL(RB.[Reviewer BSC],0) AS RB " & _
                                ",ISNULL(SA.finaloverallscore,0) AS FS " & _
                                ",ISNULL((SELECT TOP 1 grade_award FROM hrapps_ratings WHERE isvoid = 0 AND ISNULL(SA.finaloverallscore,0) >= score_from AND ISNULL(SA.finaloverallscore,0) <= score_to),'') AS PF " & _
                           "FROM hremployee_master " & _
                           "LEFT JOIN " & _
                           "( " & _
                                "SELECT " & _
                                      "hrassess_compute_score_master.employeeunkid " & _
                                     ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                                     "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                                     "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                                     " END AS finaloverallscore " & _
                                     ",hrassess_compute_score_tran.competency_value " & _
                                     ",CASE WHEN formula_typeid = 4 AND assessmodeid = 1 THEN formula_value END AS [Self Assessment] " & _
                                "FROM hrassess_compute_score_tran " & _
                                     "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                                     "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                                "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                                "AND assessmodeid = 1 AND formula_typeid = 4 " & _
                           ") AS SA ON SA.employeeunkid = hremployee_master.employeeunkid " & _
                           "LEFT JOIN " & _
                           "( " & _
                                "SELECT " & _
                                      "hrassess_compute_score_master.employeeunkid " & _
                                     ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                                     "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                                     "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                                     " END AS finaloverallscore " & _
                                     ",hrassess_compute_score_tran.competency_value " & _
                                     ",CASE WHEN formula_typeid = 5 AND assessmodeid = 2 THEN formula_value END AS [Assessor(s) Assessment] " & _
                                "FROM hrassess_compute_score_tran " & _
                                     "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                                     "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                                "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                                "AND assessmodeid = 2 AND formula_typeid = 5 " & _
                           ") AS AA ON AA.employeeunkid = hremployee_master.employeeunkid " & _
                           "LEFT JOIN " & _
                           "( " & _
                                "SELECT " & _
                                      "hrassess_compute_score_master.employeeunkid " & _
                                     ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                                     "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                                     "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                                     " END AS finaloverallscore " & _
                                     ",hrassess_compute_score_tran.competency_value " & _
                                     ",CASE WHEN formula_typeid = 6 AND assessmodeid = 3 THEN formula_value END AS [Reviewer Assessment] " & _
                                "FROM hrassess_compute_score_tran " & _
                                     "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                                     "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                                "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                                "AND assessmodeid = 3 AND formula_typeid = 6 " & _
                           ") AS RA ON RA.employeeunkid = hremployee_master.employeeunkid " & _
                           "LEFT JOIN " & _
                           "( " & _
                                "SELECT " & _
                                      "hrassess_compute_score_master.employeeunkid " & _
                                     ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                                     "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                                     "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                                     " END AS finaloverallscore " & _
                                     ",hrassess_compute_score_tran.competency_value " & _
                                     ",CASE WHEN formula_typeid = 1 AND assessmodeid = 1 THEN formula_value END AS [Self BSC] " & _
                                "FROM hrassess_compute_score_tran " & _
                                     "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                                     "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                                "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                                "AND assessmodeid = 1 AND formula_typeid = 1 " & _
                           ") AS SB ON SB.employeeunkid = hremployee_master.employeeunkid " & _
                           "LEFT JOIN " & _
                           "( " & _
                                "SELECT " & _
                                      "hrassess_compute_score_master.employeeunkid " & _
                                     ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                                     "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                                     "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                                     " END AS finaloverallscore " & _
                                     ",hrassess_compute_score_tran.competency_value " & _
                                     ",CASE WHEN formula_typeid = 2 AND assessmodeid = 2 THEN formula_value END AS [Assessor(s) BSC] " & _
                                "FROM hrassess_compute_score_tran " & _
                                     "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                                     "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                                "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                                "AND assessmodeid = 2 AND formula_typeid = 2 " & _
                           ") AS AB ON AB.employeeunkid = hremployee_master.employeeunkid " & _
                           "LEFT JOIN " & _
                           "( " & _
                                "SELECT " & _
                                      "hrassess_compute_score_master.employeeunkid " & _
                                     ",CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                                     "      WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                                     "      WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                                     " END AS finaloverallscore " & _
                                     ",hrassess_compute_score_tran.competency_value " & _
                                     ",CASE WHEN formula_typeid = 3 AND assessmodeid = 3 THEN formula_value END AS [Reviewer BSC] " & _
                                "FROM hrassess_compute_score_tran " & _
                                     "JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                                     "JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                                "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 AND hrassess_compute_score_master.periodunkid = @periodunkid " & _
                                "AND assessmodeid = 3 AND formula_typeid = 3 " & _
                           ") AS RB ON RB.employeeunkid = hremployee_master.employeeunkid " & _
                      ") AS scr ON scr.employeeunkid = hremployee_master.employeeunkid "
            If mblnShowScaleOnReport Then
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        newscale AS Scale " & _
                        "       ,employeeunkid AS EmpId " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "   FROM prsalaryincrement_tran " & _
                        "   WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                        "   AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS SAL ON SAL.EmpId = hremployee_master.employeeunkid AND SAL.rno = 1 "
            End If

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 hrassessor_master.assessormasterunkid AS AsrMId " & _
                    "		,hrassessor_tran.employeeunkid AS AEId " & _
                    "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
                    "	FROM hrassessor_tran " & _
                    "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                    "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
                    "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "	WHERE hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    ") AS ASR ON ASR.AEId = hremployee_master.employeeunkid AND ASR.analysisunkid > 0 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 hrassessor_master.assessormasterunkid AS RevMId " & _
                    "		,hrassessor_tran.employeeunkid AS REId " & _
                    "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
                    "	FROM hrassessor_tran " & _
                    "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                    "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.reviewerunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
                    "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "	WHERE hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    ") AS REV ON REV.REId = hremployee_master.employeeunkid AND REV.analysisunkid > 0 "


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE isapproved = 1 "

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAllocationIds
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mblnOnProbationSelected = True Then
                StrQ &= " AND hremployee_master.employeeunkid " & _
                        "NOT IN " & _
                        "( " & _
                        "   SELECT " & _
                        "	    Probation.employeeunkid " & _
                        "   FROM " & _
                        "   ( " & _
                        "	    SELECT " & _
                        "		     employeeunkid " & _
                        "		    ,CONVERT(CHAR(8),date1,112) AS PFD " & _
                        "		    ,CONVERT(CHAR(8),date2,112) AS PTD " & _
                        "		    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "	    FROM hremployee_dates_tran " & _
                        "	    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
                        "	    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        "   ) AS Probation WHERE Probation.Rno = 1 " & _
                        "   AND Probation.PFD <= '" & eZeeDate.convertDate(mdtEndDate) & "' AND Probation.PTD >= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
                        ")"
            End If

            If Me.OrderByQuery <> "" Then
                StrQ &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCommonQueryString; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrQ
    End Function

    Public Function Export_Assessment_Report_MultiPeriod(ByVal dtEmployeeAsOnDate As DateTime, ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean, ByVal intUserId As Integer, ByVal intCompanyId As Integer, ByRef mstrFileName As String) As Boolean
        Try
            If intCompanyId <= 0 Then
                intCompanyId = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyId
            ConfigParameter._Object._Companyunkid = intCompanyId

            If intUserId <= 0 Then
                intUserId = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserId

            Call CreateTable()

            mdtFinalTable.Columns.Add("isgrp", GetType(System.Boolean)).DefaultValue = False

            Dim iCnt As Integer = 0
            For Each iKey As Integer In mdicPeriodChecked.Keys
                If iCnt > 0 Then mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
                Dim dTRow As DataRow = Nothing
                Dim dtComputataion As DataTable = Nothing
                If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                    dtComputataion = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, iKey, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, False)
                ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                    dtComputataion = (New clsComputeScore_master).GetComputeScore(mintEmployeeId, iKey, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, mblnIsCalibrationSettingActive)
                End If
                dTRow = mdtFinalTable.NewRow
                dTRow.Item("referencename") = Language.getMessage(mstrModuleName, 18, "Period :") & " " & mdicPeriodChecked(iKey)
                dTRow.Item("isgrp") = True
                mdtFinalTable.Rows.Add(dTRow)

                dTRow = mdtFinalTable.NewRow
                dTRow = Get_BSC_Evalution(dtEmployeeAsOnDate, dtComputataion)
                mdtFinalTable.Rows.Add(dTRow)

                dTRow = mdtFinalTable.NewRow
                dTRow = Get_GEvaluation(dtEmployeeAsOnDate, dtComputataion)
                mdtFinalTable.Rows.Add(dTRow)

                Call Set_Total(dtEmployeeAsOnDate, dtComputataion)

                Dim objMRating As New clsAppraisal_Rating
                Dim dsMRating = New DataSet : Dim dtMTable As DataTable
                dsMRating = objMRating.getComboList("List", False)
                dtMTable = New DataView(dsMRating.Tables(0), "", "scrt DESC", DataViewRowState.CurrentRows).ToTable.Copy
                dsMRating.Dispose() : objMRating = Nothing

                Dim drow() As DataRow = dtMTable.Select("scrf <= " & mdecFinalScore & " AND scrt >= " & mdecFinalScore & " ")
                Dim strGrade As String = String.Empty
                If drow.Length > 0 Then
                    strGrade = drow(0)("name").ToString()
                End If
                dTRow = mdtFinalTable.NewRow
                dTRow.Item("referencename") = Language.getMessage(mstrModuleName, 28, "OVERALL PERFORMANCE RATING") & " : " & strGrade
                dTRow.Item("isgrp") = True
                mdtFinalTable.Rows.Add(dTRow)

                iCnt += 1
            Next

            Dim strBuilder As New StringBuilder
            Dim blnFlag As Boolean = False
            Dim StrValue As String = ""

            If mblnIsCalibrationSettingActive Then
                StrValue = Language.getMessage(mstrModuleName, 26, "Display Score Type") & " : " & mstrDisplayScoreName
            End If

            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 5, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='5%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='40%' colspan= " & mdtFinalTable.Columns.Count - 1 & " align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='40%' colspan= " & mdtFinalTable.Columns.Count - 1 & " align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & "FORMULAR AND A GUIDE TO OVERALL SCORES RATING" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & "Employee : " & mstrEmployeeName & ", " & StrValue & "</B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To mdtFinalTable.Columns.Count - 2
                strBuilder.Append("<TD BORDER=1 WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & mdtFinalTable.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To mdtFinalTable.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To mdtFinalTable.Columns.Count - 2
                    If CBool(mdtFinalTable.Rows(i)("isgrp")) = True Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK COLSPAN=" & mdtFinalTable.Columns.Count - 1 & "><FONT SIZE=2><B>" & mdtFinalTable.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                        Exit For
                    End If
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & mdtFinalTable.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            Dim objRating As New clsAppraisal_Rating
            Dim dsRating = New DataSet : Dim dtTable As DataTable
            dsRating = objRating.getComboList("List", False)
            dtTable = New DataView(dsRating.Tables(0), "", "scrt DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dsRating.Dispose() : objRating = Nothing
            'strBuilder.Append(" <TR> " & vbCrLf)
            'For Each dr In dtTable.Rows
            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dr("name") & "</B></FONT></TD>" & vbCrLf)
            'Next
            'strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>SCORE<B></FONT></B></TD>" & vbCrLf)
            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>LEVEL OF PERFORMANCE<B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            For Each dr In dtTable.Rows
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr("scrf").ToString & " - " & dr("scrt").ToString & "</FONT></TD>" & vbCrLf)
                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr("name") & "</FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            Dim flFileName As String = Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & mintEmployeeId.ToString()

            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If strExportPath.LastIndexOf("\") = strExportPath.Length - 1 Then
                strExportPath = strExportPath.Remove(strExportPath.LastIndexOf("\"))
            End If

            If SaveExcelfile(strExportPath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = strExportPath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            mstrFileName = Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & mintEmployeeId.ToString()
            Call ReportFunction.Open_ExportedFile(blnOpenAfterExport, StrFinalPath)

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Assessment_Report_MultiPeriod; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |11-SEP-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Self")
            Language.setMessage(mstrModuleName, 2, "Assessor")
            Language.setMessage(mstrModuleName, 3, "Reviewer")
            Language.setMessage(mstrModuleName, 5, "Prepared By :")
            Language.setMessage(mstrModuleName, 6, "Date :")
            Language.setMessage(mstrModuleName, 7, "Report successfully exported to Report export path")
            Language.setMessage(mstrModuleName, 8, "Weighted Average By Employee,Assessor,Reviewer.")
            Language.setMessage(mstrModuleName, 9, "Weighted Average By Employee,Assessor.")
            Language.setMessage(mstrModuleName, 10, "Weighted Average By Employee,Reviewer.")
            Language.setMessage(mstrModuleName, 11, "Weighted Average By Assessor,Reviewer.")
            Language.setMessage(mstrModuleName, 12, "Weighted Average By Employee.")
            Language.setMessage(mstrModuleName, 13, "Weighted Average By Assessor.")
            Language.setMessage(mstrModuleName, 14, "Weighted Average By Reviewer.")
            Language.setMessage(mstrModuleName, 15, "Weighted Score By Employee.")
            Language.setMessage(mstrModuleName, 16, "Weighted Score By Assessor.")
            Language.setMessage(mstrModuleName, 17, "Weighted Score By Reviewer.")
            Language.setMessage(mstrModuleName, 18, "Period :")
            Language.setMessage(mstrModuleName, 19, "On Probation")
            Language.setMessage(mstrModuleName, 20, "Employee Code")
            Language.setMessage(mstrModuleName, 21, "Employee Name")
            Language.setMessage(mstrModuleName, 24, "Profile")
            Language.setMessage(mstrModuleName, 25, "Appointed Date")
            Language.setMessage(mstrModuleName, 26, "Display Score Type")
            Language.setMessage(mstrModuleName, 27, "Period")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Shani(26-APR-2016) -- Start
'Public Class clsAssessSoreRatingReport1
'    Inherits IReportData

'#Region " Private Variables "

'    Private Shared ReadOnly mstrModuleName As String = "clsAssessSoreRatingReport"
'    Private mstrReportId As String = enArutiReport.EmployeeAssessSoreRatingReport
'    Private mdtFinalTable As DataTable
'    Private dsColumns As New DataSet
'    Dim StrFinalPath As String = String.Empty
'    Dim iCnt As Integer = 0
'    Dim dSelf() As DataRow = Nothing
'    Dim dAssess() As DataRow = Nothing
'    Dim dEAssess() As DataRow = Nothing
'    Dim dReview() As DataRow = Nothing
'    Dim dblBSC_Percent As Integer
'    Dim dblGeneral_Percent As Integer
'    Dim dblGETotal As Double = 0
'    'S.SANDEEP [21 AUG 2015] -- START
'    Private mintScoringOptionId As Integer = 0
'    'S.SANDEEP [21 AUG 2015] -- END

'    'S.SANDEEP [08 Jan 2016] -- START
'    Private blnIsSelfAssignCompetencies As Boolean = False
'    'S.SANDEEP [08 Jan 2016] -- END

'#End Region

'#Region " Constructor "

'    Public Sub New()
'        Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
'        Call Create_OnDetailReport()
'    End Sub

'#End Region

'#Region " Private Variables "

'    Private mintEmployeeId As Integer = 0
'    Private mstrEmployeeName As String = String.Empty
'    Private mintPeriodId As Integer = 0
'    Private mstrPeriodName As String = String.Empty
'    Private mstrAllocation As String = String.Empty
'    Private mstrAllocationIds As String = String.Empty
'    Private mintReportType_Id As Integer = -1
'    Private mstrReportType_Name As String = String.Empty
'    Private mdtStartDate As DateTime = Nothing
'    Private mdtEndDate As DateTime = Nothing

'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    Private mblnOnProbationSelected As Boolean = False
'    Private mstrCondition As String = String.Empty
'    Private mdtAppointmentDate As Date = Nothing
'    Private mstrSelectedCols As String = String.Empty
'    Private mstrSelectedJoin As String = String.Empty
'    Private mstrDisplayCols As String = String.Empty
'    Private mstrAdvanceFilter As String = String.Empty
'    Private mstrCaption As String = String.Empty
'    'S.SANDEEP [ 28 FEB 2014 ] -- END
'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    'ENHANCEMENT : Requested by Rutta
'    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
'    'S.SANDEEP [ 26 MAR 2014 ] -- END
'#End Region

'#Region " Properties "

'    Public WriteOnly Property _EmployeeId() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeName() As String
'        Set(ByVal value As String)
'            mstrEmployeeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _PeriodId() As Integer
'        Set(ByVal value As Integer)
'            mintPeriodId = value
'        End Set
'    End Property

'    Public WriteOnly Property _PeriodName() As String
'        Set(ByVal value As String)
'            mstrPeriodName = value
'        End Set
'    End Property

'    Public WriteOnly Property _Allocation() As String
'        Set(ByVal value As String)
'            mstrAllocation = value
'        End Set
'    End Property

'    Public WriteOnly Property _AllocationIds() As String
'        Set(ByVal value As String)
'            mstrAllocationIds = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReportType_Id() As Integer
'        Set(ByVal value As Integer)
'            mintReportType_Id = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReportType_Name() As String
'        Set(ByVal value As String)
'            mstrReportType_Name = value
'        End Set
'    End Property

'    Public WriteOnly Property _StartDate() As DateTime
'        Set(ByVal value As DateTime)
'            mdtStartDate = value
'        End Set
'    End Property

'    Public WriteOnly Property _EndDate() As DateTime
'        Set(ByVal value As DateTime)
'            mdtEndDate = value
'        End Set
'    End Property

'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    Public WriteOnly Property _OnProbationSelected() As Boolean
'        Set(ByVal value As Boolean)
'            mblnOnProbationSelected = value
'        End Set
'    End Property

'    Public WriteOnly Property _Condition() As String
'        Set(ByVal value As String)
'            mstrCondition = value
'        End Set
'    End Property

'    Public WriteOnly Property _AppointmentDate() As Date
'        Set(ByVal value As Date)
'            mdtAppointmentDate = value
'        End Set
'    End Property

'    Public WriteOnly Property _SelectedCols() As String
'        Set(ByVal value As String)
'            mstrSelectedCols = value
'        End Set
'    End Property

'    Public WriteOnly Property _SelectedJoin() As String
'        Set(ByVal value As String)
'            mstrSelectedJoin = value
'        End Set
'    End Property

'    Public WriteOnly Property _DisplayCols() As String
'        Set(ByVal value As String)
'            mstrDisplayCols = value
'        End Set
'    End Property

'    Public WriteOnly Property _AdvanceFilter() As String
'        Set(ByVal value As String)
'            mstrAdvanceFilter = value
'        End Set
'    End Property

'    Public WriteOnly Property _Caption() As String
'        Set(ByVal value As String)
'            mstrCaption = value
'        End Set
'    End Property
'    'S.SANDEEP [ 28 FEB 2014 ] -- END

'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    Public WriteOnly Property _FirstNamethenSurname() As Boolean
'        Set(ByVal value As Boolean)
'            mblnFirstNamethenSurname = value
'        End Set
'    End Property
'    'S.SANDEEP [ 26 MAR 2014 ] -- END


'    'S.SANDEEP [21 AUG 2015] -- START
'    Public WriteOnly Property _ScoringOptionId() As Integer
'        Set(ByVal value As Integer)
'            mintScoringOptionId = value
'        End Set
'    End Property
'    'S.SANDEEP [21 AUG 2015] -- END

'    'S.SANDEEP [08 Jan 2016] -- START
'    Public WriteOnly Property _SelfAssignCompetencies() As Boolean
'        Set(ByVal value As Boolean)
'            blnIsSelfAssignCompetencies = value
'        End Set
'    End Property
'    'S.SANDEEP [08 Jan 2016] -- END

'#End Region

'#Region " Public Function(s) And Procedures "

'    Dim iColumn_DetailReport As New IColumnCollection

'    Public Property Field_OnDetailReport() As IColumnCollection
'        Get
'            Return iColumn_DetailReport
'        End Get
'        Set(ByVal value As IColumnCollection)
'            iColumn_DetailReport = value
'        End Set
'    End Property

'    Private Sub Create_OnDetailReport()
'        Try

'            'S.SANDEEP [21 AUG 2015] -- START

'            'iColumn_DetailReport.Clear()
'            ''S.SANDEEP [ 28 FEB 2014 ] -- START
'            ''iColumn_DetailReport.Add(New IColumn("Score", Language.getMessage(mstrModuleName, 19, "Score")))
'            ''S.SANDEEP [ 28 FEB 2014 ] -- END
'            'iColumn_DetailReport.Add(New IColumn("[Employee Name]", Language.getMessage(mstrModuleName, 21, "Employee Name")))
'            'iColumn_DetailReport.Add(New IColumn("[Employee Code]", Language.getMessage(mstrModuleName, 20, "Employee Code")))
'            ''S.SANDEEP [ 28 FEB 2014 ] -- START
'            ''iColumn_DetailReport.Add(New IColumn("[Branch]", Language.getMessage(mstrModuleName, 22, "Branch")))
'            ''iColumn_DetailReport.Add(New IColumn("[Department]", Language.getMessage(mstrModuleName, 23, "Department")))
'            ''S.SANDEEP [ 28 FEB 2014 ] -- END
'            'iColumn_DetailReport.Add(New IColumn("[Profile]", Language.getMessage(mstrModuleName, 24, "Profile")))
'            'iColumn_DetailReport.Add(New IColumn("[aDate]", Language.getMessage(mstrModuleName, 25, "Appointed Date")))


'            iColumn_DetailReport.Clear()
'            If mblnFirstNamethenSurname = False Then
'                iColumn_DetailReport.Add(New IColumn("ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '')", Language.getMessage(mstrModuleName, 21, "Employee Name")))
'            Else
'                iColumn_DetailReport.Add(New IColumn("ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '')", Language.getMessage(mstrModuleName, 21, "Employee Name")))
'            End If

'            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 20, "Employee Code")))
'            iColumn_DetailReport.Add(New IColumn("Profile", Language.getMessage(mstrModuleName, 24, "Profile")))
'            iColumn_DetailReport.Add(New IColumn("appointeddate", Language.getMessage(mstrModuleName, 25, "Appointed Date")))

'            'S.SANDEEP [21 AUG 2015] -- END



'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Sub SetDefaultValue()
'        Try
'            mintEmployeeId = 0
'            mstrEmployeeName = String.Empty
'            mintPeriodId = 0
'            mstrPeriodName = String.Empty
'            mstrAllocation = String.Empty
'            mstrAllocationIds = String.Empty
'            mintReportType_Id = -1
'            mstrReportType_Name = String.Empty
'            'S.SANDEEP [ 28 FEB 2014 ] -- START
'            mblnOnProbationSelected = False
'            mstrCondition = String.Empty
'            mdtAppointmentDate = Nothing
'            mstrSelectedCols = String.Empty
'            mstrSelectedJoin = String.Empty
'            mstrDisplayCols = String.Empty
'            'S.SANDEEP [ 28 FEB 2014 ] -- END

'            'S.SANDEEP [21 AUG 2015] -- START
'            mintScoringOptionId = 0
'            'S.SANDEEP [21 AUG 2015] -- END

'            'S.SANDEEP [08 Jan 2016] -- START
'            blnIsSelfAssignCompetencies = False
'            'S.SANDEEP [08 Jan 2016] -- END

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

'    End Sub

'    'S.SANDEEP [04 JUN 2015] -- START
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

'    End Sub
'    'S.SANDEEP [04 JUN 2015] -- END

'    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
'        OrderByDisplay = ""
'        OrderByQuery = ""
'        Try
'            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
'            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
'        Try
'            Call OrderByExecute(iColumn_DetailReport)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Private Sub CreateTable()
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Try
'            mdtFinalTable = New DataTable("Data")

'            mdtFinalTable.Columns.Add("referencename", System.Type.GetType("System.String")).Caption = ""

'            'S.SANDEEP [29 Jan 2016] -- START
'            'StrQ = "    SELECT " & _
'            '       "        'S_' + CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) AS AId "
'            ''S.SANDEEP [ 26 MAR 2014 ] -- START
'            ''"        ,@Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            'If mblnFirstNamethenSurname = False Then
'            '    StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' ] ' AS Appriser  "
'            'Else
'            '    StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser "
'            'End If
'            ''S.SANDEEP [ 26 MAR 2014 ] -- END


'            'StrQ &= "        ,1 AS Mode " & _
'            '       "        ,hremployee_master.employeeunkid AS EmpId " & _
'            '       "    FROM hremployee_master " & _
'            '       "    WHERE hremployee_master.employeeunkid = '" & mintEmployeeId & "' " & _
'            '       "UNION ALL " & _
'            '       "    SELECT " & _
'            '       "         'A_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'            '       "        ,@Appriser + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            '       "        ,2 AS Mode " & _
'            '       "        ,hrassessor_master.assessormasterunkid " & _
'            '       "    FROM hrassessor_tran " & _
'            '       "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '       "        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '       "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'            '       "UNION ALL " & _
'            '       "    SELECT " & _
'            '       "         'E_' + CAST(hrexternal_assessor_master.ext_assessorunkid AS NVARCHAR(50)) AS AId " & _
'            '       "        ,@ExAssessor + CHAR(13) + ' [ ' + hrexternal_assessor_master.displayname+' ] ' " & _
'            '       "        ,3 AS Mode " & _
'            '       "        ,hrexternal_assessor_master.ext_assessorunkid " & _
'            '       "    FROM hrexternal_assessor_master " & _
'            '       "    WHERE employeeunkid = '" & mintEmployeeId & "' AND hrexternal_assessor_master.isactive = 1 " & _
'            '       "UNION ALL " & _
'            '       "    SELECT " & _
'            '       "         'R_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'            '       "        ,@Reviewer + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            '       "        ,4 AS Mode " & _
'            '       "        ,hrassessor_master.assessormasterunkid " & _
'            '       "    FROM hrassessor_tran " & _
'            '       "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '       "        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '       "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

'            'Shani(01-MAR-2016) -- Start
'            'Enhancement :PA External Approver Flow
'            'StrQ = "    SELECT " & _
'            '       "     AId " & _
'            '       "    ,Appriser " & _
'            '       "    ,Mode " & _
'            '       "    ,EmpId " & _
'            '       "FROM " & _
'            '       "( " & _
'            '       "    SELECT " & _
'            '       "        'S_' + CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) AS AId "
'            'If mblnFirstNamethenSurname = False Then
'            '    StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' ] ' AS Appriser  "
'            'Else
'            '    StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser "
'            'End If
'            'StrQ &= "        ,1 AS Mode " & _
'            '       "        ,hremployee_master.employeeunkid AS EmpId " & _
'            '       "        ,1 AS analysisunkid " & _
'            '       "    FROM hremployee_master " & _
'            '       "    WHERE hremployee_master.employeeunkid = '" & mintEmployeeId & "' " & _
'            '       "UNION ALL " & _
'            '       "    SELECT " & _
'            '       "         'A_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'            '       "        ,@Appriser + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            '       "        ,2 AS Mode " & _
'            '       "        ,hrassessor_master.assessormasterunkid " & _
'            '       "        ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
'            '       "    FROM hrassessor_tran " & _
'            '       "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '       "        LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
'            '       "                AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
'            '       "        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '       "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'            '       "UNION ALL " & _
'            '       "    SELECT " & _
'            '       "         'R_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'            '       "        ,@Reviewer + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            '       "        ,4 AS Mode " & _
'            '       "        ,hrassessor_master.assessormasterunkid " & _
'            '       "        ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
'            '       "    FROM hrassessor_tran " & _
'            '       "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '       "        LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
'            '       "                AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
'            '       "        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '       "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'            '       ") AS A WHERE 1 = 1 "

'            ''S.SANDEEP [29 Jan 2016] -- END
'            ''S.SANDEEP [ 13 FEB 2014 ] -- START
'            ''ADDED : AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0
'            ''ADDED : AND hrexternal_assessor_master.isactive = 1
'            ''S.SANDEEP [ 13 FEB 2014 ] -- END

'            '-------------------Employee Start-------------------
'            StrQ = "    SELECT " & _
'                   "     AId " & _
'                   "    ,Appriser " & _
'                   "    ,Mode " & _
'                   "    ,EmpId " & _
'                   "FROM " & _
'                   "( " & _
'                   "    SELECT " & _
'                   "        'S_' + CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) AS AId "
'            If mblnFirstNamethenSurname = False Then
'                StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' ] ' AS Appriser  "
'            Else
'                StrQ &= ", @Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser "
'            End If
'            StrQ &= "        ,1 AS Mode " & _
'                   "        ,hremployee_master.employeeunkid AS EmpId " & _
'                   "        ,1 AS analysisunkid " & _
'                   "    FROM hremployee_master " & _
'                   "    WHERE hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
'            '-------------------Employee END-------------------

'            '-------------------Assessor Start-------------------
'            StrQ &= "UNION ALL " & _
'                   "    SELECT " & _
'                   "         'A_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'                    "        ,@Appriser + CHAR(13) + ' [ ' + #ASS_Name# +' ] ' AS Appriser " & _
'                   "        ,2 AS Mode " & _
'                   "        ,hrassessor_master.assessormasterunkid " & _
'                   "        ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
'                   "    FROM hrassessor_tran " & _
'                   "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   "        LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
'                   "                AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
'                    "        #ASS_JOIN# " & _
'                    "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

'            '"        ,@Appriser + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            '"        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '-------------------Assessor END-------------------

'            '-------------------Reviewer Start-------------------
'            StrQ &= "UNION ALL " & _
'                   "    SELECT " & _
'                   "         'R_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'                    "        ,@Reviewer + CHAR(13) + ' [ ' + #REV_NAME# +' ] ' AS Appriser " & _
'                   "        ,4 AS Mode " & _
'                   "        ,hrassessor_master.assessormasterunkid " & _
'                   "        ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
'                   "    FROM hrassessor_tran " & _
'                   "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   "        LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
'                   "                AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
'                    "        #REV_JOIN# " & _
'                    "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

'            '"        ,@Reviewer + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'            '"        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '-------------------Reviewer Start-------------------
'            StrQ &= ") AS A WHERE 1 = 1 "

'            'Shani(01-MAR-2016) -- End




'            'Shani(01-MAR-2016) -- Start
'            'Enhancement :PA External Approver Flow
'            Dim strFinalQ As String = StrQ
'            Dim dsTemp As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , mintEmployeeId, , clsAssessor.enAssessorType.ASSESSOR)

'            If dsTemp.Tables(0).Rows.Count > 0 Then
'                If dsTemp.Tables(0).Rows(0)("companyunkid") <= 0 AndAlso dsTemp.Tables(0).Rows(0)("DBName").ToString.Trim.Length <= 0 Then
'                    strFinalQ = strFinalQ.Replace("#ASS_Name#", "ISNULL(cfuser_master.username,'') ")
'                    strFinalQ = strFinalQ.Replace("#ASS_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid ")
'                Else
'                    strFinalQ = strFinalQ.Replace("#ASS_Name#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
'                    strFinalQ = strFinalQ.Replace("#ASS_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
'                                                                " JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
'                End If
'                strFinalQ = strFinalQ.Replace("#DBName#", dsTemp.Tables(0).Rows(0)("DBName") & "..")
'            Else
'                strFinalQ = strFinalQ.Replace("#ASS_Name#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
'                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid")
'            End If


'            dsTemp = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , mintEmployeeId, , clsAssessor.enAssessorType.REVIEWER)
'            If dsTemp.Tables(0).Rows.Count > 0 Then
'                If dsTemp.Tables(0).Rows(0)("companyunkid") <= 0 AndAlso dsTemp.Tables(0).Rows(0)("DBName").ToString.Trim.Length <= 0 Then
'                    strFinalQ = strFinalQ.Replace("#REV_NAME#", "ISNULL(cfuser_master.username,'') ")
'                    strFinalQ = strFinalQ.Replace("#REV_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid ")
'                Else
'                    strFinalQ = strFinalQ.Replace("#REV_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
'                    strFinalQ = strFinalQ.Replace("#REV_JOIN#", " JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
'                                                                " JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
'                End If
'                strFinalQ = strFinalQ.Replace("#DBName#", dsTemp.Tables(0).Rows(0)("DBName") & "..")
'            Else
'                strFinalQ = strFinalQ.Replace("#REV_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
'                strFinalQ = strFinalQ.Replace("#REV_JOIN#", "JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid")
'            End If

'            'Shani(01-MAR-2016) -- End

'            objDataOperation.AddParameter("@Self", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Self"))
'            objDataOperation.AddParameter("@Appriser", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor"))
'            objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Reviewer"))
'            objDataOperation.AddParameter("@ExAssessor", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor"))

'            dsColumns = objDataOperation.ExecQuery(strFinalQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'            End If

'            Dim dCol As DataColumn

'            If dsColumns.Tables("List").Rows.Count > 0 Then
'                For Each dRow As DataRow In dsColumns.Tables("List").Rows
'                    dCol = New DataColumn
'                    dCol.ColumnName = "Column" & dRow.Item("AId").ToString
'                    dCol.Caption = dRow.Item("Appriser").ToString
'                    dCol.DataType = System.Type.GetType("System.Decimal")

'                    mdtFinalTable.Columns.Add(dCol)
'                Next
'            End If

'            mdtFinalTable.Columns.Add("total", System.Type.GetType("System.Decimal")).Caption = "Total"
'            mdtFinalTable.Columns.Add("total_percent", System.Type.GetType("System.Decimal")).Caption = "Total %"

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [04 JUN 2015] -- START
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    Public Sub Export_Assessment_Report(ByVal dtEmployeeAsOnDate As DateTime)
'        'Public Sub Export_Assessment_Report()
'        'S.SANDEEP [04 JUN 2015] -- END

'        Try
'            Call CreateTable()

'            'S.SANDEEP [21 AUG 2015] -- START

'            'Select Case ConfigParameter._Object._PerformanceComputation
'            '    Case 1  'WEIGHTED AVG OF
'            '        If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'            '            dSelf = dsColumns.Tables("List").Select("Mode = 1")    'SETTING SELF ASSESSMENT
'            '            dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'            '            dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'            '            dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'            '            dSelf = dsColumns.Tables("List").Select("Mode = 1")
'            '            dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'            '            dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'            '            dSelf = dsColumns.Tables("List").Select("Mode = 1") 'SETTING SELF ASSESSMENT
'            '            dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'            '            dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'            '            dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'            '            dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'            '            dSelf = dsColumns.Tables("List").Select("Mode = 1") 'SETTING SELF ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'            '            dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'            '            dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'            '            dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT
'            '        End If
'            '    Case 2  'WEIGHT SCORE OF
'            '        If ConfigParameter._Object._WScrComputType = 1 Then
'            '            dSelf = dsColumns.Tables("List").Select("Mode = 1")    'SETTING SELF ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WScrComputType = 2 Then
'            '            dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'            '            dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'            '        ElseIf ConfigParameter._Object._WScrComputType = 3 Then
'            '            dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT
'            '        End If
'            'End Select

'            dSelf = dsColumns.Tables("List").Select("Mode = 1")    'SETTING SELF ASSESSMENT
'            dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'            dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'            dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT

'            'S.SANDEEP [21 AUG 2015] -- END


'            dblBSC_Percent = 0 : dblGeneral_Percent = 0

'            Dim objEmp As New clsEmployee_Master

'            'S.SANDEEP [14 MAR 2016] -- START
'            'Dim objRatio As New clsAssessment_Ratio

'            ''S.SANDEEP [04 JUN 2015] -- START
'            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            ''objEmp._Employeeunkid = mintEmployeeId
'            'objEmp._Employeeunkid(mdtEndDate) = mintEmployeeId
'            ''S.SANDEEP [04 JUN 2015] -- END

'            'objRatio.Get_Ratio(dblGeneral_Percent, dblBSC_Percent, objEmp._Jobgroupunkid, mintPeriodId)

'            Dim objRatio As New clsassess_ratio_master
'            objRatio.Get_Ratio(dblGeneral_Percent, dblBSC_Percent, mintPeriodId, mintEmployeeId, dtEmployeeAsOnDate)

'            objEmp._Employeeunkid(mdtEndDate) = mintEmployeeId

'            'S.SANDEEP [14 MAR 2016] -- END


'            Dim dTRow As DataRow = Nothing

'            dTRow = mdtFinalTable.NewRow

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dTRow = Get_BSC_Evalution()       'BSC
'            dTRow = Get_BSC_Evalution(dtEmployeeAsOnDate)       'BSC
'            'S.SANDEEP [04 JUN 2015] -- END

'            mdtFinalTable.Rows.Add(dTRow)

'            iCnt = 0 : dblGETotal = 0

'            dTRow = mdtFinalTable.NewRow

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dTRow = Get_GEvaluation()       'GENERAL EVALUATION 
'            dTRow = Get_GEvaluation(dtEmployeeAsOnDate)       'GENERAL EVALUATION 
'            'S.SANDEEP [04 JUN 2015] -- END

'            mdtFinalTable.Rows.Add(dTRow)




'            Call Set_Total()                'SETTING COLUMN TOTAL


'            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
'                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
'            End If

'            iCnt = 0 : dblGETotal = 0

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Export_Assessment_Report; Module Name: " & mstrModuleName)
'        Finally
'            dSelf = Nothing
'            dAssess = Nothing
'            dEAssess = Nothing
'            dReview = Nothing
'        End Try
'    End Sub

'    Private Function GetScore(ByVal intMode As Integer, ByVal intEmpId As Integer, ByVal blnIsBSC As Boolean, ByVal mdtEmployeeAsOnDate As DateTime) As DataSet
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Dim dsList As New DataSet
'        Dim dtTable As DataTable
'        Dim iCnt As Integer = 0
'        Dim StrGrpId As Integer = 0
'        Dim dblResult As Double = 0
'        Try
'            dtTable = New DataTable("List")
'            dtTable.Columns.Add("Result", System.Type.GetType("System.Double"))


'            'S.SANDEEP [21 AUG 2015] -- START

'            Dim xSelfScore, xAssessorScore, xReviewerScore As Decimal
'            xSelfScore = 0 : xAssessorScore = 0 : xReviewerScore = 0 : dblResult = 0

'            Dim xEvaluation As New clsevaluation_analysis_master
'            'S.SANDEEP [21 AUG 2015] -- END


'            If blnIsBSC = False Then

'                'S.SANDEEP [ 05 NOV 2014 ] -- START
'                'StrQ = "SELECT " & _
'                '       " Result AS Result " & _
'                '       ",assessmodeid as Id " & _
'                '       "FROM " & _
'                '       "( " & _
'                '       "SELECT " & _
'                '       "     hrassess_analysis_master.assessmodeid " & _
'                '       "    ,CAST(ISNULL(CAST((CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS Result " & _
'                '       "FROM hrassess_analysis_tran " & _
'                '       "    JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                '       "    JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                '       "    JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                '       "    JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                '       "WHERE hrassess_analysis_master.isvoid = 0 AND hrassess_analysis_tran.isvoid = 0 AND hrassess_analysis_master.periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "


'                StrQ = "SELECT " & _
'                       " Result AS Result " & _
'                       ",assessmodeid as Id " & _
'                       "FROM " & _
'                       "( " & _
'                       "SELECT " & _
'                       "         assessmodeid " & _
'                       "        ,ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0) AS Result " & _
'                       "    FROM hrevaluation_analysis_master " & _
'                       "        JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                       "        JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'                       "        JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'                       "            AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                       "        JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                       "    WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                       "AND iscommitted = 1 "
'                'S.SANDEEP [ 05 NOV 2014 ] -- END



'                'S.SANDEEP [ 17 FEB 2014 ] -- START
'                'CAST((resultname * (hrassess_item_master.weight/100)) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) -- REMOVED
'                '" & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) -- ADDED
'                'S.SANDEEP [ 17 FEB 2014 ] -- END



'                Select Case intMode
'                    Case 1 ' SELF
'                        StrQ &= " AND selfemployeeunkid = '" & mintEmployeeId & "'"
'                    Case 2 ' ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                    Case 3 ' EX ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND ext_assessorunkid = '" & intEmpId & "'"
'                    Case 4 ' REVIEWER
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                End Select
'                StrQ &= ")AS A "

'                objDataOperation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                'S.SANDEEP [21 AUG 2015] -- START

'                'For Each dRow As DataRow In dsList.Tables(0).Rows
'                '    If IsNumeric(dRow.Item("Result")) = True Then
'                '        dblResult += CDbl(dRow.Item("Result"))
'                '        If StrGrpId <> dRow.Item("Id").ToString Then
'                '            iCnt += 1
'                '            StrGrpId = dRow.Item("Id").ToString
'                '        End If
'                '    End If
'                'Next

'                If intMode = 1 Then
'                    xSelfScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                                                           False, _
'                                                           mintScoringOptionId, _
'                                                           enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, mdtEmployeeAsOnDate, _
'                                                           mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If

'                If intMode = 2 Then
'                    xAssessorScore = xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                               False, _
'                                                               mintScoringOptionId, _
'                                                               enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, mdtEmployeeAsOnDate, _
'                                                               mintEmployeeId, mintPeriodId, , , intEmpId, , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If


'                If intMode = 4 Then
'                    xReviewerScore = xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                                               False, _
'                                                               mintScoringOptionId, _
'                                                               enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, mdtEmployeeAsOnDate, _
'                                                               mintEmployeeId, mintPeriodId, , , intEmpId, , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If



'                dblResult = xSelfScore + xAssessorScore + xReviewerScore
'                'S.SANDEEP [21 AUG 2015] -- END



'                Dim dtRow As DataRow = dtTable.NewRow


'                'S.SANDEEP [21 AUG 2015] -- START

'                'If iCnt > 0 Then
'                '    dtRow.Item("Result") = ((dblResult * 100) / (iCnt * 100)).ToString("###.##")
'                'Else
'                '    dtRow.Item("Result") = 0
'                'End If

'                dtRow.Item("Result") = dblResult
'                'S.SANDEEP [21 AUG 2015] -- END



'                dtTable.Rows.Add(dtRow)
'                dsList.Tables.RemoveAt(0)
'                dsList.Tables.Add(dtTable)
'            Else

'                'S.SANDEEP [ 04 MAR 2014 ] -- START
'                'StrQ = "SELECT " & _
'                '      "    resultname AS Result " & _
'                '      "FROM hrbsc_analysis_tran " & _
'                '      "    JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                '      "    JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
'                '      "WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_master.isvoid = 0 " & _
'                '      "    AND kpiunkid <=0 AND targetunkid <=0 AND initiativeunkid < = 0 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "


'                ''S.SANDEEP [ 05 NOV 2014 ] -- START
'                'StrQ = "SELECT " & _
'                '       "    resultname AS Result " & _
'                '       "FROM hrbsc_analysis_tran " & _
'                '       "    JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                '       "    JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
'                '       "WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_master.isvoid = 0 " & _
'                '      "    AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "

'                ''S.SANDEEP [ 30 MAY 2014 ] -- START
'                ''Dim objWSetting As New clsWeight_Setting(True)
'                'Dim objWSetting As New clsWeight_Setting(mintPeriodId)
'                ''S.SANDEEP [ 30 MAY 2014 ] -- END

'                'If objWSetting._Weight_Typeid <= 0 Then
'                '    StrQ &= " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
'                'ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
'                '    StrQ &= " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
'                'ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
'                '    StrQ &= " AND kpiunkid > 0 "
'                'ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
'                '    StrQ &= " AND targetunkid > 0 "
'                'ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
'                '    StrQ &= " AND initiativeunkid > 0 "
'                'End If
'                ''S.SANDEEP [ 04 MAR 2014 ] -- END
'                StrQ = "SELECT " & _
'                            "ISNULL(hrgoals_analysis_tran.result,0) AS Result " & _
'                       "FROM hrevaluation_analysis_master " & _
'                            "JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                       "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                       "AND iscommitted = 1 "
'                'S.SANDEEP [ 05 NOV 2014 ] -- END



'                Select Case intMode
'                    Case 1 ' SELF
'                        StrQ &= " AND selfemployeeunkid = '" & mintEmployeeId & "'"
'                    Case 2 ' ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                    Case 3 ' EX ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND ext_assessorunkid = '" & intEmpId & "'"
'                    Case 4 ' REVIEWER
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                End Select

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If


'                'S.SANDEEP [21 AUG 2015] -- START

'                'For Each dRow As DataRow In dsList.Tables(0).Rows
'                '    If IsNumeric(dRow.Item("Result")) = True Then
'                '        dblResult += CDbl(dRow.Item("Result"))
'                '    End If
'                'Next

'                If intMode = 1 Then
'                    xSelfScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                                                       True, _
'                                                       mintScoringOptionId, _
'                                                       enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, mdtEmployeeAsOnDate, _
'                                                       mintEmployeeId, mintPeriodId, , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If

'                If intMode = 2 Then
'                    xAssessorScore = xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                           True, _
'                                                           mintScoringOptionId, _
'                                                           enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, mdtEmployeeAsOnDate, _
'                                                           mintEmployeeId, mintPeriodId, , , intEmpId, , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If


'                If intMode = 4 Then
'                    xReviewerScore = xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                                           True, _
'                                                           mintScoringOptionId, _
'                                                           enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, mdtEmployeeAsOnDate, _
'                                                           mintEmployeeId, mintPeriodId, , , intEmpId, , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If



'                dblResult = xSelfScore + xAssessorScore + xReviewerScore

'                'S.SANDEEP [21 AUG 2015] -- END




'                Dim dtRow As DataRow = dtTable.NewRow

'                dtRow.Item("Result") = dblResult

'                dtTable.Rows.Add(dtRow)
'                dsList.Tables.RemoveAt(0)
'                dsList.Tables.Add(dtTable)

'            End If


'            'S.SANDEEP [21 AUG 2015] -- START

'            xEvaluation = Nothing
'            'S.SANDEEP [21 AUG 2015] -- END


'            Return dsList
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetScore; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function

'    'S.SANDEEP [04 JUN 2015] -- START
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'    Private Function Get_BSC_Evalution(ByVal dtEmployeeAsOnDate As DateTime) As DataRow
'        'Private Function Get_BSC_Evalution() As DataRow
'        'S.SANDEEP [04 JUN 2015] -- END
'        Dim dBRow As DataRow = mdtFinalTable.NewRow
'        Dim dsTList As New DataSet
'        Try
'            If dSelf IsNot Nothing Then
'                For i As Integer = 0 To dSelf.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dSelf(i)("Mode"), dSelf(i)("EmpId"), True)
'                    dsTList = GetScore(dSelf(i)("Mode"), dSelf(i)("EmpId"), True, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "S_" & dSelf(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                    End If
'                    iCnt += 1
'                Next
'            End If

'            If dAssess IsNot Nothing Then
'                For i As Integer = 0 To dAssess.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dAssess(i)("Mode"), dAssess(i)("EmpId"), True)
'                    dsTList = GetScore(dAssess(i)("Mode"), dAssess(i)("EmpId"), True, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "A_" & dAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dEAssess IsNot Nothing Then
'                For i As Integer = 0 To dEAssess.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dEAssess(i)("Mode"), dEAssess(i)("EmpId"), True)
'                    dsTList = GetScore(dEAssess(i)("Mode"), dEAssess(i)("EmpId"), True, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "E_" & dEAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dReview IsNot Nothing Then
'                For i As Integer = 0 To dReview.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dReview(i)("Mode"), dReview(i)("EmpId"), True)
'                    dsTList = GetScore(dReview(i)("Mode"), dReview(i)("EmpId"), True, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "R_" & dReview(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            dBRow.Item("total") = CDbl(dblGETotal)

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'dBRow.Item("total_percent") = ((dblGETotal * dblGeneral_Percent) / (iCnt * 100)).ToString("###.#0")
'            If iCnt = 0 Then
'                dBRow.Item("total_percent") = (0).ToString("###.#0")
'            Else
'                dBRow.Item("total_percent") = ((dblGETotal * dblBSC_Percent) / (iCnt * 100)).ToString("###.#0")
'            End If

'            'S.SANDEEP [ 05 MARCH 2012 ] -- END




'            Return dBRow

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_BSC_Row; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function


'    'S.SANDEEP [04 JUN 2015] -- START
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


'    Private Function Get_GEvaluation(ByVal dtEmployeeAsOnDate As DateTime) As DataRow
'        'Private Function Get_GEvaluation() As DataRow
'        'S.SANDEEP [04 JUN 2015] -- END

'        Dim dGRow As DataRow = mdtFinalTable.NewRow
'        Dim dsTList As New DataSet
'        Try
'            If dSelf IsNot Nothing Then
'                For i As Integer = 0 To dSelf.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dSelf(i)("Mode"), dSelf(i)("EmpId"), False)
'                    dsTList = GetScore(dSelf(i)("Mode"), dSelf(i)("EmpId"), False, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "S_" & dSelf(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                    End If
'                    iCnt += 1
'                Next
'            End If

'            If dAssess IsNot Nothing Then
'                For i As Integer = 0 To dAssess.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dAssess(i)("Mode"), dAssess(i)("EmpId"), False)
'                    dsTList = GetScore(dAssess(i)("Mode"), dAssess(i)("EmpId"), False, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "A_" & dAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dEAssess IsNot Nothing Then
'                For i As Integer = 0 To dEAssess.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dEAssess(i)("Mode"), dEAssess(i)("EmpId"), False)
'                    dsTList = GetScore(dEAssess(i)("Mode"), dEAssess(i)("EmpId"), False, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "E_" & dEAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dReview IsNot Nothing Then
'                For i As Integer = 0 To dReview.Length - 1

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsTList = GetScore(dReview(i)("Mode"), dReview(i)("EmpId"), False)
'                    dsTList = GetScore(dReview(i)("Mode"), dReview(i)("EmpId"), False, dtEmployeeAsOnDate)
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "R_" & dReview(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            dGRow.Item("total") = CDbl(dblGETotal)
'            If iCnt = 0 Then
'                dGRow.Item("total_percent") = (0).ToString("###.#0")
'            Else
'                dGRow.Item("total_percent") = ((dblGETotal * dblGeneral_Percent) / (iCnt * 100)).ToString("###.#0")
'            End If


'            Return dGRow

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_GEvaluation; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function

'    Private Sub Set_Total()
'        Try
'            mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
'            mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
'            For Each dRow As DataRow In dsColumns.Tables("List").Rows
'                mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("referencename") = "FORMULA FOR TOTAL SCORES " & vbCrLf & "Scores of BSC + Scores of General Evaluation "
'                mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("Column" & dRow("AId").ToString) = mdtFinalTable.Compute("SUM(Column" & dRow("AId").ToString & ")", "")
'                mdtFinalTable.AcceptChanges()
'            Next

'            mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("total") = mdtFinalTable.Compute("SUM(total)", "")
'            mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("total_percent") = mdtFinalTable.Compute("SUM(total_percent)", "")

'            dblGETotal = mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("total_percent")

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Set_Total; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub


'    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
'        Dim strBuilder As New StringBuilder
'        Dim blnFlag As Boolean = False
'        Dim StrValue As String = ""
'        Try
'            Select Case ConfigParameter._Object._PerformanceComputation
'                Case 1  'WEIGHTED AVG OF
'                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrValue = Language.getMessage(mstrModuleName, 8, "Weighted Average By Employee,Assessor,Reviewer.")
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrValue = Language.getMessage(mstrModuleName, 9, "Weighted Average By Employee,Assessor.")
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrValue = Language.getMessage(mstrModuleName, 10, "Weighted Average By Employee,Reviewer.")
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrValue = Language.getMessage(mstrModuleName, 11, "Weighted Average By Assessor,Reviewer.")
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrValue = Language.getMessage(mstrModuleName, 12, "Weighted Average By Employee.")
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrValue = Language.getMessage(mstrModuleName, 13, "Weighted Average By Assessor.")
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrValue = Language.getMessage(mstrModuleName, 14, "Weighted Average By Reviewer.")
'                    End If
'                Case 2  'WEIGHT SCORE OF
'                    If ConfigParameter._Object._WScrComputType = 1 Then
'                        StrValue = Language.getMessage(mstrModuleName, 15, "Weighted Score By Employee.")
'                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
'                        StrValue = Language.getMessage(mstrModuleName, 16, "Weighted Score By Assessor.")
'                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
'                        StrValue = Language.getMessage(mstrModuleName, 17, "Weighted Score By Reviewer.")
'                    End If
'            End Select

'            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
'            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
'            If ConfigParameter._Object._IsDisplayLogo = True Then
'                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
'                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
'                Dim objAppSett As New clsApplicationSettings
'                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
'                objAppSett = Nothing
'                If Company._Object._Image IsNot Nothing Then
'                    Company._Object._Image.Save(strImPath)
'                End If
'                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
'                strBuilder.Append(" </TD> " & vbCrLf)
'                strBuilder.Append(" </TR> " & vbCrLf)
'            End If
'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 5, "Prepared By :") & " </B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='5%' align='left' ><FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(User._Object._Username & vbCrLf)
'            strBuilder.Append(" </FONT></TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='40%' colspan= " & objDataReader.Columns.Count - 1 & " align='center' > " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
'            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(Now.Date & vbCrLf)
'            strBuilder.Append(" </FONT></TD> " & vbCrLf)
'            strBuilder.Append(" <TD width='40%' colspan= " & objDataReader.Columns.Count - 1 & " align='center' > " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=3><b> " & "FORMULAR AND A GUIDE TO OVERALL SCORES RATING" & "</B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" <HR> " & vbCrLf)
'            strBuilder.Append(" <B> " & "Employee : " & mstrEmployeeName & ", Period : " & mstrPeriodName & ", " & StrValue & "</B><BR> " & vbCrLf)
'            strBuilder.Append(" </HR> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH=100%> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

'            For j As Integer = 0 To objDataReader.Columns.Count - 1
'                strBuilder.Append("<TD BORDER=1 WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            For i As Integer = 0 To objDataReader.Rows.Count - 1
'                strBuilder.Append(" <TR> " & vbCrLf)
'                For k As Integer = 0 To objDataReader.Columns.Count - 1
'                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
'                Next
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" <TD colspan = " & objDataReader.Columns.Count & " BORDER = 0 ALIGN='LEFT'><FONT SIZE=2><B>" & "OVERALL PERFORMANCE RATING (Tick on relevant box to show overall quality of the Appraisee performance)" & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            'S.SANDEEP [29 Jan 2016] -- START
'            'strBuilder.Append(" <TR> " & vbCrLf)
'            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Excellent" & "</FONT></TD>" & vbCrLf)
'            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Very Good" & "</FONT></TD>" & vbCrLf)
'            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Good" & "</FONT></TD>" & vbCrLf)
'            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Average" & "</FONT></TD>" & vbCrLf)
'            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Fair" & "</FONT></TD>" & vbCrLf)
'            'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Poor" & "</FONT></TD>" & vbCrLf)
'            'strBuilder.Append(" </TR> " & vbCrLf)

'            'strBuilder.Append(" <TR> " & vbCrLf)
'            'If dblGETotal >= 91 Then
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            'Else
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            'End If

'            'If dblGETotal >= 70 And dblGETotal <= 90 Then
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            'Else
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            'End If

'            'If dblGETotal >= 60 And dblGETotal <= 69 Then
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            'Else
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            'End If

'            'If dblGETotal >= 50 And dblGETotal <= 59 Then
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            'Else
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            'End If

'            'If dblGETotal >= 35 And dblGETotal <= 49 Then
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            'Else
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            'End If

'            'If dblGETotal >= 0 And dblGETotal <= 34 Then
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            'Else
'            '    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            'End If
'            'strBuilder.Append(" </TR> " & vbCrLf)

'            Dim objRating As New clsAppraisal_Rating
'            Dim dsRating = New DataSet : Dim dtTable As DataTable
'            dsRating = objRating.getComboList("List", False)
'            dtTable = New DataView(dsRating.Tables(0), "", "scrt DESC", DataViewRowState.CurrentRows).ToTable.Copy
'            dsRating.Dispose() : objRating = Nothing
'            strBuilder.Append(" <TR> " & vbCrLf)
'            For Each dr In dtTable.Rows
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dr("name") & "</B></FONT></TD>" & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            Dim blnAboveMaxRating As Boolean = False
'            For Each dr In dtTable.Rows
'                If blnAboveMaxRating = False Then
'                    If dblGETotal > dtTable.Compute("MAX(scrt)", "") Then
'                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X</B></FONT></TD>" & vbCrLf)
'                    Else
'                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'                    End If
'                    blnAboveMaxRating = True
'                    Continue For
'                End If

'                If dblGETotal >= dr("scrf") AndAlso dblGETotal <= dr("scrt") Then
'                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X</B></FONT></TD>" & vbCrLf)
'                Else
'                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'                End If

'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            'S.SANDEEP [29 Jan 2016] -- END

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)


'            'S.SANDEEP [29 Jan 2016] -- START
'            'For i As Integer = 0 To objDataReader.Columns.Count - 1
'            '    strBuilder.Append(" <TR> " & vbCrLf)
'            '    Select Case i
'            '        Case 0
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>SCORE<B></FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>LEVEL OF PERFORMANCE<B></FONT></TD>" & vbCrLf)
'            '        Case 1
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 91 and above </FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Excellent</FONT></TD>" & vbCrLf)
'            '        Case 2
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 70-90 </FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Very Good</FONT></TD>" & vbCrLf)
'            '        Case 3
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 60-69 </FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Good</FONT></TD>" & vbCrLf)
'            '        Case 4
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 50-59 </FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Average</FONT></TD>" & vbCrLf)
'            '        Case 5
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 35-49 </FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Fair</FONT></TD>" & vbCrLf)
'            '        Case 6
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 0-34 </FONT></TD>" & vbCrLf)
'            '            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Fail</FONT></TD>" & vbCrLf)
'            '    End Select
'            '    strBuilder.Append(" </TR> " & vbCrLf)
'            'Next
'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>SCORE<B></FONT></B></TD>" & vbCrLf)
'            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>LEVEL OF PERFORMANCE<B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            For Each dr In dtTable.Rows
'                strBuilder.Append(" <TR> " & vbCrLf)
'                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr("scrf").ToString & " - " & dr("scrt").ToString & "</FONT></TD>" & vbCrLf)
'                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr("name") & "</FONT></TD>" & vbCrLf)
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next
'            'S.SANDEEP [29 Jan 2016] -- END

'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" </HTML> " & vbCrLf)

'            If System.IO.Directory.Exists(SavePath) = False Then
'                Dim dig As New Windows.Forms.FolderBrowserDialog
'                dig.Description = "Select Folder Where to export report."

'                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
'                    SavePath = dig.SelectedPath
'                Else
'                    Exit Function
'                End If
'            End If

'            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
'                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
'            End If

'            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
'                StrFinalPath = SavePath & "\" & flFileName & ".xls"
'                blnFlag = True
'            Else
'                blnFlag = False
'            End If

'            Return blnFlag

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
'        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
'        Dim strWriter As New StreamWriter(fsFile)
'        Try
'            With strWriter
'                .BaseStream.Seek(0, SeekOrigin.End)
'                .WriteLine(sb)
'                .Close()
'            End With
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            sb = Nothing
'            strWriter = Nothing
'            fsFile = Nothing
'        End Try
'    End Function

'    'S.SANDEEP [ 28 FEB 2014 ] -- START
'    'Public Sub Report_Allocation_Based()
'    '    Dim StrQ As String = String.Empty
'    '    Dim objDataOperation As New clsDataOperation
'    '    Dim dsList As New DataSet
'    '    Dim dtRating As DataTable
'    '    Dim exForce As Exception
'    '    Try
'    '        'S.SANDEEP [ 10 SEPT 2013 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        StrQ &= "SELECT " & _
'    '                "  [Employee Code] " & _
'    '                " ,[Employee Name] " & _
'    '                " ,[Branch] " & _
'    '                " ,[Department] " & _
'    '                " ,[Job] " & _
'    '                " ,[Date Hired] " & _
'    '                " ,Score " & _
'    '                " ,aDate " & _
'    '                " ,[Profile] FROM ("
'    '        'S.SANDEEP [ 10 SEPT 2013 ] -- END
'    '        StrQ &= "SELECT " & _
'    '               "  hremployee_master.employeecode AS [Employee Code] " & _
'    '               " ,firstname + ' ' + surname  AS [Employee Name] " & _
'    '               " ,ISNULL(hrstation_master.name,'') AS [Branch] " & _
'    '               " ,ISNULL(hrdepartment_master.name,'') AS [Department] " & _
'    '               " ,ISNULL(hrjob_master.job_name,'') AS [Job] " & _
'    '               " ,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
'    '               " ,appointeddate AS aDate "
'    '        Select Case ConfigParameter._Object._PerformanceComputation
'    '            Case 1 'Weight Avg Of
'    '                If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'    '                    StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'    '                    StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'    '                    StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                End If
'    '            Case 2 'Weight Score Of
'    '                If ConfigParameter._Object._WScrComputType = 1 Then
'    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WScrComputType = 2 Then
'    '                    StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                ElseIf ConfigParameter._Object._WScrComputType = 3 Then
'    '                    StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'    '                End If
'    '        End Select
'    '        StrQ &= ",'' AS [Profile] " & _
'    '                "FROM hremployee_master " & _
'    '                "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
'    '                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'    '                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'    '                "	JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & mintPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
'    '                "LEFT JOIN " & _
'    '                "( " & _
'    '                "   SELECT " & _
'    '                "        selfemployeeunkid AS EmpId " & _
'    '                "       ,ISNULL(SUM(SELF),0) AS S_GA " & _
'    '                "   FROM hrassess_analysis_master " & _
'    '                "   JOIN " & _
'    '                "   ( " & _
'    '                "       SELECT " & _
'    '                "            hrassess_analysis_tran.analysisunkid " & _
'    '                "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'    '                "       FROM hrassess_analysis_tran " & _
'    '                "           JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'    '                "           JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'    '                "           JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                "           JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'    '                "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                "       GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'    '                "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'    '                "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'    '                "   GROUP BY selfemployeeunkid " & _
'    '                ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
'    '                "LEFT JOIN " & _
'    '                "( " & _
'    '                "   SELECT " & _
'    '                "        selfemployeeunkid " & _
'    '                "       ,SELF AS S_BSC " & _
'    '                "   FROM hrbsc_analysis_master " & _
'    '                "   JOIN " & _
'    '                "   ( " & _
'    '                "       SELECT " & _
'    '                "            analysisunkid " & _
'    '                "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'    '                "       FROM hrbsc_analysis_tran " & _
'    '                "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                "           AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                "       GROUP BY  analysisunkid " & _
'    '                "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'    '                ") AS S_BSC ON S_BSC.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "LEFT JOIN " & _
'    '                "( " & _
'    '                "   SELECT " & _
'    '                "        assessedemployeeunkid " & _
'    '                "       ,CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS A_GA " & _
'    '                "   FROM hrassess_analysis_master " & _
'    '                "   JOIN " & _
'    '                "   ( " & _
'    '                "       SELECT " & _
'    '                "            hrassess_analysis_tran.analysisunkid " & _
'    '                "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                "       FROM hrassess_analysis_tran " & _
'    '                "       JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'    '                "       JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'    '                "       JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                "       JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'    '                "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'    '                "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & mintPeriodId & "' " & _
'    '                "   GROUP BY assessedemployeeunkid " & _
'    '                ") AS A_GA ON A_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "LEFT JOIN " & _
'    '                "( " & _
'    '                "   SELECT " & _
'    '                "        assessedemployeeunkid " & _
'    '                "       ,CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS A_BSC " & _
'    '                "   FROM hrbsc_analysis_master " & _
'    '                "   JOIN " & _
'    '                "   ( " & _
'    '                "       SELECT " & _
'    '                "            analysisunkid " & _
'    '                "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'    '                "       FROM hrbsc_analysis_tran " & _
'    '                "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                "           AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                "       GROUP BY  analysisunkid " & _
'    '                "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'    '                "   GROUP BY assessedemployeeunkid " & _
'    '                ") AS A_BSC ON A_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "LEFT JOIN " & _
'    '                "( " & _
'    '                "   SELECT " & _
'    '                "        assessedemployeeunkid " & _
'    '                "       ,ISNULL(SUM(RevScore),0) AS R_GA " & _
'    '                "   FROM hrassess_analysis_master " & _
'    '                "   JOIN " & _
'    '                "   ( " & _
'    '                "       SELECT " & _
'    '                "            hrassess_analysis_tran.analysisunkid " & _
'    '                "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'    '                "       FROM hrassess_analysis_tran " & _
'    '                "           JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'    '                "           JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'    '                "           JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                "           JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'    '                "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'    '                "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & mintPeriodId & "' " & _
'    '                "   GROUP BY assessedemployeeunkid " & _
'    '                ") AS R_GA ON R_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "LEFT JOIN " & _
'    '                "( " & _
'    '                "   SELECT " & _
'    '                "        assessedemployeeunkid " & _
'    '                "       ,RevScore AS R_BSC " & _
'    '                "   FROM hrbsc_analysis_master " & _
'    '                "   JOIN " & _
'    '                "   ( " & _
'    '                "       SELECT " & _
'    '                "            analysisunkid " & _
'    '                "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'    '                "       FROM hrbsc_analysis_tran " & _
'    '                "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                "           AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                "       GROUP BY  analysisunkid " & _
'    '                "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'    '                ") AS R_BSC ON R_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                " WHERE 1 = 1 "
'    '        If mstrAllocationIds.Trim.Length > 0 Then
'    '            StrQ &= " AND " & mstrAllocationIds
'    '        End If
'    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'    '                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "

'    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
'    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
'    '        End If

'    '        If ConfigParameter._Object._ExcludeProbatedEmpOnPerformance = True Then
'    '            StrQ &= "		AND hremployee_master.employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM athremployee_master " & _
'    '                    "		WHERE CONVERT(CHAR(8),probation_from_date,112)<= @end_date AND CONVERT(CHAR(8),probation_to_date,112)>= @end_date) "

'    '            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
'    '        End If

'    '        'S.SANDEEP [ 10 SEPT 2013 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        StrQ &= ") AS A WHERE 1 = 1 AND Score > 0 "

'    '        If Me.OrderByQuery <> "" Then
'    '            StrQ &= "ORDER BY " & Me.OrderByQuery
'    '        End If
'    '        'S.SANDEEP [ 10 SEPT 2013 ] -- END


'    '        'S.SANDEEP [ 17 FEB 2014 ] -- START
'    '        'ENHANCEMENT : ENHANCEMENT
'    '        ' CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2)) -- Removed
'    '        ' IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))")  -- ADDED
'    '        'S.SANDEEP [ 17 FEB 2014 ] -- END


'    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Dim objRating As New clsAppraisal_Rating
'    '        dtRating = objRating._DataTable
'    '        objRating = Nothing

'    '        For Each dtRow As DataRow In dsList.Tables("List").Rows
'    '            If dtRating.Rows.Count > 0 Then
'    '                Dim dtmp() As DataRow = dtRating.Select(CDec(dtRow.Item("Score")) & " >= score_from AND " & CDec(dtRow.Item("Score")) & " <= score_to ")
'    '                If dtmp.Length > 0 Then
'    '                    dtRow.Item("Profile") = dtmp(0).Item("grade_award")
'    '                End If
'    '            End If
'    '            dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
'    '        Next

'    '        dsList.Tables("List").Columns.Remove("aDate")

'    '        Dim intArrayColumnWidth As Integer() = Nothing
'    '        ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
'    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
'    '            Select Case i
'    '                Case 0
'    '                    intArrayColumnWidth(i) = 80
'    '                Case 1, 2, 3, 4, 7
'    '                    intArrayColumnWidth(i) = 125
'    '                Case Else
'    '                    intArrayColumnWidth(i) = 50
'    '            End Select
'    '        Next
'    '        Dim sExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")
'    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, sExTitle & mstrPeriodName, " ", Nothing, "", True, Nothing, Nothing, Nothing)

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: Report_Allocation_Based; Module Name: " & mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub
'    Public Sub Report_Allocation_Based()
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Dim dsList As New DataSet
'        Dim dtRating As DataTable
'        Dim exForce As Exception
'        Try
'            Dim iStringValue As String = String.Empty
'            'S.SANDEEP [ 30 MAY 2014 ] -- START
'            'Dim objWSetting As New clsWeight_Setting(True)
'            Dim objWSetting As New clsWeight_Setting(mintPeriodId)
'            'S.SANDEEP [ 30 MAY 2014 ] -- END
'            If objWSetting._Weight_Typeid <= 0 Then
'                iStringValue = " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
'                iStringValue = " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
'                iStringValue = " AND kpiunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
'                iStringValue = " AND targetunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
'                iStringValue = " AND initiativeunkid > 0 "
'            End If

'            StrQ &= "SELECT " & _
'                    "  [Employee Code] " & _
'                    " ,[Employee Name] " & _
'                    mstrDisplayCols & " " & _
'                    " ,[Date Hired] " & _
'                    " ,CASE WHEN Score <= 0 THEN '' ELSE CAST(Score AS NVARCHAR(MAX)) END AS Score " & _
'                    " ,aDate "
'            If mblnOnProbationSelected = True Then
'                StrQ &= " ,CASE WHEN Score <= 0 THEN CASE WHEN [Date Hired] " & mstrCondition & " '" & eZeeDate.convertDate(mdtAppointmentDate) & "' THEN @Probation ELSE '' END ELSE '' END AS [Profile] "
'            Else
'                StrQ &= " ,[Profile] "
'            End If
'            Select Case ConfigParameter._Object._PerformanceComputation
'                Case 1 'Weight Avg Of
'                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", [Self Assessment] " & _
'                                ", [Assessor(s) Assessment] " & _
'                                ", [Reviewer Assessment] " & _
'                                ", [Self BSC] " & _
'                                ", [Assessor(s) BSC] " & _
'                                ", [Reviewer BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ", [Self Assessment] " & _
'                                ", [Assessor(s) Assessment] " & _
'                                ", [Self BSC] " & _
'                                ", [Assessor(s) BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", [Self Assessment] " & _
'                                ", [Reviewer Assessment] " & _
'                                ", [Self BSC] " & _
'                                ", [Reviewer BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", [Assessor(s) Assessment] " & _
'                                ", [Reviewer Assessment] " & _
'                                ", [Assessor(s) BSC] " & _
'                                ", [Reviewer BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ", [Self Assessment] " & _
'                                ", [Self BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ", [Assessor(s) Assessment] " & _
'                                ", [Assessor(s) BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", [Reviewer Assessment] " & _
'                                ", [Reviewer BSC] "
'                    End If
'                Case 2 'Weight Score Of
'                    If ConfigParameter._Object._WScrComputType = 1 Then
'                        StrQ &= ", [Self Assessment] " & _
'                                ", [Self BSC] "
'                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
'                        StrQ &= ", [Assessor(s) Assessment] " & _
'                                ", [Assessor(s) BSC] "
'                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
'                        StrQ &= ", [Reviewer Assessment] " & _
'                                ", [Reviewer BSC] "
'                    End If
'            End Select
'            StrQ &= " FROM ( " & _
'                    "SELECT " & _
'                   "  hremployee_master.employeecode AS [Employee Code] "
'            'S.SANDEEP [ 26 MAR 2014 ] -- START
'            ' " ,firstname + ' ' + surname  AS [Employee Name] " & _
'            If mblnFirstNamethenSurname = False Then
'                StrQ &= ", ISNULL(surname, '') + ' ' " & _
'                            "+ ISNULL(firstname, '') + ' ' " & _
'                            "+ ISNULL(othername, '') AS [Employee Name] "
'            Else
'                StrQ &= ", ISNULL(firstname, '') + ' ' " & _
'                            "+ ISNULL(othername, '') + ' ' " & _
'                            "+ ISNULL(surname, '') AS [Employee Name] "
'            End If
'            'S.SANDEEP [ 26 MAR 2014 ] -- END


'            StrQ &= mstrSelectedCols & " " & _
'                   " ,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
'                   " ,appointeddate AS aDate "
'            Select Case ConfigParameter._Object._PerformanceComputation
'                Case 1 'Weight Avg Of
'                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", CASE WHEN S_GA.S_GA <= 0 THEN '' ELSE ISNULL(CAST(S_GA.S_GA AS NVARCHAR(MAX)), '') END AS [Self Assessment] " & _
'                                ", CASE WHEN A_GA.A_GA <= 0 THEN '' ELSE ISNULL(CAST(A_GA.A_GA AS NVARCHAR(MAX)), '') END AS [Assessor(s) Assessment] " & _
'                                ", CASE WHEN R_GA.R_GA <= 0 THEN '' ELSE ISNULL(CAST(R_GA.R_GA AS NVARCHAR(MAX)), '') END AS [Reviewer Assessment] " & _
'                                ", CASE WHEN S_BSC.S_BSC <= 0 THEN '' ELSE ISNULL(CAST(S_BSC.S_BSC AS NVARCHAR(MAX)), '') END AS [Self BSC] " & _
'                                ", CASE WHEN A_BSC.A_BSC <= 0 THEN '' ELSE ISNULL(CAST(A_BSC.A_BSC AS NVARCHAR(MAX)), '') END AS [Assessor(s) BSC] " & _
'                                ", CASE WHEN R_BSC.R_BSC <= 0 THEN '' ELSE ISNULL(CAST(R_BSC.R_BSC AS NVARCHAR(MAX)), '') END AS [Reviewer BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ", CASE WHEN S_GA.S_GA <= 0 THEN '' ELSE ISNULL(CAST(S_GA.S_GA AS NVARCHAR(MAX)), '') END AS [Self Assessment] " & _
'                                ", CASE WHEN A_GA.A_GA <= 0 THEN '' ELSE ISNULL(CAST(A_GA.A_GA AS NVARCHAR(MAX)), '') END AS [Assessor(s) Assessment] " & _
'                                ", CASE WHEN S_BSC.S_BSC <= 0 THEN '' ELSE ISNULL(CAST(S_BSC.S_BSC AS NVARCHAR(MAX)), '') END AS [Self BSC] " & _
'                                ", CASE WHEN A_BSC.A_BSC <= 0 THEN '' ELSE ISNULL(CAST(A_BSC.A_BSC AS NVARCHAR(MAX)), '') END AS [Assessor(s) BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", CASE WHEN S_GA.S_GA <= 0 THEN '' ELSE ISNULL(CAST(S_GA.S_GA AS NVARCHAR(MAX)), '') END AS [Self Assessment] " & _
'                                ", CASE WHEN R_GA.R_GA <= 0 THEN '' ELSE ISNULL(CAST(R_GA.R_GA AS NVARCHAR(MAX)), '') END AS [Reviewer Assessment] " & _
'                                ", CASE WHEN S_BSC.S_BSC <= 0 THEN '' ELSE ISNULL(CAST(S_BSC.S_BSC AS NVARCHAR(MAX)), '') END AS [Self BSC] " & _
'                                ", CASE WHEN R_BSC.R_BSC <= 0 THEN '' ELSE ISNULL(CAST(R_BSC.R_BSC AS NVARCHAR(MAX)), '') END AS [Reviewer BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", CASE WHEN A_GA.A_GA <= 0 THEN '' ELSE ISNULL(CAST(A_GA.A_GA AS NVARCHAR(MAX)), '') END AS [Assessor(s) Assessment] " & _
'                                ", CASE WHEN R_GA.R_GA <= 0 THEN '' ELSE ISNULL(CAST(R_GA.R_GA AS NVARCHAR(MAX)), '') END AS [Reviewer Assessment] " & _
'                                ", CASE WHEN A_BSC.A_BSC <= 0 THEN '' ELSE ISNULL(CAST(A_BSC.A_BSC AS NVARCHAR(MAX)), '') END AS [Assessor(s) BSC] " & _
'                                ", CASE WHEN R_BSC.R_BSC <= 0 THEN '' ELSE ISNULL(CAST(R_BSC.R_BSC AS NVARCHAR(MAX)), '') END AS [Reviewer BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ", CASE WHEN S_GA.S_GA <= 0 THEN '' ELSE ISNULL(CAST(S_GA.S_GA AS NVARCHAR(MAX)), '') END AS [Self Assessment] " & _
'                                ", CASE WHEN S_BSC.S_BSC <= 0 THEN '' ELSE ISNULL(CAST(S_BSC.S_BSC AS NVARCHAR(MAX)), '') END AS [Self BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ", CASE WHEN A_GA.A_GA <= 0 THEN '' ELSE ISNULL(CAST(A_GA.A_GA AS NVARCHAR(MAX)), '') END AS [Assessor(s) Assessment] " & _
'                                ", CASE WHEN A_BSC.A_BSC <= 0 THEN '' ELSE ISNULL(CAST(A_BSC.A_BSC AS NVARCHAR(MAX)), '') END AS [Assessor(s) BSC] "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ", CASE WHEN R_GA.R_GA <= 0 THEN '' ELSE ISNULL(CAST(R_GA.R_GA AS NVARCHAR(MAX)), '') END AS [Reviewer Assessment] " & _
'                                ", CASE WHEN R_BSC.R_BSC <= 0 THEN '' ELSE ISNULL(CAST(R_BSC.R_BSC AS NVARCHAR(MAX)), '') END AS [Reviewer BSC] "
'                    End If
'                Case 2 'Weight Score Of
'                    If ConfigParameter._Object._WScrComputType = 1 Then
'                        StrQ &= ", CASE WHEN S_GA.S_GA <= 0 THEN '' ELSE ISNULL(CAST(S_GA.S_GA AS NVARCHAR(MAX)), '') END AS [Self Assessment] " & _
'                                ", CASE WHEN S_BSC.S_BSC <= 0 THEN '' ELSE ISNULL(CAST(S_BSC.S_BSC AS NVARCHAR(MAX)), '') END AS [Self BSC] "
'                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
'                        StrQ &= ", CASE WHEN A_GA.A_GA <= 0 THEN '' ELSE ISNULL(CAST(A_GA.A_GA AS NVARCHAR(MAX)), '') END AS [Assessor(s) Assessment] " & _
'                                ", CASE WHEN A_BSC.A_BSC <= 0 THEN '' ELSE ISNULL(CAST(A_BSC.A_BSC AS NVARCHAR(MAX)), '') END AS [Assessor(s) BSC] "
'                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
'                        StrQ &= ", CASE WHEN R_GA.R_GA <= 0 THEN '' ELSE ISNULL(CAST(R_GA.R_GA AS NVARCHAR(MAX)), '') END AS [Reviewer Assessment] " & _
'                                ", CASE WHEN R_BSC.R_BSC <= 0 THEN '' ELSE ISNULL(CAST(R_BSC.R_BSC AS NVARCHAR(MAX)), '') END AS [Reviewer BSC] "
'                    End If
'            End Select
'            Select Case ConfigParameter._Object._PerformanceComputation
'                Case 1 'Weight Avg Of
'                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
'                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
'                        StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    End If
'                Case 2 'Weight Score Of
'                    If ConfigParameter._Object._WScrComputType = 1 Then
'                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
'                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
'                        StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
'                    End If
'            End Select
'            'S.SANDEEP [ 05 NOV 2014 ] -- START
'            'StrQ &= ",'' AS [Profile] " & _
'            '        "FROM hremployee_master " & _
'            '        mstrSelectedJoin & " " & _
'            '        "	JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & mintPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        selfemployeeunkid AS EmpId " & _
'            '        "       ,ISNULL(SUM(SELF),0) AS S_GA " & _
'            '        "   FROM hrassess_analysis_master " & _
'            '        "   JOIN " & _
'            '        "   ( " & _
'            '        "       SELECT " & _
'            '        "            hrassess_analysis_tran.analysisunkid " & _
'            '        "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'            '        "       FROM hrassess_analysis_tran " & _
'            '        "           JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'            '        "           JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'            '        "           JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'            '        "           JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'            '        "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'            '        "       GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'            '        "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'            '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "   GROUP BY selfemployeeunkid " & _
'            '        ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        selfemployeeunkid " & _
'            '        "       ,SELF AS S_BSC " & _
'            '        "   FROM hrbsc_analysis_master " & _
'            '        "   JOIN " & _
'            '        "   ( " & _
'            '        "       SELECT " & _
'            '        "            analysisunkid " & _
'            '        "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'            '        "       FROM hrbsc_analysis_tran " & _
'            '        "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'            '        "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'            '        iStringValue & " " & _
'            '        "       GROUP BY  analysisunkid " & _
'            '        "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'            '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        ") AS S_BSC ON S_BSC.selfemployeeunkid = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        assessedemployeeunkid " & _
'            '        "       ,CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS A_GA " & _
'            '        "   FROM hrassess_analysis_master " & _
'            '        "   JOIN " & _
'            '        "   ( " & _
'            '        "       SELECT " & _
'            '        "            hrassess_analysis_tran.analysisunkid " & _
'            '        "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'            '        "       FROM hrassess_analysis_tran " & _
'            '        "       JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'            '        "       JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'            '        "       JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'            '        "       JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'            '        "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'            '        "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'            '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & mintPeriodId & "' " & _
'            '        "   GROUP BY assessedemployeeunkid " & _
'            '        ") AS A_GA ON A_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        assessedemployeeunkid " & _
'            '        "       ,CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS A_BSC " & _
'            '        "   FROM hrbsc_analysis_master " & _
'            '        "   JOIN " & _
'            '        "   ( " & _
'            '        "       SELECT " & _
'            '        "            analysisunkid " & _
'            '        "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'            '        "       FROM hrbsc_analysis_tran " & _
'            '        "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'            '        "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'            '        iStringValue & " " & _
'            '        "       GROUP BY  analysisunkid " & _
'            '        "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'            '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "   GROUP BY assessedemployeeunkid " & _
'            '        ") AS A_BSC ON A_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        assessedemployeeunkid " & _
'            '        "       ,ISNULL(SUM(RevScore),0) AS R_GA " & _
'            '        "   FROM hrassess_analysis_master " & _
'            '        "   JOIN " & _
'            '        "   ( " & _
'            '        "       SELECT " & _
'            '        "            hrassess_analysis_tran.analysisunkid " & _
'            '        "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'            '        "       FROM hrassess_analysis_tran " & _
'            '        "           JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'            '        "           JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'            '        "           JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'            '        "           JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'            '        "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'            '        "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'            '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & mintPeriodId & "' " & _
'            '        "   GROUP BY assessedemployeeunkid " & _
'            '        ") AS R_GA ON R_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        assessedemployeeunkid " & _
'            '        "       ,RevScore AS R_BSC " & _
'            '        "   FROM hrbsc_analysis_master " & _
'            '        "   JOIN " & _
'            '        "   ( " & _
'            '        "       SELECT " & _
'            '        "            analysisunkid " & _
'            '        "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'            '        "       FROM hrbsc_analysis_tran " & _
'            '        "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'            '        "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'            '        iStringValue & " " & _
'            '        "       GROUP BY  analysisunkid " & _
'            '        "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'            '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        ") AS R_BSC ON R_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'            '        " WHERE 1 = 1 "


'            'S.SANDEEP [14 MAR 2016] -- START
'            'StrQ &= ",'' AS [Profile] " & _
'            '        "FROM hremployee_master " & _
'            '        mstrSelectedJoin & " " & _
'            '        "	JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & mintPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "        selfemployeeunkid AS EmpId " & _
'            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
'            '        "       FROM hrevaluation_analysis_master " & _
'            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
'            '        "   GROUP BY selfemployeeunkid " & _
'            '        ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "            selfemployeeunkid AS EmpId " & _
'            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
'            '        "       FROM hrevaluation_analysis_master " & _
'            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
'            '        "       GROUP BY selfemployeeunkid " & _
'            '        "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "            assessedemployeeunkid AS EmpId " & _
'            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
'            '        "       FROM hrevaluation_analysis_master " & _
'            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
'            '        "   GROUP BY assessedemployeeunkid " & _
'            '        "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "            assessedemployeeunkid AS EmpId " & _
'            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
'            '        "       FROM hrevaluation_analysis_master " & _
'            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
'            '        "   GROUP BY assessedemployeeunkid " & _
'            '        "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "            assessedemployeeunkid AS EmpId " & _
'            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
'            '        "       FROM hrevaluation_analysis_master " & _
'            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
'            '        "   GROUP BY assessedemployeeunkid " & _
'            '        "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "   SELECT " & _
'            '        "            assessedemployeeunkid AS EmpId " & _
'            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
'            '        "       FROM hrevaluation_analysis_master " & _
'            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
'            '        "       GROUP BY assessedemployeeunkid " & _
'            '        "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
'            '        " WHERE 1 = 1 "

'            StrQ &= ",'' AS [Profile] " & _
'                    "FROM hremployee_master " & _
'                    mstrSelectedJoin & " " & _
'                    "	JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & mintPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "   SELECT " & _
'                    "        selfemployeeunkid AS EmpId " & _
'                    "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
'                    "       FROM hrevaluation_analysis_master " & _
'                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                    "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
'                    "   GROUP BY selfemployeeunkid " & _
'                    ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "   SELECT " & _
'                    "            selfemployeeunkid AS EmpId " & _
'                    "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
'                    "       FROM hrevaluation_analysis_master " & _
'                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                    "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
'                    "       GROUP BY selfemployeeunkid " & _
'                    "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "   SELECT " & _
'                    "            assessedemployeeunkid AS EmpId " & _
'                    "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
'                    "       FROM hrevaluation_analysis_master " & _
'                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
'                    "   GROUP BY assessedemployeeunkid " & _
'                    "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "   SELECT " & _
'                    "            assessedemployeeunkid AS EmpId " & _
'                    "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
'                    "       FROM hrevaluation_analysis_master " & _
'                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
'                    "   GROUP BY assessedemployeeunkid " & _
'                    "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "   SELECT " & _
'                    "            assessedemployeeunkid AS EmpId " & _
'                    "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
'                    "       FROM hrevaluation_analysis_master " & _
'                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
'                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
'                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
'                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
'                    "   GROUP BY assessedemployeeunkid " & _
'                    "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "   SELECT " & _
'                    "            assessedemployeeunkid AS EmpId " & _
'                    "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
'                    "       FROM hrevaluation_analysis_master " & _
'                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
'                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
'                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
'                    "       GROUP BY assessedemployeeunkid " & _
'                    "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
'                    " WHERE 1 = 1 "
'            'S.SANDEEP [14 MAR 2016] -- END


'            'S.SANDEEP [ 05 NOV 2014 ] -- END


'            If mstrAllocationIds.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAllocationIds
'            End If

'            If mstrAdvanceFilter.Trim.Length > 0 Then
'                StrQ &= mstrAdvanceFilter
'            End If

'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
'            End If

'            If ConfigParameter._Object._ExcludeProbatedEmpOnPerformance = True Then
'                StrQ &= "		AND hremployee_master.employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM athremployee_master " & _
'                        "		WHERE CONVERT(CHAR(8),probation_from_date,112)<= @end_date AND CONVERT(CHAR(8),probation_to_date,112)>= @end_date) "

'                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
'            End If

'            StrQ &= ") AS A WHERE 1 = 1 "

'            If Me.OrderByQuery <> "" Then
'                StrQ &= "ORDER BY " & Me.OrderByQuery
'            End If

'            objDataOperation.AddParameter("@Probation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "On Probation"))

'            'S.SANDEEP [ 05 NOV 2014 ] -- START
'            objDataOperation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)
'            'S.SANDEEP [ 05 NOV 2014 ] -- END


'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Dim objRating As New clsAppraisal_Rating
'            dtRating = objRating._DataTable
'            objRating = Nothing

'            For Each dtRow As DataRow In dsList.Tables("List").Rows
'                If dtRating.Rows.Count > 0 Then
'                    If dtRow.Item("Score").ToString.Trim.Length > 0 Then
'                        Dim dtmp() As DataRow = dtRating.Select(CDec(dtRow.Item("Score")) & " >= score_from AND " & CDec(dtRow.Item("Score")) & " <= score_to ")
'                        If dtmp.Length > 0 Then
'                            dtRow.Item("Profile") = dtmp(0).Item("grade_award")
'                        End If
'                    End If
'                End If
'                dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
'            Next

'            dsList.Tables("List").Columns.Remove("aDate")

'            Dim intArrayColumnWidth As Integer() = Nothing
'            ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
'            For i As Integer = 0 To intArrayColumnWidth.Length - 1
'                Select Case i
'                    Case 0
'                        intArrayColumnWidth(i) = 80
'                    Case 1, 2, 3, 4, 7
'                        intArrayColumnWidth(i) = 125
'                    Case Else
'                        intArrayColumnWidth(i) = 50
'                End Select
'            Next
'            Dim iExFliter As String = String.Empty
'            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")
'            iExFliter = mstrCaption & " " & mstrCondition & " " & mdtAppointmentDate.Date.ToShortDateString
'            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing)

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Report_Allocation_Based; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 28 FEB 2014 ] -- END

'    Public Function Get_Probated_EmpIds(ByVal dtEndDate As DateTime) As String
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim strIds As String = String.Empty
'        Dim dsList As New DataSet
'        Try
'            Dim objData As New clsDataOperation

'            StrQ = "SELECT ISNULL( STUFF((SELECT DISTINCT ',' + CAST(employeeunkid AS NVARCHAR(50)) FROM athremployee_master " & _
'                   "WHERE CONVERT(CHAR(8),probation_from_date,112)<= @end_date AND CONVERT(CHAR(8),probation_to_date,112)>= @end_date " & _
'                   "FOR XML PATH('')),1,1,''),'') AS Ids "
'            objData.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtEndDate))

'            dsList = objData.ExecQuery(StrQ, "List")

'            If objData.ErrorMessage <> "" Then
'                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                strIds = dsList.Tables(0).Rows(0).Item("Ids").ToString.Trim
'            End If

'            Return strIds
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_Probated_Emp; Module Name: " & mstrModuleName)
'            Return strIds
'        Finally
'        End Try
'    End Function

'    'S.SANDEEP [21 AUG 2015] -- START
'    Public Sub Report_Allocation_Based_New(ByVal strDatabaseName As String, _
'                                           ByVal intUserUnkid As Integer, _
'                                           ByVal intYearUnkid As Integer, _
'                                           ByVal intCompanyUnkid As Integer, _
'                                           ByVal dtPeriodStart As Date, _
'                                           ByVal dtPeriodEnd As Date, _
'                                           ByVal strUserModeSetting As String, _
'                                           ByVal blnOnlyApproved As Boolean)
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Dim dsList As New DataSet
'        Dim dtRating As DataTable
'        Dim exForce As Exception
'        Try
'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            dtPeriodStart = mdtStartDate : dtPeriodEnd = mdtEndDate

'            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
'            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
'            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
'            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
'            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
'            'S.SANDEEP [04 JUN 2015] -- END

'            StrQ = "SELECT " & _
'                    "	 employeecode AS [Employee Code] "
'            If mblnFirstNamethenSurname = False Then
'                StrQ &= ", ISNULL(surname, '') + ' ' " & _
'                            "+ ISNULL(firstname, '') + ' ' " & _
'                            "+ ISNULL(othername, '') AS [Employee Name] "
'            Else
'                StrQ &= ", ISNULL(firstname, '') + ' ' " & _
'                            "+ ISNULL(othername, '') + ' ' " & _
'                            "+ ISNULL(surname, '') AS [Employee Name] "
'            End If

'            'S.SANDEEP [27 Jan 2016] -- START
'            'StrQ &= mstrSelectedCols & " " & _
'            '        "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2)) AS Score " & _
'            '        "   ,'' AS [Profile] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2)) AS [Self Assessment] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2)) AS [Assessor(s) Assessment] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2)) AS [Reviewer Assessment] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2)) AS [Self BSC] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2))AS [Assessor(s) BSC] " & _
'            '        "   ,CAST(0 AS DECIMAL(30,2)) AS [Reviewer BSC] " & _
'            '        "	,hremployee_master.employeeunkid AS EId " & _
'            '        "	,'" & mintPeriodId & "' AS PId " & _
'            '        "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
'            '        "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
'            '        "   ,appointeddate AS aDate " & _
'            '        "FROM hremployee_master " & _
'            '        mstrSelectedJoin & " " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "	SELECT " & _
'            '        "		 hrassessor_master.assessormasterunkid AS AsrMId " & _
'            '        "		,hrassessor_tran.employeeunkid AS AEId " & _
'            '        "	FROM hrassessor_tran " & _
'            '        "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '        "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '        "	WHERE hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'            '        ") AS ASR ON ASR.AEId = hremployee_master.employeeunkid " & _
'            '        "LEFT JOIN " & _
'            '        "( " & _
'            '        "	SELECT " & _
'            '        "		 hrassessor_master.assessormasterunkid AS RevMId " & _
'            '        "		,hrassessor_tran.employeeunkid AS REId " & _
'            '        "	FROM hrassessor_tran " & _
'            '        "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '        "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '        "	WHERE hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'            '        ") AS REV ON REV.REId = hremployee_master.employeeunkid " & _
'            '        "WHERE isapproved = 1 "

'            StrQ &= mstrSelectedCols & " " & _
'                    "	,CONVERT(CHAR(8),appointeddate,112) AS [Date Hired] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2)) AS Score " & _
'                    "   ,'' AS [Profile] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2)) AS [Self Assessment] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2)) AS [Assessor(s) Assessment] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2)) AS [Reviewer Assessment] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2)) AS [Self BSC] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2))AS [Assessor(s) BSC] " & _
'                    "   ,CAST(0 AS DECIMAL(30,2)) AS [Reviewer BSC] " & _
'                    "	,hremployee_master.employeeunkid AS EId " & _
'                    "	,'" & mintPeriodId & "' AS PId " & _
'                    "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
'                    "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
'                    "   ,appointeddate AS aDate " & _
'                    "FROM hremployee_master " & _
'                    mstrSelectedJoin & " " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "	SELECT " & _
'                    "		 hrassessor_master.assessormasterunkid AS AsrMId " & _
'                    "		,hrassessor_tran.employeeunkid AS AEId " & _
'                    "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
'                    "	FROM hrassessor_tran " & _
'                    "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                    "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
'                    "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
'                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'                    "	WHERE hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'                    ") AS ASR ON ASR.AEId = hremployee_master.employeeunkid AND ASR.analysisunkid > 0 " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "	SELECT " & _
'                    "		 hrassessor_master.assessormasterunkid AS RevMId " & _
'                    "		,hrassessor_tran.employeeunkid AS REId " & _
'                    "       ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) analysisunkid " & _
'                    "	FROM hrassessor_tran " & _
'                    "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                    "   LEFT JOIN hrevaluation_analysis_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.reviewerunkid AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' AND hrevaluation_analysis_master.isvoid = 0 " & _
'                    "       AND hrevaluation_analysis_master.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
'                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'                    "	WHERE hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
'                    ") AS REV ON REV.REId = hremployee_master.employeeunkid AND REV.analysisunkid > 0 "


'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            If xDateJoinQry.Trim.Length > 0 Then
'                StrQ &= xDateJoinQry
'            End If

'            If xUACQry.Trim.Length > 0 Then
'                StrQ &= xUACQry
'            End If

'            If xAdvanceJoinQry.Trim.Length > 0 Then
'                StrQ &= xAdvanceJoinQry
'            End If

'            'S.SANDEEP [04 JUN 2015] -- END

'            StrQ &= "WHERE isapproved = 1 "

'            If mstrAllocationIds.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAllocationIds
'            End If

'            If mstrAdvanceFilter.Trim.Length > 0 Then
'                StrQ &= " AND " & mstrAdvanceFilter
'            End If

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "

'            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
'            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
'            'End If


'            If xUACFiltrQry.Trim.Length > 0 Then
'                StrQ &= " AND " & xUACFiltrQry & " "
'            End If

'            If xDateFilterQry.Trim.Length > 0 Then
'                StrQ &= xDateFilterQry & " "
'            End If
'            'S.SANDEEP [04 JUN 2015] -- END


'            If mblnOnProbationSelected = True Then
'                StrQ &= " AND hremployee_master.employeeunkid " & _
'                        "NOT IN " & _
'                        "( " & _
'                        "   SELECT " & _
'                        "	    Probation.employeeunkid " & _
'                        "   FROM " & _
'                        "   ( " & _
'                        "	    SELECT " & _
'                        "		     employeeunkid " & _
'                        "		    ,CONVERT(CHAR(8),date1,112) AS PFD " & _
'                        "		    ,CONVERT(CHAR(8),date2,112) AS PTD " & _
'                        "		    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
'                        "	    FROM hremployee_dates_tran " & _
'                        "	    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
'                        "	    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
'                        "   ) AS Probation WHERE Probation.Rno = 1 " & _
'                        "   AND Probation.PFD <= '" & eZeeDate.convertDate(mdtEndDate) & "' AND Probation.PTD >= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
'                        ")"
'            End If

'            If Me.OrderByQuery <> "" Then
'                StrQ &= "ORDER BY " & Me.OrderByQuery
'            End If

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If


'            Dim objRating As New clsAppraisal_Rating
'            dtRating = objRating._DataTable
'            objRating = Nothing


'            Dim objCmpuMst As New clsassess_computation_master
'            Dim mblnIsSelfIncluded As Boolean = False
'            Dim mblnIsAssrIncluded As Boolean = False
'            Dim mblnIsRevrIncluded As Boolean = False
'            Dim xFormulaTable As DataTable = Nothing

'            xFormulaTable = objCmpuMst.Get_Computation_TranVariables(mintPeriodId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE)
'            If xFormulaTable.Rows.Count > 0 Then
'                Dim xMatchValue As String = String.Empty
'                For Each eValue As enAssess_Computation_Formulas In [Enum].GetValues(GetType(enAssess_Computation_Formulas))
'                    For Each xRow As DataRow In xFormulaTable.Rows
'                        xMatchValue = "1000" & eValue
'                        Select Case eValue
'                            Case enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, enAssess_Computation_Formulas.EMP_OVERALL_SCORE
'                                If xRow.Item("computation_typeid") = xMatchValue Then
'                                    mblnIsSelfIncluded = True
'                                End If
'                            Case enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, enAssess_Computation_Formulas.ASR_OVERALL_SCORE
'                                If xRow.Item("computation_typeid") = xMatchValue Then
'                                    mblnIsAssrIncluded = True
'                                End If
'                            Case enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, enAssess_Computation_Formulas.REV_OVERALL_SCORE
'                                If xRow.Item("computation_typeid") = xMatchValue Then
'                                    mblnIsRevrIncluded = True
'                                End If
'                        End Select
'                    Next
'                Next
'            End If
'            objCmpuMst = Nothing


'            For Each dtRow As DataRow In dsList.Tables("List").Rows

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'Call SetScoreValue(dtRow, mblnIsSelfIncluded, mblnIsAssrIncluded, mblnIsRevrIncluded)
'                Call SetScoreValue(dtRow, mblnIsSelfIncluded, mblnIsAssrIncluded, mblnIsRevrIncluded, mdtEndDate)
'                'S.SANDEEP [04 JUN 2015] -- END


'                If dtRating.Rows.Count > 0 Then
'                    If dtRow.Item("Score").ToString.Trim.Length > 0 Then
'                        Dim dtmp() As DataRow = dtRating.Select(CDec(dtRow.Item("Score")) & " >= score_from AND " & CDec(dtRow.Item("Score")) & " <= score_to ")
'                        If dtmp.Length > 0 Then
'                            dtRow.Item("Profile") = dtmp(0).Item("grade_award")
'                        End If
'                    End If
'                End If

'                If mblnOnProbationSelected = True Then
'                    'S.SANDEEP [15 FEB 2016] -- START
'                    'If dtRow.Item("Score").ToString.Trim.Length > 0 AndAlso CDec(dtRow.Item("Score")) <= 0 Then
'                    '                        If dtRow.Item("Date Hired") & " " & mstrCondition & " '" & eZeeDate.convertDate(mdtAppointmentDate) & "' " Then
'                    '                            dtRow.Item("Profile") = Language.getMessage(mstrModuleName, 19, "On Probation")
'                    '                        End If
'                    '                    End If

'                    If dtRow.Item("Score").ToString.Trim.Length > 0 AndAlso CDec(dtRow.Item("Score")) <= 0 Then
'                        Dim mstrFilterCondition As String = "[Date Hired] " & mstrCondition & eZeeDate.convertDate(mdtAppointmentDate)
'                        If dsList.Tables(0).Select(mstrFilterCondition & " AND [Employee Code] = '" & dtRow("Employee Code") & "'").Length > 0 Then
'                            dtRow.Item("Profile") = Language.getMessage(mstrModuleName, 19, "On Probation")
'                        End If
'                    End If
'                    'S.SANDEEP [15 FEB 2016] -- END                    

'                End If

'                dtRow.Item("Date Hired") = eZeeDate.convertDate(dtRow.Item("Date Hired").ToString).ToShortDateString
'            Next

'            dsList.Tables("List").Columns.Remove("aDate")
'            dsList.Tables("List").Columns.Remove("EId")
'            dsList.Tables("List").Columns.Remove("PId")
'            dsList.Tables("List").Columns.Remove("AsrMstId")
'            dsList.Tables("List").Columns.Remove("RevMstId")

'            Dim intArrayColumnWidth As Integer() = Nothing
'            ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
'            For i As Integer = 0 To intArrayColumnWidth.Length - 1
'                Select Case i
'                    Case 0
'                        intArrayColumnWidth(i) = 80
'                    Case 1, 2, 3, 4, 7
'                        intArrayColumnWidth(i) = 125
'                    Case Else
'                        intArrayColumnWidth(i) = 50
'                End Select
'            Next

'            Dim iExFliter As String = String.Empty
'            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")
'            iExFliter = mstrCaption & " " & mstrCondition & " " & mdtAppointmentDate.Date.ToShortDateString

'            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName & " " & mstrReportType_Name, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing)

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Report_Allocation_Based; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [04 JUN 2015] -- START
'    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    Private Sub SetScoreValue(ByVal dRow As DataRow, ByVal mblnIsSelfIncluded As Boolean, ByVal mblnIsAssrIncluded As Boolean, ByVal mblnIsRevrIncluded As Boolean, ByVal dtEmployeeAsOnDate As DateTime)
'        'Private Sub SetScoreValue(ByVal dRow As DataRow, ByVal mblnIsSelfIncluded As Boolean, ByVal mblnIsAssrIncluded As Boolean, ByVal mblnIsRevrIncluded As Boolean)
'        'S.SANDEEP [04 JUN 2015] -- END

'        Try
'            Dim xEvaluation As New clsevaluation_analysis_master
'            If dRow IsNot Nothing Then

'                dRow.Item("Self Assessment") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                                                                                     False, _
'                                                                                     mintScoringOptionId, _
'                                                                                     enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                                                                                     dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                dRow.Item("Assessor(s) Assessment") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                                                     False, _
'                                                                                     mintScoringOptionId, _
'                                                                                     enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                dRow.Item("Reviewer Assessment") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                                                                     False, _
'                                                                                     mintScoringOptionId, _
'                                                                                     enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END



'                'S.SANDEEP [27 Jan 2016] -- START
'                'dRow.Item("Self BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                '                                                                     False, _
'                '                                                                     mintScoringOptionId, _
'                '                                                                     enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                'dRow.Item("Assessor(s) BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                '                                                                     False, _
'                '                                                                     mintScoringOptionId, _
'                '                                                                     enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                'dRow.Item("Reviewer BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                '                                                                     False, _
'                '                                                                     mintScoringOptionId, _
'                '                                                                     enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                '                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END


'                dRow.Item("Self BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                                                                              True, _
'                                                                                     mintScoringOptionId, _
'                                                                                     enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                                                                                     dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                dRow.Item("Assessor(s) BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                                                     True, _
'                                                                                     mintScoringOptionId, _
'                                                                                     enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                dRow.Item("Reviewer BSC") = Format(CDec(xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                                                                     True, _
'                                                                                     mintScoringOptionId, _
'                                                                                     enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, dtEmployeeAsOnDate, _
'                                                                                     dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies)), "#####0.#0") 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

'                'S.SANDEEP [27 Jan 2016] -- END

'                Dim xOverallScore As Decimal = 0
'                If mblnIsSelfIncluded Then
'                    xOverallScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If

'                If mblnIsAssrIncluded Then
'                    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If

'                If mblnIsRevrIncluded Then
'                    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                    xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dRow.Item("EId"), dRow.Item("PId"), , , dRow.Item("RevMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
'                End If

'                dRow.Item("Score") = CDec(Format(CDec(xOverallScore), "#####0.#0"))


'                dRow.AcceptChanges()

'            End If
'            xEvaluation = Nothing
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetScoreValue; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [21 AUG 2015] -- END

'#End Region

'End Class
'Shani(26-APR-2016) -- End





'Public Class clsAssessSoreRatingReport1
'    Inherits IReportData
'    Private Shared ReadOnly mstrModuleName As String = "clsAssessSoreRatingReport"
'    Private mstrReportId As String = enArutiReport.EmployeeAssessSoreRatingReport
'    Private mdtFinalTable As DataTable
'    Private dsColumns As New DataSet
'    Dim dblBSC_Percent As Integer
'    Dim dblGeneral_Percent As Integer
'    Dim dblGETotal As Double = 0
'    Dim iCnt As Integer = 0
'    Dim dSelf() As DataRow = Nothing
'    Dim dAssess() As DataRow = Nothing
'    Dim dEAssess() As DataRow = Nothing
'    Dim dReview() As DataRow = Nothing
'    Dim StrFinalPath As String = String.Empty

'#Region " Constructor "

'    Public Sub New()
'        Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
'    End Sub

'#End Region

'    'S.SANDEEP [ 02 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'#Region " ENUM "

'    Public Enum enWeightedAverage
'        WEIGHTEDAVG_ALL = 1
'        WEIGHTEDAVG_ASSESSOR = 2
'        WEIGHTEDAVG_REVIEWER = 3
'        WEIGHTEDAVG_ASSESSOR_REVIEWER = 4
'    End Enum

'#End Region
'    'S.SANDEEP [ 28 APRIL 2012 ] -- END

'#Region " Private Variables "

'    Private mintEmployeeId As Integer = 0
'    Private mstrEmployeeName As String = String.Empty
'    Private mintPeriodId As Integer = 0
'    Private mstrPeriodName As String = String.Empty
'    Private blnIncludeUncommitedInfo As Boolean = False

'    'S.SANDEEP [ 02 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintWeightedAvgBy As Integer = 0
'    'S.SANDEEP [ 28 APRIL 2012 ] -- END

'#End Region

'#Region " Properties "

'    Public WriteOnly Property _EmployeeId() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeName() As String
'        Set(ByVal value As String)
'            mstrEmployeeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _PeriodId() As Integer
'        Set(ByVal value As Integer)
'            mintPeriodId = value
'        End Set
'    End Property

'    Public WriteOnly Property _PeriodName() As String
'        Set(ByVal value As String)
'            mstrPeriodName = value
'        End Set
'    End Property

'    Public WriteOnly Property _IncludeUncommitedInfo() As Boolean
'        Set(ByVal value As Boolean)
'            blnIncludeUncommitedInfo = value
'        End Set
'    End Property

'    'S.SANDEEP [ 02 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _WeightedAvgBy() As Integer
'        Set(ByVal value As Integer)
'            mintWeightedAvgBy = value
'        End Set
'    End Property
'    'S.SANDEEP [ 28 APRIL 2012 ] -- END

'#End Region

'#Region " Public Function(s) And Procedures "

'    Public Sub SetDefaultValue()
'        Try
'            mintEmployeeId = 0
'            mstrEmployeeName = String.Empty
'            mintPeriodId = 0
'            mstrPeriodName = String.Empty
'            blnIncludeUncommitedInfo = False
'            'S.SANDEEP [ 02 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mintWeightedAvgBy = 0
'            'S.SANDEEP [ 28 APRIL 2012 ] -- END
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

'    End Sub

'    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

'    End Sub

'    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

'    End Sub

'    Private Sub CreateTable()
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Try
'            mdtFinalTable = New DataTable("Data")

'            mdtFinalTable.Columns.Add("referencename", System.Type.GetType("System.String")).Caption = ""

'            StrQ = "    SELECT " & _
'                   "        'S_' + CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) AS AId " & _
'                   "        ,@Self + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'                   "        ,1 AS Mode " & _
'                   "        ,hremployee_master.employeeunkid AS EmpId " & _
'                   "    FROM hremployee_master " & _
'                   "    WHERE hremployee_master.employeeunkid = '" & mintEmployeeId & "' " & _
'                   "UNION ALL " & _
'                   "    SELECT " & _
'                   "         'A_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'                   "        ,@Appriser + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'                   "        ,2 AS Mode " & _
'                   "        ,hrassessor_master.assessormasterunkid " & _
'                   "    FROM hrassessor_tran " & _
'                   "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   "        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'                   "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 0 " & _
'                   "UNION ALL " & _
'                   "    SELECT " & _
'                   "         'E_' + CAST(hrexternal_assessor_master.ext_assessorunkid AS NVARCHAR(50)) AS AId " & _
'                   "        ,@ExAssessor + CHAR(13) + ' [ ' + hrexternal_assessor_master.displayname+' ] ' " & _
'                   "        ,3 AS Mode " & _
'                   "        ,hrexternal_assessor_master.ext_assessorunkid " & _
'                   "    FROM hrexternal_assessor_master " & _
'                   "    WHERE employeeunkid = '" & mintEmployeeId & "' " & _
'                   "UNION ALL " & _
'                   "    SELECT " & _
'                   "         'R_' + CAST(hrassessor_master.assessormasterunkid AS NVARCHAR(50)) AS AId " & _
'                   "        ,@Reviewer + CHAR(13) + ' [ ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') +' ] ' AS Appriser " & _
'                   "        ,4 AS Mode " & _
'                   "        ,hrassessor_master.assessormasterunkid " & _
'                   "    FROM hrassessor_tran " & _
'                   "        JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   "        JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'                   "    WHERE hrassessor_tran.employeeunkid = '" & mintEmployeeId & "' AND hrassessor_master.isreviewer = 1 "

'            objDataOperation.AddParameter("@Self", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Self"))
'            objDataOperation.AddParameter("@Appriser", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor"))
'            objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Reviewer"))
'            objDataOperation.AddParameter("@ExAssessor", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor"))

'            dsColumns = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'            End If

'            Dim dCol As DataColumn

'            If dsColumns.Tables("List").Rows.Count > 0 Then
'                For Each dRow As DataRow In dsColumns.Tables("List").Rows
'                    dCol = New DataColumn

'                    dCol.ColumnName = "Column" & dRow.Item("AId").ToString
'                    dCol.Caption = dRow.Item("Appriser").ToString
'                    dCol.DataType = System.Type.GetType("System.Decimal")

'                    mdtFinalTable.Columns.Add(dCol)
'                Next
'            End If

'            mdtFinalTable.Columns.Add("total", System.Type.GetType("System.Decimal")).Caption = "Total"
'            mdtFinalTable.Columns.Add("total_percent", System.Type.GetType("System.Decimal")).Caption = "Total %"

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Public Sub Export_Assessment_Report()
'        Try
'            Call CreateTable()

'            Select Case mintWeightedAvgBy
'                Case enWeightedAverage.WEIGHTEDAVG_ALL

'                    dSelf = dsColumns.Tables("List").Select("Mode = 1")    'SETTING SELF ASSESSMENT
'                    dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'                    dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'                    dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT

'                Case enWeightedAverage.WEIGHTEDAVG_ASSESSOR

'                    dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'                    dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT

'                Case enWeightedAverage.WEIGHTEDAVG_REVIEWER

'                    dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT

'                Case enWeightedAverage.WEIGHTEDAVG_ASSESSOR_REVIEWER

'                    dAssess = dsColumns.Tables("List").Select("Mode = 2")  'SETTING ASSESSOR ASSESSMENT
'                    dEAssess = dsColumns.Tables("List").Select("Mode = 3") 'SETTING EXTERNAL ASSESSOR ASSESSMENT
'                    dReview = dsColumns.Tables("List").Select("Mode = 4")  'SETTING REVIEWER ASSESSMENT
'            End Select

'            dblBSC_Percent = 0 : dblGeneral_Percent = 0

'            Dim objEmp As New clsEmployee_Master
'            Dim objJob As New clsJobs
'            Dim objJGroup As New clsJobGroup

'            objEmp._Employeeunkid = mintEmployeeId

'            If objEmp._Jobgroupunkid <= 0 Then
'                objJob._Jobunkid = objEmp._Jobunkid
'                objJGroup._Jobgroupunkid = objJob._Jobgroupunkid
'            Else
'                objJGroup._Jobgroupunkid = objEmp._Jobgroupunkid
'            End If

'            dblGeneral_Percent = objJGroup._Weight
'            dblBSC_Percent = 100 - objJGroup._Weight

'            Dim dTRow As DataRow = Nothing

'            dTRow = mdtFinalTable.NewRow
'            dTRow = Get_BSC_Evalution()       'BSC
'            mdtFinalTable.Rows.Add(dTRow)

'            iCnt = 0 : dblGETotal = 0

'            dTRow = mdtFinalTable.NewRow
'            dTRow = Get_GEvaluation()       'GENERAL EVALUATION 
'            mdtFinalTable.Rows.Add(dTRow)

'            Call Set_Total()                'SETTING COLUMN TOTAL


'            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
'                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
'            End If

'            iCnt = 0 : dblGETotal = 0

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Export_Assessment_Report; Module Name: " & mstrModuleName)
'        Finally
'            'S.SANDEEP [ 02 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            dSelf = Nothing
'            dAssess = Nothing
'            dEAssess = Nothing
'            dReview = Nothing
'            'S.SANDEEP [ 28 APRIL 2012 ] -- END
'        End Try
'    End Sub

'    'Private Function GetScore(ByVal intMode As Integer, ByVal intEmpId As Integer, ByVal blnIsBSC As Boolean) As DataSet
'    '    Dim StrQ As String = String.Empty
'    '    Dim objDataOperation As New clsDataOperation
'    '    Dim dsList As New DataSet
'    '    Try

'    '        If blnIsBSC = False Then
'    '            StrQ = "SELECT " & _
'    '                   "    ISNULL((SUM(Result) * 100)/(COUNT(assessgroupunkid)*100),0) AS Result " & _
'    '                   "FROM " & _
'    '                   "( " & _
'    '                   "SELECT " & _
'    '                   "     assessgroupunkid " & _
'    '                   "    ,ISNULL(SUM(CAST(resultname AS DECIMAL)),0) AS Result " & _
'    '                   "FROM hrassess_analysis_tran " & _
'    '                   "    JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                   "    JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'    '                   "WHERE hrassess_analysis_master.isvoid = 0 AND hrassess_analysis_tran.isvoid = 0 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "
'    '            Select Case intMode
'    '                Case 1 ' SELF
'    '                    StrQ &= " AND selfemployeeunkid = '" & mintEmployeeId & "'"
'    '                Case 2 ' ASSESSOR
'    '                    StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'    '                Case 3 ' EX ASSESSOR
'    '                    StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND ext_assessorunkid = '" & intEmpId & "'"
'    '                Case 4 ' REVIEWER
'    '                    StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'    '            End Select
'    '            StrQ &= "GROUP BY assessgroupunkid " & _
'    '                   ")AS A "
'    '        Else
'    '            StrQ = "SELECT " & _
'    '                   "    ISNULL(SUM(CAST(resultname AS DECIMAL)),0) AS Result " & _
'    '                   "FROM hrbsc_analysis_tran " & _
'    '                   "    JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                   "    JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
'    '                   "WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_master.isvoid = 0 " & _
'    '                   "    AND kpiunkid <=0 AND targetunkid <=0 AND initiativeunkid < = 0 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "
'    '            Select Case intMode
'    '                Case 1 ' SELF
'    '                    StrQ &= " AND selfemployeeunkid = '" & mintEmployeeId & "'"
'    '                Case 2 ' ASSESSOR
'    '                    StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'    '                Case 3 ' EX ASSESSOR
'    '                    StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND ext_assessorunkid = '" & intEmpId & "'"
'    '                Case 4 ' REVIEWER
'    '                    StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'    '            End Select
'    '        End If


'    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '        End If

'    '        Return dsList
'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: GetScore; Module Name: " & mstrModuleName)
'    '        Return Nothing
'    '    Finally
'    '    End Try
'    'End Function

'    Private Function GetScore(ByVal intMode As Integer, ByVal intEmpId As Integer, ByVal blnIsBSC As Boolean) As DataSet
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Dim dsList As New DataSet
'        Dim dtTable As DataTable
'        Dim iCnt As Integer = 0
'        Dim StrGrpId As Integer = 0
'        Dim dblResult As Double = 0
'        Try
'            dtTable = New DataTable("List")
'            dtTable.Columns.Add("Result", System.Type.GetType("System.Double"))


'            If blnIsBSC = False Then

'                StrQ = "SELECT " & _
'                       " Result AS Result " & _
'                       ",assessgroupunkid as Id " & _
'                       "FROM " & _
'                       "( " & _
'                       "SELECT " & _
'                       "     assessgroupunkid " & _
'                       "    ,resultname AS Result " & _
'                       "FROM hrassess_analysis_tran " & _
'                       "    JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                       "    JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                       "WHERE hrassess_analysis_master.isvoid = 0 AND hrassess_analysis_tran.isvoid = 0 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "
'                Select Case intMode
'                    Case 1 ' SELF
'                        StrQ &= " AND selfemployeeunkid = '" & mintEmployeeId & "'"
'                    Case 2 ' ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                    Case 3 ' EX ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND ext_assessorunkid = '" & intEmpId & "'"
'                    Case 4 ' REVIEWER
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                End Select
'                StrQ &= ")AS A "

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                For Each dRow As DataRow In dsList.Tables(0).Rows
'                    If IsNumeric(dRow.Item("Result")) = True Then
'                        dblResult += CDbl(dRow.Item("Result"))
'                        If StrGrpId <> dRow.Item("Id").ToString Then
'                            iCnt += 1
'                            StrGrpId = dRow.Item("Id").ToString
'                        End If
'                    End If
'                Next

'                Dim dtRow As DataRow = dtTable.NewRow

'                If iCnt > 0 Then
'                    dtRow.Item("Result") = ((dblResult * 100) / (iCnt * 100)).ToString("###.##")
'                Else
'                    dtRow.Item("Result") = 0
'                End If

'                dtTable.Rows.Add(dtRow)
'                dsList.Tables.RemoveAt(0)
'                dsList.Tables.Add(dtTable)
'            Else
'                StrQ = "SELECT " & _
'                       "    resultname AS Result " & _
'                       "FROM hrbsc_analysis_tran " & _
'                       "    JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                       "    JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
'                       "WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_master.isvoid = 0 " & _
'                       "    AND kpiunkid <=0 AND targetunkid <=0 AND initiativeunkid < = 0 AND periodunkid = '" & mintPeriodId & "' AND iscommitted = 1 "
'                Select Case intMode
'                    Case 1 ' SELF
'                        StrQ &= " AND selfemployeeunkid = '" & mintEmployeeId & "'"
'                    Case 2 ' ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                    Case 3 ' EX ASSESSOR
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND ext_assessorunkid = '" & intEmpId & "'"
'                    Case 4 ' REVIEWER
'                        StrQ &= " AND assessedemployeeunkid = '" & mintEmployeeId & "' AND assessormasterunkid = '" & intEmpId & "'"
'                End Select

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                For Each dRow As DataRow In dsList.Tables(0).Rows
'                    If IsNumeric(dRow.Item("Result")) = True Then
'                        dblResult += CDbl(dRow.Item("Result"))
'                    End If
'                Next

'                Dim dtRow As DataRow = dtTable.NewRow

'                dtRow.Item("Result") = dblResult

'                dtTable.Rows.Add(dtRow)
'                dsList.Tables.RemoveAt(0)
'                dsList.Tables.Add(dtTable)

'            End If


'            Return dsList
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetScore; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function

'    Private Function Get_BSC_Evalution() As DataRow
'        Dim dBRow As DataRow = mdtFinalTable.NewRow
'        Dim dsTList As New DataSet
'        Try
'            If dSelf IsNot Nothing Then
'                For i As Integer = 0 To dSelf.Length - 1
'                    dsTList = GetScore(dSelf(i)("Mode"), dSelf(i)("EmpId"), True)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "S_" & dSelf(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                    End If
'                    iCnt += 1
'                Next
'            End If

'            If dAssess IsNot Nothing Then
'                For i As Integer = 0 To dAssess.Length - 1
'                    dsTList = GetScore(dAssess(i)("Mode"), dAssess(i)("EmpId"), True)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "A_" & dAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dEAssess IsNot Nothing Then
'                For i As Integer = 0 To dEAssess.Length - 1
'                    dsTList = GetScore(dEAssess(i)("Mode"), dEAssess(i)("EmpId"), True)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "E_" & dEAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dReview IsNot Nothing Then
'                For i As Integer = 0 To dReview.Length - 1
'                    dsTList = GetScore(dReview(i)("Mode"), dReview(i)("EmpId"), True)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dBRow.Item("referencename") = "FORMULA FROM SCORES FOR BSC " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblBSC_Percent & ")/100"
'                        dBRow.Item("Column" & "R_" & dReview(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            dBRow.Item("total") = CDbl(dblGETotal)

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'dBRow.Item("total_percent") = ((dblGETotal * dblGeneral_Percent) / (iCnt * 100)).ToString("###.#0")
'            If iCnt = 0 Then
'                dBRow.Item("total_percent") = (0).ToString("###.#0")
'            Else
'                dBRow.Item("total_percent") = ((dblGETotal * dblBSC_Percent) / (iCnt * 100)).ToString("###.#0")
'            End If

'            'S.SANDEEP [ 05 MARCH 2012 ] -- END




'            Return dBRow

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_BSC_Row; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function

'    Private Function Get_GEvaluation() As DataRow
'        Dim dGRow As DataRow = mdtFinalTable.NewRow
'        Dim dsTList As New DataSet
'        Try
'            If dSelf IsNot Nothing Then
'                For i As Integer = 0 To dSelf.Length - 1
'                    dsTList = GetScore(dSelf(i)("Mode"), dSelf(i)("EmpId"), False)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "S_" & dSelf(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                    End If
'                    iCnt += 1
'                Next
'            End If

'            If dAssess IsNot Nothing Then
'                For i As Integer = 0 To dAssess.Length - 1
'                    dsTList = GetScore(dAssess(i)("Mode"), dAssess(i)("EmpId"), False)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "A_" & dAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dEAssess IsNot Nothing Then
'                For i As Integer = 0 To dEAssess.Length - 1
'                    dsTList = GetScore(dEAssess(i)("Mode"), dEAssess(i)("EmpId"), False)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "E_" & dEAssess(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            If dReview IsNot Nothing Then
'                For i As Integer = 0 To dReview.Length - 1
'                    dsTList = GetScore(dReview(i)("Mode"), dReview(i)("EmpId"), False)
'                    If dsTList.Tables(0).Rows.Count > 0 Then
'                        dGRow.Item("referencename") = "FORMULA FROM SCORES FOR GENERAL ASSESSMENT " & vbCrLf & " (Total Weighted Scores for General Assessment X " & dblGeneral_Percent & ")/100"
'                        dGRow.Item("Column" & "R_" & dReview(i)("EmpId")) = CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        dblGETotal += CDbl(dsTList.Tables(0).Rows(0)("Result"))
'                        If CDbl(dsTList.Tables(0).Rows(0)("Result")) > 0 Then
'                            iCnt += 1
'                        End If
'                    End If
'                Next
'            End If

'            dGRow.Item("total") = CDbl(dblGETotal)
'            If iCnt = 0 Then
'                dGRow.Item("total_percent") = (0).ToString("###.#0")
'            Else
'                dGRow.Item("total_percent") = ((dblGETotal * dblGeneral_Percent) / (iCnt * 100)).ToString("###.#0")
'            End If


'            Return dGRow

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_GEvaluation; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function

'    Private Sub Set_Total()
'        Try
'            mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
'            mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
'            For Each dRow As DataRow In dsColumns.Tables("List").Rows
'                mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("referencename") = "FORMULA FOR TOTAL SCORES " & vbCrLf & "Scores of BSC + Scores of General Evaluation "
'                mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("Column" & dRow("AId").ToString) = mdtFinalTable.Compute("SUM(Column" & dRow("AId").ToString & ")", "")
'                mdtFinalTable.AcceptChanges()
'            Next

'            mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("total") = mdtFinalTable.Compute("SUM(total)", "")
'            mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("total_percent") = mdtFinalTable.Compute("SUM(total_percent)", "")

'            dblGETotal = mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1)("total_percent")

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Set_Total; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
'        Dim strBuilder As New StringBuilder
'        Dim blnFlag As Boolean = False
'        Dim StrValue As String = ""
'        Try
'            Select Case mintWeightedAvgBy
'                Case enWeightedAverage.WEIGHTEDAVG_ALL
'                    StrValue = Language.getMessage(mstrModuleName, 8, "Weighted Average By All")
'                Case enWeightedAverage.WEIGHTEDAVG_ASSESSOR
'                    StrValue = Language.getMessage(mstrModuleName, 9, "Weighted Average By Assessor")
'                Case enWeightedAverage.WEIGHTEDAVG_REVIEWER
'                    StrValue = Language.getMessage(mstrModuleName, 10, "Weighted Average By Reviewer")
'                Case enWeightedAverage.WEIGHTEDAVG_ASSESSOR_REVIEWER
'                    StrValue = Language.getMessage(mstrModuleName, 11, "Weighted Average By Assessor And Reviewer")
'            End Select

'            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
'            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
'            If ConfigParameter._Object._IsDisplayLogo = True Then
'                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
'                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
'                Dim objAppSett As New clsApplicationSettings
'                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
'                objAppSett = Nothing
'                If Company._Object._Image IsNot Nothing Then
'                    Company._Object._Image.Save(strImPath)
'                End If
'                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
'                strBuilder.Append(" </TD> " & vbCrLf)
'                strBuilder.Append(" </TR> " & vbCrLf)
'            End If
'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 5, "Prepared By :") & " </B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='5%' align='left' ><FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(User._Object._Username & vbCrLf)
'            strBuilder.Append(" </FONT></TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='40%' colspan= " & objDataReader.Columns.Count - 1 & " align='center' > " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='5%'> " & vbCrLf)
'            strBuilder.Append(" &nbsp; " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
'            strBuilder.Append(" <TD width='5%'> " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Date :") & "</B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
'            strBuilder.Append(Now.Date & vbCrLf)
'            strBuilder.Append(" </FONT></TD> " & vbCrLf)
'            strBuilder.Append(" <TD width='40%' colspan= " & objDataReader.Columns.Count - 1 & " align='center' > " & vbCrLf)
'            strBuilder.Append(" <FONT SIZE=3><b> " & "FORMULAR AND A GUIDE TO OVERALL SCORES RATING" & "</B></FONT> " & vbCrLf)
'            strBuilder.Append(" </TD> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" <HR> " & vbCrLf)
'            strBuilder.Append(" <B> " & "Employee : " & mstrEmployeeName & ", Period : " & mstrPeriodName & ", " & StrValue & "</B><BR> " & vbCrLf)
'            strBuilder.Append(" </HR> " & vbCrLf)
'            strBuilder.Append(" <BR> " & vbCrLf)
'            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH=100%> " & vbCrLf)
'            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

'            For j As Integer = 0 To objDataReader.Columns.Count - 1
'                strBuilder.Append("<TD BORDER=1 WIDTH='40%' ALIGN='LEFT'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
'            Next
'            strBuilder.Append(" </TR> " & vbCrLf)

'            For i As Integer = 0 To objDataReader.Rows.Count - 1
'                strBuilder.Append(" <TR> " & vbCrLf)
'                For k As Integer = 0 To objDataReader.Columns.Count - 1
'                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
'                Next
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" <TD colspan = " & objDataReader.Columns.Count & " BORDER = 0 ALIGN='LEFT'><FONT SIZE=2><B>" & "OVERALL PERFORMANCE RATING (Tick on relevant box to show overall quality of the Appraisee performance)" & "</B></FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Excellent" & "</FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Very Good" & "</FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Good" & "</FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Average" & "</FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Fair" & "</FONT></TD>" & vbCrLf)
'            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & "Poor" & "</FONT></TD>" & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            If dblGETotal >= 91 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            End If

'            If dblGETotal >= 70 And dblGETotal <= 90 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            End If

'            If dblGETotal >= 60 And dblGETotal <= 69 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            End If

'            If dblGETotal >= 50 And dblGETotal <= 59 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            End If

'            If dblGETotal >= 35 And dblGETotal <= 49 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            End If

'            If dblGETotal >= 0 And dblGETotal <= 34 Then
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=3><B>X<B></FONT></TD>" & vbCrLf)
'            Else
'                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER'><FONT SIZE=2>&nbsp;</FONT></TD>" & vbCrLf)
'            End If
'            strBuilder.Append(" </TR> " & vbCrLf)

'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)
'            strBuilder.Append(" <TR> " & vbCrLf)
'            strBuilder.Append(" </TR> " & vbCrLf)


'            For i As Integer = 0 To objDataReader.Columns.Count - 1
'                strBuilder.Append(" <TR> " & vbCrLf)
'                Select Case i
'                    Case 0
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>SCORE<B></FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>LEVEL OF PERFORMANCE<B></FONT></TD>" & vbCrLf)
'                    Case 1
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 91 and above </FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Excellent</FONT></TD>" & vbCrLf)
'                    Case 2
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 70-90 </FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Very Good</FONT></TD>" & vbCrLf)
'                    Case 3
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 60-69 </FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Good</FONT></TD>" & vbCrLf)
'                    Case 4
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 50-59 </FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Average</FONT></TD>" & vbCrLf)
'                    Case 5
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 35-49 </FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Fair</FONT></TD>" & vbCrLf)
'                    Case 6
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> 0-34 </FONT></TD>" & vbCrLf)
'                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>Fail</FONT></TD>" & vbCrLf)
'                End Select
'                strBuilder.Append(" </TR> " & vbCrLf)
'            Next
'            strBuilder.Append(" </TABLE> " & vbCrLf)
'            strBuilder.Append(" </HTML> " & vbCrLf)

'            If System.IO.Directory.Exists(SavePath) = False Then
'                Dim dig As New Windows.Forms.FolderBrowserDialog
'                dig.Description = "Select Folder Where to export report."

'                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
'                    SavePath = dig.SelectedPath
'                Else
'                    Exit Function
'                End If
'            End If

'            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
'                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
'            End If

'            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
'                StrFinalPath = SavePath & "\" & flFileName & ".xls"
'                blnFlag = True
'            Else
'                blnFlag = False
'            End If

'            Return blnFlag

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
'        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
'        Dim strWriter As New StreamWriter(fsFile)
'        Try
'            With strWriter
'                .BaseStream.Seek(0, SeekOrigin.End)
'                .WriteLine(sb)
'                .Close()
'            End With
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            sb = Nothing
'            strWriter = Nothing
'            fsFile = Nothing
'        End Try
'    End Function

'#End Region

'End Class
