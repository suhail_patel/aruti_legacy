﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessmentSoreRatingReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessmentSoreRatingReport))
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.radWA_AssessReviewer = New System.Windows.Forms.RadioButton
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.radWA_Reviewer = New System.Windows.Forms.RadioButton
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkUncommited = New System.Windows.Forms.CheckBox
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.radWA_Assessor = New System.Windows.Forms.RadioButton
        Me.radWA_All = New System.Windows.Forms.RadioButton
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objsp1 = New System.Windows.Forms.SplitContainer
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboFiancialYear = New System.Windows.Forms.ComboBox
        Me.objchkAllPeriod = New System.Windows.Forms.CheckBox
        Me.dgvPeriod = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblSubReportType = New System.Windows.Forms.Label
        Me.cboSubReportType = New System.Windows.Forms.ComboBox
        Me.fpnlData = New System.Windows.Forms.FlowLayoutPanel
        Me.plnScoreType = New System.Windows.Forms.Panel
        Me.cboScoreOption = New System.Windows.Forms.ComboBox
        Me.lblDisplayScore = New System.Windows.Forms.Label
        Me.pnlAllocation = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.pnlSorting = New System.Windows.Forms.Panel
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.objStLine2 = New eZee.Common.eZeeStraightLine
        Me.chkConsiderEmployeeTermination = New System.Windows.Forms.CheckBox
        Me.pnlCondition = New System.Windows.Forms.Panel
        Me.chkShowEmpSalary = New System.Windows.Forms.CheckBox
        Me.objchkdisplay = New System.Windows.Forms.CheckBox
        Me.lvDisplayCol = New eZee.Common.eZeeListView(Me.components)
        Me.colhDisplayName = New System.Windows.Forms.ColumnHeader
        Me.objcolhSelectCol = New System.Windows.Forms.ColumnHeader
        Me.objcolhJoin = New System.Windows.Forms.ColumnHeader
        Me.objcolhDisplay = New System.Windows.Forms.ColumnHeader
        Me.dtpAppDate = New System.Windows.Forms.DateTimePicker
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.lblOverallCondition = New System.Windows.Forms.Label
        Me.chkOption = New System.Windows.Forms.CheckBox
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.lblRType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.objsp1.Panel1.SuspendLayout()
        Me.objsp1.Panel2.SuspendLayout()
        Me.objsp1.SuspendLayout()
        CType(Me.dgvPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fpnlData.SuspendLayout()
        Me.plnScoreType.SuspendLayout()
        Me.pnlAllocation.SuspendLayout()
        Me.pnlSorting.SuspendLayout()
        Me.pnlCondition.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(994, 58)
        Me.EZeeHeader1.TabIndex = 1
        Me.EZeeHeader1.Title = "Assessment Score Rating"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.radWA_AssessReviewer)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.radWA_Reviewer)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.chkUncommited)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Controls.Add(Me.radWA_Assessor)
        Me.EZeeFooter1.Controls.Add(Me.radWA_All)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 502)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(994, 55)
        Me.EZeeFooter1.TabIndex = 33
        '
        'btnAdvanceFilter
        '
        Me.btnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.btnAdvanceFilter.BackgroundImage = CType(resources.GetObject("btnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.btnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.btnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.btnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Location = New System.Drawing.Point(590, 13)
        Me.btnAdvanceFilter.Name = "btnAdvanceFilter"
        Me.btnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Size = New System.Drawing.Size(105, 30)
        Me.btnAdvanceFilter.TabIndex = 87
        Me.btnAdvanceFilter.Text = "&Advance Filter"
        Me.btnAdvanceFilter.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(701, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'radWA_AssessReviewer
        '
        Me.radWA_AssessReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWA_AssessReviewer.Location = New System.Drawing.Point(152, 20)
        Me.radWA_AssessReviewer.Name = "radWA_AssessReviewer"
        Me.radWA_AssessReviewer.Size = New System.Drawing.Size(13, 17)
        Me.radWA_AssessReviewer.TabIndex = 0
        Me.radWA_AssessReviewer.TabStop = True
        Me.radWA_AssessReviewer.Text = "Weighted Average for Assessor and Reviewer"
        Me.radWA_AssessReviewer.UseVisualStyleBackColor = True
        Me.radWA_AssessReviewer.Visible = False
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(797, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'radWA_Reviewer
        '
        Me.radWA_Reviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWA_Reviewer.Location = New System.Drawing.Point(152, 20)
        Me.radWA_Reviewer.Name = "radWA_Reviewer"
        Me.radWA_Reviewer.Size = New System.Drawing.Size(13, 17)
        Me.radWA_Reviewer.TabIndex = 0
        Me.radWA_Reviewer.TabStop = True
        Me.radWA_Reviewer.Text = "Weighted Average for Reviewer"
        Me.radWA_Reviewer.UseVisualStyleBackColor = True
        Me.radWA_Reviewer.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(893, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkUncommited
        '
        Me.chkUncommited.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUncommited.Location = New System.Drawing.Point(152, 20)
        Me.chkUncommited.Name = "chkUncommited"
        Me.chkUncommited.Size = New System.Drawing.Size(13, 17)
        Me.chkUncommited.TabIndex = 86
        Me.chkUncommited.Text = "Include Uncommitted info"
        Me.chkUncommited.UseVisualStyleBackColor = True
        Me.chkUncommited.Visible = False
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'radWA_Assessor
        '
        Me.radWA_Assessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWA_Assessor.Location = New System.Drawing.Point(152, 20)
        Me.radWA_Assessor.Name = "radWA_Assessor"
        Me.radWA_Assessor.Size = New System.Drawing.Size(13, 17)
        Me.radWA_Assessor.TabIndex = 0
        Me.radWA_Assessor.TabStop = True
        Me.radWA_Assessor.Text = "Weighted Average for Assessor"
        Me.radWA_Assessor.UseVisualStyleBackColor = True
        Me.radWA_Assessor.Visible = False
        '
        'radWA_All
        '
        Me.radWA_All.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radWA_All.Location = New System.Drawing.Point(152, 20)
        Me.radWA_All.Name = "radWA_All"
        Me.radWA_All.Size = New System.Drawing.Size(13, 17)
        Me.radWA_All.TabIndex = 0
        Me.radWA_All.TabStop = True
        Me.radWA_All.Text = "Weighted Average for ALL"
        Me.radWA_All.UseVisualStyleBackColor = True
        Me.radWA_All.Visible = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objsp1)
        Me.gbFilterCriteria.Controls.Add(Me.lblSubReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboSubReportType)
        Me.gbFilterCriteria.Controls.Add(Me.fpnlData)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine2)
        Me.gbFilterCriteria.Controls.Add(Me.chkConsiderEmployeeTermination)
        Me.gbFilterCriteria.Controls.Add(Me.pnlCondition)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lblRType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(973, 397)
        Me.gbFilterCriteria.TabIndex = 34
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objsp1
        '
        Me.objsp1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objsp1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objsp1.IsSplitterFixed = True
        Me.objsp1.Location = New System.Drawing.Point(423, 26)
        Me.objsp1.Name = "objsp1"
        Me.objsp1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objsp1.Panel1
        '
        Me.objsp1.Panel1.Controls.Add(Me.lblYear)
        Me.objsp1.Panel1.Controls.Add(Me.cboFiancialYear)
        '
        'objsp1.Panel2
        '
        Me.objsp1.Panel2.Controls.Add(Me.objchkAllPeriod)
        Me.objsp1.Panel2.Controls.Add(Me.dgvPeriod)
        Me.objsp1.Size = New System.Drawing.Size(266, 362)
        Me.objsp1.SplitterDistance = 38
        Me.objsp1.SplitterWidth = 2
        Me.objsp1.TabIndex = 139
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(4, 13)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(78, 15)
        Me.lblYear.TabIndex = 99
        Me.lblYear.Text = "Financial  Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFiancialYear
        '
        Me.cboFiancialYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFiancialYear.DropDownWidth = 145
        Me.cboFiancialYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFiancialYear.FormattingEnabled = True
        Me.cboFiancialYear.Location = New System.Drawing.Point(88, 10)
        Me.cboFiancialYear.Name = "cboFiancialYear"
        Me.cboFiancialYear.Size = New System.Drawing.Size(168, 21)
        Me.cboFiancialYear.TabIndex = 98
        '
        'objchkAllPeriod
        '
        Me.objchkAllPeriod.AutoSize = True
        Me.objchkAllPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAllPeriod.Location = New System.Drawing.Point(7, 6)
        Me.objchkAllPeriod.Name = "objchkAllPeriod"
        Me.objchkAllPeriod.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllPeriod.TabIndex = 103
        Me.objchkAllPeriod.UseVisualStyleBackColor = True
        Me.objchkAllPeriod.Visible = False
        '
        'dgvPeriod
        '
        Me.dgvPeriod.AllowUserToAddRows = False
        Me.dgvPeriod.BackgroundColor = System.Drawing.Color.White
        Me.dgvPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhPeriod, Me.objdgcolhPId})
        Me.dgvPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPeriod.Location = New System.Drawing.Point(0, 0)
        Me.dgvPeriod.Name = "dgvPeriod"
        Me.dgvPeriod.RowHeadersVisible = False
        Me.dgvPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPeriod.Size = New System.Drawing.Size(266, 322)
        Me.dgvPeriod.TabIndex = 0
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        '
        'objdgcolhPId
        '
        Me.objdgcolhPId.HeaderText = "objdgcolhPId"
        Me.objdgcolhPId.Name = "objdgcolhPId"
        Me.objdgcolhPId.ReadOnly = True
        Me.objdgcolhPId.Visible = False
        '
        'lblSubReportType
        '
        Me.lblSubReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubReportType.Location = New System.Drawing.Point(8, 66)
        Me.lblSubReportType.Name = "lblSubReportType"
        Me.lblSubReportType.Size = New System.Drawing.Size(97, 15)
        Me.lblSubReportType.TabIndex = 138
        Me.lblSubReportType.Text = "Sub Report Type"
        Me.lblSubReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSubReportType
        '
        Me.cboSubReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubReportType.DropDownWidth = 145
        Me.cboSubReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSubReportType.FormattingEnabled = True
        Me.cboSubReportType.Location = New System.Drawing.Point(111, 63)
        Me.cboSubReportType.Name = "cboSubReportType"
        Me.cboSubReportType.Size = New System.Drawing.Size(259, 21)
        Me.cboSubReportType.TabIndex = 137
        '
        'fpnlData
        '
        Me.fpnlData.AutoSize = True
        Me.fpnlData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.fpnlData.Controls.Add(Me.plnScoreType)
        Me.fpnlData.Controls.Add(Me.pnlAllocation)
        Me.fpnlData.Controls.Add(Me.pnlSorting)
        Me.fpnlData.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.fpnlData.Location = New System.Drawing.Point(3, 141)
        Me.fpnlData.Name = "fpnlData"
        Me.fpnlData.Size = New System.Drawing.Size(405, 250)
        Me.fpnlData.TabIndex = 136
        '
        'plnScoreType
        '
        Me.plnScoreType.Controls.Add(Me.cboScoreOption)
        Me.plnScoreType.Controls.Add(Me.lblDisplayScore)
        Me.plnScoreType.Location = New System.Drawing.Point(3, 3)
        Me.plnScoreType.Name = "plnScoreType"
        Me.plnScoreType.Size = New System.Drawing.Size(399, 25)
        Me.plnScoreType.TabIndex = 137
        '
        'cboScoreOption
        '
        Me.cboScoreOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScoreOption.DropDownWidth = 180
        Me.cboScoreOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScoreOption.FormattingEnabled = True
        Me.cboScoreOption.Location = New System.Drawing.Point(105, 2)
        Me.cboScoreOption.Name = "cboScoreOption"
        Me.cboScoreOption.Size = New System.Drawing.Size(259, 21)
        Me.cboScoreOption.TabIndex = 132
        '
        'lblDisplayScore
        '
        Me.lblDisplayScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplayScore.Location = New System.Drawing.Point(2, 5)
        Me.lblDisplayScore.Name = "lblDisplayScore"
        Me.lblDisplayScore.Size = New System.Drawing.Size(97, 15)
        Me.lblDisplayScore.TabIndex = 133
        Me.lblDisplayScore.Text = "Display Score"
        Me.lblDisplayScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAllocation
        '
        Me.pnlAllocation.Controls.Add(Me.objchkAll)
        Me.pnlAllocation.Controls.Add(Me.lvAllocation)
        Me.pnlAllocation.Controls.Add(Me.cboAllocations)
        Me.pnlAllocation.Controls.Add(Me.txtSearch)
        Me.pnlAllocation.Controls.Add(Me.lblAllocation)
        Me.pnlAllocation.Location = New System.Drawing.Point(3, 34)
        Me.pnlAllocation.Name = "pnlAllocation"
        Me.pnlAllocation.Size = New System.Drawing.Size(399, 182)
        Me.pnlAllocation.TabIndex = 104
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(342, 63)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 102
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(105, 57)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(259, 122)
        Me.lvAllocation.TabIndex = 101
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 250
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(105, 3)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(259, 21)
        Me.cboAllocations.TabIndex = 98
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(105, 30)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(259, 21)
        Me.txtSearch.TabIndex = 100
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(2, 6)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(74, 15)
        Me.lblAllocation.TabIndex = 99
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSorting
        '
        Me.pnlSorting.Controls.Add(Me.txtOrderBy)
        Me.pnlSorting.Controls.Add(Me.objbtnSort)
        Me.pnlSorting.Controls.Add(Me.lblOrderBy)
        Me.pnlSorting.Location = New System.Drawing.Point(3, 222)
        Me.pnlSorting.Name = "pnlSorting"
        Me.pnlSorting.Size = New System.Drawing.Size(399, 25)
        Me.pnlSorting.TabIndex = 139
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(105, 2)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(259, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(370, 2)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(3, 5)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(96, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'objStLine2
        '
        Me.objStLine2.BackColor = System.Drawing.Color.Transparent
        Me.objStLine2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine2.Location = New System.Drawing.Point(414, 24)
        Me.objStLine2.Name = "objStLine2"
        Me.objStLine2.Size = New System.Drawing.Size(3, 374)
        Me.objStLine2.TabIndex = 135
        Me.objStLine2.Text = "EZeeStraightLine2"
        '
        'chkConsiderEmployeeTermination
        '
        Me.chkConsiderEmployeeTermination.BackColor = System.Drawing.Color.Transparent
        Me.chkConsiderEmployeeTermination.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsiderEmployeeTermination.Location = New System.Drawing.Point(704, 4)
        Me.chkConsiderEmployeeTermination.Name = "chkConsiderEmployeeTermination"
        Me.chkConsiderEmployeeTermination.Size = New System.Drawing.Size(264, 17)
        Me.chkConsiderEmployeeTermination.TabIndex = 130
        Me.chkConsiderEmployeeTermination.Text = "Consider Employee Active Status On Period Date"
        Me.chkConsiderEmployeeTermination.UseVisualStyleBackColor = False
        '
        'pnlCondition
        '
        Me.pnlCondition.Controls.Add(Me.chkShowEmpSalary)
        Me.pnlCondition.Controls.Add(Me.objchkdisplay)
        Me.pnlCondition.Controls.Add(Me.lvDisplayCol)
        Me.pnlCondition.Controls.Add(Me.dtpAppDate)
        Me.pnlCondition.Controls.Add(Me.cboCondition)
        Me.pnlCondition.Controls.Add(Me.lblOverallCondition)
        Me.pnlCondition.Controls.Add(Me.chkOption)
        Me.pnlCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCondition.Location = New System.Drawing.Point(704, 33)
        Me.pnlCondition.Name = "pnlCondition"
        Me.pnlCondition.Size = New System.Drawing.Size(260, 358)
        Me.pnlCondition.TabIndex = 109
        '
        'chkShowEmpSalary
        '
        Me.chkShowEmpSalary.Location = New System.Drawing.Point(4, 336)
        Me.chkShowEmpSalary.Name = "chkShowEmpSalary"
        Me.chkShowEmpSalary.Size = New System.Drawing.Size(252, 17)
        Me.chkShowEmpSalary.TabIndex = 105
        Me.chkShowEmpSalary.Text = "Show Employee Scale On Report"
        Me.chkShowEmpSalary.UseVisualStyleBackColor = True
        '
        'objchkdisplay
        '
        Me.objchkdisplay.AutoSize = True
        Me.objchkdisplay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkdisplay.Location = New System.Drawing.Point(215, 60)
        Me.objchkdisplay.Name = "objchkdisplay"
        Me.objchkdisplay.Size = New System.Drawing.Size(15, 14)
        Me.objchkdisplay.TabIndex = 103
        Me.objchkdisplay.UseVisualStyleBackColor = True
        '
        'lvDisplayCol
        '
        Me.lvDisplayCol.BackColorOnChecked = False
        Me.lvDisplayCol.CheckBoxes = True
        Me.lvDisplayCol.ColumnHeaders = Nothing
        Me.lvDisplayCol.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDisplayName, Me.objcolhSelectCol, Me.objcolhJoin, Me.objcolhDisplay})
        Me.lvDisplayCol.CompulsoryColumns = ""
        Me.lvDisplayCol.FullRowSelect = True
        Me.lvDisplayCol.GridLines = True
        Me.lvDisplayCol.GroupingColumn = Nothing
        Me.lvDisplayCol.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDisplayCol.HideSelection = False
        Me.lvDisplayCol.Location = New System.Drawing.Point(4, 55)
        Me.lvDisplayCol.MinColumnWidth = 50
        Me.lvDisplayCol.MultiSelect = False
        Me.lvDisplayCol.Name = "lvDisplayCol"
        Me.lvDisplayCol.OptionalColumns = ""
        Me.lvDisplayCol.ShowMoreItem = False
        Me.lvDisplayCol.ShowSaveItem = False
        Me.lvDisplayCol.ShowSelectAll = True
        Me.lvDisplayCol.ShowSizeAllColumnsToFit = True
        Me.lvDisplayCol.Size = New System.Drawing.Size(252, 275)
        Me.lvDisplayCol.Sortable = True
        Me.lvDisplayCol.TabIndex = 11
        Me.lvDisplayCol.UseCompatibleStateImageBehavior = False
        Me.lvDisplayCol.View = System.Windows.Forms.View.Details
        '
        'colhDisplayName
        '
        Me.colhDisplayName.Tag = "colhDisplayName"
        Me.colhDisplayName.Text = "Display Column On Report"
        Me.colhDisplayName.Width = 245
        '
        'objcolhSelectCol
        '
        Me.objcolhSelectCol.Tag = "objcolhSelectCol"
        Me.objcolhSelectCol.Text = ""
        Me.objcolhSelectCol.Width = 0
        '
        'objcolhJoin
        '
        Me.objcolhJoin.Tag = "objcolhJoin"
        Me.objcolhJoin.Text = ""
        Me.objcolhJoin.Width = 0
        '
        'objcolhDisplay
        '
        Me.objcolhDisplay.Tag = "objcolhDisplay"
        Me.objcolhDisplay.Text = ""
        Me.objcolhDisplay.Width = 0
        '
        'dtpAppDate
        '
        Me.dtpAppDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppDate.Location = New System.Drawing.Point(159, 27)
        Me.dtpAppDate.Name = "dtpAppDate"
        Me.dtpAppDate.Size = New System.Drawing.Size(97, 21)
        Me.dtpAppDate.TabIndex = 10
        '
        'cboCondition
        '
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(73, 27)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(80, 21)
        Me.cboCondition.TabIndex = 8
        '
        'lblOverallCondition
        '
        Me.lblOverallCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOverallCondition.Location = New System.Drawing.Point(3, 29)
        Me.lblOverallCondition.Name = "lblOverallCondition"
        Me.lblOverallCondition.Size = New System.Drawing.Size(64, 16)
        Me.lblOverallCondition.TabIndex = 9
        Me.lblOverallCondition.Text = "Condition"
        Me.lblOverallCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOption
        '
        Me.chkOption.BackColor = System.Drawing.Color.Transparent
        Me.chkOption.Location = New System.Drawing.Point(4, 4)
        Me.chkOption.Name = "chkOption"
        Me.chkOption.Size = New System.Drawing.Size(252, 17)
        Me.chkOption.TabIndex = 0
        Me.chkOption.Text = "Consider on Probation  if Appointment Date is"
        Me.chkOption.UseVisualStyleBackColor = False
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(695, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(3, 372)
        Me.objStLine1.TabIndex = 107
        Me.objStLine1.Text = "EZeeStraightLine2"
        '
        'lblRType
        '
        Me.lblRType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRType.Location = New System.Drawing.Point(8, 39)
        Me.lblRType.Name = "lblRType"
        Me.lblRType.Size = New System.Drawing.Size(97, 15)
        Me.lblRType.TabIndex = 97
        Me.lblRType.Text = "Report Type"
        Me.lblRType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 145
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(111, 36)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(259, 21)
        Me.cboReportType.TabIndex = 96
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 93)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(97, 15)
        Me.lblPeriod.TabIndex = 79
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 120)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(97, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 210
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(111, 90)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(259, 21)
        Me.cboPeriod.TabIndex = 77
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(111, 117)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(259, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(376, 116)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'frmAssessmentSoreRatingReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 557)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmAssessmentSoreRatingReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.objsp1.Panel1.ResumeLayout(False)
        Me.objsp1.Panel2.ResumeLayout(False)
        Me.objsp1.Panel2.PerformLayout()
        Me.objsp1.ResumeLayout(False)
        CType(Me.dgvPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fpnlData.ResumeLayout(False)
        Me.plnScoreType.ResumeLayout(False)
        Me.pnlAllocation.ResumeLayout(False)
        Me.pnlAllocation.PerformLayout()
        Me.pnlSorting.ResumeLayout(False)
        Me.pnlSorting.PerformLayout()
        Me.pnlCondition.ResumeLayout(False)
        Me.pnlCondition.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkUncommited As System.Windows.Forms.CheckBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radWA_AssessReviewer As System.Windows.Forms.RadioButton
    Friend WithEvents radWA_Reviewer As System.Windows.Forms.RadioButton
    Friend WithEvents radWA_Assessor As System.Windows.Forms.RadioButton
    Friend WithEvents radWA_All As System.Windows.Forms.RadioButton
    Friend WithEvents lblRType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents pnlAllocation As System.Windows.Forms.Panel
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents pnlCondition As System.Windows.Forms.Panel
    Friend WithEvents chkOption As System.Windows.Forms.CheckBox
    Friend WithEvents dtpAppDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblOverallCondition As System.Windows.Forms.Label
    Friend WithEvents btnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents lvDisplayCol As eZee.Common.eZeeListView
    Friend WithEvents colhDisplayName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSelectCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhJoin As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDisplay As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkdisplay As System.Windows.Forms.CheckBox
    Friend WithEvents chkConsiderEmployeeTermination As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowEmpSalary As System.Windows.Forms.CheckBox
    Friend WithEvents cboScoreOption As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisplayScore As System.Windows.Forms.Label
    Friend WithEvents objStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents fpnlData As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents plnScoreType As System.Windows.Forms.Panel
    Friend WithEvents lblSubReportType As System.Windows.Forms.Label
    Friend WithEvents cboSubReportType As System.Windows.Forms.ComboBox
    Friend WithEvents pnlSorting As System.Windows.Forms.Panel
    Friend WithEvents objsp1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboFiancialYear As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPeriod As System.Windows.Forms.DataGridView
    Friend WithEvents objchkAllPeriod As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
