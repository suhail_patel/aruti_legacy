'************************************************************************************************************************************
'Class Name : clsTrainingPlanningReport.vb
'Purpose    :
'Date       : 02/04/2012
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class clsTrainingPlanReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingPlanReport"
    Private mstrReportId As String = CStr(enArutiReport.Training_Planning_Report)  '73
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mdtFinalTable As DataTable
    Private StrFinalPath As String = String.Empty
    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private mstrAllocationTypeName As String = String.Empty
    Private mintAllocationTypeId As Integer = 0
    Private mstrAllocationNames As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationNames() As String
        Set(ByVal value As String)
            mstrAllocationNames = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            mstrAllocationTypeName = String.Empty
            mintAllocationTypeId = 0
            mstrAllocationNames = String.Empty
            mstrAllocationIds = String.Empty
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        'S.SANDEEP [08-FEB-2017] -- START
        'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
        Me._FilterQuery = ""
        'S.SANDEEP [08-FEB-2017] -- END
        Try

            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                Me._FilterTitle = Language.getMessage(mstrModuleName, 16, "From Date :") & " " & mdtFromDate.Date & " " & _
                                  Language.getMessage(mstrModuleName, 17, "to") & " " & mdtToDate.Date & " "
            End If

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            If mintAllocationTypeId > 0 Then
                objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
                Me._FilterQuery &= "AND allocationtypeunkid = @allocationtypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Allocation Binded :") & " " & mstrAllocationTypeName & " "
            End If
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub CreateTable()
        Try
            mdtFinalTable = New DataTable("Data")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "Column1"
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "S/N")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column2"
            dCol.Caption = Language.getMessage(mstrModuleName, 2, "COURSE TITLE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            'S.SANDEEP [13-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            dCol = New DataColumn
            dCol.ColumnName = "Column15"
            dCol.Caption = Language.getMessage(mstrModuleName, 24, "TRAINING OBJECTIVE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)
            'S.SANDEEP [13-FEB-2017] -- END

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            dCol = New DataColumn
            dCol.ColumnName = "Column16"
            dCol.Caption = Language.getMessage(mstrModuleName, 200, "BINDED ALLOCATION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)
            'S.SANDEEP [08-FEB-2017] -- END

            dCol = New DataColumn
            dCol.ColumnName = "Column3"
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "TARGET GROUP")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column4"
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "VENUE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column5"
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "DATES")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column6"
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "DURATION")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column7"
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "NO. OF STAFF")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column8"
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "TUITION & CONF.")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column9"
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "UP KEEP")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column10"
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "FARE")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column11"
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "TOTAL COST")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column12"
            dCol.Caption = Language.getMessage(mstrModuleName, 12, "PROVIDER")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            'S.SANDEEP [13-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            dCol = New DataColumn
            dCol.ColumnName = "Column17"
            dCol.Caption = Language.getMessage(mstrModuleName, 25, "TRAINING HOURS")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)
            'S.SANDEEP [13-FEB-2017] -- END

            dCol = New DataColumn
            dCol.ColumnName = "Column13"
            dCol.Caption = Language.getMessage(mstrModuleName, 13, "SOURCE OF FUNDING")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Column14"
            dCol.Caption = Language.getMessage(mstrModuleName, 14, "ORIGINATING COORDINATING DEPT")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            'S.SANDEEP [13-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            dCol = New DataColumn
            dCol.ColumnName = "Column18"
            dCol.Caption = Language.getMessage(mstrModuleName, 15, "STRATEGIC GOAL")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)
            'S.SANDEEP [13-FEB-2017] -- END




        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try
            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)

            ''LOGO SETTINGS
            'ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            'If ConfigParameter._Object._IsDisplayLogo = True Then
            '    strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
            '    strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
            '    Dim objAppSett As New clsApplicationSettings
            '    Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
            '    objAppSett = Nothing
            '    If Company._Object._Image IsNot Nothing Then
            '        Company._Object._Image.Save(strImPath)
            '    End If

            '    strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
            '    strBuilder.Append(" </TD> " & vbCrLf)
            '    strBuilder.Append(" </TR> " & vbCrLf)
            'End If
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 23, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='80%' colspan = 12 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 26, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(ConfigParameter._Object._CurrentDateAndTime.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='80%' colspan = 12 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR Width = 50%> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =0 WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    Select Case k
                        Case 0
                            If Val(objDataReader.Rows(i)(0)) = 0 Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN = 10 ><FONT SIZE=2><B>&nbsp;" & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                                Exit For
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            End If
                        Case 7, 8, 9, 10
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                        Case Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End Select
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Sub Export_Training_Plan_Report()
    Public Sub Export_Training_Plan_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal xExportPath As String) 'S.SANDEEP [13-FEB-2017] -- START {xExportPath} -- END


        'Shani(24-Aug-2015) -- End  
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation
            Call CreateTable()

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '         " ISNULL(cfcommon_master.name,'') AS COURSE " & _
            '         ",ISNULL(Target_Grp,'') AS TARGET_GROUP " & _
            '         ",ISNULL(training_venue,'') AS VENUE " & _
            '         ",ISNULL(CONVERT(CHAR(8),start_date,112),'') AS STDATE " & _
            '         ",ISNULL(CONVERT(CHAR(8),end_date,112),'') AS EDDATE " & _
            '         ",CASE WHEN DATEDIFF(week,start_date,end_date)>= 4 THEN CAST(DATEDIFF(mm,start_date,end_date) AS NVARCHAR(50)) + @MONTH  ELSE CAST(DATEDIFF(week,start_date,end_date) AS NVARCHAR(50))+ @Weeks END AS Duration " & _
            '         ",position_no AS NOOFSTAFF " & _
            '         ",ISNULL(fees,0)+ISNULL(misc,0) AS TUITION_CONF " & _
            '         ",ISNULL(accomodation,0)+ISNULL(allowance,0)+ISNULL(meals,0) AS UP_KEEP " & _
            '         ",ISNULL(travel,0) AS FARE " & _
            '         ",ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(accomodation,0)+ISNULL(allowance,0)+ISNULL(meals,0)+ISNULL(travel,0) AS TOTAL " & _
            '         ",ISNULL(hrinstitute_master.institute_name,'') AS PROVIDER " & _
            '         ",ISNULL(Dept,'') AS ORIGINATING_CORDINATING_DEPT " & _
            '         ",ISNULL(Goal,'') AS Goal " & _
            '         ",fundingids " & _
            '    "FROM hrtraining_scheduling " & _
            '         "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND mastertype = 30 " & _
            '         "LEFT JOIN hrinstitute_master ON dbo.hrtraining_scheduling.traininginstituteunkid = dbo.hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '    "LEFT JOIN " & _
            '    "( " & _
            '         "SELECT " & _
            '           "courseunkid " & _
            '          ",cfcommon_master.name AS Goal " & _
            '         "FROM hrtnacoursepriority_tran " & _
            '              "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtnacoursepriority_tran.goalunkid AND mastertype = 31 " & _
            '         "WHERE hrtnacoursepriority_tran.isvoid = 0 " & _
            '    ") AS SG ON SG.courseunkid = hrtraining_scheduling.course_title " & _
            '    "LEFT JOIN " & _
            '    "( " & _
            '         "SELECT " & _
            '           "courseunkid " & _
            '          ",STUFF((SELECT DISTINCT '\r\n' + hrjobgroup_master.name FROM hrjobgroup_master " & _
            '                    "JOIN hremployee_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
            '                    "JOIN hrtnaemp_course_tran AS t1 ON t1.employeeunkid = hremployee_master.employeeunkid " & _
            '                        "AND t1.courseunkid = hrtnaemp_course_tran.courseunkid " & _
            '                   "FOR XML PATH('')),1,4,'') AS Target_Grp " & _
            '          ",STUFF((SELECT DISTINCT '\r\n' + hrdepartment_master.name FROM hrdepartment_master " & _
            '                   "JOIN hremployee_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
            '                   "JOIN hrtnaemp_course_tran AS t1 ON t1.employeeunkid = hremployee_master.employeeunkid " & _
            '                        "AND t1.courseunkid = hrtnaemp_course_tran.courseunkid " & _
            '                   "FOR XML PATH('')),1,4,'') AS Dept " & _
            '         "FROM hrtnaemp_course_tran " & _
            '         "GROUP BY courseunkid " & _
            '    ") AS Emp_Data ON Emp_Data.courseunkid = hrtraining_scheduling.course_title "

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'StrQ = "SELECT " & _
            '         " ISNULL(cfcommon_master.name,'') AS COURSE " & _
            '         ",ISNULL(Target_Grp,'') AS TARGET_GROUP " & _
            '         ",ISNULL(training_venue,'') AS VENUE " & _
            '         ",ISNULL(CONVERT(CHAR(8),hrtraining_scheduling.start_date,112),'') AS STDATE " & _
            '         ",ISNULL(CONVERT(CHAR(8),hrtraining_scheduling.end_date,112),'') AS EDDATE " & _
            '         ",CASE WHEN DATEDIFF(week,start_date,end_date)>= 4 THEN CAST(DATEDIFF(mm,start_date,end_date) AS NVARCHAR(50)) + @MONTH  ELSE CAST(DATEDIFF(week,start_date,end_date) AS NVARCHAR(50))+ @Weeks END AS Duration " & _
            '         ",position_no AS NOOFSTAFF " & _
            '         ",ISNULL(fees,0)+ISNULL(misc,0) AS TUITION_CONF " & _
            '         ",ISNULL(accomodation,0)+ISNULL(allowance,0)+ISNULL(meals,0) AS UP_KEEP " & _
            '         ",ISNULL(travel,0) AS FARE " & _
            '         ",ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(accomodation,0)+ISNULL(allowance,0)+ISNULL(meals,0)+ISNULL(travel,0) AS TOTAL " & _
            '         ",ISNULL(hrinstitute_master.institute_name,'') AS PROVIDER " & _
            '         ",ISNULL(Dept,'') AS ORIGINATING_CORDINATING_DEPT " & _
            '         ",ISNULL(Goal,'') AS Goal " & _
            '         ",fundingids " & _
            '    "FROM hrtraining_scheduling "

            StrQ = "SELECT " & _
                     " ISNULL(cfcommon_master.name,'') AS COURSE " & _
                     ",ISNULL(Target_Grp,'') AS TARGET_GROUP " & _
                     ",ISNULL(training_venue,'') AS VENUE " & _
                     ",ISNULL(CONVERT(CHAR(8),hrtraining_scheduling.start_date,112),'') AS STDATE " & _
                     ",ISNULL(CONVERT(CHAR(8),hrtraining_scheduling.end_date,112),'') AS EDDATE " & _
                     ",CASE WHEN DATEDIFF(week,start_date,end_date)>= 4 THEN CAST(DATEDIFF(mm,start_date,end_date) AS NVARCHAR(50)) + @MONTH  ELSE CAST(DATEDIFF(week,start_date,end_date) AS NVARCHAR(50))+ @Weeks END AS Duration " & _
                     ",position_no AS NOOFSTAFF " & _
                     ",ISNULL(fees,0)+ISNULL(misc,0) AS TUITION_CONF " & _
                     ",ISNULL(accomodation,0)+ISNULL(allowance,0)+ISNULL(meals,0) AS UP_KEEP " & _
                     ",ISNULL(travel,0) AS FARE " & _
                     ",ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(accomodation,0)+ISNULL(allowance,0)+ISNULL(meals,0)+ISNULL(travel,0) AS TOTAL " & _
                     ",ISNULL(hrinstitute_master.institute_name,'') AS PROVIDER " & _
                     ",ISNULL(Dept,'') AS ORIGINATING_CORDINATING_DEPT " & _
                     ",ISNULL(Goal,'') AS Goal " & _
                     ",fundingids " & _
                     ",(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
                     ",(DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END AS Months " & _
                     ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),end_date) AS Days " & _
                     ",CASE WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.BRANCH & "' THEN @BRANCH " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN @DEPARTMENT_GROUP " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT & "' THEN @DEPARTMENT " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION_GROUP & "' THEN @SECTION_GROUP " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION & "' THEN @SECTION " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT_GROUP & "' THEN @UNIT_GROUP " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT & "' THEN @UNIT " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.TEAM & "' THEN @TEAM " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOB_GROUP & "' THEN @JOB_GROUP " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOBS & "' THEN @JOBS " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASS_GROUP & "' THEN @CLASS_GROUP " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASSES & "' THEN @CLASSES " & _
                     "      WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.COST_CENTER & "' THEN @COST_CENTER " & _
                     " END AS alloc_binded " & _
                     ",hrtraining_scheduling.allocationtypeunkid " & _
                     ",hrtraining_scheduling.trainingschedulingunkid " & _
                     ",hrtraining_scheduling.training_remark Column15 " & _
                     ",DATEDIFF(HOUR, CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), start_time, 108) AS DATETIME), CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), end_time, 108) AS DATETIME)) AS Column17 " & _
                "FROM hrtraining_scheduling "
            'S.SANDEEP [08-FEB-2017] -- END

            'S.SANDEEP [13-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            '/***** ADDED *******/

            'S.SANDEEP [13-FEB-2017] -- END


            StrQ &= "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND mastertype = 30 " & _
                     "LEFT JOIN hrinstitute_master ON dbo.hrtraining_scheduling.traininginstituteunkid = dbo.hrinstitute_master.instituteunkid AND ishospital = 0 " & _
                "LEFT JOIN " & _
                "( " & _
                     "SELECT " & _
                       "courseunkid " & _
                      ",cfcommon_master.name AS Goal " & _
                     "FROM hrtnacoursepriority_tran " & _
                          "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtnacoursepriority_tran.goalunkid AND mastertype = 31 " & _
                     "WHERE hrtnacoursepriority_tran.isvoid = 0 " & _
                ") AS SG ON SG.courseunkid = hrtraining_scheduling.course_title " & _
                "LEFT JOIN " & _
                "( " & _
                     "SELECT " & _
                       "courseunkid " & _
                      ",STUFF((SELECT DISTINCT '\r\n' + hrjobgroup_master.name FROM hrjobgroup_master " & _
                    "                   LEFT JOIN " & _
                    "                   ( " & _
                    "                       SELECT " & _
                    "                           jobunkid " & _
                    "                           ,jobgroupunkid " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_categorization_tran " & _
                    "                       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "                   ) AS Jobs ON Jobs.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND Jobs.rno = 1 " & _
                                "JOIN hremployee_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                    "                   JOIN hrtnaemp_course_tran AS t1 ON t1.employeeunkid = hremployee_master.employeeunkid AND t1.courseunkid = hrtnaemp_course_tran.courseunkid " & _
                               "FOR XML PATH('')),1,4,'') AS Target_Grp " & _
                      ",STUFF((SELECT DISTINCT '\r\n' + hrdepartment_master.name FROM hrdepartment_master " & _
                    "                   LEFT JOIN " & _
                    "                       ( " & _
                    "                           SELECT " & _
                    "                               departmentunkid " & _
                    "                               ,employeeunkid " & _
                    "                               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                           FROM hremployee_transfer_tran " & _
                    "                           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "                       ) AS Alloc ON Alloc.departmentunkid = hrdepartment_master.departmentunkid AND Alloc.rno = 1 " & _
                    "                   JOIN hremployee_master ON hremployee_master.employeeunkid = Alloc.employeeunkid " & _
                    "                   JOIN hrtnaemp_course_tran AS t1 ON t1.employeeunkid = hremployee_master.employeeunkid AND t1.courseunkid = hrtnaemp_course_tran.courseunkid " & _
                               "FOR XML PATH('')),1,4,'') AS Dept " & _
                     "FROM hrtnaemp_course_tran " & _
                     "GROUP BY courseunkid " & _
                ") AS Emp_Data ON Emp_Data.courseunkid = hrtraining_scheduling.course_title "

            'Shani(24-Aug-2015) -- End

            objDataOperation.AddParameter("@MONTH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, " Month(s)"))
            objDataOperation.AddParameter("@Weeks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, " Week(s)"))

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
            objDataOperation.AddParameter("@COST_CENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 586, "Cost Center"))
            'S.SANDEEP [08-FEB-2017] -- END

            StrQ &= " WHERE 1 = 1 " & _
                    " AND hrtraining_scheduling.isvoid = 0 AND hrtraining_scheduling.iscancel = 0 "

            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@fDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                objDataOperation.AddParameter("@tDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))

                StrQ &= " AND ISNULL(CONVERT(CHAR(8),start_date,112),'') >= @fDate AND ISNULL(CONVERT(CHAR(8),end_date,112),'') <= @tDate "
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim i As Integer = 1
            Dim objCMaster As New clsCommon_Master
            Dim StrFunds As String = String.Empty
            Dim dicIsAdded As New Dictionary(Of String, String)
            Dim dFilter As DataTable = Nothing
            Dim dtRow As DataRow = Nothing
            For Each dRow As DataRow In dsList.Tables("List").Rows
                If dicIsAdded.ContainsKey(dRow.Item("Goal")) = True Then Continue For
                dicIsAdded.Add(dRow.Item("Goal"), dRow.Item("Goal"))

                dtRow = mdtFinalTable.NewRow

                'S.SANDEEP [13-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
                'dtRow.Item("Column1") = Language.getMessage(mstrModuleName, 20, "Strategic Goal :") & " " & dRow.Item("Goal")
                'mdtFinalTable.Rows.Add(dtRow)
                'S.SANDEEP [13-FEB-2017] -- END

                dFilter = New DataView(dsList.Tables("List"), "Goal = '" & dRow.Item("Goal") & "'", "", DataViewRowState.CurrentRows).ToTable
                i = 1
                For Each dFRow As DataRow In dFilter.Rows
                    StrFunds = String.Empty
                    dtRow = mdtFinalTable.NewRow
                    Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                    If mstrAllocationIds.Trim.Length > 0 Then
                        If objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                    End If
                    If dFRow.Item("alloc_binded").ToString.Length > 0 Then
                        dtRow.Item("Column16") = dFRow.Item("alloc_binded") & " -> " & objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds)
                        dtRow.Item("Column16") = dtRow.Item("Column16").ToString.Replace("&lt;", "<")
                        dtRow.Item("Column16") = dtRow.Item("Column16").ToString.Replace("&gt;", ">")
                        dtRow.Item("Column16") = dtRow.Item("Column16").ToString.Replace("&amp;", "&")
                    End If
                    objTrainingAlloc = Nothing

                    dtRow.Item("Column1") = i.ToString
                    dtRow.Item("Column2") = dFRow.Item("COURSE")
                    dtRow.Item("Column3") = dFRow.Item("TARGET_GROUP").ToString.Replace("\r\n", ",")
                    dtRow.Item("Column4") = dFRow.Item("VENUE")
                    dtRow.Item("Column5") = eZeeDate.convertDate(dFRow.Item("STDATE").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dFRow.Item("EDDATE").ToString).ToShortDateString
                    'S.SANDEEP [08-FEB-2017] -- START
                    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                    'dtRow.Item("Column6") = dFRow.Item("Duration")
                    Dim strDurationText As String = ""
                    If dFRow.Item("Years") > 0 Then
                        strDurationText = dFRow.Item("Years").ToString & " " & Language.getMessage(mstrModuleName, 100, "Year(s)")
                    End If
                    If dFRow.Item("Months") > 0 Then
                        strDurationText &= " " & dFRow.Item("Months").ToString & " " & Language.getMessage(mstrModuleName, 18, " Month(s)")
                    End If
                    If dFRow.Item("Days") > 0 Then
                        strDurationText &= " " & dFRow.Item("Days").ToString & " " & Language.getMessage(mstrModuleName, 101, " Day(s)")
                    End If
                    dtRow.Item("Column6") = strDurationText
                    'S.SANDEEP [08-FEB-2017] -- END
                    dtRow.Item("Column7") = dFRow.Item("NOOFSTAFF")
                    dtRow.Item("Column8") = Format(CDec(dFRow.Item("TUITION_CONF")), GUI.fmtCurrency)
                    dtRow.Item("Column9") = Format(CDec(dFRow.Item("UP_KEEP")), GUI.fmtCurrency)
                    dtRow.Item("Column10") = Format(CDec(dFRow.Item("FARE")), GUI.fmtCurrency)
                    dtRow.Item("Column11") = Format(CDec(dFRow.Item("TOTAL")), GUI.fmtCurrency)
                    dtRow.Item("Column12") = dFRow.Item("PROVIDER")

                    For Each StrId As String In dFRow.Item("fundingids").ToString.Split(",")
                        If StrId.Trim.Length > 0 Then
                            objCMaster._Masterunkid = StrId
                            StrFunds &= "," & objCMaster._Name
                        End If
                    Next
                    If StrFunds.Trim.Length > 0 Then
                        StrFunds = Mid(StrFunds, 2)
                    End If
                    dtRow.Item("Column13") = StrFunds
                    dtRow.Item("Column14") = dFRow.Item("ORIGINATING_CORDINATING_DEPT").ToString.Replace("\r\n", ",")

                    'S.SANDEEP [13-FEB-2017] -- START
                    'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
                    dtRow.Item("Column15") = dFRow.Item("Column15")
                    dtRow.Item("Column17") = dFRow.Item("Column17") & " " & Language.getMessage(mstrModuleName, 300, "Hrs")
                    dtRow.Item("Column18") = IIf(dFRow.Item("Goal").ToString.Trim.Length <= 0, Language.getMessage(mstrModuleName, 301, "N/A"), dFRow.Item("Goal"))
                    'S.SANDEEP [13-FEB-2017] -- END


                    mdtFinalTable.Rows.Add(dtRow)
                    i += 1
                Next
            Next

            'S.SANDEEP [13-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            '    Call ReportFunction.Open_ExportedFile(xOpenReportAfterExport, StrFinalPath)
            'End If

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtFinalTable.Columns.Count - 1)
            For j As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(j) = 125
            Next
            Dim strarrGroupColumns As String() = Nothing
            mdtFinalTable.Columns("Column18").Caption = Language.getMessage(mstrModuleName, 20, "Strategic Goal :")
            Dim strGrpCols As String() = {"Column18"}
            strarrGroupColumns = strGrpCols

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportPath, xOpenReportAfterExport, mdtFinalTable, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", "", Nothing, "", True, Nothing, Nothing, Nothing)
            'S.SANDEEP [13-FEB-2017] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Training_Plan_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 586, "Cost Center")
            Language.setMessage(mstrModuleName, 1, "S/N")
            Language.setMessage(mstrModuleName, 2, "COURSE TITLE")
            Language.setMessage(mstrModuleName, 3, "TARGET GROUP")
            Language.setMessage(mstrModuleName, 4, "VENUE")
            Language.setMessage(mstrModuleName, 5, "DATES")
            Language.setMessage(mstrModuleName, 6, "DURATION")
            Language.setMessage(mstrModuleName, 7, "NO. OF STAFF")
            Language.setMessage(mstrModuleName, 8, "TUITION & CONF.")
            Language.setMessage(mstrModuleName, 9, "UP KEEP")
            Language.setMessage(mstrModuleName, 10, "FARE")
            Language.setMessage(mstrModuleName, 11, "TOTAL COST")
            Language.setMessage(mstrModuleName, 12, "PROVIDER")
            Language.setMessage(mstrModuleName, 13, "SOURCE OF FUNDING")
            Language.setMessage(mstrModuleName, 14, "ORIGINATING COORDINATING DEPT")
            Language.setMessage(mstrModuleName, 15, "STRATEGIC GOAL")
            Language.setMessage(mstrModuleName, 16, "From Date :")
            Language.setMessage(mstrModuleName, 17, "to")
            Language.setMessage(mstrModuleName, 18, " Month(s)")
            Language.setMessage(mstrModuleName, 19, " Week(s)")
            Language.setMessage(mstrModuleName, 20, "Strategic Goal :")
            Language.setMessage(mstrModuleName, 23, "Prepared By :")
            Language.setMessage(mstrModuleName, 24, "TRAINING OBJECTIVE")
            Language.setMessage(mstrModuleName, 25, "TRAINING HOURS")
            Language.setMessage(mstrModuleName, 26, "Date :")
            Language.setMessage(mstrModuleName, 100, "Year(s)")
            Language.setMessage(mstrModuleName, 101, " Day(s)")
            Language.setMessage(mstrModuleName, 102, "Allocation Binded :")
            Language.setMessage(mstrModuleName, 200, "BINDED ALLOCATION")
            Language.setMessage(mstrModuleName, 300, "Hrs")
            Language.setMessage(mstrModuleName, 301, "N/A")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
