'************************************************************************************************************************************
'Class Name : frmEmployeeMovementReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : Sandeep Sharma { 04 Sep 2015 }
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeMovementReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeMovementReport"
    Private objEmovementReport As clsEmployeeMovementReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing

#End Region

#Region " Contructor "

    Public Sub New()
        objEmovementReport = New clsEmployeeMovementReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmovementReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Public Function Validation() As Boolean
        Try
            If dtpFromdate.Checked And dtpToDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select To Date. To Date is compulsory information."), enMsgBoxStyle.Information)
                dtpTodate.Focus()
                Return False
            ElseIf dtpFromdate.Checked = False And dtpToDate.Checked Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select From Date. From Date is compulsory information."), enMsgBoxStyle.Information)
                dtpFromdate.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            mdtDate1, _
                                            mdtDate2, _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Employee Movement Allocation Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Employee Movement Re-categorization Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Employee Movement Grades Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Employee Movement Costcenter Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Employee Movement Summary"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            txtFromScale.Text = 0
            txtToScale.Text = 0
            dtpFromdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpFromdate.Checked = False
            dtpTodate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpTodate.Checked = False
            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""
            chkShowEmpScale.Checked = False
            cboAllocationReason.SelectedValue = 0
            mdtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            mdtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmovementReport.SetDefaultValue()

            objEmovementReport._EmployeeId = cboEmployee.SelectedValue
            objEmovementReport._EmployeeName = cboEmployee.Text

            If dtpFromdate.Checked And dtpTodate.Checked Then
                objEmovementReport._FromDate = dtpFromdate.Value.Date
                objEmovementReport._ToDate = dtpTodate.Value.Date

                mdtDate1 = dtpFromdate.Value
                mdtDate2 = dtpTodate.Value
            Else
                mdtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If txtFromScale.Text.Trim = "" Then
                objEmovementReport._FromScale = 0
            Else
                objEmovementReport._FromScale = CDbl(txtFromScale.Text.Trim)
            End If

            If txtToScale.Text.Trim = "" Then
                objEmovementReport._ToScale = 0
            Else
                objEmovementReport._ToScale = CDbl(txtToScale.Text.Trim)
            End If

            objEmovementReport._IsActive = chkInactiveemp.Checked
            objEmovementReport._Advance_Filter = mstrAdvanceFilter
            objEmovementReport._ShowEmployeeScale = chkShowEmpScale.Checked
            objEmovementReport._ReportId = CInt(cboReportType.SelectedIndex)
            objEmovementReport._ReportTypeName = cboReportType.Text
            objEmovementReport._AllocationReasonId = CInt(cboAllocationReason.SelectedValue)
            objEmovementReport._AllocationReason = IIf(cboAllocationReason.SelectedValue > 0, cboAllocationReason.Text, "")

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeMovementReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmovementReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMovementReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me.eZeeHeader.Title = objEmovementReport._ReportName
            Me.eZeeHeader.Message = objEmovementReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

            chkShowEmpScale.Visible = User._Object.Privilege._AllowTo_View_Scale

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMovementReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMovementReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeMovementReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Validation() Then
                If Not SetFilter() Then Exit Sub
                objEmovementReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                                     ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                                     cboReportType.SelectedIndex, enPrintAction.None, enExportAction.None)
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAllocationReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAllocationReason.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboAllocationReason.DataSource
            frm.ValueMember = cboAllocationReason.ValueMember
            frm.DisplayMember = cboAllocationReason.DisplayMember
            If frm.DisplayDialog Then
                cboAllocationReason.SelectedValue = frm.SelectedValue
                cboAllocationReason.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAllocationReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeMovementReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeMovementReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " ComboBox Event "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            cboAllocationReason.DataSource = Nothing
            If cboReportType.SelectedIndex <= 3 Then
                cboAllocationReason.Enabled = True : objbtnSearchAllocationReason.Enabled = True
                Dim dsList As New DataSet : Dim objCMaster As New clsCommon_Master
                Select Case cboReportType.SelectedIndex
                    Case 0
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
                    Case 1
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
                    Case 2
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "List")
                    Case 3
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COST_CENTER, True, "List")
                    Case Else
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
                End Select
                With cboAllocationReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = 0
                End With
                dsList.Dispose()
                objCMaster = Nothing
            Else
                cboAllocationReason.Enabled = False : objbtnSearchAllocationReason.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblFromscale.Text = Language._Object.getCaption(Me.lblFromscale.Name, Me.lblFromscale.Text)
            Me.lblToScale.Text = Language._Object.getCaption(Me.lblToScale.Name, Me.lblToScale.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.Name, Me.chkShowEmpScale.Text)
            Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
            Me.LblAllocationReason.Text = Language._Object.getCaption(Me.LblAllocationReason.Name, Me.LblAllocationReason.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select To Date. To Date is compulsory information.")
            Language.setMessage(mstrModuleName, 2, "Please select From Date. From Date is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Employee Movement Report")
            Language.setMessage(mstrModuleName, 4, "Employee Movement Report With Allocations")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class frmEmployeeMovementReport

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmEmployeeMovementReport"
'    Private objEmovementReport As clsEmployeeMovementReport
'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrAdvanceFilter As String = String.Empty
'    'S.SANDEEP [ 13 FEB 2013 ] -- END

'#End Region

'#Region " Contructor "

'    Public Sub New()
'        objEmovementReport = New clsEmployeeMovementReport(User._Object._Languageunkid,Company._Object._Companyunkid)
'        objEmovementReport.SetDefaultValue()
'        InitializeComponent()
'        'S.SANDEEP [ 13 FEB 2013 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        _Show_AdvanceFilter = True
'        'S.SANDEEP [ 13 FEB 2013 ] -- END
'    End Sub

'#End Region

'#Region " Private Function "

'    Public Function Validation() As Boolean
'        Try
'            If dtpFromdate.Checked And dtpToDate.Checked = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select To Date. To Date is compulsory information."), enMsgBoxStyle.Information)
'                dtpTodate.Focus()
'                Return False
'            ElseIf dtpFromdate.Checked = False And dtpToDate.Checked Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select From Date. From Date is compulsory information."), enMsgBoxStyle.Information)
'                dtpFromdate.Focus()
'                Return False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
'        End Try
'        Return True
'    End Function

'    Private Sub FillCombo()
'        Dim ObjEmp As New clsEmployee_Master
'        Dim objCost As New clscostcenter_master
'        Dim ObjBMaster As New clsStation
'        Dim ObjDMaster As New clsDepartment
'        Dim ObjSection As New clsSections
'        Dim ObjJMaster As New clsJobs
'        Dim ObjGradeGrp As New clsGradeGroup
'        Dim dsList As New DataSet
'        Try


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            'dsList = ObjEmp.GetEmployeeList("Emp", True, True)

'            'S.SANDEEP [ 15 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'dsList = ObjEmp.GetEmployeeList("Emp", True, False)

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
'            'Else
'            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'            'End If

'            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                            User._Object._Userunkid, _
'                                            FinancialYear._Object._YearUnkid, _
'                                            Company._Object._Companyunkid, _
'                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                            ConfigParameter._Object._UserAccessModeSetting, _
'                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            'S.SANDEEP [ 15 MAY 2012 ] -- END

'            'Pinkal (24-Jun-2011) -- End


'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsList.Tables("Emp")
'                .SelectedValue = 0
'            End With

'            dsList = objCost.getComboList("CostCenter", True)
'            With cboCostcenter
'                .ValueMember = "costcenterunkid"
'                .DisplayMember = "costcentername"
'                .DataSource = dsList.Tables("CostCenter")
'                .SelectedValue = 0
'            End With

'            dsList = ObjBMaster.getComboList("Branch", True)
'            With cboBranch
'                .ValueMember = "stationunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Branch")
'                .SelectedValue = 0
'            End With

'            dsList = ObjDMaster.getComboList("Dept", True)
'            With cboDepartment
'                .ValueMember = "departmentunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Dept")
'                .SelectedValue = 0
'            End With

'            dsList = ObjJMaster.getComboList("Job", True)
'            With cboJobs
'                .ValueMember = "jobunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Job")
'                .SelectedValue = 0
'            End With

'            dsList = ObjGradeGrp.getComboList("GradeGroup", True)
'            With cboGradeGroup
'                .ValueMember = "gradegroupunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("GradeGroup")
'                .SelectedValue = 0
'            End With


'            dsList = ObjGradeGrp.getComboList("GradeGroup", True)
'            With cboGradeGroup
'                .ValueMember = "gradegroupunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("GradeGroup")
'                .SelectedValue = 0
'            End With

'            'Pinkal (03-Nov-2014) -- Start
'            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

'            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Employee Movement Report"))
'            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "Employee Movement Report With Allocations"))
'            cboReportType.SelectedIndex = 0

'            Dim objvoidReason As New clsVoidReason
'            dsList = objvoidReason.getComboList(1, True, "List")
'            With cboAllocationReason
'                .ValueMember = "Id"
'                .DisplayMember = "NAME"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With

'            'Pinkal (03-Nov-2014) -- End


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            ObjEmp = Nothing
'            ObjBMaster = Nothing
'            ObjDMaster = Nothing
'            ObjJMaster = Nothing
'            dsList.Dispose()
'        End Try
'    End Sub

'    Private Sub ResetValue()
'        Try
'            cboEmployee.SelectedValue = 0
'            cboCostcenter.SelectedValue = 0
'            cboBranch.SelectedValue = 0
'            cboDepartment.SelectedValue = 0
'            cboSection.SelectedValue = 0
'            cboUnit.SelectedValue = 0
'            cboJobs.SelectedValue = 0
'            cboGradeGroup.SelectedValue = 0
'            cboGrade.SelectedValue = 0
'            cboGradeLevel.SelectedValue = 0
'            txtFromScale.Text = 0
'            txtToScale.Text = 0
'            dtpFromdate.Value = ConfigParameter._Object._CurrentDateAndTime
'            dtpFromdate.Checked = False
'            dtpTodate.Value = ConfigParameter._Object._CurrentDateAndTime
'            dtpTodate.Checked = False
'            ' objEmovementReport.setDefaultOrderBy(0)
'            ' txtOrderBy.Text = objEmovementReport.OrderByDisplay


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            chkInactiveemp.Checked = False
'            'Pinkal (24-Jun-2011) -- End

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mstrAdvanceFilter = ""
'            'S.SANDEEP [ 13 FEB 2013 ] -- END


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes
'            chkShowEmpScale.Checked = False
'            'Pinkal (24-Apr-2013) -- End


'            'Pinkal (03-Nov-2014) -- Start
'            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
'            cboAllocationReason.SelectedIndex = 0
'            'Pinkal (03-Nov-2014) -- End


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Function SetFilter() As Boolean
'        Try
'            objEmovementReport.SetDefaultValue()

'            objEmovementReport._EmployeeId = cboEmployee.SelectedValue
'            objEmovementReport._EmployeeName = cboEmployee.Text

'            objEmovementReport._CostCenterId = cboCostcenter.SelectedValue
'            objEmovementReport._CostCenterName = cboCostcenter.Text

'            objEmovementReport._BranchId = cboBranch.SelectedValue
'            objEmovementReport._BranchName = cboBranch.Text

'            objEmovementReport._DepartmentId = cboDepartment.SelectedValue
'            objEmovementReport._DepartmentName = cboDepartment.Text

'            objEmovementReport._SectionId = cboSection.SelectedValue
'            objEmovementReport._SectionName = cboSection.Text

'            objEmovementReport._UnitId = cboUnit.SelectedValue
'            objEmovementReport._UnitName = cboUnit.Text

'            objEmovementReport._JobId = cboJobs.SelectedValue
'            objEmovementReport._JobName = cboJobs.Text

'            objEmovementReport._GradeGroupId = cboGradeGroup.SelectedValue
'            objEmovementReport._GradeGroupName = cboGradeGroup.Text

'            objEmovementReport._GradeId = cboGrade.SelectedValue
'            objEmovementReport._GradeName = cboGrade.Text

'            objEmovementReport._GradeLevelId = cboGradeLevel.SelectedValue
'            objEmovementReport._GradeLevelName = cboGradeLevel.Text

'            If dtpFromdate.Checked And dtpTodate.Checked Then
'                objEmovementReport._FromDate = dtpFromdate.Value.Date
'                objEmovementReport._ToDate = dtpTodate.Value.Date
'            End If

'            If txtFromScale.Text.Trim = "" Then
'                objEmovementReport._FromScale = 0
'            Else
'                objEmovementReport._FromScale = CDbl(txtFromScale.Text.Trim)
'            End If

'            If txtToScale.Text.Trim = "" Then
'                objEmovementReport._ToScale = 0
'            Else
'                objEmovementReport._ToScale = CDbl(txtToScale.Text.Trim)
'            End If


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
'            objEmovementReport._IsActive = chkInactiveemp.Checked
'            'Pinkal (24-Jun-2011) -- End

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objEmovementReport._Advance_Filter = mstrAdvanceFilter
'            'S.SANDEEP [ 13 FEB 2013 ] -- END


'            'Pinkal (24-Apr-2013) -- Start
'            'Enhancement : TRA Changes
'            objEmovementReport._ShowEmployeeScale = chkShowEmpScale.Checked
'            'Pinkal (24-Apr-2013) -- End


'            'Pinkal (03-Nov-2014) -- Start
'            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
'            objEmovementReport._ReportId = CInt(cboReportType.SelectedIndex)
'            objEmovementReport._ReportTypeName = cboReportType.Text
'            objEmovementReport._AllocationReasonId = CInt(cboAllocationReason.SelectedValue)
'            objEmovementReport._AllocationReason = IIf(cboAllocationReason.SelectedValue > 0, cboAllocationReason.Text, "")
'            'Pinkal (03-Nov-2014) -- End


'            Return True

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Forms "

'    Private Sub frmEmployeeMovementReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Try
'            objEmovementReport = Nothing
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_FormClosed", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmEmployeeMovementReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Language.setLanguage(Me.Name)

'            'S.SANDEEP [ 03 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call OtherSettings()
'            'S.SANDEEP [ 03 SEP 2012 ] -- END

'            Me._Title = objEmovementReport._ReportName
'            Me._Message = objEmovementReport._ReportDesc

'            Call FillCombo()
'            Call ResetValue()


'            'Pinkal (30-Apr-2013) -- Start
'            'Enhancement : TRA Changes
'            chkShowEmpScale.Visible = User._Object.Privilege._AllowTo_View_Scale
'            'Pinkal (30-Apr-2013) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmEmployeeMovementReport_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmEmployeeMovementReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.Control Then
'                If e.KeyCode = Windows.Forms.Keys.R Then
'                    Call frmEmployeeMovementReport_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmEmployeeMovementReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        Try
'            Select Case e.KeyChar
'                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
'                    Windows.Forms.SendKeys.Send("{TAB}")
'                    e.Handled = True
'                    Exit Select
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Buttons "

'    Private Sub frmEmployeeMovementReport_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
'        Try
'            If Validation() Then
'                If Not SetFilter() Then Exit Sub

'                'Pinkal (03-Nov-2014) -- Start
'                'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
'                If CInt(cboReportType.SelectedIndex) = 0 Then
'                objEmovementReport.generateReport(0, e.Type, enExportAction.None)
'                ElseIf CInt(cboReportType.SelectedIndex) = 1 Then
'                    objEmovementReport.Generate_MovementReportWithAllocations()
'                End If
'                'Pinkal (03-Nov-2014) -- End
'            End If
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Report_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub frmEmployeeMovementReport_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
'        Try
'            If Validation() Then
'                If Not SetFilter() Then Exit Sub
'                'Pinkal (03-Nov-2014) -- Start
'                'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
'                If CInt(cboReportType.SelectedIndex) = 0 Then
'                objEmovementReport.generateReport(0, enPrintAction.None, e.Type)
'                ElseIf CInt(cboReportType.SelectedIndex) = 1 Then
'                    objEmovementReport.Generate_MovementReportWithAllocations()
'                End If
'                'Pinkal (03-Nov-2014) -- End
'            End If
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Export_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub frmEmployeeMovementReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
'        Try
'            Call ResetValue()
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Reset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmEmployeeMovementReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmEmployeeMovementReport_Cancel_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim frm As New frmCommonSearch
'        Try
'            frm.DataSource = cboEmployee.DataSource
'            frm.ValueMember = cboEmployee.ValueMember
'            frm.DisplayMember = cboEmployee.DisplayMember
'            frm.CodeMember = "employeecode"
'            If frm.DisplayDialog Then
'                cboEmployee.SelectedValue = frm.SelectedValue
'                cboEmployee.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub


'    'Pinkal (03-Nov-2014) -- Start
'    'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

'    Private Sub objbtnSearchAllocationReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAllocationReason.Click
'        Dim frm As New frmCommonSearch
'        Try
'            frm.DataSource = cboAllocationReason.DataSource
'            frm.ValueMember = cboAllocationReason.ValueMember
'            frm.DisplayMember = cboAllocationReason.DisplayMember
'            If frm.DisplayDialog Then
'                cboAllocationReason.SelectedValue = frm.SelectedValue
'                cboAllocationReason.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchAllocationReason_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    'Pinkal (03-Nov-2014) -- End


'    'S.SANDEEP [ 03 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsEmployeeMovementReport.SetMessages()
'            objfrm._Other_ModuleNames = "clsEmployeeMovementReport"
'            objfrm.displayDialog(Me)

'            Call Language.setLanguage(Me.Name)
'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 03 SEP 2012 ] -- END

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
'        Dim frm As New frmAdvanceSearch
'        Try
'            frm._Hr_EmployeeTable_Alias = "hremployee_master"
'            frm.ShowDialog()
'            mstrAdvanceFilter = frm._GetFilterString
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 13 FEB 2013 ] -- END

'#End Region

'#Region " Controls "

'    Private Sub cboDepartment_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim dsList As New DataSet
'        Dim ObjSection As New clsSections
'        Try
'            If cboDepartment.SelectedValue IsNot Nothing Then
'                dsList = ObjSection.getComboList("Section", True, CInt(cboDepartment.SelectedValue))
'                With cboSection
'                    .ValueMember = "sectionunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Section")
'                    .SelectedValue = 0
'                End With
'            End If

'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "cboDepartment_SelectedValueChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboSection_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim dsList As New DataSet
'        Dim Objunit As New clsUnits
'        Try
'            If cboSection.SelectedValue IsNot Nothing Then
'                dsList = Objunit.getComboList("Unit", True, CInt(cboSection.SelectedValue))
'                With cboUnit
'                    .ValueMember = "unitunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Unit")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "cboSection_SelectedValueChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboGradeGroup_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim dsList As New DataSet
'        Dim ObjGrade As New clsGrade
'        Try
'            If cboGradeGroup.SelectedValue IsNot Nothing Then
'                dsList = ObjGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
'                With cboGrade
'                    .ValueMember = "gradeunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Grade")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedValueChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboGrade_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim dsList As New DataSet
'        Dim ObjGradeLevel As New clsGradeLevel
'        Try
'            If cboGrade.SelectedValue IsNot Nothing Then
'                dsList = ObjGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
'                With cboGradeLevel
'                    .ValueMember = "gradelevelunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("GradeLevel")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedValueChanged", mstrModuleName)
'        End Try
'    End Sub


'#End Region

'	'<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'	Private Sub OtherSettings()
'		Try
'			Me.SuspendLayout()

'			Call SetLanguage()

'			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
'			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


'			Me.ResumeLayout()
'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
'		End Try
'	End Sub


'	Private Sub SetLanguage()
'		Try
'			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
'			Me.lblJobs.Text = Language._Object.getCaption(Me.lblJobs.Name, Me.lblJobs.Text)
'			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
'			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
'			Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
'			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
'			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
'			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
'			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
'			Me.lblGradeGRoup.Text = Language._Object.getCaption(Me.lblGradeGRoup.Name, Me.lblGradeGRoup.Text)
'			Me.lblFromscale.Text = Language._Object.getCaption(Me.lblFromscale.Name, Me.lblFromscale.Text)
'			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
'			Me.lblToScale.Text = Language._Object.getCaption(Me.lblToScale.Name, Me.lblToScale.Text)
'			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
'			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
'			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
'			Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.Name, Me.chkShowEmpScale.Text)
'			Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
'			Me.LblAllocationReason.Text = Language._Object.getCaption(Me.LblAllocationReason.Name, Me.LblAllocationReason.Text)

'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
'		End Try
'	End Sub


'	Private Sub SetMessages()
'		Try
'			Language.setMessage(mstrModuleName, 1, "Please select To Date. To Date is compulsory information.")
'			Language.setMessage(mstrModuleName, 2, "Please select From Date. From Date is compulsory information.")
'			Language.setMessage(mstrModuleName, 3, "Employee Movement Report")
'			Language.setMessage(mstrModuleName, 4, "Employee Movement Report With Allocations")

'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
'		End Try
'	End Sub
'#End Region 'Language & UI Settings
'	'</Language>

'End Class
