'************************************************************************************************************************************
'Class Name : frmNotAssessedEmployeeReport.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmNotAssessedEmployeeReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmNotAssessedEmployeeReport"
    Private objNAReport As clsNotAssessedEmployee_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        objNAReport = New clsNotAssessedEmployee_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objNAReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        'S.SANDEEP [ 22 OCT 2013 ] -- START
        Dim objMaster As New clsMasterData
        'S.SANDEEP [ 22 OCT 2013 ] -- END
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 1)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            With cboViewMode
                .Items.Clear()
                '.Items.Add(Language.getMessage(mstrModuleName, 1, "Both (General Assessment & BSC)"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "General Assessment"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "BSC Assessment"))
            End With

            With cboReportMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Self Assessment"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Assessor Assessment"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Reviewer Assessment"))
            End With

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            dsList = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 1
            End With
            'S.SANDEEP [ 22 OCT 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboViewMode.SelectedIndex = 0
            cboReportMode.SelectedIndex = 0
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 22 OCT 2013 ] -- START
            cboAppointment.SelectedValue = 1
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [ 03 NOV 2014 ] -- START
            chkConsiderEmployeeTermination.Checked = False
            'S.SANDEEP [ 03 NOV 2014 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboReportMode.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Report Mode."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Return False
            End If

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
                If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
                    dtpDate2.Focus()
                    Return False
                ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
                    dtpDate1.Focus()
                    Return False
                ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                    If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Appointment To Date cannot be less than Appointment From Date."), enMsgBoxStyle.Information)
                        dtpDate2.Focus()
                        Return False
                    End If
                End If
            End If
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            objNAReport.SetDefaultValue()

            objNAReport._Advance_Filter = mstrAdvanceFilter
            objNAReport._ViewByIds = mstrStringIds
            objNAReport._ViewIndex = mintViewIdx
            objNAReport._ViewByName = mstrStringName
            objNAReport._Analysis_Fields = mstrAnalysis_Fields
            objNAReport._Analysis_Join = mstrAnalysis_Join
            objNAReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objNAReport._Report_GroupName = mstrReport_GroupName
            objNAReport._ModeSelectedId = cboViewMode.SelectedIndex
            objNAReport._ModeSelectedName = cboViewMode.Text
            objNAReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objNAReport._PeriodName = cboPeriod.Text
            objNAReport._ReportModeId = cboReportMode.SelectedIndex
            objNAReport._ReportModeName = cboReportMode.Text
            'S.SANDEEP [ 22 OCT 2013 ] -- START
            objNAReport._AppointmentTypeId = cboAppointment.SelectedValue
            objNAReport._AppointmentTypeName = cboAppointment.Text
            If dtpDate1.Checked = True Then
                objNAReport._Date1 = dtpDate1.Value.Date
            End If

            If dtpDate2.Visible = True Then
                If dtpDate2.Checked = True Then
                    objNAReport._Date2 = dtpDate2.Value.Date
                End If
            End If
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [ 03 NOV 2014 ] -- START
            objNAReport._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked
            'S.SANDEEP [ 03 NOV 2014 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub btnAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmNotAssessedEmployeeReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objNAReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNotAssessedEmployeeReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNotAssessedEmployeeReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNotAssessedEmployeeReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNotAssessedEmployeeReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            EZeeHeader1.Title = objNAReport._ReportName
            EZeeHeader1.Message = objNAReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNotAssessedEmployeeReport_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objNAReport.Export_Not_Assessed_Report()
            objNAReport.Export_Not_Assessed_Report(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) 'S.SANDEEP [13-JUL-2017] -- START -- END
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsNotAssessedEmployee_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsNotAssessedEmployee_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssetDeclarationCategoryWise_Language_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 22 OCT 2013 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.Name, Me.lblReportMode.Text)
            Me.lblViewMode.Text = Language._Object.getCaption(Me.lblViewMode.Name, Me.lblViewMode.Text)
            Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
            Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.chkConsiderEmployeeTermination.Text = Language._Object.getCaption(Me.chkConsiderEmployeeTermination.Name, Me.chkConsiderEmployeeTermination.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "General Assessment")
            Language.setMessage(mstrModuleName, 3, "BSC Assessment")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Self Assessment")
            Language.setMessage(mstrModuleName, 6, "Assessor Assessment")
            Language.setMessage(mstrModuleName, 7, "Reviewer Assessment")
            Language.setMessage(mstrModuleName, 8, "Please select Period.")
            Language.setMessage(mstrModuleName, 9, "Please select Report Mode.")
            Language.setMessage(mstrModuleName, 10, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date.")
            Language.setMessage(mstrModuleName, 11, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Appointment To Date cannot be less than Appointment From Date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
