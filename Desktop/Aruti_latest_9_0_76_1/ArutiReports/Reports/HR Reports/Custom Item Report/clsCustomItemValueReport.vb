'************************************************************************************************************************************
'Class Name : clsCustomItemValueReport.vb
'Purpose    :
'Date       : 15 FEB 2017
'Written By : Shani K. Sheladiya 
'************************************************************************************************************************************

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO
#End Region

Public Class clsCustomItemValueReport
    Inherits IReportData

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsCustomItemValueReport"
    Private mstrReportId As String = enArutiReport.Custom_Item_Value_Report
    Private mdtFinalTable As DataTable = Nothing
    Private StrFinalPath As String = String.Empty
#End Region

#Region " Properties Value "
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrCaption As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private drItem() As DataRow = Nothing
    Private mstrReportName As String = "Custom Item Value Report"
    Private mstrCustomHeader As String = String.Empty
    Private mstrAnalysis_Join As String = ""
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrAnalysis_OrderBy As String = String.Empty
    Private mstrAnalysis_Fields As String = String.Empty

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mblnAddUserAccessLevel As Boolean = False
    'S.SANDEEP [01-OCT-2018] -- END

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Caption() As String
        Set(ByVal value As String)
            mstrCaption = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _Item_Rows() As DataRow()
        Set(ByVal value As DataRow())
            drItem = value
        End Set
    End Property

    Public WriteOnly Property _CustomHeaderText() As String
        Set(ByVal value As String)
            mstrCustomHeader = value
        End Set
    End Property

    Public WriteOnly Property _Report_Name() As String
        Set(ByVal value As String)
            mstrReportName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _AddUserAccessLevel() As Boolean
        Set(ByVal value As Boolean)
            mblnAddUserAccessLevel = value
        End Set
    End Property
    'S.SANDEEP [01-OCT-2018] -- END
#End Region

#Region " Public Function(s) And Procedures "
    Dim iColumn_DetailReport As New IColumnCollection

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '')", Language.getMessage(mstrModuleName, 1, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '')", Language.getMessage(mstrModuleName, 1, "Employee Name")))
            End If
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 2, "Employee Code")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mintPeriodId = 0
            mstrEmployeeName = ""
            mstrPeriodName = ""
            mstrCustomHeader = ""
            mstrAnalysis_Join = ""
            mstrViewByName = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = String.Empty
            drItem = Nothing
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_Fields = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    'S.SANDEEP [13-JUL-2017] -- START
    'ISSUE/ENHANCEMENT : 
    Public Sub Export_Custom_Item_Value_Report(ByVal dtEmployeeAsOnDate As DateTime, ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean, ByVal intCompanyUnkid As Integer, ByVal intUserUnkid As Integer, ByRef strFileName As String)
        'Public Sub Export_Custom_Item_Value_Report(ByVal dtEmployeeAsOnDate As DateTime)
        'S.SANDEEP [13-JUL-2017] -- END

        Try
            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid
            'S.SANDEEP [13-JUL-2017] -- END

            Call CreateTable()

            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, mdtFinalTable) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
            '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
            'End If

            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), strExportPath, mdtFinalTable) Then
                strFileName = Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
                Call ReportFunction.Open_ExportedFile(blnOpenAfterExport, StrFinalPath)
            End If
            'S.SANDEEP [13-JUL-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Assessment_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |21-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {HPO Chart Issue}
    'Private Sub CreateTable()
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dsList As DataSet
    '    Try
    '        mdtFinalTable = New DataTable("Data")

    '        StrQ = "SELECT " & _
    '               "    hremployee_master.employeeunkid " & _
    '               "   ,assessmodeid " & _
    '               "   ,hremployee_master.EmployeeCode "

    '        If mblnFirstNamethenSurname = False Then
    '            StrQ &= "   ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmployeeName "
    '        Else
    '            StrQ &= "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmployeeName "
    '        End If

    '        StrQ &= "   ,CASE assessmodeid " & _
    '                "       WHEN " & enAssessmentMode.SELF_ASSESSMENT & " THEN @Self " & _
    '                "       WHEN " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN @Appriser " & _
    '                "       WHEN " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN @Reviewer " & _
    '                "    END AS Assess_Mode "
    '        StrQ &= "," & String.Join(",", drItem.Cast(Of DataRow).Select(Function(x) "Item.[" & x.Item("custom_item").ToString & "]").ToArray()) & " "

    '        If mstrAnalysis_Join.Trim.Length > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        End If

    '        StrQ &= "FROM hrevaluation_analysis_master " & _
    '                "   LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid OR hremployee_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid " & _
    '                "   JOIN " & _
    '                "   ( " & _
    '                "       SELECT " & _
    '                "            A.analysisunkid " & _
    '                "           ,A.custom_value AS [" & drItem(0).Item("custom_item") & "] "
    '        If drItem.Length > 1 Then
    '            StrQ &= "," & String.Join(",", drItem.Cast(Of DataRow).Where(Function(x) CInt(x.Item("customitemunkid")) <> CInt(drItem(0).Item("customitemunkid"))) _
    '                            .Select(Function(x) "Item_" & CInt(x.Item("customitemunkid")) & ".[" & x.Item("custom_item").ToString & "]").ToArray())

    '        End If

    '        StrQ &= "       FROM " & _
    '                "       ( " & _
    '                "           SELECT " & _
    '                "               hrcompetency_customitem_tran.analysisunkid "

    '        If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
    '            Select Case CInt(drItem(0).Item("selectionmodeid"))
    '                Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, _
    '                     clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                    StrQ &= "       ,Sec.name AS custom_value "
    '                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                    StrQ &= "       ,Sec.field_data AS custom_value "


    '                    'S.SANDEEP |16-AUG-2019| -- START
    '                    'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
    '                Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
    '                    StrQ &= "       ,Sec.name AS custom_value "
    '                    'S.SANDEEP |16-AUG-2019| -- END
    '            End Select
    '        Else
    '            StrQ &= "               ,hrcompetency_customitem_tran.custom_value "
    '        End If

    '        StrQ &= "               ,ROW_NUMBER()OVER(PARTITION BY hrcompetency_customitem_tran.customitemunkid,hrcompetency_customitem_tran.analysisunkid " & _
    '                "                   ORDER BY customitemunkid,hrcompetency_customitem_tran.analysisunkid DESC) as Rno " & _
    '                "           FROM hrcompetency_customitem_tran "

    '        If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
    '            Select Case CInt(drItem(0).Item("selectionmodeid"))
    '                Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
    '                    StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
    '                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                    StrQ &= "   LEFT JOIN hrassess_competencies_master AS Sec ON hrassess_competencies_master.competenciesunkid = hrcompetency_customitem_tran.custom_value "
    '                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                    StrQ &= "   LEFT JOIN hrassess_empfield1_master AS Sec ON hrassess_empfield1_master.empfield1unkid = hrcompetency_customitem_tran.custom_value "
    '                    'S.SANDEEP |16-AUG-2019| -- START
    '                    'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
    '                Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
    '                    StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
    '                    'S.SANDEEP |16-AUG-2019| -- END
    '            End Select
    '        End If

    '        StrQ &= "           WHERE hrcompetency_customitem_tran.periodunkid = " & mintPeriodId & " AND hrcompetency_customitem_tran.isvoid = 0 AND " & _
    '                "               customitemunkid = " & drItem(0).Item("customitemunkid") & " " & _
    '                "       ) AS A "

    '        For Each drow As DataRow In drItem
    '            If CInt(drow("customitemunkid")) <> CInt(drItem(0).Item("customitemunkid")) Then
    '                StrQ &= "LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        hrcompetency_customitem_tran.analysisunkid "

    '                If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
    '                    Select Case CInt(drItem(0).Item("selectionmodeid"))
    '                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, _
    '                             clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                            StrQ &= "       ,Sec.name AS [" & drow("custom_item") & "] "
    '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                            StrQ &= "       ,Sec.field_data AS [" & drow("custom_item") & "] "
    '                            'S.SANDEEP |16-AUG-2019| -- START
    '                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
    '                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
    '                            StrQ &= "       ,Sec.name AS [" & drow("custom_item") & "] "
    '                            'S.SANDEEP |16-AUG-2019| -- END
    '                    End Select
    '                Else
    '                    StrQ &= "   ,hrcompetency_customitem_tran.custom_value AS [" & drow("custom_item") & "] "
    '                End If

    '                StrQ &= "       ,ROW_NUMBER()OVER(PARTITION BY hrcompetency_customitem_tran.customitemunkid,hrcompetency_customitem_tran.analysisunkid " & _
    '                        "           ORDER BY customitemunkid,hrcompetency_customitem_tran.analysisunkid DESC) as Rno " & _
    '                        "   FROM hrcompetency_customitem_tran "

    '                If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
    '                    Select Case CInt(drItem(0).Item("selectionmodeid"))
    '                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
    '                            StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
    '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                            StrQ &= "   LEFT JOIN hrassess_competencies_master AS Sec ON hrassess_competencies_master.competenciesunkid = hrcompetency_customitem_tran.custom_value "
    '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                            StrQ &= "   LEFT JOIN hrassess_empfield1_master AS Sec ON hrassess_empfield1_master.empfield1unkid = hrcompetency_customitem_tran.custom_value "
    '                            'S.SANDEEP |16-AUG-2019| -- START
    '                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
    '                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
    '                            StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
    '                            'S.SANDEEP |16-AUG-2019| -- END

    '                    End Select
    '                End If

    '                StrQ &= "   WHERE hrcompetency_customitem_tran.periodunkid = " & mintPeriodId & " AND hrcompetency_customitem_tran.isvoid = 0 AND " & _
    '                        "       customitemunkid = " & drow("customitemunkid") & " " & _
    '                        ") AS Item_" & drow("customitemunkid") & " ON Item_" & drow("customitemunkid") & ".analysisunkid = A.analysisunkid AND Item_" & drow("customitemunkid") & ".Rno = A.Rno "
    '            End If
    '        Next

    '        StrQ &= "   ) AS Item ON Item.analysisunkid = hrevaluation_analysis_master.analysisunkid "

    '        If mstrAnalysis_Join.Trim.Length > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If

    '        StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 "

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "   AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
    '        End If

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If


    '        'Shani(04-MAR-2017) -- Start
    '        'Enhancement - Add Membership and Benefit in employee dependant's screen on web
    '        'StrQ &= "ORDER by hremployee_master.employeeunkid,hrevaluation_analysis_master.periodunkid "
    '        StrQ &= "ORDER by "

    '        If mstrAnalysis_OrderBy.Trim.Length > 0 Then
    '            StrQ &= "Gname,"
    '        End If

    '        If mblnFirstNamethenSurname = False Then
    '            StrQ &= "   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')"
    '        Else
    '            StrQ &= "   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')"
    '        End If
    '        StrQ &= ",hrevaluation_analysis_master.periodunkid "
    '        'Shani(04-MAR-2017) -- End


    '        objDataOperation.AddParameter("@Self", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Self"))
    '        objDataOperation.AddParameter("@Appriser", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Assessor"))
    '        objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Reviewer"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        mdtFinalTable = dsList.Tables(0)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub CreateTable()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As DataSet
        Try
            mdtFinalTable = New DataTable("Data")

            StrQ = "SELECT " & _
                   "    hremployee_master.employeeunkid " & _
                   "   ,assessmodeid " & _
                   "   ,hremployee_master.EmployeeCode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= "   ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmployeeName "
            Else
                StrQ &= "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmployeeName "
            End If

            StrQ &= "   ,CASE assessmodeid " & _
                    "       WHEN " & enAssessmentMode.SELF_ASSESSMENT & " THEN @Self " & _
                    "       WHEN " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN @Appriser " & _
                    "       WHEN " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN @Reviewer " & _
                    "    END AS Assess_Mode "
            StrQ &= "," & String.Join(",", drItem.Cast(Of DataRow).Select(Function(x) "Item.[" & x.Item("customitemunkid").ToString & "]").ToArray()) & " "

            If mstrAnalysis_Join.Trim.Length > 0 Then
                StrQ &= mstrAnalysis_Fields
            End If

            StrQ &= "FROM hrevaluation_analysis_master " & _
                    "   LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid OR hremployee_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            A.analysisunkid " & _
                    "           ,A.custom_value AS [" & drItem(0).Item("customitemunkid") & "] "
            If drItem.Length > 1 Then
                StrQ &= "," & String.Join(",", drItem.Cast(Of DataRow).Where(Function(x) CInt(x.Item("customitemunkid")) <> CInt(drItem(0).Item("customitemunkid"))) _
                                .Select(Function(x) "Item_" & CInt(x.Item("customitemunkid")) & ".[" & x.Item("customitemunkid").ToString & "]").ToArray())

            End If

            StrQ &= "       FROM " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               hrcompetency_customitem_tran.analysisunkid "

            If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
                Select Case CInt(drItem(0).Item("selectionmodeid"))
                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, _
                         clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                        StrQ &= "       ,Sec.name AS custom_value "
                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                        StrQ &= "       ,Sec.field_data AS custom_value "


                        'S.SANDEEP |16-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                        StrQ &= "       ,Sec.name AS custom_value "
                        'S.SANDEEP |16-AUG-2019| -- END
                End Select
            Else
                StrQ &= "               ,hrcompetency_customitem_tran.custom_value "
            End If

            StrQ &= "               ,ROW_NUMBER()OVER(PARTITION BY hrcompetency_customitem_tran.customitemunkid,hrcompetency_customitem_tran.analysisunkid " & _
                    "                   ORDER BY customitemunkid,hrcompetency_customitem_tran.analysisunkid DESC) as Rno " & _
                    "           FROM hrcompetency_customitem_tran "

            If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
                Select Case CInt(drItem(0).Item("selectionmodeid"))
                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                        StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                        StrQ &= "   LEFT JOIN hrassess_competencies_master AS Sec ON hrassess_competencies_master.competenciesunkid = hrcompetency_customitem_tran.custom_value "
                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                        StrQ &= "   LEFT JOIN hrassess_empfield1_master AS Sec ON hrassess_empfield1_master.empfield1unkid = hrcompetency_customitem_tran.custom_value "
                        'S.SANDEEP |16-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                        StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
                        'S.SANDEEP |16-AUG-2019| -- END
                End Select
            End If

            StrQ &= "           WHERE hrcompetency_customitem_tran.periodunkid = " & mintPeriodId & " AND hrcompetency_customitem_tran.isvoid = 0 AND " & _
                    "               customitemunkid = " & drItem(0).Item("customitemunkid") & " " & _
                    "       ) AS A "

            For Each drow As DataRow In drItem
                If CInt(drow("customitemunkid")) <> CInt(drItem(0).Item("customitemunkid")) Then
                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        hrcompetency_customitem_tran.analysisunkid "

                    If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
                        Select Case CInt(drItem(0).Item("selectionmodeid"))
                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, _
                                 clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                StrQ &= "       ,Sec.name AS [" & drow("customitemunkid") & "] "
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                StrQ &= "       ,Sec.field_data AS [" & drow("customitemunkid") & "] "
                                'S.SANDEEP |16-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                StrQ &= "       ,Sec.name AS [" & drow("customitemunkid") & "] "
                                'S.SANDEEP |16-AUG-2019| -- END
                        End Select
                    Else
                        StrQ &= "   ,hrcompetency_customitem_tran.custom_value AS [" & drow("customitemunkid") & "] "
                    End If

                    StrQ &= "       ,ROW_NUMBER()OVER(PARTITION BY hrcompetency_customitem_tran.customitemunkid,hrcompetency_customitem_tran.analysisunkid " & _
                            "           ORDER BY customitemunkid,hrcompetency_customitem_tran.analysisunkid DESC) as Rno " & _
                            "   FROM hrcompetency_customitem_tran "

                    If CInt(drItem(0).Item("itemtypeid")) = clsassess_custom_items.enCustomType.SELECTION Then
                        Select Case CInt(drItem(0).Item("selectionmodeid"))
                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                StrQ &= "   LEFT JOIN hrassess_competencies_master AS Sec ON hrassess_competencies_master.competenciesunkid = hrcompetency_customitem_tran.custom_value "
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                StrQ &= "   LEFT JOIN hrassess_empfield1_master AS Sec ON hrassess_empfield1_master.empfield1unkid = hrcompetency_customitem_tran.custom_value "
                                'S.SANDEEP |16-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                StrQ &= "   LEFT JOIN cfcommon_master AS Sec ON Sec.masterunkid = hrcompetency_customitem_tran.custom_value "
                                'S.SANDEEP |16-AUG-2019| -- END

                        End Select
                    End If

                    StrQ &= "   WHERE hrcompetency_customitem_tran.periodunkid = " & mintPeriodId & " AND hrcompetency_customitem_tran.isvoid = 0 AND " & _
                            "       customitemunkid = " & drow("customitemunkid") & " " & _
                            ") AS Item_" & drow("customitemunkid") & " ON Item_" & drow("customitemunkid") & ".analysisunkid = A.analysisunkid AND Item_" & drow("customitemunkid") & ".Rno = A.Rno "
                End If
            Next

            StrQ &= "   ) AS Item ON Item.analysisunkid = hrevaluation_analysis_master.analysisunkid "

            If mstrAnalysis_Join.Trim.Length > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            StrQ &= "WHERE hrevaluation_analysis_master.isvoid = 0 "

            If mintEmployeeId > 0 Then
                StrQ &= "   AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'StrQ &= "ORDER by hremployee_master.employeeunkid,hrevaluation_analysis_master.periodunkid "
            StrQ &= "ORDER by "

            If mstrAnalysis_OrderBy.Trim.Length > 0 Then
                StrQ &= "Gname,"
            End If

            If mblnFirstNamethenSurname = False Then
                StrQ &= "   ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')"
            Else
                StrQ &= "   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')"
            End If
            StrQ &= ",hrevaluation_analysis_master.periodunkid "
            'Shani(04-MAR-2017) -- End


            objDataOperation.AddParameter("@Self", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Self"))
            objDataOperation.AddParameter("@Appriser", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Assessor"))
            objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Reviewer"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            Dim iCol As IEnumerable(Of DataColumn) = dsList.Tables(0).Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) Val(x.ColumnName.ToString()) <> 0)
            If iCol IsNot Nothing AndAlso iCol.Count > 0 Then
                For Each col As DataColumn In iCol
                    Dim xCol As DataColumn = col
                    Dim xHeader As String = drItem.AsEnumerable().Where(Function(x) x.Field(Of Integer)("customitemunkid").ToString() = xCol.ColumnName).Select(Function(x) x.Field(Of String)("custom_item")).FirstOrDefault()
                    dsList.Tables(0).Columns(xCol.ColumnName).ColumnName = xHeader
                Next
            End If

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            mdtFinalTable = dsList.Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |21-SEP-2019| -- END


    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColumnCount As Integer = 0
        Try
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            strBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE = Tahoma FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=100%> " & vbCrLf)

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD ALIGN = CENTER WIDTH=100%  colspan = " & objDataReader.Columns.Count - 3 & ">" & vbCrLf)
            strBuilder.Append(" <FONT SIZE=5><b>" & mstrReportName & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD>")
            strBuilder.Append(" </TR> " & vbCrLf)

            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH=40%> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 7, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD align='left' WIDTH=60% colspan = " & objDataReader.Columns.Count - 4 & "><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & vbCrLf)
            If mintEmployeeId > 0 Then
                strBuilder.Append("Employee : " & mstrEmployeeName & "," & vbCrLf)
            End If
            strBuilder.Append("Period : " & mstrPeriodName & "<BR> Custom Header : " & mstrCustomHeader & " " & vbCrLf)

            If mstrAnalysis_Join.Trim.Length > 0 Then
                strBuilder.Append("<BR>" & mstrReport_GroupName & "" & mstrViewByName & vbCrLf)
            End If

            strBuilder.Append("</B><BR>" & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH=100%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                Select Case objDataReader.Columns(j).Caption.ToUpper
                    Case "EMPLOYEEUNKID", "ASSESSMODEID", "ID", "GNAME"

                    Case Else
                        strBuilder.Append("<TD BORDER=1 WIDTH='20%' ALIGN='LEFT' style='background-color:blue; color:white; font-weight:bold; font-size:12px;'><B>" & objDataReader.Columns(j).Caption & "</B></TD>" & vbCrLf)
                        intColumnCount = intColumnCount + 1
                End Select
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            Dim intEmpid As Integer = 0
            Dim intAssessmode As Integer = 0
            Dim intGId As Integer = -1
            Dim blnEmpIdChange As Boolean = False
            For Each drow As DataRow In objDataReader.Rows

                If mstrAnalysis_Join.Trim.Trim.Length > 0 Then
                    If intGId <> CInt(drow.Item("id")) Then
                        strBuilder.Append(" <TR> " & vbCrLf)
                        strBuilder.Append(" <TD ALIGN = 'CENTER' WIDTH='100%' COLSPAN = '" & intColumnCount & "' BGCOLOR = '#DDDDDD' style='font-size:12px;'><B> " & vbCrLf)
                        strBuilder.Append(mstrReport_GroupName & drow.Item("Gname") & vbCrLf)
                        strBuilder.Append(" </B></TD> " & vbCrLf)
                        strBuilder.Append(" </TR> " & vbCrLf)
                        intGId = CInt(drow.Item("id"))
                    End If
                End If

                strBuilder.Append(" <TR> " & vbCrLf)
                If intEmpid <> CInt(drow("employeeunkid")) Then
                    intEmpid = drow("employeeunkid")
                    blnEmpIdChange = True
                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;'>" & drow.Item("EmployeeCode") & "</TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;'>" & drow.Item("EmployeeName") & "</TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%'></TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%'></TD>" & vbCrLf)
                End If
                If intAssessmode <> CInt(drow("assessmodeid")) OrElse blnEmpIdChange Then
                    intAssessmode = drow("assessmodeid")
                    blnEmpIdChange = False
                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;'>" & drow.Item("Assess_Mode") & "</TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%'></TD>" & vbCrLf)
                End If


                For Each dcol As DataColumn In objDataReader.Columns
                    Dim dc As DataColumn = dcol
                    Select Case dcol.Caption.ToUpper
                        Case "EMPLOYEEUNKID", "ASSESSMODEID", "EMPLOYEECODE", "EMPLOYEENAME", "ASSESS_MODE", "ID", "GNAME"
                            strBuilder.Append("")
                        Case Else
                            Dim intType As Integer = drItem.Cast(Of DataRow).Where(Function(x) x.Item("custom_item").ToString = dc.ColumnName.ToString).Select(Function(x) CInt(x.Item("itemtypeid"))).First
                            If intType = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                                'Pinkal (08-Feb-2019) -- Start
                                'Bug [Support ID : 3483   Error message when trying to export custom item value report.] - Solved  Bug when Target Date is blank it was giving error for CCK.
                                'strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;' >" & eZeeDate.convertDate(drow.Item(dcol.ColumnName).ToString) & "</TD>" & vbCrLf)
                                If drow.Item(dcol.ColumnName).ToString().Trim.Length > 0 Then
                                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;' >" & eZeeDate.convertDate(drow.Item(dcol.ColumnName).ToString) & "</TD>" & vbCrLf)
                                Else
                                    strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;' ></TD>" & vbCrLf)
                                End If
                                'Pinkal (08-Feb-2019) -- End
                            Else
                                strBuilder.Append("<TD ALIGN='LEFT' WIDTH='20%' style='font-size:12px;'>" & drow.Item(dcol.ColumnName) & "</TD>" & vbCrLf)
                            End If
                    End Select
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </FONT></BODY>" & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function


#End Region

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
#Region " Report Generation "

    Private Function GetFinancial_Year_Data(ByVal intCompanyId As Integer) As DataSet
        Try
            Dim dsList As New DataSet
            Dim StrQ As String = String.Empty
            Using objDo As New clsDataOperation
                StrQ = "SELECT TOP 2 " & _
                       "     financialyear_name AS D_Year " & _
                       "    ,CONVERT(NVARCHAR(8),start_date,112) AS S_Date " & _
                       "    ,CONVERT(NVARCHAR(8),end_date,112) AS E_Date " & _
                       "    ,isclosed " & _
                       "FROM hrmsConfiguration..cffinancial_year_tran " & _
                       "WHERE companyunkid = '" & intCompanyId & "' ORDER BY start_date DESC "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
            End Using
            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFinancial_Year_Data; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function GetEmployee_AssessorReviewerDetails(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal strDataBaseName As String, ByVal dtEmpAsOnDate As Date) As DataSet
        Dim dsList As New DataSet
        Dim objEvalMaster As New clsevaluation_analysis_master
        Try
            dsList = objEvalMaster.GetEmployee_AssessorReviewerDetails(intPeriodId, strDataBaseName, Nothing, intEmployeeId)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_AssessorReviewerDetails; Module Name: " & mstrModuleName)
        Finally
            objEvalMaster = Nothing
        End Try
        Return dsList
    End Function

    Private Function GetCustomItemResult(ByVal intPeriodId As Integer, ByVal intEmployeeId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation

            StrQ = "SELECT " & _
                   "     customitemunkid AS Cid " & _
                   "    ,custom_value AS cvalue " & _
                   "    ,hrcompetency_customitem_tran.analysisunkid " & _
                   "    ,assessmodeid AS ModeId " & _
                   "    ,hrcompetency_customitem_tran.employeeunkid " & _
                   "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Col_FullEmpName " & _
                   "    ,SUM(1)OVER(PARTITION BY customitemunkid) AS xCnt " & _
                   "FROM hrcompetency_customitem_tran " & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hrcompetency_customitem_tran.employeeunkid " & _
                   "    JOIN hrevaluation_analysis_master ON hrcompetency_customitem_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                   "WHERE hrcompetency_customitem_tran.isvoid = 0 AND hrcompetency_customitem_tran.periodunkid = '" & intPeriodId & "' "
            If intEmployeeId > 0 Then
                StrQ &= " AND hrcompetency_customitem_tran.employeeunkid = '" & intEmployeeId & "' "
            End If
            StrQ &= "UNION ALL " & _
                    "SELECT " & _
                    "     customitemunkid AS Cid " & _
                    "    ,custom_value AS cvalue " & _
                    "    ,0 AS analysisunkid " & _
                    "    ,0 AS ModeId " & _
                    "    ,hrassess_plan_customitem_tran.employeeunkid " & _
                    "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Col_FullEmpName " & _
                    "    ,SUM(1)OVER(PARTITION BY customitemunkid) AS xCnt " & _
                    "FROM hrassess_plan_customitem_tran " & _
                    "    JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_plan_customitem_tran.employeeunkid " & _
                    "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_plan_customitem_tran.customheaderunkid " & _
                    "WHERE hrassess_plan_customitem_tran.isvoid = 0 AND hrassess_custom_headers.isinclude_planning = 1 " & _
                    "    AND hrassess_plan_customitem_tran.periodunkid = '" & intPeriodId & "' "

            'S.SANDEEP [11-OCT-2018] -- START
            'If intEmployeeId > 0 Then
            '    StrQ &= " AND hrcompetency_customitem_tran.employeeunkid = '" & intEmployeeId & "' "
            'End If

            If intEmployeeId > 0 Then
                StrQ &= " AND hrassess_plan_customitem_tran.employeeunkid = '" & intEmployeeId & "' "
            End If
            'S.SANDEEP [11-OCT-2018] -- END

            StrQ &= "    AND hrassess_plan_customitem_tran.employeeunkid NOT IN " & _
                    "(SELECT employeeunkid FROM hrcompetency_customitem_tran WHERE hrcompetency_customitem_tran.isvoid = 0 AND hrcompetency_customitem_tran.periodunkid = '" & intPeriodId & "') "

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function GetCustomItemDataList(ByVal intPeriodId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation
            StrQ = "SELECT " & _
                   "     @CustomSectionTitles AS Col_CustomItemCategory_Caption " & _
                   "    ,name AS Col_CustomItemCategory_Value " & _
                   "    ,custom_item " & _
                   "    ,customitemunkid " & _
                   "    ,hrassess_custom_items.customheaderunkid " & _
                   "    ,itemtypeid " & _
                   "    ,selectionmodeid " & _
                   "    ,ROW_NUMBER()OVER(PARTITION BY hrassess_custom_items.customheaderunkid ORDER BY hrassess_custom_headers.customheaderunkid) AS xFc " & _
                   "FROM hrassess_custom_items " & _
                   "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                   "WHERE hrassess_custom_headers.isactive = 1 AND hrassess_custom_items.isactive = 1 " & _
                   "AND hrassess_custom_items.periodunkid = '" & intPeriodId & "' "

            objDo.AddParameter("@CustomSectionTitles", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Custom Section Titles"))

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsResult As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsPerformanceAssessment
        Try
            Dim objDataOperation As New clsDataOperation
            rpt_Data = New ArutiReport.Designer.dsPerformanceAssessment

            '******************************** EMPLOYEE ASSESSOR/REVIEWER ********************************* START
            dsResult = GetEmployee_AssessorReviewerDetails(mintEmployeeId, mintPeriodId, strDatabaseName, dtPeriodEnd)
            '******************************** EMPLOYEE ASSESSOR/REVIEWER ********************************* END

            '******************************** COMPANY DATA *************************** STRAT
            Dim rpt_CRow As DataRow = rpt_Data.Tables("CompanyData").NewRow
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Dim StrCompanyName As String = objCompany._Name
            'S.SANDEEP |25-OCT-2019| -- END

            rpt_CRow.Item("Col_CompanyCode") = objCompany._Code
            rpt_CRow.Item("Col_CompanyName") = objCompany._Name
            rpt_CRow.Item("Col_CompanyAddress") = objCompany._Address1 & " " & objCompany._Address2
            rpt_CRow.Item("Col_CompanyCountry") = objCompany._Country_Name
            rpt_CRow.Item("Col_CompanyState") = objCompany._State_Name
            rpt_CRow.Item("Col_CompanyCity") = objCompany._City_Name
            rpt_CRow.Item("Col_CompanyTel1") = objCompany._Phone1
            rpt_CRow.Item("Col_CompanyTel2") = objCompany._Phone2
            rpt_CRow.Item("Col_CompanyTel3") = objCompany._Phone3
            rpt_CRow.Item("Col_CompanyFax") = objCompany._Fax
            rpt_CRow.Item("Col_CompanyEmail") = objCompany._Email
            rpt_CRow.Item("Col_CompanyWebsite") = objCompany._Website
            rpt_CRow.Item("Col_CompanyRegNo") = objCompany._Registerdno
            rpt_CRow.Item("Col_CompanyTINNo") = objCompany._Tinno
            rpt_CRow.Item("Col_CompanyVRNNo") = objCompany._Vatno
            rpt_CRow.Item("Col_PeriodName") = mstrPeriodName
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(strDatabaseName) = mintPeriodId
            rpt_CRow.Item("Col_PeriodStartDate") = objPrd._Start_Date.ToShortDateString
            rpt_CRow.Item("Col_PeriodEndDate") = objPrd._End_Date.ToShortDateString
            objPrd = Nothing
            Dim objImg(0) As Object
            If objCompany._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(objCompany._Image)
            Else
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            rpt_CRow.Item("Col_CompanyLogo") = objImg(0)
            dsList = GetFinancial_Year_Data(intCompanyUnkid)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim tmprow() As DataRow = dsList.Tables(0).Select("isclosed = 0")
                If tmprow.Length > 0 Then
                    rpt_CRow.Item("Col_CurrentFinancial_Year") = tmprow(0)("D_Year")
                Else
                    rpt_CRow.Item("Col_CurrentFinancial_Year") = ""
                End If

                tmprow = dsList.Tables(0).Select("isclosed = 1")
                If tmprow.Length > 0 Then
                    rpt_CRow.Item("Col_PreviousFinancial_Year") = tmprow(0)("D_Year")
                Else
                    rpt_CRow.Item("Col_PreviousFinancial_Year") = ""
                End If
            End If
            objCompany = Nothing
            rpt_Data.Tables("CompanyData").Rows.Add(rpt_CRow)
            '******************************** COMPANY DATA *************************** END

            '******************************** EMPLOYEE DATA *************************** STRAT

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnAddUserAccessLevel = True Then Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnAddUserAccessLevel = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            If mblnAddUserAccessLevel = True Then Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT " & _
                         "ISNULL(ttype.name,'') AS Col_Title " & _
                        ",ISNULL(hremployee_master.employeecode,'') AS Col_EmpCode " & _
                        ",ISNULL(hremployee_master.firstname,'') AS Col_EmpFirstName " & _
                        ",ISNULL(hremployee_master.surname,'') AS Col_EmpSurName " & _
                        ",ISNULL(hremployee_master.othername,'') AS Col_OtherName " & _
                        ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Col_FullEmpName " & _
                        ",ISNULL(hremployee_master.displayname,'') AS Col_DisplayName " & _
                        ",ISNULL(hremployee_master.email,'') AS Col_Email " & _
                        ",CASE WHEN gender = 1 THEN '@Male' WHEN gender = 2 THEN '@Female' ELSE '' END AS Col_Gender " & _
                        ",ISNULL(cfcommon_master.name,'') AS Col_EmployementType " & _
                        ",ISNULL(hrstation_master.name,'') AS Col_Branch " & _
                        ",ISNULL(hrdepartment_group_master.name,'') AS Col_DepartmentGroup " & _
                        ",ISNULL(hrdepartment_master.name,'') AS Col_Department " & _
                        ",ISNULL(hrsectiongroup_master.name, '') AS Col_SectionGroup " & _
                        ",ISNULL(hrsection_master.name,'') AS Col_Section " & _
                        ",ISNULL(hrunitgroup_master.name, '') AS Col_UnitGroup " & _
                        ",ISNULL(hrunit_master.name,'') AS Col_Unit " & _
                        ",ISNULL(hrteam_master.name,'') AS Col_Team " & _
                        ",ISNULL(hrclassgroup_master.name,'') AS Col_ClassGroup " & _
                        ",ISNULL(hrclasses_master.name,'') AS Col_Class " & _
                        ",ISNULL(hrjobgroup_master.name,'') AS Col_JobGroup " & _
                        ",ISNULL(hrjob_master.job_name,'') AS Col_Job " & _
                        ",ISNULL(prcostcenter_master.costcentername,'') AS Col_CostCenter " & _
                        ",ISNULL(Sft.ShiftName,'') AS Col_ShiftName " & _
                        ",ISNULL(CGrades.CurrentGradeGroup,'') AS Col_CurrentGradeGroup " & _
                        ",ISNULL(CGrades.CurrentGrade,'') AS Col_CurrentGrade " & _
                        ",ISNULL(CGrades.CurrentGradeLevel,'') AS Col_CurrentGradeLevel " & _
                        ",ISNULL(CGrades.CurrentSalary,0) AS Col_CurrentSalary " & _
                        ",ISNULL(NGrades.NextGradeGroup,'') AS Col_NextGradeGroup " & _
                        ",ISNULL(NGrades.NextGrade,'') AS Col_NextGrade " & _
                        ",ISNULL(NGrades.NextGradeLevel,'') AS Col_NextGradeLevel " & _
                        ",ISNULL(NGrades.NextSalary,0) AS Col_NextSalary " & _
                        ",ISNULL(nct.country_name,'') AS Col_Nationality " & _
                        ",ISNULL(mtype.name,'') AS Col_MaritalStatus " & _
                        ",ISNULL(RepTable.Reporting_Code,'') AS Col_ReportingCode " & _
                        ",ISNULL(RepTable.Reporting_Name, '') AS Col_FullReportingName " & _
                        ",ISNULL(RepTable.Reporting_Job, '') AS Col_ReportingJob " & _
                        ",ISNULL(RepTable.rfname,'') AS Col_ReportingFirstName " & _
                        ",ISNULL(RepTable.roname,'') AS Col_ReportingOtherName " & _
                        ",ISNULL(RepTable.rsname,'') AS Col_ReportingSurName " & _
                        ",ISNULL(CONVERT(CHAR(8),hremployee_master.firstappointment_date,112),'') AS Col_FirstAppointmentDate " & _
                        ",ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS Col_AppointmentDate " & _
                        ",ISNULL(CONVERT(CHAR(8),ECNF.confirmation_date,112),'') AS Col_ConfirmationDate " & _
                        ",ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS Col_BirthDate " & _
                        ",ISNULL(CONVERT(CHAR(8),ESUSP.suspended_from_date,112),'') AS Col_SuspendedFromDate " & _
                        ",ISNULL(CONVERT(CHAR(8),ESUSP.suspended_to_date,112),'') AS Col_SuspendedToDate " & _
                        ",ISNULL(CONVERT(CHAR(8),EPROB.probation_from_date,112),'') AS Col_ProbationFromDate " & _
                        ",ISNULL(CONVERT(CHAR(8),EPROB.probation_to_date, 112),'') AS Col_ProbationToDate " & _
                        ",ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate , 112),'') AS Col_EndOfContract_Date " & _
                        ",ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date,112),'') AS Col_LeavingDate " & _
                        ",ISNULL(CONVERT(CHAR(8),ERET.termination_to_date,112),'') AS Col_RetirementDate " & _
                        ",ISNULL(CONVERT(CHAR(8),ERH.reinstatment_date , 112),'') AS Col_Reinstatement_Date " & _
                        ",ISNULL(present_address1,'') AS Col_PresentAddress1 " & _
                        ",ISNULL(present_address2,'') AS Col_PresentAddress2 " & _
                        ",ISNULL(pct.country_name,'') AS Col_PresentCountry " & _
                        ",ISNULL(pstat.name,'') AS Col_PresentState " & _
                        ",ISNULL(pcity.name,'') AS Col_PresentCity " & _
                        ",ISNULL(YOCP.YearsOnCurrentPosition,0) AS YearsOnCurrentPosition " & _
                        ",ISNULL(hrjob_master.desciription,'') AS Col_JobPurpose " & _
                        ",ISNULL(Col_AssessItem_Commit_Date,'') AS Col_AssessItem_Commit_Date " & _
                        ",ISNULL(Col_Employee_Assessment_Date,'') AS Col_Employee_Assessment_Date " & _
                        ",ISNULL(Col_Employee_Committed_Date,'') AS Col_Employee_Committed_Date " & _
                        ",ISNULL(Col_Assessor_Assessment_Date,'') AS Col_Assessor_Assessment_Date " & _
                        ",ISNULL(Col_Assessor_Committed_Date,'') AS Col_Assessor_Committed_Date " & _
                        ",ISNULL(Col_Reviewer_Assessment_Date,'') AS Col_Reviewer_Assessment_Date " & _
                        ",ISNULL(Col_Reviewer_Committed_Date,'') AS Col_Reviewer_Committed_Date " & _
                        ",ISNULL(Qulification.qualificationname,'') AS Col_Qualification " & _
                        ",ISNULL(Qulification.QStartDate,'') AS Col_Qualification_StartDate " & _
                        ",ISNULL(Qulification.QEndDate,'') AS Col_Qualification_EndDate " & _
                        ",ISNULL(Qulification.QStartYear,'') AS Col_Qualification_StartYear " & _
                        ",ISNULL(Qulification.QEndYear,'') AS Col_Qualification_EndYear " & _
                        ",ISNULL(Qulification.QLevel,'') AS Col_Qualification_GroupLevel " & _
                        ",ISNULL(OtherQulification.other_qualificationgrp,'') AS Col_Other_Qualification_Group " & _
                        ",ISNULL(OtherQulification.other_qualification,'') AS Col_Other_Qualification " & _
                        ",ISNULL(OtherQulification.other_resultcode,'') AS Col_Other_Qualification_ResultCode " & _
                        ",ISNULL(OtherQulification.QStartDate,'') AS Col_Other_Qualification_StartDate " & _
                        ",ISNULL(OtherQulification.QEndDate,'') AS Col_Other_Qualification_EndDate " & _
                        ",ISNULL(OtherQulification.QStartYear,'') AS Col_Other_Qualification_StartYear " & _
                        ",ISNULL(OtherQulification.QEndYear,'') AS Col_Other_Qualification_EndYear " & _
                        ",ISNULL(CGrades.incrementdate,'') AS Col_LastDateOfPromotion " & _
                        ",ISNULL(empTraning.Traning_Name,'') AS Col_Traning_Attended " & _
                        ",ISNULL(empTraning.enroll_date,'') AS Col_Traning_Enrollment_Date " & _
                        ",ISNULL(empTraning.start_date,'') AS Col_Traning_Start_Date " & _
                        ",ISNULL(empTraning.end_date,'') AS Col_Traning_End_Date " & _
                        ",eimg.photo AS Col_Employee_Image " & _
                        ",empsignature AS Col_Employee_Signature " & _
                        ",ISNULL(teffdate,'') AS Col_Transfer_EffectiveDate " & _
                        ",ISNULL(reffdate,'') AS Col_Recategorization_EffectiveDate " & _
                        ",ISNULL(ceffdate,'') AS Col_Costcenter_EffectiveDate " & _
                        ",'' AS Col_WorkPermit_EffectiveDate " & _
                        ",'' AS Col_Resident_EffectiveDate " & _
                        ",REmp_Signature AS Col_Reporting_Signature " & _
                    "FROM hremployee_master " & _
                    "JOIN " & _
                    "( " & _
                         "SELECT DISTINCT " & _
                              "CASE WHEN selfemployeeunkid <=0 THEN assessedemployeeunkid ELSE selfemployeeunkid END AS aempid " & _
                         "FROM hrevaluation_analysis_master " & _
                         "WHERE isvoid = 0 AND periodunkid = '" & mintPeriodId & "' " & _
                         "UNION " & _
                         "SELECT DISTINCT employeeunkid AS aempid FROM hrassess_plan_customitem_tran WHERE isvoid = 0 " & _
                         "AND periodunkid = '" & mintPeriodId & "' " & _
                    ") AS EV ON EV.aempid = hremployee_master.employeeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= "LEFT JOIN arutiimages..hremployee_images AS eimg ON eimg.employeeunkid = hremployee_master.employeeunkid AND eimg.companyunkid = hremployee_master.companyunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "stationunkid " & _
                            ",deptgroupunkid " & _
                            ",departmentunkid " & _
                            ",sectiongroupunkid " & _
                            ",sectionunkid " & _
                            ",unitgroupunkid " & _
                            ",unitunkid " & _
                            ",teamunkid " & _
                            ",classgroupunkid " & _
                            ",classunkid " & _
                            ",employeeunkid " & _
                            ",ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_transfer_tran " & _
                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "jobunkid " & _
                            ",jobgroupunkid " & _
                            ",employeeunkid " & _
                            ",ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_categorization_tran " & _
                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid " & _
                    "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid " & _
                    "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                    "LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                    "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid " & _
                    "LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid " & _
                    "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid " & _
                    "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid " & _
                    "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid " & _
                    "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                    "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = Jobs.jobgroupunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "cctranheadvalueid AS costcenterunkid " & _
                            ",employeeunkid " & _
                            ",ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_cctranhead_tran " & _
                        "WHERE istransactionhead = 0 AND isvoid = 0 " & _
                        "AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                             "E_Emp.employeeunkid AS employeeunkid " & _
                            ",ISNULL(R_Emp.employeecode, '') AS Reporting_Code " & _
                            ",ISNULL(R_Emp.firstname, '') + ' ' + ISNULL(R_Emp.othername, '') + ' ' + ISNULL(R_Emp.surname, '') AS Reporting_Name " & _
                            ",ISNULL(R_Emp.firstname, '') AS rfname " & _
                            ",ISNULL(R_Emp.othername, '') AS roname " & _
                            ",ISNULL(R_Emp.surname, '') AS rsname " & _
                            ",ISNULL(Jb.job_name, '') AS Reporting_Job " & _
                            ",CASE WHEN hremployee_reportto.ishierarchy = 1 THEN 1 ELSE 0 END AS Default_Reporting " & _
                            ",R_Emp.empsignature AS REmp_Signature " & _
                        "FROM hremployee_reportto " & _
                            "LEFT JOIN hremployee_master AS E_Emp ON hremployee_reportto.employeeunkid = E_Emp.employeeunkid " & _
                            "LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                                "SELECT " & _
                                        "jobunkid " & _
                                    ",jobgroupunkid " & _
                                    ",employeeunkid " & _
                                    ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "FROM hremployee_categorization_tran " & _
                                "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS RJobs ON RJobs.employeeunkid = R_Emp.employeeunkid AND RJobs.rno = 1 " & _
                            "LEFT JOIN hrjob_master AS Jb ON Jb.jobunkid = RJobs.jobunkid " & _
                        "WHERE ishierarchy = 1 AND hremployee_reportto.isvoid = 0 " & _
                    ") AS RepTable ON RepTable.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                    "SUSP.SDEmpId " & _
                                    ",SUSP.suspended_from_date " & _
                                    ",SUSP.suspended_to_date " & _
                                    ",SUSP.SDEfDt " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                    "SDT.employeeunkid AS SDEmpId " & _
                                ",CONVERT(CHAR(8),SDT.date1,112) AS suspended_from_date " & _
                                ",CONVERT(CHAR(8),SDT.date2,112) AS suspended_to_date " & _
                                ",CONVERT(CHAR(8),SDT.effectivedate,112) AS SDEfDt " & _
                                ",ROW_NUMBER()OVER(PARTITION BY SDT.employeeunkid ORDER BY SDT.effectivedate DESC) AS Rno " & _
                            "FROM hremployee_dates_tran AS SDT " & _
                            "WHERE isvoid = 0 AND SDT.datetypeunkid = '3' AND CONVERT(CHAR(8),SDT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS SUSP WHERE SUSP.Rno = 1 " & _
                    ") AS ESUSP ON ESUSP.SDEmpId = hremployee_master.employeeunkid AND ESUSP.SDEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                    "PROB.PDEmpId " & _
                                    ",PROB.probation_from_date " & _
                                    ",PROB.probation_to_date " & _
                                    ",PROB.PDEfDt " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                    "PDT.employeeunkid AS PDEmpId " & _
                                ",CONVERT(CHAR(8),PDT.date1,112) AS probation_from_date " & _
                                ",CONVERT(CHAR(8),PDT.date2,112) AS probation_to_date " & _
                                ",CONVERT(CHAR(8),PDT.effectivedate,112) AS PDEfDt " & _
                                ",ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                            "FROM hremployee_dates_tran AS PDT " & _
                            "WHERE isvoid = 0 AND PDT.datetypeunkid = '1' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS PROB WHERE PROB.Rno = 1 " & _
                    ") AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid AND EPROB.PDEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                    "TERM.TEEmpId " & _
                                    ",TERM.empl_enddate " & _
                                    ",TERM.termination_from_date " & _
                                    ",TERM.TEfDt " & _
                                    ",TERM.isexclude_payroll " & _
                                    ",TERM.changereasonunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                    "TRM.employeeunkid AS TEEmpId " & _
                                ",CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                                ",CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                                ",CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                                ",TRM.isexclude_payroll " & _
                                ",ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                ",TRM.changereasonunkid " & _
                            "FROM hremployee_dates_tran AS TRM " & _
                            "WHERE isvoid = 0 AND TRM.datetypeunkid = '4' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "CNF.CEmpId " & _
                                ",CNF.confirmation_date " & _
                            ",CNF.CEfDt " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                    "CNF.employeeunkid AS CEmpId " & _
                                ",CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                                ",CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                                ",ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                                ",CNF.changereasonunkid " & _
                            "FROM hremployee_dates_tran AS CNF " & _
                            "WHERE isvoid = 0 AND CNF.datetypeunkid = '2' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS CNF WHERE CNF.Rno = 1 " & _
                    ") AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "RET.REmpId " & _
                                ",RET.termination_to_date " & _
                                ",RET.REfDt " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                    "RTD.employeeunkid AS REmpId " & _
                                ",CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                                ",CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                                ",ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                            "FROM hremployee_dates_tran AS RTD " & _
                            "WHERE isvoid = 0 AND RTD.datetypeunkid = '6' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "RH.RHEmpId " & _
                            ",RH.reinstatment_date " & _
                            ",RH.RHEfDt " & _
                            ",RH.changereasonunkid AS RH_changereasonunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                    "ERT.employeeunkid AS RHEmpId " & _
                                ",CONVERT(CHAR(8),ERT.reinstatment_date,112) AS reinstatment_date " & _
                                ",CONVERT(CHAR(8),ERT.effectivedate,112) AS RHEfDt " & _
                                ",ROW_NUMBER()OVER(PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " & _
                                ",ERT.changereasonunkid " & _
                            "FROM hremployee_rehire_tran AS ERT " & _
                            "WHERE isvoid = 0 AND CONVERT(CHAR(8),ERT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS RH WHERE RH.Rno = 1 " & _
                    ")AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid AND ERH.RHEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "employeeunkid " & _
                            ",ISNULL(hrgradegroup_master.name,'') AS CurrentGradeGroup " & _
                            ",ISNULL(hrgrade_master.name,'') AS CurrentGrade " & _
                            ",ISNULL(hrgradelevel_master.name,'') AS CurrentGradeLevel " & _
                            ",newscale AS CurrentSalary " & _
                            ",CONVERT(CHAR(8),incrementdate,112) AS incrementdate " & _
                            ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC) AS rno " & _
                        "FROM prsalaryincrement_tran " & _
                            "JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                            "JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
                            "JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                        "WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS CGrades ON CGrades.employeeunkid = hremployee_master.employeeunkid AND CGrades.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "employeeunkid " & _
                            ",ISNULL(hrgradegroup_master.name,'') AS NextGradeGroup " & _
                            ",ISNULL(hrgrade_master.name,'') AS NextGrade " & _
                            ",ISNULL(hrgradelevel_master.name,'') AS NextGradeLevel " & _
                            ",newscale AS NextSalary " & _
                            ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY incrementdate ASC) AS rno " & _
                        "FROM prsalaryincrement_tran " & _
                            "JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                            "JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
                            "JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                        "WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) > '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS NGrades ON NGrades.employeeunkid = hremployee_master.employeeunkid AND NGrades.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "employeeunkid " & _
                            ",shiftname AS ShiftName " & _
                            ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as rno " & _
                        "FROM hremployee_shift_tran " & _
                            "JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS Sft ON Sft.employeeunkid = hremployee_master.employeeunkid AND Sft.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "DATEDIFF(YEAR,effectivedate,GETDATE()) AS YearsOnCurrentPosition " & _
                            ",employeeunkid AS EmpId " & _
                            ",jobunkid AS Job_ID " & _
                            ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "FROM hremployee_categorization_tran " & _
                        "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS YOCP ON YOCP.EmpId = hremployee_master.employeeunkid AND YOCP.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                             "ISNULL(CONVERT(NVARCHAR(8),assessmentdate,112),'') AS Col_Employee_Assessment_Date " & _
                            ",ISNULL(CONVERT(NVARCHAR(8),committeddatetime,112),'') AS Col_Employee_Committed_Date " & _
                            ",Col_Assessor_Assessment_Date " & _
                            ",Col_Assessor_Committed_Date " & _
                            ",Col_Reviewer_Assessment_Date " & _
                            ",Col_Reviewer_Committed_Date " & _
                            ",selfemployeeunkid " & _
                        "FROM hrevaluation_analysis_master " & _
                        "LEFT JOIN " & _
                        "( " & _
                            "SELECT " & _
                                    "ISNULL(CONVERT(NVARCHAR(8),assessmentdate,112),'') AS Col_Assessor_Assessment_Date " & _
                                ",ISNULL(CONVERT(NVARCHAR(8),committeddatetime,112),'') AS Col_Assessor_Committed_Date " & _
                                ",assessedemployeeunkid " & _
                            "FROM hrevaluation_analysis_master WHERE assessmodeid = 2 AND isvoid = 0 " & _
                            "AND periodunkid = '" & mintPeriodId & "' " & _
                        ") AS Ar ON Ar.assessedemployeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                            "SELECT " & _
                                    "ISNULL(CONVERT(NVARCHAR(8),assessmentdate,112),'') AS Col_Reviewer_Assessment_Date " & _
                                ",ISNULL(CONVERT(NVARCHAR(8),committeddatetime,112),'') AS Col_Reviewer_Committed_Date " & _
                                ",assessedemployeeunkid " & _
                            "FROM hrevaluation_analysis_master WHERE assessmodeid = 3 AND isvoid = 0 " & _
                            "AND periodunkid = '" & mintPeriodId & "' " & _
                        ") AS Rr ON Ar.assessedemployeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
                        "WHERE isvoid = 0 AND periodunkid = '" & mintPeriodId & "' " & _
                    ") AS EDt ON EDt.selfemployeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                                "ISNULL(CONVERT(NVARCHAR(8),status_date,112),'') AS Col_AssessItem_Commit_Date " & _
                            ",hrassess_empstatus_tran.employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY hrassess_empstatus_tran.employeeunkid ORDER BY status_date DESC) AS rno " & _
                        "FROM hrassess_empstatus_tran " & _
                        "WHERE periodunkid = '" & mintPeriodId & "' AND statustypeid = '2' " & _
                    ") AS Es ON Es.employeeunkid = hremployee_master.employeeunkid AND Es.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "hrqualification_master.qualificationname " & _
                            ",hrqualification_master.qualificationcode " & _
                            ",CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS QStartDate " & _
                            ",CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS QEndDate " & _
                            ",YEAR(hremp_qualification_tran.award_start_date) AS QStartYear " & _
                            ",YEAR(hremp_qualification_tran.award_end_date) AS QEndYear " & _
                            ",cfcommon_master.qlevel AS QLevel " & _
                            ",hremp_qualification_tran.employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY hremp_qualification_tran.employeeunkid ORDER BY cfcommon_master.qlevel DESC,ISNULL(hremp_qualification_tran.award_start_date,GETDATE()) DESC) AS rno " & _
                        "FROM hremp_qualification_tran " & _
                            "JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                            "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                        "WHERE hremp_qualification_tran.isvoid = 0 " & _
                            "AND hremp_qualification_tran.qualificationgroupunkid <> 0 AND hremp_qualification_tran.qualificationunkid <> 0 " & _
                    ") AS Qulification ON Qulification.employeeunkid = hremployee_master.employeeunkid AND Qulification.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "hremp_qualification_tran.other_qualificationgrp " & _
                            ",hremp_qualification_tran.other_qualification " & _
                            ",hremp_qualification_tran.other_resultcode " & _
                            ",CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS QStartDate " & _
                            ",CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS QEndDate " & _
                            ",YEAR(hremp_qualification_tran.award_start_date) AS QStartYear " & _
                            ",YEAR(hremp_qualification_tran.award_end_date) AS QEndYear " & _
                            ",hremp_qualification_tran.employeeunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY hremp_qualification_tran.employeeunkid ORDER BY hremp_qualification_tran.transaction_date DESC) AS rno " & _
                        "FROM hremp_qualification_tran " & _
                        "WHERE hremp_qualification_tran.isvoid = 0 " & _
                            "AND hremp_qualification_tran.qualificationgroupunkid = 0 AND hremp_qualification_tran.qualificationunkid = 0 " & _
                    ") AS OtherQulification ON OtherQulification.employeeunkid = hremployee_master.employeeunkid AND OtherQulification.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                            "cfcommon_master.name AS Traning_Name " & _
                            ",CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112) AS enroll_date " & _
                            ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS start_date " & _
                            ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS end_date " & _
                            ",hrtraining_enrollment_tran.employeeunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY hrtraining_enrollment_tran.employeeunkid  ORDER BY hrtraining_enrollment_tran.enroll_date DESC) AS rno " & _
                        "FROM hrtraining_enrollment_tran " & _
                            "JOIN hrtraining_scheduling ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
                            "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND cfcommon_master.mastertype = 30 " & _
                        "WHERE hrtraining_enrollment_tran.isvoid = 0 AND hrtraining_enrollment_tran.status_id = 3 " & _
                                "AND cfcommon_master.coursetypeid = 1 " & _
                    ") AS empTraning ON empTraning.employeeunkid = hremployee_master.employeeunkid  AND empTraning.rno = 1 " & _
                    "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.costcenterunkid " & _
                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.employmenttypeunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' " & _
                    "LEFT JOIN cfcommon_master AS ttype ON ttype.masterunkid = hremployee_master.titleunkid AND ttype.mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' " & _
                    "LEFT JOIN hrmsConfiguration..cfcountry_master AS nct ON nct.countryunkid = hremployee_master.nationalityunkid " & _
                    "LEFT JOIN cfcommon_master AS mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' " & _
                    "LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "LEFT JOIN hrmsConfiguration..cfcity_master AS pcity ON pcity.cityunkid = hremployee_master.present_post_townunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfstate_master AS pstat ON pstat.stateunkid = hremployee_master.present_stateunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfcountry_master AS pct ON pct.countryunkid = hremployee_master.present_countryunkid " & _
                    "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If


            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each xRow As DataRow In dsList.Tables("List").Rows
                    Dim rpt_ERow As DataRow = rpt_Data.Tables("EmployeeData").NewRow
                    rpt_ERow.Item("Col_Title") = xRow.Item("Col_Title")
                    rpt_ERow.Item("Col_EmpCode") = xRow.Item("Col_EmpCode")
                    rpt_ERow.Item("Col_EmpFirstName") = xRow.Item("Col_EmpFirstName")
                    rpt_ERow.Item("Col_EmpSurName") = xRow.Item("Col_EmpSurName")
                    rpt_ERow.Item("Col_OtherName") = xRow.Item("Col_OtherName")
                    rpt_ERow.Item("Col_FullEmpName") = xRow.Item("Col_FullEmpName")
                    rpt_ERow.Item("Col_DisplayName") = xRow.Item("Col_DisplayName")
                    rpt_ERow.Item("Col_Email") = xRow.Item("Col_Email")
                    rpt_ERow.Item("Col_Gender") = xRow.Item("Col_Gender")
                    rpt_ERow.Item("Col_EmployementType") = xRow.Item("Col_EmployementType")
                    rpt_ERow.Item("Col_Branch") = xRow.Item("Col_Branch")
                    rpt_ERow.Item("Col_DepartmentGroup") = xRow.Item("Col_DepartmentGroup")
                    rpt_ERow.Item("Col_Department") = xRow.Item("Col_Department")
                    rpt_ERow.Item("Col_SectionGroup") = xRow.Item("Col_SectionGroup")
                    rpt_ERow.Item("Col_Section") = xRow.Item("Col_Section")
                    rpt_ERow.Item("Col_UnitGroup") = xRow.Item("Col_UnitGroup")
                    rpt_ERow.Item("Col_Unit") = xRow.Item("Col_Unit")
                    rpt_ERow.Item("Col_Team") = xRow.Item("Col_Team")
                    rpt_ERow.Item("Col_ClassGroup") = xRow.Item("Col_ClassGroup")
                    rpt_ERow.Item("Col_Class") = xRow.Item("Col_Class")
                    rpt_ERow.Item("Col_JobGroup") = xRow.Item("Col_JobGroup")
                    rpt_ERow.Item("Col_Job") = xRow.Item("Col_Job")
                    rpt_ERow.Item("Col_CostCenter") = xRow.Item("Col_CostCenter")
                    rpt_ERow.Item("Col_ShiftName") = xRow.Item("Col_ShiftName")
                    rpt_ERow.Item("Col_CurrentGrade") = xRow.Item("Col_CurrentGrade")
                    rpt_ERow.Item("Col_CurrentGradeGroup") = xRow.Item("Col_CurrentGradeGroup")
                    rpt_ERow.Item("Col_CurrentGradeLevel") = xRow.Item("Col_CurrentGradeLevel")
                    rpt_ERow.Item("Col_CurrentSalary") = Format(CDec(xRow.Item("Col_CurrentSalary")), GUI.fmtCurrency)
                    rpt_ERow.Item("Col_NextGrade") = xRow.Item("Col_NextGrade")
                    rpt_ERow.Item("Col_NextGradeGroup") = xRow.Item("Col_NextGradeGroup")
                    rpt_ERow.Item("Col_NextGradeLevel") = xRow.Item("Col_NextGradeLevel")
                    rpt_ERow.Item("Col_NextSalary") = Format(CDec(xRow.Item("Col_NextSalary")), GUI.fmtCurrency)
                    rpt_ERow.Item("Col_Nationality") = xRow.Item("Col_Nationality")
                    rpt_ERow.Item("Col_MaritalStatus") = xRow.Item("Col_MaritalStatus")
                    rpt_ERow.Item("Col_ReportingCode") = xRow.Item("Col_ReportingCode")
                    rpt_ERow.Item("Col_FullReportingName") = xRow.Item("Col_FullReportingName")
                    rpt_ERow.Item("Col_ReportingJob") = xRow.Item("Col_ReportingJob")
                    rpt_ERow.Item("Col_ReportingFirstName") = xRow.Item("Col_ReportingFirstName")
                    rpt_ERow.Item("Col_ReportingOtherName") = xRow.Item("Col_ReportingOtherName")
                    rpt_ERow.Item("Col_ReportingSurName") = xRow.Item("Col_ReportingSurName")
                    Dim dtmp() As DataRow = Nothing
                    dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        rpt_ERow.Item("Col_AssessorCode") = dtmp(0).Item("arCode")
                        rpt_ERow.Item("Col_AssessorName") = dtmp(0).Item("arName")
                        rpt_ERow.Item("Col_AssessorJob") = dtmp(0).Item("AR_Job")
                        rpt_ERow.Item("Col_AssessorDepartment") = dtmp(0).Item("AR_Department")
                    Else
                        dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                        If dtmp.Length > 0 Then
                            rpt_ERow.Item("Col_AssessorCode") = dtmp(0).Item("arCode")
                            rpt_ERow.Item("Col_AssessorName") = dtmp(0).Item("arName")
                            rpt_ERow.Item("Col_AssessorJob") = dtmp(0).Item("AR_Job")
                            rpt_ERow.Item("Col_AssessorDepartment") = dtmp(0).Item("AR_Department")
                        End If
                    End If
                    dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        rpt_ERow.Item("Col_ReviewerCode") = dtmp(0).Item("arCode")
                        rpt_ERow.Item("Col_ReviwerName") = dtmp(0).Item("arName")
                        rpt_ERow.Item("Col_ReviewerJob") = dtmp(0).Item("AR_Job")
                        rpt_ERow.Item("Col_ReviewerDepartment") = dtmp(0).Item("AR_Department")
                    Else
                        dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")
                        If dtmp.Length > 0 Then
                            rpt_ERow.Item("Col_ReviewerCode") = dtmp(0).Item("arCode")
                            rpt_ERow.Item("Col_ReviwerName") = dtmp(0).Item("arName")
                            rpt_ERow.Item("Col_ReviewerJob") = dtmp(0).Item("AR_Job")
                            rpt_ERow.Item("Col_ReviewerDepartment") = dtmp(0).Item("AR_Department")
                        End If
                    End If
                    rpt_ERow.Item("Col_Qualification") = xRow.Item("Col_Qualification")
                    If xRow.Item("Col_Qualification_StartDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Qualification_StartDate") = eZeeDate.convertDate(xRow.Item("Col_Qualification_StartDate"))
                    Else
                        rpt_ERow.Item("Col_Qualification_StartDate") = ""
                    End If

                    If xRow.Item("Col_Qualification_EndDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Qualification_EndDate") = eZeeDate.convertDate(xRow.Item("Col_Qualification_EndDate"))
                    Else
                        rpt_ERow.Item("Col_Qualification_EndDate") = ""
                    End If

                    rpt_ERow.Item("Col_Qualification_StartYear") = xRow.Item("Col_Qualification_StartYear")
                    rpt_ERow.Item("Col_Qualification_EndYear") = xRow.Item("Col_Qualification_EndYear")
                    rpt_ERow.Item("Col_Qualification_GroupLevel") = xRow.Item("Col_Qualification_GroupLevel")
                    rpt_ERow.Item("Col_Other_Qualification_Group") = xRow.Item("Col_Other_Qualification_Group")
                    rpt_ERow.Item("Col_Other_Qualification") = xRow.Item("Col_Other_Qualification")
                    rpt_ERow.Item("Col_Other_Qualification_ResultCode") = xRow.Item("Col_Other_Qualification_ResultCode")

                    If xRow.Item("Col_Other_Qualification_StartDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Other_Qualification_StartDate") = eZeeDate.convertDate(xRow.Item("Col_Other_Qualification_StartDate"))
                    Else
                        rpt_ERow.Item("Col_Other_Qualification_StartDate") = ""
                    End If
                    If xRow.Item("Col_Other_Qualification_EndDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Other_Qualification_EndDate") = eZeeDate.convertDate(xRow.Item("Col_Other_Qualification_EndDate"))
                    Else
                        rpt_ERow.Item("Col_Other_Qualification_EndDate") = ""
                    End If
                    rpt_ERow.Item("Col_Other_Qualification_StartYear") = xRow.Item("Col_Other_Qualification_StartYear")
                    rpt_ERow.Item("Col_Other_Qualification_EndYear") = xRow.Item("Col_Other_Qualification_EndYear")
                    'Shani(01-MAR-2016) -- End

                    'Shani(22-MAR-2016) -- Start
                    rpt_ERow.Item("Col_Traning_Attended") = xRow.Item("Col_Traning_Attended")
                    If xRow.Item("Col_LastDateOfPromotion").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_LastDateOfPromotion") = eZeeDate.convertDate(xRow.Item("Col_LastDateOfPromotion").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_LastDateOfPromotion") = ""
                    End If
                    If xRow.Item("Col_Traning_Enrollment_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Traning_Enrollment_Date") = eZeeDate.convertDate(xRow.Item("Col_Traning_Enrollment_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Traning_Enrollment_Date") = ""
                    End If
                    If xRow.Item("Col_Traning_Start_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Traning_Start_Date") = eZeeDate.convertDate(xRow.Item("Col_Traning_Start_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Traning_Start_Date") = ""
                    End If
                    If xRow.Item("Col_Traning_End_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Traning_End_Date") = eZeeDate.convertDate(xRow.Item("Col_Traning_End_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Traning_End_Date") = ""
                    End If
                    'Shani(22-MAR-2016) -- End

                    If xRow.Item("Col_FirstAppointmentDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_FirstAppointmentDate") = eZeeDate.convertDate(xRow.Item("Col_FirstAppointmentDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_FirstAppointmentDate") = ""
                    End If

                    If xRow.Item("Col_AppointmentDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_AppointmentDate") = eZeeDate.convertDate(xRow.Item("Col_AppointmentDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_AppointmentDate") = ""
                    End If

                    If xRow.Item("Col_ConfirmationDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_ConfirmationDate") = eZeeDate.convertDate(xRow.Item("Col_ConfirmationDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_ConfirmationDate") = ""
                    End If

                    If xRow.Item("Col_SuspendedFromDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_SuspendedFromDate") = eZeeDate.convertDate(xRow.Item("Col_SuspendedFromDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_SuspendedFromDate") = ""
                    End If

                    If xRow.Item("Col_SuspendedToDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_SuspendedToDate") = eZeeDate.convertDate(xRow.Item("Col_SuspendedToDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_SuspendedToDate") = ""
                    End If

                    If xRow.Item("Col_ProbationFromDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_ProbationFromDate") = eZeeDate.convertDate(xRow.Item("Col_ProbationFromDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_ProbationFromDate") = ""
                    End If

                    If xRow.Item("Col_ProbationToDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_ProbationToDate") = eZeeDate.convertDate(xRow.Item("Col_ProbationToDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_ProbationToDate") = ""
                    End If

                    If xRow.Item("Col_EndOfContract_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_EndOfContract_Date") = eZeeDate.convertDate(xRow.Item("Col_EndOfContract_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_EndOfContract_Date") = ""
                    End If

                    If xRow.Item("Col_LeavingDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_LeavingDate") = eZeeDate.convertDate(xRow.Item("Col_LeavingDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_LeavingDate") = ""
                    End If

                    If xRow.Item("Col_RetirementDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_RetirementDate") = eZeeDate.convertDate(xRow.Item("Col_RetirementDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_RetirementDate") = ""
                    End If

                    If xRow.Item("Col_Reinstatement_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Reinstatement_Date") = eZeeDate.convertDate(xRow.Item("Col_Reinstatement_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Reinstatement_Date") = ""
                    End If

                    If xRow.Item("Col_BirthDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_BirthDate") = eZeeDate.convertDate(xRow.Item("Col_BirthDate").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_BirthDate") = ""
                    End If

                    rpt_ERow.Item("Col_PresentAddress1") = xRow.Item("Col_PresentAddress1")
                    rpt_ERow.Item("Col_PresentAddress2") = xRow.Item("Col_PresentAddress2")
                    rpt_ERow.Item("Col_PresentCity") = xRow.Item("Col_PresentCity")
                    rpt_ERow.Item("Col_PresentCountry") = xRow.Item("Col_PresentCountry")
                    rpt_ERow.Item("Col_PresentState") = xRow.Item("Col_PresentState")
                    rpt_ERow.Item("Col_YearsOnCurrentPosition") = xRow.Item("YearsOnCurrentPosition")
                    If xRow.Item("Col_JobPurpose").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_JobPurpose") = xRow.Item("Col_JobPurpose")
                    End If
                    If xRow.Item("Col_AssessItem_Commit_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_AssessItem_Commit_Date") = eZeeDate.convertDate(xRow.Item("Col_AssessItem_Commit_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_AssessItem_Commit_Date") = ""
                    End If

                    If xRow.Item("Col_Employee_Assessment_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Employee_Assessment_Date") = eZeeDate.convertDate(xRow.Item("Col_Employee_Assessment_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Employee_Assessment_Date") = ""
                    End If

                    If xRow.Item("Col_Employee_Committed_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Employee_Committed_Date") = eZeeDate.convertDate(xRow.Item("Col_Employee_Committed_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Employee_Committed_Date") = ""
                    End If

                    If xRow.Item("Col_Assessor_Assessment_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Assessor_Assessment_Date") = eZeeDate.convertDate(xRow.Item("Col_Assessor_Assessment_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Assessor_Assessment_Date") = ""
                    End If

                    If xRow.Item("Col_Assessor_Committed_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Assessor_Committed_Date") = eZeeDate.convertDate(xRow.Item("Col_Assessor_Committed_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Assessor_Committed_Date") = ""
                    End If

                    If xRow.Item("Col_Reviewer_Assessment_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Reviewer_Assessment_Date") = eZeeDate.convertDate(xRow.Item("Col_Reviewer_Assessment_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Reviewer_Assessment_Date") = ""
                    End If

                    If xRow.Item("Col_Reviewer_Committed_Date").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Reviewer_Committed_Date") = eZeeDate.convertDate(xRow.Item("Col_Reviewer_Committed_Date").ToString).ToShortDateString
                    Else
                        rpt_ERow.Item("Col_Reviewer_Committed_Date") = ""
                    End If

                    Dim img As Image
                    Dim objEImg(0) As Object
                    If IsDBNull(xRow.Item("Col_Employee_Image")) = False Then
                        img = eZeeDataType.data2Image(xRow.Item("Col_Employee_Image"))
                        objEImg(0) = eZeeDataType.image2Data(img)
                    Else
                        Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                        objEImg(0) = eZeeDataType.image2Data(imgBlank)
                    End If
                    rpt_ERow.Item("Col_Employee_Image") = objEImg(0)

                    Dim objSImg(0) As Object
                    If IsDBNull(xRow.Item("Col_Employee_Signature")) = False Then
                        'S.SANDEEP |06-JUL-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD)
                        'img = eZeeDataType.data2Image(xRow.Item("Col_Employee_Signature"))
                        Dim mbytEmpSignature As Byte() = Nothing
                        Try
                            mbytEmpSignature = clsSecurity.DecryptByte(CType(xRow.Item("Col_Employee_Signature"), Byte()), "ezee")
                        Catch ex As Exception
                            mbytEmpSignature = CType(xRow.Item("Col_Employee_Signature"), Byte())
                        End Try
                        img = eZeeDataType.data2Image(mbytEmpSignature)
                        'S.SANDEEP |06-JUL-2020| -- END                        
                        objSImg(0) = eZeeDataType.image2Data(img)
                    Else
                        Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                        objSImg(0) = eZeeDataType.image2Data(imgBlank)
                    End If
                    rpt_ERow.Item("Col_Employee_Signature") = objSImg(0)


                    Dim objRImg(0) As Object
                    If IsDBNull(xRow.Item("Col_Reporting_Signature")) = False Then
                        'S.SANDEEP |06-JUL-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD)
                        'img = eZeeDataType.data2Image(xRow.Item("Col_Reporting_Signature"))
                        Dim mbytEmpSignature As Byte() = Nothing
                        Try
                            mbytEmpSignature = clsSecurity.DecryptByte(CType(xRow.Item("Col_Reporting_Signature"), Byte()), "ezee")
                        Catch ex As Exception
                            mbytEmpSignature = CType(xRow.Item("Col_Reporting_Signature"), Byte())
                        End Try
                        img = eZeeDataType.data2Image(mbytEmpSignature)
                        'S.SANDEEP |06-JUL-2020| -- END                                        
                        objRImg(0) = eZeeDataType.image2Data(img)
                    Else
                        Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                        objRImg(0) = eZeeDataType.image2Data(imgBlank)
                    End If
                    rpt_ERow.Item("Col_Reporting_Signature") = objRImg(0)



                    If xRow.Item("Col_Transfer_EffectiveDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Transfer_EffectiveDate") = eZeeDate.convertDate(xRow.Item("Col_Transfer_EffectiveDate").ToString.Trim).ToShortDateString()
                    End If

                    If xRow.Item("Col_Recategorization_EffectiveDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Recategorization_EffectiveDate") = eZeeDate.convertDate(xRow.Item("Col_Recategorization_EffectiveDate").ToString.Trim).ToShortDateString()
                    End If

                    If xRow.Item("Col_Costcenter_EffectiveDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Costcenter_EffectiveDate") = eZeeDate.convertDate(xRow.Item("Col_Costcenter_EffectiveDate").ToString.Trim).ToShortDateString()
                    End If

                    If xRow.Item("Col_WorkPermit_EffectiveDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_WorkPermit_EffectiveDate") = eZeeDate.convertDate(xRow.Item("Col_WorkPermit_EffectiveDate").ToString.Trim).ToShortDateString()
                    End If

                    If xRow.Item("Col_Resident_EffectiveDate").ToString.Trim.Length > 0 Then
                        rpt_ERow.Item("Col_Resident_EffectiveDate") = eZeeDate.convertDate(xRow.Item("Col_Resident_EffectiveDate").ToString.Trim).ToShortDateString()
                    End If

                    rpt_Data.Tables("EmployeeData").Rows.Add(rpt_ERow)
                Next
            End If
            '******************************** EMPLOYEE DATA *************************** END

            '******************************** CUSTOM ITEM DATA *************************** START
            Dim dsCusHeaders As New DataSet
            dsCusHeaders = GetCustomItemDataList(mintPeriodId)
            dsResult = GetCustomItemResult(mintPeriodId, mintEmployeeId)
            Dim iCustomId, iNewCustomId As String
            iCustomId = "" : iNewCustomId = ""
            Dim rpt_IRow As DataRow = Nothing
            Dim intValue As Integer = 0
            'S.SANDEEP |09-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
            'Dim iNames As List(Of String) = dsResult.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("Col_FullEmpName")).Distinct().ToList()
            Dim iNames As List(Of Integer) = dsResult.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid")).Distinct().ToList()
            'S.SANDEEP |09-FEB-2019| -- END
            For Each iname In iNames
                'S.SANDEEP |09-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}
                'Dim dtTable = New DataView(dsResult.Tables(0), "Col_FullEmpName = '" & iname & "'", "Cid", DataViewRowState.CurrentRows).ToTable()
                Dim dtTable = New DataView(dsResult.Tables(0), "employeeunkid = '" & iname & "'", "Cid", DataViewRowState.CurrentRows).ToTable()
                'S.SANDEEP |09-FEB-2019| -- END
                For Each xRow As DataRow In dsCusHeaders.Tables(0).Rows
                    iCustomId = xRow.Item("Col_CustomItemCategory_Value")
                    If iCustomId <> iNewCustomId Then
                        rpt_IRow = rpt_Data.Tables("CustomItems").NewRow
                        rpt_Data.Tables("CustomItems").Rows.Add(rpt_IRow)
                        iNewCustomId = xRow.Item("Col_CustomItemCategory_Value")
                    End If
                    rpt_IRow.Item("Col_CustomItemCategory_Caption") = xRow.Item("Col_CustomItemCategory_Caption")
                    rpt_IRow.Item("Col_CustomItemCategory_Value") = xRow.Item("Col_CustomItemCategory_Value")
                    Dim dval() As DataRow = dtTable.Select("Cid = '" & xRow("customitemunkid") & "'")
                    If dval.Length > 0 Then
                        intValue = 0
                        For intRowIndex As Integer = 0 To dval.Length - 1
                            intValue = intValue + 1
                            Select Case CInt(xRow.Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.FREE_TEXT
                                    If dval(intRowIndex).Item("xCnt") > 1 Then
                                        Select Case dval(intRowIndex).Item("ModeId")
                                            Case enAssessmentMode.SELF_ASSESSMENT
                                                rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                            Case Else
                                                rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= intValue.ToString() & ". " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                        End Select
                                    Else
                                        rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                    End If
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    If dval(intRowIndex).Item("cvalue").ToString.Trim.Length > 0 Then
                                        Select Case CInt(xRow.Item("selectionmodeid"))
                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dval(intRowIndex).Item("cvalue"))
                                                If dval(intRowIndex).Item("xCnt") > 1 Then
                                                    Select Case dval(intRowIndex).Item("ModeId")
                                                        Case enAssessmentMode.SELF_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case Else
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                    End Select
                                                Else
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & objCMaster._Name
                                                End If
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dval(intRowIndex).Item("cvalue"))
                                                If dval(intRowIndex).Item("xCnt") > 1 Then
                                                    Select Case dval(intRowIndex).Item("ModeId")
                                                        Case enAssessmentMode.SELF_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case Else
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= objCMaster._Name & vbCrLf & vbCrLf
                                                    End Select
                                                Else
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                End If
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                                Dim objEmpField1 As New clsassess_empfield1_master
                                                objEmpField1._Empfield1unkid = CInt(dval(intRowIndex).Item("cvalue"))
                                                If dval(intRowIndex).Item("xCnt") > 1 Then
                                                    Select Case dval(intRowIndex).Item("ModeId")
                                                        Case enAssessmentMode.SELF_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & objEmpField1._Field_Data & vbCrLf
                                                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & objEmpField1._Field_Data & vbCrLf
                                                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & objEmpField1._Field_Data & vbCrLf
                                                        Case Else
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= objEmpField1._Field_Data & vbCrLf
                                                    End Select
                                                Else
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & objEmpField1._Field_Data & vbCrLf
                                                End If
                                                objEmpField1 = Nothing


                                                'S.SANDEEP |16-AUG-2019| -- START
                                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dval(intRowIndex).Item("cvalue"))
                                                If dval(intRowIndex).Item("xCnt") > 1 Then
                                                    Select Case dval(intRowIndex).Item("ModeId")
                                                        Case enAssessmentMode.SELF_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                        Case Else
                                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= intValue.ToString() & ". " & objCMaster._Name & vbCrLf
                                                    End Select
                                                Else
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & objCMaster._Name
                                                End If
                                                objCMaster = Nothing
                                                'S.SANDEEP |16-AUG-2019| -- END
                                        End Select
                                    End If
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dval(intRowIndex).Item("cvalue").ToString.Trim.Length > 0 Then
                                        If dval(intRowIndex).Item("xCnt") > 1 Then
                                            Select Case dval(intRowIndex).Item("ModeId")
                                                Case enAssessmentMode.SELF_ASSESSMENT
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                                Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                                Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                                Case Else
                                                    rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= intValue.ToString() & ". " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                            End Select
                                        Else
                                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                        End If
                                    End If
                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                    If dval(intRowIndex).Item("cvalue").ToString.Trim.Length > 0 Then
                                        If IsNumeric(dval(intRowIndex).Item("cvalue")) Then
                                            If dval(intRowIndex).Item("xCnt") > 1 Then
                                                Select Case dval(intRowIndex).Item("ModeId")
                                                    Case enAssessmentMode.SELF_ASSESSMENT
                                                        rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 9, "Self") & " : " & intValue.ToString() & ". " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                        rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 10, "Assessor") & " : " & intValue.ToString() & ". " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                        rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= Language.getMessage(mstrModuleName, 11, "Reviewer") & " : " & intValue.ToString() & ". " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                                    Case Else
                                                        rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") &= intValue.ToString() & ". " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                                End Select
                                            Else
                                                rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Value") = intValue.ToString() & ". " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                            End If
                                        End If
                                    End If
                            End Select
                            rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Caption") = xRow.Item("custom_item")
                            If dval.Length > 0 Then
                                rpt_IRow.Item("Col_EmployeeName_Value") = dval(0).Item("Col_FullEmpName")
                            End If
                            rpt_IRow.Item("Col_AssessorName_Value") = ""
                            rpt_IRow.Item("Col_ReviewerName_Value") = ""
                        Next
                    Else
                        rpt_IRow.Item("Col_CustomItem" & xRow.Item("xFc") & "_Caption") = xRow.Item("custom_item")
                    End If
                Next
            Next

            If rpt_Data.Tables("CustomItems").Rows.Count <= 0 Then
                rpt_Data.Tables("CustomItems").Rows.Add(rpt_Data.Tables("CustomItems").NewRow)
                rpt_Data.Tables("CustomItems").Rows(0).Item("Col_EmployeeName_Value") = ""
            End If
            '******************************** CUSTOM ITEM DATA *************************** END

            objRpt = New ArutiReport.Designer.rptAssessmentCustomItemPDP

            Call ReportFunction.TextChange(objRpt, "txtPDP", Language.getMessage(mstrModuleName, 12, "PERSONAL DEVELOPMENT PLAN FORM (PDP)"))
            Call ReportFunction.TextChange(objRpt, "txtPersonalData", Language.getMessage(mstrModuleName, 13, "PERSONAL DATA"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeParticulars", Language.getMessage(mstrModuleName, 14, "Employee Particulars:"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 15, "Name"))
            Call ReportFunction.TextChange(objRpt, "txtLineManager", Language.getMessage(mstrModuleName, 16, "Line Manager"))
            Call ReportFunction.TextChange(objRpt, "txtStaffId", Language.getMessage(mstrModuleName, 17, "Staff Id"))
            Call ReportFunction.TextChange(objRpt, "txtSectionGroup", Language.getMessage(mstrModuleName, 18, "Section Group"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 19, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 20, "Branch"))
            Call ReportFunction.TextChange(objRpt, "txtUnit", Language.getMessage(mstrModuleName, 21, "Unit"))
            Call ReportFunction.TextChange(objRpt, "txtSection", Language.getMessage(mstrModuleName, 22, "Section"))
            Call ReportFunction.TextChange(objRpt, "txtGradeGrpGrade", Language.getMessage(mstrModuleName, 23, "Grade Group/Grade"))
            Call ReportFunction.TextChange(objRpt, "txtDateJob", Language.getMessage(mstrModuleName, 24, "Date on Job"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 25, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 26, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtEmpSignature", Language.getMessage(mstrModuleName, 27, "EMPLOYEE’S SIGNATURE"))
            Call ReportFunction.TextChange(objRpt, "txtManagerSignature", Language.getMessage(mstrModuleName, 28, "LINE MANAGER’S SIGNATURE"))
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'Call ReportFunction.TextChange(objRpt, "txtNote", Language.getMessage(mstrModuleName, 29, "NOTE: Every employee is responsible for his/her own development; therefore, you need to take initiative and take control of your own development, Line Manager, L&TD team and the NMB Bank will be there only to support."))
            Call ReportFunction.TextChange(objRpt, "txtNote", Language.getMessage(mstrModuleName, 30, "NOTE: Every employee is responsible for his/her own development; therefore, you need to take initiative and take control of your own development, Line Manager, L&TD team and the") & _
                                           " " & StrCompanyName & " " & Language.getMessage(mstrModuleName, 31, "will be there only to support."))
            'S.SANDEEP |25-OCT-2019| -- END

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", ConfigParameter._Object._CurrentDateAndTime)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", User._Object._Username)

            objRpt.SetDataSource(rpt_Data)
            For index As Integer = 0 To objRpt.Subreports.Count - 1
                objRpt.Subreports(index).SetDataSource(rpt_Data)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

#End Region
    'S.SANDEEP [01-OCT-2018] -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage(mstrModuleName, 1, "Employee Name")
            Language.setMessage(mstrModuleName, 2, "Employee Code")
            Language.setMessage(mstrModuleName, 3, "Report successfully exported to Report export path")
            Language.setMessage(mstrModuleName, 4, "Self")
            Language.setMessage(mstrModuleName, 5, "Assessor")
            Language.setMessage(mstrModuleName, 6, "Reviewer")
            Language.setMessage(mstrModuleName, 7, "Date :")
            Language.setMessage(mstrModuleName, 8, "Custom Section Titles")
            Language.setMessage(mstrModuleName, 9, "Self")
            Language.setMessage(mstrModuleName, 10, "Assessor")
            Language.setMessage(mstrModuleName, 11, "Reviewer")
            Language.setMessage(mstrModuleName, 12, "PERSONAL DEVELOPMENT PLAN FORM (PDP)")
            Language.setMessage(mstrModuleName, 13, "PERSONAL DATA")
            Language.setMessage(mstrModuleName, 14, "Employee Particulars:")
            Language.setMessage(mstrModuleName, 15, "Name")
            Language.setMessage(mstrModuleName, 16, "Line Manager")
            Language.setMessage(mstrModuleName, 17, "Staff Id")
            Language.setMessage(mstrModuleName, 18, "Section Group")
            Language.setMessage(mstrModuleName, 19, "Job Title")
            Language.setMessage(mstrModuleName, 20, "Branch")
            Language.setMessage(mstrModuleName, 21, "Unit")
            Language.setMessage(mstrModuleName, 22, "Section")
            Language.setMessage(mstrModuleName, 23, "Grade Group/Grade")
            Language.setMessage(mstrModuleName, 24, "Date on Job")
            Language.setMessage(mstrModuleName, 25, "Printed By :")
            Language.setMessage(mstrModuleName, 26, "Printed Date :")
            Language.setMessage(mstrModuleName, 27, "EMPLOYEE’S SIGNATURE")
            Language.setMessage(mstrModuleName, 28, "LINE MANAGER’S SIGNATURE")
            Language.setMessage(mstrModuleName, 30, "NOTE: Every employee is responsible for his/her own development; therefore, you need to take initiative and take control of your own development, Line Manager, L&TD team and the")
            Language.setMessage(mstrModuleName, 31, "will be there only to support.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
