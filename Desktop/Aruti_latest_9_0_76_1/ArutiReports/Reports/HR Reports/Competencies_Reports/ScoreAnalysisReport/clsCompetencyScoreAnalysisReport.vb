﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsCompetencyScoreAnalysisReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsCompetencyScoreAnalysisReport"
    Private mstrReportId As String = enArutiReport.Competence_Score_Analysis_Report

#Region " Properties Value "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mintCompetenceGroupId As Integer = 0
    Private mstrCompetenceGroup As String = ""
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrAdvanceFilter As String = ""
    Private mblnFirstNamethenSurname As Boolean = True
    Private mdecScoreFrom As Decimal = -1
    Private mdecScoreTo As Decimal = -1
    Private mblnShowOnlyCommittedAssessment As Boolean = False
    Private mblnShowEmployeeWithNoCompetencies As Boolean = False

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _CompetenceGroupId() As Integer
        Set(ByVal value As Integer)
            mintCompetenceGroupId = value
        End Set
    End Property

    Public WriteOnly Property _CompetenceGroup() As String
        Set(ByVal value As String)
            mstrCompetenceGroup = value
        End Set
    End Property

    Public WriteOnly Property _ScoreFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecScoreFrom = value
        End Set
    End Property

    Public WriteOnly Property _ScoreTo() As Decimal
        Set(ByVal value As Decimal)
            mdecScoreTo = value
        End Set
    End Property

    Public WriteOnly Property _ShowOnlyCommittedAssessment() As Boolean
        Set(ByVal value As Boolean)
            mblnShowOnlyCommittedAssessment = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeWithNoCompetencies() As String
        Set(ByVal value As String)
            mblnShowEmployeeWithNoCompetencies = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mblnFirstNamethenSurname = True
            mintCompetenceGroupId = 0
            mstrCompetenceGroup = ""
            mdecScoreFrom = -1
            mdecScoreTo = -1
            mblnShowOnlyCommittedAssessment = False
            mblnShowEmployeeWithNoCompetencies = False
            mstrAdvanceFilter = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub ExportScoreAnalysisReport(ByVal strDatabaseName As String, _
                                         ByVal intUserUnkid As Integer, _
                                         ByVal intYearUnkid As Integer, _
                                         ByVal intCompanyUnkid As Integer, _
                                         ByVal dtPeriodStart As Date, _
                                         ByVal dtPeriodEnd As Date, _
                                         ByVal strUserModeSetting As String, _
                                         ByVal blnOnlyApproved As Boolean, _
                                         ByVal strExportPath As String, _
                                         ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "DECLARE @ModeId INT " & _
                    "SET @ModeId = 0 " & _
                    "SELECT @ModeId = MAX(assessmodeid) FROM hrevaluation_analysis_master WHERE isvoid = 0 AND evaltypeid = 3 AND periodunkid = '" & mintPeriodId & "' AND assessmodeid > 1 " & _
                    " " & _
                    "SELECT " & _
                          "hremployee_master.employeecode " & _
                         ",hremployee_master.firstname+' '+hremployee_master.surname as ename " & _
                         ",ISNULL(A.category,'') AS category " & _
                         ",ISNULL(A.tScore,0) AS tScore " & _
                    "FROM hremployee_master "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "EV.assessedemployeeunkid " & _
                              ",CC.name AS category " & _
                              ",SUM(CT.result)/COUNT(CM.competence_categoryunkid) AS tScore " & _
                         "FROM hrevaluation_analysis_master AS EV " & _
                              "JOIN hrcompetency_analysis_tran AS CT ON CT.analysisunkid = EV.analysisunkid " & _
                              "JOIN hrassess_competencies_master AS CM ON CM.competenciesunkid = CT.competenciesunkid " & _
                              "JOIN cfcommon_master AS CC ON CC.masterunkid = CM.competence_categoryunkid " & _
                         "WHERE EV.periodunkid = @periodunkid AND EV.evaltypeid = 3 AND EV.assessmodeid = @ModeId "
            If mblnShowOnlyCommittedAssessment Then
                StrQ &= " AND AND EV.iscommitted = 1 "
            End If

            If mintCompetenceGroupId > 0 Then
                StrQ &= " AND CM.competence_categoryunkid = @competence_categoryunkid "
            End If

            StrQ &= "AND CT.isvoid = 0 AND EV.isvoid = 0 " & _
                         "GROUP BY " & _
                          "EV.assessedemployeeunkid " & _
                         ",CC.name " & _
                         ",CT.analysisunkid " & _
                    ") AS A ON A.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mblnShowEmployeeWithNoCompetencies = False Then
                StrQ &= " AND ISNULL(A.category,'') <> '' "
            End If

            If mdecScoreFrom > 0 Then
                StrQ &= " AND ISNULL(A.tScore,0) >= @scf "
            End If

            If mdecScoreTo > 0 Then
                StrQ &= " AND ISNULL(A.tScore,0) <= @sct "
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceGroupId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@scf", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecScoreFrom)
            objDataOperation.AddParameter("@sct", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecScoreTo)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList.Tables(0).Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 100, "Code")
            dsList.Tables(0).Columns("ename").Caption = Language.getMessage(mstrModuleName, 101, "Name")
            dsList.Tables(0).Columns("category").Caption = Language.getMessage(mstrModuleName, 102, "Competence Category")
            dsList.Tables(0).Columns("tScore").Caption = Language.getMessage(mstrModuleName, 103, "Score")

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables(0).Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 80
                    Case 1, 2
                        intArrayColumnWidth(i) = 125
                    Case Else
                        intArrayColumnWidth(i) = 50
                End Select
            Next

            Dim strFilter As String = ""

            If mintPeriodId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 200, "Period") & " : " & mstrPeriodName & " "
            End If

            If mintCompetenceGroupId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 102, "Competence Category") & " : " & mstrCompetenceGroup & " "
            End If

            If mintEmployeeId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 202, "Employee") & " : " & mstrEmployeeName & " "
            End If

            If mdecScoreFrom > -1 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 203, "Score From") & " : " & mdecScoreFrom & " "
            End If

            If mdecScoreTo > -1 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 204, "Score To") & " : " & mdecScoreTo & " "
            End If

            strFilter = Mid(strFilter, 2)

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables(0), intArrayColumnWidth, True, True, False, Nothing, Me._ReportName, "", strFilter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportScoreAnalysisReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Code")
            Language.setMessage(mstrModuleName, 101, "Name")
            Language.setMessage(mstrModuleName, 102, "Competence Category")
            Language.setMessage(mstrModuleName, 103, "Score")
            Language.setMessage(mstrModuleName, 200, "Period")
            Language.setMessage(mstrModuleName, 202, "Employee")
            Language.setMessage(mstrModuleName, 203, "Score From")
            Language.setMessage(mstrModuleName, 204, "Score To")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
