'************************************************************************************************************************************
'Class Name : clsEmployeeList_Asr_Rer.vb
'Purpose    :
'Date       : 24 OCT 2016
'Written By : Shani Sheladiya
'Modified   :
'************************************************************************************************************************************
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class clsEmployeeList_Asr_Rer
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeList_Asr_Rer"
    Private mstrReportId As String = CStr(enArutiReport.Employee_List_Assessor_Reviewer_Report)  '184
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private iColumn_DetailReport As New IColumnCollection
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mstrAdvance_Filter As String = String.Empty
    Private mintCompanyUnkid As Integer = -1
    Private mintUserUnkid As Integer = -1
#End Region

#Region " Properties "
    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
#End Region

#Region " Public Function & Procedures "

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Employee :") & " " & mstrEmployeeName & " "
                Me._FilterQuery &= "AND hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "' "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = String.Empty
            mblnIncludeInactiveEmp = False
            Rpt = Nothing
            mstrAdvance_Filter = ""
            mblnIncludeAccessFilterQry = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid > 0 Then
                Company._Object._Companyunkid = mintCompanyUnkid
                ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            End If
            If mintUserUnkid > 0 Then
                User._Object._Userunkid = mintUserUnkid
            End If

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If mintCompanyUnkid <= 0 Then
                If Not IsNothing(objRpt) Then
                    Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 2, "Employee Code")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(surname,'')+' '+ ISNULL(firstname,'')+' '+ISNULL(othername,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)



            Dim StrExternalJoin As String = "   #UNI# " & _
                                            "      SELECT " & _
                                            "            hrassessor_tran.employeeunkid " & _
                                            "           ,#ASRNAME# AS ASR_Name " & _
                                            "           ,#ASRCODE# AS ASR_Code" & _
                                            "           ,#ASRJOB# AS ASR_JOB " & _
                                            "       FROM hrassessor_tran " & _
                                            "           LEFT JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                                            "           #LEFT_DATA_JOIN# " & _
                                            "       WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 and hrassessor_master.isreviewer = #ISREVIEWER# " & _
                                            "           AND hrassessor_master.isexternalapprover = #ISEXTERNAL# AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "
            'S.SANDEEP [27 DEC 2016] -- START {hrassessor_tran.visibletypeid} -- END



            Dim dsExAR As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , , , clsAssessor.enAssessorType.ALL_DATA)



            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Call FilterTitleAndFilterQuery()

            StrQ = "SELECT " & _
                   "    A.employeecode AS EMP_Code "

            If mblnFirstNamethenSurname = False Then
                StrQ &= "	,ISNULL(A.employeecode + ' - ','') + ISNULL(A.surname,'')+' '+ISNULL(A.firstname,'')+' '+ISNULL(A.othername,'') AS EMP_NAME "
            Else
                StrQ &= "	,ISNULL(A.employeecode + ' - ','') + ISNULL(A.firstname,'')+' '+ISNULL(A.othername,'')+' '+ISNULL(A.surname,'') AS EMP_NAME "
            End If

            StrQ &= "   ,A.JOB AS EMP_JOB " & _
                    "   ,A.ASR_Code AS ASR_Code " & _
                    "   ,A.ASR_Name AS ASR_Name " & _
                    "   ,A.ASR_JOB AS ASR_JOB " & _
                    "   ,A.REV_Code AS REV_Code " & _
                    "   ,A.REV_Name AS REV_Name " & _
                    "   ,A.REV_JOB AS REV_JOB " & _
                    "   ,A.Type AS 'Type' " & _
                    "   ,A.TypeId AS 'TypeId' " & _
                    "FROM " & _
                    "( " & _
                    "SELECT " & _
                    "    hremployee_master.employeecode " & _
                    "   ,hremployee_master.firstname " & _
                    "   ,hremployee_master.surname " & _
                    "   ,hremployee_master.othername " & _
                    "   ,hrjob_master.job_name AS JOB " & _
                    "   ,@Assessor AS 'Type' " & _
                    "   ,0 AS 'TypeId' " & _
                    "   ,ISNULL(EMPASR.ASR_Code,'') AS ASR_Code " & _
                    "   ,ISNULL(EMPASR.ASR_Name,'') AS ASR_Name " & _
                    "   ,ISNULL(EMPASR.ASR_JOB,'') AS ASR_JOB " & _
                    "   ,'' AS REV_Code " & _
                    "   ,'' AS REV_Name " & _
                    "   ,'' AS REV_JOB " & _
                    "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END            
            StrQ &= "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   LEFT JOIN hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    " " & StrExternalJoin & " "

            StrQ = StrQ.Replace("#ASRNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
            StrQ = StrQ.Replace("#ASRCODE#", "hremployee_master.employeecode")
            StrQ = StrQ.Replace("#ASRJOB#", "hrjob_master.job_name")
            StrQ = StrQ.Replace("#LEFT_DATA_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
                                                    "LEFT JOIN " & _
                                                    "( " & _
                                                    "   SELECT " & _
                                                    "        jobunkid " & _
                                                    "       ,jobgroupunkid " & _
                                                    "       ,employeeunkid " & _
                                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                    "   FROM hremployee_categorization_tran " & _
                                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                    "LEFT JOIN hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid ")
            StrQ = StrQ.Replace("#ISREVIEWER#", "0")
            StrQ = StrQ.Replace("#ISEXTERNAL#", "0")
            StrQ = StrQ.Replace("#UNI#", "")
            If dsExAR IsNot Nothing AndAlso dsExAR.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsExAR.Tables(0).Select("isreviewer = False")
                    StrQ &= StrExternalJoin
                    StrQ = StrQ.Replace("#UNI#", " UNION ALL ")
                    If dr("DBName").ToString.Trim.Length <= 0 AndAlso dr("companyunkid") <= 0 Then
                        StrQ = StrQ.Replace("#ASRNAME#", "ISNULL(UEmp.username,'') ")
                        StrQ = StrQ.Replace("#ASRCODE#", "''")
                        StrQ = StrQ.Replace("#ASRJOB#", "''")
                        StrQ = StrQ.Replace("#LEFT_DATA_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid ")
                    Else
                        StrQ = StrQ.Replace("#ASRNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
                        StrQ = StrQ.Replace("#ASRCODE#", "hremployee_master.employeecode")
                        StrQ = StrQ.Replace("#ASRJOB#", "hrjob_master.job_name")
                        StrQ = StrQ.Replace("#LEFT_DATA_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid " & _
                                                                "LEFT JOIN #DName#hremployee_master ON hremployee_master.employeeunkid = UEmp.employeeunkid " & _
                                                                "LEFT JOIN " & _
                                                                "( " & _
                                                                "   SELECT " & _
                                                                "        jobunkid " & _
                                                                "       ,jobgroupunkid " & _
                                                                "       ,employeeunkid " & _
                                                                "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                                "   FROM #DName#hremployee_categorization_tran " & _
                                                                "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & dr.Item("EMPASONDATE") & "' " & _
                                                                ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                                "LEFT JOIN #DName#hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid ")

                        StrQ = StrQ.Replace("#DName#", dr("DBName") & "..")
                    End If
                    StrQ = StrQ.Replace("#ISREVIEWER#", "0")
                    StrQ = StrQ.Replace("#ISEXTERNAL#", "1")
                Next
            End If

            StrQ &= "   ) AS EMPASR ON EMPASR.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= Me._FilterQuery

            StrQ &= "UNION ALL " & _
                    "SELECT " & _
                    "    hremployee_master.employeecode " & _
                    "   ,hremployee_master.firstname " & _
                    "   ,hremployee_master.surname " & _
                    "   ,hremployee_master.othername " & _
                    "   ,hrjob_master.job_name AS JOB " & _
                    "   ,@Reviewer AS 'Type' " & _
                    "   ,1 AS 'TypeId' " & _
                    "   ,'' AS ASR_Code " & _
                    "   ,'' AS ASR_Name " & _
                    "   ,'' AS ASR_JOB " & _
                    "   ,ISNULL(EMPREW.ASR_Code,'') AS REV_Code " & _
                    "   ,ISNULL(EMPREW.ASR_Name,'') AS REV_Name " & _
                    "   ,ISNULL(EMPREW.ASR_JOB,'') AS REV_JOB " & _
                    "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   LEFT JOIN hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    " " & StrExternalJoin & " "

            StrQ = StrQ.Replace("#ASRNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
            StrQ = StrQ.Replace("#ASRCODE#", "hremployee_master.employeecode")
            StrQ = StrQ.Replace("#ASRJOB#", "hrjob_master.job_name")
            StrQ = StrQ.Replace("#LEFT_DATA_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
                                                    "LEFT JOIN " & _
                                                    "( " & _
                                                    "   SELECT " & _
                                                    "        jobunkid " & _
                                                    "       ,jobgroupunkid " & _
                                                    "       ,employeeunkid " & _
                                                    "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                    "   FROM hremployee_categorization_tran " & _
                                                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                    "LEFT JOIN hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid ")
            StrQ = StrQ.Replace("#ISREVIEWER#", "1")
            StrQ = StrQ.Replace("#ISEXTERNAL#", "0")
            StrQ = StrQ.Replace("#UNI#", "")

            If dsExAR IsNot Nothing AndAlso dsExAR.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsExAR.Tables(0).Select("isreviewer = True")
                    StrQ &= StrExternalJoin
                    StrQ = StrQ.Replace("#UNI#", " UNION ALL ")
                    If dr("DBName").ToString.Trim.Length <= 0 AndAlso dr("companyunkid") <= 0 Then
                        StrQ = StrQ.Replace("#ASRNAME#", "ISNULL(UEmp.username,'') ")
                        StrQ = StrQ.Replace("#ASRCODE#", "''")
                        StrQ = StrQ.Replace("#ASRJOB#", "''")
                        StrQ = StrQ.Replace("#LEFT_DATA_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid ")
                    Else
                        StrQ = StrQ.Replace("#ASRNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
                        StrQ = StrQ.Replace("#ASRCODE#", "hremployee_master.employeecode")
                        StrQ = StrQ.Replace("#ASRJOB#", "hrjob_master.job_name")
                        StrQ = StrQ.Replace("#LEFT_DATA_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid " & _
                                                                "LEFT JOIN #DName#hremployee_master ON hremployee_master.employeeunkid = UEmp.employeeunkid " & _
                                                                "LEFT JOIN " & _
                                                                "( " & _
                                                                "   SELECT " & _
                                                                "        jobunkid " & _
                                                                "       ,jobgroupunkid " & _
                                                                "       ,employeeunkid " & _
                                                                "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                                "   FROM #DName#hremployee_categorization_tran " & _
                                                                "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & dr.Item("EMPASONDATE") & "' " & _
                                                                ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                                "LEFT JOIN #DName#hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid ")

                        StrQ = StrQ.Replace("#DName#", dr("DBName") & "..")
                    End If
                    StrQ = StrQ.Replace("#ISREVIEWER#", "1")
                    StrQ = StrQ.Replace("#ISEXTERNAL#", "1")
                Next
            End If

            StrQ &= "   ) AS EMPREW ON EMPREW.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            StrQ &= Me._FilterQuery
            StrQ &= ") AS A  ORDER BY A.employeecode,A.TypeId "





            objDataOperation.AddParameter("@Assessor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Assessor"))
            objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Reviewer"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("EMP_Code")
                rpt_Rows.Item("Column2") = dtRow.Item("EMP_NAME")
                rpt_Rows.Item("Column3") = dtRow.Item("EMP_JOB")
                rpt_Rows.Item("Column4") = dtRow.Item("ASR_Code")
                rpt_Rows.Item("Column5") = dtRow.Item("ASR_Name")
                rpt_Rows.Item("Column6") = dtRow.Item("ASR_JOB")
                rpt_Rows.Item("Column7") = dtRow.Item("REV_Code")
                rpt_Rows.Item("Column8") = dtRow.Item("REV_Name")
                rpt_Rows.Item("Column9") = dtRow.Item("REV_JOB")
                rpt_Rows.Item("Column10") = dtRow.Item("Type")
                rpt_Rows.Item("Column11") = dtRow.Item("TypeId")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployee_List_Asr_Rer

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 1, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeJob", Language.getMessage(mstrModuleName, 3, "Employee Job"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 8, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 9, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee")
            Language.setMessage(mstrModuleName, 2, "Employee Code")
            Language.setMessage(mstrModuleName, 3, "Employee Job")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Assessor")
            Language.setMessage(mstrModuleName, 8, "Printed By :")
            Language.setMessage(mstrModuleName, 9, "Printed Date :")
            Language.setMessage(mstrModuleName, 10, "Employee :")
            Language.setMessage(mstrModuleName, 11, "Reviewer")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
