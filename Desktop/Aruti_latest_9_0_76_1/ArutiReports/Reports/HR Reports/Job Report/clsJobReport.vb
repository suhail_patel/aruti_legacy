
Imports Aruti.Data
Imports eZeeCommonLib


Public Class clsJobReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsJobReport"
    Private mstrReportId As String = enArutiReport.JobReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrOrderByQuery As String = String.Empty
    Private mintJobId As Integer = -1
    Private mstrJobName As String = String.Empty
    'Private mstrReport_GroupName As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
    Private mintEmployeeID As Integer = -1
    Private mstrEmployee As String = ""
    Private mintGradeID As Integer = -1
    Private mstrGrade As String = ""
    Private mstrEmployeeAsonDate As String = ""
    'Pinkal (03-Dec-2015) -- End

    Private mintReportId As Integer = 0  'Hemant (15 Nov 2019)

#End Region

#Region "Properties"

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _JobName() As String
        Set(ByVal value As String)
            mstrJobName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployee = value
        End Set
    End Property

    Public WriteOnly Property _GradeId() As Integer
        Set(ByVal value As Integer)
            mintGradeID = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeGrade() As String
        Set(ByVal value As String)
            mstrGrade = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeAsOnDate() As String
        Set(ByVal value As String)
            mstrEmployeeAsonDate = value
        End Set
    End Property

    'Pinkal (03-Dec-2015) -- End

    'Hemant (15 Nov 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property
    'Hemant (15 Nov 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintJobId = 0
            mstrJobName = ""


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            mintEmployeeID = 0
            mstrEmployee = ""
            mintGradeID = 0
            mstrGrade = ""
            'Pinkal (03-Dec-2015) -- End

            mstrOrderByQuery = ""
            mintReportId = 0  'Hemant (15 Nov 2019)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            'If mintJobId > 0 Then
            '    Me._FilterQuery &= " AND hrjob_master.jobunkid = @jobunkid "
            '    objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Job : ") & " " & mstrJobName & " "
            'End If


            'If Me.OrderByQuery <> "" Then
            '    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Order By : ") & " " & Me.OrderByDisplay
            'End If

            'Pinkal (03-Dec-2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'Sohail (13 May 2020) -- Start
            'NMB Issue # : On Job report; logo is not getting displayed on report on MSS �� Staff requisition approval page.
            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid
            'Sohail (13 May 2020) -- End
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            'objRpt = Generate_DetailReport()
            objRpt = Generate_DetailReport(mintReportId)
            'Hemant (15 Nov 2019) -- End
            Rpt = objRpt

            'Pinkal (14-Aug-2012) -- End
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn())
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.


    Private Function Generate_DetailReport(ByVal intJobReportTemplate As Integer _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Hemant (15 Nov 2019) -- [intJobReportTemplate]
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsKeyduties As New DataSet
        Dim dsExperience As New DataSet
        Dim dsSkill As New DataSet
        Dim dsQualification As New DataSet
        Dim dsCompetencies As New DataSet
        Dim dtInputs As New DataTable
        Dim dsReportTo As New DataSet
        Dim dsJobEvaluation As New DataSet 'Hemant (15 Nov 2019)

        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_JobPurpose As ArutiReport.Designer.dsArutiReport
        Dim rpt_KeyDuties As ArutiReport.Designer.dsArutiReport
        Dim rpt_Inputs As ArutiReport.Designer.dsArutiReport
        Dim rpt_OtherDimension As ArutiReport.Designer.dsArutiReport
        'Hemant (15 Nov 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
        Dim rpt_CommunicationInteraction As ArutiReport.Designer.dsArutiReport
        'Hemant (15 Nov 2019) -- End

        'Dim rpt_Skill As ArutiReport.Designer.dsArutiReport
        'Dim rpt_Quali As ArutiReport.Designer.dsArutiReport

        Try

            If dtInputs IsNot Nothing Then
                dtInputs.Columns.Add("Particulars", Type.GetType("System.String"))
                dtInputs.Columns.Add("Jobunkid", Type.GetType("System.Int32"))
                dtInputs.Columns.Add("CategoryID", Type.GetType("System.Int32"))
                dtInputs.Columns.Add("CategoryCode", Type.GetType("System.String"))
                dtInputs.Columns.Add("CategoryName", Type.GetType("System.String"))
                dtInputs.Columns.Add("ID", Type.GetType("System.Int32"))
                dtInputs.Columns.Add("Code", Type.GetType("System.String"))
                dtInputs.Columns.Add("Name", Type.GetType("System.String"))
            End If

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim mstrDirectReportToJobunkid As String = ""
            Dim mstrInDirectReportToJobunkid As String = ""

            'StrQ = "SELECT  jobunkid " & _
            '              ", job_name " & _
            '              ", ISNULL(CONVERT(CHAR(8), create_date, 112), '') AS create_date " & _
            '              ", total_position " & _
            '              ", desciription " & _
            '        "FROM    hrjob_master " & _
            '        "WHERE   isactive = 1 "

            StrQ = "SELECT  hrjob_master.jobunkid " & _
                      ", hrjob_master.job_name " & _
                      ", ISNULL(hjm.job_name,'') AS ReportTo " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ','  + cast(jobunkid as nvarchar(6))  FROM hrjob_master WHERE hrjob_master.isactive = 1 AND report_tounkid = @jobunkid " & _
                      " FOR XML PATH('')),1,1,''),'')) AS Job_DirectReporttounkid " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ','  + cast(jobunkid as nvarchar(6))  FROM hrjob_master WHERE hrjob_master.isactive = 1 AND indirectreport_tounkid = @jobunkid " & _
                      " FOR XML PATH('')),1,1,''),'')) AS Job_InDirectReporttounkid " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ','  + job_name FROM hrjob_master WHERE hrjob_master.isactive = 1 AND report_tounkid = @jobunkid " & _
                      " FOR XML PATH('')),1,1,''),'')) AS Job_responsible " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ','  + hrdepartment_master.name FROM hrjob_master  " & _
                      "                                       LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrjob_master.jobdepartmentunkid " & _
                      "                                       AND hrdepartment_master.isactive =1 WHERE hrjob_master.isactive = 1 AND hrjob_master.jobunkid = @jobunkid " & _
                      " FOR XML PATH(''),TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')) AS Job_Department " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ','  + hrclassgroup_master.name FROM hrjob_master  " & _
                      "                                       LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrjob_master.jobclassgroupunkid " & _
                      "                                       AND hrclassgroup_master.isactive =1 WHERE hrjob_master.isactive = 1 AND report_tounkid = @jobunkid " & _
                      " FOR XML PATH(''),TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')) AS Job_Region " & _
                      ", ISNULL(hrjob_master.working_hrs,'') AS working_hrs " & _
                      ", ISNULL(CONVERT(CHAR(8), hrjob_master.create_date, 112), '') AS create_date " & _
                      ", ISNULL(hrgrade_master.name,'') AS Job_Grade " & _
                      ", ISNULL(hrjob_master.desciription,'') AS description " & _
                      ", ISNULL(hrunit_master.name, '') AS Job_Unit " & _
                      ", ISNULL(hrsection_master.name, '') AS Job_Section " & _
                      ", ISNULL(hrjob_master.experience_comment, '') AS experience_comment " & _
                      ", ISNULL(cfcommon_master.name, '') AS jobtype " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ','  + ISNULL(cfcommon_master.name, '') FROM hrjob_language_tran  " & _
                      "                                       LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjob_language_tran.masterunkid " & _
                      "                                       WHERE hrjob_language_tran.isactive =1 AND cfcommon_master.isactive = 1 AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " AND hrjob_language_tran.jobunkid = @jobunkid " & _
                      " FOR XML PATH(''),TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')) AS Job_Language " & _
                      ", (SELECT ISNULL(STUFF((SELECT DISTINCT ', '  + ISNULL(hrjob_master.job_name, '') FROM hrjob_master  " & _
                      "                                       WHERE hrjob_master.isactive =1 AND hrjob_master.report_tounkid = @jobunkid " & _
                      " FOR XML PATH(''),TYPE).value('.[1]','nvarchar(max)'),1,1,''),'')) AS DirectReports " & _
                      " FROM hrjob_master " & _
                      " LEFT JOIN hrjob_master hjm ON hjm.jobunkid = hrjob_master.report_tounkid AND hjm.isactive = 1 " & _
                      " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrjob_master.jobclassgroupunkid " & _
                      " LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hrjob_master.jobgradeunkid " & _
                      " LEFT JOIN hrunit_master	ON hrunit_master.unitunkid = hrjob_master.jobunitunkid " & _
                      " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrjob_master.jobsectionunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjob_master.jobtypeunkid AND cfcommon_master.isactive = 1 AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " " & _
                      " WHERE  hrjob_master.isactive = 1 AND  hrjob_master.jobunkid = @jobunkid "
            'Sohail (17 Apr 2020) - [DirectReports]
            'Sohail (17 Apr 2020) - [LEFT JOIN hrsection_master]
            'Sohail (18 Feb 2020) - [jobtype, LEFT JOIN cfcommon_master]
            'Hemant (15 Nov 2019) -- [Job_Unit,experience_comment, AND report_tounkid = @jobunkid --> AND hrjob_master.jobunkid = @jobunkid ]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
            '    Dim rpt_Rows As DataRow
            '    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

            '    rpt_Rows.Item("Column1") = dtRow.Item("jobunkid")
            '    rpt_Rows.Item("Column2") = dtRow.Item("job_name")
            '    If dtRow.Item("create_date").ToString.Trim = "" Then
            '        rpt_Rows.Item("Column3") = ""
            '    Else
            '        rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("create_date").ToString).ToShortDateString
            '    End If
            '    rpt_Rows.Item("Column4") = dtRow.Item("total_position")
            '    rpt_Rows.Item("Column5") = dtRow.Item("desciription")

            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            'Next

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("jobunkid").ToString()
                rpt_Rows.Item("Column2") = dtRow.Item("job_name").ToString()
                rpt_Rows.Item("Column3") = dtRow.Item("ReportTo").ToString()
                rpt_Rows.Item("Column4") = dtRow.Item("Job_Department").ToString()
                rpt_Rows.Item("Column5") = dtRow.Item("Job_Region").ToString()
                rpt_Rows.Item("Column6") = dtRow.Item("working_hrs").ToString()
                If dtRow.Item("create_date").ToString.Trim = "" Then
                    rpt_Rows.Item("Column7") = ""
                Else
                    rpt_Rows.Item("Column7") = eZeeDate.convertDate(dtRow.Item("create_date").ToString).ToShortDateString
                End If
                rpt_Rows.Item("Column8") = dtRow.Item("Job_Grade").ToString()
                If dtRow.Item("Job_responsible").ToString().Trim.Length > 0 Then
                    rpt_Rows.Item("Column9") = "• " & dtRow.Item("Job_responsible").ToString().Replace(",", vbCrLf & "• ")
                Else
                    rpt_Rows.Item("Column9") = ""
                End If

                rpt_Rows.Item("Column10") = mstrEmployee
                rpt_Rows.Item("Column11") = mstrGrade
                'Hemant (15 Nov 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
                'Sohail (17 Apr 2020) -- Start
                'NMB Issue # : On Job Report, for unit system should pick data from section mapped on Job master.
                'rpt_Rows.Item("Column12") = dtRow.Item("Job_Unit").ToString()
                rpt_Rows.Item("Column12") = dtRow.Item("Job_Section").ToString()
                'Sohail (17 Apr 2020) -- End
                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : Pick employment type for type of job on job master.
                'rpt_Rows.Item("Column13") = ""
                rpt_Rows.Item("Column13") = dtRow.Item("jobtype").ToString()
                'Sohail (18 Feb 2020) -- End
                'Sohail (24 Apr 2020) -- Start
                'NMB Enhancement # : Show list of jobs in which selected job is used as Reports to in Direct Report option on Job Listing Report.
                'rpt_Rows.Item("Column14") = "N/A"
                rpt_Rows.Item("Column14") = dtRow.Item("DirectReports").ToString()
                'Sohail (24 Apr 2020) -- End
                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
                'rpt_Rows.Item("Column15") = ""
                rpt_Rows.Item("Column15") = "• " & dtRow.Item("Job_Language").ToString().Replace(",", vbCrLf & "• ")
                'Sohail (18 Feb 2020) -- End
                'Hemant (15 Nov 2019) -- End
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                If dtRow.Item("Job_DirectReporttounkid").ToString().Trim.Length <= 0 Then
                    mstrDirectReportToJobunkid = "0"
                Else
                    mstrDirectReportToJobunkid = dtRow.Item("Job_DirectReporttounkid").ToString()
                End If
                If dtRow.Item("Job_InDirectReporttounkid").ToString().Trim.Length <= 0 Then
                    mstrInDirectReportToJobunkid = "0"
                Else
                    mstrInDirectReportToJobunkid = dtRow.Item("Job_InDirectReporttounkid").ToString()
                End If

            Next



            '****  Job Purpose

            rpt_JobPurpose = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_JobPurpose.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("description")
                rpt_JobPurpose.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            '****  Communication & Interaction

            rpt_CommunicationInteraction = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_CommunicationInteraction.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("experience_comment")
                'Sohail (18 Apr 2020) -- Start
                'NMB Enhancement # : On Job Report, for External, Pick data from working hours comment tab of job master.
                rpt_Rows.Item("Column2") = dtRow.Item("working_hrs")
                'Sohail (18 Apr 2020) -- End
                rpt_CommunicationInteraction.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            'Hemant (15 Nov 2019) -- End


            '****  KEY DUTIES AND RESPONSIBILITIES

            StrQ = " SELECT jobkeydutiesunkid,ISNULL(keyduties,'') AS Keyduties,jobunkid " & _
                       " FROM  hrjobkeyduties_tran  " & _
                       " WHERE isvoid = 0 AND jobunkid = @jobunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            dsKeyduties = objDataOperation.ExecQuery(StrQ, "KeyDuties")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_KeyDuties = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsKeyduties.Tables("KeyDuties").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_KeyDuties.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("jobunkid")
                rpt_Rows.Item("Column2") = "•" & " " & dtRow.Item("keyduties").ToString()
                rpt_KeyDuties.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next



            '***    QUALIFICATION   ***
            StrQ = "SELECT  " & _
                          " '" & Language.getMessage(mstrModuleName, 33, "Key Qualifications") & "' AS Particulars" & _
                          ", hrjob_qualification_tran.jobunkid " & _
                          ", cfcommon_master.masterunkid AS CategoryID " & _
                          ", cfcommon_master.code AS CategoryCode " & _
                          ", cfcommon_master.name AS CategoryName " & _
                          ", hrqualification_master.qualificationunkid AS ID " & _
                          ", hrqualification_master.qualificationcode AS Code " & _
                          ", hrqualification_master.qualificationname AS Name " & _
                          " FROM    hrjob_qualification_tran " & _
                          " LEFT JOIN hrjob_master ON hrjob_qualification_tran.jobunkid = hrjob_master.jobunkid " & _
                          "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjob_qualification_tran.qualificationgroupunkid AND mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
                          " LEFT JOIN hrqualification_master ON hrqualification_master.qualificationunkid = hrjob_qualification_tran.qualificationunkid " & _
                          "  WHERE   hrjob_master.isactive = 1 " & _
                          " AND cfcommon_master.isactive = 1 AND hrqualification_master.isactive = 1 " & _
                          " AND hrjob_qualification_tran.jobunkid = @jobunkid " & _
                          " AND hrjob_qualification_tran.isactive = 1 "

            'Pinkal (16-Nov-2021)-- NMB Staff Requisition Approval Enhancements. [AND hrjob_qualification_tran.isactive = 1]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            dsQualification = objDataOperation.ExecQuery(StrQ, "Qualification")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsQualification IsNot Nothing Then
                dtInputs.Merge(dsQualification.Tables(0), True)
            End If

            'rpt_Quali = New ArutiReport.Designer.dsArutiReport

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
            '    Dim rpt_Rows As DataRow
            '    rpt_Rows = rpt_Quali.Tables("ArutiTable").NewRow

            '    rpt_Rows.Item("Column1") = dtRow.Item("jobunkid")
            '    rpt_Rows.Item("Column2") = dtRow.Item("qualigrpid")
            '    rpt_Rows.Item("Column3") = dtRow.Item("qualigrpcode")
            '    rpt_Rows.Item("Column4") = dtRow.Item("qualigrpname") & " ( " & dtRow.Item("qualigrpcode") & " )"
            '    rpt_Rows.Item("Column5") = dtRow.Item("qualificationunkid")
            '    rpt_Rows.Item("Column6") = dtRow.Item("qualificationcode")
            '    rpt_Rows.Item("Column7") = dtRow.Item("qualificationname") & " ( " & dtRow.Item("qualificationcode") & " )"

            '    rpt_Quali.Tables("ArutiTable").Rows.Add(rpt_Rows)
            'Next



            '***    EXPERIENCE   ***

            StrQ = "SELECT  " & _
                          " '" & Language.getMessage(mstrModuleName, 34, "Experience") & "' AS Particulars" & _
                          ", jobunkid " & _
                          ", -1 AS CategoryID " & _
                          ", ' ' AS CategoryCode " & _
                          ", ' ' AS CategoryName " & _
                          ", -1  AS ID " & _
                          ", experience_comment AS Code " & _
                          ", CASE WHEN ISNULL(experience_year,0) > 0 OR ISNULL(experience_month,0) > 0 THEN " & _
                          "                 CONVERT(char(4), ISNULL(experience_year,0)) + '" & Language.getMessage(mstrModuleName, 39, "Years") & "' + '  '  + CONVERT(char(4), ISNULL(experience_month,0))  + '" & Language.getMessage(mstrModuleName, 40, "Months") & " '   " & _
                          " ELSE ' '  END AS Name " & _
                          " FROM  hrjob_master " & _
                          " WHERE hrjob_master.isactive = 1 AND hrjob_master. jobunkid = @jobunkid AND (ISNULL(experience_year,0) > 0 OR ISNULL(experience_month,0) > 0 )"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            dsExperience = objDataOperation.ExecQuery(StrQ, "Experience")

            If dsExperience IsNot Nothing Then
                dtInputs.Merge(dsExperience.Tables(0), True)
            End If

            '***    SKILL   ***
            StrQ = "SELECT  " & _
                          " '" & Language.getMessage(mstrModuleName, 35, "Skills") & "' AS Particulars" & _
                          ", hrjob_skill_tran.jobunkid " & _
                          ", cfcommon_master.masterunkid  AS CategoryID " & _
                          ", cfcommon_master.code AS CategoryCode " & _
                          ", cfcommon_master.name AS CategoryName " & _
                          ", hrskill_master.skillunkid  AS ID " & _
                          ", hrskill_master.skillcode  AS Code " & _
                          ", hrskill_master.skillname  AS Name " & _
                          " FROM  hrjob_skill_tran " & _
                          " LEFT JOIN hrjob_master ON hrjob_skill_tran.jobunkid = hrjob_master.jobunkid " & _
                          " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjob_skill_tran.skillcategoryunkid AND mastertype = " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & " " & _
                          " LEFT JOIN hrskill_master ON hrjob_skill_tran.skillunkid = hrskill_master.skillunkid " & _
                          " WHERE hrjob_master.isactive = 1 AND cfcommon_master.isactive = 1 " & _
                          " AND hrskill_master.isactive = 1 AND hrjob_skill_tran.jobunkid = @jobunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
            dsSkill = objDataOperation.ExecQuery(StrQ, "Skill")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            If dsSkill IsNot Nothing Then
                dtInputs.Merge(dsSkill.Tables(0), True)
            End If

            'rpt_Skill = New ArutiReport.Designer.dsArutiReport

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
            '    Dim rpt_Rows As DataRow
            '    rpt_Rows = rpt_Skill.Tables("ArutiTable").NewRow

            '    rpt_Rows.Item("Column1") = dtRow.Item("jobunkid")
            '    rpt_Rows.Item("Column2") = dtRow.Item("skillcategoryid")
            '    rpt_Rows.Item("Column3") = dtRow.Item("skillcategorycode")
            '    rpt_Rows.Item("Column4") = dtRow.Item("skillcategoryname") & " ( " & dtRow.Item("skillcategorycode") & " )"
            '    rpt_Rows.Item("Column5") = dtRow.Item("skillunkid")
            '    rpt_Rows.Item("Column6") = dtRow.Item("skillcode")
            '    rpt_Rows.Item("Column7") = dtRow.Item("skillname") & " ( " & dtRow.Item("skillcode") & " )"

            '    rpt_Skill.Tables("ArutiTable").Rows.Add(rpt_Rows)
            'Next



            '***** Competencies ****
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Report Then
                'Hemant (15 Nov 2019) -- End

                StrQ = "SELECT  " & _
                          " '" & Language.getMessage(mstrModuleName, 36, "Competencies") & "' AS Particulars" & _
                          " ,hrjobcompetencies_tran.jobunkid " & _
                          ", cfcommon_master.masterunkid AS CategoryID " & _
                          ", cfcommon_master.code AS CategoryCode " & _
                          ", cfcommon_master.name AS CategoryName " & _
                          ", hrjobcompetencies_tran.competenciesunkid AS ID " & _
                          ", '' AS Code " & _
                          ", hrassess_competencies_master.name AS Name " & _
                          " FROM  hrjobcompetencies_tran " & _
                          " LEFT JOIN hrjob_master ON hrjobcompetencies_tran.jobunkid = hrjob_master.jobunkid " & _
                          " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjobcompetencies_tran.competence_categoryunkid AND mastertype = " & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & _
                          " LEFT JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrjobcompetencies_tran.competenciesunkid AND hrassess_competencies_master.isactive = 1 " & _
                          " WHERE hrjobcompetencies_tran.isvoid = 0 AND hrjob_master.isactive = 1 " & _
                          " AND cfcommon_master.isactive = 1  AND hrjobcompetencies_tran. jobunkid = @jobunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                dsCompetencies = objDataOperation.ExecQuery(StrQ, "Competencies")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsCompetencies IsNot Nothing Then
                    dtInputs.Merge(dsCompetencies.Tables(0), True)
                End If
            End If 'Hemant (15 Nov 2019)

            rpt_Inputs = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dtInputs.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Inputs.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = "•" & " " & dtRow.Item("Particulars")
                rpt_Rows.Item("Column2") = "•" & " " & dtRow.Item("Name").ToString()
                rpt_Rows.Item("Column3") = "•" & " " & dtRow.Item("Code").ToString()
                rpt_Inputs.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                StrQ = "SELECT" & _
                       " job_level " & _
                       ", ISNULL(hrgrade_master.name, '') AS Job_Grade " & _
                       " FROM hrjob_master " & _
                       " LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hrjob_master.jobgradeunkid " & _
                       " WHERE jobunkid = @jobunkid "
                'Sohail (17 Apr 2020) - [LEFT JOIN hrgrade_master]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                dsJobEvaluation = objDataOperation.ExecQuery(StrQ, "JobEvaluation")


                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                rpt_OtherDimension = New ArutiReport.Designer.dsArutiReport


                For Each dtRow As DataRow In dsJobEvaluation.Tables(0).Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_OtherDimension.Tables("ArutiTable").NewRow
                    'Sohail (17 Apr 2020) -- Start
                    'NMB Issue # : On Job Report, for Confirmed job level system should pick data from Grade mapped on Job master.
                    'rpt_Rows.Item("Column1") = dtRow.Item("job_level").ToString
                    rpt_Rows.Item("Column1") = dtRow.Item("Job_Grade").ToString
                    'Sohail (17 Apr 2020) -- End
                    rpt_OtherDimension.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            Else
                'Hemant (15 Nov 2019) -- End
                StrQ = " SELECT  " & _
                           " '" & Language.getMessage(mstrModuleName, 37, "Direct Report To") & "' AS Particulars" & _
                           ", Employeecode,Employee,jobunkid,effectivedate  " & _
                           " FROM ( " & _
                           "                SELECT " & _
                           "                 ISNULL(hremployee_master.employeecode,'') AS Employeecode " & _
                           "                , ISNULL(hremployee_master.firstname,'') + ' ' +  ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                           "                ,hremployee_categorization_tran.jobunkid " & _
                           "                , effectivedate " & _
                           "                , ROW_NUMBER () over (PARTITION BY hremployee_categorization_tran.employeeunkid,hremployee_categorization_tran.jobunkid order by effectivedate DESC   ) as Srno " & _
                           "                 from hremployee_categorization_tran " & _
                           "                LEFT JOIN hremployee_master ON hremployee_categorization_tran.employeeunkid = hremployee_master.employeeunkid " & _
                           "                WHERe isvoid =0  AND convert(char(8),hremployee_categorization_tran.effectivedate,112) <= @employeeAsonDate " & _
                           "            )  as A where a.Srno = 1 AND A.jobunkid in (" & mstrDirectReportToJobunkid & ")"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeAsonDate)
                dsReportTo = objDataOperation.ExecQuery(StrQ, "ReportTo")


                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                StrQ = " SELECT  " & _
                           " '" & Language.getMessage(mstrModuleName, 38, "InDirect Report To") & "' AS Particulars" & _
                           ", Employeecode,Employee,jobunkid,effectivedate  " & _
                           " FROM ( " & _
                           "                SELECT " & _
                           "                 ISNULL(hremployee_master.employeecode,'') AS Employeecode " & _
                           "                , ISNULL(hremployee_master.firstname,'') + ' ' +  ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                           "                ,hremployee_categorization_tran.jobunkid " & _
                           "                , effectivedate " & _
                           "                , ROW_NUMBER () over (PARTITION BY hremployee_categorization_tran.employeeunkid,hremployee_categorization_tran.jobunkid order by effectivedate DESC   ) as Srno " & _
                           "                 from hremployee_categorization_tran " & _
                           "                LEFT JOIN hremployee_master ON hremployee_categorization_tran.employeeunkid = hremployee_master.employeeunkid " & _
                           "                WHERe isvoid =0  AND convert(char(8),hremployee_categorization_tran.effectivedate,112) <= @employeeAsonDate " & _
                           "            )  as A where a.Srno = 1 AND A.jobunkid in (" & mstrInDirectReportToJobunkid & ")"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeAsonDate)
                Dim dsInDirectReportTo As DataSet = objDataOperation.ExecQuery(StrQ, "InDirectReportTo")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If


                If dsReportTo IsNot Nothing AndAlso dsInDirectReportTo IsNot Nothing Then
                    dsReportTo.Tables(0).Merge(dsInDirectReportTo.Tables(0), True)
                End If


                rpt_OtherDimension = New ArutiReport.Designer.dsArutiReport


                For Each dtRow As DataRow In dsReportTo.Tables(0).Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_OtherDimension.Tables("ArutiTable").NewRow
                    rpt_Rows.Item("Column1") = "•" & " " & dtRow.Item("Particulars")
                    rpt_Rows.Item("Column2") = "•" & " " & dtRow.Item("Employeecode").ToString()
                    rpt_Rows.Item("Column3") = dtRow.Item("Employee").ToString()
                    rpt_OtherDimension.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

            End If 'Hemant (15 Nov 2019)



            'objRpt = New ArutiReport.Designer.rptJobReport
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                objRpt = New ArutiReport.Designer.rptJobListingReport
            Else
                objRpt = New ArutiReport.Designer.rptNewJobReport
            End If
            'Hemant (15 Nov 2019) -- End


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'If ConfigParameter._Object._IsShowPreparedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 8, "Prepared By :"))
            '    Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            'End If

            'If ConfigParameter._Object._IsShowCheckedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 9, "Checked By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            'End If

            'If ConfigParameter._Object._IsShowApprovedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 10, "Approved By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            'End If

            'If ConfigParameter._Object._IsShowReceivedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 11, "Received By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            'End If


            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptSubJobPurpose").SetDataSource(rpt_JobPurpose)
            objRpt.Subreports("rptsubKeyDuties").SetDataSource(rpt_KeyDuties)
            objRpt.Subreports("rptSubQuaSkill_Competencies").SetDataSource(rpt_Inputs)
            objRpt.Subreports("rptSubOtherDimensions").SetDataSource(rpt_OtherDimension)
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                objRpt.Subreports("rptCommunication").SetDataSource(rpt_CommunicationInteraction)
            End If
            'Hemant (15 Nov 2019) -- End

            'Call ReportFunction.TextChange(objRpt, "txtJobReport", Language.getMessage(mstrModuleName, 12, "JOB REPORT"))
            'Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 13, "Job Title :"))
            'Call ReportFunction.TextChange(objRpt, "txtDateCreated", Language.getMessage(mstrModuleName, 14, "Date Created :"))
            'Call ReportFunction.TextChange(objRpt, "txtPosition", Language.getMessage(mstrModuleName, 15, "Position : "))
            'Call ReportFunction.TextChange(objRpt.Subreports("rptJobSkill"), "txtJobSkill", Language.getMessage(mstrModuleName, 16, "A:Job Skills"))
            'Call ReportFunction.TextChange(objRpt.Subreports("rptJobQualification"), "txtJobQuali", Language.getMessage(mstrModuleName, 16, "B:Job Qualification"))
            'Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 16, "C:Job Description:"))

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                Call ReportFunction.TextChange(objRpt, "txtRoleProfile", Language.getMessage(mstrModuleName, 54, "ROLE PROFILE"))
                Call ReportFunction.TextChange(objRpt, "txtRoleProfileDescripition", Language.getMessage(mstrModuleName, 55, "Please attach departmental Organizational Structure which shows the role position as an appendix to this Role Profile"))
                Call ReportFunction.TextChange(objRpt, "txtJobIdentification", Language.getMessage(mstrModuleName, 43, "JOB IDENTIFICATION"))
                Call ReportFunction.TextChange(objRpt, "txtUnit", Language.getMessage(mstrModuleName, 44, "UNIT"))
                'Sohail (18 Apr 2020) -- Start
                'NMB Enhancement # : On Job Report, for External, Pick data from working hours comment tab of job master.
                Call ReportFunction.TextChange(objRpt.Subreports("rptCommunication"), "txtInternal", Language.getMessage(mstrModuleName, 60, "1. Internal"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCommunication"), "txtExternal", Language.getMessage(mstrModuleName, 60, "2. External"))
                'Sohail (18 Apr 2020) -- End
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt, "txtJobDetail", Language.getMessage(mstrModuleName, 1, "JOB DETAILS"))

            End If 'Hemant (15 Nov 2019)
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 2, "JOB TITLE"))
            Call ReportFunction.TextChange(objRpt, "txtReportTo", Language.getMessage(mstrModuleName, 3, "REPORTS TO"))


            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt, "txtJobResponsibleFor", Language.getMessage(mstrModuleName, 4, "JOBS RESPONSIBLE FOR"))
            End If 'Hemant (15 Nov 2019)
            Call ReportFunction.TextChange(objRpt, "txtDeptProgram", Language.getMessage(mstrModuleName, 5, "DEPARTMENT / PROGRAM"))
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                Call ReportFunction.TextChange(objRpt, "txtTypeOfJob", Language.getMessage(mstrModuleName, 45, "TYPE OF JOB"))
                Call ReportFunction.TextChange(objRpt, "txtDirectReports", Language.getMessage(mstrModuleName, 46, "DIRECT REPORTS"))
                Call ReportFunction.TextChange(objRpt, "txtLanguageRequirements", Language.getMessage(mstrModuleName, 47, "LANGUAGE REQUIREMENTS"))
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt, "txtRegionLocation", Language.getMessage(mstrModuleName, 6, "REGION / LOCATION"))
                Call ReportFunction.TextChange(objRpt, "txtWorkingHrs", Language.getMessage(mstrModuleName, 7, "WORKING HOURS"))
                Call ReportFunction.TextChange(objRpt, "txtJobCreationDate", Language.getMessage(mstrModuleName, 8, "JOB CREATION DATE"))
                Call ReportFunction.TextChange(objRpt, "txtJobGrade", Language.getMessage(mstrModuleName, 9, "JOB GRADE"))
                Call ReportFunction.TextChange(objRpt, "txtNameJobHolder", Language.getMessage(mstrModuleName, 10, "NAME JOB HOLDER"))
                Call ReportFunction.TextChange(objRpt, "txtGradeJobHolder", Language.getMessage(mstrModuleName, 11, "GRADE JOB HOLDER"))
                Call ReportFunction.TextChange(objRpt, "txtStructure", Language.getMessage(mstrModuleName, 12, "ORGANISATION / DEPARTMENTAL STRUCTURE"))
                Call ReportFunction.TextChange(objRpt, "txtOrganisationalStruct", Language.getMessage(mstrModuleName, 13, "Organisational structure available"))
                Call ReportFunction.TextChange(objRpt, "txtDepartmentStruct", Language.getMessage(mstrModuleName, 14, "Departmental structure available"))
                Call ReportFunction.TextChange(objRpt, "txtOrgYes", Language.getMessage(mstrModuleName, 15, "Yes"))
                Call ReportFunction.TextChange(objRpt, "txtOrgNo", Language.getMessage(mstrModuleName, 16, "No"))
                Call ReportFunction.TextChange(objRpt, "txtDeptYes", Language.getMessage(mstrModuleName, 15, "Yes"))
                Call ReportFunction.TextChange(objRpt, "txtDeptNo", Language.getMessage(mstrModuleName, 16, "No"))

            End If 'Hemant (15 Nov 2019) 


            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                Call ReportFunction.TextChange(objRpt, "txtConfirmatioAgreement", Language.getMessage(mstrModuleName, 51, "CONFIRMATION ON AGREEMENT FOR THE ROLE"))
                Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 57, "Name: "))
                Call ReportFunction.TextChange(objRpt, "txtsignature", Language.getMessage(mstrModuleName, 58, "Signature: "))
                Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 59, "Date: "))
                Call ReportFunction.TextChange(objRpt, "txtAgreedByEmployee", Language.getMessage(mstrModuleName, 56, "AGREED BY EMPLOYEE:"))
                Call ReportFunction.TextChange(objRpt, "txtAgreedByLineManager", Language.getMessage(mstrModuleName, 52, "AGREED BY LINE MANAGER:"))
                Call ReportFunction.TextChange(objRpt, "txtAgreedByCHRO_HRBP", Language.getMessage(mstrModuleName, 53, "AGREED BY CHRO/HRBP:"))
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt, "txtAuthorisationDetail", Language.getMessage(mstrModuleName, 17, "AUTHORISATION DETAILS"))
                Call ReportFunction.TextChange(objRpt, "txtAuthorizeName", Language.getMessage(mstrModuleName, 18, "Name: "))
                Call ReportFunction.TextChange(objRpt, "txtsignature", Language.getMessage(mstrModuleName, 19, "Signature: "))
                Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 20, "Date: "))
                Call ReportFunction.TextChange(objRpt, "txtJobProfile", Language.getMessage(mstrModuleName, 21, "Job Profile confirmed (HR):"))
                Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 22, "Employee:"))
                Call ReportFunction.TextChange(objRpt, "txtManagerDirector", Language.getMessage(mstrModuleName, 23, "Manager / Director:"))
            End If  'Hemant (15 Nov 2019)

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubJobPurpose"), "txtAOutPut", Language.getMessage(mstrModuleName, 24, "A.OUTPUTS"))
            End If 'Hemant (15 Nov 2019)
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubJobPurpose"), "txtJobPurpose", Language.getMessage(mstrModuleName, 25, "JOB PURPOSE"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptsubKeyDuties"), "txtKeyDuties", Language.getMessage(mstrModuleName, 26, "DUTIES AND KEY RESPONSIBILITIES"))

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubQuaSkill_Competencies"), "txtInputs", Language.getMessage(mstrModuleName, 27, "B. INPUTS"))
            End If 'Hemant (15 Nov 2019)
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubQuaSkill_Competencies"), "txtKeyQuaSkillCom", Language.getMessage(mstrModuleName, 28, "KEY QUALIFICATIONS,EXPERIENCE, SKILLS & COMPETENCIES"))

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            If intJobReportTemplate = enJob_Report_Template.Job_Listing_Report Then
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOtherDimensions"), "txtJobEvaluation", Language.getMessage(mstrModuleName, 48, "JOB EVALUATION"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOtherDimensions"), "txtConfirmedJobLevel", Language.getMessage(mstrModuleName, 49, "CONFIRMED JOB LEVEL"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptCommunication"), "txtCommunicationInteraction", Language.getMessage(mstrModuleName, 50, "COMMUNICATION & INTERACTION"))
            Else
                'Hemant (15 Nov 2019) -- End
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOtherDimensions"), "txtOtherDimension", Language.getMessage(mstrModuleName, 29, "OTHER DIMENSIONS (if applicable)"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOtherDimensions"), "txtfinancial", Language.getMessage(mstrModuleName, 30, "FINANCIAL (e.g. Budget,turnover,expenses,assets,profit)"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOtherDimensions"), "txtEmployeeManage", Language.getMessage(mstrModuleName, 31, "EMPLOYEES MANAGED (direct/indirect)"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOtherDimensions"), "txtStackHolderManaged", Language.getMessage(mstrModuleName, 32, "STACKHOLDERS MANAGED"))
            End If 'Hemant (15 Nov 2019)



            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 41, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 42, "Printed Date :"))
            'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 19, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (03-Dec-2015) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "JOB DETAILS")
            Language.setMessage(mstrModuleName, 2, "JOB TITLE")
            Language.setMessage(mstrModuleName, 3, "REPORTS TO")
            Language.setMessage(mstrModuleName, 4, "JOBS RESPONSIBLE FOR")
            Language.setMessage(mstrModuleName, 5, "DEPARTMENT / PROGRAM")
            Language.setMessage(mstrModuleName, 6, "REGION / LOCATION")
            Language.setMessage(mstrModuleName, 7, "WORKING HOURS")
            Language.setMessage(mstrModuleName, 8, "JOB CREATION DATE")
            Language.setMessage(mstrModuleName, 9, "JOB GRADE")
            Language.setMessage(mstrModuleName, 10, "NAME JOB HOLDER")
            Language.setMessage(mstrModuleName, 11, "GRADE JOB HOLDER")
            Language.setMessage(mstrModuleName, 12, "ORGANISATION / DEPARTMENTAL STRUCTURE")
            Language.setMessage(mstrModuleName, 13, "Organisational structure available")
            Language.setMessage(mstrModuleName, 14, "Departmental structure available")
            Language.setMessage(mstrModuleName, 15, "Yes")
            Language.setMessage(mstrModuleName, 16, "No")
            Language.setMessage(mstrModuleName, 17, "AUTHORISATION DETAILS")
            Language.setMessage(mstrModuleName, 18, "Name:")
            Language.setMessage(mstrModuleName, 19, "Signature:")
            Language.setMessage(mstrModuleName, 20, "Date:")
            Language.setMessage(mstrModuleName, 21, "Job Profile confirmed (HR):")
            Language.setMessage(mstrModuleName, 22, "Employee:")
            Language.setMessage(mstrModuleName, 23, "Manager / Director:")
            Language.setMessage(mstrModuleName, 24, "A.OUTPUTS")
            Language.setMessage(mstrModuleName, 25, "JOB PURPOSE")
            Language.setMessage(mstrModuleName, 26, "DUTIES AND KEY RESPONSIBILITIES")
            Language.setMessage(mstrModuleName, 27, "B. INPUTS")
            Language.setMessage(mstrModuleName, 28, "KEY QUALIFICATIONS,EXPERIENCE, SKILLS & COMPETENCIES")
            Language.setMessage(mstrModuleName, 29, "OTHER DIMENSIONS (if applicable)")
            Language.setMessage(mstrModuleName, 30, "FINANCIAL (e.g. Budget,turnover,expenses,assets,profit)")
            Language.setMessage(mstrModuleName, 31, "EMPLOYEES MANAGED (direct/indirect)")
            Language.setMessage(mstrModuleName, 32, "STACKHOLDERS MANAGED")
            Language.setMessage(mstrModuleName, 33, "Key Qualifications")
            Language.setMessage(mstrModuleName, 34, "Experience")
            Language.setMessage(mstrModuleName, 35, "Skills")
            Language.setMessage(mstrModuleName, 36, "Competencies")
            Language.setMessage(mstrModuleName, 37, "Direct Report To")
            Language.setMessage(mstrModuleName, 38, "InDirect Report To")
            Language.setMessage(mstrModuleName, 39, "Years")
            Language.setMessage(mstrModuleName, 40, "Months")
            Language.setMessage(mstrModuleName, 41, "Printed By :")
            Language.setMessage(mstrModuleName, 42, "Printed Date :")
            Language.setMessage(mstrModuleName, 43, "JOB IDENTIFICATION")
            Language.setMessage(mstrModuleName, 44, "UNIT")
            Language.setMessage(mstrModuleName, 45, "TYPE OF JOB")
            Language.setMessage(mstrModuleName, 46, "DIRECT REPORTS")
            Language.setMessage(mstrModuleName, 47, "LANGUAGE REQUIREMENTS")
            Language.setMessage(mstrModuleName, 48, "JOB EVALUATION")
            Language.setMessage(mstrModuleName, 49, "CONFIRMED JOB LEVEL")
            Language.setMessage(mstrModuleName, 50, "COMMUNICATION & INTERACTION")
            Language.setMessage(mstrModuleName, 51, "CONFIRMATION ON AGREEMENT FOR THE ROLE")
            Language.setMessage(mstrModuleName, 52, "AGREED BY LINE MANAGER:")
            Language.setMessage(mstrModuleName, 53, "AGREED BY CHRO/HRBP:")
            Language.setMessage(mstrModuleName, 54, "ROLE PROFILE")
            Language.setMessage(mstrModuleName, 55, "Please attach departmental Organizational Structure which shows the role position as an appendix to this Role Profile")
            Language.setMessage(mstrModuleName, 56, "AGREED BY EMPLOYEE:")
            Language.setMessage(mstrModuleName, 57, "Name:")
            Language.setMessage(mstrModuleName, 58, "Signature:")
            Language.setMessage(mstrModuleName, 59, "Date:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
