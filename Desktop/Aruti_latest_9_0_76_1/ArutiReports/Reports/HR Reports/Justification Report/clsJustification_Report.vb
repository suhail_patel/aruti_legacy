'************************************************************************************************************************************
'Class Name : clsJustification_Report.vb
'Purpose    :
'Date       : 07/03/2020
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsJustification_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsJustification_Report"
    Private mstrReportId As String = enArutiReport.Justification_Report  '221
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportType As String = ""
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportType() As String
        Set(ByVal value As String)
            mstrReportType = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try


            If mintReportTypeId = 1 Then
                Me._FilterQuery &= " AND TRN.datetypeunkid = @datetypeunkid "
                objDataOperation.AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enEmp_Dates_Transaction.DT_TERMINATION)
            End If

            If mdtFromDate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Start Date: ") & " " & mdtFromDate.Date & " "
            End If

            If mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "End Date: ") & " " & mdtToDate.Date & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND  ISNULL(hremployee_master.employeeunkid, 0) = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= "ORDER BY lvleaveform.formno,lvapproverlevel_master.priority," & mstrAnalysis_OrderBy_GName & ", " & Me.OrderByQuery
                End If
            Else
                mstrOrderByQuery &= Me.OrderByQuery
            End If


            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 1, "Code")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_TerminatedEmployeeReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objDataOperation = New clsDataOperation

            Dim dsCompany As New DataSet

            objDataOperation.ClearParameters()


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            StrQ = "SELECT ISNULL(hremployee_master.employeeunkid, 0) AS employeeunkid " & _
                                 ", ISNULL(hremployee_master.employeecode, '') AS Code " & _
                                 ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                                 ", ISNULL(dept.name,'') As Department " & _
                                 ", ISNULL(sgm.name,'') AS [Section Group] " & _
                                 ", ISNULL(sm.name,'') AS [Section] " & _
                                 ", ISNULL(cgm.name,'') AS [Class Group] " & _
                                 ", ISNULL(cm.name,'') AS [Class] " & _
                                 ", TRN.actualdate AS [Actual Date] " & _
                                 ", TRN.EffectiveDate AS [Effective Date] " & _
                                 ", TRN.datetypeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " LEFT JOIN " & _
                         "( " & _
                         "    SELECT " & _
                         "         departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                         "    FROM hremployee_transfer_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate " & _
                         ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                         " LEFT JOIN hrdepartment_master AS dept ON T.departmentunkid = dept.departmentunkid " & _
                         " LEFT JOIN hrsectiongroup_master AS sgm ON T.sectiongroupunkid = sgm.sectiongroupunkid  " & _
                         " LEFT JOIN hrsection_master AS sm ON T.sectionunkid = sm.sectionunkid  " & _
                         " LEFT JOIN hrclassgroup_master AS cgm ON T.classgroupunkid = cgm.classgroupunkid  " & _
                         " LEFT JOIN hrclasses_master AS cm ON T.classunkid = cm.classesunkid  " & _
                         " JOIN " & _
                         "( " & _
                         "   SELECT " & _
                         "		 TERM.employeeunkid " & _
                         "		,TERM.empl_enddate " & _
                         "		,TERM.termination_from_date " & _
                         "		,TERM.EffectiveDate " & _
                         "		,TERM.isexclude_payroll " & _
                         "		,TERM.changereasonunkid " & _
                         "		,TERM.actualdate " & _
                         "		,TERM.datetypeunkid " & _
                         "   FROM " & _
                         "   ( " & _
                         "       SELECT " & _
                         "            hremployee_dates_tran.employeeunkid  " & _
                         "           ,CONVERT(CHAR(8),hremployee_dates_tran.date1,112) AS empl_enddate " & _
                         "           ,CONVERT(CHAR(8),hremployee_dates_tran.date2,112) AS termination_from_date " & _
                         "           ,CONVERT(CHAR(8),hremployee_dates_tran.effectivedate,112) AS EffectiveDate " & _
                         "           ,hremployee_dates_tran.isexclude_payroll " & _
                         "			 ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS Rno " & _
                         "           ,hremployee_dates_tran.changereasonunkid " & _
                         "           ,CONVERT(CHAR(8),hremployee_dates_tran.actualdate,112) As actualdate " & _
                         "           ,hremployee_dates_tran.datetypeunkid " & _
                         "       FROM hremployee_dates_tran " & _
                         "		 WHERE hremployee_dates_tran.isvoid = 0 AND hremployee_dates_tran.datetypeunkid = @datetypeunkid " & _
                         "      /*AND CONVERT(CHAR(8),hremployee_dates_tran.effectivedate,112) <= @EndDate*/ AND hremployee_dates_tran.actualdate IS NOT NULL " & _
                         "      AND CONVERT(CHAR(8),hremployee_dates_tran.actualdate,112) BETWEEN @StartDate AND @EndDate AND hremployee_dates_tran.rehiretranunkid <= 0 " & _
                         "   ) AS TERM WHERE 1 = 1 /*TERM.Rno = 1*/  " & _
                         ") AS TRN  ON TRN.employeeunkid = hremployee_master.employeeunkid "


            'S.SANDEEP |11-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT
            'on justification termination report - do not consider actual date which has been set on rehire screen. Only consider dates from termination screen
            'AND hremployee_dates_tran.rehiretranunkid <= 0 -- ADDED
            'S.SANDEEP |11-MAR-2020| -- END

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE 1 = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            Dim mdtTableExcel As DataTable = New DataView(dsList.Tables(0), "", "Code,Employee", DataViewRowState.CurrentRows).ToTable


            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("datetypeunkid") Then
                mdtTableExcel.Columns.Remove("datetypeunkid")
            End If

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If


            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------



            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
                If mdtTableExcel.Rows(i)("Actual Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Actual Date") = eZeeDate.convertDate(mdtTableExcel.Rows(i)("Actual Date").ToString()).ToShortDateString()
                End If
                If mdtTableExcel.Rows(i)("Effective Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Effective Date") = eZeeDate.convertDate(mdtTableExcel.Rows(i)("Effective Date").ToString()).ToShortDateString()
                End If
            Next


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("Code").Caption = Language.getMessage(mstrModuleName, 1, "Code")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 2, "Employee")
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 3, "Department")
            mdtTableExcel.Columns("Section Group").Caption = Language.getMessage(mstrModuleName, 4, "Section Group")
            mdtTableExcel.Columns("Section").Caption = Language.getMessage(mstrModuleName, 5, "Section")
            mdtTableExcel.Columns("Class Group").Caption = Language.getMessage(mstrModuleName, 6, "Class Group")
            mdtTableExcel.Columns("Class").Caption = Language.getMessage(mstrModuleName, 7, "Class")
            mdtTableExcel.Columns("Actual Date").Caption = Language.getMessage(mstrModuleName, 8, "Actual Date")
            mdtTableExcel.Columns("Effective Date").Caption = Language.getMessage(mstrModuleName, 9, "Effective Date")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " " & mstrReportType, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_TerminatedEmployeeReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_RehiredEmployeeReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objDataOperation = New clsDataOperation

            Dim dsCompany As New DataSet

            objDataOperation.ClearParameters()


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            StrQ = "SELECT ISNULL(hremployee_master.employeeunkid, 0) AS employeeunkid " & _
                                 ", ISNULL(hremployee_master.employeecode, '') AS Code " & _
                                 ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                                 ", ISNULL(dept.name,'') As Department " & _
                                 ", ISNULL(sgm.name,'') AS [Section Group] " & _
                                 ", ISNULL(sm.name,'') AS [Section] " & _
                                 ", ISNULL(cgm.name,'') AS [Class Group] " & _
                                 ", ISNULL(cm.name,'') AS [Class] " & _
                                 ", ERH.actualdate AS [Actual Date] " & _
                                 ", ERH.EffectiveDate AS [Effective Date] "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM hremployee_master " & _
                         " LEFT JOIN " & _
                         "( " & _
                         "    SELECT " & _
                         "         departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                         "    FROM hremployee_transfer_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EndDate " & _
                         ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                         " LEFT JOIN hrdepartment_master AS dept ON T.departmentunkid = dept.departmentunkid " & _
                         " LEFT JOIN hrsectiongroup_master AS sgm ON T.sectiongroupunkid = sgm.sectiongroupunkid  " & _
                         " LEFT JOIN hrsection_master AS sm ON T.sectionunkid = sm.sectionunkid  " & _
                         " LEFT JOIN hrclassgroup_master AS cgm ON T.classgroupunkid = cgm.classgroupunkid  " & _
                         " LEFT JOIN hrclasses_master AS cm ON T.classunkid = cm.classesunkid  " & _
                         " JOIN " & _
                         " ( " & _
                         "   SELECT " & _
                         "		 RH.RHEmpId " & _
                         "		,RH.reinstatment_date " & _
                         "		,RH.EffectiveDate " & _
                         "		,RH.changereasonunkid AS RH_changereasonunkid " & _
                         "		,RH.actualdate " & _
                         "   FROM " & _
                         "   ( " & _
                         "       SELECT " & _
                         "            ERT.employeeunkid AS RHEmpId " & _
                         "           ,CONVERT(CHAR(8),ERT.reinstatment_date,112) AS reinstatment_date " & _
                         "           ,CONVERT(CHAR(8),ERT.effectivedate,112) AS EffectiveDate " & _
                         "           ,ROW_NUMBER()OVER(PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " & _
                         "           ,ERT.changereasonunkid " & _
                         "           ,CONVERT(CHAR(8),ERT.actualdate,112) AS actualdate " & _
                         "       FROM hremployee_rehire_tran AS ERT " & _
                         "       WHERE isvoid = 0  /*AND CONVERT(CHAR(8),ERT.effectivedate,112) <= @EndDate */ " & _
                         "       AND ERT.actualdate IS NOT NULL  AND CONVERT(CHAR(8),ERT.actualdate,112) BETWEEN @StartDate AND @EndDate" & _
                         "   ) AS RH WHERE 1 =1  /*RH.Rno = 1*/   " & _
                         ") AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = New DataView(dsList.Tables(0), "", "Code,Employee", DataViewRowState.CurrentRows).ToTable


            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------


            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
                If mdtTableExcel.Rows(i)("Actual Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Actual Date") = eZeeDate.convertDate(mdtTableExcel.Rows(i)("Actual Date").ToString()).ToShortDateString()
                End If
                If mdtTableExcel.Rows(i)("Effective Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Effective Date") = eZeeDate.convertDate(mdtTableExcel.Rows(i)("Effective Date").ToString()).ToShortDateString()
                End If
            Next


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("Code").Caption = Language.getMessage(mstrModuleName, 1, "Code")
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 2, "Employee")
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 3, "Department")
            mdtTableExcel.Columns("Section Group").Caption = Language.getMessage(mstrModuleName, 4, "Section Group")
            mdtTableExcel.Columns("Section").Caption = Language.getMessage(mstrModuleName, 5, "Section")
            mdtTableExcel.Columns("Class Group").Caption = Language.getMessage(mstrModuleName, 6, "Class Group")
            mdtTableExcel.Columns("Class").Caption = Language.getMessage(mstrModuleName, 7, "Class")
            mdtTableExcel.Columns("Actual Date").Caption = Language.getMessage(mstrModuleName, 8, "Actual Date")
            mdtTableExcel.Columns("Effective Date").Caption = Language.getMessage(mstrModuleName, 9, "Effective Date")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " " & mstrReportType, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_RehiredEmployeeReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Department")
            Language.setMessage(mstrModuleName, 4, "Section Group")
            Language.setMessage(mstrModuleName, 5, "Section")
            Language.setMessage(mstrModuleName, 6, "Class Group")
            Language.setMessage(mstrModuleName, 7, "Class")
            Language.setMessage(mstrModuleName, 8, "Actual Date")
            Language.setMessage(mstrModuleName, 9, "Effective Date")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By")
            Language.setMessage(mstrModuleName, 13, "Start Date:")
            Language.setMessage(mstrModuleName, 14, "End Date:")
            Language.setMessage(mstrModuleName, 15, "Employee:")
            Language.setMessage(mstrModuleName, 16, " Order By :")
            Language.setMessage(mstrModuleName, 17, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
