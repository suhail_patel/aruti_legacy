'************************************************************************************************************************************
'Class Name : frmEmployeeAssetRegister.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSC_Planning_Report

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmBSC_Planning_Report"
    Private objBSCPlanning As clsBSC_Planning_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty

    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing

    'S.SANDEEP [05-JUN-2017] -- START
    'ISSUE/ENHANCEMENT : PLANNING REPORT
    Dim dvEmpView As DataView
    Dim mdtEmployee As DataTable
    'S.SANDEEP [05-JUN-2017] -- END


#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        objBSCPlanning = New clsBSC_Planning_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        _Show_AdvanceFilter = True
        objBSCPlanning.SetDefaultValue()
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        'S.SANDEEP [ 22 OCT 2013 ] -- START
        Dim objMaster As New clsMasterData
        'S.SANDEEP [ 22 OCT 2013 ] -- END
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [05-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : PLANNING REPORT
            Dim dCol As New DataColumn
            dCol.ColumnName = "ischeck"
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtEmployee = dsList.Tables(0).Copy
            mdtEmployee.Columns.Add(dCol)

            Dim xr As DataRow() = mdtEmployee.Select("employeeunkid = 0")
            If xr.Length > 0 Then
                mdtEmployee.Rows.Remove(xr(0))
            End If
            mdtEmployee.AcceptChanges()
            dgvAEmployee.AutoGenerateColumns = False
            dgcolhEName.DataPropertyName = "EmpCodeName"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            objdgcolhECheck.DataPropertyName = "ischeck"
            dvEmpView = mdtEmployee.DefaultView()
            dgvAEmployee.DataSource = dvEmpView
            'S.SANDEEP [05-JUN-2017] -- END




            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 1)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            With cboStatus
                .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 1, "Submitted For Approval"), 1))
                'S.SANDEEP |12-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                '.Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 2, "Final Saved"), 2))
                .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 100, "Final Approved"), 2))
                'S.SANDEEP |12-MAR-2019| -- END
                .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 3, "Opened For Changes"), 3))
                .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 4, "Not Submitted For Approval"), 4))
                .Items.Add(New ComboBoxValue(Language.getMessage("clsBSC_Planning_Report", 5, "Not Planned"), 999))
                .SelectedIndex = 0
            End With

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            dsList = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 1
            End With
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [05-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : PLANNING REPORT
            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 101, "BSC Planning Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 102, "Employee Planning Report"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP [05-JUN-2017] -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsList = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedIndex = 0

            dtpS_Dt_From.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpS_Dt_To.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpS_Dt_From.Checked = False
            dtpS_Dt_To.Checked = False

            cboStatus.SelectedIndex = 0
            chkIncludeSummary.Checked = True

            objBSCPlanning.setDefaultOrderBy(0)
            txtOrderBy.Text = objBSCPlanning.OrderByDisplay

            mstrAdvanceFilter = ""

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            cboAppointment.SelectedValue = 1
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'S.SANDEEP [05-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : PLANNING REPORT
            chkShowComputedScore.Checked = False
            'S.SANDEEP [05-JUN-2017] -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Call chkShowComputedScore_CheckedChanged(New Object, New EventArgs())
            'S.SANDEEP |27-MAY-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objBSCPlanning.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            'S.SANDEEP [05-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : PLANNING REPORT
            'If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
            '    If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
            '        dtpDate2.Focus()
            '        Return False
            '    ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
            '        dtpDate1.Focus()
            '        Return False
            '    ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
            '        If dtpDate2.Value.Date < dtpDate1.Value.Date Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Appointment To Date cannot be less than Appointment From Date."), enMsgBoxStyle.Information)
            '            dtpDate2.Focus()
            '            Return False
            '        End If
            '    End If
            'End If
            ''S.SANDEEP [ 22 OCT 2013 ] -- END

            'objBSCPlanning.SetDefaultValue()

            'objBSCPlanning._EmployeeId = cboEmployee.SelectedValue
            'objBSCPlanning._EmployeeName = cboEmployee.Text

            'objBSCPlanning._PeriodId = cboPeriod.SelectedValue
            'objBSCPlanning._PeriodName = cboPeriod.Text
            'objBSCPlanning._Period_St_Date = mdtPeriodStart
            'objBSCPlanning._Period_Ed_Date = mdtPeriodEnd

            'If dtpS_Dt_From.Checked = True AndAlso dtpS_Dt_To.Checked = True Then
            '    objBSCPlanning._FilterDate1 = dtpS_Dt_From.Value.Date
            '    objBSCPlanning._FilterDate2 = dtpS_Dt_To.Value.Date
            'End If

            'objBSCPlanning._StatusId = CType(cboStatus.SelectedItem, ComboBoxValue).Value
            'objBSCPlanning._StatusName = cboStatus.Text

            'objBSCPlanning._IncludeSummary = chkIncludeSummary.Checked

            'objBSCPlanning._ViewByIds = mstrStringIds
            'objBSCPlanning._ViewIndex = mintViewIdx
            'objBSCPlanning._ViewByName = mstrStringName

            'objBSCPlanning._Analysis_Fields = mstrAnalysis_Fields
            'objBSCPlanning._Analysis_Join = mstrAnalysis_Join
            'objBSCPlanning._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objBSCPlanning._Report_GroupName = mstrReport_GroupName

            'objBSCPlanning._Advance_Filter = mstrAdvanceFilter

            ''S.SANDEEP [ 25 SEPT 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'objBSCPlanning._ExcludeInactiveEmployee = chkExcludeInactive.Checked
            ''S.SANDEEP [ 25 SEPT 2013 ] -- END

            ''S.SANDEEP [ 22 OCT 2013 ] -- START
            'objBSCPlanning._AppointmentTypeId = cboAppointment.SelectedValue
            'objBSCPlanning._AppointmentTypeName = cboAppointment.Text
            'If dtpDate1.Checked = True Then
            '    objBSCPlanning._Date1 = dtpDate1.Value.Date
            'End If

            'If dtpDate2.Visible = True Then
            '    If dtpDate2.Checked = True Then
            '        objBSCPlanning._Date2 = dtpDate2.Value.Date
            '    End If
            'End If
            ''S.SANDEEP [ 22 OCT 2013 ] -- END

            ''S.SANDEEP [ 03 NOV 2014 ] -- START
            'objBSCPlanning._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked
            ''S.SANDEEP [ 03 NOV 2014 ] -- END

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objBSCPlanning._Database_Start_Date = FinancialYear._Object._Database_Start_Date
            ''Sohail (21 Aug 2015) -- End


            objBSCPlanning._PeriodId = cboPeriod.SelectedValue
            objBSCPlanning._PeriodName = cboPeriod.Text
            objBSCPlanning._Period_St_Date = mdtPeriodStart
            objBSCPlanning._Period_Ed_Date = mdtPeriodEnd
            objBSCPlanning._ReportTypeId = cboReportType.SelectedIndex
            objBSCPlanning._ReportTypeName = cboReportType.Text
            If cboReportType.SelectedIndex = 0 Then
            If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
                If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
                    dtpDate2.Focus()
                    Return False
                ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
                    dtpDate1.Focus()
                    Return False
                ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                    If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Appointment To Date cannot be less than Appointment From Date."), enMsgBoxStyle.Information)
                        dtpDate2.Focus()
                        Return False
                    End If
                End If
            End If
            objBSCPlanning._EmployeeId = cboEmployee.SelectedValue
            objBSCPlanning._EmployeeName = cboEmployee.Text
            If dtpS_Dt_From.Checked = True AndAlso dtpS_Dt_To.Checked = True Then
                objBSCPlanning._FilterDate1 = dtpS_Dt_From.Value.Date
                objBSCPlanning._FilterDate2 = dtpS_Dt_To.Value.Date
            End If
            objBSCPlanning._StatusId = CType(cboStatus.SelectedItem, ComboBoxValue).Value
            objBSCPlanning._StatusName = cboStatus.Text
            objBSCPlanning._IncludeSummary = chkIncludeSummary.Checked
            objBSCPlanning._ViewByIds = mstrStringIds
            objBSCPlanning._ViewIndex = mintViewIdx
            objBSCPlanning._ViewByName = mstrStringName
            objBSCPlanning._Analysis_Fields = mstrAnalysis_Fields
            objBSCPlanning._Analysis_Join = mstrAnalysis_Join
            objBSCPlanning._Analysis_OrderBy = mstrAnalysis_OrderBy
            objBSCPlanning._Report_GroupName = mstrReport_GroupName
            objBSCPlanning._Advance_Filter = mstrAdvanceFilter
            objBSCPlanning._ExcludeInactiveEmployee = chkExcludeInactive.Checked
            objBSCPlanning._AppointmentTypeId = cboAppointment.SelectedValue
            objBSCPlanning._AppointmentTypeName = cboAppointment.Text
            If dtpDate1.Checked = True Then
                objBSCPlanning._Date1 = dtpDate1.Value.Date
            End If
            If dtpDate2.Visible = True Then
                If dtpDate2.Checked = True Then
                    objBSCPlanning._Date2 = dtpDate2.Value.Date
                End If
            End If
            objBSCPlanning._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked
                objBSCPlanning._Database_Start_Date = FinancialYear._Object._Database_Start_Date

            ElseIf cboReportType.SelectedIndex = 1 Then
                mdtEmployee.AcceptChanges()
                Dim dtmp() As DataRow = Nothing
                dtmp = mdtEmployee.Select("ischeck = true")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, Please check atleast one employee to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
                objBSCPlanning._SeletedEmpids = dtmp.ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("employeeunkid"), Function(row) row.Field(Of String)("EmpCodeName"))
                objBSCPlanning._CascadingTypeId = ConfigParameter._Object._CascadingTypeId
                objBSCPlanning._ShowComputedScore = chkShowComputedScore.Checked
            End If
            'S.SANDEEP [05-JUN-2017] -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            objBSCPlanning._IsCalibrationSettingActive = ConfigParameter._Object._IsCalibrationSettingActive
            objBSCPlanning._DisplayScoreName = cboScoreOption.Text
            objBSCPlanning._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmBSC_Planning_Report_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBSCPlanning = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmBSC_Planning_Report_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objBSCPlanning._ReportName
            Me._Message = objBSCPlanning._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub frmBSC_Planning_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmBSC_Planning_Report_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objBSCPlanning.generateReport(0, enPrintAction.None, e.Type)
            objBSCPlanning.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objBSCPlanning.generateReport(0, e.Type, enExportAction.None)
            objBSCPlanning.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Planning_Report_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Planning_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBSC_Planning_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsBSC_Planning_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmBSC_Planning_Report_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try

            'S.SANDEEP [ 25 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet
            'S.SANDEEP [ 25 SEPT 2013 ] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim dtStartDate, dtEndDate As Date
            dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            'S.SANDEEP [04 JUN 2015] -- END

            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''S.SANDEEP [ 25 SEPT 2013 ] -- START
                ''ENHANCEMENT : TRA CHANGES
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , mdtPeriodStart, mdtPeriodEnd)
                'Else
                '    dsList = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                'End If
                ''S.SANDEEP [ 25 SEPT 2013 ] -- END

                dtStartDate = mdtPeriodStart
                dtEndDate = mdtPeriodEnd

                'S.SANDEEP [04 JUN 2015] -- END

            Else
                mdtPeriodStart = Nothing
                mdtPeriodEnd = Nothing

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''S.SANDEEP [ 10 SEPT 2013 ] -- START
                ''ENHANCEMENT : TRA CHANGES
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
                'Else
                '    dsList = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                'End If
                ''S.SANDEEP [ 10 SEPT 2013 ] -- END

                dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

                'S.SANDEEP [04 JUN 2015] -- END
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            dtStartDate, _
                                            dtEndDate, _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 25 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmployee = Nothing
            'S.SANDEEP [ 25 SEPT 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objBSCPlanning.setOrderBy(0)
            txtOrderBy.Text = objBSCPlanning.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 22 OCT 2013 ] -- END


    'S.SANDEEP [ 06 OCT 2014 ] -- START
    Private Sub chkConsiderEmployeeTermination_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConsiderEmployeeTermination.CheckedChanged
        Try
            If chkConsiderEmployeeTermination.Checked = True Then
                chkExcludeInactive.Checked = False : chkExcludeInactive.Enabled = False
            Else
                chkExcludeInactive.Checked = True : chkExcludeInactive.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkConsiderEmployeeTermination_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 06 OCT 2014 ] -- END

    'S.SANDEEP [05-JUN-2017] -- START
    'ISSUE/ENHANCEMENT : PLANNING REPORT
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                Panel1.Visible = True : tblpAssessorEmployee.Visible = False : lnkSetAnalysis.Visible = True : chkShowComputedScore.Visible = False
            ElseIf cboReportType.SelectedIndex = 1 Then
                Panel1.Visible = False : tblpAssessorEmployee.Visible = True : lnkSetAnalysis.Visible = False : chkShowComputedScore.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dvEmpView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvEmpView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvEmpView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dvEmpView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [05-JUN-2017] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Private Sub chkShowComputedScore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowComputedScore.CheckedChanged
        Try
            cboScoreOption.Enabled = chkShowComputedScore.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowComputedScore_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |27-MAY-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkIncludeSummary.Text = Language._Object.getCaption(Me.chkIncludeSummary.Name, Me.chkIncludeSummary.Text)
            Me.lblSDateTo.Text = Language._Object.getCaption(Me.lblSDateTo.Name, Me.lblSDateTo.Text)
            Me.lblSDateFrom.Text = Language._Object.getCaption(Me.lblSDateFrom.Name, Me.lblSDateFrom.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.chkExcludeInactive.Text = Language._Object.getCaption(Me.chkExcludeInactive.Name, Me.chkExcludeInactive.Text)
            Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.chkConsiderEmployeeTermination.Text = Language._Object.getCaption(Me.chkConsiderEmployeeTermination.Name, Me.chkConsiderEmployeeTermination.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsBSC_Planning_Report", 1, "Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 100, "Final Approved")
            Language.setMessage("clsBSC_Planning_Report", 3, "Opened For Changes")
            Language.setMessage("clsBSC_Planning_Report", 4, "Not Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 5, "Not Planned")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Please select Period.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Appointment To Date cannot be less than Appointment From Date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
