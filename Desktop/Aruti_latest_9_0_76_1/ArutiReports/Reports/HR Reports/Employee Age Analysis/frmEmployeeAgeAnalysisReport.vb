'************************************************************************************************************************************
'Class Name : frmEmployeeAgeAnalysisReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeAgeAnalysisReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeAgeAnalysisReport"
    Private objAgeAnalysis As clsEmployeeAgeAnalysisReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objAgeAnalysis = New clsEmployeeAgeAnalysisReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAgeAnalysis.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try

            'Pinkal (24-Jun-2011) -- Start

            ' dsList = ObjEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''S.SANDEEP [ 15 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''dsList = ObjEmp.GetEmployeeList("Emp", True, False)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If
            ''S.SANDEEP [ 15 MAY 2012 ] -- END

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(True)
            dsList = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dRow As DataRow = dsList.Tables(0).NewRow
            dRow.Item("id") = "5"
            dRow.Item("Name") = "="
            dsList.Tables(0).Rows.InsertAt(dRow, 1)
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objMaster = Nothing
            'S.SANDEEP [ 22 FEB 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            txtCode.Text = ""
            objAgeAnalysis.setDefaultOrderBy(0)
            txtOrderBy.Text = objAgeAnalysis.OrderByDisplay
            'Sandeep [ 12 MARCH 2011 ] -- Start
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'Sandeep [ 12 MARCH 2011 ] -- End 
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            chkReportType.Checked = False
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboCondition.SelectedValue = 0
            nudYear.Value = 0
            chkSlabWiseReport.Checked = True
            'S.SANDEEP [ 22 FEB 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAgeAnalysis.SetDefaultValue()

            'S.SANDEEP [ 24 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If chkSlabWiseReport.CheckState = CheckState.Checked Then
                If nudToYear.Value < nudFromYear.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Ending Year cannot be less or equal to Starting Year."), enMsgBoxStyle.Information)
                    nudToYear.Focus()
                    Return False
                End If

                If (nudToYear.Value - nudFromYear.Value) < nudYearGap.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Year Gap cannot be less than the Year difference of From Year and To Year."), enMsgBoxStyle.Information)
                    nudYearGap.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 24 MARCH 2012 ] -- END


            objAgeAnalysis._DateAsOn = dtpDateAsOn.Value

            objAgeAnalysis._EmployeeCode = txtCode.Text

            objAgeAnalysis._EmployeeId = cboEmployee.SelectedValue
            objAgeAnalysis._EmployeeName = cboEmployee.Text

            objAgeAnalysis._ViewByIds = mstrStringIds
            objAgeAnalysis._ViewIndex = mintViewIdx
            objAgeAnalysis._ViewByName = mstrStringName
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            objAgeAnalysis._Analysis_Fields = mstrAnalysis_Fields
            objAgeAnalysis._Analysis_Join = mstrAnalysis_Join
            objAgeAnalysis._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAgeAnalysis._Report_GroupName = mstrReport_GroupName
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objAgeAnalysis._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objAgeAnalysis._IsFromAppointmentDate = chkReportType.Checked
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 24 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If chkSlabWiseReport.CheckState = CheckState.Checked Then
                objAgeAnalysis._StartingYear = nudFromYear.Value
                objAgeAnalysis._EndingYear = nudToYear.Value
                objAgeAnalysis._YearGap = nudYearGap.Value
            End If
            'S.SANDEEP [ 24 MARCH 2012 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAgeAnalysis._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If pnlCriteria.Enabled = True Then
                If cboCondition.SelectedValue > 0 Then
                    objAgeAnalysis._Condition = cboCondition.Text
                    objAgeAnalysis._AgeFilter = CInt(nudYear.Value)
                End If
            End If
            'S.SANDEEP [ 22 FEB 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeAgeAnalysisReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAgeAnalysis = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeSkillsReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeAgeAnalysisReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objAgeAnalysis._ReportName
            Me._Message = objAgeAnalysis._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAgeAnalysisReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAgeAnalysis.generateReport(0, e.Type, enExportAction.None)
            objAgeAnalysis.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAgeAnalysis.generateReport(0, enPrintAction.None, e.Type)
            objAgeAnalysis.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeAgeAnalysisReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeAgeAnalysisReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objAgeAnalysis.setOrderBy(0)
            txtOrderBy.Text = objAgeAnalysis.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (16 May 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkSlabWiseReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSlabWiseReport.CheckedChanged
        Try
            If chkSlabWiseReport.Checked = False Then
                nudFromYear.Enabled = False
                nudToYear.Enabled = False
                nudYearGap.Enabled = False
                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                pnlCriteria.Enabled = True
                'S.SANDEEP [ 22 FEB 2013 ] -- END
            Else
                nudFromYear.Enabled = True
                nudToYear.Enabled = True
                nudYearGap.Enabled = True
                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                cboCondition.SelectedValue = 0
                nudYear.Value = 0
                pnlCriteria.Enabled = False
                'S.SANDEEP [ 22 FEB 2013 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSlabWiseReport_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 12 MAY 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblDateAsOn.Text = Language._Object.getCaption(Me.lblDateAsOn.Name, Me.lblDateAsOn.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkReportType.Text = Language._Object.getCaption(Me.chkReportType.Name, Me.chkReportType.Text)
			Me.chkSlabWiseReport.Text = Language._Object.getCaption(Me.chkSlabWiseReport.Name, Me.chkSlabWiseReport.Text)
			Me.lblYearGap.Text = Language._Object.getCaption(Me.lblYearGap.Name, Me.lblYearGap.Text)
			Me.lblToYear.Text = Language._Object.getCaption(Me.lblToYear.Name, Me.lblToYear.Text)
			Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblCondition.Text = Language._Object.getCaption(Me.lblCondition.Name, Me.lblCondition.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Ending Year cannot be less or equal to Starting Year.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Year Gap cannot be less than the Year difference of From Year and To Year.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
