'************************************************************************************************************************************
'Class Name : clsEmp_PersonalParticular_Report.vb
'Purpose    :
'Date       : 26/03/2012
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmp_PersonalParticular_Report
    Inherits IReportData
    Private Shared mstrModuleName As String = ""
    Private mstrReportId As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal menReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        mstrReportId = menReport
        Select Case menReport
            Case enArutiReport.EmployeePersonalParticular
                mstrModuleName = "clsEmp_PersonalParticular_Report"
            Case enArutiReport.EmployeeCVReport
                mstrModuleName = "clsEmployeeCVReport"
        End Select
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Varibales "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mblnDisplayMembershipData As Boolean = False
    Private mblnDisplaySpouseInfo As Boolean = False
    Private mblnDisplayDependantsInfo As Boolean = False
    Private mblnDisplayQualificationInfo As Boolean = False
    Private mblnDisplayHistroyInfo As Boolean = False
    Private mblnDisplayTrainingInfo As Boolean = False

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _DisplayMembershipData() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplayMembershipData = value
        End Set
    End Property

    Public WriteOnly Property _DisplaySpouseInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplaySpouseInfo = value
        End Set
    End Property

    Public WriteOnly Property _DisplayDependantsInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplayDependantsInfo = value
        End Set
    End Property

    Public WriteOnly Property _DisplayQualificationInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplayQualificationInfo = value
        End Set
    End Property

    Public WriteOnly Property _DisplayHistroyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplayHistroyInfo = value
        End Set
    End Property

    Public WriteOnly Property _DisplayTrainingInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnDisplayTrainingInfo = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = String.Empty
            mblnDisplayMembershipData = False
            mblnDisplaySpouseInfo = False
            mblnDisplayDependantsInfo = False
            mblnDisplayQualificationInfo = False
            mblnDisplayHistroyInfo = False
            mblnDisplayTrainingInfo = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Dependants As ArutiReport.Designer.dsArutiReport
        Dim rpt_Education As ArutiReport.Designer.dsArutiReport
        Dim rpt_History As ArutiReport.Designer.dsArutiReport
        Dim rpt_Training As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            '/************************ PART 1 {START} ************************/ MAIN DETAIL

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '       "	 ISNULL(surname,'') AS SURNAME " & _
            '       "	,ISNULL(firstname,'') AS FORENAME " & _
            '       "	,ISNULL(hrdepartment_master.name,'') AS DEPARTMENT " & _
            '       "	,ISNULL(hrjob_master.job_name,'') AS PRESENT_POSITION " & _
            '       "	,ISNULL(initial.name,'') AS INITIALS " & _
            '       "	,ISNULL(employeecode,'') AS EMPLOYEE_NUMBER " & _
            '       "	,ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS DATE_OF_BIRTH " & _
            '       "	,CASE WHEN gender = 1 THEN @MALE " & _
            '       "		  WHEN gender = 2 THEN @FEMALE END AS GENDER " & _
            '       "	,ISNULL(DOB_CITY.name,'') AS DISTRICT_OF_BIRTH " & _
            '       "	,ISNULL(NATION.country_name,'') AS NATIONALITY " & _
            '       "	,ISNULL(marry.name,'') AS MARITAL_STATUS " & _
            '       "	,ISNULL(DOM_CITY.name,'') AS PLACE_OF_DOMICILE " & _
            '       "	,ISNULL(present_address1,'')+'\r\n'+ISNULL(present_address2,'')+'\r\n'+ISNULL(PCNTRY.country_name,'')+'\r\n'+ISNULL(PSTATE.name,'')+'\r\n'+ISNULL(PCITY.name,'')+'\r\n'+ISNULL(PZCODE.zipcode_no,'') AS PRESENT_ADDRESS " & _
            '       "	,ISNULL(CONVERT(CHAR(8),anniversary_date,112),'') AS DATE_OF_MARRIAGE " & _
            '       "	,ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS DATE_OF_JOINING " & _
            '       "	,ISNULL(CONVERT(CHAR(8),termination_to_date,112),'') AS RETIREMENT_DATE " & _
            '       "FROM hremployee_master " & _
            '       "	LEFT JOIN hrmsConfiguration..cfzipcode_master AS PZCODE ON hremployee_master.present_postcodeunkid = PZCODE.zipcodeunkid " & _
            '       "	LEFT JOIN hrmsConfiguration..cfstate_master AS PSTATE ON hremployee_master.present_stateunkid = PSTATE.stateunkid " & _
            '       "	LEFT JOIN hrmsConfiguration..cfcity_master AS PCITY ON hremployee_master.present_post_townunkid = PCITY.cityunkid " & _
            '       "	LEFT JOIN hrmsConfiguration..cfcountry_master AS PCNTRY ON PCNTRY.countryunkid = hremployee_master.present_countryunkid " & _
            '       "	LEFT JOIN hrmsConfiguration..cfcity_master AS DOM_CITY ON hremployee_master.domicile_post_townunkid = DOM_CITY.cityunkid " & _
            '       "	LEFT JOIN cfcommon_master AS marry ON hremployee_master.maritalstatusunkid = marry.masterunkid AND marry.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
            '       "	LEFT JOIN hrmsConfiguration..cfcountry_master AS NATION ON NATION.countryunkid = hremployee_master.nationalityunkid " & _
            '       "	LEFT JOIN hrmsConfiguration..cfcity_master AS DOB_CITY ON hremployee_master.birthcityunkid = DOB_CITY.cityunkid " & _
            '       "	LEFT JOIN cfcommon_master AS initial ON hremployee_master.titleunkid = initial.masterunkid AND initial.mastertype = " & clsCommon_Master.enCommonMaster.TITLE & " " & _
            '       "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '       "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '       "WHERE employeeunkid = '" & mintEmployeeUnkid & "' "

            StrQ = "SELECT " & _
                   "	 ISNULL(surname,'') AS SURNAME " & _
                   "	,ISNULL(firstname,'') AS FORENAME " & _
                   "	,ISNULL(othername,'') AS OTHERNAME " & _
                   "	,ISNULL(hrdepartment_master.name,'') AS DEPARTMENT " & _
                   "	,ISNULL(hrjob_master.job_name,'') AS PRESENT_POSITION " & _
                   "	,ISNULL(initial.name,'') AS INITIALS " & _
                   "	,ISNULL(employeecode,'') AS EMPLOYEE_NUMBER " & _
                   "	,ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS DATE_OF_BIRTH " & _
                   "	,CASE WHEN gender = 1 THEN @MALE " & _
                   "		  WHEN gender = 2 THEN @FEMALE END AS GENDER " & _
                   "	,ISNULL(DOB_CITY.name,'') AS DISTRICT_OF_BIRTH " & _
                   "	,ISNULL(NATION.country_name,'') AS NATIONALITY " & _
                   "	,ISNULL(marry.name,'') AS MARITAL_STATUS " & _
                   "	,ISNULL(DOM_CITY.name,'') AS PLACE_OF_DOMICILE " & _
                   "	,ISNULL(present_address1,'')+'\r\n'+ISNULL(present_address2,'')+'\r\n'+ISNULL(PCNTRY.country_name,'')+'\r\n'+ISNULL(PSTATE.name,'')+'\r\n'+ISNULL(PCITY.name,'')+'\r\n'+ISNULL(PZCODE.zipcode_no,'') AS PRESENT_ADDRESS " & _
                   "	,ISNULL(CONVERT(CHAR(8),anniversary_date,112),'') AS DATE_OF_MARRIAGE " & _
                   "	,ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS DATE_OF_JOINING " & _
                   "	,ISNULL(CONVERT(CHAR(8),RT.termination_to_date,112),'') AS RETIREMENT_DATE " & _
                   "    ,ISNULL(hrclassgroup_master.name,'') AS CLASS_GRP " & _
                   "    ,ISNULL(hrclasses_master.name,'') AS CLASS " & _
                   "    ,ISNULL(hremployee_master.present_mobile,'') AS PRESENT_TELNO " & _
                   "    ,ISNULL(hremployee_master.emer_con_firstname,'') AS EMER_FNAME1 " & _
                   "    ,ISNULL(hremployee_master.emer_con_lastname,'') AS EMER_LNAME1 " & _
                   "    ,ISNULL(hremployee_master.emer_con_address,'') AS EMER_ADDR1 " & _
                   "    ,ISNULL(hremployee_master.emer_con_mobile,'') AS EMER_MOBILE1 " & _
                   "    ,ISNULL(hremployee_master.emer_con_email,'') AS EMER_EMAIL1 " & _
                   "    ,ISNULL(hremployee_master.emer_con_firstname2,'') AS EMER_FNAME2 " & _
                   "    ,ISNULL(hremployee_master.emer_con_lastname2,'') AS EMER_LNAME2 " & _
                   "    ,ISNULL(hremployee_master.emer_con_address2,'') AS EMER_ADDR2 " & _
                   "    ,ISNULL(hremployee_master.emer_con_mobile2,'') AS EMER_MOBILE2 " & _
                   "    ,ISNULL(hremployee_master.emer_con_email2,'') AS EMER_EMAIL2 " & _
                   "    ,ISNULL(hremployee_master.emer_con_firstname3,'') AS EMER_FNAME3 " & _
                   "    ,ISNULL(hremployee_master.emer_con_lastname3,'') AS EMER_LNAME3 " & _
                   "    ,ISNULL(hremployee_master.emer_con_address3,'') AS EMER_ADDR3 " & _
                   "    ,ISNULL(hremployee_master.emer_con_mobile3,'') AS EMER_MOBILE3 " & _
                   "    ,ISNULL(hremployee_master.emer_con_email3,'') AS EMER_EMAIL3 " & _
                   "FROM hremployee_master " & _
                   "	LEFT JOIN hrmsConfiguration..cfzipcode_master AS PZCODE ON hremployee_master.present_postcodeunkid = PZCODE.zipcodeunkid " & _
                   "	LEFT JOIN hrmsConfiguration..cfstate_master AS PSTATE ON hremployee_master.present_stateunkid = PSTATE.stateunkid " & _
                   "	LEFT JOIN hrmsConfiguration..cfcity_master AS PCITY ON hremployee_master.present_post_townunkid = PCITY.cityunkid " & _
                   "	LEFT JOIN hrmsConfiguration..cfcountry_master AS PCNTRY ON PCNTRY.countryunkid = hremployee_master.present_countryunkid " & _
                   "	LEFT JOIN hrmsConfiguration..cfcity_master AS DOM_CITY ON hremployee_master.domicile_post_townunkid = DOM_CITY.cityunkid " & _
                   "	LEFT JOIN cfcommon_master AS marry ON hremployee_master.maritalstatusunkid = marry.masterunkid AND marry.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " " & _
                   "	LEFT JOIN hrmsConfiguration..cfcountry_master AS NATION ON NATION.countryunkid = hremployee_master.nationalityunkid " & _
                   "	LEFT JOIN hrmsConfiguration..cfcity_master AS DOB_CITY ON hremployee_master.birthcityunkid = DOB_CITY.cityunkid " & _
                   "	LEFT JOIN cfcommon_master AS initial ON hremployee_master.titleunkid = initial.masterunkid AND initial.mastertype = " & clsCommon_Master.enCommonMaster.TITLE & " " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             jobunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_categorization_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeUnkid & "'  " & _
                   "    )AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
                   "    JOIN hrjob_master ON Job.jobunkid = hrjob_master.jobunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             departmentunkid " & _
                   "            ,employeeunkid " & _
                   "            ,classgroupunkid " & _
                   "            ,classunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeUnkid & "'  " & _
                   "    ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "    LEFT JOIN hrdepartment_master ON Alloc.departmentunkid = hrdepartment_master.departmentunkid " & _
                   "    LEFT JOIN hrclassgroup_master ON Alloc.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                   "    LEFT JOIN hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             employeeunkid " & _
                   "            ,date1 AS termination_to_date " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_dates_tran " & _
                   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
                   "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        AND hremployee_dates_tran.employeeunkid = '" & mintEmployeeUnkid & "'  " & _
                   "    )RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                   "WHERE hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "' "
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [22-APR-2017] -- START
            'ISSUE/ENHANCEMENT : CHANGES REQ. FOR EMP. (CV & PERSONAL) REPORT
            ' OTHERNAME -- ADDED,' CLASS_GRP -- ADDED,' CLASS -- ADDED,' PRESENT_TELNO -- ADDED
            ' EMER_FNAME1 -- ADDED,' EMER_LNAME1 -- ADDED,' EMER_ADDR1 -- ADDED
            ' EMER_MOBILE1 -- ADDED,' EMER_EMAIL1 -- ADDED
            ' EMER_FNAME2 -- ADDED,' EMER_LNAME2 -- ADDED
            ' EMER_ADDR2 -- ADDED,' EMER_MOBILE2 -- ADDED
            ' EMER_EMAIL2 -- ADDED,' EMER_FNAME3 -- ADDED
            ' EMER_LNAME3 -- ADDED,' EMER_ADDR3 -- ADDED
            ' EMER_MOBILE3 -- ADDED,' EMER_EMAIL3 -- ADDED
            'S.SANDEEP [22-APR-2017] -- END




            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim objMData As New clsMasterData
            Dim dsTranLst As New DataSet

            dsTranLst.Tables.Add(objMData.Get_Current_Scale("CScale", mintEmployeeUnkid, ConfigParameter._Object._CurrentDateAndTime.Date).Tables(0).Copy)
            dsTranLst.Tables.Add(clsEmployee_Master.GetEmployee_Membership(mintEmployeeUnkid, False, "MList").Tables(0).Copy)
            Dim objGrade As New clsGrade
            If dsTranLst.Tables("CScale").Rows.Count > 0 Then
                objGrade._Gradeunkid = dsTranLst.Tables("CScale").Rows(0).Item("gradeunkid")
            Else
                objGrade._Gradeunkid = -1
            End If



            Dim StrMembership As String = String.Empty

            For Each dMRow As DataRow In dsTranLst.Tables("MList").Rows
                StrMembership &= dMRow.Item("Mem_Name") & " : " & dMRow.Item("Mem_No") & vbCrLf
            Next


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("SURNAME")
                rpt_Rows.Item("Column2") = dtRow.Item("FORENAME")
                rpt_Rows.Item("Column3") = dtRow.Item("DEPARTMENT")
                rpt_Rows.Item("Column4") = dtRow.Item("PRESENT_POSITION")
                rpt_Rows.Item("Column5") = dtRow.Item("INITIALS")
                'If dsTranLst.Tables("CScale").Rows.Count > 0 Then
                ' rpt_Rows.Item("Column6") = Format(CDec(dsTranLst.Tables("CScale").Rows(0).Item("currentscale")), GUI.fmtCurrency)   'SCALE  currentscale
                rpt_Rows.Item("Column6") = objGrade._Name
                'Else
                'rpt_Rows.Item("Column6") = ""
                'End If
                rpt_Rows.Item("Column7") = dtRow.Item("EMPLOYEE_NUMBER")
                If dtRow.Item("DATE_OF_BIRTH").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("DATE_OF_BIRTH").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column8") = ""
                End If
                rpt_Rows.Item("Column9") = dtRow.Item("DISTRICT_OF_BIRTH")
                rpt_Rows.Item("Column10") = dtRow.Item("NATIONALITY")
                rpt_Rows.Item("Column11") = dtRow.Item("GENDER")
                'rpt_Rows.Item("Column12") = "" 'LEFT OUT FOR NO. OF CHILDREN
                rpt_Rows.Item("Column13") = StrMembership
                rpt_Rows.Item("Column14") = dtRow.Item("MARITAL_STATUS")
                If dtRow.Item("DATE_OF_MARRIAGE").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column15") = eZeeDate.convertDate(dtRow.Item("DATE_OF_MARRIAGE").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column15") = ""
                End If
                rpt_Rows.Item("Column16") = dtRow.Item("PLACE_OF_DOMICILE")
                rpt_Rows.Item("Column17") = dtRow.Item("PRESENT_ADDRESS").ToString.Replace("\r\n", vbCrLf)
                rpt_Rows.Item("Column18") = eZeeDate.convertDate(dtRow.Item("DATE_OF_JOINING").ToString).ToShortDateString
                rpt_Rows.Item("Column19") = eZeeDate.convertDate(dtRow.Item("RETIREMENT_DATE").ToString).ToShortDateString

                'S.SANDEEP [22-APR-2017] -- START
                'ISSUE/ENHANCEMENT : CHANGES REQ. FOR EMP. (CV & PERSONAL) REPORT
                rpt_Rows.Item("Column20") = dtRow.Item("OTHERNAME")
                rpt_Rows.Item("Column21") = dtRow.Item("CLASS_GRP")
                rpt_Rows.Item("Column22") = dtRow.Item("CLASS")
                rpt_Rows.Item("Column23") = dtRow.Item("PRESENT_TELNO")

                rpt_Rows.Item("Column24") = dtRow.Item("EMER_FNAME1")
                rpt_Rows.Item("Column25") = dtRow.Item("EMER_LNAME1")
                rpt_Rows.Item("Column26") = dtRow.Item("EMER_ADDR1")
                rpt_Rows.Item("Column27") = dtRow.Item("EMER_MOBILE1")
                rpt_Rows.Item("Column28") = dtRow.Item("EMER_EMAIL1")

                rpt_Rows.Item("Column29") = dtRow.Item("EMER_FNAME2")
                rpt_Rows.Item("Column30") = dtRow.Item("EMER_LNAME2")
                rpt_Rows.Item("Column31") = dtRow.Item("EMER_ADDR2")
                rpt_Rows.Item("Column32") = dtRow.Item("EMER_MOBILE2")
                rpt_Rows.Item("Column33") = dtRow.Item("EMER_EMAIL2")

                rpt_Rows.Item("Column34") = dtRow.Item("EMER_FNAME3")
                rpt_Rows.Item("Column35") = dtRow.Item("EMER_LNAME3")
                rpt_Rows.Item("Column36") = dtRow.Item("EMER_ADDR3")
                rpt_Rows.Item("Column37") = dtRow.Item("EMER_MOBILE3")
                rpt_Rows.Item("Column38") = dtRow.Item("EMER_EMAIL3")
                'S.SANDEEP [22-APR-2017] -- END


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objGrade = Nothing

            '/************************ PART 1 {END} ************************/ MAIN DETAIL

            '/************************ PART 2 {START} ************************/ DEPENDANT & BENEFICIARIES

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If mintEmployeeUnkid > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeUnkid > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & mintEmployeeUnkid & " "
            End If

            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= "SELECT " & _
                   "	 ISNULL(first_name,'')+' '+ISNULL(middle_name,'')+' '+ISNULL(last_name,'') AS FULLNAME " & _
                   "	,ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS BIRTHDATE " & _
                   "	,CASE WHEN isdependant = 1 THEN @YES " & _
                   "		  WHEN isdependant = 0 THEN @NO END AS BENEFICIARY " & _
                   "	,ISNULL(cfcommon_master.name,'') AS RELATIONSHIP " & _
                   "	,ISNULL(address,'')+'\r\n'+ISNULL(hrmsConfiguration..cfcountry_master.country_name,'')+'\r\n'+ISNULL(hrmsConfiguration..cfstate_master.name,'')+'\r\n'+ISNULL(hrmsConfiguration..cfcity_master.name,'') AS ADDRESS " & _
                   "FROM hrdependants_beneficiaries_tran " & _
                   "    JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cfcity_master ON hrdependants_beneficiaries_tran.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cfstate_master ON hrdependants_beneficiaries_tran.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cfcountry_master ON hrdependants_beneficiaries_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.RELATIONS & "' " & _
                   "WHERE employeeunkid = '" & mintEmployeeUnkid & "' AND hrdependants_beneficiaries_tran.isvoid = 0 AND #TableDepn.isactive = 1 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "YES"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 56, "NO"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_Dependants = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Dependants.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("FULLNAME")
                If dtRow.Item("BIRTHDATE").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column2") = eZeeDate.convertDate(dtRow.Item("BIRTHDATE").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column2") = ""
                End If
                rpt_Rows.Item("Column3") = dtRow.Item("BENEFICIARY")
                rpt_Rows.Item("Column4") = dtRow.Item("RELATIONSHIP")
                rpt_Rows.Item("Column5") = dtRow.Item("ADDRESS").ToString.Replace("\r\n", vbCrLf)

                rpt_Dependants.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '/************************ PART 2 {END} ************************/ DEPENDANT & BENEFICIARIES

            '/************************ PART 3 {START} ************************/ QUALIFICATION

            'S.SANDEEP [ 26 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            StrQ = "SELECT " & _
                   "	 ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS DATE_FROM " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS DATE_TO " & _
                   "	,ISNULL(hrinstitute_master.institute_name,'') AS INSTITUTE " & _
                   "	,ISNULL(cfcommon_master.name,'') AS COURSE " & _
                   "	,ISNULL(hrqualification_master.qualificationname,'') AS QUALIFICATION " & _
                   "	,ISNULL(hrresult_master.resultname,'') AS GRADE " & _
                   "FROM hremp_qualification_tran " & _
                   "	LEFT JOIN hrresult_master ON hremp_qualification_tran.resultunkid = hrresult_master.resultunkid " & _
                   "	LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                   "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = 18 " & _
                   "	LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                   "WHERE employeeunkid = '" & mintEmployeeUnkid & "' AND isvoid = 0 AND ISNULL(other_qualificationgrp,'') ='' "


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            StrQ &= " ORDER BY award_end_date DESC"
            'Pinkal (25-APR-2012) -- End



            'S.SANDEEP [ 26 APRIL 2012 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_Education = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Education.Tables("ArutiTable").NewRow

                If dtRow.Item("DATE_FROM").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column1") = eZeeDate.convertDate(dtRow.Item("DATE_FROM").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column1") = ""
                End If

                If dtRow.Item("DATE_TO").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column2") = eZeeDate.convertDate(dtRow.Item("DATE_TO").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column2") = ""
                End If
                rpt_Rows.Item("Column3") = dtRow.Item("INSTITUTE")
                rpt_Rows.Item("Column4") = dtRow.Item("COURSE")
                rpt_Rows.Item("Column5") = dtRow.Item("QUALIFICATION")
                rpt_Rows.Item("Column6") = dtRow.Item("GRADE")

                rpt_Education.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '/************************ PART 3 {END} ************************/ QUALIFICATION

            '/************************ PART 4 {START} ************************/ JOB HISTORY

            'S.SANDEEP [22-APR-2017] -- START
            'ISSUE/ENHANCEMENT : CHANGES REQ. FOR EMP. (CV & PERSONAL) REPORT
            'StrQ = "SELECT " & _
            '       "  ISNULL(CONVERT(CHAR(8),start_date,112),'') AS DATE_FROM " & _
            '       " ,ISNULL(CONVERT(CHAR(8),end_date,112),'') AS DATE_TO " & _
            '       " ,ISNULL(company,'') AS EMPLOYER " & _
            '       " ,ISNULL(old_job,'') AS POSITION_HELD " & _
            '       "FROM hremp_experience_tran " & _
            '       "WHERE employeeunkid = '" & mintEmployeeUnkid & "' AND isvoid = 0 "


            ''Pinkal (25-APR-2012) -- Start
            ''Enhancement : TRA Changes
            'StrQ &= " ORDER BY start_date DESC "
            ''Pinkal (25-APR-2012) -- End

            StrQ = "SELECT " & _
                    "* " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "ISNULL(CONVERT(CHAR(8),start_date,112),'') AS DATE_FROM " & _
                              ",ISNULL(CONVERT(CHAR(8),end_date,112),'') AS DATE_TO " & _
                              ",ISNULL(company,'') AS EMPLOYER " & _
                              ",ISNULL(old_job,'') AS POSITION_HELD " & _
                         "FROM hremp_experience_tran WHERE hremp_experience_tran.employeeunkid = '" & mintEmployeeUnkid & "' AND hremp_experience_tran.isvoid = 0 " & _
                         "UNION ALL " & _
                         "SELECT " & _
                               "DATE_FROM " & _
                              ",DATE_TO " & _
                              ",EMPLOYER " & _
                              ",POSITION_HELD " & _
                         "FROM " & _
                         "( " & _
                              "SELECT " & _
                                   "ISNULL(CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112),'') AS DATE_FROM " & _
                                   ",'' AS DATE_TO " & _
                                   ",'' AS EMPLOYER " & _
                                   ",hrjob_master.job_name AS POSITION_HELD " & _
                                   ",ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) as rno " & _
                              "FROM hremployee_categorization_tran " & _
                                   "JOIN hrjob_master ON hremployee_categorization_tran.jobunkid = hrjob_master.jobunkid " & _
                              "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeUnkid & "' " & _
                         ") AS A WHERE A.rno = 1 " & _
                    ") AS B WHERE 1 = 1 ORDER BY B.DATE_FROM DESC "
            'S.SANDEEP [22-APR-2017] -- END





            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_History = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_History.Tables("ArutiTable").NewRow

                If dtRow.Item("DATE_FROM").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column1") = eZeeDate.convertDate(dtRow.Item("DATE_FROM").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column1") = ""
                End If

                If dtRow.Item("DATE_TO").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column2") = eZeeDate.convertDate(dtRow.Item("DATE_TO").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column2") = ""
                End If
                rpt_Rows.Item("Column3") = dtRow.Item("EMPLOYER")
                rpt_Rows.Item("Column4") = dtRow.Item("POSITION_HELD")

                rpt_History.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '/************************ PART 4 {END} ************************/ JOB HISTORY

            '/************************ PART 5 {START} ************************/
            'StrQ = "SELECT " & _
            '        "	 ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS SDate " & _
            '        "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS EDate " & _
            '        "	,ISNULL(cfcommon_master.name,'') AS COURSE_TITLE " & _
            '        "	,ISNULL(hrqualification_master.qualificationname,'') AS AWARD " & _
            '        "	,ISNULL(hrinstitute_master.institute_name,'') AS PROVIDER " & _
            '        "FROM hrtraining_enrollment_tran " & _
            '        "	JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '        "	LEFT JOIN hrinstitute_master ON dbo.hrtraining_scheduling.traininginstituteunkid = dbo.hrinstitute_master.instituteunkid " & _
            '        "	JOIN hremp_qualification_tran ON hrtraining_scheduling.qualificationunkid = hremp_qualification_tran.qualificationunkid " & _
            '        "	JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' " & _
            '        "WHERE hrtraining_enrollment_tran.employeeunkid = '" & mintEmployeeUnkid & "' AND hremp_qualification_tran.employeeunkid = '" & mintEmployeeUnkid & "' AND isqualificaionupdated = 1 " & _
            '        "	AND hrtraining_enrollment_tran.isvoid = 0 AND hrtraining_enrollment_tran.iscancel = 0 "

            StrQ = "SELECT " & _
                   "	 ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS SDate " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS EDate " & _
                   "	,ISNULL(hrinstitute_master.institute_name,'') AS PROVIDER " & _
                   "	,ISNULL(other_qualificationgrp,'') AS COURSE_TITLE " & _
                   "	,ISNULL(other_qualification,'') AS AWARD " & _
                   "	,ISNULL(other_resultcode,'') AS GRADE " & _
                   "FROM hremp_qualification_tran " & _
                   "	LEFT JOIN hrresult_master ON hremp_qualification_tran.resultunkid = hrresult_master.resultunkid " & _
                   "	LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                   "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                   "	LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                   "WHERE employeeunkid = '" & mintEmployeeUnkid & "' AND isvoid = 0 AND ISNULL(other_qualificationgrp,'') <>'' "



            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            StrQ &= " ORDER BY award_end_date DESC "
            'Pinkal (25-APR-2012) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_Training = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Training.Tables("ArutiTable").NewRow

                If dtRow.Item("SDate").ToString.Trim.Length > 0 AndAlso dtRow.Item("EDate").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column1") = eZeeDate.convertDate(dtRow.Item("SDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("EDate").ToString).ToShortDateString
                ElseIf dtRow.Item("SDate").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column1") = eZeeDate.convertDate(dtRow.Item("SDate").ToString).ToShortDateString
                ElseIf dtRow.Item("EDate").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column1") = eZeeDate.convertDate(dtRow.Item("EDate").ToString).ToShortDateString
                End If

                rpt_Rows.Item("Column2") = dtRow.Item("COURSE_TITLE")
                rpt_Rows.Item("Column3") = dtRow.Item("AWARD")
                rpt_Rows.Item("Column4") = dtRow.Item("PROVIDER")

                rpt_Training.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '/************************ PART 5 {END} ************************/


            objRpt = New ArutiReport.Designer.rptEmployeePersonalParticular

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "")

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptDependants").SetDataSource(rpt_Dependants)
            objRpt.Subreports("rptQualification").SetDataSource(rpt_Education)
            objRpt.Subreports("rptEmployeeHistory").SetDataSource(rpt_History)
            objRpt.Subreports("rptEmployeeTraining").SetDataSource(rpt_Training)

            Select Case CInt(mstrReportId)
                Case enArutiReport.EmployeePersonalParticular
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection8", True)
                    'S.SANDEEP [22-APR-2017] -- START
                    'ISSUE/ENHANCEMENT : CHANGES REQ. FOR EMP. (CV & PERSONAL) REPORT
                    ReportFunction.EnableSuppress(objRpt, "lblMarriedDate", True)
                    ReportFunction.EnableSuppress(objRpt, "Column151", True)
                    'S.SANDEEP [22-APR-2017] -- END
                    '
                Case enArutiReport.EmployeeCVReport
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection7", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection2", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection3", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection4", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection5", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection6", True)
                    ReportFunction.EnableSuppressSection(objRpt, "Section3", True)
                    'S.SANDEEP [22-APR-2017] -- START
                    'ISSUE/ENHANCEMENT : CHANGES REQ. FOR EMP. (CV & PERSONAL) REPORT
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection9", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection10", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection11", True)
                    ReportFunction.EnableSuppressSection(objRpt, "ReportHeaderSection12", True)
                    'S.SANDEEP [22-APR-2017] -- END
            End Select

            'ReportFunction.EnableSuppress(objRpt, "lblMemberships", Not mblnDisplayMembershipData)
            'ReportFunction.EnableSuppress(objRpt, "Column131", Not mblnDisplayMembershipData)

            ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", Not mblnDisplayHistroyInfo)
            ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", Not mblnDisplayQualificationInfo)
            ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", Not mblnDisplayTrainingInfo)



            ReportFunction.TextChange(objRpt, "lblPart1", Language.getMessage(mstrModuleName, 50, "? PERSONAL DETAILS"))
            ReportFunction.TextChange(objRpt.Subreports("rptDependants"), "lblPart2", Language.getMessage(mstrModuleName, 51, "? CHILDREN/DEPENDANTS"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "lblPart3", Language.getMessage(mstrModuleName, 52, "? EDUCATION/PROFESSIONAL QUALIFICATIONS"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeHistory"), "lblPart4", Language.getMessage(mstrModuleName, 53, "? EMPLOYMENT HISTORY"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeTraining"), "lblPart5", Language.getMessage(mstrModuleName, 54, "? TRAINING/SHORT COURSES"))

            ReportFunction.TextChange(objRpt, "lblSurname", Language.getMessage(mstrModuleName, 1, "SURNAME: "))
            ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 2, "DEPARTMENT: "))
            ReportFunction.TextChange(objRpt, "lblFirstname", Language.getMessage(mstrModuleName, 3, "FORENAME: "))
            ReportFunction.TextChange(objRpt, "lblPosition", Language.getMessage(mstrModuleName, 4, "PRESENT POSITION: "))
            ReportFunction.TextChange(objRpt, "lblInitials", Language.getMessage(mstrModuleName, 5, "INITIALS: "))
            ReportFunction.TextChange(objRpt, "lblScale", Language.getMessage(mstrModuleName, 6, "PRESENT SCALE: "))
            ReportFunction.TextChange(objRpt, "lblEmployeeNumber", Language.getMessage(mstrModuleName, 7, "EMPLOYEE NUMBER: "))
            ReportFunction.TextChange(objRpt, "lblDOB", Language.getMessage(mstrModuleName, 8, "DATE OF BIRTH: "))
            ReportFunction.TextChange(objRpt, "lblBirthDistrict", Language.getMessage(mstrModuleName, 9, "DISTRICT OF BIRTH: "))
            ReportFunction.TextChange(objRpt, "lblNationality", Language.getMessage(mstrModuleName, 10, "NATIONALITY: "))
            ReportFunction.TextChange(objRpt, "lblGender", Language.getMessage(mstrModuleName, 11, "GENDER: "))
            ReportFunction.TextChange(objRpt, "lblMaritalStatus", Language.getMessage(mstrModuleName, 13, "MARITAL STATUS: "))
            ReportFunction.TextChange(objRpt, "lblDomicilePlace", Language.getMessage(mstrModuleName, 14, "PLACE OF DOMICILE: "))
            ReportFunction.TextChange(objRpt, "lblPresentAddress", Language.getMessage(mstrModuleName, 15, "PRESENT ADDRESS: "))
            'S.SANDEEP [22-APR-2017] -- START
            'ISSUE/ENHANCEMENT : CHANGES REQ. FOR EMP. (CV & PERSONAL) REPORT
            ReportFunction.TextChange(objRpt, "txtOtherName", Language.getMessage(mstrModuleName, 57, "OHERNAME: "))

            ReportFunction.TextChange(objRpt, "txtPresentTelNo", Language.getMessage(mstrModuleName, 58, "PRESENT TELNO :"))
            ReportFunction.TextChange(objRpt, "txtClassGrp", Language.getMessage(mstrModuleName, 59, "CLASS GROUP: "))
            ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 60, "CLASS: "))

            ReportFunction.TextChange(objRpt, "txtEFName1", Language.getMessage(mstrModuleName, 61, "FIRSTNAME: "))
            ReportFunction.TextChange(objRpt, "txtELName1", Language.getMessage(mstrModuleName, 62, "LASTNAME: "))
            ReportFunction.TextChange(objRpt, "txtEAddress1", Language.getMessage(mstrModuleName, 63, "ADDRESS: "))
            ReportFunction.TextChange(objRpt, "txtEMobile1", Language.getMessage(mstrModuleName, 64, "MOBILE: "))
            ReportFunction.TextChange(objRpt, "txtEEmail1", Language.getMessage(mstrModuleName, 65, "EMAIL: "))

            ReportFunction.TextChange(objRpt, "txtEFName2", Language.getMessage(mstrModuleName, 61, "FIRSTNAME: "))
            ReportFunction.TextChange(objRpt, "txtELName2", Language.getMessage(mstrModuleName, 62, "LASTNAME: "))
            ReportFunction.TextChange(objRpt, "txtEAddress2", Language.getMessage(mstrModuleName, 63, "ADDRESS: "))
            ReportFunction.TextChange(objRpt, "txtEMobile2", Language.getMessage(mstrModuleName, 64, "MOBILE: "))
            ReportFunction.TextChange(objRpt, "txtEEmail2", Language.getMessage(mstrModuleName, 65, "EMAIL: "))

            ReportFunction.TextChange(objRpt, "txtEFName3", Language.getMessage(mstrModuleName, 61, "FIRSTNAME: "))
            ReportFunction.TextChange(objRpt, "txtELName3", Language.getMessage(mstrModuleName, 62, "LASTNAME: "))
            ReportFunction.TextChange(objRpt, "txtEAddress3", Language.getMessage(mstrModuleName, 63, "ADDRESS: "))
            ReportFunction.TextChange(objRpt, "txtEMobile3", Language.getMessage(mstrModuleName, 64, "MOBILE: "))
            ReportFunction.TextChange(objRpt, "txtEEmail3", Language.getMessage(mstrModuleName, 65, "EMAIL: "))

            ReportFunction.TextChange(objRpt, "lblEmerContact1", Language.getMessage(mstrModuleName, 66, "? EMERGENCY CONTACT 1"))
            ReportFunction.TextChange(objRpt, "lblEmerContact2", Language.getMessage(mstrModuleName, 67, "? EMERGENCY CONTACT 2"))
            ReportFunction.TextChange(objRpt, "lblEmerContact3", Language.getMessage(mstrModuleName, 67, "? EMERGENCY CONTACT 3"))
            'S.SANDEEP [22-APR-2017] -- END



            'ReportFunction.TextChange(objRpt, "lblChildren", Language.getMessage(mstrModuleName, 12, "NO. OF CHILDREN: "))
            'If mblnDisplaySpouseInfo = True Then
            '    ReportFunction.TextChange(objRpt, "lblSpouseDetails", Language.getMessage(mstrModuleName, 16, "SPOUSE DETAILS: "))
            '    ReportFunction.TextChange(objRpt, "lblSpouseName", Language.getMessage(mstrModuleName, 17, "NAME OF SPOUSE: "))
            '    ReportFunction.TextChange(objRpt, "lblMarriedDate", Language.getMessage(mstrModuleName, 18, "DATE OF MARRIAGE: "))
            'Else
            '    ReportFunction.EnableSuppress(objRpt, "lblSpouseDetails", True)
            '    ReportFunction.EnableSuppress(objRpt, "lblSpouseName", True)
            '    ReportFunction.EnableSuppress(objRpt, "lblMarriedDate", True)
            'End If

            ReportFunction.TextChange(objRpt, "lblDOJ", Language.getMessage(mstrModuleName, 19, "DATE OF JOINING : "))
            ReportFunction.TextChange(objRpt, "lblRetirementDate", Language.getMessage(mstrModuleName, 20, "RETIREMENT DATE: "))
            ReportFunction.TextChange(objRpt, "lblMemberships", Language.getMessage(mstrModuleName, 22, "MEMBERSHIPS"))

            ReportFunction.TextChange(objRpt.Subreports("rptDependants"), "txtFullName", Language.getMessage(mstrModuleName, 23, "FULL NAME"))
            ReportFunction.TextChange(objRpt.Subreports("rptDependants"), "txtBirthDate", Language.getMessage(mstrModuleName, 24, "BIRTH DATE"))
            ReportFunction.TextChange(objRpt.Subreports("rptDependants"), "txtDependant", Language.getMessage(mstrModuleName, 25, "BENEFICIARY"))
            ReportFunction.TextChange(objRpt.Subreports("rptDependants"), "txtRelationship", Language.getMessage(mstrModuleName, 26, "RELATIONSHIP"))
            ReportFunction.TextChange(objRpt.Subreports("rptDependants"), "txtAddress", Language.getMessage(mstrModuleName, 27, "ADDRESS"))

            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "txtDateFrom", Language.getMessage(mstrModuleName, 28, "DATE FROM"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "txtDateTo", Language.getMessage(mstrModuleName, 29, "DATE TO"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "txtInstitute", Language.getMessage(mstrModuleName, 30, "SCHOOL/INSTITUTE"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "txtCourse", Language.getMessage(mstrModuleName, 31, "COURSE"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "txtQualification", Language.getMessage(mstrModuleName, 32, "QUALIFICATION"))
            ReportFunction.TextChange(objRpt.Subreports("rptQualification"), "txtGrade", Language.getMessage(mstrModuleName, 33, "GRADE"))

            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeHistory"), "txtDateFrom", Language.getMessage(mstrModuleName, 34, "DATE FROM"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeHistory"), "txtDateTo", Language.getMessage(mstrModuleName, 35, "DATE TO"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeHistory"), "txtEmployer", Language.getMessage(mstrModuleName, 36, "EMPLOYER"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeHistory"), "txtPositionHeld", Language.getMessage(mstrModuleName, 37, "POSITION HELD"))

            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeTraining"), "txtYear", Language.getMessage(mstrModuleName, 38, "YEAR"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeTraining"), "txtCourseTitle", Language.getMessage(mstrModuleName, 39, "COURSE TITLE"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeTraining"), "txtAward", Language.getMessage(mstrModuleName, 40, "AWARD"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployeeTraining"), "txtProvider", Language.getMessage(mstrModuleName, 41, "PROVIDER"))

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 42, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 43, "Printed Date :"))
            ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 44, "Page :"))

            ReportFunction.TextChange(objRpt, "txtCVName", Language.getMessage(mstrModuleName, 45, "NAME: "))
            ReportFunction.TextChange(objRpt, "txtCVEmpNo", Language.getMessage(mstrModuleName, 7, "EMPLOYEE NUMBER: "))
            ReportFunction.TextChange(objRpt, "txtCVDOB", Language.getMessage(mstrModuleName, 46, "DATE OF BIRTH: "))
            ReportFunction.TextChange(objRpt, "txtCVPOB", Language.getMessage(mstrModuleName, 47, "PLACE OF BIRTH: "))
            ReportFunction.TextChange(objRpt, "txtCVNationality", Language.getMessage(mstrModuleName, 10, "NATIONALITY: "))
            ReportFunction.TextChange(objRpt, "txtCVMStatus", Language.getMessage(mstrModuleName, 13, "MARITAL STATUS: "))
            ReportFunction.TextChange(objRpt, "txtCVGender", Language.getMessage(mstrModuleName, 48, "SEX: "))
            ReportFunction.TextChange(objRpt, "txtCVAddress", Language.getMessage(mstrModuleName, 49, "PRESENT ADDRESS: "))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage(mstrModuleName, 1, "SURNAME:")
            Language.setMessage(mstrModuleName, 2, "DEPARTMENT:")
            Language.setMessage(mstrModuleName, 3, "FORENAME:")
            Language.setMessage(mstrModuleName, 4, "PRESENT POSITION:")
            Language.setMessage(mstrModuleName, 5, "INITIALS:")
            Language.setMessage(mstrModuleName, 6, "PRESENT SCALE:")
            Language.setMessage(mstrModuleName, 7, "EMPLOYEE NUMBER:")
            Language.setMessage(mstrModuleName, 8, "DATE OF BIRTH:")
            Language.setMessage(mstrModuleName, 9, "DISTRICT OF BIRTH:")
            Language.setMessage(mstrModuleName, 10, "NATIONALITY:")
            Language.setMessage(mstrModuleName, 11, "GENDER:")
            Language.setMessage(mstrModuleName, 13, "MARITAL STATUS:")
            Language.setMessage(mstrModuleName, 14, "PLACE OF DOMICILE:")
            Language.setMessage(mstrModuleName, 15, "PRESENT ADDRESS:")
            Language.setMessage(mstrModuleName, 19, "DATE OF JOINING :")
            Language.setMessage(mstrModuleName, 20, "RETIREMENT DATE:")
            Language.setMessage(mstrModuleName, 22, "MEMBERSHIPS")
            Language.setMessage(mstrModuleName, 23, "FULL NAME")
            Language.setMessage(mstrModuleName, 24, "BIRTH DATE")
            Language.setMessage(mstrModuleName, 25, "BENEFICIARY")
            Language.setMessage(mstrModuleName, 26, "RELATIONSHIP")
            Language.setMessage(mstrModuleName, 27, "ADDRESS")
            Language.setMessage(mstrModuleName, 28, "DATE FROM")
            Language.setMessage(mstrModuleName, 29, "DATE TO")
            Language.setMessage(mstrModuleName, 30, "SCHOOL/INSTITUTE")
            Language.setMessage(mstrModuleName, 31, "COURSE")
            Language.setMessage(mstrModuleName, 32, "QUALIFICATION")
            Language.setMessage(mstrModuleName, 33, "GRADE")
            Language.setMessage(mstrModuleName, 34, "DATE FROM")
            Language.setMessage(mstrModuleName, 35, "DATE TO")
            Language.setMessage(mstrModuleName, 36, "EMPLOYER")
            Language.setMessage(mstrModuleName, 37, "POSITION HELD")
            Language.setMessage(mstrModuleName, 38, "YEAR")
            Language.setMessage(mstrModuleName, 39, "COURSE TITLE")
            Language.setMessage(mstrModuleName, 40, "AWARD")
            Language.setMessage(mstrModuleName, 41, "PROVIDER")
            Language.setMessage(mstrModuleName, 42, "Printed By :")
            Language.setMessage(mstrModuleName, 43, "Printed Date :")
            Language.setMessage(mstrModuleName, 44, "Page :")
            Language.setMessage(mstrModuleName, 45, "NAME:")
            Language.setMessage(mstrModuleName, 46, "DATE OF BIRTH:")
            Language.setMessage(mstrModuleName, 47, "PLACE OF BIRTH:")
            Language.setMessage(mstrModuleName, 48, "SEX:")
            Language.setMessage(mstrModuleName, 49, "PRESENT ADDRESS:")
            Language.setMessage(mstrModuleName, 50, "? PERSONAL DETAILS")
            Language.setMessage(mstrModuleName, 51, "? CHILDREN/DEPENDANTS")
            Language.setMessage(mstrModuleName, 52, "? EDUCATION/PROFESSIONAL QUALIFICATIONS")
            Language.setMessage(mstrModuleName, 53, "? EMPLOYMENT HISTORY")
            Language.setMessage(mstrModuleName, 54, "? TRAINING/SHORT COURSES")
            Language.setMessage(mstrModuleName, 55, "YES")
            Language.setMessage(mstrModuleName, 56, "NO")
            Language.setMessage(mstrModuleName, 57, "OHERNAME:")
            Language.setMessage(mstrModuleName, 58, "PRESENT TELNO :")
            Language.setMessage(mstrModuleName, 59, "CLASS GROUP:")
            Language.setMessage(mstrModuleName, 60, "CLASS:")
            Language.setMessage(mstrModuleName, 61, "FIRSTNAME:")
            Language.setMessage(mstrModuleName, 62, "LASTNAME:")
            Language.setMessage(mstrModuleName, 63, "ADDRESS:")
            Language.setMessage(mstrModuleName, 64, "MOBILE:")
            Language.setMessage(mstrModuleName, 65, "EMAIL:")
            Language.setMessage(mstrModuleName, 66, "? EMERGENCY CONTACT 1")
            Language.setMessage(mstrModuleName, 67, "? EMERGENCY CONTACT 2")
            Language.setMessage(mstrModuleName, 67, "? EMERGENCY CONTACT 3")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
