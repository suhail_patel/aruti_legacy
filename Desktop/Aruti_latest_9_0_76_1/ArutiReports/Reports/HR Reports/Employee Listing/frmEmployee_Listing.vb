#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployee_Listing

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployee_Listing"
    Private objEmpList As clsEmployee_Listing

    'S.SANDEEP [ 06 SEP 2011 ] -- START
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'S.SANDEEP [ 06 SEP 2011 ] -- END 


    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (09-Jan-2013) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP |15-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
    Dim mdtStartDate As Date = Nothing
    Dim mdtEndDate As Date = Nothing
    'S.SANDEEP |15-JUL-2019| -- END

#End Region

#Region " Constructor "
    Public Sub New()
        objEmpList = New clsEmployee_Listing(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpList.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objEmpType As New clsCommon_Master
            Dim objBranch As New clsStation
            Dim objDept As New clsDepartment
            Dim objjob As New clsJobs
            Dim objCostCenter As New clscostcenter_master
            Dim dsList As New DataSet

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            Dim oPeriod As New clscommom_period_Tran
            'S.SANDEEP |15-JUL-2019| -- END

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'Pinkal (24-Jun-2011) -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            dsList = objEmpType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            With cboEmployeementType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmpType = Nothing
            dsList = Nothing

            'dsList = objBranch.getComboList("Branch", True)
            'With cboBranch
            '    .ValueMember = "stationunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            'objBranch = Nothing
            'dsList = Nothing

            'dsList = objDept.getComboList("Department", True)
            'With cboDepartment
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            'objDept = Nothing
            'dsList = Nothing

            'dsList = objjob.getComboList("Job", True)
            'With cboJob
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            'objjob = Nothing
            'dsList = Nothing

            'dsList = objCostCenter.getComboList("Job", True)
            'With cboCostcenter
            '    .ValueMember = "costcenterunkid"
            '    .DisplayMember = "costcentername"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            'objCostCenter = Nothing
            'dsList = Nothing

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            dsList = oPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP |15-JUL-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboEmployeementType.SelectedValue = 0
            'cboBranch.SelectedValue = 0
            'cboDepartment.SelectedValue = 0
            'cboJob.SelectedValue = 0
            'cboCostcenter.SelectedValue = 0


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (09-Jan-2013) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            dtpBirthdateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpBirthdateFrom.Checked = False
            dtpBirthdateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpBirthdateTo.Checked = False

            dtpAnniversaryDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAnniversaryDateFrom.Checked = False
            dtpAnniversaryDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAnniversaryDateTo.Checked = False

            If ConfigParameter._Object._ShowFirstAppointmentDate Then
                dtpFirstAppointDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpFirstAppointDateFrom.Checked = False
                dtpFirstAppointDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpFirstAppointDateTo.Checked = False
            End If

            dtpAppointmentDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAppointmentDateFrom.Checked = False
            dtpAppointmentDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpAppointmentDateTo.Checked = False

            dtpConfirmationDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpConfirmationDateFrom.Checked = False
            dtpConfirmationDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpConfirmationDateTo.Checked = False

            dtpProbationStartDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpProbationStartDateFrom.Checked = False
            dtpProbationStartDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpProbationStartDateTo.Checked = False

            dtpProbationEndDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpProbationEndDateFrom.Checked = False
            dtpProbationEndDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpProbationEndDateTo.Checked = False

            dtpSuspendedStartDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpSuspendedStartDateFrom.Checked = False
            dtpSuspendedStartDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpSuspendedStartDateTo.Checked = False

            dtpSuspendedEndDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpSuspendedEndDateFrom.Checked = False
            dtpSuspendedEndDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpSuspendedEndDateTo.Checked = False

            dtpReinstatementDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpReinstatementDateFrom.Checked = False
            dtpReinstatementDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpReinstatementDateTo.Checked = False

            dtpEOCDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpEOCDateFrom.Checked = False
            dtpEOCDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpEOCDateTo.Checked = False

            'Pinkal (27-Mar-2013) -- End


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Checked = False
            'Pinkal (24-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployee_Listing_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Listing_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Listing_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me.EZeeHeader1.Title = objEmpList._ReportName
            Me.EZeeHeader1.Message = objEmpList._ReportDesc

            Call FillCombo()
            Call ResetValue()

            dtpFirstAppointDateFrom.Enabled = ConfigParameter._Object._ShowFirstAppointmentDate
            dtpFirstAppointDateTo.Enabled = ConfigParameter._Object._ShowFirstAppointmentDate


            'Pinkal (30-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Visible = User._Object.Privilege._AllowTo_View_Scale
            'Pinkal (30-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Listing_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Listing_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployee_Listing_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_Listing_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployee_Listing_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            objEmpList.SetDefaultValue()

            objEmpList._EmployeeId = cboEmployee.SelectedValue
            objEmpList._EmployeeName = cboEmployee.Text

            objEmpList._EmployeementTypeId = cboEmployeementType.SelectedValue
            objEmpList._EmployeementType = cboEmployeementType.Text

            'objEmpList._BranchId = cboBranch.SelectedValue
            'objEmpList._Branch = cboBranch.Text

            'objEmpList._DepartmentId = cboDepartment.SelectedValue
            'objEmpList._Department = cboDepartment.Text

            'objEmpList._JobId = cboJob.SelectedValue
            'objEmpList._Job = cboJob.Text

            'objEmpList._CostCenterId = cboCostcenter.SelectedValue
            'objEmpList._CostCenter = cboCostcenter.Text


            'S.SANDEEP [ 06 SEP 2011 ] -- START
            objEmpList._ViewByIds = mstrStringIds
            objEmpList._ViewIndex = mintViewIdx
            objEmpList._ViewByName = mstrStringName
            'S.SANDEEP [ 06 SEP 2011 ] -- END 

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEmpList._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes
            objEmpList._Analysis_Fields = mstrAnalysis_Fields
            objEmpList._Analysis_Join = mstrAnalysis_Join
            objEmpList._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpList._Report_GroupName = mstrReport_GroupName
            'Pinkal (09-Jan-2013) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmpList._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END



            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            If dtpBirthdateFrom.Checked Then
                objEmpList._BirthDateFrom = dtpBirthdateFrom.Value.Date
            End If

            If dtpBirthdateTo.Checked Then
                objEmpList._BirthDateTo = dtpBirthdateTo.Value.Date
            End If

            If dtpAnniversaryDateFrom.Checked Then
                objEmpList._AnniversaryDateFrom = dtpAnniversaryDateFrom.Value.Date
            End If

            If dtpAnniversaryDateTo.Checked Then
                objEmpList._AnniversaryDateTo = dtpAnniversaryDateTo.Value.Date
            End If


            If ConfigParameter._Object._ShowFirstAppointmentDate Then

                If dtpFirstAppointDateFrom.Checked Then
                    objEmpList._FirstAppointedDateFrom = dtpFirstAppointDateFrom.Value.Date
                End If

                If dtpFirstAppointDateTo.Checked Then
                    objEmpList._FirstAppointedDateTo = dtpFirstAppointDateTo.Value.Date
                End If

            End If

            If dtpAppointmentDateFrom.Checked Then
                objEmpList._AppointedDateFrom = dtpAppointmentDateFrom.Value.Date
            End If

            If dtpAppointmentDateTo.Checked Then
                objEmpList._AppointedDateTo = dtpAppointmentDateTo.Value.Date
            End If

            If dtpConfirmationDateFrom.Checked Then
                objEmpList._ConfirmationDateFrom = dtpConfirmationDateFrom.Value.Date
            End If

            If dtpConfirmationDateTo.Checked Then
                objEmpList._ConfirmationDateTo = dtpConfirmationDateTo.Value.Date
            End If


            If dtpProbationStartDateFrom.Checked Then
                objEmpList._ProbationStartDateFrom = dtpProbationStartDateFrom.Value.Date
            End If

            If dtpProbationStartDateTo.Checked Then
                objEmpList._ProbationStartDateTo = dtpProbationStartDateTo.Value.Date
            End If

            If dtpProbationEndDateFrom.Checked Then
                objEmpList._ProbationEndDateFrom = dtpProbationEndDateFrom.Value.Date
            End If

            If dtpProbationStartDateTo.Checked Then
                objEmpList._ProbationEndDateTo = dtpProbationEndDateTo.Value.Date
            End If

            If dtpSuspendedStartDateFrom.Checked Then
                objEmpList._SuspendedStartDateFrom = dtpSuspendedStartDateFrom.Value.Date
            End If

            If dtpSuspendedStartDateTo.Checked Then
                objEmpList._SuspendedStartDateTo = dtpSuspendedStartDateTo.Value.Date
            End If

            If dtpSuspendedEndDateFrom.Checked Then
                objEmpList._SuspendedEndDateFrom = dtpSuspendedEndDateFrom.Value.Date
            End If

            If dtpSuspendedEndDateTo.Checked Then
                objEmpList._SuspendedEndDateTo = dtpSuspendedEndDateTo.Value.Date
            End If

            If dtpReinstatementDateFrom.Checked Then
                objEmpList._ReinstatementDateFrom = dtpReinstatementDateFrom.Value.Date
            End If

            If dtpReinstatementDateTo.Checked Then
                objEmpList._ReinstatementDateTo = dtpReinstatementDateTo.Value.Date
            End If

            If dtpEOCDateFrom.Checked Then
                objEmpList._EOCDateFrom = dtpEOCDateFrom.Value.Date
            End If

            If dtpEOCDateTo.Checked Then
                objEmpList._EOCDateTo = dtpEOCDateTo.Value.Date
            End If


            'Pinkal (27-Mar-2013) -- End


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objEmpList._ShowEmployeeScale = chkShowEmpScale.Checked
            'Pinkal (24-Apr-2013) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpList.Generate_DetailReport()

            'S.SANDEEP |15-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}
            objEmpList._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpList._PeriodName = cboPeriod.Text
            'objEmpList.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
            '                                 ConfigParameter._Object._OpenAfterExport, False, ConfigParameter._Object._ShowFirstAppointmentDate)

            objEmpList.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             mdtStartDate, _
                                             mdtEndDate, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, False, ConfigParameter._Object._ShowFirstAppointmentDate)
            'S.SANDEEP |15-JUL-2019| -- END
            
            'S.SANDEEP [04 JUN 2015] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 06 SEP 2011 ] -- START
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Pinkal (09-Jan-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 06 SEP 2011 ] -- END 

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Listing.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Listing"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END


    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (09-Jan-2013) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Combobox Event(S) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date.Date
                mdtEndDate = objPrd._End_Date.Date
                objPrd = Nothing
            Else
                mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployeementType.Text = Language._Object.getCaption(Me.lblEmployeementType.Name, Me.lblEmployeementType.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.btnAdvanceFilter.Text = Language._Object.getCaption(Me.btnAdvanceFilter.Name, Me.btnAdvanceFilter.Text)
			Me.LblBirthdateFrom.Text = Language._Object.getCaption(Me.LblBirthdateFrom.Name, Me.LblBirthdateFrom.Text)
			Me.LblBirthdateTo.Text = Language._Object.getCaption(Me.LblBirthdateTo.Name, Me.LblBirthdateTo.Text)
			Me.LblAnniversaryFrom.Text = Language._Object.getCaption(Me.LblAnniversaryFrom.Name, Me.LblAnniversaryFrom.Text)
			Me.LblAnniversaryDateTo.Text = Language._Object.getCaption(Me.LblAnniversaryDateTo.Name, Me.LblAnniversaryDateTo.Text)
			Me.LblConfirmationDateTo.Text = Language._Object.getCaption(Me.LblConfirmationDateTo.Name, Me.LblConfirmationDateTo.Text)
			Me.LblConfirmationDateFrom.Text = Language._Object.getCaption(Me.LblConfirmationDateFrom.Name, Me.LblConfirmationDateFrom.Text)
			Me.LblSuspendedStartDateTo.Text = Language._Object.getCaption(Me.LblSuspendedStartDateTo.Name, Me.LblSuspendedStartDateTo.Text)
			Me.LblSuspendedStartDateFrom.Text = Language._Object.getCaption(Me.LblSuspendedStartDateFrom.Name, Me.LblSuspendedStartDateFrom.Text)
			Me.LblProbationEndDateTo.Text = Language._Object.getCaption(Me.LblProbationEndDateTo.Name, Me.LblProbationEndDateTo.Text)
			Me.LblProbationEndDateFrom.Text = Language._Object.getCaption(Me.LblProbationEndDateFrom.Name, Me.LblProbationEndDateFrom.Text)
			Me.LblProbationStartDateTo.Text = Language._Object.getCaption(Me.LblProbationStartDateTo.Name, Me.LblProbationStartDateTo.Text)
			Me.LblProbationStartDateFrom.Text = Language._Object.getCaption(Me.LblProbationStartDateFrom.Name, Me.LblProbationStartDateFrom.Text)
			Me.LblAppointDateTo.Text = Language._Object.getCaption(Me.LblAppointDateTo.Name, Me.LblAppointDateTo.Text)
			Me.LblAppointDateFrom.Text = Language._Object.getCaption(Me.LblAppointDateFrom.Name, Me.LblAppointDateFrom.Text)
			Me.LblSuspendedEndDateTo.Text = Language._Object.getCaption(Me.LblSuspendedEndDateTo.Name, Me.LblSuspendedEndDateTo.Text)
			Me.LblSuspendedEndDateFrom.Text = Language._Object.getCaption(Me.LblSuspendedEndDateFrom.Name, Me.LblSuspendedEndDateFrom.Text)
			Me.LblEOCDateTo.Text = Language._Object.getCaption(Me.LblEOCDateTo.Name, Me.LblEOCDateTo.Text)
			Me.LblEOCDateFrom.Text = Language._Object.getCaption(Me.LblEOCDateFrom.Name, Me.LblEOCDateFrom.Text)
			Me.LblReinstatementDateTo.Text = Language._Object.getCaption(Me.LblReinstatementDateTo.Name, Me.LblReinstatementDateTo.Text)
			Me.LblReinstatementDateFrom.Text = Language._Object.getCaption(Me.LblReinstatementDateFrom.Name, Me.LblReinstatementDateFrom.Text)
			Me.LblFirstAppointDateTo.Text = Language._Object.getCaption(Me.LblFirstAppointDateTo.Name, Me.LblFirstAppointDateTo.Text)
			Me.LblFirstAppointDateFrom.Text = Language._Object.getCaption(Me.LblFirstAppointDateFrom.Name, Me.LblFirstAppointDateFrom.Text)
			Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.Name, Me.chkShowEmpScale.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
