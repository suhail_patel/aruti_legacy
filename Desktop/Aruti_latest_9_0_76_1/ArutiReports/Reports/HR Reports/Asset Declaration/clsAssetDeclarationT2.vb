'************************************************************************************************************************************
'Class Name : clsAssetDeclarationT2.vb
'Purpose    :
'Date       :25/10/2018
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System

#End Region

''' <summary>
''' Purpose: Report Generation Class
''' Developer: Hemant
''' </summary>
Public Class clsAssetDeclarationT2
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssetDeclarationT2"
    Dim objDataOperation As clsDataOperation
    Dim mstrCompanyCode As String = Company._Object._Code
    Dim mstrCompanyName As String = Company._Object._Name

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
#Region " Private variables "

    Private mintAssetDeclarationUnkid As Integer = -1
    Private mintEmployeeUnkid As Integer = -1
    Private mstrEmployeeCode As String = String.Empty
    Private mstrEmployeeName As String = String.Empty
    Private mstrEmployeeDesignation As String = String.Empty
    Private mstrEmployeeDepartment As String = String.Empty
    Private mdtFinancialYear As DateTime
    Private mstrEmployeeTINno As String = String.Empty
    Private mstrWorkStation As String = String.Empty
    Private mstrTitle As String = String.Empty

    Private mstrNonOfficialDeclaration As String = String.Empty
    Private objCompany As New clsCompany_Master
    Private objConfig As New clsConfigOptions
    Private mintCompanyUnkid As Integer = -1
    Private mintLanguageid As Integer = 0
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    Public WriteOnly Property _AssetDeclarationUnkid() As Integer
        Set(ByVal value As Integer)
            mintAssetDeclarationUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeDesignation() As String
        Set(ByVal value As String)
            mstrEmployeeDesignation = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeDepartment() As String
        Set(ByVal value As String)
            mstrEmployeeDepartment = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeTINno() As String
        Set(ByVal value As String)
            mstrEmployeeTINno = value
        End Set
    End Property

    Public WriteOnly Property _FinancialYear() As DateTime
        Set(ByVal value As DateTime)
            mdtFinancialYear = value
        End Set
    End Property

    Public WriteOnly Property _WorkStation() As String
        Set(ByVal value As String)
            mstrWorkStation = value
        End Set
    End Property

    Public WriteOnly Property _Title() As String
        Set(ByVal value As String)
            mstrTitle = value
        End Set
    End Property

    Public WriteOnly Property _NonOfficialDeclaration() As String
        Set(ByVal value As String)
            mstrNonOfficialDeclaration = value
        End Set
    End Property
    'Sohail (29 Jan 2013) -- End


    'Sohail (11 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property
    'Sohail (11 Apr 2012) -- End

    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    'Sohail (23 Apr 2012) -- End


    'Anjan [ 22 Nov 2013 ] -- Start
    'ENHANCEMENT : Requested by Rutta
    Public WriteOnly Property _LanguageId() As Integer
        Set(ByVal value As Integer)
            mintLanguageid = value
        End Set
    End Property
    'Anjan [22 Nov 2013 ] -- End

    'Sohail (16 Apr 2015) -- Start
    'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
    'Sohail (16 Apr 2015) -- End

#End Region

#Region " Report Generation "

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
       
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then mintCompanyUnkid = xCompanyUnkid
            objCompany._Companyunkid = mintCompanyUnkid
            objConfig._Companyunkid = mintCompanyUnkid
            mstrCompanyCode = objCompany._Code
            mstrCompanyName = objCompany._Name

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt
            If Not IsNothing(objRpt) Then
                Dim strPath As String = xExportReportPath

                If Right(strPath, 1) = "\" Then
                    strPath = Left(strPath, Len(strPath) - 1)
                End If
                Me._ReportName = "Asset Declaration"

                Call ReportExecute(objRpt, PrintAction, ExportAction, strPath, xOpenReportAfterExport)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rptRow As DataRow

        Dim objAssetDeclaration As New clsAssetdeclaration_master
        Dim objAssetBusinessDealing As New clsAsset_business_dealT2_tran
        Dim objAssetSecurities As New clsAsset_securitiesT2_tran
        Dim objAssetBank As New clsAsset_bankT2_tran
        Dim objAssetProperties As New clsAsset_propertiesT2_tran
        Dim objAssetLiabilities As New clsAsset_liabilitiesT2_tran
        Dim objAssetRelatives As New clsAsset_relativesT2_tran
        Dim objAssetBusinessDealingDependant As New clsAsset_businessdealT2depn_tran
        Dim objAssetSecuritiesDependant As New clsAsset_securitiesT2depn_tran
        Dim objAssetBankAccountsDependant As New clsAsset_bankT2depn_tran
        Dim objAssetPropertiesDependant As New clsAsset_propertiesT2depn_tran

        Dim rpt_DataBusinessDealing As DataTable
        Dim rpt_DataSecurities As DataTable
        Dim rpt_DataBank As DataTable
        Dim rpt_DataProperties As DataTable
        Dim rpt_DataLiabilities As DataTable
        Dim rpt_DataRelatives As DataTable
        Dim rpt_DataBusinessDealingDependant As DataTable
        Dim rpt_DataSecuritiesDependant As DataTable
        Dim rpt_DataBankAccountsDependant As DataTable
        Dim rpt_DataPropertiesDependant As DataTable


        Dim dtBusinessDealing As DataTable
        Dim dtSecurities As DataTable
        Dim dtBank As DataTable
        Dim dtProperties As DataTable
        Dim dtLiabilities As DataTable
        Dim dtRelatives As DataTable
        Dim dtBusinessDealingDependant As DataTable
        Dim dtSecuritiesDependant As DataTable
        Dim dtBankAccountsDependant As DataTable
        Dim dtPropertiesDependant As DataTable

        'Hemant (03 Dec 2018) -- Start
        'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
        Dim strfilter As String
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim dtDependantList As DataTable
        'Hemant (03 Dec 2018) -- End

        Try
            objDataOperation = New clsDataOperation

            Dim dicCurr As New Dictionary(Of Integer, String)
            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", False)
            For Each dsRow As DataRow In dsList.Tables("Currency").Rows
                dicCurr.Add(CInt(dsRow.Item("countryunkid")), dsRow.Item("currency_sign").ToString)
            Next
            objCurrency = Nothing

            If mstrDatabaseName.Trim = "" Then mstrDatabaseName = strDatabaseName

            objAssetDeclaration._DatabaseName = mstrDatabaseName

            objAssetDeclaration._Assetdeclarationunkid(strDatabaseName) = mintAssetDeclarationUnkid


            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            strfilter = "hremployee_master.employeeunkid = " & mintEmployeeUnkid & ""
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'dsList = objDependant.GetList(strDatabaseName, _
            '                          intUserUnkid, _
            '                          intYearUnkid, _
            '                          intCompanyUnkid, _
            '                          dtPeriodStart, _
            '                          dtPeriodStart, _
            '                          strUserModeSetting, True, _
            '                          False, "List", , , strfilter, _
            '                          True)
            dsList = objDependant.GetList(strDatabaseName, _
                                      intUserUnkid, _
                                      intYearUnkid, _
                                      intCompanyUnkid, _
                                      dtPeriodStart, _
                                      dtPeriodEnd, _
                                      strUserModeSetting, True, _
                                      False, "List", , , strfilter, _
                                      True, , dtPeriodEnd)
            'Sohail (18 May 2019) -- End

            dtDependantList = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'Hemant (03 Dec 2018) -- End


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            rptRow = rpt_Data.Tables("ArutiTable").NewRow

            rptRow.Item("column1") = mintEmployeeUnkid.ToString
            rptRow.Item("column2") = mstrEmployeeName
            rptRow.Item("column3") = mstrEmployeeDesignation
            rptRow.Item("column4") = mstrEmployeeDepartment
            rptRow.Item("column5") = mstrEmployeeTINno

            rptRow.Item("column21") = objAssetDeclaration._Resources
            rptRow.Item("column22") = objAssetDeclaration._Debt
            rptRow.Item("column23") = mstrCompanyName

            rpt_Data.Tables("ArutiTable").Rows.Add(rptRow)


            objAssetBusinessDealing._DatabaseName = mstrDatabaseName

            objAssetBusinessDealing._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtBusinessDealing = objAssetBusinessDealing._Datasource
            rpt_DataBusinessDealing = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtBusinessDealing.Rows
                rptRow = rpt_DataBusinessDealing.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column2") = dtRow.Item("assetsectorunkid").ToString
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")
                Dim drSector As DataRow() = dsList.Tables("Asset Sector").Select("masterunkid = " & CInt(dtRow.Item("assetsectorunkid").ToString) & " ", "name")
                If drSector.Length > 0 Then
                    rptRow.Item("column2") = drSector(0).Item("name").ToString
                Else
                    rptRow.Item("column2") = ""
                End If
                'Hemant (15 Nov 2018) -- End
                rptRow.Item("column3") = dtRow.Item("company_org_name").ToString
                rptRow.Item("column4") = dtRow.Item("company_tin_no").ToString
                rptRow.Item("column5") = CDate(dtRow.Item("registration_date").ToString).ToShortDateString()
                rptRow.Item("column6") = dtRow.Item("address_location").ToString
                rptRow.Item("column7") = dtRow.Item("business_type").ToString
                rptRow.Item("column8") = dtRow.Item("position_held").ToString

                dsList = Nothing
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                'dsList = (New clsAsset_business_dealT2_tran).getClientsList("Clients", True)
                dsList = (New clsMasterData).getComboListAssetsupplierborrowerList("Clients")
                'Sohail (22 Nov 2018) -- End
                Dim drClient As DataRow() = dsList.Tables("Clients").Select("id = " & CInt(dtRow.Item("is_supplier_client").ToString) & " ", "name")
                If drClient.Length > 0 Then
                    rptRow.Item("column9") = drClient(0).Item("name").ToString
                Else
                    rptRow.Item("column9") = ""
                End If

                rptRow.Item("column10") = dtRow.Item("monthly_annual_earnings").ToString
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column11") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column11") = ""
                End If
                rptRow.Item("column12") = dtRow.Item("business_contact_no").ToString
                rptRow.Item("column13") = dtRow.Item("shareholders_names").ToString
                rpt_DataBusinessDealing.Rows.Add(rptRow)
            Next

            'Securities
            objAssetSecurities._DatabaseName = mstrDatabaseName
            objAssetSecurities._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtSecurities = objAssetSecurities._Datasource
            rpt_DataSecurities = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtSecurities.Rows
                rptRow = rpt_DataSecurities.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column2") = dtRow.Item("securitiestypeunkid").ToString
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECURITIES, True, "Asset Securities")
                Dim drSecurities As DataRow() = dsList.Tables("Asset Securities").Select("masterunkid = " & CInt(dtRow.Item("securitiestypeunkid").ToString) & " ", "name")
                If drSecurities.Length > 0 Then
                    rptRow.Item("column2") = drSecurities(0).Item("name").ToString
                Else
                    rptRow.Item("column2") = ""
                End If
                'Hemant (15 Nov 2018) -- End
                rptRow.Item("column3") = dtRow.Item("certificate_no").ToString
                rptRow.Item("column4") = dtRow.Item("no_of_shares").ToString
                rptRow.Item("column5") = dtRow.Item("company_business_name").ToString
                rptRow.Item("column6") = Format(CDec(dtRow.Item("market_value")), GUI.fmtCurrency)
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column7") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column7") = ""
                End If

                rptRow.Item("column8") = CDate(dtRow.Item("acquisition_date").ToString).ToShortDateString()

                rpt_DataSecurities.Rows.Add(rptRow)
            Next

            'Bank Accounts
            objAssetBank._DatabaseName = mstrDatabaseName

            objAssetBank._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtBank = objAssetBank._Datasource
            rpt_DataBank = rpt_Data.Tables("ArutiTable").Clone
            For Each dtRow As DataRow In dtBank.Rows
                rptRow = rpt_DataBank.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("bank_name").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column3") = dtRow.Item("account_type").ToString
                dsList = Nothing
                dsList = (New clsBankAccType).getComboList(True, "AccType")
                Dim drAccType As DataRow() = dsList.Tables("AccType").Select("accounttypeunkid = " & CInt(dtRow.Item("accounttypeunkid").ToString) & " ", "name")
                If drAccType.Length > 0 Then
                    rptRow.Item("column3") = drAccType(0).Item("name").ToString
                Else
                    rptRow.Item("column3") = ""
                End If
                'Hemant (15 Nov 2018) -- End
                rptRow.Item("column4") = dtRow.Item("account_no").ToString
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column5") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column5") = ""
                End If
                rptRow.Item("column6") = dtRow.Item("deposits_source").ToString
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                rptRow.Item("column7") = dtRow.Item("specify").ToString
                'Hemant (03 Dec 2018) -- End

                rpt_DataBank.Rows.Add(rptRow)
            Next

            'Real properties
            objAssetProperties._DatabaseName = mstrDatabaseName
            objAssetProperties._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtProperties = objAssetProperties._Datasource
            rpt_DataProperties = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtProperties.Rows
                rptRow = rpt_DataProperties.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("asset_name").ToString
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = dtRow.Item("registration_title_no").ToString
                rptRow.Item("column5") = Format(CDec(dtRow.Item("estimated_value")), GUI.fmtCurrency)
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column6") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column6") = ""
                End If
                rptRow.Item("column7") = dtRow.Item("funds_source").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column8") = dtRow.Item("asset_location").ToString

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'rptRow.Item("column8") = CDate(dtRow.Item("acquisition_date").ToString).ToShortDateString()
                If dtRow.Item("acquisition_date") IsNot DBNull.Value Then
                    rptRow.Item("column8") = CDate(dtRow.Item("acquisition_date").ToString).ToShortDateString()
                End If
                'Hemant (03 Dec 2018) -- End


                'Hemant (15 Nov 2018) -- End
                rptRow.Item("column9") = dtRow.Item("asset_use").ToString


                rpt_DataProperties.Rows.Add(rptRow)
            Next


            'Liabilities
            objAssetLiabilities._DatabaseName = mstrDatabaseName
            objAssetLiabilities._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtLiabilities = objAssetLiabilities._Datasource
            rpt_DataLiabilities = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtLiabilities.Rows
                rptRow = rpt_DataLiabilities.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("institution_name").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column3") = dtRow.Item("liability_type").ToString
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.LIABILITY_TYPE_ASSET_DECLARATION_TEMPLATE2, True, "Liability Type")
                Dim drLiabilityType As DataRow() = dsList.Tables("Liability Type").Select("masterunkid = " & CInt(dtRow.Item("liabilitytypeunkid").ToString) & " ", "name")
                If drLiabilityType.Length > 0 Then
                    rptRow.Item("column3") = drLiabilityType(0).Item("name").ToString
                Else
                    rptRow.Item("column3") = ""
                End If
                'Hemant (15 Nov 2018) -- End
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'If dicCurr.ContainsKey(CInt(dtRow.Item("original_balance"))) = True Then
                If dicCurr.ContainsKey(CInt(dtRow.Item("origcountryunkid"))) = True Then
                    'Hemant (15 Nov 2018) -- End
                    rptRow.Item("column4") = Format(CDec(dtRow.Item("original_balance")), GUI.fmtCurrency) + " " + dicCurr.Item(CInt(dtRow.Item("origcountryunkid"))).ToString
                Else
                    rptRow.Item("column4") = ""
                End If

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'If dicCurr.ContainsKey(CInt(dtRow.Item("outstanding_balance"))) = True Then
                If dicCurr.ContainsKey(CInt(dtRow.Item("outcountryunkid"))) = True Then
                    'Hemant (15 Nov 2018) -- End
                    rptRow.Item("column5") = Format(CDec(dtRow.Item("outstanding_balance")), GUI.fmtCurrency) + " " + dicCurr.Item(CInt(dtRow.Item("outcountryunkid"))).ToString
                Else
                    rptRow.Item("column5") = ""
                End If
                rptRow.Item("column6") = dtRow.Item("duration").ToString
                rptRow.Item("column7") = dtRow.Item("loan_purpose").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'rptRow.Item("column8") = CDate(dtRow.Item("liabilitydate").ToString).ToShortDateString()
                'rptRow.Item("column9") = CDate(dtRow.Item("payoffdate").ToString).ToShortDateString()
                If dtRow.Item("liabilitydate") IsNot DBNull.Value Then
                    rptRow.Item("column8") = CDate(dtRow.Item("liabilitydate").ToString).ToShortDateString()
                End If
                If dtRow.Item("payoffdate") IsNot DBNull.Value Then
                    rptRow.Item("column9") = CDate(dtRow.Item("payoffdate").ToString).ToShortDateString()
                End If
                'Hemant (03 Dec 2018) -- End                
                rptRow.Item("column10") = dtRow.Item("liability_type").ToString
                'Hemant (15 Nov 2018) -- End

                rpt_DataLiabilities.Rows.Add(rptRow)
            Next


            'relatives
            objAssetRelatives._DatabaseName = mstrDatabaseName
            objAssetRelatives._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtRelatives = objAssetRelatives._Datasource
            rpt_DataRelatives = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtRelatives.Rows
                rptRow = rpt_DataRelatives.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("relative_employeename").ToString

                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
                Dim drRelations As DataRow() = dsList.Tables(0).Select("masterunkid = " & dtRow.Item("relationshipunkid").ToString & " ", "name")
                If drRelations.Length > 0 Then
                    rptRow.Item("column3") = drRelations(0).Item("name").ToString
                Else
                    rptRow.Item("column3") = ""
                End If

                rpt_DataRelatives.Rows.Add(rptRow)
            Next


            'Business Dealing Dependant
            objAssetBusinessDealingDependant._DatabaseName = mstrDatabaseName
            objAssetBusinessDealingDependant._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtBusinessDealingDependant = objAssetBusinessDealingDependant._Datasource
            rpt_DataBusinessDealingDependant = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtBusinessDealingDependant.Rows
                rptRow = rpt_DataBusinessDealingDependant.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column2") = dtRow.Item("assetsectorunkid").ToString
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")
                Dim drSector As DataRow() = dsList.Tables("Asset Sector").Select("masterunkid = " & CInt(dtRow.Item("assetsectorunkid").ToString) & " ", "name")
                If drSector.Length > 0 Then
                    rptRow.Item("column2") = drSector(0).Item("name").ToString
                Else
                    rptRow.Item("column2") = ""
                End If
                'Hemant (15 Nov 2018) -- End
                rptRow.Item("column3") = dtRow.Item("company_org_name").ToString
                rptRow.Item("column4") = dtRow.Item("company_tin_no").ToString
                rptRow.Item("column5") = CDate(dtRow.Item("registration_date").ToString).ToShortDateString()
                rptRow.Item("column6") = dtRow.Item("address_location").ToString
                rptRow.Item("column7") = dtRow.Item("business_type").ToString
                rptRow.Item("column8") = dtRow.Item("position_held").ToString
                rptRow.Item("column9") = dtRow.Item("is_supplier_client").ToString

                dsList = Nothing
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                'dsList = (New clsAsset_business_dealT2_tran).getClientsList("Clients", True)
                dsList = (New clsMasterData).getComboListAssetsupplierborrowerList("Clients")
                'Sohail (22 Nov 2018) -- End
                Dim drClient As DataRow() = dsList.Tables("Clients").Select("id = " & CInt(dtRow.Item("is_supplier_client").ToString) & " ", "name")
                If drClient.Length > 0 Then
                    rptRow.Item("column9") = drClient(0).Item("name").ToString
                Else
                    rptRow.Item("column9") = ""
                End If

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'If dicCurr.ContainsKey(CInt(dtRow.Item("monthly_annual_earnings"))) = True Then
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    'Hemant (15 Nov 2018) -- End
                    rptRow.Item("column10") = Format(CDec(dtRow.Item("monthly_annual_earnings")), GUI.fmtCurrency) + " " + dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column10") = ""
                End If
                rptRow.Item("column11") = dtRow.Item("business_contact_no").ToString
                rptRow.Item("column12") = dtRow.Item("shareholders_names").ToString

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
                Dim drRelations As DataRow() = dsList.Tables("Relations").Select("masterunkid = " & CInt(dtRow.Item("relationshipunkid").ToString) & " ", "name")
                If drRelations.Length > 0 Then
                    rptRow.Item("column13") = drRelations(0).Item("name").ToString
                Else
                End If

                If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & CInt(dtRow.Item("dependantunkid").ToString) & " ", "DpndtBefName")
                    If drDependantList.Length > 0 Then
                        rptRow.Item("column14") = drDependantList(0).Item("DpndtBefName").ToString
                    Else
                    End If
                End If
                'Hemant (03 Dec 2018) -- End

                rpt_DataBusinessDealingDependant.Rows.Add(rptRow)
            Next

            'Securities Dependant
            objAssetSecuritiesDependant._DatabaseName = mstrDatabaseName
            objAssetSecuritiesDependant._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtSecuritiesDependant = objAssetSecuritiesDependant._Datasource
            rpt_DataSecuritiesDependant = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtSecuritiesDependant.Rows
                rptRow = rpt_DataSecuritiesDependant.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column2") = dtRow.Item("relationshipunkid").ToString
                'rptRow.Item("column3") = dtRow.Item("securitiestypeunkid").ToString
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
                Dim drRelations As DataRow() = dsList.Tables("Relations").Select("masterunkid = " & CInt(dtRow.Item("relationshipunkid").ToString) & " ", "name")
                If drRelations.Length > 0 Then
                    rptRow.Item("column2") = drRelations(0).Item("name").ToString
                Else
                End If

                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECURITIES, True, "Asset Securities")
                Dim drSecurities As DataRow() = dsList.Tables("Asset Securities").Select("masterunkid = " & CInt(dtRow.Item("securitiestypeunkid").ToString) & " ", "name")
                If drSecurities.Length > 0 Then
                    rptRow.Item("column3") = drSecurities(0).Item("name").ToString
                Else
                    rptRow.Item("column3") = ""
                End If
                'Hemant (15 Nov 2018) -- End

                rptRow.Item("column4") = dtRow.Item("certificate_no").ToString
                rptRow.Item("column5") = dtRow.Item("no_of_shares").ToString
                rptRow.Item("column6") = dtRow.Item("company_business_name").ToString
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column7") = Format(CDec(dtRow.Item("market_value")), GUI.fmtCurrency) + " " + dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column7") = ""
                End If
                rptRow.Item("column8") = CDate(dtRow.Item("acquisition_date").ToString).ToShortDateString()

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & CInt(dtRow.Item("dependantunkid").ToString) & " ", "DpndtBefName")
                    If drDependantList.Length > 0 Then
                        rptRow.Item("column9") = drDependantList(0).Item("DpndtBefName").ToString
                    Else
                    End If
                End If
                'Hemant (03 Dec 2018) -- End

                rpt_DataSecuritiesDependant.Rows.Add(rptRow)
            Next

            ' Bank Accounts Dependant
            objAssetBankAccountsDependant._DatabaseName = mstrDatabaseName
            objAssetBankAccountsDependant._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtBankAccountsDependant = objAssetBankAccountsDependant._Datasource
            rpt_DataBankAccountsDependant = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtBankAccountsDependant.Rows
                rptRow = rpt_DataBankAccountsDependant.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("bank_name").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'rptRow.Item("column3") = dtRow.Item("account_type").ToString
                dsList = Nothing
                dsList = (New clsBankAccType).getComboList(True, "AccType")
                Dim drAccType As DataRow() = dsList.Tables("AccType").Select("accounttypeunkid = " & CInt(dtRow.Item("accounttypeunkid").ToString) & " ", "name")
                If drAccType.Length > 0 Then
                    rptRow.Item("column3") = drAccType(0).Item("name").ToString
                Else
                    rptRow.Item("column3") = ""
                End If
                'Hemant (15 Nov 2018) -- End
                rptRow.Item("column4") = dtRow.Item("account_no").ToString
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column5") = dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column5") = ""
                End If

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
                Dim drRelations As DataRow() = dsList.Tables("Relations").Select("masterunkid = " & CInt(dtRow.Item("relationshipunkid").ToString) & " ", "name")
                If drRelations.Length > 0 Then
                    rptRow.Item("column6") = drRelations(0).Item("name").ToString
                Else
                End If

                If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & CInt(dtRow.Item("dependantunkid").ToString) & " ", "DpndtBefName")
                    If drDependantList.Length > 0 Then
                        rptRow.Item("column7") = drDependantList(0).Item("DpndtBefName").ToString
                    Else
                    End If
                End If

                rptRow.Item("column8") = dtRow.Item("specify").ToString
                'Hemant (03 Dec 2018) -- End

                rpt_DataBankAccountsDependant.Rows.Add(rptRow)
            Next

            'Real properties Dependant
            objAssetPropertiesDependant._DatabaseName = mstrDatabaseName
            objAssetPropertiesDependant._Assetdeclarationt2unkid(strDatabaseName) = mintAssetDeclarationUnkid

            dtPropertiesDependant = objAssetPropertiesDependant._Datasource
            rpt_DataPropertiesDependant = rpt_Data.Tables("ArutiTable").Clone

            For Each dtRow As DataRow In dtPropertiesDependant.Rows
                rptRow = rpt_DataPropertiesDependant.NewRow

                rptRow.Item("column1") = mintEmployeeUnkid.ToString
                rptRow.Item("column2") = dtRow.Item("asset_name").ToString
                rptRow.Item("column3") = dtRow.Item("location").ToString
                rptRow.Item("column4") = dtRow.Item("registration_title_no").ToString
                rptRow.Item("column5") = Format(CDec(dtRow.Item("estimated_value")), GUI.fmtCurrency)
                If dicCurr.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                    rptRow.Item("column5") = Format(CDec(dtRow.Item("estimated_value")), GUI.fmtCurrency) + " " + dicCurr.Item(CInt(dtRow.Item("countryunkid"))).ToString
                Else
                    rptRow.Item("column5") = Format(CDec(dtRow.Item("estimated_value")), GUI.fmtCurrency)
                End If
                'Hemant (23 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                If dtRow.Item("acquisition_date") IsNot DBNull.Value Then
                    rptRow.Item("column6") = CDate(dtRow.Item("acquisition_date").ToString).ToShortDateString()
                End If
                'Hemant (23 Nov 2018) -- End

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                dsList = Nothing
                dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
                Dim drRelations As DataRow() = dsList.Tables("Relations").Select("masterunkid = " & CInt(dtRow.Item("relationshipunkid").ToString) & " ", "name")
                If drRelations.Length > 0 Then
                    rptRow.Item("column7") = drRelations(0).Item("name").ToString
                Else
                End If

                If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & CInt(dtRow.Item("dependantunkid").ToString) & " ", "DpndtBefName")
                    If drDependantList.Length > 0 Then
                        rptRow.Item("column8") = drDependantList(0).Item("DpndtBefName").ToString
                    Else
                    End If
                End If
                'Hemant (03 Dec 2018) -- End

                rpt_DataPropertiesDependant.Rows.Add(rptRow)
            Next

            objRpt = New ArutiReport.Designer.rptAssetDeclarationT2

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                               True, _
                                                False, _
                                                "arutiLogo1", _
                                                "arutiLogo2", _
                                                arrImageRow, _
                                                , _
                                                , _
                                                , _
                                                objConfig._GetLeftMargin, _
                                                objConfig._GetRightMargin, _
                                                objConfig._TOPPageMarging)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            objRpt.SetDataSource(rpt_Data)

            objRpt.Subreports("rptADBank").SetDataSource(rpt_DataBank)
            objRpt.Subreports("rptADBusinessDealings").SetDataSource(rpt_DataBusinessDealing)
            objRpt.Subreports("rptADShare").SetDataSource(rpt_DataSecurities)
            objRpt.Subreports("rptADProperties").SetDataSource(rpt_DataProperties)
            objRpt.Subreports("rptADLiabilities").SetDataSource(rpt_DataLiabilities)
            objRpt.Subreports("rptADRelatives").SetDataSource(rpt_DataRelatives)
            objRpt.Subreports("rptADDeptBusiness").SetDataSource(rpt_DataBusinessDealingDependant)
            objRpt.Subreports("rptADDeptShare").SetDataSource(rpt_DataSecuritiesDependant)
            objRpt.Subreports("rptADDeptBank").SetDataSource(rpt_DataBankAccountsDependant)
            objRpt.Subreports("rptADDeptProperties").SetDataSource(rpt_DataPropertiesDependant)


            Call ReportFunction.TextChange(objRpt, "lblFormCaption", Language.getMessage(mstrModuleName, 1, "NMB CODE OF CONDUCT ACKNOWLEGMENT AND CONFLICT OF INTEREST DISCLOSURE FORM", ))

            Call ReportFunction.TextChange(objRpt, "lblone", Language.getMessage(mstrModuleName, 4, "1.", ))
            Call ReportFunction.TextChange(objRpt, "txtone", Language.getMessage(mstrModuleName, 5, "GENERAL PERSONAL INFORMATION:"))

            Call ReportFunction.TextChange(objRpt, "lblstaffname", Language.getMessage(mstrModuleName, 6, "Staff Name"))
            Call ReportFunction.TextChange(objRpt, "lblDesignation", Language.getMessage(mstrModuleName, 7, "Designation"))
            Call ReportFunction.TextChange(objRpt, "lblDept", Language.getMessage(mstrModuleName, 8, "Department / Unit"))
            Call ReportFunction.TextChange(objRpt, "lblTinNo", Language.getMessage(mstrModuleName, 9, "TIN Number: Personal/Business"))
            Call ReportFunction.TextChange(objRpt, "lblgenralinfonote", Language.getMessage(mstrModuleName, 10, "(Note: No. of years in total including employment prior to joining") & " " & mstrCompanyCode & " " & Language.getMessage(mstrModuleName, 11, "and at") & " " & mstrCompanyCode)

            Call ReportFunction.TextChange(objRpt, "lbltwo", Language.getMessage(mstrModuleName, 12, "2."))
            Call ReportFunction.TextChange(objRpt, "txttwo", Language.getMessage(mstrModuleName, 13, "INTRODUCTORY NOTES: "))

            Call ReportFunction.TextChange(objRpt, "txt2_1", mstrCompanyCode & " " & Language.getMessage(mstrModuleName, 14, "Board of Directors and Senior Management require all staff to ensure compliance to ") & " " & mstrCompanyCode & " " & Language.getMessage(mstrModuleName, 15, "Code of Conduct Policy Requirements. This is critical to ensure we demonstrate good governance in order to protect the integrity and credibility of the bank and maintain trust & confidence of our stakeholders."))

            Call ReportFunction.TextChange(objRpt, "txt2_2", Language.getMessage(mstrModuleName, 16, "Compliance to the Code of Conduct is a responsibility for all staff members of ") & _
                                                             Language.getMessage(mstrModuleName, 17, "Bank Plc. ( ") & " " & mstrCompanyCode & " " & _
                                                             Language.getMessage(mstrModuleName, 18, ") as it sets the standards of behaviour expected of all staff. In accordance with the ") & " " & mstrCompanyCode & " " & _
                                                             Language.getMessage(mstrModuleName, 19, "Code of Conduct on Conflict of Interests, members of staff are required to declare the followings: "))

            Call ReportFunction.TextChange(objRpt, "lblCompliance_a", Language.getMessage(mstrModuleName, 20, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_a", Language.getMessage(mstrModuleName, 21, "Positions held outside ") & " " & mstrCompanyCode & " " & _
                                                                      Language.getMessage(mstrModuleName, 22, "( both Paid or Unpaid) and business dealings where the staff member is a Director, Shareholder or benefit from income that is not related to his / her employment with ") & " " & mstrCompanyCode)

            Call ReportFunction.TextChange(objRpt, "lblCompliance_b", Language.getMessage(mstrModuleName, 133, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_b", Language.getMessage(mstrModuleName, 23, "All Bank accounts held with ") & " " & mstrCompanyCode & " " & _
                                                                      Language.getMessage(mstrModuleName, 24, "and any other Bank in or outside Tanzania "))

            Call ReportFunction.TextChange(objRpt, "lblCompliance_c", Language.getMessage(mstrModuleName, 25, "(c)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_c", Language.getMessage(mstrModuleName, 26, "All liabilities (i.e. loans / other financial commitments) due by him or her "))

            Call ReportFunction.TextChange(objRpt, "lblCompliance_d", Language.getMessage(mstrModuleName, 27, "(d)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_d", Language.getMessage(mstrModuleName, 28, "All assets i.e. Real properties, shares, business ownerships, stocks, bonds held by the staff "))

            Call ReportFunction.TextChange(objRpt, "lblCompliance_e", Language.getMessage(mstrModuleName, 29, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_e", Language.getMessage(mstrModuleName, 30, "Any Relatives in employment with ") & " " & mstrCompanyCode & " " & _
                                                                      Language.getMessage(mstrModuleName, 31, "and respective department, which includes members of: "))


            Call ReportFunction.TextChange(objRpt, "lblCompliance_e_1", Language.getMessage(mstrModuleName, 32, "(i)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_e_1", Language.getMessage(mstrModuleName, 33, "Immediate family by blood (brother, sister, child (including legally adopted child), parent) "))

            Call ReportFunction.TextChange(objRpt, "lblCompliance_e_2", Language.getMessage(mstrModuleName, 134, "(ii)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_e_2", Language.getMessage(mstrModuleName, 135, "Extended family (grandparent, grandchild, cousin, uncle, aunt, niece, nephew) "))

            Call ReportFunction.TextChange(objRpt, "lblCompliance_e_3", Language.getMessage(mstrModuleName, 34, "(iii)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_e_3", Language.getMessage(mstrModuleName, 35, "Immediate family by marriage (spouse, brother-in-law, sister-in-law, father-in-law, mother-in-law) "))

            Call ReportFunction.TextChange(objRpt, "txt2_3", Language.getMessage(mstrModuleName, 36, "In addition, members of staff are also required to declare assets, interests and liabilities owned by their Dependants (spouse or children under 18 years)."))
            Call ReportFunction.TextChange(objRpt, "txt2_4", Language.getMessage(mstrModuleName, 37, "Failure to declare and submit the declaration within the prescribed time shall constitute to a breach of the ") & " " & mdtFinancialYear & " " & _
                                                                     Language.getMessage(mstrModuleName, 136, "Any changes to previous disclosures should be declared to Line Manager immediately and also declared to Huris system within 30 days from the date of the change. "))


            Call ReportFunction.TextChange(objRpt, "txt2_5", Language.getMessage(mstrModuleName, 38, "In addition, Human Resources will ensure that new staff complete this form immediately after joining ") & mstrCompanyCode & _
                                                             Language.getMessage(mstrModuleName, 39, ". The signed and completed form must be returned by employees to the Head of Human Resources. "))

            Call ReportFunction.TextChange(objRpt, "txt2_6", Language.getMessage(mstrModuleName, 40, "The followings constitute violations/noncompliance to ") & " " & mstrCompanyCode & " " & _
                                                             Language.getMessage(mstrModuleName, 41, "Code of Conduct Policy Requirements:"))

            Call ReportFunction.TextChange(objRpt, "lblConductPolicy_a", Language.getMessage(mstrModuleName, 42, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_a", Language.getMessage(mstrModuleName, 43, "Failure to declare and submit the declaration within the prescribed time shall constitute to a breach of the ") & " " & mstrCompanyCode & " " & _
                                                                      Language.getMessage(mstrModuleName, 44, "Code of Conduct. "))

            Call ReportFunction.TextChange(objRpt, "lblConductPolicy_b", Language.getMessage(mstrModuleName, 45, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtCompliance_a", Language.getMessage(mstrModuleName, 46, "Submitting a false declaration or information relating to his/her assets, interests or liabilities or withholding relevant information. "))
            Call ReportFunction.TextChange(objRpt, "txt2_7", Language.getMessage(mstrModuleName, 47, "Management reserves the right to take disciplinary action against any member of staff who violated the above conditions."))


            '*** Business Dealing
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblOneBusiness", Language.getMessage(mstrModuleName, 48, "1."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "txtOneBusiness", Language.getMessage(mstrModuleName, 49, "Business Dealings"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessSector", Language.getMessage(mstrModuleName, 50, "Asset Sector"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessCompanyName", Language.getMessage(mstrModuleName, 51, "Name of Company / Organisation"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessCompanyTinno", Language.getMessage(mstrModuleName, 52, "Company TIN No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessDateOfReg", Language.getMessage(mstrModuleName, 53, "Date of Registration"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessAddress", Language.getMessage(mstrModuleName, 54, "Physical Address"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessTypeOfBusiness", Language.getMessage(mstrModuleName, 55, "Type / Nature of Business"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessPosition", Language.getMessage(mstrModuleName, 56, "Position Held"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessIsSupplier", Language.getMessage(mstrModuleName, 57, "Supplier / Borrower"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessEarning", Language.getMessage(mstrModuleName, 58, "Monthly/Annual Earnings"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessContactno", Language.getMessage(mstrModuleName, 59, "Business Contact No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBusinessDealings"), "lblBusinessShareHolder", Language.getMessage(mstrModuleName, 60, "Shareholders Names"))

            '*** Staff Ownership
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblTwoShare", Language.getMessage(mstrModuleName, 61, "2."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "txtTwoShare", Language.getMessage(mstrModuleName, 62, "Staff Ownership of Shares/Stocks or Bonds"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblShareName", Language.getMessage(mstrModuleName, 63, "Asset Securities"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblShareCertificateNo", Language.getMessage(mstrModuleName, 64, "Certificate No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblShareNoOfShare", Language.getMessage(mstrModuleName, 65, "No of shares"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblShareNameOfCompany", Language.getMessage(mstrModuleName, 66, "Name of the Company"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblShareMarketValue", Language.getMessage(mstrModuleName, 67, "Current Market Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADShare"), "lblShareDateOfAcquisition", Language.getMessage(mstrModuleName, 68, "Date of Acquisition"))

            '*** Bank Accounts
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblThreeBank", Language.getMessage(mstrModuleName, 69, "3."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "txtThreeBank", Language.getMessage(mstrModuleName, 70, " Bank Accounts"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblBank", Language.getMessage(mstrModuleName, 71, "Name of Bank / Financial Institution"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblAcccountType", Language.getMessage(mstrModuleName, 72, "Type of Account"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblAcccountNo", Language.getMessage(mstrModuleName, 73, "Account Number"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblAmountBank", Language.getMessage(mstrModuleName, 74, "Currency"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblExpectedSourceBank", Language.getMessage(mstrModuleName, 75, "Expected Source of deposits/funds"))
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Call ReportFunction.TextChange(objRpt.Subreports("rptADBank"), "lblBankSpecify", Language.getMessage(mstrModuleName, 159, "Specify"))
            'Hemant (03 Dec 2018) -- End


            '*** Properties
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblFourProperties", Language.getMessage(mstrModuleName, 76, "4."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "txtFourProperties", Language.getMessage(mstrModuleName, 77, "Real properties"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblPropertiesName", Language.getMessage(mstrModuleName, 78, "Name of Asset"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesLocation", Language.getMessage(mstrModuleName, 79, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesRegno", Language.getMessage(mstrModuleName, 80, "Registration No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesEstimateValue", Language.getMessage(mstrModuleName, 81, "Estimated Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesSourceFund", Language.getMessage(mstrModuleName, 82, "Sources of funds"))
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            'Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesAssestlocation", Language.getMessage(mstrModuleName, 83, "Location of the asset"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesacquisitiondate", Language.getMessage(mstrModuleName, 83, "Acquisition Date"))
            'Hemant (15 Nov 2018) -- End
            Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesNatureofUse", Language.getMessage(mstrModuleName, 84, "Nature of use"))

            '*** Liabilities
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblFiveLiabilities", Language.getMessage(mstrModuleName, 85, "5."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "txtFiveLiabilities", Language.getMessage(mstrModuleName, 86, "Liabilities"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesInstitutionName", Language.getMessage(mstrModuleName, 87, "Name of Institution"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesType", Language.getMessage(mstrModuleName, 88, "Type of liability"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesOriginalBal", Language.getMessage(mstrModuleName, 89, "Original  Balance"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lbllblLiabilitiesOutstandingBal", Language.getMessage(mstrModuleName, 90, "Outstanding Balance"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesDuration", Language.getMessage(mstrModuleName, 91, "Duration"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesLoanPurpose", Language.getMessage(mstrModuleName, 92, "Purpose of Loan"))
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesDateOfLiability", Language.getMessage(mstrModuleName, 155, "Date Of Liability"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesPayOffDate", Language.getMessage(mstrModuleName, 156, "PayOff Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADLiabilities"), "lblLiabilitiesRemark", Language.getMessage(mstrModuleName, 157, "Remark"))
            'Hemant (15 Nov 2018) -- End

            '*** Relatives
            Call ReportFunction.TextChange(objRpt.Subreports("rptADRelatives"), "lblSixRelative", Language.getMessage(mstrModuleName, 93, "6."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADRelatives"), "txtSixRelative", Language.getMessage(mstrModuleName, 94, "Relatives in employment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADRelatives"), "lblRelativesName", Language.getMessage(mstrModuleName, 95, "Name of Relative"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADRelatives"), "lblRelativesNatureOfRelationship", Language.getMessage(mstrModuleName, 96, "Nature of Relationship"))

            '*** Business Dealing Dependant
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblSevenDeptBusiness", Language.getMessage(mstrModuleName, 97, "7."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "txtSevenDeptBusiness", Language.getMessage(mstrModuleName, 98, "Business or Commercial undertaking(s) or operation(s) owned by the Dependant"))
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessRelationship", Language.getMessage(mstrModuleName, 112, "Relationship"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessDependentName", Language.getMessage(mstrModuleName, 158, "Dependent Name"))
            'Hemant (03 Dec 2018) -- End
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessSector", Language.getMessage(mstrModuleName, 99, "Asset Sector"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessCompanyName", Language.getMessage(mstrModuleName, 100, "Name of Company / Organisation"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessCompanyTinno", Language.getMessage(mstrModuleName, 101, "Company TIN No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessDateOfReg", Language.getMessage(mstrModuleName, 102, "Date of Registration"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessAddress", Language.getMessage(mstrModuleName, 103, "Physical Address"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessTypeOfBusiness", Language.getMessage(mstrModuleName, 104, "Type / Nature of Business"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessPosition", Language.getMessage(mstrModuleName, 105, "Position Held"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessIsSupplier", Language.getMessage(mstrModuleName, 106, "Supplier / Borrower"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessEarning", Language.getMessage(mstrModuleName, 107, "Monthly/Annual Earnings"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessContactno", Language.getMessage(mstrModuleName, 108, "Business Contact No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBusiness"), "lblDeptBusinessShareHolder", Language.getMessage(mstrModuleName, 109, "Shareholders Names"))

            '*** Securities Dependant
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblOneBusiness", Language.getMessage(mstrModuleName, 110, "8."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "txtOneBusiness", Language.getMessage(mstrModuleName, 111, "Shares/Stocks or Bonds owned by the Dependant"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareRelationship", Language.getMessage(mstrModuleName, 112, "Relationship"))
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareDependantName", Language.getMessage(mstrModuleName, 158, "Dependent Name"))
            'Hemant (03 Dec 2018) -- End
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareName", Language.getMessage(mstrModuleName, 113, "Asset Securities"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareCertificateNo", Language.getMessage(mstrModuleName, 114, "Certificate No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareNoOfShare", Language.getMessage(mstrModuleName, 115, "No of shares"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareNameOfCompany", Language.getMessage(mstrModuleName, 116, "Name of the Company"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareMarketValue", Language.getMessage(mstrModuleName, 117, "Current Market Value"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptShare"), "lblDeptShareDateOfAcquisition", Language.getMessage(mstrModuleName, 118, "Date of Acquisition"))

            '*** Bank Accounts Dependant
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblEightDeptBank", Language.getMessage(mstrModuleName, 119, "9."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "txtEightDeptBank", Language.getMessage(mstrModuleName, 120, "Bank Accounts held by Dependant"))
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptBankRelationship", Language.getMessage(mstrModuleName, 112, "Relationship"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptBankDependentName", Language.getMessage(mstrModuleName, 158, "Dependent Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptBankSpecify", Language.getMessage(mstrModuleName, 159, "Specify"))
            'Hemant (03 Dec 2018) -- End
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptBankName", Language.getMessage(mstrModuleName, 121, "Name of Bank / Financial Institution"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptAccountType", Language.getMessage(mstrModuleName, 122, "Type of Account"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptAccountNo", Language.getMessage(mstrModuleName, 123, "Account Number"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptBank"), "lblDeptCurrency", Language.getMessage(mstrModuleName, 124, "Currency"))

            '*** Properties Dependant
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblTenDeptProperties", Language.getMessage(mstrModuleName, 125, "10."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "txtTenDeptProperties", Language.getMessage(mstrModuleName, 126, "Real properties"))
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblDeptPropertiesRelationship", Language.getMessage(mstrModuleName, 112, "Relationship"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblDeptPropertiesDependentName", Language.getMessage(mstrModuleName, 158, "Dependent Name"))
            'Hemant (03 Dec 2018) -- End
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblDeptPropertiesAssestName", Language.getMessage(mstrModuleName, 127, "Name of Asset"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblDeptPropertiesLocation", Language.getMessage(mstrModuleName, 128, "Location"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblDeptPropertiesRegNo", Language.getMessage(mstrModuleName, 129, "Registration No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lblDeptPropertiesEstimateValue", Language.getMessage(mstrModuleName, 130, "Estimated Value"))
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            'Call ReportFunction.TextChange(objRpt.Subreports("rptADProperties"), "lblpropertiesAssestlocation", Language.getMessage(mstrModuleName, 83, "Location of the asset"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptADDeptProperties"), "lbldepnpropertiesacquisitiondate", Language.getMessage(mstrModuleName, 83, "Acquisition Date"))
            'Hemant (15 Nov 2018) -- End

            'Call ReportFunction.TextChange(objRpt, "lblTransactionDate", Language.getMessage(mstrModuleName, 131, "Declaration Date :"))
            'Call ReportFunction.TextChange(objRpt, "txtTransactionDate", objAssetDeclaration._TransactionDate.ToShortDateString)
            'Call ReportFunction.TextChange(objRpt, "lblFY", Language.getMessage(mstrModuleName, 132, "Financial Year :"))
            Dim objMaster As New clsMasterData
            'Dim dsYear As DataSet = objMaster.Get_Database_Year_List("DBYear", True, Company._Object._Companyunkid)
            'Dim dRow() As DataRow = dsYear.Tables("DBYear").Select(" start_date <= '" & eZeeDate.convertDate(objAssetDeclaration._TransactionDate) & "'  AND  end_date >= '" & eZeeDate.convertDate(objAssetDeclaration._TransactionDate) & "' ")
            'If dRow.Length > 0 Then
            '    Call ReportFunction.TextChange(objRpt, "txtFY", dRow(0).Item("financialyear_name").ToString)
            'Else
            '    Call ReportFunction.TextChange(objRpt, "txtFY", "")
            'End If


            Call ReportFunction.TextChange(objRpt, "lblthree", Language.getMessage(mstrModuleName, 137, "3"))
            Call ReportFunction.TextChange(objRpt, "txtthree", Language.getMessage(mstrModuleName, 138, "DECLARATION OF INTERESTS, ASSETS AND LIABILITIES FOR STAFF MEMBERS :"))

            Call ReportFunction.TextChange(objRpt, "lblfour", Language.getMessage(mstrModuleName, 139, "4"))
            Call ReportFunction.TextChange(objRpt, "txtfour", Language.getMessage(mstrModuleName, 140, "DECLARATION OF ASSETS AND INTERESTS FOR DEPENDANTS :"))

            Call ReportFunction.TextChange(objRpt, "lblfive", Language.getMessage(mstrModuleName, 141, "5"))
            Call ReportFunction.TextChange(objRpt, "txtfive", Language.getMessage(mstrModuleName, 142, "DEPONENT :"))

            Call ReportFunction.TextChange(objRpt, "txtFive_one", Language.getMessage(mstrModuleName, 144, "I"))
            Call ReportFunction.TextChange(objRpt, "txtFive_two", Language.getMessage(mstrModuleName, 145, "Do hereby declare that"))

            Call ReportFunction.TextChange(objRpt, "txtFive_three", Language.getMessage(mstrModuleName, 146, "I have read and understood the ") & " " & mstrCompanyCode & " " & _
                                                             Language.getMessage(mstrModuleName, 147, "Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct."))
            Call ReportFunction.TextChange(objRpt, "txtFive_four", Language.getMessage(mstrModuleName, 148, "I undertake to fully comply with the letter and spirit of the Code of Conduct. I understand that I have a personal responsibility to ensure that I am fully aware of the requirements under the Code of Conduct and if I am not aware of the requirements, I should familiarize myself with the content before signing this acknowledgement form."))


            Call ReportFunction.TextChange(objRpt, "txtFive_five", Language.getMessage(mstrModuleName, 149, "I have made the disclosures above as required under the ") & " " & mstrCompanyCode & " " & _
                                                                    Language.getMessage(mstrModuleName, 150, "Code of Conduct and confirm that the said disclosures are made willingly and reflect the truth of my status as of the date indicated below."))

            Call ReportFunction.TextChange(objRpt, "txtFive_six", Language.getMessage(mstrModuleName, 151, "I further understand that in the event that the said disclosures are found to be incomplete or untrue, I will face disciplinary action which may include termination of my employment with the Bank."))
            Call ReportFunction.TextChange(objRpt, "lblsignaturenote", Language.getMessage(mstrModuleName, 152, "1"))
            Call ReportFunction.TextChange(objRpt, "txtsignaturenote", Language.getMessage(mstrModuleName, 153, "1"))
            Call ReportFunction.TextChange(objRpt, "txtFive_seven", Language.getMessage(mstrModuleName, 154, "No ‘wet-ink’ signature of Staff is required when the online form is completed.  The declaration is considered signed by the relevant Staff after clicking ‘Submit Now’ field on Aruti"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "NMB CODE OF CONDUCT ACKNOWLEGMENT AND CONFLICT OF INTEREST DISCLOSURE FORM")
            Language.setMessage(mstrModuleName, 4, "1.")
            Language.setMessage(mstrModuleName, 5, "GENERAL PERSONAL INFORMATION:")
            Language.setMessage(mstrModuleName, 6, "Staff Name")
            Language.setMessage(mstrModuleName, 7, "Designation")
            Language.setMessage(mstrModuleName, 8, "Department / Unit")
            Language.setMessage(mstrModuleName, 9, "TIN Number: Personal/Business")
            Language.setMessage(mstrModuleName, 10, "(Note: No. of years in total including employment prior to joining")
            Language.setMessage(mstrModuleName, 11, "and at")
            Language.setMessage(mstrModuleName, 12, "2.")
            Language.setMessage(mstrModuleName, 13, "INTRODUCTORY NOTES:")
            Language.setMessage(mstrModuleName, 14, "Board of Directors and Senior Management require all staff to ensure compliance to")
            Language.setMessage(mstrModuleName, 15, "Code of Conduct Policy Requirements. This is critical to ensure we demonstrate good governance in order to protect the integrity and credibility of the bank and maintain trust & confidence of our stakeholders.")
            Language.setMessage(mstrModuleName, 16, "Compliance to the Code of Conduct is a responsibility for all staff members of")
            Language.setMessage(mstrModuleName, 17, "Bank Plc. (")
            Language.setMessage(mstrModuleName, 18, ") as it sets the standards of behaviour expected of all staff. In accordance with the")
            Language.setMessage(mstrModuleName, 19, "Code of Conduct on Conflict of Interests, members of staff are required to declare the followings:")
            Language.setMessage(mstrModuleName, 20, "(a)")
            Language.setMessage(mstrModuleName, 21, "Positions held outside")
            Language.setMessage(mstrModuleName, 22, "( both Paid or Unpaid) and business dealings where the staff member is a Director, Shareholder or benefit from income that is not related to his / her employment with")
            Language.setMessage(mstrModuleName, 23, "All Bank accounts held with")
            Language.setMessage(mstrModuleName, 24, "and any other Bank in or outside Tanzania")
            Language.setMessage(mstrModuleName, 25, "(c)")
            Language.setMessage(mstrModuleName, 26, "All liabilities (i.e. loans / other financial commitments) due by him or her")
            Language.setMessage(mstrModuleName, 27, "(d)")
            Language.setMessage(mstrModuleName, 28, "All assets i.e. Real properties, shares, business ownerships, stocks, bonds held by the staff")
            Language.setMessage(mstrModuleName, 29, "(b)")
            Language.setMessage(mstrModuleName, 30, "Any Relatives in employment with")
            Language.setMessage(mstrModuleName, 31, "and respective department, which includes members of:")
            Language.setMessage(mstrModuleName, 32, "(i)")
            Language.setMessage(mstrModuleName, 33, "Immediate family by blood (brother, sister, child (including legally adopted child), parent)")
            Language.setMessage(mstrModuleName, 34, "(iii)")
            Language.setMessage(mstrModuleName, 35, "Immediate family by marriage (spouse, brother-in-law, sister-in-law, father-in-law, mother-in-law)")
            Language.setMessage(mstrModuleName, 36, "In addition, members of staff are also required to declare assets, interests and liabilities owned by their Dependants (spouse or children under 18 years).")
            Language.setMessage(mstrModuleName, 37, "Failure to declare and submit the declaration within the prescribed time shall constitute to a breach of the")
            Language.setMessage(mstrModuleName, 38, "In addition, Human Resources will ensure that new staff complete this form immediately after joining")
            Language.setMessage(mstrModuleName, 39, ". The signed and completed form must be returned by employees to the Head of Human Resources.")
            Language.setMessage(mstrModuleName, 40, "The followings constitute violations/noncompliance to")
            Language.setMessage(mstrModuleName, 41, "Code of Conduct Policy Requirements:")
            Language.setMessage(mstrModuleName, 42, "(a)")
            Language.setMessage(mstrModuleName, 43, "Failure to declare and submit the declaration within the prescribed time shall constitute to a breach of the")
            Language.setMessage(mstrModuleName, 44, "Code of Conduct.")
            Language.setMessage(mstrModuleName, 45, "(b)")
            Language.setMessage(mstrModuleName, 46, "Submitting a false declaration or information relating to his/her assets, interests or liabilities or withholding relevant information.")
            Language.setMessage(mstrModuleName, 47, "Management reserves the right to take disciplinary action against any member of staff who violated the above conditions.")
            Language.setMessage(mstrModuleName, 48, "1.")
            Language.setMessage(mstrModuleName, 49, "Business Dealings")
            Language.setMessage(mstrModuleName, 50, "Asset Sector")
            Language.setMessage(mstrModuleName, 51, "Name of Company / Organisation")
            Language.setMessage(mstrModuleName, 52, "Company TIN No.")
            Language.setMessage(mstrModuleName, 53, "Date of Registration")
            Language.setMessage(mstrModuleName, 54, "Physical Address")
            Language.setMessage(mstrModuleName, 55, "Type / Nature of Business")
            Language.setMessage(mstrModuleName, 56, "Position Held")
            Language.setMessage(mstrModuleName, 57, "Supplier / Borrower")
            Language.setMessage(mstrModuleName, 58, "Monthly/Annual Earnings")
            Language.setMessage(mstrModuleName, 59, "Business Contact No")
            Language.setMessage(mstrModuleName, 60, "Shareholders Names")
            Language.setMessage(mstrModuleName, 61, "2.")
            Language.setMessage(mstrModuleName, 62, "Staff Ownership of Shares/Stocks or Bonds")
            Language.setMessage(mstrModuleName, 63, "Asset Securities")
            Language.setMessage(mstrModuleName, 64, "Certificate No")
            Language.setMessage(mstrModuleName, 65, "No of shares")
            Language.setMessage(mstrModuleName, 66, "Name of the Company")
            Language.setMessage(mstrModuleName, 67, "Current Market Value")
            Language.setMessage(mstrModuleName, 68, "Date of Acquisition")
            Language.setMessage(mstrModuleName, 69, "3.")
            Language.setMessage(mstrModuleName, 70, " Bank Accounts")
            Language.setMessage(mstrModuleName, 71, "Name of Bank / Financial Institution")
            Language.setMessage(mstrModuleName, 72, "Type of Account")
            Language.setMessage(mstrModuleName, 73, "Account Number")
            Language.setMessage(mstrModuleName, 74, "Currency")
            Language.setMessage(mstrModuleName, 75, "Expected Source of deposits/funds")
            Language.setMessage(mstrModuleName, 76, "4.")
            Language.setMessage(mstrModuleName, 77, "Real properties")
            Language.setMessage(mstrModuleName, 78, "Name of Asset")
            Language.setMessage(mstrModuleName, 79, "Location")
            Language.setMessage(mstrModuleName, 80, "Registration No")
            Language.setMessage(mstrModuleName, 81, "Estimated Value")
            Language.setMessage(mstrModuleName, 82, "Sources of funds")
            Language.setMessage(mstrModuleName, 83, "Acquisition Date")
            Language.setMessage(mstrModuleName, 84, "Nature of use")
            Language.setMessage(mstrModuleName, 85, "5.")
            Language.setMessage(mstrModuleName, 86, "Liabilities")
            Language.setMessage(mstrModuleName, 87, "Name of Institution")
            Language.setMessage(mstrModuleName, 88, "Type of liability")
            Language.setMessage(mstrModuleName, 89, "Original  Balance")
            Language.setMessage(mstrModuleName, 90, "Outstanding Balance")
            Language.setMessage(mstrModuleName, 91, "Duration")
            Language.setMessage(mstrModuleName, 92, "Purpose of Loan")
            Language.setMessage(mstrModuleName, 93, "6.")
            Language.setMessage(mstrModuleName, 94, "Relatives in employment")
            Language.setMessage(mstrModuleName, 95, "Name of Relative")
            Language.setMessage(mstrModuleName, 96, "Nature of Relationship")
            Language.setMessage(mstrModuleName, 97, "7.")
            Language.setMessage(mstrModuleName, 98, "Business or Commercial undertaking(s) or operation(s) owned by the Dependant")
            Language.setMessage(mstrModuleName, 99, "Asset Sector")
            Language.setMessage(mstrModuleName, 100, "Name of Company / Organisation")
            Language.setMessage(mstrModuleName, 101, "Company TIN No.")
            Language.setMessage(mstrModuleName, 102, "Date of Registration")
            Language.setMessage(mstrModuleName, 103, "Physical Address")
            Language.setMessage(mstrModuleName, 104, "Type / Nature of Business")
            Language.setMessage(mstrModuleName, 105, "Position Held")
            Language.setMessage(mstrModuleName, 106, "Supplier / Borrower")
            Language.setMessage(mstrModuleName, 107, "Monthly/Annual Earnings")
            Language.setMessage(mstrModuleName, 108, "Business Contact No")
            Language.setMessage(mstrModuleName, 109, "Shareholders Names")
            Language.setMessage(mstrModuleName, 110, "8.")
            Language.setMessage(mstrModuleName, 111, "Shares/Stocks or Bonds owned by the Dependant")
            Language.setMessage(mstrModuleName, 112, "Relationship")
            Language.setMessage(mstrModuleName, 113, "Asset Securities")
            Language.setMessage(mstrModuleName, 114, "Certificate No")
            Language.setMessage(mstrModuleName, 115, "No of shares")
            Language.setMessage(mstrModuleName, 116, "Name of the Company")
            Language.setMessage(mstrModuleName, 117, "Current Market Value")
            Language.setMessage(mstrModuleName, 118, "Date of Acquisition")
            Language.setMessage(mstrModuleName, 119, "9.")
            Language.setMessage(mstrModuleName, 120, "Bank Accounts held by Dependant")
            Language.setMessage(mstrModuleName, 121, "Name of Bank / Financial Institution")
            Language.setMessage(mstrModuleName, 122, "Type of Account")
            Language.setMessage(mstrModuleName, 123, "Account Number")
            Language.setMessage(mstrModuleName, 124, "Currency")
            Language.setMessage(mstrModuleName, 125, "10.")
            Language.setMessage(mstrModuleName, 126, "Real properties")
            Language.setMessage(mstrModuleName, 127, "Name of Asset")
            Language.setMessage(mstrModuleName, 128, "Location")
            Language.setMessage(mstrModuleName, 129, "Registration No")
            Language.setMessage(mstrModuleName, 130, "Estimated Value")
            Language.setMessage(mstrModuleName, 133, "(b)")
            Language.setMessage(mstrModuleName, 134, "(ii)")
            Language.setMessage(mstrModuleName, 135, "Extended family (grandparent, grandchild, cousin, uncle, aunt, niece, nephew)")
            Language.setMessage(mstrModuleName, 136, "Any changes to previous disclosures should be declared to Line Manager immediately and also declared to Huris system within 30 days from the date of the change.")
            Language.setMessage(mstrModuleName, 137, "3")
            Language.setMessage(mstrModuleName, 138, "DECLARATION OF INTERESTS, ASSETS AND LIABILITIES FOR STAFF MEMBERS :")
            Language.setMessage(mstrModuleName, 139, "4")
            Language.setMessage(mstrModuleName, 140, "DECLARATION OF ASSETS AND INTERESTS FOR DEPENDANTS :")
            Language.setMessage(mstrModuleName, 141, "5")
            Language.setMessage(mstrModuleName, 142, "DEPONENT :")
            Language.setMessage(mstrModuleName, 144, "I")
            Language.setMessage(mstrModuleName, 145, "Do hereby declare that")
            Language.setMessage(mstrModuleName, 146, "I have read and understood the")
            Language.setMessage(mstrModuleName, 147, "Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct.")
            Language.setMessage(mstrModuleName, 148, "I undertake to fully comply with the letter and spirit of the Code of Conduct. I understand that I have a personal responsibility to ensure that I am fully aware of the requirements under the Code of Conduct and if I am not aware of the requirements, I should familiarize myself with the content before signing this acknowledgement form.")
            Language.setMessage(mstrModuleName, 149, "I have made the disclosures above as required under the")
            Language.setMessage(mstrModuleName, 150, "Code of Conduct and confirm that the said disclosures are made willingly and reflect the truth of my status as of the date indicated below.")
            Language.setMessage(mstrModuleName, 151, "I further understand that in the event that the said disclosures are found to be incomplete or untrue, I will face disciplinary action which may include termination of my employment with the Bank.")
            Language.setMessage(mstrModuleName, 152, "1")
            Language.setMessage(mstrModuleName, 153, "1")
            Language.setMessage(mstrModuleName, 154, "No ‘wet-ink’ signature of Staff is required when the online form is completed.  The declaration is considered signed by the relevant Staff after clicking ‘Submit Now’ field on Aruti")
            Language.setMessage(mstrModuleName, 155, "Date Of Liability")
            Language.setMessage(mstrModuleName, 156, "PayOff Date")
            Language.setMessage(mstrModuleName, 157, "Remark")
            Language.setMessage(mstrModuleName, 158, "Dependent Name")
            Language.setMessage(mstrModuleName, 159, "Specify")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
