'************************************************************************************************************************************
'Class Name : frmAssetDeclarationCategoryWise.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssetDeclarationCategoryWise

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentListReport"
    Private objAssetDeclaration As clsAssetDeclarationCategoryWise

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrReport_GroupName As String = ""

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objAssetDeclaration = New clsAssetDeclarationCategoryWise(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAssetDeclaration.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Employees Declared Value Analysis"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Employee Category Wise Asset Declaration Analysis"))
                .SelectedIndex = 0
            End With

            dsList = objMData.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 1
            End With

            dsList = objMData.GetAD_Categories_List(True, "List")
            With cboADCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMData.GetCondition(True, True, True, True)
            dsList = objMData.GetCondition(True, True, True, True, True)
            'Nilay (10-Nov-2016) -- End
            With cboDeclareOperator
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            With cboLiabOperator
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboADCatOperator
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboAppointment.SelectedValue = 1
            cboEmployee.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            dtpAsOnDate.Value = ConfigParameter._Object._CurrentDateAndTime

            dtpDate1.Value = dtpAsOnDate.Value
            dtpDate2.Value = dtpAsOnDate.Value

            dtpDate1.Checked = False
            dtpDate2.Checked = False

            cboDeclareOperator.SelectedValue = 0
            txtDeclareAmtFrom.Text = ""
            txtDeclareAmtTo.Text = ""
            cboLiabOperator.SelectedValue = 0
            txtLiabAmtFrom.Text = ""
            txtLiabAmtTo.Text = ""
            cboADCategory.SelectedValue = 0
            cboADCatOperator.SelectedValue = 0
            txtADCatAmtFrom.Text = 0
            txtADCatAmtTo.Text = 0
            chkInactiveemp.Checked = False
            chkShowEmpSal.Checked = False

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            objAssetDeclaration.setDefaultOrderBy(chkShowEmpSal.Checked)
            txtOrderBy.Text = objAssetDeclaration.OrderByDisplay

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAssetDeclaration.SetDefaultValue()

            If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
                If dtpDate1.Checked = True AndAlso dtpDate2.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), enMsgBoxStyle.Information)
                    dtpDate2.Focus()
                    Return False
                ElseIf dtpDate1.Checked = False AndAlso dtpDate2.Checked = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), enMsgBoxStyle.Information)
                    dtpDate1.Focus()
                    Return False
                ElseIf dtpDate1.Checked = True AndAlso dtpDate2.Checked = True Then
                    If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Appointment To Date cannot be less then Appointment From Date."), enMsgBoxStyle.Information)
                        dtpDate2.Focus()
                        Return False
                    End If
                End If
            End If

            If CInt(cboDeclareOperator.SelectedValue) > 0 AndAlso txtDeclareAmtFrom.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter Estimated Delcared Amount."), enMsgBoxStyle.Information)
                txtDeclareAmtFrom.Focus()
                Return False
            ElseIf CInt(cboDeclareOperator.SelectedValue) = enComparison_Operator.BETWEEN AndAlso txtDeclareAmtTo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please enter Estimated Delcared To Amount."), enMsgBoxStyle.Information)
                txtDeclareAmtTo.Focus()
                Return False
            End If

            If CInt(cboLiabOperator.SelectedValue) > 0 AndAlso txtLiabAmtFrom.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please enter Estimated Liability Amount."), enMsgBoxStyle.Information)
                txtLiabAmtFrom.Focus()
                Return False
            ElseIf CInt(cboLiabOperator.SelectedValue) = enComparison_Operator.BETWEEN AndAlso txtLiabAmtTo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please enter Estimated Liability To Amount."), enMsgBoxStyle.Information)
                txtLiabAmtTo.Focus()
                Return False
            End If

            If CInt(cboADCategory.SelectedValue) > 0 Then
                'If CInt(cboADCatOperator.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select comparison operator."), enMsgBoxStyle.Information) '
                '    cboADCatOperator.Focus()
                '    Return False
                'Else
                If CInt(cboADCatOperator.SelectedValue) > 0 AndAlso txtADCatAmtFrom.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please enter Estimated Asset Category Amount."), enMsgBoxStyle.Information)
                    txtADCatAmtFrom.Focus()
                    Return False
                ElseIf CInt(cboADCatOperator.SelectedValue) = enComparison_Operator.BETWEEN AndAlso txtADCatAmtTo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please enter Estimated Asset Category To Amount."), enMsgBoxStyle.Information)
                    txtADCatAmtTo.Focus()
                    Return False
                End If
            End If

            objAssetDeclaration._AppointmentTypeId = cboAppointment.SelectedValue
            objAssetDeclaration._AppointmentTypeName = cboAppointment.Text
            objAssetDeclaration._AsOnDate = dtpAsOnDate.Value.Date
            objAssetDeclaration._EmployeeId = cboEmployee.SelectedValue
            objAssetDeclaration._EmployeeName = cboEmployee.Text
            objAssetDeclaration._IncluderInactiveEmp = chkInactiveemp.Checked




            If dtpDate1.Checked = True Then
                objAssetDeclaration._Date1 = dtpDate1.Value.Date
            End If

            If dtpDate2.Visible = True Then
                If dtpDate2.Checked = True Then
                    objAssetDeclaration._Date2 = dtpDate2.Value.Date
                End If
            End If

            objAssetDeclaration._DeclareOperatorId = CInt(cboDeclareOperator.SelectedValue)
            objAssetDeclaration._DeclareOperatorName = cboDeclareOperator.Text
            If txtDeclareAmtFrom.Text <> "" Then
                objAssetDeclaration._DeclareAmtFrom = txtDeclareAmtFrom.Decimal
                objAssetDeclaration._DeclareAmtTo = txtDeclareAmtTo.Decimal
            End If

            objAssetDeclaration._LiabOperatorId = CInt(cboLiabOperator.SelectedValue)
            objAssetDeclaration._LiabOperatorName = cboLiabOperator.Text
            If txtLiabAmtFrom.Text <> "" Then
                objAssetDeclaration._LiabAmtFrom = txtLiabAmtFrom.Decimal
                objAssetDeclaration._LiabAmtTo = txtLiabAmtTo.Decimal
            End If

            objAssetDeclaration._AssetCategoryId = CInt(cboADCategory.SelectedValue)
            objAssetDeclaration._AssetCategoryName = cboADCategory.Text
            objAssetDeclaration._ADCatOperatorId = CInt(cboADCatOperator.SelectedValue)
            objAssetDeclaration._ADCatOperatorName = cboADCatOperator.Text
            objAssetDeclaration._ADCatAmtFrom = txtADCatAmtFrom.Decimal
            objAssetDeclaration._ADCatAmtTo = txtADCatAmtTo.Decimal

            objAssetDeclaration._ReportTypeId = cboReportType.SelectedIndex
            objAssetDeclaration._ReportTypeName = cboReportType.Text
            objAssetDeclaration._ShowSalary = chkShowEmpSal.Checked
            objAssetDeclaration._ViewByIds = mstrStringIds
            objAssetDeclaration._ViewIndex = mintViewIdx
            objAssetDeclaration._ViewByName = mstrStringName
            objAssetDeclaration._Analysis_Fields = mstrAnalysis_Fields
            objAssetDeclaration._Analysis_Join = mstrAnalysis_Join
            objAssetDeclaration._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAssetDeclaration._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            objAssetDeclaration._Report_GroupName = mstrReport_GroupName
            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAssetDeclaration._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmAssetDeclarationCategoryWise_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAssetDeclaration = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssetDeclarationCategoryWise_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssetDeclarationCategoryWise_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssetDeclarationCategoryWise_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssetDeclarationCategoryWise_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objAssetDeclaration._ReportName
            Me._Message = objAssetDeclaration._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssetDeclarationCategoryWise_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssetDeclarationCategoryWise_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssetDeclarationCategoryWise.SetMessages()
            objfrm._Other_ModuleNames = "clsEmp_AD_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssetDeclarationCategoryWise_Language_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclaration.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
            objAssetDeclaration.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, _
                                                  cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclaration.generateReport(cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            objAssetDeclaration.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, _
                                                  cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objAssetDeclaration.setOrderBy(chkShowEmpSal.Checked)
            txtOrderBy.Text = objAssetDeclaration.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Controls Events "

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAppointment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'cboADCategory.SelectedValue = 0
            'cboADCatOperator.SelectedValue = 0
            'txtADCatAmtFrom.Text = "" : txtADCatAmtTo.Text = ""
            'Select Case cboReportType.SelectedIndex
            '    Case 0
            '        cboADCategory.Enabled = False
            '        cboADCatOperator.Enabled = False
            '        txtADCatAmtFrom.Enabled = False
            '        txtADCatAmtTo.Enabled = False
            '    Case 1
            '        cboADCategory.Enabled = True
            '        cboADCatOperator.Enabled = True
            '        txtADCatAmtFrom.Enabled = True
            '        txtADCatAmtTo.Enabled = True
            'End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDeclareOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeclareOperator.SelectedIndexChanged
        Try
            If CInt(cboDeclareOperator.SelectedValue) > 0 Then
                txtDeclareAmtFrom.Enabled = True
            Else
                txtDeclareAmtFrom.Text = ""
                txtDeclareAmtFrom.Enabled = False
            End If
            If CInt(cboDeclareOperator.SelectedValue) = enComparison_Operator.BETWEEN Then
                lblDeclareAnd.Visible = True
                txtDeclareAmtTo.Visible = True
            Else
                lblDeclareAnd.Visible = False
                txtDeclareAmtTo.Text = ""
                txtDeclareAmtTo.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeclareOperator_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLiabOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLiabOperator.SelectedIndexChanged
        Try
            If CInt(cboLiabOperator.SelectedValue) > 0 Then
                txtLiabAmtFrom.Enabled = True
            Else
                txtLiabAmtFrom.Text = ""
                txtLiabAmtFrom.Enabled = False
            End If
            If CInt(cboLiabOperator.SelectedValue) = enComparison_Operator.BETWEEN Then
                lblLiabAnd.Visible = True
                txtLiabAmtTo.Visible = True
            Else
                lblLiabAnd.Visible = False
                txtLiabAmtTo.Text = ""
                txtLiabAmtTo.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLiabOperator_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboADCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboADCategory.SelectedIndexChanged
        Try
            If CInt(cboADCategory.SelectedValue) > 0 Then
                cboADCatOperator.Enabled = True
            Else
                cboADCatOperator.SelectedValue = 0
                cboADCatOperator.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboADCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboADCatOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboADCatOperator.SelectedIndexChanged
        Try
            If CInt(cboADCatOperator.SelectedValue) > 0 Then
                txtADCatAmtFrom.Enabled = True
            Else
                txtADCatAmtFrom.Text = ""
                txtADCatAmtFrom.Enabled = False
            End If
            If CInt(cboADCatOperator.SelectedValue) = enComparison_Operator.BETWEEN Then
                lblADCatAnd.Visible = True
                txtADCatAmtTo.Visible = True
            Else
                lblADCatAnd.Visible = False
                txtADCatAmtTo.Text = ""
                txtADCatAmtTo.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboADCatOperator_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowEmpSal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowEmpSal.CheckedChanged
        Try
            If chkShowEmpSal.Checked = True Then
                objAssetDeclaration.setDefaultOrderBy(1)
            Else
                objAssetDeclaration.setDefaultOrderBy(0)
            End If
            txtOrderBy.Text = objAssetDeclaration.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowEmpSal_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region
   


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkShowEmpSal.Text = Language._Object.getCaption(Me.chkShowEmpSal.Name, Me.chkShowEmpSal.Text)
			Me.lblLiabAnd.Text = Language._Object.getCaption(Me.lblLiabAnd.Name, Me.lblLiabAnd.Text)
			Me.lblDeclareAnd.Text = Language._Object.getCaption(Me.lblDeclareAnd.Name, Me.lblDeclareAnd.Text)
			Me.lblLiability.Text = Language._Object.getCaption(Me.lblLiability.Name, Me.lblLiability.Text)
			Me.lblDecAmt.Text = Language._Object.getCaption(Me.lblDecAmt.Name, Me.lblDecAmt.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
			Me.lblAppointment.Text = Language._Object.getCaption(Me.lblAppointment.Name, Me.lblAppointment.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblADCatAnd.Text = Language._Object.getCaption(Me.lblADCatAnd.Name, Me.lblADCatAnd.Text)
			Me.gbADCategory.Text = Language._Object.getCaption(Me.gbADCategory.Name, Me.gbADCategory.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employees Declared Value Analysis")
			Language.setMessage(mstrModuleName, 2, "Employee Category Wise Asset Declaration Analysis")
			Language.setMessage(mstrModuleName, 4, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Appointment To Date cannot be less then Appointment From Date.")
			Language.setMessage(mstrModuleName, 7, "Please enter Estimated Delcared Amount.")
			Language.setMessage(mstrModuleName, 8, "Please enter Estimated Delcared To Amount.")
			Language.setMessage(mstrModuleName, 9, "Please enter Estimated Liability Amount.")
			Language.setMessage(mstrModuleName, 10, "Please enter Estimated Liability To Amount.")
			Language.setMessage(mstrModuleName, 11, "Please enter Estimated Asset Category Amount.")
			Language.setMessage(mstrModuleName, 12, "Please enter Estimated Asset Category To Amount.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
