Imports Aruti.Data
Imports eZeeCommonLib

Public Class clsAssetDeclarationCategoryWiseT2
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsAssetDeclarationCategoryWiseT2"
    'Hemant (24 Dec 2018) -- Start
    'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
    Private Shared ReadOnly mstrModuleNameForm As String = "frmEmployeeDeclarationCategorywiseReportT2"
    'Hemant (24 Dec 2018) -- End
    Private mstrReportId As String = enArutiReport.Asset_Declaration_Category_Wise
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mintAppointmentTypeId As Integer = 0
    Private mstrAppointmentTypeName As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mintAssetCategoryId As Integer
    Private mstrAssetCategoryName As String


    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvance_Filter As String = String.Empty


    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mstrDeclareValueQuery As String = ""

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname



#End Region

#Region " Properties "
    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeName() As String
        Set(ByVal value As String)
            mstrAppointmentTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property


    Public WriteOnly Property _AssetCategoryId() As Integer
        Set(ByVal value As Integer)
            mintAssetCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _AssetCategoryName() As String
        Set(ByVal value As String)
            mstrAssetCategoryName = value
        End Set
    End Property




    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mdtAsOnDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mblnIncluderInactiveEmp = False
            mintAppointmentTypeId = 0
            mstrAppointmentTypeName = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mstrUserAccessFilter = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Dim strDeclareValueQuery As String = ""
        Dim strLiabValueQuery As String = ""
        Dim strADCatQuery As String = ""
        Try

            If mstrAdvance_Filter.Trim.Length > 0 Then
                Me._FilterQuery &= " AND " & mstrAdvance_Filter
            End If

            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate).ToString)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "As On Date : ") & " " & mdtAsOnDate.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Employee : ") & " " & mstrEmployeeName & " "
            End If






            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate2).ToString)
                        Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN @Date1 AND @Date2 "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Appointed Date From : ") & " " & mdtDate1.ToShortDateString & " " & _
                                           Language.getMessage(mstrModuleName, 16, "To : ") & " " & mdtDate2.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < @Date1 "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Appointed Before Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > @Date1 "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Appointed After Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
            End Select


            If mintAssetCategoryId > 0 Then
                If mintAssetCategoryId = enAD_CategoriesT2.BUSINESS_DEALINGS Then
                    strADCatQuery &= "JOIN hrasset_businessdealT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_businessdealT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_businessdealT2_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " BUSINESS DEALINGS"

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.STAFF_OWNERSHIP Then
                    strADCatQuery &= "JOIN hrasset_securitiesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_securitiesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_securitiesT2_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " STAFF OWNERSHIP"

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.BANK_ACCOUNTS Then
                    strADCatQuery &= "JOIN hrasset_bankT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_bankT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_bankT2_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " BANK ACCOUNTS"


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.REAL_PROPERTIES Then
                    strADCatQuery &= "JOIN hrasset_propertiesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_propertiesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_propertiesT2_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " REAL PROPERTIES"

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.LIABILITIES Then
                    strADCatQuery &= "JOIN hrasset_liabilitiesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_liabilitiesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_liabilitiesT2_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " LIABILITIES"


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.RELATIVES_IN_EMPLOYMENT Then
                    strADCatQuery &= "JOIN hrasset_relativesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_relativesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_relativesT2_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " RELATIVES IN EMPLOYMENT"


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_BUSINESS Then
                    strADCatQuery &= "JOIN hrasset_businessdealT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_businessdealT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_businessdealT2depn_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " DEPENDANT BUSINESS"


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_SHARES Then
                    strADCatQuery &= "JOIN hrasset_securitiesT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_securitiesT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_securitiesT2depn_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " DEPENDANT SHARES"


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_BANK Then
                    strADCatQuery &= "JOIN hrasset_bankT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_bankT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_bankT2depn_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " DEPENDANT BANK"

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_REAL_PROPERTIES Then
                    strADCatQuery &= "JOIN hrasset_propertiesT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_propertiesT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_propertiesT2depn_tran.isvoid = 0 "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Declaration Category : ") & " DEPENDANT REAL PROPERTIES"


                End If
            End If

            'mstrDeclareValueQuery = strADCatQuery


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Order By :") & " " & Me.OrderByDisplay & " "
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY " & mstrAnalysis_OrderBy_GroupName & "," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'If intReportType = 0 Then
            '    OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            '    OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            'Else

            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            'If intReportType = 0 Then
            '    Call OrderByExecute(iColumn_DetailReport)
            'Else

            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            'iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 43, "Employee Code")))
            'If mblnFirstNamethenSurname = False Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            'End If

            'iColumn_DetailReport.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 45, "Appointed Date")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 46, "Department")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(JBM.job_name,'')", Language.getMessage(mstrModuleName, 47, "Job Title")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(ClsG.name,'')", Language.getMessage(mstrModuleName, 48, "Region")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Cls.name,'')", Language.getMessage(mstrModuleName, 49, "WorkStation")))

            'iColumn_DetailReport.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
            '                                    "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
            '                                    "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
            '                                    "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 50, "Declared Amount")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hrassetdeclaration_master.debttotal, 0)", Language.getMessage(mstrModuleName, 51, "Liability Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)



            StrQ = "SELECT hrassetdeclarationT2_master.assetdeclarationt2unkid " & _
                          ",hrassetdeclarationT2_master.employeeunkid " & _
                          ", hremployee_master.employeecode "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS employeename "
            End If

            StrQ &= ", ISNULL(CONVERT(CHAR(8), appointeddate, 112), '') AS App_Date " & _
                          ", ISNULL(DM.name,'') AS Department " & _
                          ", ISNULL(JM.job_name,'') AS Job_Title " & _
                          ", ISNULL(CGM.name,'') AS Region " & _
                          ", ISNULL(CM.name,'') AS WorkStation " & _
                          ", ISNULL(GM.name,'') as GradeLevel " & _
                          ", ISNULL(hremployee_master.email,'') as Email " & _
                          ", ISNULL(hremployee_master.present_mobile,'') as MobileNo "


            StrQ &= "     , hrassetdeclarationT2_master.finyear_start " & _
                          ", hrassetdeclarationT2_master.finyear_end " & _
                          ", hrassetdeclarationT2_master.isfinalsaved " & _
                          ", hrassetdeclarationT2_master.userunkid " & _
                          ", hrassetdeclarationT2_master.isvoid " & _
                          ", hrassetdeclarationT2_master.voiduserunkid " & _
                          ", hrassetdeclarationT2_master.voiddatetime " & _
                          ", hrassetdeclarationT2_master.voidreason " & _
                          ", hrassetdeclarationT2_master.loginemployeeunkid " & _
                          ", hrassetdeclarationT2_master.voidloginemployeeunkid " & _
                          ", hrassetdeclarationT2_master.transactiondate " & _
                          ", hrassetdeclarationT2_master.savedate " & _
                          ", hrassetdeclarationT2_master.finalsavedate " & _
                          ", hrassetdeclarationT2_master.unlockfinalsavedate "



            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM  hrassetdeclarationT2_master "


            'Gajanan (1 Dec 2018) -- Start

            If mintAssetCategoryId > 0 Then
                If mintAssetCategoryId = enAD_CategoriesT2.BUSINESS_DEALINGS Then
                    StrQ &= "JOIN hrasset_businessdealT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_businessdealT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_businessdealT2_tran.isvoid = 0 "

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.STAFF_OWNERSHIP Then
                    StrQ &= "JOIN hrasset_securitiesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_securitiesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_securitiesT2_tran.isvoid = 0 "

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.BANK_ACCOUNTS Then
                    StrQ &= "JOIN hrasset_bankT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_bankT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_bankT2_tran.isvoid = 0 "


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.REAL_PROPERTIES Then
                    StrQ &= "JOIN hrasset_propertiesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_propertiesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_propertiesT2_tran.isvoid = 0 "

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.LIABILITIES Then
                    StrQ &= "JOIN hrasset_liabilitiesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_liabilitiesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_liabilitiesT2_tran.isvoid = 0 "


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.RELATIVES_IN_EMPLOYMENT Then
                    StrQ &= "JOIN hrasset_relativesT2_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_relativesT2_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_relativesT2_tran.isvoid = 0 "


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_BUSINESS Then
                    StrQ &= "JOIN hrasset_businessdealT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_businessdealT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_businessdealT2depn_tran.isvoid = 0 "


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_SHARES Then
                    StrQ &= "JOIN hrasset_securitiesT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_securitiesT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_securitiesT2depn_tran.isvoid = 0 "


                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_BANK Then
                    StrQ &= "JOIN hrasset_bankT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_bankT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_bankT2depn_tran.isvoid = 0 "

                ElseIf mintAssetCategoryId = enAD_CategoriesT2.DEPENDANT_REAL_PROPERTIES Then
                    StrQ &= "JOIN hrasset_propertiesT2depn_tran " & _
                            "ON hrassetdeclarationT2_master.assetdeclarationt2unkid = hrasset_propertiesT2depn_tran.assetdeclarationt2unkid " & _
                              "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                              "AND hrasset_propertiesT2depn_tran.isvoid = 0 "

                End If
            End If
            'Gajanan(1 Dec 2018) -- End


            StrQ &= " JOIN hremployee_master ON hrassetdeclarationT2_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "      ( " & _
                    "             SELECT " & _
                    "              departmentunkid " & _
                    "              ,classgroupunkid " & _
                    "              ,classunkid " & _
                    "              ,employeeunkid " & _
                    "              ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "          FROM hremployee_transfer_tran " & _
                    "          WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "      ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "   JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "   LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "   LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "LEFT JOIN ( " & _
                         "SELECT gradegroupunkid " & _
                                 ",gradeunkid " & _
                                 ",gradelevelunkid " & _
                                 ",scale " & _
                                 ",employeeunkid AS GEmpId " & _
                         "FROM ( " & _
                                   "SELECT gradegroupunkid " & _
                                   ",gradeunkid " & _
                                   ",gradelevelunkid " & _
                                   ",employeeunkid " & _
                                   ",newscale AS scale " & _
                                   ",ROW_NUMBER() OVER ( " & _
                                        "PARTITION BY employeeunkid ORDER BY incrementdate DESC " & _
                                             ",salaryincrementtranunkid DESC " & _
                                        ") AS Rno " & _
                              "FROM prsalaryincrement_tran " & _
                              "WHERE isvoid = 0 " & _
                                   "AND isapproved = 1 " & _
                                   "AND CONVERT(CHAR(8), incrementdate, 112) <= '20171231' " & _
                              ") AS GRD " & _
                         "WHERE GRD.Rno = 1 " & _
                         ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                    "INNER JOIN hrgradelevel_master AS GM ON EGRD.gradelevelunkid = gm.gradelevelunkid " & _
                    "   LEFT JOIN " & _
                    "      ( " & _
                    "          SELECT " & _
                    "              jobunkid " & _
                    "              ,jobgroupunkid " & _
                    "              ,employeeunkid " & _
                    "              ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "          FROM hremployee_categorization_tran " & _
                    "          WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "      ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE ISNULL(hrassetdeclarationT2_master.isvoid, 0) = 0 " & _
                    "AND hrassetdeclarationT2_master.isfinalsaved = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim iCnt As Integer = 1
            Dim decSalGrpTotal As Decimal = 0
            Dim decDeclareGrpTotal As Decimal = 0
            Dim decDebtGrpTotal As Decimal = 0
            Dim decSalTotal As Decimal = 0
            Dim decDeclareTotal As Decimal = 0
            Dim decDebtTotal As Decimal = 0
            Dim intPrevId As Integer = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = iCnt.ToString
                rpt_Rows.Item("Column3") = dtRow.Item("employeecode") 'eZeeDate.convertDate(dtRow.Item("App_Date").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("employeename") 'dtRow.Item("Job_Title")
                rpt_Rows.Item("Column5") = dtRow.Item("GradeLevel")
                rpt_Rows.Item("Column6") = dtRow.Item("Email")
                rpt_Rows.Item("Column7") = dtRow.Item("MobileNo")
                'rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency)

                'rpt_Rows.Item("Column12") = 
                'rpt_Rows.Item("Column13") = Format(CDec(dtRow.Item("banktotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column14") = Format(CDec(dtRow.Item("sharedividendtotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column15") = Format(CDec(dtRow.Item("housetotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column16") = Format(CDec(dtRow.Item("parkfarmtotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column17") = Format(CDec(dtRow.Item("vehicletotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column18") = Format(CDec(dtRow.Item("machinerytotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column19") = Format(CDec(dtRow.Item("otherbusinesstotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column20") = Format(CDec(dtRow.Item("otherresourcetotal")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column21") = Format(CDec(dtRow.Item("debttotal")), GUI.fmtCurrency)

                'If intPrevId <> CInt(dtRow.Item("Id")) Then
                '    decSalGrpTotal = CDec(Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency))
                '    decDeclareGrpTotal = CDec(Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency))
                '    decDebtGrpTotal = CDec(Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency))

                'Else
                '    decSalGrpTotal += CDec(Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency))
                '    decDeclareGrpTotal += CDec(Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency))
                '    decDebtGrpTotal += CDec(Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency))
                'End If

                'decSalTotal += CDec(Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency))
                'decDeclareTotal += CDec(Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency))
                'decDebtTotal += CDec(Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency))

                'rpt_Rows.Item("Column82") = Format(decSalGrpTotal, GUI.fmtCurrency)
                'rpt_Rows.Item("Column83") = Format(decDeclareGrpTotal, GUI.fmtCurrency)
                'rpt_Rows.Item("Column84") = Format(decDebtGrpTotal, GUI.fmtCurrency)

                iCnt += 1
                'intPrevId = CInt(dtRow.Item("Id"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next
            objRpt = New ArutiReport.Designer.rptADCategoryWiseT2
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtSN", Language.getMessage(mstrModuleName, 5, "Sno."))
            ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 6, "Employee Code"))
            ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 7, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtGradelevel", Language.getMessage(mstrModuleName, 8, "Grade Level"))
            ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 9, "Email"))
            ReportFunction.TextChange(objRpt, "txtMobile", Language.getMessage(mstrModuleName, 10, "Mobile"))
            ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            'Select Case mintReportTypeId
            '    Case 0 'Employee Declared Value Analysis
            '        ReportFunction.TextChange(objRpt, "txtDecAmt", Language.getMessage(mstrModuleName, 18, "Declared Amount"))
            '        ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))

            '    Case 1 'Employee Wise Asset Declaration Analysis
            '        ReportFunction.TextChange(objRpt, "txtDecAmt", Language.getMessage(mstrModuleName, 20, "ASSET CATEGORY"))
            '        ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 21, "TOTAL"))

            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.CURRENT_ASSETS Then
            '            ReportFunction.TextChange(objRpt, "txtCurrAsset", Language.getMessage(mstrModuleName, 22, "CURRENT ASSETS"))
            '        Else
            '            ReportFunction.TextChange(objRpt, "txtCurrAsset", "")
            '            ReportFunction.EnableSuppress(objRpt, "Column131", True)
            '            ReportFunction.EnableUnderlaySection(objRpt, "DetailCurrAsset", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.SHARE_AND_DIVIDENDS Then
            '            ReportFunction.TextChange(objRpt, "txtShareDividend", Language.getMessage(mstrModuleName, 23, "SHARE AND DIVIDENDS"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailShareDividend", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailShareDividend", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.HOUSES_AND_BUILDINGS Then
            '            ReportFunction.TextChange(objRpt, "txtHouse", Language.getMessage(mstrModuleName, 24, "HOUSES AND BUILDINGS"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailHouse", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailHouse", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.PARK_FARMS_AND_MINES Then
            '            ReportFunction.TextChange(objRpt, "txtParkFarm", Language.getMessage(mstrModuleName, 25, "PARK, FARMS AND MINES"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailParkFarm", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailParkFarm", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.TRANSPORT_EQUIPMENTS Then
            '            ReportFunction.TextChange(objRpt, "txtVehicle", Language.getMessage(mstrModuleName, 26, "TRANSPORT EQUIPMENTS"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailVehicle", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailVehicle", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.MACHINERIES_AND_INDUSTRIES Then
            '            ReportFunction.TextChange(objRpt, "txtMachine", Language.getMessage(mstrModuleName, 27, "MACHINERIES AND INDUSTRIES"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailMachine", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailMachine", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.OTHER_BUSINESSES Then
            '            ReportFunction.TextChange(objRpt, "txtBusiness", Language.getMessage(mstrModuleName, 28, "OTHER BUSINESSES"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailBusiness", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailBusiness", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.OTHER_COMMERCIAL_INTERESTS Then
            '            ReportFunction.TextChange(objRpt, "txtResource", Language.getMessage(mstrModuleName, 29, "OTHER COMMERCIAL INTERESTS"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailResource", False)
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailResource", True)
            '        End If
            '        If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS Then
            '            ReportFunction.TextChange(objRpt, "txtLiabValue", Language.getMessage(mstrModuleName, 30, "DEBTS"))
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailLiabValue", False)
            '            If mintAssetCategoryId = enAD_Categories.DEBTS Then
            '                Call ReportFunction.EnableSuppressSection(objRpt, "DetailDeclaredValue", True)
            '            End If
            '        Else
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailLiabValue", True)
            '            Call ReportFunction.EnableSuppressSection(objRpt, "DetailDeclaredValue", False)
            '        End If
            'End Select


            'If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
            '    Call ReportFunction.TextChange(objRpt, "txtTotalAmt1", Format(decSalTotal, GUI.fmtCurrency))
            '    Call ReportFunction.TextChange(objRpt, "txtTotalAmt2", Format(decDeclareTotal, GUI.fmtCurrency))
            '    Call ReportFunction.TextChange(objRpt, "txtTotalAmt3", Format(decDebtTotal, GUI.fmtCurrency))
            'End If

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 11, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 12, "Printed Date :"))

            ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            'ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Language.setLanguage(mstrModuleNameForm)
            ReportFunction.TextChange(objRpt, "txtReportName", Language._Object.getCaption(mstrModuleNameForm, Me._ReportName))
            'Hemant (24 Dec 2018) -- End

            ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Sno.")
            Language.setMessage(mstrModuleName, 6, "Employee Code")
            Language.setMessage(mstrModuleName, 7, "Employee Name")
            Language.setMessage(mstrModuleName, 8, "Grade Level")
            Language.setMessage(mstrModuleName, 9, "Email")
            Language.setMessage(mstrModuleName, 10, "Mobile")
            Language.setMessage(mstrModuleName, 11, "Printed By :")
            Language.setMessage(mstrModuleName, 12, "Printed Date :")
            Language.setMessage(mstrModuleName, 13, "As On Date :")
            Language.setMessage(mstrModuleName, 14, "Employee :")
            Language.setMessage(mstrModuleName, 15, "Appointed Date From :")
            Language.setMessage(mstrModuleName, 16, "To :")
            Language.setMessage(mstrModuleName, 17, "Appointed Before Date :")
            Language.setMessage(mstrModuleName, 18, "Appointed After Date :")
            Language.setMessage(mstrModuleName, 19, "Declaration Category :")
            Language.setMessage(mstrModuleName, 20, "Order By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
