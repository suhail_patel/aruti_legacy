'************************************************************************************************************************************
'Class Name : clsEmployeeBeneficiariesReport.vb
'Purpose    :
'Date       :10/9/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeBeneficiariesReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeBeneficiariesReport"
    Private mstrReportId As String = enArutiReport.Employee_Beneficairy_Report  '20
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrEmployeeCode As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintBenefitTypeId As Integer = -1
    Private mstrBenefitTypeName As String = String.Empty
    Private mintModeId As Integer = -1
    Private mstrModeName As String = String.Empty
    Private mstrMobileNo As String = String.Empty
    Private mstrFirstName As String = String.Empty
    Private mstrLastName As String = String.Empty
    Private mstrOrderBy As String = String.Empty


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _BenefitTypeId() As Integer
        Set(ByVal value As Integer)
            mintBenefitTypeId = value
        End Set
    End Property

    Public WriteOnly Property _BenefitTypeName() As String
        Set(ByVal value As String)
            mstrBenefitTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ModeId() As Integer
        Set(ByVal value As Integer)
            mintModeId = value
        End Set
    End Property

    Public WriteOnly Property _ModeName() As String
        Set(ByVal value As String)
            mstrModeName = value
        End Set
    End Property

    Public WriteOnly Property _MobileNo() As String
        Set(ByVal value As String)
            mstrMobileNo = value
        End Set
    End Property

    Public WriteOnly Property _FirstName() As String
        Set(ByVal value As String)
            mstrFirstName = value
        End Set
    End Property

    Public WriteOnly Property _LastName() As String
        Set(ByVal value As String)
            mstrLastName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Anjan (17 May 2012)-End 


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeCode = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintBenefitTypeId = 0
            mstrBenefitTypeName = ""
            mintModeId = 0
            mstrModeName = ""
            mstrMobileNo = ""
            mstrFirstName = ""
            mstrLastName = ""
            mstrOrderBy = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End  
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mstrEmployeeCode.Trim <> "" Then
                objDataOperation.AddParameter("@EmployeeCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmployeeCode & "%")
                Me._FilterQuery &= " AND ISNULL(hremployee_master.employeecode,'') LIKE @EmployeeCode "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Code :") & " " & mstrEmployeeCode & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Employee Name :") & " " & mstrEmployeeName & " "
            End If

            If mintBenefitTypeId > 0 Then
                objDataOperation.AddParameter("@BenefitTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitTypeId)
                'Anjan (28 Aug 2017) -Start
                'Me._FilterQuery &= " AND cfcommon_master.masterunkid = @BenefitTypeId "
                Me._FilterQuery &= " AND hrbenefitplan_master.benefitplanunkid = @BenefitTypeId "
                'Anjan (28 Aug 2017) -End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Benefit Type :") & " " & mstrBenefitTypeName & " "
            End If

            If mintModeId > 0 Then
                objDataOperation.AddParameter("@ModeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeId)
                Me._FilterQuery &= " AND hrdependant_benefit_tran.value_id = @ModeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Benefit In :") & " " & mstrModeName & " "
            End If

            If mstrMobileNo.Trim <> "" Then
                objDataOperation.AddParameter("@MobileNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrMobileNo & "%")
                Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') LIKE @MobileNo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Mobile :") & " " & mstrMobileNo & " "
            End If

            If mstrFirstName.Trim <> "" Then
                objDataOperation.AddParameter("@FirstName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrFirstName & "%")
                Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.first_name,'') LIKE @FirstName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "FirstName :") & " " & mstrFirstName & " "
            End If

            If mstrLastName.Trim <> "" Then
                objDataOperation.AddParameter("@LastName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrLastName & "%")
                Me._FilterQuery &= " AND ISNULL(hrdependants_beneficiaries_tran.last_name,'') LIKE @LastName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "LastName :") & " " & mstrLastName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Order By :") & " " & Me.OrderByDisplay
                mstrOrderBy &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If
        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 1, "Code")))
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END
            iColumn_DetailReport.Add(New IColumn("ISNULL(cfcommon_master.name,'')", Language.getMessage(mstrModuleName, 3, "Benefit Type")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hrdependants_beneficiaries_tran.mobile_no,'')", Language.getMessage(mstrModuleName, 4, "Mobile No.")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If mintEmployeeId > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeId > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @EmployeeId "
            End If

            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            ' Pinkal (05-Jul-2011) -- Start

            'StrQ = "SELECT " & _
            '        "	 ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
            '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '        "	,ISNULL(cfcommon_master.name,'') AS BenefitType " & _
            '        "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS Beneficiary " & _
            '        "	,CASE WHEN hrdependant_benefit_tran.value_id = 1 THEN SUM(hrdependant_benefit_tran.benefit_amount) " & _
            '        "		  WHEN hrdependant_benefit_tran.value_id = 2 THEN SUM(hrdependant_benefit_tran.benefit_percent) " & _
            '        "	 END AS Mode " & _
            '        "	,ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') AS Mobile " & _
            '        "	,ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
            '        "	,hrdependant_benefit_tran.value_id AS value_id " & _
            '        "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FName " & _
            '        "	,ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LName " & _
            '        "FROM hrdependants_beneficiaries_tran " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
            '        "	JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	JOIN hrdependant_benefit_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = hrdependant_benefit_tran.dpndtbeneficetranunkid " & _
            '        "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 "


            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '       "	 ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
            '       "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '       "	,ISNULL(cfcommon_master.name,'') AS BenefitType " & _
            '       "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS Beneficiary " & _
            '       "	,SUM(hrdependant_benefit_tran.benefit_amount) benefit_amount " & _
            '       "	,SUM(hrdependant_benefit_tran.benefit_percent) benefit_percent " & _
            '       "	,ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') AS Mobile " & _
            '       "	,ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
            '       "	,hrdependant_benefit_tran.value_id AS value_id " & _
            '       "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FName " & _
            '       "	,ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LName " & _
            '       "FROM hrdependants_beneficiaries_tran " & _
            '       "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
            '       "	JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '       "	JOIN hrdependant_benefit_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = hrdependant_benefit_tran.dpndtbeneficetranunkid " & _
            '       "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 "

            StrQ &= "SELECT " & _
                    "	 ISNULL(hremployee_master.employeecode,'') AS EmpCode "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                StrQ &= "	,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= "	,ISNULL(cfcommon_master.name,'') AS Relation " & _
                    "   ,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BenefitType " & _
                    "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS Beneficiary " & _
                    "	,SUM(hrdependant_benefit_tran.benefit_amount) benefit_amount " & _
                    "	,SUM(hrdependant_benefit_tran.benefit_percent) benefit_percent " & _
                    "	,ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') AS Mobile " & _
                    "	,ISNULL(hrdependants_beneficiaries_tran.address,'') AS Address " & _
                    "	,hrdependant_benefit_tran.value_id AS value_id " & _
                    "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FName " & _
                    "	,ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LName " & _
                    "FROM hrdependants_beneficiaries_tran " & _
                    "   JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                    "	JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "	JOIN hrdependant_benefit_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = hrdependant_benefit_tran.dpndtbeneficetranunkid " & _
                    "   JOIN hrbenefitplan_master ON hrbenefitplan_master.benefitplanunkid = hrdependant_benefit_tran.benefitplanunkid "
            'Sohail (18 May 2019) - [JOIN #TableDepn]

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 " & _
            "AND ISNULL(#TableDepn.isactive, 1) = 1 "
            'Sohail (18 May 2019) - [isactive]
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " AND hremployee_master.isactive = 1"
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If
            ''Pinkal (24-Jun-2011) -- End

            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "GROUP BY ISNULL(hremployee_master.employeecode,'') "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                StrQ &= "	,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') "
            Else
                StrQ &= "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "
            End If

            StrQ &= "	,ISNULL(cfcommon_master.name,'') " & _
                "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') " & _
                "	,hrdependant_benefit_tran.value_id " & _
                "	,ISNULL(hrdependants_beneficiaries_tran.mobile_no,'') " & _
                "	,ISNULL(hrdependants_beneficiaries_tran.address,'') " & _
                "	,ISNULL(hrdependants_beneficiaries_tran.first_name,'')  " & _
                "	,ISNULL(hrdependants_beneficiaries_tran.last_name,'')  "

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            StrQ &= " ,ISNULL(hrbenefitplan_master.benefitplanname,'') "
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            StrQ &= mstrOrderBy

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If mdtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            End If

            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("EmpCode")
                rpt_Rows.Item("Column2") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column3") = dtRow.Item("BenefitType")
                rpt_Rows.Item("Column4") = dtRow.Item("Beneficiary")

                Select Case CInt(dtRow.Item("value_id"))
                    Case 1     'AMNOUNT
                        rpt_Rows.Item("Column5") = Format(dtRow.Item("benefit_amount"), GUI.fmtCurrency)
                    Case 2     'PERCENTAGE
                        rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("benefit_percent")), "##0.00") & " " & "%"
                End Select
                rpt_Rows.Item("Column6") = dtRow.Item("Mobile")
                rpt_Rows.Item("Column7") = dtRow.Item("Address")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            ' Pinkal (05-Jul-2011) -- End


            objRpt = New ArutiReport.Designer.rptEmployeeBeneficiaries


            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 21, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 22, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 23, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 


            objRpt.SetDataSource(rpt_Data)


            ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 1, "Code"))
            ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 2, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtBeneficeType", Language.getMessage(mstrModuleName, 3, "Benefit Type"))
            ReportFunction.TextChange(objRpt, "txtBeneficiaryName", Language.getMessage(mstrModuleName, 5, "Beneficiary Name"))
            ReportFunction.TextChange(objRpt, "txtMode", Language.getMessage(mstrModuleName, 6, "Benefit in Amount Or (%)"))
            ReportFunction.TextChange(objRpt, "txtMobile", Language.getMessage(mstrModuleName, 4, "Mobile No."))
            ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 7, "Address"))
            ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 8, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 11, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Benefit Type")
            Language.setMessage(mstrModuleName, 4, "Mobile No.")
            Language.setMessage(mstrModuleName, 5, "Beneficiary Name")
            Language.setMessage(mstrModuleName, 6, "Benefit in Amount Or (%)")
            Language.setMessage(mstrModuleName, 7, "Address")
            Language.setMessage(mstrModuleName, 8, "Grand Total :")
            Language.setMessage(mstrModuleName, 9, "Printed By :")
            Language.setMessage(mstrModuleName, 10, "Printed Date :")
            Language.setMessage(mstrModuleName, 11, "Page :")
            Language.setMessage(mstrModuleName, 12, "Prepared By :")
            Language.setMessage(mstrModuleName, 13, "Code :")
            Language.setMessage(mstrModuleName, 14, "Employee Name :")
            Language.setMessage(mstrModuleName, 15, "Benefit Type :")
            Language.setMessage(mstrModuleName, 16, "Benefit In :")
            Language.setMessage(mstrModuleName, 17, "Mobile :")
            Language.setMessage(mstrModuleName, 18, "FirstName :")
            Language.setMessage(mstrModuleName, 19, "LastName :")
            Language.setMessage(mstrModuleName, 20, "Order By :")
            Language.setMessage(mstrModuleName, 21, "Checked By :")
            Language.setMessage(mstrModuleName, 22, "Approved By :")
            Language.setMessage(mstrModuleName, 23, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
