﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingBasedOnReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTrainingBasedOnReport))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblReporttype = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objstline1 = New eZee.Common.eZeeStraightLine
        Me.lvSourceFunding = New eZee.Common.eZeeListView(Me.components)
        Me.colhFundingSource = New System.Windows.Forms.ColumnHeader
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.objbtnSearchProvider = New eZee.Common.eZeeGradientButton
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblInstitute = New System.Windows.Forms.Label
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.objbtnSearchTraining = New eZee.Common.eZeeGradientButton
        Me.lblTraining = New System.Windows.Forms.Label
        Me.cboInstitute = New System.Windows.Forms.ComboBox
        Me.cboTraining = New System.Windows.Forms.ComboBox
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.lblToDate = New System.Windows.Forms.Label
        Me.objchkSAll = New System.Windows.Forms.CheckBox
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(850, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Training Based On Report"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 465)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(850, 55)
        Me.EZeeFooter1.TabIndex = 6
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(557, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(653, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(749, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objchkSAll)
        Me.gbFilterCriteria.Controls.Add(Me.lblReporttype)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.objstline1)
        Me.gbFilterCriteria.Controls.Add(Me.lvSourceFunding)
        Me.gbFilterCriteria.Controls.Add(Me.objchkAll)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchProvider)
        Me.gbFilterCriteria.Controls.Add(Me.txtSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.lvAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocations)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTraining)
        Me.gbFilterCriteria.Controls.Add(Me.lblTraining)
        Me.gbFilterCriteria.Controls.Add(Me.cboInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.cboTraining)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(780, 393)
        Me.gbFilterCriteria.TabIndex = 7
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReporttype
        '
        Me.lblReporttype.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReporttype.Location = New System.Drawing.Point(8, 36)
        Me.lblReporttype.Name = "lblReporttype"
        Me.lblReporttype.Size = New System.Drawing.Size(98, 16)
        Me.lblReporttype.TabIndex = 222
        Me.lblReporttype.Text = "Report Type"
        Me.lblReporttype.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(112, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(265, 21)
        Me.cboReportType.TabIndex = 221
        '
        'objstline1
        '
        Me.objstline1.BackColor = System.Drawing.Color.Transparent
        Me.objstline1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objstline1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstline1.Location = New System.Drawing.Point(410, 34)
        Me.objstline1.Name = "objstline1"
        Me.objstline1.Size = New System.Drawing.Size(9, 350)
        Me.objstline1.TabIndex = 220
        Me.objstline1.Text = "EZeeStraightLine2"
        '
        'lvSourceFunding
        '
        Me.lvSourceFunding.BackColorOnChecked = False
        Me.lvSourceFunding.CheckBoxes = True
        Me.lvSourceFunding.ColumnHeaders = Nothing
        Me.lvSourceFunding.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFundingSource})
        Me.lvSourceFunding.CompulsoryColumns = ""
        Me.lvSourceFunding.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvSourceFunding.FullRowSelect = True
        Me.lvSourceFunding.GridLines = True
        Me.lvSourceFunding.GroupingColumn = Nothing
        Me.lvSourceFunding.HideSelection = False
        Me.lvSourceFunding.Location = New System.Drawing.Point(112, 142)
        Me.lvSourceFunding.MinColumnWidth = 50
        Me.lvSourceFunding.MultiSelect = False
        Me.lvSourceFunding.Name = "lvSourceFunding"
        Me.lvSourceFunding.OptionalColumns = ""
        Me.lvSourceFunding.ShowMoreItem = False
        Me.lvSourceFunding.ShowSaveItem = False
        Me.lvSourceFunding.ShowSelectAll = True
        Me.lvSourceFunding.ShowSizeAllColumnsToFit = True
        Me.lvSourceFunding.Size = New System.Drawing.Size(265, 242)
        Me.lvSourceFunding.Sortable = True
        Me.lvSourceFunding.TabIndex = 216
        Me.lvSourceFunding.UseCompatibleStateImageBehavior = False
        Me.lvSourceFunding.View = System.Windows.Forms.View.Details
        '
        'colhFundingSource
        '
        Me.colhFundingSource.Tag = "colhFundingSource"
        Me.colhFundingSource.Text = "Sponser(s)"
        Me.colhFundingSource.Width = 240
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(722, 89)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 207
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'objbtnSearchProvider
        '
        Me.objbtnSearchProvider.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProvider.BorderSelected = False
        Me.objbtnSearchProvider.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProvider.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProvider.Location = New System.Drawing.Point(383, 115)
        Me.objbtnSearchProvider.Name = "objbtnSearchProvider"
        Me.objbtnSearchProvider.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProvider.TabIndex = 215
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(491, 61)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(277, 21)
        Me.txtSearch.TabIndex = 208
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 230
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(112, 142)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(265, 21)
        Me.cboStatus.TabIndex = 218
        Me.cboStatus.Visible = False
        '
        'lblInstitute
        '
        Me.lblInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitute.Location = New System.Drawing.Point(8, 117)
        Me.lblInstitute.Name = "lblInstitute"
        Me.lblInstitute.Size = New System.Drawing.Size(98, 16)
        Me.lblInstitute.TabIndex = 214
        Me.lblInstitute.Text = "Provider"
        Me.lblInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(491, 83)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(277, 301)
        Me.lvAllocation.TabIndex = 206
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 270
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(122, 144)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(98, 16)
        Me.lblStatus.TabIndex = 217
        Me.lblStatus.Text = "Enrollment Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblStatus.Visible = False
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(491, 34)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(277, 21)
        Me.cboAllocations.TabIndex = 204
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(425, 36)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(60, 16)
        Me.lblAllocation.TabIndex = 205
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTraining
        '
        Me.objbtnSearchTraining.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTraining.BorderSelected = False
        Me.objbtnSearchTraining.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTraining.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTraining.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTraining.Location = New System.Drawing.Point(383, 61)
        Me.objbtnSearchTraining.Name = "objbtnSearchTraining"
        Me.objbtnSearchTraining.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTraining.TabIndex = 212
        '
        'lblTraining
        '
        Me.lblTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraining.Location = New System.Drawing.Point(8, 63)
        Me.lblTraining.Name = "lblTraining"
        Me.lblTraining.Size = New System.Drawing.Size(98, 16)
        Me.lblTraining.TabIndex = 211
        Me.lblTraining.Text = "Training"
        Me.lblTraining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInstitute
        '
        Me.cboInstitute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitute.DropDownWidth = 450
        Me.cboInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitute.FormattingEnabled = True
        Me.cboInstitute.Location = New System.Drawing.Point(112, 115)
        Me.cboInstitute.Name = "cboInstitute"
        Me.cboInstitute.Size = New System.Drawing.Size(265, 21)
        Me.cboInstitute.TabIndex = 213
        '
        'cboTraining
        '
        Me.cboTraining.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTraining.DropDownWidth = 500
        Me.cboTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTraining.FormattingEnabled = True
        Me.cboTraining.Location = New System.Drawing.Point(112, 61)
        Me.cboTraining.Name = "cboTraining"
        Me.cboTraining.Size = New System.Drawing.Size(265, 21)
        Me.cboTraining.TabIndex = 210
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(112, 88)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpFromDate.TabIndex = 7
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(268, 88)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpToDate.TabIndex = 7
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 90)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(98, 16)
        Me.lblFromDate.TabIndex = 202
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(226, 90)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(36, 16)
        Me.lblToDate.TabIndex = 202
        Me.lblToDate.Text = "To"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkSAll
        '
        Me.objchkSAll.AutoSize = True
        Me.objchkSAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkSAll.Location = New System.Drawing.Point(334, 149)
        Me.objchkSAll.Name = "objchkSAll"
        Me.objchkSAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSAll.TabIndex = 224
        Me.objchkSAll.UseVisualStyleBackColor = True
        '
        'frmTrainingBasedOnReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 520)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmTrainingBasedOnReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Based On Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchTraining As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTraining As System.Windows.Forms.Label
    Friend WithEvents cboTraining As System.Windows.Forms.ComboBox
    Friend WithEvents lblInstitute As System.Windows.Forms.Label
    Friend WithEvents cboInstitute As System.Windows.Forms.ComboBox
    Friend WithEvents lvSourceFunding As eZee.Common.eZeeListView
    Friend WithEvents colhFundingSource As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchProvider As eZee.Common.eZeeGradientButton
    Friend WithEvents objstline1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblReporttype As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objchkSAll As System.Windows.Forms.CheckBox
End Class
