'************************************************************************************************************************************
'Class Name : clsTraningBasedOnReport.vb
'Purpose    :
'Date       : 15/02/2017
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsTraningBasedOnReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTraningBasedOnReport"
    Private mstrReportId As String = CStr(enArutiReport.Training_BasedOn_Report)  '185
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mdtFinalTable As DataTable = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintTrainingScheduleId As Integer = 0
    Private mstrTrainingScheduleName As String = String.Empty
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mintProviderId As Integer = 0
    Private mstrProviderName As String = String.Empty
    Private mstrSponsersIds As String = String.Empty
    Private mstrSponsersName As String = String.Empty
    Private mintAllocationTypeId As Integer = 0
    Private mstrAllocationTypeName As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    Private mstrAllocationNames As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingScheduleId() As Integer
        Set(ByVal value As Integer)
            mintTrainingScheduleId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingScheduleName() As String
        Set(ByVal value As String)
            mstrTrainingScheduleName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ProviderId() As Integer
        Set(ByVal value As Integer)
            mintProviderId = value
        End Set
    End Property

    Public WriteOnly Property _ProviderName() As String
        Set(ByVal value As String)
            mstrProviderName = value
        End Set
    End Property

    Public WriteOnly Property _SponsersIds() As String
        Set(ByVal value As String)
            mstrSponsersIds = value
        End Set
    End Property

    Public WriteOnly Property _SponsersName() As String
        Set(ByVal value As String)
            mstrSponsersName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _AllocationNames() As String
        Set(ByVal value As String)
            mstrAllocationNames = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintTrainingScheduleId = 0
            mstrTrainingScheduleName = String.Empty
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintProviderId = 0
            mstrProviderName = String.Empty
            mstrSponsersIds = String.Empty
            mstrSponsersName = String.Empty
            mintAllocationTypeId = 0
            mstrAllocationTypeName = String.Empty
            mstrAllocationIds = String.Empty
            mstrAllocationNames = String.Empty
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Me._FilterQuery = ""
        Try
            If mintTrainingScheduleId > 0 Then
                objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingScheduleId)
                Me._FilterQuery &= " AND ts.trainingschedulingunkid = @trainingschedulingunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 200, "Course Title :") & " " & mstrTrainingScheduleName & " "
            End If

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                objDataOperation.AddParameter("@sdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate).ToString)
                objDataOperation.AddParameter("@edate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate).ToString)
                Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8),ts.start_date,112),'') >= @sdate AND ISNULL(CONVERT(CHAR(8),ts.end_date,112),'') <= @edate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 201, "Schedule Date From :") & " " & mdtStartDate.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 202, "To :") & " " & mdtEndDate.ToShortDateString & " "
            End If

            If mintProviderId > 0 Then
                objDataOperation.AddParameter("@traininginstituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProviderId)
                Me._FilterQuery &= " AND ts.traininginstituteunkid = @traininginstituteunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 203, "Provider :") & " " & mstrProviderName & " "
            End If

            If mstrSponsersIds.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 204, "Sponsers :") & " " & mstrSponsersName & " "
            End If

            If mintAllocationTypeId > 0 Then
                objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
                Me._FilterQuery &= "AND ts.allocationtypeunkid = @allocationtypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 205, "Allocation Binded :") & " " & mstrAllocationTypeName & " "
            End If

            If mstrAllocationIds.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 206, "Allocation(s) :") & " " & mstrAllocationNames & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Private Sub CreateTable()
        Try
            mdtFinalTable = New DataTable("Data")
            Dim dCol As DataColumn

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col1"
                .Caption = Language.getMessage(mstrModuleName, 100, "S/N")
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col2"
                .Caption = Language.getMessage(mstrModuleName, 101, "Course Title")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col3"
                .Caption = Language.getMessage(mstrModuleName, 102, "Training Objective")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col4"
                .Caption = Language.getMessage(mstrModuleName, 103, "Binded Allocation")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col5"
                .Caption = Language.getMessage(mstrModuleName, 104, "Start Date")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col6"
                .Caption = Language.getMessage(mstrModuleName, 105, "End Date")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col7"
                .Caption = Language.getMessage(mstrModuleName, 106, "Venue")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col8"
                .Caption = Language.getMessage(mstrModuleName, 107, "Provider")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col9"
                .Caption = Language.getMessage(mstrModuleName, 108, "Total Staff")
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col10"
                .Caption = Language.getMessage(mstrModuleName, 109, "Enrolled Staff")
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col11"
                .Caption = Language.getMessage(mstrModuleName, 110, "Duration")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col12"
                .Caption = Language.getMessage(mstrModuleName, 111, "Hours")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Col13"
                .Caption = Language.getMessage(mstrModuleName, 112, "Sponser")
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtFinalTable.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function GetTraining_Sponsers()
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim dsFundings As New DataSet
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            StrQ = "DECLARE @tab AS table(tid int,mid nvarchar(max),rid int) " & _
                   "DECLARE @loop int, @max int, @tid int " & _
                   "DECLARE @split TABLE(tid int, mid int) " & _
                   "INSERT INTO @tab (tid,mid,rid) " & _
                   "SELECT " & _
                   "     hrtraining_scheduling.trainingschedulingunkid " & _
                   "    ,hrtraining_scheduling.fundingids " & _
                   "    ,ROW_NUMBER()OVER(ORDER BY hrtraining_scheduling.trainingschedulingunkid) AS rno " & _
                   "FROM hrtraining_scheduling " & _
                   "WHERE hrtraining_scheduling.isvoid = 0 AND hrtraining_scheduling.iscancel = 0 "

            If mintTrainingScheduleId > 0 Then
                StrQ &= " AND hrtraining_scheduling.trainingschedulingunkid = '" & mintTrainingScheduleId & "' "
            End If

            StrQ &= "    SET @loop = 1 " & _
                    "    SET @max = ISNULL((SELECT MAX([@tab].rid) FROM @tab),1) " & _
                    "WHILE @loop <= @max " & _
                    "BEGIN " & _
                    "    DECLARE @words VARCHAR (MAX) " & _
                    "    SET @tid = ISNULL((SELECT [@tab].tid FROM @tab WHERE [@tab].rid = @loop),0) " & _
                    "    SET @words = ISNULL((SELECT [@tab].mid FROM @tab WHERE [@tab].rid = @loop),'') " & _
                    "    DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                    "    SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                    "    WHILE @start < @stop " & _
                    "    BEGIN " & _
                    "        SELECT " & _
                    "             @end = CHARINDEX(',',@words,@start) " & _
                    "            ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                    "            ,@start = @end + 1 " & _
                    "        INSERT @split (tid,mid) VALUES (@tid,CAST(@word AS INT)) " & _
                    "    END " & _
                    "SET @loop = @loop + 1 " & _
                    "END " & _
                    "SELECT ISNULL(fs.name,'') AS fns ,[@split].tid ,[@split].mid " & _
                    "FROM @split " & _
                    "    LEFT JOIN cfcommon_master as fs ON fs.masterunkid = [@split].mid " & _
                    "WHERE 1  = 1 "

            If mstrSponsersIds.Trim.Length > 0 Then
                StrQ &= " AND [@split].mid IN (" & mstrSponsersIds & ") "
            End If

            dsFundings = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsFundings
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTraining_Sponsers; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

#Region " Report Generation "

    Public Sub Export_Training_BasedOn_Report(ByVal strDatabaseName As String, _
                                              ByVal intUserUnkid As Integer, _
                                              ByVal intYearUnkid As Integer, _
                                              ByVal intCompanyUnkid As Integer, _
                                              ByVal dtPeriodStart As Date, _
                                              ByVal dtPeriodEnd As Date, _
                                              ByVal strUserModeSetting As String, _
                                              ByVal blnOnlyApproved As Boolean, _
                                              ByVal xOpenReportAfterExport As Boolean, _
                                              ByVal xExportPath As String)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation
            Call CreateTable()

            StrQ = "SELECT " & _
                   "     0 AS Col1 " & _
                   "    ,A.course_title AS Col2 " & _
                   "    ,A.training_objective AS Col3 " & _
                   "    ,'' AS Col4 " & _
                   "    ,A.start_date AS Col5 " & _
                   "    ,A.end_date AS Col6 " & _
                   "    ,A.training_venue AS Col7 " & _
                   "    ,A.tprovider AS Col8 " & _
                   "    ,A.available_staff AS Col9 " & _
                   "    ,A.enroll_staff AS Col10 " & _
                   "    ,CASE WHEN A.Years > 0 AND A.Months > 0 AND A.Days > 0 THEN CAST(A.Years AS NVARCHAR(max)) +' '+ @Yr + ', '+ CAST(A.Months AS NVARCHAR(max)) +' '+ @Mo + ', '+ CAST(A.Days AS NVARCHAR(max)) +' '+ @Dy " & _
                   "          WHEN A.Years <= 0 AND A.Months > 0 AND A.Days > 0 THEN CAST(A.Months AS NVARCHAR(max)) +' '+  @Mo + ', '+ CAST(A.Days AS NVARCHAR(max)) +' '+ @Dy " & _
                   "          WHEN A.Years <= 0 AND A.Months <= 0 AND A.Days > 0 THEN CAST(A.Days AS NVARCHAR(max)) +' '+ @Dy " & _
                   "          WHEN A.Years > 0 AND A.Months <= 0 AND A.Days > 0 THEN CAST(A.Years AS NVARCHAR(max)) +' '+ @Yr + ', '+ CAST(A.Days AS NVARCHAR(max)) +' '+ @Dy " & _
                   "          WHEN A.Years <= 0 AND A.Months > 0 AND A.Days <= 0 THEN CAST(A.Months AS NVARCHAR(max)) +' '+ @Mo " & _
                   "          WHEN A.Years <= 0 AND A.Months <= 0 AND A.Days <= 0 THEN CAST(A.Days + 1 AS NVARCHAR(max)) +' '+ @Dy " & _
                   "          WHEN A.Years > 0 AND A.Months <= 0 AND A.Days <= 0 THEN CAST(A.Years AS NVARCHAR(max)) +' '+ @Yr " & _
                   "     END AS Col11 " & _
                   "    ,CAST(A.Hrs AS NVARCHAR(MAX)) +' '+ @Hr AS Col12 " & _
                   "    ,'' AS Col13 " & _
                   "    ,A.trainingschedulingunkid " & _
                   "    ,A.allocationtypeunkid " & _
                   "    ,A.fundingids " & _
                   "    ,A.alloc_binded " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         ts.trainingschedulingunkid " & _
                   "        ,ct.name as course_title " & _
                   "        ,ts.training_remark as training_objective " & _
                   "        ,CONVERT(NVARCHAR(8),ts.start_date,112) AS start_date " & _
                   "        ,CONVERT(NVARCHAR(8),ts.end_date,112) AS end_date " & _
                   "        ,ts.training_venue " & _
                   "        ,ISNULL(hrinstitute_master.institute_name,'') as tprovider " & _
                   "        ,(CAST(CONVERT(CHAR(8),ts.end_date,112) AS INT)-CAST(CONVERT(CHAR(8),ts.start_date,112) AS INT))/10000 AS Years " & _
                   "        ,(DATEDIFF(MONTH,ts.start_date,ts.end_date)+12)%12 - CASE WHEN DAY(ts.start_date)>DAY(ts.end_date) THEN 1 ELSE 0 END AS Months " & _
                   "        ,DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,ts.start_date,ts.end_date)+12)%12 - CASE WHEN DAY(ts.start_date)>DAY(ts.end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),ts.end_date,112) AS INT)-CAST(CONVERT(CHAR(8),ts.start_date,112) AS INT))/10000,ts.start_date)),ts.end_date) AS Days " & _
                   "        ,DATEDIFF(HOUR, CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), start_time, 108) AS DATETIME), CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), end_time, 108) AS DATETIME)) AS Hrs " & _
                   "        ,ts.position_no as available_staff " & _
                   "        ,ISNULL(enr.enroll_staff,0) AS enroll_staff " & _
                   "        ,ts.allocationtypeunkid " & _
                   "        ,ts.fundingids " & _
                   "        ,CASE WHEN ts.allocationtypeunkid = '" & enAllocation.BRANCH & "' THEN @BRANCH " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN @DEPARTMENT_GROUP " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.DEPARTMENT & "' THEN @DEPARTMENT " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.SECTION_GROUP & "' THEN @SECTION_GROUP " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.SECTION & "' THEN @SECTION " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.UNIT_GROUP & "' THEN @UNIT_GROUP " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.UNIT & "' THEN @UNIT " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.TEAM & "' THEN @TEAM " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.JOB_GROUP & "' THEN @JOB_GROUP " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.JOBS & "' THEN @JOBS " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.CLASS_GROUP & "' THEN @CLASS_GROUP " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.CLASSES & "' THEN @CLASSES " & _
                   "              WHEN ts.allocationtypeunkid = '" & enAllocation.COST_CENTER & "' THEN @COST_CENTER " & _
                   "         END AS alloc_binded " & _
                   "    FROM hrtraining_scheduling AS ts " & _
                   "        JOIN cfcommon_master ct ON ct.masterunkid = ts.course_title AND ct.mastertype = 30 " & _
                   "        LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 hrtraining_enrollment_tran.trainingschedulingunkid " & _
                   "                ,COUNT(1) as enroll_staff " & _
                   "            FROM hrtraining_enrollment_tran " & _
                   "            WHERE hrtraining_enrollment_tran.isvoid = 0 AND hrtraining_enrollment_tran.iscancel = 0 " & _
                   "            GROUP BY hrtraining_enrollment_tran.trainingschedulingunkid " & _
                   "        ) AS enr ON enr.trainingschedulingunkid = ts.trainingschedulingunkid " & _
                   "        LEFT JOIN hrinstitute_master ON ts.traininginstituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital = 0 " & _
                   "    WHERE ts.isvoid = 0 AND ts.iscancel = 0 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= ") AS A "

            objDataOperation.AddParameter("@Yr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 300, "Year(s)"))
            objDataOperation.AddParameter("@Mo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 301, "Month(s)"))
            objDataOperation.AddParameter("@Dy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 302, "Day(s)"))
            objDataOperation.AddParameter("@Hr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 303, "Hrs."))

            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
            objDataOperation.AddParameter("@COST_CENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 586, "Cost Center"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim StrGroupColName As String = String.Empty
            Select Case mintReportTypeId
                Case 0  'DURATION WISE
                    StrGroupColName = "Col11"
                Case 1  'VENUE WISE
                    StrGroupColName = "Col7"
                Case 2  'PROVIDER WISE
                    StrGroupColName = "Col8"
                Case 3  'SPONSER WISE
                    StrGroupColName = "Col13"
                Case 4  'BINDED ALLOCATION WISE
                    StrGroupColName = "Col4"
            End Select

            Dim i As Integer = 1
            Dim objCMaster As New clsCommon_Master
            Dim StrFunds As String = String.Empty
            Dim dFilter As DataTable = Nothing
            Dim xDicGroup As New Dictionary(Of String, String)
            If mintReportTypeId = 3 Then
                Dim dsFundings As New DataSet : dsFundings = GetTraining_Sponsers()
                Dim dtTable As DataTable = dsList.Tables("List").Copy
                Dim StrSchedulingTranIds As String = String.Empty
                If mstrSponsersIds.Trim.Length > 0 Then
                    StrSchedulingTranIds = String.Join(",", dsFundings.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("tid").ToString).ToArray)
                    If StrSchedulingTranIds.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), "trainingschedulingunkid IN(" & StrSchedulingTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables("List"), "trainingschedulingunkid IN(0)", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

                For Each dRow As DataRow In dsFundings.Tables(0).Rows
                    If xDicGroup.ContainsKey(dRow.Item("fns").ToString.Trim) = True Then Continue For
                    xDicGroup.Add(dRow.Item("fns").ToString.Trim, dRow.Item("fns").ToString.Trim)
                    Dim xSource As String = dRow.Item("fns").ToString.Trim
                    StrSchedulingTranIds = String.Join(",", dsFundings.Tables(0).AsEnumerable().Where(Function(y) y.Field(Of String)("fns") = xSource).Select(Function(x) x.Field(Of Integer)("tid").ToString).ToArray)
                    If StrSchedulingTranIds.Trim.Length <= 0 Then Continue For
                    dFilter = New DataView(dtTable, "trainingschedulingunkid IN (" & StrSchedulingTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    i = 1
                    For Each dFRow As DataRow In dFilter.Rows
                        StrFunds = String.Empty
                        Dim dtRow As DataRow = mdtFinalTable.NewRow
                        Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                        If mstrAllocationIds.Trim.Length > 0 Then
                            If objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                        End If
                        If dFRow.Item("alloc_binded").ToString.Length > 0 Then
                            dtRow.Item("Col4") = dFRow.Item("alloc_binded") & " -> " & objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds)
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&lt;", "<")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&gt;", ">")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&amp;", "&")
                        End If
                        objTrainingAlloc = Nothing
                        dtRow.Item("Col1") = i.ToString
                        dtRow.Item("Col2") = dFRow.Item("Col2")
                        dtRow.Item("Col3") = dFRow.Item("Col3")
                        dtRow.Item("Col5") = eZeeDate.convertDate(dFRow.Item("Col5").ToString).ToShortDateString
                        dtRow.Item("Col6") = eZeeDate.convertDate(dFRow.Item("Col6").ToString).ToShortDateString
                        dtRow.Item("Col7") = dFRow.Item("Col7")
                        dtRow.Item("Col8") = dFRow.Item("Col8")
                        dtRow.Item("Col9") = dFRow.Item("Col9")
                        dtRow.Item("Col10") = dFRow.Item("Col10")
                        dtRow.Item("Col11") = dFRow.Item("Col11")
                        dtRow.Item("Col12") = dFRow.Item("Col12")
                        'For Each StrId As String In dFRow.Item("fundingids").ToString.Split(",")
                        '    If StrId.Trim.Length > 0 Then
                        '        objCMaster._Masterunkid = StrId
                        '        StrFunds &= "," & objCMaster._Name
                        '    End If
                        'Next
                        'If StrFunds.Trim.Length > 0 Then
                        '    StrFunds = Mid(StrFunds, 2)
                        'End If
                        dtRow.Item("Col13") = dRow.Item("fns")

                        mdtFinalTable.Rows.Add(dtRow)
                        i += 1
                    Next
                Next

            ElseIf mintReportTypeId = 4 Then
                Dim dsFundings As New DataSet
                StrQ = "SELECT " & _
                       "     hrtraining_scheduling.trainingschedulingunkid " & _
                       "    ,CASE WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.BRANCH & "' THEN @BRANCH " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN @DEPARTMENT_GROUP " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT & "' THEN @DEPARTMENT " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION_GROUP & "' THEN @SECTION_GROUP " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION & "' THEN @SECTION " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT_GROUP & "' THEN @UNIT_GROUP " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT & "' THEN @UNIT " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.TEAM & "' THEN @TEAM " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOB_GROUP & "' THEN @JOB_GROUP " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOBS & "' THEN @JOBS " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASS_GROUP & "' THEN @CLASS_GROUP " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASSES & "' THEN @CLASSES " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.COST_CENTER & "' THEN @COST_CENTER " & _
                       "     END AS alloc_binded " & _
                       "    ,CASE WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.BRANCH & "' THEN hrstation_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN hrdepartment_group_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT & "' THEN hrdepartment_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION_GROUP & "' THEN hrsectiongroup_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION & "' THEN hrsection_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT_GROUP & "' THEN hrunitgroup_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT & "' THEN hrunit_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.TEAM & "' THEN hrteam_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOB_GROUP & "' THEN hrjobgroup_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOBS & "' THEN hrjob_master.job_name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASS_GROUP & "' THEN hrclassgroup_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASSES & "' THEN hrclasses_master.name " & _
                       "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.COST_CENTER & "' THEN prcostcenter_master.costcentername " & _
                       "     ELSE '' " & _
                       "     END AS allocation " & _
                       "FROM hrtraining_allocation_tran " & _
                       "    JOIN hrtraining_scheduling ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_allocation_tran.trainingschedulingunkid " & _
                       "    LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrtraining_allocation_tran.allocationunkid AND hrstation_master.isactive = 1 " & _
                       "    LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrtraining_allocation_tran.allocationunkid AND hrdepartment_group_master.isactive = 1 " & _
                       "    LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrtraining_allocation_tran.allocationunkid AND hrdepartment_master.isactive = 1 " & _
                       "    LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrtraining_allocation_tran.allocationunkid AND hrsectiongroup_master.isactive = 1 " & _
                       "    LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrtraining_allocation_tran.allocationunkid AND hrsection_master.isactive = 1 " & _
                       "    LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrtraining_allocation_tran.allocationunkid AND hrunitgroup_master.isactive = 1 " & _
                       "    LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrtraining_allocation_tran.allocationunkid AND hrunit_master.isactive = 1 " & _
                       "    LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrtraining_allocation_tran.allocationunkid AND hrteam_master.isactive = 1 " & _
                       "    LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrtraining_allocation_tran.allocationunkid AND hrjobgroup_master.isactive = 1 " & _
                       "    LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrtraining_allocation_tran.allocationunkid AND hrjob_master.isactive = 1 " & _
                       "    LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrtraining_allocation_tran.allocationunkid AND hrclassgroup_master.isactive = 1 " & _
                       "    LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrtraining_allocation_tran.allocationunkid AND hrclasses_master.isactive = 1 " & _
                       "    LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hrtraining_allocation_tran.allocationunkid AND prcostcenter_master.isactive = 1 " & _
                       "WHERE hrtraining_allocation_tran.isvoid = 0 AND hrtraining_allocation_tran.iscancel = 0 "

                If mintTrainingScheduleId > 0 Then
                    StrQ &= " AND hrtraining_scheduling.trainingschedulingunkid = '" & mintTrainingScheduleId & "' "
                End If

                If mstrAllocationIds.Trim.Length > 0 Then
                    StrQ &= " AND CASE WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.BRANCH & "' THEN hrstation_master.stationunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN hrdepartment_group_master.deptgroupunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.DEPARTMENT & "' THEN hrdepartment_master.departmentunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION_GROUP & "' THEN hrsectiongroup_master.sectiongroupunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.SECTION & "' THEN hrsection_master.sectionunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT_GROUP & "' THEN hrunitgroup_master.unitgroupunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.UNIT & "' THEN hrunit_master.unitunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.TEAM & "' THEN hrteam_master.teamunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOB_GROUP & "' THEN hrjobgroup_master.jobgroupunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.JOBS & "' THEN hrjob_master.jobunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASS_GROUP & "' THEN hrclassgroup_master.classgroupunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.CLASSES & "' THEN hrclasses_master.classesunkid " & _
                            "          WHEN hrtraining_scheduling.allocationtypeunkid = '" & enAllocation.COST_CENTER & "' THEN prcostcenter_master.costcenterunkid " & _
                            "ELSE 0 END IN (" & mstrAllocationIds & ") "
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
                objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
                objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
                objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
                objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
                objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
                objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
                objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
                objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
                objDataOperation.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
                objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
                objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
                objDataOperation.AddParameter("@COST_CENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 586, "Cost Center"))

                dsFundings = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dsSponsers As New DataSet
                dsSponsers = GetTraining_Sponsers()
                Dim dtTable As DataTable = dsList.Tables("List").Copy
                Dim StrSchedulingTranIds As String = String.Empty
                If mstrSponsersIds.Trim.Length > 0 Then
                    StrSchedulingTranIds = String.Join(",", dsSponsers.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("tid").ToString).ToArray)
                    If StrSchedulingTranIds.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), "trainingschedulingunkid IN(" & StrSchedulingTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables("List"), "trainingschedulingunkid IN(0)", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

                For Each dRow As DataRow In dsFundings.Tables(0).Rows
                    If xDicGroup.ContainsKey(dRow.Item("allocation").ToString.Trim) = True Then Continue For
                    xDicGroup.Add(dRow.Item("allocation").ToString.Trim, dRow.Item("allocation").ToString.Trim)
                    Dim xSource As String = dRow.Item("allocation").ToString.Trim
                    StrSchedulingTranIds = String.Join(",", dsFundings.Tables(0).AsEnumerable().Where(Function(y) y.Field(Of String)("allocation") = xSource).Select(Function(x) x.Field(Of Integer)("trainingschedulingunkid").ToString).ToArray)
                    If StrSchedulingTranIds.Trim.Length <= 0 Then Continue For
                    dFilter = New DataView(dtTable, "trainingschedulingunkid IN (" & StrSchedulingTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    i = 1
                    For Each dFRow As DataRow In dFilter.Rows
                        StrFunds = String.Empty
                        Dim dtRow As DataRow = mdtFinalTable.NewRow
                        Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                        If mstrAllocationIds.Trim.Length > 0 Then
                            If objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                        End If
                        If dFRow.Item("alloc_binded").ToString.Length > 0 Then
                            dtRow.Item("Col4") = dRow.Item("allocation")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&lt;", "<")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&gt;", ">")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&amp;", "&")
                        End If
                        objTrainingAlloc = Nothing
                        dtRow.Item("Col1") = i.ToString
                        dtRow.Item("Col2") = dFRow.Item("Col2")
                        dtRow.Item("Col3") = dFRow.Item("Col3")
                        dtRow.Item("Col5") = eZeeDate.convertDate(dFRow.Item("Col5").ToString).ToShortDateString
                        dtRow.Item("Col6") = eZeeDate.convertDate(dFRow.Item("Col6").ToString).ToShortDateString
                        dtRow.Item("Col7") = dFRow.Item("Col7")
                        dtRow.Item("Col8") = dFRow.Item("Col8")
                        dtRow.Item("Col9") = dFRow.Item("Col9")
                        dtRow.Item("Col10") = dFRow.Item("Col10")
                        dtRow.Item("Col11") = dFRow.Item("Col11")
                        dtRow.Item("Col12") = dFRow.Item("Col12")
                        For Each StrId As String In dFRow.Item("fundingids").ToString.Split(",")
                            If StrId.Trim.Length > 0 Then
                                objCMaster._Masterunkid = StrId
                                StrFunds &= "," & objCMaster._Name
                            End If
                        Next
                        If StrFunds.Trim.Length > 0 Then
                            StrFunds = Mid(StrFunds, 2)
                        End If
                        dtRow.Item("Col13") = StrFunds

                        mdtFinalTable.Rows.Add(dtRow)
                        i += 1
                    Next
                Next

            Else

                Dim dtTable As DataTable = dsList.Tables("List").Copy
                If mstrSponsersIds.Trim.Length > 0 Then
                    Dim dsFundings As New DataSet
                    dsFundings = GetTraining_Sponsers()
                    Dim StrSchedulingTranIds As String = String.Empty
                    StrSchedulingTranIds = String.Join(",", dsFundings.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("tid").ToString).ToArray)
                    If StrSchedulingTranIds.Trim.Length > 0 Then
                        dtTable = New DataView(dsList.Tables("List"), "trainingschedulingunkid IN(" & StrSchedulingTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables("List"), "trainingschedulingunkid IN(0)", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

                For Each dRow As DataRow In dtTable.Rows
                    If xDicGroup.ContainsKey(dRow.Item(StrGroupColName).ToString.Trim) = True Then Continue For
                    xDicGroup.Add(dRow.Item(StrGroupColName).ToString.Trim, dRow.Item(StrGroupColName).ToString.Trim)
                    dFilter = New DataView(dtTable, StrGroupColName & " = '" & dRow.Item(StrGroupColName).ToString.Replace("'", "''").Trim & "'", "", DataViewRowState.CurrentRows).ToTable
                    i = 1
                    For Each dFRow As DataRow In dFilter.Rows
                        StrFunds = String.Empty
                        Dim dtRow As DataRow = mdtFinalTable.NewRow
                        Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                        If mstrAllocationIds.Trim.Length > 0 Then
                            If objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                        End If
                        If dFRow.Item("alloc_binded").ToString.Length > 0 Then
                            dtRow.Item("Col4") = dFRow.Item("alloc_binded") & " -> " & objTrainingAlloc.GetCSV_AllocationName(dFRow.Item("allocationtypeunkid"), dFRow.Item("trainingschedulingunkid"), mstrAllocationIds)
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&lt;", "<")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&gt;", ">")
                            dtRow.Item("Col4") = dtRow.Item("Col4").ToString.Replace("&amp;", "&")
                        End If
                        objTrainingAlloc = Nothing
                        dtRow.Item("Col1") = i.ToString
                        dtRow.Item("Col2") = dFRow.Item("Col2")
                        dtRow.Item("Col3") = dFRow.Item("Col3")
                        dtRow.Item("Col5") = eZeeDate.convertDate(dFRow.Item("Col5").ToString).ToShortDateString
                        dtRow.Item("Col6") = eZeeDate.convertDate(dFRow.Item("Col6").ToString).ToShortDateString
                        dtRow.Item("Col7") = dFRow.Item("Col7")
                        dtRow.Item("Col8") = dFRow.Item("Col8")
                        dtRow.Item("Col9") = dFRow.Item("Col9")
                        dtRow.Item("Col10") = dFRow.Item("Col10")
                        dtRow.Item("Col11") = dFRow.Item("Col11")
                        dtRow.Item("Col12") = dFRow.Item("Col12")
                        For Each StrId As String In dFRow.Item("fundingids").ToString.Split(",")
                            If StrId.Trim.Length > 0 Then
                                objCMaster._Masterunkid = StrId
                                StrFunds &= "," & objCMaster._Name
                            End If
                        Next
                        If StrFunds.Trim.Length > 0 Then
                            StrFunds = Mid(StrFunds, 2)
                        End If
                        dtRow.Item("Col13") = StrFunds

                        mdtFinalTable.Rows.Add(dtRow)
                        i += 1
                    Next
                Next
            End If

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtFinalTable.Columns.Count - 1)
            For j As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(j) = 125
            Next
            Dim strarrGroupColumns As String() = Nothing
            Dim strGrpCols As String() = {StrGroupColName}
            strarrGroupColumns = strGrpCols

            'S.SANDEEP [04-APR-2017] -- START
            'ISSUE/ENHANCEMENT : Big Name Issue in Opening
            Dim strExtraTitle As String = mstrReportTypeName
            'S.SANDEEP [04-APR-2017] -- END

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportPath, xOpenReportAfterExport, mdtFinalTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, strExtraTitle, "", Nothing, "", False, Nothing, Nothing, Nothing)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Training_BasedOn_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 586, "Cost Center")
            Language.setMessage(mstrModuleName, 100, "S/N")
            Language.setMessage(mstrModuleName, 101, "Course Title")
            Language.setMessage(mstrModuleName, 102, "Training Objective")
            Language.setMessage(mstrModuleName, 103, "Binded Allocation")
            Language.setMessage(mstrModuleName, 104, "Start Date")
            Language.setMessage(mstrModuleName, 105, "End Date")
            Language.setMessage(mstrModuleName, 106, "Venue")
            Language.setMessage(mstrModuleName, 107, "Provider")
            Language.setMessage(mstrModuleName, 108, "Total Staff")
            Language.setMessage(mstrModuleName, 109, "Enrolled Staff")
            Language.setMessage(mstrModuleName, 110, "Duration")
            Language.setMessage(mstrModuleName, 111, "Hours")
            Language.setMessage(mstrModuleName, 112, "Sponser")
            Language.setMessage(mstrModuleName, 200, "Course Title :")
            Language.setMessage(mstrModuleName, 201, "Schedule Date From :")
            Language.setMessage(mstrModuleName, 202, "To :")
            Language.setMessage(mstrModuleName, 203, "Provider :")
            Language.setMessage(mstrModuleName, 204, "Sponsers :")
            Language.setMessage(mstrModuleName, 205, "Allocation Binded :")
            Language.setMessage(mstrModuleName, 206, "Allocation(s) :")
            Language.setMessage(mstrModuleName, 300, "Year(s)")
            Language.setMessage(mstrModuleName, 301, "Month(s)")
            Language.setMessage(mstrModuleName, 302, "Day(s)")
            Language.setMessage(mstrModuleName, 303, "Hrs.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
