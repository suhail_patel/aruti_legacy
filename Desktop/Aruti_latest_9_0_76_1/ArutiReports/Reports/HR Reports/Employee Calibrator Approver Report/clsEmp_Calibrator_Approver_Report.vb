'************************************************************************************************************************************
'Class Name : clsEmp_Calibrator_Approver_Report.vb
'Purpose    :
'Date          :16/07/2020
'Written By :Pinkal Jariwwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsEmp_Calibrator_Approver_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmp_Calibrator_Approver_Report"
    Private mstrReportId As String = enArutiReport.Employee_Calibrator_Approver_Report  '233
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mdtEmployeeAsonDate As Date = Nothing
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeAsonDate() As Date
        Set(ByVal value As Date)
            mdtEmployeeAsonDate = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = String.Empty
            mblnIncludeInactiveEmp = False
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeUnkid > 0 Then
                Me._FilterQuery &= " AND CEM.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Order By :") & " " & Me.OrderByDisplay & " "

                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY " & mstrAnalysis_OrderBy_GName & "," & Me.OrderByQuery & " ,LM.priority,CM.iscalibrator "
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery & " ,LM.priority,CM.iscalibrator "
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid
            User._Object._Userunkid = xUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(CEM.employeecode,'')", Language.getMessage(mstrModuleName, 3, "Employee Code")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(CEM.surname,'')+' '+ ISNULL(CEM.firstname,'')", Language.getMessage(mstrModuleName, 4, "Employee")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(CEM.firstname,'')+' '+ ISNULL(CEM.surname,'')", Language.getMessage(mstrModuleName, 4, "Employee")))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                       " DROP TABLE #Results " & _
                       " CREATE TABLE #Results " & _
                       " ( " & _
                             "ecode NVARCHAR(50), " & _
                             "ename NVARCHAR(Max), " & _
                             "dept_name NVARCHAR(MAX), " & _
                             "job_name NVARCHAR(MAX), " & _
                             "astatus BIT, " & _
                             "empid INT, " & _
                             "usrid INT " & _
                        " ) " & _
                        " DECLARE @DName  NVARCHAR(MAX), @Ddate  NVARCHAR(MAX), @EmpId INT, @UsrId INT " & _
                        " DECLARE cursor_name CURSOR " & _
                        " FOR " & _
                        "    SELECT DISTINCT " & _
                        "        ISNULL(FM.database_name, '') AS dName " & _
                        "       ,ISNULL(CF.key_value, '') AS ddate " & _
                        "       ,UM.employeeunkid " & _
                        "       ,UM.userunkid " & _
                        "   FROM hrscore_calibration_approver_master AS CM " & _
                        "   JOIN hrmsConfiguration..cfuser_master AS UM ON CM.mapuserunkid = UM.userunkid " & _
                        "   JOIN hrmsConfiguration..cffinancial_year_tran AS FM ON FM.companyunkid = UM.companyunkid AND FM.isclosed = 0 " & _
                        "   JOIN hrmsConfiguration..cfconfiguration AS CF ON CF.companyunkid = UM.companyunkid AND UPPER([CF].[key_name]) = 'EMPLOYEEASONDATE' " & _
                        "   WHERE 1 = 1 AND CM.isvoid = 0 AND CM.visibletypeid = 1 " & _
                        "   OPEN cursor_name " & _
                        "   FETCH NEXT FROM cursor_name INTO @DName,@Ddate,@EmpId,@UsrId " & _
                        "   WHILE @@FETCH_STATUS = 0 " & _
                        "   BEGIN " & _
                        "       DECLARE @Q AS NVARCHAR(MAX) " & _
                        "       SET @Q = 'INSERT INTO #Results(ecode,ename,dept_name,job_name,astatus,empid,usrid) " & _
                        "       Select   EM.employeecode AS ecode " & _
                        "       ,EM.firstname+ '' '' +EM.surname AS ename " & _
                        "       ,edm.name as dept_name " & _
                        "       ,ejm.job_name " & _
                        "       ,CASE WHEN CONVERT(CHAR(8), EM.appointeddate, 112) <= CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112) " & _
                        "               AND ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112)) >= CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112) " & _
                        "               AND ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112)) >= CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112) " & _
                        "               AND ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112)) >= CONVERT(CHAR(8), CAST(''' + @Ddate + ''' AS DATETIME), 112) " & _
                        "       THEN 1 ELSE 0 END AS astatus " & _
                        "       ,' + CAST(@EmpId AS NVARCHAR(10)) + ' ,' + CAST(@UsrId AS NVARCHAR(10)) + ' " & _
                        "       FROM ' + @DName + '..hremployee_master AS EM " & _
                        "       JOIN " & _
                        "       ( " & _
                        "               SELECT " & _
                        "                    et.employeeunkid " & _
                        "                   ,et.departmentunkid " & _
                        "                   ,ROW_NUMBER()OVER(PARTITION BY et.employeeunkid ORDER BY et.effectivedate DESC) as rno " & _
                        "               FROM ' + @DName + '..hremployee_transfer_tran AS et " & _
                        "               WHERE et.isvoid = 0 AND CONVERT(NVARCHAR(8),et.effectivedate,112) <= ' + @Ddate + ' " & _
                        "               AND et.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(10)) + ' " & _
                        "       ) AS edt ON edt.employeeunkid = EM.employeeunkid AND edt.rno = 1 " & _
                        "       JOIN ' + @DName + '..hrdepartment_master AS edm ON edm.departmentunkid = edt.departmentunkid " & _
                        "       JOIN " & _
                        "       ( " & _
                        "               SELECT " & _
                        "                    ect.employeeunkid " & _
                        "                   ,ect.jobunkid " & _
                        "                   ,ROW_NUMBER()OVER(PARTITION BY ect.employeeunkid ORDER BY ect.effectivedate DESC) as rno " & _
                        "               FROM ' + @DName + '..hremployee_categorization_tran AS ect " & _
                        "               WHERE ect.isvoid = 0 AND CONVERT(NVARCHAR(8),ect.effectivedate,112) <= ' + @Ddate + ' " & _
                        "               AND ect.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(10)) + ' " & _
                        "       ) AS ejb ON ejb.employeeunkid = EM.employeeunkid AND ejb.rno = 1 " & _
                        "   JOIN ' + @DName + '..hrjob_master AS ejm ON ejb.jobunkid = ejm.jobunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            T.EmpId " & _
                        "           ,T.EOC " & _
                        "           ,T.LEAVING " & _
                        "           ,T.isexclude_payroll AS IsExPayroll " & _
                        "       FROM " & _
                        "       ( " & _
                        "               SELECT " & _
                        "                   employeeunkid AS EmpId " & _
                        "                   ,date1 AS EOC " & _
                        "                   ,date2 AS LEAVING " & _
                        "                   ,effectivedate " & _
                        "                   ,isexclude_payroll " & _
                        "                   ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                        "              FROM ' + @DName + '..hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " & _
                        "              AND CONVERT(CHAR(8),effectivedate,112) <= ' + @Ddate + ' " & _
                        "              AND hremployee_dates_tran.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(10)) + ' " & _
                        "       ) AS T WHERE T.xNo = 1 " & _
                        "   ) AS TRM ON TRM.EmpId = EM.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "           SELECT " & _
                        "                R.EmpId " & _
                        "               ,R.RETIRE " & _
                        "           FROM " & _
                        "           ( " & _
                        "               SELECT " & _
                        "                    employeeunkid AS EmpId " & _
                        "                   ,date1 AS RETIRE " & _
                        "                   ,effectivedate " & _
                        "                   ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                        "               FROM ' + @DName + '..hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " & _
                        "               AND CONVERT(CHAR(8),effectivedate,112) <= ' + @Ddate + ' " & _
                        "               AND hremployee_dates_tran.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(10)) + ' " & _
                        "   ) AS R WHERE R.xNo = 1 " & _
                        " ) AS RET ON RET.EmpId = EM.employeeunkid " & _
                        " LEFT JOIN " & _
                        " ( " & _
                        "       SELECT " & _
                        "            RH.EmpId " & _
                        "           ,RH.REHIRE " & _
                        "       FROM " & _
                        "       ( " & _
                        "               SELECT " & _
                        "                    employeeunkid AS EmpId " & _
                        "                   ,reinstatment_date AS REHIRE " & _
                        "                   ,effectivedate " & _
                        "                   ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                        "               FROM ' + @DName + '..hremployee_rehire_tran WHERE isvoid = 0 " & _
                        "               AND CONVERT(CHAR(8),effectivedate,112) <= ' + @Ddate + ' " & _
                        "               AND hremployee_rehire_tran.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(10)) + ' " & _
                        "      ) AS RH WHERE RH.xNo = 1 " & _
                        " ) AS HIRE ON HIRE.EmpId = EM.employeeunkid " & _
                        "   WHERE EM.employeeunkid = ' + CAST(@EmpId AS NVARCHAR(10)) + '' " & _
                        " EXEC (@Q) " & _
                        " FETCH NEXT FROM cursor_name INTO @DName, @Ddate, @EmpId, @UsrId " & _
                        " END " & _
                        " CLOSE cursor_name " & _
                        "   DEALLOCATE cursor_name " & _
                        "   SELECT " & _
                        "        CASE WHEN ISNULL(#Results.ename, '') = '' THEN UM.username ELSE #Results.ename END AS username " & _
                        "       ,CASE WHEN CM.iscalibrator = 1 THEN @Calibrator  WHEN CM.iscalibrator = 0 THEN @Approver END AS usrtype " & _
                        "       ,CAST(CM.iscalibrator AS INT) AS iscalibrator " & _
                        "       ,ISNULL(LM.levelname, '') AS levelname " & _
                        "       ,ISNULL(#Results.dept_name, '') AS dept_name " & _
                        "       ,ISNULL(#Results.job_name, '') AS job_name " & _
                        "       ,CASE WHEN CM.isactive = 0 THEN 0 WHEN CM.isactive = 1 THEN ISNULL(#Results.astatus, UM.isactive) END AS actstatus " & _
                        "       ,CEM.employeecode AS Code "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(CEM.surname,'')+' '+ ISNULL(CEM.firstname,'') AS Employee "
            Else
                StrQ &= ", ISNULL(CEM.firstname,'')+' '+ ISNULL(CEM.surname,'') AS Employee "
            End If

            StrQ &= "       ,edm.name AS EmpDept " & _
                        "       ,ejm.job_name AS EmpJob "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "   FROM hrscore_calibration_approver_master AS CM " & _
                        "   JOIN hrscore_calibration_approver_tran AS CT ON CM.mappingunkid = CT.mappingunkid " & _
                        "   JOIN hremployee_master AS CEM ON CEM.employeeunkid = CT.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry.Trim().Replace("hremployee_master", "CEM") & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry.Trim().Replace("hremployee_master", "CEM") & " "
                End If
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "   JOIN (   SELECT " & _
                        "                    et.employeeunkid " & _
                        "                   ,et.departmentunkid " & _
                        "                   ,ROW_NUMBER() OVER (PARTITION BY et.employeeunkid ORDER BY et.effectivedate DESC) AS rno " & _
                        "               FROM hremployee_transfer_tran AS et " & _
                        "               WHERE et.isvoid = 0 AND CONVERT(NVARCHAR(8), et.effectivedate, 112) <= @EmployeeAsonDate " & _
                        "           ) AS edept ON edept.employeeunkid = CEM.employeeunkid AND edept.rno = 1 " & _
                        "   LEFT JOIN hrdepartment_master AS edm ON edept.departmentunkid = edm.departmentunkid " & _
                        "   JOIN (   SELECT " & _
                        "                    ect.employeeunkid " & _
                        "                   ,ect.jobunkid " & _
                        "                   ,ROW_NUMBER() OVER (PARTITION BY ect.employeeunkid ORDER BY ect.effectivedate DESC) AS rno " & _
                        "              FROM hremployee_categorization_tran AS ect " & _
                        "              WHERE ect.isvoid = 0 AND CONVERT(NVARCHAR(8), ect.effectivedate, 112) <= @EmployeeAsonDate " & _
                        "           ) AS ejb ON ejb.employeeunkid = CEM.employeeunkid AND ejb.rno = 1 " & _
                        "   JOIN hrjob_master AS ejm ON ejb.jobunkid = ejm.jobunkid " & _
                        "   LEFT JOIN hrscore_calibration_approverlevel_master AS LM  ON CM.levelunkid = LM.levelunkid " & _
                        "   JOIN hrmsConfiguration..cfuser_master AS UM  ON CM.mapuserunkid = UM.userunkid " & _
                        "   LEFT JOIN #Results ON #Results.usrid = UM.userunkid AND UM.employeeunkid = #Results.empid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join.Replace("hremployee_master", "CEM") & " "
            End If


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If mblnIncludeInactiveEmp = False Then
            '    If xDateJoinQry.Trim.Length > 0 Then
            '        StrQ &= xDateJoinQry.Trim().Replace("hremployee_master", "CEM") & " "
            '    End If
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry.Trim().Replace("hremployee_master", "CEM") & " "
            'End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "   WHERE CM.isvoid = 0 AND CM.visibletypeid = 1 " & _
                    " AND CT.isvoid = 0 AND CT.visibletypeid = 1 "

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry.Trim().Replace("hremployee_master", "CEM") & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "   IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                        "   DROP TABLE #Results "

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate.Date))
            objDataOperation.AddParameter("@Calibrator", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 111, "Calibrator"))
            objDataOperation.AddParameter("@Approver", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 112, "Approver"))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("Code")
                rpt_Rows.Item("Column3") = dtRow.Item("Employee")
                rpt_Rows.Item("Column4") = dtRow.Item("EmpDept")
                rpt_Rows.Item("Column5") = dtRow.Item("EmpJob")
                rpt_Rows.Item("Column6") = dtRow.Item("usrtype")

                If dtRow.Item("usrtype") = Language.getMessage(mstrModuleName, 111, "Calibrator") Then
                    rpt_Rows.Item("Column7") = dtRow.Item("username")
                    rpt_Rows.Item("Column8") = dtRow.Item("job_name")
                    rpt_Rows.Item("Column13") = dtRow.Item("dept_name")
                ElseIf dtRow.Item("usrtype") = Language.getMessage(mstrModuleName, 112, "Approver") Then
                    rpt_Rows.Item("Column9") = dtRow.Item("levelname")
                    rpt_Rows.Item("Column10") = dtRow.Item("username")
                    rpt_Rows.Item("Column11") = dtRow.Item("job_name")
                    rpt_Rows.Item("Column14") = dtRow.Item("dept_name")
                End If
                rpt_Rows.Item("Column12") = dtRow.Item("iscalibrator")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmpCalibrator_Approver_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 7, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 8, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 9, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 10, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 3, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 5, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 6, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 11, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 12, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Order By :")
            Language.setMessage(mstrModuleName, 3, "Employee Code")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Job Title")
            Language.setMessage(mstrModuleName, 7, "Prepared By :")
            Language.setMessage(mstrModuleName, 8, "Checked By :")
            Language.setMessage(mstrModuleName, 9, "Approved By :")
            Language.setMessage(mstrModuleName, 10, "Received By :")
            Language.setMessage(mstrModuleName, 11, "Sub Total :")
            Language.setMessage(mstrModuleName, 12, "Grand Total :")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
