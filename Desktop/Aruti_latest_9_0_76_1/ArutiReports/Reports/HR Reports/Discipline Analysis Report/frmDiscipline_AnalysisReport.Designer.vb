﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiscipline_AnalysisReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchAgainst = New eZee.Common.eZeeGradientButton
        Me.lblAgainstPerson = New System.Windows.Forms.Label
        Me.cboAgainst = New System.Windows.Forms.ComboBox
        Me.objbtnSearchInvPerson = New eZee.Common.eZeeGradientButton
        Me.lblInvolvedPerson = New System.Windows.Forms.Label
        Me.cboInvolvedPerson = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.lblDisciplineAction = New System.Windows.Forms.Label
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.cboDisciplineAction = New System.Windows.Forms.ComboBox
        Me.objbtnSearchInvestigator = New eZee.Common.eZeeGradientButton
        Me.lblInvestigator = New System.Windows.Forms.Label
        Me.cboInvestigator = New System.Windows.Forms.ComboBox
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 500)
        Me.NavPanel.Size = New System.Drawing.Size(751, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 279)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(466, 63)
        Me.gbSortBy.TabIndex = 26
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(418, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(82, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(100, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(312, 21)
        Me.txtOrderBy.TabIndex = 9
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAgainst)
        Me.gbFilterCriteria.Controls.Add(Me.lblAgainstPerson)
        Me.gbFilterCriteria.Controls.Add(Me.cboAgainst)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchInvPerson)
        Me.gbFilterCriteria.Controls.Add(Me.lblInvolvedPerson)
        Me.gbFilterCriteria.Controls.Add(Me.cboInvolvedPerson)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchInvestigator)
        Me.gbFilterCriteria.Controls.Add(Me.lblInvestigator)
        Me.gbFilterCriteria.Controls.Add(Me.cboInvestigator)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 75)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(466, 198)
        Me.gbFilterCriteria.TabIndex = 25
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAgainst
        '
        Me.objbtnSearchAgainst.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAgainst.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAgainst.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAgainst.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAgainst.BorderSelected = False
        Me.objbtnSearchAgainst.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAgainst.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAgainst.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAgainst.Location = New System.Drawing.Point(427, 141)
        Me.objbtnSearchAgainst.Name = "objbtnSearchAgainst"
        Me.objbtnSearchAgainst.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAgainst.TabIndex = 196
        '
        'lblAgainstPerson
        '
        Me.lblAgainstPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgainstPerson.Location = New System.Drawing.Point(8, 144)
        Me.lblAgainstPerson.Name = "lblAgainstPerson"
        Me.lblAgainstPerson.Size = New System.Drawing.Size(86, 15)
        Me.lblAgainstPerson.TabIndex = 194
        Me.lblAgainstPerson.Text = "Against"
        Me.lblAgainstPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAgainst
        '
        Me.cboAgainst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAgainst.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAgainst.FormattingEnabled = True
        Me.cboAgainst.Location = New System.Drawing.Point(109, 141)
        Me.cboAgainst.Name = "cboAgainst"
        Me.cboAgainst.Size = New System.Drawing.Size(312, 21)
        Me.cboAgainst.TabIndex = 7
        '
        'objbtnSearchInvPerson
        '
        Me.objbtnSearchInvPerson.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInvPerson.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInvPerson.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInvPerson.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInvPerson.BorderSelected = False
        Me.objbtnSearchInvPerson.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInvPerson.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInvPerson.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInvPerson.Location = New System.Drawing.Point(427, 114)
        Me.objbtnSearchInvPerson.Name = "objbtnSearchInvPerson"
        Me.objbtnSearchInvPerson.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInvPerson.TabIndex = 193
        '
        'lblInvolvedPerson
        '
        Me.lblInvolvedPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvolvedPerson.Location = New System.Drawing.Point(8, 117)
        Me.lblInvolvedPerson.Name = "lblInvolvedPerson"
        Me.lblInvolvedPerson.Size = New System.Drawing.Size(86, 15)
        Me.lblInvolvedPerson.TabIndex = 191
        Me.lblInvolvedPerson.Text = "Involved Person"
        Me.lblInvolvedPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInvolvedPerson
        '
        Me.cboInvolvedPerson.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInvolvedPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInvolvedPerson.FormattingEnabled = True
        Me.cboInvolvedPerson.Location = New System.Drawing.Point(109, 114)
        Me.cboInvolvedPerson.Name = "cboInvolvedPerson"
        Me.cboInvolvedPerson.Size = New System.Drawing.Size(312, 21)
        Me.cboInvolvedPerson.TabIndex = 6
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 180
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(109, 168)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(117, 21)
        Me.cboStatus.TabIndex = 8
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(9, 171)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(86, 15)
        Me.lblStatus.TabIndex = 182
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(8, 62)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(86, 15)
        Me.lblDisciplineType.TabIndex = 93
        Me.lblDisciplineType.Text = "Discipline Type"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDisciplineAction
        '
        Me.lblDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineAction.Location = New System.Drawing.Point(233, 63)
        Me.lblDisciplineAction.Name = "lblDisciplineAction"
        Me.lblDisciplineAction.Size = New System.Drawing.Size(66, 15)
        Me.lblDisciplineAction.TabIndex = 92
        Me.lblDisciplineAction.Text = "Action"
        Me.lblDisciplineAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.DropDownWidth = 180
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(109, 60)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(117, 21)
        Me.cboDisciplineType.TabIndex = 3
        '
        'cboDisciplineAction
        '
        Me.cboDisciplineAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineAction.DropDownWidth = 180
        Me.cboDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineAction.FormattingEnabled = True
        Me.cboDisciplineAction.Location = New System.Drawing.Point(303, 60)
        Me.cboDisciplineAction.Name = "cboDisciplineAction"
        Me.cboDisciplineAction.Size = New System.Drawing.Size(117, 21)
        Me.cboDisciplineAction.TabIndex = 4
        '
        'objbtnSearchInvestigator
        '
        Me.objbtnSearchInvestigator.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInvestigator.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInvestigator.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInvestigator.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInvestigator.BorderSelected = False
        Me.objbtnSearchInvestigator.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInvestigator.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInvestigator.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInvestigator.Location = New System.Drawing.Point(427, 87)
        Me.objbtnSearchInvestigator.Name = "objbtnSearchInvestigator"
        Me.objbtnSearchInvestigator.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInvestigator.TabIndex = 89
        '
        'lblInvestigator
        '
        Me.lblInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvestigator.Location = New System.Drawing.Point(8, 90)
        Me.lblInvestigator.Name = "lblInvestigator"
        Me.lblInvestigator.Size = New System.Drawing.Size(86, 15)
        Me.lblInvestigator.TabIndex = 87
        Me.lblInvestigator.Text = "Investigator"
        Me.lblInvestigator.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInvestigator
        '
        Me.cboInvestigator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInvestigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInvestigator.FormattingEnabled = True
        Me.cboInvestigator.Location = New System.Drawing.Point(109, 87)
        Me.cboInvestigator.Name = "cboInvestigator"
        Me.cboInvestigator.Size = New System.Drawing.Size(312, 21)
        Me.cboInvestigator.TabIndex = 5
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(234, 36)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(66, 15)
        Me.lblToDate.TabIndex = 86
        Me.lblToDate.Text = "To"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(303, 33)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpToDate.TabIndex = 2
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(86, 15)
        Me.lblFromDate.TabIndex = 84
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(109, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(117, 21)
        Me.dtpFromDate.TabIndex = 1
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(233, 171)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(188, 16)
        Me.chkInactiveemp.TabIndex = 71
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'frmDiscipline_AnalysisReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(751, 555)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.KeyPreview = True
        Me.Name = "frmDiscipline_AnalysisReport"
        Me.Text = "frmDiscipline_analysis_Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents lblDisciplineAction As System.Windows.Forms.Label
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents cboDisciplineAction As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchInvestigator As eZee.Common.eZeeGradientButton
    Friend WithEvents lblInvestigator As System.Windows.Forms.Label
    Friend WithEvents cboInvestigator As System.Windows.Forms.ComboBox
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchAgainst As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAgainstPerson As System.Windows.Forms.Label
    Friend WithEvents cboAgainst As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchInvPerson As eZee.Common.eZeeGradientButton
    Friend WithEvents lblInvolvedPerson As System.Windows.Forms.Label
    Friend WithEvents cboInvolvedPerson As System.Windows.Forms.ComboBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class
