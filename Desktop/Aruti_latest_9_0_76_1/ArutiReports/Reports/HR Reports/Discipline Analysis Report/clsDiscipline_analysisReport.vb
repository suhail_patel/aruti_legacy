'************************************************************************************************************************************
'Class Name : clsDiscipline_analysisReport.vb
'Purpose    :
'Date       : 22-Apr-2011
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>


Public Class clsDiscipline_analysisReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_analysisReport"
    Private mstrReportId As String = enArutiReport.DisciplineAnalysisReport
    Dim objDataOperation As clsDataOperation
    Dim dtFinalTable As DataTable

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mdtFromDate As DateTime
    Private mdtToDate As DateTime
    Private mintDisciplineTypeId As Integer = 0
    Private mstrDisciplineTypeName As String = String.Empty
    Private mintActionId As Integer = 0
    Private mstrActionName As String = String.Empty
    Private mintInvastigatorId As Integer = 0
    Private mstrInvastigatorName As String = String.Empty
    Private mintInvolvedPersonId As Integer = 0
    Private mstrInvolvedPersonName As String = String.Empty
    Private mintAgaintstPersonId As Integer = 0
    Private mstrAgainstPersonName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatus As String = String.Empty
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _ActionId() As Integer
        Set(ByVal value As Integer)
            mintActionId = value
        End Set
    End Property

    Public WriteOnly Property _ActionName() As String
        Set(ByVal value As String)
            mstrActionName = value
        End Set
    End Property

    Public WriteOnly Property _DisciplineTypeId() As Integer
        Set(ByVal value As Integer)
            mintDisciplineTypeId = value
        End Set
    End Property

    Public WriteOnly Property _DisciplineTypeName() As String
        Set(ByVal value As String)
            mstrDisciplineTypeName = value
        End Set
    End Property

    Public WriteOnly Property _InvastigatorId() As Integer
        Set(ByVal value As Integer)
            mintInvastigatorId = value
        End Set
    End Property

    Public WriteOnly Property _InvastigatorName() As String
        Set(ByVal value As String)
            mstrInvastigatorName = value
        End Set
    End Property

    Public WriteOnly Property _InvolvedPersonId() As Integer
        Set(ByVal value As Integer)
            mintInvolvedPersonId = value
        End Set
    End Property

    Public WriteOnly Property _InvolvedPersonName() As String
        Set(ByVal value As String)
            mstrInvolvedPersonName = value
        End Set
    End Property

    Public WriteOnly Property _AgaintstPersonId() As Integer
        Set(ByVal value As Integer)
            mintAgaintstPersonId = value
        End Set
    End Property

    Public WriteOnly Property _AgainstPersonName() As String
        Set(ByVal value As String)
            mstrAgainstPersonName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _Status() As String
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintDisciplineTypeId = 0
            mstrDisciplineTypeName = ""
            mintActionId = 0
            mstrActionName = ""
            mintInvastigatorId = 0
            mstrInvastigatorName = ""
            mintInvolvedPersonId = 0
            mstrInvolvedPersonName = ""
            mintAgaintstPersonId = 0
            mstrAgainstPersonName = ""
            mintStatusId = 0
            mstrStatus = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@Complete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Complete"))
            objDataOperation.AddParameter("@InComplete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "In Complete"))

            If mdtFromDate <> Nothing And mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterQuery &= " AND Convert(varchar(10), hrdiscipline_analysis.discipline_tran_date,112) Between @FromDate  AND @ToDate "

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Date From :") & "" & mdtFromDate.Date & " " & _
                                   Language.getMessage(mstrModuleName, 25, "To") & " " & mdtToDate.Date & " "
            End If

            If mintDisciplineTypeId > 0 Then
                objDataOperation.AddParameter("@DisciplineTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineTypeId)
                Me._FilterQuery &= " AND hrdiscipline_analysis.disciplinetypeunkid = @DisciplineTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "Discipline Type :") & " " & mstrDisciplineTypeName & " "
            End If

            If mintActionId > 0 Then
                objDataOperation.AddParameter("@ActionId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionId)
                Me._FilterQuery &= " AND hrdiscipline_analysis.disciplinaryactionunkid = @ActionId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Discipline Action :") & " " & mstrActionName & " "
            End If

            If mintInvastigatorId > 0 Then
                objDataOperation.AddParameter("@InvastigatorId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvastigatorId)
                Me._FilterQuery &= " AND hrdiscipline_analysis.investigatorunkid = @InvastigatorId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Investigator :") & " " & mstrInvastigatorName & " "
            End If

            If mintInvolvedPersonId > 0 Then
                objDataOperation.AddParameter("@InvpersonId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolvedPersonId)
                Me._FilterQuery &= " AND hrdiscipline_analysis.involved_personunkid = @InvpersonId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Involved Person :") & " " & mstrInvolvedPersonName & " "
            End If

            If mintAgaintstPersonId > 0 Then
                objDataOperation.AddParameter("@AgnstpersonId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgaintstPersonId)
                Me._FilterQuery &= " AND hrdiscipline_analysis.against_personunkid = @AgnstpersonId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Against :") & " " & mstrAgainstPersonName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND hrdiscipline_analysis.status = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Stauts :") & " " & mstrStatus & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "generateReport", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setDefaultOrderBy", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setOrderBy", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = Value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("name", Language.getMessage(mstrModuleName, 1, "Discipline Type")))
            iColumn_DetailReport.Add(New IColumn("reason_action", Language.getMessage(mstrModuleName, 2, "Discipline Action")))
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Create_OnDetailReport", mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataoperation

            StrQ = " SELECT Convert(varchar(10),discipline_tran_date,112) discipline_tran_date " & _
                       ", hrdisciplinetype_master.name  " & _
                       ", hraction_reason_master.reason_action " & _
                       ", Convert(varchar(10),action_start_date,112) action_start_date" & _
                       ", Convert(varchar(10),action_end_date,112) action_end_date " & _
                       ", ISNULL(inv.firstname, '') + ' ' + ISNULL(inv.surname, '') investigator  " & _
                       ", ISNULL(inpsn.firstname, '') + ' ' + ISNULL(inpsn.surname, '') involveperson  " & _
                       ", ISNULL(agpsn.firstname, '') + ' ' + ISNULL(agpsn.surname, '') againstperson  " & _
                       ", incident_caused , CASE WHEN status = 1 THEN @Complete" & _
                       "                                    WHEN status = 2 THEN  @InComplete  END [status] " & _
                       ", hrdiscipline_analysis.remark , resolution_steps " & _
                       ", hrmsConfiguration..cfuser_master.username " & _
                       " FROM hrdiscipline_analysis " & _
                       " JOIN hrdisciplinetype_master ON hrdiscipline_analysis.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                       " JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_analysis.disciplinaryactionunkid " & _
                       " AND hraction_reason_master.isreason = 0 AND hraction_reason_master.isactive = 1 " & _
                       " JOIN hremployee_master inv ON inv.employeeunkid = hrdiscipline_analysis.investigatorunkid " & _
                       " JOIN hremployee_master inpsn ON inpsn.employeeunkid = hrdiscipline_analysis.involved_personunkid " & _
                       " LEFT JOIN hremployee_master agpsn ON agpsn.employeeunkid = hrdiscipline_analysis.against_personunkid " & _
                       " LEFT JOIN hrmsConfiguration..cfuser_master ON hrdiscipline_analysis.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                       " WHERE   hrdiscipline_analysis.isvoid = 0 "

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            If mblnIsActive = False Then
                StrQ &= " AND inv.isactive = 1 AND inpsn.isactive = 1 AND agpsn.isactive = 1 "
            End If
            'Pinkal (24-Jun-2011) -- End


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = eZeeDate.convertDate(dtRow.Item("discipline_tran_date").ToString).ToShortDateString
                rpt_Row.Item("Column2") = dtRow.Item("name")
                rpt_Row.Item("Column3") = dtRow.Item("reason_action")
                rpt_Row.Item("Column4") = dtRow.Item("investigator")
                rpt_Row.Item("Column5") = dtRow.Item("involveperson")
                rpt_Row.Item("Column6") = dtRow.Item("againstperson")
                rpt_Row.Item("Column7") = dtRow.Item("incident_caused")

                If dtRow.Item("action_start_date").ToString <> "" Then
                    rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("action_start_date").ToString).ToShortDateString
                End If

                If dtRow.Item("action_end_date").ToString <> "" Then
                    rpt_Row.Item("Column9") = eZeeDate.convertDate(dtRow.Item("action_end_date").ToString).ToShortDateString
                End If

                rpt_Row.Item("Column10") = dtRow.Item("status")
                rpt_Row.Item("Column11") = dtRow.Item("resolution_steps")
                rpt_Row.Item("Column12") = dtRow.Item("Remark")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptDisciplineAnalysisReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 3, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 4, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 5, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 6, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 7, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtDisciplineType", Language.getMessage(mstrModuleName, 8, "Discipline Type"))
            Call ReportFunction.TextChange(objRpt, "txtDisciplineAction", Language.getMessage(mstrModuleName, 9, "Discipline Action"))
            Call ReportFunction.TextChange(objRpt, "txtInvestigator", Language.getMessage(mstrModuleName, 10, "Investigator"))
            Call ReportFunction.TextChange(objRpt, "txtInvolvedPerson", Language.getMessage(mstrModuleName, 11, "Involved Person"))
            Call ReportFunction.TextChange(objRpt, "txtAgainst", Language.getMessage(mstrModuleName, 12, "Against"))
            Call ReportFunction.TextChange(objRpt, "txtIncidentCaused", Language.getMessage(mstrModuleName, 13, "Incident Caused"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 14, "Action St.Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 15, "Action End Date"))
            Call ReportFunction.TextChange(objRpt, "txtStaus", Language.getMessage(mstrModuleName, 16, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtResolutionStep", Language.getMessage(mstrModuleName, 17, "Resolution Step"))
            Call ReportFunction.TextChange(objRpt, "txtRemark", Language.getMessage(mstrModuleName, 18, "Remark"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 19, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 20, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 21, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Generate_DetailReport", mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Discipline Type")
			Language.setMessage(mstrModuleName, 2, "Discipline Action")
			Language.setMessage(mstrModuleName, 3, "Prepared By :")
			Language.setMessage(mstrModuleName, 4, "Checked By :")
			Language.setMessage(mstrModuleName, 5, "Approved By :")
			Language.setMessage(mstrModuleName, 6, "Received By :")
			Language.setMessage(mstrModuleName, 7, "Date")
			Language.setMessage(mstrModuleName, 8, "Discipline Type")
			Language.setMessage(mstrModuleName, 9, "Discipline Action")
			Language.setMessage(mstrModuleName, 10, "Investigator")
			Language.setMessage(mstrModuleName, 11, "Involved Person")
			Language.setMessage(mstrModuleName, 12, "Against")
			Language.setMessage(mstrModuleName, 13, "Incident Caused")
			Language.setMessage(mstrModuleName, 14, "Action St.Date")
			Language.setMessage(mstrModuleName, 15, "Action End Date")
			Language.setMessage(mstrModuleName, 16, "Status")
			Language.setMessage(mstrModuleName, 17, "Resolution Step")
			Language.setMessage(mstrModuleName, 18, "Remark")
			Language.setMessage(mstrModuleName, 19, "Printed By :")
			Language.setMessage(mstrModuleName, 20, "Printed Date :")
			Language.setMessage(mstrModuleName, 21, "Page :")
			Language.setMessage(mstrModuleName, 22, "Complete")
			Language.setMessage(mstrModuleName, 23, "In Complete")
			Language.setMessage(mstrModuleName, 24, "Date From :")
			Language.setMessage(mstrModuleName, 25, "To")
			Language.setMessage(mstrModuleName, 26, "Discipline Type :")
			Language.setMessage(mstrModuleName, 27, "Discipline Action :")
			Language.setMessage(mstrModuleName, 28, "Investigator :")
			Language.setMessage(mstrModuleName, 29, "Involved Person :")
			Language.setMessage(mstrModuleName, 30, "Against :")
			Language.setMessage(mstrModuleName, 31, "Stauts :")
			Language.setMessage(mstrModuleName, 32, "Order By :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
