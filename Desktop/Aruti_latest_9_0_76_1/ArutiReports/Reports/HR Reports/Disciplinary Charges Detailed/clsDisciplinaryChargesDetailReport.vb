﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsDisciplinaryChargesDetailReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDisciplinaryChargesDetailReport"
    Private mstrReportId As String = enArutiReport.Disciplinary_Charges_Detailed_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtChargeDateFrom As Date = Nothing
    Private mdtChargeDateTo As Date = Nothing
    Private mintOffenceCategoryId As Integer = 0
    Private mstrOffenceCategory As String = ""
    Private mintOffenceDescrId As Integer = 0
    Private mstrOffenceDescription As String = ""
    Private mintResponseTypeId As Integer = 0
    Private mstrResponseType As String = ""
    Private mdtResponseDateFrom As Date = Nothing
    Private mdtResponseDateTo As Date = Nothing
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnApplyAccessFilter As Boolean = True
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvance_Filter As String = ""
    Private mblnIncludeInactiveEmployee As Boolean = False
    Private mblnShowAllocationBasedOnChargeDate As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ChargeDateFrom() As Date
        Set(ByVal value As Date)
            mdtChargeDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ChargeDateTo() As Date
        Set(ByVal value As Date)
            mdtChargeDateTo = value
        End Set
    End Property

    Public WriteOnly Property _OffenceCategoryId() As Integer
        Set(ByVal value As Integer)
            mintOffenceCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _OffenceCategory() As String
        Set(ByVal value As String)
            mstrOffenceCategory = value
        End Set
    End Property

    Public WriteOnly Property _OffenceDescrId() As Integer
        Set(ByVal value As Integer)
            mintOffenceDescrId = value
        End Set
    End Property

    Public WriteOnly Property _OffenceDescription() As String
        Set(ByVal value As String)
            mstrOffenceDescription = value
        End Set
    End Property

    Public WriteOnly Property _ResponseTypeId() As Integer
        Set(ByVal value As Integer)
            mintResponseTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ResponseType() As String
        Set(ByVal value As String)
            mstrResponseType = value
        End Set
    End Property

    Public WriteOnly Property _ResponseDateFrom() As Date
        Set(ByVal value As Date)
            mdtResponseDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ResponseDateTo() As Date
        Set(ByVal value As Date)
            mdtResponseDateTo = value
        End Set
    End Property

    Public WriteOnly Property _ShowAllocationBasedOnChargeDate() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAllocationBasedOnChargeDate = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmployee() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmployee = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtChargeDateFrom = Nothing
            mdtChargeDateTo = Nothing
            mintOffenceCategoryId = 0
            mstrOffenceCategory = ""
            mintOffenceDescrId = 0
            mstrOffenceDescription = ""
            mintResponseTypeId = 0
            mstrResponseType = ""
            mdtResponseDateFrom = Nothing
            mdtResponseDateTo = Nothing

            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnApplyAccessFilter = True

            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrAdvance_Filter = ""
            mblnIncludeInactiveEmployee = False
            mblnShowAllocationBasedOnChargeDate = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EM.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Employee :") & " " & mstrEmployeeName & " "
            End If
            If mintOffenceCategoryId > 0 Then
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOffenceCategoryId)
                Me._FilterQuery &= " AND CM.masterunkid = @masterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Offence Category :") & " " & mstrOffenceCategory & " "
            End If
            If mintOffenceDescrId > 0 Then
                objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOffenceDescrId)
                Me._FilterQuery &= " AND DTY.disciplinetypeunkid = @disciplinetypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Offence :") & " " & mstrOffenceDescription & " "
            End If
            If mdtChargeDateFrom <> Nothing AndAlso mdtChargeDateTo <> Nothing Then
                objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtChargeDateFrom))
                objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtChargeDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),DFM.chargedate,112) BETWEEN @Date1 AND @Date2 "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, "Charge Date From :") & " " & mdtChargeDateFrom.Date.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 104, "To :") & " " & mdtChargeDateTo.Date.ToShortDateString & " "
            End If

            If mintResponseTypeId > 0 Then
                objDataOperation.AddParameter("@ResponseTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResponseTypeId)
                Me._FilterQuery &= " AND RT.masterunkid = @ResponseTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 105, "Response Type :") & " " & mstrResponseType & " "
            End If

            If mdtResponseDateFrom <> Nothing AndAlso mdtResponseDateTo <> Nothing Then
                objDataOperation.AddParameter("@ResDate1", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtResponseDateFrom))
                objDataOperation.AddParameter("@ResDate2", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtResponseDateTo))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),DFT.response_date,112) BETWEEN @ResDate1 AND @ResDate2 "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 106, "Response Date From :") & " " & mdtResponseDateFrom.Date.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 107, "Response Date To :") & " " & mdtResponseDateTo.Date.ToShortDateString & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            'Rpt = objRpt

            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean)
        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Dim exForce As Exception = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "EM")
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "EM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "EM")

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : OLD - 501
            'StrQ = "IF OBJECT_ID('tempdb..##usrnamejob') IS NOT NULL " & _
            '             "DROP TABLE ##usrnamejob " & _
            '        "CREATE TABLE ##usrnamejob ( " & _
            '              "ueunkid INT " & _
            '             ",uename NVARCHAR(max) " & _
            '             ",uejob NVARCHAR(max) " & _
            '             ") " & _
            '        "DECLARE @Ddate NVARCHAR(8),@EmpId INT,@DName NVARCHAR(MAX),@UId INT " & _
            '        "DECLARE posteduj CURSOR " & _
            '        "FOR " & _
            '        "SELECT DISTINCT DFM.userunkid " & _
            '             ",UM.employeeunkid " & _
            '             ",ISNULL(FY.database_name, '') AS dname " & _
            '             ",ISNULL(CF.key_value, '') AS efdt " & _
            '        "FROM hrdiscipline_file_master AS DFM " & _
            '        "JOIN hrmsConfiguration..cfuser_master AS UM ON DFM.userunkid = UM.userunkid " & _
            '        "LEFT JOIN hrmsConfiguration..cffinancial_year_tran FY ON FY.companyunkid = UM.companyunkid AND FY.isclosed = 0 " & _
            '        "LEFT JOIN hrmsConfiguration..cfconfiguration CF ON CF.companyunkid = UM.companyunkid AND UPPER(CF.key_name) = 'EMPLOYEEASONDATE' " & _
            '        "WHERE DFM.isvoid = 0 " & _
            '        "OPEN posteduj " & _
            '        "FETCH NEXT FROM posteduj INTO @UId,@EmpId,@DName,@Ddate " & _
            '        "WHILE @@FETCH_STATUS = 0 " & _
            '        "BEGIN " & _
            '             "DECLARE @Q AS NVARCHAR(MAX) " & _
            '             "SET @Q = 'INSERT INTO ##usrnamejob(ueunkid,uename,uejob) ' " & _
            '             "IF @EmpId <= 0 " & _
            '             "BEGIN " & _
            '                  "SET @Q = @Q + ' SELECT userunkid,username,'''' FROM hrmsConfiguration..cfuser_master WHERE userunkid = ' + CAST(@UId AS NVARCHAR(10)) " & _
            '             "END " & _
            '          "IF @EmpId > 0 " & _
            '          "BEGIN " & _
            '            "SET @Q = @Q + ' SELECT '''+CAST(@UId AS NVARCHAR(10))+''',EM.firstname + '' '' + EM.surname,ISNULL(EJM.job_name,'''') FROM hremployee_master AS EM " & _
            '                           "LEFT JOIN " & _
            '                            "( " & _
            '                              "SELECT " & _
            '                                "CT.jobunkid " & _
            '                               ",CT.employeeunkid " & _
            '                               ",ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
            '                               "FROM hremployee_categorization_tran AS CT " & _
            '                               "WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '''+@Ddate+''' " & _
            '                               "AND CT.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
            '                            ") AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
            '                            "LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
            '                            "WHERE 1 = 1 AND EM.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) " & _
            '          "END " & _
            '             "EXEC (@Q) " & _
            '             "FETCH NEXT FROM posteduj INTO @UId,@EmpId,@DName,@Ddate " & _
            '        "END " & _
            '        "CLOSE posteduj " & _
            '        "DEALLOCATE posteduj "


            'S.SANDEEP |02-MAR-2022| -- START

            'S.SANDEEP |02-MAR-2022| -- END
            'StrQ = "IF OBJECT_ID('tempdb..##usrnamejob') IS NOT NULL " & _
            '             "DROP TABLE ##usrnamejob " & _
            '        "CREATE TABLE ##usrnamejob ( " & _
            '              "ueunkid INT " & _
            '             ",uename NVARCHAR(max) " & _
            '             ",uejob NVARCHAR(max) " & _
            '             ",disciplinefileunkid INT " & _
            '             ") " & _
            '        "DECLARE @Ddate NVARCHAR(8),@EmpId INT,@DName NVARCHAR(MAX),@UId INT, @DFlId INT " & _
            '        "DECLARE posteduj CURSOR " & _
            '        "FOR " & _
            '        "SELECT DISTINCT ADFM.audituserunkid " & _
            '             ",UM.employeeunkid " & _
            '             ",ISNULL(FY.database_name, '') AS dname " & _
            '             ",ISNULL(CF.key_value, '') AS efdt " & _
            '             ",DFM.disciplinefileunkid " & _
            '        "FROM hrdiscipline_file_master AS DFM " & _
            '        "JOIN " & _
            '        "( " & _
            '              "SELECT " & _
            '                    "disciplinefileunkid " & _
            '                   ",audituserunkid " & _
            '              "FROM athrdiscipline_file_master " & _
            '              "WHERE audittype = 1 " & _
            '        ") AS ADFM ON ADFM.disciplinefileunkid = DFM.disciplinefileunkid " & _
            '        "JOIN hrmsConfiguration..cfuser_master AS UM ON ADFM.audituserunkid = UM.userunkid " & _
            '        "LEFT JOIN hrmsConfiguration..cffinancial_year_tran FY ON FY.companyunkid = UM.companyunkid AND FY.isclosed = 0 " & _
            '        "LEFT JOIN hrmsConfiguration..cfconfiguration CF ON CF.companyunkid = UM.companyunkid AND UPPER(CF.key_name) = 'EMPLOYEEASONDATE' " & _
            '        "WHERE DFM.isvoid = 0 " & _
            '        "OPEN posteduj " & _
            '        "FETCH NEXT FROM posteduj INTO @UId,@EmpId,@DName,@Ddate,@DFlId " & _
            '        "WHILE @@FETCH_STATUS = 0 " & _
            '        "BEGIN " & _
            '             "DECLARE @Q AS NVARCHAR(MAX) " & _
            '             "SET @Q = 'INSERT INTO ##usrnamejob(ueunkid,uename,uejob,disciplinefileunkid) ' " & _
            '             "IF @EmpId <= 0 " & _
            '             "BEGIN " & _
            '                  "SET @Q = @Q + ' SELECT userunkid,username,'''',''' + CAST(@DFlId AS NVARCHAR(MAX)) + ''' FROM hrmsConfiguration..cfuser_master WHERE userunkid = ' + CAST(@UId AS NVARCHAR(10)) " & _
            '             "END " & _
            '          "IF @EmpId > 0 " & _
            '          "BEGIN " & _
            '            "SET @Q = @Q + ' SELECT '''+CAST(@UId AS NVARCHAR(10))+''',EM.firstname + '' '' + EM.surname,ISNULL(EJM.job_name,''''), ''' + CAST(@DFlId AS NVARCHAR(MAX)) + ''' FROM hremployee_master AS EM " & _
            '                           "LEFT JOIN " & _
            '                            "( " & _
            '                              "SELECT " & _
            '                                "CT.jobunkid " & _
            '                               ",CT.employeeunkid " & _
            '                               ",ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
            '                               "FROM hremployee_categorization_tran AS CT " & _
            '                               "WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '''+@Ddate+''' " & _
            '                               "AND CT.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
            '                            ") AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
            '                            "LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
            '                            "WHERE 1 = 1 AND EM.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) " & _
            '          "END " & _
            '             "EXEC (@Q) " & _
            '             "FETCH NEXT FROM posteduj INTO @UId,@EmpId,@DName,@Ddate,@DFlId " & _
            '        "END " & _
            '        "CLOSE posteduj " & _
            '        "DEALLOCATE posteduj "
            StrQ = "IF OBJECT_ID('tempdb..##usrnamejob') IS NOT NULL " & _
                         "DROP TABLE ##usrnamejob " & _
                    "CREATE TABLE ##usrnamejob ( " & _
                          "ueunkid INT " & _
                         ",uename NVARCHAR(max) " & _
                         ",uejob NVARCHAR(max) " & _
                         ",disciplinefileunkid INT " & _
                         ") " & _
                    "DECLARE @Ddate NVARCHAR(8),@EmpId INT,@DName NVARCHAR(MAX),@UId INT, @DFlId INT " & _
                    "DECLARE posteduj CURSOR " & _
                    "FOR " & _
                    "SELECT DISTINCT " & _
                         " /*ADFM.audituserunkid */ " & _
                         " ISNULL(DFM.postuserunkid,0) " & _
                         ",ISNULL(UM.employeeunkid,0) " & _
                         ",ISNULL(FY.database_name, '') AS dname " & _
                         ",ISNULL(CF.key_value, '') AS efdt " & _
                         ",DFM.disciplinefileunkid " & _
                    "FROM hrdiscipline_file_master AS DFM " & _
                    "/*JOIN " & _
                    "( " & _
                          "SELECT " & _
                                "disciplinefileunkid " & _
                               ",audituserunkid " & _
                          "FROM athrdiscipline_file_master " & _
                          "WHERE audittype = 1 " & _
                    ") AS ADFM ON ADFM.disciplinefileunkid = DFM.disciplinefileunkid " & _
                    "JOIN hrmsConfiguration..cfuser_master AS UM ON ADFM.audituserunkid = UM.userunkid  */ " & _
                    "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON DFM.postuserunkid = UM.userunkid " & _
                    "LEFT JOIN hrmsConfiguration..cffinancial_year_tran FY ON FY.companyunkid = UM.companyunkid AND FY.isclosed = 0 " & _
                    "LEFT JOIN hrmsConfiguration..cfconfiguration CF ON CF.companyunkid = UM.companyunkid AND UPPER(CF.key_name) = 'EMPLOYEEASONDATE' " & _
                    "WHERE DFM.isvoid = 0 " & _
                    "OPEN posteduj " & _
                    "FETCH NEXT FROM posteduj INTO @UId,@EmpId,@DName,@Ddate,@DFlId " & _
                    "WHILE @@FETCH_STATUS = 0 " & _
                    "BEGIN " & _
                         "DECLARE @Q AS NVARCHAR(MAX) " & _
                         "SET @Q = 'INSERT INTO ##usrnamejob(ueunkid,uename,uejob,disciplinefileunkid) ' " & _
                         "IF @EmpId <= 0 " & _
                         "BEGIN " & _
                              "SET @Q = @Q + ' SELECT userunkid,username,'''',''' + CAST(@DFlId AS NVARCHAR(MAX)) + ''' FROM hrmsConfiguration..cfuser_master WHERE userunkid = ' + CAST(@UId AS NVARCHAR(10)) " & _
                         "END " & _
                      "IF @EmpId > 0 " & _
                      "BEGIN " & _
                        "SET @Q = @Q + ' SELECT '''+CAST(@UId AS NVARCHAR(10))+''',EM.firstname + '' '' + EM.surname,ISNULL(EJM.job_name,''''), ''' + CAST(@DFlId AS NVARCHAR(MAX)) + ''' FROM hremployee_master AS EM " & _
                                       "LEFT JOIN " & _
                                        "( " & _
                                          "SELECT " & _
                                            "CT.jobunkid " & _
                                           ",CT.employeeunkid " & _
                                           ",ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                                           "FROM hremployee_categorization_tran AS CT " & _
                                           "WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '''+@Ddate+''' " & _
                                           "AND CT.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                                        ") AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                                        "LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                                        "WHERE 1 = 1 AND EM.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) " & _
                      "END " & _
                         "EXEC (@Q) " & _
                         "FETCH NEXT FROM posteduj INTO @UId,@EmpId,@DName,@Ddate,@DFlId " & _
                    "END " & _
                    "CLOSE posteduj " & _
                    "DEALLOCATE posteduj "
            'S.SANDEEP |28-OCT-2021| -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mblnShowAllocationBasedOnChargeDate Then
                StrQ = "IF OBJECT_ID('tempdb..##Results') IS NOT NULL " & _
                       "DROP TABLE ##Results " & _
                       "CREATE TABLE ##Results " & _
                       "( " & _
                       "    empid int, " & _
                       "    edate nvarchar(8), " & _
                       "    dept nvarchar(max), " & _
                       "    unit nvarchar(max), " & _
                       "    ejob nvarchar(max) " & _
                       ") " & _
                       "DECLARE @Ddate  NVARCHAR(8), @EmpId INT " & _
                       "DECLARE discpl_alloc CURSOR " & _
                       "FOR " & _
                       "SELECT DISTINCT " & _
                       "     CONVERT(NVARCHAR(8),chargedate,112) AS efdate " & _
                       "    ,involved_employeeunkid as empid " & _
                       "FROM hrdiscipline_file_master " & _
                       "WHERE isvoid = 0 " & _
                       "OPEN discpl_alloc " & _
                       "FETCH NEXT FROM discpl_alloc INTO @Ddate,@EmpId " & _
                       "WHILE @@FETCH_STATUS = 0 " & _
                       "BEGIN " & _
                       "DECLARE @Q AS NVARCHAR(MAX) " & _
                       "SET @Q = 'INSERT INTO ##Results(empid,edate,dept,unit,ejob) " & _
                       "    SELECT " & _
                       "         EM.employeeunkid " & _
                       "        ,'''+@Ddate+''' " & _
                       "        ,ISNULL(EDP.name,'''') AS dept " & _
                       "        ,ISNULL(ECG.name,'''') as unit " & _
                       "        ,ISNULL(EJM.job_name,'''') as ejob " & _
                       "    FROM hremployee_master AS EM " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             CT.jobunkid " & _
                       "            ,CT.employeeunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran AS CT " & _
                       "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '''+@Ddate+''' " & _
                       "        AND CT.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                       "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                       "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ET.unitunkid " & _
                       "            ,ET.employeeunkid " & _
                       "            ,ET.departmentunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                       "        FROM hremployee_transfer_tran AS ET " & _
                       "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '''+@Ddate+''' " & _
                       "        AND ET.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                       "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                       "    LEFT JOIN hrdepartment_master AS EDP ON EC.departmentunkid = EDP.departmentunkid " & _
                       "    LEFT JOIN hrunit_master AS ECG ON EC.unitunkid = ECG.unitunkid " & _
                       "    WHERE EM.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) " & _
                       "    EXEC(@Q) " & _
                       "FETCH NEXT FROM discpl_alloc INTO @Ddate,@EmpId " & _
                       "END " & _
                       "CLOSE discpl_alloc " & _
                       "DEALLOCATE discpl_alloc "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            StrQ = "SELECT " & _
                   "     EM.employeecode " & _
                   "    ,EM.firstname + ' ' + EM.surname AS employee "
            If mblnShowAllocationBasedOnChargeDate Then
                StrQ &= "    ,##Results.dept " & _
                        "    ,##Results.unit " & _
                        "    ,##Results.ejob "
            Else
                StrQ &= "   ,ISNULL(EDP.name,'') AS dept " & _
                        "   ,ISNULL(ECG.name,'') as unit " & _
                        "   ,ISNULL(EJM.job_name,'') as ejob "
            End If
            StrQ &= "    ,ISNULL(CM.name,'') AS OffCategory " & _
                    "    ,DTY.name AS Offence " & _
                    "    ,DFT.incident_description " & _
                    "    ,CONVERT(NVARCHAR(8),DFM.chargedate,112) AS chargedate " & _
                    "    ,ISNULL(uename,'') AS cOffr " & _
                    "    ,ISNULL(uejob,'') AS cOffj " & _
                    "    ,DFM.reference_no " & _
                    "    ,ISNULL(CONVERT(NVARCHAR(8),DFT.response_date,112),'') AS response_date " & _
                    "    ,ISNULL(RT.name,'') AS rtype " & _
                    "    ,DFT.response_remark "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hrdiscipline_file_master AS DFM "

            If mblnShowAllocationBasedOnChargeDate Then
                StrQ &= "    JOIN ##Results ON ##Results.empid = DFM.involved_employeeunkid AND ##Results.edate = CONVERT(CHAR(8),DFM.chargedate,112) "
            End If

            'JOIN ##usrnamejob ON DFM.userunkid = ##usrnamejob.ueunkid '
            StrQ &= "    JOIN ##usrnamejob ON DFM.disciplinefileunkid = ##usrnamejob.disciplinefileunkid " & _
                    "    JOIN hremployee_master AS EM ON EM.employeeunkid = DFM.involved_employeeunkid "
            If mblnShowAllocationBasedOnChargeDate = False Then
                StrQ &= "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             CT.jobunkid " & _
                        "            ,CT.employeeunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                        "        FROM hremployee_categorization_tran AS CT " & _
                        "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                        "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             ET.unitunkid " & _
                        "            ,ET.employeeunkid " & _
                        "            ,ET.departmentunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                        "        FROM hremployee_transfer_tran AS ET " & _
                        "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                        "    LEFT JOIN hrdepartment_master AS EDP ON EC.departmentunkid = EDP.departmentunkid " & _
                        "    LEFT JOIN hrunit_master AS ECG ON EC.unitunkid = ECG.unitunkid "
            End If
            StrQ &= "    JOIN hrdiscipline_file_tran AS DFT ON DFM.disciplinefileunkid = DFT.disciplinefileunkid " & _
                    "    JOIN hrdisciplinetype_master AS DTY ON DTY.disciplinetypeunkid = DFT.offenceunkid " & _
                    "    LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = DTY.offencecategoryunkid " & _
                    "    LEFT JOIN cfcommon_master AS RT ON RT.masterunkid = DFT.responsetypeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= mstrAnalysis_Join

            StrQ &= "WHERE DFM.isvoid = 0 AND DFT.isvoid = 0 "

            If mblnIncludeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " ORDER BY DFM.reference_no "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim mdtTableExcel As DataTable = Nothing
            If mintViewIndex > 0 Then
                mdtTableExcel = New DataView(dsList.Tables(0), "", "GName,reference_no", DataViewRowState.CurrentRows).ToTable()
            Else
                mdtTableExcel = dsList.Tables(0)
            End If

            Dim dcHeader As New Dictionary(Of Integer, String)
            Dim intColumnIndex As Integer = -1

            If IsNothing(mdtTableExcel) = False AndAlso mdtTableExcel.Rows.Count > 0 Then
                For Each rows As DataRow In mdtTableExcel.Rows
                    If rows("chargedate").ToString.Trim.Length > 0 Then
                        rows("chargedate") = eZeeDate.convertDate(rows("chargedate")).ToShortDateString
                    End If
                    If rows("response_date").ToString.Trim.Length > 0 Then
                        rows("response_date") = eZeeDate.convertDate(rows("response_date")).ToShortDateString
                    End If
                Next
            End If

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If


            If mdtTableExcel.Columns.Contains("Id") Then
                mdtTableExcel.Columns.Remove("Id")
            End If

            If mintViewIndex <= 0 AndAlso mdtTableExcel.Columns.Contains("GName") Then
                mdtTableExcel.Columns.Remove("GName")
            End If


            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 300, "Charged Employee No.")
            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 301, "Charged Employee")

            mdtTableExcel.Columns("dept").Caption = Language.getMessage(mstrModuleName, 302, "Employee Division")
            mdtTableExcel.Columns("unit").Caption = Language.getMessage(mstrModuleName, 303, "Employee Station")
            mdtTableExcel.Columns("ejob").Caption = Language.getMessage(mstrModuleName, 304, "Charged Employee Job")

            mdtTableExcel.Columns("OffCategory").Caption = Language.getMessage(mstrModuleName, 211, "Offence Category")
            mdtTableExcel.Columns("Offence").Caption = Language.getMessage(mstrModuleName, 212, "Offence Committed")
            mdtTableExcel.Columns("incident_description").Caption = Language.getMessage(mstrModuleName, 213, "Facts of the Offence")
            mdtTableExcel.Columns("chargedate").Caption = Language.getMessage(mstrModuleName, 215, "Date Charged")
            mdtTableExcel.Columns("cOffr").Caption = Language.getMessage(mstrModuleName, 216, "Charging Officer")
            mdtTableExcel.Columns("cOffj").Caption = Language.getMessage(mstrModuleName, 216, "Charging Officer Job")
            mdtTableExcel.Columns("reference_no").Caption = Language.getMessage(mstrModuleName, 216, "Case Ref No.")
            mdtTableExcel.Columns("response_date").Caption = Language.getMessage(mstrModuleName, 216, "Response Date")
            mdtTableExcel.Columns("rtype").Caption = Language.getMessage(mstrModuleName, 216, "Response Type")
            mdtTableExcel.Columns("response_remark").Caption = Language.getMessage(mstrModuleName, 216, "Charged Employee Response")


            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)


            'row = New WorksheetRow()
            'Dim intCount As Integer = 0
            'Do While intCount <= mdtTableExcel.Columns.Count - 1
            '    wcell = New WorksheetCell("", "HeaderStyle")

            '    row.Cells.Add(wcell)
            '    intCount += 1
            'Loop
            'rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: Generate_DetailReport" & mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
    '                                       ByVal intUserUnkid As Integer, _
    '                                       ByVal intYearUnkid As Integer, _
    '                                       ByVal intCompanyUnkid As Integer, _
    '                                       ByVal dtPeriodStart As Date, _
    '                                       ByVal dtPeriodEnd As Date, _
    '                                       ByVal strUserModeSetting As String, _
    '                                       ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet

    '    Dim exForce As Exception = Nothing
    '    Try
    '        objDataOperation = New clsDataOperation

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "EM")
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "EM")
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "EM")


    '        StrQ = "SELECT " & _
    '               "     EM.firstname+' '+EM.surname AS employee " & _
    '               "    ,ECG.name as clsgrp " & _
    '               "    ,ECL.name as cls " & _
    '               "    ,EJM.job_name as ejob " & _
    '               "    ,CM.name as offcat " & _
    '               "    ,DS.name as offdes " & _
    '               "    ,CONVERT(CHAR(8),FM.chargedate,112) AS chargedate " & _
    '               "    ,FM.reference_no "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If
    '        StrQ &= "FROM hrdiscipline_file_master AS FM " & _
    '                "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
    '                "    LEFT JOIN " & _
    '                "    ( " & _
    '                "        SELECT " & _
    '                "             CT.jobunkid " & _
    '                "            ,CT.employeeunkid " & _
    '                "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
    '                "        FROM hremployee_categorization_tran AS CT " & _
    '                "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
    '                "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
    '                "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
    '                "    LEFT JOIN " & _
    '                "    ( " & _
    '                "        SELECT " & _
    '                "             ET.classgroupunkid " & _
    '                "            ,ET.classunkid " & _
    '                "            ,ET.employeeunkid " & _
    '                "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
    '                "        FROM hremployee_transfer_tran AS ET " & _
    '                "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
    '                "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
    '                "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid " & _
    '                "    LEFT JOIN hrclasses_master AS ECL ON EC.classunkid = ECL.classesunkid " & _
    '                "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
    '                "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
    '                "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid "

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE FM.isvoid = 0 AND FT.isvoid = 0 "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= xDateFilterQry & " "
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= " ORDER BY FM.reference_no "

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Dim dtFinalTable As DataTable
    '        'Dim dtCol As DataColumn
    '        'Dim mdtTableExcel As New DataTable
    '        'dtFinalTable = New DataTable("Discipline")

    '        'dtCol = New DataColumn("ChargedEmployeeNo", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Charged Employee No.")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("ChargedEmployee", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Charged Employee")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("ChargedEmployeeJob", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Charged Employee Job")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("EmployeeDivision", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 4, "ReportEmployee Division")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("EmployeeStation", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Employee Station")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("OffenceCommitted", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Offence Committed")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("FactsoftheOffence", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Facts of the Offence")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("DateCharged", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Date Charged")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("ChargingOfficer", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Charging Officer")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)

    '        'dtCol = New DataColumn("CaseRefNo", System.Type.GetType("System.String"))
    '        'dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Case Ref No.")
    '        'dtCol.DefaultValue = ""
    '        'dtFinalTable.Columns.Add(dtCol)


    '        'Dim strCurrentEmp As String = ""
    '        'Dim strPreviousEmp As String = ""
    '        'Dim rpt_Row As DataRow = Nothing
    '        'Dim drRow As DataRow
    '        'Dim iCnt As Integer = 0
    '        'Dim intRowCount As Integer = dsList.Tables("DataTable").Rows.Count

    '        'For ii As Integer = 0 To intRowCount - 1
    '        '    drRow = dsList.Tables("DataTable").Rows(ii)


    '        '    rpt_Row = dtFinalTable.NewRow

    '        '    rpt_Row.Item("EmployeeCode") = drRow.Item("employeecode")
    '        '    rpt_Row.Item("Name") = drRow.Item("employeename")
    '        '    rpt_Row.Item("Function") = drRow.Item("EmployeeDepartment")
    '        '    rpt_Row.Item("Department") = drRow.Item("EmployeeSectionGrp")
    '        '    rpt_Row.Item("Zone") = drRow.Item("EmployeeClassGrp")
    '        '    rpt_Row.Item("Branch") = drRow.Item("EmployeeClass")
    '        '    rpt_Row.Item("Title") = drRow.Item("EmployeeJob")
    '        '    rpt_Row.Item("Level") = drRow.Item("EmployeeGraderGrp")
    '        '    rpt_Row.Item("ExitDate") = CDate(drRow.Item("ExitDate")).ToShortDateString
    '        '    rpt_Row.Item("Reason") = drRow.Item("Reason")

    '        '    dtFinalTable.Rows.Add(rpt_Row)
    '        'Next

    '        'dtFinalTable.AcceptChanges()

    '        'mdtTableExcel = dtFinalTable

    '        'ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        'Dim strarrGroupColumns As String() = Nothing
    '        'Dim rowsArrayHeader As New ArrayList
    '        'Dim rowsArrayFooter As New ArrayList
    '        'Dim row As WorksheetRow
    '        'Dim wcell As WorksheetCell

    '        'Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
    '        'mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

    '        'row = New WorksheetRow()

    '        'wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
    '        'rowsArrayHeader.Add(row)
    '        'row = New WorksheetRow()

    '        'If Me._FilterTitle.ToString.Length > 0 Then
    '        '    wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
    '        '    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
    '        '    row.Cells.Add(wcell)
    '        'End If
    '        'rowsArrayHeader.Add(row)

    '        'row = New WorksheetRow()
    '        'wcell = New WorksheetCell("", "s10bw")
    '        'row.Cells.Add(wcell)
    '        'rowsArrayHeader.Add(row)


    '        'row = New WorksheetRow()
    '        'wcell = New WorksheetCell("", "s10bw")
    '        'row.Cells.Add(wcell)
    '        'rowsArrayFooter.Add(row)

    '        'If ConfigParameter._Object._IsShowPreparedBy = True Then
    '        '    row = New WorksheetRow()

    '        '    Dim objUser As New clsUserAddEdit
    '        '    objUser._Userunkid = xUserUnkid
    '        '    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
    '        '    wcell.MergeAcross = 4
    '        '    row.Cells.Add(wcell)
    '        '    objUser = Nothing

    '        '    wcell = New WorksheetCell("", "s10bw")
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)

    '        '    row = New WorksheetRow()
    '        '    wcell = New WorksheetCell("", "s10bw")
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)
    '        'End If


    '        'If ConfigParameter._Object._IsShowCheckedBy = True Then
    '        '    row = New WorksheetRow()
    '        '    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
    '        '    wcell.MergeAcross = 4
    '        '    row.Cells.Add(wcell)

    '        '    wcell = New WorksheetCell("", "s10bw")
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)

    '        '    row = New WorksheetRow()
    '        '    wcell = New WorksheetCell("", "s10bw")
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)
    '        'End If


    '        'If ConfigParameter._Object._IsShowApprovedBy = True Then
    '        '    row = New WorksheetRow()
    '        '    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
    '        '    wcell.MergeAcross = 4
    '        '    row.Cells.Add(wcell)

    '        '    wcell = New WorksheetCell("", "s10bw")
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)

    '        '    row = New WorksheetRow()
    '        '    wcell = New WorksheetCell("", "s10bw")
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)
    '        'End If

    '        'If ConfigParameter._Object._IsShowReceivedBy = True Then
    '        '    row = New WorksheetRow()
    '        '    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
    '        '    wcell.MergeAcross = 4
    '        '    row.Cells.Add(wcell)
    '        '    rowsArrayFooter.Add(row)
    '        'End If

    '        ''SET EXCEL CELL WIDTH  
    '        'Dim intArrayColumnWidth As Integer() = Nothing
    '        'ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
    '        'For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '        '    intArrayColumnWidth(i) = 125
    '        'Next
    '        ''SET EXCEL CELL WIDTH


    '        'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", False, rowsArrayHeader)


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return objRpt
    'End Function

#End Region

End Class
