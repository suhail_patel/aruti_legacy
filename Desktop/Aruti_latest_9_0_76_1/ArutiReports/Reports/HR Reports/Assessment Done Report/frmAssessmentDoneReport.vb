'************************************************************************************************************************************
'Class Name : frmAssessmentDoneReport.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 05-Oct-2017
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentDoneReport

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentDoneReport"
    Private objAssessmetDone As clsAssessmentDoneReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing

#End Region

#Region " Constructor "

    Public Sub New()
        objAssessmetDone = New clsAssessmentDoneReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAssessmetDone.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing : ObjPeriod = Nothing : dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            chkAssessorAssessment.Checked = False
            chkReviewerAssessment.Checked = False
            chkSelfAssessment.Checked = False
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            mdtStartDate = Nothing
            mdtEndDate = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAssessmetDone.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select period to continue"), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            'If chkSelfAssessment.Checked = False AndAlso _
            '   chkAssessorAssessment.Checked = False AndAlso _
            '   chkReviewerAssessment.Checked = False Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Assessment Done is mandatory information. Please check atleast one of the assessment done to continue."), enMsgBoxStyle.Information)
            'End If

            objAssessmetDone._EmployeeId = cboEmployee.SelectedValue
            objAssessmetDone._EmplyeeName = cboEmployee.Text
            objAssessmetDone._IncludeAssessorAssessment = chkAssessorAssessment.Checked
            objAssessmetDone._IncludeReviewerAssessment = chkReviewerAssessment.Checked
            objAssessmetDone._IncludeSelfAssessment = chkSelfAssessment.Checked
            objAssessmetDone._ReviewerAssessmentCaption = chkReviewerAssessment.Text
            objAssessmetDone._SelfAssessmentCaption = chkSelfAssessment.Text
            objAssessmetDone._AssessorAssessmentCaption = chkAssessorAssessment.Text
            objAssessmetDone._PeriodId = CInt(cboPeriod.SelectedValue)
            objAssessmetDone._PeriodName = cboPeriod.Text
            objAssessmetDone._ViewByIds = mstrStringIds
            objAssessmetDone._ViewIndex = mintViewIdx
            objAssessmetDone._ViewByName = mstrStringName
            objAssessmetDone._Analysis_Fields = mstrAnalysis_Fields
            objAssessmetDone._Analysis_Join = mstrAnalysis_Join
            objAssessmetDone._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAssessmetDone._Report_GroupName = mstrReport_GroupName
            objAssessmetDone._Advance_Filter = mstrAdvanceFilter

            If CInt(cboPeriod.SelectedValue) > 0 Then
                If chkConsiderEmployeeTermination.Checked = True Then
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    mdtStartDate = objPrd._Start_Date
                    mdtEndDate = objPrd._End_Date
                    objPrd = Nothing
                Else
                    mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
            Else
                mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Events "

    Private Sub frmAssessmentDoneReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAssessmetDone = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAssessmentDoneReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentDoneReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objAssessmetDone._ReportName
            Me._Message = objAssessmetDone._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentDoneReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentDoneReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessmentDoneReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessmentDoneReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentDoneReport_Language_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    If chkConsiderEmployeeTermination.Checked = True Then
            '        Dim objPrd As New clscommom_period_Tran
            '        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            '        mdtStartDate = objPrd._Start_Date
            '        mdtEndDate = objPrd._End_Date
            '        objPrd = Nothing
            '    Else
            '        mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            '        mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            '    End If
            'Else
            '    mdtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            '    mdtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            objAssessmetDone.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             mdtStartDate, _
                                             mdtEndDate, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objAssessmetDone.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             mdtStartDate, _
                                             mdtEndDate, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkConsiderEmployeeTermination.Text = Language._Object.getCaption(Me.chkConsiderEmployeeTermination.Name, Me.chkConsiderEmployeeTermination.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.chkReviewerAssessment.Text = Language._Object.getCaption(Me.chkReviewerAssessment.Name, Me.chkReviewerAssessment.Text)
            Me.chkAssessorAssessment.Text = Language._Object.getCaption(Me.chkAssessorAssessment.Name, Me.chkAssessorAssessment.Text)
            Me.chkSelfAssessment.Text = Language._Object.getCaption(Me.chkSelfAssessment.Name, Me.chkSelfAssessment.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
    
End Class
