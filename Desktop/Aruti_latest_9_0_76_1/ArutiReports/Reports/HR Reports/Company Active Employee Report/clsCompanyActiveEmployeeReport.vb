'************************************************************************************************************************************
'Class Name : clsCompanyActiveEmployeeReport.vb
'Purpose    :
'Date       :01/05/2013
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsCompanyActiveEmployeeReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCompanyActiveEmployeeReport"
    'Private mstrReportId As String = CStr(enArutiReport.CompanyActiveEmployeeReport)
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        'Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintCompanyUnkid As Integer = -1
    Private mstrCompany As String = ""
    Private mdtFromdate As Date = Nothing
    Private mdtTodate As Date = Nothing
    Private mintUserUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Company() As String
        Set(ByVal value As String)
            mstrCompany = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCompanyUnkid = -1
            mstrCompany = ""
            mdtFromdate = Nothing
            mdtTodate = Nothing
            mstrAdvance_Filter = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintCompanyUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Company :") & " " & mstrCompany & " "
            End If

            'Sohail (04 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "From Date :") & " " & mdtFromdate.ToShortDateString & " "
            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "To Date :") & " " & mdtTodate.ToShortDateString & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "As On Date :") & " " & mdtFromdate.ToShortDateString & " "
            'Sohail (04 Jun 2013) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (15 Feb 2016) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Feb 2016) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkID As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As DateTime _
                                           , ByVal xPeriodEnd As DateTime _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQDetail As String = ""
        Dim StrQSummary As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Summary As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Dim dtDetailData As DataTable = Nothing
        Dim dtSummaryData As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim dtTable As DataTable = Nothing
            Dim objCompany As New clsCompany_Master
            dtTable = objCompany.Get_DataBaseList("Database", False)

            Dim strFilter As String = "isclosed = 0 AND IsGrp = 0"

            If mintCompanyUnkid > 0 Then
                strFilter &= " AND GrpId = " & mintCompanyUnkid
            End If

            dtTable = New DataView(dtTable, strFilter, "Database ASC", DataViewRowState.CurrentRows).ToTable


            For Each dtRow As DataRow In dtTable.Rows
                objCompany._Companyunkid = CInt(dtRow.Item("GrpId"))



                'Pinkal (21-AUG-2014) -- Start
                'Enhancement - IN VOLTAMP,IT WAS DISPLAYING WRONG ACTIVE EMPLOYEE COUNT.IT DISPLAYED TERMINETED AND EOC DATE EMPLOYEE INCLUDING ACTIVE EMPLOYEE INSTEAD OF DISPLAY ONLY ACTIVE EMPLOYEE. IT WAS  DUR TO LOGIC SET FOR PAYROLL THAT IN ONE MONTH EMPLOYEES CANNOT BE PROCESSED MORE THAN LICENSE BOUGHT.

                'Sohail (04 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'Sohail (15 Feb 2016) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQDetail = "SELECT " & CInt(dtRow.Item("GrpId")) & " AS CompanyId,'" & objCompany._Name & "' AS Company " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.employeeunkid AS Employeeunkid " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.employeecode AS Employeecode " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.firstname + ' ' + " & dtRow.Item("Database").ToString.Trim & "..hremployee_master.othername + ' ' +  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master.surname AS Employee " & _
                '           ",ISNULL(Dept.name,'') AS Department " & _
                '           ",ISNULL(Job.job_name,'') AS Job " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.appointeddate As AppointmentDate " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.termination_from_date As LeavingDate " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.termination_to_date As RetirementDate " & _
                '           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.empl_enddate As EOCDate " & _
                '           "FROM  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master " & _
                '           "JOIN " & dtRow.Item("Database").ToString.Trim & "..hrjob_master AS Job ON hremployee_master.jobunkid = Job.jobunkid " & _
                '           "JOIN " & dtRow.Item("Database").ToString.Trim & "..hrdepartment_master AS Dept ON hremployee_master.departmentunkid = Dept.departmentunkid " & _
                '           "WHERE   1 = 1 " & _
                '           "AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                '           "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
                '           "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
                '           "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , dtRow.Item("Database").ToString.Trim)
                'S.SANDEEP [15 NOV 2016] -- START
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, dtRow.Item("Database").ToString.Trim, xUserUnkid, CInt(dtRow.Item("GrpId")), CInt(dtRow.Item("YearId")), xUserModeSetting)
                'S.SANDEEP [15 NOV 2016] -- END
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, dtRow.Item("Database").ToString.Trim)

                StrQDetail = "SELECT " & CInt(dtRow.Item("GrpId")) & " AS CompanyId,'" & objCompany._Name & "' AS Company " & _
                           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.employeeunkid AS Employeeunkid " & _
                           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.employeecode AS Employeecode " & _
                           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.firstname + ' ' + " & dtRow.Item("Database").ToString.Trim & "..hremployee_master.othername + ' ' +  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master.surname AS Employee " & _
                           ",ISNULL(Dept.name,'') AS Department " & _
                           ",ISNULL(Job.job_name,'') AS Job " & _
                           "," & dtRow.Item("Database").ToString.Trim & "..hremployee_master.appointeddate As AppointmentDate " & _
                                   ", ETERM.termination_from_date As LeavingDate " & _
                                   ", ERET.termination_to_date As RetirementDate " & _
                                   ", ETERM.empl_enddate As EOCDate " & _
                           "FROM   " & dtRow.Item("Database").ToString.Trim & "..hremployee_master "

                StrQDetail &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & dtRow.Item("Database").ToString.Trim & "..hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         jobgroupunkid " & _
                            "        ,jobunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM " & dtRow.Item("Database").ToString.Trim & "..hremployee_categorization_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "		 TERM.TEEmpId " & _
                            "		,TERM.empl_enddate " & _
                            "		,TERM.termination_from_date " & _
                            "		,TERM.TEfDt " & _
                            "		,TERM.isexclude_payroll " & _
                            "		,TERM.TR_REASON " & _
                            "		,TERM.changereasonunkid " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            TRM.employeeunkid AS TEEmpId " & _
                            "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                            "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                            "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                            "           ,TRM.isexclude_payroll " & _
                            "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                            "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                            "           ,TRM.changereasonunkid " & _
                            "       FROM " & dtRow.Item("Database").ToString.Trim & "..hremployee_dates_tran AS TRM " & _
                            "			LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "			LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                            "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            "   ) AS TERM WHERE TERM.Rno = 1 " & _
                            ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "		 RET.REmpId " & _
                            "		,RET.termination_to_date " & _
                            "		,RET.REfDt " & _
                            "		,RET.RET_REASON " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            RTD.employeeunkid AS REmpId " & _
                            "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                            "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                            "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                            "       FROM " & dtRow.Item("Database").ToString.Trim & "..hremployee_dates_tran AS RTD " & _
                            "			LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "			LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                            "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            "   ) AS RET WHERE RET.Rno = 1 " & _
                            ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) "

                StrQDetail &= "      LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..hrjob_master AS Job ON J.jobunkid = Job.jobunkid " & _
                                   " LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..hrdepartment_master AS Dept ON T.departmentunkid = Dept.departmentunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQDetail &= xDateJoinQry
                End If

                StrQDetail &= "WHERE   1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDetail &= xDateFilterQry
                End If
                'Sohail ((15 Feb 2016) -- End

                'StrQDetail = "SELECT " & CInt(dtRow.Item("GrpId")) & " AS CompanyId,'" & objCompany._Name & "' AS Company " & _
                '          ",A.employeeunkid AS Employeeunkid " & _
                '          ",A.employeecode AS Employeecode " & _
                '          ",A.firstname + ' ' + A.othername + ' ' +  A.surname AS Employee " & _
                '           ",ISNULL(Dept.name,'') AS Department " & _
                '           ",ISNULL(Job.job_name,'') AS Job " & _
                '          ",A.appointeddate As AppointmentDate " & _
                '          ",A.termination_from_date As LeavingDate " & _
                '          ",A.termination_to_date As RetirementDate " & _
                '          ",A.empl_enddate As EOCDate " & _
                '          "FROM  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS A " & _
                '          "JOIN " & dtRow.Item("Database").ToString.Trim & "..hrjob_master AS Job ON A.jobunkid = Job.jobunkid " & _
                '          "JOIN " & dtRow.Item("Database").ToString.Trim & "..hrdepartment_master AS Dept ON A.departmentunkid = Dept.departmentunkid " & _
                '          "LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS B ON A.employeeunkid = B.employeeunkid " & _
                '                "AND ISNULL(CONVERT(CHAR(8), B.termination_from_date, 112), @startdate) <= @startdate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), B.termination_to_date, 112), @startdate) <= @startdate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), B.empl_enddate, 112), @startdate) <= @startdate " & _
                '           "WHERE   1 = 1 " & _
                '        "AND    B.employeeunkid IS NULL "
                'Sohail (04 Jun 2013) -- End

                'Pinkal (21-AUG-2014) -- End

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromdate))
                'Pinkal (21-AUG-2014) -- Start
                'Enhancement - IN VOLTAMP,IT WAS DISPLAYING WRONG ACTIVE EMPLOYEE COUNT.IT DISPLAYED TERMINETED AND EOC DATE EMPLOYEE INCLUDING ACTIVE EMPLOYEE INSTEAD OF DISPLAY ONLY ACTIVE EMPLOYEE. IT WAS  DUR TO LOGIC SET FOR PAYROLL THAT IN ONE MONTH EMPLOYEES CANNOT BE PROCESSED MORE THAN LICENSE BOUGHT.
                'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTodate)) 'Sohail (04 Jun 2013)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTodate))
                'Pinkal (21-AUG-2014) -- End
                dsList = objDataOperation.ExecQuery(StrQDetail, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dtDetailData Is Nothing Then dtDetailData = dsList.Tables(0).Clone

                If dsList.Tables("List").Rows.Count > 0 Then

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        dtDetailData.ImportRow(dr)
                    Next

                End If

                dsList = Nothing

                'Pinkal (21-AUG-2014) -- Start
                'Enhancement - IN VOLTAMP,IT WAS DISPLAYING WRONG ACTIVE EMPLOYEE COUNT.IT DISPLAYED TERMINETED AND EOC DATE EMPLOYEE INCLUDING ACTIVE EMPLOYEE INSTEAD OF DISPLAY ONLY ACTIVE EMPLOYEE. IT WAS  DUR TO LOGIC SET FOR PAYROLL THAT IN ONE MONTH EMPLOYEES CANNOT BE PROCESSED MORE THAN LICENSE BOUGHT.

                'StrQSummary = "SELECT " & CInt(dtRow.Item("GrpId")) & " AS CompanyId,'" & objCompany._Name & "' AS Company " & _
                '                ",Count (A.employeeunkid ) As EmpCount " & _
                '                "FROM  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS A " & _
                '                " LEFT JOIN " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS B ON A.employeeunkid = B.employeeunkid " & _
                '                         "AND ISNULL(CONVERT(CHAR(8), B.termination_from_date, 112), @startdate) <= @startdate " & _
                '                         "AND ISNULL(CONVERT(CHAR(8), B.termination_to_date, 112), @startdate) <= @startdate " & _
                '                         "AND ISNULL(CONVERT(CHAR(8), B.empl_enddate, 112), @startdate) <= @startdate " & _
                '                "WHERE   1 = 1 " & _
                '                 "AND    B.employeeunkid IS NULL "
                'Sohail (15 Feb 2016) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQSummary = "SELECT " & CInt(dtRow.Item("GrpId")) & " AS CompanyId,'" & objCompany._Name & "' AS Company " & _
                '                       ",Count (A.employeeunkid ) As EmpCount " & _
                '                       "FROM  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS A " & _
                '                       "JOIN " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS B ON A.employeeunkid = B.employeeunkid " & _
                '                                "AND CONVERT(CHAR(8),A.appointeddate,112) <= @enddate " & _
                '                                "AND ISNULL(CONVERT(CHAR(8), B.termination_from_date, 112), @startdate) >= @startdate " & _
                '                                "AND ISNULL(CONVERT(CHAR(8), B.termination_to_date, 112), @startdate) >= @startdate " & _
                '                                "AND ISNULL(CONVERT(CHAR(8), B.empl_enddate, 112), @startdate) >= @startdate " & _
                '                       "WHERE   1 = 1 "
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , dtRow.Item("Database").ToString.Trim, "A")
                'S.SANDEEP [15 NOV 2016] -- START
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, dtRow.Item("Database").ToString.Trim, xUserUnkid, CInt(dtRow.Item("GrpId")), CInt(dtRow.Item("YearId")), xUserModeSetting, "A")
                'S.SANDEEP [15 NOV 2016] -- END
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, dtRow.Item("Database").ToString.Trim, "A")

                StrQSummary = "SELECT " & CInt(dtRow.Item("GrpId")) & " AS CompanyId,'" & objCompany._Name & "' AS Company " & _
                                       ",Count (A.employeeunkid ) As EmpCount " & _
                                       "FROM  " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS A " & _
                                       "JOIN " & dtRow.Item("Database").ToString.Trim & "..hremployee_master AS B ON A.employeeunkid = B.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQSummary &= xDateJoinQry
                End If

                StrQSummary &= "WHERE   1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQSummary &= xDateFilterQry
                End If
                'Sohail ((15 Feb 2016) -- End
                'Pinkal (21-AUG-2014) -- Start

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromdate))
                'Pinkal (21-AUG-2014) -- Start
                'Enhancement - IN VOLTAMP,IT WAS DISPLAYING WRONG ACTIVE EMPLOYEE COUNT.IT DISPLAYED TERMINETED AND EOC DATE EMPLOYEE INCLUDING ACTIVE EMPLOYEE INSTEAD OF DISPLAY ONLY ACTIVE EMPLOYEE. IT WAS  DUR TO LOGIC SET FOR PAYROLL THAT IN ONE MONTH EMPLOYEES CANNOT BE PROCESSED MORE THAN LICENSE BOUGHT.
                'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTodate)) 'Sohail (04 Jun 2013)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTodate))
                'Pinkal (21-AUG-2014) -- End
                dsList = objDataOperation.ExecQuery(StrQSummary, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dtSummaryData Is Nothing Then dtSummaryData = dsList.Tables(0).Clone

                If dsList.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In dsList.Tables(0).Rows
                        dtSummaryData.ImportRow(dr)
                    Next
                End If

            Next

            If dtSummaryData IsNot Nothing AndAlso dtSummaryData.Rows.Count > 0 Then
                dtSummaryData = New DataView(dtSummaryData, "1=1", "Company ASC", DataViewRowState.CurrentRows).ToTable()
            End If

            FilterTitleAndFilterQuery()

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dtDetailData.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("Company")
                rpt_Row.Item("Column2") = dtRow.Item("Employeecode")
                rpt_Row.Item("Column3") = dtRow.Item("Employee")
                rpt_Row.Item("Column4") = dtRow.Item("Department")
                rpt_Row.Item("Column5") = dtRow.Item("Job")

                If Not IsDBNull(dtRow.Item("AppointmentDate")) Then
                    rpt_Row.Item("Column6") = CDate(dtRow.Item("AppointmentDate")).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("LeavingDate")) Then
                    rpt_Row.Item("Column7") = CDate(eZeeDate.convertDate(dtRow.Item("LeavingDate").ToString)).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("RetirementDate")) Then
                    rpt_Row.Item("Column8") = CDate(eZeeDate.convertDate(dtRow.Item("RetirementDate").ToString)).ToShortDateString
                End If

                If Not IsDBNull(dtRow.Item("EOCDate")) Then
                    rpt_Row.Item("Column9") = CDate(eZeeDate.convertDate(dtRow.Item("EOCDate").ToString)).ToShortDateString
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            rpt_Summary = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dtSummaryData.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Summary.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Company")
                rpt_Rows.Item("Column81") = dtRow.Item("EmpCount")
                rpt_Summary.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptCompanyActiveEmployee

            'ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription")

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If
            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)


            objRpt.SetDataSource(rpt_Data)

            objRpt.Subreports("rpt_CompanyActiveEmployee_Summary").SetDataSource(rpt_Summary)
            ReportFunction.TextChange(objRpt.Subreports("rpt_CompanyActiveEmployee_Summary"), "txtCompany", Language.getMessage(mstrModuleName, 20, "Company"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_CompanyActiveEmployee_Summary"), "txtTotalEmployee", Language.getMessage(mstrModuleName, 21, "Active Employee"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_CompanyActiveEmployee_Summary"), "txtSummary", Language.getMessage(mstrModuleName, 22, "Summary"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_CompanyActiveEmployee_Summary"), "txtActiveEmployee", Language.getMessage(mstrModuleName, 23, "Total Active Employee"))

            ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 5, "Company : "))
            ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 6, "Employee Code"))
            ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 7, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 8, "Department"))
            ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 9, "Job"))

            Call ReportFunction.TextChange(objRpt, "txtAppointmentDate", Language.getMessage(mstrModuleName, 16, "Appointment Date"))
            Call ReportFunction.TextChange(objRpt, "txtLeavingDate", Language.getMessage(mstrModuleName, 17, "Leaving Date"))
            Call ReportFunction.TextChange(objRpt, "txtRetirement", Language.getMessage(mstrModuleName, 18, "Retirement Date"))
            Call ReportFunction.TextChange(objRpt, "txtEOCDate", Language.getMessage(mstrModuleName, 19, "E.O.C Date"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 10, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 11, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Dim objGroup As New clsGroup_Master
            Dim dtList As DataSet = objGroup.GetList("Group")
            If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtCompanyName", dtList.Tables(0).Rows(0)("groupname").ToString())
            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 5, "Company :")
            Language.setMessage(mstrModuleName, 6, "Employee Code")
            Language.setMessage(mstrModuleName, 7, "Employee Name")
            Language.setMessage(mstrModuleName, 8, "Department")
            Language.setMessage(mstrModuleName, 9, "Job")
            Language.setMessage(mstrModuleName, 10, "Sub Total :")
            Language.setMessage(mstrModuleName, 11, "Grand Total :")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "As On Date :")
            Language.setMessage(mstrModuleName, 16, "Appointment Date")
            Language.setMessage(mstrModuleName, 17, "Leaving Date")
            Language.setMessage(mstrModuleName, 18, "Retirement Date")
            Language.setMessage(mstrModuleName, 19, "E.O.C Date")
            Language.setMessage(mstrModuleName, 20, "Company")
            Language.setMessage(mstrModuleName, 21, "Active Employee")
            Language.setMessage(mstrModuleName, 22, "Summary")
            Language.setMessage(mstrModuleName, 23, "Total Active Employee")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

