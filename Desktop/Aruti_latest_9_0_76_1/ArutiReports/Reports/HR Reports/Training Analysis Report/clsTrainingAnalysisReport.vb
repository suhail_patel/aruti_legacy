'************************************************************************************************************************************
'Class Name : clsTrainingAnalysisReport.vb
'Purpose    :
'Date       :14/02/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsTrainingAnalysisReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingAnalysisReport"
    Private mstrReportId As String = enArutiReport.TrainingAnalysisReport   '35
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintTrainingId As Integer = 0
    Private mstrTrainingName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintTrainerId As Integer = 0
    Private mstrTrainerName As String = String.Empty
    Private mintResultId As Integer = 0
    Private mstrResultName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "
    Public WriteOnly Property _TrainingId() As Integer
        Set(ByVal value As Integer)
            mintTrainingId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _TrainerId() As Integer
        Set(ByVal value As Integer)
            mintTrainerId = value
        End Set
    End Property

    Public WriteOnly Property _TrainerName() As String
        Set(ByVal value As String)
            mstrTrainerName = value
        End Set
    End Property

    Public WriteOnly Property _ResultId() As Integer
        Set(ByVal value As Integer)
            mintResultId = value
        End Set
    End Property

    Public WriteOnly Property _ResultName() As String
        Set(ByVal value As String)
            mstrResultName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintTrainingId = 0
            mstrTrainingName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintTrainerId = 0
            mstrTrainerName = ""
            mintResultId = 0
            mstrResultName = ""
            mintStatusId = 0
            mstrStatusName = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Level"))
            objDataOperation.AddParameter("@Incomplete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Incomplete"))
            objDataOperation.AddParameter("@Complete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Complete"))

            If mintTrainingId > 0 Then
                objDataOperation.AddParameter("@TrainingId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingId)
                Me._FilterQuery &= " AND TrainingId = @TrainingId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Course Title :") & " " & mstrTrainingName & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & mstrEmployeeName & " "
            End If

            'If mintTrainerId > 0 Then
            '    objDataOperation.AddParameter("@TrainerId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainerId)
            '    Me._FilterQuery &= ""
            '    Me._FilterTitle &= ""
            'End If

            'If mstrTrainerName.Trim <> "" Then
            '    objDataOperation.AddParameter("@TrainerName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrTrainerName & "%")
            '    Me._FilterQuery &= ""
            '    Me._FilterTitle &= ""
            'End If

            If mintResultId > 0 Then
                objDataOperation.AddParameter("@ResultId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultId)
                Me._FilterQuery &= " AND ResultId = @ResultId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Result :") & " " & mstrResultName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND StatusId = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Status :") & " " & mstrStatusName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '          " Title AS Title " & _
            '          ",ECode AS ECode " & _
            '          ",EName AS EName " & _
            '          ",TraineeRemark AS TraineeRemark " & _
            '          ",Trainers AS Trainers " & _
            '          ",CASE WHEN ISNULL(CAST(LevelId AS NVARCHAR(10)),'') = 0 THEN '' ELSE @Level+' '+ CAST(LevelId AS NVARCHAR(10)) END AS Level " & _
            '          ",TrainerRemark AS TrainerRemark " & _
            '          ",Result AS Result " & _
            '          ",Status AS Status " & _
            '        "FROM " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "ISNULL(hrtraining_scheduling.course_title,'') AS Title " & _
            '                  ",hremployee_master.employeecode AS ECode " & _
            '                  ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
            '                  ",ISNULL(hrtraining_analysis_master.trainee_remark,'') AS TraineeRemark " & _
            '                  ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN ISNULL(hrtraining_trainers_tran.other_name,'') ELSE " & _
            '                            "ISNULL(Trainer.firstname,'')+' '+ISNULL(Trainer.othername,'')+' '+ISNULL(Trainer.surname,'')  END AS Trainers " & _
            '                  ",ISNULL(hrtraining_trainers_tran.trainer_level_id,'') AS LevelId " & _
            '                  ",ISNULL(hrtraining_analysis_tran.reviewer_remark,'') AS TrainerRemark " & _
            '                  ",ISNULL(hrresult_master.resultname,'') AS Result " & _
            '                  ",CASE WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 0 THEN @Incomplete " & _
            '                         "WHEN ISNULL(hrtraining_analysis_master.iscomplete,1) = 1 THEN @Complete " & _
            '                   "END AS STATUS " & _
            '                  ",hrtraining_trainers_tran.trainerstranunkid AS TrainerId " & _
            '                  ",hrtraining_scheduling.trainingschedulingunkid AS TrainingId " & _
            '                  ",hremployee_master.employeeunkid AS EmpId " & _
            '                  ",hrresult_master.resultunkid AS ResultId " & _
            '                  ",CASE WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 0 THEN 1 " & _
            '                              "WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 1 THEN 2 " & _
            '                   "END AS StatusId " & _
            '             "FROM hrtraining_enrollment_tran " & _
            '                  "LEFT JOIN hrtraining_analysis_master ON hrtraining_enrollment_tran.trainingenrolltranunkid = hrtraining_analysis_master.trainingenrolltranunkid " & _
            '                  "LEFT JOIN hrtraining_analysis_tran ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
            '                  "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                  "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
            '                  "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
            '                  "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '                  "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '             "WHERE ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0  AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 " & _
            '                  "AND ISNULL(hrtraining_scheduling.iscancel,0) = 0 AND ISNULL(hrtraining_scheduling.isvoid,0) = 0 " & _
            '                  "AND ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_analysis_tran.isvoid,0) = 0 "

            StrQ = "SELECT " & _
                      " Title AS Title " & _
                      ",ECode AS ECode " & _
                      ",EName AS EName " & _
                      ",TraineeRemark AS TraineeRemark " & _
                      ",Trainers AS Trainers " & _
                      ",CASE WHEN ISNULL(CAST(LevelId AS NVARCHAR(10)),'') = 0 THEN '' ELSE @Level+' '+ CAST(LevelId AS NVARCHAR(10)) END AS Level " & _
                      ",TrainerRemark AS TrainerRemark " & _
                      ",Result AS Result " & _
                      ",Status AS Status " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                              "cfcommon_master.name AS Title " & _
                              ",hremployee_master.employeecode AS ECode "

            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '" ,ISNULL(surname,'') + ' ' + ISNULL(firstname,'') as employeename "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",ISNULL(hrtraining_analysis_master.trainee_remark,'') AS TraineeRemark " & _
            '              ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN ISNULL(hrtraining_trainers_tran.other_name,'') ELSE " & _
            '                        "ISNULL(Trainer.firstname,'')+' '+ISNULL(Trainer.othername,'')+' '+ISNULL(Trainer.surname,'')  END AS Trainers " & _
            '              ",ISNULL(hrtraining_trainers_tran.trainer_level_id,'') AS LevelId " & _
            '              ",ISNULL(hrtraining_analysis_tran.reviewer_remark,'') AS TrainerRemark " & _
            '              ",ISNULL(hrresult_master.resultname,'') AS Result " & _
            '              ",CASE WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 0 THEN @Incomplete " & _
            '                     "WHEN ISNULL(hrtraining_analysis_master.iscomplete,1) = 1 THEN @Complete " & _
            '               "END AS STATUS " & _
            '              ",hrtraining_trainers_tran.trainerstranunkid AS TrainerId " & _
            '              ",hrtraining_scheduling.trainingschedulingunkid AS TrainingId " & _
            '              ",hremployee_master.employeeunkid AS EmpId " & _
            '              ",hrresult_master.resultunkid AS ResultId " & _
            '              ",CASE WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 0 THEN 1 " & _
            '                          "WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 1 THEN 2 " & _
            '               "END AS StatusId " & _
            '         "FROM hrtraining_enrollment_tran " & _
            '              "LEFT JOIN hrtraining_analysis_master ON hrtraining_enrollment_tran.trainingenrolltranunkid = hrtraining_analysis_master.trainingenrolltranunkid " & _
            '              "LEFT JOIN hrtraining_analysis_tran ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
            '              "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '              "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
            '              "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
            '              "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
            '              "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '              "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) " & _
            '         "WHERE ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0  AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 " & _
            '              "AND ISNULL(hrtraining_scheduling.iscancel,0) = 0 AND ISNULL(hrtraining_scheduling.isvoid,0) = 0 " & _
            '              "AND ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_analysis_tran.isvoid,0) = 0 "

            ''Anjan (10 Feb 2012)-End

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'If mblnIsActive = False Then
            '    StrQ &= " AND hremployee_master.isactive = 1"
            'End If

            StrQ &= ",ISNULL(hrtraining_analysis_master.trainee_remark,'') AS TraineeRemark " & _
                          ",CASE WHEN hrtraining_trainers_tran.employeeunkid = -1 THEN ISNULL(hrtraining_trainers_tran.other_name,'') ELSE " & _
                                    "ISNULL(Trainer.firstname,'')+' '+ISNULL(Trainer.othername,'')+' '+ISNULL(Trainer.surname,'')  END AS Trainers " & _
                          ",ISNULL(hrtraining_trainers_tran.trainer_level_id,'') AS LevelId " & _
                          ",ISNULL(hrtraining_analysis_tran.reviewer_remark,'') AS TrainerRemark " & _
                          ",ISNULL(hrresult_master.resultname,'') AS Result " & _
                          ",CASE WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 0 THEN @Incomplete " & _
                                 "WHEN ISNULL(hrtraining_analysis_master.iscomplete,1) = 1 THEN @Complete " & _
                           "END AS STATUS " & _
                          ",hrtraining_trainers_tran.trainerstranunkid AS TrainerId " & _
                          ",hrtraining_scheduling.trainingschedulingunkid AS TrainingId " & _
                          ",hremployee_master.employeeunkid AS EmpId " & _
                          ",hrresult_master.resultunkid AS ResultId " & _
                          ",CASE WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 0 THEN 1 " & _
                                      "WHEN ISNULL(hrtraining_analysis_master.iscomplete,0) = 1 THEN 2 " & _
                           "END AS StatusId " & _
                     "FROM hrtraining_enrollment_tran " & _
                          "LEFT JOIN hrtraining_analysis_master ON hrtraining_enrollment_tran.trainingenrolltranunkid = hrtraining_analysis_master.trainingenrolltranunkid " & _
                          "LEFT JOIN hrtraining_analysis_tran ON hrtraining_analysis_master.analysisunkid = hrtraining_analysis_tran.analysisunkid " & _
                          "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                          "LEFT JOIN hrtraining_trainers_tran ON hrtraining_analysis_tran.trainerstranunkid = hrtraining_trainers_tran.trainerstranunkid " & _
                          "LEFT JOIN hremployee_master AS Trainer ON hrtraining_trainers_tran.employeeunkid = Trainer.employeeunkid " & _
                          "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                          "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                          "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0  AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 " & _
                    "AND ISNULL(hrtraining_scheduling.iscancel,0) = 0 AND ISNULL(hrtraining_scheduling.isvoid,0) = 0 " & _
                    "AND ISNULL(hrtraining_analysis_master.isvoid,0) = 0 AND ISNULL(hrtraining_analysis_tran.isvoid,0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End

            StrQ &= " ) AS Analysis WHERE 1 = 1 "

            'Pinkal (24-Jun-2011) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Title")
                rpt_Rows.Item("Column2") = dtRow.Item("ECode")
                rpt_Rows.Item("Column3") = dtRow.Item("EName")
                rpt_Rows.Item("Column4") = dtRow.Item("TraineeRemark")
                rpt_Rows.Item("Column5") = dtRow.Item("Trainers")
                rpt_Rows.Item("Column6") = dtRow.Item("Level")
                rpt_Rows.Item("Column7") = dtRow.Item("Result")
                rpt_Rows.Item("Column8") = dtRow.Item("TrainerRemark")
                rpt_Rows.Item("Column9") = dtRow.Item("Status")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingAnalysis

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 14, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 15, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 16, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 17, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtTraining", Language.getMessage(mstrModuleName, 1, "Course Title :"))
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 2, "Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 3, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 4, "Status :"))
            Call ReportFunction.TextChange(objRpt, "txtTraineeRermak", Language.getMessage(mstrModuleName, 5, "Trainee Remark :"))
            Call ReportFunction.TextChange(objRpt, "txtTrainers", Language.getMessage(mstrModuleName, 6, "Trainers"))
            Call ReportFunction.TextChange(objRpt, "txtLevel", Language.getMessage(mstrModuleName, 7, "Level"))
            Call ReportFunction.TextChange(objRpt, "txtResult", Language.getMessage(mstrModuleName, 8, "Result"))
            Call ReportFunction.TextChange(objRpt, "txtRemark", Language.getMessage(mstrModuleName, 9, "Trainer Remark"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 11, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 12, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 13, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Course Title :")
            Language.setMessage(mstrModuleName, 2, "Code :")
            Language.setMessage(mstrModuleName, 3, "Employee :")
            Language.setMessage(mstrModuleName, 4, "Status :")
            Language.setMessage(mstrModuleName, 5, "Trainee Remark :")
            Language.setMessage(mstrModuleName, 6, "Trainers")
            Language.setMessage(mstrModuleName, 7, "Level")
            Language.setMessage(mstrModuleName, 8, "Result")
            Language.setMessage(mstrModuleName, 9, "Trainer Remark")
            Language.setMessage(mstrModuleName, 11, "Printed By :")
            Language.setMessage(mstrModuleName, 12, "Printed Date :")
            Language.setMessage(mstrModuleName, 13, "Page :")
            Language.setMessage(mstrModuleName, 14, "Result :")
            Language.setMessage(mstrModuleName, 15, "Level")
            Language.setMessage(mstrModuleName, 16, "Incomplete")
            Language.setMessage(mstrModuleName, 17, "Complete")
            Language.setMessage(mstrModuleName, 14, "Prepared By :")
            Language.setMessage(mstrModuleName, 15, "Checked By :")
            Language.setMessage(mstrModuleName, 16, "Approved By :")
            Language.setMessage(mstrModuleName, 17, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
