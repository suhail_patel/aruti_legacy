'************************************************************************************************************************************
'Class Name : clsPerformanceEvaluationReport.vb
'Purpose    :
'Date       :02-Sep-2013
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsPerformanceEvaluationReport
    Inherits IReportData

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsPerformanceEvaluationReport"
    Private mstrReportId As String = enArutiReport.Performance_Evaluation_Report
    Private mdtFinalTable As DataTable
    Private mdicScoreRating As New Dictionary(Of Integer, String)
    Private mdicExInfo As New Dictionary(Of Integer, Object)

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Property Variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrAllocation As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private iSelectColName As String = String.Empty
    Private iUnkidColName As String = String.Empty
    Private iJoinTblName As String = String.Empty
    'S.SANDEEP [21 AUG 2015] -- START
    Private mintScoringOptionId As Integer = 0
    'S.SANDEEP [21 AUG 2015] -- END

    'S.SANDEEP [08 Jan 2016] -- START
    Private blnIsSelfAssignCompetencies As Boolean = False
    'S.SANDEEP [08 Jan 2016] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Private mblnIsCalibrationSettingActive As Boolean = False
    Private mintDisplayScoreType As Integer = 0
    Private mstrDisplayScoreName As String = String.Empty
    'S.SANDEEP |27-MAY-2019| -- END

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private mstrInnFilter As String = String.Empty
    'S.SANDEEP |27-JUL-2019| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Allocation() As String
        Set(ByVal value As String)
            mstrAllocation = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _iSelectColName() As String
        Set(ByVal value As String)
            iSelectColName = value
        End Set
    End Property

    Public WriteOnly Property _iUnkidColName() As String
        Set(ByVal value As String)
            iUnkidColName = value
        End Set
    End Property

    Public WriteOnly Property _iJoinTblName() As String
        Set(ByVal value As String)
            iJoinTblName = value
        End Set
    End Property

    'S.SANDEEP [21 AUG 2015] -- START
    Public WriteOnly Property _ScoringOptionId() As Integer
        Set(ByVal value As Integer)
            mintScoringOptionId = value
        End Set
    End Property
    'S.SANDEEP [21 AUG 2015] -- END


    'S.SANDEEP [08 Jan 2016] -- START
    Public WriteOnly Property _SelfAssignCompetencies() As Boolean
        Set(ByVal value As Boolean)
            blnIsSelfAssignCompetencies = value
        End Set
    End Property
    'S.SANDEEP [08 Jan 2016] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public WriteOnly Property _IsCalibrationSettingActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCalibrationSettingActive = value
        End Set
    End Property
    Public WriteOnly Property _DisplayScoreType() As Integer
        Set(ByVal value As Integer)
            mintDisplayScoreType = value
        End Set
    End Property
    Public WriteOnly Property _DisplayScoreName() As String
        Set(ByVal value As String)
            mstrDisplayScoreName = value
        End Set
    End Property
    'S.SANDEEP |27-MAY-2019| -- END


    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Public WriteOnly Property _InnFilter() As String
        Set(ByVal value As String)
            mstrInnFilter = value
        End Set
    End Property
    'S.SANDEEP |27-JUL-2019| -- END

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrAllocation = String.Empty
            mstrAllocationIds = String.Empty
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            'S.SANDEEP [21 AUG 2015] -- START
            mintScoringOptionId = 0
            'S.SANDEEP [21 AUG 2015] -- END

            'S.SANDEEP [08 Jan 2016] -- START
            blnIsSelfAssignCompetencies = False
            'S.SANDEEP [08 Jan 2016] -- END

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            mblnIsCalibrationSettingActive = False
            mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            mstrDisplayScoreName = ""
            'S.SANDEEP |27-MAY-2019| -- END

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            mstrInnFilter = String.Empty
            'S.SANDEEP |27-JUL-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Get_Allocation_Data(ByVal strDatabaseName As String, _
                                         ByVal intUserUnkid As Integer, _
                                         ByVal intYearUnkid As Integer, _
                                         ByVal intCompanyUnkid As Integer, _
                                         ByVal dtPeriodStart As Date, _
                                         ByVal dtPeriodEnd As Date, _
                                         ByVal strUserModeSetting As String, _
                                         ByVal blnOnlyApproved As Boolean, _
                                         ByVal blnIsIncludeInactiveEmp As Boolean) As DataSet
        'Private Function Get_Allocation_Data() As DataSet
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        'S.SANDEEP [ 10 SEPT 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim blnAddUnion As Boolean = False
        'S.SANDEEP [ 10 SEPT 2013 ] -- END
        Try
            Dim objData As New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'StrQ = "SELECT " & _
            '            " " & iSelectColName & " AS allocation " & _
            '            ",ISNULL(TS.Total,0) AS staff " & _
            '            ",ISNULL(B.[Not Rated],0) AS nrate " & _
            '            ",ISNULL(PB.Probation,0) AS prob " & _
            '            ",0 AS total " & _
            '            ",CAST(0 AS DECIMAL(10,2)) AS avgscr " & _
            '            ",'' AS grade " & _
            '            "," & iJoinTblName & "." & iUnkidColName & " AS allocid " & _
            '        "FROM " & iJoinTblName & " " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                  " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            '                  ",COUNT(DISTINCT employeeunkid) AS Probation " & _
            '             "FROM athremployee_master " & _
            '             "WHERE CONVERT(CHAR(8),probation_from_date,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
            '             "AND CONVERT(CHAR(8),probation_to_date,112) >= '" & eZeeDate.convertDate(mdtEndDate) & "' " & _
            '             "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            '        ") AS PB ON " & iJoinTblName & "." & iUnkidColName & " = PB." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                  "" & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",COUNT(employeeunkid) AS Total " & _
            '             "FROM hremployee_master WHERE 1 = 1 "
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            ''S.SANDEEP [ 10 SEPT 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            '                    ") AS TS ON " & iJoinTblName & "." & iUnkidColName & " = TS." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " "

            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                    ") AS TS ON " & iJoinTblName & "." & iUnkidColName & " = TS." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                    "LEFT JOIN " & _
            ''                    "( " & _
            ''                         "SELECT " & _
            ''                              " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                              ",COUNT(iTotal) AS [Not Rated] " & _
            ''                         "FROM " & _
            ''                         "( " & _
            ''                              "SELECT " & _
            ''                                        " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                                        ",COUNT(employeeunkid) AS iTotal " & _
            ''                                        ",employeeunkid " & _
            ''                                   "FROM hremployee_master WHERE employeeunkid NOT IN (SELECT DISTINCT selfemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "') "
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",employeeunkid " & _
            ''                  "UNION " & _
            ''                       "SELECT " & _
            ''                            " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                            ",COUNT(employeeunkid) AS iTotal " & _
            ''                            ",employeeunkid " & _
            ''                       "FROM hremployee_master WHERE employeeunkid NOT IN (SELECT DISTINCT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "') "
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",employeeunkid " & _
            ''                  "UNION " & _
            ''                       "SELECT " & _
            ''                            " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                            ",COUNT(employeeunkid) AS iTotal " & _
            ''                            ",employeeunkid " & _
            ''                       "FROM hremployee_master WHERE employeeunkid NOT IN (SELECT DISTINCT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "') "
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",employeeunkid " & _
            ''             ")AS NR GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''        ") AS B ON " & iJoinTblName & "." & iUnkidColName & " = B." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''        "WHERE isactive = 1 "
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                    ") AS TS ON " & iJoinTblName & "." & iUnkidColName & " = TS." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                    "LEFT JOIN " & _
            ''                    "( " & _
            ''                         "SELECT " & _
            ''                              " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                              ",COUNT(iTotal) AS [Not Rated] " & _
            ''                         "FROM " & _
            ''                         "( " & _
            ''                              "SELECT " & _
            ''                                        " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                                        ",COUNT(employeeunkid) AS iTotal " & _
            ''                                        ",employeeunkid " & _
            ''                                   "FROM hremployee_master WHERE employeeunkid NOT IN (SELECT DISTINCT selfemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "') "
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",employeeunkid " & _
            ''                  "UNION " & _
            ''                       "SELECT " & _
            ''                            " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                            ",COUNT(employeeunkid) AS iTotal " & _
            ''                            ",employeeunkid " & _
            ''                       "FROM hremployee_master WHERE employeeunkid NOT IN (SELECT DISTINCT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "') "
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",employeeunkid " & _
            ''                  "UNION " & _
            ''                       "SELECT " & _
            ''                            " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''                            ",COUNT(employeeunkid) AS iTotal " & _
            ''                            ",employeeunkid " & _
            ''                       "FROM hremployee_master WHERE employeeunkid NOT IN (SELECT DISTINCT assessedemployeeunkid FROM hrassess_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "') "
            ''If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If
            ''StrQ &= "GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & ",employeeunkid " & _
            ''             ")AS NR GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''        ") AS B ON " & iJoinTblName & "." & iUnkidColName & " = B." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            ''        "WHERE isactive = 1 "
            'StrQ &= "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                  " " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            '        "       ,COUNT(employeeunkid) [Not Rated] " & _
            '        "   FROM hremployee_master " & _
            '        "   WHERE employeeunkid NOT IN " & _
            '        "   ( "

            ''S.SANDEEP [21 AUG 2015] -- START

            ''Select Case ConfigParameter._Object._PerformanceComputation
            ''    Case 1  'AVG SCORE OF
            ''        If ConfigParameter._Object._WAvgEmpComputationType = True Then
            ''            StrQ &= "SELECT DISTINCT selfemployeeunkid AS id FROM hrbsc_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            ''                    "UNION " & _
            ''                    "SELECT DISTINCT selfemployeeunkid AS id FROM hrassess_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''            blnAddUnion = True
            ''        End If

            ''        If ConfigParameter._Object._WAvgAseComputType = True Then
            ''            If blnAddUnion = True Then
            ''                StrQ &= " UNION "
            ''            End If
            ''            StrQ &= "SELECT DISTINCT assessedemployeeunkid AS id FROM hrbsc_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            ''                    "UNION " & _
            ''                    "SELECT DISTINCT assessedemployeeunkid AS id FROM hrassess_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''            blnAddUnion = True
            ''        End If

            ''        If ConfigParameter._Object._WAvgRevComputType = True Then
            ''            If blnAddUnion = True Then
            ''                StrQ &= " UNION "
            ''            End If
            ''            StrQ &= "SELECT DISTINCT assessedemployeeunkid AS id FROM hrbsc_analysis_master WHERE isvoid  = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            ''                  "UNION " & _
            ''                    "SELECT DISTINCT assessedemployeeunkid AS id FROM hrassess_analysis_master WHERE isvoid  = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''        End If

            ''    Case 2  'WEIGHT SCORE OF
            ''        If ConfigParameter._Object._WScrComputType = 1 Then
            ''            StrQ &= "SELECT DISTINCT selfemployeeunkid AS id FROM hrbsc_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            ''                    "UNION " & _
            ''                    "SELECT DISTINCT selfemployeeunkid AS id FROM hrassess_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''        ElseIf ConfigParameter._Object._WScrComputType = 2 Then
            ''            StrQ &= "SELECT DISTINCT assessedemployeeunkid AS id FROM hrbsc_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            ''                    "UNION " & _
            ''                    "SELECT DISTINCT assessedemployeeunkid AS id FROM hrassess_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''        ElseIf ConfigParameter._Object._WScrComputType = 3 Then
            ''            StrQ &= "SELECT DISTINCT assessedemployeeunkid AS id FROM hrbsc_analysis_master WHERE isvoid  = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            ''                  "UNION " & _
            ''                    "SELECT DISTINCT assessedemployeeunkid AS id FROM hrassess_analysis_master WHERE isvoid  = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''        End If
            ''End Select

            'StrQ &= "SELECT DISTINCT selfemployeeunkid AS id FROM hrevaluation_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            '        "UNION " & _
            '        "SELECT DISTINCT assessedemployeeunkid AS id FROM hrevaluation_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
            '        "UNION " & _
            '        "SELECT DISTINCT assessedemployeeunkid AS id FROM hrevaluation_analysis_master WHERE isvoid  = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' "
            ''S.SANDEEP [21 AUG 2015] -- END


            'StrQ &= "   ) "
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'StrQ &= " GROUP BY " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " "
            'StrQ &= ") AS B ON " & iJoinTblName & "." & iUnkidColName & " = B." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
            '        "WHERE isactive = 1 "
            ''S.SANDEEP [ 10 SEPT 2013 ] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            'S.SANDEEP [15 NOV 2016] -- START
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [15 NOV 2016] -- END
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            StrQ = "SELECT " & _
                   " " & iSelectColName & " AS allocation " & _
                   "    ,ISNULL(TS.Total,0) AS staff " & _
                   "    ,ISNULL(B.[Not Rated],0) AS nrate " & _
                   "    ,ISNULL(PB.Probation,0) AS prob " & _
                   "    ,0 AS total " & _
                   "    ,CAST(0 AS DECIMAL(10,2)) AS avgscr " & _
                   "    ,'' AS grade " & _
                   "    ," & iJoinTblName & "." & iUnkidColName & " AS allocid " & _
                   "FROM " & iJoinTblName & " " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                   "        ,COUNT(DISTINCT AL.employeeunkid) AS Probation " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hremployee_transfer_tran." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                   "            ,hremployee_master.employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                   "        FROM hremployee_master " & _
                   "            JOIN hremployee_transfer_tran ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS AL " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hremployee_dates_tran.employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                   "            ,CONVERT(CHAR(8),date1,112) AS date1 " & _
                   "            ,CONVERT(CHAR(8),date2,112) AS date2 " & _
                   "        FROM hremployee_master " & _
                   "            JOIN hremployee_dates_tran ON hremployee_dates_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "        WHERE isvoid = 0 AND datetypeunkid = 1 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        AND CONVERT(CHAR(8),date1,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' AND CONVERT(CHAR(8),date2,112) >= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "    ) AS PR ON AL.employeeunkid = PR.employeeunkid AND PR.Rno = 1 " & _
                   "    WHERE AL.Rno = 1 GROUP BY AL." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                   ") AS PB ON " & iJoinTblName & "." & iUnkidColName & " = PB." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                   "        ,COUNT(DISTINCT AL.employeeunkid) AS Total " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hremployee_transfer_tran." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                   "            ,hremployee_master.employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                   "        FROM hremployee_master " & _
                   "            JOIN hremployee_transfer_tran ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            StrQ &= "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "

            If blnIsIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "    ) AS AL WHERE AL.Rno = 1 " & _
                    "    GROUP BY AL." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                    ") AS TS ON " & iJoinTblName & "." & iUnkidColName & " = TS." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "        " & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                    "        ,COUNT(DISTINCT AL.employeeunkid) AS [Not Rated] " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             hremployee_transfer_tran." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                    "            ,hremployee_master.employeeunkid " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "        FROM hremployee_master " & _
                    "            JOIN hremployee_transfer_tran ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            StrQ &= "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    " AND hremployee_master.employeeunkid NOT IN " & _
                    "( " & _
                    "   SELECT DISTINCT selfemployeeunkid AS id FROM hrevaluation_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
                    "UNION " & _
                    "   SELECT DISTINCT assessedemployeeunkid AS id FROM hrevaluation_analysis_master WHERE isvoid  = 0 AND iscommitted = 1 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
                    "UNION " & _
                    "   SELECT DISTINCT assessedemployeeunkid AS id FROM hrevaluation_analysis_master WHERE isvoid  = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & mintPeriodId & "' " & _
                    " ) "

            If blnIsIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "    ) AS AL WHERE AL.Rno = 1 " & _
                    "    GROUP BY AL." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " " & _
                    ") AS B ON " & iJoinTblName & "." & iUnkidColName & " = B." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " "

            'S.SANDEEP [04 JUN 2015] -- END



            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAllocationIds
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
            'objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
            'S.SANDEEP [04 JUN 2015] -- END

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Allocation_Data; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub Fill_Data(ByVal strDatabaseName As String, _
                          ByVal intUserUnkid As Integer, _
                          ByVal intYearUnkid As Integer, _
                          ByVal intCompanyUnkid As Integer, _
                          ByVal dtPeriodStart As Date, _
                          ByVal dtPeriodEnd As Date, _
                          ByVal strUserModeSetting As String, _
                          ByVal blnOnlyApproved As Boolean, _
                          ByVal blnIsIncludeInactiveEmp As Boolean)
        'Private Sub Fill_Data()
        'S.SANDEEP [04 JUN 2015] -- END

        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = Get_Allocation_Data(ByVal strDatabaseName As String, _
            '                             ByVal intUserUnkid As Integer, _
            '                             ByVal intYearUnkid As Integer, _
            '                             ByVal intCompanyUnkid As Integer, _
            '                             ByVal dtPeriodStart As Date, _
            '                             ByVal dtPeriodEnd As Date, _
            '                             ByVal strUserModeSetting As String, _
            '                             ByVal blnOnlyApproved As Boolean, _
            '                             ByVal blnIsIncludeInactiveEmp As Boolean)

            dsList = Get_Allocation_Data(strDatabaseName, _
                                         intUserUnkid, _
                                         intYearUnkid, _
                                         intCompanyUnkid, _
                                         dtPeriodStart, _
                                         dtPeriodEnd, _
                                         strUserModeSetting, _
                                         blnOnlyApproved, _
                                         blnIsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- END

            Dim mdicDivTotal As New Dictionary(Of Integer, Integer)
            Dim mdicTotal As New Dictionary(Of Integer, Integer)
            Dim mdicAllocScore As New Dictionary(Of Integer, Decimal)

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dtRow As DataRow = mdtFinalTable.NewRow
                    dtRow.Item("allocation") = dRow.Item("allocation")
                    dtRow.Item("staff") = dRow.Item("staff")
                    dtRow.Item("nrate") = dRow.Item("nrate")
                    dtRow.Item("prob") = dRow.Item("prob")
                    dtRow.Item("total") = dRow.Item("total")
                    dtRow.Item("avgscr") = dRow.Item("avgscr")
                    dtRow.Item("grade") = dRow.Item("grade")
                    dtRow.Item("allocid") = dRow.Item("allocid")
                    If mdicDivTotal.ContainsKey(dRow.Item("allocid")) = False Then
                        mdicDivTotal.Add(dRow.Item("allocid"), dRow.Item("nrate"))
                    End If
                    If mdicTotal.ContainsKey(dtRow.Item("allocid")) = False Then
                        mdicTotal.Add(dRow.Item("allocid"), dRow.Item("nrate") + dRow.Item("prob"))
                    End If
                    mdtFinalTable.Rows.Add(dtRow)
                Next
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = Get_Employee_Score() : Dim dtRating As DataTable
            dsList = Get_Employee_Score(dtPeriodEnd, strDatabaseName, dtPeriodStart, blnIsIncludeInactiveEmp) : Dim dtRating As DataTable
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objRating As New clsAppraisal_Rating
            dtRating = objRating._DataTable
            objRating = Nothing

            If dsList.Tables(0).Rows.Count > 0 Then
                If dtRating.Rows.Count > 0 Then
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        Dim dtmp() As DataRow = dtRating.Select(CDec(dRow.Item("Score")) & " >= score_from AND " & CDec(dRow.Item("Score")) & " <= score_to ")
                        If dtmp.Length > 0 Then
                            Dim dfinal() As DataRow = mdtFinalTable.Select("allocid = '" & dRow.Item("allocid") & "'")
                            If mdicAllocScore.ContainsKey(dRow.Item("allocid")) = False Then
                                mdicAllocScore.Add(dRow.Item("allocid"), CDec(dRow.Item("Score")))
                            Else
                                mdicAllocScore(dRow.Item("allocid")) = CDec(mdicAllocScore(dRow.Item("allocid"))) + CDec(dRow.Item("Score"))
                            End If
                            If dfinal.Length > 0 Then
                                If mdicScoreRating.ContainsKey(dtmp(0).Item("appratingunkid")) = True Then
                                    dfinal(0).Item(mdicScoreRating(dtmp(0).Item("appratingunkid"))) = CInt(dfinal(0).Item(mdicScoreRating(dtmp(0).Item("appratingunkid")))) + 1
                                    mdtFinalTable.AcceptChanges()
                                    If mdicDivTotal.ContainsKey(dRow.Item("allocid")) = False Then
                                        mdicDivTotal.Add(dRow.Item("allocid"), 1)
                                    Else
                                        mdicDivTotal(dRow.Item("allocid")) = mdicDivTotal(dRow.Item("allocid")) + 1
                                    End If

                                    If mdicTotal.ContainsKey(dRow.Item("allocid")) = False Then
                                        mdicTotal.Add(dRow.Item("allocid"), 1)
                                    Else
                                        mdicTotal(dRow.Item("allocid")) = mdicTotal(dRow.Item("allocid")) + 1
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            Else
                mdtFinalTable.Rows.Clear()
                Exit Try
            End If

            For Each iKey As Integer In mdicTotal.Keys
                Dim dfinal() As DataRow = mdtFinalTable.Select("allocid = '" & iKey & "'")
                If dfinal.Length > 0 Then
                    dfinal(0).Item("total") = mdicTotal(iKey)
                End If
            Next

            For Each iKey As Integer In mdicAllocScore.Keys
                Dim dfinal() As DataRow = mdtFinalTable.Select("allocid = '" & iKey & "'")
                If dfinal.Length > 0 Then
                    If mdicDivTotal.ContainsKey(iKey) Then
                        'S.SANDEEP |21-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                        'dfinal(0).Item("avgscr") = Format(CDec(mdicAllocScore(iKey) / mdicDivTotal(iKey)), "####.#0")
                        dfinal(0).Item("avgscr") = Format(CDec(mdicAllocScore(iKey) / mdicDivTotal(iKey)), "######################0.#0")
                        'S.SANDEEP |21-AUG-2019| -- END
                        Dim dtmp() As DataRow = dtRating.Select(CDec(dfinal(0).Item("avgscr")) & " >= score_from AND " & CDec(dfinal(0).Item("avgscr")) & " <= score_to ")
                        If dtmp.Length > 0 Then
                            dfinal(0).Item("grade") = dtmp(0).Item("grade_award")
                        End If
                    End If
                End If
            Next
            mdtFinalTable.AcceptChanges()
            mdtFinalTable.Columns.Remove("allocid")
            mdicExInfo = New Dictionary(Of Integer, Object)
            Dim iTotal As Decimal = mdtFinalTable.Compute("SUM(avgscr)", "")
            Dim dt() As DataRow = dtRating.Select(CDec(iTotal) & " >= score_from AND " & CDec(iTotal) & " <= score_to ")
            If dt.Length > 0 Then
                mdicExInfo.Add(mdtFinalTable.Columns("grade").Ordinal, dt(0).Item("grade_award"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Fill_Data; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub CreateTable(ByVal strDatabaseName As String, _
                            ByVal intUserUnkid As Integer, _
                            ByVal intYearUnkid As Integer, _
                            ByVal intCompanyUnkid As Integer, _
                            ByVal dtPeriodStart As Date, _
                            ByVal dtPeriodEnd As Date, _
                            ByVal strUserModeSetting As String, _
                            ByVal blnOnlyApproved As Boolean, _
                            ByVal blnIsIncludeInactiveEmp As Boolean)
        'Private Sub CreateTable()
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            mdtFinalTable = New DataTable

            mdicScoreRating = New Dictionary(Of Integer, String)

            Dim dCol As DataColumn
            dCol = New DataColumn
            dCol.ColumnName = "allocation"
            dCol.Caption = mstrAllocation 'Language.getMessage(mstrModuleName, 1, "Department")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "staff"
            dCol.Caption = Language.getMessage(mstrModuleName, 2, "Total Staff")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtFinalTable.Columns.Add(dCol)

            Dim objData As New clsDataOperation

            StrQ = "SELECT appratingunkid AS RateId ,CAST(score_from AS NVARCHAR(MAX)) + ' - ' + CAST(score_to AS NVARCHAR(MAX)) + '\r\n' + grade_award AS iScore	 " & _
                   "FROM hrapps_ratings WHERE isvoid = 0 ORDER BY score_to DESC "

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    dCol = New DataColumn
                    dCol.ColumnName = "Column_" & dRow.Item("RateId")
                    dCol.Caption = dRow.Item("iScore").ToString.Replace("\r\n", vbCrLf)
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    mdtFinalTable.Columns.Add(dCol)
                    If mdicScoreRating.ContainsKey(dRow.Item("RateId")) = False Then
                        mdicScoreRating.Add(dRow.Item("RateId"), dCol.ColumnName)
                    End If
                Next
            End If

            dCol = New DataColumn
            dCol.ColumnName = "nrate"
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "Not Rated")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "prob"
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Probation")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "total"
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "Total")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "avgscr"
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "Average Score %")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "grade"
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "Score")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "allocid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinalTable.Columns.Add(dCol)


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call Fill_Data()

            Call Fill_Data(strDatabaseName, _
                           intUserUnkid, _
                           intYearUnkid, _
                           intCompanyUnkid, _
                           dtPeriodStart, _
                           dtPeriodEnd, _
                           strUserModeSetting, _
                           blnOnlyApproved, _
                           blnIsIncludeInactiveEmp)
            'S.SANDEEP [04 JUN 2015] -- END



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTable; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Export_Evaluation_Report(ByVal strDatabaseName As String, _
                                             ByVal intUserUnkid As Integer, _
                                             ByVal intYearUnkid As Integer, _
                                             ByVal intCompanyUnkid As Integer, _
                                             ByVal dtPeriodStart As Date, _
                                             ByVal dtPeriodEnd As Date, _
                                             ByVal strUserModeSetting As String, _
                                             ByVal blnOnlyApproved As Boolean, _
                                             ByVal blnIsIncludeInactiveEmp As Boolean, _
                                             ByVal strExportPath As String, ByVal blnOpenAfterExport As Boolean) As Boolean 'S.SANDEEP [13-JUL-2017] -- START -- END
        'Public Function Export_Evaluation_Report() As Boolean
        'S.SANDEEP [04 JUN 2015] -- END

        Try

            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid
            'S.SANDEEP [13-JUL-2017] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call CreateTable()
            Call CreateTable(strDatabaseName, _
                             intUserUnkid, _
                             intYearUnkid, _
                             intCompanyUnkid, _
                             dtPeriodStart, _
                             dtPeriodEnd, _
                             strUserModeSetting, _
                             blnOnlyApproved, _
                             blnIsIncludeInactiveEmp)
            'S.SANDEEP [04 JUN 2015] -- END
            If mdtFinalTable.Rows.Count > 0 AndAlso mdtFinalTable IsNot Nothing Then
                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(mdtFinalTable.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    Select Case i
                        Case 0
                            intArrayColumnWidth(i) = 85
                        Case i > 7 And i < 12
                            intArrayColumnWidth(i) = 70
                        Case Else
                            intArrayColumnWidth(i) = 75
                    End Select
                Next

                Dim sExTitle As String = Language.getMessage(mstrModuleName, 8, "Period : ")

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                Dim strExFilter As String = String.Empty
                If mblnIsCalibrationSettingActive Then
                    strExFilter = Language.getMessage(mstrModuleName, 9, "Display Score Type") & " : " & mstrDisplayScoreName
                End If
                'S.SANDEEP |27-MAY-2019| -- END


                'S.SANDEEP [13-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : 
                'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtFinalTable, intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, sExTitle & mstrPeriodName, " ", Nothing, "", True, Nothing, Nothing, mdicExInfo)

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, mdtFinalTable, intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, sExTitle & mstrPeriodName, " ", Nothing, "", True, Nothing, Nothing, mdicExInfo)
                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, mdtFinalTable, intArrayColumnWidth, True, True, True, Nothing, Me._ReportName, sExTitle & mstrPeriodName, strExFilter, Nothing, "", True, Nothing, Nothing, mdicExInfo)
                'S.SANDEEP |27-MAY-2019| -- END

                'S.SANDEEP [13-JUL-2017] -- END
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Evaluation_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [21 AUG 2015] -- START


    'Private Function Get_Employee_Score() As DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        'S.SANDEEP [ 04 MAR 2014 ] -- START
    '        Dim iStringValue As String = String.Empty

    '        'S.SANDEEP [ 30 MAY 2014 ] -- START
    '        'Dim objWSetting As New clsWeight_Setting(True)
    '        Dim objWSetting As New clsWeight_Setting(mintPeriodId)
    '        'S.SANDEEP [ 30 MAY 2014 ] -- END

    '        If objWSetting._Weight_Typeid <= 0 Then
    '            iStringValue = " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
    '            iStringValue = " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
    '            iStringValue = " AND kpiunkid > 0 "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
    '            iStringValue = " AND targetunkid > 0 "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
    '            iStringValue = " AND initiativeunkid > 0 "
    '        End If
    '        'S.SANDEEP [ 04 MAR 2014 ] -- END


    '        'S.SANDEEP [ 10 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        StrQ &= "SELECT allocid,Score FROM ("
    '        'S.SANDEEP [ 10 SEPT 2013 ] -- END
    '        StrQ &= "SELECT " & _
    '               " hremployee_master." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " AS allocid "
    '        Select Case ConfigParameter._Object._PerformanceComputation
    '            Case 1 'Weight Avg Of
    '                If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
    '                    StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
    '                    StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
    '                    StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                End If
    '            Case 2 'Weight Score Of
    '                If ConfigParameter._Object._WScrComputType = 1 Then
    '                    StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WScrComputType = 2 Then
    '                    StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                ElseIf ConfigParameter._Object._WScrComputType = 3 Then
    '                    StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
    '                End If
    '        End Select

    '        'S.SANDEEP [ 05 NOV 2014 ] -- START
    '        'StrQ &= "FROM hremployee_master " & _
    '        '        "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
    '        '        "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '        '        "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '        '        "	JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & mintPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        selfemployeeunkid AS EmpId " & _
    '        '        "       ,ISNULL(SUM(SELF),0) AS S_GA " & _
    '        '        "   FROM hrassess_analysis_master " & _
    '        '        "   JOIN " & _
    '        '        "   ( " & _
    '        '        "       SELECT " & _
    '        '        "            hrassess_analysis_tran.analysisunkid " & _
    '        '        "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
    '        '        "       FROM hrassess_analysis_tran " & _
    '        '        "           JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
    '        '        "           JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
    '        '        "           JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "           JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
    '        '        "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
    '        '        "       GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
    '        '        "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
    '        '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '        '        "   GROUP BY selfemployeeunkid " & _
    '        '        ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        selfemployeeunkid " & _
    '        '        "       ,SELF AS S_BSC " & _
    '        '        "   FROM hrbsc_analysis_master " & _
    '        '        "   JOIN " & _
    '        '        "   ( " & _
    '        '        "       SELECT " & _
    '        '        "            analysisunkid " & _
    '        '        "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
    '        '        "       FROM hrbsc_analysis_tran " & _
    '        '        "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
    '        '        iStringValue & " " & _
    '        '        "       GROUP BY  analysisunkid " & _
    '        '        "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
    '        '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '        '        ") AS S_BSC ON S_BSC.selfemployeeunkid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        assessedemployeeunkid " & _
    '        '        "       ,CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS A_GA " & _
    '        '        "   FROM hrassess_analysis_master " & _
    '        '        "   JOIN " & _
    '        '        "   ( " & _
    '        '        "       SELECT " & _
    '        '        "            hrassess_analysis_tran.analysisunkid " & _
    '        '        "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
    '        '        "       FROM hrassess_analysis_tran " & _
    '        '        "       JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
    '        '        "       JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
    '        '        "       JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "       JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
    '        '        "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
    '        '        "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
    '        '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & mintPeriodId & "' " & _
    '        '        "   GROUP BY assessedemployeeunkid " & _
    '        '        ") AS A_GA ON A_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        assessedemployeeunkid " & _
    '        '        "       ,CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS A_BSC " & _
    '        '        "   FROM hrbsc_analysis_master " & _
    '        '        "   JOIN " & _
    '        '        "   ( " & _
    '        '        "       SELECT " & _
    '        '        "            analysisunkid " & _
    '        '        "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
    '        '        "       FROM hrbsc_analysis_tran " & _
    '        '        "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
    '        '        iStringValue & " " & _
    '        '        "       GROUP BY  analysisunkid " & _
    '        '        "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
    '        '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '        '        "   GROUP BY assessedemployeeunkid " & _
    '        '        ") AS A_BSC ON A_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        assessedemployeeunkid " & _
    '        '        "       ,ISNULL(SUM(RevScore),0) AS R_GA " & _
    '        '        "   FROM hrassess_analysis_master " & _
    '        '        "   JOIN " & _
    '        '        "   ( " & _
    '        '        "       SELECT " & _
    '        '        "            hrassess_analysis_tran.analysisunkid " & _
    '        '        "           ,CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
    '        '        "       FROM hrassess_analysis_tran " & _
    '        '        "           JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
    '        '        "           JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
    '        '        "           JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "           JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
    '        '        "       WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
    '        '        "   ) AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
    '        '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & mintPeriodId & "' " & _
    '        '        "   GROUP BY assessedemployeeunkid " & _
    '        '        ") AS R_GA ON R_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        assessedemployeeunkid " & _
    '        '        "       ,RevScore AS R_BSC " & _
    '        '        "   FROM hrbsc_analysis_master " & _
    '        '        "   JOIN " & _
    '        '        "   ( " & _
    '        '        "       SELECT " & _
    '        '        "            analysisunkid " & _
    '        '        "           ,ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
    '        '        "       FROM hrbsc_analysis_tran " & _
    '        '        "           JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '        "       WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
    '        '        iStringValue & " " & _
    '        '        "       GROUP BY  analysisunkid " & _
    '        '        "   ) AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
    '        '        "   WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '        '        ") AS R_BSC ON R_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
    '        '        " WHERE 1 = 1 "
    'StrQ &= "FROM hremployee_master " & _
    '        "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
    '        "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '        "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '        "	JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & mintPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
    '        "LEFT JOIN " & _
    '        "( " & _
    '        "   SELECT " & _
    '        "        selfemployeeunkid AS EmpId " & _
    '                "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
    '                "       FROM hrevaluation_analysis_master " & _
    '                "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
    '                "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '                "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '                "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
    '        "   GROUP BY selfemployeeunkid " & _
    '        ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
    '        "LEFT JOIN " & _
    '        "( " & _
    '        "   SELECT " & _
    '                "            selfemployeeunkid AS EmpId " & _
    '                "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
    '                "       FROM hrevaluation_analysis_master " & _
    '                "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '                "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
    '                "       GROUP BY selfemployeeunkid " & _
    '                "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
    '        "LEFT JOIN " & _
    '        "( " & _
    '        "   SELECT " & _
    '                "            assessedemployeeunkid AS EmpId " & _
    '                "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
    '                "       FROM hrevaluation_analysis_master " & _
    '                "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
    '                "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '                "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '                "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
    '        "   GROUP BY assessedemployeeunkid " & _
    '                "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
    '        "LEFT JOIN " & _
    '        "( " & _
    '        "   SELECT " & _
    '                "            assessedemployeeunkid AS EmpId " & _
    '                "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
    '                "       FROM hrevaluation_analysis_master " & _
    '                "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '                "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
    '        "   GROUP BY assessedemployeeunkid " & _
    '                "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
    '        "LEFT JOIN " & _
    '        "( " & _
    '        "   SELECT " & _
    '                "            assessedemployeeunkid AS EmpId " & _
    '                "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
    '                "       FROM hrevaluation_analysis_master " & _
    '                "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
    '                "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '                "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '                "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
    '        "   GROUP BY assessedemployeeunkid " & _
    '                "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
    '        "LEFT JOIN " & _
    '        "( " & _
    '        "   SELECT " & _
    '                "            assessedemployeeunkid AS EmpId " & _
    '                "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
    '                "       FROM hrevaluation_analysis_master " & _
    '                "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & mintPeriodId & "' " & _
    '                "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
    '                "       GROUP BY assessedemployeeunkid " & _
    '                "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
    '        " WHERE 1 = 1 "
    '        'S.SANDEEP [ 05 NOV 2014 ] -- END


    '        If mstrAllocationIds.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAllocationIds.Replace(iJoinTblName, "hremployee_master").Replace("classesunkid", "classunkid")
    '        End If
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '        End If

    '        'S.SANDEEP [ 22 OCT 2013 ] -- START
    '        'If ConfigParameter._Object._ExcludeProbatedEmpOnPerformance = True Then
    '        '    StrQ &= " AND hremployee_master.employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM athremployee_master " & _
    '        '            " WHERE CONVERT(CHAR(8),probation_from_date,112)<= @end_date AND CONVERT(CHAR(8),probation_to_date,112)>= @end_date) "

    '        '    objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '        'End If
    '        'S.SANDEEP [ 22 OCT 2013 ] -- END


    '        'S.SANDEEP [ 10 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        StrQ &= ") AS A WHERE 1 = 1  AND Score > 0 "
    '        'S.SANDEEP [ 10 SEPT 2013 ] -- END


    '        'S.SANDEEP [ 28 FEB 2014 ] -- START
    '        'REMOVED : CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2))
    '        'ADDED   : CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2))
    '        'S.SANDEEP [ 28 FEB 2014 ] -- END


    '        'S.SANDEEP [ 04 MAR 2014 ] -- START
    '        '   "           AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " -- ADDED

    '        'S.SANDEEP [ 04 MAR 2014 ] -- END

    '        'S.SANDEEP [ 05 NOV 2014 ] -- START
    '        objDataOperation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)
    '        'S.SANDEEP [ 05 NOV 2014 ] -- END

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtmp() As DataRow = dsList.Tables(0).Select("Score > 0")

    '        If dtmp.Length <= 0 Then
    '            dsList.Tables(0).Rows.Clear()
    '        End If

    '        Return dsList
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_Employee_Score; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Get_Employee_Score(ByVal dtEmployeeAsOnDate As DateTime, ByVal strDatabaseName As String, ByVal dtPeriodStartDate As DateTime, ByVal blnIsIncludeInactiveEmp As Boolean) As DataSet
        'Private Function Get_Employee_Score() As DataSet
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStartDate, dtEmployeeAsOnDate, , , strDatabaseName)

            StrQ = "SELECT " & _
                   "     Alloc." & IIf(iUnkidColName = "classesunkid", "classunkid", iUnkidColName) & " AS allocid " & _
                   "    ,0 AS Score " & _
                   "    ,hremployee_master.employeeunkid AS EId " & _
                   "    ,'" & mintPeriodId & "' AS PId " & _
                   "	,ISNULL(ASR.AsrMId,0) AS AsrMstId " & _
                   "	,ISNULL(REV.RevMId,0) AS RevMstId " & _
                   "FROM hremployee_master " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         stationunkid " & _
                   "        ,deptgroupunkid " & _
                   "        ,departmentunkid " & _
                   "        ,sectiongroupunkid " & _
                   "        ,sectionunkid " & _
                   "        ,unitgroupunkid " & _
                   "        ,unitunkid " & _
                   "        ,teamunkid " & _
                   "        ,classgroupunkid " & _
                   "        ,classunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_transfer_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEmployeeAsOnDate) & "' " & _
                   ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         jobunkid " & _
                   "        ,jobgroupunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEmployeeAsOnDate) & "' " & _
                   ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "   SELECT " & _
                   "		 hrassessor_master.assessormasterunkid AS AsrMId " & _
                   "		,hrassessor_tran.employeeunkid AS AEId " & _
                   "	FROM hrassessor_tran " & _
                   "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                   "	WHERE hrassessor_master.isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                   ") AS ASR ON ASR.AEId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                   "		 hrassessor_master.assessormasterunkid AS RevMId " & _
                   "		,hrassessor_tran.employeeunkid AS REId " & _
                   "	FROM hrassessor_tran " & _
                   "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                   "	WHERE hrassessor_master.isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                   ") AS REV ON REV.REId = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= "WHERE hremployee_master.isapproved = 1 "

            If mstrAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAllocationIds.Replace(iJoinTblName, "Alloc").Replace("classesunkid", "classunkid")
            End If

            If blnIsIncludeInactiveEmp = False Then

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDate))
            End If

            'Shani (24-May-2016) -- Start
            'Dim objCmpuMst As New clsassess_computation_master
            'Dim mblnIsSelfIncluded As Boolean = False
            'Dim mblnIsAssrIncluded As Boolean = False
            'Dim mblnIsRevrIncluded As Boolean = False
            'Dim xFormulaTable As DataTable = Nothing

            'xFormulaTable = objCmpuMst.Get_Computation_TranVariables(mintPeriodId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE)
            'If xFormulaTable.Rows.Count > 0 Then
            '    Dim xMatchValue As String = String.Empty
            '    For Each eValue As enAssess_Computation_Formulas In [Enum].GetValues(GetType(enAssess_Computation_Formulas))
            '        For Each xRow As DataRow In xFormulaTable.Rows
            '            xMatchValue = "1000" & eValue
            '            Select Case eValue
            '                Case enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, enAssess_Computation_Formulas.EMP_OVERALL_SCORE
            '                    If xRow.Item("computation_typeid") = xMatchValue Then
            '                        mblnIsSelfIncluded = True
            '                    End If
            '                Case enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, enAssess_Computation_Formulas.ASR_OVERALL_SCORE
            '                    If xRow.Item("computation_typeid") = xMatchValue Then
            '                        mblnIsAssrIncluded = True
            '                    End If
            '                Case enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, enAssess_Computation_Formulas.REV_OVERALL_SCORE
            '                    If xRow.Item("computation_typeid") = xMatchValue Then
            '                        mblnIsRevrIncluded = True
            '                    End If
            '            End Select
            '        Next
            '    Next
            'End If
            'objCmpuMst = Nothing

            'dsList = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Dim xEvaluation As New clsevaluation_analysis_master
            'For Each dtRow As DataRow In dsList.Tables("List").Rows
            '    Dim xOverallScore As Decimal = 0

            '    If mblnIsSelfIncluded Then
            '        xOverallScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dtRow.Item("EId"), dtRow.Item("PId"), , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '        xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dtRow.Item("EId"), dtRow.Item("PId"), , , , , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '    End If

            '    If mblnIsAssrIncluded Then
            '        xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dtRow.Item("EId"), dtRow.Item("PId"), , , dtRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '        xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dtRow.Item("EId"), dtRow.Item("PId"), , , dtRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '    End If

            '    If mblnIsRevrIncluded Then
            '        xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dtRow.Item("EId"), dtRow.Item("PId"), , , dtRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '        xOverallScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, mintScoringOptionId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, dtEmployeeAsOnDate, dtRow.Item("EId"), dtRow.Item("PId"), , , dtRow.Item("AsrMstId"), , blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '    End If

            '    dtRow.Item("Score") = CDec(Format(CDec(xOverallScore), "#####0.#0"))
            'Next
            'xEvaluation = Nothing

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strEmpids = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("EId").ToString).ToArray)

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'Dim dtComputataion As DataTable = (New clsComputeScore_master).GetComputeScore(strEmpids, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
            Dim dtComputataion As DataTable = Nothing
            If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                dtComputataion = (New clsComputeScore_master).GetComputeScore(strEmpids, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, False)
            ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                dtComputataion = (New clsComputeScore_master).GetComputeScore(strEmpids, mintPeriodId, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, mblnIsCalibrationSettingActive)
            End If
            'S.SANDEEP |27-MAY-2019| -- END

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                Dim xOverallScore As Decimal = 0
                Dim dRow As DataRow = dtRow
                If dtComputataion IsNot Nothing AndAlso dtComputataion.Rows.Count > 0 Then
                    Dim xScore = dtComputataion.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dRow("EId").ToString)) _
                                                             .Select(Function(x) x.Field(Of Decimal)("finaloverallscore"))
                    If xScore.Count > 0 Then
                        xOverallScore = xScore(0)
                    End If
                End If
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dtRow.Item("Score") = CDec(Format(CDec(xOverallScore), "#####0.#0"))
                dtRow.Item("Score") = CDec(Format(CDec(xOverallScore), "#####################0.#0"))
                'S.SANDEEP |21-AUG-2019| -- END
            Next
            'Shani (24-May-2016) -- End

            dsList.Tables(0).Columns.Remove("EId")
            dsList.Tables(0).Columns.Remove("PId")
            dsList.Tables(0).Columns.Remove("AsrMstId")
            dsList.Tables(0).Columns.Remove("RevMstId")

            Dim dView As DataView = dsList.Tables(0).DefaultView

            dView.RowFilter = "Score > 0"

            dsList.Tables.RemoveAt(0) : dsList.Tables.Add(dView.ToTable)

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Employee_Score; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [21 AUG 2015] -- END



    Private Function Set_Chart_Data() As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            StrQ = "SELECT " & _
                   "   xAxis " & _
                   "  ,yAxis1 " & _
                   "  ,yAxis2 " & _
                   "  ,Col " & _
                   "FROM " & _
                   "( " & _
                   "	SELECT " & _
                   "			 @Probation AS xAxis " & _
                   "			,0 AS yAxis1 " & _
                   "			,CAST(0 AS DECIMAL(10,2)) AS yAxis2 " & _
                   "			,'prob' AS Col " & _
                   "			,-1 AS iSort " & _
                   "UNION " & _
                   "		SELECT " & _
                   "			 @NotRated AS xAxis " & _
                   "			,0 AS yAxis1 " & _
                   "			,CAST(0 AS DECIMAL(10,2)) AS yAxis2 " & _
                   "			,'nrate' AS Col " & _
                   "			,0 AS iSort " & _
                   "UNION " & _
                   "		SELECT " & _
                   "			 CAST(score_from AS NVARCHAR(MAX)) + ' - ' + CAST(score_to AS NVARCHAR(MAX)) AS xAxis " & _
                   "			,0 AS yAxis1 " & _
                   "			,CAST(0 AS DECIMAL(10,2)) AS yAxis2 " & _
                   "			,'Column_'+ CAST(appratingunkid AS NVARCHAR(MAX)) AS Col " & _
                   "			,appratingunkid AS iSort " & _
                   "		FROM hrapps_ratings WHERE isvoid = 0 " & _
                   ") AS sChart ORDER BY iSort ASC "

            objData.AddParameter("@Probation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Probation"))
            objData.AddParameter("@NotRated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Not Rated"))

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            Dim iTotal As Decimal = CDec(mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item("total"))

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'For Each dRow As DataRow In dsList.Tables(0).Rows
            '    dRow.Item("yAxis1") = mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dRow.Item("Col"))
            '    If mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dRow.Item("Col")) > 0 Then
            '        dRow.Item("yAxis2") = Format(CDbl((mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dRow.Item("Col")) * 100) / iTotal), "###.#0")
            '    Else
            '        dRow.Item("yAxis2") = Format(CDbl(0), "###.#0")
            '    End If
            'Next
            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("yAxis1") = mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dRow.Item("Col"))
                If mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dRow.Item("Col")) > 0 Then
                    dRow.Item("yAxis2") = Format(CDbl((mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dRow.Item("Col")) * 100) / iTotal), "###############0.#0")
                Else
                    dRow.Item("yAxis2") = Format(CDbl(0), "###############0.#0")
                End If
            Next
            'S.SANDEEP |21-AUG-2019| -- END
            dsList.Tables("List").Columns.Remove("Col")

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Chart_Data; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Show_Analysis_Chart(ByVal strDatabaseName As String, _
                                        ByVal intUserUnkid As Integer, _
                                        ByVal intYearUnkid As Integer, _
                                        ByVal intCompanyUnkid As Integer, _
                                        ByVal dtPeriodStart As Date, _
                                        ByVal dtPeriodEnd As Date, _
                                        ByVal strUserModeSetting As String, _
                                        ByVal blnOnlyApproved As Boolean, _
                                        ByVal blnIsIncludeInactiveEmp As Boolean) As DataSet
        'Public Function Show_Analysis_Chart() As DataSet
        'S.SANDEEP [04 JUN 2015] -- END

        Dim dsData As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call CreateTable()

            Call CreateTable(strDatabaseName, _
                             intUserUnkid, _
                             intYearUnkid, _
                             intCompanyUnkid, _
                             dtPeriodStart, _
                             dtPeriodEnd, _
                             strUserModeSetting, _
                             blnOnlyApproved, _
                             blnIsIncludeInactiveEmp)
            'S.SANDEEP [04 JUN 2015] -- END

            mdtFinalTable.Columns.Remove("allocation")
            mdtFinalTable.Columns.Remove("grade")
            mdtFinalTable.Columns.Remove("avgscr")
            mdtFinalTable.Columns.Remove("staff")

            If mdtFinalTable.Rows.Count > 0 Then
                mdtFinalTable.Rows.Add(mdtFinalTable.NewRow)
                Dim mDecTotal As Decimal = 0
                For Each dCol As DataColumn In mdtFinalTable.Columns
                    mDecTotal = mdtFinalTable.Compute("SUM(" & dCol.ColumnName & ")", "")
                    mdtFinalTable.Rows(mdtFinalTable.Rows.Count - 1).Item(dCol.Ordinal) = mDecTotal
                    mDecTotal = 0
                Next
                dsData = Set_Chart_Data()
            End If

            Return dsData
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Show_Analysis_Chart; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER

    ''S.SANDEEP |27-JUL-2019| -- START
    ''ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    'Public Function GenerateChartData(ByVal intPeriodId As Integer, _
    '                                  ByVal xDatabaseName As String, _
    '                                  ByVal xUserUnkid As Integer, _
    '                                  ByVal xYearUnkid As Integer, _
    '                                  ByVal xCompanyUnkid As Integer, _
    '                                  ByVal xUserModeSetting As String, _
    '                                  ByVal xOnlyApproved As Boolean, _
    '                                  ByVal xPeriodStart As Date, _
    '                                  ByVal xPeriodEnd As Date, _
    '                                  ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                                  Optional ByVal strListName As String = "List", _
    '                                  Optional ByVal intEmployeeID As Integer = 0, _
    '                                  Optional ByVal blnReinstatementDate As Boolean = False, _
    '                                  Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
    '                                  Optional ByVal blnAddApprovalCondition As Boolean = True, _
    '                                  Optional ByVal eDisplayScore As clsComputeScore_master.enScoreMode = Nothing, _
    '                                  Optional ByVal mstrAdvanceFilter As String = "") As DataSet 'S.SANDEEP |22-OCT-2019| -- START {xPeriodStart,xPeriodEnd} -- END

    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim dtList As DataTable = Nothing
    '    Dim intTotalEmployee As Integer = 0

    '    'S.SANDEEP |22-OCT-2019| -- START
    '    'ISSUE/ENHANCEMENT : Calibration Issues
    '    If xPeriodStart = Nothing AndAlso xPeriodEnd = Nothing Then
    '        Dim objPrd As New clscommom_period_Tran
    '        objPrd._Periodunkid(xDatabaseName) = intPeriodId
    '        xPeriodStart = objPrd._Start_Date : xPeriodEnd = objPrd._End_Date
    '    End If
    '    'S.SANDEEP |22-OCT-2019| -- END

    '    Try
    '        'S.SANDEEP |14-AUG-2019| -- START
    '        'ISSUE/ENHANCEMENT : ERROR ON IN BLOCK QUERY
    '        'Dim objPrd As New clscommom_period_Tran
    '        'objPrd._Periodunkid(xDatabaseName) = intPeriodId
    '        'Dim xPeriodStart, xPeriodEnd As Date
    '        'xPeriodStart = objPrd._Start_Date : xPeriodEnd = objPrd._End_Date

    '        'Dim objEmp As New clsEmployee_Master
    '        'dsList = objEmp.GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strListName, False, intEmployeeID, , , , , , , , , , , , , , mstrInnFilter, blnReinstatementDate, blnIncludeAccessFilterQry, blnAddApprovalCondition)
    '        'objEmp = Nothing

    '        'dtList = New DataTable("List")

    '        'dtList.Columns.Add("particulars", GetType(System.String)).DefaultValue = ""

    '        'intTotalEmployee = dsList.Tables(0).Rows.Count
    '        'If intTotalEmployee > 0 Then
    '        '    strEmpIds = String.Join("),(", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
    '        '    If strEmpIds.Trim.Length > 0 Then strEmpIds = "(" & strEmpIds & ")"
    '        'Else
    '        '    strEmpIds = "(0)"
    '        'End If

    '        'Dim objRating As New clsAppraisal_Rating
    '        'dsList = objRating.getComboList("List", False, xDatabaseName)
    '        'If dsList.Tables(0).Rows.Count > 0 Then
    '        '    Dim dView As DataView = dsList.Tables(0).DefaultView
    '        '    dView.Sort = "scrt DESC"
    '        '    For Each viewRow As DataRowView In dView
    '        '        Dim dCol As New DataColumn
    '        '        dCol.ColumnName = viewRow.Item("id").ToString() 'viewRow.Item("name").ToString()
    '        '        dCol.DataType = GetType(System.String)
    '        '        dCol.DefaultValue = ""
    '        '        dtList.Columns.Add(dCol)
    '        '        dCol = Nothing
    '        '    Next
    '        'End If
    '        'objRating = Nothing

    '        'dtList.Rows.Add(dtList.NewRow()) 'ROW (0) FOR RATING
    '        'dtList.Rows.Add(dtList.NewRow()) 'ROW (1) FOR DISTRIBUTION
    '        'For Each iCol As DataColumn In dtList.Columns
    '        '    If IsNumeric(iCol.ColumnName) = True Then
    '        '        Dim dtmp() As DataRow = Nothing
    '        '        dtmp = dsList.Tables(0).Select("id = '" & iCol.ColumnName & "' ")
    '        '        If dtmp.Length > 0 Then
    '        '            dtList.Rows(0)(iCol.ColumnName) = dtmp(0)("name")
    '        '            dtList.Rows(1)(iCol.ColumnName) = Format(CDec(dtmp(0)("distribution")), "####0.#0") & " %"
    '        '        End If
    '        '    End If
    '        'Next

    '        'dtList.Rows(0)("particulars") = Language.getMessage(mstrModuleName, 200, "Rating")
    '        'dtList.Rows(1)("particulars") = Language.getMessage(mstrModuleName, 201, "Normal Distribution (Fixed distribution as per the format currently NMB are using)")

    '        'Dim blnIsCalibrationSettingActive As Boolean = False
    '        'If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '        '    blnIsCalibrationSettingActive = False
    '        'ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
    '        '    blnIsCalibrationSettingActive = True
    '        'End If

    '        'Using objDo As New clsDataOperation

    '        '    'StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & CInt(IIf(blnIsCalibrationSettingActive = True, 1, 0)) & " " & _
    '        '    '       "SELECT " & _
    '        '    '       "     employeeunkid " & _
    '        '    '       "    ,finaloverallscore " & _
    '        '    '       "    ,(SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to) AS eratid " & _
    '        '    '       "    ,(SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to) AS edist " & _
    '        '    '       "    ,(SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to) AS erate " & _
    '        '    '       "FROM " & _
    '        '    '       "( " & _
    '        '    '       "    SELECT DISTINCT " & _
    '        '    '       "         employeeunkid " & _
    '        '    '       "        ,CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
    '        '    '       "              WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
    '        '    '       "              WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
    '        '    '       "         END AS finaloverallscore " & _
    '        '    '       "    FROM hrassess_compute_score_master " & _
    '        '    '       "    WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' "

    '        '    'If intEmployeeID > 0 Then
    '        '    '    StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmployeeID & ") "
    '        '    'End If

    '        '    'StrQ &= ") AS A "

    '        '    'StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & CInt(IIf(blnIsCalibrationSettingActive = True, 1, 0)) & " " & _
    '        '    '       "SELECT " & _
    '        '    '       "     CAST(edist AS DECIMAL(36,2)) as np " & _
    '        '    '       "    ,erate " & _
    '        '    '       "    ,CAST((COUNT(1) * 100)/100 AS DECIMAL(36,2)) AS ap " & _
    '        '    '       "    ,COUNT(1) AS total " & _
    '        '    '       "    ,eratid " & _
    '        '    '       "FROM " & _
    '        '    '       "( " & _
    '        '    '       "    SELECT " & _
    '        '    '       "         ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),0)  AS eratid " & _
    '        '    '       "        ,ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),0) AS edist " & _
    '        '    '       "        ,ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),'') AS erate " & _
    '        '    '       "    FROM " & _
    '        '    '       "    ( " & _
    '        '    '       "        SELECT DISTINCT " & _
    '        '    '       "            CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
    '        '    '       "                 WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
    '        '    '       "                 WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
    '        '    '       "            END AS finaloverallscore " & _
    '        '    '       "        FROM hrassess_compute_score_master " & _
    '        '    '       "        WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' "
    '        '    'If intEmployeeID > 0 Then
    '        '    '    StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmployeeID & ") "
    '        '    'End If
    '        '    'StrQ &= "    ) AS A " & _
    '        '    '        ") AS B " & _
    '        '    '        "GROUP BY eratid,edist,erate "


    '        '    StrQ = "DECLARE @emp AS TABLE (empid int) " & _
    '        '           "INSERT INTO @emp VALUES  " & strEmpIds & " " & _
    '        '           "SELECT " & _
    '        '           "     appratingunkid AS eratid " & _
    '        '           "    ,distribution AS np " & _
    '        '           "    ,grade_award AS erate " & _
    '        '           "    ,ISNULL(C.ap,0) AS ap " & _
    '        '           "    ,ISNULL(C.total,0) AS total " & _
    '        '           "FROM hrapps_ratings " & _
    '        '           "LEFT JOIN " & _
    '        '           "( " & _
    '        '           "    SELECT " & _
    '        '           "         CAST(edist AS DECIMAL(36,2)) as np " & _
    '        '           "        ,erate " & _
    '        '           "        ,CAST((COUNT(1) * 100)/100 AS DECIMAL(36,2)) AS ap " & _
    '        '           "        ,COUNT(1) AS total " & _
    '        '           "        ,eratid " & _
    '        '           "        ,B.statusunkid " & _
    '        '           "    FROM " & _
    '        '           "    ( " & _
    '        '           "        SELECT " & _
    '        '           "             ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),0)  AS eratid " & _
    '        '           "            ,ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),0) AS edist " & _
    '        '           "            ,ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),'') AS erate " & _
    '        '           "            ,A.statusunkid " & _
    '        '           "        FROM " & _
    '        '           "        ( " & _
    '        '           "            SELECT " & _
    '        '           "                 calibrated_score " & _
    '        '           "                ,DENSE_RANK()OVER(PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
    '        '           "                ,statusunkid " & _
    '        '           "            FROM hrassess_computescore_approval_tran " & _
    '        '           "                JOIN @emp ON [@emp].empid = hrassess_computescore_approval_tran.employeeunkid " & _
    '        '           "            WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' "
    '        '    If intEmployeeID > 0 Then
    '        '        StrQ &= " AND hrassess_computescore_approval_tran.employeeunkid IN (" & intEmployeeID & ") "
    '        '    End If
    '        '    StrQ &= "       ) AS A " & _
    '        '            "       WHERE A.rno = 1 " & _
    '        '            "   ) AS B WHERE B.statusunkid <> 3 " & _
    '        '            "   GROUP BY eratid,edist,erate,statusunkid " & _
    '        '            ") AS C ON C.eratid = hrapps_ratings.appratingunkid " & _
    '        '            "WHERE isvoid = 0 "

    '        '    dsList = objDo.ExecQuery(StrQ, "List")

    '        '    If objDo.ErrorMessage <> "" Then
    '        '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '        '    End If

    '        '    If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '        '        dsList.Tables("List").Rows.Clear()
    '        '    End If

    '        '    If IsDBNull(dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("total"))) = True Or _
    '        '                dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("total")).Count <= 0 Then

    '        '        StrQ = "DECLARE @emp AS TABLE (empid int) " & _
    '        '               "INSERT INTO @emp VALUES " & strEmpIds & " " & _
    '        '               "SELECT " & _
    '        '               "     appratingunkid AS eratid " & _
    '        '               "    ,distribution AS np " & _
    '        '               "    ,grade_award AS erate " & _
    '        '               "    ,ISNULL(B.ap,0) AS ap " & _
    '        '               "    ,ISNULL(B.total,0) AS total " & _
    '        '               "FROM hrapps_ratings " & _
    '        '               "LEFT JOIN " & _
    '        '               "( " & _
    '        '               "    SELECT " & _
    '        '               "         CAST(edist AS DECIMAL(36,2)) as np " & _
    '        '               "        ,erate " & _
    '        '               "        ,CAST((COUNT(1) * 100)/100 AS DECIMAL(36,2)) AS ap " & _
    '        '               "        ,COUNT(1) AS total " & _
    '        '               "        ,eratid " & _
    '        '               "    FROM " & _
    '        '               "    ( " & _
    '        '               "        SELECT DISTINCT " & _
    '        '               "             ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),0)  AS eratid " & _
    '        '               "            ,ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),0) AS edist " & _
    '        '               "            ,ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),'') AS erate " & _
    '        '               "            ,employeeunkid " & _
    '        '               "        FROM hrassess_compute_score_master " & _
    '        '               "            JOIN @emp ON [@emp].empid = hrassess_compute_score_master.employeeunkid " & _
    '        '               "        WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' "
    '        '        If intEmployeeID > 0 Then
    '        '            StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmployeeID & ") "
    '        '        End If
    '        '        StrQ &= "   ) AS A " & _
    '        '                "   GROUP BY eratid,edist,erate " & _
    '        '                ") AS B ON B.eratid = hrapps_ratings.appratingunkid " & _
    '        '                "WHERE isvoid = 0 "

    '        '        dsList = objDo.ExecQuery(StrQ, "List")

    '        '        If objDo.ErrorMessage <> "" Then
    '        '            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '        '        End If

    '        '        Dim dCol As New DataColumn
    '        '        With dCol
    '        '            .ColumnName = "litem"
    '        '            .DataType = GetType(System.String)
    '        '            .DefaultValue = Language.getMessage(mstrModuleName, 206, "(Provisional)")
    '        '        End With
    '        '        dsList.Tables("List").Columns.Add(dCol)

    '        '        If eDisplayScore = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
    '        '            dsList.Tables("List").Rows.Clear()
    '        '        End If

    '        '    Else
    '        '        Dim dCol As New DataColumn
    '        '        With dCol
    '        '            .ColumnName = "litem"
    '        '            .DataType = GetType(System.String)
    '        '            .DefaultValue = Language.getMessage(mstrModuleName, 205, "(Calibrated)")
    '        '        End With
    '        '        dsList.Tables("List").Columns.Add(dCol)
    '        '    End If

    '        'End Using

    '        'Dim dr As DataRow = dtList.NewRow()
    '        'Dim dp As DataRow = dtList.NewRow()
    '        'For Each iCol As DataColumn In dtList.Columns
    '        '    If IsNumeric(iCol.ColumnName) = True Then
    '        '        Dim dtmp() As DataRow = Nothing
    '        '        dtmp = dsList.Tables(0).Select("eratid = '" & iCol.ColumnName & "' ")
    '        '        If dtmp.Length > 0 Then
    '        '            dr.Item(iCol.ColumnName) = dtmp(0)("total")
    '        '            dp.Item(iCol.ColumnName) = dtmp(0)("ap") & " %"
    '        '        Else
    '        '            dr.Item(iCol.ColumnName) = 0
    '        '            dp.Item(iCol.ColumnName) = "0.00 %"
    '        '        End If
    '        '    End If
    '        'Next
    '        'dr.Item("particulars") = Language.getMessage(mstrModuleName, 202, "Total Calibrated")
    '        'dp.Item("particulars") = Language.getMessage(mstrModuleName, 203, "Actual Performance (Aruti provision rating or calibrated rating )")
    '        'dtList.Rows.InsertAt(dr, 0)
    '        'dtList.Rows.InsertAt(dp, dtList.Rows.Count)

    '        'dr = dtList.NewRow()
    '        'dr("particulars") = Language.getMessage(mstrModuleName, 204, "Total Employee")
    '        'dr(1) = intTotalEmployee
    '        'dtList.Rows.InsertAt(dr, 0)

    '        'Dim dsChart As New DataSet : dsChart = dsList.Copy()

    '        'dsList = New DataSet
    '        'dsList.Tables.Add(dtList.Copy)

    '        'dtList = New DataTable("Chart")

    '        'dsChart.Tables(0).TableName = "Chart"
    '        'dtList = dsChart.Tables(0).Copy()

    '        ''Using objDo As New clsDataOperation
    '        ''    Dim dsChart As New DataSet

    '        ''    StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & CInt(IIf(blnIsCalibrationSettingActive = True, 1, 0)) & " " & _
    '        ''           "SELECT " & _
    '        ''           "     CAST(edist AS DECIMAL(36,2)) as np " & _
    '        ''           "    ,erate " & _
    '        ''           "    ,CAST((COUNT(1) * 100)/100 AS DECIMAL(36,2)) AS ap " & _
    '        ''           "FROM " & _
    '        ''           "( " & _
    '        ''           "    SELECT " & _
    '        ''           "         ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),0)  AS eratid " & _
    '        ''           "        ,ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),0) AS edist " & _
    '        ''           "        ,ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to),'') AS erate " & _
    '        ''           "    FROM " & _
    '        ''           "    ( " & _
    '        ''           "        SELECT DISTINCT " & _
    '        ''           "            CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
    '        ''           "                 WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
    '        ''           "                 WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
    '        ''           "            END AS finaloverallscore " & _
    '        ''           "        FROM hrassess_compute_score_master " & _
    '        ''           "        WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' "
    '        ''    If intEmployeeID > 0 Then
    '        ''        StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmployeeID & ") "
    '        ''    End If
    '        ''    StrQ &= "    ) AS A " & _
    '        ''            ") AS B " & _
    '        ''            "GROUP BY eratid,edist,erate "

    '        ''    dsChart = objDo.ExecQuery(StrQ, "Chart")

    '        ''    If objDo.ErrorMessage <> "" Then
    '        ''        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '        ''    End If

    '        ''    dtList = dsChart.Tables(0).Copy()

    '        ''End Using

    '        'dsList.Tables.Add(dtList.Copy)


    '        'S.SANDEEP |22-OCT-2019| -- START
    '        'ISSUE/ENHANCEMENT : Calibration Issues
    '        'Dim objPrd As New clscommom_period_Tran
    '        'objPrd._Periodunkid(xDatabaseName) = intPeriodId
    '        'Dim xPeriodStart, xPeriodEnd As Date
    '        'xPeriodStart = objPrd._Start_Date : xPeriodEnd = objPrd._End_Date
    '        'S.SANDEEP |22-OCT-2019| -- END


    '        'Dim objEmp As New clsEmployee_Master
    '        'dsList = objEmp.GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strListName, False, intEmployeeID, , , , , , , , , , , , , , mstrInnFilter, blnReinstatementDate, blnIncludeAccessFilterQry, blnAddApprovalCondition)
    '        'objEmp = Nothing

    '        dtList = New DataTable("List")

    '        dtList.Columns.Add("particulars", GetType(System.String)).DefaultValue = ""

    '        'intTotalEmployee = dsList.Tables(0).Rows.Count

    '        Dim objRating As New clsAppraisal_Rating
    '        dsList = objRating.getComboList("List", False, xDatabaseName)
    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            Dim dView As DataView = dsList.Tables(0).DefaultView
    '            dView.Sort = "scrt DESC"
    '            For Each viewRow As DataRowView In dView
    '                Dim dCol As New DataColumn
    '                dCol.ColumnName = viewRow.Item("id").ToString() 'viewRow.Item("name").ToString()
    '                dCol.DataType = GetType(System.String)
    '                dCol.DefaultValue = ""
    '                dtList.Columns.Add(dCol)
    '                dCol = Nothing
    '            Next
    '        End If
    '        objRating = Nothing

    '        'S.SANDEEP |26-AUG-2019| -- START
    '        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '        Dim Col As New DataColumn
    '        Col.ColumnName = "objColRowType"
    '        Col.DataType = GetType(System.String)
    '        Col.DefaultValue = ""
    '        dtList.Columns.Add(Col)
    '        Col = Nothing
    '        'S.SANDEEP |26-AUG-2019| -- END

    '        dtList.Rows.Add(dtList.NewRow()) 'ROW (0) FOR RATING
    '        dtList.Rows.Add(dtList.NewRow()) 'ROW (1) FOR DISTRIBUTION
    '        For Each iCol As DataColumn In dtList.Columns
    '            If IsNumeric(iCol.ColumnName) = True Then
    '                Dim dtmp() As DataRow = Nothing
    '                dtmp = dsList.Tables(0).Select("id = '" & iCol.ColumnName & "' ")
    '                If dtmp.Length > 0 Then
    '                    dtList.Rows(0)(iCol.ColumnName) = dtmp(0)("name")
    '                    'S.SANDEEP |21-AUG-2019| -- START
    '                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
    '                    'dtList.Rows(1)(iCol.ColumnName) = Format(CDec(dtmp(0)("distribution")), "####0.#0") & " %"
    '                    dtList.Rows(1)(iCol.ColumnName) = Format(CDec(dtmp(0)("distribution")), "##################0.#0") & " %"
    '                    'S.SANDEEP |21-AUG-2019| -- END
    '                End If
    '            End If
    '        Next

    '        dtList.Rows(0)("particulars") = Language.getMessage(mstrModuleName, 200, "Rating")
    '        dtList.Rows(1)("particulars") = Language.getMessage(mstrModuleName, 201, "Normal Distribution (Fixed distribution as per the format currently NMB are using)")
    '        'S.SANDEEP |26-AUG-2019| -- START
    '        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '        dtList.Rows(1)("objColRowType") = "N"
    '        'S.SANDEEP |26-AUG-2019| -- END

    '        Dim blnIsCalibrationSettingActive As Boolean = False
    '        If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '            blnIsCalibrationSettingActive = False
    '        ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
    '            blnIsCalibrationSettingActive = True
    '        End If

    '        Using objDo As New clsDataOperation

    '            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, False, xDatabaseName)
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)
    '            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
    '            Dim iStrQ As String = _
    '                "SELECT " & _
    '                "   hremployee_master.employeeunkid " & _
    '                "FROM hremployee_master "
    '            If xAdvanceJoinQry.Trim.Length > 0 Then
    '                iStrQ &= xAdvanceJoinQry
    '            End If
    '            iStrQ &= "LEFT JOIN " & _
    '                     "( " & _
    '                     "    SELECT " & _
    '                     "         stationunkid " & _
    '                     "        ,deptgroupunkid " & _
    '                     "        ,departmentunkid " & _
    '                     "        ,sectiongroupunkid " & _
    '                     "        ,sectionunkid " & _
    '                     "        ,unitgroupunkid " & _
    '                     "        ,unitunkid " & _
    '                     "        ,teamunkid " & _
    '                     "        ,classgroupunkid " & _
    '                     "        ,classunkid " & _
    '                     "        ,employeeunkid " & _
    '                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
    '                     "    FROM hremployee_transfer_tran " & _
    '                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                     ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
    '                     "LEFT JOIN " & _
    '                     "( " & _
    '                     "    SELECT " & _
    '                     "         jobgroupunkid " & _
    '                     "        ,jobunkid " & _
    '                     "        ,employeeunkid " & _
    '                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
    '                     "    FROM hremployee_categorization_tran " & _
    '                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                     ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
    '                     "LEFT JOIN " & _
    '                     "( " & _
    '                     "    SELECT " & _
    '                     "         cctranheadvalueid AS costcenterunkid " & _
    '                     "        ,employeeunkid " & _
    '                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
    '                     "    FROM hremployee_cctranhead_tran " & _
    '                     "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                     ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
    '                     "LEFT JOIN " & _
    '                     "( " & _
    '                     "	SELECT " & _
    '                     "		 gradegroupunkid " & _
    '                     "		,gradeunkid " & _
    '                     "		,gradelevelunkid " & _
    '                     "		,employeeunkid " & _
    '                     "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
    '                     "	FROM prsalaryincrement_tran " & _
    '                     "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                     ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
    '                     "LEFT JOIN " & _
    '                     "( " & _
    '                     "   SELECT " & _
    '                     "        hremployee_shift_tran.employeeunkid " & _
    '                     "       ,hremployee_shift_tran.shiftunkid " & _
    '                     "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
    '                     "   FROM hremployee_shift_tran " & _
    '                     "   WHERE hremployee_shift_tran.isvoid = 0 " & _
    '                     "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                     ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "

    '            If xDateJoinQry.Trim.Length > 0 Then
    '                iStrQ &= xDateJoinQry
    '            End If

    '            If blnIncludeAccessFilterQry = True Then
    '                If xUACQry.Trim.Length > 0 Then
    '                    iStrQ &= xUACQry
    '                End If
    '            End If

    '            If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '                iStrQ &= "JOIN " & _
    '                         "( " & _
    '                         "   SELECT DISTINCT " & _
    '                         "        A.employeeunkid " & _
    '                         "   FROM " & _
    '                         "   ( " & _
    '                         "       SELECT " & _
    '                         "            hrassess_compute_score_master.employeeunkid " & _
    '                         "           ,hrassess_compute_score_master.finaloverallscore " & _
    '                         "           ,hrassess_compute_score_master.periodunkid " & _
    '                         "           ,ROW_NUMBER() OVER (PARTITION BY hrassess_compute_score_master.employeeunkid ,hrassess_compute_score_master.periodunkid ORDER BY employeeunkid ,finaloverallscore DESC) AS rno " & _
    '                         "       FROM hrassess_compute_score_master " & _
    '                         "           JOIN hrevaluation_analysis_master ON hrassess_compute_score_master.analysisunkid = hrevaluation_analysis_master.analysisunkid AND hrevaluation_analysis_master.isvoid = 0 " & _
    '                         "       WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_master.periodunkid = '" & intPeriodId & "' " & _
    '                         "   ) AS A " & _
    '                         "   WHERE A.rno = 1 " & _
    '                         ") AS P ON P.employeeunkid = hremployee_master.employeeunkid "

    '            ElseIf eDisplayScore = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
    '                iStrQ &= "JOIN " & _
    '                         "( " & _
    '                         "   SELECT DISTINCT " & _
    '                         "       A.employeeunkid " & _
    '                         "   FROM " & _
    '                         "   ( " & _
    '                         "       SELECT " & _
    '                         "            calibrated_score " & _
    '                         "           ,DENSE_RANK() OVER (PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
    '                         "            ,statusunkid " & _
    '                         "           ,employeeunkid " & _
    '                         "       FROM hrassess_computescore_approval_tran " & _
    '                         "       WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' " & _
    '                         "    ) AS A " & _
    '                         "    WHERE A.rno = 1 AND A.statusunkid <> 3 " & _
    '                         ") AS P ON P.employeeunkid = hremployee_master.employeeunkid "
    '                'S.SANDEEP |22-NOV-2019| -- START {REMOVED : SELECT | ADDED : SELECT DISTINCT} -- END
    '            End If

    '            iStrQ &= " WHERE 1=1 "
    '            If intEmployeeID > 0 Then
    '                iStrQ &= " AND hremployee_master.employeeunkid = " & intEmployeeID & ""
    '            End If

    '            If xDataFilterQry.Trim.Length > 0 Then
    '                iStrQ &= xDataFilterQry
    '            End If

    '            If mstrAdvanceFilter.Trim.Length > 0 Then
    '                iStrQ &= " AND " & mstrAdvanceFilter
    '            End If

    '            'S.SANDEEP |21-SEP-2019| -- START
    '            'ISSUE/ENHANCEMENT : {HPO Chart Issue}
    '            iStrQ &= " AND hremployee_master.employeeunkid IN (SELECT CASE WHEN selfemployeeunkid <= 0 THEN assessedemployeeunkid ELSE selfemployeeunkid END FROM hrevaluation_analysis_master WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "') "
    '            'S.SANDEEP |21-SEP-2019| -- END

    '            objDo.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
    '            objDo.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

    '            If mstrInnFilter.Trim <> "" Then
    '                iStrQ &= " AND " & mstrInnFilter & " "
    '            End If


    '            'S.SANDEEP |21-SEP-2019| -- START
    '            'ISSUE/ENHANCEMENT : {HPO Chart Issue}
    '            dsList = objDo.ExecQuery(iStrQ, "List")

    '            If objDo.ErrorMessage <> "" Then
    '                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '            End If

    '            intTotalEmployee = dsList.Tables(0).Rows.Count

    '            dsList = New DataSet()
    '            'S.SANDEEP |21-SEP-2019| -- END

    '            StrQ = "DECLARE @emp AS TABLE (empid int) " & _
    '                   " INSERT INTO @emp (empid) " & iStrQ & " " & _
    '                   "SELECT " & _
    '                   "     appratingunkid AS eratid " & _
    '                   "    ,distribution AS np " & _
    '                   "    ,grade_award AS erate " & _
    '                   "    ,ISNULL(C.ap,0) AS ap " & _
    '                   "    ,ISNULL(C.total,0) AS total " & _
    '                   "FROM hrapps_ratings " & _
    '                   "LEFT JOIN " & _
    '                   "( " & _
    '                   "    SELECT " & _
    '                   "         CAST(edist AS DECIMAL(36,2)) as np " & _
    '                   "        ,erate " & _
    '                   "        ,CASE WHEN " & intTotalEmployee & "<= 0 THEN 0 ELSE CAST((CAST(COUNT(1) AS FLOAT) / CAST(" & intTotalEmployee & " AS FLOAT)) * 100 AS DECIMAL(36,2)) END AS ap " & _
    '                   "        ,COUNT(DISTINCT employeeunkid) AS total " & _
    '                   "        ,eratid " & _
    '                   "        /*,B.statusunkid*/ " & _
    '                   "    FROM " & _
    '                   "    ( " & _
    '                   "        SELECT " & _
    '                   "             ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),0)  AS eratid " & _
    '                   "            ,ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),0) AS edist " & _
    '                   "            ,ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),'') AS erate " & _
    '                   "            ,A.statusunkid " & _
    '                   "            ,A.employeeunkid " & _
    '                   "        FROM " & _
    '                   "        ( " & _
    '                   "            SELECT " & _
    '                   "                 calibrated_score " & _
    '                   "                ,DENSE_RANK()OVER(PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
    '                   "                ,statusunkid " & _
    '                   "                ,employeeunkid " & _
    '                   "            FROM hrassess_computescore_approval_tran " & _
    '                   "                JOIN @emp ON [@emp].empid = hrassess_computescore_approval_tran.employeeunkid " & _
    '                   "            WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' "
    '            If intEmployeeID > 0 Then
    '                StrQ &= " AND hrassess_computescore_approval_tran.employeeunkid IN (" & intEmployeeID & ") "
    '            End If
    '            StrQ &= "       ) AS A " & _
    '                    "       WHERE A.rno = 1 " & _
    '                    "   ) AS B WHERE B.statusunkid <> 3 " & _
    '                    "   GROUP BY eratid,edist,erate/*,statusunkid*/ " & _
    '                    ") AS C ON C.eratid = hrapps_ratings.appratingunkid " & _
    '                    "WHERE isvoid = 0 "

    '            dsList = objDo.ExecQuery(StrQ, "List")

    '            If objDo.ErrorMessage <> "" Then
    '                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '            End If

    '            If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '                dsList.Tables("List").Columns.Remove("ap") : dsList.Tables("List").Columns.Remove("total")
    '                Dim xCol As New DataColumn
    '                xCol = New DataColumn
    '                With xCol
    '                    .ColumnName = "ap"
    '                    .DataType = GetType(System.Decimal)
    '                    .DefaultValue = 0
    '                End With
    '                dsList.Tables("List").Columns.Add(xCol)
    '                xCol = New DataColumn
    '                With xCol
    '                    .ColumnName = "total"
    '                    .DataType = GetType(System.Int32)
    '                    .DefaultValue = 0
    '                End With
    '                dsList.Tables("List").Columns.Add(xCol)

    '            End If

    '            If IsDBNull(dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("total"))) = True Or _
    '                        dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("total")).Count <= 0 Then

    '                StrQ = "DECLARE @emp AS TABLE (empid int) " & _
    '                       " INSERT INTO @emp (empid) " & iStrQ & " " & _
    '                       "SELECT " & _
    '                       "     appratingunkid AS eratid " & _
    '                       "    ,distribution AS np " & _
    '                       "    ,grade_award AS erate " & _
    '                       "    ,ISNULL(B.ap,0) AS ap " & _
    '                       "    ,ISNULL(B.total,0) AS total " & _
    '                       "FROM hrapps_ratings " & _
    '                       "LEFT JOIN " & _
    '                       "( " & _
    '                       "    SELECT " & _
    '                       "         CAST(edist AS DECIMAL(36,2)) as np " & _
    '                       "        ,erate " & _
    '                       "        ,CASE WHEN " & intTotalEmployee & "<= 0 THEN 0 ELSE CAST((CAST(COUNT(1) AS FLOAT) / CAST(" & intTotalEmployee & " AS FLOAT)) * 100 AS DECIMAL(36,2)) END AS ap " & _
    '                       "        ,COUNT(1) AS total " & _
    '                       "        ,eratid " & _
    '                       "    FROM " & _
    '                       "    ( " & _
    '                       "        SELECT DISTINCT " & _
    '                       "             CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
    '                       "                ELSE ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), 0) END AS eratid " & _
    '                       "            ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
    '                       "                ELSE ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), 0) END AS edist " & _
    '                       "            ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
    '                       "                ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END AS erate " & _
    '                       "            ,hrassess_compute_score_master.employeeunkid " & _
    '                       "            ,ROW_NUMBER()OVER(PARTITION BY hrassess_compute_score_master.employeeunkid,hrassess_compute_score_master.periodunkid ORDER BY hrassess_compute_score_master.employeeunkid,finaloverallscore DESC) AS rno " & _
    '                       "        FROM hrassess_compute_score_master " & _
    '                       "            JOIN @emp ON [@emp].empid = hrassess_compute_score_master.employeeunkid " & _
    '                       "            JOIN hrevaluation_analysis_master ON hrassess_compute_score_master.analysisunkid = hrevaluation_analysis_master.analysisunkid AND hrevaluation_analysis_master.isvoid = 0 " & _
    '                       "        WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_master.periodunkid = '" & intPeriodId & "' "
    '                If intEmployeeID > 0 Then
    '                    StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmployeeID & ") "
    '                End If
    '                StrQ &= "   ) AS A WHERE A.rno = 1 " & _
    '                        "   GROUP BY eratid,edist,erate " & _
    '                        ") AS B ON B.eratid = hrapps_ratings.appratingunkid " & _
    '                        "WHERE isvoid = 0 "

    '                'S.SANDEEP |21-SEP-2019| -- START
    '                'ISSUE/ENHANCEMENT : {HPO Chart Issue}
    '                '---------- REMOVED
    '                'CAST(COUNT(1)*100)/100
    '                '---------- ADDED  
    '                '{,ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid ORDER BY employeeunkid,finaloverallscore DESC) AS rno }  
    '                '{ WHERE A.rno = 1 }

    '                'S.SANDEEP |21-SEP-2019| -- END

    '                dsList = objDo.ExecQuery(StrQ, "List")

    '                If objDo.ErrorMessage <> "" Then
    '                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '                End If

    '                Dim dCol As New DataColumn
    '                With dCol
    '                    .ColumnName = "litem"
    '                    .DataType = GetType(System.String)
    '                    .DefaultValue = Language.getMessage(mstrModuleName, 206, "(Provisional)")
    '                End With
    '                dsList.Tables("List").Columns.Add(dCol)

    '                If eDisplayScore = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
    '                    dsList.Tables("List").Columns.Remove("ap") : dsList.Tables("List").Columns.Remove("total") : dsList.Tables("List").Columns.Remove("litem")
    '                    dCol = New DataColumn
    '                    With dCol
    '                        .ColumnName = "ap"
    '                        .DataType = GetType(System.Decimal)
    '                        .DefaultValue = 0
    '                    End With
    '                    dsList.Tables("List").Columns.Add(dCol)
    '                    dCol = New DataColumn
    '                    With dCol
    '                        .ColumnName = "total"
    '                        .DataType = GetType(System.Int32)
    '                        .DefaultValue = 0
    '                    End With
    '                    dsList.Tables("List").Columns.Add(dCol)

    '                    dCol = New DataColumn
    '                    With dCol
    '                        .ColumnName = "litem"
    '                        .DataType = GetType(System.String)
    '                        .DefaultValue = Language.getMessage(mstrModuleName, 205, "(Calibrated)")
    '                    End With
    '                    dsList.Tables("List").Columns.Add(dCol)
    '                End If
    '            Else
    '                Dim dCol As New DataColumn
    '                With dCol
    '                    .ColumnName = "litem"
    '                    .DataType = GetType(System.String)
    '                    .DefaultValue = Language.getMessage(mstrModuleName, 205, "(Calibrated)")
    '                End With
    '                dsList.Tables("List").Columns.Add(dCol)
    '            End If

    '        End Using

    '        Dim dr As DataRow = dtList.NewRow()
    '        Dim dp As DataRow = dtList.NewRow()
    '        For Each iCol As DataColumn In dtList.Columns
    '            If IsNumeric(iCol.ColumnName) = True Then
    '                Dim dtmp() As DataRow = Nothing
    '                dtmp = dsList.Tables(0).Select("eratid = '" & iCol.ColumnName & "' ")
    '                If dtmp.Length > 0 Then
    '                    dr.Item(iCol.ColumnName) = dtmp(0)("total")
    '                    dp.Item(iCol.ColumnName) = dtmp(0)("ap") & " %"
    '                Else
    '                    dr.Item(iCol.ColumnName) = 0
    '                    dp.Item(iCol.ColumnName) = "0.00 %"
    '                End If
    '            End If
    '        Next
    '        'If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '        '    dr.Item("particulars") = Language.getMessage(mstrModuleName, 313, "Total Provisional")
    '        'Else
    '        '    dr.Item("particulars") = Language.getMessage(mstrModuleName, 202, "Total Calibrated")
    '        'End If

    '        dr.Item("particulars") = Language.getMessage(mstrModuleName, 317, "Employee Distribution")
    '        dp.Item("particulars") = Language.getMessage(mstrModuleName, 203, "Actual Performance (Aruti provision rating or calibrated rating )")
    '        'S.SANDEEP |26-AUG-2019| -- START
    '        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '        dp.Item("objColRowType") = "A"
    '        'S.SANDEEP |26-AUG-2019| -- END
    '        dtList.Rows.InsertAt(dr, 0)
    '        dtList.Rows.InsertAt(dp, dtList.Rows.Count)

    '        dr = dtList.NewRow()

    '        'dr("particulars") = Language.getMessage(mstrModuleName, 204, "Total Employee")
    '        If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
    '            dr("particulars") = Language.getMessage(mstrModuleName, 315, "Total number of employees with provision rating")
    '        Else
    '            dr("particulars") = Language.getMessage(mstrModuleName, 316, "Total number of employees with calibration rating")
    '        End If

    '        dr(1) = intTotalEmployee
    '        dtList.Rows.InsertAt(dr, 0)

    '        Dim dsChart As New DataSet : dsChart = dsList.Copy()

    '        dsList = New DataSet
    '        dsList.Tables.Add(dtList.Copy)

    '        dtList = New DataTable("Chart")

    '        dsChart.Tables(0).TableName = "Chart"
    '        dtList = dsChart.Tables(0).Copy()

    '        dsList.Tables.Add(dtList.Copy)
    '        'S.SANDEEP |14-AUG-2019| -- END

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GenerateChartData; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList
    'End Function
    ''S.SANDEEP |27-JUL-2019| -- END

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="intViewType">0 = ALL, 1 = Calibrator, 2 = Approver</param>   
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GenerateChartData(ByVal intPeriodId As Integer, _
                                      ByVal xDatabaseName As String, _
                                      ByVal xUserUnkid As Integer, _
                                      ByVal xYearUnkid As Integer, _
                                      ByVal xCompanyUnkid As Integer, _
                                      ByVal xUserModeSetting As String, _
                                      ByVal xOnlyApproved As Boolean, _
                                      ByVal xPeriodStart As Date, _
                                      ByVal xPeriodEnd As Date, _
                                      ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                      ByVal intViewType As Integer, _
                                      Optional ByVal strListName As String = "List", _
                                      Optional ByVal intEmployeeID As Integer = 0, _
                                      Optional ByVal blnReinstatementDate As Boolean = False, _
                                      Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                      Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                      Optional ByVal eDisplayScore As clsComputeScore_master.enScoreMode = Nothing, _
                                      Optional ByVal mstrAdvanceFilter As String = "") As DataSet 'S.SANDEEP |22-OCT-2019| -- START {xPeriodStart,xPeriodEnd} -- END

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim dtList As DataTable = Nothing
        Dim intTotalEmployee As Integer = 0

        If xPeriodStart = Nothing AndAlso xPeriodEnd = Nothing Then
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(xDatabaseName) = intPeriodId
            xPeriodStart = objPrd._Start_Date : xPeriodEnd = objPrd._End_Date
        End If

        Try
            dtList = New DataTable("List")
            dtList.Columns.Add("particulars", GetType(System.String)).DefaultValue = ""
            Dim objRating As New clsAppraisal_Rating
            dsList = objRating.getComboList("List", False, xDatabaseName)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dView As DataView = dsList.Tables(0).DefaultView
                dView.Sort = "scrt DESC"
                For Each viewRow As DataRowView In dView
                    Dim dCol As New DataColumn
                    dCol.ColumnName = viewRow.Item("id").ToString() 'viewRow.Item("name").ToString()
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    dtList.Columns.Add(dCol)
                    dCol = Nothing
                Next
            End If
            objRating = Nothing

            Dim Col As New DataColumn
            Col.ColumnName = "objColRowType"
            Col.DataType = GetType(System.String)
            Col.DefaultValue = ""
            dtList.Columns.Add(Col)
            Col = Nothing

            dtList.Rows.Add(dtList.NewRow()) 'ROW (0) FOR RATING
            dtList.Rows.Add(dtList.NewRow()) 'ROW (1) FOR DISTRIBUTION
            For Each iCol As DataColumn In dtList.Columns
                If IsNumeric(iCol.ColumnName) = True Then
                    Dim dtmp() As DataRow = Nothing
                    dtmp = dsList.Tables(0).Select("id = '" & iCol.ColumnName & "' ")
                    If dtmp.Length > 0 Then
                        dtList.Rows(0)(iCol.ColumnName) = dtmp(0)("name")
                        dtList.Rows(1)(iCol.ColumnName) = Format(CDec(dtmp(0)("distribution")), "##################0.#0") & " %"
                    End If
                End If
            Next

            dtList.Rows(0)("particulars") = Language.getMessage(mstrModuleName, 200, "Rating")
            dtList.Rows(1)("particulars") = Language.getMessage(mstrModuleName, 201, "Normal Distribution (Fixed distribution as per the format currently NMB are using)")
            dtList.Rows(1)("objColRowType") = "N"

            Dim blnIsCalibrationSettingActive As Boolean = False
            If mintDisplayScoreType = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                blnIsCalibrationSettingActive = False
            ElseIf mintDisplayScoreType = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                blnIsCalibrationSettingActive = True
            End If

            Using objDo As New clsDataOperation

                Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, False, xDatabaseName)
                If intViewType = 0 Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
                Dim iStrQ As String = _
                    "SELECT " & _
                    "   hremployee_master.employeeunkid " & _
                    "FROM hremployee_master "
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    iStrQ &= xAdvanceJoinQry
                End If
                iStrQ &= "LEFT JOIN " & _
                         "( " & _
                         "    SELECT " & _
                         "         stationunkid " & _
                         "        ,deptgroupunkid " & _
                         "        ,departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,unitgroupunkid " & _
                         "        ,unitunkid " & _
                         "        ,teamunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                         "    FROM hremployee_transfer_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                         ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                         "LEFT JOIN " & _
                         "( " & _
                         "    SELECT " & _
                         "         jobgroupunkid " & _
                         "        ,jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                         ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                         "LEFT JOIN " & _
                         "( " & _
                         "    SELECT " & _
                         "         cctranheadvalueid AS costcenterunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                         "    FROM hremployee_cctranhead_tran " & _
                         "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                         ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                         "LEFT JOIN " & _
                         "( " & _
                         "	SELECT " & _
                         "		 gradegroupunkid " & _
                         "		,gradeunkid " & _
                         "		,gradelevelunkid " & _
                         "		,employeeunkid " & _
                         "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                         "	FROM prsalaryincrement_tran " & _
                         "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                         ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                         "LEFT JOIN " & _
                         "( " & _
                         "   SELECT " & _
                         "        hremployee_shift_tran.employeeunkid " & _
                         "       ,hremployee_shift_tran.shiftunkid " & _
                         "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                         "   FROM hremployee_shift_tran " & _
                         "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                         "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                         ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    iStrQ &= xDateJoinQry
                End If

                If blnIncludeAccessFilterQry = True Then
                    If xUACQry.Trim.Length > 0 Then
                        iStrQ &= xUACQry
                    End If
                End If

                If intViewType <> 0 Then
                    iStrQ &= "JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        CT.employeeunkid " & _
                            "   FROM hrscore_calibration_approver_master AS AC " & _
                            "       JOIN hrscore_calibration_approver_tran AS CT ON AC.mappingunkid = CT.mappingunkid " & _
                            "   WHERE AC.isvoid = 0 AND CT.isvoid = 0 " & _
                            "   AND AC.visibletypeid = 1 AND CT.visibletypeid = 1 "
                    Select Case intViewType
                        Case 1  'CALIBRATOR
                            iStrQ &= "  AND AC.mapuserunkid = '" & xUserUnkid & "' AND AC.iscalibrator = 1 "
                        Case 2  'APPROVER
                            iStrQ &= "  AND AC.mapuserunkid = '" & xUserUnkid & "' AND AC.iscalibrator = 0 "
                    End Select
                    iStrQ &= ") AS VT ON VT.employeeunkid = hremployee_master.employeeunkid "
                End If

                If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                    iStrQ &= "JOIN " & _
                             "( " & _
                             "   SELECT DISTINCT " & _
                             "        A.employeeunkid " & _
                             "   FROM " & _
                             "   ( " & _
                             "       SELECT " & _
                             "            hrassess_compute_score_master.employeeunkid " & _
                             "           ,hrassess_compute_score_master.finaloverallscore " & _
                             "           ,hrassess_compute_score_master.periodunkid " & _
                             "           ,ROW_NUMBER() OVER (PARTITION BY hrassess_compute_score_master.employeeunkid ,hrassess_compute_score_master.periodunkid ORDER BY employeeunkid ,finaloverallscore DESC) AS rno " & _
                             "       FROM hrassess_compute_score_master " & _
                             "           JOIN hrevaluation_analysis_master ON hrassess_compute_score_master.analysisunkid = hrevaluation_analysis_master.analysisunkid AND hrevaluation_analysis_master.isvoid = 0 " & _
                             "       WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_master.periodunkid = '" & intPeriodId & "' " & _
                             "   ) AS A " & _
                             "   WHERE A.rno = 1 " & _
                             ") AS P ON P.employeeunkid = hremployee_master.employeeunkid "

                ElseIf eDisplayScore = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                    iStrQ &= "JOIN " & _
                             "( " & _
                             "   SELECT DISTINCT " & _
                             "       A.employeeunkid " & _
                             "   FROM " & _
                             "   ( " & _
                             "       SELECT " & _
                             "            calibrated_score " & _
                             "           ,DENSE_RANK() OVER (PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
                             "            ,statusunkid " & _
                             "           ,employeeunkid " & _
                             "       FROM hrassess_computescore_approval_tran " & _
                             "       WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' AND calibrated_score > 0 " & _
                             "    ) AS A " & _
                             "    WHERE A.rno = 1 AND A.statusunkid <> 3 " & _
                             ") AS P ON P.employeeunkid = hremployee_master.employeeunkid "
                End If

                iStrQ &= " WHERE 1=1 "
                If intEmployeeID > 0 Then
                    iStrQ &= " AND hremployee_master.employeeunkid = " & intEmployeeID & ""
                End If

                If xDataFilterQry.Trim.Length > 0 Then
                    iStrQ &= xDataFilterQry
                End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    iStrQ &= " AND " & mstrAdvanceFilter
                End If

                iStrQ &= " AND hremployee_master.employeeunkid IN (SELECT CASE WHEN selfemployeeunkid <= 0 THEN assessedemployeeunkid ELSE selfemployeeunkid END FROM hrevaluation_analysis_master WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "') "

                objDo.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDo.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

                If mstrInnFilter.Trim <> "" Then
                    iStrQ &= " AND " & mstrInnFilter & " "
                End If

                dsList = objDo.ExecQuery(iStrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                intTotalEmployee = dsList.Tables(0).Rows.Count

                dsList = New DataSet()

                StrQ = "DECLARE @emp AS TABLE (empid int) " & _
                       " INSERT INTO @emp (empid) " & iStrQ & " " & _
                       "SELECT " & _
                       "     appratingunkid AS eratid " & _
                       "    ,distribution AS np " & _
                       "    ,grade_award AS erate " & _
                       "    ,ISNULL(C.ap,0) AS ap " & _
                       "    ,ISNULL(C.total,0) AS total " & _
                       "FROM hrapps_ratings " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         CAST(edist AS DECIMAL(36,2)) as np " & _
                       "        ,erate " & _
                       "        ,CASE WHEN " & intTotalEmployee & "<= 0 THEN 0 ELSE CAST((CAST(COUNT(1) AS FLOAT) / CAST(" & intTotalEmployee & " AS FLOAT)) * 100 AS DECIMAL(36,2)) END AS ap " & _
                       "        ,COUNT(DISTINCT employeeunkid) AS total " & _
                       "        ,eratid " & _
                       "        /*,B.statusunkid*/ " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),0)  AS eratid " & _
                       "            ,ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),0) AS edist " & _
                       "            ,ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND calibrated_score >= score_from AND calibrated_score <= score_to),'') AS erate " & _
                       "            ,A.statusunkid " & _
                       "            ,A.employeeunkid " & _
                       "        FROM " & _
                       "        ( " & _
                       "            SELECT " & _
                       "                 calibrated_score " & _
                       "                ,DENSE_RANK()OVER(PARTITION BY calibratnounkid,periodunkid ORDER BY statusunkid DESC) AS rno " & _
                       "                ,statusunkid " & _
                       "                ,employeeunkid " & _
                       "            FROM hrassess_computescore_approval_tran " & _
                       "                JOIN @emp ON [@emp].empid = hrassess_computescore_approval_tran.employeeunkid " & _
                       "            WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' AND calibrated_score > 0 "
                If intEmployeeID > 0 Then
                    StrQ &= " AND hrassess_computescore_approval_tran.employeeunkid IN (" & intEmployeeID & ") "
                End If
                StrQ &= "       ) AS A " & _
                        "       WHERE A.rno = 1 " & _
                        "   ) AS B WHERE B.statusunkid <> 3 " & _
                        "   GROUP BY eratid,edist,erate/*,statusunkid*/ " & _
                        ") AS C ON C.eratid = hrapps_ratings.appratingunkid " & _
                        "WHERE isvoid = 0 "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                    dsList.Tables("List").Columns.Remove("ap") : dsList.Tables("List").Columns.Remove("total")
                    Dim xCol As New DataColumn
                    xCol = New DataColumn
                    With xCol
                        .ColumnName = "ap"
                        .DataType = GetType(System.Decimal)
                        .DefaultValue = 0
                    End With
                    dsList.Tables("List").Columns.Add(xCol)
                    xCol = New DataColumn
                    With xCol
                        .ColumnName = "total"
                        .DataType = GetType(System.Int32)
                        .DefaultValue = 0
                    End With
                    dsList.Tables("List").Columns.Add(xCol)

                End If

                If IsDBNull(dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("total"))) = True Or _
                            dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("total")).Count <= 0 Then

                    StrQ = "DECLARE @emp AS TABLE (empid int) " & _
                           " INSERT INTO @emp (empid) " & iStrQ & " " & _
                           "SELECT " & _
                           "     appratingunkid AS eratid " & _
                           "    ,distribution AS np " & _
                           "    ,grade_award AS erate " & _
                           "    ,ISNULL(B.ap,0) AS ap " & _
                           "    ,ISNULL(B.total,0) AS total " & _
                           "FROM hrapps_ratings " & _
                           "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         CAST(edist AS DECIMAL(36,2)) as np " & _
                           "        ,erate " & _
                           "        ,CASE WHEN " & intTotalEmployee & "<= 0 THEN 0 ELSE CAST((CAST(COUNT(1) AS FLOAT) / CAST(" & intTotalEmployee & " AS FLOAT)) * 100 AS DECIMAL(36,2)) END AS ap " & _
                           "        ,COUNT(1) AS total " & _
                           "        ,eratid " & _
                           "    FROM " & _
                           "    ( " & _
                           "        SELECT DISTINCT " & _
                           "             CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                           "                ELSE ISNULL((SELECT appratingunkid FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), 0) END AS eratid " & _
                           "            ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                           "                ELSE ISNULL((SELECT distribution FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), 0) END AS edist " & _
                           "            ,CASE WHEN finaloverallscore > (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0) THEN (SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND score_to = (SELECT MAX(score_to) FROM hrapps_ratings WHERE isvoid = 0)) " & _
                           "                ELSE ISNULL((SELECT grade_award FROM hrapps_ratings WHERE isvoid = 0 AND finaloverallscore >= score_from AND finaloverallscore <= score_to), '') END AS erate " & _
                           "            ,hrassess_compute_score_master.employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY hrassess_compute_score_master.employeeunkid,hrassess_compute_score_master.periodunkid ORDER BY hrassess_compute_score_master.employeeunkid,finaloverallscore DESC) AS rno " & _
                           "        FROM hrassess_compute_score_master " & _
                           "            JOIN @emp ON [@emp].empid = hrassess_compute_score_master.employeeunkid " & _
                           "            JOIN hrevaluation_analysis_master ON hrassess_compute_score_master.analysisunkid = hrevaluation_analysis_master.analysisunkid AND hrevaluation_analysis_master.isvoid = 0 " & _
                           "        WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_master.periodunkid = '" & intPeriodId & "' "
                    If intEmployeeID > 0 Then
                        StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmployeeID & ") "
                    End If
                    StrQ &= "   ) AS A WHERE A.rno = 1 " & _
                            "   GROUP BY eratid,edist,erate " & _
                            ") AS B ON B.eratid = hrapps_ratings.appratingunkid " & _
                            "WHERE isvoid = 0 "

                    dsList = objDo.ExecQuery(StrQ, "List")

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    Dim dCol As New DataColumn
                    With dCol
                        .ColumnName = "litem"
                        .DataType = GetType(System.String)
                        .DefaultValue = Language.getMessage(mstrModuleName, 206, "(Provisional)")
                    End With
                    dsList.Tables("List").Columns.Add(dCol)

                    If eDisplayScore = clsComputeScore_master.enScoreMode.CALIBRATED_SCORE Then
                        dsList.Tables("List").Columns.Remove("ap") : dsList.Tables("List").Columns.Remove("total") : dsList.Tables("List").Columns.Remove("litem")
                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "ap"
                            .DataType = GetType(System.Decimal)
                            .DefaultValue = 0
                        End With
                        dsList.Tables("List").Columns.Add(dCol)
                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "total"
                            .DataType = GetType(System.Int32)
                            .DefaultValue = 0
                        End With
                        dsList.Tables("List").Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "litem"
                            .DataType = GetType(System.String)
                            .DefaultValue = Language.getMessage(mstrModuleName, 205, "(Calibrated)")
                        End With
                        dsList.Tables("List").Columns.Add(dCol)
                    End If
                Else
                    Dim dCol As New DataColumn
                    With dCol
                        .ColumnName = "litem"
                        .DataType = GetType(System.String)
                        .DefaultValue = Language.getMessage(mstrModuleName, 205, "(Calibrated)")
                    End With
                    dsList.Tables("List").Columns.Add(dCol)
                End If

            End Using

            Dim dr As DataRow = dtList.NewRow()
            Dim dp As DataRow = dtList.NewRow()
            For Each iCol As DataColumn In dtList.Columns
                If IsNumeric(iCol.ColumnName) = True Then
                    Dim dtmp() As DataRow = Nothing
                    dtmp = dsList.Tables(0).Select("eratid = '" & iCol.ColumnName & "' ")
                    If dtmp.Length > 0 Then
                        dr.Item(iCol.ColumnName) = dtmp(0)("total")
                        dp.Item(iCol.ColumnName) = dtmp(0)("ap") & " %"
                    Else
                        dr.Item(iCol.ColumnName) = 0
                        dp.Item(iCol.ColumnName) = "0.00 %"
                    End If
                End If
            Next

            dr.Item("particulars") = Language.getMessage(mstrModuleName, 317, "Employee Distribution")
            dp.Item("particulars") = Language.getMessage(mstrModuleName, 203, "Actual Performance (Aruti provision rating or calibrated rating )")
            dp.Item("objColRowType") = "A"
            dtList.Rows.InsertAt(dr, 0)
            dtList.Rows.InsertAt(dp, dtList.Rows.Count)

            dr = dtList.NewRow()

            If eDisplayScore = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE Then
                dr("particulars") = Language.getMessage(mstrModuleName, 315, "Total number of employees with provision rating")
            Else
                dr("particulars") = Language.getMessage(mstrModuleName, 316, "Total number of employees with calibration rating")
            End If

            dr(1) = intTotalEmployee
            dtList.Rows.InsertAt(dr, 0)

            Dim dsChart As New DataSet : dsChart = dsList.Copy()

            dsList = New DataSet
            dsList.Tables.Add(dtList.Copy)

            dtList = New DataTable("Chart")

            dsChart.Tables(0).TableName = "Chart"
            dtList = dsChart.Tables(0).Copy()

            dsList.Tables.Add(dtList.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateChartData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP |01-MAY-2020| -- END


    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Public Function Export_Calibration_Report(ByVal intUserUnkid As Integer, _
                                              ByVal intCompanyUnkid As Integer, _
                                              ByVal mdtFinalTable As DataTable, _
                                              ByVal strFilterUsed As String, _
                                              ByVal strReportName As String, _
                                              ByVal strExportPath As String, _
                                              ByVal blnOpenAfterExport As Boolean) As Boolean
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid
            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = intUserUnkid
            _UserName = User._Object._Username
            setReportData(CInt(mstrReportId), User._Object._Languageunkid, intCompanyUnkid)

            Dim dtFinal As DataTable : Dim intArrayColumnWidth As Integer() = Nothing
            If mdtFinalTable.Rows.Count > 0 AndAlso mdtFinalTable IsNot Nothing Then
                mdtFinalTable = New DataView(mdtFinalTable, "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable()

                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                'dtFinal = mdtFinalTable.DefaultView.ToTable("List", False, "calibration_no", "Code", "employee_name", "JobTitle", "lstpRating", "acrating", "acremark", "lstapRating", "apcalibremark", "approvalremark", "iStatus", "username", "levelname", "calibuser", "allocation")
                dtFinal = mdtFinalTable.DefaultView.ToTable("List", False, "Code", "employee_name", "JobTitle", "lstpRating", "acrating", "acremark", "lstapRating", "apcalibremark", "approvalremark", "iStatus", "username", "levelname", "calibuser", "allocation", "calibration_no")
                'S.SANDEEP |25-OCT-2019| -- END

                For Each xCol As DataColumn In dtFinal.Columns
                    Select Case xCol.ColumnName.ToUpper
                        Case "CALIBRATION_NO"
                            xCol.Caption = Language.getMessage(mstrModuleName, 300, "Calibration No")
                        Case "CODE"
                            xCol.Caption = Language.getMessage(mstrModuleName, 301, "Employee Code")
                        Case "EMPLOYEE_NAME"
                            xCol.Caption = Language.getMessage(mstrModuleName, 302, "Employee Name")
                        Case "JOBTITLE"
                            xCol.Caption = Language.getMessage(mstrModuleName, 303, "Job Title")
                        Case "LSTPRATING"
                            xCol.Caption = Language.getMessage(mstrModuleName, 304, "Provisional Rating")
                        Case "ACRATING"
                            xCol.Caption = Language.getMessage(mstrModuleName, 305, "Applied Rating")
                        Case "ACREMARK"
                            xCol.Caption = Language.getMessage(mstrModuleName, 306, "Applied Remark")
                        Case "LSTAPRATING"
                            xCol.Caption = Language.getMessage(mstrModuleName, 307, "Approver Rating")
                        Case "APCALIBREMARK"
                            xCol.Caption = Language.getMessage(mstrModuleName, 308, "Calibration Remark")
                        Case "APPROVALREMARK"
                            xCol.Caption = Language.getMessage(mstrModuleName, 309, "Approval Remark")
                        Case "ISTATUS"
                            xCol.Caption = Language.getMessage(mstrModuleName, 310, "Status")
                        Case "USERNAME"
                            xCol.Caption = Language.getMessage(mstrModuleName, 311, "Approver")
                        Case "LEVELNAME"
                            xCol.Caption = Language.getMessage(mstrModuleName, 312, "Level")
                    End Select
                Next

                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                'ReDim intArrayColumnWidth(dtFinal.Columns.Count - 1)
                'For i As Integer = 0 To intArrayColumnWidth.Length - 1
                '    intArrayColumnWidth(i) = 125
                'Next
                'S.SANDEEP |25-OCT-2019| -- END

                dtFinal.AsEnumerable().ToList().ForEach(Function(x) CreateGroupText(x))

                dtFinal.Columns.Remove("calibuser")
                dtFinal.Columns.Remove("allocation")

                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                ReDim intArrayColumnWidth(dtFinal.Columns.Count - 1)
                For i As Integer = 0 To intArrayColumnWidth.Length - 2
                    intArrayColumnWidth(i) = 125
                Next
                'S.SANDEEP |25-OCT-2019| -- END

                Dim strGrpCols As String() = Nothing
                Dim strarrGroupColumns As String() = Nothing
                strGrpCols = New String() {"calibration_no"}
                strarrGroupColumns = strGrpCols

                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dtFinal, intArrayColumnWidth, True, True, False, strarrGroupColumns, strReportName, "", strFilterUsed, , "", False, Nothing, Nothing, Nothing, Nothing, False)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_Calibration_Report; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CreateGroupText(ByVal dr As DataRow) As Boolean
        Try
            dr("calibration_no") &= " - " & Language.getMessage(mstrModuleName, 314, "By") & " - " & dr("calibuser").ToString() & _
            IIf(dr("allocation").ToString.Trim.Length <= 0, "", "(" & dr("allocation").ToString() & ")")
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateGroupText; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |26-AUG-2019| -- END

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Department")
            Language.setMessage(mstrModuleName, 2, "Total Staff")
            Language.setMessage(mstrModuleName, 3, "Not Rated")
            Language.setMessage(mstrModuleName, 4, "Probation")
            Language.setMessage(mstrModuleName, 5, "Total")
            Language.setMessage(mstrModuleName, 6, "Average Score %")
            Language.setMessage(mstrModuleName, 7, "Score")
            Language.setMessage(mstrModuleName, 8, "Period :")
            Language.setMessage(mstrModuleName, 9, "Display Score Type")
            Language.setMessage(mstrModuleName, 200, "Rating")
            Language.setMessage(mstrModuleName, 201, "Normal Distribution (Fixed distribution as per the format currently NMB are using)")
            Language.setMessage(mstrModuleName, 203, "Actual Performance (Aruti provision rating or calibrated rating )")
            Language.setMessage(mstrModuleName, 205, "(Calibrated)")
            Language.setMessage(mstrModuleName, 206, "(Provisional)")
            Language.setMessage(mstrModuleName, 300, "Calibration No")
            Language.setMessage(mstrModuleName, 301, "Employee Code")
            Language.setMessage(mstrModuleName, 302, "Employee Name")
            Language.setMessage(mstrModuleName, 303, "Job Title")
            Language.setMessage(mstrModuleName, 304, "Provisional Rating")
            Language.setMessage(mstrModuleName, 305, "Applied Rating")
            Language.setMessage(mstrModuleName, 306, "Applied Remark")
            Language.setMessage(mstrModuleName, 307, "Approver Rating")
            Language.setMessage(mstrModuleName, 308, "Calibration Remark")
            Language.setMessage(mstrModuleName, 309, "Approval Remark")
            Language.setMessage(mstrModuleName, 310, "Status")
            Language.setMessage(mstrModuleName, 311, "Approver")
            Language.setMessage(mstrModuleName, 312, "Level")
            Language.setMessage(mstrModuleName, 314, "By")
            Language.setMessage(mstrModuleName, 315, "Total number of employees with provision rating")
            Language.setMessage(mstrModuleName, 316, "Total number of employees with calibration rating")
            Language.setMessage(mstrModuleName, 317, "Employee Distribution")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
