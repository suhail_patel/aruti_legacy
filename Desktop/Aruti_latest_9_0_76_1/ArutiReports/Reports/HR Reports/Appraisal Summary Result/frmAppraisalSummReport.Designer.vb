﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAppraisalSummReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.txtWarningTo = New eZee.TextBox.NumericTextBox
        Me.txtClto = New eZee.TextBox.NumericTextBox
        Me.txtImpTo = New eZee.TextBox.NumericTextBox
        Me.txtClFrom = New eZee.TextBox.NumericTextBox
        Me.txtWarningFrom = New eZee.TextBox.NumericTextBox
        Me.lblWarningTo = New System.Windows.Forms.Label
        Me.txtImpFrom = New eZee.TextBox.NumericTextBox
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblImpTo = New System.Windows.Forms.Label
        Me.lblImprovment = New System.Windows.Forms.Label
        Me.lblClTo = New System.Windows.Forms.Label
        Me.objbtnSearchDept = New eZee.Common.eZeeGradientButton
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblCommendableletters = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 445)
        Me.NavPanel.Size = New System.Drawing.Size(691, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.gbFilterCriteria.Controls.Add(Me.txtWarningTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtClto)
        Me.gbFilterCriteria.Controls.Add(Me.txtImpTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtClFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtWarningFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblWarningTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtImpFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblWarning)
        Me.gbFilterCriteria.Controls.Add(Me.lblImpTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblImprovment)
        Me.gbFilterCriteria.Controls.Add(Me.lblClTo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDept)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblCommendableletters)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(406, 164)
        Me.gbFilterCriteria.TabIndex = 17
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(148, 141)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(224, 17)
        Me.chkIncludeInactiveEmp.TabIndex = 95
        Me.chkIncludeInactiveEmp.Text = "Include Inactive Employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        '
        'txtWarningTo
        '
        Me.txtWarningTo.AllowNegative = False
        Me.txtWarningTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWarningTo.DigitsInGroup = 0
        Me.txtWarningTo.Flags = 65536
        Me.txtWarningTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWarningTo.Location = New System.Drawing.Point(285, 114)
        Me.txtWarningTo.MaxDecimalPlaces = 0
        Me.txtWarningTo.MaxWholeDigits = 9
        Me.txtWarningTo.Name = "txtWarningTo"
        Me.txtWarningTo.Prefix = ""
        Me.txtWarningTo.RangeMax = 1.7976931348623157E+308
        Me.txtWarningTo.RangeMin = -1.7976931348623157E+308
        Me.txtWarningTo.Size = New System.Drawing.Size(87, 21)
        Me.txtWarningTo.TabIndex = 7
        Me.txtWarningTo.Text = "0"
        Me.txtWarningTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtClto
        '
        Me.txtClto.AllowNegative = False
        Me.txtClto.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtClto.DigitsInGroup = 0
        Me.txtClto.Flags = 65536
        Me.txtClto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClto.Location = New System.Drawing.Point(285, 60)
        Me.txtClto.MaxDecimalPlaces = 0
        Me.txtClto.MaxWholeDigits = 9
        Me.txtClto.Name = "txtClto"
        Me.txtClto.Prefix = ""
        Me.txtClto.RangeMax = 1.7976931348623157E+308
        Me.txtClto.RangeMin = -1.7976931348623157E+308
        Me.txtClto.Size = New System.Drawing.Size(87, 21)
        Me.txtClto.TabIndex = 3
        Me.txtClto.Text = "0"
        Me.txtClto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpTo
        '
        Me.txtImpTo.AllowNegative = False
        Me.txtImpTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtImpTo.DigitsInGroup = 0
        Me.txtImpTo.Flags = 65536
        Me.txtImpTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImpTo.Location = New System.Drawing.Point(285, 87)
        Me.txtImpTo.MaxDecimalPlaces = 0
        Me.txtImpTo.MaxWholeDigits = 9
        Me.txtImpTo.Name = "txtImpTo"
        Me.txtImpTo.Prefix = ""
        Me.txtImpTo.RangeMax = 1.7976931348623157E+308
        Me.txtImpTo.RangeMin = -1.7976931348623157E+308
        Me.txtImpTo.Size = New System.Drawing.Size(87, 21)
        Me.txtImpTo.TabIndex = 5
        Me.txtImpTo.Text = "0"
        Me.txtImpTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtClFrom
        '
        Me.txtClFrom.AllowNegative = False
        Me.txtClFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtClFrom.DigitsInGroup = 0
        Me.txtClFrom.Flags = 65536
        Me.txtClFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClFrom.Location = New System.Drawing.Point(148, 60)
        Me.txtClFrom.MaxDecimalPlaces = 0
        Me.txtClFrom.MaxWholeDigits = 9
        Me.txtClFrom.Name = "txtClFrom"
        Me.txtClFrom.Prefix = ""
        Me.txtClFrom.RangeMax = 1.7976931348623157E+308
        Me.txtClFrom.RangeMin = -1.7976931348623157E+308
        Me.txtClFrom.Size = New System.Drawing.Size(87, 21)
        Me.txtClFrom.TabIndex = 2
        Me.txtClFrom.Text = "0"
        Me.txtClFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWarningFrom
        '
        Me.txtWarningFrom.AllowNegative = False
        Me.txtWarningFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWarningFrom.DigitsInGroup = 0
        Me.txtWarningFrom.Flags = 65536
        Me.txtWarningFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWarningFrom.Location = New System.Drawing.Point(148, 114)
        Me.txtWarningFrom.MaxDecimalPlaces = 0
        Me.txtWarningFrom.MaxWholeDigits = 9
        Me.txtWarningFrom.Name = "txtWarningFrom"
        Me.txtWarningFrom.Prefix = ""
        Me.txtWarningFrom.RangeMax = 1.7976931348623157E+308
        Me.txtWarningFrom.RangeMin = -1.7976931348623157E+308
        Me.txtWarningFrom.Size = New System.Drawing.Size(87, 21)
        Me.txtWarningFrom.TabIndex = 6
        Me.txtWarningFrom.Text = "0"
        Me.txtWarningFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWarningTo
        '
        Me.lblWarningTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarningTo.Location = New System.Drawing.Point(238, 117)
        Me.lblWarningTo.Name = "lblWarningTo"
        Me.lblWarningTo.Size = New System.Drawing.Size(41, 15)
        Me.lblWarningTo.TabIndex = 93
        Me.lblWarningTo.Text = "To"
        Me.lblWarningTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtImpFrom
        '
        Me.txtImpFrom.AllowNegative = False
        Me.txtImpFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtImpFrom.DigitsInGroup = 0
        Me.txtImpFrom.Flags = 65536
        Me.txtImpFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImpFrom.Location = New System.Drawing.Point(148, 87)
        Me.txtImpFrom.MaxDecimalPlaces = 0
        Me.txtImpFrom.MaxWholeDigits = 9
        Me.txtImpFrom.Name = "txtImpFrom"
        Me.txtImpFrom.Prefix = ""
        Me.txtImpFrom.RangeMax = 1.7976931348623157E+308
        Me.txtImpFrom.RangeMin = -1.7976931348623157E+308
        Me.txtImpFrom.Size = New System.Drawing.Size(87, 21)
        Me.txtImpFrom.TabIndex = 4
        Me.txtImpFrom.Text = "0"
        Me.txtImpFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWarning
        '
        Me.lblWarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.Location = New System.Drawing.Point(8, 117)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(133, 15)
        Me.lblWarning.TabIndex = 91
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblImpTo
        '
        Me.lblImpTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpTo.Location = New System.Drawing.Point(238, 90)
        Me.lblImpTo.Name = "lblImpTo"
        Me.lblImpTo.Size = New System.Drawing.Size(41, 15)
        Me.lblImpTo.TabIndex = 89
        Me.lblImpTo.Text = "To"
        Me.lblImpTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblImprovment
        '
        Me.lblImprovment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImprovment.Location = New System.Drawing.Point(8, 90)
        Me.lblImprovment.Name = "lblImprovment"
        Me.lblImprovment.Size = New System.Drawing.Size(133, 15)
        Me.lblImprovment.TabIndex = 87
        Me.lblImprovment.Text = "Letters of Improvement"
        Me.lblImprovment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClTo
        '
        Me.lblClTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClTo.Location = New System.Drawing.Point(238, 63)
        Me.lblClTo.Name = "lblClTo"
        Me.lblClTo.Size = New System.Drawing.Size(41, 15)
        Me.lblClTo.TabIndex = 82
        Me.lblClTo.Text = "To"
        Me.lblClTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDept
        '
        Me.objbtnSearchDept.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDept.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDept.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDept.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDept.BorderSelected = False
        Me.objbtnSearchDept.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDept.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDept.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDept.Location = New System.Drawing.Point(378, 33)
        Me.objbtnSearchDept.Name = "objbtnSearchDept"
        Me.objbtnSearchDept.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDept.TabIndex = 59
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(148, 33)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(224, 21)
        Me.cboDepartment.TabIndex = 1
        '
        'lblCommendableletters
        '
        Me.lblCommendableletters.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommendableletters.Location = New System.Drawing.Point(8, 63)
        Me.lblCommendableletters.Name = "lblCommendableletters"
        Me.lblCommendableletters.Size = New System.Drawing.Size(133, 15)
        Me.lblCommendableletters.TabIndex = 80
        Me.lblCommendableletters.Text = "Commendable Letters"
        Me.lblCommendableletters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 36)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(133, 15)
        Me.lblDepartment.TabIndex = 57
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmAppraisalSummReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 500)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmAppraisalSummReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = ""
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblClTo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDept As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblCommendableletters As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblImprovment As System.Windows.Forms.Label
    Friend WithEvents lblWarningTo As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblImpTo As System.Windows.Forms.Label
    Friend WithEvents txtClFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents txtClto As eZee.TextBox.NumericTextBox
    Friend WithEvents txtImpFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents txtWarningFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents txtImpTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtWarningTo As eZee.TextBox.NumericTextBox
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
End Class
