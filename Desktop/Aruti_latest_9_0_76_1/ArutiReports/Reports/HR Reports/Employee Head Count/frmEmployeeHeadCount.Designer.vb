﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeHeadCount
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterType = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchFilterType = New eZee.Common.eZeeGradientButton
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.chkIncludeNAEmp = New System.Windows.Forms.CheckBox
        Me.chkShowChartDept = New System.Windows.Forms.CheckBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.radEmploymentType = New System.Windows.Forms.RadioButton
        Me.radReligion = New System.Windows.Forms.RadioButton
        Me.radNationality = New System.Windows.Forms.RadioButton
        Me.rabUnit = New System.Windows.Forms.RadioButton
        Me.rabClass = New System.Windows.Forms.RadioButton
        Me.rabGrade = New System.Windows.Forms.RadioButton
        Me.rabJob = New System.Windows.Forms.RadioButton
        Me.rabStation = New System.Windows.Forms.RadioButton
        Me.rabDept = New System.Windows.Forms.RadioButton
        Me.rabSection = New System.Windows.Forms.RadioButton
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchViewBy = New eZee.Common.eZeeGradientButton
        Me.lblTHTo = New System.Windows.Forms.Label
        Me.txtTotalHedsTo = New eZee.TextBox.NumericTextBox
        Me.lblTotalHeadsFrom = New System.Windows.Forms.Label
        Me.txtTotalHedsFrom = New eZee.TextBox.NumericTextBox
        Me.lblMaleTo = New System.Windows.Forms.Label
        Me.txtTotalMaleTo = New eZee.TextBox.NumericTextBox
        Me.lblTotalMaleFrom = New System.Windows.Forms.Label
        Me.txtTotalMaleFrom = New eZee.TextBox.NumericTextBox
        Me.lblTotalFemaleTo = New System.Windows.Forms.Label
        Me.txtTotalFemaleTo = New eZee.TextBox.NumericTextBox
        Me.lblTotalFemaleFrom = New System.Windows.Forms.Label
        Me.txtTotalFemaleFrom = New eZee.TextBox.NumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.cboName = New System.Windows.Forms.ComboBox
        Me.gbFilterType.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterType
        '
        Me.gbFilterType.BorderColor = System.Drawing.Color.Black
        Me.gbFilterType.Checked = False
        Me.gbFilterType.CollapseAllExceptThis = False
        Me.gbFilterType.CollapsedHoverImage = Nothing
        Me.gbFilterType.CollapsedNormalImage = Nothing
        Me.gbFilterType.CollapsedPressedImage = Nothing
        Me.gbFilterType.CollapseOnLoad = False
        Me.gbFilterType.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterType.Controls.Add(Me.objbtnSearchFilterType)
        Me.gbFilterType.Controls.Add(Me.lblViewBy)
        Me.gbFilterType.Controls.Add(Me.cboViewBy)
        Me.gbFilterType.Controls.Add(Me.chkIncludeNAEmp)
        Me.gbFilterType.Controls.Add(Me.chkShowChartDept)
        Me.gbFilterType.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterType.ExpandedHoverImage = Nothing
        Me.gbFilterType.ExpandedNormalImage = Nothing
        Me.gbFilterType.ExpandedPressedImage = Nothing
        Me.gbFilterType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterType.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterType.HeaderHeight = 25
        Me.gbFilterType.HeaderMessage = ""
        Me.gbFilterType.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterType.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterType.HeightOnCollapse = 0
        Me.gbFilterType.LeftTextSpace = 0
        Me.gbFilterType.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterType.Name = "gbFilterType"
        Me.gbFilterType.OpenHeight = 300
        Me.gbFilterType.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterType.ShowBorder = True
        Me.gbFilterType.ShowCheckBox = False
        Me.gbFilterType.ShowCollapseButton = False
        Me.gbFilterType.ShowDefaultBorderColor = True
        Me.gbFilterType.ShowDownButton = False
        Me.gbFilterType.ShowHeader = True
        Me.gbFilterType.Size = New System.Drawing.Size(366, 113)
        Me.gbFilterType.TabIndex = 12
        Me.gbFilterType.Temp = 0
        Me.gbFilterType.Text = "Filter Type"
        Me.gbFilterType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(267, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 213
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchFilterType
        '
        Me.objbtnSearchFilterType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFilterType.BorderSelected = False
        Me.objbtnSearchFilterType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFilterType.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFilterType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFilterType.Location = New System.Drawing.Point(335, 33)
        Me.objbtnSearchFilterType.Name = "objbtnSearchFilterType"
        Me.objbtnSearchFilterType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFilterType.TabIndex = 215
        '
        'lblViewBy
        '
        Me.lblViewBy.BackColor = System.Drawing.Color.Transparent
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(8, 36)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(97, 15)
        Me.lblViewBy.TabIndex = 216
        Me.lblViewBy.Text = "View By"
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 120
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(111, 33)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(218, 21)
        Me.cboViewBy.TabIndex = 29
        '
        'chkIncludeNAEmp
        '
        Me.chkIncludeNAEmp.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkIncludeNAEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeNAEmp.Location = New System.Drawing.Point(11, 83)
        Me.chkIncludeNAEmp.Name = "chkIncludeNAEmp"
        Me.chkIncludeNAEmp.Size = New System.Drawing.Size(318, 17)
        Me.chkIncludeNAEmp.TabIndex = 210
        Me.chkIncludeNAEmp.Text = "Include Employee With No Gender in Total Heads"
        Me.chkIncludeNAEmp.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkIncludeNAEmp.UseVisualStyleBackColor = True
        '
        'chkShowChartDept
        '
        Me.chkShowChartDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowChartDept.Location = New System.Drawing.Point(176, 60)
        Me.chkShowChartDept.Name = "chkShowChartDept"
        Me.chkShowChartDept.Size = New System.Drawing.Size(153, 17)
        Me.chkShowChartDept.TabIndex = 208
        Me.chkShowChartDept.Text = "Show Chart"
        Me.chkShowChartDept.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(11, 60)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(159, 17)
        Me.chkInactiveemp.TabIndex = 206
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'radEmploymentType
        '
        Me.radEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmploymentType.Location = New System.Drawing.Point(111, 3)
        Me.radEmploymentType.Name = "radEmploymentType"
        Me.radEmploymentType.Size = New System.Drawing.Size(146, 17)
        Me.radEmploymentType.TabIndex = 214
        Me.radEmploymentType.TabStop = True
        Me.radEmploymentType.Text = "Employment Type"
        Me.radEmploymentType.UseVisualStyleBackColor = True
        Me.radEmploymentType.Visible = False
        '
        'radReligion
        '
        Me.radReligion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radReligion.Location = New System.Drawing.Point(111, 3)
        Me.radReligion.Name = "radReligion"
        Me.radReligion.Size = New System.Drawing.Size(146, 17)
        Me.radReligion.TabIndex = 213
        Me.radReligion.TabStop = True
        Me.radReligion.Text = "Religion"
        Me.radReligion.UseVisualStyleBackColor = True
        Me.radReligion.Visible = False
        '
        'radNationality
        '
        Me.radNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNationality.Location = New System.Drawing.Point(418, 188)
        Me.radNationality.Name = "radNationality"
        Me.radNationality.Size = New System.Drawing.Size(146, 17)
        Me.radNationality.TabIndex = 212
        Me.radNationality.TabStop = True
        Me.radNationality.Text = "Nationality"
        Me.radNationality.UseVisualStyleBackColor = True
        Me.radNationality.Visible = False
        '
        'rabUnit
        '
        Me.rabUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabUnit.Location = New System.Drawing.Point(418, 188)
        Me.rabUnit.Name = "rabUnit"
        Me.rabUnit.Size = New System.Drawing.Size(146, 17)
        Me.rabUnit.TabIndex = 111
        Me.rabUnit.TabStop = True
        Me.rabUnit.Text = "Unit"
        Me.rabUnit.UseVisualStyleBackColor = True
        Me.rabUnit.Visible = False
        '
        'rabClass
        '
        Me.rabClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabClass.Location = New System.Drawing.Point(418, 188)
        Me.rabClass.Name = "rabClass"
        Me.rabClass.Size = New System.Drawing.Size(146, 17)
        Me.rabClass.TabIndex = 116
        Me.rabClass.TabStop = True
        Me.rabClass.Text = "Class"
        Me.rabClass.UseVisualStyleBackColor = True
        Me.rabClass.Visible = False
        '
        'rabGrade
        '
        Me.rabGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabGrade.Location = New System.Drawing.Point(418, 188)
        Me.rabGrade.Name = "rabGrade"
        Me.rabGrade.Size = New System.Drawing.Size(146, 17)
        Me.rabGrade.TabIndex = 110
        Me.rabGrade.TabStop = True
        Me.rabGrade.Text = "Grade"
        Me.rabGrade.UseVisualStyleBackColor = True
        Me.rabGrade.Visible = False
        '
        'rabJob
        '
        Me.rabJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabJob.Location = New System.Drawing.Point(418, 188)
        Me.rabJob.Name = "rabJob"
        Me.rabJob.Size = New System.Drawing.Size(146, 17)
        Me.rabJob.TabIndex = 115
        Me.rabJob.TabStop = True
        Me.rabJob.Text = "Job"
        Me.rabJob.UseVisualStyleBackColor = True
        Me.rabJob.Visible = False
        '
        'rabStation
        '
        Me.rabStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabStation.Location = New System.Drawing.Point(418, 188)
        Me.rabStation.Name = "rabStation"
        Me.rabStation.Size = New System.Drawing.Size(146, 17)
        Me.rabStation.TabIndex = 109
        Me.rabStation.TabStop = True
        Me.rabStation.Text = "Branch"
        Me.rabStation.UseVisualStyleBackColor = True
        Me.rabStation.Visible = False
        '
        'rabDept
        '
        Me.rabDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabDept.Location = New System.Drawing.Point(418, 188)
        Me.rabDept.Name = "rabDept"
        Me.rabDept.Size = New System.Drawing.Size(146, 17)
        Me.rabDept.TabIndex = 113
        Me.rabDept.TabStop = True
        Me.rabDept.Text = "Department "
        Me.rabDept.UseVisualStyleBackColor = True
        Me.rabDept.Visible = False
        '
        'rabSection
        '
        Me.rabSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabSection.Location = New System.Drawing.Point(418, 188)
        Me.rabSection.Name = "rabSection"
        Me.rabSection.Size = New System.Drawing.Size(146, 17)
        Me.rabSection.TabIndex = 114
        Me.rabSection.TabStop = True
        Me.rabSection.Text = "Section"
        Me.rabSection.UseVisualStyleBackColor = True
        Me.rabSection.Visible = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblTHTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalHedsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalHeadsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalHedsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblMaleTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalMaleTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalMaleFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalMaleFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalFemaleTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalFemaleTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalFemaleFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalFemaleFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblName)
        Me.gbFilterCriteria.Controls.Add(Me.cboName)
        Me.gbFilterCriteria.Controls.Add(Me.radEmploymentType)
        Me.gbFilterCriteria.Controls.Add(Me.radReligion)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 185)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(366, 147)
        Me.gbFilterCriteria.TabIndex = 13
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchViewBy
        '
        Me.objbtnSearchViewBy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchViewBy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchViewBy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchViewBy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchViewBy.BorderSelected = False
        Me.objbtnSearchViewBy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchViewBy.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchViewBy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchViewBy.Location = New System.Drawing.Point(335, 34)
        Me.objbtnSearchViewBy.Name = "objbtnSearchViewBy"
        Me.objbtnSearchViewBy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchViewBy.TabIndex = 67
        '
        'lblTHTo
        '
        Me.lblTHTo.BackColor = System.Drawing.Color.Transparent
        Me.lblTHTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTHTo.Location = New System.Drawing.Point(200, 117)
        Me.lblTHTo.Name = "lblTHTo"
        Me.lblTHTo.Size = New System.Drawing.Size(40, 16)
        Me.lblTHTo.TabIndex = 65
        Me.lblTHTo.Text = "To"
        '
        'txtTotalHedsTo
        '
        Me.txtTotalHedsTo.AllowNegative = True
        Me.txtTotalHedsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalHedsTo.DigitsInGroup = 0
        Me.txtTotalHedsTo.Flags = 0
        Me.txtTotalHedsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalHedsTo.Location = New System.Drawing.Point(246, 115)
        Me.txtTotalHedsTo.MaxDecimalPlaces = 0
        Me.txtTotalHedsTo.MaxWholeDigits = 9
        Me.txtTotalHedsTo.Name = "txtTotalHedsTo"
        Me.txtTotalHedsTo.Prefix = ""
        Me.txtTotalHedsTo.RangeMax = 1.7976931348623157E+308
        Me.txtTotalHedsTo.RangeMin = -1.7976931348623157E+308
        Me.txtTotalHedsTo.Size = New System.Drawing.Size(83, 21)
        Me.txtTotalHedsTo.TabIndex = 64
        Me.txtTotalHedsTo.Text = "0"
        Me.txtTotalHedsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalHeadsFrom
        '
        Me.lblTotalHeadsFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalHeadsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalHeadsFrom.Location = New System.Drawing.Point(8, 117)
        Me.lblTotalHeadsFrom.Name = "lblTotalHeadsFrom"
        Me.lblTotalHeadsFrom.Size = New System.Drawing.Size(102, 16)
        Me.lblTotalHeadsFrom.TabIndex = 63
        Me.lblTotalHeadsFrom.Text = "Total Heads From"
        '
        'txtTotalHedsFrom
        '
        Me.txtTotalHedsFrom.AllowNegative = True
        Me.txtTotalHedsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalHedsFrom.DigitsInGroup = 0
        Me.txtTotalHedsFrom.Flags = 0
        Me.txtTotalHedsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalHedsFrom.Location = New System.Drawing.Point(111, 115)
        Me.txtTotalHedsFrom.MaxDecimalPlaces = 0
        Me.txtTotalHedsFrom.MaxWholeDigits = 9
        Me.txtTotalHedsFrom.Name = "txtTotalHedsFrom"
        Me.txtTotalHedsFrom.Prefix = ""
        Me.txtTotalHedsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtTotalHedsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtTotalHedsFrom.Size = New System.Drawing.Size(83, 21)
        Me.txtTotalHedsFrom.TabIndex = 62
        Me.txtTotalHedsFrom.Text = "0"
        Me.txtTotalHedsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaleTo
        '
        Me.lblMaleTo.BackColor = System.Drawing.Color.Transparent
        Me.lblMaleTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaleTo.Location = New System.Drawing.Point(200, 90)
        Me.lblMaleTo.Name = "lblMaleTo"
        Me.lblMaleTo.Size = New System.Drawing.Size(40, 16)
        Me.lblMaleTo.TabIndex = 61
        Me.lblMaleTo.Text = "To"
        '
        'txtTotalMaleTo
        '
        Me.txtTotalMaleTo.AllowNegative = True
        Me.txtTotalMaleTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalMaleTo.DigitsInGroup = 0
        Me.txtTotalMaleTo.Flags = 0
        Me.txtTotalMaleTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalMaleTo.Location = New System.Drawing.Point(246, 88)
        Me.txtTotalMaleTo.MaxDecimalPlaces = 0
        Me.txtTotalMaleTo.MaxWholeDigits = 9
        Me.txtTotalMaleTo.Name = "txtTotalMaleTo"
        Me.txtTotalMaleTo.Prefix = ""
        Me.txtTotalMaleTo.RangeMax = 1.7976931348623157E+308
        Me.txtTotalMaleTo.RangeMin = -1.7976931348623157E+308
        Me.txtTotalMaleTo.Size = New System.Drawing.Size(83, 21)
        Me.txtTotalMaleTo.TabIndex = 60
        Me.txtTotalMaleTo.Text = "0"
        Me.txtTotalMaleTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalMaleFrom
        '
        Me.lblTotalMaleFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalMaleFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalMaleFrom.Location = New System.Drawing.Point(8, 90)
        Me.lblTotalMaleFrom.Name = "lblTotalMaleFrom"
        Me.lblTotalMaleFrom.Size = New System.Drawing.Size(102, 16)
        Me.lblTotalMaleFrom.TabIndex = 59
        Me.lblTotalMaleFrom.Text = "Total Male From"
        '
        'txtTotalMaleFrom
        '
        Me.txtTotalMaleFrom.AllowNegative = True
        Me.txtTotalMaleFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalMaleFrom.DigitsInGroup = 0
        Me.txtTotalMaleFrom.Flags = 0
        Me.txtTotalMaleFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalMaleFrom.Location = New System.Drawing.Point(111, 88)
        Me.txtTotalMaleFrom.MaxDecimalPlaces = 0
        Me.txtTotalMaleFrom.MaxWholeDigits = 9
        Me.txtTotalMaleFrom.Name = "txtTotalMaleFrom"
        Me.txtTotalMaleFrom.Prefix = ""
        Me.txtTotalMaleFrom.RangeMax = 1.7976931348623157E+308
        Me.txtTotalMaleFrom.RangeMin = -1.7976931348623157E+308
        Me.txtTotalMaleFrom.Size = New System.Drawing.Size(83, 21)
        Me.txtTotalMaleFrom.TabIndex = 58
        Me.txtTotalMaleFrom.Text = "0"
        Me.txtTotalMaleFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalFemaleTo
        '
        Me.lblTotalFemaleTo.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalFemaleTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFemaleTo.Location = New System.Drawing.Point(200, 63)
        Me.lblTotalFemaleTo.Name = "lblTotalFemaleTo"
        Me.lblTotalFemaleTo.Size = New System.Drawing.Size(40, 16)
        Me.lblTotalFemaleTo.TabIndex = 56
        Me.lblTotalFemaleTo.Text = "To"
        '
        'txtTotalFemaleTo
        '
        Me.txtTotalFemaleTo.AllowNegative = True
        Me.txtTotalFemaleTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalFemaleTo.DigitsInGroup = 0
        Me.txtTotalFemaleTo.Flags = 0
        Me.txtTotalFemaleTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalFemaleTo.Location = New System.Drawing.Point(246, 61)
        Me.txtTotalFemaleTo.MaxDecimalPlaces = 0
        Me.txtTotalFemaleTo.MaxWholeDigits = 9
        Me.txtTotalFemaleTo.Name = "txtTotalFemaleTo"
        Me.txtTotalFemaleTo.Prefix = ""
        Me.txtTotalFemaleTo.RangeMax = 1.7976931348623157E+308
        Me.txtTotalFemaleTo.RangeMin = -1.7976931348623157E+308
        Me.txtTotalFemaleTo.Size = New System.Drawing.Size(83, 21)
        Me.txtTotalFemaleTo.TabIndex = 55
        Me.txtTotalFemaleTo.Text = "0"
        Me.txtTotalFemaleTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalFemaleFrom
        '
        Me.lblTotalFemaleFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalFemaleFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFemaleFrom.Location = New System.Drawing.Point(8, 63)
        Me.lblTotalFemaleFrom.Name = "lblTotalFemaleFrom"
        Me.lblTotalFemaleFrom.Size = New System.Drawing.Size(102, 16)
        Me.lblTotalFemaleFrom.TabIndex = 54
        Me.lblTotalFemaleFrom.Text = "Total Female From"
        '
        'txtTotalFemaleFrom
        '
        Me.txtTotalFemaleFrom.AllowNegative = True
        Me.txtTotalFemaleFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalFemaleFrom.DigitsInGroup = 0
        Me.txtTotalFemaleFrom.Flags = 0
        Me.txtTotalFemaleFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalFemaleFrom.Location = New System.Drawing.Point(111, 61)
        Me.txtTotalFemaleFrom.MaxDecimalPlaces = 0
        Me.txtTotalFemaleFrom.MaxWholeDigits = 9
        Me.txtTotalFemaleFrom.Name = "txtTotalFemaleFrom"
        Me.txtTotalFemaleFrom.Prefix = ""
        Me.txtTotalFemaleFrom.RangeMax = 1.7976931348623157E+308
        Me.txtTotalFemaleFrom.RangeMin = -1.7976931348623157E+308
        Me.txtTotalFemaleFrom.Size = New System.Drawing.Size(83, 21)
        Me.txtTotalFemaleFrom.TabIndex = 53
        Me.txtTotalFemaleFrom.Text = "0"
        Me.txtTotalFemaleFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.Color.Transparent
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 38)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(97, 13)
        Me.lblName.TabIndex = 27
        Me.lblName.Text = "Name"
        '
        'cboName
        '
        Me.cboName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboName.DropDownWidth = 120
        Me.cboName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboName.FormattingEnabled = True
        Me.cboName.Location = New System.Drawing.Point(111, 34)
        Me.cboName.Name = "cboName"
        Me.cboName.Size = New System.Drawing.Size(218, 21)
        Me.cboName.TabIndex = 28
        '
        'frmEmployeeHeadCount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbFilterType)
        Me.Controls.Add(Me.radNationality)
        Me.Controls.Add(Me.rabStation)
        Me.Controls.Add(Me.rabUnit)
        Me.Controls.Add(Me.rabSection)
        Me.Controls.Add(Me.rabClass)
        Me.Controls.Add(Me.rabDept)
        Me.Controls.Add(Me.rabGrade)
        Me.Controls.Add(Me.rabJob)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeHeadCount"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Head Count Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.rabJob, 0)
        Me.Controls.SetChildIndex(Me.rabGrade, 0)
        Me.Controls.SetChildIndex(Me.rabDept, 0)
        Me.Controls.SetChildIndex(Me.rabClass, 0)
        Me.Controls.SetChildIndex(Me.rabSection, 0)
        Me.Controls.SetChildIndex(Me.rabUnit, 0)
        Me.Controls.SetChildIndex(Me.rabStation, 0)
        Me.Controls.SetChildIndex(Me.radNationality, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterType, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterType.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterType As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents rabUnit As System.Windows.Forms.RadioButton
    Friend WithEvents rabClass As System.Windows.Forms.RadioButton
    Friend WithEvents rabGrade As System.Windows.Forms.RadioButton
    Friend WithEvents rabJob As System.Windows.Forms.RadioButton
    Friend WithEvents rabStation As System.Windows.Forms.RadioButton
    Friend WithEvents rabDept As System.Windows.Forms.RadioButton
    Friend WithEvents rabSection As System.Windows.Forms.RadioButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblTotalFemaleTo As System.Windows.Forms.Label
    Public WithEvents txtTotalFemaleTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblTotalFemaleFrom As System.Windows.Forms.Label
    Public WithEvents txtTotalFemaleFrom As eZee.TextBox.NumericTextBox
    Private WithEvents lblName As System.Windows.Forms.Label
    Public WithEvents cboName As System.Windows.Forms.ComboBox
    Private WithEvents lblTHTo As System.Windows.Forms.Label
    Public WithEvents txtTotalHedsTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblTotalHeadsFrom As System.Windows.Forms.Label
    Public WithEvents txtTotalHedsFrom As eZee.TextBox.NumericTextBox
    Private WithEvents lblMaleTo As System.Windows.Forms.Label
    Public WithEvents txtTotalMaleTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblTotalMaleFrom As System.Windows.Forms.Label
    Public WithEvents txtTotalMaleFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowChartDept As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeNAEmp As System.Windows.Forms.CheckBox
    Friend WithEvents radEmploymentType As System.Windows.Forms.RadioButton
    Friend WithEvents radReligion As System.Windows.Forms.RadioButton
    Friend WithEvents radNationality As System.Windows.Forms.RadioButton
    Public WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Private WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchFilterType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchViewBy As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
End Class
