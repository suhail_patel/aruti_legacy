'************************************************************************************************************************************
'Class Name : clsTrainingCostReport.vb
'Purpose    :
'Date       :01/06/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTrainingListReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTrainingListReport"
    Private mstrReportId As String = enArutiReport.TrainingListReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintTrainingId As Integer = -1
    Private mstrTrainingName As String = String.Empty
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintInstituteId As Integer = -1
    Private mstrInstituteName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintResultId As Integer = -1
    Private mstrResultName As String = String.Empty


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private mstrAllocationTypeName As String = String.Empty
    Private mintAllocationTypeId As Integer = 0
    Private mstrAllocationNames As String = String.Empty
    Private mstrAllocationIds As String = String.Empty
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _TrainingId() As Integer
        Set(ByVal value As Integer)
            mintTrainingId = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _InstituteId() As Integer
        Set(ByVal value As Integer)
            mintInstituteId = value
        End Set
    End Property

    Public WriteOnly Property _InstituteName() As String
        Set(ByVal value As String)
            mstrInstituteName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ResultId() As Integer
        Set(ByVal value As Integer)
            mintResultId = value
        End Set
    End Property

    Public WriteOnly Property _ResultName() As String
        Set(ByVal value As String)
            mstrResultName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationNames() As String
        Set(ByVal value As String)
            mstrAllocationNames = value
        End Set
    End Property

    Public WriteOnly Property _AllocationIds() As String
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintTrainingId = 0
            mstrTrainingName = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintInstituteId = 0
            mstrInstituteName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintResultId = 0
            mstrResultName = ""

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            mstrAllocationTypeName = String.Empty
            mintAllocationTypeId = 0
            mstrAllocationNames = String.Empty
            mstrAllocationIds = String.Empty
            'S.SANDEEP [08-FEB-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        'S.SANDEEP [08-FEB-2017] -- START
        'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        'S.SANDEEP [08-FEB-2017] -- END
        Try
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'S.SANDEEP [15-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            'If mintTrainingId > 0 Then
            '    objDataOperation.AddParameter("@TrainingId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingId)
            '    Me._FilterQuery &= "And trainingschedulingunkid = @TrainingId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Training :") & " " & mstrTrainingName & " "
            'End If

            'If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
            '    objDataOperation.AddParameter("@mdtFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            '    objDataOperation.AddParameter("@mdtToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            '    Me._FilterQuery &= "AND StDate >= @mdtFromDate AND EdDate <= @mdtToDate "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Date From :") & " " & mdtFromDate & " " & _
            '                       Language.getMessage(mstrModuleName, 19, "To :") & " " & mdtToDate & " "
            'End If

            'If mintInstituteId > 0 Then
            '    objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
            '    Me._FilterQuery &= "AND instituteunkid = @InstituteId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Institute :") & " " & mstrInstituteName & " "
            'End If

            'If mintEmployeeId > 0 Then
            '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            '    Me._FilterQuery &= "AND employeeunkid = @EmployeeId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Employee :") & " " & mstrEmployeeName & " "
            'End If

            'If mintResultId > 0 Then
            '    objDataOperation.AddParameter("@ResultId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultId)
            '    Me._FilterQuery &= "AND Rid = @ResultId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Result :") & " " & mstrResultName & " "
            'End If
            ''S.SANDEEP [08-FEB-2017] -- START
            ''ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'If mintAllocationTypeId > 0 Then
            '    objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
            '    Me._FilterQuery &= "AND allocationtypeunkid = @allocationtypeunkid "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Allocation Binded :") & " " & mstrAllocationTypeName & " "
            'End If
            ''S.SANDEEP [08-FEB-2017] -- END

            If mintTrainingId > 0 Then
                objDataOperation.AddParameter("@TrainingId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingId)
                Me._FilterQuery &= "And TS.trainingschedulingunkid = @TrainingId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Training :") & " " & mstrTrainingName & " "
            End If

            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@mdtFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                objDataOperation.AddParameter("@mdtToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterQuery &= "AND CONVERT(NVARCHAR(8),TS.start_date,112) >= @mdtFromDate AND CONVERT(NVARCHAR(8),TS.end_date,112) <= @mdtToDate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Date From :") & " " & mdtFromDate & " " & _
                                   Language.getMessage(mstrModuleName, 19, "To :") & " " & mdtToDate & " "
            End If

            If mintInstituteId > 0 Then
                objDataOperation.AddParameter("@InstituteId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteId)
                Me._FilterQuery &= "AND instituteunkid = @InstituteId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Institute :") & " " & mstrInstituteName & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= "AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintResultId > 0 Then
                objDataOperation.AddParameter("@ResultId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultId)
                Me._FilterQuery &= "AND TAT.resultunkid = @ResultId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Result :") & " " & mstrResultName & " "
            End If
            If mintAllocationTypeId > 0 Then
                objDataOperation.AddParameter("@allocationtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId)
                Me._FilterQuery &= "AND TS.allocationtypeunkid = @allocationtypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Allocation Binded :") & " " & mstrAllocationTypeName & " "
            End If
            'S.SANDEEP [15-FEB-2017] -- END



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            'S.SANDEEP [15-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            objRpt = Generate_DetailReportNew(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            'S.SANDEEP [15-FEB-2017] -- END
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS included course master instead of coursetitle field.
            'StrQ = "SELECT " & _
            '         " TrainingName AS TrainingName " & _
            '         ",StDate AS StDate " & _
            '         ",EdDate AS EdDate " & _
            '         ",Venue AS Venue " & _
            '         ",StTime AS StTime " & _
            '         ",EdTime AS EdTime " & _
            '         ",DAYS*Hrs AS TotalHrs " & _
            '         ",TName+' ,'+TAddress+' '+InsCountry+' '+InsState+' '+InsCity AS TName " & _
            '         ",TEmail AS TEmail " & _
            '         ",EmpName AS EmpName " & _
            '         ",Branch AS Branch " & _
            '         ",Department AS Department " & _
            '         ",Position AS Position " & _
            '         ",Address+' '+ Country +' '+ State+' '+City+' '+ZipCode AS Address " & _
            '         ",Email AS Email " & _
            '         ",PhoneNo AS PhoneNo " & _
            '         ",Score AS Score " & _
            '         ",employeeunkid AS employeeunkid " & _
            '         ",trainingschedulingunkid AS trainingschedulingunkid " & _
            '         ",instituteunkid AS instituteunkid " & _
            '         ",stationunkid AS stationunkid " & _
            '         ",departmentunkid AS departmentunkid " & _
            '         ",jobunkid AS jobunkid " & _
            '         ",ISNULL(Rid,-1) AS Rid " & _
            '    "FROM " & _
            '         "( " & _
            '              "SELECT " & _
            '                   " course_title AS TrainingName " & _
            '                   ",CONVERT(CHAR(8),start_date,112) AS StDate " & _
            '                   ",CONVERT(CHAR(8),end_date,112) AS EdDate " & _
            '                   ",ISNULL(training_venue,'')  AS Venue " & _
            '                   ",CONVERT(CHAR(5),start_time,108) AS StTime " & _
            '                   ",CONVERT(CHAR(5),end_time,108) AS EdTime " & _
            '                   ",DATEDIFF(dd,start_date,end_date) AS DAYS " & _
            '                   ",DATEDIFF(Hour,start_time,end_time) AS Hrs " & _
            '                   ",ISNULL(hrinstitute_master.institute_name,'') AS TName " & _
            '                   ",ISNULL(hrinstitute_master.institute_address,'') AS TAddress " & _
            '                   ",ISNULL(ICountry.country_name,'') AS InsCountry " & _
            '                   ",ISNULL(IState.name,'') AS InsState " & _
            '                   ",ISNULL(ICity.name,'') AS InsCity " & _
            '                   ",ISNULL(hrinstitute_master.institute_email,'') AS TEmail " & _
            '                   ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '                   ",ISNULL(hrstation_master.name,'') AS Branch " & _
            '                   ",ISNULL(hrdepartment_master.name,'') AS Department " & _
            '                   ",ISNULL(hrjob_master.job_name,'') AS POSITION " & _
            '                   ",ISNULL(hremployee_master.present_address1,'')+' '+ISNULL(hremployee_master.present_address2,'') AS ADDRESS " & _
            '                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '                   ",ISNULL(hrmsConfiguration..cfstate_master.name,'') AS STATE " & _
            '                   ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '                   ",ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS ZipCode " & _
            '                   ",ISNULL(hremployee_master.email,'') AS Email " & _
            '                   ",ISNULL(hremployee_master.present_tel_no,'') AS PhoneNo " & _
            '                   ",hrtraining_scheduling.trainingschedulingunkid AS trainingschedulingunkid " & _
            '                   ",hremployee_master.employeeunkid " & _
            '                   ",hrinstitute_master.instituteunkid AS instituteunkid " & _
            '                   ",hrstation_master.stationunkid AS stationunkid " & _
            '                   ",hrdepartment_master.departmentunkid AS departmentunkid " & _
            '                   ",hrjob_master.jobunkid AS jobunkid " & _
            '              "FROM hrtraining_scheduling " & _
            '                   "JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
            '                   "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfzipcode_master ON dbo.hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_master.present_stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
            '                   "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '                   "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '                   "LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
            '                   "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcity_master ICity ON hrinstitute_master.cityunkid = ICity.cityunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfstate_master IState ON hrinstitute_master.stateunkid = IState.stateunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcountry_master ICountry ON ICountry.countryunkid = hrinstitute_master.countryunkid " & _
            '              "WHERE hrtraining_scheduling.iscancel=0 AND hrtraining_scheduling.isvoid=0 "

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'StrQ = "SELECT " & _
            '         " TrainingName AS TrainingName " & _
            '         ",StDate AS StDate " & _
            '         ",EdDate AS EdDate " & _
            '         ",Venue AS Venue " & _
            '         ",StTime AS StTime " & _
            '         ",EdTime AS EdTime " & _
            '         ",DAYS*Hrs AS TotalHrs " & _
            '         ",TName+' ,'+TAddress+' '+InsCountry+' '+InsState+' '+InsCity AS TName " & _
            '         ",TEmail AS TEmail " & _
            '         ",EmpName AS EmpName " & _
            '         ",Branch AS Branch " & _
            '         ",Department AS Department " & _
            '         ",Position AS Position " & _
            '         ",Address+' '+ Country +' '+ State+' '+City+' '+ZipCode AS Address " & _
            '         ",Email AS Email " & _
            '         ",PhoneNo AS PhoneNo " & _
            '         ",Score AS Score " & _
            '         ",employeeunkid AS employeeunkid " & _
            '         ",trainingschedulingunkid AS trainingschedulingunkid " & _
            '         ",instituteunkid AS instituteunkid " & _
            '         ",stationunkid AS stationunkid " & _
            '         ",departmentunkid AS departmentunkid " & _
            '         ",jobunkid AS jobunkid " & _
            '         ",ISNULL(Rid,-1) AS Rid " & _
            '    "FROM " & _
            '         "( " & _
            '              "SELECT " & _
            '                  " cfcommon_master.name AS TrainingName " & _
            '                   ",CONVERT(CHAR(8),start_date,112) AS StDate " & _
            '                   ",CONVERT(CHAR(8),end_date,112) AS EdDate " & _
            '                   ",ISNULL(training_venue,'')  AS Venue " & _
            '                   ",CONVERT(CHAR(5),start_time,108) AS StTime " & _
            '                   ",CONVERT(CHAR(5),end_time,108) AS EdTime " & _
            '                   ",DATEDIFF(dd,start_date,end_date) AS DAYS " & _
            '                   ",DATEDIFF(Hour,start_time,end_time) AS Hrs " & _
            '                   ",ISNULL(hrinstitute_master.institute_name,'') AS TName " & _
            '                   ",ISNULL(hrinstitute_master.institute_address,'') AS TAddress " & _
            '                   ",ISNULL(ICountry.country_name,'') AS InsCountry " & _
            '                   ",ISNULL(IState.name,'') AS InsState " & _
            '                   ",ISNULL(ICity.name,'') AS InsCity " & _
            '                   ",ISNULL(hrinstitute_master.institute_email,'') AS TEmail "

            StrQ = "SELECT " & _
                     " TrainingName AS TrainingName " & _
                     ",StDate AS StDate " & _
                     ",EdDate AS EdDate " & _
                     ",Venue AS Venue " & _
                     ",StTime AS StTime " & _
                     ",EdTime AS EdTime " & _
                     ",DAYS*Hrs AS TotalHrs " & _
                     ",TName+' ,'+TAddress+' '+InsCountry+' '+InsState+' '+InsCity AS TName " & _
                     ",TEmail AS TEmail " & _
                     ",EmpName AS EmpName " & _
                     ",Branch AS Branch " & _
                     ",Department AS Department " & _
                     ",Position AS Position " & _
                     ",Address+' '+ Country +' '+ State+' '+City+' '+ZipCode AS Address " & _
                     ",Email AS Email " & _
                     ",PhoneNo AS PhoneNo " & _
                     ",Score AS Score " & _
                     ",employeeunkid AS employeeunkid " & _
                     ",trainingschedulingunkid AS trainingschedulingunkid " & _
                     ",instituteunkid AS instituteunkid " & _
                     ",stationunkid AS stationunkid " & _
                     ",departmentunkid AS departmentunkid " & _
                     ",jobunkid AS jobunkid " & _
                     ",ISNULL(Rid,-1) AS Rid " & _
                     ",allocationtypeunkid " & _
                "FROM " & _
                     "( " & _
                          "SELECT " & _
                              " cfcommon_master.name AS TrainingName " & _
                               ",CONVERT(CHAR(8),start_date,112) AS StDate " & _
                               ",CONVERT(CHAR(8),end_date,112) AS EdDate " & _
                               ",ISNULL(training_venue,'')  AS Venue " & _
                               ",CONVERT(CHAR(5),start_time,108) AS StTime " & _
                               ",CONVERT(CHAR(5),end_time,108) AS EdTime " & _
                               ",DATEDIFF(dd,start_date,end_date) AS DAYS " & _
                               ",DATEDIFF(HOUR, CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), start_time, 108) AS DATETIME), CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), end_time, 108) AS DATETIME)) AS Hrs " & _
                               ",ISNULL(hrinstitute_master.institute_name,'') AS TName " & _
                               ",ISNULL(hrinstitute_master.institute_address,'') AS TAddress " & _
                               ",ISNULL(ICountry.country_name,'') AS InsCountry " & _
                               ",ISNULL(IState.name,'') AS InsState " & _
                               ",ISNULL(ICity.name,'') AS InsCity " & _
                               ",ISNULL(hrinstitute_master.institute_email,'') AS TEmail "
            'S.SANDEEP [08-FEB-2017] -- END

            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            '",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EmpName "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",ISNULL(hrstation_master.name,'') AS Branch " & _
            '                   ",ISNULL(hrdepartment_master.name,'') AS Department " & _
            '                   ",ISNULL(hrjob_master.job_name,'') AS POSITION " & _
            '                   ",ISNULL(hremployee_master.present_address1,'')+' '+ISNULL(hremployee_master.present_address2,'') AS ADDRESS " & _
            '                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '                   ",ISNULL(hrmsConfiguration..cfstate_master.name,'') AS STATE " & _
            '                   ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '                   ",ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS ZipCode " & _
            '                   ",ISNULL(hremployee_master.email,'') AS Email " & _
            '                   ",ISNULL(hremployee_master.present_tel_no,'') AS PhoneNo " & _
            '                   ",hrtraining_scheduling.trainingschedulingunkid AS trainingschedulingunkid " & _
            '                   ",hremployee_master.employeeunkid " & _
            '                   ",hrinstitute_master.instituteunkid AS instituteunkid " & _
            '                   ",hrstation_master.stationunkid AS stationunkid " & _
            '                   ",hrdepartment_master.departmentunkid AS departmentunkid " & _
            '                   ",hrjob_master.jobunkid AS jobunkid " & _
            '              "FROM hrtraining_scheduling " & _
            '                   "JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
            '                   "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfzipcode_master ON dbo.hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_master.present_stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
            '                   "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '                   "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '                   "LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
            '                   "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcity_master ICity ON hrinstitute_master.cityunkid = ICity.cityunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfstate_master IState ON hrinstitute_master.stateunkid = IState.stateunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcountry_master ICountry ON ICountry.countryunkid = hrinstitute_master.countryunkid " & _
            '                  "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) " & _
            '              "WHERE hrtraining_scheduling.iscancel=0 AND hrtraining_scheduling.isvoid=0 "

            ''Anjan (10 Feb 2012)-End 


            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= " AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            ''Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            'StrQ &= ",ISNULL(hrstation_master.name,'') AS Branch " & _
            '                   ",ISNULL(hrdepartment_master.name,'') AS Department " & _
            '                   ",ISNULL(hrjob_master.job_name,'') AS POSITION " & _
            '                   ",ISNULL(hremployee_master.present_address1,'')+' '+ISNULL(hremployee_master.present_address2,'') AS ADDRESS " & _
            '                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
            '                   ",ISNULL(hrmsConfiguration..cfstate_master.name,'') AS STATE " & _
            '                   ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
            '                   ",ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS ZipCode " & _
            '                   ",ISNULL(hremployee_master.email,'') AS Email " & _
            '                   ",ISNULL(hremployee_master.present_tel_no,'') AS PhoneNo " & _
            '                   ",hrtraining_scheduling.trainingschedulingunkid AS trainingschedulingunkid " & _
            '                   ",hremployee_master.employeeunkid " & _
            '                   ",hrinstitute_master.instituteunkid AS instituteunkid " & _
            '                   ",hrstation_master.stationunkid AS stationunkid " & _
            '                   ",hrdepartment_master.departmentunkid AS departmentunkid " & _
            '                   ",hrjob_master.jobunkid AS jobunkid " & _
            '              "FROM hrtraining_scheduling " & _
            '                   "JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
            '                   "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfzipcode_master ON dbo.hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_master.present_stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
            '                  "LEFT JOIN " & _
            '                  "     ( " & _
            '                  "         SELECT " & _
            '                  "             jobunkid " & _
            '                  "             ,employeeunkid " & _
            '                  "             ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                  "         FROM hremployee_categorization_tran " & _
            '                  "         WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                  "     ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
            '                  "JOIN hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid " & _
            '                  "LEFT JOIN " & _
            '                  "     ( " & _
            '                  "         SELECT " & _
            '                  "             departmentunkid " & _
            '                  "             ,stationunkid " & _
            '                  "             ,employeeunkid " & _
            '                  "             ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                  "         FROM hremployee_transfer_tran " & _
            '                  "         WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                  "     ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
            '                  "JOIN hrdepartment_master ON Alloc.departmentunkid = hrdepartment_master.departmentunkid " & _
            '                  "LEFT JOIN hrstation_master ON Alloc.stationunkid = hrstation_master.stationunkid " & _
            '                   "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcity_master ICity ON hrinstitute_master.cityunkid = ICity.cityunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfstate_master IState ON hrinstitute_master.stateunkid = IState.stateunkid " & _
            '                   "LEFT JOIN hrmsConfiguration..cfcountry_master ICountry ON ICountry.countryunkid = hrinstitute_master.countryunkid " & _
            '                  "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) "

            StrQ &= ",ISNULL(hrstation_master.name,'') AS Branch " & _
                               ",ISNULL(hrdepartment_master.name,'') AS Department " & _
                               ",ISNULL(hrjob_master.job_name,'') AS POSITION " & _
                               ",ISNULL(hremployee_master.present_address1,'')+' '+ISNULL(hremployee_master.present_address2,'') AS ADDRESS " & _
                               ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                               ",ISNULL(hrmsConfiguration..cfstate_master.name,'') AS STATE " & _
                               ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS City " & _
                               ",ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS ZipCode " & _
                               ",ISNULL(hremployee_master.email,'') AS Email " & _
                               ",ISNULL(hremployee_master.present_tel_no,'') AS PhoneNo " & _
                               ",hrtraining_scheduling.trainingschedulingunkid AS trainingschedulingunkid " & _
                               ",hremployee_master.employeeunkid " & _
                               ",hrinstitute_master.instituteunkid AS instituteunkid " & _
                               ",hrstation_master.stationunkid AS stationunkid " & _
                               ",hrdepartment_master.departmentunkid AS departmentunkid " & _
                               ",hrjob_master.jobunkid AS jobunkid " & _
                               ",hrtraining_scheduling.allocationtypeunkid " & _
                          "FROM hrtraining_scheduling " & _
                               "JOIN hrtraining_enrollment_tran ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_enrollment_tran.trainingschedulingunkid " & _
                               "JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfzipcode_master ON dbo.hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfcity_master ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfstate_master ON hremployee_master.present_stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                              "LEFT JOIN " & _
                              "     ( " & _
                              "         SELECT " & _
                              "             jobunkid " & _
                              "             ,employeeunkid " & _
                              "             ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                              "         FROM hremployee_categorization_tran " & _
                              "         WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                              "     ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                              "JOIN hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                              "LEFT JOIN " & _
                              "     ( " & _
                              "         SELECT " & _
                              "             departmentunkid " & _
                              "             ,stationunkid " & _
                              "             ,employeeunkid " & _
                              "             ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                              "         FROM hremployee_transfer_tran " & _
                              "         WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                              "     ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                              "JOIN hrdepartment_master ON Alloc.departmentunkid = hrdepartment_master.departmentunkid " & _
                              "LEFT JOIN hrstation_master ON Alloc.stationunkid = hrstation_master.stationunkid " & _
                               "LEFT JOIN hrinstitute_master ON hrtraining_scheduling.traininginstituteunkid = hrinstitute_master.instituteunkid AND ishospital = 0 " & _
                               "LEFT JOIN hrmsConfiguration..cfcity_master ICity ON hrinstitute_master.cityunkid = ICity.cityunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfstate_master IState ON hrinstitute_master.stateunkid = IState.stateunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfcountry_master ICountry ON ICountry.countryunkid = hrinstitute_master.countryunkid " & _
                              "JOIN cfcommon_master ON cfcommon_master.masterunkid = CAST(hrtraining_scheduling.course_title AS INT) "
            'S.SANDEEP [08-FEB-2017] -- END


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE hrtraining_scheduling.iscancel=0 AND hrtraining_scheduling.isvoid=0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End

            StrQ &= ") AS Scheduling " & _
                "LEFT JOIN " & _
                     "( " & _
                          "SELECT " & _
                                "trainingschedulingunkid AS TId " & _
                               ",ISNULL(Score,'') AS Score " & _
                               ",employeeunkid AS EmpId " & _
                               ",Rid AS Rid " & _
                          "FROM hrtraining_enrollment_tran " & _
                          "LEFT JOIN " & _
                               "( " & _
                                    "SELECT " & _
                                          "trainingschedulingunkid AS TId " & _
                                         ",Score AS Score " & _
                                         ",trainingenrolltranunkid AS trainingenrolltranunkid " & _
                                         ",Rid AS Rid " & _
                                    "FROM " & _
                                         "( " & _
                                              "SELECT " & _
                                                    "trainer_level_id AS Id " & _
                                                   ",hrtraining_trainers_tran.trainingschedulingunkid " & _
                                                   ",ISNULL(hrresult_master.resultname,'') AS Score " & _
                                                   ",row_number() OVER ( PARTITION BY hrtraining_analysis_master.trainingenrolltranunkid ORDER BY hrtraining_trainers_tran.trainer_level_id DESC ) rn " & _
                                                   ",hrtraining_analysis_master.trainingenrolltranunkid " & _
                                                   ",hrresult_master.resultunkid AS Rid " & _
                                              "FROM dbo.hrtraining_trainers_tran " & _
                                                   "LEFT JOIN hrtraining_analysis_tran ON hrtraining_trainers_tran.trainerstranunkid = hrtraining_analysis_tran.trainerstranunkid " & _
                                                   "LEFT JOIN hrtraining_analysis_master ON hrtraining_analysis_tran.analysisunkid = hrtraining_analysis_master.analysisunkid " & _
                                                   "LEFT JOIN hrresult_master ON hrtraining_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                                              "WHERE hrtraining_trainers_tran.iscancel = 0 AND hrtraining_analysis_master.isvoid = 0 " & _
                                                   "AND iscomplete = 1 " & _
                                         ") AS Score " & _
                                    "WHERE Score.rn = 1 " & _
                               ") AS A ON A.trainingenrolltranunkid = hrtraining_enrollment_tran.trainingenrolltranunkid " & _
                     ") AS B ON B.TId = Scheduling.trainingschedulingunkid AND B.EmpId = Scheduling.employeeunkid " & _
                "WHERE 1=1 "
            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " ORDER BY trainingschedulingunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim strGroupId As String = String.Empty
            Dim intCnt As Integer = 1

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                If mstrAllocationIds.Trim.Length > 0 Then
                    If objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                End If
                Dim iOwnerGroupName As String = String.Empty
                iOwnerGroupName = ""
                Select Case CInt(dtRow.Item("allocationtypeunkid"))
                    Case enAllocation.BRANCH
                        iOwnerGroupName = Language.getMessage("clsMasterData", 430, "Branch")
                    Case enAllocation.DEPARTMENT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 429, "Department Group")
                    Case enAllocation.DEPARTMENT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 428, "Department")
                    Case enAllocation.SECTION_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 427, "Section Group")
                    Case enAllocation.SECTION
                        iOwnerGroupName = Language.getMessage("clsMasterData", 426, "Section")
                    Case enAllocation.UNIT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    Case enAllocation.UNIT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 424, "Unit")
                    Case enAllocation.TEAM
                        iOwnerGroupName = Language.getMessage("clsMasterData", 423, "Team")
                    Case enAllocation.JOB_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 422, "Job Group")
                    Case enAllocation.JOBS
                        iOwnerGroupName = Language.getMessage("clsMasterData", 421, "Jobs")
                    Case enAllocation.CLASS_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 420, "Class Group")
                    Case enAllocation.CLASSES
                        iOwnerGroupName = Language.getMessage("clsMasterData", 419, "Classes")
                    Case enAllocation.COST_CENTER
                        iOwnerGroupName = Language.getMessage("clsMasterData", 586, "Cost Center")
                End Select
                rpt_Row.Item("Column18") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds)
                rpt_Row.Item("Column18") = rpt_Row.Item("Column18").ToString.Replace("&lt;", "<")
                rpt_Row.Item("Column18") = rpt_Row.Item("Column18").ToString.Replace("&gt;", ">")
                rpt_Row.Item("Column18") = rpt_Row.Item("Column18").ToString.Replace("&amp;", "&")
                'S.SANDEEP [08-FEB-2017] -- END

                If strGroupId <> CStr(dtRow.Item("trainingschedulingunkid")) Then
                    intCnt = 1
                    strGroupId = CStr(dtRow.Item("trainingschedulingunkid"))
                End If

                rpt_Row.Item("Column1") = dtRow.Item("TrainingName")
                rpt_Row.Item("Column2") = eZeeDate.convertDate(dtRow.Item("StDate").ToString).ToShortDateString
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EdDate").ToString).ToShortDateString
                rpt_Row.Item("Column4") = dtRow.Item("Venue")
                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                'rpt_Row.Item("Column5") = dtRow.Item("StTime")
                rpt_Row.Item("Column5") = dtRow.Item("StTime") & " - " & dtRow.Item("EdTime")
                'S.SANDEEP [08-FEB-2017] -- END
                rpt_Row.Item("Column6") = dtRow.Item("EdTime")
                rpt_Row.Item("Column7") = dtRow.Item("TotalHrs")
                rpt_Row.Item("Column8") = dtRow.Item("TName")
                rpt_Row.Item("Column9") = dtRow.Item("TEmail")
                rpt_Row.Item("Column10") = dtRow.Item("EmpName")
                rpt_Row.Item("Column11") = dtRow.Item("Branch")
                rpt_Row.Item("Column12") = dtRow.Item("Department")
                rpt_Row.Item("Column13") = dtRow.Item("Position")
                rpt_Row.Item("Column14") = dtRow.Item("Address")
                rpt_Row.Item("Column15") = dtRow.Item("Email")
                rpt_Row.Item("Column16") = dtRow.Item("PhoneNo")
                rpt_Row.Item("Column17") = dtRow.Item("Score")
                rpt_Row.Item("Column19") = strGroupId.ToString
                rpt_Row.Item("Column20") = intCnt.ToString

                intCnt += 1
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingListReport

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtCAddress", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 27, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 28, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 29, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 23, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            'Sandeep [ 10 FEB 2011 ] -- End

            objRpt.SetDataSource(rpt_Data)

            Dim strAddress As String = String.Empty
            Dim objMaster As New clsMasterData
            Dim objZipcode As New clszipcode_master
            Dim objState As New clsstate_master
            Dim objCity As New clscity_master
            Dim dsCounrty As DataSet = objMaster.getCountryList("List", False, Company._Object._Countryunkid)
            If dsCounrty.Tables(0).Rows.Count > 0 Then
                strAddress &= dsCounrty.Tables(0).Rows(0)("country_name").ToString & " "
            End If
            objState._Stateunkid = Company._Object._Stateunkid
            objCity._Cityunkid = Company._Object._Cityunkid
            objZipcode._Zipcodeunkid = Company._Object._Postalunkid

            strAddress &= objState._Name & " " & objCity._Name & " " & objZipcode._Zipcode_No
            objMaster = Nothing
            objZipcode = Nothing
            objState = Nothing
            objCity = Nothing

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtCAddress", strAddress)
            Call ReportFunction.TextChange(objRpt, "lblNameOfTraining", Language.getMessage(mstrModuleName, 1, "Name of training"))
            Call ReportFunction.TextChange(objRpt, "lblTrainuingDateFrom", Language.getMessage(mstrModuleName, 2, "Training Date From :"))
            Call ReportFunction.TextChange(objRpt, "lblVenue", Language.getMessage(mstrModuleName, 3, "Venue"))
            Call ReportFunction.TextChange(objRpt, "lblTo", Language.getMessage(mstrModuleName, 4, "To :"))
            Call ReportFunction.TextChange(objRpt, "lblTrainingDuration", Language.getMessage(mstrModuleName, 5, "Training Duration"))
            Call ReportFunction.TextChange(objRpt, "lblTotalHours", Language.getMessage(mstrModuleName, 6, "Total Hours"))
            Call ReportFunction.TextChange(objRpt, "lblNameAndAddressofTrainers", Language.getMessage(mstrModuleName, 7, "Name and Address of Trainers"))
            Call ReportFunction.TextChange(objRpt, "lblEmailOfTrainers", Language.getMessage(mstrModuleName, 8, "Email of Trainers"))
            Call ReportFunction.TextChange(objRpt, "txtParticipantName", Language.getMessage(mstrModuleName, 9, "Participant Name"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 10, "Branch"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 11, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtPosition", Language.getMessage(mstrModuleName, 12, "Position"))
            Call ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 13, "Address"))
            Call ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 14, "Email"))
            Call ReportFunction.TextChange(objRpt, "txtPhone", Language.getMessage(mstrModuleName, 15, "Phone"))
            Call ReportFunction.TextChange(objRpt, "txtTestScore", Language.getMessage(mstrModuleName, 16, "Test Score"))
            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            Call ReportFunction.TextChange(objRpt, "lblTrainingAllocation", Language.getMessage(mstrModuleName, 100, "Allocation Binded"))
            'S.SANDEEP [08-FEB-2017] -- END

            'Sandeep [ 10 FEB 2011 ] -- Start
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 24, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 25, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 26, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            'Sandeep [ 10 FEB 2011 ] -- End 


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'S.SANDEEP [15-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
    Private Function Generate_DetailReportNew(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT " & _
                    "     CT.name AS course_title " & _
                    "    ,CONVERT(NVARCHAR(8),TS.start_date,112) AS tstart_date " & _
                    "    ,CONVERT(NVARCHAR(8),TS.end_date,112) AS tend_date " & _
                    "    ,ISNULL(hrinstitute_master.institute_name,'') AS institution " & _
                    "    ,'' AS sponser " & _
                    "    ,'' AS allocation_binded " & _
                    "    ,ISNULL(Tn.Trns,ISNULL(hrinstitute_master.institute_name,'') +' ,'+ISNULL(hrinstitute_master.institute_address,'')+', '+ISNULL(hrinstitute_master.institute_email,'')+' '+ISNULL(ICountry.country_name,'')+' '+ISNULL(IState.name,'')+' '+ISNULL(ICity.name,'')) AS trainers " & _
                    "    ,(DATEDIFF(dd,start_date,end_date) + 1) * DATEDIFF(HOUR, CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), start_time, 108) AS DATETIME), CAST(CONVERT(CHAR(8),GETDATE(),112) +' '+ CONVERT(CHAR(8), end_time, 108) AS DATETIME)) AS TotalHrs " & _
                    "    ,CONVERT(CHAR(5),start_time,108) AS StTime " & _
                    "    ,CONVERT(CHAR(5),end_time,108) AS EdTime " & _
                    "    ,(DATEDIFF(dd,start_date,end_date) + 1) AS total_days " & _
                    "    ,TS.training_venue "
            If mblnFirstNamethenSurname = False Then
                StrQ &= "   ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS ename "
            Else
                StrQ &= "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS ename "
            End If
            StrQ &= "    ,(CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),hremployee_master.birthdate,112) AS INT))/10000 AS Age " & _
                    "    ,ISNULL(ejm.job_name,'') AS job_name " & _
                    "    ,CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS appointmentdate " & _
                    "    ,Qlf.qualificationname AS current_qualification " & _
                    "    ,ISNULL(hrresult_master.resultname,'') AS gpa " & _
                    "    ,ISNULL(CAST(TAT.gpa_value AS NVARCHAR(100)),'') AS grade " & _
                    "    ,TS.allocationtypeunkid " & _
                    "    ,TS.trainingschedulingunkid " & _
                    "    ,TS.fundingids " & _
                    "    ,ISNULL(esm.name,'') AS ebranch " & _
                    "    ,ISNULL(edm.name,'') AS edepartment " & _
                    "FROM hrtraining_enrollment_tran AS TE " & _
                    "    JOIN hrtraining_scheduling AS TS ON TS.trainingschedulingunkid  = TE.trainingschedulingunkid " & _
                    "    LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = TS.traininginstituteunkid AND hrinstitute_master.ishospital = 0 " & _
                    "    LEFT JOIN hrmsConfiguration..cfcity_master ICity ON hrinstitute_master.cityunkid = ICity.cityunkid " & _
                    "    LEFT JOIN hrmsConfiguration..cfstate_master IState ON hrinstitute_master.stateunkid = IState.stateunkid " & _
                    "    LEFT JOIN hrmsConfiguration..cfcountry_master ICountry ON ICountry.countryunkid = hrinstitute_master.countryunkid " & _
                    "    LEFT JOIN hrtraining_analysis_master TAM ON TE.trainingenrolltranunkid = TAM.trainingenrolltranunkid " & _
                    "    LEFT JOIN hrtraining_analysis_tran TAT ON TAT.analysisunkid = TAM.analysisunkid " & _
                    "    LEFT JOIN hrresult_master ON TAT.resultunkid = hrresult_master.resultunkid " & _
                    "    JOIN cfcommon_master AS CT ON CT.masterunkid = TS.course_title AND CT.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                    "    JOIN hremployee_master AS hremployee_master ON TE.employeeunkid = hremployee_master.employeeunkid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            CT.employeeunkid AS empid " & _
                    "           ,CT.jobunkid AS jobid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rn " & _
                    "       FROM hremployee_categorization_tran AS CT " & _
                    "       WHERE CT.isvoid = 0 AND CONVERT(NVARCHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS JB ON hremployee_master.employeeunkid = JB.empid AND JB.rn = 1 " & _
                    "    LEFT JOIN hrjob_master AS ejm ON ejm.jobunkid = JB.jobid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            TT.employeeunkid AS empid " & _
                    "           ,TT.stationunkid AS stid " & _
                    "           ,TT.departmentunkid AS dtid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY TT.employeeunkid ORDER BY TT.effectivedate DESC) AS rn " & _
                    "       FROM hremployee_transfer_tran AS TT " & _
                    "       WHERE TT.isvoid = 0 AND CONVERT(NVARCHAR(8),TT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS AL ON hremployee_master.employeeunkid = AL.empid AND AL.rn = 1 " & _
                    "    LEFT JOIN hrstation_master AS esm ON AL.stid = esm.stationunkid " & _
                    "    LEFT JOIN hrdepartment_master as edm ON AL.dtid = edm.departmentunkid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            hrqualification_master.qualificationname " & _
                    "           ,hrqualification_master.qualificationcode " & _
                    "           ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS QStartDate " & _
                    "           ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS QEndDate " & _
                    "           ,YEAR(hremp_qualification_tran.award_start_date) AS QStartYear " & _
                    "           ,YEAR(hremp_qualification_tran.award_end_date) AS QEndYear " & _
                    "           ,cfcommon_master.qlevel AS QLevel " & _
                    "           ,hremp_qualification_tran.employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY hremp_qualification_tran.employeeunkid ORDER BY cfcommon_master.qlevel DESC,ISNULL(hremp_qualification_tran.award_start_date,GETDATE()) DESC) AS rno " & _
                    "       FROM hremp_qualification_tran " & _
                    "           JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "           JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                    "    WHERE hremp_qualification_tran.isvoid = 0 AND hremp_qualification_tran.qualificationgroupunkid <> 0 AND hremp_qualification_tran.qualificationunkid <> 0 " & _
                    "    ) AS Qlf ON Qlf.employeeunkid = hremployee_master.employeeunkid  AND Qlf.rno = 1 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "       SELECT " & _
                    "            hrtraining_trainers_tran.trainingschedulingunkid " & _
                    "           ,STUFF((SELECT ',' + CASE WHEN TT.employeeunkid <=0 THEN TT.other_name + CASE WHEN ISNULL(TT.other_contactno,'') <> '' THEN ' - ' + TT.other_contactno ELSE '' END " & _
                    "                                     WHEN TT.employeeunkid > 0 THEN hremployee_master.employeecode+' - '+hremployee_master.firstname + ' '+ hremployee_master.surname + CASE WHEN ISNULL(hremployee_master.email,'') <> '' THEN ' - ' + hremployee_master.email ELSE '' END " & _
                    "                                END " & _
                    "                   FROM hrtraining_trainers_tran AS TT " & _
                    "                       LEFT JOIN hremployee_master ON TT.employeeunkid = hremployee_master.employeeunkid " & _
                    "                   WHERE TT.isvoid = 0 AND TT.iscancel = 0 AND tt.trainingschedulingunkid = hrtraining_trainers_tran.trainingschedulingunkid " & _
                    "               FOR XML PATH('')),1,1,'') As Trns " & _
                    "       FROM hrtraining_trainers_tran " & _
                    "       WHERE hrtraining_trainers_tran.isvoid = 0 AND hrtraining_trainers_tran.iscancel = 0 " & _
                    "       GROUP BY hrtraining_trainers_tran.trainingschedulingunkid " & _
                    "    ) AS Tn ON Tn.trainingschedulingunkid = TS.trainingschedulingunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE TE.isvoid = 0 AND TE.iscancel = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= " ORDER BY TS.trainingschedulingunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim strGroupId As String = String.Empty
            Dim intCnt As Integer = 1
            Dim StrFunds As String = String.Empty
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                StrFunds = ""
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                Dim objTrainingAlloc As New clsTraining_Allocation_Tran
                If mstrAllocationIds.Trim.Length > 0 Then
                    If objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds).Trim.Length <= 0 Then Continue For
                End If
                Dim iOwnerGroupName As String = String.Empty
                iOwnerGroupName = ""
                Select Case CInt(dtRow.Item("allocationtypeunkid"))
                    Case enAllocation.BRANCH
                        iOwnerGroupName = Language.getMessage("clsMasterData", 430, "Branch")
                    Case enAllocation.DEPARTMENT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 429, "Department Group")
                    Case enAllocation.DEPARTMENT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 428, "Department")
                    Case enAllocation.SECTION_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 427, "Section Group")
                    Case enAllocation.SECTION
                        iOwnerGroupName = Language.getMessage("clsMasterData", 426, "Section")
                    Case enAllocation.UNIT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    Case enAllocation.UNIT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 424, "Unit")
                    Case enAllocation.TEAM
                        iOwnerGroupName = Language.getMessage("clsMasterData", 423, "Team")
                    Case enAllocation.JOB_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 422, "Job Group")
                    Case enAllocation.JOBS
                        iOwnerGroupName = Language.getMessage("clsMasterData", 421, "Jobs")
                    Case enAllocation.CLASS_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 420, "Class Group")
                    Case enAllocation.CLASSES
                        iOwnerGroupName = Language.getMessage("clsMasterData", 419, "Classes")
                    Case enAllocation.COST_CENTER
                        iOwnerGroupName = Language.getMessage("clsMasterData", 586, "Cost Center")
                End Select
                rpt_Row.Item("Column5") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objTrainingAlloc.GetCSV_AllocationName(dtRow.Item("allocationtypeunkid"), dtRow.Item("trainingschedulingunkid"), mstrAllocationIds)
                rpt_Row.Item("Column5") = rpt_Row.Item("Column5").ToString.Replace("&lt;", "<")
                rpt_Row.Item("Column5") = rpt_Row.Item("Column5").ToString.Replace("&gt;", ">")
                rpt_Row.Item("Column5") = rpt_Row.Item("Column5").ToString.Replace("&amp;", "&")
                If strGroupId <> CStr(dtRow.Item("trainingschedulingunkid")) Then
                    intCnt = 1
                    strGroupId = CStr(dtRow.Item("trainingschedulingunkid"))
                End If
                rpt_Row.Item("Column1") = dtRow.Item("course_title")
                rpt_Row.Item("Column2") = eZeeDate.convertDate(dtRow.Item("tstart_date").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("tend_date").ToString).ToShortDateString
                rpt_Row.Item("Column3") = dtRow.Item("institution")
                For Each StrId As String In dtRow.Item("fundingids").ToString.Split(",")
                    If StrId.Trim.Length > 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Masterunkid = StrId
                        StrFunds &= "," & objCMaster._Name
                        objCMaster = Nothing
                    End If
                Next
                If StrFunds.Trim.Length > 0 Then
                    StrFunds = Mid(StrFunds, 2)
                End If
                rpt_Row.Item("Column4") = StrFunds
                rpt_Row.Item("Column6") = dtRow.Item("trainers")
                rpt_Row.Item("Column7") = dtRow.Item("TotalHrs")
                rpt_Row.Item("Column8") = dtRow.Item("StTime") & " - " & dtRow.Item("EdTime")
                rpt_Row.Item("Column9") = dtRow.Item("total_days")
                rpt_Row.Item("Column10") = dtRow.Item("training_venue")
                rpt_Row.Item("Column11") = intCnt.ToString
                rpt_Row.Item("Column12") = dtRow.Item("ename")
                rpt_Row.Item("Column13") = dtRow.Item("Age")
                rpt_Row.Item("Column14") = dtRow.Item("job_name")
                rpt_Row.Item("Column15") = eZeeDate.convertDate(dtRow.Item("appointmentdate").ToString).ToShortDateString
                rpt_Row.Item("Column16") = dtRow.Item("current_qualification")
                rpt_Row.Item("Column17") = dtRow.Item("gpa")
                rpt_Row.Item("Column18") = dtRow.Item("grade")
                rpt_Row.Item("Column19") = dtRow.Item("ebranch")
                rpt_Row.Item("Column20") = dtRow.Item("edepartment")

                intCnt += 1
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTrainingListReportNew

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtCAddress", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 27, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 28, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 29, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 23, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Dim strAddress As String = String.Empty
            Dim objMaster As New clsMasterData
            Dim objZipcode As New clszipcode_master
            Dim objState As New clsstate_master
            Dim objCity As New clscity_master
            Dim dsCounrty As DataSet = objMaster.getCountryList("List", False, Company._Object._Countryunkid)
            If dsCounrty.Tables(0).Rows.Count > 0 Then
                strAddress &= dsCounrty.Tables(0).Rows(0)("country_name").ToString & " "
            End If
            objState._Stateunkid = Company._Object._Stateunkid
            objCity._Cityunkid = Company._Object._Cityunkid
            objZipcode._Zipcodeunkid = Company._Object._Postalunkid

            strAddress &= objState._Name & " " & objCity._Name & " " & objZipcode._Zipcode_No
            objMaster = Nothing
            objZipcode = Nothing
            objState = Nothing
            objCity = Nothing

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "txtCAddress", strAddress)

            Call ReportFunction.TextChange(objRpt, "lblNameOfTraining", Language.getMessage(mstrModuleName, 200, "Training Name :"))
            Call ReportFunction.TextChange(objRpt, "lblTrainuingDateFrom", Language.getMessage(mstrModuleName, 201, "Scheduled Dates : "))
            Call ReportFunction.TextChange(objRpt, "lblTrainingInstitute", Language.getMessage(mstrModuleName, 202, "Provider :"))
            Call ReportFunction.TextChange(objRpt, "lblVenue", Language.getMessage(mstrModuleName, 203, "Venue :"))
            Call ReportFunction.TextChange(objRpt, "lblTrainingDuration", Language.getMessage(mstrModuleName, 204, "Time Duration :"))
            Call ReportFunction.TextChange(objRpt, "lblTotalHours", Language.getMessage(mstrModuleName, 205, "Total Hours : "))
            Call ReportFunction.TextChange(objRpt, "lblTotalDays", Language.getMessage(mstrModuleName, 206, "Total Days :"))
            Call ReportFunction.TextChange(objRpt, "lblTrainingSponsers", Language.getMessage(mstrModuleName, 207, "Sponsers :"))
            Call ReportFunction.TextChange(objRpt, "lblTrainingAllocation", Language.getMessage(mstrModuleName, 208, "Allocation Binded :"))
            Call ReportFunction.TextChange(objRpt, "lblNameAndAddressofTrainers", Language.getMessage(mstrModuleName, 209, "Trainers :"))
            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 210, "S/N"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 211, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtAge", Language.getMessage(mstrModuleName, 212, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtEmpJob", Language.getMessage(mstrModuleName, 213, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtAppDate", Language.getMessage(mstrModuleName, 214, "Appointment Date"))
            Call ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 215, "Current Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtResults", Language.getMessage(mstrModuleName, 216, "Results"))
            Call ReportFunction.TextChange(objRpt, "txtGPA", Language.getMessage(mstrModuleName, 217, "GPA"))
            Call ReportFunction.TextChange(objRpt, "txtGrade", Language.getMessage(mstrModuleName, 218, "Grade"))

            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 219, "Branch"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 220, "Department"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 24, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 25, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [15-FEB-2017] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 586, "Cost Center")
            Language.setMessage(mstrModuleName, 1, "Name of training")
            Language.setMessage(mstrModuleName, 2, "Training Date From :")
            Language.setMessage(mstrModuleName, 3, "Venue")
            Language.setMessage(mstrModuleName, 4, "To :")
            Language.setMessage(mstrModuleName, 5, "Training Duration")
            Language.setMessage(mstrModuleName, 6, "Total Hours")
            Language.setMessage(mstrModuleName, 7, "Name and Address of Trainers")
            Language.setMessage(mstrModuleName, 8, "Email of Trainers")
            Language.setMessage(mstrModuleName, 9, "Participant Name")
            Language.setMessage(mstrModuleName, 10, "Branch")
            Language.setMessage(mstrModuleName, 11, "Department")
            Language.setMessage(mstrModuleName, 12, "Position")
            Language.setMessage(mstrModuleName, 13, "Address")
            Language.setMessage(mstrModuleName, 14, "Email")
            Language.setMessage(mstrModuleName, 15, "Phone")
            Language.setMessage(mstrModuleName, 16, "Test Score")
            Language.setMessage(mstrModuleName, 17, "Training :")
            Language.setMessage(mstrModuleName, 18, "Date From :")
            Language.setMessage(mstrModuleName, 19, "To :")
            Language.setMessage(mstrModuleName, 20, "Institute :")
            Language.setMessage(mstrModuleName, 21, "Employee :")
            Language.setMessage(mstrModuleName, 22, "Result :")
            Language.setMessage(mstrModuleName, 23, "Received By :")
            Language.setMessage(mstrModuleName, 24, "Printed By :")
            Language.setMessage(mstrModuleName, 25, "Printed Date :")
            Language.setMessage(mstrModuleName, 26, "Page :")
            Language.setMessage(mstrModuleName, 27, "Prepared By :")
            Language.setMessage(mstrModuleName, 28, "Checked By :")
            Language.setMessage(mstrModuleName, 29, "Approved By :")
            Language.setMessage(mstrModuleName, 100, "Allocation Binded")
            Language.setMessage(mstrModuleName, 102, "Allocation Binded :")
            Language.setMessage(mstrModuleName, 200, "Training Name :")
            Language.setMessage(mstrModuleName, 201, "Scheduled Dates :")
            Language.setMessage(mstrModuleName, 202, "Provider :")
            Language.setMessage(mstrModuleName, 203, "Venue :")
            Language.setMessage(mstrModuleName, 204, "Time Duration :")
            Language.setMessage(mstrModuleName, 205, "Total Hours :")
            Language.setMessage(mstrModuleName, 206, "Total Days :")
            Language.setMessage(mstrModuleName, 207, "Sponsers :")
            Language.setMessage(mstrModuleName, 208, "Allocation Binded :")
            Language.setMessage(mstrModuleName, 209, "Trainers :")
            Language.setMessage(mstrModuleName, 210, "S/N")
            Language.setMessage(mstrModuleName, 211, "Employee")
            Language.setMessage(mstrModuleName, 212, "Age")
            Language.setMessage(mstrModuleName, 213, "Job Title")
            Language.setMessage(mstrModuleName, 214, "Appointment Date")
            Language.setMessage(mstrModuleName, 215, "Current Qualification")
            Language.setMessage(mstrModuleName, 216, "Results")
            Language.setMessage(mstrModuleName, 217, "GPA")
            Language.setMessage(mstrModuleName, 218, "Grade")
            Language.setMessage(mstrModuleName, 219, "Branch")
            Language.setMessage(mstrModuleName, 220, "Department")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
