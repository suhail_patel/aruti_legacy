'************************************************************************************************************************************
'Class Name : clsEmployeeStaffListReport.vb
'Purpose    :
'Date       :27/03/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeStaffListReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeStaffListReport"
    Private mstrReportId As String = CStr(enArutiReport.EmployeeStaffList)  '70
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintConditionId As Integer = 0
    Private mstrCondition As String = String.Empty
    Private mintQLevel As Integer = 0
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End


    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ConditionId() As Integer
        Set(ByVal value As Integer)
            mintConditionId = value
        End Set
    End Property

    Public WriteOnly Property _Condition() As String
        Set(ByVal value As String)
            mstrCondition = value
        End Set
    End Property

    Public WriteOnly Property _QLevel() As Integer
        Set(ByVal value As Integer)
            mintQLevel = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Anjan (17 May 2012)-End 

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End



    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = String.Empty
            mintConditionId = 0
            mstrCondition = String.Empty
            mintQLevel = 0
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            If mintEmployeeUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintQLevel > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Qualification Level :") & "" & mintQLevel & " "
            End If

            If mintConditionId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Condition :") & "" & mstrCondition & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(employeecode,'')", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(surname,'')+' '+ ISNULL(firstname,'')+' '+ISNULL(othername,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END
            iColumn_DetailReport.Add(New IColumn("CASE WHEN gender = 1 THEN @MALE WHEN gender = 2 THEN @FEMALE END", Language.getMessage(mstrModuleName, 7, "Sex")))
            iColumn_DetailReport.Add(New IColumn("birthdate", Language.getMessage(mstrModuleName, 8, "Birthdate")))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iColumn_DetailReport.Add(New IColumn("termination_to_date", Language.getMessage(mstrModuleName, 9, "Retirement Date")))
            iColumn_DetailReport.Add(New IColumn("RT.termination_to_date", Language.getMessage(mstrModuleName, 9, "Retirement Date")))
            'S.SANDEEP [04 JUN 2015] -- END
            iColumn_DetailReport.Add(New IColumn("appointeddate", Language.getMessage(mstrModuleName, 10, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(DATEDIFF(YEAR,birthdate,GETDATE()),0)", Language.getMessage(mstrModuleName, 11, "Age")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            ''S.SANDEEP [ 16 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            ''StrQ = "SELECT "
            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= "  hrstation_master.name AS GName, "

            ''    Case enAnalysisReport.Department
            ''        StrQ &= "  hrdepartment_master.name AS GName, "

            ''    Case enAnalysisReport.Section
            ''        StrQ &= "  hrsection_master.name AS GName, "

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= "  hrunit_master.name AS GName, "

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " hrjob_master.job_name AS GName, "

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " prcostcenter_master.costcentername AS GName, "

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " hrsectiongroup_master.name AS GName,"

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " hrunitgroup_master.name AS GName, "

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " hrteam_master.name AS GName, "

            ''    Case Else
            ''        StrQ &= " '' AS GName, "
            ''End Select

            ''StrQ &= "  ISNULL(employeecode,'') AS ECODE " & _
            ''        ",ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
            ''        ",CASE WHEN gender = 1 THEN @MALE " & _
            ''        "      WHEN gender = 2 THEN @FEMALE " & _
            ''        " END AS SEX " & _
            ''        ",ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS DOB " & _
            ''        ",ISNULL(CAST(DATEDIFF(YEAR,birthdate,GETDATE()) AS NVARCHAR(5)),'') AS AGE " & _
            ''        ",ISNULL(CONVERT(CHAR(8),termination_to_date,112),'') AS RETIREMENT_DATE " & _
            ''        ",GM.name AS SALARY_GRADE " & _
            ''        ",DM.name AS DEPARTMENT " & _
            ''        ",ISNULL(ST.name,'') AS BRANCH " & _
            ''        ",ISNULL(present_provicnce,'') AS REGION " & _
            ''        ",ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS START_DATE " & _
            ''        ",ISNULL(JM.job_name,'') AS JOB_TITLE " & _
            ''        ",qualificationname AS EDUCATION " & _
            ''        ",qlevel AS QLEVEL " & _
            ''        ",birthdate " & _
            ''        ",termination_to_date " & _
            ''        ",appointeddate " & _
            ''        ",ISNULL(DATEDIFF(YEAR,birthdate,GETDATE()),0) SAGE " & _
            ''       "FROM hremployee_master " & _
            ''        "  JOIN hremp_qualification_tran ON dbo.hremployee_master.employeeunkid = dbo.hremp_qualification_tran.employeeunkid " & _
            ''        "  JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            ''        "  JOIN cfcommon_master ON hrqualification_master.qualificationgroupunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & "' " & _
            ''        "  JOIN hrjob_master AS JM ON hremployee_master.jobunkid = JM.jobunkid " & _
            ''        "  LEFT JOIN hrstation_master AS ST ON hremployee_master.stationunkid = ST.stationunkid " & _
            ''        "  JOIN hrdepartment_master AS DM ON hremployee_master.departmentunkid = DM.departmentunkid " & _
            ''        "  JOIN hrgrade_master AS GM ON hremployee_master.gradeunkid = GM.gradeunkid "

            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  "

            ''    Case enAnalysisReport.Department
            ''        StrQ &= " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "

            ''    Case enAnalysisReport.Section
            ''        StrQ &= " JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid "

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid "

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid "

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid "

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid "

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid "

            ''End Select

            ''StrQ &= " WHERE 1 = 1 "

            ' ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ' ''ISSUE : TRA ENHANCEMENTS
            ''If mblnIncludeInactiveEmp = False Then
            ''    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            ''End If

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'StrQ &= "SELECT " & _
            '        " ISNULL(employeecode,'') AS ECODE "


            ''SANDEEP [ 26 MAR 2014 ] -- START
            ''ENHANCEMENT : Requested by Rutta
            ''iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            'If mblnFirstNamethenSurname = False Then
            '    StrQ &= ",ISNULL(surname,'')+' '+ ISNULL(firstname,'')+' '+ISNULL(othername,'') AS ENAME "
            'Else
            '    StrQ &= ",ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME "
            'End If
            ''SANDEEP [ 26 MAR 2014 ] -- END

            'StrQ &= ",CASE WHEN gender = 1 THEN @MALE " & _
            '         "      WHEN gender = 2 THEN @FEMALE " & _
            '         " END AS SEX " & _
            '         ",ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS DOB " & _
            '         ",ISNULL(CAST(DATEDIFF(YEAR,birthdate,GETDATE()) AS NVARCHAR(5)),'') AS AGE " & _
            '         ",ISNULL(CONVERT(CHAR(8),termination_to_date,112),'') AS RETIREMENT_DATE " & _
            '         ",GM.name AS SALARY_GRADE " & _
            '         ",DM.name AS DEPARTMENT " & _
            '         ",ISNULL(ST.name,'') AS BRANCH " & _
            '         ",ISNULL(present_provicnce,'') AS REGION " & _
            '         ",ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS START_DATE " & _
            '         ",ISNULL(JM.job_name,'') AS JOB_TITLE " & _
            '         ",qualificationname AS EDUCATION " & _
            '         ",qlevel AS QLEVEL " & _
            '         ",birthdate " & _
            '         ",termination_to_date " & _
            '         ",appointeddate " & _
            '         ",ISNULL(DATEDIFF(YEAR,birthdate,GETDATE()),0) SAGE "

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= "FROM hremployee_master " & _
            '         "  JOIN hremp_qualification_tran ON hremployee_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
            '         "  JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '         "  JOIN cfcommon_master ON hrqualification_master.qualificationgroupunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & "' " & _
            '         "  JOIN hrjob_master AS JM ON hremployee_master.jobunkid = JM.jobunkid " & _
            '         "  LEFT JOIN hrstation_master AS ST ON hremployee_master.stationunkid = ST.stationunkid " & _
            '         "  JOIN hrdepartment_master AS DM ON hremployee_master.departmentunkid = DM.departmentunkid " & _
            '         "  JOIN hrgrade_master AS GM ON hremployee_master.gradeunkid = GM.gradeunkid "

            'StrQ &= mstrAnalysis_Join

            'StrQ &= " WHERE 1 = 1 "

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If



            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (12-Nov-2012) -- End


            ''S.SANDEEP [ 16 MAY 2012 ] -- END

            'If mintEmployeeUnkid > 0 Then
            '    StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "' "
            'End If

            'If mintConditionId > 0 AndAlso mintQLevel > 0 Then
            '    StrQ &= " AND cfcommon_master.qlevel " & mstrCondition & " '" & mintQLevel & "' "
            'End If


            ''S.SANDEEP [ 16 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            ''Select Case mintViewIndex

            ''    Case enAnalysisReport.Branch
            ''        StrQ &= " AND hrstation_master.stationunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Department
            ''        StrQ &= " AND hrdepartment_master.departmentunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Section
            ''        StrQ &= "AND hrsection_master.sectionunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Unit
            ''        StrQ &= " AND hrunit_master.unitunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Job
            ''        StrQ &= " AND hrjob_master.jobunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.CostCenter
            ''        StrQ &= " AND prcostcenter_master.costcenterunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.SectionGroup
            ''        StrQ &= " AND hrsectiongroup_master.sectiongroupunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.UnitGroup
            ''        StrQ &= " AND hrunitgroup_master.unitgroupunkid in (" & mstrViewByIds & ")"

            ''    Case enAnalysisReport.Team
            ''        StrQ &= " AND hrteam_master.teamunkid in (" & mstrViewByIds & ")"

            ''End Select
            ''S.SANDEEP [ 16 MAY 2012 ] -- END


            StrQ &= "SELECT " & _
                    " ISNULL(employeecode,'') AS ECODE "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(surname,'')+' '+ ISNULL(firstname,'')+' '+ISNULL(othername,'') AS ENAME "
            Else
                StrQ &= ",ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME "
            End If

            StrQ &= ",CASE WHEN gender = 1 THEN @MALE " & _
                    "      WHEN gender = 2 THEN @FEMALE " & _
                    " END AS SEX " & _
                    ",ISNULL(CONVERT(CHAR(8),birthdate,112),'') AS DOB " & _
                    ",ISNULL(CAST(DATEDIFF(YEAR,birthdate,GETDATE()) AS NVARCHAR(5)),'') AS AGE " & _
                    ",ISNULL(CONVERT(CHAR(8),RT.termination_to_date,112),'') AS RETIREMENT_DATE " & _
                    ",GM.name AS SALARY_GRADE " & _
                    ",DM.name AS DEPARTMENT " & _
                    ",ISNULL(ST.name,'') AS BRANCH " & _
                    ",ISNULL(present_provicnce,'') AS REGION " & _
                    ",ISNULL(CONVERT(CHAR(8),appointeddate,112),'') AS START_DATE " & _
                    ",ISNULL(JM.job_name,'') AS JOB_TITLE " & _
                    ",CAST(ROW_NUMBER()OVER(Partition By hremployee_master.employeeunkid order by hremployee_master.employeeunkid) AS NVARCHAR(20)) + '. ' +qualificationname AS EDUCATION " & _
                    ",qlevel AS QLEVEL " & _
                    ",birthdate " & _
                    ",RT.termination_to_date " & _
                    ",appointeddate " & _
                    ",ISNULL(DATEDIFF(YEAR,birthdate,GETDATE()),0) SAGE "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            '' REMOVED : qualificationname AS EDUCATION
            '' ADDED : CAST(ROW_NUMBER()OVER(Partition By hremployee_master.employeeunkid order by hremployee_master.employeeunkid) AS NVARCHAR(20)) + '. ' +qualificationname AS EDUCATION
            'S.SANDEEP |28-OCT-2021| -- END
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "  JOIN hremp_qualification_tran ON hremployee_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
                     "  JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                     "  JOIN cfcommon_master ON hrqualification_master.qualificationgroupunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & "' " & _
                     "LEFT JOIN " & _
                     "( " & _
                     "  SELECT " & _
                     "       stationunkid " & _
                     "      ,departmentunkid " & _
                     "      ,employeeunkid " & _
                     "      ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate) AS rno " & _
                     "  FROM hremployee_transfer_tran " & _
                     "  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                     ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                     "LEFT JOIN hrstation_master AS ST ON Alloc.stationunkid = ST.stationunkid " & _
                     "JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                     "JOIN " & _
                     "( " & _
                     "      SELECT " & _
                     "           jobunkid " & _
                     "          ,employeeunkid " & _
                     "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate) AS rno " & _
                     "      FROM hremployee_categorization_tran " & _
                     "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                     ") AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.rno = 1 " & _
                     "JOIN hrjob_master AS JM ON Job.jobunkid = JM.jobunkid " & _
                     "JOIN " & _
                     "( " & _
                     "      SELECT " & _
                     "           gradeunkid " & _
                     "          ,employeeunkid " & _
                     "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                     "      FROM prsalaryincrement_tran " & _
                     "      WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                     ") AS Grd ON Grd.employeeunkid = hremployee_master.employeeunkid AND Grd.rno = 1 " & _
                     "JOIN hrgrade_master AS GM ON Grd.gradeunkid = GM.gradeunkid " & _
                     "JOIN " & _
                    "( " & _
                    "       SELECT " & _
                    "            date1 AS termination_to_date " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate) AS rno " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "'" & _
                    ") AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 "

            'S.SANDEEP [05-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-25}
            '------- "       AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "'" & _ ------ REMOVED
            '------- "       AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "'" & _ ------ ADDED
            'S.SANDEEP [05-Mar-2018] -- END


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END


            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1 = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mintEmployeeUnkid > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "' "
            End If

            If mintConditionId > 0 AndAlso mintQLevel > 0 Then
                StrQ &= " AND cfcommon_master.qlevel " & mstrCondition & " '" & mintQLevel & "' "
            End If


            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'Dim dicEMP As New Dictionary(Of String, String)
            'Dim dtFilter As DataTable

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

            '    If dicEMP.ContainsKey(dtRow.Item("ECODE")) Then Continue For
            '    dicEMP.Add(dtRow.Item("ECODE"), dtRow.Item("ECODE"))

            '    Dim rpt_Rows As DataRow
            '    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

            '    rpt_Rows.Item("Column1") = dtRow.Item("ECODE")
            '    rpt_Rows.Item("Column2") = dtRow.Item("ENAME")
            '    rpt_Rows.Item("Column3") = dtRow.Item("SEX")
            '    If dtRow.Item("DOB").ToString.Trim.Length > 0 Then
            '        rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("DOB").ToString).ToShortDateString
            '    Else
            '        rpt_Rows.Item("Column4") = ""
            '    End If
            '    rpt_Rows.Item("Column5") = dtRow.Item("AGE")
            '    If dtRow.Item("RETIREMENT_DATE").ToString.Trim.Length > 0 Then
            '        rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("RETIREMENT_DATE").ToString).ToShortDateString
            '    Else
            '        rpt_Rows.Item("Column6") = ""
            '    End If
            '    rpt_Rows.Item("Column7") = dtRow.Item("SALARY_GRADE")
            '    rpt_Rows.Item("Column8") = dtRow.Item("DEPARTMENT")
            '    rpt_Rows.Item("Column9") = dtRow.Item("BRANCH")
            '    rpt_Rows.Item("Column10") = dtRow.Item("REGION")
            '    If dtRow.Item("START_DATE").ToString.Trim.Length > 0 Then
            '        rpt_Rows.Item("Column11") = eZeeDate.convertDate(dtRow.Item("START_DATE").ToString).ToShortDateString
            '    Else
            '        rpt_Rows.Item("Column11") = ""
            '    End If
            '    rpt_Rows.Item("Column12") = dtRow.Item("JOB_TITLE")
            '    rpt_Rows.Item("Column14") = dtRow.Item("GName")

            '    dtFilter = New DataView(dsList.Tables(0), "ECODE = '" & dtRow.Item("ECODE") & "'", "", DataViewRowState.CurrentRows).ToTable
            '    Dim iCnt As Integer = 1
            '    For Each dFRow As DataRow In dtFilter.Rows
            '        rpt_Rows.Item("Column13") &= iCnt.ToString & ". " & dFRow.Item("EDUCATION") & vbCrLf
            '        iCnt += 1
            '    Next

            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            'Next

            Dim dt As DataTable = dsList.Tables(0).Copy()
            dt.Columns.Remove("EDUCATION") : dt.Columns.Remove("QLEVEL")
            Dim items = dt.AsEnumerable().Distinct(System.Data.DataRowComparer.[Default]).ToList()
            Dim dtDistinct As DataTable = items.CopyToDataTable()
            Dim xCol As New DataColumn
            With xCol
                .ColumnName = "EDUCATION"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            dtDistinct.Columns.Add(xCol)
            dtDistinct.AsEnumerable().ToList().ForEach(Function(x) JoinQyalification(x, dsList.Tables(0)))


            For Each dtRow As DataRow In dtDistinct.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("ECODE")
                rpt_Rows.Item("Column2") = dtRow.Item("ENAME")
                rpt_Rows.Item("Column3") = dtRow.Item("SEX")
                If dtRow.Item("DOB").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("DOB").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column4") = ""
                End If
                rpt_Rows.Item("Column5") = dtRow.Item("AGE")
                If dtRow.Item("RETIREMENT_DATE").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column6") = eZeeDate.convertDate(dtRow.Item("RETIREMENT_DATE").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column6") = ""
                End If
                rpt_Rows.Item("Column7") = dtRow.Item("SALARY_GRADE")
                rpt_Rows.Item("Column8") = dtRow.Item("DEPARTMENT")
                rpt_Rows.Item("Column9") = dtRow.Item("BRANCH")
                rpt_Rows.Item("Column10") = dtRow.Item("REGION")
                If dtRow.Item("START_DATE").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column11") = eZeeDate.convertDate(dtRow.Item("START_DATE").ToString).ToShortDateString
                Else
                    rpt_Rows.Item("Column11") = ""
                End If
                rpt_Rows.Item("Column12") = dtRow.Item("JOB_TITLE")
                rpt_Rows.Item("Column14") = dtRow.Item("GName")

                rpt_Rows.Item("Column13") = dtRow.Item("EDUCATION")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            'S.SANDEEP |28-OCT-2021| -- END



            objRpt = New ArutiReport.Designer.rptStaffList_Report


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 5, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 6, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtSex", Language.getMessage(mstrModuleName, 7, "Sex"))
            Call ReportFunction.TextChange(objRpt, "txtDOB", Language.getMessage(mstrModuleName, 8, "Birthdate"))
            Call ReportFunction.TextChange(objRpt, "txtAge", Language.getMessage(mstrModuleName, 11, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtRetireDate", Language.getMessage(mstrModuleName, 9, "Retirement Date"))
            Call ReportFunction.TextChange(objRpt, "txtSalGrade", Language.getMessage(mstrModuleName, 17, "Salary Grade"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 18, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtStation", Language.getMessage(mstrModuleName, 19, "Station"))
            Call ReportFunction.TextChange(objRpt, "txtRegion", Language.getMessage(mstrModuleName, 20, "Region"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 10, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 21, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtEducation", Language.getMessage(mstrModuleName, 22, "Educational Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 23, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 24, "Grand Total :"))


            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'Select Case mintViewIndex

            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 213, "Branch :"))

            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 214, "Department :"))

            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 215, "Section :"))

            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 216, "Unit :"))

            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 217, "Job :"))

            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 218, "Cost Center :"))

            '    Case enAnalysisReport.SectionGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 219, "Section Group :"))

            '    Case enAnalysisReport.UnitGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 220, "Unit Group :"))

            '    Case enAnalysisReport.Team
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 221, "Team :"))

            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")

            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 25, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 26, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


    Private Function JoinQyalification(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Dim items = dtTable.Select("ECODE = '" & dr("ECODE") & "'")
        dr("EDUCATION") = String.Join(vbCrLf, items.AsEnumerable().Select(Function(x) x.Field(Of String)("EDUCATION")).ToArray())
        Return True
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Qualification Level :")
            Language.setMessage(mstrModuleName, 3, "Condition :")
            Language.setMessage(mstrModuleName, 4, "Order By :")
            Language.setMessage(mstrModuleName, 5, "Employee Code")
            Language.setMessage(mstrModuleName, 6, "Employee Name")
            Language.setMessage(mstrModuleName, 7, "Sex")
            Language.setMessage(mstrModuleName, 8, "Birthdate")
            Language.setMessage(mstrModuleName, 9, "Retirement Date")
            Language.setMessage(mstrModuleName, 10, "Start Date")
            Language.setMessage(mstrModuleName, 11, "Age")
            Language.setMessage(mstrModuleName, 12, "Prepared By :")
            Language.setMessage(mstrModuleName, 13, "Checked By :")
            Language.setMessage(mstrModuleName, 14, "Approved By :")
            Language.setMessage(mstrModuleName, 15, "Received By :")
            Language.setMessage(mstrModuleName, 17, "Salary Grade")
            Language.setMessage(mstrModuleName, 18, "Department")
            Language.setMessage(mstrModuleName, 19, "Station")
            Language.setMessage(mstrModuleName, 20, "Region")
            Language.setMessage(mstrModuleName, 21, "Job Title")
            Language.setMessage(mstrModuleName, 22, "Educational Qualification")
            Language.setMessage(mstrModuleName, 23, "Sub Total :")
            Language.setMessage(mstrModuleName, 24, "Grand Total :")
            Language.setMessage(mstrModuleName, 25, "Printed By :")
            Language.setMessage(mstrModuleName, 26, "Printed Date :")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
