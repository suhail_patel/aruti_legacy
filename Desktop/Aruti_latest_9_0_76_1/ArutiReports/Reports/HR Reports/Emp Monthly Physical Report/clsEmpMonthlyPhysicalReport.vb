'************************************************************************************************************************************
'Class Name : clsEmpMonthlyPhysicalReport.vb
'Purpose    :
'Date       :01/13/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmpMonthlyPhysicalReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpMonthlyPhysicalReport"
    Private mstrReportId As String = enArutiReport.EmployeeMonthlyPhysicalReport  '29
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintCurrentMonthId As Integer = -1
    Private mstrCurrentMonthName As String = String.Empty
    Private mintPreviousMonthId As Integer = -1
    Private mstrPreviousMonthName As String = String.Empty
    Private mstrCYear As String = String.Empty
    Private mstrPYear As String = String.Empty

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    Private iCDate As String = String.Empty
    Private iPDate As String = String.Empty

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _CMonthId() As Integer
        Set(ByVal value As Integer)
            mintCurrentMonthId = value
        End Set
    End Property

    Public WriteOnly Property _CMonthName() As String
        Set(ByVal value As String)
            mstrCurrentMonthName = value
        End Set
    End Property

    Public WriteOnly Property _PMonthId() As Integer
        Set(ByVal value As Integer)
            mintPreviousMonthId = value
        End Set
    End Property

    Public WriteOnly Property _PMonthName() As String
        Set(ByVal value As String)
            mstrPreviousMonthName = value
        End Set
    End Property

    Public WriteOnly Property _CYearName() As String
        Set(ByVal value As String)
            mstrCYear = value
        End Set
    End Property

    Public WriteOnly Property _PYearName() As String
        Set(ByVal value As String)
            mstrPYear = value
        End Set
    End Property

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    Public WriteOnly Property _iCDate() As String
        Set(ByVal value As String)
            iCDate = value
        End Set
    End Property

    Public WriteOnly Property _iPDate() As String
        Set(ByVal value As String)
            iPDate = value
        End Set
    End Property

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Public Functions & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintCurrentMonthId = 0
            mstrCurrentMonthName = String.Empty
            mintPreviousMonthId = 0
            mstrPreviousMonthName = String.Empty
            mstrCYear = String.Empty
            mstrPYear = String.Empty

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            iCDate = String.Empty
            iPDate = String.Empty

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function GetYearList(ByVal intCompanyId As Integer, ByVal intYearId As Integer, Optional ByVal strList As String = "List") As DataSet
        'Public Function GetYearList(Optional ByVal strList As String = "List", Optional ByVal intCompanyId As Integer = -1, Optional ByVal intYearId As Integer = -1) As DataSet
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "  YEAR(start_date) AS Years " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = @CompanyId AND yearunkid = @YearUnkid " & _
                   "UNION " & _
                   "SELECT " & _
                   "  YEAR(end_date) AS Years " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = @CompanyId AND yearunkid = @YearUnkid "

            If intCompanyId <= 0 Then
                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, Company._Object._Companyunkid)
            Else
                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            End If

            If intYearId <= 0 Then
                objDataOperation.AddParameter("@YearUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
            Else
                objDataOperation.AddParameter("@YearUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetYearList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetMonthList(Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dtMTable As New DataTable(strList)
        Dim dsList As New DataSet
        Try

            dtMTable.Columns.Add("id", System.Type.GetType("System.Int32"))
            dtMTable.Columns.Add("name", System.Type.GetType("System.String"))
            Dim dMRow As DataRow = Nothing

            For i As Integer = 0 To 12
                dMRow = dtMTable.NewRow

                If i = 0 Then
                    dMRow.Item("id") = 0
                    dMRow.Item("name") = Language.getMessage(mstrModuleName, 4, "Select")
                Else
                    dMRow.Item("id") = i
                    dMRow.Item("name") = MonthName(i)
                End If

                dtMTable.Rows.Add(dMRow)
            Next

            dsList.Tables.Add(dtMTable)

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMonthList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim exForce As Exception
    '    Dim dsCurrent As New DataSet
    '    Dim dsPrevious As New DataSet

    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ &= "SELECT " & _
    '                                 " hrdepartment_master.departmentunkid " & _
    '                                 ",ISNULL(name,'') AS Dept " & _
    '                                 ",ISNULL(CAST(ActiveEmp AS NVARCHAR(10)),'-') AS ActiveEmp " & _
    '                                 ",ISNULL(CAST(NewStaff AS NVARCHAR(10)),'-') AS NewStaff " & _
    '                                 ",ISNULL(CAST(Retermi AS NVARCHAR(10)),'-') AS Retermi " & _
    '                                 ",@Caption1 AS ActCaption " & _
    '                                 ",@Caption2 AS Ncaption " & _
    '                                 ",@Caption3 AS retCaption " & _
    '                                 ",1  AS AId " & _
    '                                 ",2 AS NId " & _
    '                                 ",3 AS RId " & _
    '                            "FROM hrdepartment_master " & _
    '                            "LEFT JOIN " & _
    '                            "( " & _
    '                                 "SELECT " & _
    '                                       "departmentunkid " & _
    '                                      ",COUNT(employeeunkid) AS ActiveEmp " & _
    '                                 "FROM hremployee_master " & _
    '                                 "WHERE 1 = 1 "

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'S.SANDEEP [ 12 MAY 2012 ] -- START
    '            'ISSUE : TRA ENHANCEMENTS
    '            'StrQ &= "  AND  hremployee_master.isactive = 1"
    '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
    '            'S.SANDEEP [ 12 MAY 2012 ] -- END
    '        End If

    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        If mstrUserAccessFilter = "" Then
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '        Else
    '            StrQ &= mstrUserAccessFilter
    '        End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- END
    '        'S.SANDEEP [ 12 MAY 2012 ] -- END

    '        'Pinkal (24-Jun-2011) -- End

    '        StrQ &= "AND DATEPART(mm,appointeddate) <> @MonthId " & _
    '                                      "AND DATEPART(yy,appointeddate) <> @YearId " & _
    '                                 "GROUP BY departmentunkid " & _
    '                            ") AS ActE ON ActE.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                            "LEFT JOIN " & _
    '                            "( " & _
    '                                 "SELECT " & _
    '                                       "departmentunkid " & _
    '                                      ",COUNT(employeeunkid) AS NewStaff " & _
    '                                 "FROM hremployee_master " & _
    '                                 "WHERE 1 = 1 "

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'S.SANDEEP [ 12 MAY 2012 ] -- START
    '            'ISSUE : TRA ENHANCEMENTS
    '            'StrQ &= "  AND  hremployee_master.isactive = 1"
    '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
    '            'S.SANDEEP [ 12 MAY 2012 ] -- END
    '        End If

    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        If mstrUserAccessFilter = "" Then
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '        Else
    '            StrQ &= mstrUserAccessFilter
    '        End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- END
    '        'S.SANDEEP [ 12 MAY 2012 ] -- END

    '        'Pinkal (24-Jun-2011) -- End

    '        StrQ &= "AND DATEPART(mm,appointeddate) = @MonthId " & _
    '                                      "AND DATEPART(yy,appointeddate) = @YearId " & _
    '                                 "GROUP BY departmentunkid " & _
    '                            ") AS NStaff ON NStaff.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                            "LEFT JOIN " & _
    '                            "( " & _
    '                                 "SELECT " & _
    '                                       "departmentunkid " & _
    '                                      ",COUNT(employeeunkid) AS Retermi " & _
    '                                 "FROM hremployee_master " & _
    '                                 "WHERE 1 = 1 "

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'S.SANDEEP [ 12 MAY 2012 ] -- START
    '            'ISSUE : TRA ENHANCEMENTS
    '            'StrQ &= "  AND  hremployee_master.isactive = 1"
    '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
    '            'S.SANDEEP [ 12 MAY 2012 ] -- END
    '        End If

    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        If mstrUserAccessFilter = "" Then
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '        Else
    '            StrQ &= mstrUserAccessFilter
    '        End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- END
    '        'S.SANDEEP [ 12 MAY 2012 ] -- END

    '        'Pinkal (24-Jun-2011) -- End

    '        StrQ &= "AND DATEPART(mm,termination_to_date) = @MonthId " & _
    '                                      "AND DATEPART(yy,termination_to_date) = @YearId " & _
    '                                 "GROUP BY departmentunkid " & _
    '                            "UNION " & _
    '                                 "SELECT " & _
    '                                       "departmentunkid " & _
    '                                      ",COUNT(employeeunkid) AS Retermi " & _
    '                                 "FROM hremployee_master " & _
    '                                 "WHERE 1 = 1 "

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'S.SANDEEP [ 12 MAY 2012 ] -- START
    '            'ISSUE : TRA ENHANCEMENTS
    '            'StrQ &= "  AND  hremployee_master.isactive = 1"
    '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
    '            'S.SANDEEP [ 12 MAY 2012 ] -- END
    '        End If

    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        'S.SANDEEP [ 12 NOV 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        If mstrUserAccessFilter = "" Then
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If
    '        Else
    '            StrQ &= mstrUserAccessFilter
    '        End If
    '        'S.SANDEEP [ 12 NOV 2012 ] -- END
    '        'S.SANDEEP [ 12 MAY 2012 ] -- END


    '        'Pinkal (24-Jun-2011) -- End

    '        StrQ &= "AND DATEPART(mm,termination_from_date) = @MonthId " & _
    '                                           "AND DATEPART(yy,termination_from_date) = @YearId " & _
    '                                      "GROUP BY departmentunkid " & _
    '                            ") AS Reti ON Reti.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                            "WHERE hrdepartment_master.isactive = 1 "

    '        For i As Integer = 1 To 2
    '            objDataOperation.ClearParameters()
    '            'S.SANDEEP [ 12 MAY 2012 ] -- START
    '            If mblnIsActive = False Then
    '                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            End If
    '            'S.SANDEEP [ 12 MAY 2012 ] -- END
    '            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "No. of Active Employee"))
    '            objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Newly Hired Employee"))
    '            objDataOperation.AddParameter("@Caption3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Retired/Terminated"))
    '            Select Case i
    '                Case 1 'Current Month
    '                    objDataOperation.AddParameter("@MonthId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrentMonthId)
    '                    objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrCYear)

    '                    dsCurrent = objDataOperation.ExecQuery(StrQ, "CDataTable")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & "  :  " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                Case 2 'Previous Month
    '                    objDataOperation.AddParameter("@MonthId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPreviousMonthId)
    '                    objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrPYear)

    '                    dsPrevious = objDataOperation.ExecQuery(StrQ, "PDataTable")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & "  :  " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '            End Select
    '        Next

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim dtTable As New DataTable
    '        For j As Integer = 1 To 3
    '            Select Case j
    '                Case 1 'Active Employee
    '                    dtTable = New DataView(dsCurrent.Tables("CDataTable"), "Aid = '" & j.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
    '                    For Each dtRow As DataRow In dtTable.Rows
    '                        Dim dtTemp() As DataRow = dsPrevious.Tables("PDataTable").Select("departmentunkid = '" & dtRow.Item("departmentunkid") & "'")
    '                        If dtTemp.Length > 0 Then
    '                            Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '                            rpt_Row.Item("Column1") = dtRow.Item("Aid")
    '                            rpt_Row.Item("Column2") = dtRow.Item("Dept")
    '                            rpt_Row.Item("Column3") = dtTemp(0)("ActiveEmp")
    '                            rpt_Row.Item("Column4") = dtRow.Item("ActiveEmp")

    '                            If dtRow.Item("ActiveEmp").ToString.Trim = "-" And dtTemp(0)("ActiveEmp").ToString.Trim = "-" Then
    '                                rpt_Row.Item("Column5") = "-"
    '                            ElseIf dtTemp(0)("ActiveEmp").ToString = "-" Then
    '                                rpt_Row.Item("Column5") = dtRow.Item("ActiveEmp")
    '                            ElseIf dtRow.Item("ActiveEmp").ToString.Trim = "-" Then
    '                                rpt_Row.Item("Column5") = dtTemp(0)("ActiveEmp")
    '                            Else
    '                                rpt_Row.Item("Column5") = dtTemp(0)("ActiveEmp") - dtRow.Item("ActiveEmp")
    '                            End If
    '                            rpt_Row.Item("Column6") = dtRow.Item("ActCaption")

    '                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '                        End If
    '                    Next
    '                Case 2 'New Staff
    '                    dtTable = New DataView(dsCurrent.Tables("CDataTable"), "Nid = '" & j.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
    '                    For Each dtRow As DataRow In dtTable.Rows
    '                        Dim dtTemp() As DataRow = dsPrevious.Tables("PDataTable").Select("departmentunkid = '" & dtRow.Item("departmentunkid") & "'")
    '                        If dtTemp.Length > 0 Then
    '                            Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '                            rpt_Row.Item("Column1") = dtRow.Item("NId")
    '                            rpt_Row.Item("Column2") = dtRow.Item("Dept")
    '                            rpt_Row.Item("Column3") = dtTemp(0)("NewStaff")
    '                            rpt_Row.Item("Column4") = dtRow.Item("NewStaff")

    '                            If dtRow.Item("NewStaff").ToString.Trim = "-" And dtTemp(0)("NewStaff").ToString.Trim = "-" Then
    '                                rpt_Row.Item("Column5") = "-"
    '                            ElseIf dtTemp(0)("NewStaff").ToString = "-" Then
    '                                rpt_Row.Item("Column5") = dtRow.Item("ActiveEmp")
    '                            ElseIf dtRow.Item("NewStaff").ToString.Trim = "-" Then
    '                                rpt_Row.Item("Column5") = dtTemp(0)("NewStaff")
    '                            Else
    '                                rpt_Row.Item("Column5") = dtTemp(0)("NewStaff") - dtRow.Item("NewStaff")
    '                            End If
    '                            rpt_Row.Item("Column6") = dtRow.Item("Ncaption")

    '                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '                        End If
    '                    Next
    '                Case 3 'Ret/Term.
    '                    dtTable = New DataView(dsCurrent.Tables("CDataTable"), "Rid = '" & j.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
    '                    For Each dtRow As DataRow In dtTable.Rows
    '                        Dim dtTemp() As DataRow = dsPrevious.Tables("PDataTable").Select("departmentunkid = '" & dtRow.Item("departmentunkid") & "'")
    '                        If dtTemp.Length > 0 Then
    '                            Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '                            rpt_Row.Item("Column1") = dtRow.Item("RId")
    '                            rpt_Row.Item("Column2") = dtRow.Item("Dept")
    '                            rpt_Row.Item("Column3") = dtTemp(0)("Retermi")
    '                            rpt_Row.Item("Column4") = dtRow.Item("Retermi")

    '                            If dtRow.Item("Retermi").ToString.Trim = "-" And dtTemp(0)("Retermi").ToString.Trim = "-" Then
    '                                rpt_Row.Item("Column5") = "-"
    '                            ElseIf dtTemp(0)("Retermi").ToString = "-" Then
    '                                rpt_Row.Item("Column5") = dtRow.Item("Retermi")
    '                            ElseIf dtRow.Item("Retermi").ToString.Trim = "-" Then
    '                                rpt_Row.Item("Column5") = dtTemp(0)("Retermi")
    '                            Else
    '                                rpt_Row.Item("Column5") = dtTemp(0)("Retermi") - dtRow.Item("Retermi")
    '                            End If
    '                            rpt_Row.Item("Column6") = dtRow.Item("retCaption")

    '                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '                        End If
    '                    Next
    '            End Select
    '        Next

    '        objRpt = New ArutiReport.Designer.rptEmpMonthlyPhysicalReport


    '        'Sandeep [ 10 FEB 2011 ] -- Start
    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 13, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 14, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 15, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 16, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If
    '        'Sandeep [ 10 FEB 2011 ] -- End 


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 17, "Department"))
    '        Call ReportFunction.TextChange(objRpt, "txtPreviousMonth", Language.getMessage(mstrModuleName, 5, "Previous Month :") & " " & mstrPreviousMonthName & " - " & mstrPYear)
    '        Call ReportFunction.TextChange(objRpt, "txtCurrentMonth", Language.getMessage(mstrModuleName, 6, "Current Month : ") & " " & mstrCurrentMonthName & " - " & mstrCYear)
    '        Call ReportFunction.TextChange(objRpt, "txtVariance", Language.getMessage(mstrModuleName, 7, "Variance"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 8, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTot", Language.getMessage(mstrModuleName, 9, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Dim dsCurrent As New DataSet
        Dim dsPrevious As New DataSet

        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'StrQ &= "SELECT " & _
            '        "    hrdepartment_master.departmentunkid " & _
            '        "   ,ISNULL(name,'') AS Dept " & _
            '        "   ,ISNULL(CAST(ActiveEmp AS NVARCHAR(10)),'-') AS ActiveEmp " & _
            '        "   ,ISNULL(CAST(NewStaff AS NVARCHAR(10)),'-') AS NewStaff " & _
            '        "   ,ISNULL(CAST(Retermi AS NVARCHAR(10)),'-') AS Retermi " & _
            '        "   ,@Caption1 AS ActCaption " & _
            '        "   ,@Caption2 AS Ncaption " & _
            '        "   ,@Caption3 AS retCaption " & _
            '        "   ,1  AS AId " & _
            '        "   ,2 AS NId " & _
            '        "   ,3 AS RId " & _
            '        "FROM hrdepartment_master " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "       departmentunkid " & _
            '        "       ,COUNT(employeeunkid) AS ActiveEmp " & _
            '        "   FROM hremployee_master " & _
            '        "   WHERE 1 = 1 "

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'If mblnIsActive = False Then
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS
            '    'StrQ &= "  AND  hremployee_master.isactive = 1"
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            'End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            ''Pinkal (24-Jun-2011) -- End

            'StrQ &= "AND DATEPART(mm,appointeddate) <= @enddate " & _
            '        "GROUP BY departmentunkid " & _
            '        ") AS ActE ON ActE.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "        departmentunkid " & _
            '        "       ,COUNT(employeeunkid) AS NewStaff " & _
            '        "   FROM hremployee_master " & _
            '        "   WHERE 1 = 1 "

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            ''Pinkal (24-Jun-2011) -- End

            'StrQ &= "AND DATEPART(mm,appointeddate) = @MonthId " & _
            '        "AND DATEPART(yy,appointeddate) = @YearId " & _
            '        "GROUP BY departmentunkid " & _
            '        ") AS NStaff ON NStaff.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "   SELECT " & _
            '        "       Id " & _
            '        "      ,ISNULL(SUM(Retermi),0) AS Retermi " & _
            '        "   FROM ( " & _
            '        "           SELECT " & _
            '        "                departmentunkid  as Id " & _
            '        "               ,COUNT(employeeunkid) AS Retermi " & _
            '        "           FROM hremployee_master " & _
            '        "           WHERE 1 = 1 "

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            'StrQ &= " AND DATEPART(mm, empl_enddate) = @MonthId AND DATEPART(yy, empl_enddate) = @YearId " & _
            '        "GROUP BY departmentunkid " & _
            '        "UNION " & _
            '        "   SELECT " & _
            '        "        departmentunkid  as Id " & _
            '        "       ,COUNT(employeeunkid) AS Retermi " & _
            '        "   FROM hremployee_master " & _
            '        "WHERE 1 = 1 "

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (24-Jun-2011) -- End

            'StrQ &= " AND DATEPART(mm, termination_from_date) = @MonthId AND DATEPART(yy, termination_from_date) = @YearId AND empl_enddate IS NULL " & _
            '        "GROUP BY departmentunkid " & _
            '        "UNION " & _
            '        "   SELECT " & _
            '        "        departmentunkid  as Id " & _
            '        "       ,COUNT(employeeunkid) AS Retermi " & _
            '        "   FROM hremployee_master " & _
            '        "WHERE 1 = 1 "

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            ''Pinkal (24-Jun-2011) -- End

            'StrQ &= " AND DATEPART(mm, termination_to_date) = @MonthId AND DATEPART(yy, termination_to_date) = @YearId AND empl_enddate IS NULL AND termination_from_date IS NULL " & _
            '        "   GROUP BY departmentunkid ) AS A group by A.Id " & _
            '        ") AS Reti ON Reti.Id = hrdepartment_master.departmentunkid " & _
            '        "WHERE hrdepartment_master.isactive = 1 "

            'For i As Integer = 1 To 2
            '    objDataOperation.ClearParameters()
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    If mblnIsActive = False Then
            '        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "No. of Active Employee (B/F)"))
            '    objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Newly Hired Employee"))
            '    objDataOperation.AddParameter("@Caption3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Retired/Terminated"))
            '    Select Case i
            '        Case 1 'Current Month
            '            objDataOperation.AddParameter("@MonthId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrentMonthId)
            '            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrCYear)

            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iCDate)
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iCDate)

            '            dsCurrent = objDataOperation.ExecQuery(StrQ, "CDataTable")

            '            If objDataOperation.ErrorMessage <> "" Then
            '                exForce = New Exception(objDataOperation.ErrorNumber & "  :  " & objDataOperation.ErrorMessage)
            '                Throw exForce
            '            End If

            '        Case 2 'Previous Month
            '            objDataOperation.AddParameter("@MonthId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPreviousMonthId)
            '            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrPYear)

            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iPDate)
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iPDate)

            '            dsPrevious = objDataOperation.ExecQuery(StrQ, "PDataTable")

            '            If objDataOperation.ErrorMessage <> "" Then
            '                exForce = New Exception(objDataOperation.ErrorNumber & "  :  " & objDataOperation.ErrorMessage)
            '                Throw exForce
            '            End If

            '    End Select
            'Next

            StrQ = "SELECT " & _
                    "    hrdepartment_master.departmentunkid " & _
                    "   ,ISNULL(name,'') AS Dept " & _
                    "   ,ISNULL(CAST(ActiveEmp AS NVARCHAR(10)),'-') AS ActiveEmp " & _
                    "   ,ISNULL(CAST(NewStaff AS NVARCHAR(10)),'-') AS NewStaff " & _
                    "   ,ISNULL(CAST(Retermi AS NVARCHAR(10)),'-') AS Retermi " & _
                    "   ,@Caption1 AS ActCaption " & _
                    "   ,@Caption2 AS Ncaption " & _
                    "   ,@Caption3 AS retCaption " & _
                    "   ,1  AS AId " & _
                    "   ,2 AS NId " & _
                    "   ,3 AS RId " & _
                    "FROM hrdepartment_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        A.departmentunkid " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS ActiveEmp " & _
                    "   FROM hremployee_master " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= dtPeriodEnd " & _
                    "   ) AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                    " Date_Join_Qry " & _
                    " Emp_UAC_Qry " & _
                    "   WHERE 1 = 1 "
            'S.SANDEEP [15 NOV 2016] -- START {Emp_UAC_Qry} -- END

            If mblnIsActive = False Then
                StrQ &= " Active_Employee_Condition "
            End If

            StrQ &= " User_Access_Filter "

            StrQ &= "AND DATEPART(mm,appointeddate) <= dtPeriodEnd " & _
                    "GROUP BY A.departmentunkid " & _
                    ") AS ActE ON ActE.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        A.departmentunkid " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS NewStaff " & _
                    "   FROM hremployee_master " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= dtPeriodEnd " & _
                    "   ) AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                    "   WHERE 1 = 1 "

            StrQ &= " User_Access_Filter "

            StrQ &= "AND DATEPART(mm,appointeddate) = MonthId " & _
                    "AND DATEPART(yy,appointeddate) = YearId " & _
                    "GROUP BY A.departmentunkid " & _
                    ") AS NStaff ON NStaff.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "       Id " & _
                    "      ,ISNULL(SUM(Retermi),0) AS Retermi " & _
                    "   FROM " & _
                    "   ( " & _
                    "      SELECT " & _
                    "        RT.departmentunkid  as Id " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS Retermi " & _
                    "      FROM hremployee_master " & _
                    "      JOIN " & _
                    "      ( " & _
                    "           SELECT " & _
                    "                departmentunkid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_transfer_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= dtPeriodEnd " & _
                    "      ) AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                    "      JOIN " & _
                    "      ( " & _
                    "           SELECT " & _
                    "                date1 AS empl_enddate " & _
                    "               ,date2 AS termination_from_date " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_dates_tran " & _
                    "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= dtPeriodEnd " & _
                    "           AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
                    "       ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.rno = 1 " & _
                    "       WHERE 1 = 1 "

            StrQ &= " User_Access_Filter "

            StrQ &= " AND (DATEPART(mm, T.empl_enddate) = MonthId AND DATEPART(yy, T.empl_enddate) = YearId OR " & _
                    "      DATEPART(mm, T.termination_from_date) = MonthId AND DATEPART(yy, T.termination_from_date) = YearId) " & _
                    "GROUP BY RT.departmentunkid " & _
                    "UNION " & _
                    "   SELECT " & _
                    "        RA.departmentunkid  as Id " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS Retermi " & _
                    "   FROM hremployee_master " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= dtPeriodEnd " & _
                    "   ) AS RA ON RA.employeeunkid = hremployee_master.employeeunkid AND RA.rno = 1 " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            date1 AS termination_to_date " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_dates_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= dtPeriodEnd " & _
                    "       AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
                    "   ) AS R ON R.employeeunkid = hremployee_master.employeeunkid AND R.rno = 1 " & _
                    "WHERE 1 = 1 "

            StrQ &= " User_Access_Filter "

            StrQ &= "   AND DATEPART(mm, R.termination_to_date) = MonthId AND DATEPART(yy, R.termination_to_date) = YearId " & _
                    "   GROUP BY RA.departmentunkid " & _
                    "   ) AS A group by A.Id " & _
                    ") AS Reti ON Reti.Id = hrdepartment_master.departmentunkid " & _
                    "WHERE hrdepartment_master.isactive = 1 "


            For i As Integer = 1 To 2
                Dim StrExecQry As String = StrQ
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "No. of Active Employee (B/F)"))
                objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Newly Hired Employee"))
                objDataOperation.AddParameter("@Caption3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Retired/Terminated"))
                Select Case i
                    Case 1 'Current Month
                        dtPeriodStart = eZeeDate.convertDate(iCDate)
                        dtPeriodEnd = eZeeDate.convertDate(iCDate)

                        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
                        If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
                        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bDate_Join_Qry\b", " " & xDateJoinQry & " ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bActive_Employee_Condition\b", " " & xDateFilterQry & " ")
                        'S.SANDEEP [15 NOV 2016] -- START
                        'StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bUser_Access_Filter\b", " AND " & xUACFiltrQry & " ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bUser_Access_Filter\b", " ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bEmp_UAC_Qry\b", " " & xUACQry & " ")
                        'S.SANDEEP [15 NOV 2016] -- END
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bdtPeriodEnd\b", " '" & iCDate & "' ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bMonthId\b", " '" & mintCurrentMonthId & "' ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bYearId\b", " '" & mstrCYear & "' ")


                        dsCurrent = objDataOperation.ExecQuery(StrExecQry, "CDataTable")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & "  :  " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Case 2 'Previous Month
                        dtPeriodStart = eZeeDate.convertDate(iPDate)
                        dtPeriodEnd = eZeeDate.convertDate(iPDate)

                        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
                        If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
                        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bDate_Join_Qry\b", " " & xDateJoinQry & " ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bActive_Employee_Condition\b", " " & xDateFilterQry & " ")
                        'S.SANDEEP [15 NOV 2016] -- START
                        'StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bUser_Access_Filter\b", " AND " & xUACFiltrQry & " ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bUser_Access_Filter\b", " ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bEmp_UAC_Qry\b", " " & xUACQry & " ")
                        'S.SANDEEP [15 NOV 2016] -- END
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bdtPeriodEnd\b", " '" & iPDate & "' ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bMonthId\b", " '" & mintCurrentMonthId & "' ")
                        StrExecQry = System.Text.RegularExpressions.Regex.Replace(StrExecQry, "\bYearId\b", " '" & mstrCYear & "' ")

                        dsPrevious = objDataOperation.ExecQuery(StrExecQry, "PDataTable")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & "  :  " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                End Select
            Next
            'S.SANDEEP [04 JUN 2015] -- END



            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim dtTable As New DataTable
            For j As Integer = 1 To 3
                Select Case j
                    Case 1 'Active Employee
                        dtTable = New DataView(dsCurrent.Tables("CDataTable"), "Aid = '" & j.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows
                            Dim dtTemp() As DataRow = dsPrevious.Tables("PDataTable").Select("departmentunkid = '" & dtRow.Item("departmentunkid") & "'")
                            If dtTemp.Length > 0 Then
                                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                                rpt_Row.Item("Column1") = dtRow.Item("Aid")
                                rpt_Row.Item("Column2") = dtRow.Item("Dept")
                                rpt_Row.Item("Column3") = dtTemp(0)("ActiveEmp")
                                rpt_Row.Item("Column4") = dtRow.Item("ActiveEmp")

                                If dtRow.Item("ActiveEmp").ToString.Trim = "-" And dtTemp(0)("ActiveEmp").ToString.Trim = "-" Then
                                    rpt_Row.Item("Column5") = "-"
                                ElseIf dtTemp(0)("ActiveEmp").ToString = "-" Then
                                    rpt_Row.Item("Column5") = dtRow.Item("ActiveEmp")
                                ElseIf dtRow.Item("ActiveEmp").ToString.Trim = "-" Then
                                    rpt_Row.Item("Column5") = dtTemp(0)("ActiveEmp")
                                Else
                                    rpt_Row.Item("Column5") = dtTemp(0)("ActiveEmp") - dtRow.Item("ActiveEmp")
                                End If
                                rpt_Row.Item("Column6") = dtRow.Item("ActCaption")

                                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                            End If
                        Next
                    Case 2 'New Staff
                        dtTable = New DataView(dsCurrent.Tables("CDataTable"), "Nid = '" & j.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows
                            Dim dtTemp() As DataRow = dsPrevious.Tables("PDataTable").Select("departmentunkid = '" & dtRow.Item("departmentunkid") & "'")
                            If dtTemp.Length > 0 Then
                                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                                rpt_Row.Item("Column1") = dtRow.Item("NId")
                                rpt_Row.Item("Column2") = dtRow.Item("Dept")
                                rpt_Row.Item("Column3") = dtTemp(0)("NewStaff")
                                rpt_Row.Item("Column4") = dtRow.Item("NewStaff")

                                If dtRow.Item("NewStaff").ToString.Trim = "-" And dtTemp(0)("NewStaff").ToString.Trim = "-" Then
                                    rpt_Row.Item("Column5") = "-"
                                ElseIf dtTemp(0)("NewStaff").ToString = "-" Then
                                    rpt_Row.Item("Column5") = dtRow.Item("NewStaff")
                                ElseIf dtRow.Item("NewStaff").ToString.Trim = "-" Then
                                    rpt_Row.Item("Column5") = dtTemp(0)("NewStaff")
                                Else
                                    rpt_Row.Item("Column5") = dtTemp(0)("NewStaff") - dtRow.Item("NewStaff")
                                End If
                                rpt_Row.Item("Column6") = dtRow.Item("Ncaption")

                                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                            End If
                        Next
                    Case 3 'Ret/Term.
                        dtTable = New DataView(dsCurrent.Tables("CDataTable"), "Rid = '" & j.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows
                            Dim dtTemp() As DataRow = dsPrevious.Tables("PDataTable").Select("departmentunkid = '" & dtRow.Item("departmentunkid") & "'")
                            If dtTemp.Length > 0 Then
                                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                                rpt_Row.Item("Column1") = dtRow.Item("RId")
                                rpt_Row.Item("Column2") = dtRow.Item("Dept")
                                rpt_Row.Item("Column3") = dtTemp(0)("Retermi")
                                rpt_Row.Item("Column4") = dtRow.Item("Retermi")

                                If dtRow.Item("Retermi").ToString.Trim = "-" And dtTemp(0)("Retermi").ToString.Trim = "-" Then
                                    rpt_Row.Item("Column5") = "-"
                                ElseIf dtTemp(0)("Retermi").ToString = "-" Then
                                    rpt_Row.Item("Column5") = dtRow.Item("Retermi")
                                ElseIf dtRow.Item("Retermi").ToString.Trim = "-" Then
                                    rpt_Row.Item("Column5") = dtTemp(0)("Retermi")
                                Else
                                    rpt_Row.Item("Column5") = dtTemp(0)("Retermi") - dtRow.Item("Retermi")
                                End If
                                rpt_Row.Item("Column6") = dtRow.Item("retCaption")

                                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                            End If
                        Next
                End Select
            Next

            objRpt = New ArutiReport.Designer.rptEmpMonthlyPhysicalReport


            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 13, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 14, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 15, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 16, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 17, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtPreviousMonth", Language.getMessage(mstrModuleName, 5, "Previous Month :") & " " & mstrPreviousMonthName & " - " & mstrPYear)
            Call ReportFunction.TextChange(objRpt, "txtCurrentMonth", Language.getMessage(mstrModuleName, 6, "Current Month : ") & " " & mstrCurrentMonthName & " - " & mstrCYear)
            Call ReportFunction.TextChange(objRpt, "txtVariance", Language.getMessage(mstrModuleName, 7, "Variance"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 8, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTot", Language.getMessage(mstrModuleName, 9, "Grand Total (C/F) :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            'Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "No. of Active Employee (B/F)")
            Language.setMessage(mstrModuleName, 2, "Newly Hired Employee")
            Language.setMessage(mstrModuleName, 3, "Retired/Terminated")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Previous Month :")
            Language.setMessage(mstrModuleName, 6, "Current Month :")
            Language.setMessage(mstrModuleName, 7, "Variance")
            Language.setMessage(mstrModuleName, 8, "Sub Total :")
            Language.setMessage(mstrModuleName, 9, "Grand Total (C/F) :")
            Language.setMessage(mstrModuleName, 10, "Printed By :")
            Language.setMessage(mstrModuleName, 11, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Page :")
            Language.setMessage(mstrModuleName, 13, "Prepared By :")
            Language.setMessage(mstrModuleName, 14, "Checked By :")
            Language.setMessage(mstrModuleName, 15, "Approved By :")
            Language.setMessage(mstrModuleName, 16, "Received By :")
            Language.setMessage(mstrModuleName, 17, "Department")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
