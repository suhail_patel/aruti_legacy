'************************************************************************************************************************************
'Class Name : frmEmployeeAssetRegister.vb
'Purpose    : 
'Written By : Anjan
'Modified   : 
'************************************************************************************************************************************
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployeeAssetRegister

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeAssetRegister"
    Private objAssetsRegister As clsEmployeeAssetRegister
    'S.SANDEEP [ 06 SEP 2011 ] -- START
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'S.SANDEEP [ 06 SEP 2011 ] -- END 

    'Anjan (17 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Anjan (17 May 2012)-End 

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        objAssetsRegister = New clsEmployeeAssetRegister(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAssetsRegister.SetDefaultValue()
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END

    End Sub

#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCommonMaster As New clsCommon_Master
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData 'Anjan (28 Aug 2017)


        Try

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmployee.GetEmployeeList("List", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objEmployee.GetEmployeeList("List", True, False)
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If
            'S.SANDEEP [ 15 MAY 2012 ] -- END
            'Pinkal (24-Jun-2011) -- End

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "List")
            With cboCondition
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Anjan (28 Aug 2017) -- Start
            'dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ASSET_STATUS, True, "List")
            'With cboStatus
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("List")
            '    .SelectedValue = 0
            'End With

            dsList = objMaster.getComboListForEmployeeAseetStatus("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Status")
            End With
            'Anjan (28 Aug 2017) -- End

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "List")
            With cboAssets
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "EmployeeWise Assets Register"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "AssetsWise Assets Register"))
            cboReportType.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()

        Try
            cboAssets.SelectedValue = 0
            cboCondition.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            cboStatus.SelectedValue = 0
            objAssetsRegister.setDefaultOrderBy(0)
            txtOrderBy.Text = objAssetsRegister.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try

    End Sub


    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    'Private Sub SetValue()

    '    Try
    '        If cboAssets.SelectedValue > 0 Then
    '            objAssetsRegister._AssetsId = cboAssets.SelectedValue
    '            objAssetsRegister._AssetsName = cboAssets.Text
    '        End If

    '        If cboCondition.SelectedValue > 0 Then
    '            objAssetsRegister._AssetsCondition_Id = cboCondition.SelectedValue
    '            objAssetsRegister._AssetsConditionName = cboCondition.Text
    '        End If

    '        If cboEmployee.SelectedValue > 0 Then
    '            objAssetsRegister._EmployeeId = cboEmployee.SelectedValue
    '            objAssetsRegister._EmployeeName = cboEmployee.Text
    '        End If


    '        objAssetsRegister._ReportId = cboReportType.SelectedIndex
    '        objAssetsRegister._ReportType = cboReportType.Text



    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
    '    End Try

    'End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END


    Private Function SetFilter() As Boolean

        Try
            objAssetsRegister.SetDefaultValue()

            objAssetsRegister._ReportId = cboReportType.SelectedIndex
            objAssetsRegister._ReportType = cboReportType.Text

            objAssetsRegister._EmployeeId = cboEmployee.SelectedValue
            objAssetsRegister._EmployeeName = cboEmployee.Text

            objAssetsRegister._AssetsId = cboAssets.SelectedValue
            objAssetsRegister._AssetsName = cboAssets.Text

            objAssetsRegister._AssetsCondition_Id = cboCondition.SelectedValue
            objAssetsRegister._AssetsConditionName = cboCondition.Text

            objAssetsRegister._AssetsStatusId = cboStatus.SelectedValue
            objAssetsRegister._AssetsStatus = cboStatus.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objAssetsRegister._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [ 06 SEP 2011 ] -- START
            objAssetsRegister._ViewByIds = mstrStringIds
            objAssetsRegister._ViewIndex = mintViewIdx
            objAssetsRegister._ViewByName = mstrStringName
            'S.SANDEEP [ 06 SEP 2011 ] -- END 

            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objAssetsRegister._Analysis_Fields = mstrAnalysis_Fields
            objAssetsRegister._Analysis_Join = mstrAnalysis_Join
            objAssetsRegister._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAssetsRegister._Report_GroupName = mstrReport_GroupName
            'Anjan (17 May 2012)-End 

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAssetsRegister._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function
#End Region

#Region " Forms "

    Private Sub frmEmployeeAssetsRegister_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        Try
            objAssetsRegister = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAssetsRegister_FormClosed", mstrModuleName)
        End Try

    End Sub
    Private Sub frmEmployeeAssetRegister_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objAssetsRegister._ReportName
            Me._Message = objAssetsRegister._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAssetRegister_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyPress", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Buttons "
    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click

        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetsRegister.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
            objAssetsRegister.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                                cboReportType.SelectedIndex, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetsRegister.generateReport(cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            objAssetsRegister.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, _
                                                cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click

        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeAssetRegister.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeAssetRegister"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click

        Try
            objAssetsRegister.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objAssetsRegister.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.SelectedValue
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Anjan (17 May 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblCondition.Text = Language._Object.getCaption(Me.lblCondition.Name, Me.lblCondition.Text)
			Me.lblAssets.Text = Language._Object.getCaption(Me.lblAssets.Name, Me.lblAssets.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "EmployeeWise Assets Register")
			Language.setMessage(mstrModuleName, 2, "AssetsWise Assets Register")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
