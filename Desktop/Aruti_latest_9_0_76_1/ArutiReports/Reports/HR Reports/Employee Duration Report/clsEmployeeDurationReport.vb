'************************************************************************************************************************************
'Class Name : clsEmployeeDurationReport.vb
'Purpose    :
'Date       :28/01/2013
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsEmployeeDurationReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeDurationReport"
    Private mstrReportId As String = enArutiReport.EmployeeDurationReport '110
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintFromYear As Integer = 0
    Private mintToYear As Integer = 0
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintJobId As Integer = -1
    Private mstrjobName As String = String.Empty
    Private mblnIsActive As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintAllocationTypeId As Integer = 1
    Private mstrAllocationName As String = String.Empty
    Private mstrSelectedAllocationIds As String = String.Empty
    Private mstrSelectAlloc As String = String.Empty
    Private mstrAlloctionJoin As String = String.Empty
    Private mintAllocYearFrom As Integer = -1
    Private mintAllocYearTo As Integer = -1
    Private mintAllocMonthFrom As Integer = -1
    Private mintAllocMonthTo As Integer = -1
    Private mintAllocDayFrom As Integer = -1
    Private mintAllocDayTo As Integer = -1
    Private mblnFromCurrDate As Boolean = False
    Private mstrCurrDateCaption As String = String.Empty
    Private mblnCurrAllocation As Boolean = False
    Private mstrCurrAllocationCaption As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrSelectAllocIdColumn As String = String.Empty
    Private mstrAllocationColName As String = String.Empty
    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
    'Sohail (24 Dec 2018) -- Start
    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
    Private mdtAsOnDate As Date
    'Sohail (24 Dec 2018) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _JobName() As String
        Set(ByVal value As String)
            mstrjobName = value
        End Set
    End Property

    Public WriteOnly Property _FromYear() As Integer
        Set(ByVal value As Integer)
            mintFromYear = value
        End Set
    End Property

    Public WriteOnly Property _ToYear() As Integer
        Set(ByVal value As Integer)
            mintToYear = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _Allocation_Name() As String
        Set(ByVal value As String)
            mstrAllocationName = value
        End Set
    End Property

    Public WriteOnly Property _SelectedAllocationIds() As String
        Set(ByVal value As String)
            mstrSelectedAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _SelectAlloc() As String
        Set(ByVal value As String)
            mstrSelectAlloc = value
        End Set
    End Property

    Public WriteOnly Property _AlloctionJoin() As String
        Set(ByVal value As String)
            mstrAlloctionJoin = value
        End Set
    End Property

    Public WriteOnly Property _Alloc_YearFrom() As Integer
        Set(ByVal value As Integer)
            mintAllocYearFrom = value
        End Set
    End Property

    Public WriteOnly Property _Alloc_YearTo() As Integer
        Set(ByVal value As Integer)
            mintAllocYearTo = value
        End Set
    End Property

    Public WriteOnly Property _Alloc_MonthFrom() As Integer
        Set(ByVal value As Integer)
            mintAllocMonthFrom = value
        End Set
    End Property

    Public WriteOnly Property _Alloc_MonthTo() As Integer
        Set(ByVal value As Integer)
            mintAllocMonthTo = value
        End Set
    End Property

    Public WriteOnly Property _Alloc_DayFrom() As Integer
        Set(ByVal value As Integer)
            mintAllocDayFrom = value
        End Set
    End Property

    Public WriteOnly Property _Alloc_DayTo() As Integer
        Set(ByVal value As Integer)
            mintAllocDayTo = value
        End Set
    End Property

    Public WriteOnly Property _Exp_FromCurrDate() As Boolean
        Set(ByVal value As Boolean)
            mblnFromCurrDate = value
        End Set
    End Property

    Public WriteOnly Property _CurrDate_Caption() As String
        Set(ByVal value As String)
            mstrCurrDateCaption = value
        End Set
    End Property

    Public WriteOnly Property _CurrAllocation() As Boolean
        Set(ByVal value As Boolean)
            mblnCurrAllocation = value
        End Set
    End Property

    Public WriteOnly Property _CurrAllocationCaption() As String
        Set(ByVal value As String)
            mstrCurrAllocationCaption = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _SelectAllocIdColumn() As String
        Set(ByVal value As String)
            mstrSelectAllocIdColumn = value
        End Set
    End Property

    Public WriteOnly Property _AllocationColName() As String
        Set(ByVal value As String)
            mstrAllocationColName = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

    'Sohail (24 Dec 2018) -- Start
    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (24 Dec 2018) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintFromYear = 0
            mintToYear = 0
            mintJobId = 0
            mstrjobName = String.Empty
            mblnIsActive = True
            mstrUserAccessFilter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintAllocationTypeId = 1
            mstrAllocationName = String.Empty
            mstrSelectedAllocationIds = String.Empty
            mstrSelectAlloc = String.Empty
            mstrAlloctionJoin = String.Empty
            mintAllocYearFrom = -1
            mintAllocYearTo = -1
            mintAllocMonthFrom = -1
            mintAllocMonthTo = -1
            mintAllocDayFrom = -1
            mintAllocDayTo = -1
            mblnFromCurrDate = False
            mstrCurrDateCaption = String.Empty
            mblnCurrAllocation = False
            mstrCurrAllocationCaption = String.Empty
            mstrAdvance_Filter = ""
            mstrSelectAllocIdColumn = String.Empty
            mstrAllocationColName = String.Empty
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.ClearParameters()

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            Select Case mintReportTypeId
                Case 0  'EMPLOYEE JOBS DURATION
                    If mintEmployeeId > 0 Then
                        objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                        Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
                    End If

                    If mintJobId > 0 Then
                        objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                        Me._FilterQuery &= " AND JB.jobunkid = @JobId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If mblnFromCurrDate = False Then
                    '    objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
                    '    Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

                    '    If mintToYear > 0 Then
                    '        objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
                    '        Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
                    '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                    '    End If
                    'ElseIf mblnFromCurrDate = True Then
                    '    objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
                    '    objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)

                    '    Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) BETWEEN  @FromYear AND @ToYear "
                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                    'End If

                    If mblnFromCurrDate = False Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

                        If mintToYear > 0 Then
                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                        End If
                    ElseIf mblnFromCurrDate = True Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END



                    If Me.OrderByQuery <> "" Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
                        Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                    End If

                Case 1  'ALLOCATION DURATION
                    If mblnCurrAllocation = True Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrAllocationCaption & " "
                    End If

                    If mintEmployeeId > 0 Then
                        objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                        Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
                    End If


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    'If mintAllocYearFrom >= 0 AndAlso mintAllocYearTo >= 0 Then
                    '    objDataOperation.AddParameter("@AFYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocYearFrom)
                    '    objDataOperation.AddParameter("@ATYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocYearTo)
                    '    Me._FilterQuery &= " AND ((CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)- CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000) BETWEEN @AFYear AND @ATYear "

                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "From Year :") & " " & mintAllocYearFrom & _
                    '                       Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocYearTo & " "
                    'End If

                    'If mintAllocMonthFrom >= 0 AndAlso mintAllocMonthTo >= 0 Then
                    '    objDataOperation.AddParameter("@AFMonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocMonthFrom)
                    '    objDataOperation.AddParameter("@ATMonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocMonthTo)

                    '    Me._FilterQuery &= " AND ((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END) BETWEEN @AFMonth AND @ATMonth "

                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "From Month :") & " " & mintAllocMonthFrom & _
                    '                       Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocMonthTo & " "
                    'End If

                    'If mintAllocDayFrom >= 0 AndAlso mintAllocDayTo >= 0 Then
                    '    objDataOperation.AddParameter("@AFDay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocDayFrom)
                    '    objDataOperation.AddParameter("@ATDay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocDayTo)

                    '    Me._FilterQuery &= " AND (DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),ISNULL(hrallocation_tracking.end_date,GETDATE()))) BETWEEN @AFDay AND @ATDay "

                    '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "From Day :") & " " & mintAllocDayFrom & _
                    '                       Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocDayTo & " "
                    'End If


                    If mintAllocYearFrom >= 0 AndAlso mintAllocYearTo >= 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "From Year :") & " " & mintAllocYearFrom & _
                                           Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocYearTo & " "
                    End If

                    If mintAllocMonthFrom >= 0 AndAlso mintAllocMonthTo >= 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "From Month :") & " " & mintAllocMonthFrom & _
                                           Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocMonthTo & " "
                    End If

                    If mintAllocDayFrom >= 0 AndAlso mintAllocDayTo >= 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "From Day :") & " " & mintAllocDayFrom & _
                                           Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocDayTo & " "
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END




                    If mintAllocationTypeId > 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Allocation :") & " " & mstrAllocationName & " "
                    End If
                Case 2  'EMPLOYEE EXPERIENCE DURATION
                    If mblnFromCurrDate = False Then
                        If mintEmployeeId > 0 Then
                            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                            Me._FilterQuery &= " AND hremp_experience_tran.employeeunkid = @EmployeeId "
                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
                        End If

                        If mintJobId > 0 Then
                            objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                            Me._FilterQuery &= " AND hremp_experience_tran.jobunkid = @JobId "
                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
                        End If


                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
                        'Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
                        'Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

                        'If mintToYear > 0 Then
                        '    objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
                        '    Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
                        '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                        'End If

                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

                        If mintToYear > 0 Then
                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                        End If
                        'S.SANDEEP [04 JUN 2015] -- END


                    Else
                        If mintEmployeeId > 0 Then
                            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                            Me._FilterQuery &= " AND B.employeeunkid = @EmployeeId "
                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
                        End If

                        If mintJobId > 0 Then
                            objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                            Me._FilterQuery &= " AND B.jobunkid = @JobId "
                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
                        End If


                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
                        'objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
                        'Me._FilterQuery &= " AND B.Years BETWEEN @FromYear AND @ToYear "
                        'Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
                        'Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
                        'Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "

                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                        'S.SANDEEP [04 JUN 2015] -- END
                    End If

                    If Me.OrderByQuery <> "" Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
                        Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                    End If

                    'Sohail (24 Dec 2018) -- Start
                    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                Case 3
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "As On Date :") & " " & mdtAsOnDate.ToShortDateString & " "

                    If mintEmployeeId > 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                    End If

                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

                    If mintToYear > 0 Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
                    End If

                    If Me.OrderByQuery <> "" Then
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
                    End If
                    'Sohail (24 Dec 2018) -- End

            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Select Case pintReportType
                Case 0  'EMPLOYEE JOBS
                    objRpt = Generate_JobTitleReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                Case 1  'ALLOCATION
                    objRpt = Generate_AllocationReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                Case 2  'EXPERIENCE
                    objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                    'Sohail (24 Dec 2018) -- Start
                    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                Case 3 'YEAR OF SERVICE
                    objRpt = Generate_YearOfServiceReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                    'Sohail (24 Dec 2018) -- End

            End Select

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Set_Sort_Collection(ByVal blnIsCurrDate As Boolean)
        Try
            iColumn_DetailReport.Clear()
            If blnIsCurrDate = False Then
                'Sohail (24 Dec 2018) -- Start
                'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                If mintReportTypeId = 3 Then

                    iColumn_DetailReport.Add(New IColumn("Column2", Language.getMessage(mstrModuleName, 30, "Employee Name")))
                    iColumn_DetailReport.Add(New IColumn("Column1", Language.getMessage(mstrModuleName, 31, "Employee Code")))
                    iColumn_DetailReport.Add(New IColumn("Column81", Language.getMessage(mstrModuleName, 32, "Years")))


                Else
                    'Sohail (24 Dec 2018) -- End

                    If mblnFirstNamethenSurname = False Then
                        iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
                    Else
                        iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
                    End If
                    iColumn_DetailReport.Add(New IColumn("start_date", Language.getMessage(mstrModuleName, 4, "Start Date")))
                    iColumn_DetailReport.Add(New IColumn("end_date", Language.getMessage(mstrModuleName, 5, "End Date")))

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'iColumn_DetailReport.Add(New IColumn("ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0)", Language.getMessage(mstrModuleName, 6, "Years")))
                    'S.SANDEEP [04 JUN 2015] -- END

                End If 'Sohail (24 Dec 2018)

            Else
                Select Case mintReportTypeId
                    Case 0  'EMPLOYEE JOB
                        If mblnFirstNamethenSurname = False Then
                            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
                        Else
                            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
                        End If

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'iColumn_DetailReport.Add(New IColumn("ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0)", Language.getMessage(mstrModuleName, 6, "Years")))

                        'iColumn_DetailReport.Add(New IColumn("start_date", Language.getMessage(mstrModuleName, 4, "Start Date")))
                        'iColumn_DetailReport.Add(New IColumn("end_date", Language.getMessage(mstrModuleName, 5, "End Date")))

                        'S.SANDEEP [04 JUN 2015] -- END
                    Case 2  'EMPLOYEE EXPERIENCE
                        iColumn_DetailReport.Add(New IColumn("B.Ename", Language.getMessage(mstrModuleName, 20, "Employee")))
                        iColumn_DetailReport.Add(New IColumn("B.auditdatetime", Language.getMessage(mstrModuleName, 4, "Start Date")))
                        iColumn_DetailReport.Add(New IColumn("GETDATE()", Language.getMessage(mstrModuleName, 5, "End Date")))
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'iColumn_DetailReport.Add(New IColumn("Years", Language.getMessage(mstrModuleName, 6, "Years")))
                        'S.SANDEEP [04 JUN 2015] -- END
                End Select
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Sort_Collection; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            Call Set_Sort_Collection(False)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            If mblnFromCurrDate = False Then
                StrQ = " SELECT  " & _
                       " ISNULL(hremployee_master.employeecode, '') employeecode "
                If mblnFirstNamethenSurname = False Then
                    StrQ &= ",ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') employee "
                Else
                    StrQ &= ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee "
                End If

                StrQ &= ",ISNULL(hremp_experience_tran.old_job, '') Job " & _
                        ",start_date " & _
                        ",end_date " & _
                        ",CONVERT(CHAR(8),start_date,112) AS SDate " & _
                        ",CONVERT(CHAR(8),end_date,112) AS EDate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM hremp_experience_tran " & _
                        " JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid "

                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE 1 = 1 "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mblnIsActive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

            Else

                StrQ = "SELECT " & _
                       "     Ecode AS employeecode " & _
                       "    ,Ename AS employee " & _
                       "    ,Job AS Job " & _
                       "    ,start_date AS SDate " & _
                       "    ,end_date AS EDate " & _
                       "    ,Id AS Id " & _
                       "    ,GName AS GName " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "        hremployee_master.employeecode AS Ecode "
                If mblnFirstNamethenSurname = False Then
                    StrQ &= "  ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS Ename "
                Else
                    StrQ &= "  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename "
                End If
                StrQ &= ",CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS start_date " & _
                        ",CONVERT(CHAR(8),GETDATE(),112) AS end_date " & _
                        ",hremployee_master.employeeunkid " & _
                        ",old_job AS Job " & _
                        ",ROW_NUMBER()OVER (PARTITION BY hremployee_master.employeeunkid ORDER BY start_date DESC) AS Rno "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM hremp_experience_tran " & _
                        " JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid "

                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 AND end_date IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mblnIsActive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                StrQ &= " ) AS B WHERE Rno = 1 AND Ecode IS NOT NULL "

            End If

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing

            Dim Date1, Date2 As DateTime
            Date1 = Nothing : Date2 = Nothing
            Dim objDateDifference As New clsGetDateDifference

            For Each dFRow As DataRow In dsList.Tables(0).Rows

                Date1 = Nothing : Date2 = Nothing

                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dFRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dFRow.Item("employee")
                rpt_Rows.Item("Column3") = dFRow.Item("Job")

                If dFRow.Item("SDate").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dFRow.Item("SDate").ToString).ToShortDateString
                    Date1 = CDate(eZeeDate.convertDate(dFRow.Item("SDate").ToString)).Date
                Else
                    rpt_Rows.Item("Column4") = DBNull.Value
                End If
                If mblnFromCurrDate = True Then
                    rpt_Rows.Item("Column5") = ""
                    Date2 = Now
                Else
                    If dFRow.Item("EDate").ToString.Trim.Trim.Length > 0 Then
                        rpt_Rows.Item("Column5") = eZeeDate.convertDate(dFRow.Item("EDate").ToString).ToShortDateString
                        Date2 = CDate(eZeeDate.convertDate(dFRow.Item("EDate").ToString)).Date
                    Else
                        rpt_Rows.Item("Column5") = DBNull.Value
                    End If
                End If

                If Date1 <> Nothing AndAlso Date2 <> Nothing Then
                    objDateDifference.PerformDateOperation(Date1, Date2)
                    rpt_Rows.Item("Column6") = objDateDifference.Years
                    rpt_Rows.Item("Column7") = objDateDifference.Months
                    rpt_Rows.Item("Column8") = objDateDifference.Days
                Else
                    rpt_Rows.Item("Column6") = ""
                    rpt_Rows.Item("Column7") = ""
                    rpt_Rows.Item("Column8") = ""
                End If
                rpt_Rows.Item("Column9") = dFRow.Item("GName")
                rpt_Rows.Item("Column10") = dFRow.Item("Id")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            Dim StrFilter As String = String.Empty

            If mblnFromCurrDate = False Then
                StrFilter = "Column6 >= '" & mintFromYear & "' "
                If mintToYear > 0 Then
                    StrFilter &= " AND Column6 <= '" & mintToYear & "' "
                End If
            Else
                StrFilter = "Column6 >= '" & mintFromYear & "' AND Column6 <= '" & mintToYear & "' "
            End If

            Dim dv As DataView = New DataView(rpt_Data.Tables("ArutiTable"), StrFilter, "", DataViewRowState.CurrentRows)
            rpt_Data.Tables.Remove("ArutiTable")
            rpt_Data.Tables.Add(dv.ToTable)

            objDateDifference = Nothing

            objRpt = New ArutiReport.Designer.rptEmployeeDurationReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 21, "Employee Count :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

    Private Function Generate_AllocationReport(ByVal strDatabaseName As String, _
                                               ByVal intUserUnkid As Integer, _
                                               ByVal intYearUnkid As Integer, _
                                               ByVal intCompanyUnkid As Integer, _
                                               ByVal dtPeriodStart As Date, _
                                               ByVal dtPeriodEnd As Date, _
                                               ByVal strUserModeSetting As String, _
                                               ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim objCompnay As New clsCompany_Master
            Dim dsCompList As DataSet = objCompnay.GetFinancialYearList(intCompanyUnkid)
            objCompnay = Nothing

            If dsCompList IsNot Nothing Then
                StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,allocid INT,sdate DATETIME, edate DATETIME) " & _
                       "DECLARE @allocid INT,@empid INT,@date DATETIME,@Id INT,@lallocid INT, @lempid INT " & _
                       "SET @allocid = 0 " & _
                       "SET @Id = 0 " & _
                       "SET @lallocid = 0 " & _
                       "SET @lempid = 0 " & _
                       "DECLARE EM CURSOR FOR " & _
                       "SELECT " & _
                       "     hremployee_master.employeeunkid " & _
                       "    ,AL.allocationunkid " & _
                       "    ,AL.SDate " & _
                       "FROM " & _
                       "" & strDatabaseName & "..hremployee_master " & _
                       "JOIN " & _
                       "( "

                For i As Integer = 0 To dsCompList.Tables(0).Rows.Count - 1
                    StrQ &= "SELECT " & _
                            "    employeeunkid " & _
                            "" & mstrSelectAllocIdColumn.Replace("ISNULL(", "ISNULL(" & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..") & " " & _
                            "   ,CONVERT(CHAR(8),effectivedate,112) AS SDate "
                    If mblnCurrAllocation Then
                        StrQ &= "   ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno "
                    Else
                        StrQ &= "   ,ROW_NUMBER()OVER(PARTITION BY employeeunkid," & mstrAllocationColName & " ORDER BY effectivedate ASC) AS rno "
                    End If

                    StrQ &= "FROM " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..hremployee_transfer_tran " & _
                            "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            " " & mstrSelectedAllocationIds.Replace("AND", "AND " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..") & " "

                    If i <> dsCompList.Tables(0).Rows.Count - 1 Then
                        StrQ &= " UNION "
                    End If

                Next

                StrQ &= ") AS AL ON AL.employeeunkid = " & strDatabaseName & "..hremployee_master.employeeunkid AND AL.rno = 1 "


                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END



                StrQ &= " WHERE 1 = 1 "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mblnIsActive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                StrQ &= "ORDER BY " & strDatabaseName & "..hremployee_master.employeeunkid,AL.SDate " & _
                        "OPEN EM " & _
                        "   FETCH NEXT FROM EM INTO @empid, @allocid, @date " & _
                        "       WHILE @@FETCH_STATUS = 0 " & _
                        "       BEGIN " & _
                        "           IF @empid <> @lempid " & _
                        "           BEGIN " & _
                        "               SET @lallocid = 0 " & _
                        "               SET @lempid = @empid " & _
                        "           END " & _
                        "           IF @allocid <> @lallocid " & _
                        "           BEGIN " & _
                        "               INSERT INTO @EM(empid,allocid,sdate,edate)SELECT @empid, @allocid,@date,NULL " & _
                        "               SET @Id = @@IDENTITY; " & _
                        "               UPDATE @EM SET edate = CASE WHEN DATEADD(DAY,-1,@date) < sdate THEN @date ELSE DATEADD(DAY,-1,@date) END WHERE id = @Id - 1 AND empid = @empid " & _
                        "               SET @lallocid = @allocid " & _
                        "           END " & _
                        "           FETCH NEXT FROM EM into @empid, @allocid, @date " & _
                        "       END " & _
                        "CLOSE EM " & _
                        "DEALLOCATE EM " & _
                        "SELECT " & _
                        "    hremployee_master.employeecode AS ECode " & _
                        "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                        "   ,CONVERT(CHAR(8),[@EM].sdate,112) AS SDate " & _
                        "   ,CONVERT(CHAR(8),[@EM].edate,112) AS EDate " & _
                        "" & mstrSelectAlloc & " " & _
                        "FROM @EM " & _
                        "   JOIN hremployee_master ON [@EM].empid = hremployee_master.employeeunkid " & _
                        "" & mstrAlloctionJoin & " " & _
                        "WHERE 1 = 1 "

                If mblnCurrAllocation = True Then
                    StrQ &= " AND [@EM].edate IS NULL "
                End If


                Call FilterTitleAndFilterQuery()
                StrQ &= Me._FilterQuery


                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                rpt_Data = New ArutiReport.Designer.dsArutiReport
                Dim rpt_Rows As DataRow = Nothing

                Dim Date1, Date2 As DateTime
                Date1 = Nothing : Date2 = Nothing
                Dim objDateDifference As New clsGetDateDifference

                For Each dFRow As DataRow In dsList.Tables(0).Rows

                    Date1 = Nothing : Date2 = Nothing

                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dFRow.Item("ECode")
                    rpt_Rows.Item("Column2") = dFRow.Item("EName")
                    rpt_Rows.Item("Column3") = dFRow.Item("Allocation")
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dFRow.Item("SDate").ToString).ToShortDateString

                    Date1 = CDate(eZeeDate.convertDate(dFRow.Item("SDate").ToString))

                    If dFRow.Item("EDate").ToString.Trim.Length > 0 Then
                        rpt_Rows.Item("Column5") = eZeeDate.convertDate(dFRow.Item("EDate").ToString).ToShortDateString
                        Date2 = CDate(eZeeDate.convertDate(dFRow.Item("EDate").ToString))
                    Else
                        Date2 = Now
                    End If

                    If Date1 <> Nothing AndAlso Date2 <> Nothing Then
                        objDateDifference.PerformDateOperation(Date1, Date2)
                        rpt_Rows.Item("Column6") = objDateDifference.Years
                        rpt_Rows.Item("Column7") = objDateDifference.Months
                        rpt_Rows.Item("Column8") = objDateDifference.Days
                    Else
                        rpt_Rows.Item("Column6") = ""
                        rpt_Rows.Item("Column7") = ""
                        rpt_Rows.Item("Column8") = ""
                    End If


                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

                Dim StrFilter As String = String.Empty

                If mintAllocYearFrom >= 0 AndAlso mintAllocYearTo >= 0 Then
                    StrFilter &= "AND Column6 >= '" & mintAllocYearFrom & "' AND Column6 <= '" & mintAllocYearTo & "' "
                End If

                If mintAllocMonthFrom >= 0 AndAlso mintAllocMonthTo >= 0 Then
                    StrFilter &= "AND Column7 >= '" & mintAllocMonthFrom & "' AND Column7 <= '" & mintAllocMonthTo & "' "
                End If

                If mintAllocDayFrom >= 0 AndAlso mintAllocDayTo >= 0 Then
                    StrFilter &= "AND Column8 >= '" & mintAllocDayFrom & "' AND Column8 <= '" & mintAllocDayTo & "' "
                End If

                If StrFilter.Trim.Length > 0 Then
                    StrFilter = StrFilter.Substring(3)
                End If

                Dim dv As DataView = New DataView(rpt_Data.Tables("ArutiTable"), StrFilter, "", DataViewRowState.CurrentRows)
                rpt_Data.Tables.Remove("ArutiTable")
                rpt_Data.Tables.Add(dv.ToTable)



                objDateDifference = Nothing

                objRpt = New ArutiReport.Designer.rptEmpAllocDuration

                ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

                Dim arrImageRow As DataRow = Nothing
                arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

                ReportFunction.Logo_Display(objRpt, _
                                            ConfigParameter._Object._IsDisplayLogo, _
                                            ConfigParameter._Object._ShowLogoRightSide, _
                                            "arutiLogo1", _
                                            "arutiLogo2", _
                                            arrImageRow, _
                                            "txtCompanyName", _
                                            "txtReportName", _
                                            "txtFilterDescription", _
                                            ConfigParameter._Object._GetLeftMargin, _
                                            ConfigParameter._Object._GetRightMargin)

                rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


                If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                    rpt_Data.Tables("ArutiTable").Rows.Add("")
                End If

                If ConfigParameter._Object._IsShowPreparedBy = True Then
                    Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                    Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Else
                    Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
                End If

                If ConfigParameter._Object._IsShowCheckedBy = True Then
                    Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
                Else
                    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
                End If

                If ConfigParameter._Object._IsShowApprovedBy = True Then
                    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
                Else
                    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
                End If

                If ConfigParameter._Object._IsShowReceivedBy = True Then
                    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
                Else
                    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
                End If

                objRpt.SetDataSource(rpt_Data)

                Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
                Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 2, "Employee :"))
                Call ReportFunction.TextChange(objRpt, "txtAllocation", Language.getMessage(mstrModuleName, 22, "Allocation"))
                Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
                Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
                Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
                Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
                Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))

                Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
                Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

                Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
                Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

                Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName & " - [" & mstrAllocationName & "] ")
                Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
                Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_AllocationReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

    Private Function Generate_JobTitleReport(ByVal strDatabaseName As String, _
                                             ByVal intUserUnkid As Integer, _
                                             ByVal intYearUnkid As Integer, _
                                             ByVal intCompanyUnkid As Integer, _
                                             ByVal dtPeriodStart As Date, _
                                             ByVal dtPeriodEnd As Date, _
                                             ByVal strUserModeSetting As String, _
                                             ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            If mblnFromCurrDate = False Then
                StrQ = "DECLARE @EJ AS TABLE(id INT IDENTITY,empid INT,jobunkid INT,start_date DATETIME, end_date DATETIME) " & _
                       "DECLARE @jobunkid INT,@empid INT,@date DATETIME,@Id INT,@ljobunkid INT, @lempid INT " & _
                       "SET @jobunkid = 0 " & _
                       "SET @Id = 0 " & _
                       "SET @ljobunkid = 0 " & _
                       "SET @lempid = 0 " & _
                       "DECLARE EJ CURSOR FOR " & _
                       "SELECT " & _
                       " hremployee_master.employeeunkid " & _
                       ",Jobs.jobunkid " & _
                       ",Jobs.SDate " & _
                       "FROM hremployee_master "
                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END
                StrQ &= "JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,jobunkid " & _
                       "        ,effectivedate AS SDate " & _
                       "        ,ROW_NUMBER() OVER (PARTITION BY employeeunkid,jobunkid ORDER BY effectivedate ASC ) AS rno " & _
                       "    FROM hremployee_categorization_tran " & _
                       "    WHERE isvoid = 0 " & _
                       "    AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                       ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 "

                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END
                

                StrQ &= " WHERE 1 = 1 "


                If mintEmployeeId > 0 Then
                    StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mblnIsActive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                StrQ &= "ORDER BY hremployee_master.employeeunkid,SDate "


                StrQ &= "OPEN EJ " & _
                        "   FETCH NEXT FROM EJ INTO @empid, @jobunkid, @date " & _
                        "   WHILE @@FETCH_STATUS = 0 " & _
                        "       BEGIN " & _
                        "           IF @empid <> @lempid " & _
                        "           BEGIN " & _
                        "               SET @ljobunkid = 0 " & _
                        "               SET @lempid = @empid " & _
                        "           END " & _
                        "           IF @jobunkid <> @ljobunkid " & _
                        "           BEGIN " & _
                        "               INSERT INTO @EJ(empid,jobunkid,start_date,end_date)SELECT @empid, @jobunkid,@date,NULL " & _
                        "               SET @Id = @@IDENTITY; " & _
                        "               UPDATE @EJ SET end_date = CASE WHEN DATEADD(DAY,-1,@date) < start_date THEN @date ELSE DATEADD(DAY,-1,@date) END WHERE id = @Id - 1 AND empid = @empid " & _
                        "               SET @ljobunkid = @jobunkid " & _
                        "           END " & _
                        "           FETCH NEXT FROM EJ into @empid, @jobunkid, @date " & _
                        "       END " & _
                        "CLOSE EJ " & _
                        "DEALLOCATE EJ " & _
                        "SELECT " & _
                        "     hremployee_master.employeecode "
                If mblnFirstNamethenSurname = False Then
                    StrQ &= ",ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') employee "
                Else
                    StrQ &= ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee "
                End If
                StrQ &= "   ,CONVERT(CHAR(8),[@EJ].start_date,112) AS SDate " & _
                        "   ,CONVERT(CHAR(8),[@EJ].end_date,112) AS EDate " & _
                        "   ,start_date AS [start_date] " & _
                        "   ,end_date AS end_date " & _
                        "   ,ISNULL(JB.job_name,'') AS Job " & _
                        "   ,[@EJ].jobunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM @EJ " & _
                        "   JOIN hremployee_master ON [@EJ].empid = hremployee_master.employeeunkid " & _
                        "   JOIN hrjob_master AS JB ON [@EJ].jobunkid = JB.jobunkid "

                StrQ &= mstrAnalysis_Join

                StrQ &= "WHERE 1 = 1 "


            Else

                StrQ = "SELECT " & _
                       " hremployee_master.employeecode "
                If mblnFirstNamethenSurname = False Then
                    StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS employee "
                Else
                    StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employee "
                End If
                StrQ &= ",JB.job_name AS Job " & _
                        ",CONVERT(CHAR(8),Jobs.start_date,112) AS SDate "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= "FROM hremployee_master "
                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
                'S.SANDEEP |28-OCT-2021| -- END
                StrQ &= "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        employeeunkid " & _
                        "       ,jobunkid " & _
                        "       ,effectivedate AS start_date " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
                If mintEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & mintEmployeeId & "' "
                End If
                StrQ &= ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        "JOIN hrjob_master AS JB ON Jobs.jobunkid = JB.jobunkid "

                'S.SANDEEP |28-OCT-2021| -- START
                'ISSUE : REPORT OPTIMZATION
                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                'S.SANDEEP |28-OCT-2021| -- END
               

                StrQ &= mstrAnalysis_Join

                StrQ &= "WHERE 1 = 1 "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry & " "
                End If

                If mblnIsActive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

            End If

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim Date1, Date2 As DateTime
            Date1 = Nothing : Date2 = Nothing
            Dim objDateDifference As New clsGetDateDifference

            Dim rpt_Rows As DataRow = Nothing
            For Each dFRow As DataRow In dsList.Tables(0).Rows

                Date1 = Nothing : Date2 = Nothing

                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dFRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dFRow.Item("employee")
                rpt_Rows.Item("Column3") = dFRow.Item("Job")
                If dFRow.Item("SDate").ToString.Trim.Length > 0 Then
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dFRow.Item("SDate").ToString).ToShortDateString
                    Date1 = CDate(eZeeDate.convertDate(dFRow.Item("SDate").ToString))
                Else
                    rpt_Rows.Item("Column4") = DBNull.Value
                End If
                If mblnFromCurrDate = True Then
                    rpt_Rows.Item("Column5") = ""
                    Date2 = Now
                Else
                    If dFRow.Item("EDate").ToString.Trim.Trim.Length > 0 Then
                        If dFRow.Item("EDate") = eZeeDate.convertDate(Now) Then
                            rpt_Rows.Item("Column5") = DBNull.Value
                        Else
                            rpt_Rows.Item("Column5") = eZeeDate.convertDate(dFRow.Item("EDate").ToString).ToShortDateString
                            Date2 = CDate(eZeeDate.convertDate(dFRow.Item("EDate").ToString))
                        End If
                    Else
                        rpt_Rows.Item("Column5") = DBNull.Value
                        Date2 = Now
                    End If
                End If
                If Date1 <> Nothing AndAlso Date2 <> Nothing Then
                    objDateDifference.PerformDateOperation(Date1, Date2)
                    rpt_Rows.Item("Column6") = objDateDifference.Years
                    rpt_Rows.Item("Column7") = objDateDifference.Months
                    rpt_Rows.Item("Column8") = objDateDifference.Days
                Else
                    rpt_Rows.Item("Column6") = ""
                    rpt_Rows.Item("Column7") = ""
                    rpt_Rows.Item("Column8") = ""
                End If
                rpt_Rows.Item("Column9") = dFRow.Item("GName")
                rpt_Rows.Item("Column10") = dFRow.Item("Id")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            Dim StrFilter As String = String.Empty

            If mblnFromCurrDate = False Then
                StrFilter = "Column6 >= " & mintFromYear
                If mintToYear > 0 Then
                    StrFilter &= " AND Column6 <= " & mintToYear
                End If
            Else
                StrFilter = "Column6 >= " & mintFromYear & " AND Column6 <= " & mintToYear
            End If

            Dim dv As DataView = New DataView(rpt_Data.Tables("ArutiTable"), StrFilter, "", DataViewRowState.CurrentRows)
            rpt_Data.Tables.Remove("ArutiTable")
            rpt_Data.Tables.Add(dv.ToTable)


            objDateDifference = Nothing

            objRpt = New ArutiReport.Designer.rptEmployeeDurationReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 21, "Employee Count :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_JobTitleReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

    'Sohail (24 Dec 2018) -- Start
    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
    Private Function Generate_YearOfServiceReport(ByVal strDatabaseName As String _
                                                  , ByVal intUserUnkid As Integer _
                                                  , ByVal intYearUnkid As Integer _
                                                  , ByVal intCompanyUnkid As Integer _
                                                  , ByVal dtPeriodStart As Date _
                                                  , ByVal dtPeriodEnd As Date _
                                                  , ByVal strUserModeSetting As String _
                                                  , ByVal blnOnlyApproved As Boolean _
                                                  ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim objEmpDates As New clsemployee_dates_tran
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT  hremployee_master.employeeunkid " & _
                          ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
                          ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                          ", ISNULL(CONVERT(CHAR(8), ETERM.termination_from_date, 112), '') AS termination_from_date " & _
                          ", ISNULL(CONVERT(CHAR(8), ERET.termination_to_date, 112), '') AS termination_to_date " & _
                          ", ISNULL(CONVERT(CHAR(8), ETERM.empl_enddate, 112), '') AS empl_enddate "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') employee "
            Else
                StrQ &= ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "		 TERM.TEEmpId " & _
                            "		,TERM.empl_enddate " & _
                            "		,TERM.termination_from_date " & _
                            "		,TERM.TEfDt " & _
                            "		,TERM.isexclude_payroll " & _
                            "		,TERM.TR_REASON " & _
                            "		,TERM.changereasonunkid " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            TRM.employeeunkid AS TEEmpId " & _
                            "           ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                            "           ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                            "           ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                            "           ,TRM.isexclude_payroll " & _
                            "			,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                            "			,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                            "           ,TRM.changereasonunkid " & _
                            "       FROM hremployee_dates_tran AS TRM " & _
                            "			LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "			LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                            "           LEFT JOIN hremployee_master ON hremployee_master.employeeunkid =  TRM.employeeunkid " & _
                            "		WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                            "   ) AS TERM WHERE TERM.Rno = 1 " & _
                            ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                     "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "		 RET.REmpId " & _
                            "		,RET.termination_to_date " & _
                            "		,RET.REfDt " & _
                            "		,RET.RET_REASON " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            RTD.employeeunkid AS REmpId " & _
                            "           ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                            "           ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                            "			,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                            "       FROM hremployee_dates_tran AS RTD " & _
                            "			LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                            "			LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                            "           RIGHT JOIN hremployee_master ON hremployee_master.employeeunkid =  RTD.employeeunkid " & _
                            "		WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                            "   ) AS RET WHERE RET.Rno = 1 " & _
                            ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

            Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, intCompanyUnkid, strDatabaseName, strEmpIDs, mdtAsOnDate)

            Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable

            Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                          Select New With { _
                            Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                            , Key .employeecode = e.Field(Of String)("employeecode") _
                            , Key .employeename = e.Field(Of String)("employee") _
                            , Key .appointeddate = e.Field(Of String)("appointeddate") _
                            , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                            , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                            , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                            , Key .GId = e.Field(Of Integer)("Id") _
                            , Key .GName = e.Field(Of String)("GName") _
                            , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                            , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                            , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                            })

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing
            Dim dblTotDays As Double = 0
            Dim intPrevEmp As Integer = 0

            For Each drow In result

                Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                Dim EndDate As Date = mdtAsOnDate

                If intPrevEmp <> drow.employeeunkid Then
                    Dim intEmpId As Integer = drow.employeeunkid
                    Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length
                    If intCount <= 0 Then 'No Dates record found                       
                        If drow.termination_from_date.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                        End If
                        If drow.termination_to_date.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                        End If
                        If drow.empl_enddate.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                        End If
                        dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                    ElseIf intCount = 1 Then
                        dblTotDays = drow.ServiceDays
                    Else
                        dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                    End If
                End If

                If mintFromYear > 0 AndAlso Int(dblTotDays / 365) < mintFromYear Then
                    intPrevEmp = drow.employeeunkid
                    Continue For
                End If

                If mintToYear > 0 AndAlso Int(dblTotDays / 365) > mintToYear Then
                    intPrevEmp = drow.employeeunkid
                    Continue For
                End If

                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = drow.employeecode
                rpt_Rows.Item("Column2") = drow.employeename
                rpt_Rows.Item("Column3") = drow.employeeunkid

                If drow.FromDate.Trim <> "" Then
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(drow.FromDate).ToShortDateString
                Else
                    rpt_Rows.Item("Column4") = StartDate.ToShortDateString
                End If

                If drow.ToDate.Trim <> "" Then
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(drow.ToDate).ToShortDateString
                Else
                    rpt_Rows.Item("Column5") = EndDate.ToShortDateString
                End If


                rpt_Rows.Item("Column6") = Int(dblTotDays / 365) 'Total Years
                rpt_Rows.Item("Column7") = Int((dblTotDays Mod 365) / 30) 'Balance Months
                rpt_Rows.Item("Column8") = ((dblTotDays Mod 365) Mod 30) 'Balance Days
                If drow.FromDate.Trim <> "" Then
                    rpt_Rows.Item("Column11") = Int(drow.ServiceDays / 365) 'Years
                    rpt_Rows.Item("Column12") = Int((drow.ServiceDays Mod 365) / 30) 'Months
                    rpt_Rows.Item("Column13") = ((drow.ServiceDays Mod 365) Mod 30) ' Days
                    rpt_Rows.Item("Column14") = drow.ServiceDays 'Days
                Else
                    rpt_Rows.Item("Column11") = Int(dblTotDays / 365) 'Years
                    rpt_Rows.Item("Column12") = Int((dblTotDays Mod 365) / 30) 'Months
                    rpt_Rows.Item("Column13") = ((dblTotDays Mod 365) Mod 30) 'Days
                    rpt_Rows.Item("Column14") = dblTotDays 'Days
                End If
                rpt_Rows.Item("Column15") = dblTotDays 'Total Days
                rpt_Rows.Item("Column81") = dblTotDays 'Total Days

                rpt_Rows.Item("Column9") = drow.GName
                rpt_Rows.Item("Column10") = drow.GId

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                intPrevEmp = drow.employeeunkid
            Next
            rpt_Data.Tables("ArutiTable").AcceptChanges()

            Dim dtTable As DataTable = New DataView(rpt_Data.Tables("ArutiTable"), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
            dtTable.TableName = rpt_Data.Tables(0).TableName
            rpt_Data.Tables.Remove("ArutiTable")
            rpt_Data.Tables.Add(dtTable)


            objRpt = New ArutiReport.Designer.rptEmployeeYearOfServiceReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 21, "Employee Count :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalDays", Language.getMessage(mstrModuleName, 29, "Total Days"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function
    'Sohail (24 Dec 2018) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code :")
            Language.setMessage(mstrModuleName, 2, "Employee :")
            Language.setMessage(mstrModuleName, 3, "Job")
            Language.setMessage(mstrModuleName, 4, "Start Date")
            Language.setMessage(mstrModuleName, 5, "End Date")
            Language.setMessage(mstrModuleName, 6, "Years")
            Language.setMessage(mstrModuleName, 7, "Months")
            Language.setMessage(mstrModuleName, 8, "Days")
            Language.setMessage(mstrModuleName, 9, "Printed By :")
            Language.setMessage(mstrModuleName, 10, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Prepared By :")
            Language.setMessage(mstrModuleName, 13, "Checked By :")
            Language.setMessage(mstrModuleName, 14, "Approved By :")
            Language.setMessage(mstrModuleName, 15, "Received By :")
            Language.setMessage(mstrModuleName, 16, "Job :")
            Language.setMessage(mstrModuleName, 17, "From Year :")
            Language.setMessage(mstrModuleName, 18, "To Year :")
            Language.setMessage(mstrModuleName, 19, "Order By :")
            Language.setMessage(mstrModuleName, 20, "Employee")
            Language.setMessage(mstrModuleName, 21, "Employee Count :")
            Language.setMessage(mstrModuleName, 22, "Allocation")
            Language.setMessage(mstrModuleName, 23, "Allocation :")
            Language.setMessage(mstrModuleName, 24, "Report Based On :")
            Language.setMessage(mstrModuleName, 25, "To")
            Language.setMessage(mstrModuleName, 26, "From Month :")
            Language.setMessage(mstrModuleName, 27, "From Day :")
            Language.setMessage(mstrModuleName, 28, "From Year :")
            Language.setMessage(mstrModuleName, 29, "Total Days")
            Language.setMessage(mstrModuleName, 30, "Employee Name")
            Language.setMessage(mstrModuleName, 31, "Employee Code")
            Language.setMessage(mstrModuleName, 32, "Years")
            Language.setMessage(mstrModuleName, 33, "As On Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsEmployeeDurationReport2
'    Inherits IReportData
'    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeDurationReport"
'    Private mstrReportId As String = enArutiReport.EmployeeDurationReport '110
'    Private objDataOperation As clsDataOperation


'#Region " Constructor "

'    Public Sub New()
'        Me.setReportData(CInt(mstrReportID),intLangId,intCompanyId)
'        Call Create_OnDetailReport()
'    End Sub

'#End Region

'#Region " Private Variables "

'    Private mintFromYear As Integer = 0
'    Private mintToYear As Integer = 0
'    Private mintEmployeeId As Integer = -1
'    Private mstrEmployeeName As String = String.Empty
'    Private mintJobId As Integer = -1
'    Private mstrjobName As String = String.Empty
'    Private mblnIsActive As Boolean = True
'    Private mintViewIndex As Integer = -1
'    Private mstrViewByIds As String = String.Empty
'    Private mstrViewByName As String = String.Empty
'    Private mstrAnalysis_Fields As String = ""
'    Private mstrAnalysis_Join As String = ""
'    Private mstrAnalysis_OrderBy As String = ""
'    Private mstrReport_GroupName As String = ""
'    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
'    Private mintUserUnkid As Integer = -1
'    Private mintCompanyUnkid As Integer = -1
'    Private mstrUserAccessFilter As String = String.Empty
'    'S.SANDEEP [ 01 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintReportTypeId As Integer = 0
'    Private mstrReportTypeName As String = String.Empty
'    Private mintAllocationTypeId As Integer = 1
'    Private mstrAllocationName As String = String.Empty
'    Private mstrSelectedAllocationIds As String = String.Empty
'    Private mstrSelectAlloc As String = String.Empty
'    Private mstrAlloctionJoin As String = String.Empty
'    Private mintAllocYearFrom As Integer = -1
'    Private mintAllocYearTo As Integer = -1
'    Private mintAllocMonthFrom As Integer = -1
'    Private mintAllocMonthTo As Integer = -1
'    Private mintAllocDayFrom As Integer = -1
'    Private mintAllocDayTo As Integer = -1
'    'S.SANDEEP [ 01 FEB 2013 ] -- END

'    'S.SANDEEP [ 22 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mblnFromCurrDate As Boolean = False
'    Private mstrCurrDateCaption As String = String.Empty
'    Private mblnCurrAllocation As Boolean = False
'    Private mstrCurrAllocationCaption As String = String.Empty
'    'S.SANDEEP [ 22 FEB 2013 ] -- END

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrAdvance_Filter As String = String.Empty
'    'S.SANDEEP [ 13 FEB 2013 ] -- END

'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    'ENHANCEMENT : Requested by Rutta
'    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
'    'S.SANDEEP [ 26 MAR 2014 ] -- END

'#End Region

'#Region " Properties "

'    Public WriteOnly Property _EmployeeId() As Integer
'        Set(ByVal value As Integer)
'            mintEmployeeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _EmployeeName() As String
'        Set(ByVal value As String)
'            mstrEmployeeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _JobId() As Integer
'        Set(ByVal value As Integer)
'            mintJobId = value
'        End Set
'    End Property

'    Public WriteOnly Property _JobName() As String
'        Set(ByVal value As String)
'            mstrjobName = value
'        End Set
'    End Property

'    Public WriteOnly Property _FromYear() As Integer
'        Set(ByVal value As Integer)
'            mintFromYear = value
'        End Set
'    End Property

'    Public WriteOnly Property _ToYear() As Integer
'        Set(ByVal value As Integer)
'            mintToYear = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewIndex() As Integer
'        Set(ByVal value As Integer)
'            mintViewIndex = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewByIds() As String
'        Set(ByVal value As String)
'            mstrViewByIds = value
'        End Set
'    End Property

'    Public WriteOnly Property _ViewByName() As String
'        Set(ByVal value As String)
'            mstrViewByName = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_Fields() As String
'        Set(ByVal value As String)
'            mstrAnalysis_Fields = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_Join() As String
'        Set(ByVal value As String)
'            mstrAnalysis_Join = value
'        End Set
'    End Property

'    Public WriteOnly Property _Analysis_OrderBy() As String
'        Set(ByVal value As String)
'            mstrAnalysis_OrderBy = value
'        End Set
'    End Property

'    Public WriteOnly Property _Report_GroupName() As String
'        Set(ByVal value As String)
'            mstrReport_GroupName = value
'        End Set
'    End Property

'    Public WriteOnly Property _IsActive() As Boolean
'        Set(ByVal value As Boolean)
'            mblnIsActive = value
'        End Set
'    End Property

'    Public WriteOnly Property _UserAccessFilter() As String
'        Set(ByVal value As String)
'            mstrUserAccessFilter = value
'        End Set
'    End Property

'    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Get
'            Return Rpt
'        End Get
'    End Property

'    Public WriteOnly Property _CompanyUnkId() As Integer
'        Set(ByVal value As Integer)
'            mintCompanyUnkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _UserUnkId() As Integer
'        Set(ByVal value As Integer)
'            mintUserUnkid = value
'        End Set
'    End Property

'    'S.SANDEEP [ 01 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _ReportTypeId() As Integer
'        Set(ByVal value As Integer)
'            mintReportTypeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReportTypeName() As String
'        Set(ByVal value As String)
'            mstrReportTypeName = value
'        End Set
'    End Property

'    Public WriteOnly Property _AllocationTypeId() As Integer
'        Set(ByVal value As Integer)
'            mintAllocationTypeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _Allocation_Name() As String
'        Set(ByVal value As String)
'            mstrAllocationName = value
'        End Set
'    End Property

'    Public WriteOnly Property _SelectedAllocationIds() As String
'        Set(ByVal value As String)
'            mstrSelectedAllocationIds = value
'        End Set
'    End Property

'    Public WriteOnly Property _SelectAlloc() As String
'        Set(ByVal value As String)
'            mstrSelectAlloc = value
'        End Set
'    End Property

'    Public WriteOnly Property _AlloctionJoin() As String
'        Set(ByVal value As String)
'            mstrAlloctionJoin = value
'        End Set
'    End Property

'    Public WriteOnly Property _Alloc_YearFrom() As Integer
'        Set(ByVal value As Integer)
'            mintAllocYearFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _Alloc_YearTo() As Integer
'        Set(ByVal value As Integer)
'            mintAllocYearTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _Alloc_MonthFrom() As Integer
'        Set(ByVal value As Integer)
'            mintAllocMonthFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _Alloc_MonthTo() As Integer
'        Set(ByVal value As Integer)
'            mintAllocMonthTo = value
'        End Set
'    End Property

'    Public WriteOnly Property _Alloc_DayFrom() As Integer
'        Set(ByVal value As Integer)
'            mintAllocDayFrom = value
'        End Set
'    End Property

'    Public WriteOnly Property _Alloc_DayTo() As Integer
'        Set(ByVal value As Integer)
'            mintAllocDayTo = value
'        End Set
'    End Property
'    'S.SANDEEP [ 01 FEB 2013 ] -- END

'    'S.SANDEEP [ 22 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _Exp_FromCurrDate() As Boolean
'        Set(ByVal value As Boolean)
'            mblnFromCurrDate = value
'        End Set
'    End Property

'    Public WriteOnly Property _CurrDate_Caption() As String
'        Set(ByVal value As String)
'            mstrCurrDateCaption = value
'        End Set
'    End Property

'    Public WriteOnly Property _CurrAllocation() As Boolean
'        Set(ByVal value As Boolean)
'            mblnCurrAllocation = value
'        End Set
'    End Property

'    Public WriteOnly Property _CurrAllocationCaption() As String
'        Set(ByVal value As String)
'            mstrCurrAllocationCaption = value
'        End Set
'    End Property
'    'S.SANDEEP [ 22 FEB 2013 ] -- END

'    'S.SANDEEP [ 13 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public WriteOnly Property _Advance_Filter() As String
'        Set(ByVal value As String)
'            mstrAdvance_Filter = value
'        End Set
'    End Property
'    'S.SANDEEP [ 13 FEB 2013 ] -- END

'    'S.SANDEEP [ 26 MAR 2014 ] -- START
'    Public WriteOnly Property _FirstNamethenSurname() As Boolean
'        Set(ByVal value As Boolean)
'            mblnFirstNamethenSurname = value
'        End Set
'    End Property
'    'S.SANDEEP [ 26 MAR 2014 ] -- END
'#End Region

'#Region "Public Function & Procedures "

'    Public Sub SetDefaultValue()
'        Try
'            mintEmployeeId = 0
'            mstrEmployeeName = String.Empty
'            mintFromYear = 0
'            mintToYear = 0
'            mintJobId = 0
'            mstrjobName = String.Empty
'            mblnIsActive = True
'            mstrUserAccessFilter = ""
'            mintViewIndex = -1
'            mstrViewByIds = ""
'            mstrAnalysis_Fields = ""
'            mstrAnalysis_Join = ""
'            mstrAnalysis_OrderBy = ""
'            'S.SANDEEP [ 01 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mintReportTypeId = 0
'            mstrReportTypeName = String.Empty
'            mintAllocationTypeId = 1
'            mstrAllocationName = String.Empty
'            mstrSelectedAllocationIds = String.Empty
'            mstrSelectAlloc = String.Empty
'            mstrAlloctionJoin = String.Empty
'            mintAllocYearFrom = -1
'            mintAllocYearTo = -1
'            mintAllocMonthFrom = -1
'            mintAllocMonthTo = -1
'            mintAllocDayFrom = -1
'            mintAllocDayTo = -1
'            'S.SANDEEP [ 01 FEB 2013 ] -- END

'            'S.SANDEEP [ 22 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mblnFromCurrDate = False
'            mstrCurrDateCaption = String.Empty
'            mblnCurrAllocation = False
'            mstrCurrAllocationCaption = String.Empty
'            'S.SANDEEP [ 22 FEB 2013 ] -- END

'            'S.SANDEEP [ 13 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mstrAdvance_Filter = ""
'            'S.SANDEEP [ 13 FEB 2013 ] -- END

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [29 JAN 2015] -- START
'    'Private Sub FilterTitleAndFilterQuery()
'    '    Me._FilterQuery = ""
'    '    Me._FilterTitle = ""
'    '    Try

'    '        'Pinkal (27-Jun-2013) -- Start
'    '        'Enhancement : TRA Changes
'    '        objDataOperation.ClearParameters()
'    '        'Pinkal (27-Jun-2013) -- End

'    '        If mblnIsActive = False Then
'    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'    '        End If

'    '        'S.SANDEEP [ 01 FEB 2013 ] -- START
'    '        'ENHANCEMENT : TRA CHANGES
'    '        'If mintEmployeeId > 0 Then
'    '        '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'    '        '    Me._FilterQuery &= " AND hremp_experience_tran.employeeunkid = @EmployeeId "
'    '        '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'    '        'End If

'    '        'If mintJobId > 0 Then
'    '        '    objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'    '        '    Me._FilterQuery &= " AND hremp_experience_tran.jobunkid = @JobId "
'    '        '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'    '        'End If

'    '        'objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'    '        'Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
'    '        'Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

'    '        'If mintToYear > 0 Then
'    '        '    objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'    '        '    Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
'    '        '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'    '        'End If

'    '        'If Me.OrderByQuery <> "" Then
'    '        '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
'    '        '    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
'    '        'End If
'    '        If mintReportTypeId = 0 Then
'    '            'S.SANDEEP [ 22 FEB 2013 ] -- START
'    '            'ENHANCEMENT : TRA CHANGES
'    '            'If mintEmployeeId > 0 Then
'    '            '    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'    '            '    Me._FilterQuery &= " AND hremp_experience_tran.employeeunkid = @EmployeeId "
'    '            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'    '            'End If

'    '            'If mintJobId > 0 Then
'    '            '    objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'    '            '    Me._FilterQuery &= " AND hremp_experience_tran.jobunkid = @JobId "
'    '            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'    '            'End If
'    '            'objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'    '            'Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
'    '            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

'    '            'If mintToYear > 0 Then
'    '            '    objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'    '            '    Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
'    '            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'    '            'End If
'    '            If mblnFromCurrDate = False Then
'    '                If mintEmployeeId > 0 Then
'    '                    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'    '                    Me._FilterQuery &= " AND hremp_experience_tran.employeeunkid = @EmployeeId "
'    '                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'    '                End If

'    '                If mintJobId > 0 Then
'    '                    objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'    '                    Me._FilterQuery &= " AND hremp_experience_tran.jobunkid = @JobId "
'    '                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'    '                End If

'    '                objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'    '                Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

'    '                If mintToYear > 0 Then
'    '                    objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'    '                    Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
'    '                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'    '                End If
'    '            Else
'    '                If mintEmployeeId > 0 Then
'    '                    objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'    '                    Me._FilterQuery &= " AND B.employeeunkid = @EmployeeId "
'    '                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'    '                End If

'    '                If mintJobId > 0 Then
'    '                    objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'    '                    Me._FilterQuery &= " AND B.jobunkid = @JobId "
'    '                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'    '                End If

'    '                objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'    '                objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'    '                Me._FilterQuery &= " AND B.Years BETWEEN @FromYear AND @ToYear "
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'    '            End If
'    '            'S.SANDEEP [ 22 FEB 2013 ] -- END

'    '            If Me.OrderByQuery <> "" Then
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
'    '                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
'    '            End If

'    '        ElseIf mintReportTypeId = 1 Then
'    '            'S.SANDEEP [ 22 FEB 2013 ] -- START
'    '            'ENHANCEMENT : TRA CHANGES
'    '            If mblnCurrAllocation = True Then
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrAllocationCaption & " "
'    '            End If
'    '            'S.SANDEEP [ 22 FEB 2013 ] -- END
'    '            If mintEmployeeId > 0 Then
'    '                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'    '                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'    '            End If

'    '            If mintAllocYearFrom >= 0 AndAlso mintAllocYearTo >= 0 Then
'    '                objDataOperation.AddParameter("@AFYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocYearFrom)
'    '                objDataOperation.AddParameter("@ATYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocYearTo)

'    '                'S.SANDEEP [ 15 OCT 2013 ] -- START
'    '                'ENHANCEMENT : ENHANCEMENT
'    '                'Me._FilterQuery &= " AND ISNULL(((YEAR(ISNULL(hrallocation_tracking.end_date,GETDATE()))*12+MONTH(ISNULL(hrallocation_tracking.end_date,GETDATE()))) - (YEAR(hrallocation_tracking.start_date)*12+MONTH(hrallocation_tracking.start_date)))/12,0) BETWEEN @AFYear AND @ATYear "
'    '                Me._FilterQuery &= " AND ((CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)- CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000) BETWEEN @AFYear AND @ATYear "
'    '                'S.SANDEEP [ 15 OCT 2013 ] -- END


'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "From Year :") & " " & mintAllocYearFrom & _
'    '                                   Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocYearTo & " "
'    '            End If

'    '            If mintAllocMonthFrom >= 0 AndAlso mintAllocMonthTo >= 0 Then
'    '                objDataOperation.AddParameter("@AFMonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocMonthFrom)
'    '                objDataOperation.AddParameter("@ATMonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocMonthTo)

'    '                'S.SANDEEP [ 15 OCT 2013 ] -- START
'    '                'ENHANCEMENT : ENHANCEMENT
'    '                'Me._FilterQuery &= " AND ISNULL(((YEAR(ISNULL(hrallocation_tracking.end_date,GETDATE()))*12+MONTH(ISNULL(hrallocation_tracking.end_date,GETDATE()))) - (YEAR(hrallocation_tracking.start_date)*12+MONTH(hrallocation_tracking.start_date))) - " & _
'    '                '                      " (((((YEAR(ISNULL(hrallocation_tracking.end_date,GETDATE()))*12+MONTH(ISNULL(hrallocation_tracking.end_date,GETDATE()))) - (YEAR(hrallocation_tracking.start_date)*12+MONTH(hrallocation_tracking.start_date))))/12)*12),0) BETWEEN @AFMonth AND @ATMonth "

'    '                Me._FilterQuery &= " AND ((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END) BETWEEN @AFMonth AND @ATMonth "
'    '                'S.SANDEEP [ 15 OCT 2013 ] -- END


'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "From Month :") & " " & mintAllocMonthFrom & _
'    '                                   Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocMonthTo & " "
'    '            End If

'    '            If mintAllocDayFrom >= 0 AndAlso mintAllocDayTo >= 0 Then
'    '                objDataOperation.AddParameter("@AFDay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocDayFrom)
'    '                objDataOperation.AddParameter("@ATDay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocDayTo)

'    '                'S.SANDEEP [ 15 OCT 2013 ] -- START
'    '                'ENHANCEMENT : ENHANCEMENT
'    '                'Me._FilterQuery &= " AND ISNULL(CASE WHEN DAY(hrallocation_tracking.start_date) - DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) <= 0 THEN DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) - DAY(hrallocation_tracking.start_date) ELSE DAY(hrallocation_tracking.start_date) - DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) END,0) BETWEEN @AFDay AND @ATDay "
'    '                Me._FilterQuery &= " AND (DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),ISNULL(hrallocation_tracking.end_date,GETDATE()))) BETWEEN @AFDay AND @ATDay "
'    '                'S.SANDEEP [ 15 OCT 2013 ] -- END

'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "From Day :") & " " & mintAllocDayFrom & _
'    '                                   Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocDayTo & " "
'    '            End If


'    '            If mintAllocationTypeId > 0 Then
'    '                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Allocation :") & " " & mstrAllocationName & " "
'    '            End If
'    '        End If
'    '        'S.SANDEEP [ 01 FEB 2013 ] -- END

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
'    '    End Try
'    'End Sub

'    Private Sub FilterTitleAndFilterQuery()
'        Me._FilterQuery = ""
'        Me._FilterTitle = ""
'        Try
'            objDataOperation.ClearParameters()

'            If mblnIsActive = False Then
'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            End If

'            Select Case mintReportTypeId
'                Case 0  'EMPLOYEE JOBS DURATION
'                    If mintEmployeeId > 0 Then
'                        objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                        Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'                    End If

'                    If mintJobId > 0 Then
'                        objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'                        Me._FilterQuery &= " AND JB.jobunkid = @JobId "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'                    End If

'                    If mblnFromCurrDate = False Then
'                        objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'                        Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

'                        If mintToYear > 0 Then
'                            objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'                            Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
'                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'                        End If
'                    ElseIf mblnFromCurrDate = True Then
'                        objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'                        objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)

'                        Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) BETWEEN  @FromYear AND @ToYear "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'                    End If

'                    If Me.OrderByQuery <> "" Then
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
'                        Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
'                    End If

'                Case 1  'ALLOCATION DURATION
'                    If mblnCurrAllocation = True Then
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrAllocationCaption & " "
'                    End If

'                    If mintEmployeeId > 0 Then
'                        objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                        Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'                    End If

'                    If mintAllocYearFrom >= 0 AndAlso mintAllocYearTo >= 0 Then
'                        objDataOperation.AddParameter("@AFYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocYearFrom)
'                        objDataOperation.AddParameter("@ATYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocYearTo)
'                        Me._FilterQuery &= " AND ((CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)- CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000) BETWEEN @AFYear AND @ATYear "

'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "From Year :") & " " & mintAllocYearFrom & _
'                                           Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocYearTo & " "
'                    End If

'                    If mintAllocMonthFrom >= 0 AndAlso mintAllocMonthTo >= 0 Then
'                        objDataOperation.AddParameter("@AFMonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocMonthFrom)
'                        objDataOperation.AddParameter("@ATMonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocMonthTo)

'                        Me._FilterQuery &= " AND ((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END) BETWEEN @AFMonth AND @ATMonth "

'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "From Month :") & " " & mintAllocMonthFrom & _
'                                           Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocMonthTo & " "
'                    End If

'                    If mintAllocDayFrom >= 0 AndAlso mintAllocDayTo >= 0 Then
'                        objDataOperation.AddParameter("@AFDay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocDayFrom)
'                        objDataOperation.AddParameter("@ATDay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocDayTo)

'                        Me._FilterQuery &= " AND (DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),ISNULL(hrallocation_tracking.end_date,GETDATE()))) BETWEEN @AFDay AND @ATDay "

'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "From Day :") & " " & mintAllocDayFrom & _
'                                           Language.getMessage(mstrModuleName, 25, "To") & " " & mintAllocDayTo & " "
'                    End If


'                    If mintAllocationTypeId > 0 Then
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Allocation :") & " " & mstrAllocationName & " "
'                    End If
'                Case 2  'EMPLOYEE EXPERIENCE DURATION
'                    If mblnFromCurrDate = False Then
'                        If mintEmployeeId > 0 Then
'                            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                            Me._FilterQuery &= " AND hremp_experience_tran.employeeunkid = @EmployeeId "
'                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'                        End If

'                        If mintJobId > 0 Then
'                            objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'                            Me._FilterQuery &= " AND hremp_experience_tran.jobunkid = @JobId "
'                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'                        End If

'                        objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'                        Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) >= @FromYear "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "

'                        If mintToYear > 0 Then
'                            objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'                            Me._FilterQuery &= " AND ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) <= @ToYear "
'                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'                        End If
'                    Else
'                        If mintEmployeeId > 0 Then
'                            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                            Me._FilterQuery &= " AND B.employeeunkid = @EmployeeId "
'                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
'                        End If

'                        If mintJobId > 0 Then
'                            objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
'                            Me._FilterQuery &= " AND B.jobunkid = @JobId "
'                            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Job :") & " " & mstrEmployeeName & " "
'                        End If

'                        objDataOperation.AddParameter("@FromYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromYear)
'                        objDataOperation.AddParameter("@ToYear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToYear)
'                        Me._FilterQuery &= " AND B.Years BETWEEN @FromYear AND @ToYear "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Report Based On : ") & " " & mstrCurrDateCaption & " "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "From Year :") & " " & mintFromYear & " "
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "To Year :") & " " & mintToYear & " "
'                    End If

'                    If Me.OrderByQuery <> "" Then
'                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Order By :") & " " & Me.OrderByDisplay & " "
'                        Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
'                    End If
'            End Select
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [29 JAN 2015] -- END



'    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim strReportExportFile As String = ""
'        Try

'            If mintCompanyUnkid <= 0 Then
'                mintCompanyUnkid = Company._Object._Companyunkid
'            End If

'            Company._Object._Companyunkid = mintCompanyUnkid
'            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

'            If mintUserUnkid <= 0 Then
'                mintUserUnkid = User._Object._Userunkid
'            End If

'            User._Object._Userunkid = mintUserUnkid

'            'S.SANDEEP [ 01 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Select Case pintReportType
'                'S.SANDEEP [29 JAN 2015] -- START
'                'Case 0
'                '    
'                'Case 1
'                '    objRpt = Generate_AllocationReport()
'                Case 0  'EMPLOYEE JOBS
'                    objRpt = Generate_JobTitleReport()
'                Case 1  'ALLOCATION
'                    objRpt = Generate_AllocationReport()
'                Case 2  'EXPERIENCE
'                    objRpt = Generate_DetailReport()
'                    'S.SANDEEP [29 JAN 2015] -- END
'            End Select
'            'S.SANDEEP [ 01 FEB 2013 ] -- END

'            Rpt = objRpt

'            If Not IsNothing(objRpt) Then
'                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
'        OrderByDisplay = ""
'        OrderByQuery = ""
'        Try
'            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
'            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
'        Try
'            Call OrderByExecute(iColumn_DetailReport)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 22 FEB 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES


'    'S.SANDEEP [29 JAN 2015] -- START
'    'Public Sub Set_Sort_Collection(ByVal blnIsCurrDate As Boolean)
'    '    Try
'    '        iColumn_DetailReport.Clear()
'    '        If blnIsCurrDate = False Then
'    '            'S.SANDEEP [ 26 MAR 2014 ] -- START
'    '            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'    '            If mblnFirstNamethenSurname = False Then
'    '                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'    '            Else
'    '                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'    '            End If
'    '            'S.SANDEEP [ 26 MAR 2014 ] -- END

'    '            iColumn_DetailReport.Add(New IColumn("start_date", Language.getMessage(mstrModuleName, 4, "Start Date")))
'    '            iColumn_DetailReport.Add(New IColumn("end_date", Language.getMessage(mstrModuleName, 5, "End Date")))
'    '            iColumn_DetailReport.Add(New IColumn("ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0)", Language.getMessage(mstrModuleName, 6, "Years")))
'    '        Else
'    '            iColumn_DetailReport.Add(New IColumn("B.Ename", Language.getMessage(mstrModuleName, 20, "Employee")))
'    '            iColumn_DetailReport.Add(New IColumn("B.auditdatetime", Language.getMessage(mstrModuleName, 4, "Start Date")))
'    '            iColumn_DetailReport.Add(New IColumn("GETDATE()", Language.getMessage(mstrModuleName, 5, "End Date")))
'    '            iColumn_DetailReport.Add(New IColumn("Years", Language.getMessage(mstrModuleName, 6, "Years")))
'    '        End If
'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: Set_Sort_Collection; Module Name: " & mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Sub

'    Public Sub Set_Sort_Collection(ByVal blnIsCurrDate As Boolean)
'        Try
'            iColumn_DetailReport.Clear()
'            If blnIsCurrDate = False Then
'                If mblnFirstNamethenSurname = False Then
'                    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'                Else
'                    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'                End If
'                iColumn_DetailReport.Add(New IColumn("start_date", Language.getMessage(mstrModuleName, 4, "Start Date")))
'                iColumn_DetailReport.Add(New IColumn("end_date", Language.getMessage(mstrModuleName, 5, "End Date")))
'                iColumn_DetailReport.Add(New IColumn("ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0)", Language.getMessage(mstrModuleName, 6, "Years")))
'            Else
'                Select Case mintReportTypeId
'                    Case 0  'EMPLOYEE JOB
'                        If mblnFirstNamethenSurname = False Then
'                            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'                        Else
'                            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 20, "Employee")))
'                        End If
'                        iColumn_DetailReport.Add(New IColumn("start_date", Language.getMessage(mstrModuleName, 4, "Start Date")))
'                        iColumn_DetailReport.Add(New IColumn("end_date", Language.getMessage(mstrModuleName, 5, "End Date")))
'                        iColumn_DetailReport.Add(New IColumn("ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0)", Language.getMessage(mstrModuleName, 6, "Years")))
'                    Case 2  'EMPLOYEE EXPERIENCE
'                        iColumn_DetailReport.Add(New IColumn("B.Ename", Language.getMessage(mstrModuleName, 20, "Employee")))
'                        iColumn_DetailReport.Add(New IColumn("B.auditdatetime", Language.getMessage(mstrModuleName, 4, "Start Date")))
'                        iColumn_DetailReport.Add(New IColumn("GETDATE()", Language.getMessage(mstrModuleName, 5, "End Date")))
'                        iColumn_DetailReport.Add(New IColumn("Years", Language.getMessage(mstrModuleName, 6, "Years")))
'                End Select
'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Set_Sort_Collection; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [29 JAN 2015] -- END


'    'S.SANDEEP [ 22 FEB 2013 ] -- END

'#End Region

'#Region " Report Generation "
'    Dim iColumn_DetailReport As New IColumnCollection

'    Public Property Field_OnDetailReport() As IColumnCollection
'        Get
'            Return iColumn_DetailReport
'        End Get
'        Set(ByVal value As IColumnCollection)
'            iColumn_DetailReport = value
'        End Set
'    End Property

'    Private Sub Create_OnDetailReport()
'        Try
'            Call Set_Sort_Collection(False)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
'        End Try
'    End Sub

'    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim blnFlag As Boolean = False
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Try
'            objDataOperation = New clsDataOperation

'            'S.SANDEEP [ 22 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = " SELECT  " & _
'            '       " ISNULL(hremployee_master.employeecode, '') employeecode " & _
'            '       ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee " & _
'            '       ",ISNULL(jb.job_name,'' ) Job " & _
'            '       ",start_date " & _
'            '       ",end_date " & _
'            '       ",ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) AS Years " & _
'            '       ",ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date))) - (((((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date))))/12)*12),0) AS Months  " & _
'            '       ",ISNULL(CASE WHEN DAY(start_date) - DAY(end_date) <= 0 THEN DAY(end_date) - DAY(start_date) ELSE DAY(start_date) - DAY(end_date) END,0) AS Days "

'            'If mintViewIndex > 0 Then
'            '    StrQ &= mstrAnalysis_Fields
'            'Else
'            '    StrQ &= ", 0 AS Id, '' AS GName "
'            'End If

'            'StrQ &= " FROM hremp_experience_tran " & _
'            '             " JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
'            '             " LEFT JOIN hrjob_master jb ON hremployee_master.jobunkid = jb.jobunkid "


'            'StrQ &= mstrAnalysis_Join

'            'StrQ &= " WHERE 1 = 1 "

'            'If mblnIsActive = False Then
'            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'            'End If


'            'If mstrUserAccessFilter = "" Then
'            '    If UserAccessLevel._AccessLevel.Length > 0 Then
'            '        StrQ &= UserAccessLevel._AccessLevelFilterString
'            '    End If
'            'Else
'            '    StrQ &= mstrUserAccessFilter
'            'End If
'            If mblnFromCurrDate = False Then

'                'S.SANDEEP [ 22 MAR 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'StrQ = " SELECT  " & _
'                '       " ISNULL(hremployee_master.employeecode, '') employeecode " & _
'                '       ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee " & _
'                '       ",ISNULL(jb.job_name,'' ) Job " & _
'                '       ",start_date " & _
'                '       ",end_date " & _
'                '       ",CONVERT(CHAR(8),start_date,112) AS SDate " & _
'                '       ",CONVERT(CHAR(8),end_date,112) AS EDate " & _
'                '       ",ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) AS Years " & _
'                '       ",ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date))) - (((((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date))))/12)*12),0) AS Months  " & _
'                '       ",ISNULL(CASE WHEN DAY(start_date) - DAY(end_date) <= 0 THEN DAY(end_date) - DAY(start_date) ELSE DAY(start_date) - DAY(end_date) END,0) AS Days "

'                'S.SANDEEP [ 15 OCT 2013 ] -- START
'                'ENHANCEMENT : ENHANCEMENT
'                'StrQ = " SELECT  " & _
'                '       " ISNULL(hremployee_master.employeecode, '') employeecode " & _
'                '       ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee " & _
'                '       ",ISNULL(hremp_experience_tran.old_job, '') Job " & _
'                '       ",start_date " & _
'                '       ",end_date " & _
'                '       ",CONVERT(CHAR(8),start_date,112) AS SDate " & _
'                '       ",CONVERT(CHAR(8),end_date,112) AS EDate " & _
'                '       ",ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) AS Years " & _
'                '       ",ISNULL(((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date))) - (((((YEAR(end_date)*12+MONTH(end_date)) - (YEAR(start_date)*12+MONTH(start_date))))/12)*12),0) AS Months  " & _
'                '       ",ISNULL(CASE WHEN DAY(start_date) - DAY(end_date) <= 0 THEN DAY(end_date) - DAY(start_date) ELSE DAY(start_date) - DAY(end_date) END,0) AS Days "
'                StrQ = " SELECT  " & _
'                              " ISNULL(hremployee_master.employeecode, '') employeecode "
'                'S.SANDEEP [ 26 MAR 2014 ] -- START
'                '",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee " & _
'                If mblnFirstNamethenSurname = False Then
'                    StrQ &= ",ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') employee "
'                Else
'                    StrQ &= ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee "
'                End If
'                'S.SANDEEP [ 26 MAR 2014 ] -- END

'                StrQ &= ",ISNULL(hremp_experience_tran.old_job, '') Job " & _
'                      ",start_date " & _
'                      ",end_date " & _
'                       ",CONVERT(CHAR(8),start_date,112) AS SDate " & _
'                       ",CONVERT(CHAR(8),end_date,112) AS EDate " & _
'                       ",(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
'                       ",(DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END AS Months " & _
'                       ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),end_date) AS Days "
'                'S.SANDEEP [ 15 OCT 2013 ] -- END

'                'S.SANDEEP [ 22 MAR 2013 ] -- END


'                If mintViewIndex > 0 Then
'                    StrQ &= mstrAnalysis_Fields
'                Else
'                    StrQ &= ", 0 AS Id, '' AS GName "
'                End If

'                'S.SANDEEP [ 23 MAR 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'StrQ &= " FROM hremp_experience_tran " & _
'                '             " JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                '             " LEFT JOIN hrjob_master jb ON hremployee_master.jobunkid = jb.jobunkid "

'                StrQ &= " FROM hremp_experience_tran " & _
'                            " JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid "
'                'S.SANDEEP [ 23 MAR 2013 ] -- END




'                StrQ &= mstrAnalysis_Join

'                StrQ &= " WHERE 1 = 1 "

'                If mblnIsActive = False Then
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                End If

'                'S.SANDEEP [ 13 FEB 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If mstrAdvance_Filter.Trim.Length > 0 Then
'                    StrQ &= " AND " & mstrAdvance_Filter
'                End If
'                'S.SANDEEP [ 13 FEB 2013 ] -- END

'                If mstrUserAccessFilter = "" Then
'                    If UserAccessLevel._AccessLevel.Length > 0 Then
'                        StrQ &= UserAccessLevel._AccessLevelFilterString
'                    End If
'                Else
'                    StrQ &= mstrUserAccessFilter
'                End If

'            Else
'                'S.SANDEEP [ 23 MAR 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'StrQ = "SELECT " & _
'                '       " Ecode AS employeecode " & _
'                '       ",Ename AS employee " & _
'                '       ",Job AS Job " & _
'                '       ",start_date AS SDate " & _
'                '       ",end_date AS EDate " & _
'                '       ",Years AS Years " & _
'                '       ",Months AS Months " & _
'                '       ",Days AS Days " & _
'                '       ",Id AS Id " & _
'                '       ",GName AS GName " & _
'                '    "FROM " & _
'                '    "( " & _
'                '         "SELECT " & _
'                '               "athremployee_master.employeeunkid " & _
'                '              ",athremployee_master.jobunkid " & _
'                '              ",auditdatetime " & _
'                '              ",job.employeecode AS Ecode " & _
'                '              ",job.firstname+' '+ job.othername + ' ' + job.surname AS Ename " & _
'                '              ",job.job_name AS Job " & _
'                '              ",CONVERT(CHAR(8),auditdatetime,112) AS start_date " & _
'                '              ",CONVERT(CHAR(8),GETDATE(),112) AS end_date " & _
'                '              ",ROW_NUMBER() OVER(PARTITION BY dbo.athremployee_master.employeeunkid,job.jobunkid ORDER BY auditdatetime ASC) AS Rno " & _
'                '              ",ISNULL(((YEAR(GETDATE())*12+MONTH(GETDATE())) - (YEAR(auditdatetime)*12+MONTH(auditdatetime)))/12,0) AS Years " & _
'                '              ",ISNULL(((YEAR(GETDATE())*12+MONTH(GETDATE())) - (YEAR(auditdatetime)*12+MONTH(auditdatetime))) - (((((YEAR(GETDATE())*12+MONTH(GETDATE())) - (YEAR(auditdatetime)*12+MONTH(auditdatetime))))/12)*12),0) AS Months " & _
'                '              ",ISNULL(CASE WHEN DAY(auditdatetime) - DAY(GETDATE()) <= 0 THEN DAY(GETDATE()) - DAY(auditdatetime) ELSE DAY(auditdatetime) - DAY(GETDATE()) END,0) AS Days " & _
'                '              ",Id AS Id " & _
'                '              ",GName AS GName " & _
'                '         "FROM athremployee_master " & _
'                '         "JOIN " & _
'                '         "( " & _
'                '              "SELECT " & _
'                '                    "employeeunkid " & _
'                '                   ",hremployee_master.jobunkid AS jobunkid " & _
'                '                   ",ISNULL(hremployee_master.firstname,'') AS firstname " & _
'                '                   ",ISNULL(hremployee_master.surname,'') AS surname " & _
'                '                   ",ISNULL(hremployee_master.othername,'') AS othername " & _
'                '                   ",jb.job_name AS job_name " & _
'                '                   ",hremployee_master.employeecode AS employeecode "


'                'If mintViewIndex > 0 Then
'                '    StrQ &= mstrAnalysis_Fields
'                'Else
'                '    StrQ &= ", 0 AS Id, '' AS GName "
'                'End If

'                'StrQ &= " FROM hremployee_master " & _
'                '        "   JOIN hrjob_master  AS jb ON hremployee_master.jobunkid = jb.jobunkid "


'                'StrQ &= mstrAnalysis_Join

'                'StrQ &= "WHERE 1 = 1 "

'                'If mblnIsActive = False Then
'                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                'End If

'                'If mstrUserAccessFilter = "" Then
'                '    If UserAccessLevel._AccessLevel.Length > 0 Then
'                '        StrQ &= UserAccessLevel._AccessLevelFilterString
'                '    End If
'                'Else
'                '    StrQ &= mstrUserAccessFilter
'                'End If
'                'StrQ &= ") AS Job ON athremployee_master.employeeunkid = Job.employeeunkid AND athremployee_master.jobunkid = Job.jobunkid "
'                'StrQ &= ") AS B " & _
'                '    "WHERE Rno = 1 "

'                'S.SANDEEP [ 15 OCT 2013 ] -- START
'                'ENHANCEMENT : ENHANCEMENT
'                'StrQ = "SELECT " & _
'                '       " Ecode AS employeecode " & _
'                '       ",Ename AS employee " & _
'                '       ",Job AS Job " & _
'                '       ",start_date AS SDate " & _
'                '       ",end_date AS EDate " & _
'                '       ",Years AS Years " & _
'                '       ",Months AS Months " & _
'                '       ",Days AS Days " & _
'                '       ",Id AS Id " & _
'                '       ",GName AS GName " & _
'                '    "FROM " & _
'                '    "( " & _
'                '         "SELECT " & _
'                '             "   hremployee_master.employeecode AS Ecode " & _
'                '             "  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
'                '             "  ,CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS start_date " & _
'                '             "  ,CONVERT(CHAR(8),GETDATE(),112) AS end_date " & _
'                '             "  ,ISNULL(((YEAR(GETDATE())*12+MONTH(GETDATE())) - (YEAR(start_date)*12+MONTH(start_date)))/12,0) AS Years " & _
'                '             "  ,ISNULL(((YEAR(GETDATE())*12+MONTH(GETDATE())) - (YEAR(start_date)*12+MONTH(start_date))) - (((((YEAR(GETDATE())*12+MONTH(GETDATE())) - (YEAR(start_date)*12+MONTH(start_date))))/12)*12),0) AS Months " & _
'                '             "  ,ISNULL(CASE WHEN DAY(start_date) - DAY(GETDATE()) <= 0 THEN DAY(GETDATE()) - DAY(start_date) ELSE DAY(start_date) - DAY(GETDATE()) END,0) AS Days " & _
'                '             "  ,hremployee_master.employeeunkid " & _
'                '             "  ,hremployee_master.jobunkid " & _
'                '             "  ,old_job AS Job " & _
'                '             "  ,ROW_NUMBER()OVER (PARTITION BY hremployee_master.employeeunkid ORDER BY start_date DESC) AS Rno "
'                StrQ = "SELECT " & _
'                       " Ecode AS employeecode " & _
'                       ",Ename AS employee " & _
'                       ",Job AS Job " & _
'                       ",start_date AS SDate " & _
'                       ",end_date AS EDate " & _
'                       ",Years AS Years " & _
'                       ",Months AS Months " & _
'                       ",Days AS Days " & _
'                       ",Id AS Id " & _
'                       ",GName AS GName " & _
'                    "FROM " & _
'                    "( " & _
'                         "SELECT " & _
'                             "   hremployee_master.employeecode AS Ecode "
'                'S.SANDEEP [ 26 MAR 2014 ] -- START
'                '"  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
'                If mblnFirstNamethenSurname = False Then
'                    StrQ &= "  ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS Ename "
'                Else
'                    StrQ &= "  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename "
'                End If
'                'S.SANDEEP [ 26 MAR 2014 ] -- END
'                StrQ &= "  ,CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS start_date " & _
'                              ",CONVERT(CHAR(8),GETDATE(),112) AS end_date " & _
'                             "  ,(CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
'                             "  ,(DATEDIFF(MONTH,start_date,GETDATE())+12)%12 - CASE WHEN DAY(start_date)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
'                             "  ,DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,GETDATE())+12)%12 - CASE WHEN DAY(start_date)>DAY(GETDATE()) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),GETDATE()) AS Days " & _
'                             "  ,hremployee_master.employeeunkid " & _
'                             "  ,hremployee_master.jobunkid " & _
'                             "  ,old_job AS Job " & _
'                             "  ,ROW_NUMBER()OVER (PARTITION BY hremployee_master.employeeunkid ORDER BY start_date DESC) AS Rno "
'                'S.SANDEEP [ 15 OCT 2013 ] -- END



'                If mintViewIndex > 0 Then
'                    StrQ &= mstrAnalysis_Fields
'                Else
'                    StrQ &= ", 0 AS Id, '' AS GName "
'                End If
'                StrQ &= "FROM hremp_experience_tran " & _
'                        " JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid "
'                StrQ &= mstrAnalysis_Join

'                StrQ &= " WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 AND end_date IS NULL "

'                If mblnIsActive = False Then
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                End If

'                'S.SANDEEP [ 13 FEB 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If mstrAdvance_Filter.Trim.Length > 0 Then
'                    StrQ &= " AND " & mstrAdvance_Filter
'                End If
'                'S.SANDEEP [ 13 FEB 2013 ] -- END

'                If mstrUserAccessFilter = "" Then
'                    If UserAccessLevel._AccessLevel.Length > 0 Then
'                        StrQ &= UserAccessLevel._AccessLevelFilterString
'                    End If
'                Else
'                    StrQ &= mstrUserAccessFilter
'                End If

'                StrQ &= " ) AS B WHERE Rno = 1 AND Ecode IS NOT NULL "
'                'S.SANDEEP [ 23 MAR 2013 ] -- END

'            End If
'            'S.SANDEEP [ 22 FEB 2013 ] -- END

'            Call FilterTitleAndFilterQuery()
'            StrQ &= Me._FilterQuery

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport
'            Dim rpt_Rows As DataRow = Nothing
'            For Each dFRow As DataRow In dsList.Tables(0).Rows
'                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
'                rpt_Rows.Item("Column1") = dFRow.Item("employeecode")
'                rpt_Rows.Item("Column2") = dFRow.Item("employee")
'                rpt_Rows.Item("Column3") = dFRow.Item("Job")

'                'S.SANDEEP [ 22 FEB 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If Not IsDBNull(dFRow.Item("start_date")) Then
'                '    rpt_Rows.Item("Column4") = CDate(dFRow.Item("start_date").ToString).ToShortDateString
'                'Else
'                '    rpt_Rows.Item("Column4") = DBNull.Value
'                'End If

'                'If Not IsDBNull(dFRow.Item("end_date")) Then
'                '    rpt_Rows.Item("Column5") = CDate(dFRow.Item("end_date").ToString).ToShortDateString
'                'Else
'                '    rpt_Rows.Item("Column5") = DBNull.Value
'                'End If

'                If dFRow.Item("SDate").ToString.Trim.Length > 0 Then
'                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dFRow.Item("SDate").ToString).ToShortDateString
'                Else
'                    rpt_Rows.Item("Column4") = DBNull.Value
'                End If
'                If mblnFromCurrDate = True Then
'                    rpt_Rows.Item("Column5") = ""
'                Else
'                    If dFRow.Item("EDate").ToString.Trim.Trim.Length > 0 Then
'                        rpt_Rows.Item("Column5") = eZeeDate.convertDate(dFRow.Item("EDate").ToString).ToShortDateString
'                    Else
'                        rpt_Rows.Item("Column5") = DBNull.Value
'                    End If
'                End If
'                'S.SANDEEP [ 22 FEB 2013 ] -- END



'                rpt_Rows.Item("Column6") = dFRow.Item("Years")
'                rpt_Rows.Item("Column7") = dFRow.Item("Months")
'                rpt_Rows.Item("Column8") = dFRow.Item("Days")
'                rpt_Rows.Item("Column9") = dFRow.Item("GName")
'                rpt_Rows.Item("Column10") = dFRow.Item("Id")
'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

'            Next

'            objRpt = New ArutiReport.Designer.rptEmployeeDurationReport

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription", _
'                                        ConfigParameter._Object._GetLeftMargin, _
'                                        ConfigParameter._Object._GetRightMargin)

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If

'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If

'            If mintViewIndex <= 0 Then
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
'            End If


'            objRpt.SetDataSource(rpt_Data)

'            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
'            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee :"))
'            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
'            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
'            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
'            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
'            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
'            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))
'            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 21, "Employee Count :"))

'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

'            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

'            Return objRpt

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Private Function Generate_AllocationReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim blnFlag As Boolean = False
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Try
'            objDataOperation = New clsDataOperation

'            'Pinkal (27-Jun-2013) -- Start
'            'Enhancement : TRA Changes

'            Dim objCompnay As New clsCompany_Master

'            If mintCompanyUnkid <= 0 Then
'                mintCompanyUnkid = Company._Object._Companyunkid
'            End If

'            Dim dsCompList As DataSet = objCompnay.GetFinancialYearList(mintCompanyUnkid)

'            For i As Integer = 0 To dsCompList.Tables(0).Rows.Count - 1

'                StrQ &= "SELECT " & _
'                      " hremployee_master.employeecode AS ECode "
'                'S.SANDEEP [ 26 MAR 2014 ] -- START
'                '",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ ISNULL(hremployee_master.surname,'') AS EName "
'                If mblnFirstNamethenSurname = False Then
'                    StrQ &= "  ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS Ename "
'                Else
'                    StrQ &= "  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename "
'                End If
'                'S.SANDEEP [ 26 MAR 2014 ] -- END


'                'StrQ &= mstrSelectAlloc
'                StrQ &= mstrSelectAlloc.Replace("ISNULL(", "ISNULL(" & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..")

'                'S.SANDEEP [ 15 OCT 2013 ] -- START
'                'ENHANCEMENT : ENHANCEMENT
'                'StrQ &= ",CONVERT(CHAR(8),hrallocation_tracking.start_date,112) AS SDate " & _
'                '        ",ISNULL(CONVERT(CHAR(8),hrallocation_tracking.end_date,112),'') AS EDate " & _
'                '        ",ISNULL(((YEAR(ISNULL(hrallocation_tracking.end_date,GETDATE()))*12+MONTH(ISNULL(hrallocation_tracking.end_date,GETDATE()))) - (YEAR(hrallocation_tracking.start_date)*12+MONTH(hrallocation_tracking.start_date)))/12,0) AS Years " & _
'                '        ",ISNULL(((YEAR(ISNULL(hrallocation_tracking.end_date,GETDATE()))*12+MONTH(ISNULL(hrallocation_tracking.end_date,GETDATE()))) - (YEAR(hrallocation_tracking.start_date)*12+MONTH(hrallocation_tracking.start_date))) - " & _
'                '        " (((((YEAR(ISNULL(hrallocation_tracking.end_date,GETDATE()))*12+MONTH(ISNULL(hrallocation_tracking.end_date,GETDATE()))) - (YEAR(hrallocation_tracking.start_date)*12+MONTH(hrallocation_tracking.start_date))))/12)*12),0) AS Months " & _
'                '        ",ISNULL(CASE WHEN DAY(hrallocation_tracking.start_date) - DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) <= 0 THEN DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) - DAY(hrallocation_tracking.start_date) ELSE DAY(hrallocation_tracking.start_date) - DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) END,0) AS Days " & _
'                '        "FROM  " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..hrallocation_tracking " & _
'                '        "JOIN " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..hremployee_master ON hrallocation_tracking.employeeunkid = hremployee_master.employeeunkid "


'                StrQ &= ",CONVERT(CHAR(8),hrallocation_tracking.start_date,112) AS SDate " & _
'                          ",ISNULL(CONVERT(CHAR(8),hrallocation_tracking.end_date,112),'') AS EDate " & _
'                            ",(CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)- CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
'                            ",(DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END AS Months " & _
'                            ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,ISNULL(hrallocation_tracking.end_date,GETDATE()))+12)%12 - CASE WHEN DAY(start_date)>DAY(ISNULL(hrallocation_tracking.end_date,GETDATE())) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),ISNULL(hrallocation_tracking.end_date,GETDATE()),112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),ISNULL(hrallocation_tracking.end_date,GETDATE())) AS Days " & _
'                                 "FROM  " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..hrallocation_tracking " & _
'                                 "JOIN " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..hremployee_master ON hrallocation_tracking.employeeunkid = hremployee_master.employeeunkid "
'                'S.SANDEEP [ 15 OCT 2013 ] -- END

'                'StrQ &= mstrAlloctionJoin
'                StrQ &= mstrAlloctionJoin.Replace("JOIN", "JOIN " & dsCompList.Tables(0).Rows(i)("database_name").ToString() & "..")

'                StrQ &= "WHERE allocationtypeid = '" & mintAllocationTypeId & "' "

'                If mstrSelectedAllocationIds.Trim.Length > 0 Then StrQ &= mstrSelectedAllocationIds

'                If mblnIsActive = False Then
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                End If

'                'S.SANDEEP [ 13 FEB 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If mstrAdvance_Filter.Trim.Length > 0 Then
'                    StrQ &= " AND " & mstrAdvance_Filter
'                End If
'                'S.SANDEEP [ 13 FEB 2013 ] -- END

'                'S.SANDEEP [ 22 FEB 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If mblnCurrAllocation = True Then
'                    StrQ &= " AND end_date IS NULL "
'                End If
'                'S.SANDEEP [ 22 FEB 2013 ] -- END

'                If mstrUserAccessFilter = "" Then
'                    If UserAccessLevel._AccessLevel.Length > 0 Then
'                        StrQ &= UserAccessLevel._AccessLevelFilterString
'                    End If
'                Else
'                    StrQ &= mstrUserAccessFilter
'                End If

'                Call FilterTitleAndFilterQuery()

'                StrQ &= Me._FilterQuery

'                If i <> dsCompList.Tables(0).Rows.Count - 1 Then
'                    StrQ &= " UNION "
'                End If

'            Next

'            'Pinkal (27-Jun-2013) -- End


'            StrQ &= " ORDER BY employeecode "


'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport
'            Dim rpt_Rows As DataRow = Nothing
'            For Each dFRow As DataRow In dsList.Tables(0).Rows
'                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

'                rpt_Rows.Item("Column1") = dFRow.Item("ECode")
'                rpt_Rows.Item("Column2") = dFRow.Item("EName")
'                rpt_Rows.Item("Column3") = dFRow.Item("Allocation")
'                rpt_Rows.Item("Column4") = eZeeDate.convertDate(dFRow.Item("SDate").ToString).ToShortDateString
'                If dFRow.Item("EDate").ToString.Trim.Length > 0 Then
'                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dFRow.Item("EDate").ToString).ToShortDateString
'                End If
'                rpt_Rows.Item("Column6") = dFRow.Item("Years")
'                rpt_Rows.Item("Column7") = dFRow.Item("Months")
'                rpt_Rows.Item("Column8") = dFRow.Item("Days")

'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
'            Next

'            objRpt = New ArutiReport.Designer.rptEmpAllocDuration

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription", _
'                                        ConfigParameter._Object._GetLeftMargin, _
'                                        ConfigParameter._Object._GetRightMargin)

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If

'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If

'            objRpt.SetDataSource(rpt_Data)

'            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
'            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 2, "Employee :"))
'            Call ReportFunction.TextChange(objRpt, "txtAllocation", Language.getMessage(mstrModuleName, 22, "Allocation"))
'            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
'            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
'            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
'            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
'            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))

'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

'            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName & " - [" & mstrAllocationName & "] ")
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Return objRpt

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_AllocationReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    'S.SANDEEP [29 JAN 2015] -- START
'    Private Function Generate_JobTitleReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
'        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim blnFlag As Boolean = False
'        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
'        Try
'            objDataOperation = New clsDataOperation

'            If mblnFromCurrDate = False Then
'                StrQ = "DECLARE @EJ AS TABLE(id INT IDENTITY,empid INT,jobid INT DEFAULT 0,start_date DATETIME,end_date DATETIME DEFAULT GETDATE()) " & _
'                        "DECLARE @nxtJId INT,@empid INT,@jbid INT,@date DATETIME,@Id INT " & _
'                        "SET @nxtJId = 0 " & _
'                        "SET @Id = 0 " & _
'                        "   DECLARE EJ CURSOR FOR SELECT A.atempid,A.atjobid ,A.atdate FROM(SELECT employeeunkid AS atempid " & _
'                        "                                                                         ,auditdatetime as atdate " & _
'                        "                                                                         ,jobunkid AS atjobid " & _
'                        "                                                                         ,ROW_NUMBER()OVER (PARTITION BY jobunkid,employeeunkid ORDER BY auditdatetime ASC) AS xRNo " & _
'                        "                                                                   FROM athremployee_master  WHERE 1 = 1 "
'                If mintEmployeeId > 0 Then
'                    StrQ &= " AND employeeunkid = '" & mintEmployeeId & "' "
'                End If
'                StrQ &= "                                                                  ) AS A WHERE A.xRNo = 1 ORDER BY A.atempid,A.atdate  ASC " & _
'                        "   OPEN EJ " & _
'                        "       FETCH NEXT FROM EJ INTO @empid, @jbid, @date " & _
'                        "           WHILE @@FETCH_STATUS = 0 " & _
'                        "           BEGIN " & _
'                        "               IF @jbid <> @nxtJId " & _
'                        "                   BEGIN " & _
'                        "                       INSERT INTO @EJ(empid,jobid,start_date)SELECT @empid, @jbid,@date " & _
'                        "                       SET @Id = @@IDENTITY; " & _
'                        "                       UPDATE @EJ SET end_date = @date - 1 WHERE id = @Id - 1 AND empid = @empid " & _
'                        "                       SET @nxtJId = @jbid " & _
'                        "                   END " & _
'                        "               FETCH NEXT FROM EJ into @empid, @jbid, @date " & _
'                        "           END " & _
'                        "   CLOSE EJ " & _
'                        "   DEALLOCATE EJ " & _
'                        "SELECT " & _
'                        "   employeecode "
'                If mblnFirstNamethenSurname = False Then
'                    StrQ &= ",ISNULL(hremployee_master.surname, '')  + ' ' +  ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') employee "
'                Else
'                    StrQ &= ",ISNULL(hremployee_master.firstname, '')  + ' ' +  ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') employee "
'                End If
'                StrQ &= ",ISNULL(JB.job_name,'') AS Job " & _
'                        ",start_date AS [start_date] " & _
'                        ",end_date AS end_date " & _
'                        ",CONVERT(CHAR(8),start_date,112) AS SDate " & _
'                        ",CONVERT(CHAR(8),end_date,112) AS EDate " & _
'                        ",(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
'                        ",(DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END AS Months " & _
'                        ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT (CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),end_date) AS Days "
'                If mintViewIndex > 0 Then
'                    StrQ &= mstrAnalysis_Fields
'                Else
'                    StrQ &= ", 0 AS Id, '' AS GName "
'                End If
'                StrQ &= "FROM @EJ " & _
'                        "   JOIN hremployee_master ON [@EJ].empid = hremployee_master.employeeunkid " & _
'                        "   JOIN hrjob_master AS JB ON [@EJ].jobid = JB.jobunkid "
'                StrQ &= mstrAnalysis_Join
'                StrQ &= "WHERE 1 = 1 "

'                If mblnIsActive = False Then
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                End If

'                If mstrAdvance_Filter.Trim.Length > 0 Then
'                    StrQ &= " AND " & mstrAdvance_Filter
'                End If

'                If mstrUserAccessFilter = "" Then
'                    If UserAccessLevel._AccessLevel.Length > 0 Then
'                        StrQ &= UserAccessLevel._AccessLevelFilterString
'                    End If
'                Else
'                    StrQ &= mstrUserAccessFilter
'                End If
'            Else
'                StrQ = "SELECT " & _
'                       " employeecode "
'                If mblnFirstNamethenSurname = False Then
'                    StrQ &= ",ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS employee "
'                Else
'                    StrQ &= ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employee "
'                End If
'                StrQ &= ",JB.job_name AS Job " & _
'                        ",CONVERT(CHAR(8),start_date,112) AS SDate " & _
'                        ",CONVERT(CHAR(8),end_date,112) AS EDate " & _
'                        ",(CAST(CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000 AS Years " & _
'                        ",(DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END AS Months " & _
'                        ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,start_date,end_date)+12)%12 - CASE WHEN DAY(start_date)>DAY(end_date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST (CONVERT(CHAR(8),end_date,112) AS INT)-CAST(CONVERT(CHAR(8),start_date,112) AS INT))/10000,start_date)),end_date) AS [Days] "
'                If mintViewIndex > 0 Then
'                    StrQ &= mstrAnalysis_Fields
'                Else
'                    StrQ &= ", 0 AS Id, '' AS GName "
'                End If

'                StrQ &= "FROM hremployee_master "
'                StrQ &= mstrAnalysis_Join
'                StrQ &= "JOIN hrjob_master AS JB ON hremployee_master.jobunkid = JB.jobunkid " & _
'                        "LEFT JOIN " & _
'                        "( " & _
'                        "   SELECT " & _
'                        "        employeeunkid AS atempid " & _
'                        "       ,auditdatetime as start_date " & _
'                        "       ,GETDATE() AS end_date " & _
'                        "       ,jobunkid AS atjobid " & _
'                        "       ,ROW_NUMBER()OVER(PARTITION BY jobunkid,employeeunkid ORDER BY auditdatetime ASC) AS xRNo " & _
'                        "   FROM athremployee_master WHERE 1 = 1 "
'                If mintEmployeeId > 0 Then
'                    StrQ &= " AND employeeunkid = '" & mintEmployeeId & "' "
'                End If
'                StrQ &= ") AS B ON B.atempid = employeeunkid AND B.atjobid = hremployee_master.jobunkid AND B.xRNo = 1 " & _
'                        "WHERE 1 = 1 "

'                If mblnIsActive = False Then
'                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
'                End If

'                If mstrAdvance_Filter.Trim.Length > 0 Then
'                    StrQ &= " AND " & mstrAdvance_Filter
'                End If

'                If mstrUserAccessFilter = "" Then
'                    If UserAccessLevel._AccessLevel.Length > 0 Then
'                        StrQ &= UserAccessLevel._AccessLevelFilterString
'                    End If
'                Else
'                    StrQ &= mstrUserAccessFilter
'                End If
'            End If

'            Call FilterTitleAndFilterQuery()
'            StrQ &= Me._FilterQuery

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
'            End If

'            rpt_Data = New ArutiReport.Designer.dsArutiReport

'            Dim rpt_Rows As DataRow = Nothing
'            For Each dFRow As DataRow In dsList.Tables(0).Rows
'                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
'                rpt_Rows.Item("Column1") = dFRow.Item("employeecode")
'                rpt_Rows.Item("Column2") = dFRow.Item("employee")
'                rpt_Rows.Item("Column3") = dFRow.Item("Job")
'                If dFRow.Item("SDate").ToString.Trim.Length > 0 Then
'                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dFRow.Item("SDate").ToString).ToShortDateString
'                Else
'                    rpt_Rows.Item("Column4") = DBNull.Value
'                End If
'                If mblnFromCurrDate = True Then
'                    rpt_Rows.Item("Column5") = ""
'                Else
'                    If dFRow.Item("EDate").ToString.Trim.Trim.Length > 0 Then
'                        If dFRow.Item("EDate") = eZeeDate.convertDate(Now) Then
'                            rpt_Rows.Item("Column5") = DBNull.Value
'                        Else
'                            rpt_Rows.Item("Column5") = eZeeDate.convertDate(dFRow.Item("EDate").ToString).ToShortDateString
'                        End If
'                    Else
'                        rpt_Rows.Item("Column5") = DBNull.Value
'                    End If
'                End If
'                rpt_Rows.Item("Column6") = dFRow.Item("Years")
'                rpt_Rows.Item("Column7") = dFRow.Item("Months")
'                rpt_Rows.Item("Column8") = dFRow.Item("Days")
'                rpt_Rows.Item("Column9") = dFRow.Item("GName")
'                rpt_Rows.Item("Column10") = dFRow.Item("Id")
'                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
'            Next

'            objRpt = New ArutiReport.Designer.rptEmployeeDurationReport

'            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

'            Dim arrImageRow As DataRow = Nothing
'            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

'            ReportFunction.Logo_Display(objRpt, _
'                                        ConfigParameter._Object._IsDisplayLogo, _
'                                        ConfigParameter._Object._ShowLogoRightSide, _
'                                        "arutiLogo1", _
'                                        "arutiLogo2", _
'                                        arrImageRow, _
'                                        "txtCompanyName", _
'                                        "txtReportName", _
'                                        "txtFilterDescription", _
'                                        ConfigParameter._Object._GetLeftMargin, _
'                                        ConfigParameter._Object._GetRightMargin)

'            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

'            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
'                rpt_Data.Tables("ArutiTable").Rows.Add("")
'            End If

'            If ConfigParameter._Object._IsShowPreparedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By :"))
'                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
'            End If

'            If ConfigParameter._Object._IsShowCheckedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 13, "Checked By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
'            End If

'            If ConfigParameter._Object._IsShowApprovedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 14, "Approved By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
'            End If

'            If ConfigParameter._Object._IsShowReceivedBy = True Then
'                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 15, "Received By :"))
'            Else
'                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
'            End If

'            If mintViewIndex <= 0 Then
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
'                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
'            End If


'            objRpt.SetDataSource(rpt_Data)

'            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code :"))
'            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee :"))
'            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 3, "Job"))
'            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 4, "Start Date"))
'            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 5, "End Date"))
'            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 6, "Years"))
'            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 7, "Months"))
'            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 8, "Days"))
'            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 21, "Employee Count :"))

'            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 9, "Printed By :"))
'            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 10, "Printed Date :"))

'            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
'            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

'            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
'            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
'            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

'            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

'            Return objRpt

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Generate_JobTitleReport; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [29 JAN 2015] -- END

'#End Region


'    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

'    End Sub
'End Class
