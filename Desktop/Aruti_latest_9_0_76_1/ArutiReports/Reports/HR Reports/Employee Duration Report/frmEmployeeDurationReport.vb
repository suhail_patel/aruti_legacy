'************************************************************************************************************************************
'Class Name : frmEmployeeDurationReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeDurationReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDurationReport"
    Private objEmpDuration As clsEmployeeDurationReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objEmpDuration = New clsEmployeeDurationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpDuration.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim ObjEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            Dim objJob As New clsJobs
            dsList = objJob.getComboList("Job", True)
            cboJob.DisplayMember = "name"
            cboJob.ValueMember = "jobunkid"
            cboJob.DataSource = dsList.Tables("Job")

            'S.SANDEEP [ 01 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Employee Duration on Job Title"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Employee Duration Allocation Wise"))
                'S.SANDEEP [29 JAN 2015] -- START
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Employee Duration On Experience"))
                'S.SANDEEP [29 JAN 2015] -- END
                'Sohail (24 Dec 2018) -- Start
                'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Employee Year of Service"))
                'Sohail (24 Dec 2018) -- End
                .SelectedIndex = 0
            End With
            Dim objMData As New clsMasterData
            dsList = objMData.GetEAllocation_Notification("List")
            'S.SANDEEP [29 JAN 2015] -- START
            'Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [29 JAN 2015] -- END
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With
            'S.SANDEEP [ 01 FEB 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboJob.SelectedValue = 0
            nudFromYear.Value = 0
            nudToYear.Value = 0
            chkInactiveemp.Checked = False
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            objEmpDuration.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpDuration.OrderByDisplay
            'S.SANDEEP [ 01 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboAllocations.SelectedValue = 1
            cboReportType.SelectedIndex = 0
            Call Do_Operation(False)
            chkYear.Checked = False : chkMonth.Checked = False : chkDays.Checked = False
            txtYFromYear.Text = "" : txtYToYear.Text = ""
            txtMFromYear.Text = "" : txtMToYear.Text = ""
            txtDFromYear.Text = "" : txtDToYear.Text = ""

            txtYFromYear.Enabled = chkYear.Checked
            txtYToYear.Enabled = chkYear.Checked
            txtMFromYear.Enabled = chkMonth.Checked
            txtMToYear.Enabled = chkMonth.Checked
            txtDFromYear.Enabled = chkDays.Checked
            txtDToYear.Enabled = chkDays.Checked
            'S.SANDEEP [ 01 FEB 2013 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpDuration.SetDefaultValue()
            objEmpDuration._EmployeeId = cboEmployee.SelectedValue
            objEmpDuration._EmployeeName = cboEmployee.Text

            objEmpDuration._ReportTypeId = cboReportType.SelectedIndex
            objEmpDuration._ReportTypeName = cboReportType.Text


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If cboReportType.SelectedIndex = 0 Then
            '    objEmpDuration._JobId = cboJob.SelectedValue
            '    objEmpDuration._JobName = cboJob.Text
            '    objEmpDuration._FromYear = nudFromYear.Value
            '    objEmpDuration._ToYear = nudToYear.Value
            '    objEmpDuration._IsActive = chkInactiveemp.Checked
            '    objEmpDuration._ViewByIds = mstrStringIds
            '    objEmpDuration._ViewIndex = mintViewIdx
            '    objEmpDuration._ViewByName = mstrStringName
            '    objEmpDuration._Analysis_Fields = mstrAnalysis_Fields
            '    objEmpDuration._Analysis_Join = mstrAnalysis_Join
            '    objEmpDuration._Analysis_OrderBy = mstrAnalysis_OrderBy
            '    objEmpDuration._Report_GroupName = mstrReport_GroupName

            '    'S.SANDEEP [ 22 FEB 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    objEmpDuration._Exp_FromCurrDate = chkFromCurrDate.Checked
            '    objEmpDuration._CurrDate_Caption = chkFromCurrDate.Text
            '    'S.SANDEEP [ 22 FEB 2013 ] -- END

            'ElseIf cboReportType.SelectedIndex = 1 Then
            '    Dim StrStringIds As String = ""
            '    If lvAllocation.CheckedItems.Count > 0 Then
            '        For Each lvItem As ListViewItem In lvAllocation.CheckedItems
            '            StrStringIds &= "," & lvItem.Tag.ToString
            '        Next
            '        If StrStringIds.Length > 0 Then StrStringIds = Mid(StrStringIds, 2)
            '    End If

            'Select Case cboAllocations.SelectedValue
            '    Case enAllocation.BRANCH
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrstation_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrstation_master ON hrstation_master.stationunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.DEPARTMENT_GROUP
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrdepartment_group_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.DEPARTMENT
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrdepartment_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.SECTION_GROUP
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrsectiongroup_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.SECTION
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrsection_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrsection_master ON hrsection_master.sectionunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.UNIT_GROUP
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrunitgroup_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.UNIT
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrunit_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrunit_master ON hrunit_master.unitunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.TEAM
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrteam_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrteam_master ON hrteam_master.teamunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.JOB_GROUP
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrjobgroup_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.JOBS
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrjob_master.job_name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrjob_master ON hrjob_master.jobunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.CLASS_GROUP
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrclassgroup_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"

            '    Case enAllocation.CLASSES
            '        objEmpDuration._SelectAlloc = ",ISNULL(hrclasses_master.name,'') AS Allocation "
            '        objEmpDuration._AlloctionJoin = "JOIN hrclasses_master ON hrclasses_master.classesunkid = hrallocation_tracking.allocationunkid "
            '        If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hrallocation_tracking.allocationunkid IN(" & StrStringIds & ")"
            'End Select

            '    objEmpDuration._IsActive = False
            '    objEmpDuration._Allocation_Name = cboAllocations.Text
            '    objEmpDuration._AllocationTypeId = cboAllocations.SelectedValue

            '    If chkYear.Checked = True Then
            '        If txtYFromYear.Text <> "" AndAlso txtYToYear.Text <> "" Then
            '            objEmpDuration._Alloc_YearFrom = txtYFromYear.Decimal
            '            objEmpDuration._Alloc_YearTo = txtYToYear.Decimal
            '        End If
            '    End If

            '    If chkMonth.Checked = True Then
            '        If txtMFromYear.Text <> "" AndAlso txtMToYear.Text <> "" Then
            '            objEmpDuration._Alloc_MonthFrom = txtMFromYear.Decimal
            '            objEmpDuration._Alloc_MonthTo = txtMToYear.Decimal
            '        End If
            '    End If

            '    If chkDays.Checked = True Then
            '        If txtDFromYear.Text <> "" AndAlso txtDToYear.Text <> "" Then
            '            objEmpDuration._Alloc_DayFrom = txtDFromYear.Decimal
            '            objEmpDuration._Alloc_DayTo = txtDToYear.Decimal
            '        End If
            '    End If

            '    'S.SANDEEP [ 22 FEB 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    objEmpDuration._CurrAllocation = chkCurrentAllocation.Checked
            '    objEmpDuration._CurrAllocationCaption = chkCurrentAllocation.Text
            '    'S.SANDEEP [ 22 FEB 2013 ] -- END

            'End If


            Select Case cboReportType.SelectedIndex
                Case 0, 2, 3
                    'Sohail (24 Dec 2018) - [3]

                    objEmpDuration._JobId = cboJob.SelectedValue
                    objEmpDuration._JobName = cboJob.Text
                    objEmpDuration._FromYear = nudFromYear.Value
                    objEmpDuration._ToYear = nudToYear.Value
                    objEmpDuration._IsActive = chkInactiveemp.Checked
                    objEmpDuration._ViewByIds = mstrStringIds
                    objEmpDuration._ViewIndex = mintViewIdx
                    objEmpDuration._ViewByName = mstrStringName
                    objEmpDuration._Analysis_Fields = mstrAnalysis_Fields
                    objEmpDuration._Analysis_Join = mstrAnalysis_Join
                    objEmpDuration._Analysis_OrderBy = mstrAnalysis_OrderBy
                    objEmpDuration._Report_GroupName = mstrReport_GroupName
                    objEmpDuration._Exp_FromCurrDate = chkFromCurrDate.Checked
                    objEmpDuration._CurrDate_Caption = chkFromCurrDate.Text
                Case 1
                    Dim StrStringIds As String = ""
                    If lvAllocation.CheckedItems.Count > 0 Then
                        For Each lvItem As ListViewItem In lvAllocation.CheckedItems
                            StrStringIds &= "," & lvItem.Tag.ToString
                        Next
                        If StrStringIds.Length > 0 Then StrStringIds = Mid(StrStringIds, 2)
                    End If

                    Select Case cboAllocations.SelectedValue
                        Case enAllocation.BRANCH
                            objEmpDuration._SelectAlloc = ",ISNULL(hrstation_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.stationunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "stationunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrstation_master ON hrstation_master.stationunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.stationunkid IN(" & StrStringIds & ")"

                        Case enAllocation.DEPARTMENT_GROUP
                            objEmpDuration._SelectAlloc = ",ISNULL(hrdepartment_group_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.deptgroupunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "deptgroupunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.deptgroupunkid IN(" & StrStringIds & ")"

                        Case enAllocation.DEPARTMENT
                            objEmpDuration._SelectAlloc = ",ISNULL(hrdepartment_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.departmentunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "departmentunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.departmentunkid IN(" & StrStringIds & ")"

                        Case enAllocation.SECTION_GROUP
                            objEmpDuration._SelectAlloc = ",ISNULL(hrsectiongroup_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.sectiongroupunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "sectiongroupunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.sectiongroupunkid IN(" & StrStringIds & ")"

                        Case enAllocation.SECTION
                            objEmpDuration._SelectAlloc = ",ISNULL(hrsection_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.sectionunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "sectionunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrsection_master ON hrsection_master.sectionunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.sectionunkid IN(" & StrStringIds & ")"

                        Case enAllocation.UNIT_GROUP
                            objEmpDuration._SelectAlloc = ",ISNULL(hrunitgroup_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.unitgroupunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "unitgroupunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.unitgroupunkid IN(" & StrStringIds & ")"

                        Case enAllocation.UNIT
                            objEmpDuration._SelectAlloc = ",ISNULL(hrunit_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.unitunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "unitunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrunit_master ON hrunit_master.unitunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.unitunkid IN(" & StrStringIds & ")"

                        Case enAllocation.TEAM
                            objEmpDuration._SelectAlloc = ",ISNULL(hrteam_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.teamunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "teamunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrteam_master ON hrteam_master.teamunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.teamunkid IN(" & StrStringIds & ")"

                        Case enAllocation.CLASS_GROUP
                            objEmpDuration._SelectAlloc = ",ISNULL(hrclassgroup_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.classgroupunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "classgroupunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.classgroupunkid IN(" & StrStringIds & ")"

                        Case enAllocation.CLASSES
                            objEmpDuration._SelectAlloc = ",ISNULL(hrclasses_master.name,'') AS Allocation "
                            objEmpDuration._SelectAllocIdColumn = ",ISNULL(hremployee_transfer_tran.classunkid,0) AS allocationunkid "
                            objEmpDuration._AllocationColName = "classunkid"
                            objEmpDuration._AlloctionJoin = "JOIN hrclasses_master ON hrclasses_master.classesunkid = [@EM].allocid "
                            If StrStringIds.Trim.Length > 0 Then objEmpDuration._SelectedAllocationIds = " AND hremployee_transfer_tran.classunkid IN(" & StrStringIds & ")"
                    End Select

                    objEmpDuration._IsActive = False
                    objEmpDuration._Allocation_Name = cboAllocations.Text
                    objEmpDuration._AllocationTypeId = cboAllocations.SelectedValue

                    If chkYear.Checked = True Then
                        If txtYFromYear.Text <> "" AndAlso txtYToYear.Text <> "" Then
                            objEmpDuration._Alloc_YearFrom = txtYFromYear.Decimal
                            objEmpDuration._Alloc_YearTo = txtYToYear.Decimal
                        End If
                    End If

                    If chkMonth.Checked = True Then
                        If txtMFromYear.Text <> "" AndAlso txtMToYear.Text <> "" Then
                            objEmpDuration._Alloc_MonthFrom = txtMFromYear.Decimal
                            objEmpDuration._Alloc_MonthTo = txtMToYear.Decimal
                        End If
                    End If

                    If chkDays.Checked = True Then
                        If txtDFromYear.Text <> "" AndAlso txtDToYear.Text <> "" Then
                            objEmpDuration._Alloc_DayFrom = txtDFromYear.Decimal
                            objEmpDuration._Alloc_DayTo = txtDToYear.Decimal
                        End If
                    End If

                    objEmpDuration._CurrAllocation = chkCurrentAllocation.Checked
                    objEmpDuration._CurrAllocationCaption = chkCurrentAllocation.Text

            End Select

            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            objEmpDuration._AsOnDate = dtpAsOnDate.Value
            'Sohail (24 Dec 2018) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmpDuration._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Function Validation() As Boolean
        Try
            If cboReportType.SelectedIndex = 0 Then
                If nudToYear.Value <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select to year Value greater than 0."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    nudToYear.Select()
                    Return False
                End If

                If nudToYear.Value < nudFromYear.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid value for From Year.Please Select correct value for From Year."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    nudFromYear.Select()
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function


    'S.SANDEEP [ 01 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 285 - 20
                objchkAll.Location = New Point(337, 86)
            Else
                colhAllocations.Width = 285
                objchkAll.Location = New Point(357, 86)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 01 FEB 2013 ] -- END

#End Region

#Region " Forms "

    Private Sub frmEmployeeDurationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpDuration = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeDurationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDurationReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objEmpDuration._ReportName
            Me._Message = objEmpDuration._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDurationReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDistributionReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeDistributionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Validation() Then
                If Not SetFilter() Then Exit Sub

                'S.SANDEEP [ 30 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objEmpDuration.generateReport(0, e.Type, enExportAction.None)
                objEmpDuration.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, e.Type, enExportAction.None)
                'S.SANDEEP [ 30 JAN 2013 ] -- END

            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                If Not SetFilter() Then Exit Sub
                'S.SANDEEP [ 30 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objEmpDuration.generateReport(0, enPrintAction.None, e.Type)
                objEmpDuration.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, enPrintAction.None, e.Type)
                'S.SANDEEP [ 30 JAN 2013 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboJob.DataSource
            frm.ValueMember = cboJob.ValueMember
            frm.DisplayMember = cboJob.DisplayMember
            If frm.DisplayDialog Then
                cboJob.SelectedValue = frm.SelectedValue
                cboJob.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDurationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeDurationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpDuration.setOrderBy(0)
            txtOrderBy.Text = objEmpDuration.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region "LinkButton Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 01 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region "Controls Events "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'Sohail (24 Dec 2018) -- Start
            'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
            lblAsOnDate.Visible = False
            dtpAsOnDate.Visible = False
            'Sohail (24 Dec 2018) -- End

            Select Case cboReportType.SelectedIndex
                'S.SANDEEP [29 JAN 2015] -- START
                'Case 0
                Case 0, 2
                    If cboReportType.SelectedIndex = 0 Then
                        chkFromCurrDate.Checked = False
                        chkFromCurrDate.Enabled = False

                        chkFromCurrDate.Enabled = True

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        cboJob.Enabled = True
                        objbtnSearchJob.Enabled = True
                        'S.SANDEEP [04 JUN 2015] -- END

                    Else
                        chkFromCurrDate.Checked = False
                        chkFromCurrDate.Enabled = False

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        cboJob.Enabled = False
                        objbtnSearchJob.Enabled = False
                        'S.SANDEEP [04 JUN 2015] -- END

                    End If
                    'S.SANDEEP [29 JAN 2015] -- END
                    objPanel2.Visible = False
                    objPanel1.Visible = True
                    gbMandatoryInfo.Size = New Point(417, 165)
                    objPanel1.Size = New Point(411, 79)
                    objPanel1.Location = New Point(3, 82)
                    lnkSetAnalysis.Visible = True
                    gbSortBy.Visible = True
                    gbOptionalFilter.Visible = False
                    'S.SANDEEP [ 22 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    chkCurrentAllocation.Checked = False
                    'S.SANDEEP [ 22 FEB 2013 ] -- END

                Case 1
                    'PLEASE DO NOT GIVE ANY USER DEFINED ORDER FOR THIS REPORT TYPE ELSE REPORT WILL BE DISTURBED.
                    objPanel1.Visible = False
                    objPanel2.Visible = True
                    gbMandatoryInfo.Size = New Point(417, 330)
                    objPanel2.Size = New Point(411, 241)
                    objPanel2.Location = New Point(3, 82)
                    lnkSetAnalysis.Visible = False
                    gbSortBy.Visible = False
                    gbOptionalFilter.Visible = True
                    'S.SANDEEP [ 22 FEB 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    chkFromCurrDate.Checked = False
                    'S.SANDEEP [ 22 FEB 2013 ] -- END

                    'Sohail (24 Dec 2018) -- Start
                    'NMB Enhancement - 76.1 - New Report type Employee Year of Service Report for - Ability  to  auto  compute  year  of  service  allowance  based  on  accumulated  number  of  years  employed  including  rehired  period.
                Case 3
                    chkFromCurrDate.Checked = False
                    chkFromCurrDate.Enabled = False

                    cboJob.Enabled = False
                    objbtnSearchJob.Enabled = False

                    lblAsOnDate.Visible = True
                    dtpAsOnDate.Visible = True

                    objPanel2.Visible = False
                    objPanel1.Visible = True
                    gbMandatoryInfo.Size = New Point(417, 191)
                    objPanel1.Size = New Point(411, 105)
                    objPanel1.Location = New Point(3, 82)
                    lnkSetAnalysis.Visible = True
                    gbSortBy.Visible = True
                    gbOptionalFilter.Visible = False
                    chkCurrentAllocation.Checked = False

                    objEmpDuration._ReportTypeId = cboReportType.SelectedIndex
                    Call chkFromCurrDate_CheckedChanged(chkFromCurrDate, New System.EventArgs)
                    'Sohail (24 Dec 2018) -- End

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub chkYear_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkYear.CheckedChanged, _
                                                                                                           chkMonth.CheckedChanged, _
                                                                                                           chkDays.CheckedChanged
        Try
            Select Case CType(sender, CheckBox).Name.ToUpper()
                Case "CHKYEAR"
                    txtYFromYear.Enabled = chkYear.Checked
                    txtYToYear.Enabled = chkYear.Checked
                    If chkYear.Checked = True Then
                        txtYFromYear.Text = "" : txtYToYear.Text = ""
                    End If
                Case "CHKMONTH"
                    txtMFromYear.Enabled = chkMonth.Checked
                    txtMToYear.Enabled = chkMonth.Checked
                    If chkMonth.Checked = True Then
                        txtMFromYear.Text = "" : txtMToYear.Text = ""
                    End If
                Case "CHKDAYS"
                    txtDFromYear.Enabled = chkDays.Checked
                    txtDToYear.Enabled = chkDays.Checked
                    If chkDays.Checked = True Then
                        txtDFromYear.Text = "" : txtDToYear.Text = ""
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkYear_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 22 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkFromCurrDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFromCurrDate.CheckedChanged
        Try
            objEmpDuration.Set_Sort_Collection(chkFromCurrDate.Checked)
            objEmpDuration.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpDuration.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkFromCurrDate_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 22 FEB 2013 ] -- END

#End Region
    'S.SANDEEP [ 01 FEB 2013 ] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOptionalFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOptionalFilter.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.lblToYear.Text = Language._Object.getCaption(Me.lblToYear.Name, Me.lblToYear.Text)
            Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
            Me.LblJob.Text = Language._Object.getCaption(Me.LblJob.Name, Me.LblJob.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.lblRType.Text = Language._Object.getCaption(Me.lblRType.Name, Me.lblRType.Text)
            Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
            Me.gbOptionalFilter.Text = Language._Object.getCaption(Me.gbOptionalFilter.Name, Me.gbOptionalFilter.Text)
            Me.chkDays.Text = Language._Object.getCaption(Me.chkDays.Name, Me.chkDays.Text)
            Me.chkMonth.Text = Language._Object.getCaption(Me.chkMonth.Name, Me.chkMonth.Text)
            Me.chkYear.Text = Language._Object.getCaption(Me.chkYear.Name, Me.chkYear.Text)
            Me.lblYTo.Text = Language._Object.getCaption(Me.lblYTo.Name, Me.lblYTo.Text)
            Me.lblYFrom.Text = Language._Object.getCaption(Me.lblYFrom.Name, Me.lblYFrom.Text)
            Me.lblDTo.Text = Language._Object.getCaption(Me.lblDTo.Name, Me.lblDTo.Text)
            Me.lblDFrom.Text = Language._Object.getCaption(Me.lblDFrom.Name, Me.lblDFrom.Text)
            Me.lblMTo.Text = Language._Object.getCaption(Me.lblMTo.Name, Me.lblMTo.Text)
            Me.lblMFrom.Text = Language._Object.getCaption(Me.lblMFrom.Name, Me.lblMFrom.Text)
            Me.chkFromCurrDate.Text = Language._Object.getCaption(Me.chkFromCurrDate.Name, Me.chkFromCurrDate.Text)
            Me.chkCurrentAllocation.Text = Language._Object.getCaption(Me.chkCurrentAllocation.Name, Me.chkCurrentAllocation.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Duration on Job Title")
            Language.setMessage(mstrModuleName, 2, "Employee Duration Allocation Wise")
            Language.setMessage(mstrModuleName, 3, "Please select to year Value greater than 0.")
            Language.setMessage(mstrModuleName, 4, "Invalid value for From Year.Please Select correct value for From Year.")
            Language.setMessage(mstrModuleName, 5, "Employee Duration On Experience")
			Language.setMessage(mstrModuleName, 6, "Employee Year of Service")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
