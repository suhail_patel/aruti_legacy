'************************************************************************************************************************************
'Class Name : frmEmployeeBiodata.vb
'Purpose    : 
'Written By : Anjan
'Modified   : 
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeBiodata


#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeBiodata"
    Private objEmployeeBiodata As clsEmployeeBiodata
#End Region

#Region " Constructor "
    Public Sub New()
        objEmployeeBiodata = New clsEmployeeBiodata(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmployeeBiodata.SetDefaultValue()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub
#End Region

#Region " Private Functions "
    Private Sub FillCombo()
        Dim dsList As DataSet
        Dim objEmployee As New clsEmployee_Master

        Try


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmployee.GetEmployeeList("List", True, True)


            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objEmployee.GetEmployeeList("List", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmployee.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .DisplayMember = "employeename"
                .ValueMember = "employeeunkid"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    Private Sub ResetValue()

        Try
            cboEmployee.SelectedValue = 0
            chkPrintBlank.Checked = False


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Checked = False
            'Pinkal (24-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try

    End Sub

    Function setFilter() As Boolean

        Try
            objEmployeeBiodata.SetDefaultValue()
            objEmployeeBiodata._EmployeeId = cboEmployee.SelectedValue
            objEmployeeBiodata._EmployeeName = cboEmployee.Text
            If chkIsDomicile.Checked = True Then
                objEmployeeBiodata._IsDomicileAddress = True
            Else
                objEmployeeBiodata._IsDomicileAddress = False
            End If


            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objEmployeeBiodata._IsImageInDb = ConfigParameter._Object._IsImgInDataBase
            'Pinkal (03-Apr-2013) -- End


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objEmployeeBiodata._ShowEmployeeScale = chkShowEmpScale.Checked
            'Pinkal (24-Apr-2013) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objEmployeeBiodata._FormateCurrency = GUI.fmtCurrency
            'Shani(24-Aug-2015) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            objEmployeeBiodata._AsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'Sohail (18 May 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setFilter", mstrModuleName)
        End Try

    End Function
#End Region

#Region " Forms "
    Private Sub frmEmployeeBiodataReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeBiodata = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeBiodataReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBiodata_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objEmployeeBiodata._ReportName
            Me._Message = objEmployeeBiodata._ReportDesc

            Call FillCombo()
            Call ResetValue()

            'Pinkal (30-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Visible = User._Object.Privilege._AllowTo_View_Scale
            'Pinkal (30-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBiodata_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyPress", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Buttons "
    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click

        Try
            If cboEmployee.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Employee is mandatory information.Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub

            End If
            If Not setFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeBiodata.generateReport(0, e.Type, enExportAction.None)
            objEmployeeBiodata.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                 ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, _
                                                 0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click

        Try
            If cboEmployee.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Employee is mandatory information.Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub

            End If
            If Not setFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeBiodata.generateReport(0, enPrintAction.None, e.Type)
            objEmployeeBiodata.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                 ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, _
                                                 0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click

        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch

        Try
            frm.DataSource = cboEmployee.DataSource
            frm.SelectedValue = cboEmployee.SelectedValue
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeBiodata.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeBiodata"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls "
   
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.chkPrintBlank.Text = Language._Object.getCaption(Me.chkPrintBlank.Name, Me.chkPrintBlank.Text)
			Me.chkIsDomicile.Text = Language._Object.getCaption(Me.chkIsDomicile.Name, Me.chkIsDomicile.Text)
			Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.Name, Me.chkShowEmpScale.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 111, "Employee is mandatory information.Please select Employee.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
