'************************************************************************************************************************************
'Class Name : clsEmployeeBiodata.vb
'Purpose    :
'Date       : 03/02/2011
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************


Imports Aruti.Data
Imports eZeeCommonLib

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Anjan
''' </summary>

Public Class clsEmployeeBiodata
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeBiodata"
    Private mstrReportId As String = enArutiReport.EmployeeBiodataReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        'Call Create_OnDetailReport()

    End Sub
#End Region

#Region " Private Variables "
    Private mstrEmployeeCode As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIsDomicileAddress As Boolean = False
    Private mblnIsPrintBlank As Boolean = False

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End


    'Pinkal (03-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnIsInImageDb As Boolean = False
    'Pinkal (03-Apr-2013) -- End


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnShowEmpScale As Boolean = False
    'Pinkal (24-Apr-2013) -- End


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mstrFormateCurrency As String = ""
    'Shani(24-Aug-2015) -- End

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IsDomicileAddress() As Boolean
        Set(ByVal value As Boolean)
            mblnIsDomicileAddress = value
        End Set
    End Property

    Public WriteOnly Property _IsPrintBlank() As Boolean
        Set(ByVal value As Boolean)
            mblnIsPrintBlank = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End


    'Pinkal (03-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _IsImageInDb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsInImageDb = value
        End Set
    End Property

    'Pinkal (03-Apr-2013) -- End


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    'Pinkal (24-Apr-2013) -- End

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Property _FormateCurrency() As String
        Get
            Return mstrFormateCurrency
        End Get
        Set(ByVal value As String)
            mstrFormateCurrency = value
        End Set
    End Property

    'Shani(24-Aug-2015) -- End

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()

        Try
            mstrEmployeeCode = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            mblnShowEmpScale = False
            'Pinkal (24-Apr-2013) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        If mintEmployeeId > 0 Then
            objDataOperation.AddParameter("@Employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
        End If

        If mblnIsDomicileAddress = True Then
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Print Domicile Address") & " "
        Else
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Print Present Address") & " "
        End If
    End Sub

#End Region


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try
            'Pinkal (14-Aug-2012) -- Start
            'Enhancement : TRA Changes
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt

            'Pinkal (14-Aug-2012) -- End
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END



    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub


#Region " Report Generation "
    Dim iColumn_DetailReport As IColumnCollection

    Public Property _Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsFinal As New DataSet
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim objEmployeeData As clsEmployee_Master
        Try

            objDataOperation = New clsDataOperation

            StrQ = "SELECT employeecode AS 'Emp Code' " & _
                           ", ISNULL (surname,'') AS 'Surname' " & _
                           ", ISNULL(firstname,'') AS 'Firstname' " & _
                           ", ISNULL(othername,'') AS 'Othername' " & _
                           ", ISNULL(CASE WHEN gender =1 THEN 'Male' " & _
                                  "WHEN gender=2 THEN 'Female' " & _
                                   "END,'') AS 'Gender' " & _
                          ", ISNULL(cfcommon_master.name,'') AS 'Marital Status' " & _
                          ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS 'Nationality' " & _
                          ", ISNULL(religion.name,'') AS 'Religion' " & _
                          ", ISNULL(language1.name,'') AS 'Language1' " & _
                          ", ISNULL(language2.name,'') AS 'Language2' " & _
                "FROM hremployee_master " & _
                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid= hremployee_master.maritalstatusunkid AND cfcommon_master.mastertype=15 " & _
                "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.nationalityunkid " & _
                "LEFT JOIN cfcommon_master AS religion ON religion.masterunkid=hremployee_master.religionunkid AND cfcommon_master.isactive = 1 AND religion.mastertype=" & clsCommon_Master.enCommonMaster.RELIGION & " " & _
                "LEFT JOIN cfcommon_master AS language1 ON language1.masterunkid=hremployee_master.language1unkid AND cfcommon_master.isactive = 1 AND language1.mastertype= " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                "LEFT JOIN cfcommon_master AS language2 ON language2.masterunkid=hremployee_master.language2unkid AND cfcommon_master.isactive = 1 AND language2.mastertype= " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                "WHERE employeeunkid = @Employeeunkid  "

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim intCnt As Integer = 0
            Dim rpt_Rows As DataRow = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                For Each dtColumn As DataColumn In dsList.Tables(0).Columns
                    If intCnt <> 9 Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    End If
                    rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 9, "Employee Details")
                    If dtColumn.ColumnName <> "Language2" Then
                        rpt_Rows.Item("Column2") = dtColumn.ColumnName
                        rpt_Rows.Item("Column3") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    Else
                        rpt_Rows.Item("Column4") = dtColumn.ColumnName
                        rpt_Rows.Item("Column5") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    End If

                    If intCnt <> 9 Then
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If
                    intCnt += 1
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 9, "Employee Details")
            End If

            StrQ = ""
            If mblnIsDomicileAddress = True Then
                StrQ = "SELECT  hremployee_master.domicile_address1 +' '+ hremployee_master.domicile_address2 AS 'Domicile Address' " & _
                                ",hremployee_master.domicile_email AS 'Domicile Email' " & _
                                ",hremployee_master.domicile_mobile AS 'Domicile Phone' " & _
                                ",hremployee_master.domicile_mobile AS 'Domiclie Mob' " & _
                        "FROM hremployee_master " & _
                        "WHERE employeeunkid = @Employeeunkid "
            Else
                StrQ = "SELECT hremployee_master.present_address1 +' '+ hremployee_master.present_address2 AS 'Present Address' " & _
                                ",hremployee_master.present_email AS 'Present Email' " & _
                                ",hremployee_master.present_mobile AS 'Present Phone' " & _
                                ",hremployee_master.present_mobile AS 'Present Mob' " & _
                        "FROM hremployee_master " & _
                        "WHERE employeeunkid = @Employeeunkid "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dsEmergencyDetail As New DataSet
            Dim strQEdetails As String = ""

            strQEdetails = "SELECT hremployee_master.emer_con_address AS 'Emergency address' " & _
                                ",hremployee_master.emer_con_email AS 'Emergency email' " & _
                                ",hremployee_master.emer_con_tel_no AS 'Emergency phone' " & _
                                ",hremployee_master.emer_con_mobile AS 'Emergency Mob' " & _
                        "FROM hremployee_master " & _
                        "WHERE employeeunkid = @Employeeunkid "

            dsEmergencyDetail = objDataOperation.ExecQuery(strQEdetails, "List")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            intCnt = 0

            If dsList.Tables(0).Rows.Count > 0 Or dsEmergencyDetail.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                Dim j As Integer = 0
                While j <= dsList.Tables(0).Columns.Count - 1
                    For i As Integer = 0 To dsEmergencyDetail.Tables(0).Columns.Count - 1
                        If j > dsList.Tables(0).Columns.Count Then Exit For
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 10, "Employee Address")
                        rpt_Rows.Item("Column2") = dsList.Tables(0).Columns(j).ColumnName
                        rpt_Rows.Item("Column3") = dsList.Tables(0).Columns(dsList.Tables(0).Columns(j).ColumnName).Table(0).Item(j)
                        rpt_Rows.Item("Column4") = dsEmergencyDetail.Tables(0).Columns(i).ColumnName
                        rpt_Rows.Item("Column5") = dsEmergencyDetail.Tables(0).Columns(dsEmergencyDetail.Tables(0).Columns(i).ColumnName).Table(0).Item(i)
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                        j += 1
                    Next
                    If j > dsList.Tables(0).Columns.Count Then Exit While
                End While
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 10, "Employee Address")
            End If

            StrQ = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT CONVERT(CHAR(8) ,birthdate ,112) AS 'Date of Birth' " & _
            '               ", CASE WHEN ISNULL(birthdate,'') <>'' THEN  DATEDIFF(YEAR ,birthdate ,GETDATE()) ELSE  '' END  AS 'Age' " & _
            '               ",CONVERT(CHAR(8) ,appointeddate ,112) AS 'Appointment Date' " & _
            '               ",CONVERT(CHAR(8) ,termination_to_date ,112) AS 'Retirement Date' " & _
            '               ",CONVERT(CHAR(8) ,termination_from_date ,112) AS 'Termination Date' " & _
            '               ",ISNULL(TR.name,'') AS 'Reason' " & _
            '        "FROM  hremployee_master " & _
            '                " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
            '                " AND TR.isactive = 1  " & _
            '        "WHERE employeeunkid = @Employeeunkid "
            StrQ = "SELECT " & _
                   "     CONVERT(CHAR(8), hremployee_master.birthdate, 112) AS 'Date of Birth' " & _
                   "    ,CASE  WHEN ISNULL(hremployee_master.birthdate, '') <> '' THEN DATEDIFF(YEAR, hremployee_master.birthdate, GETDATE()) " & _
                   "            ELSE '' " & _
                   "     END AS 'Age' " & _
                   "    ,CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS 'Appointment Date' " & _
                   "    ,ISNULL(CONVERT(CHAR(8), RetrDt.date1,112),'') AS 'Retirement Date' " & _
                   "    ,ISNULL(CONVERT(CHAR(8), EocDt.date2,112),'') AS 'Termination Date' " & _
                           ",ISNULL(TR.name,'') AS 'Reason' " & _
                    "FROM  hremployee_master " & _
                   "    LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '48' AND TR.isactive = 1 " & _
                   "    LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                date1 " & _
                   "                ,date2 " & _
                   "                ,employeeunkid " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "            FROM hremployee_dates_tran " & _
                   "            WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                date1 " & _
                   "                ,employeeunkid " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "            FROM hremployee_dates_tran " & _
                   "            WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        )AS RetrDt ON RetrDt.employeeunkid = hremployee_master.employeeunkid AND RetrDt.rno = 1 " & _
                   "WHERE hremployee_master.employeeunkid = @Employeeunkid "
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If

            intCnt = 0
            If dsList.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                For Each dtColumn As DataColumn In dsList.Tables(0).Columns
                    If intCnt <> 1 And intCnt <> 5 Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    End If

                    rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 11, "Employee Dates")

                    If dtColumn.ColumnName <> "Age" And dtColumn.ColumnName <> "Reason" Then
                        rpt_Rows.Item("Column2") = dtColumn.ColumnName
                        If Not IsDBNull(dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)) Then
                            If dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt).ToString.Trim.Length > 0 Then
                                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt).ToString).ToShortDateString
                            End If
                        End If
                    Else
                        rpt_Rows.Item("Column4") = dtColumn.ColumnName
                        rpt_Rows.Item("Column5") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    End If

                    If intCnt <> 1 And intCnt <> 5 Then
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If
                    intCnt += 1
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 11, "Employee Dates")
            End If


            StrQ = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT hrstation_master.name AS 'Branch Name' " & _
            '               ",hrdepartment_master.name AS 'Department Name' " & _
            '               ",hrsection_master.name AS 'Section Name' " & _
            '               ",hrunit_master.name AS 'Unit Name' " & _
            '               ",hrjob_master.job_name AS 'Job Name' " & _
            '        "FROM hremployee_master " & _
            '                "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hremployee_master.stationunkid AND hrstation_master.isactive = 1 " & _
            '                "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
            '                "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid  AND hrsection_master.isactive = 1 " & _
            '                "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hremployee_master.unitunkid AND hrunit_master.isactive = 1 " & _
            '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid AND hrjob_master.isactive = 1 " & _
            '        "WHERE employeeunkid = @Employeeunkid "
            StrQ = "SELECT " & _
                   "     SM.name AS 'Branch Name' " & _
                   "    ,DM.name AS 'Department Name' " & _
                   "    ,SECM.name AS 'Section Name' " & _
                   "    ,UM.name AS 'Unit Name' " & _
                   "    ,JM.job_name AS 'Job Name' " & _
                    "FROM hremployee_master " & _
                   "    LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                stationunkid " & _
                   "                ,departmentunkid " & _
                   "                ,sectionunkid " & _
                   "                ,unitunkid " & _
                   "                ,employeeunkid " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "            FROM hremployee_transfer_tran " & _
                   "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "    LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
                   "    JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                   "    LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
                   "    LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
                   "    LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                jobunkid " & _
                   "                ,employeeunkid " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "            FROM hremployee_categorization_tran " & _
                   "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                   "    JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
                   "WHERE hremployee_master.employeeunkid = @Employeeunkid "

            'Shani(24-Aug-2015) -- End
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If

            intCnt = 0

            If dsList.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                For Each dtColumn As DataColumn In dsList.Tables(0).Columns
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 12, "Employee Allocation")
                    rpt_Rows.Item("Column2") = dtColumn.ColumnName
                    rpt_Rows.Item("Column3") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    rpt_Rows.Item("Column4") = ""
                    rpt_Rows.Item("Column5") = ""

                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    intCnt += 1
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 12, "Employee Allocation")
            End If

            StrQ = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT  prcostcenter_master.costcentername AS 'Cost Center Name' " & _
            '               ",hrgrade_master.name AS 'Grade Name' " & _
            '               ",hrgradelevel_master.name AS 'Level Name' "
            ''Pinkal (24-Apr-2013) -- Start
            ''Enhancement : TRA Changes
            'If mblnShowEmpScale Then
            '    StrQ &= ",scale AS salary "
            'End If
            ''Pinkal (24-Apr-2013) -- End
            'StrQ &= "FROM  hremployee_master " & _
            '                "JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_master.gradeunkid AND hrgrade_master.isactive = 1 " & _
            '                "JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_master.gradelevelunkid  AND hrgradelevel_master.isactive = 1 " & _
            '                "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = hremployee_master.costcenterunkid  AND prcostcenter_master.isactive = 1 " & _
            '        "WHERE employeeunkid = @Employeeunkid "
            StrQ = "SELECT " & _
                   "    CCM.costcentername AS 'Cost Center Name' " & _
                   "    ,GM.name AS 'Grade Name' " & _
                   "    ,GLM.name AS 'Level Name' "
            If mblnShowEmpScale Then
                StrQ &= ",Scale.newscale AS salary "
            End If
            StrQ &= "FROM  hremployee_master " & _
                    "   JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               gradegroupunkid " & _
                    "               ,gradeunkid " & _
                    "               ,gradelevelunkid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                    "           FROM prsalaryincrement_tran " & _
                    "           WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                    "   JOIN hrgrade_master AS GM ON Grds.gradeunkid = GM.gradeunkid " & _
                    "   JOIN hrgradelevel_master AS GLM ON Grds.gradelevelunkid = GLM.gradelevelunkid " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               cctranheadvalueid " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "           FROM hremployee_cctranhead_tran " & _
                    "           WHERE istransactionhead = 0 AND isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "       ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "   JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = CC.cctranheadvalueid " & _
                    "   LEFT JOIN " & _
                    "       ( " & _
                    "           SELECT " & _
                    "               newscale " & _
                    "               ,employeeunkid " & _
                    "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                    "           FROM prsalaryincrement_tran " & _
                    "           WHERE prsalaryincrement_tran.isvoid = 0 AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) < = '" & eZeeDate.convertDate(dtPeriodEnd) & "' AND prsalaryincrement_tran.isapproved = 1 " & _
                    "       ) AS Scale ON Scale.employeeunkid = hremployee_master.employeeunkid AND Scale.rno = 1 " & _
                    "WHERE hremployee_master.employeeunkid = @Employeeunkid "

            'Shani(23-Jan-2016) -- Old --> "WHERE hremployee_master.employeeunkid = 92 " -- > "WHERE hremployee_master.employeeunkid = @Employeeunkid "

            'Shani(24-Aug-2015) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If

            intCnt = 0

            If dsList.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                For Each dtColumn As DataColumn In dsList.Tables(0).Columns

                    If intCnt <> 2 Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    End If


                    rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 13, "Employee Salaries / Wages")
                    If dtColumn.ColumnName <> "Level Name" Then
                        rpt_Rows.Item("Column2") = dtColumn.ColumnName
                        If dtColumn.ColumnName.ToUpper = "SALARY" Then
                            rpt_Rows.Item("Column3") = Format(CDec(dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)), mstrFormateCurrency)
                        Else
                            rpt_Rows.Item("Column3") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                        End If
                    Else
                        rpt_Rows.Item("Column4") = dtColumn.ColumnName
                        rpt_Rows.Item("Column5") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    End If

                    If intCnt <> 2 Then
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If

                    intCnt += 1
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 13, "Employee Salaries / Wages")
            End If


            StrQ = ""
            StrQ = "SELECT  qualificationname AS 'Qualification' " & _
                            ",ISNULL(reference_no,'') AS 'Reference No' " & _
                    "FROM " & _
                            "hremployee_master " & _
                            "LEFT JOIN hremp_qualification_tran ON hremp_qualification_tran.employeeunkid=hremployee_master.employeeunkid AND isvoid=0 " & _
                            "JOIN hrqualification_master ON hrqualification_master.qualificationunkid = hremp_qualification_tran.qualificationunkid AND hremployee_master.isactive=1 " & _
                            "WHERE hremployee_master.employeeunkid = @Employeeunkid "


            dsList = objDataOperation.ExecQuery(StrQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    For i As Integer = 0 To dtRow.ItemArray.Length - 1
                        rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 14, "Employee Qualification & Reference No.")

                        If i = 0 Then
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        Else
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column5") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        End If
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 14, "Employee Qualification & Reference No.")
            End If


            StrQ = ""
            StrQ = " SELECT  hrskill_master.skillname AS 'Skill' " & _
                    "FROM hremployee_master " & _
                        "JOIN hremp_app_skills_tran ON hremp_app_skills_tran.emp_app_unkid = hremployee_master.employeeunkid AND isvoid = 0 " & _
                        "JOIN hrskill_master ON hrskill_master.skillunkid = hremp_app_skills_tran.skillunkid AND hrskill_master.isactive = 1 " & _
                    "WHERE hremployee_master.employeeunkid = @Employeeunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    For i As Integer = 0 To dtRow.ItemArray.Length - 1
                        rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 15, "Employee Skills")

                        rpt_Rows.Item("Column2") = dtRow.Table.Columns(i).ColumnName
                        rpt_Rows.Item("Column3") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 15, "Employee Skills")
            End If



            StrQ = ""
            StrQ = "SELECT  hrmembership_master.membershipname AS 'Membership Name' " & _
                            ",hremployee_meminfo_tran.membershipno AS 'Membership No.' " & _
                    "FROM hremployee_meminfo_tran " & _
                            "LEFT JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid AND hremployee_meminfo_tran.isactive = 1 " & _
                    "WHERE employeeunkid = @Employeeunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    For i As Integer = 0 To dtRow.ItemArray.Length - 1


                        rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 16, "Employee Membership & Membership No.")
                        If i = 0 Then
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        Else
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column5") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        End If
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 16, "Employee Membership & Membership No.")
            End If

            StrQ = ""
            StrQ = "SELECT  cfcommon_master.name AS 'Identity type' " & _
                          ",hremployee_idinfo_tran.identity_no AS 'Identity No' " & _
                    "FROM hremployee_master " & _
                        "LEFT JOIN hremployee_idinfo_tran ON hremployee_master.employeeunkid = hremployee_idinfo_tran.employeeunkid " & _
                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid AND mastertype = " & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & " " & _
                    "WHERE hremployee_master.employeeunkid = @Employeeunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    For i As Integer = 0 To dtRow.ItemArray.Length - 1
                        rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 17, "Employee Identity & Identity No.")
                        If i = 0 Then
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        Else
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column5") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        End If
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 17, "Employee Identity & Identity No.")
            End If

            StrQ = ""
            StrQ = "SELECT hremployee_referee_tran.name AS refreename " & _
                        ",address AS Address " & _
                        ",cfcommon_master.name AS relation " & _
                        ",mobile_no AS mobile_no " & _
                "FROM hremployee_referee_tran " & _
                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_referee_tran.relationunkid AND mastertype=" & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
                "WHERE employeeunkid = @Employeeunkid "

            'S.SANDEEP [05-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-24}
            StrQ &= " AND hremployee_referee_tran.isvoid = 0 "
            'S.SANDEEP [05-Mar-2018] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If

            intCnt = 0

            If dsList.Tables(0).Rows.Count > 0 Then
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                For Each dtColumn As DataColumn In dsList.Tables(0).Columns
                    If intCnt <> 1 And intCnt <> 3 Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    End If

                    rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 18, "Employee Referee")

                    If dtColumn.ColumnName <> "Address" And dtColumn.ColumnName <> "mobile_no" Then
                        rpt_Rows.Item("Column2") = dtColumn.ColumnName
                        rpt_Rows.Item("Column3") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    Else
                        rpt_Rows.Item("Column4") = dtColumn.ColumnName
                        rpt_Rows.Item("Column5") = dsList.Tables(0).Columns(dtColumn.ColumnName).Table(0).Item(intCnt)
                    End If

                    If intCnt <> 1 And intCnt <> 3 Then
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If

                    intCnt += 1
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 18, "Employee Referee")

            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If mintEmployeeId > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeId > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @Employeeunkid "
            End If

            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @AsonDate "
                objDataOperation.AddParameter("@AsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= " SELECT first_name+''+last_name AS dependant " & _
                        " , cfcommon_master.name AS relation " & _
                    "FROM hrdependants_beneficiaries_tran " & _
                    "JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype= " & clsCommon_Master.enCommonMaster.RELATIONS & "  " & _
                    "WHERE employeeunkid = @Employeeunkid AND #TableDepn.isactive = 1 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

            'S.SANDEEP [05-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-24}
            StrQ &= " AND hrdependants_beneficiaries_tran.isvoid = 0 "
            'S.SANDEEP [05-Mar-2018] -- END

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
            End If
            'Sohail (18 May 2019) -- End

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    For i As Integer = 0 To dtRow.ItemArray.Length - 1
                        rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 19, "Employee Dependants & Relation")
                        If i = 0 Then
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        Else
                            rpt_Rows.Item("Column4") = ""
                            rpt_Rows.Item("Column5") = dtRow.Item(dtRow.Table.Columns(i).ColumnName)
                        End If
                    Next
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Data.Tables("ArutiTable").NewRow)
                rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)("Column1") = Language.getMessage(mstrModuleName, 19, "Employee Dependants & Relation")
            End If


            objRpt = New ArutiReport.Designer.rptEmployeeBiodata

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            objEmployeeData = New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeData._Employeeunkid = mintEmployeeId
            objEmployeeData._Employeeunkid(dtPeriodEnd) = mintEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objEImg(0) As Object
            Dim strEmployeeImage As String = ""


            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If mblnIsInImageDb = False Then
                If objEmployeeData._ImagePath <> "" Then
                    If ConfigParameter._Object._PhotoPath.ToString.LastIndexOf("\") = ConfigParameter._Object._PhotoPath.ToString.Length - 1 Then
                        strEmployeeImage = ConfigParameter._Object._PhotoPath & objEmployeeData._ImagePath
                    Else
                        strEmployeeImage = ConfigParameter._Object._PhotoPath & "\" & objEmployeeData._ImagePath
                    End If
                    objEImg(0) = eZeeDataType.image2Data(System.Drawing.Image.FromFile(strEmployeeImage))
                Else
                    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    objEImg(0) = eZeeDataType.image2Data(imgBlank)
                End If

            ElseIf mblnIsInImageDb = True Then

                Dim objEmpImg As New clsemp_Images
                objEmpImg.GetData(Company._Object._Companyunkid, mintEmployeeId, True, Nothing)

                If objEmpImg._Photo IsNot Nothing Then
                    objEImg(0) = objEmpImg._Photo
                Else
                    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    objEImg(0) = eZeeDataType.image2Data(imgBlank)
                End If

            End If

            'Pinkal (03-Apr-2013) -- End

            arrImageRow.Item("guestimage") = objEImg(0)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)



            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End           

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 5, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 6, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            ' Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Printed By :")
            Language.setMessage(mstrModuleName, 6, "Printed Date :")
            Language.setMessage(mstrModuleName, 7, "Print Domicile Address")
            Language.setMessage(mstrModuleName, 8, "Print Present Address")
            Language.setMessage(mstrModuleName, 9, "Employee Details")
            Language.setMessage(mstrModuleName, 10, "Employee Address")
            Language.setMessage(mstrModuleName, 11, "Employee Dates")
            Language.setMessage(mstrModuleName, 12, "Employee Allocation")
            Language.setMessage(mstrModuleName, 13, "Employee Salaries / Wages")
            Language.setMessage(mstrModuleName, 14, "Employee Qualification & Reference No.")
            Language.setMessage(mstrModuleName, 15, "Employee Skills")
            Language.setMessage(mstrModuleName, 16, "Employee Membership & Membership No.")
            Language.setMessage(mstrModuleName, 17, "Employee Identity & Identity No.")
            Language.setMessage(mstrModuleName, 18, "Employee Referee")
            Language.setMessage(mstrModuleName, 19, "Employee Dependants & Relation")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
