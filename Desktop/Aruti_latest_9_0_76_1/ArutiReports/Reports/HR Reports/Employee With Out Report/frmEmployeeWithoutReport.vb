'************************************************************************************************************************************
'Class Name : frmEmployeeWithoutReport.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeWithoutReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeWithoutReport"
    Private objWOR As clsEmployeeWithOutReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        objWOR = New clsEmployeeWithOutReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Dependants Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Qualifications Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Experiences Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "References Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Skills Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Benefits Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Company Assets Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Branches Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Department Groups Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Departments Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 11, "Section Group Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 12, "Section Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 13, "Unit Group Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 14, "Unit Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 15, "Team Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 16, "Job Group Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 17, "Jobs Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 18, "Class Group Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 19, "Classes Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 20, "CostCenter Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 21, "Assessment Assessor Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 22, "Assessment Reviewer Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 23, "Shift Wise"))
                If ConfigParameter._Object._PolicyManagementTNA Then
                    .Items.Add(Language.getMessage(mstrModuleName, 24, "Policy Wise"))
                End If
                'S.SANDEEP |01-MAY-2020| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                If ConfigParameter._Object._IsCalibrationSettingActive Then
                    .Items.Add(Language.getMessage(mstrModuleName, 25, "Calibration Calibrator Wise"))
                    .Items.Add(Language.getMessage(mstrModuleName, 26, "Calibration Approver Wise"))
                End If
                'S.SANDEEP |01-MAY-2020| -- END
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objWOR.SetDefaultValue()

            objWOR._Advance_Filter = mstrAdvanceFilter
            objWOR._Analysis_Fields = mstrAnalysis_Fields
            objWOR._Analysis_Join = mstrAnalysis_Join
            objWOR._Analysis_OrderBy = mstrAnalysis_OrderBy
            objWOR._EmployeeId = cboEmployee.SelectedValue
            objWOR._EmployeeName = cboEmployee.Text
            objWOR._IsActive = chkInactiveemp.Checked
            objWOR._Report_GroupName = mstrReport_GroupName
            'objWOR._ReportTypeId = cboReportType.SelectedIndex
            If cboReportType.Text = Language.getMessage(mstrModuleName, 24, "Policy Wise") Then
                objWOR._ReportTypeId = 23
            ElseIf cboReportType.Text = Language.getMessage(mstrModuleName, 25, "Calibration Calibrator Wise") Then
                objWOR._ReportTypeId = 24
            ElseIf cboReportType.Text = Language.getMessage(mstrModuleName, 26, "Calibration Approver Wise") Then
                objWOR._ReportTypeId = 25
            Else
            objWOR._ReportTypeId = cboReportType.SelectedIndex
            End If

            'Gajanan [31-Aug-2021] -- Start
            objWOR._Is_Name_Consolidate = ChkIs_Name_Consolidate.Checked
            'Gajanan [31-Aug-2021] -- End

            objWOR._ReportTypeName = cboReportType.Text
            objWOR._ViewByIds = mstrStringIds
            objWOR._ViewIndex = mintViewIdx
            objWOR._ViewByName = mstrStringName

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objWOR.setDefaultOrderBy(0)
            txtOrderBy.Text = objWOR.OrderByDisplay
            cboEmployee.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            'Gajanan [31-Aug-2021] -- Start
            ChkIs_Name_Consolidate.Checked = False
            'Gajanan [31-Aug-2021] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeWithoutReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWithoutReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWithoutReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Me._Title = objWOR._ReportName
            Me._Message = objWOR._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWithoutReport_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmployeeWithoutReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeWithoutReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeWithOutReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeWithOutReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub frmEmployeeWithoutReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objWOR.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
            objWOR.generateReportNew(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._ExportReportPath, _
                                     ConfigParameter._Object._OpenAfterExport, _
                                     cboReportType.SelectedIndex, e.Type, _
                                     enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWithoutReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWithoutReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objWOR.generateReport(cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            objWOR.generateReportNew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                      ConfigParameter._Object._UserAccessModeSetting, True, _
                                      ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, _
                                      cboReportType.SelectedIndex, _
                                      enPrintAction.None, _
                                      e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWithoutReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWithoutReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWithoutReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWithoutReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWithoutReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objWOR.setOrderBy(0)
            txtOrderBy.Text = objWOR.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()

            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Dependants Wise")
			Language.setMessage(mstrModuleName, 2, "Qualifications Wise")
			Language.setMessage(mstrModuleName, 3, "Experiences Wise")
			Language.setMessage(mstrModuleName, 4, "References Wise")
			Language.setMessage(mstrModuleName, 5, "Skills Wise")
			Language.setMessage(mstrModuleName, 6, "Benefits Wise")
			Language.setMessage(mstrModuleName, 7, "Company Assets Wise")
			Language.setMessage(mstrModuleName, 8, "Branches Wise")
			Language.setMessage(mstrModuleName, 9, "Department Groups Wise")
			Language.setMessage(mstrModuleName, 10, "Departments Wise")
			Language.setMessage(mstrModuleName, 11, "Section Group Wise")
			Language.setMessage(mstrModuleName, 12, "Section Wise")
			Language.setMessage(mstrModuleName, 13, "Unit Group Wise")
			Language.setMessage(mstrModuleName, 14, "Unit Wise")
			Language.setMessage(mstrModuleName, 15, "Team Wise")
			Language.setMessage(mstrModuleName, 16, "Job Group Wise")
			Language.setMessage(mstrModuleName, 17, "Jobs Wise")
			Language.setMessage(mstrModuleName, 18, "Class Group Wise")
			Language.setMessage(mstrModuleName, 19, "Classes Wise")
			Language.setMessage(mstrModuleName, 20, "CostCenter Wise")
			Language.setMessage(mstrModuleName, 21, "Assessment Assessor Wise")
			Language.setMessage(mstrModuleName, 22, "Assessment Reviewer Wise")
			Language.setMessage(mstrModuleName, 23, "Shift Wise")
			Language.setMessage(mstrModuleName, 24, "Policy Wise")
            Language.setMessage(mstrModuleName, 25, "Calibration Calibrator Wise")
            Language.setMessage(mstrModuleName, 26, "Calibration Approver Wise")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
