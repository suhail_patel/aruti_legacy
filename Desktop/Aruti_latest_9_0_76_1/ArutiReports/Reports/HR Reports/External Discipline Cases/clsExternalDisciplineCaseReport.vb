'************************************************************************************************************************************
'Class Name : clsExternalDisciplineCaseReport.vb
'Purpose    :
'Date       :24-04-12
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsExternalDisciplineCaseReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsExternalDisciplineCaseReport"
    Private mstrReportId As String = enArutiReport.External_Discipline_Cases    '88
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mstrCaseNo As String = String.Empty
    Private mstrCourtName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Private mblnIsActive As Boolean = False
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Properties "

    Public WriteOnly Property _CaseNo() As String
        Set(ByVal value As String)
            mstrCaseNo = value
        End Set
    End Property

    Public WriteOnly Property _CourtName() As String
        Set(ByVal value As String)
            mstrCourtName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrCaseNo = ""
            mstrCourtName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@FDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@TDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtFromDate.Date & " " & _
                               Language.getMessage(mstrModuleName, 2, "To") & " " & mdtToDate & " "

            If mstrCaseNo.Trim <> "" Then
                objDataOperation.AddParameter("@CaseNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrCaseNo & "%")
                Me._FilterQuery &= " AND caseno LIKE @CaseNo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Case No :") & " " & mstrCaseNo & " "
            End If

            If mstrCourtName.Trim <> "" Then
                objDataOperation.AddParameter("@CourtName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrCourtName & "%")
                Me._FilterQuery &= " AND courtname LIKE @CourtName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Court Name :") & " " & mstrCourtName & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("caseno", Language.getMessage(mstrModuleName, 7, "Case No.")))
            iColumn_DetailReport.Add(New IColumn("courtname", Language.getMessage(mstrModuleName, 8, "Court Name")))
            iColumn_DetailReport.Add(New IColumn("hrdiscipline_resolutions.trandate", Language.getMessage(mstrModuleName, 9, "Transaction Date")))
            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 10, "Parties")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 10, "Parties")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 10, "Parties")))
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT " & _
                    "	 caseno AS CASE_NO " & _
                    "	,courtname AS COURT_NAME " & _
                    "	,CONVERT(CHAR(8),hrdiscipline_resolutions.trandate,112) AS TRAN_DATE "
            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 10, "Parties")))
            If mblnFirstNamethenSurname = False Then
                StrQ &= "	,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS PARTIES "
            Else
                StrQ &= "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS PARTIES "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "	,ISNULL(cfcommon_master.name,'')+','+ISNULL(hrdisciplinetype_master.name,'') AS PARTICULARS " & _
            '    "	,ISNULL(LAWYER,'') AS LAWYER " & _
            '    "	,resolutionstep AS STATUS " & _
            '    "	,hrdiscipline_resolutions.resolutionunkid " & _
            '    "	,hrdiscipline_resolutions.trandate as TDate " & _
            '    "FROM hrdiscipline_resolutions " & _
            '    "JOIN hrdiscipline_file ON hrdiscipline_resolutions.disciplinefileunkid = hrdiscipline_file.disciplinefileunkid " & _
            '    "JOIN hrdisciplinetype_master ON dbo.hrdiscipline_file.disciplinetypeunkid = dbo.hrdisciplinetype_master.disciplinetypeunkid " & _
            '    "LEFT JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid " & _
            '    "LEFT JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid " & _
            '    "LEFT JOIN " & _
            '    "( " & _
            '    "	SELECT " & _
            '    "		 resolutionunkid " & _
            '    "		,STUFF((SELECT ' , '+ " & _
            '    "					CASE WHEN hrdiscipline_investigators.committeetranunkid > 0 THEN " & _
            '    "								CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN " & _
            '    "										ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
            '    "									 WHEN hrdiscipline_committee.employeeunkid < 0 THEN " & _
            '    "										ISNULL(hrdiscipline_committee.ex_name,'') END " & _
            '    "						 WHEN hrdiscipline_investigators.committeetranunkid < 0 THEN " & _
            '    "								CASE WHEN hrdiscipline_investigators.employeeunkid > 0 THEN " & _
            '    "										ISNULL(Invest.firstname,'')+' '+ISNULL(Invest.othername,'')+' '+ISNULL(Invest.surname,'') " & _
            '    "									 WHEN hrdiscipline_investigators.employeeunkid < 0 THEN " & _
            '    "										ISNULL(hrdiscipline_investigators.ex_name,'') END END " & _
            '    "	FROM hrdiscipline_investigators " & _
            '    "		LEFT JOIN hremployee_master AS Invest ON hrdiscipline_investigators.employeeunkid = Invest.employeeunkid " & _
            '    "		LEFT JOIN hrdiscipline_committee ON hrdiscipline_investigators.committeetranunkid = hrdiscipline_committee.committeetranunkid " & _
            '    "		LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
            '    "	WHERE investigators.resolutionunkid = hrdiscipline_investigators.resolutionunkid " & _
            '    "	FOR XML PATH('')), 1, 3, '') AS LAWYER " & _
            '    "FROM hrdiscipline_investigators AS investigators " & _
            '    "GROUP BY investigators.resolutionunkid " & _
            '    ") AS B ON B.resolutionunkid = hrdiscipline_resolutions.resolutionunkid " & _
            '    "WHERE caseno <> '' AND CONVERT(CHAR(8),hrdiscipline_resolutions.trandate,112) BETWEEN @FDate AND @TDate "

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If

            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''S.SANDEEP [ 12 MAY 2012 ] -- END

            StrQ &= "	,ISNULL(cfcommon_master.name,'')+','+ISNULL(hrdisciplinetype_master.name,'') AS PARTICULARS " & _
                "	,ISNULL(LAWYER,'') AS LAWYER " & _
                "	,resolutionstep AS STATUS " & _
                "	,hrdiscipline_resolutions.resolutionunkid " & _
                "	,hrdiscipline_resolutions.trandate as TDate " & _
                "FROM hrdiscipline_resolutions " & _
                "JOIN hrdiscipline_file ON hrdiscipline_resolutions.disciplinefileunkid = hrdiscipline_file.disciplinefileunkid " & _
                "JOIN hrdisciplinetype_master ON dbo.hrdiscipline_file.disciplinetypeunkid = dbo.hrdisciplinetype_master.disciplinetypeunkid " & _
                "LEFT JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid " & _
                "LEFT JOIN hremployee_master ON hrdiscipline_file.involved_employeeunkid = hremployee_master.employeeunkid " & _
                "LEFT JOIN " & _
                "( " & _
                "	SELECT " & _
                "		 resolutionunkid " & _
                "		,STUFF((SELECT ' , '+ " & _
                "					CASE WHEN hrdiscipline_investigators.committeetranunkid > 0 THEN " & _
                "								CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN " & _
                "										ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                "									 WHEN hrdiscipline_committee.employeeunkid < 0 THEN " & _
                "										ISNULL(hrdiscipline_committee.ex_name,'') END " & _
                "						 WHEN hrdiscipline_investigators.committeetranunkid < 0 THEN " & _
                "								CASE WHEN hrdiscipline_investigators.employeeunkid > 0 THEN " & _
                "										ISNULL(Invest.firstname,'')+' '+ISNULL(Invest.othername,'')+' '+ISNULL(Invest.surname,'') " & _
                "									 WHEN hrdiscipline_investigators.employeeunkid < 0 THEN " & _
                "										ISNULL(hrdiscipline_investigators.ex_name,'') END END " & _
                "	FROM hrdiscipline_investigators " & _
                "		LEFT JOIN hremployee_master AS Invest ON hrdiscipline_investigators.employeeunkid = Invest.employeeunkid " & _
                "		LEFT JOIN hrdiscipline_committee ON hrdiscipline_investigators.committeetranunkid = hrdiscipline_committee.committeetranunkid " & _
                "		LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
                "	WHERE investigators.resolutionunkid = hrdiscipline_investigators.resolutionunkid " & _
                "	FOR XML PATH('')), 1, 3, '') AS LAWYER " & _
                "FROM hrdiscipline_investigators AS investigators " & _
                "GROUP BY investigators.resolutionunkid " & _
                ") AS B ON B.resolutionunkid = hrdiscipline_resolutions.resolutionunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE caseno <> '' AND CONVERT(CHAR(8),hrdiscipline_resolutions.trandate,112) BETWEEN @FDate AND @TDate "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("CASE_NO")
                rpt_Rows.Item("Column2") = dtRow.Item("COURT_NAME")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("TRAN_DATE").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("PARTIES")
                rpt_Rows.Item("Column5") = dtRow.Item("PARTICULARS")
                rpt_Rows.Item("Column6") = dtRow.Item("LAWYER")
                rpt_Rows.Item("Column7") = dtRow.Item("STATUS")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            objRpt = New ArutiReport.Designer.rptExternalDisciplineReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 18, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 19, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 20, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 21, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 9, "Transaction Date"))
            Call ReportFunction.TextChange(objRpt, "txtParties", Language.getMessage(mstrModuleName, 10, "Parties"))
            Call ReportFunction.TextChange(objRpt, "txtParticulars", Language.getMessage(mstrModuleName, 11, "Particulars"))
            Call ReportFunction.TextChange(objRpt, "txtLawyers", Language.getMessage(mstrModuleName, 12, "Lawyer"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 13, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 7, "Case No."))
            Call ReportFunction.TextChange(objRpt, "txtCourtName", Language.getMessage(mstrModuleName, 8, "Court Name"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Date :")
            Language.setMessage(mstrModuleName, 2, "To")
            Language.setMessage(mstrModuleName, 3, "Case No :")
            Language.setMessage(mstrModuleName, 4, "Court Name :")
            Language.setMessage(mstrModuleName, 5, "Employee :")
            Language.setMessage(mstrModuleName, 6, "Order By :")
            Language.setMessage(mstrModuleName, 7, "Case No.")
            Language.setMessage(mstrModuleName, 8, "Court Name")
            Language.setMessage(mstrModuleName, 9, "Transaction Date")
            Language.setMessage(mstrModuleName, 10, "Parties")
            Language.setMessage(mstrModuleName, 11, "Particulars")
            Language.setMessage(mstrModuleName, 12, "Lawyer")
            Language.setMessage(mstrModuleName, 13, "Status")
            Language.setMessage(mstrModuleName, 14, "Sub Total :")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")
            Language.setMessage(mstrModuleName, 17, "Page :")
            Language.setMessage(mstrModuleName, 18, "Prepared By :")
            Language.setMessage(mstrModuleName, 19, "Checked By :")
            Language.setMessage(mstrModuleName, 20, "Approved By :")
            Language.setMessage(mstrModuleName, 21, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
