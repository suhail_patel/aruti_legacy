#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class clsDisciplineAppealReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDisciplineAppealReport"
    Private mstrReportId As String = enArutiReport.DisciplineAppealReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtChargeDateFrom As Date = Nothing
    Private mdtChargeDateTo As Date = Nothing
    Private mintOffenceCategoryId As Integer = 0
    Private mstrOffenceCategory As String = ""
    Private mintOffenceDescrId As Integer = 0
    Private mstrOffenceDescription As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnApplyAccessFilter As Boolean = True
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvance_Filter As String = ""
    Private mintActionId As Integer = 0
    Private mstrActionName As String = ""
'S.SANDEEP |10-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
    Private mblnShowAllocationBasedOnChargeDate As Boolean = False
    Private mstrShowAllocationBasedOnChargeDateString As String = ""
    'S.SANDEEP |10-JUN-2020| -- END

    Private mblnIncludeInactiveEmployee As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ChargeDateFrom() As Date
        Set(ByVal value As Date)
            mdtChargeDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _ChargeDateTo() As Date
        Set(ByVal value As Date)
            mdtChargeDateTo = value
        End Set
    End Property

    Public WriteOnly Property _OffenceCategoryId() As Integer
        Set(ByVal value As Integer)
            mintOffenceCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _OffenceCategory() As String
        Set(ByVal value As String)
            mstrOffenceCategory = value
        End Set
    End Property

    Public WriteOnly Property _OffenceDescrId() As Integer
        Set(ByVal value As Integer)
            mintOffenceDescrId = value
        End Set
    End Property

    Public WriteOnly Property _OffenceDescription() As String
        Set(ByVal value As String)
            mstrOffenceDescription = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ActionId() As Integer
        Set(ByVal value As Integer)
            mintActionId = value
        End Set
    End Property

    Public WriteOnly Property _ActionName() As String
        Set(ByVal value As String)
            mstrActionName = value
        End Set
    End Property

'S.SANDEEP |10-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
    Public WriteOnly Property _ShowAllocationBasedOnChargeDate() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAllocationBasedOnChargeDate = value
        End Set
    End Property
    Public WriteOnly Property _ShowAllocationBasedOnChargeDateString() As String
        Set(ByVal value As String)
            mstrShowAllocationBasedOnChargeDateString = value
        End Set
    End Property
    'S.SANDEEP |10-JUN-2020| -- END

    Public WriteOnly Property _IncludeInactiveEmployee() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmployee = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtChargeDateFrom = Nothing
            mdtChargeDateTo = Nothing
            mintOffenceCategoryId = 0
            mstrOffenceCategory = ""
            mintOffenceDescrId = 0
            mstrOffenceDescription = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnApplyAccessFilter = True
            Rpt = Nothing
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrAdvance_Filter = ""
            mintActionId = 0
            mstrActionName = ""
 'S.SANDEEP |10-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
            mblnShowAllocationBasedOnChargeDate = False
            mstrShowAllocationBasedOnChargeDateString = ""
            'S.SANDEEP |10-JUN-2020| -- END

            mblnIncludeInactiveEmployee = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 106, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 107, "No"))

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EM.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Employee :") & " " & mstrEmployeeName & " "
            End If
            If mintOffenceCategoryId > 0 Then
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOffenceCategoryId)
                Me._FilterQuery &= " AND CM.masterunkid = @masterunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Offence Category :") & " " & mstrOffenceCategory & " "
            End If
            If mintOffenceDescrId > 0 Then
                objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOffenceDescrId)
                Me._FilterQuery &= " AND DS.disciplinetypeunkid = @disciplinetypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Offence :") & " " & mstrOffenceDescription & " "
            End If
            If mdtChargeDateFrom <> Nothing AndAlso mdtChargeDateTo <> Nothing Then
                objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtChargeDateFrom))
                objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtChargeDateTo))
                Me._FilterQuery &= " AND RP.appldate BETWEEN @Date1 AND @Date2 "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, "Appeal Date :") & " " & mdtChargeDateFrom.Date.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 104, "To :") & " " & mdtChargeDateTo.Date.ToShortDateString & " "
            End If
            If mintActionId > 0 Then
                objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionId)
                Me._FilterQuery &= " AND RP.actionreasonunkid = @actionreasonunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 105, "Appeal Outcome :") & " " & mstrActionName & " "
            End If

            'S.SANDEEP |10-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
            If mblnShowAllocationBasedOnChargeDate Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 106, "Display Allocation Based On :") & " " & mstrShowAllocationBasedOnChargeDateString
            End If
            'S.SANDEEP |10-JUN-2020| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName, "EM")
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting, "EM")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName, "EM")

            If mblnShowAllocationBasedOnChargeDate Then
                StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                       "DROP TABLE #Results " & _
                       "CREATE TABLE #Results " & _
                       "( " & _
                       "    empid int, " & _
                       "    edate nvarchar(8), " & _
                       "    clsgrp nvarchar(max), " & _
                       "    cls nvarchar(max), " & _
                       "    ejob nvarchar(max) " & _
                       ") " & _
                       "DECLARE @Ddate  NVARCHAR(8), @EmpId INT " & _
                       "DECLARE discpl_alloc CURSOR " & _
                       "FOR " & _
                       "SELECT DISTINCT " & _
                       "     CONVERT(NVARCHAR(8),chargedate,112) AS efdate " & _
                       "    ,involved_employeeunkid as empid " & _
                       "FROM hrdiscipline_file_master " & _
                       "WHERE isvoid = 0 " & _
                       "OPEN discpl_alloc " & _
                       "FETCH NEXT FROM discpl_alloc INTO @Ddate,@EmpId " & _
                       "WHILE @@FETCH_STATUS = 0 " & _
                       "BEGIN " & _
                       "DECLARE @Q AS NVARCHAR(MAX) " & _
                       "SET @Q = 'INSERT INTO #Results(empid,edate,clsgrp,cls,ejob) " & _
                       "    SELECT " & _
                       "         EM.employeeunkid " & _
                       "        ,'''+@Ddate+''' " & _
                       "        ,ISNULL(ECG.name,'''') as clsgrp " & _
                       "        ,ISNULL(ECL.name,'''') as cls " & _
                       "        ,ISNULL(EJM.job_name,'''') as ejob " & _
                       "    FROM hremployee_master AS EM " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             CT.jobunkid " & _
                       "            ,CT.employeeunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                       "        FROM hremployee_categorization_tran AS CT " & _
                       "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '''+@Ddate+''' " & _
                       "        AND CT.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                       "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                       "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ET.classgroupunkid " & _
                       "            ,ET.classunkid " & _
                       "            ,ET.employeeunkid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                       "        FROM hremployee_transfer_tran AS ET " & _
                       "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '''+@Ddate+''' " & _
                       "        AND ET.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) + ' " & _
                       "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                       "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid " & _
                       "    LEFT JOIN hrclasses_master AS ECL ON EC.classunkid = ECL.classesunkid " & _
                       "    WHERE EM.employeeunkid = ' + CAST(@EmpId as NVARCHAR(10)) " & _
                       "    EXEC(@Q) " & _
                       "FETCH NEXT FROM discpl_alloc INTO @Ddate,@EmpId " & _
                       "END " & _
                       "CLOSE discpl_alloc " & _
                       "DEALLOCATE discpl_alloc " & _
                       "SELECT " & _
                       "     EM.firstname+' '+EM.surname AS employee " & _
                       "    ,#Results.clsgrp " & _
                       "    ,#Results.cls " & _
                       "    ,#Results.ejob " & _
                       "    ,CM.name as offcat " & _
                       "    ,DS.name as offdes " & _
                       "    ,CASE WHEN RP.disciplinestatusunkid = 3 THEN @Yes ELSE @No END AS appeal " & _
                       "    ,RP.appldate AS appealdate " & _
                       "    ,FM.reference_no " & _
                       "    ,RP.applaction AS outcome " & _
                       "    ,RP.actionreasonunkid " & _
                       "    ,ISNULL(RP.oreason,'') AS oreason "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'StrQ &= "FROM #Results " & _
                '        "    JOIN hrdiscipline_file_master AS FM ON FM.involved_employeeunkid = #Results.empid AND #Results.edate = CONVERT(CHAR(8),FM.chargedate,112) " & _
                '        "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
                '        "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                '        "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
                '        "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid " & _
                '        "    JOIN " & _
                '        "    ( " & _
                '        "       SELECT " & _
                '        "            hrdiscipline_status_tran.disciplinefileunkid " & _
                '        "           ,hrdiscipline_status_tran.disciplinestatusunkid " & _
                '        "           ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_status_tran.disciplinefileunkid ORDER BY hrdiscipline_status_tran.statusdate DESC) AS rno " & _
                '        "           ,CONVERT(CHAR(8),ISNULL(PMT.proceeding_date,PMP.proceeding_date),112) AS appldate " & _
                '        "           ,ISNULL(ISNULL(PMTR.reason_action,PMPR.reason_action),'') AS applaction " & _
                '        "           ,ISNULL(ISNULL(PMTR.actionreasonunkid,PMPR.actionreasonunkid),0) AS actionreasonunkid " & _
                '        "           ,ISNULL(CM.name,'') AS oreason " & _
                '        "       FROM hrdiscipline_status_tran " & _
                '        "           LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = hrdiscipline_status_tran.openreasonunkid " & _
                '        "           JOIN hrdiscipline_file_master AS FM ON FM.disciplinefileunkid = hrdiscipline_status_tran.disciplinefileunkid " & _
                '        "           JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                '        "           LEFT JOIN hrdiscipline_proceeding_master AS PMT ON PMT.disciplinefiletranunkid = FT.disciplinefiletranunkid AND PMT.isvoid = 0 " & _
                '        "           LEFT JOIN hraction_reason_master AS PMTR ON PMT.actionreasonunkid = PMTR.actionreasonunkid AND PMTR.isactive = 1 " & _
                '        "           LEFT JOIN hrdiscipline_proceeding_master AS PMP ON PMP.disciplinefileunkid = FM.disciplinefileunkid AND PMP.isvoid = 0 " & _
                '        "           LEFT JOIN hraction_reason_master AS PMPR ON PMP.actionreasonunkid = PMPR.actionreasonunkid AND PMPR.isactive = 1 " & _
                '        "       WHERE hrdiscipline_status_tran.isvoid = 0 " & _
                '        "   ) AS RP ON RP.disciplinefileunkid = FM.disciplinefileunkid AND RP.rno = 1 AND RP.disciplinestatusunkid = 3 "


                StrQ &= "FROM #Results " & _
                        "    JOIN hrdiscipline_file_master AS FM ON FM.involved_employeeunkid = #Results.empid AND #Results.edate = CONVERT(CHAR(8),FM.chargedate,112) " & _
                        "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
                        "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                        "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
                        "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid " & _
                        "    JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            hrdiscipline_status_tran.disciplinefileunkid " & _
                        "           ,hrdiscipline_status_tran.disciplinestatusunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_status_tran.disciplinefileunkid ORDER BY hrdiscipline_status_tran.statusdate DESC) AS rno " & _
                        "           ,ISNULL(CONVERT(CHAR(8), ISNULL(PMT.proceeding_date, CASE WHEN PMT.disciplinefiletranunkid <= 0 THEN PMP.proceeding_date ELSE NULL END), 112), '') AS appldate " & _
                        "           ,ISNULL(ISNULL(PMTR.reason_action, CASE WHEN PMT.disciplinefiletranunkid <= 0 THEN PMPR.reason_action ELSE '' END), '') AS applaction " & _
                        "           ,ISNULL(ISNULL(PMTR.actionreasonunkid,CASE WHEN PMT.disciplinefiletranunkid <= 0 THEN PMPR.actionreasonunkid ELSE 0 END),0) AS actionreasonunkid " & _
                        "           ,ISNULL(CM.name,'') AS oreason " & _
                        "       FROM hrdiscipline_status_tran " & _
                        "           LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = hrdiscipline_status_tran.openreasonunkid " & _
                        "           JOIN hrdiscipline_file_master AS FM ON FM.disciplinefileunkid = hrdiscipline_status_tran.disciplinefileunkid " & _
                        "           JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                        "           LEFT JOIN hrdiscipline_proceeding_master AS PMT ON PMT.disciplinefiletranunkid = FT.disciplinefiletranunkid AND PMT.isvoid = 0 " & _
                        "           LEFT JOIN hraction_reason_master AS PMTR ON PMT.actionreasonunkid = PMTR.actionreasonunkid AND PMTR.isactive = 1 " & _
                        "           LEFT JOIN hrdiscipline_proceeding_master AS PMP ON PMP.disciplinefileunkid = FM.disciplinefileunkid AND PMP.isvoid = 0 " & _
                        "           LEFT JOIN hraction_reason_master AS PMPR ON PMP.actionreasonunkid = PMPR.actionreasonunkid AND PMPR.isactive = 1 " & _
                        "       WHERE hrdiscipline_status_tran.isvoid = 0 " & _
                        "   ) AS RP ON RP.disciplinefileunkid = FM.disciplinefileunkid AND RP.rno = 1 AND RP.disciplinestatusunkid = 3 "

                'S.SANDEEP |01-MAY-2020| -- START - MODIFICATION ON REPORT (ADDTIONAL COLUMNS) {LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = hrdiscipline_status_tran.openreasonunkid, oreason} -- END

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE FM.isvoid = 0 AND FT.isvoid = 0 "

                If mblnIncludeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
                End If

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= " ORDER BY FM.reference_no "

                StrQ &= "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                      "DROP TABLE #Results "
            Else
                StrQ = "SELECT " & _
                       "     EM.firstname+' '+EM.surname AS employee " & _
                       "    ,ECG.name as clsgrp " & _
                       "    ,ECL.name as cls " & _
                       "    ,EJM.job_name as ejob " & _
                       "    ,CM.name as offcat " & _
                       "    ,DS.name as offdes " & _
                       "    ,CASE WHEN RP.disciplinestatusunkid = 3 THEN @Yes ELSE @No END AS appeal " & _
                       "    ,RP.appldate AS appealdate " & _
                       "    ,FM.reference_no " & _
                       "    ,RP.applaction AS outcome " & _
                       "    ,RP.actionreasonunkid " & _
                       "    ,ISNULL(RP.oreason,'') AS oreason "
                'S.SANDEEP |01-MAY-2020| -- START - MODIFICATION ON REPORT (ADDTIONAL COLUMNS) {oreason} -- END
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'StrQ &= "FROM hrdiscipline_file_master AS FM " & _
                '        "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
                '        "    LEFT JOIN " & _
                '        "    ( " & _
                '        "        SELECT " & _
                '        "             CT.jobunkid " & _
                '        "            ,CT.employeeunkid " & _
                '        "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                '        "        FROM hremployee_categorization_tran AS CT " & _
                '        "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                '        "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                '        "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                '        "    LEFT JOIN " & _
                '        "    ( " & _
                '        "        SELECT " & _
                '        "             ET.classgroupunkid " & _
                '        "            ,ET.classunkid " & _
                '        "            ,ET.employeeunkid " & _
                '        "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                '        "        FROM hremployee_transfer_tran AS ET " & _
                '        "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                '        "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                '        "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid " & _
                '        "    LEFT JOIN hrclasses_master AS ECL ON EC.classunkid = ECL.classesunkid " & _
                '        "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                '        "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
                '        "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid " & _
                '        "    JOIN " & _
                '        "    ( " & _
                '        "       SELECT " & _
                '        "            hrdiscipline_status_tran.disciplinefileunkid " & _
                '        "           ,hrdiscipline_status_tran.disciplinestatusunkid " & _
                '        "           ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_status_tran.disciplinefileunkid ORDER BY hrdiscipline_status_tran.statusdate DESC) AS rno " & _
                '        "           ,CONVERT(CHAR(8),ISNULL(PMT.proceeding_date,PMP.proceeding_date),112) AS appldate " & _
                '        "           ,ISNULL(ISNULL(PMTR.reason_action,PMPR.reason_action),'') AS applaction " & _
                '        "           ,ISNULL(ISNULL(PMTR.actionreasonunkid,PMPR.actionreasonunkid),0) AS actionreasonunkid " & _
                '        "           ,ISNULL(CM.name,'') AS oreason " & _
                '        "       FROM hrdiscipline_status_tran " & _
                '        "           LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = hrdiscipline_status_tran.openreasonunkid " & _
                '        "           JOIN hrdiscipline_file_master AS FM ON FM.disciplinefileunkid = hrdiscipline_status_tran.disciplinefileunkid " & _
                '        "           JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                '        "           LEFT JOIN hrdiscipline_proceeding_master AS PMT ON PMT.disciplinefiletranunkid = FT.disciplinefiletranunkid AND PMT.isvoid = 0 " & _
                '        "           LEFT JOIN hraction_reason_master AS PMTR ON PMT.actionreasonunkid = PMTR.actionreasonunkid AND PMTR.isactive = 1 " & _
                '        "           LEFT JOIN hrdiscipline_proceeding_master AS PMP ON PMP.disciplinefileunkid = FM.disciplinefileunkid AND PMP.isvoid = 0 " & _
                '        "           LEFT JOIN hraction_reason_master AS PMPR ON PMP.actionreasonunkid = PMPR.actionreasonunkid AND PMPR.isactive = 1 " & _
                '        "       WHERE hrdiscipline_status_tran.isvoid = 0 " & _
                '        "   ) AS RP ON RP.disciplinefileunkid = FM.disciplinefileunkid AND RP.rno = 1 AND RP.disciplinestatusunkid = 3 "

                StrQ &= "FROM hrdiscipline_file_master AS FM " & _
                        "    JOIN hremployee_master AS EM ON EM.employeeunkid = FM.involved_employeeunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             CT.jobunkid " & _
                        "            ,CT.employeeunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS rno " & _
                        "        FROM hremployee_categorization_tran AS CT " & _
                        "        WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS EJ ON EM.employeeunkid = EJ.employeeunkid AND EJ.rno = 1 " & _
                        "    LEFT JOIN hrjob_master AS EJM ON EJ.jobunkid = EJM.jobunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             ET.classgroupunkid " & _
                        "            ,ET.classunkid " & _
                        "            ,ET.employeeunkid " & _
                        "            ,ROW_NUMBER()OVER(PARTITION BY ET.employeeunkid ORDER BY ET.effectivedate DESC) AS rno " & _
                        "        FROM hremployee_transfer_tran AS ET " & _
                        "        WHERE ET.isvoid = 0 AND CONVERT(CHAR(8),ET.effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString() & "' " & _
                        "    ) AS EC ON EM.employeeunkid = EC.employeeunkid AND EC.rno = 1 " & _
                        "    LEFT JOIN hrclassgroup_master AS ECG ON EC.classgroupunkid = ECG.classgroupunkid " & _
                        "    LEFT JOIN hrclasses_master AS ECL ON EC.classunkid = ECL.classesunkid " & _
                        "    JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                        "    JOIN hrdisciplinetype_master AS DS ON DS.disciplinetypeunkid = FT.offenceunkid " & _
                        "    JOIN cfcommon_master AS CM ON CM.masterunkid = DS.offencecategoryunkid " & _
                        "    JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            hrdiscipline_status_tran.disciplinefileunkid " & _
                        "           ,hrdiscipline_status_tran.disciplinestatusunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY hrdiscipline_status_tran.disciplinefileunkid ORDER BY hrdiscipline_status_tran.statusdate DESC) AS rno " & _
                        "           ,ISNULL(CONVERT(CHAR(8), ISNULL(PMT.proceeding_date, CASE WHEN PMT.disciplinefiletranunkid <= 0 THEN PMP.proceeding_date ELSE NULL END), 112), '') AS appldate " & _
                        "           ,ISNULL(ISNULL(PMTR.reason_action, CASE WHEN PMT.disciplinefiletranunkid <= 0 THEN PMPR.reason_action ELSE '' END), '') AS applaction " & _
                        "           ,ISNULL(ISNULL(PMTR.actionreasonunkid,CASE WHEN PMT.disciplinefiletranunkid <= 0 THEN PMPR.actionreasonunkid ELSE 0 END),0) AS actionreasonunkid " & _
                        "           ,ISNULL(CM.name,'') AS oreason " & _
                        "       FROM hrdiscipline_status_tran " & _
                        "           LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = hrdiscipline_status_tran.openreasonunkid " & _
                        "           JOIN hrdiscipline_file_master AS FM ON FM.disciplinefileunkid = hrdiscipline_status_tran.disciplinefileunkid " & _
                        "           JOIN hrdiscipline_file_tran AS FT ON FM.disciplinefileunkid = FT.disciplinefileunkid " & _
                        "           LEFT JOIN hrdiscipline_proceeding_master AS PMT ON PMT.disciplinefiletranunkid = FT.disciplinefiletranunkid AND PMT.isvoid = 0 " & _
                        "           LEFT JOIN hraction_reason_master AS PMTR ON PMT.actionreasonunkid = PMTR.actionreasonunkid AND PMTR.isactive = 1 " & _
                        "           LEFT JOIN hrdiscipline_proceeding_master AS PMP ON PMP.disciplinefileunkid = FM.disciplinefileunkid AND PMP.isvoid = 0 " & _
                        "           LEFT JOIN hraction_reason_master AS PMPR ON PMP.actionreasonunkid = PMPR.actionreasonunkid AND PMPR.isactive = 1 " & _
                        "       WHERE hrdiscipline_status_tran.isvoid = 0 " & _
                        "   ) AS RP ON RP.disciplinefileunkid = FM.disciplinefileunkid AND RP.rno = 1 AND RP.disciplinestatusunkid = 3 "
                'S.SANDEEP |01-MAY-2020| -- START - MODIFICATION ON REPORT (ADDTIONAL COLUMNS) {LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = hrdiscipline_status_tran.openreasonunkid, oreason} -- END

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= mstrAnalysis_Join

                StrQ &= " WHERE FM.isvoid = 0 AND FT.isvoid = 0 "

                If mblnIncludeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
                End If

                Call FilterTitleAndFilterQuery()

                StrQ &= Me._FilterQuery

                StrQ &= " ORDER BY FM.reference_no "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("employee")
                rpt_Row.Item("Column2") = dtRow.Item("clsgrp")
                rpt_Row.Item("Column3") = dtRow.Item("cls")
                rpt_Row.Item("Column4") = dtRow.Item("ejob")
                rpt_Row.Item("Column5") = dtRow.Item("offcat")
                rpt_Row.Item("Column6") = dtRow.Item("offdes")
                rpt_Row.Item("Column7") = dtRow.Item("appeal")
                If dtRow.Item("appealdate").ToString.Trim.Length > 0 Then
                rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("appealdate").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column8") = ""
                End If
                rpt_Row.Item("Column9") = dtRow.Item("reference_no")
                rpt_Row.Item("Column10") = dtRow("outcome")
                rpt_Row.Item("Column13") = dtRow.Item("GName")
                'S.SANDEEP |01-MAY-2020| -- START
                'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
                rpt_Row.Item("Column14") = dtRow.Item("oreason")
                'S.SANDEEP |01-MAY-2020| -- END

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            objRpt = New ArutiReport.Designer.rptDisciplineAppealReport

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

            ReportFunction.Logo_Display(objRpt, _
                                       ConfigParameter._Object._IsDisplayLogo, _
                                       ConfigParameter._Object._ShowLogoRightSide, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "txtCompanyName", _
                                       "txtReportName", _
                                       "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            Call ReportFunction.TextChange(objRpt, "txtSr", Language.getMessage(mstrModuleName, 200, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 201, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtClassGrp", Language.getMessage("clsMasterData", 420, "Class Group"))
            Call ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage("clsMasterData", 419, "Classes"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage("clsMasterData", 421, "Jobs"))
            Call ReportFunction.TextChange(objRpt, "txtOffenceCategory", Language.getMessage(mstrModuleName, 202, "Offence Category"))
            Call ReportFunction.TextChange(objRpt, "txtOffenceDescr", Language.getMessage(mstrModuleName, 203, "Nature of allegations"))
            Call ReportFunction.TextChange(objRpt, "txtChargeDate", Language.getMessage(mstrModuleName, 204, "Appeal Date"))
            Call ReportFunction.TextChange(objRpt, "txtRefNo", Language.getMessage(mstrModuleName, 205, "Reference No"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtOutCome", Language.getMessage(mstrModuleName, 208, "Appeal Outcome"))
            Call ReportFunction.TextChange(objRpt, "txtAppeal", Language.getMessage(mstrModuleName, 209, "Appeal"))
            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
            Call ReportFunction.TextChange(objRpt, "txtReason", Language.getMessage(mstrModuleName, 210, "Opening Reason"))
            'S.SANDEEP |01-MAY-2020| -- END

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 206, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 207, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return objRpt
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage(mstrModuleName, 100, "Employee :")
            Language.setMessage(mstrModuleName, 101, "Offence Category :")
            Language.setMessage(mstrModuleName, 102, "Offence :")
            Language.setMessage(mstrModuleName, 103, "Appeal Date :")
            Language.setMessage(mstrModuleName, 104, "To :")
            Language.setMessage(mstrModuleName, 105, "Appeal Outcome :")
            Language.setMessage(mstrModuleName, 106, "Yes")
            Language.setMessage(mstrModuleName, 107, "No")
            Language.setMessage(mstrModuleName, 200, "Sr.No")
            Language.setMessage(mstrModuleName, 201, "Employee")
            Language.setMessage(mstrModuleName, 202, "Offence Category")
            Language.setMessage(mstrModuleName, 203, "Nature of allegations")
            Language.setMessage(mstrModuleName, 204, "Appeal Date")
            Language.setMessage(mstrModuleName, 205, "Reference No")
            Language.setMessage(mstrModuleName, 206, "Printed By :")
            Language.setMessage(mstrModuleName, 207, "Printed Date :")
            Language.setMessage(mstrModuleName, 208, "Appeal Outcome")
            Language.setMessage(mstrModuleName, 209, "Appeal")
            Language.setMessage(mstrModuleName, 210, "Opening Reason")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
