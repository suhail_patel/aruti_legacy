'************************************************************************************************************************************
'Class Name : clsSeniorityLevelReprot.vb
'Purpose    : KBC/ TNP
'Date       : 01/04/2016
'Written By : Shani
'Modified   :
'************************************************************************************************************************************

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO
Imports ExcelWriter

#End Region

Public Class clsEmployeeSeniorityReport
    Inherits IReportData

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeSeniorityReport"
    Private mstrReportId As String = enArutiReport.Employee_Seniority_Report
    Private mstrFinalPath As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrHighestQualificationIds As String = ""
    Private mstrProfessionQualificationIds As String = ""
    Private mstrDispalyColumnIds As String = ""
    Private mdtExcelTable As DataTable
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    'Gajanan [31-Aug-2021] -- Start
    Private mblnIsNameConsolidate As Boolean = False
    'Gajanan [31-Aug-2021] -- End

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Enum"
    Public Enum enDisplay_Column
        BRANCH = 1
        DEPARTMENT_GROUP = 2
        DEPARTMENT = 3
        SECTION_GROUP = 4
        SECTION = 5
        JOB_GROUP = 6
        JOB = 7
        CLASS_GROUP = 8
        CLASSES = 9
        GRADE = 10
        GRADE_LEVEL = 11
        SCALE = 12
        EMPLOYMENT_TYPE = 13
        DATE_HIRED = 14
        BIRTHDATE = 15
        GENDER = 16
        CONFIRMATION_DATE = 17
        ETHINICITY = 18
        DISABLILITY = 19
        HIGHEST_QUALIFICATION = 20
        PROFESSION_QUALIFICATION = 21
    End Enum
#End Region

#Region " Properties "
    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Highest_QualificationIds() As String
        Set(ByVal value As String)
            mstrHighestQualificationIds = value
        End Set
    End Property

    Public WriteOnly Property _Profession_QualificationIds() As String
        Set(ByVal value As String)
            mstrProfessionQualificationIds = value
        End Set
    End Property

    Public WriteOnly Property _Display_ColumnsIds() As String
        Set(ByVal value As String)
            mstrDispalyColumnIds = value
        End Set
    End Property

    'Gajanan [31-Aug-2021] -- Start
    Public WriteOnly Property _Is_Name_Consolidate() As Boolean
        Set(ByVal value As Boolean)
            mblnIsNameConsolidate = value
        End Set
    End Property
    'Gajanan [31-Aug-2021] -- End

#End Region

#Region "Private Funcation"
    Public Function GetDisplayColumn(ByVal StrList As String) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation
            StrQ = "          SELECT " & enDisplay_Column.BRANCH & " AS Id, @BRANCH AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.DEPARTMENT & " AS Id, @DEPARTMENT AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.SECTION & " AS Id, @SECTION AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.JOB & " AS Id, @JOB AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.HIGHEST_QUALIFICATION & " AS Id, @HIGHEST_QUALIFICATION AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.PROFESSION_QUALIFICATION & " AS Id, @PROFESSION_QUALIFICATION AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.GRADE_LEVEL & " AS Id, @GRADE_LEVEL AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.SCALE & " AS Id, @SCALE AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.EMPLOYMENT_TYPE & " AS Id, @EMPLOYMENT_TYPE AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.DATE_HIRED & " AS Id, @DATE_HIRED AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.BIRTHDATE & " AS Id, @BIRTHDATE AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.GENDER & " AS Id, @GENDER AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.DEPARTMENT_GROUP & " AS Id, @DEPARTMENT_GROUP AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.SECTION_GROUP & " AS Id, @SECTION_GROUP AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.JOB_GROUP & " AS Id, @JOB_GROUP AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.CLASS_GROUP & " AS Id, @CLASS_GROUP AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.CLASSES & " AS Id, @CLASSES AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.CONFIRMATION_DATE & " AS Id, @CONFIRMATION_DATE AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.GRADE & " AS Id, @GRADE AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.ETHINICITY & " AS Id, @ETHINICITY AS Name " & _
                   "    UNION SELECT " & enDisplay_Column.DISABLILITY & " AS Id, @DISABLILITY AS Name "


            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Section"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Job Group"))
            objDataOperation.AddParameter("@JOB", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Job"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Class"))
            objDataOperation.AddParameter("@GRADE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Grade"))
            objDataOperation.AddParameter("@GRADE_LEVEL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Grade Level"))
            objDataOperation.AddParameter("@SCALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Scale"))
            objDataOperation.AddParameter("@EMPLOYMENT_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Employee Type"))
            objDataOperation.AddParameter("@DATE_HIRED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Date Hired"))
            objDataOperation.AddParameter("@BIRTHDATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Birthdate"))
            objDataOperation.AddParameter("@GENDER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Gender"))
            objDataOperation.AddParameter("@CONFIRMATION_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Confirmation Date"))
            objDataOperation.AddParameter("@ETHINICITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Ethinicity"))
            objDataOperation.AddParameter("@DISABLILITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Disablility"))
            objDataOperation.AddParameter("@HIGHEST_QUALIFICATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Highest Qualification"))
            objDataOperation.AddParameter("@PROFESSION_QUALIFICATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Profession Qualification"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEData_Notification; Module Name: " & mstrModuleName)
            Return Nothing
        Finally

        End Try
    End Function
#End Region

#Region " Public Function & Procedures "

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim bln As Boolean = False
        Try
            bln = Generate_DetailReport(xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, xExportReportPath, xOpenReportAfterExport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try


    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub SetDefaultValue()
        Try
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrHighestQualificationIds = ""
            mstrProfessionQualificationIds = ""
            mstrDispalyColumnIds = ""
            'Gajanan [31-Aug-2021] -- Start
            mblnIsNameConsolidate = False
            'Gajanan [31-Aug-2021] -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Public Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xExportPath As String, _
                                          ByVal xOpenAfterExport As Boolean) As Boolean
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Dim rowsArrayHeader As New ArrayList
        Dim rowsArrayFooter As New ArrayList
        Dim row As WorksheetRow
        Dim wcell As WorksheetCell = Nothing
        Dim intArrayColumnWidth As Integer() = Nothing
        Dim strarrGroupColumns As String() = Nothing
        Try
            Call Generate_Table()

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            StrQ = "SELECT " & _
                   "     hremployee_master.employeeunkid " & _
                   "    ,hremployee_master.employeecode " & _
                   "    ,ISNULL(hremployee_master.firstname, '') AS firstname " & _
                   "    ,ISNULL(hremployee_master.othername, '') AS othername " & _
                   "    ,ISNULL(hremployee_master.surname, '') AS surname " & _
                   "    ,ISNULL(BM.name, '') AS col_" & enDisplay_Column.BRANCH & " " & _
                   "    ,ISNULL(DM.name, '') AS col_" & enDisplay_Column.DEPARTMENT & " " & _
                   "    ,ISNULL(SM.name, '') AS col_" & enDisplay_Column.SECTION & " " & _
                   "    ,ISNULL(JM.job_name, '') AS col_" & enDisplay_Column.JOB & " "
            If mstrHighestQualificationIds.Trim.Length > 0 Then
                StrQ &= "    ,ISNULL(H_Qualification.H_Quali,'') AS col_" & enDisplay_Column.HIGHEST_QUALIFICATION & " "
            Else
                StrQ &= "    , '' AS col_" & enDisplay_Column.HIGHEST_QUALIFICATION & " "
            End If

            If mstrProfessionQualificationIds.Trim.Length > 0 Then
                StrQ &= "    ,ISNULL(P_Qualification.P_Quali,'') AS col_" & enDisplay_Column.PROFESSION_QUALIFICATION & " "
            Else
                StrQ &= "    , '' AS col_" & enDisplay_Column.PROFESSION_QUALIFICATION & " "
            End If
            StrQ &= "    ,ISNULL(GLM.name, '') AS col_" & enDisplay_Column.GRADE_LEVEL & " " & _
                    "    ,CAST (EGRD.newscale AS DECIMAL(36, 6)) AS col_" & enDisplay_Column.SCALE & " " & _
                    "    ,ISNULL(ET.name,'') AS col_" & enDisplay_Column.EMPLOYMENT_TYPE & " " & _
                    "    ,ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate, 112), '') AS col_" & enDisplay_Column.DATE_HIRED & " " & _
                    "    ,ISNULL(CONVERT(CHAR(8), hremployee_master.birthdate, 112), '') AS col_" & enDisplay_Column.BIRTHDATE & " " & _
                    "    ,CASE WHEN gender <= 0 THEN '' WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END AS col_" & enDisplay_Column.GENDER & " " & _
                    "    ,ISNULL(DGM.name, '') AS col_" & enDisplay_Column.DEPARTMENT_GROUP & " " & _
                    "    ,ISNULL(SGM.name, '') AS col_" & enDisplay_Column.SECTION_GROUP & " " & _
                    "    ,ISNULL(JGM.name, '') AS col_" & enDisplay_Column.JOB_GROUP & " " & _
                    "    ,ISNULL(CGM.name, '') AS col_" & enDisplay_Column.CLASS_GROUP & " " & _
                    "    ,ISNULL(CM.name, '') AS col_" & enDisplay_Column.CLASSES & " " & _
                    "    ,ISNULL(CONVERT(CHAR(8), CnfDt.date1,112),'') AS col_" & enDisplay_Column.CONFIRMATION_DATE & " " & _
                    "    ,ISNULL(ETHC.name,'') AS col_" & enDisplay_Column.ETHINICITY & " " & _
                    "    ,iSNULL(Disibility.Disbty,'') AS col_" & enDisplay_Column.DISABLILITY & " " & _
                    "    ,ISNULL(EGRD.incrementdate,'') AS incrementdate "
            'S.SANDEEP [13 JAN 2017] -- START
            StrQ &= "    ,ISNULL(GM.name, '') AS col_" & enDisplay_Column.GRADE & " "
            'S.SANDEEP [13 JAN 2017] -- END
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             Trf.TrfEmpId " & _
                    "            ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
                    "            ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
                    "            ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                    "            ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "            ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
                    "            ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
                    "            ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
                    "            ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
                    "            ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
                    "            ,ISNULL(Trf.classunkid,0) AS classunkid " & _
                    "            ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                    "        FROM " & _
                    "        ( " & _
                    "            SELECT " & _
                    "                 ETT.employeeunkid AS TrfEmpId " & _
                    "                ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                    "                ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                    "                ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                    "                ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "                ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                    "                ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                    "                ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                    "                ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                    "                ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                    "                ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                    "                ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                    "                ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                    "            FROM hremployee_transfer_tran AS ETT " & _
                    "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "        ) AS Trf WHERE Trf.Rno = 1 " & _
                    "    ) AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "    LEFT JOIN hrstation_master AS BM ON ETRF.stationunkid = BM.stationunkid AND BM.isactive = 1 " & _
                    "    LEFT JOIN hrdepartment_group_master AS DGM ON ETRF.deptgroupunkid = DGM.deptgroupunkid AND DGM.isactive = 1 " & _
                    "    LEFT JOIN hrdepartment_master AS DM ON ETRF.departmentunkid = DM.departmentunkid AND DM.isactive = 1 " & _
                    "    LEFT JOIN hrsectiongroup_master AS SGM ON ETRF.sectiongroupunkid = SGM.sectiongroupunkid AND SGM.isactive = 1 " & _
                    "    LEFT JOIN hrsection_master AS SM ON ETRF.sectionunkid = SM.sectionunkid AND SM.isactive = 1 " & _
                    "    LEFT JOIN hrunitgroup_master AS UGM ON ETRF.unitgroupunkid = UGM.unitgroupunkid AND UGM.isactive = 1 " & _
                    "    LEFT JOIN hrunit_master AS UM ON ETRF.unitunkid = UM.unitunkid AND UM.isactive = 1 " & _
                    "    LEFT JOIN hrteam_master AS TM ON ETRF.teamunkid = TM.teamunkid AND TM.isactive = 1 " & _
                    "    LEFT JOIN hrclassgroup_master AS CGM ON ETRF.classgroupunkid = CGM.classgroupunkid AND CGM.isactive = 1 " & _
                    "    LEFT JOIN hrclasses_master AS CM ON ETRF.classunkid = CM.classesunkid AND CM.isactive = 1 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             Cat.CatEmpId " & _
                    "            ,Cat.jobgroupunkid " & _
                    "            ,Cat.jobunkid " & _
                    "            ,Cat.CEfDt " & _
                    "        FROM " & _
                    "        ( " & _
                    "            SELECT " & _
                    "                 ECT.employeeunkid AS CatEmpId " & _
                    "                ,ECT.jobgroupunkid " & _
                    "                ,ECT.jobunkid " & _
                    "                ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                    "                ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                    "            FROM hremployee_categorization_tran AS ECT " & _
                    "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "        ) AS Cat WHERE Cat.Rno = 1 " & _
                    "    ) AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "    LEFT JOIN hrjob_master AS JM ON ERECAT.jobunkid = JM.jobunkid AND JM.isactive = 1 " & _
                    "    LEFT JOIN hrjobgroup_master AS JGM ON ERECAT.jobgroupunkid = JGM.jobgroupunkid AND JGM.isactive = 1 " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             gradegroupunkid " & _
                    "            ,gradeunkid " & _
                    "            ,gradelevelunkid " & _
                    "            ,newscale " & _
                    "            ,incrementdate " & _
                    "            ,employeeunkid AS GEmpId " & _
                    "        FROM " & _
                    "        ( " & _
                    "            SELECT " & _
                    "                gradegroupunkid " & _
                    "                ,gradeunkid " & _
                    "                ,gradelevelunkid " & _
                    "                ,newscale " & _
                    "                ,employeeunkid " & _
                    "                ,CONVERT(CHAR(8),incrementdate,112) AS incrementdate " & _
                    "                ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "            FROM prsalaryincrement_tran " & _
                    "            WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "        ) AS GRD WHERE GRD.Rno = 1 " & _
                    "    ) AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                    "    LEFT JOIN hrgradegroup_master GGM ON GGM.gradegroupunkid = EGRD.gradegroupunkid AND GGM.isactive = 1 " & _
                    "    LEFT JOIN hrgrade_master AS GM ON GM.gradeunkid = EGRD.gradeunkid AND GM.isactive = 1 " & _
                    "    LEFT JOIN hrgradelevel_master AS GLM ON GLM.gradelevelunkid = EGRD.gradelevelunkid AND GLM.isactive = 1 " & _
                    "    LEFT JOIN cfcommon_master AS ET ON ET.masterunkid = hremployee_master.employmenttypeunkid " & _
                    "    LEFT JOIN " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             date1 " & _
                    "            ,employeeunkid " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "        FROM hremployee_dates_tran " & _
                    "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "    ) AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 "
            If mstrHighestQualificationIds.Trim.Length > 0 Then
                'S.SANDEEP [13 JAN 2017] -- START
                'StrQ &= "   LEFT JOIN " & _
                '        "   ( " & _
                '        "       SELECT " & _
                '        "           employeeunkid, " & _
                '        "           STUFF((SELECT '#10;,' + hrqualification_master.qualificationname " & _
                '        "                   FROM hrqualification_master " & _
                '        "                       JOIN hremp_qualification_tran ON hremp_qualification_tran.qualificationtranunkid = hrqualification_master.qualificationunkid " & _
                '        "                       JOIN cfcommon_master AS DIS ON hremp_qualification_tran.qualificationgroupunkid= DIS.masterunkid " & _
                '        "                   where hremp_qualification_tran.employeeunkid = t1.employeeunkid  AND hremp_qualification_tran.qualificationgroupunkid  IN (" & mstrHighestQualificationIds & ") " & _
                '        "                   ORDER bY DIS.qlevel DESC " & _
                '        "           FOR XML PATH('')), " & _
                '        "           1,5,'') AS H_Quali " & _
                '        "       FROM hremp_qualification_tran t1 " & _
                '        "       WHERE 1=1" & _
                '        "       GROUP BY employeeunkid " & _
                '        "    ) AS H_Qualification ON H_Qualification.employeeunkid = hremployee_master.employeeunkid "
                StrQ &= "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "           employeeunkid, " & _
                        "           STUFF((SELECT '#10;,' + hrqualification_master.qualificationname " & _
                        "                   FROM hrqualification_master " & _
                        "                       JOIN hremp_qualification_tran ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                        "                       JOIN cfcommon_master AS DIS ON hremp_qualification_tran.qualificationgroupunkid= DIS.masterunkid " & _
                        "                   where hremp_qualification_tran.employeeunkid = t1.employeeunkid  AND hremp_qualification_tran.qualificationgroupunkid  IN (" & mstrHighestQualificationIds & ") " & _
                        "                       AND hremp_qualification_tran.isvoid = 0 " & _
                        "                   ORDER bY DIS.qlevel DESC " & _
                        "           FOR XML PATH('')), " & _
                        "           1,5,'') AS H_Quali " & _
                        "       FROM hremp_qualification_tran t1 " & _
                        "       WHERE 1=1 AND t1.isvoid = 0 " & _
                        "       GROUP BY employeeunkid " & _
                        "    ) AS H_Qualification ON H_Qualification.employeeunkid = hremployee_master.employeeunkid "
                'S.SANDEEP [13 JAN 2017] -- END

                'S.SANDEEP [05-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#ARUTI-23}
                '<AND hremp_qualification_tran.isvoid = 0> -- ADDED
                '<AND t1.isvoid = 0> -- ADDED
                'S.SANDEEP [05-Mar-2018] -- END

            End If
            If mstrProfessionQualificationIds.Trim.Length > 0 Then
                'S.SANDEEP [13 JAN 2017] -- START
                'StrQ &= "    LEFT JOIN " & _
                '        "    ( " & _
                '        "       SELECT " & _
                '        "           employeeunkid, " & _
                '        "           STUFF((SELECT '#10;,' + hrqualification_master.qualificationname " & _
                '        "                   FROM hrqualification_master " & _
                '        "                       JOIN hremp_qualification_tran ON hremp_qualification_tran.qualificationtranunkid = hrqualification_master.qualificationunkid " & _
                '        "                       JOIN cfcommon_master AS DIS ON hremp_qualification_tran.qualificationgroupunkid= DIS.masterunkid " & _
                '        "                   where hremp_qualification_tran.employeeunkid = t1.employeeunkid AND hremp_qualification_tran.qualificationgroupunkid  IN (" & mstrProfessionQualificationIds & ") " & _
                '        "               ORDER bY DIS.qlevel DESC " & _
                '        "               FOR XML PATH('')), " & _
                '        "               1,5,'') AS P_Quali " & _
                '        "       FROM hremp_qualification_tran t1 " & _
                '        "       WHERE 1=1 " & _
                '        "       GROUP BY employeeunkid " & _
                '        "    ) AS P_Qualification ON P_Qualification.employeeunkid = hremployee_master.employeeunkid "
                StrQ &= "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "           employeeunkid, " & _
                        "           STUFF((SELECT '#10;,' + hrqualification_master.qualificationname " & _
                        "                   FROM hrqualification_master " & _
                        "                       JOIN hremp_qualification_tran ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                        "                       JOIN cfcommon_master AS DIS ON hremp_qualification_tran.qualificationgroupunkid= DIS.masterunkid " & _
                        "                   where hremp_qualification_tran.employeeunkid = t1.employeeunkid AND hremp_qualification_tran.qualificationgroupunkid  IN (" & mstrProfessionQualificationIds & ") " & _
                        "                      AND hremp_qualification_tran.isvoid = 0 " & _
                        "               ORDER bY DIS.qlevel DESC " & _
                        "               FOR XML PATH('')), " & _
                        "               1,5,'') AS P_Quali " & _
                        "       FROM hremp_qualification_tran t1 " & _
                        "       WHERE 1=1 AND t1.isvoid = 0 " & _
                        "       GROUP BY employeeunkid " & _
                        "    ) AS P_Qualification ON P_Qualification.employeeunkid = hremployee_master.employeeunkid "
                'S.SANDEEP [13 JAN 2017] -- END

                'S.SANDEEP [05-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#ARUTI-23}
                '<AND hremp_qualification_tran.isvoid = 0> -- ADDED
                '<AND t1.isvoid = 0> -- ADDED
                'S.SANDEEP [05-Mar-2018] -- END

            End If

            StrQ &= "   LEFT JOIN cfcommon_master AS ETHC ON ETHC.masterunkid =hremployee_master.ethnicityunkid AND ETHC.mastertype = " & clsCommon_Master.enCommonMaster.ETHNICITY & " " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           employeeunkid, " & _
                    "           STUFF((SELECT '#10;,' + DIS.name " & _
                    "                FROM hrdisabilities_tran " & _
                    "                JOIN cfcommon_master AS DIS ON hrdisabilities_tran.disabilitiesunkid = DIS.masterunkid " & _
                    "                where hrdisabilities_tran.employeeunkid = t1.employeeunkid " & _
                    "           FOR XML PATH('')), " & _
                    "           1,5,'') AS Disbty " & _
                    "       FROM hrdisabilities_tran t1 " & _
                    "       GROUP BY employeeunkid " & _
                    "   ) AS Disibility ON Disibility.employeeunkid = hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mintViewIndex > 0 Then
                StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ",  hremployee_master.employeecode "
            Else
                StrQ &= "ORDER BY  hremployee_master.employeecode "
            End If

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim dtFinalRow As DataRow
                mdtExcelTable.Rows.Clear()

                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    dtFinalRow = mdtExcelTable.NewRow
                    dtFinalRow.Item("SrNo") = dsList.Tables(0).Rows.IndexOf(dtRow) + 1
                    dtFinalRow.Item("Employee_Code") = dtRow.Item("employeecode")

                    'Gajanan [31-Aug-2021] -- Start

                    'dtFinalRow.Item("Firstname") = dtRow.Item("firstname")
                    'dtFinalRow.Item("Othername") = dtRow.Item("othername")
                    'dtFinalRow.Item("Surname") = dtRow.Item("surname")

                    If mblnIsNameConsolidate Then
                        dtFinalRow.Item("Name") = dtRow.Item("firstname") + " " + dtRow.Item("othername") + " " + dtRow.Item("surname")
                        'dtFinalRow.Item("Othername") =
                        'dtFinalRow.Item("Surname") = dtRow.Item("surname")
                    Else
                    dtFinalRow.Item("Firstname") = dtRow.Item("firstname")
                    dtFinalRow.Item("Othername") = dtRow.Item("othername")
                    dtFinalRow.Item("Surname") = dtRow.Item("surname")
                    End If
                    'Gajanan [31-Aug-2021] -- End

                    If IsDBNull(dtRow.Item("incrementdate")) = False AndAlso dtRow.Item("incrementdate").ToString.Trim <> "" Then
                        dtFinalRow.Item("Date_of_Last_Promotion") = eZeeDate.convertDate(dtRow.Item("incrementdate").ToString).ToShortDateString
                    Else
                        dtFinalRow.Item("Date_of_Last_Promotion") = ""
                    End If

                    dtFinalRow.Item("GName") = dtRow("GName")
                    If mstrDispalyColumnIds.Trim.Length > 0 Then
                        For Each xColumnId As String In mstrDispalyColumnIds.Split(",")
                            If dsList.Tables(0).Columns.Contains("Col_" & xColumnId) Then
                                Select Case xColumnId
                                    Case enDisplay_Column.SCALE
                                        If dtRow.Item("Col_" & xColumnId).ToString.Trim.Length > 0 Then
                                            dtFinalRow.Item("Col_" & xColumnId) = Format(CDec(dtRow.Item("Col_" & xColumnId)), GUI.fmtCurrency)
                                        Else
                                            dtFinalRow.Item("Col_" & xColumnId) = Format(0, GUI.fmtCurrency)
                                        End If
                                    Case enDisplay_Column.DATE_HIRED, enDisplay_Column.CONFIRMATION_DATE, enDisplay_Column.BIRTHDATE
                                        If dtRow.Item("Col_" & xColumnId).ToString.Trim.Length > 0 Then
                                            dtFinalRow.Item("Col_" & xColumnId) = eZeeDate.convertDate(dtRow.Item("Col_" & xColumnId).ToString).ToShortDateString
                                        Else
                                            dtFinalRow.Item("Col_" & xColumnId) = ""
                                        End If

                                    Case Else
                                        dtFinalRow.Item("Col_" & xColumnId) = dtRow.Item("Col_" & xColumnId).ToString.Replace("&amp;", "&")
                                End Select
                            End If
                        Next
                    End If
                    mdtExcelTable.Rows.Add(dtFinalRow)
                Next

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Record Not Found"), enMsgBoxStyle.Information)
                Exit Function
            End If

            If mdtExcelTable.Columns.Contains(("Col_" & enDisplay_Column.PROFESSION_QUALIFICATION)) Then
                If mstrProfessionQualificationIds.Trim.Length <= 0 Then
                    mdtExcelTable.Columns.Remove("Col_" & enDisplay_Column.PROFESSION_QUALIFICATION)
                End If
            End If

            If mdtExcelTable.Columns.Contains(("Col_" & enDisplay_Column.HIGHEST_QUALIFICATION)) Then
                If mstrHighestQualificationIds.Trim.Length <= 0 Then
                    mdtExcelTable.Columns.Remove("Col_" & enDisplay_Column.HIGHEST_QUALIFICATION)
                End If
            End If

            If mintViewIndex <= 0 Then
                mdtExcelTable.Columns.Remove("GName")
            Else
                mdtExcelTable.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If


            ReDim intArrayColumnWidth(mdtExcelTable.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(ii) = 125
            Next

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 30, "From :") & " " & dtPeriodStart.Date & Language.getMessage(mstrModuleName, 31, " To ") & dtPeriodEnd, "s10bw")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtExcelTable.Columns.Count - 1
            rowsArrayHeader.Add(row)

            Dim objDic As New Dictionary(Of Integer, Object)
            objDic.Add(0, Language.getMessage(mstrModuleName, 32, "Sub Total"))
            Dim dicSubTotal() As Dictionary(Of Integer, Object) = {objDic}
            Dim dicGrandTotal As New Dictionary(Of Integer, Object)
            dicGrandTotal.Add(0, Language.getMessage(mstrModuleName, 33, "Grand Total"))

            Call ReportExecute(xCompanyUnkid, CInt(mstrReportId), Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportPath, xOpenAfterExport, mdtExcelTable, intArrayColumnWidth, True, True, True, strarrGroupColumns, "Employee Seniority Report", , , , , , rowsArrayHeader, rowsArrayFooter, dicGrandTotal, dicSubTotal)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Sub Generate_Table()
        Dim blnFlag As Boolean = False
        Try
            mdtExcelTable = New DataTable("List")
            Dim dCol As DataColumn

            dCol = New DataColumn
            With dCol
                .ColumnName = "SrNo"
                .Caption = "Sr. No"
                .DataType = System.Type.GetType("System.Int64")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Employee_Code"
                .Caption = Language.getMessage(mstrModuleName, 22, "Employee Code")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            'Gajanan [31-Aug-2021] -- Start


            'dCol = New DataColumn
            'With dCol
            '    .ColumnName = "Firstname"
            '    .Caption = Language.getMessage(mstrModuleName, 23, "Firstname")
            '    .DataType = System.Type.GetType("System.String")
            '    .DefaultValue = 0
            'End With
            'mdtExcelTable.Columns.Add(dCol)

            'dCol = New DataColumn
            'With dCol
            '    .ColumnName = "Othername"
            '    .Caption = Language.getMessage(mstrModuleName, 24, "Othername")
            '    .DataType = System.Type.GetType("System.String")
            '    .DefaultValue = 0
            'End With
            'mdtExcelTable.Columns.Add(dCol)

            'dCol = New DataColumn
            'With dCol
            '    .ColumnName = "Surname"
            '    .Caption = Language.getMessage(mstrModuleName, 25, "Surname")
            '    .DataType = System.Type.GetType("System.String")
            '    .DefaultValue = 0
            'End With
            'mdtExcelTable.Columns.Add(dCol)

            If mblnIsNameConsolidate Then
                dCol = New DataColumn
                With dCol
                    .ColumnName = "Name"
                    .Caption = Language.getMessage(mstrModuleName, 34, "Name")
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = 0
                End With
                mdtExcelTable.Columns.Add(dCol)
            Else
            dCol = New DataColumn
            With dCol
                .ColumnName = "Firstname"
                .Caption = Language.getMessage(mstrModuleName, 23, "Firstname")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Othername"
                .Caption = Language.getMessage(mstrModuleName, 24, "Othername")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Surname"
                .Caption = Language.getMessage(mstrModuleName, 25, "Surname")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)


            End If
            'Gajanan [31-Aug-2021] -- End



            Dim strColumnName As String = ""
            If mstrDispalyColumnIds.Trim.Length > 0 Then
                For Each xColumnId As String In mstrDispalyColumnIds.Split(",")

                    Select Case xColumnId
                        Case enDisplay_Column.BRANCH
                            strColumnName = Language.getMessage(mstrModuleName, 1, "Branch")
                        Case enDisplay_Column.DEPARTMENT
                            strColumnName = Language.getMessage(mstrModuleName, 3, "Department")
                        Case enDisplay_Column.SECTION
                            strColumnName = Language.getMessage(mstrModuleName, 5, "Section")
                        Case enDisplay_Column.JOB
                            strColumnName = Language.getMessage(mstrModuleName, 7, "Job")
                        Case enDisplay_Column.HIGHEST_QUALIFICATION
                            strColumnName = Language.getMessage(mstrModuleName, 20, "Highest Qualification")
                        Case enDisplay_Column.PROFESSION_QUALIFICATION
                            strColumnName = Language.getMessage(mstrModuleName, 21, "Profession Qualification")
                        Case enDisplay_Column.GRADE_LEVEL
                            strColumnName = Language.getMessage(mstrModuleName, 11, "Grade Level")
                        Case enDisplay_Column.SCALE
                            strColumnName = Language.getMessage(mstrModuleName, 12, "Scale")
                        Case enDisplay_Column.EMPLOYMENT_TYPE
                            strColumnName = Language.getMessage(mstrModuleName, 13, "Employee Type")
                        Case enDisplay_Column.DATE_HIRED
                            strColumnName = Language.getMessage(mstrModuleName, 14, "Date Hired")
                        Case enDisplay_Column.BIRTHDATE
                            strColumnName = Language.getMessage(mstrModuleName, 15, "Birthdate")
                        Case enDisplay_Column.GENDER
                            strColumnName = Language.getMessage(mstrModuleName, 16, "Gender")
                        Case enDisplay_Column.DEPARTMENT_GROUP
                            strColumnName = Language.getMessage(mstrModuleName, 2, "Department Group")
                        Case enDisplay_Column.SECTION_GROUP
                            strColumnName = Language.getMessage(mstrModuleName, 4, "Section Group")
                        Case enDisplay_Column.JOB_GROUP
                            strColumnName = Language.getMessage(mstrModuleName, 6, "Job Group")
                        Case enDisplay_Column.CLASS_GROUP
                            strColumnName = Language.getMessage(mstrModuleName, 8, "Class Group")
                        Case enDisplay_Column.CLASSES
                            strColumnName = Language.getMessage(mstrModuleName, 9, "Class")
                        Case enDisplay_Column.CONFIRMATION_DATE
                            strColumnName = Language.getMessage(mstrModuleName, 17, "Confirmation Date")
                        Case enDisplay_Column.GRADE
                            strColumnName = Language.getMessage(mstrModuleName, 10, "Grade")
                        Case enDisplay_Column.ETHINICITY
                            strColumnName = Language.getMessage(mstrModuleName, 18, "Ethinicity")
                        Case enDisplay_Column.DISABLILITY
                            strColumnName = Language.getMessage(mstrModuleName, 19, "Disablility")
                    End Select
                    'Select Case xColumnId
                    '    Case enDisplay_Column.BIRTHDATE, enDisplay_Column.CONFIRMATION_DATE, enDisplay_Column.DATE_HIRED
                    '        dCol = New DataColumn
                    '        With dCol
                    '            .ColumnName = "Col_" & xColumnId
                    '            .Caption = strColumnName
                    '            .DataType = System.Type.GetType("System.DateTime")
                    '            .DefaultValue = Nothing
                    '        End With
                    '        mdtExcelTable.Columns.Add(dCol)
                    '    Case Else
                    '        dCol = New DataColumn
                    '        With dCol
                    '            .ColumnName = "Col_" & xColumnId
                    '            .Caption = strColumnName
                    '            .DataType = System.Type.GetType("System.String")
                    '            .DefaultValue = ""
                    '        End With
                    '        mdtExcelTable.Columns.Add(dCol)
                    'End Select
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "Col_" & xColumnId
                        .Caption = strColumnName
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtExcelTable.Columns.Add(dCol)
                Next
            End If

            dCol = New DataColumn
            With dCol
                .ColumnName = "Date_of_Last_Promotion"
                .Caption = Language.getMessage(mstrModuleName, 26, "Date of Last Promotion")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GName"
                .Caption = "GName"
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Table; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Branch")
            Language.setMessage(mstrModuleName, 2, "Department Group")
            Language.setMessage(mstrModuleName, 3, "Department")
            Language.setMessage(mstrModuleName, 4, "Section Group")
            Language.setMessage(mstrModuleName, 5, "Section")
            Language.setMessage(mstrModuleName, 6, "Job Group")
            Language.setMessage(mstrModuleName, 7, "Job")
            Language.setMessage(mstrModuleName, 8, "Class Group")
            Language.setMessage(mstrModuleName, 9, "Class")
            Language.setMessage(mstrModuleName, 10, "Grade")
            Language.setMessage(mstrModuleName, 11, "Grade Level")
            Language.setMessage(mstrModuleName, 12, "Scale")
            Language.setMessage(mstrModuleName, 13, "Employee Type")
            Language.setMessage(mstrModuleName, 14, "Date Hired")
            Language.setMessage(mstrModuleName, 15, "Birthdate")
            Language.setMessage(mstrModuleName, 16, "Gender")
            Language.setMessage(mstrModuleName, 17, "Confirmation Date")
            Language.setMessage(mstrModuleName, 18, "Ethinicity")
            Language.setMessage(mstrModuleName, 19, "Disablility")
            Language.setMessage(mstrModuleName, 20, "Highest Qualification")
            Language.setMessage(mstrModuleName, 21, "Profession Qualification")
            Language.setMessage(mstrModuleName, 22, "Employee Code")
            Language.setMessage(mstrModuleName, 23, "Firstname")
            Language.setMessage(mstrModuleName, 24, "Othername")
            Language.setMessage(mstrModuleName, 25, "Surname")
            Language.setMessage(mstrModuleName, 26, "Date of Last Promotion")
            Language.setMessage(mstrModuleName, 27, "Male")
            Language.setMessage(mstrModuleName, 28, "Female")
            Language.setMessage(mstrModuleName, 29, "Record Not Found")
            Language.setMessage(mstrModuleName, 30, "From :")
            Language.setMessage(mstrModuleName, 31, " To")
            Language.setMessage(mstrModuleName, 32, "Sub Total")
            Language.setMessage(mstrModuleName, 33, "Grand Total")
            Language.setMessage(mstrModuleName, 34, "Name")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
