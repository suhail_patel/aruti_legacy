#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployeeSeniorityReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeSeniorityReport"
    Private objSeniorityLevel As clsEmployeeSeniorityReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrProQualificationGrpId As String = ""
    Private mstrHightsQualificaitonGrpId As String = ""
    Private mstrDisplayColumn As String = ""
#End Region

#Region " Contructor "

    Public Sub New()
        objSeniorityLevel = New clsEmployeeSeniorityReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        'objSeniorityLevel.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub ResetValue()
        Try
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrProQualificationGrpId = ""
            mstrHightsQualificaitonGrpId = ""
            mstrDisplayColumn = ""
            dtpStartdate.Checked = False
            dtpStartdate.Value = Date.Now.Date
            dtpToDate.Checked = False
            dtpToDate.Value = Date.Now.Date
            lvDisplayCol.Items.Clear()
            'Gajanan [31-Aug-2021] -- Start
            ChkIs_Name_Consolidate.Checked = False
            'Gajanan [31-Aug-2021] -- End
            Call GetValue()
            Call Fill_Column_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objSeniorityLevel.SetDefaultValue()
            objSeniorityLevel._ViewByIds = mstrViewByIds
            objSeniorityLevel._ViewIndex = mintViewIndex
            objSeniorityLevel._ViewByName = mstrViewByName
            objSeniorityLevel._Analysis_Fields = mstrAnalysis_Fields
            objSeniorityLevel._Analysis_Join = mstrAnalysis_Join
            objSeniorityLevel._Analysis_OrderBy = mstrAnalysis_OrderBy
            objSeniorityLevel._Report_GroupName = mstrReport_GroupName
            objSeniorityLevel._Highest_QualificationIds = mstrHightsQualificaitonGrpId
            objSeniorityLevel._Profession_QualificationIds = mstrProQualificationGrpId
            objSeniorityLevel._Display_ColumnsIds = mstrDisplayColumn
            objSeniorityLevel._Profession_QualificationIds = mstrProQualificationGrpId
            'Gajanan [31-Aug-2021] -- Start
            objSeniorityLevel._Is_Name_Consolidate = ChkIs_Name_Consolidate.Checked
            'Gajanan [31-Aug-2021] -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Column_List()
        Dim objCommon As New clsCommon_Master
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Dim strFilter As String = ""
        Try
            dsList = objSeniorityLevel.GetDisplayColumn("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                If mstrDisplayColumn.Length > 0 Then
                    For Each Str As String In mstrDisplayColumn.Split(",")
                        Dim dtRow() As DataRow = dsList.Tables(0).Select("ID = " & Str & " ")
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow(0).Item("Name").ToString
                        lvItem.Tag = dtRow(0).Item("ID").ToString
                        lvItem.Checked = True
                        lvDisplayCol.Items.Add(lvItem)
                    Next
                End If

                If mstrDisplayColumn.Length > 0 Then
                    strFilter = "ID NOT IN (" & mstrDisplayColumn & ")"
                End If

                For Each dtRow As DataRow In dsList.Tables(0).Select(strFilter)
                    lvItem = New ListViewItem
                    lvItem.Text = dtRow.Item("Name").ToString
                    lvItem.Tag = dtRow.Item("ID").ToString
                    lvDisplayCol.Items.Add(lvItem)
                Next
            End If

            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, "List")
            dsList.Tables(0).Columns.Add("HightsQuaCheck", System.Type.GetType("System.Boolean"))
            dsList.Tables(0).Columns.Add("ProQuaCheck", System.Type.GetType("System.Boolean"))

            If mstrProQualificationGrpId.Length > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Select("masterunkid IN (" & mstrProQualificationGrpId & ")")
                    dtRow.Item("ProQuaCheck") = True
                Next
                If dsList.Tables(0).Rows.Count = dsList.Tables(0).Select("masterunkid IN (" & mstrProQualificationGrpId & ")").Count Then
                    RemoveHandler chkProQualification.CheckedChanged, AddressOf chkProQualification_CheckedChanged
                    chkProQualification.Checked = True
                    AddHandler chkProQualification.CheckedChanged, AddressOf chkProQualification_CheckedChanged
                End If
            End If

            If mstrHightsQualificaitonGrpId.Length > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Select("masterunkid IN (" & mstrHightsQualificaitonGrpId & ")")
                    dtRow.Item("HightsQuaCheck") = True
                Next

                If dsList.Tables(0).Rows.Count = dsList.Tables(0).Select("masterunkid IN (" & mstrHightsQualificaitonGrpId & ")").Count Then
                    RemoveHandler chkHighQualification.CheckedChanged, AddressOf chkHighQualification_CheckedChanged
                    chkHighQualification.Checked = True
                    AddHandler chkHighQualification.CheckedChanged, AddressOf chkHighQualification_CheckedChanged
                End If
            End If

            colhhightsQualification.DataPropertyName = "name"
            colhProfessionQualification.DataPropertyName = "name"
            objcolhQulificaitonUnkId.DataPropertyName = "masterunkid"
            objcolhHighQualification.DataPropertyName = "HightsQuaCheck"
            objcolhProfessionQualification.DataPropertyName = "ProQuaCheck"
            objcolhQulificaitonUnkId.ReadOnly = True
            colhProfessionQualification.ReadOnly = True
            dgvQualifiction.AutoGenerateColumns = False
            dgvQualifiction.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean, ByVal LvList As ListView)
        Try
            For Each LItem As ListViewItem In LvList.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Employee_Seniority_Report)
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))
                        Case 1  'Profession Qualification
                            mstrProQualificationGrpId = dsRow.Item("transactionheadid")
                        Case 2  'Highest Qualification
                            mstrHightsQualificaitonGrpId = dsRow.Item("transactionheadid")
                        Case 3 'Display Column
                            mstrDisplayColumn = dsRow.Item("transactionheadid")
                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmAssessmentSoreRatingReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSeniorityLevel = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAssessmentSoreRatingReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentSoreRatingReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Me.EZeeHeader.Title = objSeniorityLevel._ReportName
            'Me.EZeeHeader.Message = objSeniorityLevel._ReportDesc
            Call GetValue()
            Call Fill_Column_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentSoreRatingReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            mstrProQualificationGrpId = ""
            mstrHightsQualificaitonGrpId = ""
            mstrDisplayColumn = ""

            If dgvQualifiction.Rows.Count > 0 Then
                For Each dtRow As DataRow In CType(dgvQualifiction.DataSource, DataTable).Select("ProQuaCheck = 'True'")
                    mstrProQualificationGrpId &= "," & dtRow.Item("masterunkid")
                Next
                For Each dtRow As DataRow In CType(dgvQualifiction.DataSource, DataTable).Select("HightsQuaCheck = 'True'")
                    mstrHightsQualificaitonGrpId &= "," & dtRow.Item("masterunkid")
                Next

                If mstrHightsQualificaitonGrpId.Length > 0 Then
                    mstrHightsQualificaitonGrpId = mstrHightsQualificaitonGrpId.Substring(1)
                End If

                If mstrProQualificationGrpId.Length > 0 Then
                    mstrProQualificationGrpId = mstrProQualificationGrpId.Substring(1)
                End If
            End If

            If lvDisplayCol.CheckedItems.Count > 0 Then
                For Each lvItem As ListViewItem In lvDisplayCol.CheckedItems
                    mstrDisplayColumn &= "," & lvItem.Tag
                Next

                If mstrDisplayColumn.Length > 0 Then
                    mstrDisplayColumn = mstrDisplayColumn.Substring(1)
                End If
            End If

            If SetFilter() = False Then Exit Sub
            objSeniorityLevel.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                dtpStartdate.Value.Date, _
                                                dtpToDate.Value.Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportDataPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, enPrintAction.None, enExportAction.None, _
                                                ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeSeniorityReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeSeniorityReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvDisplayCol.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvDisplayCol.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvDisplayCol.Items(SelIndex - 1)
                lvDisplayCol.Items(SelIndex - 1) = lvDisplayCol.Items(SelIndex).Clone
                lvDisplayCol.Items(SelIndex) = tItem.Clone
                lvDisplayCol.Items(SelIndex - 1).Selected = True
                lvDisplayCol.Items(SelIndex - 1).EnsureVisible()
            End If
            lvDisplayCol.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvDisplayCol.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvDisplayCol.SelectedIndices.Item(0)
                If SelIndex = lvDisplayCol.Items.Count - 1 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvDisplayCol.Items(SelIndex + 1)
                lvDisplayCol.Items(SelIndex + 1) = lvDisplayCol.Items(SelIndex).Clone
                lvDisplayCol.Items(SelIndex) = tItem.Clone
                lvDisplayCol.Items(SelIndex + 1).Selected = True
                lvDisplayCol.Items(SelIndex + 1).EnsureVisible()
            End If
            lvDisplayCol.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub lvDisplayCol_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDisplayCol.ItemChecked
        Try
            RemoveHandler objchkdisplay.CheckedChanged, AddressOf objchkdisplay_CheckedChanged
            If lvDisplayCol.CheckedItems.Count <= 0 Then
                objchkdisplay.CheckState = CheckState.Unchecked
            ElseIf lvDisplayCol.CheckedItems.Count < lvDisplayCol.Items.Count Then
                objchkdisplay.CheckState = CheckState.Indeterminate
            ElseIf lvDisplayCol.CheckedItems.Count = lvDisplayCol.Items.Count Then
                objchkdisplay.CheckState = CheckState.Checked
            End If
            AddHandler objchkdisplay.CheckedChanged, AddressOf objchkdisplay_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDisplayCol_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkdisplay_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkdisplay.CheckedChanged
        Try
            RemoveHandler lvDisplayCol.ItemChecked, AddressOf lvDisplayCol_ItemChecked
            Call Do_Operation(CBool(objchkdisplay.CheckState), lvDisplayCol)
            AddHandler lvDisplayCol.ItemChecked, AddressOf lvDisplayCol_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkdisplay_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub lvHighestQualification_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        RemoveHandler chkHighQualification.CheckedChanged, AddressOf chkHighQualification_CheckedChanged
    '        If lvHighestQualification.CheckedItems.Count <= 0 Then
    '            chkHighQualification.CheckState = CheckState.Unchecked
    '        ElseIf lvHighestQualification.CheckedItems.Count < lvHighestQualification.Items.Count Then
    '            chkHighQualification.CheckState = CheckState.Indeterminate
    '        ElseIf lvHighestQualification.CheckedItems.Count = lvHighestQualification.Items.Count Then
    '            chkHighQualification.CheckState = CheckState.Checked
    '        End If
    '        AddHandler chkHighQualification.CheckedChanged, AddressOf chkHighQualification_CheckedChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvDisplayCol_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub chkHighQualification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        RemoveHandler lvHighestQualification.ItemChecked, AddressOf lvHighestQualification_ItemChecked
    '        Call Do_Operation(CBool(chkHighQualification.CheckState), lvHighestQualification)
    '        AddHandler lvHighestQualification.ItemChecked, AddressOf lvHighestQualification_ItemChecked
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkHighQualification_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lvProQualificaiton_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        RemoveHandler chkProQualification.CheckedChanged, AddressOf chkProQualification_CheckedChanged
    '        If lvProQualificaiton.CheckedItems.Count <= 0 Then
    '            chkProQualification.CheckState = CheckState.Unchecked
    '        ElseIf lvProQualificaiton.CheckedItems.Count < lvProQualificaiton.Items.Count Then
    '            chkProQualification.CheckState = CheckState.Indeterminate
    '        ElseIf lvProQualificaiton.CheckedItems.Count = lvProQualificaiton.Items.Count Then
    '            chkProQualification.CheckState = CheckState.Checked
    '        End If
    '        AddHandler chkProQualification.CheckedChanged, AddressOf chkProQualification_CheckedChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvDisplayCol_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub chkProQualification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        RemoveHandler lvProQualificaiton.ItemChecked, AddressOf lvProQualificaiton_ItemChecked
    '        Call Do_Operation(CBool(chkProQualification.CheckState), lvProQualificaiton)
    '        AddHandler lvProQualificaiton.ItemChecked, AddressOf lvProQualificaiton_ItemChecked
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkProQualification_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub dgvQualifiction_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvQualifiction.CellContentClick, dgvQualifiction.CellContentDoubleClick
        Try
            If dgvQualifiction.RowCount <= 0 Then Exit Sub

            If Me.dgvQualifiction.IsCurrentCellDirty Then
                Me.dgvQualifiction.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            RemoveHandler dgvQualifiction.CellContentClick, AddressOf dgvQualifiction_CellContentClick
            If e.ColumnIndex = objcolhHighQualification.Index Then
                If CBool(dgvQualifiction.Rows(e.RowIndex).Cells(objcolhHighQualification.Index).Value) = True Then
                    dgvQualifiction.Rows(e.RowIndex).Cells(objcolhProfessionQualification.Index).Value = Not CBool(dgvQualifiction.Rows(e.RowIndex).Cells(objcolhHighQualification.Index).Value)
                End If
            ElseIf e.ColumnIndex = objcolhProfessionQualification.Index Then
                If CBool(dgvQualifiction.Rows(e.RowIndex).Cells(objcolhProfessionQualification.Index).Value) = True Then
                    dgvQualifiction.Rows(e.RowIndex).Cells(objcolhHighQualification.Index).Value = Not CBool(dgvQualifiction.Rows(e.RowIndex).Cells(objcolhProfessionQualification.Index).Value)
                End If
            End If

            AddHandler dgvQualifiction.CellContentClick, AddressOf dgvQualifiction_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualifiction_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSaveSelecation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveSelecation.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1

        Try
            mstrProQualificationGrpId = ""
            mstrHightsQualificaitonGrpId = ""
            mstrDisplayColumn = ""

            If dgvQualifiction.Rows.Count > 0 Then
                For Each dtRow As DataRow In CType(dgvQualifiction.DataSource, DataTable).Select("ProQuaCheck = 'True'")
                    mstrProQualificationGrpId &= "," & dtRow.Item("masterunkid")
                Next
                For Each dtRow As DataRow In CType(dgvQualifiction.DataSource, DataTable).Select("HightsQuaCheck = 'True'")
                    mstrHightsQualificaitonGrpId &= "," & dtRow.Item("masterunkid")
                Next
            End If

            If lvDisplayCol.CheckedItems.Count > 0 Then
                For Each lvItem As ListViewItem In lvDisplayCol.CheckedItems
                    mstrDisplayColumn &= "," & lvItem.Tag
                Next
            End If

            '============ Save Profession Qualification setting CSV formate================
            If mstrProQualificationGrpId IsNot Nothing AndAlso mstrProQualificationGrpId.Length > 0 Then
                mstrProQualificationGrpId = mstrProQualificationGrpId.Substring(1)
            End If

            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Employee_Seniority_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            objUserDefRMode._Headtypeid = 1 'Profession Qualification CSV Formate
            objUserDefRMode._EarningTranHeadIds = mstrProQualificationGrpId
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Seniority_Report, 0, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            '============ Save Highest Qualification setting CSV formate================
            If mstrHightsQualificaitonGrpId IsNot Nothing AndAlso mstrHightsQualificaitonGrpId.Length > 0 Then
                mstrHightsQualificaitonGrpId = mstrHightsQualificaitonGrpId.Substring(1)
            End If
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Employee_Seniority_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            objUserDefRMode._Headtypeid = 2 'Highest Qualification CSV Formate
            objUserDefRMode._EarningTranHeadIds = mstrHightsQualificaitonGrpId
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Seniority_Report, 0, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            '============ Save Display Column setting CSV formate================
            If mstrDisplayColumn IsNot Nothing AndAlso mstrDisplayColumn.Length > 0 Then
                mstrDisplayColumn = mstrDisplayColumn.Substring(1)
            End If
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Employee_Seniority_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0

            objUserDefRMode._Headtypeid = 3 'Display Column CSV Formate
            objUserDefRMode._EarningTranHeadIds = mstrDisplayColumn
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Seniority_Report, 0, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSaveSelecation_LinkClicked", mstrModuleName)
        End Try
    End Sub


    Private Sub chkHighQualification_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHighQualification.CheckedChanged
        Try
            RemoveHandler dgvQualifiction.CellContentClick, AddressOf dgvQualifiction_CellContentClick
            For Each dgvRow As DataGridViewRow In dgvQualifiction.Rows
                dgvRow.Cells(objcolhHighQualification.Index).Value = CBool(chkHighQualification.Checked)
                If chkHighQualification.Checked Then
                    dgvRow.Cells(objcolhProfessionQualification.Index).Value = Not CBool(chkHighQualification.Checked)
                    chkProQualification.Checked = False
                End If
            Next
            CType(dgvQualifiction.DataSource, DataTable).AcceptChanges()
            AddHandler dgvQualifiction.CellContentClick, AddressOf dgvQualifiction_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkHighQualification_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkProQualification_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkProQualification.CheckedChanged
        Try
            RemoveHandler dgvQualifiction.CellContentClick, AddressOf dgvQualifiction_CellContentClick
            For Each dgvRow As DataGridViewRow In dgvQualifiction.Rows
                dgvRow.Cells(objcolhProfessionQualification.Index).Value = CBool(chkProQualification.Checked)
                If chkProQualification.Checked Then
                    dgvRow.Cells(objcolhHighQualification.Index).Value = Not CBool(chkProQualification.Checked)
                    chkHighQualification.Checked = False
                End If
            Next
            CType(dgvQualifiction.DataSource, DataTable).AcceptChanges()
            AddHandler dgvQualifiction.CellContentClick, AddressOf dgvQualifiction_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkProQualification_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDisplayColumn.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDisplayColumn.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbQualificaiton.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbQualificaiton.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnDown.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnDown.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title", Me.EZeeHeader.Title)
            'Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message", Me.EZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.colhDisplayName.Text = Language._Object.getCaption(CStr(Me.colhDisplayName.Tag), Me.colhDisplayName.Text)
            Me.gbDisplayColumn.Text = Language._Object.getCaption(Me.gbDisplayColumn.Name, Me.gbDisplayColumn.Text)
            Me.gbQualificaiton.Text = Language._Object.getCaption(Me.gbQualificaiton.Name, Me.gbQualificaiton.Text)
            Me.lnkSaveSelecation.Text = Language._Object.getCaption(Me.lnkSaveSelecation.Name, Me.lnkSaveSelecation.Text)
            'Me.chkProQualification.Text = Language._Object.getCaption(Me.chkProQualification.Name, Me.chkProQualification.Text)
            'Me.chkHighQualification.Text = Language._Object.getCaption(Me.chkHighQualification.Name, Me.chkHighQualification.Text)
            Me.colhhightsQualification.HeaderText = Language._Object.getCaption(Me.colhhightsQualification.Name, Me.colhhightsQualification.HeaderText)
            Me.colhProfessionQualification.HeaderText = Language._Object.getCaption(Me.colhProfessionQualification.Name, Me.colhProfessionQualification.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
