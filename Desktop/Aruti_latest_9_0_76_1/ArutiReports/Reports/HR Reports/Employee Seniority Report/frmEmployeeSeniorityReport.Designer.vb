﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSeniorityReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSeniorityReport))
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.LblToDate = New System.Windows.Forms.Label
        Me.dtpStartdate = New System.Windows.Forms.DateTimePicker
        Me.LblFromDate = New System.Windows.Forms.Label
        Me.pnlCondition = New System.Windows.Forms.Panel
        Me.objchkdisplay = New System.Windows.Forms.CheckBox
        Me.lvDisplayCol = New eZee.Common.eZeeListView(Me.components)
        Me.colhDisplayName = New System.Windows.Forms.ColumnHeader
        Me.objcolhSelectCol = New System.Windows.Forms.ColumnHeader
        Me.objcolhJoin = New System.Windows.Forms.ColumnHeader
        Me.objcolhDisplay = New System.Windows.Forms.ColumnHeader
        Me.gbDisplayColumn = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkSaveSelecation = New System.Windows.Forms.LinkLabel
        Me.gbQualificaiton = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkProQualification = New System.Windows.Forms.CheckBox
        Me.chkHighQualification = New System.Windows.Forms.CheckBox
        Me.dgvQualifiction = New System.Windows.Forms.DataGridView
        Me.objcolhHighQualification = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhhightsQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhProfessionQualification = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhProfessionQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhQulificaitonUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ChkIs_Name_Consolidate = New System.Windows.Forms.CheckBox
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlCondition.SuspendLayout()
        Me.gbDisplayColumn.SuspendLayout()
        Me.gbQualificaiton.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvQualifiction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = ""
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(767, 60)
        Me.EZeeHeader.TabIndex = 21
        Me.EZeeHeader.Title = "Employee Seniority Report"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 512)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(767, 55)
        Me.EZeeFooter1.TabIndex = 22
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(474, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(570, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(666, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpStartdate)
        Me.gbFilterCriteria.Controls.Add(Me.LblFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(403, 65)
        Me.gbFilterCriteria.TabIndex = 23
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(307, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 200
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(267, 37)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(98, 21)
        Me.dtpToDate.TabIndex = 213
        '
        'LblToDate
        '
        Me.LblToDate.BackColor = System.Drawing.Color.Transparent
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(198, 39)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(59, 16)
        Me.LblToDate.TabIndex = 212
        Me.LblToDate.Text = "To Date"
        '
        'dtpStartdate
        '
        Me.dtpStartdate.Checked = False
        Me.dtpStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartdate.Location = New System.Drawing.Point(96, 37)
        Me.dtpStartdate.Name = "dtpStartdate"
        Me.dtpStartdate.Size = New System.Drawing.Size(98, 21)
        Me.dtpStartdate.TabIndex = 211
        '
        'LblFromDate
        '
        Me.LblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.LblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromDate.Location = New System.Drawing.Point(8, 39)
        Me.LblFromDate.Name = "LblFromDate"
        Me.LblFromDate.Size = New System.Drawing.Size(84, 16)
        Me.LblFromDate.TabIndex = 210
        Me.LblFromDate.Text = "From Date"
        '
        'pnlCondition
        '
        Me.pnlCondition.Controls.Add(Me.objchkdisplay)
        Me.pnlCondition.Controls.Add(Me.lvDisplayCol)
        Me.pnlCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCondition.Location = New System.Drawing.Point(3, 24)
        Me.pnlCondition.Name = "pnlCondition"
        Me.pnlCondition.Size = New System.Drawing.Size(292, 383)
        Me.pnlCondition.TabIndex = 215
        '
        'objchkdisplay
        '
        Me.objchkdisplay.AutoSize = True
        Me.objchkdisplay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkdisplay.Location = New System.Drawing.Point(224, 7)
        Me.objchkdisplay.Name = "objchkdisplay"
        Me.objchkdisplay.Size = New System.Drawing.Size(15, 14)
        Me.objchkdisplay.TabIndex = 103
        Me.objchkdisplay.UseVisualStyleBackColor = True
        '
        'lvDisplayCol
        '
        Me.lvDisplayCol.BackColorOnChecked = False
        Me.lvDisplayCol.CheckBoxes = True
        Me.lvDisplayCol.ColumnHeaders = Nothing
        Me.lvDisplayCol.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDisplayName, Me.objcolhSelectCol, Me.objcolhJoin, Me.objcolhDisplay})
        Me.lvDisplayCol.CompulsoryColumns = ""
        Me.lvDisplayCol.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDisplayCol.FullRowSelect = True
        Me.lvDisplayCol.GridLines = True
        Me.lvDisplayCol.GroupingColumn = Nothing
        Me.lvDisplayCol.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDisplayCol.HideSelection = False
        Me.lvDisplayCol.Location = New System.Drawing.Point(0, 0)
        Me.lvDisplayCol.MinColumnWidth = 50
        Me.lvDisplayCol.MultiSelect = False
        Me.lvDisplayCol.Name = "lvDisplayCol"
        Me.lvDisplayCol.OptionalColumns = ""
        Me.lvDisplayCol.ShowMoreItem = False
        Me.lvDisplayCol.ShowSaveItem = False
        Me.lvDisplayCol.ShowSelectAll = True
        Me.lvDisplayCol.ShowSizeAllColumnsToFit = True
        Me.lvDisplayCol.Size = New System.Drawing.Size(292, 383)
        Me.lvDisplayCol.Sortable = True
        Me.lvDisplayCol.TabIndex = 11
        Me.lvDisplayCol.UseCompatibleStateImageBehavior = False
        Me.lvDisplayCol.View = System.Windows.Forms.View.Details
        '
        'colhDisplayName
        '
        Me.colhDisplayName.Tag = "colhDisplayName"
        Me.colhDisplayName.Text = "Display Column On Report"
        Me.colhDisplayName.Width = 245
        '
        'objcolhSelectCol
        '
        Me.objcolhSelectCol.Tag = "objcolhSelectCol"
        Me.objcolhSelectCol.Text = ""
        Me.objcolhSelectCol.Width = 0
        '
        'objcolhJoin
        '
        Me.objcolhJoin.Tag = "objcolhJoin"
        Me.objcolhJoin.Text = ""
        Me.objcolhJoin.Width = 0
        '
        'objcolhDisplay
        '
        Me.objcolhDisplay.Tag = "objcolhDisplay"
        Me.objcolhDisplay.Text = ""
        Me.objcolhDisplay.Width = 0
        '
        'gbDisplayColumn
        '
        Me.gbDisplayColumn.BorderColor = System.Drawing.Color.Black
        Me.gbDisplayColumn.Checked = False
        Me.gbDisplayColumn.CollapseAllExceptThis = False
        Me.gbDisplayColumn.CollapsedHoverImage = Nothing
        Me.gbDisplayColumn.CollapsedNormalImage = Nothing
        Me.gbDisplayColumn.CollapsedPressedImage = Nothing
        Me.gbDisplayColumn.CollapseOnLoad = False
        Me.gbDisplayColumn.Controls.Add(Me.objbtnUp)
        Me.gbDisplayColumn.Controls.Add(Me.objbtnDown)
        Me.gbDisplayColumn.Controls.Add(Me.pnlCondition)
        Me.gbDisplayColumn.ExpandedHoverImage = Nothing
        Me.gbDisplayColumn.ExpandedNormalImage = Nothing
        Me.gbDisplayColumn.ExpandedPressedImage = Nothing
        Me.gbDisplayColumn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDisplayColumn.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDisplayColumn.HeaderHeight = 25
        Me.gbDisplayColumn.HeaderMessage = ""
        Me.gbDisplayColumn.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDisplayColumn.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDisplayColumn.HeightOnCollapse = 0
        Me.gbDisplayColumn.LeftTextSpace = 0
        Me.gbDisplayColumn.Location = New System.Drawing.Point(421, 66)
        Me.gbDisplayColumn.Name = "gbDisplayColumn"
        Me.gbDisplayColumn.OpenHeight = 300
        Me.gbDisplayColumn.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDisplayColumn.ShowBorder = True
        Me.gbDisplayColumn.ShowCheckBox = False
        Me.gbDisplayColumn.ShowCollapseButton = False
        Me.gbDisplayColumn.ShowDefaultBorderColor = True
        Me.gbDisplayColumn.ShowDownButton = False
        Me.gbDisplayColumn.ShowHeader = True
        Me.gbDisplayColumn.Size = New System.Drawing.Size(334, 411)
        Me.gbDisplayColumn.TabIndex = 215
        Me.gbDisplayColumn.TabStop = True
        Me.gbDisplayColumn.Temp = 0
        Me.gbDisplayColumn.Text = "Report Column"
        Me.gbDisplayColumn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.ArutiReports.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(299, 25)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 228
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.ArutiReports.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(299, 56)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 229
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'lnkSaveSelecation
        '
        Me.lnkSaveSelecation.BackColor = System.Drawing.Color.Transparent
        Me.lnkSaveSelecation.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSaveSelecation.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSaveSelecation.Location = New System.Drawing.Point(10, 489)
        Me.lnkSaveSelecation.Name = "lnkSaveSelecation"
        Me.lnkSaveSelecation.Size = New System.Drawing.Size(120, 17)
        Me.lnkSaveSelecation.TabIndex = 217
        Me.lnkSaveSelecation.TabStop = True
        Me.lnkSaveSelecation.Text = "Save Selection"
        Me.lnkSaveSelecation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbQualificaiton
        '
        Me.gbQualificaiton.BorderColor = System.Drawing.Color.Black
        Me.gbQualificaiton.Checked = False
        Me.gbQualificaiton.CollapseAllExceptThis = False
        Me.gbQualificaiton.CollapsedHoverImage = Nothing
        Me.gbQualificaiton.CollapsedNormalImage = Nothing
        Me.gbQualificaiton.CollapsedPressedImage = Nothing
        Me.gbQualificaiton.CollapseOnLoad = False
        Me.gbQualificaiton.Controls.Add(Me.Panel1)
        Me.gbQualificaiton.ExpandedHoverImage = Nothing
        Me.gbQualificaiton.ExpandedNormalImage = Nothing
        Me.gbQualificaiton.ExpandedPressedImage = Nothing
        Me.gbQualificaiton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbQualificaiton.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbQualificaiton.HeaderHeight = 25
        Me.gbQualificaiton.HeaderMessage = ""
        Me.gbQualificaiton.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbQualificaiton.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbQualificaiton.HeightOnCollapse = 0
        Me.gbQualificaiton.LeftTextSpace = 0
        Me.gbQualificaiton.Location = New System.Drawing.Point(12, 137)
        Me.gbQualificaiton.Name = "gbQualificaiton"
        Me.gbQualificaiton.OpenHeight = 300
        Me.gbQualificaiton.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbQualificaiton.ShowBorder = True
        Me.gbQualificaiton.ShowCheckBox = False
        Me.gbQualificaiton.ShowCollapseButton = False
        Me.gbQualificaiton.ShowDefaultBorderColor = True
        Me.gbQualificaiton.ShowDownButton = False
        Me.gbQualificaiton.ShowHeader = True
        Me.gbQualificaiton.Size = New System.Drawing.Size(403, 340)
        Me.gbQualificaiton.TabIndex = 215
        Me.gbQualificaiton.TabStop = True
        Me.gbQualificaiton.Temp = 0
        Me.gbQualificaiton.Text = "Qualificaiton Column"
        Me.gbQualificaiton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkProQualification)
        Me.Panel1.Controls.Add(Me.chkHighQualification)
        Me.Panel1.Controls.Add(Me.dgvQualifiction)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(403, 340)
        Me.Panel1.TabIndex = 224
        '
        'chkProQualification
        '
        Me.chkProQualification.AutoSize = True
        Me.chkProQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkProQualification.Location = New System.Drawing.Point(208, 6)
        Me.chkProQualification.Name = "chkProQualification"
        Me.chkProQualification.Size = New System.Drawing.Size(15, 14)
        Me.chkProQualification.TabIndex = 224
        Me.chkProQualification.UseVisualStyleBackColor = True
        '
        'chkHighQualification
        '
        Me.chkHighQualification.AutoSize = True
        Me.chkHighQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHighQualification.Location = New System.Drawing.Point(8, 7)
        Me.chkHighQualification.Name = "chkHighQualification"
        Me.chkHighQualification.Size = New System.Drawing.Size(15, 14)
        Me.chkHighQualification.TabIndex = 223
        Me.chkHighQualification.UseVisualStyleBackColor = True
        '
        'dgvQualifiction
        '
        Me.dgvQualifiction.AllowUserToAddRows = False
        Me.dgvQualifiction.AllowUserToDeleteRows = False
        Me.dgvQualifiction.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvQualifiction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQualifiction.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhHighQualification, Me.colhhightsQualification, Me.objcolhProfessionQualification, Me.colhProfessionQualification, Me.objcolhQulificaitonUnkId})
        Me.dgvQualifiction.Location = New System.Drawing.Point(1, 2)
        Me.dgvQualifiction.Name = "dgvQualifiction"
        Me.dgvQualifiction.RowHeadersVisible = False
        Me.dgvQualifiction.Size = New System.Drawing.Size(399, 334)
        Me.dgvQualifiction.TabIndex = 225
        '
        'objcolhHighQualification
        '
        Me.objcolhHighQualification.HeaderText = ""
        Me.objcolhHighQualification.Name = "objcolhHighQualification"
        Me.objcolhHighQualification.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhHighQualification.ToolTipText = "Highest Qualification"
        Me.objcolhHighQualification.Width = 25
        '
        'colhhightsQualification
        '
        Me.colhhightsQualification.HeaderText = "Highest Qualification"
        Me.colhhightsQualification.Name = "colhhightsQualification"
        Me.colhhightsQualification.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhhightsQualification.Width = 176
        '
        'objcolhProfessionQualification
        '
        Me.objcolhProfessionQualification.HeaderText = ""
        Me.objcolhProfessionQualification.Name = "objcolhProfessionQualification"
        Me.objcolhProfessionQualification.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhProfessionQualification.ToolTipText = "Profession Qualification"
        Me.objcolhProfessionQualification.Width = 25
        '
        'colhProfessionQualification
        '
        Me.colhProfessionQualification.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhProfessionQualification.HeaderText = "Professional Qualification"
        Me.colhProfessionQualification.Name = "colhProfessionQualification"
        Me.colhProfessionQualification.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'objcolhQulificaitonUnkId
        '
        Me.objcolhQulificaitonUnkId.HeaderText = "objcolhQulificaitonUnkId"
        Me.objcolhQulificaitonUnkId.Name = "objcolhQulificaitonUnkId"
        Me.objcolhQulificaitonUnkId.Visible = False
        Me.objcolhQulificaitonUnkId.Width = 5
        '
        'ChkIs_Name_Consolidate
        '
        Me.ChkIs_Name_Consolidate.AutoSize = True
        Me.ChkIs_Name_Consolidate.Location = New System.Drawing.Point(421, 489)
        Me.ChkIs_Name_Consolidate.Name = "ChkIs_Name_Consolidate"
        Me.ChkIs_Name_Consolidate.Size = New System.Drawing.Size(212, 17)
        Me.ChkIs_Name_Consolidate.TabIndex = 231
        Me.ChkIs_Name_Consolidate.Text = "Consolidate Name Into a Single Column"
        Me.ChkIs_Name_Consolidate.UseVisualStyleBackColor = True
        '
        'frmEmployeeSeniorityReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.ChkIs_Name_Consolidate)
        Me.Controls.Add(Me.gbQualificaiton)
        Me.Controls.Add(Me.gbDisplayColumn)
        Me.Controls.Add(Me.lnkSaveSelecation)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.EZeeHeader)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frmEmployeeSeniorityReport"
        Me.Text = "Employee Seniority Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.pnlCondition.ResumeLayout(False)
        Me.pnlCondition.PerformLayout()
        Me.gbDisplayColumn.ResumeLayout(False)
        Me.gbQualificaiton.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvQualifiction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Private WithEvents LblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartdate As System.Windows.Forms.DateTimePicker
    Private WithEvents LblFromDate As System.Windows.Forms.Label
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlCondition As System.Windows.Forms.Panel
    Friend WithEvents objchkdisplay As System.Windows.Forms.CheckBox
    Friend WithEvents lvDisplayCol As eZee.Common.eZeeListView
    Friend WithEvents colhDisplayName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSelectCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhJoin As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDisplay As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbDisplayColumn As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbQualificaiton As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSaveSelecation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkProQualification As System.Windows.Forms.CheckBox
    Friend WithEvents chkHighQualification As System.Windows.Forms.CheckBox
    Friend WithEvents dgvQualifiction As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhHighQualification As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhhightsQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhProfessionQualification As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhProfessionQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhQulificaitonUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChkIs_Name_Consolidate As System.Windows.Forms.CheckBox
End Class
