'************************************************************************************************************************************
'Class Name : clsMAP_Malawi_Report.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class clsMAP_Malawi_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMAP_Malawi_Report"
    Private mstrReportId As String = enArutiReport.Medical_Monthly_Payment
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_DetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mintEmplTypeId As Integer = 0
    Private mstrEmplTypeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintMedicalCatId As Integer = 0
    Private mstrMedicalCatName As String = String.Empty
    Private mDecCatAmt_From As Decimal = 0
    Private mDecCatAmt_To As Decimal = 0
    Private mintDep_Cnt_From As Integer = 0
    Private mintDep_Cnt_To As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _EmplTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmplTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplTypeName() As String
        Set(ByVal value As String)
            mstrEmplTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _MedicalCatId() As Integer
        Set(ByVal value As Integer)
            mintMedicalCatId = value
        End Set
    End Property

    Public WriteOnly Property _MedicalCatName() As String
        Set(ByVal value As String)
            mstrMedicalCatName = value
        End Set
    End Property

    Public WriteOnly Property _CatAmt_From() As Decimal
        Set(ByVal value As Decimal)
            mDecCatAmt_From = value
        End Set
    End Property

    Public WriteOnly Property _CatAmt_To() As Decimal
        Set(ByVal value As Decimal)
            mDecCatAmt_To = value
        End Set
    End Property

    Public WriteOnly Property _Dep_Cnt_From() As Integer
        Set(ByVal value As Integer)
            mintDep_Cnt_From = value
        End Set
    End Property

    Public WriteOnly Property _Dep_Cnt_To() As Integer
        Set(ByVal value As Integer)
            mintDep_Cnt_To = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintEmplTypeId = 0
            mstrEmplTypeName = String.Empty
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintMedicalCatId = 0
            mstrMedicalCatName = String.Empty
            mDecCatAmt_From = 0
            mDecCatAmt_To = 0
            mintDep_Cnt_From = 0
            mintDep_Cnt_To = 0
            mstrAdvanceFilter = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = ""
            mblnIncludeInactiveEmp = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Period : ") & " " & mstrPeriodName & " "

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND EmpId = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintMedicalCatId > 0 Then
                Me._FilterQuery &= " AND MCatId = @MCatId "
                objDataOperation.AddParameter("@MCatId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalCatId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "Medical Category : ") & " " & mstrMedicalCatName & " "
            End If

            If mintEmplTypeId > 0 Then
                Me._FilterQuery &= " AND EmplTypeId = @EmplTypeId "
                objDataOperation.AddParameter("@EmplTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmplTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Employment Type : ") & " " & mstrEmplTypeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, " Order By : ") & " " & Me.OrderByDisplay
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyUnkid As Integer = 0)
        Dim ObjRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid < 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid < 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid

            ObjRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = ObjRpt
            If Not IsNothing(ObjRpt) Then
                Call ReportExecute(ObjRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan [08 September 2015] -- End



    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_DetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_DetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("Membership_No", Language.getMessage(mstrModuleName, 200, "Member's Number")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 201, "Member's Name")))
            iColumn_DetailReport.Add(New IColumn("EmplType", Language.getMessage(mstrModuleName, 202, "Employment Type")))
            iColumn_DetailReport.Add(New IColumn("Medical_Category", Language.getMessage(mstrModuleName, 203, "Medical Category")))
            iColumn_DetailReport.Add(New IColumn("Rate", Language.getMessage(mstrModuleName, 204, "Category Amount")))
            iColumn_DetailReport.Add(New IColumn("Tot_Dpndt", Language.getMessage(mstrModuleName, 205, "Employee Dependants")))
            iColumn_DetailReport.Add(New IColumn("Excess_dpndt", Language.getMessage(mstrModuleName, 206, "Excess Dependants")))
            iColumn_DetailReport.Add(New IColumn("Emp_Amt", Language.getMessage(mstrModuleName, 207, "Employee Contribution")))
            iColumn_DetailReport.Add(New IColumn("Empr_Amt", Language.getMessage(mstrModuleName, 208, "Employer Contribution")))
            iColumn_DetailReport.Add(New IColumn("Total", Language.getMessage(mstrModuleName, 209, "Total")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Anjan [08 September 2015] -- End


        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)



            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    mstrAdvanceFilter = " AND " & mstrAdvanceFilter
            'End If
            'Anjan [08 September 2015] -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If mintEmployeeId > 0 Then
                StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeId > 0 Then
                StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @Employeeunkid "
            End If

            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @AsonDate "
                objDataOperation.AddParameter("@AsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= "SELECT " & _
                    "	 Membership_No " & _
                    "	,EmpName " & _
                    "	,EmplType " & _
                    "	,Medical_Category " & _
                    "	,Rate " & _
                    "	,ISNULL(Tot_Dpndt,0) AS Tot_Dpndt " & _
                    "	,ISNULL(Excess_dpndt,0) AS Excess_dpndt " & _
                    "	,Emp_Amt " & _
                    "	,Empr_Amt " & _
                    "	,Total " & _
                    "   ,Id " & _
                    "   ,GName " & _
                    "   ,EmpId " & _
                    "   ,MCatId " & _
                    "   ,EmplTypeId " & _
                    "FROM " & _
                    "( " & _
                    "	SELECT " & _
                    "		 Membership_No " & _
                    "		,EmpName " & _
                    "		,EmplType " & _
                    "		,Medical_Category " & _
                    "		,Rate " & _
                    "		,Tot_Dpndt " & _
                    "		,Excess_dpndt " & _
                    "		,Emp_Amt " & _
                    "		,ISNULL(Empr_Amt,0) AS Empr_Amt " & _
                    "		,Emp_Amt + ISNULL(Empr_Amt,0) AS Total " & _
                    "       ,Emp_Contrib.Id " & _
                    "       ,Emp_Contrib.GName " & _
                    "       ,Emp_Contrib.EmpId " & _
                    "       ,MCatId " & _
                    "       ,EmplTypeId " & _
                    "	FROM " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                    "			,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                    "			,ISNULL(cfcommon_master.name,'') AS EmplType " & _
                    "			,ISNULL(mdmedical_master.mdmastername,'') AS Medical_Category " & _
                    "			,mdmedical_category_master.amount AS Rate " & _
                    "			,Dpndt.Tot_Dpndt " & _
                    "			,CASE WHEN ISNULL(Dpndt.Tot_Dpndt,0) > dependants_no THEN (Dpndt.Tot_Dpndt - dependants_no) ELSE 0 END AS Excess_dpndt " & _
                    "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Emp_Amt " & _
                    "			,hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                    "			,hremployee_master.employeeunkid AS EmpId " & _
                    "           ,mdmedical_category_master.mdcategorymasterunkid AS MCatId " & _
                    "           ,cfcommon_master.masterunkid AS EmplTypeId " & _
                    "			,periodunkid AS Periodid "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "		FROM prpayrollprocess_tran " & _
                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "			JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "' " & _
                    "			JOIN mdmedical_cover ON hremployee_master.employeeunkid = mdmedical_cover.employeeunkid " & _
                    "			JOIN mdmedical_category_master ON mdmedical_cover.medicalcategoryunkid = mdmedical_category_master.mdcategorymasterunkid " & _
                    "			JOIN mdmedical_master ON mdmedical_category_master.mdcategorymasterunkid = mdmedical_master.mdmasterunkid " & _
                    "			LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                    "			JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Anjan [08 September 2015] -- End
            StrQ &= mstrAnalysis_Join

            StrQ &= "			LEFT JOIN " & _
                    "			( " & _
                    "				SELECT " & _
                    "					 employeeunkid AS dEmpId " & _
                    "					,COUNT(hrdependants_beneficiaries_tran.dpndtbeneficetranunkid) AS Tot_Dpndt " & _
                    "				FROM hrdependants_beneficiaries_tran " & _
                    "               JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "				WHERE hrdependants_beneficiaries_tran.isvoid = 0 AND #TableDepn.isactive = 1 GROUP BY employeeunkid " & _
                    "			) AS Dpndt ON Dpndt.dEmpId = hremployee_master.employeeunkid " & _
                    "		WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "		AND cfcommon_period_tran.isactive = 1 AND hrmembership_master.membershipunkid = @MemId " & _
                    "		AND periodunkid = @Pid "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= "AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            If mstrAdvanceFilter.Length > 0 Then
                StrQ &= "AND " & mstrAdvanceFilter & " "
            End If


            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Anjan [08 September 2015] -- End

            StrQ &= "	)Emp_Contrib " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		 SELECT " & _
                    "			 hremployee_master.employeeunkid AS Empid " & _
                    "			,prtranhead_master.trnheadname AS EmployerHead " & _
                    "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                    "			,ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                    "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Empr_Amt " & _
                    "			,hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                    "			,hrmembership_master.membershipunkid "
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            StrQ &= "		FROM prpayrollprocess_tran " & _
                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "			LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                    "			JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                    "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
            StrQ &= mstrAnalysis_Join

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "		WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 AND hrmembership_master.membershipunkid = @MemId " & _
                    "			AND periodunkid = @Pid "

            If mstrAdvanceFilter.Length > 0 Then
                StrQ &= "AND " & mstrAdvanceFilter & " "
            End If

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            'Anjan [08 September 2015] -- End



            StrQ &= "	) AS Empr_Contrib ON Emp_Contrib.Empid = Empr_Contrib.Empid AND Emp_Contrib.MembershipUnkId = Empr_Contrib.MembershipId AND Emp_Contrib.Periodid = Empr_Contrib.Periodid " & _
                    ") AS Final_Tab " & _
                    "WHERE 1 = 1 "


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                iCnt += 1
                rpt_Row.Item("Column1") = iCnt.ToString
                rpt_Row.Item("Column2") = dtRow.Item("Membership_No")
                rpt_Row.Item("Column3") = dtRow.Item("EmpName")
                rpt_Row.Item("Column4") = dtRow.Item("EmplType")
                rpt_Row.Item("Column5") = dtRow.Item("Medical_Category")
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("Rate")), GUI.fmtCurrency)
                If dtRow.Item("Tot_Dpndt") <= 0 Then
                    rpt_Row.Item("Column7") = "-"
                Else
                    rpt_Row.Item("Column7") = dtRow.Item("Tot_Dpndt")
                End If
                If dtRow.Item("Excess_dpndt") <= 0 Then
                    rpt_Row.Item("Column8") = "-"
                Else
                    rpt_Row.Item("Column8") = dtRow.Item("Excess_dpndt")
                End If

                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Emp_Amt")), GUI.fmtCurrency)
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("Empr_Amt")), GUI.fmtCurrency)
                rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("Total")), GUI.fmtCurrency)
                rpt_Row.Item("Column12") = dtRow.Item("GName")

                rpt_Row.Item("Column61") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Rate)", "GName ='" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Row.Item("Column62") = dsList.Tables("DataTable").Compute("SUM(Tot_Dpndt)", "GName ='" & dtRow.Item("GName") & "'")
                rpt_Row.Item("Column63") = dsList.Tables("DataTable").Compute("SUM(Excess_dpndt)", "GName ='" & dtRow.Item("GName") & "'")
                rpt_Row.Item("Column64") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Emp_Amt)", "GName ='" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Row.Item("Column65") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Empr_Amt)", "GName ='" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)
                rpt_Row.Item("Column66") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Total)", "GName ='" & dtRow.Item("GName") & "'")), GUI.fmtCurrency)

                rpt_Row.Item("Column71") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Rate)", "")), GUI.fmtCurrency)
                rpt_Row.Item("Column72") = dsList.Tables("DataTable").Compute("SUM(Tot_Dpndt)", "")
                rpt_Row.Item("Column73") = dsList.Tables("DataTable").Compute("SUM(Excess_dpndt)", "")
                rpt_Row.Item("Column74") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Emp_Amt)", "")), GUI.fmtCurrency)
                rpt_Row.Item("Column75") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Empr_Amt)", "")), GUI.fmtCurrency)
                rpt_Row.Item("Column76") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(Total)", "")), GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptMAP_Malawi_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 300, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 301, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 302, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 303, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 304, "SrNo."))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 305, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 306, "Total :"))

            Call ReportFunction.TextChange(objRpt, "txtMemberNo", Language.getMessage(mstrModuleName, 200, "Member's Number"))
            Call ReportFunction.TextChange(objRpt, "txtMemberName", Language.getMessage(mstrModuleName, 201, "Member's Name"))
            Call ReportFunction.TextChange(objRpt, "txtEmplType", Language.getMessage(mstrModuleName, 202, "Employment Type"))
            Call ReportFunction.TextChange(objRpt, "txtMedCategory", Language.getMessage(mstrModuleName, 203, "Medical Category"))
            Call ReportFunction.TextChange(objRpt, "txtCategoryAmt", Language.getMessage(mstrModuleName, 204, "Category Amount"))
            Call ReportFunction.TextChange(objRpt, "txtTotalDependant", Language.getMessage(mstrModuleName, 205, "Employee Dependants"))
            Call ReportFunction.TextChange(objRpt, "txtExcessDependant", Language.getMessage(mstrModuleName, 206, "Excess Dependants"))
            Call ReportFunction.TextChange(objRpt, "txtEmplConrib", Language.getMessage(mstrModuleName, 207, "Employee Contribution"))
            Call ReportFunction.TextChange(objRpt, "txtEmplrContrib", Language.getMessage(mstrModuleName, 208, "Employer Contribution"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 209, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Period :")
            Language.setMessage(mstrModuleName, 101, "Employee :")
            Language.setMessage(mstrModuleName, 102, "Employment Type :")
            Language.setMessage(mstrModuleName, 103, " Order By :")
            Language.setMessage(mstrModuleName, 200, "Member's Number")
            Language.setMessage(mstrModuleName, 201, "Member's Name")
            Language.setMessage(mstrModuleName, 202, "Employment Type")
            Language.setMessage(mstrModuleName, 203, "Medical Category")
            Language.setMessage(mstrModuleName, 204, "Category Amount")
            Language.setMessage(mstrModuleName, 205, "Employee Dependants")
            Language.setMessage(mstrModuleName, 206, "Excess Dependants")
            Language.setMessage(mstrModuleName, 207, "Employee Contribution")
            Language.setMessage(mstrModuleName, 208, "Employer Contribution")
            Language.setMessage(mstrModuleName, 209, "Total")
            Language.setMessage(mstrModuleName, 300, "Prepared By :")
            Language.setMessage(mstrModuleName, 301, "Checked By :")
            Language.setMessage(mstrModuleName, 302, "Approved By :")
            Language.setMessage(mstrModuleName, 303, "Received By :")
            Language.setMessage(mstrModuleName, 304, "SrNo.")
            Language.setMessage(mstrModuleName, 305, "Sub Total :")
            Language.setMessage(mstrModuleName, 306, "Total :")
            Language.setMessage(mstrModuleName, 101, "Medical Category :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
