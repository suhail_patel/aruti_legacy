﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShortListedApplicantReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.objckhAll = New System.Windows.Forms.CheckBox
        Me.dgvApplicant = New System.Windows.Forms.DataGridView
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.lblMappedVac = New System.Windows.Forms.Label
        Me.txtVacancy = New System.Windows.Forms.TextBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.chkFinalApplicant = New System.Windows.Forms.CheckBox
        Me.objbtnSearchRefNo = New eZee.Common.eZeeGradientButton
        Me.lblReferenceNo = New System.Windows.Forms.Label
        Me.cboReferenceNo = New System.Windows.Forms.ComboBox
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhApplicant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        CType(Me.dgvApplicant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 456)
        Me.NavPanel.Size = New System.Drawing.Size(728, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objpnlData)
        Me.gbFilterCriteria.Controls.Add(Me.lblMappedVac)
        Me.gbFilterCriteria.Controls.Add(Me.txtVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.chkFinalApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblReferenceNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboReferenceNo)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(408, 384)
        Me.gbFilterCriteria.TabIndex = 4
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.objckhAll)
        Me.objpnlData.Controls.Add(Me.dgvApplicant)
        Me.objpnlData.Controls.Add(Me.txtSearch)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(100, 135)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(281, 246)
        Me.objpnlData.TabIndex = 114
        '
        'objckhAll
        '
        Me.objckhAll.AutoSize = True
        Me.objckhAll.Location = New System.Drawing.Point(19, 29)
        Me.objckhAll.Name = "objckhAll"
        Me.objckhAll.Size = New System.Drawing.Size(15, 14)
        Me.objckhAll.TabIndex = 115
        Me.objckhAll.UseVisualStyleBackColor = True
        '
        'dgvApplicant
        '
        Me.dgvApplicant.AllowUserToAddRows = False
        Me.dgvApplicant.AllowUserToDeleteRows = False
        Me.dgvApplicant.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvApplicant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvApplicant.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvApplicant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvApplicant.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhApplicant})
        Me.dgvApplicant.Location = New System.Drawing.Point(12, 24)
        Me.dgvApplicant.Name = "dgvApplicant"
        Me.dgvApplicant.RowHeadersVisible = False
        Me.dgvApplicant.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvApplicant.Size = New System.Drawing.Size(255, 218)
        Me.dgvApplicant.TabIndex = 113
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(12, 2)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(255, 21)
        Me.txtSearch.TabIndex = 114
        '
        'lblMappedVac
        '
        Me.lblMappedVac.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedVac.Location = New System.Drawing.Point(8, 114)
        Me.lblMappedVac.Name = "lblMappedVac"
        Me.lblMappedVac.Size = New System.Drawing.Size(98, 15)
        Me.lblMappedVac.TabIndex = 112
        Me.lblMappedVac.Text = "Vancancy Mapped"
        Me.lblMappedVac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVacancy
        '
        Me.txtVacancy.BackColor = System.Drawing.SystemColors.Info
        Me.txtVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVacancy.Location = New System.Drawing.Point(112, 111)
        Me.txtVacancy.Name = "txtVacancy"
        Me.txtVacancy.ReadOnly = True
        Me.txtVacancy.Size = New System.Drawing.Size(255, 21)
        Me.txtVacancy.TabIndex = 111
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 86)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(98, 15)
        Me.lblStatus.TabIndex = 109
        Me.lblStatus.Text = "Approval Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 255
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(112, 83)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(255, 21)
        Me.cboStatus.TabIndex = 108
        '
        'chkFinalApplicant
        '
        Me.chkFinalApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFinalApplicant.Location = New System.Drawing.Point(112, 60)
        Me.chkFinalApplicant.Name = "chkFinalApplicant"
        Me.chkFinalApplicant.Size = New System.Drawing.Size(255, 17)
        Me.chkFinalApplicant.TabIndex = 106
        Me.chkFinalApplicant.Text = "Generate for final short listed applicants"
        Me.chkFinalApplicant.UseVisualStyleBackColor = True
        '
        'objbtnSearchRefNo
        '
        Me.objbtnSearchRefNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRefNo.BorderSelected = False
        Me.objbtnSearchRefNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRefNo.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRefNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRefNo.Location = New System.Drawing.Point(373, 33)
        Me.objbtnSearchRefNo.Name = "objbtnSearchRefNo"
        Me.objbtnSearchRefNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRefNo.TabIndex = 105
        '
        'lblReferenceNo
        '
        Me.lblReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceNo.Location = New System.Drawing.Point(8, 36)
        Me.lblReferenceNo.Name = "lblReferenceNo"
        Me.lblReferenceNo.Size = New System.Drawing.Size(98, 15)
        Me.lblReferenceNo.TabIndex = 93
        Me.lblReferenceNo.Text = "Reference No"
        Me.lblReferenceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReferenceNo
        '
        Me.cboReferenceNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReferenceNo.DropDownWidth = 360
        Me.cboReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReferenceNo.FormattingEnabled = True
        Me.cboReferenceNo.Location = New System.Drawing.Point(112, 33)
        Me.cboReferenceNo.Name = "cboReferenceNo"
        Me.cboReferenceNo.Size = New System.Drawing.Size(255, 21)
        Me.cboReferenceNo.TabIndex = 91
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhApplicant
        '
        Me.objdgcolhApplicant.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.objdgcolhApplicant.HeaderText = ""
        Me.objdgcolhApplicant.Name = "objdgcolhApplicant"
        Me.objdgcolhApplicant.ReadOnly = True
        Me.objdgcolhApplicant.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmShortListedApplicantReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(728, 511)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmShortListedApplicantReport"
        Me.Text = "frmShortListedApplicantReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.objpnlData.ResumeLayout(False)
        Me.objpnlData.PerformLayout()
        CType(Me.dgvApplicant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblReferenceNo As System.Windows.Forms.Label
    Friend WithEvents cboReferenceNo As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchRefNo As eZee.Common.eZeeGradientButton
    Friend WithEvents chkFinalApplicant As System.Windows.Forms.CheckBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents txtVacancy As System.Windows.Forms.TextBox
    Friend WithEvents lblMappedVac As System.Windows.Forms.Label
    Friend WithEvents dgvApplicant As System.Windows.Forms.DataGridView
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objckhAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhApplicant As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
