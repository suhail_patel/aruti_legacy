'************************************************************************************************************************************
'Class Name : clsInterviewAnalysisReport.vb
'Purpose    :
'Date       :23/04/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsInterviewAnalysisReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsInterviewAnalysisReport"
    Private mstrReportId As String = enArutiReport.Interview_Analysis_Report     '53
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintVacancyId As Integer = 0
    Private mstrVacancyName As String = String.Empty
    Private mintApplicantId As Integer = 0
    Private mstrApplicantName As String = String.Empty
    Private mintResultId As Integer = 0
    Private mstrResultName As String = String.Empty
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = String.Empty
    Private mstrBatchName As String = String.Empty
    Private mstrInterviewerName As String = String.Empty


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _VacancyId() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantId() As Integer
        Set(ByVal value As Integer)
            mintApplicantId = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantName() As String
        Set(ByVal value As String)
            mstrApplicantName = value
        End Set
    End Property

    Public WriteOnly Property _ResultId() As Integer
        Set(ByVal value As Integer)
            mintResultId = value
        End Set
    End Property

    Public WriteOnly Property _ResultName() As String
        Set(ByVal value As String)
            mstrResultName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _BatchName() As String
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    Public WriteOnly Property _InterviewerName() As String
        Set(ByVal value As String)
            mstrInterviewerName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintVacancyId = 0
            mstrVacancyName = ""
            mintApplicantId = 0
            mstrApplicantName = ""
            mintResultId = 0
            mstrResultName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mstrBatchName = ""
            mstrInterviewerName = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Level"))
            objDataOperation.AddParameter("@Complete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Complete"))
            objDataOperation.AddParameter("@InComplete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Incomplete"))

            If mintVacancyId > 0 Then
                objDataOperation.AddParameter("@VacancyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
                Me._FilterQuery &= " AND IAnalysis.VacId = @VacancyId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Vacancy :") & " " & mstrVacancyName & " "
            End If

            If mintApplicantId > 0 Then
                objDataOperation.AddParameter("@ApplicantId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantId)
                Me._FilterQuery &= " AND IAnalysis.ApplId = @ApplicantId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Applicant :") & " " & mstrApplicantName & " "
            End If

            If mintResultId > 0 Then
                objDataOperation.AddParameter("@ResultId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultId)
                Me._FilterQuery &= " AND IAnalysis.ResultId = @ResultId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Result :") & " " & mstrResultName & " "
            End If

            If mintStatusId > 0 Then
                objDataOperation.AddParameter("@StatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND IAnalysis.StatusId = @StatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Status :") & " " & mstrStatusName & " "
            End If

            If mstrBatchName.Trim <> "" Then
                objDataOperation.AddParameter("@BatchName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrBatchName & "%")
                Me._FilterQuery &= " AND IAnalysis .BatchName LIKE @BatchName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Batch :") & " " & mstrBatchName & " "
            End If

            If mstrInterviewerName.Trim <> "" Then
                objDataOperation.AddParameter("@InterviewerName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrInterviewerName & "%")
                Me._FilterQuery &= " AND IAnalysis.Interviewer LIKE @InterviewerName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Interviewer :") & " " & mstrBatchName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_Interview_Status(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = " SELECT 0 AS Id, @Select AS Name " & _
                        "UNION SELECT 1 AS Id, @Complete AS Name " & _
                        " UNION SELECT 2 AS Id, @InComplete AS Name  "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "Select"))
            objDataOperation.AddParameter("@Complete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Complete"))
            objDataOperation.AddParameter("@InComplete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Incomplete"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Interview_Status; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("Vacancy", Language.getMessage(mstrModuleName, 11, "Vacancy")))
            iColumn_DetailReport.Add(New IColumn("BatchName", Language.getMessage(mstrModuleName, 12, "Batch")))
            iColumn_DetailReport.Add(New IColumn("Applicant", Language.getMessage(mstrModuleName, 13, "Applicant")))
            iColumn_DetailReport.Add(New IColumn("Interviewer", Language.getMessage(mstrModuleName, 14, "Interviewer")))
            iColumn_DetailReport.Add(New IColumn("AnaStatus", Language.getMessage(mstrModuleName, 15, "Status")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT" & _
            '            "     Vacancy AS Vacancy" & _
            '            "    ,BatchName AS BatchName" & _
            '            "    ,Applicant AS Applicant" & _
            '            "    ,Interviewer AS Interviewer" & _
            '            "    ,ILevel AS ILevel" & _
            '            "    ,Result AS Result" & _
            '            "    ,Remark AS Remark" & _
            '            "    ,AnaStatus AS AnaStatus" & _
            '            "    ,VacId AS VacId " & _
            '            "FROM " & _
            '            "(" & _
            '            "    SELECT" & _
            '            "         ISNULL(rcvacancy_master.vacancytitle,'') AS Vacancy" & _
            '            "        ,ISNULL(rcbatchschedule_master.batchname,'') AS BatchName" & _
            '            "        ,ISNULL(rcapplicant_master.firstname,'')+'  '+ISNULL(rcapplicant_master.othername,'')+'  '+ISNULL(rcapplicant_master.surname,'') AS Applicant" & _
            '            "        ,ISNULL(CASE WHEN interviewerunkid < 0 THEN ISNULL(rcinterviewer_tran.otherinterviewer_name,'')" & _
            '            "                     WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') END,'') AS Interviewer" & _
            '            "        ,ISNULL(@Level +' '+(CAST(rcinterviewer_tran.interviewer_level AS NVARCHAR(50))),'') AS ILevel" & _
            '            "        ,ISNULL(hrresult_master.resultname,'') AS Result" & _
            '            "        ,ISNULL(rcinterviewanalysis_tran.remark,'') AS Remark" & _
            '            "        ,ISNULL(CASE WHEN rcinterviewanalysis_master.iscomplete = 1 THEN @Complete " & _
            '            "                     WHEN rcinterviewanalysis_master.iscomplete = 0 THEN @InComplete END,@InComplete) AS AnaStatus " & _
            '            "        ,rcvacancy_master.vacancyunkid AS VacId" & _
            '            "        ,rcapplicant_master.applicantunkid AS ApplId" & _
            '            "        ,ISNULL(CASE WHEN rcinterviewanalysis_master.iscomplete = 1 THEN 1" & _
            '            "                     WHEN rcinterviewanalysis_master.iscomplete = 0 THEN 2 END,2) AS StatusId " & _
            '            "        ,ISNULL(hrresult_master.resultunkid,0) AS ResultId               " & _
            '            "    FROM rcapplicant_batchschedule_tran" & _
            '            "        JOIN rcapplicant_master ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid" & _
            '            "        JOIN rcbatchschedule_master ON rcapplicant_batchschedule_tran.batchscheduleunkid = rcbatchschedule_master.batchscheduleunkid" & _
            '            "        JOIN rcvacancy_master ON rcbatchschedule_master.vacancyunkid = rcvacancy_master.vacancyunkid" & _
            '            "        LEFT JOIN rcinterviewanalysis_master ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid AND rcinterviewanalysis_master.isvoid = 0" & _
            '            "        LEFT JOIN rcinterviewanalysis_tran ON rcinterviewanalysis_master.analysisunkid = rcinterviewanalysis_tran.analysisunkid" & _
            '            "        LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid" & _
            '            "        LEFT JOIN rcinterviewer_tran ON rcinterviewanalysis_tran.interviewertranunkid = rcinterviewer_tran.interviewertranunkid AND rcinterviewanalysis_tran.isvoid = 0" & _
            '            "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcinterviewer_tran.interviewerunkid" & _
            '            "    WHERE rcapplicant_batchschedule_tran.isvoid = 0"

            StrQ = "SELECT" & _
                        "     Vacancy AS Vacancy" & _
                        "    ,BatchName AS BatchName" & _
                        "    ,Applicant AS Applicant" & _
                        "    ,Interviewer AS Interviewer" & _
                        "    ,ILevel AS ILevel" & _
                        "    ,Result AS Result" & _
                        "    ,Remark AS Remark" & _
                        "    ,AnaStatus AS AnaStatus" & _
                        "    ,VacId AS VacId " & _
                        "FROM " & _
                        "(" & _
                        "    SELECT" & _
                        "         cfcommon_master.name AS Vacancy" & _
                        "        ,ISNULL(rcbatchschedule_master.batchname,'') AS BatchName" & _
                        "        ,ISNULL(rcapplicant_master.firstname,'')+'  '+ISNULL(rcapplicant_master.othername,'')+'  '+ISNULL(rcapplicant_master.surname,'') AS Applicant" & _
                        "        ,ISNULL(CASE WHEN interviewerunkid < 0 THEN ISNULL(rcbatchschedule_interviewer_tran.otherinterviewer_name,'')" & _
                        "                     WHEN interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') END,'') AS Interviewer" & _
                        "        ,ISNULL(@Level +' '+(CAST(rcbatchschedule_interviewer_tran.interviewer_level AS NVARCHAR(50))),'') AS ILevel" & _
                        "        ,ISNULL(hrresult_master.resultname,'') AS Result" & _
                        "        ,ISNULL(rcinterviewanalysis_tran.remark,'') AS Remark" & _
                        "        ,ISNULL(CASE WHEN rcinterviewanalysis_master.iscomplete = 1 THEN @Complete " & _
                        "                     WHEN rcinterviewanalysis_master.iscomplete = 0 THEN @InComplete END,@InComplete) AS AnaStatus " & _
                        "        ,rcvacancy_master.vacancyunkid AS VacId" & _
                        "        ,rcapplicant_master.applicantunkid AS ApplId" & _
                        "        ,ISNULL(CASE WHEN rcinterviewanalysis_master.iscomplete = 1 THEN 1" & _
                        "                     WHEN rcinterviewanalysis_master.iscomplete = 0 THEN 2 END,2) AS StatusId " & _
                        "        ,ISNULL(hrresult_master.resultunkid,0) AS ResultId               " & _
                        "    FROM rcapplicant_batchschedule_tran" & _
                        "        JOIN rcapplicant_master ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid" & _
                        "        JOIN rcbatchschedule_master ON rcapplicant_batchschedule_tran.batchscheduleunkid = rcbatchschedule_master.batchscheduleunkid" & _
                        "        JOIN rcvacancy_master ON rcbatchschedule_master.vacancyunkid = rcvacancy_master.vacancyunkid" & _
                        "        LEFT JOIN rcinterviewanalysis_master ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid AND rcinterviewanalysis_master.isvoid = 0" & _
                        "        LEFT JOIN rcinterviewanalysis_tran ON rcinterviewanalysis_master.analysisunkid = rcinterviewanalysis_tran.analysisunkid" & _
                        "        LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid" & _
                        "        LEFT JOIN rcbatchschedule_interviewer_tran ON rcinterviewanalysis_tran.interviewertranunkid = rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid AND rcinterviewanalysis_tran.isvoid = 0" & _
                        "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid" & _
                        "        JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle and cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "  " & _
                        "    WHERE rcapplicant_batchschedule_tran.isvoid = 0 "
            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran]
            'Anjan (02 Mar 2012)-End 




            If mblnIsActive = False Then
                StrQ &= " AND hremployee_master.isactive = 1"
            End If

            StrQ &= " ) AS IAnalysis " & _
                        "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("VacId")
                rpt_Rows.Item("Column2") = dtRow.Item("Vacancy")
                rpt_Rows.Item("Column3") = dtRow.Item("BatchName")
                rpt_Rows.Item("Column4") = dtRow.Item("Applicant")
                rpt_Rows.Item("Column5") = dtRow.Item("Interviewer")
                rpt_Rows.Item("Column6") = dtRow.Item("ILevel")
                rpt_Rows.Item("Column7") = dtRow.Item("Result")
                rpt_Rows.Item("Column8") = dtRow.Item("Remark")
                rpt_Rows.Item("Column9") = dtRow.Item("AnaStatus")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptInterviewAnalysisReport


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 16, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 17, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 18, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 19, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtVacancytitle", Language.getMessage(mstrModuleName, 20, "Vacancy :"))
            Call ReportFunction.TextChange(objRpt, "txtBatchName", Language.getMessage(mstrModuleName, 21, "Batch Name :"))
            Call ReportFunction.TextChange(objRpt, "txtApplicant", Language.getMessage(mstrModuleName, 22, "Applicant :"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 23, "Status :"))
            Call ReportFunction.TextChange(objRpt, "txtInterviewer", Language.getMessage(mstrModuleName, 14, "Interviewer"))
            Call ReportFunction.TextChange(objRpt, "txtLevel", Language.getMessage(mstrModuleName, 24, "Level"))
            Call ReportFunction.TextChange(objRpt, "txtResult", Language.getMessage(mstrModuleName, 25, "Result"))
            Call ReportFunction.TextChange(objRpt, "txtRemark", Language.getMessage(mstrModuleName, 26, "Remark"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 27, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 28, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Level")
            Language.setMessage(mstrModuleName, 2, "Complete")
            Language.setMessage(mstrModuleName, 3, "Incomplete")
            Language.setMessage(mstrModuleName, 4, "Vacancy :")
            Language.setMessage(mstrModuleName, 5, "Applicant :")
            Language.setMessage(mstrModuleName, 6, "Result :")
            Language.setMessage(mstrModuleName, 7, "Status :")
            Language.setMessage(mstrModuleName, 8, "Batch :")
            Language.setMessage(mstrModuleName, 9, "Interviewer :")
            Language.setMessage(mstrModuleName, 10, "Order By :")
            Language.setMessage(mstrModuleName, 11, "Vacancy")
            Language.setMessage(mstrModuleName, 12, "Batch")
            Language.setMessage(mstrModuleName, 13, "Applicant")
            Language.setMessage(mstrModuleName, 14, "Interviewer")
            Language.setMessage(mstrModuleName, 15, "Status")
            Language.setMessage(mstrModuleName, 16, "Prepared By :")
            Language.setMessage(mstrModuleName, 17, "Checked By :")
            Language.setMessage(mstrModuleName, 18, "Approved By :")
            Language.setMessage(mstrModuleName, 19, "Received By :")
            Language.setMessage(mstrModuleName, 20, "Vacancy :")
            Language.setMessage(mstrModuleName, 21, "Batch Name :")
            Language.setMessage(mstrModuleName, 22, "Applicant :")
            Language.setMessage(mstrModuleName, 23, "Status :")
            Language.setMessage(mstrModuleName, 24, "Level")
            Language.setMessage(mstrModuleName, 25, "Result")
            Language.setMessage(mstrModuleName, 26, "Remark")
            Language.setMessage(mstrModuleName, 27, "Printed By :")
            Language.setMessage(mstrModuleName, 28, "Printed Date :")
            Language.setMessage(mstrModuleName, 29, "Page :")
            Language.setMessage(mstrModuleName, 30, "Select")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
