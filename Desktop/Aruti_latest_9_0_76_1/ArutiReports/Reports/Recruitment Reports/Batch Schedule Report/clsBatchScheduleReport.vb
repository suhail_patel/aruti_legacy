'************************************************************************************************************************************
'Class Name : clsBatchScheduleReport.vb
'Purpose    :
'Date           : 22/04/2011
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsBatchScheduleReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsBatchScheduleReport"
    Private mstrReportId As String = enArutiReport.Batch_Schedule_Report     '52
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintVacancyId As Integer = 0
    Private mstrVacancyName As String = String.Empty
    Private mintInterviewTypeId As Integer = 0
    Private mstrInterviewTypeName As String = String.Empty
    Private mstrInterviewerName As String = String.Empty
    Private mstrBatchName As String = String.Empty
    Private mstrLocationName As String = String.Empty
    Private mintResultGroupId As Integer = 0
    Private mstrResultGroupName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _VacancyId() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    Public WriteOnly Property _InterviewTypeId() As Integer
        Set(ByVal value As Integer)
            mintInterviewTypeId = value
        End Set
    End Property

    Public WriteOnly Property _InterviewTypeName() As String
        Set(ByVal value As String)
            mstrInterviewTypeName = value
        End Set
    End Property

    Public WriteOnly Property _InterviewerName() As String
        Set(ByVal value As String)
            mstrInterviewerName = value
        End Set
    End Property

    Public WriteOnly Property _BatchName() As String
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    Public WriteOnly Property _LocationName() As String
        Set(ByVal value As String)
            mstrLocationName = value
        End Set
    End Property

    Public WriteOnly Property _ResultGroupId() As Integer
        Set(ByVal value As Integer)
            mintResultGroupId = value
        End Set
    End Property

    Public WriteOnly Property _ResultGroupName() As String
        Set(ByVal value As String)
            mstrResultGroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintVacancyId = 0
            mstrVacancyName = ""
            mintInterviewTypeId = 0
            mstrInterviewTypeName = ""
            mstrInterviewerName = ""
            mstrBatchName = ""
            mstrLocationName = ""
            mintResultGroupId = 0
            mstrResultGroupName = ""
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintVacancyId > 0 Then
                objDataOperation.AddParameter("@VacancyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
                Me._FilterQuery &= " AND Batch.VacId = @VacancyId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Vacancy  : ") & " " & mstrVacancyName & " "
            End If

            If mintInterviewTypeId > 0 Then
                objDataOperation.AddParameter("@InterviewTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterviewTypeId)
                Me._FilterQuery &= " AND Batch.IntTypeId = @InterviewTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Interview Type  : ") & " " & mstrInterviewTypeName & " "
            End If

            If mstrInterviewerName.Trim <> "" Then
                objDataOperation.AddParameter("@InterviewerName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrInterviewerName & "%")
                Me._FilterQuery &= " AND Batch.Interviewer LIKE @InterviewerName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Interviewer : ") & " " & mstrInterviewerName & " "
            End If
            If mstrBatchName.Trim <> "" Then
                objDataOperation.AddParameter("@BatchName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrBatchName & "%")
                Me._FilterQuery &= " AND Batch.BName LIKE @BatchName  "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Batch : ") & " " & mstrBatchName & " "
            End If
            If mstrLocationName.Trim <> "" Then
                objDataOperation.AddParameter("@LocationName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrLocationName & "%")
                Me._FilterQuery &= " AND Batch.Location LIKE @LocationName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Location : ") & " " & mstrLocationName & " "
            End If
            If mintResultGroupId > 0 Then
                objDataOperation.AddParameter("@ResultGroupId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultGroupId)
                Me._FilterQuery &= " AND Batch.ResultId = @ResultGroupId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Result Group : ") & " " & mstrResultGroupName & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Order By : ") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show(-1, ex.Message, "generateReport", mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "generateReport", mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setDefaultOrderBy", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setOrderBy", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("VacTitle", Language.getMessage(mstrModuleName, 1, "Vacancy Title")))
            iColumn_DetailReport.Add(New IColumn("BName", Language.getMessage(mstrModuleName, 2, "Batch Name")))
            iColumn_DetailReport.Add(New IColumn("Location", Language.getMessage(mstrModuleName, 3, "Location")))
            iColumn_DetailReport.Add(New IColumn("IntType", Language.getMessage(mstrModuleName, 4, "Interview Type")))
            iColumn_DetailReport.Add(New IColumn("Interviewer", Language.getMessage(mstrModuleName, 5, "Interviewer")))
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Create_OnDetailReport", mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = intUserUnkid
            'Shani(24-Aug-2015) -- End

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT" & _
            '            "     VacId AS VacId" & _
            '            "    ,VacTitle AS VacTitle" & _
            '            "    ,BCode AS BCode" & _
            '            "    ,BName AS BName" & _
            '            "    ,IDate AS IDate" & _
            '            "    ,ITime AS ITime" & _
            '            "    ,Location AS Location" & _
            '            "    ,IntType AS IntType" & _
            '            "    ,Interviewer AS Interviewer " & _
            '            "    ,ResultG AS ResultG " & _
            '            "FROM " & _
            '            "(" & _
            '            "  SELECT" & _
            '            "     rcvacancy_master.vacancyunkid AS VacId" & _
            '            "    ,ISNULL(rcvacancy_master.vacancytitle,'') AS VacTitle" & _
            '            "    ,ISNULL(batchcode,'') AS BCode" & _
            '            "    ,ISNULL(batchname,'') AS BName" & _
            '            "    ,CONVERT(CHAR(8),interviewdate,112) AS IDate" & _
            '            "    ,CONVERT(CHAR(8),interviewtime,108) AS ITime" & _
            '            "    ,ISNULL(location,'') AS Location" & _
            '            "    ,ISNULL(IntType.name,'') AS IntType" & _
            '            "    ,CASE WHEN rcinterviewer_tran.interviewerunkid < 0 THEN ISNULL(rcinterviewer_tran.otherinterviewer_name,'')" & _
            '            "               WHEN rcinterviewer_tran.interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') " & _
            '            "     END AS Interviewer" & _
            '            "    ,ISNULL(RG.name,'') AS ResultG" & _
            '            "    ,IntType.masterunkid AS IntTypeId" & _
            '            "    ,RG.masterunkid AS ResultId" & _
            '            "  FROM rcbatchschedule_master" & _
            '            "    JOIN cfcommon_master AS RG ON rcbatchschedule_master.resultgroupunkid = RG.masterunkid AND RG.mastertype = 21" & _
            '            "    JOIN cfcommon_master AS IntType ON rcbatchschedule_master.interviewtypeunkid = IntType.masterunkid AND IntType.mastertype = 13" & _
            '            "    JOIN rcvacancy_master ON rcbatchschedule_master.vacancyunkid = rcvacancy_master.vacancyunkid" & _
            '            "    JOIN rcinterviewer_tran ON rcvacancy_master.vacancyunkid = rcinterviewer_tran.vacancyunkid " & _
            '            "         AND rcinterviewer_tran.interviewtypeunkid = rcbatchschedule_master.interviewtypeunkid" & _
            '            "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcinterviewer_tran.interviewerunkid" & _
            '            "  WHERE ISNULL(rcbatchschedule_master.iscancel,0) = 0 AND ISNULL(rcbatchschedule_master.isvoid,0) = 0" & _
            '            "    AND ISNULL(rcbatchschedule_master.isclosed,0) = 0" & _
            '            ") AS Batch " & _
            '            "WHERE 1 = 1 "

            StrQ = "SELECT" & _
                        "     VacId AS VacId" & _
                        "    ,VacTitle AS VacTitle" & _
                        "    ,BCode AS BCode" & _
                        "    ,BName AS BName" & _
                        "    ,IDate AS IDate" & _
                        "    ,ITime AS ITime" & _
                        "    ,Location AS Location" & _
                        "    ,IntType AS IntType" & _
                        "    ,Interviewer AS Interviewer " & _
                        "    ,ResultG AS ResultG " & _
                        "FROM " & _
                        "(" & _
                        "  SELECT" & _
                        "     rcvacancy_master.vacancyunkid AS VacId" & _
                        "    ,VacTitle.name AS VacTitle" & _
                        "    ,ISNULL(batchcode,'') AS BCode" & _
                        "    ,ISNULL(batchname,'') AS BName" & _
                        "    ,CONVERT(CHAR(8),interviewdate,112) AS IDate" & _
                        "    ,CONVERT(CHAR(8),interviewtime,108) AS ITime" & _
                        "    ,ISNULL(location,'') AS Location" & _
                        "    ,ISNULL(IntType.name,'') AS IntType" & _
                        "    ,CASE WHEN rcbatchschedule_interviewer_tran.interviewerunkid < 0 THEN ISNULL(rcbatchschedule_interviewer_tran.otherinterviewer_name,'')" & _
                        "               WHEN rcbatchschedule_interviewer_tran.interviewerunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+''+ISNULL(hremployee_master.othername,'')+''+ISNULL(hremployee_master.surname,'') " & _
                        "     END AS Interviewer" & _
                        "    ,ISNULL(RG.name,'') AS ResultG" & _
                        "    ,IntType.masterunkid AS IntTypeId" & _
                        "    ,RG.masterunkid AS ResultId" & _
                        "  FROM rcbatchschedule_master" & _
                        "    JOIN cfcommon_master AS RG ON rcbatchschedule_master.resultgroupunkid = RG.masterunkid AND RG.mastertype = " & clsCommon_Master.enCommonMaster.RESULT_GROUP & " " & _
                        "    JOIN cfcommon_master AS IntType ON rcbatchschedule_master.interviewtypeunkid = IntType.masterunkid AND IntType.mastertype = " & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & " " & _
                        "    JOIN rcvacancy_master ON rcbatchschedule_master.vacancyunkid = rcvacancy_master.vacancyunkid" & _
                        "    JOIN rcbatchschedule_interviewer_tran ON rcvacancy_master.vacancyunkid = rcbatchschedule_interviewer_tran.vacancyunkid " & _
                        "         AND rcbatchschedule_interviewer_tran.interviewtypeunkid = rcbatchschedule_master.interviewtypeunkid" & _
                        "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid" & _
                        "    JOIN cfcommon_master AS VacTitle ON VacTitle.masterunkid =rcvacancy_master.vacancytitle AND VacTitle.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
                        "  WHERE ISNULL(rcbatchschedule_master.iscancel,0) = 0 AND ISNULL(rcbatchschedule_master.isvoid,0) = 0" & _
                        "    AND ISNULL(rcbatchschedule_master.isclosed,0) = 0" & _
                        ") AS Batch " & _
                        "WHERE 1 = 1 "

            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran]
            'Anjan (02 Mar 2012)-End 


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("VacId")
                rpt_Row.Item("Column2") = dtRow.Item("VacTitle")
                rpt_Row.Item("Column3") = dtRow.Item("BCode")
                rpt_Row.Item("Column4") = dtRow.Item("BName")
                rpt_Row.Item("Column5") = eZeeDate.convertDate(dtRow.Item("IDate").ToString).ToShortDateString & vbCrLf & dtRow.Item("ITime").ToString
                rpt_Row.Item("Column6") = dtRow.Item("Location")
                rpt_Row.Item("Column7") = dtRow.Item("IntType")
                rpt_Row.Item("Column8") = dtRow.Item("Interviewer")
                rpt_Row.Item("Column9") = dtRow.Item("ResultG")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptBatchScheduleReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtVacancyTitle", Language.getMessage(mstrModuleName, 1, "Vacancy Title"))
            Call ReportFunction.TextChange(objRpt, "txtBatchCode", Language.getMessage(mstrModuleName, 10, "Batch Code"))
            Call ReportFunction.TextChange(objRpt, "txtBatchName", Language.getMessage(mstrModuleName, 2, "Batch Name"))
            Call ReportFunction.TextChange(objRpt, "txtDateTime", Language.getMessage(mstrModuleName, 11, "Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 3, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtInterviewType", Language.getMessage(mstrModuleName, 4, "Interview Type"))
            Call ReportFunction.TextChange(objRpt, "txtInterviewer", Language.getMessage(mstrModuleName, 5, "Interviewer"))
            Call ReportFunction.TextChange(objRpt, "txtResultGroup", Language.getMessage(mstrModuleName, 12, "Result Group"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 16, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Generate_DetailReport", mstrModuleName)
            Return Nothing

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Finally : objUser = Nothing
            'Shani(24-Aug-2015) -- End
        End Try
    End Function

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Vacancy Title")
            Language.setMessage(mstrModuleName, 2, "Batch Name")
            Language.setMessage(mstrModuleName, 3, "Location")
            Language.setMessage(mstrModuleName, 4, "Interview Type")
            Language.setMessage(mstrModuleName, 5, "Interviewer")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Batch Code")
            Language.setMessage(mstrModuleName, 11, "Date & Time")
            Language.setMessage(mstrModuleName, 12, "Result Group")
            Language.setMessage(mstrModuleName, 14, "Printed By :")
            Language.setMessage(mstrModuleName, 15, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, "Page :")
            Language.setMessage(mstrModuleName, 17, "Vacancy  :")
            Language.setMessage(mstrModuleName, 18, "Interview Type  :")
            Language.setMessage(mstrModuleName, 19, "Interviewer :")
            Language.setMessage(mstrModuleName, 20, "Batch :")
            Language.setMessage(mstrModuleName, 21, "Location :")
            Language.setMessage(mstrModuleName, 22, "Result Group :")
            Language.setMessage(mstrModuleName, 23, "Order By :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
