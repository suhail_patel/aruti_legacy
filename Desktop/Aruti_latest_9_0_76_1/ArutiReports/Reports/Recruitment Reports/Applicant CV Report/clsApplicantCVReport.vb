'************************************************************************************************************************************
'Class Name :clsApplicantCVReport.vb
'Purpose    :
'Date       :10 May 2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Net

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsApplicantCVReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsApplicantCVReport"
    Private mstrReportId As String = enArutiReport.ApplicantCVReport     '90
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintApplicantUnkid As Integer = 0
    Private mblnShowProfessional_Skills As Boolean = False
    Private mblnShowOther_Skills As Boolean = False
    Private mblnShowProfessional_Qualification As Boolean = False
    Private mblnShowOther_Qualification As Boolean = False
    Private mblnShowJobHistory As Boolean = False
    Private mblnShowReferences As Boolean = False
    'Sohail (02 Jun 2015) -- Start
    'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
    Private mblnShowAttachments As Boolean = False
    Private marrPath As New ArrayList
    'Sohail (02 Jun 2015) -- End

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Private strDocumentPath As String = ""
    'Shani(24-Aug-2015) -- End

    'Varsha (03 Jan 2018) -- Start
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintVacancyTypeId As Integer = 0
    Private mstrVacancyTypeName As String = ""
    Private mintVacancyId As Integer = 0
    Private mstrVacancyName As String = ""
    Private mintApprovalStatusId As Integer = 0
    Private mstrApprovalStatusName As String = ""
    'Varsha (03 Jan 2018) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _ApplicantUnkid() As Integer
        Set(ByVal value As Integer)
            mintApplicantUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ShowProfessional_Skills() As Boolean
        Set(ByVal value As Boolean)
            mblnShowProfessional_Skills = value
        End Set
    End Property

    Public WriteOnly Property _ShowProfessional_Qualification() As Boolean
        Set(ByVal value As Boolean)
            mblnShowProfessional_Qualification = value
        End Set
    End Property

    Public WriteOnly Property _ShowOther_Qualification() As Boolean
        Set(ByVal value As Boolean)
            mblnShowOther_Qualification = value
        End Set
    End Property

    Public WriteOnly Property _ShowJobHistory() As Boolean
        Set(ByVal value As Boolean)
            mblnShowJobHistory = value
        End Set
    End Property

    Public WriteOnly Property _ShowReferences() As Boolean
        Set(ByVal value As Boolean)
            mblnShowReferences = value
        End Set
    End Property

    Public WriteOnly Property _ShowOther_Skills() As Boolean
        Set(ByVal value As Boolean)
            mblnShowOther_Skills = value
        End Set
    End Property

    'Sohail (02 Jun 2015) -- Start
    'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
    Public WriteOnly Property _ShowAttachments() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAttachments = value
        End Set
    End Property
    'Sohail (02 Jun 2015) -- End

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public WriteOnly Property _DocumentPath() As String
        Set(ByVal value As String)
            strDocumentPath = value
        End Set
    End Property

    'Shani(24-Aug-2015) -- End

    'Varsha (03 Jan 2018) -- Start
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property
    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property
    Public WriteOnly Property _VacancyTypeId() As Integer
        Set(ByVal value As Integer)
            mintVacancyTypeId = value
        End Set
    End Property
    Public WriteOnly Property _VacancyTypeName() As String
        Set(ByVal value As String)
            mstrVacancyTypeName = value
        End Set
    End Property
    Public WriteOnly Property _VacancyId() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property
    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property
    Public WriteOnly Property _ApprovalStatusId() As Integer
        Set(ByVal value As Integer)
            mintApprovalStatusId = value
        End Set
    End Property
    Public WriteOnly Property _ApprovalStatusName() As String
        Set(ByVal value As String)
            mstrApprovalStatusName = value
        End Set
    End Property
    'Varsha (03 Jan 2018) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintApplicantUnkid = 0
            mblnShowProfessional_Skills = False
            mblnShowOther_Skills = False
            mblnShowProfessional_Qualification = False
            mblnShowOther_Qualification = False
            mblnShowJobHistory = False
            mblnShowReferences = False
            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            mblnShowAttachments = False
            'Sohail (02 Jun 2015) -- End

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintVacancyTypeId = 0
            mstrVacancyTypeName = ""
            mintVacancyId = 0
            mstrVacancyName = ""
            mintApprovalStatusId = 0
            mstrApprovalStatusName = ""
            'Varsha (03 Jan 2018) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)

        '        'Sohail (02 Jun 2015) -- Start
        '        'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
        '        For Each sPath In marrPath
        '            'IO.File.op(CStr(sPath), AppWinStyle.MaximizedFocus, True)
        '            Process.Start(CStr(sPath))
        '        Next
        '        'Sohail (02 Jun 2015) -- End
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                For Each sPath In marrPath
                    Process.Start(CStr(sPath))
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "


    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_MainDetails As ArutiReport.Designer.dsArutiReport
        Dim rpt_ProfSkill As ArutiReport.Designer.dsArutiReport
        Dim rpt_OtherSkill As ArutiReport.Designer.dsArutiReport
        Dim rpt_ProfQualif As ArutiReport.Designer.dsArutiReport
        Dim rpt_OtherQualif As ArutiReport.Designer.dsArutiReport
        Dim rpt_JobHistory As ArutiReport.Designer.dsArutiReport
        Dim rpt_References As ArutiReport.Designer.dsArutiReport
        'Sohail (02 Jun 2015) -- Start
        'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
        Dim rpt_Attachments As ArutiReport.Designer.dsArutiReport
        Dim strAppRefNo As String = ""
        'Sohail (02 Jun 2015) -- End
        Try
            objDataOperation = New clsDataOperation

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            'Sohail (18 Apr 2020) -- Start
            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
            Dim strVacTypeFilter As String = ""
            If mintVacancyTypeId = enVacancyType.EXTERNAL_VACANCY Then
                strVacTypeFilter = " AND (Vac.isexternalvacancy = 1 AND ISNULL(Vac.isbothintext, 0) = 0) "
            ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_VACANCY Then
                strVacTypeFilter = " AND (Vac.isexternalvacancy = 0 AND ISNULL(Vac.isbothintext, 0) = 0) "
            ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_AND_EXTERNAL Then
                strVacTypeFilter = " AND ISNULL(Vac.isbothintext, 0) = 1 "
            End If
            'Sohail (18 Apr 2020) -- End

            '*************************> MAIN DETAILS <************************* [START]

            StrQ = "IF OBJECT_ID('tempdb..##TEMP') IS NOT NULL  DROP TABLE ##TEMP; SELECT * INTO ##Temp FROM " & _
                    " ( " & _
                    "    SELECT DISTINCT " & _
                    "	 ISNULL(cfcommon_master.name,'') AS TITLE " & _
                    "	,ISNULL(surname,'') AS SURNAME " & _
                    "	,ISNULL(firstname,'') AS FIRSTNAME " & _
                    "	,ISNULL(othername,'') AS OTHERNAME " & _
                    "	,CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS GENDER " & _
                    "	,ISNULL(employeecode,'') AS EMP_CODE " & _
                    "	,ISNULL(email,'') AS EMAIL " & _
                    "	,ISNULL(CONVERT(CHAR(8),birth_date,112),'') AS BIRTHDATE " & _
                    "	,ISNULL(MS.name,'') AS MARITAL_STATUS " & _
                    "	,ISNULL(CONVERT(CHAR(8),anniversary_date,112),'') AS MARRIED_DATE " & _
                    "	,ISNULL(Vac.vacancy,'') AS VACANCY " & _
                    "	,ISNULL(Vac.ODate,'') AS ODate " & _
                    "	,ISNULL(Vac.CDate,'') AS CDate " & _
                    "	,ISNULL(Lang1.name,'') AS LANGUAGE1 " & _
                    "	,ISNULL(Lang2.name,'') AS LANGUAGE2 " & _
                    "	,ISNULL(Lang3.name,'') AS LANGUAGE3 " & _
                    "	,ISNULL(Lang4.name,'') AS LANGUAGE4 " & _
                    "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS NATIONALITY " & _
                    "	,CASE WHEN ISNULL(present_address1,'') = '' AND ISNULL(present_address2,'') = '' THEN '' " & _
                    "		  WHEN ISNULL(present_address1,'') = '' AND ISNULL(present_address2,'') <> '' THEN ISNULL(present_address2,'') " & _
                    "		  WHEN ISNULL(present_address1,'') <> '' AND ISNULL(present_address2,'') = '' THEN ISNULL(present_address1,'') " & _
                    "		  WHEN ISNULL(present_address1,'') <> '' AND ISNULL(present_address2,'') <> '' THEN ISNULL(present_address1,'')+'\r\n'+ISNULL(present_address2,'') " & _
                    "	 END AS PRESENT_ADDRESS " & _
                    "	,ISNULL(PRECITY.name,'') AS PRESENT_CITY " & _
                    "	,ISNULL(present_plotno,'') AS PRESENT_PLOTNO " & _
                    "	,ISNULL(present_estate,'') AS PRESENT_ESTATE " & _
                    "	,ISNULL(present_province,'') AS PRESENT_STREET " & _
                    "	,ISNULL(PRECOUNTRY.country_name,'') AS PRESENT_COUNTRY " & _
                    "	,ISNULL(PRESTATE.name,'') AS PRESENT_STATE " & _
                    "	,ISNULL(PREZIP.zipcode_no,'') AS PRESENT_POSTCODE " & _
                    "	,ISNULL(present_road,'') AS PRESENT_REGION " & _
                    "	,ISNULL(present_mobileno,'') AS PRESENT_MOBILENO " & _
                    "	,ISNULL(present_alternateno,'') AS PRESENT_ALTNO " & _
                    "	,ISNULL(present_tel_no,'') AS PRESENT_TELNO " & _
                    "	,ISNULL(present_fax,'') AS PRESENT_FAX " & _
                    "	,CASE WHEN ISNULL(perm_address1,'') = '' AND ISNULL(perm_address2,'') = '' THEN '' " & _
                    "		  WHEN ISNULL(perm_address1,'') = '' AND ISNULL(perm_address2,'') <> '' THEN ISNULL(perm_address2,'') " & _
                    "		  WHEN ISNULL(perm_address1,'') <> '' AND ISNULL(perm_address2,'') = '' THEN ISNULL(perm_address1,'') " & _
                    "		  WHEN ISNULL(perm_address1,'') <> '' AND ISNULL(perm_address2,'') <> '' THEN ISNULL(perm_address1,'')+'\r\n'+ISNULL(perm_address2,'') " & _
                    "	 END AS PERMANENT_ADDRESS " & _
                    "	,ISNULL(PERCITY.name,'') AS PERMANENT_CITY " & _
                    "	,ISNULL(PERZIP.zipcode_no,'') AS PERMANENT_POSTCODE " & _
                    "	,ISNULL(perm_plotno,'') AS PERMANENT_PLOTNO " & _
                    "	,ISNULL(perm_estate,'') AS PERMANENT_ESTATE " & _
                    "	,ISNULL(perm_province,'') AS PERMANENT_STREET " & _
                    "	,ISNULL(PERCOUNTRY.country_name,'') AS PERMANENT_COUNTRY " & _
                    "	,ISNULL(PERSTATE.name,'') AS PERMANENT_STATE " & _
                    "	,ISNULL(perm_road,'') AS PERMANENT_REGION " & _
                    "	,ISNULL(perm_mobileno,'') AS PERMANENT_MOBILENO " & _
                    "	,ISNULL(perm_alternateno,'') AS PERMANENT_ALTNO " & _
                    "	,ISNULL(perm_tel_no,'') AS PERMANENT_TELNO " & _
                    "	,ISNULL(perm_fax,'') AS PERMANENT_FAX " & _
                    "   ,rcapplicant_master.referenceno " & _
                    "   ,rcapplicant_master.UserId " & _
                    "   ,ISNULL(rcapplicant_master.applicantunkid,0) AS APPLICANTUNKID " & _
                    " FROM rcapplicant_master " & _
                    "	LEFT JOIN hrmsConfiguration..cfzipcode_master AS PERZIP ON PERZIP.zipcodeunkid = rcapplicant_master.present_zipcode " & _
                    "	LEFT JOIN hrmsConfiguration..cfcity_master AS PERCITY ON PERCITY.cityunkid = rcapplicant_master.present_post_townunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfstate_master AS PERSTATE ON PERSTATE.stateunkid = rcapplicant_master.present_stateunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfcountry_master AS PERCOUNTRY ON PERCOUNTRY.countryunkid = rcapplicant_master.present_countryunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfzipcode_master AS PREZIP ON PREZIP.zipcodeunkid = rcapplicant_master.present_zipcode " & _
                    "	LEFT JOIN hrmsConfiguration..cfcity_master AS PRECITY ON PRECITY.cityunkid = rcapplicant_master.present_post_townunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfstate_master AS PRESTATE ON PRESTATE.stateunkid = rcapplicant_master.present_stateunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfcountry_master AS PRECOUNTRY ON PRECOUNTRY.countryunkid = rcapplicant_master.present_countryunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.nationality " & _
                    "	LEFT JOIN cfcommon_master AS Lang1 ON Lang1.masterunkid = rcapplicant_master.language1unkid AND Lang1.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                    "	LEFT JOIN cfcommon_master AS Lang2 ON Lang2.masterunkid = rcapplicant_master.language2unkid AND Lang2.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                    "	LEFT JOIN cfcommon_master AS Lang3 ON Lang3.masterunkid = rcapplicant_master.language3unkid AND Lang3.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                    "	LEFT JOIN cfcommon_master AS Lang4 ON Lang4.masterunkid = rcapplicant_master.language4unkid AND Lang4.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 CVac.applicantunkid " & _
                    "			,CVac.vacancyunkid " & _
                    "			,cfcommon_master.name AS vacancy " & _
                    "           ,rcvacancy_master.isexternalvacancy " & _
                    "           ,rcvacancy_master.isbothintext " & _
                    "			,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                    "			,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                    "		FROM " & _
                    "		( " & _
                    "			SELECT " & _
                    "				 applicantunkid " & _
                    "				,vacancyunkid " & _
                    "				,ROW_NUMBER() OVER (PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC) AS Srno " & _
                    "			FROM rcapp_vacancy_mapping " & _
                    "			WHERE ISNULL(isvoid, 0) = 0 AND rcapp_vacancy_mapping.vacancyunkid = @VacancyId " & _
                    "		) AS CVac " & _
                    "		JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                    "		JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                    "		WHERE CVac.Srno = 1  AND rcvacancy_master.vacancyunkid = @VacancyId " & _
                    "	) AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid AND Vac.vacancyunkid = @VacancyId  " & strVacTypeFilter & " " & _
                    "	LEFT JOIN cfcommon_master AS MS ON MS.masterunkid = rcapplicant_master.marital_statusunkid AND MS.mastertype = " & CInt(clsCommon_Master.enCommonMaster.MARRIED_STATUS) & " " & _
                    "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicant_master.titleunkid AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.TITLE)
            'Sohail (25 Jun 2020) - [isbothintext]
            'Sohail (18 Apr 2020) - [AND Vac.isexternalvacancy = @isexternalvacancy] = [strVacTypeFilter]
            'Sohail (09 Oct 2018) - [isactive = 1]=[ISNULL(isvoid, 0) = 0]
            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            '"WHERE rcapplicant_master.applicantunkid = '" & mintApplicantUnkid & "'"
            'Added this :  "   ,rcapplicant_master.applicantunkid AS APPLICANTUNKID " & _  
            If mintReportTypeId <> 0 Then
                StrQ &= " LEFT JOIN rcshortlist_master ON rcshortlist_master.vacancyunkid = Vac.vacancyunkid " & _
                        " LEFT JOIN rcshortlist_finalapplicant ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid AND rcapplicant_master.applicantunkid = rcshortlist_finalapplicant.applicantunkid"
            End If


            'Pinkal (05-Apr-2018) -- Start
            'Bug - (RefNo: 0002156)  Applicant CV report not applying the vacancy filter.
            ' StrQ &= " WHERE 1 = 1 "
            StrQ &= " WHERE 1 = 1  AND Vac.vacancyunkid = @VacancyId "
            'Pinkal (05-Apr-2018) -- End


            If mintReportTypeId <> 0 Then
                StrQ &= " AND rcshortlist_master.isvoid = 0 AND rcshortlist_finalapplicant.isvoid = 0 "

                If mintReportTypeId = 1 Then
                    StrQ &= " AND rcshortlist_finalapplicant.isshortlisted = 1 AND rcshortlist_finalapplicant.isfinalshortlisted = 0"
                ElseIf mintReportTypeId = 2 Then
                    StrQ &= " AND rcshortlist_finalapplicant.isshortlisted = 1 AND rcshortlist_finalapplicant.isfinalshortlisted = 1"
                    If mintApprovalStatusId = enShortListing_Status.SC_APPROVED Then
                        StrQ &= " AND  app_statusid = " & enShortListing_Status.SC_APPROVED
                    ElseIf mintApprovalStatusId = enShortListing_Status.SC_PENDING Then
                        StrQ &= " AND  app_statusid = " & enShortListing_Status.SC_PENDING
                    ElseIf mintApprovalStatusId = enShortListing_Status.SC_REJECT Then
                        StrQ &= " AND  app_statusid = " & enShortListing_Status.SC_REJECT
                    End If
                End If
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
            objDataOperation.AddParameter("@VacancyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)
            If mintVacancyTypeId = enVacancyType.EXTERNAL_VACANCY Then
                objDataOperation.AddParameter("@isexternalvacancy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_VACANCY Then
                objDataOperation.AddParameter("@isexternalvacancy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            End If

            If mintApplicantUnkid > 0 Then
                StrQ &= " AND rcapplicant_master.applicantunkid = @ApplicantUnkid  "
                objDataOperation.AddParameter("@ApplicantUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantUnkid)
            End If

            StrQ &= " ) AS A" & _
                        " SELECT * FROM ##Temp "

            'Varsha (03 Jan 2018) -- End


            'Sohail (05 Dec 2016) - [UserId]
            'Sohail (02 Jun 2015) - [referenceno]

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_MainDetails = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_MainDetails.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("TITLE")
                rpt_Rows.Item("Column2") = dtRow.Item("SURNAME")
                rpt_Rows.Item("Column3") = dtRow.Item("FIRSTNAME")
                rpt_Rows.Item("Column4") = dtRow.Item("OTHERNAME")
                rpt_Rows.Item("Column5") = dtRow.Item("GENDER")
                rpt_Rows.Item("Column6") = dtRow.Item("EMP_CODE")
                rpt_Rows.Item("Column7") = dtRow.Item("EMAIL")
                If dtRow.Item("BIRTHDATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column8") = ""
                Else
                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("BIRTHDATE").ToString).ToShortDateString
                End If
                rpt_Rows.Item("Column9") = dtRow.Item("MARITAL_STATUS")
                If dtRow.Item("MARRIED_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column10") = ""
                Else
                    rpt_Rows.Item("Column10") = eZeeDate.convertDate(dtRow.Item("MARRIED_DATE").ToString).ToShortDateString
                End If

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                If dtRow.Item("ODate").ToString().Trim().Length <= 0 AndAlso dtRow.Item("CDate").ToString().Trim().Length <= 0 Then
                    rpt_Rows.Item("Column11") = dtRow.Item("VACANCY").ToString.Trim()
                Else
                    rpt_Rows.Item("Column11") = dtRow.Item("VACANCY").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & _
                                                                                      " - " & eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                End If
                'Varsha (03 Jan 2018) -- End

                rpt_Rows.Item("Column12") = dtRow.Item("LANGUAGE1")
                rpt_Rows.Item("Column13") = dtRow.Item("LANGUAGE2")
                rpt_Rows.Item("Column14") = dtRow.Item("LANGUAGE3")
                rpt_Rows.Item("Column15") = dtRow.Item("LANGUAGE4")
                rpt_Rows.Item("Column16") = dtRow.Item("NATIONALITY")
                rpt_Rows.Item("Column17") = dtRow.Item("PRESENT_ADDRESS").ToString.Replace("\r\n", vbCrLf)
                rpt_Rows.Item("Column18") = dtRow.Item("PRESENT_CITY")
                rpt_Rows.Item("Column19") = dtRow.Item("PRESENT_PLOTNO")
                rpt_Rows.Item("Column20") = dtRow.Item("PRESENT_ESTATE")
                rpt_Rows.Item("Column21") = dtRow.Item("PRESENT_STREET")
                rpt_Rows.Item("Column22") = dtRow.Item("PRESENT_COUNTRY")
                rpt_Rows.Item("Column23") = dtRow.Item("PRESENT_STATE")
                rpt_Rows.Item("Column24") = dtRow.Item("PRESENT_POSTCODE")
                rpt_Rows.Item("Column25") = dtRow.Item("PRESENT_REGION")
                rpt_Rows.Item("Column26") = dtRow.Item("PRESENT_MOBILENO")
                rpt_Rows.Item("Column27") = dtRow.Item("PRESENT_ALTNO")
                rpt_Rows.Item("Column28") = dtRow.Item("PRESENT_TELNO")
                rpt_Rows.Item("Column29") = dtRow.Item("PRESENT_FAX")
                rpt_Rows.Item("Column30") = dtRow.Item("PERMANENT_ADDRESS").ToString.Replace("\r\n", vbCrLf)
                rpt_Rows.Item("Column31") = dtRow.Item("PERMANENT_CITY")
                rpt_Rows.Item("Column32") = dtRow.Item("PERMANENT_POSTCODE")
                rpt_Rows.Item("Column33") = dtRow.Item("PERMANENT_PLOTNO")
                rpt_Rows.Item("Column34") = dtRow.Item("PERMANENT_ESTATE")
                rpt_Rows.Item("Column35") = dtRow.Item("PERMANENT_STREET")
                rpt_Rows.Item("Column36") = dtRow.Item("PERMANENT_COUNTRY")
                rpt_Rows.Item("Column37") = dtRow.Item("PERMANENT_STATE")
                rpt_Rows.Item("Column38") = dtRow.Item("PERMANENT_REGION")
                rpt_Rows.Item("Column39") = dtRow.Item("PERMANENT_MOBILENO")
                rpt_Rows.Item("Column40") = dtRow.Item("PERMANENT_ALTNO")
                rpt_Rows.Item("Column41") = dtRow.Item("PERMANENT_TELNO")
                rpt_Rows.Item("Column42") = dtRow.Item("PERMANENT_FAX")

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column43") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End

                rpt_MainDetails.Tables("ArutiTable").Rows.Add(rpt_Rows)

                'Sohail (02 Jun 2015) -- Start
                'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'strAppRefNo = clsCrypto.Dicrypt(dtRow.Item("referenceno").ToString)
                If dtRow.Item("referenceno").ToString <> "" Then
                    strAppRefNo = clsCrypto.Dicrypt(dtRow.Item("referenceno").ToString)
                Else
                    strAppRefNo = dtRow.Item("Email").ToString
                End If
                'Sohail (05 Dec 2016) -- End
                'Sohail (02 Jun 2015) -- End

            Next
            '*************************> MAIN DETAILS <************************* [END]

            '*************************> PROFESSIONAL SKILLS <************************* [START]
            StrQ = "SELECT " & _
                   "	 ISNULL(cfcommon_master.name,'') AS SKILL_CATEGORY " & _
                   "	,ISNULL(hrskill_master.skillname,'') AS SKILL " & _
                   "	,ISNULL(remark,'') AS REMARK " & _
                   "    ,ISNULL(rcapplicantskill_tran.applicantunkid,0) AS APPLICANTUNKID " & _
                   "FROM rcapplicantskill_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
                   "	JOIN hrskill_master ON rcapplicantskill_tran.skillunkid = hrskill_master.skillunkid " & _
                   "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantskill_tran.skillcategoryunkid AND mastertype = " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & " " & _
                   "    JOIN ##Temp ON ##TEMP.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
                   " WHERE rcapplicantskill_tran.skillcategoryunkid > 0 " & _
                        "AND rcapplicant_master.isvoid = 0 " & _
                        "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
            'Sohail (20 Oct 2020) - [JOIN rcapplicant_master]

            'Pinkal (21-APR-2018) -- Start
            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
            StrQ &= " AND rcapplicantskill_tran.isvoid = 0 "
            'Pinkal (21-APR-2018) -- End


            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Added this :  "   ,rapplicantunkid AS APPLICANTUNKID " & _ 
            'applicantunkid = '" & mintApplicantUnkid & "' AND 
            'Varsha (03 Jan 2018) -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            StrQ &= " UNION ALL "

            StrQ &= "SELECT " & _
                   "	 ISNULL(cfcommon_master.name,'') AS SKILL_CATEGORY " & _
                   "	,ISNULL(hrskill_master.skillname,'') AS SKILL " & _
                   "	,ISNULL(hremp_app_skills_tran.description,'') AS REMARK " & _
                   "    ,ISNULL(rcapplicant_master.applicantunkid,0) AS APPLICANTUNKID " & _
                   "FROM hremp_app_skills_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
                   "	JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
                   "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid AND mastertype = " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & " " & _
                   "    JOIN ##Temp ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                   " WHERE hremp_app_skills_tran.skillcategoryunkid > 0 " & _
                        "AND rcapplicant_master.isvoid = 0 " & _
                        " AND hremp_app_skills_tran.isvoid = 0 " & _
                        "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
            'Sohail (20 Oct 2020) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_ProfSkill = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_ProfSkill.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("SKILL_CATEGORY")
                rpt_Rows.Item("Column2") = dtRow.Item("SKILL")
                rpt_Rows.Item("Column3") = dtRow.Item("REMARK")
                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column4") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End


                rpt_ProfSkill.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '*************************> PROFESSIONAL SKILLS <************************* [END]

            '*************************> OTHER SKILLS <************************* [START]
            StrQ = "SELECT " & _
                   "	 ISNULL(other_skillcategory,'') AS SKILL_CATEGORY " & _
                   "	,ISNULL(other_skill,'') AS SKILL " & _
                   "	,ISNULL(remark,'') AS REMARK " & _
                   "    ,ISNULL(rcapplicantskill_tran.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM rcapplicantskill_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
                   "    JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicantskill_tran.applicantunkid " & _
                   " WHERE rcapplicantskill_tran.skillcategoryunkid <= 0 " & _
                        "AND rcapplicant_master.isvoid = 0 " & _
                        "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
            'Sohail (20 Oct 2020) - [JOIN rcapplicant_master]

            'Pinkal (21-APR-2018) -- Start
            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
            StrQ &= " AND rcapplicantskill_tran.isvoid = 0 "
            'Pinkal (21-APR-2018) -- End

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Added this :  "   ,rapplicantunkid AS APPLICANTUNKID " & _ 
            'applicantunkid = '" & mintApplicantUnkid & "' AND
            'Varsha (03 Jan 2018) -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            StrQ &= " UNION ALL "

            StrQ &= "SELECT " & _
                   "	 ISNULL(other_skillcategory,'') AS SKILL_CATEGORY " & _
                   "	,ISNULL(other_skill,'') AS SKILL " & _
                   "	,ISNULL(hremp_app_skills_tran.description,'') AS REMARK " & _
                   "    ,ISNULL(rcapplicant_master.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM hremp_app_skills_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid " & _
                   "    JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                   " WHERE hremp_app_skills_tran.skillcategoryunkid <= 0 " & _
                        " AND hremp_app_skills_tran.isvoid = 0 " & _
                        "AND rcapplicant_master.isvoid = 0 " & _
                        "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
            'Sohail (20 Oct 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_OtherSkill = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_OtherSkill.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("SKILL_CATEGORY")
                rpt_Rows.Item("Column2") = dtRow.Item("SKILL")
                rpt_Rows.Item("Column3") = dtRow.Item("REMARK")

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column4") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End

                rpt_OtherSkill.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '*************************> OTHER SKILLS <************************* [END]

            '*************************> PROFESSIONAL QUALIFICATION <************************* [START]
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            'StrQ = "SELECT " & _
            '       "	 ISNULL(cfcommon_master.name,'') AS QUALIFICATION_GROUP " & _
            '       "	,ISNULL(hrqualification_master.qualificationname,'') AS QUALIFICATION " & _
            '       "	,ISNULL(hrresult_master.resultname,'') AS RESULT " & _
            '       "	,CASE WHEN gpacode <= 0 THEN '' ELSE ISNULL(CAST(gpacode AS NVARCHAR(50)),'') END AS GPA " & _
            '       "	,ISNULL(reference_no,'') AS REFERENCE_NO " & _
            '       "	,ISNULL(hrinstitute_master.institute_name,'') AS INSTITUTE " & _
            '       "	,ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS START_DATE " & _
            '       "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS END_DATE " & _
            '       "    ,ISNULL(rcapplicantqualification_tran.applicantunkid,0) AS APPLICANTUNKID " & _
            '       " FROM rcapplicantqualification_tran " & _
            '       "	LEFT JOIN hrinstitute_master ON rcapplicantqualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
            '       "	LEFT JOIN hrresult_master ON rcapplicantqualification_tran.resultunkid = hrresult_master.resultunkid " & _
            '       "	JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
            '       "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
            '       "    JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
            '       " WHERE rcapplicantqualification_tran.qualificationgroupunkid > 0 "
            StrQ = "SELECT " & _
                   "	 CASE WHEN rcapplicantqualification_tran.qualificationgroupunkid > 0 THEN ISNULL(cfcommon_master.name, '') ELSE rcapplicantqualification_tran.other_qualificationgrp END AS QUALIFICATION_GROUP " & _
                   "	,CASE WHEN rcapplicantqualification_tran.qualificationunkid > 0 THEN ISNULL(hrqualification_master.qualificationname, '') ELSE rcapplicantqualification_tran.other_qualification END AS QUALIFICATION " & _
                   "	,CASE WHEN rcapplicantqualification_tran.resultunkid > 0 THEN ISNULL(hrresult_master.resultname, '') ELSE rcapplicantqualification_tran.other_resultcode END AS RESULT " & _
                   "	,CASE WHEN gpacode <= 0 THEN '' ELSE ISNULL(CAST(gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                   "	,ISNULL(reference_no,'') AS REFERENCE_NO " & _
                   "	,CASE WHEN rcapplicantqualification_tran.instituteunkid > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE rcapplicantqualification_tran.other_institute END AS INSTITUTE " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS START_DATE " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS END_DATE " & _
                   "    ,ISNULL(rcapplicantqualification_tran.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM rcapplicantqualification_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
                   "	LEFT JOIN hrinstitute_master ON rcapplicantqualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                   "	LEFT JOIN hrresult_master ON rcapplicantqualification_tran.resultunkid = hrresult_master.resultunkid " & _
                   "	JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                   "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                   "    JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
                   " WHERE rcapplicantqualification_tran.isvoid = 0 " & _
                   " AND rcapplicant_master.isvoid = 0 " & _
                   " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
            'Sohail (20 Oct 2020) -- End

            'Pinkal (21-APR-2018) -- Start
            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            'StrQ &= " AND rcapplicantqualification_tran.isvoid = 0 ORDER BY award_end_date DESC "
            'Sohail (20 Oct 2020) -- End
            'Pinkal (21-APR-2018) -- End


            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Added this :  "   ,rapplicantunkid AS APPLICANTUNKID " & _ 
            'applicantunkid = '" & mintApplicantUnkid & "' AND 
            'Varsha (03 Jan 2018) -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            StrQ &= " UNION ALL "

            StrQ &= "SELECT " & _
                   "	 CASE WHEN hremp_qualification_tran.qualificationgroupunkid > 0 THEN ISNULL(cfcommon_master.name, '') ELSE hremp_qualification_tran.other_qualificationgrp END AS QUALIFICATION_GROUP " & _
                   "	,CASE WHEN hremp_qualification_tran.qualificationunkid > 0 THEN ISNULL(hrqualification_master.qualificationname, '') ELSE hremp_qualification_tran.other_qualification END AS QUALIFICATION " & _
                   "	,CASE WHEN hremp_qualification_tran.resultunkid > 0 THEN ISNULL(hrresult_master.resultname, '') ELSE hremp_qualification_tran.other_resultcode END AS RESULT " & _
                   "	,CASE WHEN gpacode <= 0 THEN '' ELSE ISNULL(CAST(gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                   "	,ISNULL(reference_no,'') AS REFERENCE_NO " & _
                   "	,CASE WHEN hremp_qualification_tran.instituteunkid > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE hremp_qualification_tran.other_institute END AS INSTITUTE " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS START_DATE " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS END_DATE " & _
                   "    ,ISNULL(rcapplicant_master.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM hremp_qualification_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
                   "	LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                   "	LEFT JOIN hrresult_master ON hremp_qualification_tran.resultunkid = hrresult_master.resultunkid " & _
                   "	JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                   "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                   "    JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                   " WHERE hremp_qualification_tran.isvoid = 0 " & _
                   " AND rcapplicant_master.isvoid = 0 " & _
                   " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "

            StrQ &= " ORDER BY ISNULL(CONVERT(CHAR(8),award_start_date,112), '') DESC "
            'Sohail (20 Oct 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_ProfQualif = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_ProfQualif.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("QUALIFICATION_GROUP")
                rpt_Rows.Item("Column2") = dtRow.Item("QUALIFICATION")
                rpt_Rows.Item("Column3") = dtRow.Item("RESULT")
                rpt_Rows.Item("Column4") = dtRow.Item("GPA")
                rpt_Rows.Item("Column5") = dtRow.Item("REFERENCE_NO")
                rpt_Rows.Item("Column6") = dtRow.Item("INSTITUTE")

                If dtRow.Item("START_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column7") = ""
                Else
                    rpt_Rows.Item("Column7") = eZeeDate.convertDate(dtRow.Item("START_DATE").ToString).ToShortDateString
                End If

                If dtRow.Item("END_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column8") = ""
                Else
                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("END_DATE").ToString).ToShortDateString
                End If

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column9") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End

                rpt_ProfQualif.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '*************************> PROFESSIONAL QUALIFICATION <************************* [END]

            '*************************> OTHER QUALIFICATION/SHORT COURSES <************************* [START]
            StrQ = "SELECT " & _
                   "	 ISNULL(other_qualificationgrp,'') AS QUALIFICATION_GROUP " & _
                   "	,ISNULL(other_qualification,'') AS QUALIFICATION " & _
                   "	,ISNULL(other_resultcode,'') AS RESULT " & _
                   "	,CASE WHEN gpacode <= 0 THEN '' ELSE ISNULL(CAST(gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                   "	,ISNULL(reference_no,'') AS REFERENCE_NO " & _
                   "	,ISNULL(other_institute,'') AS INSTITUTE " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_start_date,112),'') AS START_DATE " & _
                   "	,ISNULL(CONVERT(CHAR(8),award_end_date,112),'') AS END_DATE " & _
                   "    ,ISNULL(rcapplicantqualification_tran.applicantunkid,0) AS APPLICANTUNKID " & _
                   "  FROM rcapplicantqualification_tran " & _
                   "  JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
                   "  WHERE 1 = 2 /*rcapplicantqualification_tran.qualificationgroupunkid <= 0*/ "
            'Sohail (20 Oct 2020) - [WHERE 1 = 2]

            'Pinkal (21-APR-2018) -- Start
            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
            StrQ &= " /*AND rcapplicantqualification_tran.isvoid = 0 ORDER BY award_end_date DESC*/ " 'Sohail (20 Oct 2020) - [/* */]
            'Pinkal (21-APR-2018) -- End

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Added this :  "   ,rapplicantunkid AS APPLICANTUNKID " & _ 
            'applicantunkid = '" & mintApplicantUnkid & "' AND
            'Varsha (03 Jan 2018) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_OtherQualif = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_OtherQualif.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("QUALIFICATION_GROUP")
                rpt_Rows.Item("Column2") = dtRow.Item("QUALIFICATION")
                rpt_Rows.Item("Column3") = dtRow.Item("RESULT")
                rpt_Rows.Item("Column4") = dtRow.Item("GPA")
                rpt_Rows.Item("Column5") = dtRow.Item("REFERENCE_NO")
                rpt_Rows.Item("Column6") = dtRow.Item("INSTITUTE")

                If dtRow.Item("START_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column7") = ""
                Else
                    rpt_Rows.Item("Column7") = eZeeDate.convertDate(dtRow.Item("START_DATE").ToString).ToShortDateString
                End If

                If dtRow.Item("END_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column8") = ""
                Else
                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("END_DATE").ToString).ToShortDateString
                End If

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column9") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End

                rpt_OtherQualif.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '*************************> OTHER QUALIFICATION/SHORT COURSES <************************* [END]

            '*************************> JOB HISTORY <************************* [START]
            StrQ = "SELECT " & _
                    "	 ISNULL(employername,'') AS EMPLOYER_NAME " & _
                    "	,ISNULL(companyname,'') AS COMPANY_NAME " & _
                    "	,ISNULL(officephone,'') AS OFFICE_PHONE " & _
                    "	,ISNULL(CONVERT(CHAR(8),joiningdate,112),'') AS JOIN_DATE " & _
                    "	,ISNULL(CONVERT(CHAR(8),terminationdate,112),'') AS TERM_DATE " & _
                    "	,ISNULL(designation,'') AS DESIGNATION " & _
                    "	,ISNULL(responsibility,'') AS RESPOSIBILITY " & _
                    "	,ISNULL(leavingreason,'') AS LEAVING_REASON " & _
                    "   ,ISNULL(rcjobhistory.applicantunkid,0) AS APPLICANTUNKID " & _
                    " FROM rcjobhistory  " & _
                     " JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcjobhistory.applicantunkid " & _
                     " JOIN ##TEMP ON ##TEMP.applicantunkid = rcjobhistory.applicantunkid "

            'Pinkal (21-APR-2018) -- Start
            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            'StrQ &= " WHERE rcjobhistory.isvoid = 0  ORDER BY joiningdate DESC "
            StrQ &= " WHERE rcjobhistory.isvoid = 0 " & _
                    "AND rcapplicant_master.isvoid = 0 " & _
                    "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
            'Sohail (20 Oct 2020) -- End
            'Pinkal (21-APR-2018) -- End


            ' "

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Added this :  "   ,rapplicantunkid AS APPLICANTUNKID " & _ 
            'WHERE applicantunkid = '" & mintApplicantUnkid & "' " & _
            'Varsha (03 Jan 2018) -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            StrQ &= " UNION ALL "

            StrQ &= "SELECT " & _
                   "	 ISNULL(contact_person,'') AS EMPLOYER_NAME " & _
                   "	,ISNULL(company,'') AS COMPANY_NAME " & _
                   "	,ISNULL(contact_no,'') AS OFFICE_PHONE " & _
                   "	,ISNULL(CONVERT(CHAR(8),hremp_experience_tran.start_date,112),'') AS JOIN_DATE " & _
                   "	,ISNULL(CONVERT(CHAR(8),hremp_experience_tran.end_date,112),'') AS TERM_DATE " & _
                   "	,ISNULL(old_job,'') AS DESIGNATION " & _
                   "	,ISNULL(remark,'') AS RESPOSIBILITY " & _
                   "	,ISNULL(leave_reason,'') AS LEAVING_REASON " & _
                   "   ,ISNULL(rcapplicant_master.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM hremp_experience_tran  " & _
                    " JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_experience_tran.employeeunkid " & _
                    " JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                    " WHERE hremp_experience_tran.isvoid = 0 " & _
                    "AND rcapplicant_master.isvoid = 0 " & _
                    "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "

            StrQ &= " ORDER BY ISNULL(CONVERT(CHAR(8), joiningdate, 112), '') DESC "
            'Sohail (20 Oct 2020) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_JobHistory = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_JobHistory.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("EMPLOYER_NAME")
                rpt_Rows.Item("Column2") = dtRow.Item("COMPANY_NAME")
                rpt_Rows.Item("Column3") = dtRow.Item("OFFICE_PHONE")
                If dtRow.Item("JOIN_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column4") = ""
                Else
                    rpt_Rows.Item("Column4") = eZeeDate.convertDate(dtRow.Item("JOIN_DATE").ToString).ToShortDateString
                End If
                If dtRow.Item("TERM_DATE").ToString.Trim.Length <= 0 Then
                    rpt_Rows.Item("Column5") = ""
                Else
                    rpt_Rows.Item("Column5") = eZeeDate.convertDate(dtRow.Item("TERM_DATE").ToString).ToShortDateString
                End If
                rpt_Rows.Item("Column6") = dtRow.Item("DESIGNATION")
                rpt_Rows.Item("Column7") = dtRow.Item("RESPOSIBILITY")
                rpt_Rows.Item("Column8") = dtRow.Item("LEAVING_REASON")

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column9") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End

                rpt_JobHistory.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '*************************> JOB HISTORY <************************* [END]

            '*************************> REFERENCES <************************* [START]
            StrQ = "SELECT " & _
                   "	 ISNULL(rcapp_reference_tran.name,'') AS REF_NAME " & _
                   "	,ISNULL(cfcommon_master.name,'') AS REF_TYPE " & _
                   "	,ISNULL(position,'') AS POSITION " & _
                   "	,CASE WHEN rcapp_reference_tran.gender = 1 THEN @Male WHEN rcapp_reference_tran.gender = 2 THEN @Female ELSE '' END AS GENDER " & _
                   "	,ISNULL(rcapp_reference_tran.address,'') AS ADDRESS " & _
                   "	,ISNULL(rcapp_reference_tran.telephone_no,'') AS TEL_NO " & _
                   "	,ISNULL(rcapp_reference_tran.mobile_no,'') AS MOBILE " & _
                   "	,ISNULL(rcapp_reference_tran.email,'') AS EMAIL " & _
                   "   ,ISNULL(rcapp_reference_tran.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM rcapp_reference_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapp_reference_tran.applicantunkid " & _
                   "	LEFT JOIN cfcommon_master ON rcapp_reference_tran.relationunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & _
                   "   JOIN ##TEMP ON ##TEMP.applicantunkid = rcapp_reference_tran.applicantunkid "

            'Pinkal (21-APR-2018) -- Start
            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
            StrQ &= " WHERE rcapp_reference_tran.isvoid = 0 "
            'Pinkal (21-APR-2018) -- End

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Added this :  "   ,rapplicantunkid AS APPLICANTUNKID " & _ 
            '"WHERE applicantunkid = '" & mintApplicantUnkid & "'"
            'Varsha (03 Jan 2018) -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            StrQ &= " AND rcapplicant_master.isvoid = 0 " & _
                    " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "

            StrQ &= " UNION ALL "

            StrQ &= "SELECT " & _
                   "	 ISNULL(hremployee_referee_tran.name,'') AS REF_NAME " & _
                   "	,ISNULL(cfcommon_master.name,'') AS REF_TYPE " & _
                   "	,ISNULL(ref_position,'') AS POSITION " & _
                   "	,CASE WHEN hremployee_referee_tran.gender = 1 THEN @Male WHEN hremployee_referee_tran.gender = 2 THEN @Female ELSE '' END AS GENDER " & _
                   "	,ISNULL(hremployee_referee_tran.address,'') AS ADDRESS " & _
                   "	,ISNULL(hremployee_referee_tran.telephone_no,'') AS TEL_NO " & _
                   "	,ISNULL(hremployee_referee_tran.mobile_no,'') AS MOBILE " & _
                   "	,ISNULL(hremployee_referee_tran.email,'') AS EMAIL " & _
                   "   ,ISNULL(rcapplicant_master.applicantunkid,0) AS APPLICANTUNKID " & _
                   " FROM hremployee_referee_tran " & _
                   "    JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremployee_referee_tran.employeeunkid " & _
                   "	LEFT JOIN cfcommon_master ON hremployee_referee_tran.relationunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & _
                   "   JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                   " WHERE hremployee_referee_tran.isvoid = 0 " & _
                        " AND rcapplicant_master.isvoid = 0 " & _
                        " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
            'Sohail (20 Oct 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_References = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_References.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("REF_NAME")
                rpt_Rows.Item("Column2") = dtRow.Item("REF_TYPE")
                rpt_Rows.Item("Column3") = dtRow.Item("POSITION")
                rpt_Rows.Item("Column4") = dtRow.Item("GENDER")
                rpt_Rows.Item("Column5") = dtRow.Item("ADDRESS")
                rpt_Rows.Item("Column6") = dtRow.Item("TEL_NO")
                rpt_Rows.Item("Column7") = dtRow.Item("MOBILE")
                rpt_Rows.Item("Column8") = dtRow.Item("EMAIL")

                'Varsha (03 Jan 2018) -- Start
                'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                'This filter should allow user to report on all cv's shortlisted for that particular position. 
                'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                rpt_Rows.Item("Column9") = dtRow.Item("APPLICANTUNKID")
                'Varsha (03 Jan 2018) -- End

                rpt_References.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '*************************> REFERENCES <************************* [END]

            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            'StrQ = "SELECT * FROM rcattachfiletran WHERE applicantunkid = " & mintApplicantUnkid & "; DROP TABLE ##TEMP "
            StrQ = "SELECT rcattachfiletran.filename " & _
                            ", rcattachfiletran.filepath " & _
                            ", rcattachfiletran.fileuniquename " & _
                            ", rcapplicant_master.applicantunkid " & _
                    "FROM rcattachfiletran " & _
                        "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcattachfiletran.applicantunkid " & _
                        "JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                    "WHERE rcattachfiletran.isvoid = 0 " & _
                          "AND rcapplicant_master.isvoid = 0 " & _
                          "AND rcattachfiletran.attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " " & _
                          "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 " & _
                    "UNION ALL " & _
                    "SELECT hrdocuments_tran.filename " & _
                            ", hrdocuments_tran.filepath " & _
                            ", hrdocuments_tran.fileuniquename " & _
                            ", rcapplicant_master.applicantunkid " & _
                    "FROM hrdocuments_tran " & _
                        "JOIN rcapplicant_master ON hrdocuments_tran.employeeunkid = rcapplicant_master.employeeunkid " & _
                        "JOIN ##TEMP ON ##TEMP.applicantunkid = rcapplicant_master.applicantunkid " & _
                    "WHERE hrdocuments_tran.isactive = 1 " & _
                          "AND rcapplicant_master.isvoid = 0 " & _
                          "AND hrdocuments_tran.scanattachrefid = " & enScanAttactRefId.QUALIFICATIONS & " " & _
                          "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "

            StrQ &= "; DROP TABLE ##TEMP "
            'Sohail (20 Oct 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Attachments = New ArutiReport.Designer.dsArutiReport

            Dim aImageRow As DataRow = Nothing
            Dim idx As Integer = 0
            Dim fs As IO.FileStream
            Dim strPath As String = ""
            'S.SANDEEP |26-APR-2019| -- START
            'Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocType("Docs")
            Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocType(0, False, "Docs")
            'S.SANDEEP |26-APR-2019| -- END
            Dim strFolderName As String = (From p In dsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.QUALIFICATIONS) Select (p.Item("Name").ToString)).FirstOrDefault
            'If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\" 'Sohail (20 Oct 2020)
            'Dim objImg(0) As Object
            Dim objImg As Byte()
            marrPath = New ArrayList

            If mblnShowAttachments = True Then
                'Sohail (20 Oct 2020) -- Start
                'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
                'aImageRow = rpt_Attachments.Tables("ArutiImage").NewRow()

                'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                '    'strPath = dtRow.Item("filepath").ToString

                '    'Shani(24-Aug-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                '    'strPath = ConfigParameter._Object._Document_Path & "\" & strFolderName & strAppRefNo & "_" & dtRow.Item("filename").ToString
                '    If strDocumentPath.Trim = "" Then strDocumentPath = ConfigParameter._Object._Document_Path
                '    strPath = strDocumentPath & "\" & strFolderName & strAppRefNo & "_" & dtRow.Item("filename").ToString
                '    'Shani(24-Aug-2015) -- End

                '    If IO.File.Exists(strPath) = True Then
                '        objImg(0) = Nothing

                '        fs = New IO.FileStream(strPath, IO.FileMode.Open, IO.FileAccess.Read)
                '        objImg(0) = New Byte(fs.Length) {}
                '        fs.Read(objImg(0), 0, Convert.ToInt32(fs.Length))
                '        fs.Close()

                '        If idx = 0 Then
                '            aImageRow.Item("arutiLogo") = objImg(0)
                '            marrPath.Add(strPath)
                '        ElseIf idx = 1 Then
                '            aImageRow.Item("guestsign") = objImg(0)
                '            marrPath.Add(strPath)
                '        ElseIf idx = 2 Then
                '            aImageRow.Item("guestimage") = objImg(0)
                '            marrPath.Add(strPath)
                '        Else
                '            Exit For
                '        End If

                '        idx += 1
                '    End If
                'Next

                'If idx > 0 Then
                '    rpt_Attachments.Tables("ArutiImage").Rows.Add(aImageRow)
                'End If
                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()

                For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                    objImg = Nothing

                    Dim sExt As String = IO.Path.GetExtension(dtRow.Item("filepath").ToString).ToLower.Replace(".", "")

                    If sExt.StartsWith("jpg") OrElse sExt.StartsWith("jpeg") OrElse sExt.StartsWith("bmp") OrElse sExt.StartsWith("png") OrElse sExt.StartsWith("gif") Then

                        Dim blnExist As Boolean = False
                        If dtRow.Item("filepath").ToString.ToLower.StartsWith("http") = True Then
                            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
                            Try
                                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
                            Catch ex As Exception

                            End Try
                            Dim request As HttpWebRequest = CType(System.Net.WebRequest.Create(dtRow.Item("filepath").ToString), HttpWebRequest)
                            Using response As HttpWebResponse = CType(request.GetResponse, HttpWebResponse)
                                blnExist = response.StatusCode = HttpStatusCode.OK
                            End Using
                        Else
                            blnExist = IO.File.Exists(dtRow.Item("filepath").ToString)
                        End If

                        If blnExist = True Then

                            Dim strError As String = ""
                            If blnIsIISInstalled Then
                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(dtRow.Item("filepath").ToString, dtRow.Item("fileuniquename").ToString, strFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    Throw New Exception(strError & ":Generate_DetailReport;" & mstrModuleName)
                                    Exit Function
                                End If

                                If filebytes IsNot Nothing Then
                                    objImg = Nothing

                                    'Dim br As BinaryReader

                                    'Dim ms As New MemoryStream(filebytes)
                                    'fs = New FileStream(dtRow.Item("filename").ToString, FileMode.Create, FileAccess.Write)
                                    'ms.WriteTo(fs)
                                    ''ms.Close()
                                    ''br = New BinaryReader(fs)
                                    ''objImg = New Byte(fs.Length) {}
                                    ''objImg = br.ReadBytes(Convert.ToInt32(fs.Length))
                                    ''fs.Read(objImg, 0, Convert.ToInt32(fs.Length))

                                    ''br.Close()
                                    'fs.Close()
                                    'fs.Dispose()
                                    'ms.Close()

                                    'objImg = Nothing
                                    ''fs = New FileStream(dtRow.Item("filename").ToString, FileMode.Open, FileAccess.Read)
                                    'fs = New FileStream("C:\inetpub\wwwroot\ArutiSelfService\UploadImage\Qualifications\NPK_c5af3364-ba41-4d79-914c-ab4d8cf09586.png", FileMode.Open, FileAccess.Read)
                                    'br = New BinaryReader(fs)
                                    'objImg = New Byte(fs.Length) {}
                                    'objImg = br.ReadBytes(Convert.ToInt32(fs.Length))

                                    'br.Close()
                                    'fs.Close()
                                    'fs.Dispose()

                                    Dim rpt_Rows As DataRow = rpt_Attachments.Tables("ArutiTable").NewRow
                                    rpt_Rows.Item("Column9") = dtRow.Item("applicantunkid")
                                    rpt_Rows.Item("Sign1") = filebytes ' objImg

                                    rpt_Attachments.Tables("ArutiTable").Rows.Add(rpt_Rows)

                                End If
                            Else
                                objImg = Nothing
                                fs = New IO.FileStream(dtRow.Item("filepath").ToString, IO.FileMode.Open, IO.FileAccess.Read)
                                objImg = New Byte(fs.Length) {}
                                fs.Read(objImg, 0, Convert.ToInt32(fs.Length))
                                fs.Close()

                                Dim rpt_Rows As DataRow = rpt_Attachments.Tables("ArutiTable").NewRow
                                rpt_Rows.Item("Column9") = dtRow.Item("applicantunkid")
                                rpt_Rows.Item("Sign1") = objImg

                                rpt_Attachments.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If

                End If
            End If

                Next
                'Sohail (20 Oct 2020) -- End
            End If
            'Sohail (02 Jun 2015) -- End

            objRpt = New ArutiReport.Designer.rptApplicantCVReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_MainDetails.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_MainDetails.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_MainDetails.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_MainDetails.Tables("ArutiTable").Rows.Add("")
            End If

            If mblnShowProfessional_Skills = False Then
                ReportFunction.EnableSuppressSection(objRpt, "Section3", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "Section3", False)
            End If

            If mblnShowOther_Skills = False Then
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", False)
            End If

            If mblnShowProfessional_Qualification = False Then

                ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", False)

            End If

            If mblnShowOther_Qualification = False Then
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", False)
            End If

            If mblnShowJobHistory = False Then
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection4", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection4", False)
            End If

            If mblnShowReferences = False Then
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection5", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection5", False)
            End If

            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            If mblnShowAttachments = False Then
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection6", True)
            Else
                ReportFunction.EnableSuppressSection(objRpt, "DetailSection6", False)
            End If
            'Sohail (02 Jun 2015) -- End


            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
            objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column43}) <> {ArutiTable.Column43} then 0 Else 1 "
            'Varsha (03 Jan 2018) -- End


            objRpt.SetDataSource(rpt_MainDetails)
            objRpt.Subreports("rptProfessionalSkill").SetDataSource(rpt_ProfSkill)
            objRpt.Subreports("rptOtherSkills").SetDataSource(rpt_OtherSkill)
            objRpt.Subreports("rptProfessionalQualification").SetDataSource(rpt_ProfQualif)
            objRpt.Subreports("rptOtherQualification").SetDataSource(rpt_OtherQualif)
            objRpt.Subreports("rptJobHistory").SetDataSource(rpt_JobHistory)
            objRpt.Subreports("rptReferences").SetDataSource(rpt_References)
            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            objRpt.Subreports("rptAttachments").SetDataSource(rpt_Attachments)
            'Sohail (02 Jun 2015) -- End


            'Sohail (02 Jun 2015) -- Start
            'Enhancement - Provide preview option for applicant's attachmentson and for applicant CV report.
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            'For j = 2 To idx Step -1
            '    If j = 2 Then
            '        ReportFunction.EnableSuppressSection(objRpt.Subreports("rptAttachments"), "DSGuestImage", True)
            '    ElseIf j = 1 Then
            '        ReportFunction.EnableSuppressSection(objRpt.Subreports("rptAttachments"), "DSGuestSign", True)
            '    ElseIf j = 0 Then
            '        ReportFunction.EnableSuppressSection(objRpt.Subreports("rptAttachments"), "DSArutiLogo", True)
            '    End If
            'Next
            'Sohail (20 Oct 2020) -- End
            'Sohail (02 Jun 2015) -- End

            '************ CAPTION SETTINGS ************[STRAR]
            Call ReportFunction.TextChange(objRpt, "lblPart1", Language.getMessage(mstrModuleName, 1, "PERSONAL DETAILS"))
            Call ReportFunction.TextChange(objRpt, "txtTitle", Language.getMessage(mstrModuleName, 2, "Title :"))
            Call ReportFunction.TextChange(objRpt, "txtSurname", Language.getMessage(mstrModuleName, 3, "Surname :"))
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 4, "Firstname :"))
            Call ReportFunction.TextChange(objRpt, "txtOtherName", Language.getMessage(mstrModuleName, 5, "Othername :"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 6, "Gender :"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "Emp Code :"))
            Call ReportFunction.TextChange(objRpt, "txtEmail", Language.getMessage(mstrModuleName, 8, "Email :"))
            Call ReportFunction.TextChange(objRpt, "txtBirthDate", Language.getMessage(mstrModuleName, 9, "Birthdate :"))
            Call ReportFunction.TextChange(objRpt, "txtMaritalStatus", Language.getMessage(mstrModuleName, 10, "Marital Status :"))
            Call ReportFunction.TextChange(objRpt, "txtMarriedDate", Language.getMessage(mstrModuleName, 11, "Married Date :"))
            Call ReportFunction.TextChange(objRpt, "txtVacancy", Language.getMessage(mstrModuleName, 12, "Vacancy :"))
            Call ReportFunction.TextChange(objRpt, "txtLanguage1", Language.getMessage(mstrModuleName, 13, "Language1 :"))
            Call ReportFunction.TextChange(objRpt, "txtLanguage2", Language.getMessage(mstrModuleName, 14, "Language2 :"))
            Call ReportFunction.TextChange(objRpt, "txtLanguage3", Language.getMessage(mstrModuleName, 15, "Language3 :"))
            Call ReportFunction.TextChange(objRpt, "txtLanguage4", Language.getMessage(mstrModuleName, 16, "Language4 :"))
            Call ReportFunction.TextChange(objRpt, "txtNationality", Language.getMessage(mstrModuleName, 17, "Nationality :"))

            Call ReportFunction.TextChange(objRpt, "lblPart2", Language.getMessage(mstrModuleName, 18, "ADDRESS"))
            Call ReportFunction.TextChange(objRpt, "txtPresentAddress", Language.getMessage(mstrModuleName, 19, "Present Address"))
            Call ReportFunction.TextChange(objRpt, "txtPREAddress", Language.getMessage(mstrModuleName, 20, "Address :"))
            Call ReportFunction.TextChange(objRpt, "txtPREPlotNo", Language.getMessage(mstrModuleName, 21, "Plot No :"))
            Call ReportFunction.TextChange(objRpt, "txtPREEstate", Language.getMessage(mstrModuleName, 22, "Estate :"))
            Call ReportFunction.TextChange(objRpt, "txtPREStreet", Language.getMessage(mstrModuleName, 23, "Street :"))
            Call ReportFunction.TextChange(objRpt, "txtPRECountry", Language.getMessage(mstrModuleName, 24, "Country :"))
            Call ReportFunction.TextChange(objRpt, "txtPREState", Language.getMessage(mstrModuleName, 25, "State :"))
            Call ReportFunction.TextChange(objRpt, "txtPREPostTown", Language.getMessage(mstrModuleName, 26, "Post Town :"))
            Call ReportFunction.TextChange(objRpt, "txtPREPostCode", Language.getMessage(mstrModuleName, 27, "Post Code :"))
            Call ReportFunction.TextChange(objRpt, "txtPRERegion", Language.getMessage(mstrModuleName, 28, "Region :"))
            Call ReportFunction.TextChange(objRpt, "txtPREMobile", Language.getMessage(mstrModuleName, 29, "Mobile :"))
            Call ReportFunction.TextChange(objRpt, "txtPREAltNo", Language.getMessage(mstrModuleName, 30, "Alt. No. :"))
            Call ReportFunction.TextChange(objRpt, "txtPRETel", Language.getMessage(mstrModuleName, 31, "Tel. No. :"))
            Call ReportFunction.TextChange(objRpt, "txtPREFax", Language.getMessage(mstrModuleName, 32, "Fax :"))

            Call ReportFunction.TextChange(objRpt, "txtPermanentAddress", Language.getMessage(mstrModuleName, 33, "Permanent Address"))
            Call ReportFunction.TextChange(objRpt, "txtPERAddress", Language.getMessage(mstrModuleName, 20, "Address :"))
            Call ReportFunction.TextChange(objRpt, "txtPERPlotNo", Language.getMessage(mstrModuleName, 21, "Plot No :"))
            Call ReportFunction.TextChange(objRpt, "txtPEREstate", Language.getMessage(mstrModuleName, 22, "Estate :"))
            Call ReportFunction.TextChange(objRpt, "txtPERStreet", Language.getMessage(mstrModuleName, 23, "Street :"))
            Call ReportFunction.TextChange(objRpt, "txtPERCountry", Language.getMessage(mstrModuleName, 24, "Country :"))
            Call ReportFunction.TextChange(objRpt, "txtPERState", Language.getMessage(mstrModuleName, 25, "State :"))
            Call ReportFunction.TextChange(objRpt, "txtPERPostTown", Language.getMessage(mstrModuleName, 26, "Post Town :"))
            Call ReportFunction.TextChange(objRpt, "txtPERPostCode", Language.getMessage(mstrModuleName, 27, "Post Code :"))
            Call ReportFunction.TextChange(objRpt, "txtPERRegion", Language.getMessage(mstrModuleName, 28, "Region :"))
            Call ReportFunction.TextChange(objRpt, "txtPERMobile", Language.getMessage(mstrModuleName, 29, "Mobile :"))
            Call ReportFunction.TextChange(objRpt, "txtPERAltNo", Language.getMessage(mstrModuleName, 30, "Alt. No. :"))
            Call ReportFunction.TextChange(objRpt, "txtPERTel", Language.getMessage(mstrModuleName, 31, "Tel. No. :"))
            Call ReportFunction.TextChange(objRpt, "txtPERFax", Language.getMessage(mstrModuleName, 32, "Fax :"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalSkill"), "lblPSkill", Language.getMessage(mstrModuleName, 67, "PROFESSIONAL SKILLS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalSkill"), "txtSkillCategory", Language.getMessage(mstrModuleName, 34, "Skill Category"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalSkill"), "txtSkills", Language.getMessage(mstrModuleName, 35, "Skill"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalSkill"), "txtRemark", Language.getMessage(mstrModuleName, 36, "Remark"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherSkills"), "lblOSkill", Language.getMessage(mstrModuleName, 37, "OTHER SKILLS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherSkills"), "txtOtherSkillCategory", Language.getMessage(mstrModuleName, 34, "Skill Category"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherSkills"), "txtOtherSkills", Language.getMessage(mstrModuleName, 35, "Skill"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherSkills"), "txtRemark", Language.getMessage(mstrModuleName, 36, "Remark"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "lblPQualification", Language.getMessage(mstrModuleName, 68, "PROFESSIONAL QUALIFICATION"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtQualificationGrp", Language.getMessage(mstrModuleName, 38, "Qualification Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtQualification", Language.getMessage(mstrModuleName, 39, "Qualification"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtResult", Language.getMessage(mstrModuleName, 40, "Result"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtGPA", Language.getMessage(mstrModuleName, 41, "GPA"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtRefNo", Language.getMessage(mstrModuleName, 42, "Reference No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtInstitution", Language.getMessage(mstrModuleName, 43, "Institute"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtStartDate", Language.getMessage(mstrModuleName, 44, "Award Start Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptProfessionalQualification"), "txtEndDate", Language.getMessage(mstrModuleName, 45, "Award End Date"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtOQualification", Language.getMessage(mstrModuleName, 46, "OTHER QUALIFICATION/SHORT COURSES"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtQualificationGrp", Language.getMessage(mstrModuleName, 38, "Qualification Group"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtQualification", Language.getMessage(mstrModuleName, 39, "Qualification"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtResult", Language.getMessage(mstrModuleName, 40, "Result"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtGPA", Language.getMessage(mstrModuleName, 41, "GPA"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtRefNo", Language.getMessage(mstrModuleName, 42, "Reference No"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtInstitution", Language.getMessage(mstrModuleName, 43, "Institute"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtStartDate", Language.getMessage(mstrModuleName, 44, "Award Start Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptOtherQualification"), "txtEndDate", Language.getMessage(mstrModuleName, 45, "Award End Date"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "lblJobHistory", Language.getMessage(mstrModuleName, 47, "JOB HISTORY"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtEmployer", Language.getMessage(mstrModuleName, 48, "Employer"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtCompany", Language.getMessage(mstrModuleName, 49, "Company"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtOfficePhone", Language.getMessage(mstrModuleName, 50, "Office Phone"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtJoinDate", Language.getMessage(mstrModuleName, 51, "Join Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtTerminationDate", Language.getMessage(mstrModuleName, 52, "Termination Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtDesignation", Language.getMessage(mstrModuleName, 53, "Designation"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtResposibility", Language.getMessage(mstrModuleName, 54, "Responsibility"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptJobHistory"), "txtLeavingReson", Language.getMessage(mstrModuleName, 55, "Leaving Reason"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "lblReferences", Language.getMessage(mstrModuleName, 56, "REFERENCES"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtName", Language.getMessage(mstrModuleName, 57, "Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtRefType", Language.getMessage(mstrModuleName, 58, "Referee Type"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtPosition", Language.getMessage(mstrModuleName, 59, "Position"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtGender", Language.getMessage(mstrModuleName, 60, "Gender"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtAddress", Language.getMessage(mstrModuleName, 61, "Address"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtTelNo", Language.getMessage(mstrModuleName, 62, "Tel No."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtMobile", Language.getMessage(mstrModuleName, 63, "Mobile"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptReferences"), "txtEmail", Language.getMessage(mstrModuleName, 64, "Email"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 65, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 66, "Printed Date :"))

            'Varsha (03 Jan 2018) -- Start
            'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
            'This filter should allow user to report on all cv's shortlisted for that particular position. 
            'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
            'Call ReportFunction.TextChange(objRpt, "txtReportName", me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            'Varsha (03 Jan 2018) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", "")
            '************ CAPTION SETTINGS ************[END]

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            Try
                objDataOperation.ExecNonQuery("IF OBJECT_ID('tempdb..##TEMP') IS NOT NULL  DROP TABLE ##TEMP ")
            Catch ex As Exception
            End Try
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "PERSONAL DETAILS")
            Language.setMessage(mstrModuleName, 2, "Title :")
            Language.setMessage(mstrModuleName, 3, "Surname :")
            Language.setMessage(mstrModuleName, 4, "Firstname :")
            Language.setMessage(mstrModuleName, 5, "Othername :")
            Language.setMessage(mstrModuleName, 6, "Gender :")
            Language.setMessage(mstrModuleName, 7, "Emp Code :")
            Language.setMessage(mstrModuleName, 8, "Email :")
            Language.setMessage(mstrModuleName, 9, "Birthdate :")
            Language.setMessage(mstrModuleName, 10, "Marital Status :")
            Language.setMessage(mstrModuleName, 11, "Married Date :")
            Language.setMessage(mstrModuleName, 12, "Vacancy :")
            Language.setMessage(mstrModuleName, 13, "Language1 :")
            Language.setMessage(mstrModuleName, 14, "Language2 :")
            Language.setMessage(mstrModuleName, 15, "Language3 :")
            Language.setMessage(mstrModuleName, 16, "Language4 :")
            Language.setMessage(mstrModuleName, 17, "Nationality :")
            Language.setMessage(mstrModuleName, 18, "ADDRESS")
            Language.setMessage(mstrModuleName, 19, "Present Address")
            Language.setMessage(mstrModuleName, 20, "Address :")
            Language.setMessage(mstrModuleName, 21, "Plot No :")
            Language.setMessage(mstrModuleName, 22, "Estate :")
            Language.setMessage(mstrModuleName, 23, "Street :")
            Language.setMessage(mstrModuleName, 24, "Country :")
            Language.setMessage(mstrModuleName, 25, "State :")
            Language.setMessage(mstrModuleName, 26, "Post Town :")
            Language.setMessage(mstrModuleName, 27, "Post Code :")
            Language.setMessage(mstrModuleName, 28, "Region :")
            Language.setMessage(mstrModuleName, 29, "Mobile :")
            Language.setMessage(mstrModuleName, 30, "Alt. No. :")
            Language.setMessage(mstrModuleName, 31, "Tel. No. :")
            Language.setMessage(mstrModuleName, 32, "Fax :")
            Language.setMessage(mstrModuleName, 33, "Permanent Address")
            Language.setMessage(mstrModuleName, 34, "Skill Category")
            Language.setMessage(mstrModuleName, 35, "Skill")
            Language.setMessage(mstrModuleName, 36, "Remark")
            Language.setMessage(mstrModuleName, 37, "OTHER SKILLS")
            Language.setMessage(mstrModuleName, 38, "Qualification Group")
            Language.setMessage(mstrModuleName, 39, "Qualification")
            Language.setMessage(mstrModuleName, 40, "Result")
            Language.setMessage(mstrModuleName, 41, "GPA")
            Language.setMessage(mstrModuleName, 42, "Reference No")
            Language.setMessage(mstrModuleName, 43, "Institute")
            Language.setMessage(mstrModuleName, 44, "Award Start Date")
            Language.setMessage(mstrModuleName, 45, "Award End Date")
            Language.setMessage(mstrModuleName, 46, "OTHER QUALIFICATION/SHORT COURSES")
            Language.setMessage(mstrModuleName, 47, "JOB HISTORY")
            Language.setMessage(mstrModuleName, 48, "Employer")
            Language.setMessage(mstrModuleName, 49, "Company")
            Language.setMessage(mstrModuleName, 50, "Office Phone")
            Language.setMessage(mstrModuleName, 51, "Join Date")
            Language.setMessage(mstrModuleName, 52, "Termination Date")
            Language.setMessage(mstrModuleName, 53, "Designation")
            Language.setMessage(mstrModuleName, 54, "Responsibility")
            Language.setMessage(mstrModuleName, 55, "Leaving Reason")
            Language.setMessage(mstrModuleName, 56, "REFERENCES")
            Language.setMessage(mstrModuleName, 57, "Name")
            Language.setMessage(mstrModuleName, 58, "Referee Type")
            Language.setMessage(mstrModuleName, 59, "Position")
            Language.setMessage(mstrModuleName, 60, "Gender")
            Language.setMessage(mstrModuleName, 61, "Address")
            Language.setMessage(mstrModuleName, 62, "Tel No.")
            Language.setMessage(mstrModuleName, 63, "Mobile")
            Language.setMessage(mstrModuleName, 64, "Email")
            Language.setMessage(mstrModuleName, 65, "Printed By :")
            Language.setMessage(mstrModuleName, 66, "Printed Date :")
            Language.setMessage(mstrModuleName, 67, "PROFESSIONAL SKILLS")
            Language.setMessage(mstrModuleName, 68, "PROFESSIONAL QUALIFICATION")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
