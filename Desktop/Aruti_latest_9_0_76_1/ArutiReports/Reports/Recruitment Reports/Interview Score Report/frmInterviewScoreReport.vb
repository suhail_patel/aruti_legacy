#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmInterviewScoreReport
#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmInterviewScoreReport"
    Private objInterviewScoreReport As clsInterviewScoreReport
    
#End Region

#Region " Constructor "
    Public Sub New()
        objInterviewScoreReport = New clsInterviewScoreReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objInterviewScoreReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objVacancy As New clsVacancy
        Dim objInterviewType As New clsCommon_Master
        Dim objBatch As New clsBatchSchedule
        Try
            dsCombo = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            dsCombo = objBatch.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)

            Dim dtFilter = New DataView(dsCombo.Tables(0), "vacancyid IN(0," & CInt(cboVacancy.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboBatch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dtFilter
                .SelectedValue = 0
            End With

            dsCombo = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True)
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objVacancy = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboVacancyType.SelectedIndex = 0
            cboVacancy.SelectedIndex = 0
            cboBatch.SelectedIndex = 0
            cboInterviewType.SelectedIndex = 0

            objInterviewScoreReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objInterviewScoreReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objInterviewScoreReport.SetDefaultValue()

            objInterviewScoreReport._VacancyID = cboVacancy.SelectedValue
            objInterviewScoreReport._VacancyName = cboVacancy.Text

            objInterviewScoreReport._VacancyTypeID = cboVacancyType.SelectedValue
            objInterviewScoreReport._VacancyTypeName = cboVacancyType.Text

            objInterviewScoreReport._BatchID = cboBatch.SelectedValue
            objInterviewScoreReport._BatchName = cboBatch.Text

            objInterviewScoreReport._InterviewTypeID = cboInterviewType.SelectedValue
            objInterviewScoreReport._InterviewTypeName = cboInterviewType.Text

          
            'objInterviewScoreReport._Advance_Filter = mstrAdvanceFilter
            'objInterviewScoreReport._ViewByIds = mstrViewByIds
            objInterviewScoreReport._ViewIndex = 1
            'objInterviewScoreReport._ViewByName = mstrViewByName
            'objInterviewScoreReport._Analysis_Fields = mstrAnalysis_Fields
            'objInterviewScoreReport._Analysis_Join = mstrAnalysis_Join
            'objInterviewScoreReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objInterviewScoreReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objInterviewScoreReport._Report_GroupName = "BatchName"


            'objInterviewScoreReport._IgnoreSameLevelIfApproved = chkIgnoreSameLevel.Checked


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "
    Private Sub frmInterviewScoreReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objInterviewScoreReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantVacancyReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApplicantVacancyReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantVacancyReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLvForm_ApprovalStatus_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLvForm_ApprovalStatus_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLvForm_ApprovalStatus_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLvForm_ApprovalStatus_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsInterviewScoreReport.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewScoreReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "
    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = cboVacancy.DataSource
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            If CInt(cboVacancyType.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Vacancy Type."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Exit Sub
            End If

            If CInt(cboVacancy.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Vacancy."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Exit Sub
            End If

            If CInt(cboBatch.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Batch."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If


            If CInt(cboInterviewType.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Interview Type."), enMsgBoxStyle.Information)
                cboInterviewType.Focus()
                Exit Sub
            End If

            objInterviewScoreReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                      , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, ConfigParameter._Object._UserAccessModeSetting, True)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CombBox Event "
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsCombo As New DataSet

            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", -1, CInt(cboVacancyType.SelectedValue))

            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            If CInt(cboVacancy.SelectedValue) > 0 Then
                Dim dsBatch As New DataSet
                Dim objBatch As New clsBatchSchedule

                dsBatch = objBatch.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)

                Dim dtFilter = New DataView(dsBatch.Tables(0), "vacancyid IN(0," & CInt(cboVacancy.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable
                With cboBatch
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dtFilter
                    .SelectedValue = 0
                End With
            Else
                cboBatch.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objInterviewScoreReport.setOrderBy(0)
            txtOrderBy.Text = objInterviewScoreReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LalblVacancy.Text = Language._Object.getCaption(Me.LalblVacancy.Name, Me.LalblVacancy.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
			Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Vacancy Type.")
			Language.setMessage(mstrModuleName, 2, "Please Select Vacancy.")
			Language.setMessage(mstrModuleName, 3, "Please Select Batch.")
			Language.setMessage(mstrModuleName, 4, "Please Select Interview Type.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
