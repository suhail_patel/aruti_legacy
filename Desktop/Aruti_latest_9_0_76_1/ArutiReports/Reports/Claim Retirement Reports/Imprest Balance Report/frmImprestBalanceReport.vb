'************************************************************************************************************************************
'Class Name : frmImprestBalanceReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImprestBalanceReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImprestBalanceReport"
    Private ObjImprest As clsImprestBalanceReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""



#End Region

#Region " Contructor "

    Public Sub New()
        ObjImprest = New clsImprestBalanceReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        ObjImprest.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 10, "Imprest Retired Balance Report"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 11, "Imprest UnRetired Balance Report"))
            cboReportType.SelectedIndex = 0

            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
            End With

            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboExpenseCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
            End With

            FillStatus()

            Dim objPeriod As New clscommom_period_Tran
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            RemoveHandler cboFromPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged
            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            AddHandler cboFromPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged

            RemoveHandler cboToPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged
            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            AddHandler cboToPeriod.SelectedIndexChanged, AddressOf cboFromPeriod_SelectedIndexChanged
            objPeriod = Nothing


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            dsCombos = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombos = objExchange.getComboList("List", True, False)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables(0)
            End With
            objExchange = Nothing
            'Pinkal (30-Mar-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub FillStatus()
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMasterData.getLeaveStatusList("List", "")
            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable

            Dim dRow As DataRow = dtab.NewRow
            dRow("statusunkid") = 999
            dRow("name") = Language.getMessage(mstrModuleName, 9, "Not Retired")
            dtab.Rows.InsertAt(dRow, dtab.Rows.Count)

            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "name"
                .DataSource = dtab
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillStatus", mstrModuleName)
        Finally
            objMasterData = Nothing
            dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            dtpTranFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTranToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTranFromDate.Checked = False
            dtpTranToDate.Checked = False
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            chkShowRetirementStatus.Checked = True
            cboFromPeriod.SelectedValue = 0
            cboToPeriod.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            cboExpense.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            cboCurrency.SelectedValue = 0
            'Pinkal (30-Mar-2021) -- End

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            chkShowCRetirementNo.Checked = True
            'S.SANDEEP |21-FEB-2022| -- End

            ObjImprest.setDefaultOrderBy(0)
            txtOrderBy.Text = ObjImprest.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            ObjImprest.SetDefaultValue()

            If (dtpTranFromDate.Checked = True AndAlso dtpTranToDate.Checked = False) OrElse (dtpTranFromDate.Checked = False AndAlso dtpTranToDate.Checked = True) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue."), enMsgBoxStyle.Information)
                Return False

            ElseIf CInt(cboFromPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Select()
                Exit Function

            ElseIf CInt(cboFromPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Function

            ElseIf cboToPeriod.SelectedIndex < cboFromPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, " To Period cannot be less than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            End If

            ObjImprest._ReportTypeId = CInt(cboReportType.SelectedIndex)
            ObjImprest._ReportType = cboReportType.Text
            ObjImprest._FromDate = IIf(dtpTranFromDate.Checked = True, dtpTranFromDate.Value.Date, Nothing)
            ObjImprest._ToDate = IIf(dtpTranToDate.Checked = True, dtpTranToDate.Value.Date, Nothing)
            ObjImprest._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            ObjImprest._ExpCateName = cboExpenseCategory.Text.ToString()
            ObjImprest._EmpUnkId = CInt(cboEmployee.SelectedValue)
            ObjImprest._EmpName = cboEmployee.Text.ToString
            If CInt(cboFromPeriod.SelectedValue) > 0 AndAlso CInt(cboToPeriod.SelectedValue) > 0 Then
                ObjImprest._StatusId = cboStatus.SelectedIndex
            Else
                ObjImprest._StatusId = CInt(cboStatus.SelectedValue)
            End If
            ObjImprest._StatusName = cboStatus.Text.ToString
            ObjImprest._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            ObjImprest._UserUnkid = User._Object._Userunkid
            ObjImprest._CompanyUnkId = Company._Object._Companyunkid
            ObjImprest._ShowRetirememtFormStatus = chkShowRetirementStatus.Checked

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            ObjImprest._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
            ObjImprest._FromPeriod = cboFromPeriod.Text
            ObjImprest._PeriodStartDate = objPeriod._Start_Date.Date

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            ObjImprest._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            ObjImprest._ToPeriod = cboToPeriod.Text
            ObjImprest._PeriodEndDate = objPeriod._End_Date.Date
            objPeriod = Nothing

            ObjImprest._ExpenseID = CInt(cboExpense.SelectedValue)
            ObjImprest._Expense = cboExpense.Text
            ObjImprest._ViewByIds = mstrStringIds
            ObjImprest._ViewIndex = mintViewIdx
            ObjImprest._ViewByName = mstrStringName
            ObjImprest._Analysis_Fields = mstrAnalysis_Fields
            ObjImprest._Analysis_Join = mstrAnalysis_Join
            ObjImprest._Analysis_OrderBy = mstrAnalysis_OrderBy
            ObjImprest._Report_GroupName = mstrReport_GroupName

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            ObjImprest._CurrencyId = CInt(cboCurrency.SelectedValue)
            ObjImprest._Currency = cboCurrency.Text
            'Pinkal (30-Mar-2021) -- End

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            ObjImprest._ShowClaimRetirementNumber = chkShowCRetirementNo.Checked
            'S.SANDEEP |21-FEB-2022| -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmImprestBalanceReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            ObjImprest = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmImprestBalanceReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImprestBalanceReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = ObjImprest._ReportName
            Me._Message = ObjImprest._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImprestBalanceReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImprestBalanceReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsImprestBalanceReport.SetMessages()
            objfrm._Other_ModuleNames = "clsImprestBalanceReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmImprestBalanceReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            ObjImprest.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            
            ObjImprest.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboStatus.DataSource
            frm.ValueMember = cboStatus.ValueMember
            frm.DisplayMember = cboStatus.DisplayMember
            If frm.DisplayDialog Then
                cboStatus.SelectedValue = frm.SelectedValue
                cboStatus.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSector_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpenseCategory.DataSource
            frm.ValueMember = cboExpenseCategory.ValueMember
            frm.DisplayMember = cboExpenseCategory.DisplayMember
            If frm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = frm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            ObjImprest.setOrderBy(0)
            txtOrderBy.Text = ObjImprest.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExpense.DataSource
            frm.ValueMember = cboExpense.ValueMember
            frm.DisplayMember = cboExpense.DisplayMember
            If frm.DisplayDialog Then
                cboExpense.SelectedValue = frm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) = 0 Then
                cboStatus.Enabled = True
            Else
                cboStatus.Enabled = False
            End If
            cboFromPeriod_SelectedIndexChanged(cboFromPeriod, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", , , , "cmexpense_master.isimprest = 1", "")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objExpense = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFromPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromPeriod.SelectedIndexChanged, cboToPeriod.SelectedIndexChanged
        Try
            If cboFromPeriod.SelectedValue > 0 AndAlso cboToPeriod.SelectedValue > 0 Then
                dtpTranFromDate.Checked = False
                dtpTranToDate.Checked = False
                dtpTranFromDate.Enabled = False
                dtpTranToDate.Enabled = False
                If cboStatus.DataSource IsNot Nothing Then cboStatus.DataSource = Nothing
                cboStatus.Items.Clear()
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 6, "Select"))
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 7, "Posted"))
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 8, "Not Posted"))
                cboStatus.SelectedIndex = 0
                cboStatus.Enabled = True
            Else
                dtpTranFromDate.Enabled = True
                dtpTranToDate.Enabled = True
                If cboStatus.DataSource Is Nothing AndAlso cboStatus.Items.Count > 0 Then cboStatus.Items.Clear()
                FillStatus()
                If CInt(cboReportType.SelectedIndex) = 0 Then
                    cboStatus.Enabled = True
                Else
                    cboStatus.Enabled = False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFromPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region


   
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTranFromDate.Text = Language._Object.getCaption(Me.lblTranFromDate.Name, Me.lblTranFromDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
			Me.lblTranToDate.Text = Language._Object.getCaption(Me.lblTranToDate.Name, Me.lblTranToDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkShowRetirementStatus.Text = Language._Object.getCaption(Me.chkShowRetirementStatus.Name, Me.chkShowRetirementStatus.Text)
			Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.Name, Me.LblExpense.Text)
			Me.LblPostedPeriodFrom.Text = Language._Object.getCaption(Me.LblPostedPeriodFrom.Name, Me.LblPostedPeriodFrom.Text)
			Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.Name, Me.LblTo.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.Name, Me.LblReportType.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue.")
			Language.setMessage(mstrModuleName, 3, "Please Select From Period.")
			Language.setMessage(mstrModuleName, 4, "Please Select To Period.")
			Language.setMessage(mstrModuleName, 5, " To Period cannot be less than From Period.")
			Language.setMessage(mstrModuleName, 6, "Select")
			Language.setMessage(mstrModuleName, 7, "Posted")
			Language.setMessage(mstrModuleName, 8, "Not Posted")
			Language.setMessage(mstrModuleName, 9, "Not Retired")
			Language.setMessage(mstrModuleName, 10, "Imprest Retired Balance Report")
			Language.setMessage(mstrModuleName, 11, "Imprest UnRetired Balance Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
