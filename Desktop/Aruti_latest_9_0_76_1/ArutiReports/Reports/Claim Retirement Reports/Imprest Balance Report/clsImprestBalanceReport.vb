'************************************************************************************************************************************
'Class Name :clsImprestBalanceReport.vb
'Purpose    :
'Date       :  
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsImprestBalanceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsImprestBalanceReport"
    Private mstrReportId As String = enArutiReport.Imprest_Balance_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mintReportTypeId As Integer = 0
    Private mstrReportType As String = ""
    Private mdtFromdate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintExpCateId As Integer = 0
    Private mstrExpCateName As String = ""
    Private mintEmpUnkId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Dim mblnShowRetirememtFormStatus As Boolean = True
    Private mintFromPeriodID As Integer = 0
    Private mstrFromPeriod As String = ""
    Private mintToPeriodID As Integer = 0
    Private mstrToPeriod As String = ""
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintExpenseID As Integer = -1
    Private mstrExpense As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

    'Pinkal (30-Mar-2021)-- Start
    'NMB Enhancement  -  Working on Employee Recategorization history Report.
    Private mintCurrencyId As Integer = 0
    Private mstrCurrency As String = ""
    'Pinkal (30-Mar-2021) -- End

    'S.SANDEEP |21-FEB-2022| -- START
    'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
    Private mblnShowClaimRetirementNumber As Boolean = False
    'S.SANDEEP |21-FEB-2022| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportType() As String
        Set(ByVal value As String)
            mstrReportType = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromdate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateId() As Integer
        Set(ByVal value As Integer)
            mintExpCateId = value
        End Set
    End Property

    Public WriteOnly Property _ExpCateName() As String
        Set(ByVal value As String)
            mstrExpCateName = value
        End Set
    End Property

    Public WriteOnly Property _EmpUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmpUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkid() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ShowRetirememtFormStatus() As Boolean
        Set(ByVal value As Boolean)
            mblnShowRetirememtFormStatus = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriod() As String
        Set(ByVal value As String)
            mstrFromPeriod = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriod() As String
        Set(ByVal value As String)
            mstrToPeriod = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ExpenseID() As Integer
        Set(ByVal value As Integer)
            mintExpenseID = value
        End Set
    End Property

    Public WriteOnly Property _Expense() As String
        Set(ByVal value As String)
            mstrExpense = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    'Pinkal (30-Mar-2021)-- Start
    'NMB Enhancement  -  Working on Employee Recategorization history Report.
    Public Property _CurrencyId() As Integer
        Get
            Return mintCurrencyId
        End Get
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public Property _Currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property
    'Pinkal (30-Mar-2021) -- End

    'S.SANDEEP |21-FEB-2022| -- START
    'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
    Public WriteOnly Property _ShowClaimRetirementNumber() As String
        Set(ByVal value As String)
            mblnShowClaimRetirementNumber = value
        End Set
    End Property
    'S.SANDEEP |21-FEB-2022| -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportType = ""
            mdtFromdate = Nothing
            mdtToDate = Nothing
            mintExpCateId = 0
            mstrExpCateName = ""
            mintEmpUnkId = 0
            mstrEmpName = ""
            mintStatusId = 0
            mstrStatusName = ""
            mintUserUnkid = 0
            mintCompanyUnkid = 0
            mblnShowRetirememtFormStatus = True
            mintFromPeriodID = 0
            mstrFromPeriod = ""
            mintToPeriodID = 0
            mstrToPeriod = ""
            mdtPeriodStartDate = Nothing
            mdtPeriodEndDate = Nothing
            mintExpenseID = 0
            mstrExpense = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            mintCurrencyId = 0
            mstrCurrency = ""
            'Pinkal (30-Mar-2021) -- End

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            mblnShowClaimRetirementNumber = False
            'S.SANDEEP |21-FEB-2022| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterRetiredTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtFromdate <> Nothing AndAlso mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromdate))
                objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtFromdate.ToShortDateString & " To " & mdtToDate.ToShortDateString & " "
            End If

            If mintExpCateId > 0 Then
                objDataOperation.AddParameter("@expcateid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpCateId)
                Me._FilterQuery &= " AND cmclaim_retirement_master.expensetypeunkid = @expcateid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Expense Category :") & " " & mstrExpCateName & " "
            End If

            If mintEmpUnkId > 0 Then
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpUnkId)
                Me._FilterQuery &= " AND cmclaim_retirement_master.employeeunkid = @empunkid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & mstrEmpName & " "
            End If

            If mintStatusId > 0 AndAlso (mdtPeriodStartDate = Nothing AndAlso mdtPeriodEndDate = Nothing) Then
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterQuery &= " AND cmclaim_retirement_master.statusunkid  = @statusid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Status :") & " " & mstrStatusName & " "
            ElseIf mintStatusId > 0 AndAlso (mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing) Then
                objDataOperation.AddParameter("@statusid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IIf(mintStatusId > 1, False, True))
                Me._FilterQuery &= " AND ISNULL(ApprTable.isposted,0) = @statusid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Status :") & " " & mstrStatusName & " "
            End If

            If mintFromPeriodID > 0 AndAlso mintToPeriodID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Posted Period From :") & " " & mstrFromPeriod & " " & Language.getMessage(mstrModuleName, 34, "Posted Period To :") & mstrToPeriod & " "
            End If

            If mintExpenseID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Expense :") & " " & mstrExpense & " "
            End If

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 38, "Currency :") & " " & mstrCurrency & " "
            End If
            'Pinkal (30-Mar-2021) -- End


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Order By : ") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterRetiredTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterUnRetiredTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mdtFromdate <> Nothing AndAlso mdtToDate <> Nothing Then
                objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromdate))
                objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterQuery &= " AND CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) BETWEEN @fromdate AND @todate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtFromdate.ToShortDateString & " To " & mdtToDate.ToShortDateString & " "
            End If

            If mintExpCateId > 0 Then
                objDataOperation.AddParameter("@expcateid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpCateId)
                Me._FilterQuery &= " AND cmclaim_request_master.expensetypeid = @expcateid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Expense Category :") & " " & mstrExpCateName & " "
            End If

            If mintEmpUnkId > 0 Then
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpUnkId)
                Me._FilterQuery &= " AND cmclaim_request_master.employeeunkid = @empunkid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & mstrEmpName & " "
            End If

            If mintStatusId > 0 AndAlso (mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing) Then
                objDataOperation.AddParameter("@statusid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IIf(mintStatusId > 1, False, True))
                Me._FilterQuery &= " AND ISNULL(ClaimApprTable.isposted,0) = @statusid"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Status :") & " " & mstrStatusName & " "
            End If

            If mintFromPeriodID > 0 AndAlso mintToPeriodID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Posted Period From :") & " " & mstrFromPeriod & " " & Language.getMessage(mstrModuleName, 34, "Posted Period To :") & mstrToPeriod & " "
            End If

            If mintExpenseID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Expense :") & " " & mstrExpense & " "
            End If

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 38, "Currency :") & " " & mstrCurrency & " "
            End If
            'Pinkal (30-Mar-2021) -- End


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Order By : ") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY GName," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterUnRetiredTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If mintReportTypeId = 0 Then
                objRpt = Generate_RetiredReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            ElseIf mintReportTypeId = 1 Then
                objRpt = Generate_UnRetiredReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("cmclaim_request_master.claimrequestno", Language.getMessage(mstrModuleName, 6, "Claim No")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 27, "Employee Code")))
            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') ", Language.getMessage(mstrModuleName, 7, "Employee Name")))
            End If
            iColumn_DetailReport.Add(New IColumn("cmclaim_retirement_master.transactiondate", Language.getMessage(mstrModuleName, 8, "Transcation Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_RetiredReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim StrQuery As String = String.Empty
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty


            StrQ = " SELECT " & _
                       " ISNULL(hremployee_master.employeecode,'') AS Code "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            StrQ &= ", ISNULL(hrgrade_master.name,'') AS grade " & _
                         ", ISNULL(cmclaim_request_master.claimrequestno,'') AS ClaimNo " & _
                         ", cmclaim_retirement_master.transactiondate AS RetireDate " & _
                         ", cmclaim_retirement_master.expensetypeunkid AS expensetypeunkid " & _
                         ", cmclaim_retirement_master.retirement_remark AS Description " & _
                         ", CASE WHEN cmclaim_retirement_master.expensetypeunkid = 1 THEN @Leave " & _
                         "          WHEN cmclaim_retirement_master.expensetypeunkid = 2 THEN @Medical " & _
                         "          WHEN cmclaim_retirement_master.expensetypeunkid = 3 THEN @Training " & _
                         "          WHEN cmclaim_retirement_master.expensetypeunkid = 4 THEN @Miscellaneous " & _
                         "          WHEN cmclaim_retirement_master.expensetypeunkid = 5 THEN @Imprest END AS expensetype " & _
                         ", CASE WHEN cmclaim_retirement_master.statusunkid = 1 THEN ISNULL(ApprTable.RetireAmt,0) ELSE 0.00 END AS ApprAmount " & _
                         ", CASE WHEN cmclaim_retirement_master.statusunkid = 1 THEN ISNULL(ApprTable.RetireQty,0) ELSE 0.00 END AS ApprQty " & _
                         ", cmclaim_retirement_master.statusunkid AS statusunkid " & _
                         ", CASE WHEN cmclaim_retirement_master.statusunkid = 1 then @Approve " & _
                         "          WHEN cmclaim_retirement_master.statusunkid = 2 then @Pending " & _
                         "          WHEN cmclaim_retirement_master.statusunkid = 3 then @Reject " & _
                         "          WHEN cmclaim_retirement_master.statusunkid = 6 then @Cancel " & _
                         " END as status " & _
                         ", ApprTable.ApprName " & _
                         ", cmclaim_retirement_master.claim_amount AS claim_amount " & _
                         ", CASE WHEN cmclaim_retirement_master.statusunkid = 3 THEN 0.00 ELSE ISNULL(cmclaim_retirement_master.imprest_amount, 0.00) END AS retired_amount " & _
                         ", CASE WHEN ISNULL(cmclaim_retirement_master.imprest_amount,0.00) = 0 THEN   ISNULL(cmclaim_retirement_master.claim_amount,0.00) " & _
                         "  ELSE CASE WHEN cmclaim_retirement_master.statusunkid = 3 THEN  cmclaim_retirement_master.claim_amount ELSE  ISNULL(cmclaim_retirement_master.balance,0.00) END END AS balance_due "


            'Pinkal (27-Apr-2021)-- Working on Claim Retirement Enhancement.[   ", CASE WHEN cmclaim_retirement_master.statusunkid = 3 THEN 0.00 ELSE ISNULL(cmclaim_retirement_master.imprest_amount, 0.00) END AS retired_amount " &]

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQ &= ",ISNULL(ApprTable.isposted,0) as isposted "
            End If

            StrQ &= ",CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) as transactiondate "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQ &= " ,ApprTable.countryunkid,ApprTable.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            If mblnShowClaimRetirementNumber = True Then StrQ &= ", ISNULL(cmclaim_retirement_master.claimretirementno,'') AS claimretirementno "
            'S.SANDEEP |21-FEB-2022| -- END

            StrQ &= " FROM cmclaim_retirement_master  " & _
                         " LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_master.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 "

            If mdtFromdate <> Nothing AndAlso mdtToDate <> Nothing Then
                StrQ &= " AND CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) BETWEEN @fromdate AND @todate "
            End If

            StrQ &= " JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                        " JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "        gradeunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "    FROM prsalaryincrement_tran " & _
                        "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "'" & _
                        " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                        " LEFT JOIN ( " & _
                        " SELECT " & _
                        "       claimretirementunkid ,RetireAmt " & _
                        "      ,RetireQty ,statusunkid " & _
                        "      ,A.ApprName ,ROW_NUMBER() OVER (PARTITION BY claimretirementunkid ORDER BY crpriority DESC,statusunkid) AS Rno "

            'Pinkal (27-Apr-2021)-- Working on Claim Retirement Enhancement.[ROW_NUMBER() OVER (PARTITION BY claimretirementunkid ORDER BY statusunkid,crpriority DESC) AS Rno "]


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQ &= " ,countryunkid,currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQ &= "           ,isposted "
            End If


            StrQ &= "         FROM " & _
                        "          ( "


            StrQuery = "    SELECT " & _
                             "       cmclaim_retirement_approval_tran.approverunkid " & _
                             "      ,cmclaim_retirement_approval_tran.claimretirementunkid " & _
                             "      ,SUM(cmclaim_retirement_approval_tran.amount) AS RetireAmt " & _
                             "      ,SUM(cmclaim_retirement_approval_tran.quantity) AS RetireQty " & _
                             "      ,cmclaim_retirement_approval_tran.statusunkid " & _
                             "      , #APPR_NAME# AS ApprName " & _
                             "      ,cmapproverlevel_master.crpriority "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= " , cmclaim_retirement_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= "               , cmprocess.isposted "
            End If

            StrQuery &= "               FROM cmclaim_retirement_approval_tran " & _
                               " LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                               " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_retirement_approval_tran.approverunkid " & _
                               " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                               " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_retirement_approval_tran.expenseunkid " & _
                               " #EMPL_JOIN#  "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_retirement_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_retirement_approval_tran.countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= " JOIN (SELECT cmretire_process_tran.employeeunkid,cmretire_process_tran.expenseunkid,cmretire_process_tran.isposted, cmretire_process_tran.claimretirementunkid " & _
                                   "         ,cmretire_process_tran.claimretirementapprovaltranunkid  ,cmretire_process_tran.claimretirementtranunkid " & _
                                   "           FROM cmretire_process_tran " & _
                                   "           LEFT JOIN cfcommon_period_tran  ON cmretire_process_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "           WHERE cmretire_process_tran.isvoid = 0 AND "


                If mintStatusId <= 0 Then
                    StrQuery &= "  (ISNULL(cmretire_process_tran.periodunkid,0) <=0 OR Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "') "
                ElseIf mintStatusId = 1 Then
                    StrQuery &= " ISNULL(cmretire_process_tran.periodunkid,0) > 0 AND Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'"
                ElseIf mintStatusId = 2 Then
                    StrQuery &= " ISNULL(cmretire_process_tran.periodunkid,0) <=0"
                End If

                StrQuery &= "        ) AS cmprocess ON cmprocess.employeeunkid= cmclaim_retirement_master.employeeunkid AND cmprocess.expenseunkid = cmclaim_retirement_approval_tran.expenseunkid " & _
                                   " AND cmprocess.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmprocess.claimretirementapprovaltranunkid = cmclaim_retirement_approval_tran.claimretirementapprovaltranunkid " & _
                                   " AND cmprocess.claimretirementtranunkid = cmclaim_retirement_approval_tran.claimretirementtranunkid "


            End If


            StrQCondition = " WHERE cmclaim_retirement_approval_tran.isvoid = 0 AND cmclaim_retirement_master.iscancel = 0 AND cmclaim_retirement_approval_tran.iscancel = 0 " & _
                                    " AND cmexpense_master.isimprest = 1 AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            'Pinkal (27-Apr-2021)-- Start
            'KBC Enhancement  -  Working on Claim Retirement Enhancement.
            StrQCondition &= " AND cmclaim_retirement_approval_tran.visibleid <> -1	AND cmclaim_retirement_approval_tran.visibleid  = cmclaim_retirement_approval_tran.statusunkid "
            'Pinkal (27-Apr-2021) -- End


            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_retirement_approval_tran.expenseunkid = " & mintExpenseID
            End If


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_retirement_approval_tran.countryunkid = @countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            StrQCondition &= " GROUP BY " & _
                                      " cmclaim_retirement_approval_tran.approverunkid " & _
                                      " ,cmclaim_retirement_approval_tran.claimretirementunkid " & _
                                      " ,cmclaim_retirement_approval_tran.statusunkid " & _
                                      " ,cmapproverlevel_master.crpriority " & _
                                      " ,#APPR_NAME# "


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= ",cmclaim_retirement_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End


            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQCondition &= ", cmprocess.isposted "
            End If


            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")

            Dim objExpApproverMaster As New clsExpenseApprover_Master
            Dim dsExternalCompany As DataSet = objExpApproverMaster.GetClaimExternalApproverList(Nothing, "List")
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsExternalCompany.Tables(0).Rows
                If StrFinalQurey.Trim.Length > 0 Then StrFinalQurey &= " UNION  "
                StrFinalQurey &= StrQuery
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       " ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     " ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       " LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey

            StrQuery = ""
            StrFinalQurey = ""
            StrQCondition = ""

            StrQ &= " UNION "


            StrQuery = " SELECT " & _
                             "   cmclaim_retirement_approval_tran.approverunkid " & _
                             "  ,cmclaim_retirement_approval_tran.claimretirementunkid " & _
                             "  ,SUM(cmclaim_retirement_approval_tran.amount) AS RetireAmt " & _
                             "  ,SUM(cmclaim_retirement_approval_tran.quantity) AS RetireQty " & _
                             "  ,cmclaim_retirement_approval_tran.statusunkid " & _
                             "  , #APPR_NAME# AS ApprName " & _
                             "  ,cmapproverlevel_master.crpriority "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= " , cmclaim_retirement_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= ",cmprocess.isposted "
            End If

            StrQuery &= " FROM cmclaim_retirement_approval_tran " & _
                               " LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                               " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_retirement_master.approverunkid " & _
                               " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                               " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_retirement_approval_tran.expenseunkid " & _
                               " #EMPL_JOIN#  "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_retirement_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_retirement_approval_tran.countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= " JOIN (SELECT cmretire_process_tran.employeeunkid,cmretire_process_tran.expenseunkid,cmretire_process_tran.isposted " & _
                                   "                    , cmretire_process_tran.claimretirementunkid ,cmretire_process_tran.claimretirementapprovaltranunkid  ,cmretire_process_tran.claimretirementtranunkid " & _
                                   "           FROM cmretire_process_tran " & _
                                   "           LEFT JOIN cfcommon_period_tran  ON cmretire_process_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "           WHERE cmretire_process_tran.isvoid = 0 AND "

                If mintStatusId <= 0 Then
                    StrQuery &= " (ISNULL(cmretire_process_tran.periodunkid,0) <=0 OR Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND 	 Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "') "
                ElseIf mintStatusId = 1 Then
                    StrQuery &= " ISNULL(cmretire_process_tran.periodunkid,0) > 0 AND Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'"
                ElseIf mintStatusId = 2 Then
                    StrQuery &= " ISNULL(cmretire_process_tran.periodunkid,0) <=0  "
                End If
                StrQuery &= "        ) AS cmprocess ON cmprocess.employeeunkid= cmclaim_retirement_master.employeeunkid AND cmprocess.expenseunkid = cmclaim_retirement_approval_tran.expenseunkid " & _
                                   " AND cmprocess.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmprocess.claimretirementapprovaltranunkid = cmclaim_retirement_approval_tran.claimretirementapprovaltranunkid " & _
                                   " AND cmprocess.claimretirementtranunkid = cmclaim_retirement_approval_tran.claimretirementtranunkid "

            End If

            StrQCondition = " WHERE cmclaim_retirement_approval_tran.isvoid = 0 AND cmclaim_retirement_master.iscancel  =1 " & _
                                    " AND cmexpense_master.isimprest = 1 AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            'Pinkal (27-Apr-2021)-- Start
            'KBC Enhancement  -  Working on Claim Retirement Enhancement.
            StrQCondition &= " AND cmclaim_retirement_approval_tran.visibleid <> -1	AND cmclaim_retirement_approval_tran.visibleid  = cmclaim_retirement_approval_tran.statusunkid "
            'Pinkal (27-Apr-2021) -- End


            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_retirement_approval_tran.expenseunkid = " & mintExpenseID
            End If


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_retirement_approval_tran.countryunkid = @countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End

            StrQCondition &= "GROUP BY " & _
                                    " cmclaim_retirement_approval_tran.approverunkid " & _
                                    ",cmclaim_retirement_approval_tran.claimretirementunkid " & _
                                    ",cmclaim_retirement_approval_tran.statusunkid " & _
                                    ",cmapproverlevel_master.crpriority " & _
                                    ",#APPR_NAME# "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= ",cmclaim_retirement_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQCondition &= ", cmprocess.isposted "
            End If

            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")

            For Each dr In dsExternalCompany.Tables(0).Rows
                If StrFinalQurey.Trim.Length > 0 Then StrFinalQurey &= " UNION  "
                StrFinalQurey &= StrQuery
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       " ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     " ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                     " LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey

            StrQ &= " )  AS A  " & _
                         " ) AS ApprTable ON ApprTable.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid AND ApprTable.Rno <=1 "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  cmclaim_retirement_master.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            Call FilterRetiredTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Code")
                rpt_Rows.Item("Column2") = dtRow.Item("employee")
                'rpt_Rows.Item("Column3") = dtRow.Item("grade")
                rpt_Rows.Item("Column3") = CDate(dtRow.Item("RetireDate")).ToShortDateString()
                rpt_Rows.Item("Column4") = dtRow.Item("ClaimNo")
                rpt_Rows.Item("Column5") = Format(dtRow.Item("claim_amount"), GUI.fmtCurrency)
                rpt_Rows.Item("Column6") = Format(dtRow.Item("retired_amount"), GUI.fmtCurrency)
                rpt_Rows.Item("Column7") = Format(dtRow.Item("balance_due"), GUI.fmtCurrency)

                If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                    rpt_Rows.Item("Column8") = IIf(CBool(dtRow.Item("isposted")), Language.getMessage(mstrModuleName, 31, "Posted"), Language.getMessage(mstrModuleName, 32, "Not Posted"))
                Else
                    rpt_Rows.Item("Column8") = dtRow.Item("status") & " BY :- " & dtRow.Item("ApprName")
                End If

                rpt_Rows.Item("Column9") = dtRow.Item("expensetype").ToString()

                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column13") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)

                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column15") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)

                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column16") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(balance_due)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)

                Else
                    rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), 0, dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), GUI.fmtCurrency)
                End If

                rpt_Rows.Item("Column17") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "1=1")), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "1=1")), GUI.fmtCurrency)
                rpt_Rows.Item("Column18") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "1=1")), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "1=1")), GUI.fmtCurrency)
                rpt_Rows.Item("Column19") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "1=1")), 0, dsList.Tables(0).Compute("SUM(balance_due)", "1=1")), GUI.fmtCurrency)

                'S.SANDEEP |21-FEB-2022| -- START
                'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
                If mblnShowClaimRetirementNumber = True Then rpt_Rows.Item("Column20") = dtRow.Item("claimretirementno")
                'S.SANDEEP |21-FEB-2022| -- END

                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column14") = dtRow.Item("GName").ToString()
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptImprestBalanceReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If mintViewIndex <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationGroupTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 36, " Total :"))
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 16, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 17, "Employee"))
            'Call ReportFunction.TextChange(objRpt, "txtSalaryGrade", Language.getMessage(mstrModuleName, 18, "Salary Grade"))
            Call ReportFunction.TextChange(objRpt, "txtSalaryGrade", Language.getMessage(mstrModuleName, 18, "Transaction Date"))
            Call ReportFunction.TextChange(objRpt, "txtClaimNo", Language.getMessage(mstrModuleName, 19, "Claim No."))
            Call ReportFunction.TextChange(objRpt, "txtClaimAppAmt", Language.getMessage(mstrModuleName, 20, "Approved Claim"))
            Call ReportFunction.TextChange(objRpt, "txtRetireAmt", Language.getMessage(mstrModuleName, 21, "Retired Amount"))
            Call ReportFunction.TextChange(objRpt, "txtBalancedue", Language.getMessage(mstrModuleName, 22, "Balance Due"))
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 23, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtCategory", Language.getMessage(mstrModuleName, 24, "Expense Category : "))
            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 25, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            Call ReportFunction.TextChange(objRpt, "txtRetNo", Language.getMessage(mstrModuleName, 38, "Claim Retirement No."))
            If mblnShowClaimRetirementNumber = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtRetNo", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column201", True)
            End If
            'S.SANDEEP |21-FEB-2022| -- END

            Call ReportFunction.EnableSuppress(objRpt, "txtStatus", Not mblnShowRetirememtFormStatus)
            Call ReportFunction.EnableSuppress(objRpt, "Column81", Not mblnShowRetirememtFormStatus)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportType)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_RetiredReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Function Generate_UnRetiredReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim StrQuery As String = String.Empty
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty


            StrQ = " SELECT " & _
                       " ISNULL(hremployee_master.employeecode,'') AS Code "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            StrQ &= ", ISNULL(hrgrade_master.name,'') AS grade " & _
                         ", ISNULL(cmclaim_request_master.claimrequestno,'') AS ClaimNo " & _
                         ", cmclaim_request_master.transactiondate AS RetireDate " & _
                         ", cmclaim_request_master.expensetypeid AS expensetypeunkid " & _
                         ", cmclaim_request_master.claim_remark AS Description " & _
                         ", CASE WHEN cmclaim_request_master.expensetypeid = 1 THEN @Leave " & _
                         "          WHEN cmclaim_request_master.expensetypeid = 2 THEN @Medical " & _
                         "          WHEN cmclaim_request_master.expensetypeid = 3 THEN @Training " & _
                         "          WHEN cmclaim_request_master.expensetypeid = 4 THEN @Miscellaneous " & _
                         "          WHEN cmclaim_request_master.expensetypeid = 5 THEN @Imprest END AS expensetype " & _
                         ", CASE WHEN cmclaim_retirement_master.statusunkid = 1 THEN ISNULL(ClaimApprTable.ClaimAmt,0) ELSE 0.00 END AS ApprAmount " & _
                         ", CASE WHEN cmclaim_retirement_master.statusunkid = 1 THEN ISNULL(ClaimApprTable.ClaimQty,0) ELSE 0.00 END AS ApprQty " & _
                         ", '' Status " & _
                         ", ClaimApprTable.ClaimAmt AS claim_amount " & _
                         ", 0.00 AS retired_amount " & _
                         ", CASE WHEN ISNULL(cmclaim_retirement_master.imprest_amount,0.00) = 0 THEN  ISNULL(cmclaim_retirement_master.claim_amount,ClaimApprTable.ClaimAmt) ELSE  ISNULL(cmclaim_retirement_master.balance,0.00) END AS balance_due "

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQ &= ",ISNULL(ClaimApprTable.isposted,0) as isposted "
            End If

            StrQ &= ",CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) as transactiondate "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQ &= " ,ClaimApprTable.countryunkid,ClaimApprTable.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            If mblnShowClaimRetirementNumber = True Then StrQ &= ", ISNULL(cmclaim_retirement_master.claimretirementno,'') AS claimretirementno "
            'S.SANDEEP |21-FEB-2022| -- END

            StrQ &= " FROM cmclaim_request_master " & _
                         " LEFT JOIN cmclaim_retirement_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_master.crmasterunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                        " JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "        gradeunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "    FROM prsalaryincrement_tran " & _
                        "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "'" & _
                        " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                        " JOIN hrgrade_master ON hrgrade_master.gradeunkid = Grds.gradeunkid " & _
                        " JOIN ( " & _
                        " SELECT " & _
                        "       crmasterunkid ,ClaimAmt " & _
                        "      ,ClaimQty ,statusunkid " & _
                        "      ,A.ApprName ,ROW_NUMBER() OVER (PARTITION BY crmasterunkid ORDER BY statusunkid, crpriority DESC) AS Rno "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQ &= " ,A.countryunkid,A.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQ &= "           ,isposted "
            End If


            StrQ &= "         FROM " & _
                        "          ( "


            StrQuery = "    SELECT " & _
                             "       cmclaim_approval_tran.crapproverunkid as crapproverunkid " & _
                             "      ,cmclaim_approval_tran.crmasterunkid " & _
                             "      ,SUM(cmclaim_approval_tran.amount) AS ClaimAmt " & _
                             "      ,SUM(cmclaim_approval_tran.quantity) AS ClaimQty " & _
                             "      ,cmclaim_approval_tran.statusunkid " & _
                             "      , #APPR_NAME# AS ApprName " & _
                             "      ,cmapproverlevel_master.crpriority "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= " , cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= "               , cmprocess.isposted "
            End If

            StrQuery &= " FROM cmclaim_approval_tran " & _
                               " LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                               " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                               " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                               " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                               " #EMPL_JOIN#  "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= " JOIN (SELECT cmretire_process_tran.employeeunkid,cmretire_process_tran.expenseunkid,cmretire_process_tran.isposted, cmretire_process_tran.crmasterunkid " & _
                                   "         ,cmretire_process_tran.crapprovaltranunkid " & _
                                   "           FROM cmretire_process_tran " & _
                                   "           LEFT JOIN cfcommon_period_tran  ON cmretire_process_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "           WHERE cmretire_process_tran.isvoid = 0 AND "


                If mintStatusId <= 0 Then
                    StrQuery &= "  (ISNULL(cmretire_process_tran.periodunkid,0) <=0 OR Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "') "
                ElseIf mintStatusId = 1 Then
                    StrQuery &= " Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'"
                ElseIf mintStatusId = 2 Then
                    StrQuery &= " ISNULL(cmretire_process_tran.periodunkid,0) <= 0"
                End If

                StrQuery &= "        ) AS cmprocess ON cmprocess.employeeunkid= cmclaim_request_master.employeeunkid AND cmprocess.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                                   " AND cmprocess.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmprocess.crapprovaltranunkid = cmclaim_approval_tran.crapprovaltranunkid "


            End If


            StrQCondition = " WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.iscancel = 0 AND cmclaim_approval_tran.iscancel = 0 " & _
                                    " AND cmexpense_master.isimprest = 1 AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.expenseunkid = " & mintExpenseID
            End If

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.countryunkid = @countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End

            StrQCondition &= " GROUP BY " & _
                                      " cmclaim_approval_tran.crapproverunkid " & _
                                      " ,cmclaim_approval_tran.crmasterunkid " & _
                                      " ,cmclaim_approval_tran.statusunkid " & _
                                      " ,cmapproverlevel_master.crpriority " & _
                                      " ,#APPR_NAME# "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= ",cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQCondition &= ", cmprocess.isposted "
            End If


            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")

            Dim objExpApproverMaster As New clsExpenseApprover_Master
            Dim dsExternalCompany As DataSet = objExpApproverMaster.GetClaimExternalApproverList(Nothing, "List")
            objExpApproverMaster = Nothing
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsExternalCompany.Tables(0).Rows
                If StrFinalQurey.Trim.Length > 0 Then StrFinalQurey &= " UNION  "
                StrFinalQurey &= StrQuery
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       " ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     " ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       " LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey

            StrQuery = ""
            StrFinalQurey = ""
            StrQCondition = ""

            StrQ &= " UNION "


            StrQuery = " SELECT " & _
                             "   cmclaim_approval_tran.crapproverunkid " & _
                             "  ,cmclaim_approval_tran.crmasterunkid " & _
                             "  ,SUM(cmclaim_approval_tran.amount) AS ClaimAmt " & _
                             "  ,SUM(cmclaim_approval_tran.quantity) AS ClaimQty " & _
                             "  ,cmclaim_approval_tran.statusunkid " & _
                             "  , #APPR_NAME# AS ApprName " & _
                             "  ,cmapproverlevel_master.crpriority "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= " , cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= ",cmprocess.isposted "
            End If

            StrQuery &= " FROM cmclaim_approval_tran " & _
                               " LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                               " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                               " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                               " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                               " #EMPL_JOIN#  "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQuery &= "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid AND cfexchange_rate.countryunkid = cmclaim_approval_tran.countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQuery &= " JOIN (SELECT cmretire_process_tran.employeeunkid,cmretire_process_tran.expenseunkid,cmretire_process_tran.isposted " & _
                                   "                    , cmretire_process_tran.crmasterunkid ,cmretire_process_tran.crapprovaltranunkid " & _
                                   "           FROM cmretire_process_tran " & _
                                   "           LEFT JOIN cfcommon_period_tran  ON cmretire_process_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "           WHERE cmretire_process_tran.isvoid = 0 AND "

                If mintStatusId <= 0 Then
                    StrQuery &= " (ISNULL(cmretire_process_tran.periodunkid,0) <=0 OR Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND 	 Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "') "
                ElseIf mintStatusId = 1 Then
                    StrQuery &= " Convert(char(8),cfcommon_period_tran.start_date,112)  >= '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' AND Convert(char(8),cfcommon_period_tran.end_date,112) <=  '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'"
                ElseIf mintStatusId = 2 Then
                    StrQuery &= " ISNULL(cmretire_process_tran.periodunkid,0) <=0  "
                End If
                StrQuery &= "        ) AS cmprocess ON cmprocess.employeeunkid= cmclaim_request_master.employeeunkid AND cmprocess.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                                   " AND cmprocess.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmprocess.crapprovaltranunkid = cmclaim_approval_tran.crapprovaltranunkid "
            End If

            StrQCondition = " WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.iscancel  =1 " & _
                                    " AND cmexpense_master.isimprest = 1 AND cmexpapprover_master.isexternalapprover = #isExternal#  "


            If mintExpenseID > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.expenseunkid = " & mintExpenseID
            End If

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= " AND cmclaim_approval_tran.countryunkid = @countryunkid "
            End If
            'Pinkal (30-Mar-2021) -- End


            StrQCondition &= "GROUP BY " & _
                                    " cmclaim_approval_tran.crapproverunkid " & _
                                    ",cmclaim_approval_tran.crmasterunkid " & _
                                    ",cmclaim_approval_tran.statusunkid " & _
                                    ",cmapproverlevel_master.crpriority " & _
                                    ",#APPR_NAME# "

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            If mintCurrencyId > 0 Then
                StrQCondition &= ",cmclaim_approval_tran.countryunkid,cfexchange_rate.currency_name "
            End If
            'Pinkal (30-Mar-2021) -- End

            If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                StrQCondition &= ", cmprocess.isposted "
            End If

            StrQuery &= StrQCondition

            StrFinalQurey = StrQuery
            StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", IIf(mblnFirstNamethenSurname, "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", " ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "))
            StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
            StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "0")

            For Each dr In dsExternalCompany.Tables(0).Rows
                If StrFinalQurey.Trim.Length > 0 Then StrFinalQurey &= " UNION  "
                StrFinalQurey &= StrQuery
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", "")
                Else

                    If mblnFirstNamethenSurname Then
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                       " ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    Else
                        StrFinalQurey = StrFinalQurey.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  = '' THEN ISNULL(cfuser_master.username,'') " & _
                                                     " ELSE ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') END ")
                    End If

                    StrFinalQurey = StrFinalQurey.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                     " LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    StrFinalQurey = StrFinalQurey.Replace("#DB_Name#", dr("DName") & "..")

                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", dr("DName") & "..")
                Else
                    StrFinalQurey = StrFinalQurey.Replace("#DName#", "")
                End If
                StrFinalQurey = StrFinalQurey.Replace("#isExternal#", "1")

            Next

            StrQ &= StrFinalQurey

            StrQ &= " )  AS A  " & _
                       " ) AS ClaimApprTable ON ClaimApprTable.crmasterunkid = cmclaim_request_master.crmasterunkid AND ClaimApprTable.Rno = 1 "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND ISNULL(cmclaim_retirement_master.claimretirementunkid,0) <= 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            Call FilterUnRetiredTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@NotRetired", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Not Retired"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Code")
                rpt_Rows.Item("Column2") = dtRow.Item("employee")
                'rpt_Rows.Item("Column3") = dtRow.Item("grade")
                rpt_Rows.Item("Column3") = CDate(dtRow.Item("RetireDate")).ToShortDateString()
                rpt_Rows.Item("Column4") = dtRow.Item("ClaimNo")
                rpt_Rows.Item("Column5") = Format(dtRow.Item("claim_amount"), GUI.fmtCurrency)
                rpt_Rows.Item("Column6") = Format(dtRow.Item("retired_amount"), GUI.fmtCurrency)
                rpt_Rows.Item("Column7") = Format(dtRow.Item("balance_due"), GUI.fmtCurrency)

                If mdtPeriodStartDate <> Nothing AndAlso mdtPeriodEndDate <> Nothing Then
                    rpt_Rows.Item("Column8") = IIf(CBool(dtRow.Item("isposted")), Language.getMessage(mstrModuleName, 31, "Posted"), Language.getMessage(mstrModuleName, 32, "Not Posted"))
                End If

                rpt_Rows.Item("Column9") = dtRow.Item("expensetype").ToString()

                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column13") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)

                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column15") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)

                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")) & " AND Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column16") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "Id = " & CInt(dtRow.Item("Id")))), 0, dsList.Tables(0).Compute("SUM(balance_due)", "Id = " & CInt(dtRow.Item("Id")))), GUI.fmtCurrency)

                Else
                    rpt_Rows.Item("Column10") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column11") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), GUI.fmtCurrency)
                    rpt_Rows.Item("Column12") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), 0, dsList.Tables(0).Compute("SUM(balance_due)", "expensetypeunkid = " & CInt(dtRow.Item("expensetypeunkid")))), GUI.fmtCurrency)
                End If

                rpt_Rows.Item("Column17") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(claim_amount)", "1=1")), 0, dsList.Tables(0).Compute("SUM(claim_amount)", "1=1")), GUI.fmtCurrency)
                rpt_Rows.Item("Column18") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(retired_amount)", "1=1")), 0, dsList.Tables(0).Compute("SUM(retired_amount)", "1=1")), GUI.fmtCurrency)
                rpt_Rows.Item("Column19") = Format(IIf(IsDBNull(dsList.Tables(0).Compute("SUM(balance_due)", "1=1")), 0, dsList.Tables(0).Compute("SUM(balance_due)", "1=1")), GUI.fmtCurrency)

                'S.SANDEEP |21-FEB-2022| -- START
                'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
                If mblnShowClaimRetirementNumber = True Then rpt_Rows.Item("Column20") = dtRow.Item("claimretirementno")
                'S.SANDEEP |21-FEB-2022| -- END

                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column14") = dtRow.Item("GName").ToString()
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptImprestBalanceReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If mintViewIndex <= 0 Then
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            Else
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationGroupTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 36, " Total :"))
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            Call ReportFunction.EnableSuppress(objRpt, "txtStatus", Not mblnShowRetirememtFormStatus)
            Call ReportFunction.EnableSuppress(objRpt, "Column81", Not mblnShowRetirememtFormStatus)

            If mintReportTypeId = 1 Then
                Call ReportFunction.EnableSuppress(objRpt, "txtRetireAmt", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column61", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column111", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column151", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column181", True)

                If mdtPeriodStartDate = Nothing AndAlso mdtPeriodEndDate = Nothing Then
                    Call ReportFunction.EnableSuppress(objRpt, "txtStatus", True)
                    Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
                End If

            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 16, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 17, "Employee"))
            'Call ReportFunction.TextChange(objRpt, "txtSalaryGrade", Language.getMessage(mstrModuleName, 18, "Salary Grade"))
            Call ReportFunction.TextChange(objRpt, "txtSalaryGrade", Language.getMessage(mstrModuleName, 18, "Transaction Date"))
            Call ReportFunction.TextChange(objRpt, "txtClaimNo", Language.getMessage(mstrModuleName, 19, "Claim No."))
            Call ReportFunction.TextChange(objRpt, "txtClaimAppAmt", Language.getMessage(mstrModuleName, 20, "Approved Claim"))
            Call ReportFunction.TextChange(objRpt, "txtRetireAmt", Language.getMessage(mstrModuleName, 21, "Retired Amount"))
            Call ReportFunction.TextChange(objRpt, "txtBalancedue", Language.getMessage(mstrModuleName, 22, "Balance Due"))

            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 23, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtCategory", Language.getMessage(mstrModuleName, 24, "Expense Category : "))
            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 25, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))

            'S.SANDEEP |21-FEB-2022| -- START
            'ISSUE : OLD-574 [KBC - Imprest Retired Balance Report to show Retirement]
            Call ReportFunction.TextChange(objRpt, "txtRetNo", Language.getMessage(mstrModuleName, 38, "Claim Retirement No."))
            If mblnShowClaimRetirementNumber = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtRetNo", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column201", True)
            End If
            'S.SANDEEP |21-FEB-2022| -- END

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportType)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_UnRetiredReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Date :")
			Language.setMessage(mstrModuleName, 2, "Expense Category :")
			Language.setMessage(mstrModuleName, 3, "Employee :")
			Language.setMessage(mstrModuleName, 4, "Status :")
			Language.setMessage(mstrModuleName, 5, " Order By :")
			Language.setMessage(mstrModuleName, 6, "Claim No")
			Language.setMessage(mstrModuleName, 7, "Employee Name")
			Language.setMessage(mstrModuleName, 8, "Transcation Date")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
			Language.setMessage(mstrModuleName, 11, "Checked By :")
			Language.setMessage(mstrModuleName, 12, "Approved By :")
			Language.setMessage(mstrModuleName, 13, "Received By :")
			Language.setMessage(mstrModuleName, 14, "Printed By :")
			Language.setMessage(mstrModuleName, 15, "Printed Date :")
			Language.setMessage(mstrModuleName, 16, "Code")
			Language.setMessage(mstrModuleName, 17, "Employee")
			Language.setMessage(mstrModuleName, 18, "Transaction Date")
			Language.setMessage(mstrModuleName, 19, "Claim No.")
			Language.setMessage(mstrModuleName, 20, "Approved Claim")
			Language.setMessage(mstrModuleName, 21, "Retired Amount")
			Language.setMessage(mstrModuleName, 22, "Balance Due")
			Language.setMessage(mstrModuleName, 23, "Status")
			Language.setMessage(mstrModuleName, 24, "Expense Category :")
			Language.setMessage(mstrModuleName, 25, "Group Total :")
			Language.setMessage(mstrModuleName, 26, "Grand Total :")
			Language.setMessage(mstrModuleName, 27, "Employee Code")
			Language.setMessage(mstrModuleName, 31, "Posted")
			Language.setMessage(mstrModuleName, 32, "Not Posted")
			Language.setMessage(mstrModuleName, 33, "Posted Period From :")
			Language.setMessage(mstrModuleName, 34, "Posted Period To :")
			Language.setMessage(mstrModuleName, 35, "Expense :")
			Language.setMessage(mstrModuleName, 36, " Total :")
            Language.setMessage(mstrModuleName, 37, "Not Retired")
            Language.setMessage(mstrModuleName, 38, "Claim Retirement No.")

			Language.setMessage("clsMasterData", 110, "Approved")
			Language.setMessage("clsMasterData", 111, "Pending")
			Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")

			Language.setMessage("clsExpCommonMethods", 2, "Leave")
			Language.setMessage("clsExpCommonMethods", 3, "Medical")
			Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
            Language.setMessage("clsExpCommonMethods", 9, "Imprest")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
