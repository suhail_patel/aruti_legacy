﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSalaryReconciliationReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployeeName = New System.Windows.Forms.ComboBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.objbtnSearchReason = New eZee.Common.eZeeGradientButton
        Me.lblReason = New System.Windows.Forms.Label
        Me.lblDateFrom = New System.Windows.Forms.Label
        Me.lblDateTo = New System.Windows.Forms.Label
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker
        Me.txtNewScaleTo = New eZee.TextBox.NumericTextBox
        Me.txtIncrementTo = New eZee.TextBox.NumericTextBox
        Me.txtNewScale = New eZee.TextBox.NumericTextBox
        Me.txtIncrement = New eZee.TextBox.NumericTextBox
        Me.txtAmountTo = New eZee.TextBox.NumericTextBox
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.lblNewScaleTo = New System.Windows.Forms.Label
        Me.lblNewScale = New System.Windows.Forms.Label
        Me.lblIncrementTo = New System.Windows.Forms.Label
        Me.lblIncrement = New System.Windows.Forms.Label
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.lblAmountTo = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.gbSortBy.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 458)
        Me.NavPanel.Size = New System.Drawing.Size(567, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 165)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 61
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(448, 63)
        Me.gbSortBy.TabIndex = 3
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(94, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(322, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(422, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(80, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboToPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblToPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboFromPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblFromPeriod)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAnalysisBy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeeName)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(448, 93)
        Me.EZeeCollapsibleContainer1.TabIndex = 2
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(288, 59)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(128, 21)
        Me.cboToPeriod.TabIndex = 2
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(229, 63)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(53, 13)
        Me.lblToPeriod.TabIndex = 255
        Me.lblToPeriod.Text = "To Period"
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(94, 59)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboFromPeriod.TabIndex = 1
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(80, 13)
        Me.lblFromPeriod.TabIndex = 253
        Me.lblFromPeriod.Text = "From Period"
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(347, 5)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 0
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(422, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployeeName
        '
        Me.cboEmployeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeName.DropDownWidth = 150
        Me.cboEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeName.FormattingEnabled = True
        Me.cboEmployeeName.Location = New System.Drawing.Point(94, 32)
        Me.cboEmployeeName.Name = "cboEmployeeName"
        Me.cboEmployeeName.Size = New System.Drawing.Size(322, 21)
        Me.cboEmployeeName.TabIndex = 0
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(80, 13)
        Me.lblEmployeeName.TabIndex = 1
        Me.lblEmployeeName.Text = "Emp. Name"
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(95, 345)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(322, 21)
        Me.cboReason.TabIndex = 11
        Me.cboReason.Visible = False
        '
        'objbtnSearchReason
        '
        Me.objbtnSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReason.BorderSelected = False
        Me.objbtnSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReason.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReason.Location = New System.Drawing.Point(421, 345)
        Me.objbtnSearchReason.Name = "objbtnSearchReason"
        Me.objbtnSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReason.TabIndex = 249
        Me.objbtnSearchReason.Visible = False
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(9, 349)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(80, 13)
        Me.lblReason.TabIndex = 248
        Me.lblReason.Text = "Reason"
        Me.lblReason.Visible = False
        '
        'lblDateFrom
        '
        Me.lblDateFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(9, 244)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(80, 13)
        Me.lblDateFrom.TabIndex = 245
        Me.lblDateFrom.Text = "Date From"
        Me.lblDateFrom.Visible = False
        '
        'lblDateTo
        '
        Me.lblDateTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(227, 243)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(56, 13)
        Me.lblDateTo.TabIndex = 244
        Me.lblDateTo.Text = "To"
        Me.lblDateTo.Visible = False
        '
        'dtpDateTo
        '
        Me.dtpDateTo.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateTo.Checked = False
        Me.dtpDateTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateTo.Location = New System.Drawing.Point(289, 240)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.ShowCheckBox = True
        Me.dtpDateTo.Size = New System.Drawing.Size(128, 20)
        Me.dtpDateTo.TabIndex = 4
        Me.dtpDateTo.Visible = False
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Checked = False
        Me.dtpDateFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateFrom.Location = New System.Drawing.Point(95, 240)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.ShowCheckBox = True
        Me.dtpDateFrom.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateFrom.TabIndex = 3
        Me.dtpDateFrom.Visible = False
        '
        'txtNewScaleTo
        '
        Me.txtNewScaleTo.AllowNegative = True
        Me.txtNewScaleTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNewScaleTo.DigitsInGroup = 0
        Me.txtNewScaleTo.Flags = 0
        Me.txtNewScaleTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewScaleTo.Location = New System.Drawing.Point(289, 319)
        Me.txtNewScaleTo.MaxDecimalPlaces = 4
        Me.txtNewScaleTo.MaxWholeDigits = 21
        Me.txtNewScaleTo.Name = "txtNewScaleTo"
        Me.txtNewScaleTo.Prefix = ""
        Me.txtNewScaleTo.RangeMax = 1.7976931348623157E+308
        Me.txtNewScaleTo.RangeMin = -1.7976931348623157E+308
        Me.txtNewScaleTo.Size = New System.Drawing.Size(128, 20)
        Me.txtNewScaleTo.TabIndex = 10
        Me.txtNewScaleTo.Text = "0"
        Me.txtNewScaleTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNewScaleTo.Visible = False
        '
        'txtIncrementTo
        '
        Me.txtIncrementTo.AllowNegative = True
        Me.txtIncrementTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIncrementTo.DigitsInGroup = 0
        Me.txtIncrementTo.Flags = 0
        Me.txtIncrementTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncrementTo.Location = New System.Drawing.Point(289, 293)
        Me.txtIncrementTo.MaxDecimalPlaces = 4
        Me.txtIncrementTo.MaxWholeDigits = 21
        Me.txtIncrementTo.Name = "txtIncrementTo"
        Me.txtIncrementTo.Prefix = ""
        Me.txtIncrementTo.RangeMax = 1.7976931348623157E+308
        Me.txtIncrementTo.RangeMin = -1.7976931348623157E+308
        Me.txtIncrementTo.Size = New System.Drawing.Size(128, 20)
        Me.txtIncrementTo.TabIndex = 8
        Me.txtIncrementTo.Text = "0"
        Me.txtIncrementTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIncrementTo.Visible = False
        '
        'txtNewScale
        '
        Me.txtNewScale.AllowNegative = True
        Me.txtNewScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNewScale.DigitsInGroup = 0
        Me.txtNewScale.Flags = 0
        Me.txtNewScale.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewScale.Location = New System.Drawing.Point(95, 318)
        Me.txtNewScale.MaxDecimalPlaces = 4
        Me.txtNewScale.MaxWholeDigits = 21
        Me.txtNewScale.Name = "txtNewScale"
        Me.txtNewScale.Prefix = ""
        Me.txtNewScale.RangeMax = 1.7976931348623157E+308
        Me.txtNewScale.RangeMin = -1.7976931348623157E+308
        Me.txtNewScale.Size = New System.Drawing.Size(126, 20)
        Me.txtNewScale.TabIndex = 9
        Me.txtNewScale.Text = "0"
        Me.txtNewScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNewScale.Visible = False
        '
        'txtIncrement
        '
        Me.txtIncrement.AllowNegative = True
        Me.txtIncrement.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIncrement.DigitsInGroup = 0
        Me.txtIncrement.Flags = 0
        Me.txtIncrement.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncrement.Location = New System.Drawing.Point(95, 292)
        Me.txtIncrement.MaxDecimalPlaces = 4
        Me.txtIncrement.MaxWholeDigits = 21
        Me.txtIncrement.Name = "txtIncrement"
        Me.txtIncrement.Prefix = ""
        Me.txtIncrement.RangeMax = 1.7976931348623157E+308
        Me.txtIncrement.RangeMin = -1.7976931348623157E+308
        Me.txtIncrement.Size = New System.Drawing.Size(126, 20)
        Me.txtIncrement.TabIndex = 7
        Me.txtIncrement.Text = "0"
        Me.txtIncrement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIncrement.Visible = False
        '
        'txtAmountTo
        '
        Me.txtAmountTo.AllowNegative = True
        Me.txtAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountTo.DigitsInGroup = 0
        Me.txtAmountTo.Flags = 0
        Me.txtAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(289, 267)
        Me.txtAmountTo.MaxDecimalPlaces = 4
        Me.txtAmountTo.MaxWholeDigits = 21
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Prefix = ""
        Me.txtAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtAmountTo.Size = New System.Drawing.Size(128, 20)
        Me.txtAmountTo.TabIndex = 6
        Me.txtAmountTo.Text = "0"
        Me.txtAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmountTo.Visible = False
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(95, 266)
        Me.txtAmount.MaxDecimalPlaces = 4
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(126, 20)
        Me.txtAmount.TabIndex = 5
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmount.Visible = False
        '
        'lblNewScaleTo
        '
        Me.lblNewScaleTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewScaleTo.Location = New System.Drawing.Point(227, 322)
        Me.lblNewScaleTo.Name = "lblNewScaleTo"
        Me.lblNewScaleTo.Size = New System.Drawing.Size(56, 13)
        Me.lblNewScaleTo.TabIndex = 14
        Me.lblNewScaleTo.Text = "To"
        Me.lblNewScaleTo.Visible = False
        '
        'lblNewScale
        '
        Me.lblNewScale.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewScale.Location = New System.Drawing.Point(9, 322)
        Me.lblNewScale.Name = "lblNewScale"
        Me.lblNewScale.Size = New System.Drawing.Size(80, 13)
        Me.lblNewScale.TabIndex = 12
        Me.lblNewScale.Text = "New Scale"
        Me.lblNewScale.Visible = False
        '
        'lblIncrementTo
        '
        Me.lblIncrementTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementTo.Location = New System.Drawing.Point(227, 296)
        Me.lblIncrementTo.Name = "lblIncrementTo"
        Me.lblIncrementTo.Size = New System.Drawing.Size(56, 13)
        Me.lblIncrementTo.TabIndex = 10
        Me.lblIncrementTo.Text = "To"
        Me.lblIncrementTo.Visible = False
        '
        'lblIncrement
        '
        Me.lblIncrement.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrement.Location = New System.Drawing.Point(9, 296)
        Me.lblIncrement.Name = "lblIncrement"
        Me.lblIncrement.Size = New System.Drawing.Size(80, 13)
        Me.lblIncrement.TabIndex = 8
        Me.lblIncrement.Text = "Increment"
        Me.lblIncrement.Visible = False
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(95, 374)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(322, 17)
        Me.chkIncludeInactiveEmp.TabIndex = 12
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        Me.chkIncludeInactiveEmp.Visible = False
        '
        'lblAmountTo
        '
        Me.lblAmountTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountTo.Location = New System.Drawing.Point(227, 270)
        Me.lblAmountTo.Name = "lblAmountTo"
        Me.lblAmountTo.Size = New System.Drawing.Size(56, 13)
        Me.lblAmountTo.TabIndex = 6
        Me.lblAmountTo.Text = "To"
        Me.lblAmountTo.Visible = False
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(9, 270)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(80, 13)
        Me.lblAmount.TabIndex = 4
        Me.lblAmount.Text = "Current Salary"
        Me.lblAmount.Visible = False
        '
        'frmSalaryReconciliationReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(567, 513)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Controls.Add(Me.cboReason)
        Me.Controls.Add(Me.lblDateFrom)
        Me.Controls.Add(Me.objbtnSearchReason)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.lblReason)
        Me.Controls.Add(Me.lblAmountTo)
        Me.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.Controls.Add(Me.lblDateTo)
        Me.Controls.Add(Me.lblIncrement)
        Me.Controls.Add(Me.dtpDateTo)
        Me.Controls.Add(Me.lblIncrementTo)
        Me.Controls.Add(Me.dtpDateFrom)
        Me.Controls.Add(Me.lblNewScale)
        Me.Controls.Add(Me.txtNewScaleTo)
        Me.Controls.Add(Me.lblNewScaleTo)
        Me.Controls.Add(Me.txtIncrementTo)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.txtNewScale)
        Me.Controls.Add(Me.txtAmountTo)
        Me.Controls.Add(Me.txtIncrement)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmSalaryReconciliationReport"
        Me.Text = "frmSalaryReconciliationReport"
        Me.Controls.SetChildIndex(Me.txtIncrement, 0)
        Me.Controls.SetChildIndex(Me.txtAmountTo, 0)
        Me.Controls.SetChildIndex(Me.txtNewScale, 0)
        Me.Controls.SetChildIndex(Me.txtAmount, 0)
        Me.Controls.SetChildIndex(Me.txtIncrementTo, 0)
        Me.Controls.SetChildIndex(Me.lblNewScaleTo, 0)
        Me.Controls.SetChildIndex(Me.txtNewScaleTo, 0)
        Me.Controls.SetChildIndex(Me.lblNewScale, 0)
        Me.Controls.SetChildIndex(Me.dtpDateFrom, 0)
        Me.Controls.SetChildIndex(Me.lblIncrementTo, 0)
        Me.Controls.SetChildIndex(Me.dtpDateTo, 0)
        Me.Controls.SetChildIndex(Me.lblIncrement, 0)
        Me.Controls.SetChildIndex(Me.lblDateTo, 0)
        Me.Controls.SetChildIndex(Me.chkIncludeInactiveEmp, 0)
        Me.Controls.SetChildIndex(Me.lblAmountTo, 0)
        Me.Controls.SetChildIndex(Me.lblReason, 0)
        Me.Controls.SetChildIndex(Me.lblAmount, 0)
        Me.Controls.SetChildIndex(Me.objbtnSearchReason, 0)
        Me.Controls.SetChildIndex(Me.lblDateFrom, 0)
        Me.Controls.SetChildIndex(Me.cboReason, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.EZeeCollapsibleContainer1, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNewScaleTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtIncrementTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtNewScale As eZee.TextBox.NumericTextBox
    Friend WithEvents txtIncrement As eZee.TextBox.NumericTextBox
    Friend WithEvents txtAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNewScaleTo As System.Windows.Forms.Label
    Friend WithEvents lblNewScale As System.Windows.Forms.Label
    Friend WithEvents lblIncrementTo As System.Windows.Forms.Label
    Friend WithEvents lblIncrement As System.Windows.Forms.Label
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents lblAmountTo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
End Class
