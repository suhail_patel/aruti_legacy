'************************************************************************************************************************************
'Class Name : clsDetailedSalaryBreakdownByCostCenterGroupReport.vb
'Purpose    :
'Date       :18/02/2017
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter


''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsDetailedSalaryBreakdownByCostCenterGroupReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDetailedSalaryBreakdownByCostCenterGroupReport"
    Private mstrReportId As String = enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrPeriodIdList As String

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""

    Dim StrFinalPath As String = String.Empty
    Private mblnIsActive As Boolean = True
    Private mintCCenterId As Integer = -1
    Private mstrCCenterName As String = String.Empty
    Private mintCCGroupId As Integer = -1
    Private mstrCCGroupName As String = String.Empty
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnIgnorezeroHeads As Boolean = False
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurrency_Rate As String = String.Empty
    Private mstrUnSelectedHeadIDs As String = String.Empty
    'Sohail (10 Jun 2020) -- Start
    'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
    Private mblnShowGroupByCostCenterGroup As Boolean = True
    'Sohail (10 Jun 2020) -- End


    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrCurrentDatabaseName As String = FinancialYear._Object._DatabaseName
    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mblnSetPayslipPaymentApproval As Boolean = ConfigParameter._Object._SetPayslipPaymentApproval
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mstrUserAccessFilter As String = ""


    Private marrDatabaseName As New ArrayList

    Private mblnIncludeEmployerContribution As Boolean = False

    Private mDicDetailedSalaryBreakdown As Dictionary(Of Integer, String)
    Private mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIdList() As String
        Set(ByVal value As String)
            mstrPeriodIdList = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _CCenterId() As Integer
        Set(ByVal value As Integer)
            mintCCenterId = value
        End Set
    End Property

    Public WriteOnly Property _CCenter_Name() As String
        Set(ByVal value As String)
            mstrCCenterName = value
        End Set
    End Property

    Public WriteOnly Property _CCGroupId() As Integer
        Set(ByVal value As Integer)
            mintCCGroupId = value
        End Set
    End Property

    Public WriteOnly Property _CCGroup_Name() As String
        Set(ByVal value As String)
            mstrCCGroupName = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IgnoreZeroHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnorezeroHeads = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property

    Public WriteOnly Property _UnSelectedHeadIDs() As String
        Set(ByVal value As String)
            mstrUnSelectedHeadIDs = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Rate() As String
        Set(ByVal value As String)
            mstrCurrency_Rate = value
        End Set
    End Property

    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ToDatabaseName() As String
        Set(ByVal value As String)
            mstrToDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _CurrentDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrentDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _SetPayslipPaymentApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnSetPayslipPaymentApproval = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeEmployerContribution() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeEmployerContribution = value
        End Set
    End Property

    Public WriteOnly Property _DetailedSalaryBreakdown() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mDicDetailedSalaryBreakdown = value
        End Set
    End Property

    Public WriteOnly Property _DetailedSalaryBreakdownReportByCCenterGroupHeadsIds() As String
        Set(ByVal value As String)
            mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds = value
        End Set
    End Property

    'Sohail (10 Jun 2020) -- Start
    'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
    Public WriteOnly Property _ShowGroupByCostCenterGroup() As Boolean
        Set(ByVal value As Boolean)
            mblnShowGroupByCostCenterGroup = value
        End Set
    End Property
    'Sohail (10 Jun 2020) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = -1
            mstrPeriodName = ""

            mintReportId = 0
            mstrReportTypeName = ""
            mstrPeriodIdList = ""


            mblnIsActive = True
            'Sohail (10 Jun 2020) -- Start
            'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
            mblnShowGroupByCostCenterGroup = True
            'Sohail (10 Jun 2020) -- End


            mintCCenterId = -1
            mstrCCenterName = ""
            mintCCGroupId = -1
            mstrCCGroupName = ""
            mintBranchId = -1
            mstrBranchName = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""


            mblnIgnorezeroHeads = False
            mstrUnSelectedHeadIDs = ""

            mstrCurrency_Sign = ""
            mdecEx_Rate = 0

            mstrCurrency_Rate = ""

            mstrFromDatabaseName = FinancialYear._Object._DatabaseName
            mstrToDatabaseName = FinancialYear._Object._DatabaseName

            mstrAdvance_Filter = ""

            marrDatabaseName.Clear()
            mblnIncludeEmployerContribution = False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period : ") & " " & mstrPeriodName & " "
            End If

            If mintCCenterId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Cost Center : ") & " " & mstrCCenterName & " "

            End If
            If mintCCGroupId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Cost Center Group ") & " " & mstrCCGroupName & " "
            End If

            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Branch :") & " " & mstrBranchName & " "
            End If


            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Currency :") & " " & mstrCurrency_Sign & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Exchange Rate:") & " " & CDbl(mdecEx_Rate) & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()




        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailedSalaryBreakdownReport(ByVal strDatabaseName As String, _
                                                      ByVal intUserUnkid As Integer, _
                                                      ByVal intYearUnkid As Integer, _
                                                      ByVal intCompanyUnkid As Integer, _
                                                      ByVal strUserModeSetting As String, _
                                                      ByVal blnOnlyApproved As Boolean)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsPayroll As New DataSet
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = "" : xUACQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodStartDate, mdtPeriodEndDate, , 1, strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, strDatabaseName)

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'StrQ = "DECLARE @tbl TABLE " & _
            '            "( " & _
            '              "costcentergroupmasterunkid INT NULL  " & _
            '            ", costcenterunkid INT NULL " & _
            '            ", Total DECIMAL(36, 6) NULL " & _
            '            ", EmpCount DECIMAL(36, 6) NULL "

            'If mDicDetailedSalaryBreakdown.Keys.Count > 0 Then
            '    For Each xKey As Integer In mDicDetailedSalaryBreakdown.Keys
            '        StrQ &= ", [" & mDicDetailedSalaryBreakdown(xKey) & "] DECIMAL(36, 6) NULL "
            '    Next
            'End If

            'StrQ &= " ) " & _
            '        "INSERT  INTO @tbl "
            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                            ", Alloc.stationunkid  " & _
                            "INTO    #TableEmp " & _
                    "FROM  hremployee_master " & _
                     "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,departmentunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "SELECT     prcostcenter_master.costcentergroupmasterunkid  " & _
                             ", ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) AS costcenterunkid " & _
                             ", prpayrollprocess_tran.tranheadunkid " & _
                             ", prpayrollprocess_tran.amount " & _
                             ", prpayrollprocess_tran.employeeunkid " & _
                    " INTO   #payroll " & _
                    " FROM      #TableEmp " & _
                                "LEFT JOIN prpayrollprocess_tran ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = prpayrollprocess_tran.costcenterunkid " & _
                                        "AND prpayrollprocess_tran.allocationbyid = " & enAllocation.COST_CENTER & " " & _
                    "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                        "   AND prtnaleave_tran.isvoid = 0 " & _
                        "   AND payperiodunkid = @PeriodId " & _
                        "   AND tranheadunkid IN ( " & mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds & " ) "

            If mintCCenterId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.costcenterunkid = @costcenterunkid "
            End If

            If mintCCGroupId > 0 Then
                StrQ &= " AND prcostcenter_master.costcentergroupmasterunkid = @costcentergroupmasterunkid "
            End If

            If mintBranchId > 0 Then
                StrQ &= " AND #TableEmp.stationunkid = @BranchId "
            End If
            'Sohail (24 Jun 2020) -- End

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'StrQ &= "SELECT     prcostcenter_master.costcentergroupmasterunkid  " & _
            '                  ", ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) AS costcenterunkid " & _
            '                  ", SUM(amount) AS Total " & _
            '                  ", COUNT(DISTINCT prpayrollprocess_tran.employeeunkid) AS EmpCount "
            StrQ &= "SELECT     #payroll.costcentergroupmasterunkid  " & _
                              ", #payroll.costcenterunkid " & _
                              ", SUM(amount) AS Total " & _
                              ", COUNT(DISTINCT #payroll.employeeunkid) AS EmpCount "
            'Sohail (24 Jun 2020) -- End
            'Sohail (07 Feb 2019) - [prpayrollprocess_tran.costcenterunkid] = [ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) AS costcenterunkid]

            For Each yKey As Integer In mDicDetailedSalaryBreakdown.Keys
                StrQ &= ", CAST(0 AS DECIMAL(36, 6)) AS [" & mDicDetailedSalaryBreakdown(yKey) & "] "
            Next

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'StrQ &= "          FROM      prpayrollprocess_tran " & _
            '                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '                            "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
            '                            "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = prpayrollprocess_tran.costcenterunkid " & _
            '                                    "AND prpayrollprocess_tran.allocationbyid = " & enAllocation.COST_CENTER & " "
            'Sohail (24 Jun 2020) - [LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid]
            StrQ &= " INTO #tbl " & _
                    "FROM      #payroll "
            'Sohail (24 Jun 2020) -- End


            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            '"LEFT JOIN " & _
            '"( " & _
            '"    SELECT " & _
            '"         stationunkid " & _
            '"        ,departmentunkid " & _
            '"        ,employeeunkid " & _
            '"        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '"    FROM hremployee_transfer_tran " & _
            '"    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
            '") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
            '"LEFT JOIN " & _
            '"( " & _
            '"    SELECT " & _
            '"         cctranheadvalueid AS costcenterunkid" & _
            '"        ,employeeunkid " & _
            '"        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '"    FROM hremployee_cctranhead_tran " & _
            '"    WHERE istransactionhead = 0 AND isvoid = 0 " & _
            '"    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
            '") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
            'Sohail (24 Jun 2020) -- End
            'Sohail (07 Feb 2019) - [AND prpayrollprocess_tran.allocationbyid = " & enAllocation.COST_CENTER & "]

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (24 Jun 2020) -- End

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
            '        "   AND prtnaleave_tran.isvoid = 0 " & _
            '        "   AND payperiodunkid = @PeriodId " & _
            '        "   AND tranheadunkid IN ( " & mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds & " ) "
            StrQ &= "WHERE 1 = 1 " & _
                  "   AND tranheadunkid IN ( " & mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds & " ) "
            'Sohail (24 Jun 2020) -- End

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'If mintCCenterId > 0 Then
            '    StrQ &= " AND CC.costcenterunkid = @costcenterunkid "
            'End If

            'If mintCCGroupId > 0 Then
            '    StrQ &= " AND prcostcenter_master.costcentergroupmasterunkid = @costcentergroupmasterunkid "
            'End If

            'If mintBranchId > 0 Then
            '    StrQ &= " AND Alloc.stationunkid = @BranchId "
            'End If
            'Sohail (24 Jun 2020) -- End

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry & " "
            '    End If
            'End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'Sohail (24 Jun 2020) -- End

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            'StrQ &= "          GROUP BY  prcostcenter_master.costcentergroupmasterunkid  " & _
            '                          ", ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) "
            StrQ &= "          GROUP BY  #payroll.costcentergroupmasterunkid  " & _
                                   ", #payroll.costcenterunkid "
            'Sohail (24 Jun 2020) -- End
            'Sohail (07 Feb 2019) - [prpayrollprocess_tran.costcenterunkid] = [ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid)]

            If mDicDetailedSalaryBreakdown.Keys.Count > 0 Then
                For Each xKey As Integer In mDicDetailedSalaryBreakdown.Keys
                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'StrQ &= "UNION ALL " & _
                    '          "SELECT    prcostcenter_master.costcentergroupmasterunkid  " & _
                    '                  ", ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) AS costcenterunkid " & _
                    '                  ", 0 AS Total " & _
                    '                  ", 0 AS EmpCount "
                    StrQ &= "INSERT INTO #tbl " & _
                             "SELECT    #payroll.costcentergroupmasterunkid  " & _
                                     ", #payroll.costcenterunkid " & _
                                     ", 0 AS Total " & _
                                     ", 0 AS EmpCount "
                    'Sohail (24 Jun 2020) -- End
                    'Sohail (07 Feb 2019) - [prpayrollprocess_tran.costcenterunkid] = [ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) AS costcenterunkid]

                    For Each yKey As Integer In mDicDetailedSalaryBreakdown.Keys
                        If xKey = yKey Then
                            StrQ &= ", SUM(amount) AS [" & mDicDetailedSalaryBreakdown(yKey) & "] "
                        Else
                            StrQ &= ", 0 AS [" & mDicDetailedSalaryBreakdown(yKey) & "] "
                        End If
                    Next
                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'StrQ &= "  FROM      prpayrollprocess_tran " & _
                    '                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    '                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                    '                    "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = prpayrollprocess_tran.costcenterunkid " & _
                    '                            "AND prpayrollprocess_tran.allocationbyid = " & enAllocation.COST_CENTER & " "
                    StrQ &= "  FROM      #payroll "
                    'Sohail (24 Jun 2020) -- End

                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    '"LEFT JOIN " & _
                    '"( " & _
                    '"    SELECT " & _
                    '"         stationunkid " & _
                    '"        ,departmentunkid " & _
                    '"        ,employeeunkid " & _
                    '"        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '"    FROM hremployee_transfer_tran " & _
                    '"    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    '"LEFT JOIN " & _
                    '"( " & _
                    '"    SELECT " & _
                    '"         cctranheadvalueid AS costcenterunkid" & _
                    '"        ,employeeunkid " & _
                    '"        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '"    FROM hremployee_cctranhead_tran " & _
                    '"    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    '"    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                    'Sohail (24 Jun 2020) -- End
                    'Sohail (07 Feb 2019) - [AND prpayrollprocess_tran.allocationbyid = " & enAllocation.COST_CENTER & "]

                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    'If xUACQry.Trim.Length > 0 Then
                    '    StrQ &= xUACQry
                    'End If
                    'Sohail (24 Jun 2020) -- End

                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'StrQ &= "WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                    '                    "AND prtnaleave_tran.isvoid = 0 " & _
                    '                    "AND payperiodunkid = @PeriodId " & _
                    '                    "AND tranheadunkid IN ( " & xKey & " ) "
                    StrQ &= "WHERE     1 = 1 " & _
                                       "AND tranheadunkid IN ( " & xKey & " ) "
                    'Sohail (24 Jun 2020) -- End

                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'If mintCCenterId > 0 Then
                    '    StrQ &= " AND CC.costcenterunkid = @costcenterunkid "
                    'End If

                    'If mintCCGroupId > 0 Then
                    '    StrQ &= " AND prcostcenter_master.costcentergroupmasterunkid = @costcentergroupmasterunkid "
                    'End If

                    'If mintBranchId > 0 Then
                    '    StrQ &= " AND Alloc.stationunkid = @BranchId "
                    'End If
                    'Sohail (24 Jun 2020) -- End
                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'If mblnIsActive = False Then
                    '    If xDateFilterQry.Trim.Length > 0 Then
                    '        StrQ &= xDateFilterQry & " "
                    '    End If
                    'End If

                    'If mstrAdvance_Filter.Trim.Length > 0 Then
                    '    StrQ &= " AND " & mstrAdvance_Filter
                    'End If
                    'Sohail (24 Jun 2020) -- End

                    'Sohail (24 Jun 2020) -- Start
                    'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
                    'StrQ &= "  GROUP BY  prcostcenter_master.costcentergroupmasterunkid  " & _
                    '           ", ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid) "
                    StrQ &= "  GROUP BY  #payroll.costcentergroupmasterunkid  " & _
                              ", #payroll.costcenterunkid "
                    'Sohail (24 Jun 2020) -- End
                    'Sohail (07 Feb 2019) - [prpayrollprocess_tran.costcenterunkid] = [ISNULL(prcostcenter_master.costcenterunkid, prtnaleave_tran.costcenterunkid)]
                Next
            End If

            StrQ &= "SELECT  ISNULL(cfpayrollgroup_master.groupname, 'N/A') AS costcentergroupmastername  " & _
                          ", A.costcentergroupmasterunkid " & _
                          ", ISNULL(prcostcenter_master.costcentername, 'N/A') AS costcentername " & _
                          ", A.costcenterunkid " & _
                          ", ISNULL(prcostcenter_master.costcentercode, 'N/A') AS costcentercode "

            If mDicDetailedSalaryBreakdown.Keys.Count > 0 Then
                For Each xKey As Integer In mDicDetailedSalaryBreakdown.Keys
                    StrQ &= ", SUM(A.[" & mDicDetailedSalaryBreakdown(xKey) & "] * " & mdecEx_Rate & ") AS [" & mDicDetailedSalaryBreakdown(xKey) & "] "
                Next
            End If

            StrQ &= "      , SUM(A.Total * " & mdecEx_Rate & ") AS Total " & _
                          ", CAST(SUM(ISNULL(A.EmpCount,0)) AS BIGINT) AS EmpCount "

            StrQ &= "FROM    #tbl AS A " & _
                            "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = A.costcenterunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON cfpayrollgroup_master.groupmasterunkid = prcostcenter_master.costcentergroupmasterunkid " & _
                            "AND cfpayrollgroup_master.isactive = 1 " & _
                            "AND cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.CostCenter & " " & _
                    "GROUP BY A.costcentergroupmasterunkid  " & _
                          ", ISNULL(cfpayrollgroup_master.groupname, 'N/A') " & _
                          ", prcostcenter_master.costcentername " & _
                          ", A.costcenterunkid " & _
                          ", ISNULL(prcostcenter_master.costcentercode, 'N/A') " & _
                    "ORDER BY ISNULL(cfpayrollgroup_master.groupname, 'N/A')  " & _
                          ", prcostcenter_master.costcentername "
            'Sohail (24 Jun 2020) - [@tbl] = [#tbl]

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)


            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintCCenterId > 0 Then
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCCenterId)
            End If

            If mintCCGroupId > 0 Then
                objDataOperation.AddParameter("@costcentergroupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCCGroupId)
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If

            Call FilterTitleAndFilterQuery()

            'Sohail (24 Jun 2020) -- Start
            'NMB Issue # : The query processor ran out of internal resources error when selecting all heads on Detail salary break down report.
            StrQ &= " DROP TABLE #TableEmp " & _
                    " DROP TABLE #tbl " & _
                    " DROP TABLE #payroll "
            'Sohail (24 Jun 2020) -- End

            dsPayroll = objDataOperation.ExecQuery(StrQ, "payroll")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTableExcel = dsPayroll.Tables("payroll")
            mdtTableExcel.Columns.Remove("costcentergroupmasterunkid")
            mdtTableExcel.Columns.Remove("costcenterunkid")

            mdtTableExcel.Columns("costcentergroupmastername").Caption = Language.getMessage(mstrModuleName, 7, "Cost Center Group")
            mdtTableExcel.Columns("costcentername").Caption = Language.getMessage(mstrModuleName, 8, "COST CENTER")
            mdtTableExcel.Columns("costcentercode").Caption = Language.getMessage(mstrModuleName, 8, "Cost Center Code")
            mdtTableExcel.Columns("Total").Caption = Language.getMessage(mstrModuleName, 10, "TOTAL")
            mdtTableExcel.Columns("EmpCount").Caption = Language.getMessage(mstrModuleName, 11, "NO. OF EMPLOYEES")



            Dim strGTotal As String = Language.getMessage(mstrModuleName, 12, "Total")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 13, "Total")
            'Sohail (10 Jun 2020) -- Start
            'NMB Enhancement # : Option to Show Group By Cost Center Group on Detail Salary Break Down Reprot by Cost Center Group to generate report without grouping.
            'Dim strarrGroupColumns As String() = {"costcentergroupmastername", "costcentername"}
            Dim strarrGroupColumns As String() = Nothing
            If mblnShowGroupByCostCenterGroup = True Then
                Dim strGrpCols As String() = {"costcentergroupmastername", "costcentername"}
                strarrGroupColumns = strGrpCols
            End If
            'Sohail (10 Jun 2020) -- End
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, _ReportName & "(" & mstrCurrency_Sign & ")", "", "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing, , , True)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailedSalaryBreakdownReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period :")
            Language.setMessage(mstrModuleName, 2, "Cost Center :")
            Language.setMessage(mstrModuleName, 3, "Cost Center Group")
            Language.setMessage(mstrModuleName, 4, "Branch :")
            Language.setMessage(mstrModuleName, 5, "Currency :")
            Language.setMessage(mstrModuleName, 6, "Exchange Rate:")
            Language.setMessage(mstrModuleName, 7, "Cost Center Group")
            Language.setMessage(mstrModuleName, 8, "COST CENTER")
            Language.setMessage(mstrModuleName, 10, "TOTAL")
            Language.setMessage(mstrModuleName, 11, "NO. OF EMPLOYEES")
            Language.setMessage(mstrModuleName, 12, "Total")
            Language.setMessage(mstrModuleName, 13, "Total")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
