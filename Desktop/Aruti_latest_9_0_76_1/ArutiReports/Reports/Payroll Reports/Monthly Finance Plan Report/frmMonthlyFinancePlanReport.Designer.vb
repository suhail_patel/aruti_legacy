﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMonthlyFinancePlanReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMonthlyFinancePlanReport))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboAllocationByID = New System.Windows.Forms.ComboBox
        Me.lblAllocationByID = New System.Windows.Forms.Label
        Me.lblSearchPeriod = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkPeriodSelectAll = New System.Windows.Forms.CheckBox
        Me.dgPeriod = New System.Windows.Forms.DataGridView
        Me.objdgperiodcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBudgetcodesunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBudget = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodStart = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodEnd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBudgetunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchPeriod = New System.Windows.Forms.TextBox
        Me.cboBudgetFor = New System.Windows.Forms.ComboBox
        Me.lblBudgetFor = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.lblPersonnelCost = New System.Windows.Forms.Label
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboBudget = New System.Windows.Forms.ComboBox
        Me.lblBudget = New System.Windows.Forms.Label
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 573)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(658, 55)
        Me.EZeeFooter1.TabIndex = 42
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(262, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 35
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        Me.objbtnAdvanceFilter.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(365, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(461, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(557, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocationByID)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocationByID)
        Me.gbFilterCriteria.Controls.Add(Me.lblSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.Panel1)
        Me.gbFilterCriteria.Controls.Add(Me.cboBudgetFor)
        Me.gbFilterCriteria.Controls.Add(Me.lblBudgetFor)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonnelCost)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboBudget)
        Me.gbFilterCriteria.Controls.Add(Me.lblBudget)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(472, 400)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocationByID
        '
        Me.cboAllocationByID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocationByID.DropDownWidth = 300
        Me.cboAllocationByID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocationByID.FormattingEnabled = True
        Me.cboAllocationByID.Location = New System.Drawing.Point(105, 87)
        Me.cboAllocationByID.Name = "cboAllocationByID"
        Me.cboAllocationByID.Size = New System.Drawing.Size(242, 21)
        Me.cboAllocationByID.TabIndex = 2
        '
        'lblAllocationByID
        '
        Me.lblAllocationByID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocationByID.Location = New System.Drawing.Point(11, 90)
        Me.lblAllocationByID.Name = "lblAllocationByID"
        Me.lblAllocationByID.Size = New System.Drawing.Size(91, 15)
        Me.lblAllocationByID.TabIndex = 212
        Me.lblAllocationByID.Text = "Allocation By"
        Me.lblAllocationByID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSearchPeriod
        '
        Me.lblSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchPeriod.Location = New System.Drawing.Point(365, 33)
        Me.lblSearchPeriod.Name = "lblSearchPeriod"
        Me.lblSearchPeriod.Size = New System.Drawing.Size(50, 15)
        Me.lblSearchPeriod.TabIndex = 209
        Me.lblSearchPeriod.Text = "Search Period"
        Me.lblSearchPeriod.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkPeriodSelectAll)
        Me.Panel1.Controls.Add(Me.dgPeriod)
        Me.Panel1.Controls.Add(Me.txtSearchPeriod)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(105, 114)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(353, 278)
        Me.Panel1.TabIndex = 207
        '
        'objchkPeriodSelectAll
        '
        Me.objchkPeriodSelectAll.AutoSize = True
        Me.objchkPeriodSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkPeriodSelectAll.Name = "objchkPeriodSelectAll"
        Me.objchkPeriodSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkPeriodSelectAll.TabIndex = 18
        Me.objchkPeriodSelectAll.UseVisualStyleBackColor = True
        '
        'dgPeriod
        '
        Me.dgPeriod.AllowUserToAddRows = False
        Me.dgPeriod.AllowUserToDeleteRows = False
        Me.dgPeriod.AllowUserToResizeRows = False
        Me.dgPeriod.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgPeriod.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgPeriod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgPeriod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgPeriod.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgperiodcolhCheck, Me.objdgcolhPeriodunkid, Me.objdgcolhBudgetcodesunkid, Me.dgColhPeriod, Me.dgcolhBudget, Me.objdgcolhPeriodStart, Me.objdgcolhPeriodEnd, Me.objdgcolhBudgetunkid})
        Me.dgPeriod.Location = New System.Drawing.Point(0, 28)
        Me.dgPeriod.Name = "dgPeriod"
        Me.dgPeriod.RowHeadersVisible = False
        Me.dgPeriod.RowHeadersWidth = 5
        Me.dgPeriod.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgPeriod.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPeriod.Size = New System.Drawing.Size(350, 247)
        Me.dgPeriod.TabIndex = 1
        '
        'objdgperiodcolhCheck
        '
        Me.objdgperiodcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgperiodcolhCheck.HeaderText = ""
        Me.objdgperiodcolhCheck.Name = "objdgperiodcolhCheck"
        Me.objdgperiodcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgperiodcolhCheck.Width = 25
        '
        'objdgcolhPeriodunkid
        '
        Me.objdgcolhPeriodunkid.HeaderText = "periodunkid"
        Me.objdgcolhPeriodunkid.Name = "objdgcolhPeriodunkid"
        Me.objdgcolhPeriodunkid.ReadOnly = True
        Me.objdgcolhPeriodunkid.Visible = False
        '
        'objdgcolhBudgetcodesunkid
        '
        Me.objdgcolhBudgetcodesunkid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhBudgetcodesunkid.HeaderText = "budgetcodesunkid"
        Me.objdgcolhBudgetcodesunkid.Name = "objdgcolhBudgetcodesunkid"
        Me.objdgcolhBudgetcodesunkid.ReadOnly = True
        Me.objdgcolhBudgetcodesunkid.Visible = False
        Me.objdgcolhBudgetcodesunkid.Width = 50
        '
        'dgColhPeriod
        '
        Me.dgColhPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgColhPeriod.HeaderText = "Period Name"
        Me.dgColhPeriod.Name = "dgColhPeriod"
        Me.dgColhPeriod.ReadOnly = True
        Me.dgColhPeriod.Width = 92
        '
        'dgcolhBudget
        '
        Me.dgcolhBudget.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhBudget.HeaderText = "Budget"
        Me.dgcolhBudget.Name = "dgcolhBudget"
        Me.dgcolhBudget.ReadOnly = True
        '
        'objdgcolhPeriodStart
        '
        Me.objdgcolhPeriodStart.HeaderText = "Period Start"
        Me.objdgcolhPeriodStart.Name = "objdgcolhPeriodStart"
        Me.objdgcolhPeriodStart.ReadOnly = True
        Me.objdgcolhPeriodStart.Visible = False
        '
        'objdgcolhPeriodEnd
        '
        Me.objdgcolhPeriodEnd.HeaderText = "Period End"
        Me.objdgcolhPeriodEnd.Name = "objdgcolhPeriodEnd"
        Me.objdgcolhPeriodEnd.ReadOnly = True
        Me.objdgcolhPeriodEnd.Visible = False
        '
        'objdgcolhBudgetunkid
        '
        Me.objdgcolhBudgetunkid.HeaderText = "Budgetunkid"
        Me.objdgcolhBudgetunkid.Name = "objdgcolhBudgetunkid"
        Me.objdgcolhBudgetunkid.ReadOnly = True
        Me.objdgcolhBudgetunkid.Visible = False
        '
        'txtSearchPeriod
        '
        Me.txtSearchPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchPeriod.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchPeriod.Location = New System.Drawing.Point(0, 3)
        Me.txtSearchPeriod.Name = "txtSearchPeriod"
        Me.txtSearchPeriod.Size = New System.Drawing.Size(350, 21)
        Me.txtSearchPeriod.TabIndex = 0
        '
        'cboBudgetFor
        '
        Me.cboBudgetFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudgetFor.DropDownWidth = 300
        Me.cboBudgetFor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudgetFor.FormattingEnabled = True
        Me.cboBudgetFor.Location = New System.Drawing.Point(105, 60)
        Me.cboBudgetFor.Name = "cboBudgetFor"
        Me.cboBudgetFor.Size = New System.Drawing.Size(242, 21)
        Me.cboBudgetFor.TabIndex = 1
        '
        'lblBudgetFor
        '
        Me.lblBudgetFor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetFor.Location = New System.Drawing.Point(11, 63)
        Me.lblBudgetFor.Name = "lblBudgetFor"
        Me.lblBudgetFor.Size = New System.Drawing.Size(91, 15)
        Me.lblBudgetFor.TabIndex = 205
        Me.lblBudgetFor.Text = "Budget For"
        Me.lblBudgetFor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownWidth = 180
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(407, 90)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(35, 21)
        Me.cboTranHead.TabIndex = 3
        Me.cboTranHead.Visible = False
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(448, 90)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 200
        Me.objbtnSearchTranHead.Visible = False
        '
        'lblPersonnelCost
        '
        Me.lblPersonnelCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonnelCost.Location = New System.Drawing.Point(368, 92)
        Me.lblPersonnelCost.Name = "lblPersonnelCost"
        Me.lblPersonnelCost.Size = New System.Drawing.Size(33, 15)
        Me.lblPersonnelCost.TabIndex = 198
        Me.lblPersonnelCost.Text = "Personnel Cost"
        Me.lblPersonnelCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPersonnelCost.Visible = False
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(365, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 196
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 450
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(105, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(242, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(11, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(91, 15)
        Me.lblReportType.TabIndex = 7
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 300
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(405, 75)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(64, 21)
        Me.cboPeriod.TabIndex = 1
        Me.cboPeriod.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(11, 120)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(91, 15)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBudget
        '
        Me.cboBudget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudget.DropDownWidth = 300
        Me.cboBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudget.FormattingEnabled = True
        Me.cboBudget.Location = New System.Drawing.Point(405, 57)
        Me.cboBudget.Name = "cboBudget"
        Me.cboBudget.Size = New System.Drawing.Size(64, 21)
        Me.cboBudget.TabIndex = 0
        Me.cboBudget.Visible = False
        '
        'lblBudget
        '
        Me.lblBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(405, 39)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(53, 15)
        Me.lblBudget.TabIndex = 1
        Me.lblBudget.Text = "Default Budget"
        Me.lblBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBudget.Visible = False
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(424, 36)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 472)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(472, 63)
        Me.gbSortBy.TabIndex = 1
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbSortBy.Visible = False
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(66, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(80, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(338, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(658, 60)
        Me.eZeeHeader.TabIndex = 43
        Me.eZeeHeader.Title = "Monthly Finance Plan Report"
        '
        'frmMonthlyFinancePlanReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 628)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMonthlyFinancePlanReport"
        Me.ShowInTaskbar = False
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgPeriod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboBudget As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPersonnelCost As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboBudgetFor As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudgetFor As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkPeriodSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgPeriod As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchPeriod As System.Windows.Forms.TextBox
    Private WithEvents lblSearchPeriod As System.Windows.Forms.Label
    Friend WithEvents objdgperiodcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBudgetcodesunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBudget As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodStart As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodEnd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBudgetunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboAllocationByID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocationByID As System.Windows.Forms.Label
End Class
