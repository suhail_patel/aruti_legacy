﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaymentJVReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtInvoiceRef = New System.Windows.Forms.TextBox
        Me.lblInvoiceRef = New System.Windows.Forms.Label
        Me.chkShowColumnHeader = New System.Windows.Forms.CheckBox
        Me.lblCustomCCenter = New System.Windows.Forms.Label
        Me.cboCustomCCenter = New System.Windows.Forms.ComboBox
        Me.lnkiScala2JVExport = New System.Windows.Forms.LinkLabel
        Me.lnkSunJV5Export = New System.Windows.Forms.LinkLabel
        Me.gbFilterTBCJV = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvTranHead = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhEContribHeadCode = New System.Windows.Forms.ColumnHeader
        Me.colhEContribHead = New System.Windows.Forms.ColumnHeader
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.lnkTBCJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkiScalaJVExport = New System.Windows.Forms.LinkLabel
        Me.lblExRate = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lnkSunJVExport = New System.Windows.Forms.LinkLabel
        Me.lblCCGroup = New System.Windows.Forms.Label
        Me.cboCCGroup = New System.Windows.Forms.ComboBox
        Me.chkShowGroupByCCGroup = New System.Windows.Forms.CheckBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.chkShowSummaryNewPage = New System.Windows.Forms.CheckBox
        Me.chkShowSummaryBottom = New System.Windows.Forms.CheckBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.lblCreditAMountTo = New System.Windows.Forms.Label
        Me.txtCreditAMountTo = New eZee.TextBox.NumericTextBox
        Me.lblCreditAMountFrom = New System.Windows.Forms.Label
        Me.txtCreditAMountFrom = New eZee.TextBox.NumericTextBox
        Me.lblDebitAmountTo = New System.Windows.Forms.Label
        Me.txtDebitAmountTo = New eZee.TextBox.NumericTextBox
        Me.lblDebitAmountFrom = New System.Windows.Forms.Label
        Me.txtDebitAmountFrom = New eZee.TextBox.NumericTextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbFilterTBCJV.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 503)
        Me.NavPanel.Size = New System.Drawing.Size(691, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 387)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(646, 61)
        Me.gbSortBy.TabIndex = 3
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(384, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 1
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(82, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(96, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(282, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtInvoiceRef)
        Me.gbFilterCriteria.Controls.Add(Me.lblInvoiceRef)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowColumnHeader)
        Me.gbFilterCriteria.Controls.Add(Me.lblCustomCCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboCustomCCenter)
        Me.gbFilterCriteria.Controls.Add(Me.lnkiScala2JVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSunJV5Export)
        Me.gbFilterCriteria.Controls.Add(Me.gbFilterTBCJV)
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnoreZero)
        Me.gbFilterCriteria.Controls.Add(Me.lnkTBCJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkiScalaJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lblExRate)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSunJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lblCCGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboCCGroup)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowGroupByCCGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowSummaryNewPage)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowSummaryBottom)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCreditAMountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtCreditAMountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCreditAMountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtCreditAMountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblDebitAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtDebitAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDebitAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtDebitAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(646, 315)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInvoiceRef
        '
        Me.txtInvoiceRef.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceRef.Location = New System.Drawing.Point(408, 195)
        Me.txtInvoiceRef.Name = "txtInvoiceRef"
        Me.txtInvoiceRef.Size = New System.Drawing.Size(153, 21)
        Me.txtInvoiceRef.TabIndex = 10
        Me.txtInvoiceRef.Visible = False
        '
        'lblInvoiceRef
        '
        Me.lblInvoiceRef.BackColor = System.Drawing.Color.Transparent
        Me.lblInvoiceRef.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvoiceRef.Location = New System.Drawing.Point(295, 197)
        Me.lblInvoiceRef.Name = "lblInvoiceRef"
        Me.lblInvoiceRef.Size = New System.Drawing.Size(107, 16)
        Me.lblInvoiceRef.TabIndex = 245
        Me.lblInvoiceRef.Text = "Invoice Reference"
        Me.lblInvoiceRef.Visible = False
        '
        'chkShowColumnHeader
        '
        Me.chkShowColumnHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowColumnHeader.Location = New System.Drawing.Point(408, 224)
        Me.chkShowColumnHeader.Name = "chkShowColumnHeader"
        Me.chkShowColumnHeader.Size = New System.Drawing.Size(223, 16)
        Me.chkShowColumnHeader.TabIndex = 12
        Me.chkShowColumnHeader.Text = "Show Column Header"
        Me.chkShowColumnHeader.UseVisualStyleBackColor = True
        '
        'lblCustomCCenter
        '
        Me.lblCustomCCenter.BackColor = System.Drawing.Color.Transparent
        Me.lblCustomCCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomCCenter.Location = New System.Drawing.Point(8, 198)
        Me.lblCustomCCenter.Name = "lblCustomCCenter"
        Me.lblCustomCCenter.Size = New System.Drawing.Size(107, 16)
        Me.lblCustomCCenter.TabIndex = 244
        Me.lblCustomCCenter.Text = "Custom C.Center"
        Me.lblCustomCCenter.Visible = False
        '
        'cboCustomCCenter
        '
        Me.cboCustomCCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomCCenter.DropDownWidth = 120
        Me.cboCustomCCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomCCenter.FormattingEnabled = True
        Me.cboCustomCCenter.Location = New System.Drawing.Point(121, 196)
        Me.cboCustomCCenter.Name = "cboCustomCCenter"
        Me.cboCustomCCenter.Size = New System.Drawing.Size(153, 21)
        Me.cboCustomCCenter.TabIndex = 9
        Me.cboCustomCCenter.Visible = False
        '
        'lnkiScala2JVExport
        '
        Me.lnkiScala2JVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkiScala2JVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkiScala2JVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkiScala2JVExport.Location = New System.Drawing.Point(8, 288)
        Me.lnkiScala2JVExport.Name = "lnkiScala2JVExport"
        Me.lnkiScala2JVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkiScala2JVExport.TabIndex = 241
        Me.lnkiScala2JVExport.TabStop = True
        Me.lnkiScala2JVExport.Text = "&iScala2 JV Export..."
        Me.lnkiScala2JVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkiScala2JVExport.Visible = False
        '
        'lnkSunJV5Export
        '
        Me.lnkSunJV5Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkSunJV5Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSunJV5Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSunJV5Export.Location = New System.Drawing.Point(8, 267)
        Me.lnkSunJV5Export.Name = "lnkSunJV5Export"
        Me.lnkSunJV5Export.Size = New System.Drawing.Size(61, 16)
        Me.lnkSunJV5Export.TabIndex = 239
        Me.lnkSunJV5Export.TabStop = True
        Me.lnkSunJV5Export.Text = "&Sun JV 5 Export..."
        Me.lnkSunJV5Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkSunJV5Export.Visible = False
        '
        'gbFilterTBCJV
        '
        Me.gbFilterTBCJV.BorderColor = System.Drawing.Color.Black
        Me.gbFilterTBCJV.Checked = False
        Me.gbFilterTBCJV.CollapseAllExceptThis = False
        Me.gbFilterTBCJV.CollapsedHoverImage = Nothing
        Me.gbFilterTBCJV.CollapsedNormalImage = Nothing
        Me.gbFilterTBCJV.CollapsedPressedImage = Nothing
        Me.gbFilterTBCJV.CollapseOnLoad = False
        Me.gbFilterTBCJV.Controls.Add(Me.Panel1)
        Me.gbFilterTBCJV.ExpandedHoverImage = Nothing
        Me.gbFilterTBCJV.ExpandedNormalImage = Nothing
        Me.gbFilterTBCJV.ExpandedPressedImage = Nothing
        Me.gbFilterTBCJV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterTBCJV.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterTBCJV.HeaderHeight = 25
        Me.gbFilterTBCJV.HeaderMessage = ""
        Me.gbFilterTBCJV.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterTBCJV.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterTBCJV.HeightOnCollapse = 0
        Me.gbFilterTBCJV.LeftTextSpace = 0
        Me.gbFilterTBCJV.Location = New System.Drawing.Point(121, 308)
        Me.gbFilterTBCJV.Name = "gbFilterTBCJV"
        Me.gbFilterTBCJV.OpenHeight = 300
        Me.gbFilterTBCJV.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterTBCJV.ShowBorder = True
        Me.gbFilterTBCJV.ShowCheckBox = False
        Me.gbFilterTBCJV.ShowCollapseButton = False
        Me.gbFilterTBCJV.ShowDefaultBorderColor = True
        Me.gbFilterTBCJV.ShowDownButton = False
        Me.gbFilterTBCJV.ShowHeader = True
        Me.gbFilterTBCJV.Size = New System.Drawing.Size(314, 121)
        Me.gbFilterTBCJV.TabIndex = 237
        Me.gbFilterTBCJV.Temp = 0
        Me.gbFilterTBCJV.Text = "SAP JV Export Filter"
        Me.gbFilterTBCJV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbFilterTBCJV.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkSelectAll)
        Me.Panel1.Controls.Add(Me.lvTranHead)
        Me.Panel1.Location = New System.Drawing.Point(3, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(308, 94)
        Me.Panel1.TabIndex = 3
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 19
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvTranHead
        '
        Me.lvTranHead.BackColorOnChecked = True
        Me.lvTranHead.CheckBoxes = True
        Me.lvTranHead.ColumnHeaders = Nothing
        Me.lvTranHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhEContribHeadCode, Me.colhEContribHead})
        Me.lvTranHead.CompulsoryColumns = ""
        Me.lvTranHead.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTranHead.FullRowSelect = True
        Me.lvTranHead.GridLines = True
        Me.lvTranHead.GroupingColumn = Nothing
        Me.lvTranHead.HideSelection = False
        Me.lvTranHead.Location = New System.Drawing.Point(0, 0)
        Me.lvTranHead.MinColumnWidth = 50
        Me.lvTranHead.MultiSelect = False
        Me.lvTranHead.Name = "lvTranHead"
        Me.lvTranHead.OptionalColumns = ""
        Me.lvTranHead.ShowMoreItem = False
        Me.lvTranHead.ShowSaveItem = False
        Me.lvTranHead.ShowSelectAll = True
        Me.lvTranHead.ShowSizeAllColumnsToFit = True
        Me.lvTranHead.Size = New System.Drawing.Size(308, 94)
        Me.lvTranHead.Sortable = True
        Me.lvTranHead.TabIndex = 0
        Me.lvTranHead.UseCompatibleStateImageBehavior = False
        Me.lvTranHead.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Tag = "colhCheck"
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhEContribHeadCode
        '
        Me.colhEContribHeadCode.Tag = "colhEContribHeadCode"
        Me.colhEContribHeadCode.Text = "Code"
        Me.colhEContribHeadCode.Width = 100
        '
        'colhEContribHead
        '
        Me.colhEContribHead.Tag = "colhEContribHead"
        Me.colhEContribHead.Text = "Employer Contribution Head"
        Me.colhEContribHead.Width = 170
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Checked = True
        Me.chkIgnoreZero.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(121, 266)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(223, 17)
        Me.chkIgnoreZero.TabIndex = 14
        Me.chkIgnoreZero.Text = "Ignore Zero"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'lnkTBCJVExport
        '
        Me.lnkTBCJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkTBCJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkTBCJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkTBCJVExport.Location = New System.Drawing.Point(8, 245)
        Me.lnkTBCJVExport.Name = "lnkTBCJVExport"
        Me.lnkTBCJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkTBCJVExport.TabIndex = 233
        Me.lnkTBCJVExport.TabStop = True
        Me.lnkTBCJVExport.Text = "&SAP JV Export..."
        Me.lnkTBCJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkTBCJVExport.Visible = False
        '
        'lnkiScalaJVExport
        '
        Me.lnkiScalaJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkiScalaJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkiScalaJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkiScalaJVExport.Location = New System.Drawing.Point(8, 226)
        Me.lnkiScalaJVExport.Name = "lnkiScalaJVExport"
        Me.lnkiScalaJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkiScalaJVExport.TabIndex = 231
        Me.lnkiScalaJVExport.TabStop = True
        Me.lnkiScalaJVExport.Text = "&iScala JV Export..."
        Me.lnkiScalaJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkiScalaJVExport.Visible = False
        '
        'lblExRate
        '
        Me.lblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExRate.Location = New System.Drawing.Point(342, 91)
        Me.lblExRate.Name = "lblExRate"
        Me.lblExRate.Size = New System.Drawing.Size(184, 14)
        Me.lblExRate.TabIndex = 229
        Me.lblExRate.Text = "#Value"
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(121, 88)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(153, 21)
        Me.cboCurrency.TabIndex = 2
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 92)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(107, 16)
        Me.lblCurrency.TabIndex = 228
        Me.lblCurrency.Text = "Currency"
        '
        'lnkSunJVExport
        '
        Me.lnkSunJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSunJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSunJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSunJVExport.Location = New System.Drawing.Point(121, 288)
        Me.lnkSunJVExport.Name = "lnkSunJVExport"
        Me.lnkSunJVExport.Size = New System.Drawing.Size(190, 16)
        Me.lnkSunJVExport.TabIndex = 223
        Me.lnkSunJVExport.TabStop = True
        Me.lnkSunJVExport.Text = "&Sun JV Export..."
        Me.lnkSunJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkSunJVExport.Visible = False
        '
        'lblCCGroup
        '
        Me.lblCCGroup.BackColor = System.Drawing.Color.Transparent
        Me.lblCCGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCCGroup.Location = New System.Drawing.Point(295, 170)
        Me.lblCCGroup.Name = "lblCCGroup"
        Me.lblCCGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblCCGroup.TabIndex = 219
        Me.lblCCGroup.Text = "C.Center Group"
        '
        'cboCCGroup
        '
        Me.cboCCGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCCGroup.DropDownWidth = 120
        Me.cboCCGroup.Enabled = False
        Me.cboCCGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCCGroup.FormattingEnabled = True
        Me.cboCCGroup.Location = New System.Drawing.Point(408, 168)
        Me.cboCCGroup.Name = "cboCCGroup"
        Me.cboCCGroup.Size = New System.Drawing.Size(153, 21)
        Me.cboCCGroup.TabIndex = 8
        '
        'chkShowGroupByCCGroup
        '
        Me.chkShowGroupByCCGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowGroupByCCGroup.Location = New System.Drawing.Point(121, 223)
        Me.chkShowGroupByCCGroup.Name = "chkShowGroupByCCGroup"
        Me.chkShowGroupByCCGroup.Size = New System.Drawing.Size(257, 16)
        Me.chkShowGroupByCCGroup.TabIndex = 11
        Me.chkShowGroupByCCGroup.Text = "Show Group By Cost Center Group"
        Me.chkShowGroupByCCGroup.UseVisualStyleBackColor = True
        '
        'lblBranch
        '
        Me.lblBranch.BackColor = System.Drawing.Color.Transparent
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 171)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(107, 16)
        Me.lblBranch.TabIndex = 214
        Me.lblBranch.Text = "Branch"
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 150
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(121, 169)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(153, 21)
        Me.cboBranch.TabIndex = 7
        '
        'chkShowSummaryNewPage
        '
        Me.chkShowSummaryNewPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSummaryNewPage.Location = New System.Drawing.Point(408, 246)
        Me.chkShowSummaryNewPage.Name = "chkShowSummaryNewPage"
        Me.chkShowSummaryNewPage.Size = New System.Drawing.Size(223, 16)
        Me.chkShowSummaryNewPage.TabIndex = 16
        Me.chkShowSummaryNewPage.Text = "Show Summary Report on New Page"
        Me.chkShowSummaryNewPage.UseVisualStyleBackColor = True
        '
        'chkShowSummaryBottom
        '
        Me.chkShowSummaryBottom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSummaryBottom.Location = New System.Drawing.Point(121, 245)
        Me.chkShowSummaryBottom.Name = "chkShowSummaryBottom"
        Me.chkShowSummaryBottom.Size = New System.Drawing.Size(259, 16)
        Me.chkShowSummaryBottom.TabIndex = 15
        Me.chkShowSummaryBottom.Text = "Show Summary Report at the Bottom"
        Me.chkShowSummaryBottom.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Checked = True
        Me.chkInactiveemp.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(408, 268)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(223, 16)
        Me.chkInactiveemp.TabIndex = 17
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        Me.chkInactiveemp.Visible = False
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 180
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(121, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(153, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(107, 16)
        Me.lblReportType.TabIndex = 63
        Me.lblReportType.Text = "Report Type"
        '
        'lblCreditAMountTo
        '
        Me.lblCreditAMountTo.BackColor = System.Drawing.Color.Transparent
        Me.lblCreditAMountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditAMountTo.Location = New System.Drawing.Point(342, 143)
        Me.lblCreditAMountTo.Name = "lblCreditAMountTo"
        Me.lblCreditAMountTo.Size = New System.Drawing.Size(60, 15)
        Me.lblCreditAMountTo.TabIndex = 61
        Me.lblCreditAMountTo.Text = "To"
        '
        'txtCreditAMountTo
        '
        Me.txtCreditAMountTo.AllowNegative = True
        Me.txtCreditAMountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCreditAMountTo.DigitsInGroup = 0
        Me.txtCreditAMountTo.Flags = 0
        Me.txtCreditAMountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCreditAMountTo.Location = New System.Drawing.Point(408, 141)
        Me.txtCreditAMountTo.MaxDecimalPlaces = 4
        Me.txtCreditAMountTo.MaxWholeDigits = 9
        Me.txtCreditAMountTo.Name = "txtCreditAMountTo"
        Me.txtCreditAMountTo.Prefix = ""
        Me.txtCreditAMountTo.RangeMax = 1.7976931348623157E+308
        Me.txtCreditAMountTo.RangeMin = -1.7976931348623157E+308
        Me.txtCreditAMountTo.Size = New System.Drawing.Size(153, 21)
        Me.txtCreditAMountTo.TabIndex = 6
        Me.txtCreditAMountTo.Text = "0"
        Me.txtCreditAMountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCreditAMountFrom
        '
        Me.lblCreditAMountFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblCreditAMountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditAMountFrom.Location = New System.Drawing.Point(8, 144)
        Me.lblCreditAMountFrom.Name = "lblCreditAMountFrom"
        Me.lblCreditAMountFrom.Size = New System.Drawing.Size(107, 16)
        Me.lblCreditAMountFrom.TabIndex = 59
        Me.lblCreditAMountFrom.Text = "Credit Amount From"
        '
        'txtCreditAMountFrom
        '
        Me.txtCreditAMountFrom.AllowNegative = True
        Me.txtCreditAMountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCreditAMountFrom.DigitsInGroup = 0
        Me.txtCreditAMountFrom.Flags = 0
        Me.txtCreditAMountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCreditAMountFrom.Location = New System.Drawing.Point(121, 142)
        Me.txtCreditAMountFrom.MaxDecimalPlaces = 4
        Me.txtCreditAMountFrom.MaxWholeDigits = 9
        Me.txtCreditAMountFrom.Name = "txtCreditAMountFrom"
        Me.txtCreditAMountFrom.Prefix = ""
        Me.txtCreditAMountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtCreditAMountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtCreditAMountFrom.Size = New System.Drawing.Size(153, 21)
        Me.txtCreditAMountFrom.TabIndex = 5
        Me.txtCreditAMountFrom.Text = "0"
        Me.txtCreditAMountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDebitAmountTo
        '
        Me.lblDebitAmountTo.BackColor = System.Drawing.Color.Transparent
        Me.lblDebitAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebitAmountTo.Location = New System.Drawing.Point(342, 117)
        Me.lblDebitAmountTo.Name = "lblDebitAmountTo"
        Me.lblDebitAmountTo.Size = New System.Drawing.Size(60, 15)
        Me.lblDebitAmountTo.TabIndex = 56
        Me.lblDebitAmountTo.Text = "To"
        '
        'txtDebitAmountTo
        '
        Me.txtDebitAmountTo.AllowNegative = True
        Me.txtDebitAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDebitAmountTo.DigitsInGroup = 0
        Me.txtDebitAmountTo.Flags = 0
        Me.txtDebitAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDebitAmountTo.Location = New System.Drawing.Point(408, 114)
        Me.txtDebitAmountTo.MaxDecimalPlaces = 4
        Me.txtDebitAmountTo.MaxWholeDigits = 9
        Me.txtDebitAmountTo.Name = "txtDebitAmountTo"
        Me.txtDebitAmountTo.Prefix = ""
        Me.txtDebitAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtDebitAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtDebitAmountTo.Size = New System.Drawing.Size(153, 21)
        Me.txtDebitAmountTo.TabIndex = 4
        Me.txtDebitAmountTo.Text = "0"
        Me.txtDebitAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDebitAmountFrom
        '
        Me.lblDebitAmountFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblDebitAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebitAmountFrom.Location = New System.Drawing.Point(8, 117)
        Me.lblDebitAmountFrom.Name = "lblDebitAmountFrom"
        Me.lblDebitAmountFrom.Size = New System.Drawing.Size(107, 16)
        Me.lblDebitAmountFrom.TabIndex = 54
        Me.lblDebitAmountFrom.Text = "Debit Amount From"
        '
        'txtDebitAmountFrom
        '
        Me.txtDebitAmountFrom.AllowNegative = True
        Me.txtDebitAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDebitAmountFrom.DigitsInGroup = 0
        Me.txtDebitAmountFrom.Flags = 0
        Me.txtDebitAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDebitAmountFrom.Location = New System.Drawing.Point(121, 115)
        Me.txtDebitAmountFrom.MaxDecimalPlaces = 4
        Me.txtDebitAmountFrom.MaxWholeDigits = 9
        Me.txtDebitAmountFrom.Name = "txtDebitAmountFrom"
        Me.txtDebitAmountFrom.Prefix = ""
        Me.txtDebitAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtDebitAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtDebitAmountFrom.Size = New System.Drawing.Size(153, 21)
        Me.txtDebitAmountFrom.TabIndex = 3
        Me.txtDebitAmountFrom.Text = "0"
        Me.txtDebitAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(107, 16)
        Me.lblPeriod.TabIndex = 27
        Me.lblPeriod.Text = "Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 120
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(121, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(153, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'frmPaymentJVReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 558)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmPaymentJVReport"
        Me.Text = "frmPaymentJVReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbFilterTBCJV.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtInvoiceRef As System.Windows.Forms.TextBox
    Private WithEvents lblInvoiceRef As System.Windows.Forms.Label
    Friend WithEvents chkShowColumnHeader As System.Windows.Forms.CheckBox
    Private WithEvents lblCustomCCenter As System.Windows.Forms.Label
    Public WithEvents cboCustomCCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lnkiScala2JVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSunJV5Export As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilterTBCJV As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvTranHead As eZee.Common.eZeeListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEContribHeadCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEContribHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents lnkTBCJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkiScalaJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lblExRate As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents lnkSunJVExport As System.Windows.Forms.LinkLabel
    Private WithEvents lblCCGroup As System.Windows.Forms.Label
    Public WithEvents cboCCGroup As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowGroupByCCGroup As System.Windows.Forms.CheckBox
    Private WithEvents lblBranch As System.Windows.Forms.Label
    Public WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowSummaryNewPage As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowSummaryBottom As System.Windows.Forms.CheckBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Private WithEvents lblCreditAMountTo As System.Windows.Forms.Label
    Public WithEvents txtCreditAMountTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblCreditAMountFrom As System.Windows.Forms.Label
    Public WithEvents txtCreditAMountFrom As eZee.TextBox.NumericTextBox
    Private WithEvents lblDebitAmountTo As System.Windows.Forms.Label
    Public WithEvents txtDebitAmountTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblDebitAmountFrom As System.Windows.Forms.Label
    Public WithEvents txtDebitAmountFrom As eZee.TextBox.NumericTextBox
    Private WithEvents lblPeriod As System.Windows.Forms.Label
    Public WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
End Class
