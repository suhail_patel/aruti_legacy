'Class Name : clsSalaryBudgetBreakdownByPeriod.vb
'Purpose    :
'Date       :02/09/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>

Public Class clsSalaryBudgetBreakdownByPeriod
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsSalaryBudgetBreakdownByPeriod"
    Private mstrReportId As String = enArutiReport.Salary_Budget_Breakdown_By_Period_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintBudgetId As Integer = -1
    Private mstrBudgetName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty

    Private mdtTableExcel As DataTable
    Private mstrMessage As String = ""

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
Private mstrAnalysis_CodeField As String = "" 'Sohail (29 Mar 2017)
#End Region

#Region " Properties "

    Public WriteOnly Property _BudgetId() As Integer
        Set(ByVal value As Integer)
            mintBudgetId = value
        End Set
    End Property

    Public WriteOnly Property _BudgetName() As String
        Set(ByVal value As String)
            mstrBudgetName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property
#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try

            mintBudgetId = -1
            mstrBudgetName = 0
            mintPeriodId = -1
            mstrPeriodName = 0
            mstrMessage = ""
            'mstrAdvance_Filter = ""



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            Me._FilterTitle &= ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'OrderByDisplay = iColumn_DetailReport.ColumnItem(1).DisplayName
            'OrderByQuery = iColumn_DetailReport.ColumnItem(1).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            'If mblnFirstNamethenSurname = True Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 9, "Employee Name")))
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Export_Report(ByVal xUserUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnApplyUserAccessFilter As Boolean _
                                  , ByVal strfmtCurrency As String _
                                  , ByVal xExportReportPath As String _
                                  , ByVal xOpenAfterExport As Boolean _
                                  , ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As Integer _
                                  )
        'Sohail (02 Aug 2017) - [xDatabaseName, xYearUnkid]

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran
        Dim mstrPreviousPeriodDBName As String

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        Dim mintPresentationModeId As Integer
        Dim mdtBudgetDate As Date
        Dim mstrPeriodIdList As String = ""
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        'Sohail (02 Aug 2017) -- Start
        'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
        objPeriod._Periodunkid(xDatabaseName) = mintPeriodId
        Dim dtPeriodStart As Date = objPeriod._Start_Date
        Dim dtPeriodEnd As Date = objPeriod._End_Date
        'Sohail (02 Aug 2017) -- End

        Try

            mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetId)
            mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetId)
            mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            objBudget._Budgetunkid = mintBudgetId
            mintViewIdx = objBudget._Allocationbyid
            mintViewById = objBudget._Viewbyid
            mintPresentationModeId = objBudget._Presentationmodeid
            mdtBudgetDate = objBudget._Budget_date.Date
            mstrPreviousPeriodDBName = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, xCompanyUnkid)


            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.1 - Column name or number of supplied values does not match table definition. 
            'If CInt(mintViewById) = enBudgetViewBy.Allocation Then
            '    Dim frm As New frmViewAnalysis
            '    frm.displayDialog(, mintViewIdx, mstrAllocationTranUnkIDs, True)
            '    mstrStringIds = frm._ReportBy_Ids
            '    mstrStringName = frm._ReportBy_Name
            '    mintViewIdx = frm._ViewIndex
            '    mstrAllocationTranUnkIDs = frm._ReportBy_Ids
            '    If mintViewIdx = 0 Then mintViewIdx = -1

            '    mstrAnalysis_Fields = frm._Analysis_Fields
            '    mstrAnalysis_Join = frm._Analysis_Join
            '    mstrAnalysis_TableName = frm._Analysis_TableName
            '    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            '    mstrReport_GroupName = frm._Report_GroupName
            '    frm = Nothing

            'ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
            '    mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetId)
            '    Dim frm As New frmEmpSelection
            '    frm._EmployeeAsOnStartDate = mdtBudgetDate
            '    frm._EmployeeAsOnEndDate = mdtBudgetDate
            '    frm.displayDialog(mstrEmployeeIDs, , True)
            '    mstrStringIds = frm._ReportBy_Ids
            '    mstrStringName = frm._ReportBy_Name
            '    mintViewIdx = frm._ViewIndex
            '    mstrAllocationTranUnkIDs = frm._ReportBy_Ids

            '    mstrAnalysis_Fields = frm._Analysis_Fields
            '    mstrAnalysis_Join = frm._Analysis_Join
            '    mstrAnalysis_TableName = frm._Analysis_TableName
            '    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            '    mstrReport_GroupName = frm._Report_GroupName
            '    frm = Nothing
            'End If
            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, mdtBudgetDate, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                If mintViewIdx = 0 Then mintViewIdx = -1
            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetId)
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                mintViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If
            'Sohail (29 Mar 2017) -- End

            Dim strTranHeadIDList As String = String.Join(",", (From p In mdicHeadMapping Select (p.Key.ToString)).ToArray)
            dsAllHeads = Nothing
            dsList = objFundProjectCode.GetList("FundProjectCode")
            mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
            Dim dicFundCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
            dsList = objActivity.GetComboList("Activity", True)
            Dim d_row() As DataRow = dsList.Tables(0).Select("fundactivityunkid = 0")
            If d_row.Length > 0 Then
                d_row(0).Item("activitycode") = "" 'Sohail (29 Mar 2017)
                d_row(0).Item("activityname") = ""
                dsList.Tables(0).AcceptChanges()
            End If
            Dim dicActivityCode As Dictionary(Of Integer, String) = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activitycode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.1 - Column name or number of supplied values does not match table definition. 
            'dsList = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetId, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodId, True, "", dsAllHeads, True)
            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            'dsList = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetId, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodId, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            dsList = objBudgetCodes.GetDataGridList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, dtPeriodStart, dtPeriodEnd, xUserModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetId, mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", mintPeriodId, True, "", dsAllHeads, True, mstrAnalysis_CodeField)
            'Sohail (02 Aug 2017) -- End
            'Sohail (29 Mar 2017) -- End

            Dim strExpression As String = ""
            For Each pair In mdicFund
                strExpression &= " + [|_" & pair.Key.ToString & "]"
                dsList.Tables(0).Columns.Add("A||_" & pair.Key.ToString, System.Type.GetType("System.String")).DefaultValue = ""
            Next
            If strExpression.Trim <> "" Then
                dsList.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
            End If
            mdtTableExcel = dsList.Tables(0)

            Dim lst_Row As List(Of DataRow) = (From p In mdtTableExcel Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            If lst_Row.Count > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, Total Percentage should be 100 for all transactions.")
                Return False
            End If

            For Each dsRow As DataRow In mdtTableExcel.Rows
                dsRow.Item("budgetamount") = Format(CDec(dsRow.Item("budgetamount")), strfmtCurrency)
                For Each pair In dicFundCode
                    dsRow.Item("|_" & pair.Key.ToString) = Format(CDec(dsRow.Item("|_" & pair.Key.ToString)), strfmtCurrency)
                    If dicActivityCode.ContainsKey(CInt(dsRow.Item("||_" & pair.Key.ToString))) = True Then
                        dsRow.Item("A||_" & pair.Key.ToString) = dicActivityCode.Item(CInt(dsRow.Item("||_" & pair.Key.ToString)))
                    Else
                        dsRow.Item("A||_" & pair.Key.ToString) = ""
                    End If
                Next
                'dsRow.Item("colhTotal") = Format(CDec(dsRow.Item("colhTotal")), strfmtCurrency)
            Next
            mdtTableExcel.AcceptChanges()

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            Dim mintColumn As Integer = 0
            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName.Replace(" :", "")
            mdtTableExcel.Columns("GName").SetOrdinal(mintColumn)
            mintColumn += 1

            If CInt(mintViewById) = enBudgetViewBy.Employee Then
                mdtTableExcel.Columns("EmpJobTitle").Caption = Language.getMessage(mstrModuleName, 2, "Job Title")
                mdtTableExcel.Columns("EmpJobTitle").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            mdtTableExcel.Columns("budgetamount").Caption = Language.getMessage(mstrModuleName, 3, "Gross Salary Budget")
            mdtTableExcel.Columns("budgetamount").SetOrdinal(mintColumn)
            mintColumn += 1

            For Each pair In dicFundCode
                mdtTableExcel.Columns("|_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 4, "(%)")
                mdtTableExcel.Columns("|_" & pair.Key.ToString).SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("A||_" & pair.Key.ToString).Caption = Language.getMessage(mstrModuleName, 5, "Activity Code")
                mdtTableExcel.Columns("A||_" & pair.Key.ToString).SetOrdinal(mintColumn)
                mintColumn += 1
            Next

            If mdtTableExcel.Columns.Contains("colhTotal") = True Then
                mdtTableExcel.Columns("colhTotal").Caption = Language.getMessage(mstrModuleName, 6, "Total")
                mdtTableExcel.Columns("colhTotal").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            For i = mintColumn To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(mintColumn)
            Next


            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "BUDGET SALARY BREAKDWON FOR") & " " & mstrBudgetName & " " & Language.getMessage(mstrModuleName, 123, "For") & " " & mstrPeriodName, "s9wc")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)
            If CInt(mintViewById) = enBudgetViewBy.Employee Then
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            End If
            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)
            For Each pair In dicFundCode
                wcell = New WorksheetCell(pair.Value.ToString, "HeaderStyle")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
            Next
            If mdtTableExcel.Columns.Contains("colhTotal") = True Then
                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)

            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "BUDGET SALARY BREAKDWON  FOR")
            Language.setMessage(mstrModuleName, 2, "Job Title")
            Language.setMessage(mstrModuleName, 3, "Gross Salary Budget")
            Language.setMessage(mstrModuleName, 4, "(%)")
            Language.setMessage(mstrModuleName, 5, "Activity Code")
            Language.setMessage(mstrModuleName, 6, "Total")
            Language.setMessage(mstrModuleName, 7, "Sorry, Total Percentage should be 100 for all transactions.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
