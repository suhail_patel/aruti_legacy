'************************************************************************************************************************************
'Class Name : frmActivityDone_Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmActivityDone_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmActivityDone_Report"
    Private objActivityDone As clsActivityDone_Report


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

    'Pinkal (06-Mar-2014) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objActivityDone = New clsActivityDone_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        _Show_AdvanceFilter = True
        objActivityDone.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objActivity As New clsActivity_Master
        Dim objMeasure As New clsUnitMeasure_Master
        Try
            dsCombo = objActivity.getComboList("List", True)
            With cboActivity
                .ValueMember = "activityunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objMeasure.getComboList("List", True)
            With cboMeasures
                .ValueMember = "measureunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objActivity = Nothing : objMeasure = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime

            cboActivity.SelectedValue = 0
            cboMeasures.SelectedValue = 0

            txtCountFrom.Text = "" : txtCountTo.Text = ""

            chkIncludeZero.Checked = False


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            'Pinkal (06-Mar-2014) -- End

            objActivityDone.setDefaultOrderBy(0)
            txtOrderBy.Text = objActivityDone.OrderByDisplay

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        objActivityDone.SetDefaultValue()
        Try
            objActivityDone._Date1 = dtpDate1.Value.Date
            objActivityDone._Date2 = dtpDate2.Value.Date

            objActivityDone._ActivityId = cboActivity.SelectedValue
            objActivityDone._ActivityName = cboActivity.Text

            objActivityDone._MeasureId = cboMeasures.SelectedValue
            objActivityDone._MeasureName = cboMeasures.Text

            objActivityDone._TotalCountFrom = txtCountFrom.Decimal
            objActivityDone._TotalCountTo = txtCountTo.Decimal

            objActivityDone._IncludeZeroFilter = chkIncludeZero.Checked

            objActivityDone._AmtPaidFrom = txtAmtPaidFrm.Decimal
            objActivityDone._AmtPaidTo = txtAmtPaidTo.Decimal

            objActivityDone._NormalHrsFrom = txtNrmlHrsFrm.Decimal
            objActivityDone._NormalHrsTo = txtNrmlHrsTo.Decimal

            objActivityDone._OTHrsFrom = txtOTHrsFrm.Decimal
            objActivityDone._OTHrsTo = txtOTHrsTo.Decimal

            objActivityDone._TotalPaidAmt = txtTotalPaidFrm.Decimal
            objActivityDone._TotalPaidTo = txtTotalPaidTo.Decimal


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            objActivityDone._ViewByIds = mstrStringIds
            objActivityDone._ViewByName = mstrStringName
            objActivityDone._ViewIndex = mintViewIdx
            objActivityDone._Analysis_Fields = mstrAnalysis_Fields
            objActivityDone._Analysis_Join = mstrAnalysis_Join
            objActivityDone._Analysis_OrderBy = mstrAnalysis_OrderBy
            objActivityDone._Report_GroupName = mstrReport_GroupName
            objActivityDone._Advance_Filter = mstrAdvanceFilter
            'Pinkal (06-Mar-2014) -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmActivityDone_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objActivityDone = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmActivityDone_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmActivityDone_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objActivityDone._ReportName
            Me._Message = objActivityDone._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmActivityDone_Report_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsActivityDone_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsActivityDone_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objActivityDone.generateReport(0, e.Type, enExportAction.None)
            objActivityDone.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpDate1.Value.Date, dtpDate2.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objActivityDone.generateReport(0, enPrintAction.None, e.Type)
            objActivityDone.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpDate1.Value.Date, dtpDate2.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objActivityDone.setOrderBy(0)
            txtOrderBy.Text = objActivityDone.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (06-Mar-2014) -- End

    'S.SANDEEP [ 15 MAR 2014 ] -- START
    Private Sub objbtnSearchMeasures_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMeasures.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboMeasures.DataSource
            frm.ValueMember = cboMeasures.ValueMember
            frm.DisplayMember = cboMeasures.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboMeasures.SelectedValue = frm.SelectedValue
                cboMeasures.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchActivity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchActivity.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboActivity.DataSource
            frm.ValueMember = cboActivity.ValueMember
            frm.DisplayMember = cboActivity.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboActivity.SelectedValue = frm.SelectedValue
                cboActivity.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMeasures_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 15 MAR 2014 ] -- END


#End Region

#Region "LinkButton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblMeasures.Text = Language._Object.getCaption(Me.lblMeasures.Name, Me.lblMeasures.Text)
			Me.lblActivity.Text = Language._Object.getCaption(Me.lblActivity.Name, Me.lblActivity.Text)
			Me.lblCountTo.Text = Language._Object.getCaption(Me.lblCountTo.Name, Me.lblCountTo.Text)
			Me.lblCountFrom.Text = Language._Object.getCaption(Me.lblCountFrom.Name, Me.lblCountFrom.Text)
			Me.chkIncludeZero.Text = Language._Object.getCaption(Me.chkIncludeZero.Name, Me.chkIncludeZero.Text)
			Me.lblTotalPaidFrm.Text = Language._Object.getCaption(Me.lblTotalPaidFrm.Name, Me.lblTotalPaidFrm.Text)
			Me.lblOTHrsFrm.Text = Language._Object.getCaption(Me.lblOTHrsFrm.Name, Me.lblOTHrsFrm.Text)
			Me.lblNrmlHrsFrom.Text = Language._Object.getCaption(Me.lblNrmlHrsFrom.Name, Me.lblNrmlHrsFrom.Text)
			Me.lblAmtPaidFrm.Text = Language._Object.getCaption(Me.lblAmtPaidFrm.Name, Me.lblAmtPaidFrm.Text)
			Me.lblTotalPaidTo.Text = Language._Object.getCaption(Me.lblTotalPaidTo.Name, Me.lblTotalPaidTo.Text)
			Me.lblOTHrsTo.Text = Language._Object.getCaption(Me.lblOTHrsTo.Name, Me.lblOTHrsTo.Text)
			Me.lblNrmlHrsTo.Text = Language._Object.getCaption(Me.lblNrmlHrsTo.Name, Me.lblNrmlHrsTo.Text)
			Me.lblAmtPaidTo.Text = Language._Object.getCaption(Me.lblAmtPaidTo.Name, Me.lblAmtPaidTo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
