'************************************************************************************************************************************
'Class Name : clsActivityDone_Report.vb
'Purpose    :
'Date       :27/06/2013
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsActivityDone_Report
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsActivityDone_Report"
    Private mstrReportId As String = enArutiReport.ActivityDoneReport   '119
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mdtDate1 As Date = Nothing
    Private mdtDate2 As Date = Nothing
    Private mintActivityId As Integer = 0
    Private mstrActivityName As String = String.Empty
    Private mintMeasureId As Integer = 0
    Private mstrUnitMeasureName As String = String.Empty
    Private mintTotalCountFrom As Integer = -1
    Private mintTotalCountTo As Integer = -1
    Private mdecAmtPaidFrom As Decimal = 0
    Private mdecAmtPaidTo As Decimal = 0
    Private mdblNormalHrsFrom As Double = 0
    Private mdblNormalHrsTo As Double = 0
    Private mdblOTHrsFrom As Double = 0
    Private mdblOTHrsTo As Double = 0
    Private mdecTotalPaidAmt As Decimal = 0
    Private mdecTotalPaidTo As Decimal = 0
    Private mblnIncludeZeroFilter As Boolean = False
    Private mstrCurrFormat As String = String.Empty
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : WTCL Changes
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (06-Mar-2014) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _Date1() As Date
        Set(ByVal value As Date)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As Date
        Set(ByVal value As Date)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _ActivityId() As Integer
        Set(ByVal value As Integer)
            mintActivityId = value
        End Set
    End Property

    Public WriteOnly Property _ActivityName() As String
        Set(ByVal value As String)
            mstrActivityName = value
        End Set
    End Property

    Public WriteOnly Property _MeasureId() As Integer
        Set(ByVal value As Integer)
            mintMeasureId = value
        End Set
    End Property

    Public WriteOnly Property _MeasureName() As String
        Set(ByVal value As String)
            mstrUnitMeasureName = value
        End Set
    End Property

    Public WriteOnly Property _TotalCountFrom() As Integer
        Set(ByVal value As Integer)
            mintTotalCountFrom = value
        End Set
    End Property

    Public WriteOnly Property _TotalCountTo() As Integer
        Set(ByVal value As Integer)
            mintTotalCountTo = value
        End Set
    End Property

    Public WriteOnly Property _AmtPaidFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecAmtPaidFrom = value
        End Set
    End Property

    Public WriteOnly Property _AmtPaidTo() As Decimal
        Set(ByVal value As Decimal)
            mdecAmtPaidTo = value
        End Set
    End Property

    Public WriteOnly Property _NormalHrsFrom() As Double
        Set(ByVal value As Double)
            mdblNormalHrsFrom = value
        End Set
    End Property

    Public WriteOnly Property _NormalHrsTo() As Double
        Set(ByVal value As Double)
            mdblNormalHrsTo = value
        End Set
    End Property

    Public WriteOnly Property _OTHrsFrom() As Double
        Set(ByVal value As Double)
            mdblOTHrsFrom = value
        End Set
    End Property

    Public WriteOnly Property _OTHrsTo() As Double
        Set(ByVal value As Double)
            mdblOTHrsTo = value
        End Set
    End Property

    Public WriteOnly Property _TotalPaidAmt() As Decimal
        Set(ByVal value As Decimal)
            mdecTotalPaidAmt = value
        End Set
    End Property

    Public WriteOnly Property _TotalPaidTo() As Decimal
        Set(ByVal value As Decimal)
            mdecTotalPaidTo = value
        End Set
    End Property

    Public WriteOnly Property _IncludeZeroFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeZeroFilter = value
        End Set
    End Property

    Public WriteOnly Property _CurrFormat() As String
        Set(ByVal value As String)
            mstrCurrFormat = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    'Pinkal (06-Mar-2014) -- End


#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mintActivityId = 0
            mstrActivityName = String.Empty
            mintMeasureId = 0
            mstrUnitMeasureName = String.Empty
            mintTotalCountFrom = -1
            mintTotalCountTo = -1
            mdecAmtPaidFrom = 0
            mdecAmtPaidTo = 0
            mdblNormalHrsFrom = 0
            mdblNormalHrsTo = 0
            mdblOTHrsFrom = 0
            mdblOTHrsTo = 0
            mdecTotalPaidAmt = 0
            mdecTotalPaidTo = 0


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""
            'Pinkal (06-Mar-2014) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1))
            objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate2))

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Date From : ") & mdtDate1.Date & " " & _
                               Language.getMessage(mstrModuleName, 101, "to ") & mdtDate2.Date

            If mintActivityId > 0 Then
                objDataOperation.AddParameter("@ActId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityId)
                Me._FilterQuery &= " AND practivity_master.activityunkid = @ActId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 102, "Activity :") & " " & mstrActivityName & " "
            End If

            If mintMeasureId > 0 Then
                objDataOperation.AddParameter("@UoMId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMeasureId)
                Me._FilterQuery &= " AND prunitmeasure_master.measureunkid = @UoMId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, "Measure :") & " " & mstrUnitMeasureName & " "
            End If

            If mintTotalCountFrom > 0 AndAlso mintTotalCountTo > 0 Then
                objDataOperation.AddParameter("@CntFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalCountFrom)
                objDataOperation.AddParameter("@CntTo", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalCountTo)
                Me._FilterQuery &= " AND A.Occ BETWEEN @CntFrom AND @CntTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 104, "Occurrence :") & " " & mintTotalCountFrom & " " & _
                                   Language.getMessage(mstrModuleName, 101, "to ") & mintTotalCountTo & " "
            End If

            Dim blnFlag As Boolean = False
            If mblnIncludeZeroFilter = True Then
                If mdecAmtPaidFrom >= 0 AndAlso mdecAmtPaidTo >= 0 Then
                    blnFlag = True
                End If
            Else
                If mdecAmtPaidFrom > 0 AndAlso mdecAmtPaidTo > 0 Then
                    blnFlag = True
                End If
            End If

            If blnFlag = True Then
                objDataOperation.AddParameter("@PaidFrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmtPaidFrom)
                objDataOperation.AddParameter("@PaidTo", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmtPaidTo)
                Me._FilterQuery &= " AND A.amt BETWEEN @PaidFrom AND @PaidTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 105, "Amount Paid From :") & " " & mdecAmtPaidFrom & " " & _
                                   Language.getMessage(mstrModuleName, 101, "to ") & mdecAmtPaidTo & " "
            End If

            blnFlag = False
            If mblnIncludeZeroFilter = True Then
                If mdblNormalHrsFrom >= 0 AndAlso mdblNormalHrsTo >= 0 Then
                    blnFlag = True
                End If
            Else
                If mdblNormalHrsFrom > 0 AndAlso mdblNormalHrsTo > 0 Then
                    blnFlag = True
                End If
            End If
            If blnFlag = True Then
                objDataOperation.AddParameter("@nHrsF", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblNormalHrsFrom)
                objDataOperation.AddParameter("@nHrsT", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblNormalHrsTo)
                Me._FilterQuery &= " AND A.nHrs BETWEEN @nHrsF AND @nHrsT "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 106, "Normal Hrs. From :") & " " & mdblNormalHrsFrom & " " & _
                                   Language.getMessage(mstrModuleName, 101, "to ") & mdblNormalHrsTo & " "
            End If

            blnFlag = False
            If mblnIncludeZeroFilter = True Then
                If mdblOTHrsFrom >= 0 AndAlso mdblOTHrsTo >= 0 Then
                    blnFlag = True
                End If
            Else
                If mdblOTHrsFrom > 0 AndAlso mdblOTHrsTo > 0 Then
                    blnFlag = True
                End If
            End If

            If blnFlag = True Then
                objDataOperation.AddParameter("@oHrsF", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblOTHrsFrom)
                objDataOperation.AddParameter("@oHrsT", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblOTHrsTo)
                Me._FilterQuery &= " AND A.oHrs BETWEEN @oHrsF AND @oHrsT "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 107, "OT Hrs. From :") & " " & mdblOTHrsFrom & " " & _
                                   Language.getMessage(mstrModuleName, 101, "to ") & mdblOTHrsTo & " "
            End If

            blnFlag = False
            If mblnIncludeZeroFilter = True Then
                If mdblOTHrsFrom >= 0 AndAlso mdblOTHrsTo >= 0 Then
                    blnFlag = True
                End If
            Else
                If mdblOTHrsFrom > 0 AndAlso mdblOTHrsTo > 0 Then
                    blnFlag = True
                End If
            End If
            If blnFlag = True Then
                objDataOperation.AddParameter("@TPaidF", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalPaidAmt)
                objDataOperation.AddParameter("@TPaidT", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalPaidTo)
                Me._FilterQuery &= " AND (A.nAmt+A.oAmt) BETWEEN @TPaidF AND @TPaidT "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 108, "Total Paid From :") & " " & mdecTotalPaidAmt & " " & _
                                   Language.getMessage(mstrModuleName, 101, "to ") & mdecTotalPaidTo & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 109, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit

        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            objConfig._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, True, True, objUser._Username, objConfig._CurrencyFormat)

            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(practivity_master.code,'')", Language.getMessage(mstrModuleName, 200, "Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(practivity_master.name,'')", Language.getMessage(mstrModuleName, 201, "Activity")))
            iColumn_DetailReport.Add(New IColumn("Occ", Language.getMessage(mstrModuleName, 202, "Count")))
            iColumn_DetailReport.Add(New IColumn("CAST(avalue AS DECIMAL(10,2))", Language.getMessage(mstrModuleName, 203, "Units")))
            iColumn_DetailReport.Add(New IColumn("amt", Language.getMessage(mstrModuleName, 204, "Amount Paid")))
            iColumn_DetailReport.Add(New IColumn("CAST(CASE WHEN amt <=0 OR avalue <=0 THEN 0 ELSE amt/avalue END AS DECMAL(10,2))", Language.getMessage(mstrModuleName, 205, "Av. Unit")))
            iColumn_DetailReport.Add(New IColumn("CAST(CASE WHEN amt <=0 OR Occ <=0 THEN 0 ELSE amt/Occ END AS DECMAL(10,2))", Language.getMessage(mstrModuleName, 206, "Av. M/Day")))
            iColumn_DetailReport.Add(New IColumn("CAST(nHrs AS DECIMAL(10,2))", Language.getMessage(mstrModuleName, 207, "Normal Hrs.")))
            iColumn_DetailReport.Add(New IColumn("CAST(oHrs AS DECIMAL(10,2))", Language.getMessage(mstrModuleName, 208, "OT Hrs.")))
            iColumn_DetailReport.Add(New IColumn("nAmt", Language.getMessage(mstrModuleName, 209, "Normal Amount")))
            iColumn_DetailReport.Add(New IColumn("oAmt", Language.getMessage(mstrModuleName, 210, "OT Amount")))
            iColumn_DetailReport.Add(New IColumn("(nAmt+oAmt)", Language.getMessage(mstrModuleName, 211, "Total Paid")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strUserName As String _
                                           , ByVal strFmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strUserName, strFmtCurrency]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : WTCL Changes REMOVE AVG UNIT AND AVG COUNT COLUMN AS WE HAVE PROBLEM IN GETTING AVERAGE WHEN APPLYING ANALYSIS BY.IN FUTURE FIRST SEE THE FEASIBILITY THEN PUT AVG UNIT AND AVG COUNT.


            '"	
            '

            StrQ = "SELECT DISTINCT " & _
                   "	 ISNULL(practivity_master.code,'') AS CODE " & _
                   "	,ISNULL(practivity_master.name,'') AS ACTIVITY " & _
                   "	,Occ AS OCC " & _
                   "	,CAST(avalue AS DECIMAL(10,2)) AS UNITS " & _
                   "	,prunitmeasure_master.name AS UOM " & _
                   "	,amt AS AMTPAID " & _
                   "	,CAST(CASE WHEN amt <=0 OR avalue <=0 THEN 0 ELSE amt/avalue END AS DECIMAL(10,2)) AS AVG_UNIT " & _
                   "	,CAST(CASE WHEN amt <=0 OR Occ <=0 THEN 0 ELSE amt/Occ END AS DECIMAL(10,2)) AS AVG_COUNT " & _
                   "	,CAST(nHrs AS DECIMAL(10,2)) AS NORMAL_HRS " & _
                   "	,CAST(oHrs AS DECIMAL(10,2)) AS OT_HRS " & _
                   "	,nAmt AS NORMAL_AMT " & _
                   "	,oAmt AS OT_AMT " & _
                   "	,(nAmt+oAmt) AS TOTAL_PAID " & _
                   "    ,Id " & _
                   "    ,GName " & _
                   "FROM practivity_master " & _
                   "JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
                   "JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "		 activityunkid " & _
                   "		,COUNT(activityunkid) AS Occ " & _
                   "		,SUM(activity_value) AS avalue " & _
                   "		,SUM(amount) AS amt " & _
                   "		,SUM(normal_amount) AS nAmt " & _
                   "		,SUM(ot_amount) AS oAmt " & _
                   "		,CASE WHEN SUM(normal_amount)<=0 THEN 0 ELSE SUM(normal_hrs) END AS nHrs " & _
                   "		,SUM(ot_hrs) AS oHrs "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "	FROM prpayactivity_tran " & _
                         " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = prpayactivity_tran.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "	WHERE CONVERT(CHAR(8),activity_date,112) BETWEEN @Date1 AND @Date2 " & _
                         "	AND isvoid = 0 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= " GROUP BY activityunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace(" AS Id,", ",").Replace(" AS GName", "")
            End If

            StrQ &= "  )AS A ON practivity_master.activityunkid = A.activityunkid " & _
                   "WHERE practivity_master.isactive = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            'Pinkal (06-Mar-2014) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrCurrFormat.Trim.Length <= 0 Then mstrCurrFormat = GUI.fmtCurrency
            If mstrCurrFormat.Trim.Length <= 0 Then mstrCurrFormat = strFmtCurrency
            'Sohail (21 Aug 2015) -- End

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("CODE")
                rpt_Rows.Item("Column2") = dtRow.Item("ACTIVITY")
                rpt_Rows.Item("Column3") = dtRow.Item("OCC")
                rpt_Rows.Item("Column4") = dtRow.Item("UNITS") & Space(2) & dtRow.Item("UOM")
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("AMTPAID")), mstrCurrFormat)
                rpt_Rows.Item("Column6") = dtRow.Item("AVG_UNIT")
                rpt_Rows.Item("Column7") = dtRow.Item("AVG_COUNT")
                rpt_Rows.Item("Column8") = dtRow.Item("NORMAL_HRS")
                rpt_Rows.Item("Column9") = dtRow.Item("OT_HRS")
                rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("NORMAL_AMT")), mstrCurrFormat)
                rpt_Rows.Item("Column11") = Format(CDec(dtRow.Item("OT_AMT")), mstrCurrFormat)
                rpt_Rows.Item("Column12") = Format(CDec(dtRow.Item("TOTAL_PAID")), mstrCurrFormat)


                If mintViewIndex > 0 Then
                    rpt_Rows.Item("Column13") = dtRow.Item("GName").ToString()
                    rpt_Rows.Item("Column14") = CInt(dsList.Tables("DataTable").Compute("SUM(OCC)", "Id = " & CInt(dtRow.Item("Id"))))
                    rpt_Rows.Item("Column15") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(AMTPAID)", "Id = " & CInt(dtRow.Item("Id")))), mstrCurrFormat)
                    rpt_Rows.Item("Column16") = CDec(dsList.Tables("DataTable").Compute("SUM(AVG_UNIT)", "Id = " & CInt(dtRow.Item("Id"))))
                    rpt_Rows.Item("Column17") = CDec(dsList.Tables("DataTable").Compute("SUM(AVG_COUNT)", "Id = " & CInt(dtRow.Item("Id"))))
                    rpt_Rows.Item("Column18") = CDec(dsList.Tables("DataTable").Compute("SUM(NORMAL_HRS)", "Id = " & CInt(dtRow.Item("Id"))))
                    rpt_Rows.Item("Column19") = CDec(dsList.Tables("DataTable").Compute("SUM(OT_HRS)", "Id = " & CInt(dtRow.Item("Id"))))
                    rpt_Rows.Item("Column20") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(NORMAL_AMT)", "Id = " & CInt(dtRow.Item("Id")))), mstrCurrFormat)
                    rpt_Rows.Item("Column21") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(OT_AMT)", "Id = " & CInt(dtRow.Item("Id")))), mstrCurrFormat)
                    rpt_Rows.Item("Column22") = Format(CDec(dsList.Tables("DataTable").Compute("SUM(TOTAL_PAID)", "Id = " & CInt(dtRow.Item("Id")))), mstrCurrFormat)
                End If

                rpt_Rows.Item("Column81") = dtRow.Item("OCC")
                rpt_Rows.Item("Column82") = dtRow.Item("AVG_UNIT")
                rpt_Rows.Item("Column83") = dtRow.Item("AVG_COUNT")
                rpt_Rows.Item("Column84") = dtRow.Item("NORMAL_HRS")
                rpt_Rows.Item("Column85") = dtRow.Item("OT_HRS")
                rpt_Rows.Item("Column86") = Format(CDec(dtRow.Item("AMTPAID")), mstrCurrFormat)
                rpt_Rows.Item("Column87") = Format(CDec(dtRow.Item("NORMAL_AMT")), mstrCurrFormat)
                rpt_Rows.Item("Column88") = Format(CDec(dtRow.Item("OT_AMT")), mstrCurrFormat)
                rpt_Rows.Item("Column89") = Format(CDec(dtRow.Item("TOTAL_PAID")), mstrCurrFormat)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptActivityDoneReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription" _
                                        )

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 301, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 302, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 303, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 304, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 200, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtActivity", Language.getMessage(mstrModuleName, 201, "Activity"))
            Call ReportFunction.TextChange(objRpt, "txtCount", Language.getMessage(mstrModuleName, 202, "Count"))
            Call ReportFunction.TextChange(objRpt, "txtUnits", Language.getMessage(mstrModuleName, 203, "Units"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 204, "Amount Paid"))
            Call ReportFunction.TextChange(objRpt, "txtAvgUnit", Language.getMessage(mstrModuleName, 205, "Av. Unit"))
            Call ReportFunction.TextChange(objRpt, "txtAvgCount", Language.getMessage(mstrModuleName, 206, "Av. M/Day"))
            Call ReportFunction.TextChange(objRpt, "txtNrmlHrs", Language.getMessage(mstrModuleName, 207, "Normal Hrs."))
            Call ReportFunction.TextChange(objRpt, "txtOTHrs", Language.getMessage(mstrModuleName, 208, "OT Hrs."))
            Call ReportFunction.TextChange(objRpt, "txtNrmlAmt", Language.getMessage(mstrModuleName, 209, "Normal Amount"))
            Call ReportFunction.TextChange(objRpt, "txtOTAmt", Language.getMessage(mstrModuleName, 210, "OT Amount"))
            Call ReportFunction.TextChange(objRpt, "txtTotalPaid", Language.getMessage(mstrModuleName, 211, "Total Paid"))

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 308, "Sub Total :"))
            'Pinkal (06-Mar-2014) -- End


            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 305, "Total :"))

            If rpt_Data.Tables("ArutiTable").Rows(0).Item("Column1").ToString.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtTotCount", rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", ""))
                Call ReportFunction.TextChange(objRpt, "txtTotAvgUnit", rpt_Data.Tables("ArutiTable").Compute("SUM(Column82)", ""))
                Call ReportFunction.TextChange(objRpt, "txtTotAvgCount", rpt_Data.Tables("ArutiTable").Compute("SUM(Column83)", ""))
                Call ReportFunction.TextChange(objRpt, "txtTotNrmlHrs", rpt_Data.Tables("ArutiTable").Compute("SUM(Column84)", ""))
                Call ReportFunction.TextChange(objRpt, "txtTotOTHrs", rpt_Data.Tables("ArutiTable").Compute("SUM(Column85)", ""))

                Call ReportFunction.TextChange(objRpt, "txtTotAmtPaid", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column86)", "")), mstrCurrFormat))
                Call ReportFunction.TextChange(objRpt, "txtTotNrmlAmt", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column87)", "")), mstrCurrFormat))
                Call ReportFunction.TextChange(objRpt, "txtTotOTAmt", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column88)", "")), mstrCurrFormat))
                Call ReportFunction.TextChange(objRpt, "txtTotTotalPaid", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column89)", "")), mstrCurrFormat))
            End If


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Pinkal (06-Mar-2014) -- End


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 306, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 307, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Date From :")
            Language.setMessage(mstrModuleName, 101, "to")
            Language.setMessage(mstrModuleName, 102, "Activity :")
            Language.setMessage(mstrModuleName, 103, "Measure :")
            Language.setMessage(mstrModuleName, 104, "Occurrence :")
            Language.setMessage(mstrModuleName, 105, "Amount Paid From :")
            Language.setMessage(mstrModuleName, 106, "Normal Hrs. From :")
            Language.setMessage(mstrModuleName, 107, "OT Hrs. From :")
            Language.setMessage(mstrModuleName, 108, "Total Paid From :")
            Language.setMessage(mstrModuleName, 109, "Order By :")
            Language.setMessage(mstrModuleName, 200, "Code")
            Language.setMessage(mstrModuleName, 201, "Activity")
            Language.setMessage(mstrModuleName, 202, "Count")
            Language.setMessage(mstrModuleName, 203, "Units")
            Language.setMessage(mstrModuleName, 204, "Amount Paid")
            Language.setMessage(mstrModuleName, 205, "Av. Unit")
            Language.setMessage(mstrModuleName, 206, "Av. M/Day")
            Language.setMessage(mstrModuleName, 207, "Normal Hrs.")
            Language.setMessage(mstrModuleName, 208, "OT Hrs.")
            Language.setMessage(mstrModuleName, 209, "Normal Amount")
            Language.setMessage(mstrModuleName, 210, "OT Amount")
            Language.setMessage(mstrModuleName, 211, "Total Paid")
            Language.setMessage(mstrModuleName, 301, "Prepared By :")
            Language.setMessage(mstrModuleName, 302, "Checked By :")
            Language.setMessage(mstrModuleName, 303, "Approved By :")
            Language.setMessage(mstrModuleName, 304, "Received By :")
            Language.setMessage(mstrModuleName, 305, "Total :")
            Language.setMessage(mstrModuleName, 306, "Printed By :")
            Language.setMessage(mstrModuleName, 307, "Printed Date :")
            Language.setMessage(mstrModuleName, 308, "Sub Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
