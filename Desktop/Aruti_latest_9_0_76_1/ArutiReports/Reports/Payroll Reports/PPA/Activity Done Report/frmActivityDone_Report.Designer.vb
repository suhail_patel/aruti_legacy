﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivityDone_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblTotalPaidFrm = New System.Windows.Forms.Label
        Me.chkIncludeZero = New System.Windows.Forms.CheckBox
        Me.txtAmtPaidFrm = New eZee.TextBox.NumericTextBox
        Me.txtTotalPaidFrm = New eZee.TextBox.NumericTextBox
        Me.txtCountTo = New eZee.TextBox.IntegerTextBox
        Me.lblOTHrsFrm = New System.Windows.Forms.Label
        Me.txtCountFrom = New eZee.TextBox.IntegerTextBox
        Me.txtOTHrsTo = New eZee.TextBox.NumericTextBox
        Me.lblCountTo = New System.Windows.Forms.Label
        Me.txtOTHrsFrm = New eZee.TextBox.NumericTextBox
        Me.lblCountFrom = New System.Windows.Forms.Label
        Me.lblNrmlHrsFrom = New System.Windows.Forms.Label
        Me.objbtnSearchActivity = New eZee.Common.eZeeGradientButton
        Me.txtTotalPaidTo = New eZee.TextBox.NumericTextBox
        Me.cboActivity = New System.Windows.Forms.ComboBox
        Me.txtNrmlHrsTo = New eZee.TextBox.NumericTextBox
        Me.lblActivity = New System.Windows.Forms.Label
        Me.lblAmtPaidFrm = New System.Windows.Forms.Label
        Me.objbtnSearchMeasures = New eZee.Common.eZeeGradientButton
        Me.lblAmtPaidTo = New System.Windows.Forms.Label
        Me.cboMeasures = New System.Windows.Forms.ComboBox
        Me.txtNrmlHrsFrm = New eZee.TextBox.NumericTextBox
        Me.lblMeasures = New System.Windows.Forms.Label
        Me.lblTotalPaidTo = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.lblNrmlHrsTo = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.txtAmtPaidTo = New eZee.TextBox.NumericTextBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.lblOTHrsTo = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 446)
        Me.NavPanel.Size = New System.Drawing.Size(710, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 347)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(400, 63)
        Me.gbSortBy.TabIndex = 23
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(370, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(85, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(99, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(265, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalPaidFrm)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeZero)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmtPaidFrm)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalPaidFrm)
        Me.gbFilterCriteria.Controls.Add(Me.txtCountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblOTHrsFrm)
        Me.gbFilterCriteria.Controls.Add(Me.txtCountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtOTHrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtOTHrsFrm)
        Me.gbFilterCriteria.Controls.Add(Me.lblCountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblNrmlHrsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchActivity)
        Me.gbFilterCriteria.Controls.Add(Me.txtTotalPaidTo)
        Me.gbFilterCriteria.Controls.Add(Me.cboActivity)
        Me.gbFilterCriteria.Controls.Add(Me.txtNrmlHrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblActivity)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmtPaidFrm)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchMeasures)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmtPaidTo)
        Me.gbFilterCriteria.Controls.Add(Me.cboMeasures)
        Me.gbFilterCriteria.Controls.Add(Me.txtNrmlHrsFrm)
        Me.gbFilterCriteria.Controls.Add(Me.lblMeasures)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalPaidTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblNrmlHrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate2)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmtPaidTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblOTHrsTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate1)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(400, 275)
        Me.gbFilterCriteria.TabIndex = 22
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalPaidFrm
        '
        Me.lblTotalPaidFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPaidFrm.Location = New System.Drawing.Point(8, 252)
        Me.lblTotalPaidFrm.Name = "lblTotalPaidFrm"
        Me.lblTotalPaidFrm.Size = New System.Drawing.Size(85, 15)
        Me.lblTotalPaidFrm.TabIndex = 161
        Me.lblTotalPaidFrm.Text = "Total Paid From"
        Me.lblTotalPaidFrm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeZero
        '
        Me.chkIncludeZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeZero.Location = New System.Drawing.Point(99, 142)
        Me.chkIncludeZero.Name = "chkIncludeZero"
        Me.chkIncludeZero.Size = New System.Drawing.Size(265, 16)
        Me.chkIncludeZero.TabIndex = 145
        Me.chkIncludeZero.Text = "Incude Zero in Filter"
        Me.chkIncludeZero.UseVisualStyleBackColor = True
        '
        'txtAmtPaidFrm
        '
        Me.txtAmtPaidFrm.AllowNegative = False
        Me.txtAmtPaidFrm.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmtPaidFrm.DigitsInGroup = 0
        Me.txtAmtPaidFrm.Flags = 65536
        Me.txtAmtPaidFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmtPaidFrm.Location = New System.Drawing.Point(99, 165)
        Me.txtAmtPaidFrm.MaxDecimalPlaces = 4
        Me.txtAmtPaidFrm.MaxWholeDigits = 21
        Me.txtAmtPaidFrm.Name = "txtAmtPaidFrm"
        Me.txtAmtPaidFrm.Prefix = ""
        Me.txtAmtPaidFrm.RangeMax = 1.7976931348623157E+308
        Me.txtAmtPaidFrm.RangeMin = -1.7976931348623157E+308
        Me.txtAmtPaidFrm.Size = New System.Drawing.Size(101, 21)
        Me.txtAmtPaidFrm.TabIndex = 146
        Me.txtAmtPaidFrm.Text = "0"
        Me.txtAmtPaidFrm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPaidFrm
        '
        Me.txtTotalPaidFrm.AllowNegative = False
        Me.txtTotalPaidFrm.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalPaidFrm.DigitsInGroup = 0
        Me.txtTotalPaidFrm.Flags = 65536
        Me.txtTotalPaidFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPaidFrm.Location = New System.Drawing.Point(99, 246)
        Me.txtTotalPaidFrm.MaxDecimalPlaces = 4
        Me.txtTotalPaidFrm.MaxWholeDigits = 21
        Me.txtTotalPaidFrm.Name = "txtTotalPaidFrm"
        Me.txtTotalPaidFrm.Prefix = ""
        Me.txtTotalPaidFrm.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPaidFrm.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPaidFrm.Size = New System.Drawing.Size(101, 21)
        Me.txtTotalPaidFrm.TabIndex = 152
        Me.txtTotalPaidFrm.Text = "0"
        Me.txtTotalPaidFrm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCountTo
        '
        Me.txtCountTo.AllowNegative = False
        Me.txtCountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCountTo.DigitsInGroup = 0
        Me.txtCountTo.Flags = 65536
        Me.txtCountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCountTo.Location = New System.Drawing.Point(263, 114)
        Me.txtCountTo.MaxDecimalPlaces = 0
        Me.txtCountTo.MaxWholeDigits = 9
        Me.txtCountTo.Name = "txtCountTo"
        Me.txtCountTo.Prefix = ""
        Me.txtCountTo.RangeMax = 2147483647
        Me.txtCountTo.RangeMin = -2147483648
        Me.txtCountTo.Size = New System.Drawing.Size(101, 21)
        Me.txtCountTo.TabIndex = 144
        Me.txtCountTo.Text = "0"
        Me.txtCountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOTHrsFrm
        '
        Me.lblOTHrsFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOTHrsFrm.Location = New System.Drawing.Point(8, 225)
        Me.lblOTHrsFrm.Name = "lblOTHrsFrm"
        Me.lblOTHrsFrm.Size = New System.Drawing.Size(85, 15)
        Me.lblOTHrsFrm.TabIndex = 160
        Me.lblOTHrsFrm.Text = "OT Hrs. From"
        Me.lblOTHrsFrm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCountFrom
        '
        Me.txtCountFrom.AllowNegative = False
        Me.txtCountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCountFrom.DigitsInGroup = 0
        Me.txtCountFrom.Flags = 65536
        Me.txtCountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCountFrom.Location = New System.Drawing.Point(99, 114)
        Me.txtCountFrom.MaxDecimalPlaces = 0
        Me.txtCountFrom.MaxWholeDigits = 9
        Me.txtCountFrom.Name = "txtCountFrom"
        Me.txtCountFrom.Prefix = ""
        Me.txtCountFrom.RangeMax = 2147483647
        Me.txtCountFrom.RangeMin = -2147483648
        Me.txtCountFrom.Size = New System.Drawing.Size(101, 21)
        Me.txtCountFrom.TabIndex = 144
        Me.txtCountFrom.Text = "0"
        Me.txtCountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOTHrsTo
        '
        Me.txtOTHrsTo.AllowNegative = False
        Me.txtOTHrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOTHrsTo.DigitsInGroup = 0
        Me.txtOTHrsTo.Flags = 65536
        Me.txtOTHrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOTHrsTo.Location = New System.Drawing.Point(263, 219)
        Me.txtOTHrsTo.MaxDecimalPlaces = 4
        Me.txtOTHrsTo.MaxWholeDigits = 21
        Me.txtOTHrsTo.Name = "txtOTHrsTo"
        Me.txtOTHrsTo.Prefix = ""
        Me.txtOTHrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtOTHrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtOTHrsTo.Size = New System.Drawing.Size(101, 21)
        Me.txtOTHrsTo.TabIndex = 151
        Me.txtOTHrsTo.Text = "0"
        Me.txtOTHrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCountTo
        '
        Me.lblCountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountTo.Location = New System.Drawing.Point(206, 117)
        Me.lblCountTo.Name = "lblCountTo"
        Me.lblCountTo.Size = New System.Drawing.Size(51, 15)
        Me.lblCountTo.TabIndex = 143
        Me.lblCountTo.Text = "To"
        Me.lblCountTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOTHrsFrm
        '
        Me.txtOTHrsFrm.AllowNegative = False
        Me.txtOTHrsFrm.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOTHrsFrm.DigitsInGroup = 0
        Me.txtOTHrsFrm.Flags = 65536
        Me.txtOTHrsFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOTHrsFrm.Location = New System.Drawing.Point(99, 219)
        Me.txtOTHrsFrm.MaxDecimalPlaces = 4
        Me.txtOTHrsFrm.MaxWholeDigits = 21
        Me.txtOTHrsFrm.Name = "txtOTHrsFrm"
        Me.txtOTHrsFrm.Prefix = ""
        Me.txtOTHrsFrm.RangeMax = 1.7976931348623157E+308
        Me.txtOTHrsFrm.RangeMin = -1.7976931348623157E+308
        Me.txtOTHrsFrm.Size = New System.Drawing.Size(101, 21)
        Me.txtOTHrsFrm.TabIndex = 150
        Me.txtOTHrsFrm.Text = "0"
        Me.txtOTHrsFrm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCountFrom
        '
        Me.lblCountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountFrom.Location = New System.Drawing.Point(8, 117)
        Me.lblCountFrom.Name = "lblCountFrom"
        Me.lblCountFrom.Size = New System.Drawing.Size(85, 15)
        Me.lblCountFrom.TabIndex = 142
        Me.lblCountFrom.Text = "Count From"
        Me.lblCountFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNrmlHrsFrom
        '
        Me.lblNrmlHrsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNrmlHrsFrom.Location = New System.Drawing.Point(8, 198)
        Me.lblNrmlHrsFrom.Name = "lblNrmlHrsFrom"
        Me.lblNrmlHrsFrom.Size = New System.Drawing.Size(85, 15)
        Me.lblNrmlHrsFrom.TabIndex = 159
        Me.lblNrmlHrsFrom.Text = "Nrml. Hrs. From"
        Me.lblNrmlHrsFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchActivity
        '
        Me.objbtnSearchActivity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchActivity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchActivity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchActivity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchActivity.BorderSelected = False
        Me.objbtnSearchActivity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchActivity.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchActivity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchActivity.Location = New System.Drawing.Point(370, 60)
        Me.objbtnSearchActivity.Name = "objbtnSearchActivity"
        Me.objbtnSearchActivity.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchActivity.TabIndex = 141
        '
        'txtTotalPaidTo
        '
        Me.txtTotalPaidTo.AllowNegative = False
        Me.txtTotalPaidTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalPaidTo.DigitsInGroup = 0
        Me.txtTotalPaidTo.Flags = 65536
        Me.txtTotalPaidTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPaidTo.Location = New System.Drawing.Point(263, 246)
        Me.txtTotalPaidTo.MaxDecimalPlaces = 4
        Me.txtTotalPaidTo.MaxWholeDigits = 21
        Me.txtTotalPaidTo.Name = "txtTotalPaidTo"
        Me.txtTotalPaidTo.Prefix = ""
        Me.txtTotalPaidTo.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPaidTo.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPaidTo.Size = New System.Drawing.Size(101, 21)
        Me.txtTotalPaidTo.TabIndex = 153
        Me.txtTotalPaidTo.Text = "0"
        Me.txtTotalPaidTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboActivity
        '
        Me.cboActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivity.DropDownWidth = 350
        Me.cboActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivity.FormattingEnabled = True
        Me.cboActivity.Location = New System.Drawing.Point(99, 60)
        Me.cboActivity.Name = "cboActivity"
        Me.cboActivity.Size = New System.Drawing.Size(265, 21)
        Me.cboActivity.TabIndex = 140
        '
        'txtNrmlHrsTo
        '
        Me.txtNrmlHrsTo.AllowNegative = False
        Me.txtNrmlHrsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNrmlHrsTo.DigitsInGroup = 0
        Me.txtNrmlHrsTo.Flags = 65536
        Me.txtNrmlHrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNrmlHrsTo.Location = New System.Drawing.Point(263, 192)
        Me.txtNrmlHrsTo.MaxDecimalPlaces = 4
        Me.txtNrmlHrsTo.MaxWholeDigits = 21
        Me.txtNrmlHrsTo.Name = "txtNrmlHrsTo"
        Me.txtNrmlHrsTo.Prefix = ""
        Me.txtNrmlHrsTo.RangeMax = 1.7976931348623157E+308
        Me.txtNrmlHrsTo.RangeMin = -1.7976931348623157E+308
        Me.txtNrmlHrsTo.Size = New System.Drawing.Size(101, 21)
        Me.txtNrmlHrsTo.TabIndex = 149
        Me.txtNrmlHrsTo.Text = "0"
        Me.txtNrmlHrsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblActivity
        '
        Me.lblActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivity.Location = New System.Drawing.Point(8, 63)
        Me.lblActivity.Name = "lblActivity"
        Me.lblActivity.Size = New System.Drawing.Size(85, 15)
        Me.lblActivity.TabIndex = 139
        Me.lblActivity.Text = "Activity"
        '
        'lblAmtPaidFrm
        '
        Me.lblAmtPaidFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmtPaidFrm.Location = New System.Drawing.Point(8, 168)
        Me.lblAmtPaidFrm.Name = "lblAmtPaidFrm"
        Me.lblAmtPaidFrm.Size = New System.Drawing.Size(85, 15)
        Me.lblAmtPaidFrm.TabIndex = 158
        Me.lblAmtPaidFrm.Text = "Amt. Paid From"
        Me.lblAmtPaidFrm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchMeasures
        '
        Me.objbtnSearchMeasures.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMeasures.BorderSelected = False
        Me.objbtnSearchMeasures.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchMeasures.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMeasures.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMeasures.Location = New System.Drawing.Point(370, 87)
        Me.objbtnSearchMeasures.Name = "objbtnSearchMeasures"
        Me.objbtnSearchMeasures.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMeasures.TabIndex = 138
        '
        'lblAmtPaidTo
        '
        Me.lblAmtPaidTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmtPaidTo.Location = New System.Drawing.Point(206, 168)
        Me.lblAmtPaidTo.Name = "lblAmtPaidTo"
        Me.lblAmtPaidTo.Size = New System.Drawing.Size(51, 15)
        Me.lblAmtPaidTo.TabIndex = 154
        Me.lblAmtPaidTo.Text = "To"
        Me.lblAmtPaidTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMeasures
        '
        Me.cboMeasures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMeasures.DropDownWidth = 350
        Me.cboMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMeasures.FormattingEnabled = True
        Me.cboMeasures.Location = New System.Drawing.Point(99, 87)
        Me.cboMeasures.Name = "cboMeasures"
        Me.cboMeasures.Size = New System.Drawing.Size(265, 21)
        Me.cboMeasures.TabIndex = 137
        '
        'txtNrmlHrsFrm
        '
        Me.txtNrmlHrsFrm.AllowNegative = False
        Me.txtNrmlHrsFrm.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNrmlHrsFrm.DigitsInGroup = 0
        Me.txtNrmlHrsFrm.Flags = 65536
        Me.txtNrmlHrsFrm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNrmlHrsFrm.Location = New System.Drawing.Point(99, 192)
        Me.txtNrmlHrsFrm.MaxDecimalPlaces = 4
        Me.txtNrmlHrsFrm.MaxWholeDigits = 21
        Me.txtNrmlHrsFrm.Name = "txtNrmlHrsFrm"
        Me.txtNrmlHrsFrm.Prefix = ""
        Me.txtNrmlHrsFrm.RangeMax = 1.7976931348623157E+308
        Me.txtNrmlHrsFrm.RangeMin = -1.7976931348623157E+308
        Me.txtNrmlHrsFrm.Size = New System.Drawing.Size(101, 21)
        Me.txtNrmlHrsFrm.TabIndex = 148
        Me.txtNrmlHrsFrm.Text = "0"
        Me.txtNrmlHrsFrm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMeasures
        '
        Me.lblMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeasures.Location = New System.Drawing.Point(8, 89)
        Me.lblMeasures.Name = "lblMeasures"
        Me.lblMeasures.Size = New System.Drawing.Size(85, 15)
        Me.lblMeasures.TabIndex = 136
        Me.lblMeasures.Text = "Measures"
        Me.lblMeasures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalPaidTo
        '
        Me.lblTotalPaidTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPaidTo.Location = New System.Drawing.Point(206, 249)
        Me.lblTotalPaidTo.Name = "lblTotalPaidTo"
        Me.lblTotalPaidTo.Size = New System.Drawing.Size(51, 15)
        Me.lblTotalPaidTo.TabIndex = 157
        Me.lblTotalPaidTo.Text = "To"
        Me.lblTotalPaidTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(206, 36)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(51, 15)
        Me.lblTo.TabIndex = 82
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNrmlHrsTo
        '
        Me.lblNrmlHrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNrmlHrsTo.Location = New System.Drawing.Point(206, 195)
        Me.lblNrmlHrsTo.Name = "lblNrmlHrsTo"
        Me.lblNrmlHrsTo.Size = New System.Drawing.Size(51, 15)
        Me.lblNrmlHrsTo.TabIndex = 155
        Me.lblNrmlHrsTo.Text = "To"
        Me.lblNrmlHrsTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(263, 33)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(101, 21)
        Me.dtpDate2.TabIndex = 81
        '
        'txtAmtPaidTo
        '
        Me.txtAmtPaidTo.AllowNegative = False
        Me.txtAmtPaidTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmtPaidTo.DigitsInGroup = 0
        Me.txtAmtPaidTo.Flags = 65536
        Me.txtAmtPaidTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmtPaidTo.Location = New System.Drawing.Point(263, 165)
        Me.txtAmtPaidTo.MaxDecimalPlaces = 4
        Me.txtAmtPaidTo.MaxWholeDigits = 21
        Me.txtAmtPaidTo.Name = "txtAmtPaidTo"
        Me.txtAmtPaidTo.Prefix = ""
        Me.txtAmtPaidTo.RangeMax = 1.7976931348623157E+308
        Me.txtAmtPaidTo.RangeMin = -1.7976931348623157E+308
        Me.txtAmtPaidTo.Size = New System.Drawing.Size(101, 21)
        Me.txtAmtPaidTo.TabIndex = 147
        Me.txtAmtPaidTo.Text = "0"
        Me.txtAmtPaidTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(85, 15)
        Me.lblFromDate.TabIndex = 80
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOTHrsTo
        '
        Me.lblOTHrsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOTHrsTo.Location = New System.Drawing.Point(206, 222)
        Me.lblOTHrsTo.Name = "lblOTHrsTo"
        Me.lblOTHrsTo.Size = New System.Drawing.Size(51, 15)
        Me.lblOTHrsTo.TabIndex = 156
        Me.lblOTHrsTo.Text = "To"
        Me.lblOTHrsTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(99, 33)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(101, 21)
        Me.dtpDate1.TabIndex = 79
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(303, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 197
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmActivityDone_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(710, 501)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmActivityDone_Report"
        Me.Text = "frmActivityDone_Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchMeasures As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMeasures As System.Windows.Forms.ComboBox
    Friend WithEvents lblMeasures As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchActivity As eZee.Common.eZeeGradientButton
    Friend WithEvents cboActivity As System.Windows.Forms.ComboBox
    Friend WithEvents lblActivity As System.Windows.Forms.Label
    Friend WithEvents txtCountTo As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtCountFrom As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblCountTo As System.Windows.Forms.Label
    Friend WithEvents lblCountFrom As System.Windows.Forms.Label
    Friend WithEvents chkIncludeZero As System.Windows.Forms.CheckBox
    Friend WithEvents txtAmtPaidTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtAmtPaidFrm As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalPaidFrm As System.Windows.Forms.Label
    Friend WithEvents lblOTHrsFrm As System.Windows.Forms.Label
    Friend WithEvents lblNrmlHrsFrom As System.Windows.Forms.Label
    Friend WithEvents lblAmtPaidFrm As System.Windows.Forms.Label
    Friend WithEvents lblTotalPaidTo As System.Windows.Forms.Label
    Friend WithEvents lblOTHrsTo As System.Windows.Forms.Label
    Friend WithEvents lblNrmlHrsTo As System.Windows.Forms.Label
    Friend WithEvents lblAmtPaidTo As System.Windows.Forms.Label
    Friend WithEvents txtTotalPaidTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtOTHrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtTotalPaidFrm As eZee.TextBox.NumericTextBox
    Friend WithEvents txtOTHrsFrm As eZee.TextBox.NumericTextBox
    Friend WithEvents txtNrmlHrsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtNrmlHrsFrm As eZee.TextBox.NumericTextBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
End Class
