'************************************************************************************************************************************
'Class Name :clsBBL_Loan_Report.vb
'Purpose    :
'Date       :08 May 2012
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsBBL_Loan_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsBBL_Loan_Report"
    Private mstrReportId As String = enArutiReport.BBL_Loan_Report  '89
    Dim objDataOperation As clsDataOperation

#Region " ENUM "

    Public Enum enDisplayMode
        BASIC_SALARY = 1
        GROSS_SALARY = 2
    End Enum

#End Region

#Region "Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mintDisplayModeSetting As Integer = 1
    'Sohail (29 Apr 2020) -- Start
    'Ifakara Enhancement # 0004668 : Advance Approve Form.
    Private mblnIsLoan As Boolean = True
    'Sohail (29 Apr 2020) -- End

    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'S.SANDEEP [ 29 May 2013 ] -- END

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mdtDbStartDate As Date = Nothing
    'Nilay (10-Oct-2015) -- End

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Private mintProcessPendingLoanunkId As Integer = -1
    Private mintLoanSchemeId As Integer = -1
    Private mstrLoanApplicationNo As String = ""
    Private mstrLoanVoucherNo As String = ""
    'Hemant (12 Nov 2021) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Public WriteOnly Property _LoanSchemeId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPendingLoanunkId() As Integer
        Set(ByVal value As Integer)
            mintProcessPendingLoanunkId = value
        End Set
    End Property

    Public WriteOnly Property _LoanApplicationNo() As String
        Set(ByVal value As String)
            mstrLoanApplicationNo = value
        End Set
    End Property

    Public WriteOnly Property _LoanVoucherNo() As String
        Set(ByVal value As String)
            mstrLoanVoucherNo = value
        End Set
    End Property
    'Hemant (12 Nov 2021) -- End

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _DisplayModeSetting() As Integer
        Set(ByVal value As Integer)
            mintDisplayModeSetting = value
        End Set
    End Property

    'Sohail (29 Apr 2020) -- Start
    'Ifakara Enhancement # 0004668 : Advance Approve Form.
    Public WriteOnly Property _IsLoan() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLoan = value
        End Set
    End Property
    'Sohail (29 Apr 2020) -- End

    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 29 May 2013 ] -- END

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public WriteOnly Property _DbStartDate() As Date
        Set(ByVal value As Date)
            mdtDbStartDate = value
        End Set
    End Property
    'Nilay (10-Oct-2015) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mblnIncludeInactiveEmp = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        Try

            ''S.SANDEEP [ 29 May 2013 ] -- START
            ''ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'If mintCompanyUnkid <= 0 Then
            '    mintCompanyUnkid = Company._Object._Companyunkid
            'End If
            'Company._Object._Companyunkid = mintCompanyUnkid
            'ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            'If mintUserUnkid <= 0 Then
            '    mintUserUnkid = User._Object._Userunkid
            'End If
            'User._Object._Userunkid = mintUserUnkid
            ''S.SANDEEP [ 29 May 2013 ] -- END

            'objRpt = Generate_DetailReport()

            ''S.SANDEEP [ 29 May 2013 ] -- START
            ''ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'Rpt = objRpt
            ''S.SANDEEP [ 29 May 2013 ] -- END

            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try

            If mblnIsLoan = True Then 'Sohail (29 Apr 2020)
                objRpt = Generate_DetailReport(xDatabaseName, _
                                               xUserUnkid, _
                                               xYearUnkid, _
                                               xCompanyUnkid, _
                                               xPeriodStart, _
                                               xPeriodStart, _
                                               xUserModeSetting, _
                                               True)

                'Sohail (29 Apr 2020) -- Start
                'Ifakara Enhancement # 0004668 : Advance Approve Form.
            Else
                objRpt = Generate_AdvanceReport(xDatabaseName, _
                                               xUserUnkid, _
                                               xYearUnkid, _
                                               xCompanyUnkid, _
                                               xPeriodStart, _
                                               xPeriodStart, _
                                               xUserModeSetting, _
                                               True)
            End If
            'Sohail (29 Apr 2020) -- End

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- End


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintProcessPendingLoanunkId > 0 Then
                Me._FilterQuery &= " AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid "
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanunkId)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Hemant (12 Nov 2021) -- End


#End Region

#Region " Report Generation "

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Nilay (10-Oct-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsTranList As New DataSet
        Dim exForce As Exception
        Dim rpt_OldLoan As ArutiReport.Designer.dsArutiReport
        Dim rpt_NewLoan As ArutiReport.Designer.dsArutiReport
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        objDataOperation = New clsDataOperation
        Dim mDecGross_Salary As Decimal = 0
        Dim blnIsDeductedOldLoan As Boolean = False
        Dim blnIsDeductedNewLoan As Boolean = False
        'S.SANDEEP [28 AUG 2015] -- START
        Dim strCurrencySign As String = String.Empty
        'S.SANDEEP [28 AUG 2015] -- END


        'Pinkal (14-Apr-2018) -- Start
        'Enhancement - (Ref # 204) Loan Approvers list should be shown in the loan approval form.
        Dim rpt_ApproverData As ArutiReport.Designer.dsArutiReport
        'Pinkal (14-Apr-2018) -- End

        Try


            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Nilay (10-Oct-2015) -- End

            objRpt = New ArutiReport.Designer.rptBBL_Loan_Report

            '************************** MAIN PART ************************** START

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT TOP 1 " & _
            '       "     hrdepartment_master.name AS DEPARTMENT " & _
            '       "    ,CONVERT(CHAR(8),application_date,112) AS DATE " & _
            '       "    ,application_no AS LOAN_REQUEST_NO " & _
            '       "    ,lnloan_scheme_master.name AS LOAN_TYPE " & _
            '       "    ,employeecode AS ID_NO " & _
            '       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EMP_NAME " & _
            '       "    ,CONVERT(CHAR(8),appointeddate,112) AS DOJ " & _
            '       "    ,loan_amount AS LOAN_AMOUNT " & _
            '       "    ,emp_remark AS PURPOSE " & _
            '       "    ,MONTHLY_SALARY AS MONTHLY_SALARY " & _
            '       "    ,lnloan_process_pending_loan.employeeunkid " & _
            '       "    ,processpendingloanunkid " & _
            '       "    ,isexternal_entity " & _
            '       "FROM lnloan_process_pending_loan " & _
            '       "    JOIN lnloan_scheme_master ON lnloan_process_pending_loan.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
            '       "    JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
            '       "    JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '       "    JOIN " & _
            '       "    ( " & _
            '       "        SELECT " & _
            '       "             MS.employeeunkid " & _
            '       "            ,MS.MONTHLY_SALARY " & _
            '       "        FROM " & _
            '       "        ( " & _
            '       "            SELECT " & _
            '       "                 employeeunkid " & _
            '       "                ,newscale AS MONTHLY_SALARY " & _
            '       "                ,ROW_NUMBER() OVER( PARTITION BY employeeunkid ORDER BY incrementdate DESC) AS SNO " & _
            '       "            FROM prsalaryincrement_tran " & _
            '       "            WHERE CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) <= CONVERT(CHAR(8),GETDATE(),112) " & _
            '       "                AND employeeunkid = " & mintEmployeeId & " " & _
            '       "                AND prsalaryincrement_tran.isapproved = 1 " & _
            '       "        ) AS MS WHERE MS.SNO = 1 " & _
            '       "    ) AS A ON A.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
            '       "WHERE loan_statusunkid = 4 AND isloan = 1 AND lnloan_process_pending_loan.employeeunkid = " & mintEmployeeId & " "
            ''       'Sohail (24 Sep 2012) - [isapproved]

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If

            StrQ = "SELECT TOP 1 " & _
                   "     ISNULL(DM.name,'') AS DEPARTMENT " & _
                   "    ,CONVERT(CHAR(8),application_date,112) AS DATE " & _
                   "    ,application_no AS LOAN_REQUEST_NO " & _
                   "    ,lnloan_scheme_master.name AS LOAN_TYPE " & _
                   "    ,employeecode AS ID_NO " & _
                   "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EMP_NAME " & _
                   "    ,CONVERT(CHAR(8),appointeddate,112) AS DOJ " & _
                   "    ,loan_amount AS LOAN_AMOUNT " & _
                   "    ,emp_remark AS PURPOSE " & _
                   "    ,ISNULL(MONTHLY_SALARY,0) AS MONTHLY_SALARY " & _
                   "    ,lnloan_process_pending_loan.employeeunkid " & _
                   "    ,processpendingloanunkid " & _
                   "    ,isexternal_entity " & _
                   "FROM lnloan_process_pending_loan " & _
                   "    JOIN lnloan_scheme_master ON lnloan_process_pending_loan.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                   "    JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             MS.employeeunkid " & _
                   "            ,MS.MONTHLY_SALARY " & _
                   "        FROM " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 employeeunkid " & _
                   "                ,newscale AS MONTHLY_SALARY " & _
                   "                ,ROW_NUMBER() OVER( PARTITION BY employeeunkid ORDER BY incrementdate DESC) AS SNO " & _
                   "            FROM prsalaryincrement_tran " & _
                   "            WHERE CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   "                AND employeeunkid = " & mintEmployeeId & " " & _
                   "                AND prsalaryincrement_tran.isapproved = 1 " & _
                   "        ) AS MS WHERE MS.SNO = 1 " & _
                   "    ) AS A ON A.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             departmentunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   "    ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "    LEFT JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= "WHERE lnloan_process_pending_loan.isvoid = 0 AND loan_statusunkid = 4 AND isloan = 1 AND lnloan_process_pending_loan.employeeunkid = " & mintEmployeeId & " "

            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery
            'Hemant (12 Nov 2021) -- End

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            'S.SANDEEP [04-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
            ' ---- REMOVED  --> xPeriodEnd
            ' ---- ADDED --> eZeeDate.convertDate(xPeriodEnd).ToString()
            'S.SANDEEP [04-May-2018] -- END


            'Nilay (10-Oct-2015) -- End

            StrQ &= "ORDER BY processpendingloanunkid DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("DataTable").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Sorry, No Loan Information Found for Selected Employee."), enMsgBoxStyle.Information)
                Return Nothing
            End If

            '************************** GROSS_SAL_PART -- START
            If mintDisplayModeSetting = enDisplayMode.GROSS_SALARY Then
                StrQ = "SELECT " & _
                       "	ISNULL(SUM(amount),0) AS Amount " & _
                       "FROM vwPayroll " & _
                       "WHERE trnheadtype_id =  1 " & _
                       "AND payperiodunkid IN (SELECT TOP 1 periodunkid FROM cfcommon_period_tran WHERE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' " & _
                       "	AND yearunkid = " & xYearUnkid & " AND modulerefid = 1 ORDER BY start_date DESC) " & _
                       "AND employeeunkid  = " & mintEmployeeId & " "
                'Nilay (10-Oct-2015) -- FinancialYear._Object._YearUnkid

                dsTranList = objDataOperation.ExecQuery(StrQ, "Sal")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsTranList.Tables("Sal").Rows.Count > 0 Then
                    mDecGross_Salary = dsTranList.Tables("Sal").Rows(0)("Amount")
                End If
            End If
            '************************** GROSS_SAL_PART -- END

            '************************** MAIN PART ************************** END

            '************************** OLD LOAN STRUCTURE ************************** START
            rpt_OldLoan = New ArutiReport.Designer.dsArutiReport
            If dsList.Tables("DataTable").Rows.Count > 0 Then

                'S.SANDEEP [28 AUG 2015] -- START
                'StrQ = "SELECT " & _
                '       "     cfcommon_period_tran.period_name AS DEDUCTION_MONTH " & _
                '       "    ,emi_amount AS DEDUCTION " & _
                '       "    ,amount AS DEDUCTED " & _
                '       "    ,0 AS OUTSTANDING " & _
                '       "    ,CASE WHEN isbrought_forward = 0 THEN net_amount ELSE balance_amount END AS REMAINING_BALANCE " & _
                '       "    ,emi_tenure " & _
                '       "FROM prpayrollprocess_tran " & _
                '       "    JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                '       "    JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '       "    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 1 " & _
                '       "WHERE prpayrollprocess_tran.loanadvancetranunkid = (SELECT TOP 1 loanadvancetranunkid " & _
                '       "FROM lnloan_advance_tran " & _
                '       "WHERE employeeunkid = '" & mintEmployeeId & "' AND isloan = 1 AND processpendingloanunkid <> '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' " & _
                '       "ORDER BY loanadvancetranunkid DESC) AND prpayrollprocess_tran.isvoid = 0 "

                StrQ = "SELECT " & _
                       "     cfcommon_period_tran.period_name AS DEDUCTION_MONTH " & _
                       "    ,LEMI.emi_amount AS DEDUCTION " & _
                       "    ,amount AS DEDUCTED " & _
                       "    ,0 AS OUTSTANDING " & _
                       "    ,LBAL.bf_amount AS REMAINING_BALANCE " & _
                       "    ,LEMI.emi_tenure " & _
                       "FROM prpayrollprocess_tran " & _
                       "    JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                       "JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         emi_amount " & _
                       "        ,emi_tenure " & _
                       "        ,basecurrency_amount " & _
                       "        ,CONVERT(CHAR(8),effectivedate,112) AS effectivedate " & _
                       "        ,loanadvancetranunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY effectivedate ASC) AS RNO " & _
                       "    FROM lnloan_emitenure_tran " & _
                       "    WHERE isvoid = 0 " & _
                       ") AS LEMI ON LEMI.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid AND LEMI.RNO = 1 " & _
                       "JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         bf_amount " & _
                       "        ,loanadvancetranunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY loanadvancetranunkid ORDER BY end_date ASC) AS rno " & _
                       "    FROM lnloan_balance_tran " & _
                       "    WHERE isvoid = 0 " & _
                       ") AS LBAL ON LBAL.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid AND LBAL.rno = 1 " & _
                       "    JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 1 " & _
                       "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                       "    AND prpayrollprocess_tran.loanadvancetranunkid = " & _
                       "    ( " & _
                       "        SELECT TOP 1 loanadvancetranunkid " & _
                       "FROM lnloan_advance_tran " & _
                       "WHERE lnloan_advance_tran.isvoid = 0 AND employeeunkid = '" & mintEmployeeId & "' AND isloan = 1 AND processpendingloanunkid <> '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' " & _
                       "        ORDER BY loanadvancetranunkid DESC " & _
                       "    ) "
                'S.SANDEEP [28 AUG 2015] -- END

                dsTranList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtTmp() As DataRow = Nothing
                dtTmp = dsTranList.Tables("DataTable").Select("DEDUCTED > 0")
                If dtTmp.Length > 0 Then
                    blnIsDeductedOldLoan = True
                End If

                Dim mDecRemaining_Balance As Decimal = 0
                For Each dtRow As DataRow In dsTranList.Tables("DataTable").Rows
                    Dim rpt_Row As DataRow = rpt_OldLoan.Tables("ArutiTable").NewRow

                    If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dtRow.Item("REMAINING_BALANCE")

                    rpt_Row.Item("Column1") = dtRow.Item("DEDUCTION_MONTH")

                    rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("DEDUCTION")), GUI.fmtCurrency)
                    rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))

                    rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("DEDUCTED")), GUI.fmtCurrency)
                    rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))

                    rpt_Row.Item("Column4") = Format(CDec(CDec(mDecRemaining_Balance) - CDec(dtRow.Item("DEDUCTED"))), GUI.fmtCurrency)
                    rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))

                    rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                    rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))

                    mDecRemaining_Balance = CDec(mDecRemaining_Balance) - CDec(dtRow.Item("DEDUCTED"))

                    rpt_OldLoan.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                objRpt.Subreports("rptOldLoan").SetDataSource(rpt_OldLoan)

            End If
            '************************** OLD LOAN STRUCTURE ************************** END

            '************************** NEW LOAN STRUCTURE ************************** START
            rpt_NewLoan = New ArutiReport.Designer.dsArutiReport
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                'S.SANDEEP [28 AUG 2015] -- START
                'StrQ = "SELECT " & _
                '      "	 CASE WHEN ISNULL(prtnaleave_tran.payperiodunkid,0) = 0 THEN lnloan_advance_tran.deductionperiodunkid ELSE prtnaleave_tran.payperiodunkid END AS periodunkid " & _
                '      "	,emi_amount AS DEDUCTION " & _
                '      "	,ISNULL(prpayrollprocess_tran.amount,0) AS DEDUCTED " & _
                '      "	,0 AS OUTSTANDING " & _
                '      "	,emi_tenure AS NO_OF_INSTALLMENTS " & _
                '      "	,CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) AS DATE " & _
                '      "	,lnloan_advance_tran.loan_amount AS APPROVED_AMOUNT " & _
                '      "	,CASE WHEN isbrought_forward = 0 THEN net_amount ELSE balance_amount END AS REMAINING_BALANCE " & _
                '      "	,loanvoucher_no AS LOAN_VOC_NO " & _
                '      "	,CASE WHEN lnloan_process_pending_loan.isexternal_entity = 0 THEN " & _
                '      "			CASE WHEN paymentmode = 1 THEN @CASH " & _
                '      "				 WHEN paymentmode = 2 THEN @CHEQUE " & _
                '      "				 WHEN paymentmode = 3 THEN @TRANSFER " & _
                '      "			END " & _
                '      "		  WHEN lnloan_process_pending_loan.isexternal_entity = 1 THEN @EXTERNAL " & _
                '      "	 END AS MODE " & _
                '      "FROM lnloan_advance_tran " & _
                '      "	JOIN lnloan_process_pending_loan ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                '      "	LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid AND referenceid = 1 " & _
                '      "	LEFT JOIN prpayrollprocess_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid AND prpayrollprocess_tran.isvoid = 0 " & _
                '      "	LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND prtnaleave_tran.isvoid = 0 " & _
                '      "WHERE lnloan_advance_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "

                StrQ = "SELECT " & _
                        "	 CASE WHEN ISNULL(prtnaleave_tran.payperiodunkid,0) = 0 THEN lnloan_advance_tran.deductionperiodunkid ELSE prtnaleave_tran.payperiodunkid END AS periodunkid " & _
                        "	,LEMI.emi_amount AS DEDUCTION " & _
                       "	,ISNULL(prpayrollprocess_tran.amount,0) AS DEDUCTED " & _
                       "	,0 AS OUTSTANDING " & _
                        "	,LEMI.emi_tenure AS NO_OF_INSTALLMENTS " & _
                       "	,CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) AS DATE " & _
                       "	,lnloan_advance_tran.loan_amount AS APPROVED_AMOUNT " & _
                        "	,CASE WHEN ISNULL(LBAL.bf_amount,0) <= 0 THEN net_amount ELSE ISNULL(LBAL.bf_amount,0) END AS REMAINING_BALANCE " & _
                       "	,loanvoucher_no AS LOAN_VOC_NO " & _
                       "	,CASE WHEN lnloan_process_pending_loan.isexternal_entity = 0 THEN " & _
                       "			CASE WHEN paymentmode = 1 THEN @CASH " & _
                       "				 WHEN paymentmode = 2 THEN @CHEQUE " & _
                       "				 WHEN paymentmode = 3 THEN @TRANSFER " & _
                       "			END " & _
                       "		  WHEN lnloan_process_pending_loan.isexternal_entity = 1 THEN @EXTERNAL " & _
                       "	 END AS MODE " & _
                       "FROM lnloan_advance_tran " & _
                        "	JOIN " & _
                        "	( " & _
                        "		SELECT " & _
                        "			 emi_amount " & _
                        "			,emi_tenure " & _
                        "			,basecurrency_amount " & _
                        "			,CONVERT(CHAR(8),effectivedate,112) AS effectivedate " & _
                        "			,loanadvancetranunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY effectivedate ASC) AS RNO " & _
                        "		FROM lnloan_emitenure_tran " & _
                        "		WHERE isvoid = 0 " & _
                        "	) AS LEMI ON LEMI.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid AND LEMI.RNO = 1 " & _
                        "	LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            bf_amount " & _
                        "           ,loanadvancetranunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY loanadvancetranunkid ORDER BY end_date ASC) AS rno " & _
                        "       FROM lnloan_balance_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "    ) AS LBAL ON LBAL.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid AND LBAL.rno = 1 " & _
                       "	JOIN lnloan_process_pending_loan ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                       "	LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid AND referenceid = 1 " & _
                       "	LEFT JOIN prpayrollprocess_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid AND prpayrollprocess_tran.isvoid = 0 " & _
                       "	LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND prtnaleave_tran.isvoid = 0 " & _
                       "WHERE lnloan_advance_tran.isvoid= 0 AND lnloan_advance_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "
                'S.SANDEEP [28 AUG 2015] -- END

                objDataOperation.AddParameter("@CASH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 43, "CASH"))
                objDataOperation.AddParameter("@CHEQUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "CHEQUE"))
                objDataOperation.AddParameter("@TRANSFER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 45, "TRANSFER"))
                objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "EXTERNAL"))

                dsTranList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtTable As DataTable = Nothing
                Dim dsPeriod As New DataSet
                Dim objPeriod As New clscommom_period_Tran
                Dim iCnt As Integer = 1
                Dim iInstallment As Integer = 0

                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", False)
                dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                     xYearUnkid, _
                                                     xDatabaseName, _
                                                     mdtDbStartDate, _
                                                     "List", False)
                'Nilay (10-Oct-2015) -- End

                If dsPeriod.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, There is no Open period found."), enMsgBoxStyle.Information)
                    Return Nothing
                End If

                If dsTranList.Tables("DataTable").Rows.Count > 0 Then
                    iInstallment = dsTranList.Tables("DataTable").Rows(0)("NO_OF_INSTALLMENTS")
                    dtTable = New DataView(dsPeriod.Tables("List"), "periodunkid > = " & dsTranList.Tables("DataTable").Rows(0)("periodunkid"), "", DataViewRowState.CurrentRows).ToTable
                End If

                Dim dtTmp() As DataRow = Nothing
                dtTmp = dsTranList.Tables("DataTable").Select("DEDUCTED > 0")
                If dtTmp.Length > 0 Then
                    blnIsDeductedNewLoan = True
                End If


                Dim mDecRemaining_Balance As Decimal = 0
                Dim mdecDeductionAmount As Decimal = 0

                If dsTranList.Tables("DataTable").Rows.Count > 0 AndAlso dtTable IsNot Nothing Then
                    For i As Integer = 0 To iInstallment - 1
                        Dim rpt_Row As DataRow = rpt_NewLoan.Tables("ArutiTable").NewRow
                        If i <= dtTable.Rows.Count - 1 Then
                            rpt_Row.Item("Column1") = i + 1.ToString & ". " & dtTable.Rows(i).Item("name")
                        Else
                            rpt_Row.Item("Column1") = i + 1.ToString & ". " & " -- "
                        End If

                        If mdecDeductionAmount <= 0 Then mdecDeductionAmount = dsTranList.Tables("DataTable").Rows(0)("DEDUCTION")
                        If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dsTranList.Tables("DataTable").Rows(0)("REMAINING_BALANCE")
                        If i <= dsTranList.Tables("DataTable").Rows.Count - 1 Then

                            rpt_Row.Item("Column2") = Format(CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTION")), GUI.fmtCurrency)
                            rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))

                            rpt_Row.Item("Column3") = Format(CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED")), GUI.fmtCurrency)
                            rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))

                            If CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED")) = 0 Then
                                rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            Else
                                rpt_Row.Item("Column4") = Format(CDec(CDec(mDecRemaining_Balance) - CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED"))), GUI.fmtCurrency)
                            End If
                            rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))

                            rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
                            mDecRemaining_Balance = CDec(mDecRemaining_Balance) - CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED"))
                        Else
                            blnIsDeductedNewLoan = False

                            rpt_Row.Item("Column2") = Format(CDec(mdecDeductionAmount), GUI.fmtCurrency)
                            rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))
                            rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
                            rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))
                            rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))
                            rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
                        End If
                        rpt_NewLoan.Tables("ArutiTable").Rows.Add(rpt_Row)
                    Next

                    '        For Each dtRow As DataRow In dtTable.Rows
                    '            If iCnt > iInstallment Then Exit For
                    '            Dim rpt_Row As DataRow = rpt_NewLoan.Tables("ArutiTable").NewRow
                    '            rpt_Row.Item("Column1") = dtRow.Item("name")
                    '            Dim dTemp() As DataRow = dsTranList.Tables("DataTable").Select("periodunkid = '" & dtRow.Item("periodunkid") & "'")
                    '            If dTemp.Length > 0 Then
                    '                If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dTemp(0).Item("REMAINING_BALANCE")
                    '                If mdecDeductionAmount <= 0 Then mdecDeductionAmount = dTemp(0).Item("DEDUCTION")

                    '                rpt_Row.Item("Column2") = Format(CDec(dTemp(0).Item("DEDUCTION")), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))

                    '                rpt_Row.Item("Column3") = Format(CDec(dTemp(0).Item("DEDUCTED")), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))

                    '                If CDec(dTemp(0).Item("DEDUCTED")) = 0 Then
                    '                    rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                    '                Else
                    '                    rpt_Row.Item("Column4") = Format(CDec(CDec(mDecRemaining_Balance) - CDec(dTemp(0).Item("DEDUCTED"))), GUI.fmtCurrency)
                    '                End If
                    '                rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))

                    '                rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
                    '                mDecRemaining_Balance = CDec(mDecRemaining_Balance) - CDec(dTemp(0).Item("DEDUCTED"))
                    '            Else
                    '                If dTemp.Length > 0 Then
                    '                    If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dTemp(0).Item("REMAINING_BALANCE")
                    '                End If

                    '                blnIsDeductedNewLoan = False

                    '                rpt_Row.Item("Column2") = Format(CDec(mdecDeductionAmount), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))
                    '                rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))
                    '                rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))
                    '                rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                    '                rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
                    '            End If
                    '            rpt_NewLoan.Tables("ArutiTable").Rows.Add(rpt_Row)
                    '            iCnt += 1
                    '        Next
                End If
                objRpt.Subreports("rptNewLoan").SetDataSource(rpt_NewLoan)
            End If

            '    If dsTranList.Tables("DataTable").Rows.Count > 0 AndAlso dtTable IsNot Nothing Then
            '        For Each dtRow As DataRow In dtTable.Rows
            '            If iCnt > iInstallment Then Exit For
            '            Dim rpt_Row As DataRow = rpt_NewLoan.Tables("ArutiTable").NewRow
            '            rpt_Row.Item("Column1") = dtRow.Item("name")
            '            Dim dTemp() As DataRow = dsTranList.Tables("DataTable").Select("periodunkid = '" & dtRow.Item("periodunkid") & "'")
            '            If dTemp.Length > 0 Then
            '                If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dTemp(0).Item("REMAINING_BALANCE")
            '                If mdecDeductionAmount <= 0 Then mdecDeductionAmount = dTemp(0).Item("DEDUCTION")

            '                rpt_Row.Item("Column2") = Format(CDec(dTemp(0).Item("DEDUCTION")), GUI.fmtCurrency)
            '                rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))

            '                rpt_Row.Item("Column3") = Format(CDec(dTemp(0).Item("DEDUCTED")), GUI.fmtCurrency)
            '                rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))

            '                If CDec(dTemp(0).Item("DEDUCTED")) = 0 Then
            '                    rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
            '                Else
            '                    rpt_Row.Item("Column4") = Format(CDec(CDec(mDecRemaining_Balance) - CDec(dTemp(0).Item("DEDUCTED"))), GUI.fmtCurrency)
            '                End If
            '                rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))

            '                rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
            '                rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
            '                mDecRemaining_Balance = CDec(mDecRemaining_Balance) - CDec(dTemp(0).Item("DEDUCTED"))
            '            Else
            '                If dTemp.Length > 0 Then
            '                    If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dTemp(0).Item("REMAINING_BALANCE")
            '                End If

            '                blnIsDeductedNewLoan = False

            '                rpt_Row.Item("Column2") = Format(CDec(mdecDeductionAmount), GUI.fmtCurrency)
            '                rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))
            '                rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
            '                rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))
            '                rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
            '                rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))
            '                rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
            '                rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
            '            End If
            '            rpt_NewLoan.Tables("ArutiTable").Rows.Add(rpt_Row)
            '            iCnt += 1
            '        Next
            '    End If
            '    objRpt.Subreports("rptNewLoan").SetDataSource(rpt_NewLoan)
            'End If
            '************************** NEW LOAN STRUCTURE ************************** END

            'S.SANDEEP [28 AUG 2015] -- START
            '************************** SETTING LOAN CURRENCY ************************** START
            StrQ = "SELECT " & _
                   " @currency_name = currency_name " & _
                   "FROM lnloan_advance_tran " & _
                   "JOIN cfexchange_rate ON cfexchange_rate.countryunkid = lnloan_advance_tran.countryunkid " & _
                   "WHERE isvoid  = 0 AND lnloan_advance_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "

            objDataOperation.AddParameter("@currency_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCurrencySign, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strCurrencySign = objDataOperation.GetParameterValue("@currency_name")

            '************************** SETTING LOAN CURRENCY ************************** END
            'S.SANDEEP [28 AUG 2015] -- END



            'Pinkal (14-Apr-2018) -- Start
            'Enhancement - (Ref # 204) Loan Approvers list should be shown in the loan approval form.
            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                   "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   " FROM lnloanapproval_process_tran " & _
                   "    JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   " WHERE lnloanapproval_process_tran.isvoid = 0 AND lnloanapprover_master.isexternalapprover = 1 AND lnloanapproval_process_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If



            StrInnerQry = "SELECT lnloanapprover_master.lnapproverunkid  " & _
                                ",lnapproverlevel_master.lnlevelunkid " & _
                                ",lnapproverlevel_master.lnlevelname " & _
                                ",lnapproverlevel_master.priority " & _
                                "    ,#APPVR_NAME# approvername " & _
                                "    ,#APPR_JOB_NAME# AS approvertitle " & _
                                ",lnloanapproval_process_tran.approvaldate " & _
                                ",lnloanapproval_process_tran.statusunkid " & _
                                ",CASE WHEN lnloanapproval_process_tran.statusunkid =1 THEN @Pending " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = 2 THEN @Approved " & _
                                "          WHEN lnloanapproval_process_tran.statusunkid = 3 THEN  @Reject " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = 4 THEN @Assigned " & _
                            " END AS status  " & _
                                ",lnloanapproval_process_tran.remark " & _
                                " FROM lnloanapproval_process_tran " & _
                                " JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid " & _
                                " JOIN lnloanapprover_master ON lnloanapproval_process_tran.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                                " JOIN lnapproverlevel_master ON lnloanapprover_master.lnlevelunkid = lnapproverlevel_master.lnlevelunkid " & _
                                " #COMM_JOIN# "


            'Pinkal (30-Apr-2018) -- Start
            'BUG - SOLVED BUG FOR LOAN APPROVAL FORM.
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Terminatted approvers do not appear on loan forms they approved before they left.
            'StrConditionQry = " WHERE lnloanapproval_process_tran.isvoid = 0 AND lnloanapprover_master.isvoid = 0 AND lnapproverlevel_master.isactive = 1 AND  lnloanapprover_master.isexternalapprover = #ExApprId# "
            StrConditionQry = " WHERE lnloanapproval_process_tran.isvoid = 0  AND lnapproverlevel_master.isactive = 1 AND  lnloanapprover_master.isexternalapprover = #ExApprId# "
            'Hemant (12 Nov 2021) -- End
            'Pinkal (30-Apr-2018) -- End

            If mintEmployeeId > 0 Then
                StrConditionQry &= " AND lnloanapproval_process_tran.employeeunkid =@employeeunkid "
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lnloanapprover_master.approverempunkid = hremployee_master.employeeunkid " & _
                                               " JOIN #DB_Name#hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid ")
            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ &= " ORDER BY lnapproverlevel_master.priority "



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Rejected"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables("DataTable").Rows(0)("processpendingloanunkid")))

            Dim dsApproverList As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "'' ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lnloanapproval_process_tran.approverempunkid ")
                Else
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lnloanapproval_process_tran.approverempunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                                       " JOIN #DB_Name#hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsApproverList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsApproverList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1

            'Sohail (05 May 2020) -- Start
            'Ifakara Issue # : Showing Pending status for other approver if one approver has approved loan / advance on same level.
            '            If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
            'Recalculate:
            '                Dim mintPrioriry As Integer = 0
            '                For i As Integer = 0 To dsApproverList.Tables(0).Rows.Count - 1

            '                    If mintPrioriry <> CInt(dsApproverList.Tables(0).Rows(i)("priority")) Then
            '                        mintPrioriry = CInt(dsApproverList.Tables(0).Rows(i)("priority"))
            '                        Dim drRow() As DataRow = dsApproverList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
            '                        If drRow.Length > 0 Then
            '                            Dim drPending() As DataRow = dsApproverList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
            '                            If drPending.Length > 0 Then
            '                                For Each dRow As DataRow In drPending
            '                                    dsApproverList.Tables(0).Rows.Remove(dRow)
            '                                    GoTo Recalculate
            '                                Next
            '                                dsApproverList.AcceptChanges()
            '                            End If
            '                        End If


            '                    End If

            '                Next
            '            End If
            If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
                Dim mintPrioriry As Integer = 0
                Dim dtTable As DataTable = New DataView(dsApproverList.Tables(0)).ToTable(True, "priority")
                For Each dtRow As DataRow In dtTable.Rows
                    mintPrioriry = CInt(dtRow.Item("priority"))
                    Dim drRow() As DataRow = dsApproverList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                    If drRow.Length > 0 Then
                        Dim drPending() As DataRow = dsApproverList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                        If drPending.Length > 0 Then
                            For Each dRow As DataRow In drRow
                                dsApproverList.Tables(0).Rows.Remove(dRow)
                            Next
                            dsApproverList.AcceptChanges()
                        End If
                    End If
                Next
            End If
            'Sohail (05 May 2020) -- End

            rpt_ApproverData = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsApproverList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_ApproverData.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("lnlevelname")
                rpt_Row.Item("Column2") = dtRow.Item("approvername")
                rpt_Row.Item("Column3") = dtRow.Item("approvertitle")

                If Not IsDBNull(dtRow.Item("approvaldate")) Then
                    rpt_Row.Item("Column4") = CDate(dtRow.Item("approvaldate")).ToShortDateString()
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                rpt_Row.Item("Column5") = dtRow.Item("status")
                rpt_Row.Item("Column6") = dtRow.Item("remark")
                rpt_ApproverData.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt.Subreports("rptApproverDetails").SetDataSource(rpt_ApproverData)
            'Pinkal (14-Apr-2018) -- End





            '************************** SETTING REPORT DATA ************************** START
            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()
            Dim objImg(0) As Object
            Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
            objImg(0) = eZeeDataType.image2Data(imgBlank)
            If Company._Object._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            arrImageRow.Item("arutiLogo") = objImg(0)
            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)
            objRpt.SetDataSource(rpt_Data)

            'SETTING CAPTION PART -- START

            'S.SANDEEP [28 AUG 2015] -- START
            'Dim ObjExRate As New clsExchangeRate
            'If ConfigParameter._Object._Base_CurrencyId.ToString.Trim.Trim.Length > 0 Then
            '    ObjExRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            'Else
            '    ObjExRate._ExchangeRateunkid = 0
            'End If
            'S.SANDEEP [28 AUG 2015] -- END

            ReportFunction.TextChange(objRpt, "lblCompany", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "FOR  INTERNAL USE ONLY"))
            ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 2, "DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 3, "DATE"))
            ReportFunction.TextChange(objRpt, "lblLoanApplicant", Language.getMessage(mstrModuleName, 4, "LOAN APPLICATION :"))
            ReportFunction.TextChange(objRpt, "lblLoanRequestNo", Language.getMessage(mstrModuleName, 5, "LOAN REQUEST NO."))
            ReportFunction.TextChange(objRpt, "lblLoanType", Language.getMessage(mstrModuleName, 6, "LOAN TYPE"))
            ReportFunction.TextChange(objRpt, "lblIDNo", Language.getMessage(mstrModuleName, 7, "ID NO"))
            ReportFunction.TextChange(objRpt, "lblFullName", Language.getMessage(mstrModuleName, 8, "NAME IN FULL"))
            ReportFunction.TextChange(objRpt, "lblDateOfJoining", Language.getMessage(mstrModuleName, 9, "DATE OF JOINING"))
            'S.SANDEEP [28 AUG 2015] -- START
            'ReportFunction.TextChange(objRpt, "lblKindlyGrant", Language.getMessage(mstrModuleName, 10, "KINDLY GRANT ME A LOAN OF ") & ObjExRate._Currency_Sign)
            ReportFunction.TextChange(objRpt, "lblKindlyGrant", Language.getMessage(mstrModuleName, 10, "KINDLY GRANT ME A LOAN OF ") & strCurrencySign)
            'S.SANDEEP [28 AUG 2015] -- END
            ReportFunction.TextChange(objRpt, "lblPurpose", Language.getMessage(mstrModuleName, 11, "PURPOSE"))
            ReportFunction.TextChange(objRpt, "lblApplicantSignature", Language.getMessage(mstrModuleName, 12, "APPLICANT SIGNATURE"))
            ReportFunction.TextChange(objRpt, "lblAcountDepartment", Language.getMessage(mstrModuleName, 13, "TO BE FILLED BY ACCOUNTS DEPARTMENT :"))
            'S.SANDEEP [28 AUG 2015] -- START
            'ReportFunction.TextChange(objRpt, "lblMonthlySalary", Language.getMessage(mstrModuleName, 14, "MONTHLY SALARY ") & ObjExRate._Currency_Sign)
            ReportFunction.TextChange(objRpt, "lblMonthlySalary", Language.getMessage(mstrModuleName, 14, "MONTHLY SALARY "))
            'S.SANDEEP [28 AUG 2015] -- END
            ReportFunction.TextChange(objRpt, "lblAccount", Language.getMessage(mstrModuleName, 15, "ACCOUNTS"))
            ReportFunction.TextChange(objRpt, "lblHeadOfDepartment", Language.getMessage(mstrModuleName, 16, "HEAD OF DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblApprovedbyDirector", Language.getMessage(mstrModuleName, 17, "APPROVED BY DIRECTOR"))

            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtPreviousLoanStructure", Language.getMessage(mstrModuleName, 18, "PREVIOUS LOAN STRUCTURE"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtMonth", Language.getMessage(mstrModuleName, 19, "MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtDeduction", Language.getMessage(mstrModuleName, 20, "DEDUCTION"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtDeducted", Language.getMessage(mstrModuleName, 21, "DEDUCTED"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtOutStanding", Language.getMessage(mstrModuleName, 22, "LOAN C/F"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotal", Language.getMessage(mstrModuleName, 23, "Sub Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtGrandtotal", Language.getMessage(mstrModuleName, 24, "Grand Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtLoanBF", Language.getMessage(mstrModuleName, 25, "LOAN B/F"))

            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovalDetails", Language.getMessage(mstrModuleName, 26, "APPROVAL DETAILS"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblLoanApprovalNo", Language.getMessage(mstrModuleName, 27, "LOAN APPROVAL NO."))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblApprovalDate", Language.getMessage(mstrModuleName, 3, "DATE"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblApprovedAmount", Language.getMessage(mstrModuleName, 28, "APPROVED AMOUNT"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblPaymentType", Language.getMessage(mstrModuleName, 29, "PAYMENT TYPE"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblNoofInstallments", Language.getMessage(mstrModuleName, 30, "NO. OF INSTALLMENTS "))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovedLoanStructure", Language.getMessage(mstrModuleName, 31, "APPROVED LOAN STRUCTURE"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtMonth", Language.getMessage(mstrModuleName, 32, "MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtDeduction", Language.getMessage(mstrModuleName, 33, "DEDUCTION"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtDeducted", Language.getMessage(mstrModuleName, 34, "DEDUCTED"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtOutStanding", Language.getMessage(mstrModuleName, 35, "LOAN C/F"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotal", Language.getMessage(mstrModuleName, 36, "Sub Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtGrandtotal", Language.getMessage(mstrModuleName, 37, "Grand Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblRemark", Language.getMessage(mstrModuleName, 38, "REMARKS"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtLoanBF", Language.getMessage(mstrModuleName, 42, "LOAN B/F"))
            'SETTING CAPTION PART -- END

            'SETTING DATA PART -- START
            ReportFunction.TextChange(objRpt, "txtDepartment", dsList.Tables("DataTable").Rows(0)("DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "txtDate", eZeeDate.convertDate(dsList.Tables("DataTable").Rows(0)("DATE").ToString).ToShortDateString)
            ReportFunction.TextChange(objRpt, "txtVocNo", dsList.Tables("DataTable").Rows(0)("LOAN_REQUEST_NO"))
            ReportFunction.TextChange(objRpt, "txtLoanType", dsList.Tables("DataTable").Rows(0)("LOAN_TYPE"))
            ReportFunction.TextChange(objRpt, "txtIDNo", dsList.Tables("DataTable").Rows(0)("ID_NO"))
            ReportFunction.TextChange(objRpt, "txtFullName", dsList.Tables("DataTable").Rows(0)("EMP_NAME"))
            ReportFunction.TextChange(objRpt, "txtDateOfJoining", eZeeDate.convertDate(dsList.Tables("DataTable").Rows(0)("DOJ").ToString).ToShortDateString)
            ReportFunction.TextChange(objRpt, "txtAmount", Format(CDec(dsList.Tables("DataTable").Rows(0)("LOAN_AMOUNT")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtPurpose", dsList.Tables("DataTable").Rows(0)("PURPOSE"))
            ReportFunction.TextChange(objRpt, "txtSignature", "")
            Select Case mintDisplayModeSetting
                Case enDisplayMode.BASIC_SALARY
                    ReportFunction.TextChange(objRpt, "txtMonthlySalary", Format(CDec(dsList.Tables("DataTable").Rows(0)("MONTHLY_SALARY")), GUI.fmtCurrency))
                Case enDisplayMode.GROSS_SALARY
                    ReportFunction.TextChange(objRpt, "txtMonthlySalary", Format(CDec(mDecGross_Salary), GUI.fmtCurrency))
            End Select
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovalNo", dsTranList.Tables("DataTable").Rows(0)("LOAN_VOC_NO"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovalDate", eZeeDate.convertDate(dsTranList.Tables("DataTable").Rows(0)("DATE").ToString).ToShortDateString)
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovedAmount", Format(CDec(dsTranList.Tables("DataTable").Rows(0)("APPROVED_AMOUNT")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtPaymentType", IIf(IsDBNull(dsTranList.Tables("DataTable").Rows(0)("MODE")), "", dsTranList.Tables("DataTable").Rows(0)("MODE")))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txttotalInstallments", dsTranList.Tables("DataTable").Rows(0)("NO_OF_INSTALLMENTS"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtRemark", "")

            If rpt_OldLoan.Tables("ArutiTable").Rows.Count > 0 Then
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn2", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn3", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))
                If blnIsDeductedOldLoan = False Then
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Rows(0)("Column83")), GUI.fmtCurrency))
                Else
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
                End If


                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn2", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn3", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))

                If blnIsDeductedOldLoan = False Then
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Rows(0)("Column83")), GUI.fmtCurrency))
                Else
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
                End If

            Else
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn2", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn3", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn4", Format(CDec(0), GUI.fmtCurrency))

                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn2", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn3", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn4", Format(CDec(0), GUI.fmtCurrency))
            End If

            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn2", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn3", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))

            If blnIsDeductedNewLoan = False Then
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Rows(0).Item("Column83")), GUI.fmtCurrency))
            Else
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
            End If

            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn2", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn3", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))

            If blnIsDeductedNewLoan = False Then
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Rows(rpt_NewLoan.Tables("ArutiTable").Rows.Count - 1).Item("Column83")), GUI.fmtCurrency))
            Else
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
            End If

            'Pinkal (14-Apr-2018) -- Start
            'Enhancement - (Ref # 204) Loan Approvers list should be shown in the loan approval form.

            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtLoanApprovals", Language.getMessage(mstrModuleName, 48, "LOAN APPROVALS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtLevelName", Language.getMessage(mstrModuleName, 49, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApproverName", Language.getMessage(mstrModuleName, 50, "Approver Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApproverTitle", Language.getMessage(mstrModuleName, 51, "Approver Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApprovalDate", Language.getMessage(mstrModuleName, 52, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApprovalStatus", Language.getMessage(mstrModuleName, 53, "Approval Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtRemarks", Language.getMessage(mstrModuleName, 54, "Remarks/Comments"))
            'Pinkal (14-Apr-2018) -- End

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 39, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 40, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            'SETTING DATA PART -- END             

            '************************** SETTING REPORT DATA ************************** END

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Sohail (29 Apr 2020) -- Start
    'Ifakara Enhancement # 0004668 : Advance Approve Form.
    Private Function Generate_AdvanceReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsTranList As New DataSet
        Dim exForce As Exception
        Dim rpt_OldLoan As ArutiReport.Designer.dsArutiReport
        Dim rpt_NewLoan As ArutiReport.Designer.dsArutiReport
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        objDataOperation = New clsDataOperation
        Dim mDecGross_Salary As Decimal = 0
        Dim blnIsDeductedOldLoan As Boolean = False
        Dim blnIsDeductedNewLoan As Boolean = False
        Dim strCurrencySign As String = String.Empty

        Dim rpt_ApproverData As ArutiReport.Designer.dsArutiReport

        Try

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            objRpt = New ArutiReport.Designer.rptBBL_Loan_Report

            '************************** MAIN PART ************************** START

            StrQ = "SELECT TOP 1 " & _
                   "     ISNULL(DM.name,'') AS DEPARTMENT " & _
                   "    ,CONVERT(CHAR(8),application_date,112) AS DATE " & _
                   "    ,application_no AS LOAN_REQUEST_NO " & _
                   "    ,@Advance AS LOAN_TYPE " & _
                   "    ,employeecode AS ID_NO " & _
                   "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EMP_NAME " & _
                   "    ,CONVERT(CHAR(8),appointeddate,112) AS DOJ " & _
                   "    ,loan_amount AS LOAN_AMOUNT " & _
                   "    ,emp_remark AS PURPOSE " & _
                   "    ,ISNULL(MONTHLY_SALARY,0) AS MONTHLY_SALARY " & _
                   "    ,lnloan_process_pending_loan.employeeunkid " & _
                   "    ,processpendingloanunkid " & _
                   "    ,isexternal_entity " & _
                   "FROM lnloan_process_pending_loan " & _
                   "    JOIN hremployee_master ON lnloan_process_pending_loan.employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             MS.employeeunkid " & _
                   "            ,MS.MONTHLY_SALARY " & _
                   "        FROM " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 employeeunkid " & _
                   "                ,newscale AS MONTHLY_SALARY " & _
                   "                ,ROW_NUMBER() OVER( PARTITION BY employeeunkid ORDER BY incrementdate DESC) AS SNO " & _
                   "            FROM prsalaryincrement_tran " & _
                   "            WHERE CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   "                AND employeeunkid = " & mintEmployeeId & " " & _
                   "                AND prsalaryincrement_tran.isapproved = 1 " & _
                   "        ) AS MS WHERE MS.SNO = 1 " & _
                   "    ) AS A ON A.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             departmentunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   "    ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "    LEFT JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= "WHERE lnloan_process_pending_loan.isvoid = 0 AND loan_statusunkid = 4 AND isloan = 0 AND lnloan_process_pending_loan.employeeunkid = " & mintEmployeeId & " "

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            StrQ &= "ORDER BY processpendingloanunkid DESC "

            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "Advance"))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("DataTable").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 56, "Sorry, No Advance Information Found for Selected Employee."), enMsgBoxStyle.Information)
                Return Nothing
            End If

            '************************** GROSS_SAL_PART -- START
            If mintDisplayModeSetting = enDisplayMode.GROSS_SALARY Then
                StrQ = "SELECT " & _
                       "	ISNULL(SUM(amount),0) AS Amount " & _
                       "FROM vwPayroll " & _
                       "WHERE trnheadtype_id =  1 " & _
                       "AND payperiodunkid IN (SELECT TOP 1 periodunkid FROM cfcommon_period_tran WHERE CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' " & _
                       "	AND yearunkid = " & xYearUnkid & " AND modulerefid = 1 ORDER BY start_date DESC) " & _
                       "AND employeeunkid  = " & mintEmployeeId & " "

                dsTranList = objDataOperation.ExecQuery(StrQ, "Sal")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsTranList.Tables("Sal").Rows.Count > 0 Then
                    mDecGross_Salary = dsTranList.Tables("Sal").Rows(0)("Amount")
                End If
            End If
            '************************** GROSS_SAL_PART -- END

            '************************** MAIN PART ************************** END

            '************************** OLD LOAN STRUCTURE ************************** START
            rpt_OldLoan = New ArutiReport.Designer.dsArutiReport
            If dsList.Tables("DataTable").Rows.Count > 0 Then

                StrQ = "SELECT " & _
                       "     cfcommon_period_tran.period_name AS DEDUCTION_MONTH " & _
                       "    ,lnloan_advance_tran.basecurrency_amount AS DEDUCTION " & _
                       "    ,prpayrollprocess_tran.amount AS DEDUCTED " & _
                       "    ,0 AS OUTSTANDING " & _
                       "    ,lnloan_advance_tran.balance_amount AS REMAINING_BALANCE " & _
                       "    ,1 AS emi_tenure " & _
                       "FROM prpayrollprocess_tran " & _
                       "    JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                       "    JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND modulerefid = 1 " & _
                       "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                       "    AND prpayrollprocess_tran.loanadvancetranunkid = " & _
                       "    ( " & _
                       "        SELECT TOP 1 loanadvancetranunkid " & _
                       "        FROM lnloan_advance_tran " & _
                       "        WHERE lnloan_advance_tran.isvoid = 0 AND employeeunkid = '" & mintEmployeeId & "' AND isloan = 0 AND processpendingloanunkid <> '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' " & _
                       "        ORDER BY loanadvancetranunkid DESC " & _
                       "    ) "

                dsTranList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtTmp() As DataRow = Nothing
                dtTmp = dsTranList.Tables("DataTable").Select("DEDUCTED > 0")
                If dtTmp.Length > 0 Then
                    blnIsDeductedOldLoan = True
                End If

                Dim mDecRemaining_Balance As Decimal = 0
                For Each dtRow As DataRow In dsTranList.Tables("DataTable").Rows
                    Dim rpt_Row As DataRow = rpt_OldLoan.Tables("ArutiTable").NewRow

                    If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dtRow.Item("REMAINING_BALANCE")

                    rpt_Row.Item("Column1") = dtRow.Item("DEDUCTION_MONTH")

                    rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("DEDUCTION")), GUI.fmtCurrency)
                    rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))

                    rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("DEDUCTED")), GUI.fmtCurrency)
                    rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))

                    rpt_Row.Item("Column4") = Format(CDec(CDec(mDecRemaining_Balance) - CDec(dtRow.Item("DEDUCTED"))), GUI.fmtCurrency)
                    rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))

                    rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                    rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))

                    mDecRemaining_Balance = CDec(mDecRemaining_Balance) - CDec(dtRow.Item("DEDUCTED"))

                    rpt_OldLoan.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next

                objRpt.Subreports("rptOldLoan").SetDataSource(rpt_OldLoan)

            End If
            '************************** OLD LOAN STRUCTURE ************************** END

            '************************** NEW LOAN STRUCTURE ************************** START
            rpt_NewLoan = New ArutiReport.Designer.dsArutiReport
            If dsList.Tables("DataTable").Rows.Count > 0 Then

                StrQ = "SELECT " & _
                        "	 CASE WHEN ISNULL(prtnaleave_tran.payperiodunkid,0) = 0 THEN lnloan_advance_tran.deductionperiodunkid ELSE prtnaleave_tran.payperiodunkid END AS periodunkid " & _
                        "	,lnloan_advance_tran.basecurrency_amount AS DEDUCTION " & _
                       "	,ISNULL(prpayrollprocess_tran.amount,0) AS DEDUCTED " & _
                       "	,0 AS OUTSTANDING " & _
                        "	,1 AS NO_OF_INSTALLMENTS " & _
                       "	,CONVERT(CHAR(8),lnloan_advance_tran.effective_date,112) AS DATE " & _
                       "	,lnloan_advance_tran.basecurrency_amount AS APPROVED_AMOUNT " & _
                        "	,lnloan_advance_tran.balance_amount AS REMAINING_BALANCE " & _
                       "	,loanvoucher_no AS LOAN_VOC_NO " & _
                       "	,CASE WHEN lnloan_process_pending_loan.isexternal_entity = 0 THEN " & _
                       "			CASE WHEN paymentmode = 1 THEN @CASH " & _
                       "				 WHEN paymentmode = 2 THEN @CHEQUE " & _
                       "				 WHEN paymentmode = 3 THEN @TRANSFER " & _
                       "			END " & _
                       "		  WHEN lnloan_process_pending_loan.isexternal_entity = 1 THEN @EXTERNAL " & _
                       "	 END AS MODE " & _
                       "FROM lnloan_advance_tran " & _
                       "	JOIN lnloan_process_pending_loan ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                       "	LEFT JOIN prpayment_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid AND referenceid = 1 " & _
                       "	LEFT JOIN prpayrollprocess_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid AND prpayrollprocess_tran.isvoid = 0 " & _
                       "	LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid AND prtnaleave_tran.isvoid = 0 " & _
                       "WHERE lnloan_advance_tran.isvoid= 0 AND lnloan_advance_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "

                objDataOperation.AddParameter("@CASH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 43, "CASH"))
                objDataOperation.AddParameter("@CHEQUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 44, "CHEQUE"))
                objDataOperation.AddParameter("@TRANSFER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 45, "TRANSFER"))
                objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "EXTERNAL"))

                dsTranList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtTable As DataTable = Nothing
                Dim dsPeriod As New DataSet
                Dim objPeriod As New clscommom_period_Tran
                Dim iCnt As Integer = 1
                Dim iInstallment As Integer = 0

                dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                     xYearUnkid, _
                                                     xDatabaseName, _
                                                     mdtDbStartDate, _
                                                     "List", False)

                If dsPeriod.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, There is no Open period found."), enMsgBoxStyle.Information)
                    Return Nothing
                End If

                If dsTranList.Tables("DataTable").Rows.Count > 0 Then
                    iInstallment = dsTranList.Tables("DataTable").Rows(0)("NO_OF_INSTALLMENTS")
                    dtTable = New DataView(dsPeriod.Tables("List"), "periodunkid > = " & dsTranList.Tables("DataTable").Rows(0)("periodunkid"), "", DataViewRowState.CurrentRows).ToTable
                End If

                Dim dtTmp() As DataRow = Nothing
                dtTmp = dsTranList.Tables("DataTable").Select("DEDUCTED > 0")
                If dtTmp.Length > 0 Then
                    blnIsDeductedNewLoan = True
                End If


                Dim mDecRemaining_Balance As Decimal = 0
                Dim mdecDeductionAmount As Decimal = 0

                If dsTranList.Tables("DataTable").Rows.Count > 0 AndAlso dtTable IsNot Nothing Then
                    For i As Integer = 0 To iInstallment - 1
                        Dim rpt_Row As DataRow = rpt_NewLoan.Tables("ArutiTable").NewRow
                        If i <= dtTable.Rows.Count - 1 Then
                            rpt_Row.Item("Column1") = i + 1.ToString & ". " & dtTable.Rows(i).Item("name")
                        Else
                            rpt_Row.Item("Column1") = i + 1.ToString & ". " & " -- "
                        End If

                        If mdecDeductionAmount <= 0 Then mdecDeductionAmount = dsTranList.Tables("DataTable").Rows(0)("DEDUCTION")
                        If mDecRemaining_Balance <= 0 Then mDecRemaining_Balance = dsTranList.Tables("DataTable").Rows(0)("REMAINING_BALANCE")
                        If i <= dsTranList.Tables("DataTable").Rows.Count - 1 Then

                            rpt_Row.Item("Column2") = Format(CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTION")), GUI.fmtCurrency)
                            rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))

                            rpt_Row.Item("Column3") = Format(CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED")), GUI.fmtCurrency)
                            rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))

                            If CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED")) = 0 Then
                                rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            Else
                                rpt_Row.Item("Column4") = Format(CDec(CDec(mDecRemaining_Balance) - CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED"))), GUI.fmtCurrency)
                            End If
                            rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))

                            rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
                            mDecRemaining_Balance = CDec(mDecRemaining_Balance) - CDec(dsTranList.Tables("DataTable").Rows(i).Item("DEDUCTED"))
                        Else
                            blnIsDeductedNewLoan = False

                            rpt_Row.Item("Column2") = Format(CDec(mdecDeductionAmount), GUI.fmtCurrency)
                            rpt_Row.Item("Column81") = CDec(rpt_Row.Item("Column2"))
                            rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
                            rpt_Row.Item("Column82") = CDec(rpt_Row.Item("Column3"))
                            rpt_Row.Item("Column4") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            rpt_Row.Item("Column83") = CDec(rpt_Row.Item("Column4"))
                            rpt_Row.Item("Column5") = Format(CDec(mDecRemaining_Balance), GUI.fmtCurrency)
                            rpt_Row.Item("Column84") = CDec(rpt_Row.Item("Column5"))
                        End If
                        rpt_NewLoan.Tables("ArutiTable").Rows.Add(rpt_Row)
                    Next

                End If
                objRpt.Subreports("rptNewLoan").SetDataSource(rpt_NewLoan)
            End If


            '************************** NEW LOAN STRUCTURE ************************** END

            '************************** SETTING LOAN CURRENCY ************************** START
            StrQ = "SELECT " & _
                   " @currency_name = currency_name " & _
                   "FROM lnloan_advance_tran " & _
                   "JOIN cfexchange_rate ON cfexchange_rate.countryunkid = lnloan_advance_tran.countryunkid " & _
                   "WHERE isvoid  = 0 AND lnloan_advance_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "

            objDataOperation.AddParameter("@currency_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCurrencySign, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strCurrencySign = objDataOperation.GetParameterValue("@currency_name")

            '************************** SETTING LOAN CURRENCY ************************** END



            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "" : StrCommJoinQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                   "   ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "  ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   " FROM lnloanapproval_process_tran " & _
                   "    JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   " WHERE lnloanapproval_process_tran.isvoid = 0 AND lnloanapprover_master.isexternalapprover = 1 AND lnloanapproval_process_tran.processpendingloanunkid = '" & dsList.Tables("DataTable").Rows(0)("processpendingloanunkid") & "' "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If



            StrInnerQry = "SELECT lnloanapprover_master.lnapproverunkid  " & _
                                ",lnapproverlevel_master.lnlevelunkid " & _
                                ",lnapproverlevel_master.lnlevelname " & _
                                ",lnapproverlevel_master.priority " & _
                                "    ,#APPVR_NAME# approvername " & _
                                "    ,#APPR_JOB_NAME# AS approvertitle " & _
                                ",lnloanapproval_process_tran.approvaldate " & _
                                ",lnloanapproval_process_tran.statusunkid " & _
                                ",CASE WHEN lnloanapproval_process_tran.statusunkid =1 THEN @Pending " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = 2 THEN @Approved " & _
                                "          WHEN lnloanapproval_process_tran.statusunkid = 3 THEN  @Reject " & _
                                 " WHEN lnloanapproval_process_tran.statusunkid = 4 THEN @Assigned " & _
                            " END AS status  " & _
                                ",lnloanapproval_process_tran.remark " & _
                                " FROM lnloanapproval_process_tran " & _
                                " JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid " & _
                                " JOIN lnloanapprover_master ON lnloanapproval_process_tran.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                                " JOIN lnapproverlevel_master ON lnloanapprover_master.lnlevelunkid = lnapproverlevel_master.lnlevelunkid " & _
                                " #COMM_JOIN# "


            StrConditionQry = " WHERE lnloanapproval_process_tran.isvoid = 0 AND lnloanapprover_master.isvoid = 0 AND lnapproverlevel_master.isactive = 1 AND  lnloanapprover_master.isexternalapprover = #ExApprId# "

            If mintEmployeeId > 0 Then
                StrConditionQry &= " AND lnloanapproval_process_tran.employeeunkid =@employeeunkid "
            End If

            StrQ = StrInnerQry
            StrQ &= StrConditionQry
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lnloanapprover_master.approverempunkid = hremployee_master.employeeunkid " & _
                                               " JOIN #DB_Name#hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid ")
            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ExApprId#", "0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ &= " ORDER BY lnapproverlevel_master.priority "



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 5, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 6, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 7, "Rejected"))
            objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsProcess_pending_loan", 10, "Assigned"))

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables("DataTable").Rows(0)("processpendingloanunkid")))

            Dim dsApproverList As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "'' ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(UEmp.username,'') ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lnloanapproval_process_tran.approverempunkid ")
                Else
                    StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
                    StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lnloanapproval_process_tran.approverempunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                                       " JOIN #DB_Name#hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DB_Name#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DB_Name#", "")
                End If

                StrQ = StrQ.Replace("#ExApprId#", "1")
                StrQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsApproverList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsApproverList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim mintPriorty As Integer = -1

            'Sohail (05 May 2020) -- Start
            'Ifakara Issue # : Showing Pending status for other approver if one approver has approved loan / advance on same level.
            '            If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
            'Recalculate:
            '                Dim mintPrioriry As Integer = 0
            '                For i As Integer = 0 To dsApproverList.Tables(0).Rows.Count - 1

            '                    If mintPrioriry <> CInt(dsApproverList.Tables(0).Rows(i)("priority")) Then
            '                        mintPrioriry = CInt(dsApproverList.Tables(0).Rows(i)("priority"))
            '                        Dim drRow() As DataRow = dsApproverList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
            '                        If drRow.Length > 0 Then
            '                            Dim drPending() As DataRow = dsApproverList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
            '                            If drPending.Length > 0 Then
            '                                For Each dRow As DataRow In drPending
            '                                    dsApproverList.Tables(0).Rows.Remove(dRow)
            '                                    GoTo Recalculate
            '                                Next
            '                                dsApproverList.AcceptChanges()
            '                            End If
            '                        End If


            '                    End If

            '                Next
            '            End If
            If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsApproverList.Tables(0)).ToTable(True, "priority")
                Dim mintPrioriry As Integer = 0
                For Each dtRow As DataRow In dtTable.Rows
                    mintPrioriry = CInt(dtRow.Item("priority"))
                    Dim drRow() As DataRow = dsApproverList.Tables(0).Select("statusunkid <> 2 AND priority  = " & mintPrioriry)
                    If drRow.Length > 0 Then
                        Dim drPending() As DataRow = dsApproverList.Tables(0).Select("statusunkid = 2 AND priority  = " & mintPrioriry)
                        If drPending.Length > 0 Then
                            For Each dRow As DataRow In drRow
                                dsApproverList.Tables(0).Rows.Remove(dRow)
                            Next
                            dsApproverList.AcceptChanges()
                        End If
                    End If
                Next
            End If
            'Sohail (05 May 2020) -- End

            rpt_ApproverData = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsApproverList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_ApproverData.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("lnlevelname")
                rpt_Row.Item("Column2") = dtRow.Item("approvername")
                rpt_Row.Item("Column3") = dtRow.Item("approvertitle")

                If Not IsDBNull(dtRow.Item("approvaldate")) Then
                    rpt_Row.Item("Column4") = CDate(dtRow.Item("approvaldate")).ToShortDateString()
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                rpt_Row.Item("Column5") = dtRow.Item("status")
                rpt_Row.Item("Column6") = dtRow.Item("remark")
                rpt_ApproverData.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt.Subreports("rptApproverDetails").SetDataSource(rpt_ApproverData)





            '************************** SETTING REPORT DATA ************************** START
            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()
            Dim objImg(0) As Object
            Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
            objImg(0) = eZeeDataType.image2Data(imgBlank)
            If Company._Object._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            arrImageRow.Item("arutiLogo") = objImg(0)
            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)
            objRpt.SetDataSource(rpt_Data)

            'SETTING CAPTION PART -- START


            ReportFunction.TextChange(objRpt, "lblCompany", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 57, "FOR  INTERNAL USE ONLY"))
            ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 58, "DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 59, "DATE"))
            ReportFunction.TextChange(objRpt, "lblLoanApplicant", Language.getMessage(mstrModuleName, 60, "ADVANCE APPLICATION :"))
            ReportFunction.TextChange(objRpt, "lblLoanRequestNo", Language.getMessage(mstrModuleName, 61, "ADVANCE REQUEST NO."))
            ReportFunction.TextChange(objRpt, "lblLoanType", Language.getMessage(mstrModuleName, 62, "ADVANCE TYPE"))
            ReportFunction.TextChange(objRpt, "lblIDNo", Language.getMessage(mstrModuleName, 63, "ID NO"))
            ReportFunction.TextChange(objRpt, "lblFullName", Language.getMessage(mstrModuleName, 64, "NAME IN FULL"))
            ReportFunction.TextChange(objRpt, "lblDateOfJoining", Language.getMessage(mstrModuleName, 65, "DATE OF JOINING"))
            ReportFunction.TextChange(objRpt, "lblKindlyGrant", Language.getMessage(mstrModuleName, 66, "KINDLY GRANT ME A ADVANCE OF ") & strCurrencySign)
            ReportFunction.TextChange(objRpt, "lblPurpose", Language.getMessage(mstrModuleName, 67, "PURPOSE"))
            ReportFunction.TextChange(objRpt, "lblApplicantSignature", Language.getMessage(mstrModuleName, 68, "APPLICANT SIGNATURE"))
            ReportFunction.TextChange(objRpt, "lblAcountDepartment", Language.getMessage(mstrModuleName, 69, "TO BE FILLED BY ACCOUNTS DEPARTMENT :"))
            ReportFunction.TextChange(objRpt, "lblMonthlySalary", Language.getMessage(mstrModuleName, 70, "MONTHLY SALARY "))
            ReportFunction.TextChange(objRpt, "lblAccount", Language.getMessage(mstrModuleName, 71, "ACCOUNTS"))
            ReportFunction.TextChange(objRpt, "lblHeadOfDepartment", Language.getMessage(mstrModuleName, 72, "HEAD OF DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "lblApprovedbyDirector", Language.getMessage(mstrModuleName, 73, "APPROVED BY DIRECTOR"))

            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtPreviousLoanStructure", Language.getMessage(mstrModuleName, 74, "PREVIOUS ADVANCE STRUCTURE"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtMonth", Language.getMessage(mstrModuleName, 75, "MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtDeduction", Language.getMessage(mstrModuleName, 76, "DEDUCTION"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtDeducted", Language.getMessage(mstrModuleName, 77, "DEDUCTED"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtOutStanding", Language.getMessage(mstrModuleName, 78, "ADVANCE C/F"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotal", Language.getMessage(mstrModuleName, 79, "Sub Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtGrandtotal", Language.getMessage(mstrModuleName, 80, "Grand Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtLoanBF", Language.getMessage(mstrModuleName, 81, "ADVANCE B/F"))

            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovalDetails", Language.getMessage(mstrModuleName, 82, "APPROVAL DETAILS"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblLoanApprovalNo", Language.getMessage(mstrModuleName, 83, "ADVANCE APPROVAL NO."))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblApprovalDate", Language.getMessage(mstrModuleName, 84, "DATE"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblApprovedAmount", Language.getMessage(mstrModuleName, 85, "APPROVED AMOUNT"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblPaymentType", Language.getMessage(mstrModuleName, 86, "PAYMENT TYPE"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblNoofInstallments", Language.getMessage(mstrModuleName, 87, "NO. OF INSTALLMENTS "))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovedLoanStructure", Language.getMessage(mstrModuleName, 88, "APPROVED ADVANCE STRUCTURE"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtMonth", Language.getMessage(mstrModuleName, 89, "MONTH"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtDeduction", Language.getMessage(mstrModuleName, 90, "DEDUCTION"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtDeducted", Language.getMessage(mstrModuleName, 91, "DEDUCTED"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtOutStanding", Language.getMessage(mstrModuleName, 92, "ADVANCE C/F"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotal", Language.getMessage(mstrModuleName, 93, "Sub Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtGrandtotal", Language.getMessage(mstrModuleName, 94, "Grand Total:"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "lblRemark", Language.getMessage(mstrModuleName, 95, "REMARKS"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtLoanBF", Language.getMessage(mstrModuleName, 96, "ADVANCE B/F"))
            'SETTING CAPTION PART -- END

            'SETTING DATA PART -- START
            ReportFunction.TextChange(objRpt, "txtDepartment", dsList.Tables("DataTable").Rows(0)("DEPARTMENT"))
            ReportFunction.TextChange(objRpt, "txtDate", eZeeDate.convertDate(dsList.Tables("DataTable").Rows(0)("DATE").ToString).ToShortDateString)
            ReportFunction.TextChange(objRpt, "txtVocNo", dsList.Tables("DataTable").Rows(0)("LOAN_REQUEST_NO"))
            ReportFunction.TextChange(objRpt, "txtLoanType", dsList.Tables("DataTable").Rows(0)("LOAN_TYPE"))
            ReportFunction.TextChange(objRpt, "txtIDNo", dsList.Tables("DataTable").Rows(0)("ID_NO"))
            ReportFunction.TextChange(objRpt, "txtFullName", dsList.Tables("DataTable").Rows(0)("EMP_NAME"))
            ReportFunction.TextChange(objRpt, "txtDateOfJoining", eZeeDate.convertDate(dsList.Tables("DataTable").Rows(0)("DOJ").ToString).ToShortDateString)
            ReportFunction.TextChange(objRpt, "txtAmount", Format(CDec(dsList.Tables("DataTable").Rows(0)("LOAN_AMOUNT")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtPurpose", dsList.Tables("DataTable").Rows(0)("PURPOSE"))
            ReportFunction.TextChange(objRpt, "txtSignature", "")
            Select Case mintDisplayModeSetting
                Case enDisplayMode.BASIC_SALARY
                    ReportFunction.TextChange(objRpt, "txtMonthlySalary", Format(CDec(dsList.Tables("DataTable").Rows(0)("MONTHLY_SALARY")), GUI.fmtCurrency))
                Case enDisplayMode.GROSS_SALARY
                    ReportFunction.TextChange(objRpt, "txtMonthlySalary", Format(CDec(mDecGross_Salary), GUI.fmtCurrency))
            End Select
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovalNo", dsTranList.Tables("DataTable").Rows(0)("LOAN_VOC_NO"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovalDate", eZeeDate.convertDate(dsTranList.Tables("DataTable").Rows(0)("DATE").ToString).ToShortDateString)
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtApprovedAmount", Format(CDec(dsTranList.Tables("DataTable").Rows(0)("APPROVED_AMOUNT")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtPaymentType", IIf(IsDBNull(dsTranList.Tables("DataTable").Rows(0)("MODE")), "", dsTranList.Tables("DataTable").Rows(0)("MODE")))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txttotalInstallments", dsTranList.Tables("DataTable").Rows(0)("NO_OF_INSTALLMENTS"))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtRemark", "")

            If rpt_OldLoan.Tables("ArutiTable").Rows.Count > 0 Then
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn2", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn3", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))
                If blnIsDeductedOldLoan = False Then
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Rows(0)("Column83")), GUI.fmtCurrency))
                Else
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
                End If


                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn2", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn3", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))

                If blnIsDeductedOldLoan = False Then
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Rows(0)("Column83")), GUI.fmtCurrency))
                Else
                    ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn4", Format(CDec(rpt_OldLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
                End If

            Else
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn2", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn3", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtSubTotColumn4", Format(CDec(0), GUI.fmtCurrency))

                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn2", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn3", Format(CDec(0), GUI.fmtCurrency))
                ReportFunction.TextChange(objRpt.Subreports("rptOldLoan"), "txtTotColumn4", Format(CDec(0), GUI.fmtCurrency))
            End If

            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn2", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn3", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))

            If blnIsDeductedNewLoan = False Then
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Rows(0).Item("Column83")), GUI.fmtCurrency))
            Else
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtSubTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
            End If

            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn2", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column81)", "")), GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn3", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column82)", "")), GUI.fmtCurrency))

            If blnIsDeductedNewLoan = False Then
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Rows(rpt_NewLoan.Tables("ArutiTable").Rows.Count - 1).Item("Column83")), GUI.fmtCurrency))
            Else
                ReportFunction.TextChange(objRpt.Subreports("rptNewLoan"), "txtTotColumn4", Format(CDec(rpt_NewLoan.Tables("ArutiTable").Compute("SUM(Column83)", "")), GUI.fmtCurrency))
            End If


            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtLoanApprovals", Language.getMessage(mstrModuleName, 97, "ADVANCE APPROVALS"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtLevelName", Language.getMessage(mstrModuleName, 98, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApproverName", Language.getMessage(mstrModuleName, 99, "Approver Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApproverTitle", Language.getMessage(mstrModuleName, 100, "Approver Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApprovalDate", Language.getMessage(mstrModuleName, 101, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtApprovalStatus", Language.getMessage(mstrModuleName, 102, "Approval Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptApproverDetails"), "txtRemarks", Language.getMessage(mstrModuleName, 103, "Remarks/Comments"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 39, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 40, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            'SETTING DATA PART -- END             

            '************************** SETTING REPORT DATA ************************** END

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_AdvanceReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'Sohail (29 Apr 2020) -- End

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "FOR  INTERNAL USE ONLY")
            Language.setMessage(mstrModuleName, 2, "DEPARTMENT")
            Language.setMessage(mstrModuleName, 3, "DATE")
            Language.setMessage(mstrModuleName, 4, "LOAN APPLICATION :")
            Language.setMessage(mstrModuleName, 5, "LOAN REQUEST NO.")
            Language.setMessage(mstrModuleName, 6, "LOAN TYPE")
            Language.setMessage(mstrModuleName, 7, "ID NO")
            Language.setMessage(mstrModuleName, 8, "NAME IN FULL")
            Language.setMessage(mstrModuleName, 9, "DATE OF JOINING")
            Language.setMessage(mstrModuleName, 10, "KINDLY GRANT ME A LOAN OF")
            Language.setMessage(mstrModuleName, 11, "PURPOSE")
            Language.setMessage(mstrModuleName, 12, "APPLICANT SIGNATURE")
            Language.setMessage(mstrModuleName, 13, "TO BE FILLED BY ACCOUNTS DEPARTMENT :")
            Language.setMessage(mstrModuleName, 14, "MONTHLY SALARY")
            Language.setMessage(mstrModuleName, 15, "ACCOUNTS")
            Language.setMessage(mstrModuleName, 16, "HEAD OF DEPARTMENT")
            Language.setMessage(mstrModuleName, 17, "APPROVED BY DIRECTOR")
            Language.setMessage(mstrModuleName, 18, "PREVIOUS LOAN STRUCTURE")
            Language.setMessage(mstrModuleName, 19, "MONTH")
            Language.setMessage(mstrModuleName, 20, "DEDUCTION")
            Language.setMessage(mstrModuleName, 21, "DEDUCTED")
            Language.setMessage(mstrModuleName, 22, "LOAN C/F")
            Language.setMessage(mstrModuleName, 23, "Sub Total:")
            Language.setMessage(mstrModuleName, 24, "Grand Total:")
            Language.setMessage(mstrModuleName, 25, "LOAN B/F")
            Language.setMessage(mstrModuleName, 26, "APPROVAL DETAILS")
            Language.setMessage(mstrModuleName, 27, "LOAN APPROVAL NO.")
            Language.setMessage(mstrModuleName, 28, "APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 29, "PAYMENT TYPE")
            Language.setMessage(mstrModuleName, 30, "NO. OF INSTALLMENTS")
            Language.setMessage(mstrModuleName, 31, "APPROVED LOAN STRUCTURE")
            Language.setMessage(mstrModuleName, 32, "MONTH")
            Language.setMessage(mstrModuleName, 33, "DEDUCTION")
            Language.setMessage(mstrModuleName, 34, "DEDUCTED")
            Language.setMessage(mstrModuleName, 35, "LOAN C/F")
            Language.setMessage(mstrModuleName, 36, "Sub Total:")
            Language.setMessage(mstrModuleName, 37, "Grand Total:")
            Language.setMessage(mstrModuleName, 38, "REMARKS")
            Language.setMessage(mstrModuleName, 39, "Printed By :")
            Language.setMessage(mstrModuleName, 40, "Printed Date :")
            Language.setMessage(mstrModuleName, 41, "Sorry, No Loan Information Found for Selected Employee.")
            Language.setMessage(mstrModuleName, 42, "LOAN B/F")
            Language.setMessage(mstrModuleName, 43, "CASH")
            Language.setMessage(mstrModuleName, 44, "CHEQUE")
            Language.setMessage(mstrModuleName, 45, "TRANSFER")
            Language.setMessage(mstrModuleName, 46, "EXTERNAL")
            Language.setMessage(mstrModuleName, 47, "Sorry, There is no Open period found.")
            Language.setMessage(mstrModuleName, 48, "LOAN APPROVALS")
            Language.setMessage(mstrModuleName, 49, "Level Name")
            Language.setMessage(mstrModuleName, 50, "Approver Name")
            Language.setMessage(mstrModuleName, 51, "Approver Title")
            Language.setMessage(mstrModuleName, 52, "Approval Date")
            Language.setMessage(mstrModuleName, 53, "Approval Status")
            Language.setMessage(mstrModuleName, 54, "Remarks/Comments")
            Language.setMessage(mstrModuleName, 55, "Advance")
            Language.setMessage(mstrModuleName, 56, "Sorry, No Advance Information Found for Selected Employee.")
            Language.setMessage(mstrModuleName, 57, "FOR  INTERNAL USE ONLY")
            Language.setMessage(mstrModuleName, 58, "DEPARTMENT")
            Language.setMessage(mstrModuleName, 59, "DATE")
            Language.setMessage(mstrModuleName, 60, "ADVANCE APPLICATION :")
            Language.setMessage(mstrModuleName, 61, "ADVANCE REQUEST NO.")
            Language.setMessage(mstrModuleName, 62, "ADVANCE TYPE")
            Language.setMessage(mstrModuleName, 63, "ID NO")
            Language.setMessage(mstrModuleName, 64, "NAME IN FULL")
            Language.setMessage(mstrModuleName, 65, "DATE OF JOINING")
            Language.setMessage(mstrModuleName, 66, "KINDLY GRANT ME A ADVANCE OF")
            Language.setMessage(mstrModuleName, 67, "PURPOSE")
            Language.setMessage(mstrModuleName, 68, "APPLICANT SIGNATURE")
            Language.setMessage(mstrModuleName, 69, "TO BE FILLED BY ACCOUNTS DEPARTMENT :")
            Language.setMessage(mstrModuleName, 70, "MONTHLY SALARY")
            Language.setMessage(mstrModuleName, 71, "ACCOUNTS")
            Language.setMessage(mstrModuleName, 72, "HEAD OF DEPARTMENT")
            Language.setMessage(mstrModuleName, 73, "APPROVED BY DIRECTOR")
            Language.setMessage(mstrModuleName, 74, "PREVIOUS ADVANCE STRUCTURE")
            Language.setMessage(mstrModuleName, 75, "MONTH")
            Language.setMessage(mstrModuleName, 76, "DEDUCTION")
            Language.setMessage(mstrModuleName, 77, "DEDUCTED")
            Language.setMessage(mstrModuleName, 78, "ADVANCE C/F")
            Language.setMessage(mstrModuleName, 79, "Sub Total:")
            Language.setMessage(mstrModuleName, 80, "Grand Total:")
            Language.setMessage(mstrModuleName, 81, "ADVANCE B/F")
            Language.setMessage(mstrModuleName, 82, "APPROVAL DETAILS")
            Language.setMessage(mstrModuleName, 83, "ADVANCE APPROVAL NO.")
            Language.setMessage(mstrModuleName, 84, "DATE")
            Language.setMessage(mstrModuleName, 85, "APPROVED AMOUNT")
            Language.setMessage(mstrModuleName, 86, "PAYMENT TYPE")
            Language.setMessage(mstrModuleName, 87, "NO. OF INSTALLMENTS")
            Language.setMessage(mstrModuleName, 88, "APPROVED ADVANCE STRUCTURE")
            Language.setMessage(mstrModuleName, 89, "MONTH")
            Language.setMessage(mstrModuleName, 90, "DEDUCTION")
            Language.setMessage(mstrModuleName, 91, "DEDUCTED")
            Language.setMessage(mstrModuleName, 92, "ADVANCE C/F")
            Language.setMessage(mstrModuleName, 93, "Sub Total:")
            Language.setMessage(mstrModuleName, 94, "Grand Total:")
            Language.setMessage(mstrModuleName, 95, "REMARKS")
            Language.setMessage(mstrModuleName, 96, "ADVANCE B/F")
            Language.setMessage(mstrModuleName, 97, "ADVANCE APPROVALS")
            Language.setMessage(mstrModuleName, 98, "Level Name")
            Language.setMessage(mstrModuleName, 99, "Approver Name")
            Language.setMessage(mstrModuleName, 100, "Approver Title")
            Language.setMessage(mstrModuleName, 101, "Approval Date")
            Language.setMessage(mstrModuleName, 102, "Approval Status")
            Language.setMessage(mstrModuleName, 103, "Remarks/Comments")
            Language.setMessage("clsProcess_pending_loan", 5, "Pending")
            Language.setMessage("clsProcess_pending_loan", 6, "Approved")
            Language.setMessage("clsProcess_pending_loan", 7, "Rejected")
            Language.setMessage("clsProcess_pending_loan", 10, "Assigned")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
