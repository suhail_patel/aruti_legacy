#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMonthlyEmployeeBudgetVarianceAnalysis

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMonthlyEmployeeBudgetVarianceAnalysisReport"
    Private objSalaryBudget As clsMonthlyEmployeeBudgetVarianceAnalysis

    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String


    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrSearchText As String = "" 'Sohail (24-Aug-2017)
    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Private dvPeriod As DataView
    Private mstrSearchPeriodText As String = ""
    'Sohail (13 Oct 2017) -- End

    Private mstrAdvanceFilter As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
#End Region

#Region " Constructor "

    Public Sub New()
        objSalaryBudget = New clsMonthlyEmployeeBudgetVarianceAnalysis(User._Object._Languageunkid,Company._Object._Companyunkid)
        objSalaryBudget.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objBudget As New clsBudget_MasterNew
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData 'Sohail (13 Oct 2017)
        Try
            cboReportType.Items.Clear()

            'Pinkal (09-Oct-2017) -- Start
            'Enhancement - Working on Budget Report Changes for CCBRT.
            'cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 1, "Monthly Employee Budget Variance Analysis"), CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis)))
            'cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 2, "Salary Cost Breakdown by Cost Center"), CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Salary_Cost_Breakdown_by_Cost_Center)))
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 1, "Monthly Employee Budget Variance Analysis By Project Report"), CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByProject)))
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 2, "Salary Cost Breakdown by Cost Center By Project Report"), CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Salary_Cost_Breakdown_by_Cost_Center_ByProject)))
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 8, "Monthly Employee Budget Variance Analysis By Activity Report"), CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByActivity)))
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 9, "Salary Cost Breakdown by Cost Center By Activity Report"), CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Salary_Cost_Breakdown_by_Cost_Center_ByActivity)))
            'Pinkal (09-Oct-2017) -- End


            cboReportType.SelectedIndex = 0

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'Dim intDefaultBudget As Integer = objBudget.GetDefaultBudgetID()
            'dsCombo = objBudget.GetComboList("Budget", False, False)
            'With cboBudget
            '    .ValueMember = "budgetunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("Budget")
            '    If .Items.Count > 0 Then
            '        If intDefaultBudget > 0 Then
            '            .SelectedValue = 0
            '        Else
            '            .SelectedIndex = 0
            '        End If
            '    End If
            'End With
            dsCombo = objMaster.getComboListForBudgetViewBy("BudgetFor", True)
            With cboBudgetFor
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("BudgetFor")
                        .SelectedValue = 0
            End With
            'Sohail (13 Oct 2017) -- End

            'Varsha Rana (24-Aug-2017) -- Start
            'Enhancement -Payroll - Monthly Employee budget variance analysis Report 
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "PersonnelCost", True, enTranHeadType.Informational)
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("PersonnelCost")
                If .Items.Count > 0 Then
                    .SelectedValue = 0
                End If
            End With
            'Varsha Rana (24-Aug-2017) -- End

            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objBudget = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objSalaryBudget.setDefaultOrderBy(0)
            txtOrderBy.Text = objSalaryBudget.OrderByDisplay
            'If cboBudget.Items.Count > 0 Then cboBudget.SelectedIndex = 0 'Sohail (13 Oct 2017)

            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvanceFilter = ""

            'Varsha Rana (24-Aug-2017) -- Start
            'Enhancement -Payroll - Monthly Employee budget variance analysis Report
            cboTranHead.SelectedValue = 0
            Call SetDefaultSearchText(cboTranHead)
            ' Varsha Rana (24-Aug-2017) -- End

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            cboBudgetFor.SelectedValue = 0
            'Sohail (13 Oct 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (24-Aug-2017) -- Start
    'Enhancement -Payroll - Monthly Employee budget variance analysis Report
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 7, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (24-Aug-2017) -- End

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Public Sub FillList()
        Dim objBudgetCodes As New clsBudgetcodes_master
        Try


            dgPeriod.DataSource = Nothing
            'Sohail (08 Nov 2017) -- Start
            'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
            RemoveHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged
            If objchkPeriodSelectAll.Checked = True Then objchkPeriodSelectAll.Checked = False
            AddHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged
            'Sohail (08 Nov 2017) -- End
            RemoveHandler txtSearchPeriod.TextChanged, AddressOf txtSearchPeriod_TextChanged
            Call SetDefaultSearchPeriodText()
            AddHandler txtSearchPeriod.TextChanged, AddressOf txtSearchPeriod_TextChanged

            Dim dsPeriodList As DataSet = objBudgetCodes.GetList("List", 0, " AND bgbudget_master.viewbyid = " & CInt(cboBudgetFor.SelectedValue) & " AND bgbudget_master.allocationbyid = " & CInt(cboAllocationByID.SelectedValue) & " ")

            Dim dtTable As DataTable = New DataView(dsPeriodList.Tables("List")).ToTable
            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvPeriod = dtTable.DefaultView
            dgPeriod.AutoGenerateColumns = False
            objdgperiodcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhPeriodunkid.DataPropertyName = "periodunkid"
            objdgcolhBudgetcodesunkid.DataPropertyName = "budgetcodesunkid"
            objdgcolhBudgetunkid.DataPropertyName = "budgetunkid"
            dgColhPeriod.DataPropertyName = "period_name"
            dgcolhBudget.DataPropertyName = "budget_name"
            objdgcolhPeriodStart.DataPropertyName = "start_date"
            objdgcolhPeriodEnd.DataPropertyName = "end_date"

            dgPeriod.DataSource = dvPeriod
            dvPeriod.Sort = "IsChecked DESC, end_date, budget_name "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchPeriodText()
        Try
            mstrSearchPeriodText = lblSearchPeriod.Text
            With txtSearchPeriod
                .ForeColor = Color.Gray
                .Text = mstrSearchPeriodText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchPeriodText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetPeriodCheckBoxValue()
        Try

            RemoveHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgPeriod.CurrentRow.Cells(objdgperiodcolhCheck.Index).Value)

            Dim intCount As Integer = dvPeriod.Table.Select("IsChecked = 1").Length

            If intCount <= 0 Then
                objchkPeriodSelectAll.CheckState = CheckState.Unchecked
            ElseIf intCount < dgPeriod.Rows.Count Then
                objchkPeriodSelectAll.CheckState = CheckState.Indeterminate
            ElseIf intCount = dgPeriod.Rows.Count Then
                objchkPeriodSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            If dvPeriod IsNot Nothing Then
                For Each dr As DataRowView In dvPeriod
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvPeriod.ToTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllPeriod", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Oct 2017) -- End

#End Region

#Region " Form's Events "

    Private Sub frmMonthlyEmployeeBudgetVarianceAnalysisReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSalaryBudget = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyEmployeeBudgetVarianceAnalysisReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMonthlyEmployeeBudgetVarianceAnalysisReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            eZeeHeader.Title = objSalaryBudget._ReportName
            eZeeHeader.Message = objSalaryBudget._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryBudgetBreakdownt_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBudget.SelectedIndexChanged
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objBudgetCodes.GetComboListPeriod("Period", True, CInt(cboBudget.SelectedValue))
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Dim objBudget As New clsBudget_MasterNew
            objBudget._Budgetunkid = CInt(cboBudget.SelectedValue)
            If objBudget._Viewbyid = enBudgetViewBy.Employee Then
                lnkAnalysisBy.Visible = True
            Else
                lnkAnalysisBy.Visible = False
            End If
            'Sohail (18 Aug 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Aug 2017) -- Start
    'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""


            'Pinkal (09-Oct-2017) -- Start
            'Enhancement - Working on Budget Report Changes for CCBRT.
            'If CType(cboReportType.SelectedItem, ComboBoxValue).Value <= clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis Then
            '    lnkAnalysisBy.Visible = True
            'Else
            '    lnkAnalysisBy.Visible = False
            'End If
            'Sohail (08 Nov 2017) -- Start
            'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
            If CInt(cboBudgetFor.SelectedValue) = enBudgetViewBy.Employee Then
                'Sohail (08 Nov 2017) -- End
            If CType(cboReportType.SelectedItem, ComboBoxValue).Value = clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByProject OrElse _
                    CType(cboReportType.SelectedItem, ComboBoxValue).Value = clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByActivity Then
                lnkAnalysisBy.Visible = True
            Else
                lnkAnalysisBy.Visible = False
            End If
                'Sohail (08 Nov 2017) -- Start
                'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
            Else
                lnkAnalysisBy.Visible = False
            End If
            'Sohail (08 Nov 2017) -- End
            'Pinkal (09-Oct-2017) -- End
        Catch ex As Exception

        End Try
    End Sub
    'Sohail (18 Aug 2017) -- End

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
    Private Sub cboBudgetFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBudgetFor.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try
            dsCombo = objMaster.GetReportAllocation("List", "", False)
            Dim dr As DataRow = dsCombo.Tables(0).NewRow
            dr.Item("Id") = 0
            dr.Item("Name") = "Employee"
            dsCombo.Tables(0).Rows.Add(dr)

            If CInt(cboBudgetFor.SelectedValue) = enBudgetViewBy.Employee Then
                dtTable = New DataView(dsCombo.Tables(0), "Id = 0 ", "", DataViewRowState.CurrentRows).ToTable

                If CType(cboReportType.SelectedItem, ComboBoxValue).Value = clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByProject OrElse _
                                   CType(cboReportType.SelectedItem, ComboBoxValue).Value = clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByActivity Then
                    lnkAnalysisBy.Visible = True
                Else
                    lnkAnalysisBy.Visible = False
                End If
            Else
                dtTable = New DataView(dsCombo.Tables(0), "Id <> 0 ", "", DataViewRowState.CurrentRows).ToTable

                lnkAnalysisBy.Visible = False
            End If

            With cboAllocationByID
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudgetFor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAllocationByID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocationByID.SelectedIndexChanged
        Try
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocationByID", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Oct 2017) -- End

    'Sohail (24-Aug-2017) -- Start
    'Enhancement -Payroll - Monthly Employee budget variance analysis Report
    Private Sub cboTranHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.GotFocus
        Try
            With cboTranHead
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTranHead.Leave
        Try
            If CInt(cboTranHead.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboTranHead)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTranHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTranHead.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboTranHead.ValueMember
                    .DisplayMember = cboTranHead.DisplayMember
                    .DataSource = CType(cboTranHead.DataSource, DataTable)
                    .CodeMember = "code"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cboTranHead.SelectedValue = frm.SelectedValue
                    cboTranHead.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboTranHead.Text = ""
                    cboTranHead.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTranHead_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Sohail (24-Aug-2017) -- End

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If CType(cboReportType.SelectedItem, ComboBoxValue).Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Report Type."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'ElseIf cboBudget.Items.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, There is no Default budget. Please set Default budget from Budget List screen"), enMsgBoxStyle.Information)
                '    Exit Sub
                'ElseIf cboBudget.SelectedValue <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Budget is compulsory infomation. Please select Budget to continue."), enMsgBoxStyle.Information)
                '    cboBudget.Focus()
                '    Exit Sub
                'ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Period is compulsory infomation. Please select Period to continue."), enMsgBoxStyle.Information)
                '    cboPeriod.Focus()
                '    Exit Sub
            ElseIf CInt(cboBudgetFor.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Budget For is compulsory infomation. Please select Budget For to continue."), enMsgBoxStyle.Information)
                cboBudgetFor.Focus()
                Exit Sub
            ElseIf dvPeriod.Table.Select("IsChecked = 1 ").Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 222, "Please Select atleast one Period."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (13 Oct 2017) -- End
            End If


            objSalaryBudget.SetDefaultValue()

            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'objSalaryBudget._BudgetId = cboBudget.SelectedValue
            'objSalaryBudget._BudgetName = cboBudget.Text

            'objSalaryBudget._PeriodId = cboPeriod.SelectedValue
            'objSalaryBudget._PeriodName = cboPeriod.Text
            objSalaryBudget._BudgetCodesIDs = String.Join(",", (From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of Integer)("budgetcodesunkid").ToString())).ToArray)

            objSalaryBudget._BudgetCodesPeriodIDs = String.Join(",", (From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of Integer)("periodunkid").ToString())).ToArray)
            objSalaryBudget._BudgetCodesPeriodNames = String.Join(", ", (From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of String)("period_name"))).ToArray)

            objSalaryBudget._Period_StartDate = eZeeDate.convertDate((From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of String)("start_date"))).Min.ToString())
            objSalaryBudget._Period_EndDate = eZeeDate.convertDate((From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of String)("end_date"))).Max.ToString())

            objSalaryBudget._BudgetIDs = String.Join(",", (From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of Integer)("budgetunkid").ToString())).Distinct().ToArray)
            objSalaryBudget._BudgetNames = String.Join(", ", (From p In dvPeriod.Table Where (p.Field(Of Boolean)("IsChecked") = True) Select (p.Field(Of String)("budget_name"))).Distinct().ToArray)

            objSalaryBudget._BudgetViewByID = CInt(cboBudgetFor.SelectedValue)
            'Sohail (13 Oct 2017) -- End

            'Sohail (08 Nov 2017) -- Start
            'Issue - 70.1 - Incorrect syntax near the keyword 'ON'.
            objSalaryBudget._AllocationByID = CInt(cboAllocationByID.SelectedValue)
            'Sohail (08 Nov 2017) -- End

            'Varsha Rana (24-Aug-2017) -- Start
            'Enhancement -Payroll - Monthly Employee budget variance analysis Report
            objSalaryBudget._TranHeadId = cboTranHead.SelectedValue
            objSalaryBudget._TranHeadName = cboTranHead.Text
            ' Varsha Rana (24-Aug-2017) -- End




            'Sohail (18 Aug 2017) -- Start
            'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
            objSalaryBudget._ViewByIds = mstrStringIds
            objSalaryBudget._ViewIndex = mintViewIdx
            objSalaryBudget._ViewByName = mstrStringName
            objSalaryBudget._Analysis_Fields = mstrAnalysis_Fields
            objSalaryBudget._Analysis_Join = mstrAnalysis_Join
            objSalaryBudget._Analysis_OrderBy = mstrAnalysis_OrderBy
            objSalaryBudget._Report_GroupName = mstrReport_GroupName
            'Sohail (18 Aug 2017) -- End


            'objReconciliation._Advance_Filter = mstrAdvanceFilter

            'Pinkal (09-Oct-2017) -- Start
            'Enhancement - Working on Budget Report Changes for CCBRT.
            'If CType(cboReportType.SelectedItem, ComboBoxValue).Value = CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis) Then
            If CType(cboReportType.SelectedItem, ComboBoxValue).Value = CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByProject) Then
                'Pinkal (09-Oct-2017) -- End
                If objSalaryBudget.Export_Report(cboReportType.Text, User._Object._Userunkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                    If objSalaryBudget._Message.Trim <> "" Then
                        eZeeMsgBox.Show(objSalaryBudget._Message, enMsgBoxStyle.Information)
                    End If
                End If

                'Pinkal (09-Oct-2017) -- Start
                'Enhancement - Working on Budget Report Changes for CCBRT.
                'ElseIf CType(cboReportType.SelectedItem, ComboBoxValue).Value = CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Salary_Cost_Breakdown_by_Cost_Center) Then
            ElseIf CType(cboReportType.SelectedItem, ComboBoxValue).Value = CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Salary_Cost_Breakdown_by_Cost_Center_ByProject) Then

                If objSalaryBudget.Export_Salary_Cost_Breakdown_by_Cost_Center_Report(cboReportType.Text, User._Object._Userunkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                    If objSalaryBudget._Message.Trim <> "" Then
                        eZeeMsgBox.Show(objSalaryBudget._Message, enMsgBoxStyle.Information)
                    End If
                End If

            ElseIf CType(cboReportType.SelectedItem, ComboBoxValue).Value = CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Monthly_Employee_Budget_Variance_Analysis_ByActivity) Then

                If objSalaryBudget.Export_Monthly_Employee_Budget_Variance_Analysis_ByActivtiy_Report(cboReportType.Text, User._Object._Userunkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                    If objSalaryBudget._Message.Trim <> "" Then
                        eZeeMsgBox.Show(objSalaryBudget._Message, enMsgBoxStyle.Information)
                    End If
                End If
            ElseIf CType(cboReportType.SelectedItem, ComboBoxValue).Value = CInt(clsMonthlyEmployeeBudgetVarianceAnalysis.enReportType.Salary_Cost_Breakdown_by_Cost_Center_ByActivity) Then

                If objSalaryBudget.Export_Salary_Cost_Breakdown_by_Cost_Center_ByActivity_Report(cboReportType.Text, User._Object._Userunkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True, False, True, GUI.fmtCurrency, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                    If objSalaryBudget._Message.Trim <> "" Then
                        eZeeMsgBox.Show(objSalaryBudget._Message, enMsgBoxStyle.Information)
                    End If
                End If

                'Pinkal (09-Oct-2017) -- End

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMonthlyEmployeeBudgetVarianceAnalysis.SetMessages()
            objfrm._Other_ModuleNames = "clsMonthlyEmployeeBudgetVarianceAnalysisReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objSalaryBudget.setOrderBy(0)
            txtOrderBy.Text = objSalaryBudget.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (24-Aug-2017) -- Start
    'Enhancement -Payroll - Monthly Employee budget variance analysis Report 

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboTranHead
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub
    'varsha Rana (24-Aug-2017) -- End
#End Region

#Region "LinkButton Event"

    'Sohail (18 Aug 2017) -- Start
    'Enhancement - 69.1 - Analysis by option on Monthly Employee Budget Variance Analysis report type in Monthly Employee Budget Variance Analysis Report if budget allocation is on employee.
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (18 Aug 2017) -- End
#End Region

    'Sohail (13 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
#Region " Gridview Events "
    Private Sub dgPeriod_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgPeriod.CurrentCellDirtyStateChanged
        Try
            If dgPeriod.IsCurrentCellDirty Then
                dgPeriod.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPeriod_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPeriod_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgPeriod.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgperiodcolhCheck.Index Then
                SetPeriodCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPeriod_CellValueChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Checkbox Events "
    Private Sub objchkPeriodSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkPeriodSelectAll.CheckedChanged
        Try
            Call CheckAllPeriod(objchkPeriodSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox Events "
    Private Sub txtSearchPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.GotFocus
        Try
            With txtSearchPeriod
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchPeriodText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.Leave
        Try
            If txtSearchPeriod.Text.Trim = "" Then
                Call SetDefaultSearchPeriodText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.TextChanged
        Try
            If txtSearchPeriod.Text.Trim = mstrSearchPeriodText Then Exit Sub
            If dvPeriod IsNot Nothing Then
                dvPeriod.RowFilter = "budget_name LIKE '%" & txtSearchPeriod.Text.Replace("'", "''") & "%'  OR period_name LIKE '%" & txtSearchPeriod.Text.Replace("'", "''") & "%'"
                dgPeriod.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_TextChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (13 Oct 2017) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblBudget.Text = Language._Object.getCaption(Me.lblBudget.Name, Me.lblBudget.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblPersonnelCost.Text = Language._Object.getCaption(Me.lblPersonnelCost.Name, Me.lblPersonnelCost.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Monthly Employee Budget Variance Analysis By Project Report")
            Language.setMessage(mstrModuleName, 2, "Salary Cost Breakdown by Cost Center By Project Report")
			Language.setMessage(mstrModuleName, 3, "Please select Report Type.")
			Language.setMessage(mstrModuleName, 4, "Sorry, There is no Default budget. Please set Default budget from Budget List screen")
			Language.setMessage(mstrModuleName, 5, "Budget is compulsory infomation. Please select Budget to continue.")
			Language.setMessage(mstrModuleName, 6, "Period is compulsory infomation. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 7, "Type to Search")
            Language.setMessage(mstrModuleName, 8, "Monthly Employee Budget Variance Analysis By Activity Report")
            Language.setMessage(mstrModuleName, 9, "Salary Cost Breakdown by Cost Center By Activity Report")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
