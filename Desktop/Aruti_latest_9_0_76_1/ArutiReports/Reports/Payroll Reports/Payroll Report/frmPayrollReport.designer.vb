﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollReport
    Inherits Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollReport))
        Me.objLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.chkShowCRInSeparateColumns = New System.Windows.Forms.CheckBox
        Me.lblCostCenterCode = New System.Windows.Forms.Label
        Me.txtCostCenterCode = New System.Windows.Forms.TextBox
        Me.chkShowSavingsInSeparateColumns = New System.Windows.Forms.CheckBox
        Me.chkShowLoansInSeparateColumns = New System.Windows.Forms.CheckBox
        Me.chkIncludeEmployerContribution = New System.Windows.Forms.CheckBox
        Me.chkShowPaymentDetails = New System.Windows.Forms.CheckBox
        Me.lblIncludeHeads = New System.Windows.Forms.Label
        Me.lblSearchHead = New System.Windows.Forms.Label
        Me.txtSearchHead = New System.Windows.Forms.TextBox
        Me.pnlTranHead = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvTranHead = New System.Windows.Forms.ListView
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.lblExRate = New System.Windows.Forms.Label
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.chkIgnorezeroHead = New System.Windows.Forms.CheckBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.chkShowCompanyLogoOnReport = New System.Windows.Forms.CheckBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbCustomSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblTrnHeadType = New System.Windows.Forms.Label
        Me.objbtnTop = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnBottom = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkAllocation = New System.Windows.Forms.CheckBox
        Me.pnlCutomHeads = New System.Windows.Forms.Panel
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkCheckAll = New System.Windows.Forms.CheckBox
        Me.lvCustomTranHead = New System.Windows.Forms.ListView
        Me.objcolhCCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCCode = New System.Windows.Forms.ColumnHeader
        Me.colhCName = New System.Windows.Forms.ColumnHeader
        Me.colhCHeadTypeID = New System.Windows.Forms.ColumnHeader
        Me.txtSearchCHeads = New System.Windows.Forms.TextBox
        Me.lvAllocation_Hierarchy = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheckAll = New System.Windows.Forms.ColumnHeader
        Me.colhAllocation = New System.Windows.Forms.ColumnHeader
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.chkShowEmpNameinSeperateColumn = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlTranHead.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.gbCustomSetting.SuspendLayout()
        Me.pnlCutomHeads.SuspendLayout()
        Me.SuspendLayout()
        '
        'objLanguage
        '
        Me.objLanguage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.objLanguage.BackColor = System.Drawing.Color.White
        Me.objLanguage.BackgroundImage = CType(resources.GetObject("objLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objLanguage.FlatAppearance.BorderSize = 0
        Me.objLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objLanguage.ForeColor = System.Drawing.Color.Black
        Me.objLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.Location = New System.Drawing.Point(9, 18)
        Me.objLanguage.Name = "objLanguage"
        Me.objLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.Size = New System.Drawing.Size(25, 24)
        Me.objLanguage.TabIndex = 0
        Me.objLanguage.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmpNameinSeperateColumn)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowCRInSeparateColumns)
        Me.gbFilterCriteria.Controls.Add(Me.lblCostCenterCode)
        Me.gbFilterCriteria.Controls.Add(Me.txtCostCenterCode)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowSavingsInSeparateColumns)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowLoansInSeparateColumns)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeEmployerContribution)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowPaymentDetails)
        Me.gbFilterCriteria.Controls.Add(Me.lblIncludeHeads)
        Me.gbFilterCriteria.Controls.Add(Me.lblSearchHead)
        Me.gbFilterCriteria.Controls.Add(Me.txtSearchHead)
        Me.gbFilterCriteria.Controls.Add(Me.pnlTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblExRate)
        Me.gbFilterCriteria.Controls.Add(Me.lblToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnorezeroHead)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowCompanyLogoOnReport)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(495, 478)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 240)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(84, 15)
        Me.lblMembership.TabIndex = 247
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 230
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(98, 235)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(118, 21)
        Me.cboMembership.TabIndex = 246
        '
        'chkShowCRInSeparateColumns
        '
        Me.chkShowCRInSeparateColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCRInSeparateColumns.Location = New System.Drawing.Point(295, 211)
        Me.chkShowCRInSeparateColumns.Name = "chkShowCRInSeparateColumns"
        Me.chkShowCRInSeparateColumns.Size = New System.Drawing.Size(196, 18)
        Me.chkShowCRInSeparateColumns.TabIndex = 244
        Me.chkShowCRInSeparateColumns.Text = "Show CR In Separate Columns"
        Me.chkShowCRInSeparateColumns.UseVisualStyleBackColor = True
        '
        'lblCostCenterCode
        '
        Me.lblCostCenterCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenterCode.Location = New System.Drawing.Point(11, 265)
        Me.lblCostCenterCode.Name = "lblCostCenterCode"
        Me.lblCostCenterCode.Size = New System.Drawing.Size(81, 34)
        Me.lblCostCenterCode.TabIndex = 242
        Me.lblCostCenterCode.Text = "Cost Center Code"
        Me.lblCostCenterCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCostCenterCode
        '
        Me.txtCostCenterCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtCostCenterCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostCenterCode.Location = New System.Drawing.Point(98, 262)
        Me.txtCostCenterCode.Name = "txtCostCenterCode"
        Me.txtCostCenterCode.Size = New System.Drawing.Size(202, 21)
        Me.txtCostCenterCode.TabIndex = 241
        '
        'chkShowSavingsInSeparateColumns
        '
        Me.chkShowSavingsInSeparateColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSavingsInSeparateColumns.Location = New System.Drawing.Point(295, 187)
        Me.chkShowSavingsInSeparateColumns.Name = "chkShowSavingsInSeparateColumns"
        Me.chkShowSavingsInSeparateColumns.Size = New System.Drawing.Size(196, 18)
        Me.chkShowSavingsInSeparateColumns.TabIndex = 239
        Me.chkShowSavingsInSeparateColumns.Text = "Show Savings In Separate Columns"
        Me.chkShowSavingsInSeparateColumns.UseVisualStyleBackColor = True
        '
        'chkShowLoansInSeparateColumns
        '
        Me.chkShowLoansInSeparateColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLoansInSeparateColumns.Location = New System.Drawing.Point(98, 187)
        Me.chkShowLoansInSeparateColumns.Name = "chkShowLoansInSeparateColumns"
        Me.chkShowLoansInSeparateColumns.Size = New System.Drawing.Size(191, 18)
        Me.chkShowLoansInSeparateColumns.TabIndex = 237
        Me.chkShowLoansInSeparateColumns.Text = "Show Loans In Separate Columns"
        Me.chkShowLoansInSeparateColumns.UseVisualStyleBackColor = True
        '
        'chkIncludeEmployerContribution
        '
        Me.chkIncludeEmployerContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeEmployerContribution.Location = New System.Drawing.Point(295, 140)
        Me.chkIncludeEmployerContribution.Name = "chkIncludeEmployerContribution"
        Me.chkIncludeEmployerContribution.Size = New System.Drawing.Size(196, 18)
        Me.chkIncludeEmployerContribution.TabIndex = 233
        Me.chkIncludeEmployerContribution.Text = "Include Employer Contribution"
        Me.chkIncludeEmployerContribution.UseVisualStyleBackColor = True
        '
        'chkShowPaymentDetails
        '
        Me.chkShowPaymentDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPaymentDetails.Location = New System.Drawing.Point(295, 163)
        Me.chkShowPaymentDetails.Name = "chkShowPaymentDetails"
        Me.chkShowPaymentDetails.Size = New System.Drawing.Size(196, 18)
        Me.chkShowPaymentDetails.TabIndex = 231
        Me.chkShowPaymentDetails.Text = "Show Payment Details"
        Me.chkShowPaymentDetails.UseVisualStyleBackColor = True
        Me.chkShowPaymentDetails.Visible = False
        '
        'lblIncludeHeads
        '
        Me.lblIncludeHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncludeHeads.Location = New System.Drawing.Point(8, 287)
        Me.lblIncludeHeads.Name = "lblIncludeHeads"
        Me.lblIncludeHeads.Size = New System.Drawing.Size(84, 121)
        Me.lblIncludeHeads.TabIndex = 229
        Me.lblIncludeHeads.Text = "Include Allowance, Other Earning and Other Deduction Heads"
        '
        'lblSearchHead
        '
        Me.lblSearchHead.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchHead.Location = New System.Drawing.Point(8, 444)
        Me.lblSearchHead.Name = "lblSearchHead"
        Me.lblSearchHead.Size = New System.Drawing.Size(84, 29)
        Me.lblSearchHead.TabIndex = 227
        Me.lblSearchHead.Text = "Search Tran. Head"
        '
        'txtSearchHead
        '
        Me.txtSearchHead.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchHead.Location = New System.Drawing.Point(98, 451)
        Me.txtSearchHead.Name = "txtSearchHead"
        Me.txtSearchHead.Size = New System.Drawing.Size(337, 21)
        Me.txtSearchHead.TabIndex = 226
        '
        'pnlTranHead
        '
        Me.pnlTranHead.Controls.Add(Me.objchkSelectAll)
        Me.pnlTranHead.Controls.Add(Me.lvTranHead)
        Me.pnlTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTranHead.Location = New System.Drawing.Point(98, 287)
        Me.pnlTranHead.Name = "pnlTranHead"
        Me.pnlTranHead.Size = New System.Drawing.Size(337, 160)
        Me.pnlTranHead.TabIndex = 224
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvTranHead
        '
        Me.lvTranHead.CheckBoxes = True
        Me.lvTranHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhCode, Me.colhName})
        Me.lvTranHead.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTranHead.FullRowSelect = True
        Me.lvTranHead.GridLines = True
        Me.lvTranHead.HideSelection = False
        Me.lvTranHead.Location = New System.Drawing.Point(0, 0)
        Me.lvTranHead.Name = "lvTranHead"
        Me.lvTranHead.Size = New System.Drawing.Size(337, 160)
        Me.lvTranHead.TabIndex = 0
        Me.lvTranHead.UseCompatibleStateImageBehavior = False
        Me.lvTranHead.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 100
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 200
        '
        'lblExRate
        '
        Me.lblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExRate.Location = New System.Drawing.Point(230, 118)
        Me.lblExRate.Name = "lblExRate"
        Me.lblExRate.Size = New System.Drawing.Size(205, 14)
        Me.lblExRate.TabIndex = 222
        Me.lblExRate.Text = "#Value"
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(227, 91)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(84, 15)
        Me.lblToPeriod.TabIndex = 199
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 230
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(317, 88)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(118, 21)
        Me.cboToPeriod.TabIndex = 3
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(84, 15)
        Me.lblReportType.TabIndex = 197
        Me.lblReportType.Text = "Report Type"
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 230
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(98, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(337, 21)
        Me.cboReportType.TabIndex = 0
        '
        'chkIgnorezeroHead
        '
        Me.chkIgnorezeroHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnorezeroHead.Location = New System.Drawing.Point(98, 163)
        Me.chkIgnorezeroHead.Name = "chkIgnorezeroHead"
        Me.chkIgnorezeroHead.Size = New System.Drawing.Size(168, 18)
        Me.chkIgnorezeroHead.TabIndex = 6
        Me.chkIgnorezeroHead.Text = "Ignore Zero Value Heads"
        Me.chkIgnorezeroHead.UseVisualStyleBackColor = True
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(393, 5)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 195
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 119)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(84, 15)
        Me.lblBranch.TabIndex = 5
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 230
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(98, 114)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(118, 21)
        Me.cboBranch.TabIndex = 4
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(98, 141)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(168, 16)
        Me.chkInactiveemp.TabIndex = 5
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 90)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(84, 15)
        Me.lblPeriod.TabIndex = 3
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(98, 87)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(118, 21)
        Me.cboPeriod.TabIndex = 2
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(441, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(98, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(337, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(84, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(317, 88)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(118, 21)
        Me.cboCurrency.TabIndex = 219
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(230, 92)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(81, 13)
        Me.lblCurrency.TabIndex = 220
        Me.lblCurrency.Text = "Currency"
        '
        'chkShowCompanyLogoOnReport
        '
        Me.chkShowCompanyLogoOnReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCompanyLogoOnReport.Location = New System.Drawing.Point(98, 211)
        Me.chkShowCompanyLogoOnReport.Name = "chkShowCompanyLogoOnReport"
        Me.chkShowCompanyLogoOnReport.Size = New System.Drawing.Size(168, 18)
        Me.chkShowCompanyLogoOnReport.TabIndex = 235
        Me.chkShowCompanyLogoOnReport.Text = "Show Company Logo"
        Me.chkShowCompanyLogoOnReport.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(1063, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Payroll Report"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 609)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(1063, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(667, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 36
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(770, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(866, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(962, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(9, 550)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(495, 63)
        Me.gbSortBy.TabIndex = 20
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(441, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(66, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(80, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(355, 21)
        Me.txtOrderBy.TabIndex = 1
        '
'gbCustomSetting
        '
        Me.gbCustomSetting.BorderColor = System.Drawing.Color.Black
        Me.gbCustomSetting.Checked = False
        Me.gbCustomSetting.CollapseAllExceptThis = False
        Me.gbCustomSetting.CollapsedHoverImage = Nothing
        Me.gbCustomSetting.CollapsedNormalImage = Nothing
        Me.gbCustomSetting.CollapsedPressedImage = Nothing
        Me.gbCustomSetting.CollapseOnLoad = False
        Me.gbCustomSetting.Controls.Add(Me.cboTrnHeadType)
        Me.gbCustomSetting.Controls.Add(Me.lblTrnHeadType)
        Me.gbCustomSetting.Controls.Add(Me.objbtnTop)
        Me.gbCustomSetting.Controls.Add(Me.objbtnBottom)
        Me.gbCustomSetting.Controls.Add(Me.objchkAllocation)
        Me.gbCustomSetting.Controls.Add(Me.pnlCutomHeads)
        Me.gbCustomSetting.Controls.Add(Me.lvAllocation_Hierarchy)
        Me.gbCustomSetting.Controls.Add(Me.lnkSave)
        Me.gbCustomSetting.ExpandedHoverImage = Nothing
        Me.gbCustomSetting.ExpandedNormalImage = Nothing
        Me.gbCustomSetting.ExpandedPressedImage = Nothing
        Me.gbCustomSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCustomSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCustomSetting.HeaderHeight = 25
        Me.gbCustomSetting.HeaderMessage = ""
        Me.gbCustomSetting.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCustomSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCustomSetting.HeightOnCollapse = 0
        Me.gbCustomSetting.LeftTextSpace = 0
        Me.gbCustomSetting.Location = New System.Drawing.Point(512, 66)
        Me.gbCustomSetting.Name = "gbCustomSetting"
        Me.gbCustomSetting.OpenHeight = 300
        Me.gbCustomSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCustomSetting.ShowBorder = True
        Me.gbCustomSetting.ShowCheckBox = False
        Me.gbCustomSetting.ShowCollapseButton = False
        Me.gbCustomSetting.ShowDefaultBorderColor = True
        Me.gbCustomSetting.ShowDownButton = False
        Me.gbCustomSetting.ShowHeader = True
        Me.gbCustomSetting.Size = New System.Drawing.Size(421, 539)
        Me.gbCustomSetting.TabIndex = 21
        Me.gbCustomSetting.Temp = 0
        Me.gbCustomSetting.Text = "Custom Settings"
        Me.gbCustomSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbCustomSetting.Visible = False
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(135, 230)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(246, 21)
        Me.cboTrnHeadType.TabIndex = 159
        '
        'lblTrnHeadType
        '
        Me.lblTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHeadType.Location = New System.Drawing.Point(3, 233)
        Me.lblTrnHeadType.Name = "lblTrnHeadType"
        Me.lblTrnHeadType.Size = New System.Drawing.Size(126, 15)
        Me.lblTrnHeadType.TabIndex = 160
        Me.lblTrnHeadType.Text = "Transaction Head Type"
        '
        'objbtnTop
        '
        Me.objbtnTop.BackColor = System.Drawing.Color.White
        Me.objbtnTop.BackgroundImage = CType(resources.GetObject("objbtnTop.BackgroundImage"), System.Drawing.Image)
        Me.objbtnTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnTop.BorderColor = System.Drawing.Color.Empty
        Me.objbtnTop.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnTop.FlatAppearance.BorderSize = 0
        Me.objbtnTop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnTop.ForeColor = System.Drawing.Color.Black
        Me.objbtnTop.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnTop.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnTop.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTop.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTop.Image = Global.ArutiReports.My.Resources.Resources.MoveUp_16
        Me.objbtnTop.Location = New System.Drawing.Point(383, 26)
        Me.objbtnTop.Name = "objbtnTop"
        Me.objbtnTop.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTop.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTop.Size = New System.Drawing.Size(32, 30)
        Me.objbtnTop.TabIndex = 228
        Me.objbtnTop.UseVisualStyleBackColor = False
        '
        'objbtnBottom
        '
        Me.objbtnBottom.BackColor = System.Drawing.Color.White
        Me.objbtnBottom.BackgroundImage = CType(resources.GetObject("objbtnBottom.BackgroundImage"), System.Drawing.Image)
        Me.objbtnBottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnBottom.BorderColor = System.Drawing.Color.Empty
        Me.objbtnBottom.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnBottom.FlatAppearance.BorderSize = 0
        Me.objbtnBottom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnBottom.ForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnBottom.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottom.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.Image = Global.ArutiReports.My.Resources.Resources.MoveDown_16
        Me.objbtnBottom.Location = New System.Drawing.Point(383, 57)
        Me.objbtnBottom.Name = "objbtnBottom"
        Me.objbtnBottom.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottom.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.Size = New System.Drawing.Size(32, 30)
        Me.objbtnBottom.TabIndex = 229
        Me.objbtnBottom.UseVisualStyleBackColor = False
        '
        'objchkAllocation
        '
        Me.objchkAllocation.AutoSize = True
        Me.objchkAllocation.Location = New System.Drawing.Point(8, 32)
        Me.objchkAllocation.Name = "objchkAllocation"
        Me.objchkAllocation.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllocation.TabIndex = 22
        Me.objchkAllocation.UseVisualStyleBackColor = True
        '
        'pnlCutomHeads
        '
        Me.pnlCutomHeads.Controls.Add(Me.objbtnUp)
        Me.pnlCutomHeads.Controls.Add(Me.objbtnDown)
        Me.pnlCutomHeads.Controls.Add(Me.objchkCheckAll)
        Me.pnlCutomHeads.Controls.Add(Me.lvCustomTranHead)
        Me.pnlCutomHeads.Controls.Add(Me.txtSearchCHeads)
        Me.pnlCutomHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCutomHeads.Location = New System.Drawing.Point(2, 257)
        Me.pnlCutomHeads.Name = "pnlCutomHeads"
        Me.pnlCutomHeads.Size = New System.Drawing.Size(416, 279)
        Me.pnlCutomHeads.TabIndex = 225
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.ArutiReports.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(381, 23)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 226
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.ArutiReports.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(381, 54)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 227
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'objchkCheckAll
        '
        Me.objchkCheckAll.AutoSize = True
        Me.objchkCheckAll.Location = New System.Drawing.Point(8, 28)
        Me.objchkCheckAll.Name = "objchkCheckAll"
        Me.objchkCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkCheckAll.TabIndex = 18
        Me.objchkCheckAll.UseVisualStyleBackColor = True
        '
        'lvCustomTranHead
        '
        Me.lvCustomTranHead.CheckBoxes = True
        Me.lvCustomTranHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCCheck, Me.colhCCode, Me.colhCName, Me.colhCHeadTypeID})
        Me.lvCustomTranHead.FullRowSelect = True
        Me.lvCustomTranHead.GridLines = True
        Me.lvCustomTranHead.HideSelection = False
        Me.lvCustomTranHead.Location = New System.Drawing.Point(2, 23)
        Me.lvCustomTranHead.MultiSelect = False
        Me.lvCustomTranHead.Name = "lvCustomTranHead"
        Me.lvCustomTranHead.Size = New System.Drawing.Size(377, 253)
        Me.lvCustomTranHead.TabIndex = 0
        Me.lvCustomTranHead.UseCompatibleStateImageBehavior = False
        Me.lvCustomTranHead.View = System.Windows.Forms.View.Details
        '
        'objcolhCCheck
        '
        Me.objcolhCCheck.Tag = "objcolhCCheck"
        Me.objcolhCCheck.Text = ""
        Me.objcolhCCheck.Width = 25
        '
        'colhCCode
        '
        Me.colhCCode.Tag = "colhCCode"
        Me.colhCCode.Text = "Code"
        Me.colhCCode.Width = 100
        '
        'colhCName
        '
        Me.colhCName.Tag = "colhCName"
        Me.colhCName.Text = "Name"
        Me.colhCName.Width = 240
        '
        'colhCHeadTypeID
        '
        Me.colhCHeadTypeID.Tag = "colhCHeadTypeID"
        Me.colhCHeadTypeID.Text = "Head Type ID"
        Me.colhCHeadTypeID.Width = 0
        '
        'txtSearchCHeads
        '
        Me.txtSearchCHeads.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchCHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchCHeads.Location = New System.Drawing.Point(0, 0)
        Me.txtSearchCHeads.Name = "txtSearchCHeads"
        Me.txtSearchCHeads.Size = New System.Drawing.Size(379, 21)
        Me.txtSearchCHeads.TabIndex = 227
        '
        'lvAllocation_Hierarchy
        '
        Me.lvAllocation_Hierarchy.BackColorOnChecked = False
        Me.lvAllocation_Hierarchy.CheckBoxes = True
        Me.lvAllocation_Hierarchy.ColumnHeaders = Nothing
        Me.lvAllocation_Hierarchy.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheckAll, Me.colhAllocation})
        Me.lvAllocation_Hierarchy.CompulsoryColumns = ""
        Me.lvAllocation_Hierarchy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation_Hierarchy.FullRowSelect = True
        Me.lvAllocation_Hierarchy.GridLines = True
        Me.lvAllocation_Hierarchy.GroupingColumn = Nothing
        Me.lvAllocation_Hierarchy.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation_Hierarchy.HideSelection = False
        Me.lvAllocation_Hierarchy.Location = New System.Drawing.Point(2, 26)
        Me.lvAllocation_Hierarchy.MinColumnWidth = 50
        Me.lvAllocation_Hierarchy.MultiSelect = False
        Me.lvAllocation_Hierarchy.Name = "lvAllocation_Hierarchy"
        Me.lvAllocation_Hierarchy.OptionalColumns = ""
        Me.lvAllocation_Hierarchy.ShowMoreItem = False
        Me.lvAllocation_Hierarchy.ShowSaveItem = False
        Me.lvAllocation_Hierarchy.ShowSelectAll = True
        Me.lvAllocation_Hierarchy.ShowSizeAllColumnsToFit = True
        Me.lvAllocation_Hierarchy.Size = New System.Drawing.Size(379, 198)
        Me.lvAllocation_Hierarchy.Sortable = True
        Me.lvAllocation_Hierarchy.TabIndex = 13
        Me.lvAllocation_Hierarchy.UseCompatibleStateImageBehavior = False
        Me.lvAllocation_Hierarchy.View = System.Windows.Forms.View.Details
        '
        'objcolhCheckAll
        '
        Me.objcolhCheckAll.Tag = "objcolhCheckAll"
        Me.objcolhCheckAll.Text = ""
        Me.objcolhCheckAll.Width = 25
        '
        'colhAllocation
        '
        Me.colhAllocation.Tag = "colhAllocation"
        Me.colhAllocation.Text = "Allocations/Other Details"
        Me.colhAllocation.Width = 300
        '
        'lnkSave
        '
        Me.lnkSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSave.BackColor = System.Drawing.Color.Transparent
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.Location = New System.Drawing.Point(277, 2)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(141, 21)
        Me.lnkSave.TabIndex = 1
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "Save Settings"
        Me.lnkSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkShowEmpNameinSeperateColumn
        '
        Me.chkShowEmpNameinSeperateColumn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmpNameinSeperateColumn.Location = New System.Drawing.Point(295, 235)
        Me.chkShowEmpNameinSeperateColumn.Name = "chkShowEmpNameinSeperateColumn"
        Me.chkShowEmpNameinSeperateColumn.Size = New System.Drawing.Size(206, 18)
        Me.chkShowEmpNameinSeperateColumn.TabIndex = 249
        Me.chkShowEmpNameinSeperateColumn.Text = "Show Emp Name in Seperate Columns"
        Me.chkShowEmpNameinSeperateColumn.UseVisualStyleBackColor = True
        '
        'frmPayrollReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1063, 664)
        Me.Controls.Add(Me.gbCustomSetting)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayrollReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payroll Report"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.pnlTranHead.ResumeLayout(False)
        Me.pnlTranHead.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbCustomSetting.ResumeLayout(False)
        Me.gbCustomSetting.PerformLayout()
        Me.pnlCutomHeads.ResumeLayout(False)
        Me.pnlCutomHeads.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents chkIgnorezeroHead As System.Windows.Forms.CheckBox
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents lblExRate As System.Windows.Forms.Label
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents pnlTranHead As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvTranHead As System.Windows.Forms.ListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Private WithEvents lblSearchHead As System.Windows.Forms.Label
    Private WithEvents txtSearchHead As System.Windows.Forms.TextBox
    Friend WithEvents lblIncludeHeads As System.Windows.Forms.Label
    Friend WithEvents chkShowPaymentDetails As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeEmployerContribution As System.Windows.Forms.CheckBox
    Friend WithEvents gbCustomSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlCutomHeads As System.Windows.Forms.Panel
    Friend WithEvents objchkCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvCustomTranHead As System.Windows.Forms.ListView
    Friend WithEvents objcolhCCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvAllocation_Hierarchy As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheckAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAllocation As System.Windows.Forms.ColumnHeader
    Private WithEvents txtSearchCHeads As System.Windows.Forms.TextBox
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAllocation As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnTop As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnBottom As eZee.Common.eZeeLightButton
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHeadType As System.Windows.Forms.Label
    Friend WithEvents chkShowCompanyLogoOnReport As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLoansInSeparateColumns As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowSavingsInSeparateColumns As System.Windows.Forms.CheckBox
    Friend WithEvents colhCHeadTypeID As System.Windows.Forms.ColumnHeader
    Private WithEvents txtCostCenterCode As System.Windows.Forms.TextBox
    Friend WithEvents lblCostCenterCode As System.Windows.Forms.Label
    Friend WithEvents chkShowCRInSeparateColumns As System.Windows.Forms.CheckBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowEmpNameinSeperateColumn As System.Windows.Forms.CheckBox
End Class
