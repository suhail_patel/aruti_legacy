﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEndOfServiceReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEndOfServiceReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.cboBalanceDaysAmt = New System.Windows.Forms.ComboBox
        Me.lblBalanceDaysAmt = New System.Windows.Forms.Label
        Me.cboBalanceMonthAmt = New System.Windows.Forms.ComboBox
        Me.lblBalanceMonthAmt = New System.Windows.Forms.Label
        Me.cboBalanceYearAmt = New System.Windows.Forms.ComboBox
        Me.lblBalanceYearAmt = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objelLineGratuity = New eZee.Common.eZeeLine
        Me.objelLineLeave = New eZee.Common.eZeeLine
        Me.objelLineOT = New eZee.Common.eZeeLine
        Me.cboLeaveBalanceHead = New System.Windows.Forms.ComboBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblLeaveBalanceHead = New System.Windows.Forms.Label
        Me.cboBalanceDays = New System.Windows.Forms.ComboBox
        Me.lblBalanceDays = New System.Windows.Forms.Label
        Me.cboBalanceMonth = New System.Windows.Forms.ComboBox
        Me.lblBalanceMonth = New System.Windows.Forms.Label
        Me.cboBalanceYear = New System.Windows.Forms.ComboBox
        Me.lblBalanceYear = New System.Windows.Forms.Label
        Me.cboForFirst3Years = New System.Windows.Forms.ComboBox
        Me.lblForFirst3Years = New System.Windows.Forms.Label
        Me.cboGrossPay = New System.Windows.Forms.ComboBox
        Me.lblGrossPay = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboOT4Amount = New System.Windows.Forms.ComboBox
        Me.lblOT4Amount = New System.Windows.Forms.Label
        Me.cboOT3Amount = New System.Windows.Forms.ComboBox
        Me.lblOT3Amount = New System.Windows.Forms.Label
        Me.cboOT2Amount = New System.Windows.Forms.ComboBox
        Me.lblOT2Amount = New System.Windows.Forms.Label
        Me.cboOT1Amount = New System.Windows.Forms.ComboBox
        Me.lblOT1Amount = New System.Windows.Forms.Label
        Me.cboOT4Hours = New System.Windows.Forms.ComboBox
        Me.lblOT4Hours = New System.Windows.Forms.Label
        Me.cboOT3Hours = New System.Windows.Forms.ComboBox
        Me.lblOT3Hours = New System.Windows.Forms.Label
        Me.cboOT2Hours = New System.Windows.Forms.ComboBox
        Me.lblOT2Hours = New System.Windows.Forms.Label
        Me.cboOT1Hours = New System.Windows.Forms.ComboBox
        Me.lblOT1Hours = New System.Windows.Forms.Label
        Me.lvOriginalHeads = New System.Windows.Forms.ListView
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhTranCode = New System.Windows.Forms.ColumnHeader
        Me.colhTranName = New System.Windows.Forms.ColumnHeader
        Me.colhTranHeadType = New System.Windows.Forms.ColumnHeader
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 583)
        Me.NavPanel.Size = New System.Drawing.Size(885, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objchkSelectAll)
        Me.gbFilterCriteria.Controls.Add(Me.cboBalanceDaysAmt)
        Me.gbFilterCriteria.Controls.Add(Me.lblBalanceDaysAmt)
        Me.gbFilterCriteria.Controls.Add(Me.cboBalanceMonthAmt)
        Me.gbFilterCriteria.Controls.Add(Me.lblBalanceMonthAmt)
        Me.gbFilterCriteria.Controls.Add(Me.cboBalanceYearAmt)
        Me.gbFilterCriteria.Controls.Add(Me.lblBalanceYearAmt)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objelLineGratuity)
        Me.gbFilterCriteria.Controls.Add(Me.objelLineLeave)
        Me.gbFilterCriteria.Controls.Add(Me.objelLineOT)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveBalanceHead)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveBalanceHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboBalanceDays)
        Me.gbFilterCriteria.Controls.Add(Me.lblBalanceDays)
        Me.gbFilterCriteria.Controls.Add(Me.cboBalanceMonth)
        Me.gbFilterCriteria.Controls.Add(Me.lblBalanceMonth)
        Me.gbFilterCriteria.Controls.Add(Me.cboBalanceYear)
        Me.gbFilterCriteria.Controls.Add(Me.lblBalanceYear)
        Me.gbFilterCriteria.Controls.Add(Me.cboForFirst3Years)
        Me.gbFilterCriteria.Controls.Add(Me.lblForFirst3Years)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrossPay)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrossPay)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT4Amount)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT4Amount)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT3Amount)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT3Amount)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT2Amount)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT2Amount)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT1Amount)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT1Amount)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT4Hours)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT4Hours)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT3Hours)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT3Hours)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT2Hours)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT2Hours)
        Me.gbFilterCriteria.Controls.Add(Me.cboOT1Hours)
        Me.gbFilterCriteria.Controls.Add(Me.lblOT1Hours)
        Me.gbFilterCriteria.Controls.Add(Me.lvOriginalHeads)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(850, 442)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(502, 38)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 22
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'cboBalanceDaysAmt
        '
        Me.cboBalanceDaysAmt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBalanceDaysAmt.DropDownWidth = 200
        Me.cboBalanceDaysAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBalanceDaysAmt.FormattingEnabled = True
        Me.cboBalanceDaysAmt.Location = New System.Drawing.Point(350, 412)
        Me.cboBalanceDaysAmt.Name = "cboBalanceDaysAmt"
        Me.cboBalanceDaysAmt.Size = New System.Drawing.Size(126, 21)
        Me.cboBalanceDaysAmt.TabIndex = 19
        '
        'lblBalanceDaysAmt
        '
        Me.lblBalanceDaysAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceDaysAmt.Location = New System.Drawing.Point(243, 414)
        Me.lblBalanceDaysAmt.Name = "lblBalanceDaysAmt"
        Me.lblBalanceDaysAmt.Size = New System.Drawing.Size(101, 15)
        Me.lblBalanceDaysAmt.TabIndex = 121
        Me.lblBalanceDaysAmt.Text = "Balance Days"
        Me.lblBalanceDaysAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBalanceMonthAmt
        '
        Me.cboBalanceMonthAmt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBalanceMonthAmt.DropDownWidth = 200
        Me.cboBalanceMonthAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBalanceMonthAmt.FormattingEnabled = True
        Me.cboBalanceMonthAmt.Location = New System.Drawing.Point(350, 385)
        Me.cboBalanceMonthAmt.Name = "cboBalanceMonthAmt"
        Me.cboBalanceMonthAmt.Size = New System.Drawing.Size(126, 21)
        Me.cboBalanceMonthAmt.TabIndex = 18
        '
        'lblBalanceMonthAmt
        '
        Me.lblBalanceMonthAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceMonthAmt.Location = New System.Drawing.Point(243, 387)
        Me.lblBalanceMonthAmt.Name = "lblBalanceMonthAmt"
        Me.lblBalanceMonthAmt.Size = New System.Drawing.Size(101, 15)
        Me.lblBalanceMonthAmt.TabIndex = 119
        Me.lblBalanceMonthAmt.Text = "Balance Month Amt"
        Me.lblBalanceMonthAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBalanceYearAmt
        '
        Me.cboBalanceYearAmt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBalanceYearAmt.DropDownWidth = 200
        Me.cboBalanceYearAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBalanceYearAmt.FormattingEnabled = True
        Me.cboBalanceYearAmt.Location = New System.Drawing.Point(350, 358)
        Me.cboBalanceYearAmt.Name = "cboBalanceYearAmt"
        Me.cboBalanceYearAmt.Size = New System.Drawing.Size(126, 21)
        Me.cboBalanceYearAmt.TabIndex = 17
        '
        'lblBalanceYearAmt
        '
        Me.lblBalanceYearAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceYearAmt.Location = New System.Drawing.Point(243, 360)
        Me.lblBalanceYearAmt.Name = "lblBalanceYearAmt"
        Me.lblBalanceYearAmt.Size = New System.Drawing.Size(101, 15)
        Me.lblBalanceYearAmt.TabIndex = 117
        Me.lblBalanceYearAmt.Text = "Balance Year Amt"
        Me.lblBalanceYearAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(494, 383)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(342, 13)
        Me.EZeeLine1.TabIndex = 114
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLineGratuity
        '
        Me.objelLineGratuity.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLineGratuity.Location = New System.Drawing.Point(8, 313)
        Me.objelLineGratuity.Name = "objelLineGratuity"
        Me.objelLineGratuity.Size = New System.Drawing.Size(468, 13)
        Me.objelLineGratuity.TabIndex = 112
        Me.objelLineGratuity.Text = "Gratuity Details"
        Me.objelLineGratuity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLineLeave
        '
        Me.objelLineLeave.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLineLeave.Location = New System.Drawing.Point(8, 257)
        Me.objelLineLeave.Name = "objelLineLeave"
        Me.objelLineLeave.Size = New System.Drawing.Size(468, 13)
        Me.objelLineLeave.TabIndex = 111
        Me.objelLineLeave.Text = "Leave Details"
        Me.objelLineLeave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLineOT
        '
        Me.objelLineOT.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLineOT.Location = New System.Drawing.Point(8, 122)
        Me.objelLineOT.Name = "objelLineOT"
        Me.objelLineOT.Size = New System.Drawing.Size(468, 13)
        Me.objelLineOT.TabIndex = 110
        Me.objelLineOT.Text = "Overtime Details"
        Me.objelLineOT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeaveBalanceHead
        '
        Me.cboLeaveBalanceHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveBalanceHead.DropDownWidth = 200
        Me.cboLeaveBalanceHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveBalanceHead.FormattingEnabled = True
        Me.cboLeaveBalanceHead.Location = New System.Drawing.Point(350, 275)
        Me.cboLeaveBalanceHead.Name = "cboLeaveBalanceHead"
        Me.cboLeaveBalanceHead.Size = New System.Drawing.Size(126, 21)
        Me.cboLeaveBalanceHead.TabIndex = 12
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(494, 399)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 21
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblLeaveBalanceHead
        '
        Me.lblLeaveBalanceHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveBalanceHead.Location = New System.Drawing.Point(243, 277)
        Me.lblLeaveBalanceHead.Name = "lblLeaveBalanceHead"
        Me.lblLeaveBalanceHead.Size = New System.Drawing.Size(101, 27)
        Me.lblLeaveBalanceHead.TabIndex = 69
        Me.lblLeaveBalanceHead.Text = "Leave Balance Head"
        Me.lblLeaveBalanceHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBalanceDays
        '
        Me.cboBalanceDays.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBalanceDays.DropDownWidth = 200
        Me.cboBalanceDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBalanceDays.FormattingEnabled = True
        Me.cboBalanceDays.Location = New System.Drawing.Point(105, 412)
        Me.cboBalanceDays.Name = "cboBalanceDays"
        Me.cboBalanceDays.Size = New System.Drawing.Size(126, 21)
        Me.cboBalanceDays.TabIndex = 16
        '
        'lblBalanceDays
        '
        Me.lblBalanceDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceDays.Location = New System.Drawing.Point(8, 414)
        Me.lblBalanceDays.Name = "lblBalanceDays"
        Me.lblBalanceDays.Size = New System.Drawing.Size(91, 15)
        Me.lblBalanceDays.TabIndex = 66
        Me.lblBalanceDays.Text = "Balance Days"
        Me.lblBalanceDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBalanceMonth
        '
        Me.cboBalanceMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBalanceMonth.DropDownWidth = 200
        Me.cboBalanceMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBalanceMonth.FormattingEnabled = True
        Me.cboBalanceMonth.Location = New System.Drawing.Point(105, 385)
        Me.cboBalanceMonth.Name = "cboBalanceMonth"
        Me.cboBalanceMonth.Size = New System.Drawing.Size(126, 21)
        Me.cboBalanceMonth.TabIndex = 15
        '
        'lblBalanceMonth
        '
        Me.lblBalanceMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceMonth.Location = New System.Drawing.Point(8, 387)
        Me.lblBalanceMonth.Name = "lblBalanceMonth"
        Me.lblBalanceMonth.Size = New System.Drawing.Size(91, 15)
        Me.lblBalanceMonth.TabIndex = 64
        Me.lblBalanceMonth.Text = "Balance Month"
        Me.lblBalanceMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBalanceYear
        '
        Me.cboBalanceYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBalanceYear.DropDownWidth = 200
        Me.cboBalanceYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBalanceYear.FormattingEnabled = True
        Me.cboBalanceYear.Location = New System.Drawing.Point(105, 358)
        Me.cboBalanceYear.Name = "cboBalanceYear"
        Me.cboBalanceYear.Size = New System.Drawing.Size(126, 21)
        Me.cboBalanceYear.TabIndex = 14
        '
        'lblBalanceYear
        '
        Me.lblBalanceYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceYear.Location = New System.Drawing.Point(8, 360)
        Me.lblBalanceYear.Name = "lblBalanceYear"
        Me.lblBalanceYear.Size = New System.Drawing.Size(91, 15)
        Me.lblBalanceYear.TabIndex = 62
        Me.lblBalanceYear.Text = "Balance Year"
        Me.lblBalanceYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboForFirst3Years
        '
        Me.cboForFirst3Years.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboForFirst3Years.DropDownWidth = 200
        Me.cboForFirst3Years.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboForFirst3Years.FormattingEnabled = True
        Me.cboForFirst3Years.Location = New System.Drawing.Point(105, 331)
        Me.cboForFirst3Years.Name = "cboForFirst3Years"
        Me.cboForFirst3Years.Size = New System.Drawing.Size(126, 21)
        Me.cboForFirst3Years.TabIndex = 13
        '
        'lblForFirst3Years
        '
        Me.lblForFirst3Years.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblForFirst3Years.Location = New System.Drawing.Point(8, 333)
        Me.lblForFirst3Years.Name = "lblForFirst3Years"
        Me.lblForFirst3Years.Size = New System.Drawing.Size(91, 15)
        Me.lblForFirst3Years.TabIndex = 60
        Me.lblForFirst3Years.Text = "For First 3 Years"
        Me.lblForFirst3Years.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrossPay
        '
        Me.cboGrossPay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrossPay.DropDownWidth = 200
        Me.cboGrossPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrossPay.FormattingEnabled = True
        Me.cboGrossPay.Location = New System.Drawing.Point(105, 88)
        Me.cboGrossPay.Name = "cboGrossPay"
        Me.cboGrossPay.Size = New System.Drawing.Size(126, 21)
        Me.cboGrossPay.TabIndex = 2
        '
        'lblGrossPay
        '
        Me.lblGrossPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrossPay.Location = New System.Drawing.Point(8, 90)
        Me.lblGrossPay.Name = "lblGrossPay"
        Me.lblGrossPay.Size = New System.Drawing.Size(91, 15)
        Me.lblGrossPay.TabIndex = 57
        Me.lblGrossPay.Text = "Gross Pay"
        Me.lblGrossPay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 200
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(105, 275)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(126, 21)
        Me.cboLeaveType.TabIndex = 11
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(8, 277)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(91, 15)
        Me.lblLeaveType.TabIndex = 55
        Me.lblLeaveType.Text = "Annual Leave"
        Me.lblLeaveType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT4Amount
        '
        Me.cboOT4Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT4Amount.DropDownWidth = 200
        Me.cboOT4Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT4Amount.FormattingEnabled = True
        Me.cboOT4Amount.Location = New System.Drawing.Point(350, 223)
        Me.cboOT4Amount.Name = "cboOT4Amount"
        Me.cboOT4Amount.Size = New System.Drawing.Size(126, 21)
        Me.cboOT4Amount.TabIndex = 10
        '
        'lblOT4Amount
        '
        Me.lblOT4Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT4Amount.Location = New System.Drawing.Point(243, 225)
        Me.lblOT4Amount.Name = "lblOT4Amount"
        Me.lblOT4Amount.Size = New System.Drawing.Size(101, 15)
        Me.lblOT4Amount.TabIndex = 52
        Me.lblOT4Amount.Text = "OT 4 Amount"
        Me.lblOT4Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT3Amount
        '
        Me.cboOT3Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT3Amount.DropDownWidth = 200
        Me.cboOT3Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT3Amount.FormattingEnabled = True
        Me.cboOT3Amount.Location = New System.Drawing.Point(350, 196)
        Me.cboOT3Amount.Name = "cboOT3Amount"
        Me.cboOT3Amount.Size = New System.Drawing.Size(126, 21)
        Me.cboOT3Amount.TabIndex = 9
        '
        'lblOT3Amount
        '
        Me.lblOT3Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT3Amount.Location = New System.Drawing.Point(243, 198)
        Me.lblOT3Amount.Name = "lblOT3Amount"
        Me.lblOT3Amount.Size = New System.Drawing.Size(101, 15)
        Me.lblOT3Amount.TabIndex = 50
        Me.lblOT3Amount.Text = "OT 3 Amount"
        Me.lblOT3Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT2Amount
        '
        Me.cboOT2Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT2Amount.DropDownWidth = 200
        Me.cboOT2Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT2Amount.FormattingEnabled = True
        Me.cboOT2Amount.Location = New System.Drawing.Point(350, 169)
        Me.cboOT2Amount.Name = "cboOT2Amount"
        Me.cboOT2Amount.Size = New System.Drawing.Size(126, 21)
        Me.cboOT2Amount.TabIndex = 8
        '
        'lblOT2Amount
        '
        Me.lblOT2Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT2Amount.Location = New System.Drawing.Point(243, 171)
        Me.lblOT2Amount.Name = "lblOT2Amount"
        Me.lblOT2Amount.Size = New System.Drawing.Size(101, 15)
        Me.lblOT2Amount.TabIndex = 48
        Me.lblOT2Amount.Text = "OT 2 Amount"
        Me.lblOT2Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT1Amount
        '
        Me.cboOT1Amount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT1Amount.DropDownWidth = 200
        Me.cboOT1Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT1Amount.FormattingEnabled = True
        Me.cboOT1Amount.Location = New System.Drawing.Point(350, 142)
        Me.cboOT1Amount.Name = "cboOT1Amount"
        Me.cboOT1Amount.Size = New System.Drawing.Size(126, 21)
        Me.cboOT1Amount.TabIndex = 7
        '
        'lblOT1Amount
        '
        Me.lblOT1Amount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT1Amount.Location = New System.Drawing.Point(243, 144)
        Me.lblOT1Amount.Name = "lblOT1Amount"
        Me.lblOT1Amount.Size = New System.Drawing.Size(101, 15)
        Me.lblOT1Amount.TabIndex = 46
        Me.lblOT1Amount.Text = "OT 1 Amount"
        Me.lblOT1Amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT4Hours
        '
        Me.cboOT4Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT4Hours.DropDownWidth = 200
        Me.cboOT4Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT4Hours.FormattingEnabled = True
        Me.cboOT4Hours.Location = New System.Drawing.Point(105, 223)
        Me.cboOT4Hours.Name = "cboOT4Hours"
        Me.cboOT4Hours.Size = New System.Drawing.Size(126, 21)
        Me.cboOT4Hours.TabIndex = 6
        '
        'lblOT4Hours
        '
        Me.lblOT4Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT4Hours.Location = New System.Drawing.Point(8, 225)
        Me.lblOT4Hours.Name = "lblOT4Hours"
        Me.lblOT4Hours.Size = New System.Drawing.Size(91, 15)
        Me.lblOT4Hours.TabIndex = 44
        Me.lblOT4Hours.Text = "OT 4 Hours"
        Me.lblOT4Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT3Hours
        '
        Me.cboOT3Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT3Hours.DropDownWidth = 200
        Me.cboOT3Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT3Hours.FormattingEnabled = True
        Me.cboOT3Hours.Location = New System.Drawing.Point(105, 196)
        Me.cboOT3Hours.Name = "cboOT3Hours"
        Me.cboOT3Hours.Size = New System.Drawing.Size(126, 21)
        Me.cboOT3Hours.TabIndex = 5
        '
        'lblOT3Hours
        '
        Me.lblOT3Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT3Hours.Location = New System.Drawing.Point(8, 198)
        Me.lblOT3Hours.Name = "lblOT3Hours"
        Me.lblOT3Hours.Size = New System.Drawing.Size(91, 15)
        Me.lblOT3Hours.TabIndex = 42
        Me.lblOT3Hours.Text = "OT 3 Hours"
        Me.lblOT3Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT2Hours
        '
        Me.cboOT2Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT2Hours.DropDownWidth = 200
        Me.cboOT2Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT2Hours.FormattingEnabled = True
        Me.cboOT2Hours.Location = New System.Drawing.Point(105, 169)
        Me.cboOT2Hours.Name = "cboOT2Hours"
        Me.cboOT2Hours.Size = New System.Drawing.Size(126, 21)
        Me.cboOT2Hours.TabIndex = 4
        '
        'lblOT2Hours
        '
        Me.lblOT2Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT2Hours.Location = New System.Drawing.Point(8, 171)
        Me.lblOT2Hours.Name = "lblOT2Hours"
        Me.lblOT2Hours.Size = New System.Drawing.Size(91, 15)
        Me.lblOT2Hours.TabIndex = 40
        Me.lblOT2Hours.Text = "OT 2 Hours"
        Me.lblOT2Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOT1Hours
        '
        Me.cboOT1Hours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOT1Hours.DropDownWidth = 200
        Me.cboOT1Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOT1Hours.FormattingEnabled = True
        Me.cboOT1Hours.Location = New System.Drawing.Point(105, 142)
        Me.cboOT1Hours.Name = "cboOT1Hours"
        Me.cboOT1Hours.Size = New System.Drawing.Size(126, 21)
        Me.cboOT1Hours.TabIndex = 3
        '
        'lblOT1Hours
        '
        Me.lblOT1Hours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOT1Hours.Location = New System.Drawing.Point(8, 144)
        Me.lblOT1Hours.Name = "lblOT1Hours"
        Me.lblOT1Hours.Size = New System.Drawing.Size(91, 15)
        Me.lblOT1Hours.TabIndex = 38
        Me.lblOT1Hours.Text = "OT 1 Hours"
        Me.lblOT1Hours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvOriginalHeads
        '
        Me.lvOriginalHeads.CheckBoxes = True
        Me.lvOriginalHeads.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhTranCode, Me.colhTranName, Me.colhTranHeadType})
        Me.lvOriginalHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvOriginalHeads.FullRowSelect = True
        Me.lvOriginalHeads.GridLines = True
        Me.lvOriginalHeads.Location = New System.Drawing.Point(494, 34)
        Me.lvOriginalHeads.Name = "lvOriginalHeads"
        Me.lvOriginalHeads.ShowItemToolTips = True
        Me.lvOriginalHeads.Size = New System.Drawing.Size(342, 346)
        Me.lvOriginalHeads.TabIndex = 20
        Me.lvOriginalHeads.UseCompatibleStateImageBehavior = False
        Me.lvOriginalHeads.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhTranCode
        '
        Me.colhTranCode.Text = "Code"
        '
        'colhTranName
        '
        Me.colhTranName.Text = "Name"
        Me.colhTranName.Width = 130
        '
        'colhTranHeadType
        '
        Me.colhTranHeadType.Text = "Head Type"
        Me.colhTranHeadType.Width = 100
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(742, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(105, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(91, 15)
        Me.lblPeriod.TabIndex = 8
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(383, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 5
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(105, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(272, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 64)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(91, 15)
        Me.lblEmployee.TabIndex = 3
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 514)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(476, 63)
        Me.gbSortBy.TabIndex = 21
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbSortBy.Visible = False
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(425, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(81, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(95, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(324, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'frmEndOfServiceReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(885, 638)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmEndOfServiceReport"
        Me.Text = "frmEndOfServiceReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lvOriginalHeads As System.Windows.Forms.ListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboOT1Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT1Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT4Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT4Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT3Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT3Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT2Hours As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT2Hours As System.Windows.Forms.Label
    Friend WithEvents cboOT4Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT4Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT3Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT3Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT2Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT2Amount As System.Windows.Forms.Label
    Friend WithEvents cboOT1Amount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOT1Amount As System.Windows.Forms.Label
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents cboGrossPay As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrossPay As System.Windows.Forms.Label
    Friend WithEvents cboForFirst3Years As System.Windows.Forms.ComboBox
    Friend WithEvents lblForFirst3Years As System.Windows.Forms.Label
    Friend WithEvents cboBalanceDays As System.Windows.Forms.ComboBox
    Friend WithEvents lblBalanceDays As System.Windows.Forms.Label
    Friend WithEvents cboBalanceMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblBalanceMonth As System.Windows.Forms.Label
    Friend WithEvents cboBalanceYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblBalanceYear As System.Windows.Forms.Label
    Friend WithEvents cboLeaveBalanceHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveBalanceHead As System.Windows.Forms.Label
    Friend WithEvents objelLineOT As eZee.Common.eZeeLine
    Friend WithEvents objelLineGratuity As eZee.Common.eZeeLine
    Friend WithEvents objelLineLeave As eZee.Common.eZeeLine
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents cboBalanceDaysAmt As System.Windows.Forms.ComboBox
    Friend WithEvents lblBalanceDaysAmt As System.Windows.Forms.Label
    Friend WithEvents cboBalanceMonthAmt As System.Windows.Forms.ComboBox
    Friend WithEvents lblBalanceMonthAmt As System.Windows.Forms.Label
    Friend WithEvents cboBalanceYearAmt As System.Windows.Forms.ComboBox
    Friend WithEvents lblBalanceYearAmt As System.Windows.Forms.Label
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
End Class
