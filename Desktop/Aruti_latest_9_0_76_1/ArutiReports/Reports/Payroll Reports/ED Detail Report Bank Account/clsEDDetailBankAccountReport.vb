Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsEDDetailBankAccountReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEDDetailBankAccountReport"
    Private mstrReportId As String = enArutiReport.EDDetail_Bank_Account_Report
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintModeId As Integer = -1
    Private mstrModeName As String = ""
    Private mintHeadID As Integer = -1
    Private mstrHeadName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mintEmpBankId As Integer = -1
    Private mstrEmpBankName As String = String.Empty
    Private mintEmpBranchId As Integer = -1
    Private mstrEmpBranchName As String = String.Empty
    Private mstrOrderByQuery As String = ""

    Private mblnIsActive As Boolean = True
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrHeadCode As String = String.Empty


    Private mintToPeriodId As Integer = -1
    Private mstrToPeriodName As String = ""
    Private mstrPeriodID As String = ""

    Private mblnIncludeNegativeHeads As Boolean = True


    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAdvance_Filter As String = String.Empty

    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mintUserUnkid As Integer = User._Object._Userunkid
    Private mintCompanyUnkid As Integer = Company._Object._Companyunkid
    Private mstrTranDatabaseName As String = FinancialYear._Object._DatabaseName

    Private mintMembershipId As Integer = -1
    Private mstrMembershipName As String = ""

    Private marrDatabaseName As New ArrayList

    Private mstrUserAccessFilter As String = ""

    'Sohail (22 Jan 2019) -- Start
    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
    Private marrYearWisePeriodIDs As New ArrayList
    Private mstrEFTCustomXLSColumnsIds As String = String.Empty
    Private mblnEFTShowColumnHeader As Boolean = True
    Private mintEFTMembershipUnkId As Integer = 0
    'Sohail (22 Jan 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ModeId() As Integer
        Set(ByVal value As Integer)
            mintModeId = value
        End Set
    End Property

    Public WriteOnly Property _ModeName() As String
        Set(ByVal value As String)
            mstrModeName = value
        End Set
    End Property

    Public WriteOnly Property _HeadId() As Integer
        Set(ByVal value As Integer)
            mintHeadID = value
        End Set
    End Property

    Public WriteOnly Property _HeadName() As String
        Set(ByVal value As String)
            mstrHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmpBankId() As Integer
        Set(ByVal value As Integer)
            mintEmpBankId = value
        End Set
    End Property

    Public WriteOnly Property _EmpBankName() As String
        Set(ByVal value As String)
            mstrEmpBankName = value
        End Set
    End Property

    Public WriteOnly Property _EmpBranchId() As Integer
        Set(ByVal value As Integer)
            mintEmpBranchId = value
        End Set
    End Property

    Public WriteOnly Property _EmpBranchName() As String
        Set(ByVal value As String)
            mstrEmpBranchName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _HeadCode() As String
        Set(ByVal value As String)
            mstrHeadCode = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _mstrPeriodID() As String
        Set(ByVal value As String)
            mstrPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _IncludeNegativeHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeNegativeHeads = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TranDatabaseName() As String
        Set(ByVal value As String)
            mstrTranDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MemberShipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Sohail (22 Jan 2019) -- Start
    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
    Public WriteOnly Property _Arr_YearWisePeriodIDs() As ArrayList
        Set(ByVal value As ArrayList)
            marrYearWisePeriodIDs = value
        End Set
    End Property

    Public WriteOnly Property _EFTCustomXLSColumnsIds() As String
        Set(ByVal value As String)
            mstrEFTCustomXLSColumnsIds = value
        End Set
    End Property

    Public WriteOnly Property _EFTShowColumnHeader() As Boolean
        Set(ByVal value As Boolean)
            mblnEFTShowColumnHeader = value
        End Set
    End Property

    Public WriteOnly Property _EFTMembershipId() As Integer
        Set(ByVal value As Integer)
            mintEFTMembershipUnkId = value
        End Set
    End Property
    'Sohail (22 Jan 2019) -- End

    'Sohail (11 Feb 2021) -- Start
    'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
    Private mblnShowEmpNameInSingleColumn As Boolean = False
    Public WriteOnly Property _ShowEmpNameInSingleColumn() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpNameInSingleColumn = value
        End Set
    End Property

    Private mstrEmpNameFormat As String = "#firstname# #middlename# #surname#"
    Public WriteOnly Property _EmpNameFormat() As String
        Set(ByVal value As String)
            mstrEmpNameFormat = value
        End Set
    End Property
    'Sohail (11 Feb 2021) -- End

    'Sohail (12 May 2021) -- Start
    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
    Private mblnExportToEFTCityDirectTXT As Boolean = False
    Public WriteOnly Property _ExportToEFTCityDirectTXT() As Boolean
        Set(ByVal value As Boolean)
            mblnExportToEFTCityDirectTXT = value
        End Set
    End Property

    Private mblnIsExportEFTCitiDirectSuccess As Boolean = False
    Public ReadOnly Property _IsExportEFTCitiDirectSuccess() As Boolean
        Get
            Return mblnIsExportEFTCitiDirectSuccess
        End Get
    End Property
    'Sohail (12 May 2021) -- End

    'Sohail (25 May 2021) -- Start
    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
    Private mstrCompanyAccountNo As String = ""
    Public WriteOnly Property _CompanyAccountNo() As String
        Set(ByVal value As String)
            mstrCompanyAccountNo = value
        End Set
    End Property
    'Sohail (25 May 2021) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintModeId = -1
            mstrModeName = ""
            mintHeadID = -1
            mstrHeadName = ""
            mintPeriodId = -1
            mstrPeriodName = ""
            mintEmpBankId = -1
            mstrEmpBankName = ""
            mintEmpBranchId = -1
            mstrEmpBranchName = ""
            mstrOrderByQuery = ""
            mintToPeriodId = -1
            mstrToPeriodName = ""
            mintMembershipId = -1
            mstrMembershipName = ""
            'Sohail (12 May 2021) -- Start
            'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            mblnExportToEFTCityDirectTXT = False
            mblnIsExportEFTCitiDirectSuccess = False
            'Sohail (12 May 2021) -- End
            'Sohail (25 May 2021) -- Start
            'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            mstrCompanyAccountNo = ""
            'Sohail (25 May 2021) -- End

            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""
        Try

            Dim mstrheadID As String = ""
            Dim mstrModeID As String = ""

            If mintMembershipId > 0 Then
                Dim objMemberShip As New clsmembership_master
                objMemberShip._Membershipunkid = mintMembershipId
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "Membership : ") & " " & mstrMembershipName & " "
                mstrheadID = "," & objMemberShip._ETransHead_Id.ToString() & "," & objMemberShip._CTransHead_Id.ToString()
                mstrModeID = "," & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintModeId > 0 Then
                If mintModeId = 99999 Then
                    Me._FilterQuery &= " AND prpayrollprocess_tran.loanadvancetranunkid > 0 "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                    'Me._FilterQuery &= " AND prpayrollprocess_tran.crprocesstranunkid > 0 "
                    Me._FilterQuery &= " AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) "
                    'Sohail (07 JUn 2021) -- End
                    'Sohail (23 Jun 2015) -- End
                Else
                    Me._FilterQuery &= " AND prtranhead_master.trnheadtype_id IN (" & mintModeId & mstrModeID & ")"
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Mode : ") & " " & mstrModeName & " "
            End If

            If mintHeadID > 0 Then
                'Sohail (23 Jun 2015) -- Start
                'Enhancement - Claim Request Expense on E&D Detail Reports.
                'If mintModeId <> 99999 Then
                '    Me._FilterQuery &= " AND prtranhead_master.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")"
                '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Head : ") & " " & mstrHeadName & " "
                'End If
                If mintModeId = 99999 Then
                    'Do Nothing
                ElseIf mintModeId = 99998 Then
                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                    'Me._FilterQuery &= " AND cmexpense_master.expenseunkid IN  ( " & mintHeadID & mstrheadID & ")"
                    Me._FilterQuery &= " AND (cmexpense_master.expenseunkid IN  ( " & mintHeadID & mstrheadID & ") OR crretireexpense.expenseunkid IN  ( " & mintHeadID & mstrheadID & ")) "
                    'Sohail (07 JUn 2021) -- End
                Else
                    Me._FilterQuery &= " AND prtranhead_master.tranheadunkid IN  ( " & mintHeadID & mstrheadID & ")"

                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Head : ") & " " & mstrHeadName & " "
                'Sohail (23 Jun 2015) -- End
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "From Period : ") & " " & mstrPeriodName & " "
            End If

            If mintToPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "To Period : ") & " " & mstrToPeriodName & " "
            End If

            Me._FilterQuery &= " AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodID & " ) "

            If mintEmpBankId > 0 Then
                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'Me._FilterQuery &= " AND hrmsConfiguration..cfbankbranch_master.bankgroupunkid = @empbankgroupunkid "
                'objDataOperation.AddParameter("@empbankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBankId)
                'Sohail (22 Jan 2019) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Employee Bank : ") & " " & mstrEmpBankName & " "
            End If

            If mintEmpBranchId > 0 Then
                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'Me._FilterQuery &= " AND premployee_bank_tran.branchunkid = @empbranchunkid "
                'objDataOperation.AddParameter("@empbranchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBranchId)
                'Sohail (22 Jan 2019) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Employee Branch : ") & " " & mstrEmpBranchName & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""

                If mintViewIndex > 0 Then
                    If Me.OrderByQuery.Trim = "" Then
                        mstrOrderByQuery &= "ORDER BY payperiodunkid,GNAME"
                    Else
                        mstrOrderByQuery &= "ORDER BY payperiodunkid,GNAME, " & Me.OrderByQuery
                    End If

                Else
                    mstrOrderByQuery &= "ORDER BY payperiodunkid," & Me.OrderByQuery
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid
        '    User._Object._Userunkid = mintUserUnkid

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then


        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubTotal As New Dictionary(Of Integer, Object)
        '        Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell
        '        Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '        Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intGroupColumn As Integer = 0

        '            If mintViewIndex > 0 Then
        '                If mintMembershipId > 0 Then
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 5
        '                Else
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 4
        '                End If
        '                Dim strGrpCols As String() = {"column1", "column10"}
        '                strarrGroupColumns = strGrpCols
        '                Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
        '                arrGroupCount = arr

        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 18, "Sub Total :"))
        '                objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
        '                arrDicGroupExtraInfo = arrDic
        '            Else
        '                If mintMembershipId > 0 Then
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 4
        '                Else
        '                    intGroupColumn = mdtTableExcel.Columns.Count - 3
        '                End If
        '                Dim strGrpCols As String() = {"column10"}
        '                strarrGroupColumns = strGrpCols
        '                objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

        '                Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
        '                arrDicGroupExtraInfo = arrDic
        '            End If

        '            objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If


        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

        '    End If


        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport(xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, xOnlyApproved, xExportReportPath, xOpenReportAfterExport)
            'Sohail (12 May 2021) - [xExportReportPath, xOpenReportAfterExport]
            Rpt = objRpt

            If Not IsNothing(objRpt) Then


                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDicGrandTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubTotal As New Dictionary(Of Integer, Object)
                Dim objDicSubGroupTotal As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 21, "Total Count :")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell
                Dim arrGroupCount() As String = {Language.getMessage(mstrModuleName, 27, "Period Count:")}
                Dim arrDicGroupExtraInfo() As Dictionary(Of Integer, Object) = Nothing


                If mdtTableExcel IsNot Nothing Then

                    Dim intGroupColumn As Integer = 0

                    If mintViewIndex > 0 Then
                        If mintMembershipId > 0 Then
                            intGroupColumn = mdtTableExcel.Columns.Count - 5
                        Else
                            intGroupColumn = mdtTableExcel.Columns.Count - 4
                        End If
                        Dim strGrpCols As String() = {"column1", "column10"}
                        strarrGroupColumns = strGrpCols
                        Dim arr() As String = {Language.getMessage(mstrModuleName, 20, "Sub Count :"), Language.getMessage(mstrModuleName, 27, "Period Count:")}
                        arrGroupCount = arr

                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 18, "Sub Total :"))
                        objDicSubGroupTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal, objDicSubGroupTotal}
                        arrDicGroupExtraInfo = arrDic
                    Else
                        If mintMembershipId > 0 Then
                            intGroupColumn = mdtTableExcel.Columns.Count - 4
                        Else
                            intGroupColumn = mdtTableExcel.Columns.Count - 3
                        End If
                        Dim strGrpCols As String() = {"column10"}
                        strarrGroupColumns = strGrpCols
                        objDicSubTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 26, "Period Total:"))

                        Dim arrDic() As Dictionary(Of Integer, Object) = {objDicSubTotal}
                        arrDicGroupExtraInfo = arrDic
                    End If

                    objDicGrandTotal.Add(intGroupColumn, Language.getMessage(mstrModuleName, 19, "Grand Total :"))


                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If


                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", arrGroupCount, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDicGrandTotal, arrDicGroupExtraInfo, True)

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 9, "Emp No")))
            iColumn_DetailReport.Add(New IColumn("surname", Language.getMessage(mstrModuleName, 10, "Surname")))
            iColumn_DetailReport.Add(New IColumn("firstname", Language.getMessage(mstrModuleName, 11, "First Name")))
            iColumn_DetailReport.Add(New IColumn("branchname", Language.getMessage(mstrModuleName, 14, "Bank Branch")))
            iColumn_DetailReport.Add(New IColumn("accountno", Language.getMessage(mstrModuleName, 30, "Account No.")))
            iColumn_DetailReport.Add(New IColumn("amount", Language.getMessage(mstrModuleName, 13, "Amount")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrInnerQ As String = ""
    '    Dim strDatabaseName As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0

    '        objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        Dim objPeriod As New clscommom_period_Tran

    '        'Sohail (21 Aug 2015) -- Start
    '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '        'objPeriod._Periodunkid = mintToPeriodId
    '        objPeriod._Periodunkid(strDatabaseName) = mintToPeriodId
    '        'Sohail (21 Aug 2015) -- End
    '        Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)

    '        objPeriod = Nothing

    '        If mstrUserAccessFilter.Trim = "" Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        '*** Uncomment for Comma Separated ****
    '        'StrQ = "WITH    cte ( employeeunkid, employeecode, surname, firstname, initial, amount, branchunkid, branchname, accountno, Id, GName, payperiodunkid, period_name, ROWNO, tranheadunkid ) AS (  " & _
    '        StrQ = "SELECT  A.employeeunkid  " & _
    '                    ", A.employeecode " & _
    '                    ", A.surname " & _
    '                    ", A.firstname " & _
    '                    ", A.initial " & _
    '                    ", SUM(A.amount) AS amount " & _
    '                    ", A.branchunkid " & _
    '                    ", A.branchname " & _
    '                    ", A.accountno " & _
    '                    ", A.Id " & _
    '                    ", A.GName " & _
    '                    ", A.payperiodunkid " & _
    '                    ", A.period_name " & _
    '                    ", A.ROWNO " & _
    '                    ", A.tranheadunkid  "

    '        If mintMembershipId > 0 Then
    '            StrQ &= " , A.membershipunkid " & _
    '                         " , A.membershipno "
    '        End If
    '        StrQ &= "              FROM ( "

    '        For i = 0 To marrDatabaseName.Count - 1
    '            strDatabaseName = marrDatabaseName(i)

    '            If i > 0 Then
    '                StrInnerQ &= " UNION ALL "
    '            End If

    '            StrInnerQ &= "               SELECT    ISNULL(hremployee_master.employeeunkid, 0) employeeunkid " & _
    '                                                ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
    '                                                ", ISNULL(hremployee_master.surname, '') surname " & _
    '                                                ", ISNULL(hremployee_master.firstname, '') firstname " & _
    '                                                ", ISNULL(hremployee_master.othername, '') initial " & _
    '                                                ", CAST(CASE modeid WHEN 2 THEN (prpayrollprocess_tran.amount * percentage) / 100 ELSE prpayrollprocess_tran.amount END AS DECIMAL(36, " & decDecimalPlaces & ")) AS amount " & _
    '                                                ", cfcommon_period_tran.period_name " & _
    '                                                ", prtnaleave_tran.payperiodunkid " & _
    '                                                ", ISNULL(premployee_bank_tran.branchunkid, 0) AS branchunkid " & _
    '                                                ", ISNULL(cfbankbranch_master.branchname, '') AS branchname " & _
    '                                                ", ISNULL(accountno, '') AS accountno " & _
    '                                                ", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY edperiod.end_date DESC ) AS ROWNO "

    '            If mintModeId = 99999 Then
    '                StrInnerQ &= "                  , 0 AS tranheadunkid "
    '                'Sohail (23 Jun 2015) -- Start
    '                'Enhancement - Claim Request Expense on E&D Detail Reports.
    '            ElseIf mintModeId = 99998 Then
    '                StrInnerQ &= "                  , ISNULL(cmexpense_master.expenseunkid, -1) AS tranheadunkid "
    '                'Sohail (23 Jun 2015) -- End
    '            Else
    '                StrInnerQ &= "                  , ISNULL(prtranhead_master.tranheadunkid,0) tranheadunkid "
    '                'Sohail (23 Jun 2015) - [", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY edperiod.end_date DESC ) AS ROWNO "]
    '            End If

    '            If mintMembershipId > 0 Then
    '                StrInnerQ &= "                  , ISNULL(hremployee_meminfo_tran.membershipunkid,0) membershipunkid " & _
    '                         "                      , ISNULL(hremployee_meminfo_tran.membershipno,'') membershipno "

    '            End If


    '            If mintViewIndex > 0 Then
    '                StrInnerQ &= mstrAnalysis_Fields
    '            Else
    '                StrInnerQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrInnerQ &= "               FROM     " & strDatabaseName & "..prpayrollprocess_tran " & _
    '                                                  "JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
    '                                                  "LEFT JOIN " & mstrTranDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                  "LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
    '                                                  "LEFT JOIN " & mstrTranDatabaseName & "..premployee_bank_tran ON prpayrollprocess_tran.employeeunkid = premployee_bank_tran.employeeunkid " & _
    '                                                  "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
    '                                                  "LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = premployee_bank_tran.periodunkid "

    '            If mintModeId = 99999 Then
    '                StrInnerQ &= "                     LEFT JOIN  " & strDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
    '                                                                "AND prpayrollprocess_tran.loanadvancetranunkid > 0 "
    '                'Sohail (23 Jun 2015) -- Start
    '                'Enhancement - Claim Request Expense on E&D Detail Reports.
    '            ElseIf mintModeId = 99998 Then
    '                StrInnerQ &= "                      LEFT JOIN " & mstrTranDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
    '                                                   "LEFT JOIN " & mstrTranDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
    '                                                   "LEFT JOIN " & mstrTranDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
    '                                                                "AND prpayrollprocess_tran.crprocesstranunkid > 0 "
    '                'Sohail (23 Jun 2015) -- End
    '            Else
    '                StrInnerQ &= "                     LEFT JOIN  " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
    '            End If

    '            If mintMembershipId > 0 Then

    '                StrInnerQ &= "                     LEFT JOIN " & mstrTranDatabaseName & "..hremployee_Meminfo_tran ON prpayrollprocess_tran.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
    '                                                                  "AND hremployee_Meminfo_tran.membershipunkid = " & mintMembershipId & _
    '                                                                  "AND hremployee_Meminfo_tran.isactive = 1 "
    '            End If

    '            'Sohail (23 Jun 2015) -- Start
    '            'Enhancement - Claim Request Expense on E&D Detail Reports.
    '            'StrInnerQ &= "                         LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
    '            '                                      "LEFT JOIN " & mstrTranDatabaseName & "..premployee_bank_tran ON prpayrollprocess_tran.employeeunkid = premployee_bank_tran.employeeunkid " & _
    '            '                                      "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
    '            '                                      "LEFT JOIN " & mstrTranDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = premployee_bank_tran.periodunkid "
    '            'Sohail (23 Jun 2015) -- End
    '            'Sohail (12 Jan 2015) -- Start
    '            'Issue : Amount is coming multiple times to real amount due to filter in join statement.
    '            ' "AND edperiod.modulerefid = " & enModuleReference.Payroll & _
    '            '"AND edperiod.end_date <=  cfcommon_period_tran.end_date " & _
    '            'Sohail (12 Jan 2015) -- End

    '            StrInnerQ &= mstrAnalysis_Join

    '            StrInnerQ &= "               WHERE  prtnaleave_tran.isvoid = 0 " & _
    '                                                 "AND prpayrollprocess_tran.isvoid = 0 " & _
    '                                                 "AND ISNULL(premployee_bank_tran.isvoid, 0) = 0 "
    '            'Sohail (23 Jun 2015) -- Removed [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
    '            'Sohail (12 Jan 2015) -- [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]

    '            If mintModeId = 99999 Then
    '                StrInnerQ &= "                    AND lnloan_advance_tran.isvoid = 0  " & _
    '                                                 "AND prpayrollprocess_tran.loanadvancetranunkid > 0  " & _
    '                                                 "AND isloan = 0 "
    '                'Sohail (23 Jun 2015) -- Start
    '                'Enhancement - Claim Request Expense on E&D Detail Reports.
    '            ElseIf mintModeId = 99998 Then
    '                StrInnerQ &= "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
    '                             "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
    '                             "AND ISNULL(cmexpense_master.isactive, 1) = 1 "
    '                'Sohail (23 Jun 2015) -- End
    '            Else
    '                StrInnerQ &= "                    AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                             "                    AND edperiod.modulerefid = " & enModuleReference.Payroll & _
    '                             "                    AND edperiod.end_date <=  cfcommon_period_tran.end_date "
    '                'Sohail (23 Jun 2015) - [AND edperiod.modulerefid = " & enModuleReference.Payroll & _ AND edperiod.end_date <=  cfcommon_period_tran.end_date]
    '            End If

    '            If mblnIncludeNegativeHeads = True Then
    '                StrInnerQ &= "                    AND ISNULL(prpayrollprocess_tran.amount, 0) <> 0 "
    '            Else
    '                StrInnerQ &= "                    AND ISNULL(prpayrollprocess_tran.amount, 0)  > 0 "
    '            End If

    '            'Issue : Diffrence with head amount on payslip.
    '            'StrInnerQ &= "                       AND   CONVERT(CHAR(8),ISNULL(cfcommon_period_tran.end_date, '" & strEndDate & "'),112) <= '" & strEndDate & "' "
    '            'Sohail (23 Jun 2015) -- Start
    '            'Enhancement - Claim Request Expense on E&D Detail Reports.
    '            'StrInnerQ &= "                        AND   CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' "
    '            If mintModeId = 99999 Then
    '                'Do Nothing
    '            ElseIf mintModeId = 99998 Then
    '                'Do Nothing
    '            Else
    '                StrInnerQ &= "                        AND   CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' "
    '            End If
    '            'Sohail (23 Jun 2015) -- End


    '            If mstrAdvance_Filter.Trim.Length > 0 Then
    '                StrInnerQ &= " AND " & mstrAdvance_Filter
    '            End If

    '            If mblnIsActive = False Then
    '                StrInnerQ &= "                    AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                                 "AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                                 "AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                                 "AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If

    '            If mstrUserAccessFilter.Length > 0 Then
    '                StrInnerQ &= mstrUserAccessFilter
    '            End If

    '            If i = 0 Then
    '                Call FilterTitleAndFilterQuery()
    '            End If

    '            StrInnerQ &= Me._FilterQuery

    '        Next

    '        StrQ &= StrInnerQ

    '        StrQ &= "  ) AS A WHERE  ROWNO = 1 " & _
    '                "GROUP BY A.employeeunkid " & _
    '                      ", A.employeecode " & _
    '                      ", A.surname " & _
    '                      ", A.firstname " & _
    '                      ", A.initial " & _
    '                      ", A.branchunkid " & _
    '                      ", A.branchname " & _
    '                      ", A.accountno " & _
    '                      ", A.Id " & _
    '                      ", A.GName " & _
    '                      ", A.payperiodunkid " & _
    '                      ", A.period_name " & _
    '                      ", A.ROWNO " & _
    '                      ", A.tranheadunkid "

    '        If mintMembershipId > 0 Then
    '            StrQ &= ", A.membershipunkid " & _
    '                    ", A.membershipno "
    '        End If

    '        '*** Uncomment for Comma Separated ****
    '        'StrQ &= "   ) " & _
    '        '    "SELECT  employeeunkid  " & _
    '        '          ", employeecode " & _
    '        '          ", surname " & _
    '        '          ", firstname " & _
    '        '          ", initial " & _
    '        '          ", amount " & _
    '        '          ", branchname = ISNULL(STUFF((SELECT   ', ' + branchname " & _
    '        '                                       "FROM     CTE AS b " & _
    '        '                                       "WHERE    a.employeeunkid = b.employeeunkid " & _
    '        '                                "FOR   XML PATH('') , ROOT('MyString') , TYPE) .value('/MyString[1]' , 'varchar(max)'), 1, 2 , ''), '') " & _
    '        '          ", accountno = ISNULL(STUFF((SELECT    ', ' + accountno " & _
    '        '                                      "FROM      cte AS b " & _
    '        '                                      "WHERE     a.employeeunkid = b.employeeunkid " & _
    '        '                               "FOR   XML PATH('')  , ROOT('MyString') , TYPE) .value('/MyString[1]' , 'varchar(max)'), 1, 2 , ''), '') " & _
    '        '          ", Id " & _
    '        '          ", GName " & _
    '        '          ", payperiodunkid " & _
    '        '          ", period_name " & _
    '        '          ", ROWNO " & _
    '        '          ", tranheadunkid " & _
    '        '    "FROM    CTE AS a " & _
    '        '    "GROUP BY employeeunkid  " & _
    '        '          ", employeecode " & _
    '        '          ", surname " & _
    '        '          ", firstname " & _
    '        '          ", initial " & _
    '        '          ", amount " & _
    '        '          ", Id " & _
    '        '          ", GName " & _
    '        '          ", payperiodunkid " & _
    '        '          ", period_name " & _
    '        '          ", ROWNO " & _
    '        '          ", tranheadunkid "

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        Dim dtList As DataTable = New DataView(dsList.Tables(0), "", "Gname", DataViewRowState.CurrentRows).ToTable()

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim objED As New clsEarningDeduction
    '        Dim strPrevGroup As String = ""
    '        Dim decSubTotal As Decimal = 0
    '        Dim strPrevPeriod As String = ""
    '        Dim decPeriodTotal As Decimal = 0

    '        For Each dtRow As DataRow In dtList.Rows
    '            Dim rpt_Row As DataRow
    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = dtRow.Item("GName")
    '            rpt_Row.Item("Column2") = dtRow.Item("employeecode")
    '            rpt_Row.Item("Column3") = dtRow.Item("surname")
    '            rpt_Row.Item("Column4") = dtRow.Item("firstname")
    '            rpt_Row.Item("Column5") = dtRow.Item("initial")
    '            rpt_Row.Item("Column7") = dtRow.Item("branchname")
    '            rpt_Row.Item("Column9") = dtRow.Item("accountno")
    '            rpt_Row.Item("Column10") = dtRow.Item("period_name")


    '            If CInt(dtRow.Item("tranheadunkid")) <> mintHeadID Then Continue For

    '            rpt_Row.Item("Column6") = Format(CDec(dtRow("amount")), mstrfmtCurrency)
    '            rpt_Row.Item("Column81") = CDec(dtRow("amount"))

    '            If mintViewIndex > 0 Then
    '                If strPrevGroup <> dtRow.Item("GName").ToString Then
    '                    decSubTotal = CDec(Format(dtRow("amount"), mstrfmtCurrency))
    '                Else
    '                    decSubTotal += CDec(Format(dtRow("amount"), mstrfmtCurrency))
    '                End If
    '                strPrevGroup = dtRow.Item("GName").ToString
    '            End If

    '            rpt_Row.Item("Column8") = Format(decSubTotal, mstrfmtCurrency)


    '            If strPrevPeriod <> dtRow.Item("period_name").ToString Then
    '                decPeriodTotal = 0
    '                decPeriodTotal = CDec(Format(dtRow("amount"), mstrfmtCurrency))
    '            Else
    '                decPeriodTotal += CDec(Format(dtRow("amount"), mstrfmtCurrency))
    '            End If

    '            strPrevPeriod = dtRow.Item("period_name").ToString
    '            rpt_Row.Item("Column11") = Format(decPeriodTotal, mstrfmtCurrency)

    '            If mintMembershipId > 0 Then
    '                Dim drMemberShip() As DataRow = dtList.Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND membershipunkid = " & mintMembershipId)
    '                If drMemberShip.Length > 0 Then
    '                    rpt_Row.Item("Column12") = drMemberShip(0)("membershipno").ToString()
    '                Else
    '                    rpt_Row.Item("Column12") = ""
    '                End If
    '            Else
    '                rpt_Row.Item("Column12") = ""
    '            End If

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '        Next


    '        objRpt = New ArutiReport.Designer.rptEDDetailBankAccount


    '        If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
    '            ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), mstrfmtCurrency))
    '        Else
    '            ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(0, mstrfmtCurrency))
    '        End If

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex <= 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 31, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If mintMembershipId <= 0 Then
    '            Call ReportFunction.EnableSuppress(objRpt, "txtMembershipNo", True)
    '            Call ReportFunction.EnableSuppress(objRpt, "Column121", True)

    '            objRpt.ReportDefinition.ReportObjects("txtBranch").Width = 2620
    '            objRpt.ReportDefinition.ReportObjects("Column71").Width = 2620

    '            objRpt.ReportDefinition.ReportObjects("txtAccountNo").Left = 8210
    '            objRpt.ReportDefinition.ReportObjects("Column92").Left = 8210

    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtMembershipNo", Language.getMessage(mstrModuleName, 28, "Membership No."))
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
    '        Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Surname"))
    '        Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 13, "Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 14, "Bank Branch"))
    '        Call ReportFunction.TextChange(objRpt, "txtAccountNo", Language.getMessage(mstrModuleName, 30, "Account No."))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 18, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 20, "Sub Count :"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 21, "Total Count :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))



    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & "  [ " & "(" & mstrHeadCode & "), " & mstrHeadName & " ] ")
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)

    '            Dim mintColumn As Integer = 0

    '            mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
    '            mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
    '            mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
    '            mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
    '            mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Bank Branch")
    '            mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 30, "Account No.")
    '            mdtTableExcel.Columns("Column9").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            If mintMembershipId > 0 Then
    '                mdtTableExcel.Columns("Column12").Caption = Language.getMessage(mstrModuleName, 28, "Membership No.")
    '                mdtTableExcel.Columns("Column12").SetOrdinal(mintColumn)
    '                mintColumn += 1
    '            End If

    '            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
    '            mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
    '            mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            If mintViewIndex > 0 Then
    '                mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
    '                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
    '                mintColumn += 1
    '            End If


    '            For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                mdtTableExcel.Columns.RemoveAt(mintColumn)
    '            Next


    '        End If

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try

    'End Function

    Public Function Generate_DetailReport(ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean, _
                                          ByVal strExportReportPath As String, _
                                          ByVal blnOpenReportAfterExport As Boolean _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (12 May 2021) - [strExportReportPath, blnOpenReportAfterExport]
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrInnerQ As String = ""
        Dim strDatabaseName As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(strDatabaseName) = mintToPeriodId
            Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodStartDate, objPeriod._End_Date, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, objPeriod._End_Date, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, objPeriod._End_Date, strDatabaseName)

            objPeriod = Nothing

            'Sohail (22 Jan 2019) -- Start
            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
            StrQ = "SELECT hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename  " & _
                    "INTO #cteEmp FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            StrQ &= ";SELECT * " & _
                    "INTO #EmpBank " & _
                    "FROM " & _
                    "( "

            For i = 0 To marrDatabaseName.Count - 1
                strDatabaseName = marrDatabaseName(i)

                If i > 0 Then
                    StrQ &= " UNION ALL "
                End If

                StrQ &= "SELECT premployee_bank_tran.* " & _
                             ", DENSE_RANK() OVER (PARTITION BY premployee_bank_tran.employeeunkid, CurrPeriod.periodunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                             ", CurrPeriod.periodunkid AS payperiodunkid " & _
                        "FROM " & strDatabaseName & "..premployee_bank_tran " & _
                            "JOIN " & strDatabaseName & "..#cteEmp ON #cteEmp.employeeunkid = premployee_bank_tran.employeeunkid " & _
                            "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_bank_tran.periodunkid " & _
                            "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran AS CurrPeriod ON 1 = 1 " & _
                        "WHERE premployee_bank_tran.isvoid = 0 " & _
                              "AND cfcommon_period_tran.isactive = 1 " & _
                              "AND cfcommon_period_tran.modulerefid = " & CInt(enModuleReference.Payroll) & " " & _
                              "AND CurrPeriod.periodunkid IN (" & marrYearWisePeriodIDs(i).ToString & ") " & _
                              "AND cfcommon_period_tran.end_date <= CurrPeriod.end_date "
            Next

            StrQ &= ") AS A " & _
                    "WHERE A.ROWNO = 1; "

            For i = 0 To marrDatabaseName.Count - 1
                strDatabaseName = marrDatabaseName(i)

                If i > 0 Then
                    StrQ &= " UNION ALL "
                End If

                StrQ &= "SELECT premployee_bank_tran.* " & _
                                 ", 1 AS ROWNO " & _
                                 ", prpayment_tran.periodunkid AS payperiodunkid " & _
                                 ", prempsalary_tran.expaidamt " & _
                                 ", prempsalary_tran.paymenttranunkid AS empsalarypaymenttranunkid " & _
                                 ", ISNULL(cfbankbranch_master.branchcode, '') AS branchcode " & _
                                 ", ISNULL(cfbankbranch_master.branchname, '') AS branchname " & _
                                 ", ISNULL(cfbankbranch_master.isclearing, 0) AS isclearing " & _
                                 ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupcode, '') AS BankGroupCode " & _
                                 ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname, '') AS BankGroupName " & _
                                 ", ISNULL(cfbankbranch_master.sortcode,'') AS SortCode " & _
                                 ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.swiftcode, '') AS SwiftCode " & _
                                 ", ISNULL(CBank.groupcode, '') AS CMP_BankGroupCode " & _
                                 ", ISNULL(CBank.groupname, '') AS CMP_BankGroupName " & _
                                 ", ISNULL(CBranch.branchcode, '') AS CMP_branchcode " & _
                                 ", ISNULL(CBranch.branchname, '') AS CMP_branchname " & _
                                 ", ISNULL(prpayment_tran.accountno, '') AS CMP_Account " & _
                                 ", ISNULL(CBranch.sortcode,'') AS CMP_SortCode " & _
                                 ", ISNULL(CBank.swiftcode, '') AS CMP_SwiftCode " & _
                                 ", ISNULL(cfexchange_rate.currency_sign, '" & objExchangeRate._Currency_Sign & "') AS currency_sign " & _
                            "FROM  #cteEmp " & _
                                "JOIN " & strDatabaseName & "..prpayment_tran ON #cteEmp.employeeunkid = prpayment_tran.employeeunkid " & _
                                "LEFT JOIN " & strDatabaseName & "..prempsalary_tran ON prpayment_tran.paymenttranunkid = prempsalary_tran.paymenttranunkid " & _
                                       "AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                       "AND prempsalary_tran.isvoid = 0 " & _
                                "LEFT JOIN " & strDatabaseName & "..premployee_bank_tran ON premployee_bank_tran.empbanktranunkid = prempsalary_tran.empbanktranid " & _
                                       "AND premployee_bank_tran.isvoid = 0 " & _
                                "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
                                "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON cfbankbranch_master.bankgroupunkid = cfpayrollgroup_master.groupmasterunkid " & _
                                "LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CBranch ON prpayment_tran.branchunkid = CBranch.branchunkid " & _
                                "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master AS CBank ON CBranch.bankgroupunkid = CBank.groupmasterunkid " & _
                                "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid " & _
                            "WHERE prpayment_tran.isvoid = 0 " & _
                                  "AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                  "AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " " & _
                                  "AND prpayment_tran.paymentmode IN ( " & CInt(enPaymentMode.CHEQUE) & ", " & CInt(enPaymentMode.TRANSFER) & " ) " & _
                                  "AND prpayment_tran.periodunkid IN (" & marrYearWisePeriodIDs(i).ToString & ") "
                'Sohail (12 May 2021) - [isclearing]

                'Sohail (25 May 2021) -- Start
                'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                If mblnExportToEFTCityDirectTXT = True Then
                    StrQ &= " AND 1 = 2 "
                End If
                'Sohail (25 May 2021) -- End

                If mintEmpBankId > 0 Then
                    StrQ &= " AND hrmsConfiguration..cfbankbranch_master.bankgroupunkid = @empbankgroupunkid "
                End If

                If mintEmpBranchId > 0 Then
                    StrQ &= " AND premployee_bank_tran.branchunkid = @empbranchunkid "
                End If

                StrQ &= "UNION ALL " & _
                          "SELECT #EmpBank.* " & _
                               ", CASE #EmpBank.modeid WHEN " & CInt(enPaymentBy.Percentage) & " THEN (prpayment_tran.amount * #EmpBank.percentage / 100) " & _
                                     "ELSE prpayment_tran.amount END AS expaidamt " & _
                               ", 0 AS empsalarypaymenttranunkid " & _
                               ", ISNULL(cfbankbranch_master.branchcode, '') AS branchcode " & _
                               ", ISNULL(cfbankbranch_master.branchname, '') AS branchname " & _
                               ", ISNULL(cfbankbranch_master.isclearing, 0) AS isclearing " & _
                               ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupcode, '') AS BankGroupCode " & _
                               ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname, '') AS BankGroupName " & _
                               ", ISNULL(cfbankbranch_master.sortcode,'') AS SortCode " & _
                               ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.swiftcode, '') AS SwiftCode " & _
                               ", '' AS CMP_BankGroupCode " & _
                               ", '' AS CMP_BankGroupName " & _
                               ", '' AS CMP_branchcode " & _
                               ", '' AS CMP_branchname " & _
                               ", '' AS CMP_Account " & _
                               ", '' AS CMP_SortCode " & _
                               ", '' AS CMP_SwiftCode " & _
                               ", '" & objExchangeRate._Currency_Sign & "' AS currency_sign " & _
                          "FROM #cteEmp " & _
                              "LEFT JOIN " & strDatabaseName & "..prpayment_tran ON #cteEmp.employeeunkid = prpayment_tran.employeeunkid " & _
                                     "AND prpayment_tran.isvoid = 0 " & _
                                     "AND prpayment_tran.referenceid = " & CInt(clsPayment_tran.enPaymentRefId.PAYSLIP) & " " & _
                                     "AND prpayment_tran.paytypeid = " & CInt(clsPayment_tran.enPayTypeId.PAYMENT) & " " & _
                                     "AND prpayment_tran.paymentmode IN ( " & CInt(enPaymentMode.CHEQUE) & ", " & CInt(enPaymentMode.TRANSFER) & " ) " & _
                                     "AND prpayment_tran.periodunkid IN (" & marrYearWisePeriodIDs(i).ToString & ") " & _
                              "LEFT JOIN #EmpBank ON #EmpBank.employeeunkid = #cteEmp.employeeunkid " & _
                                    "AND #EmpBank.payperiodunkid IN (" & marrYearWisePeriodIDs(i).ToString & ") " & _
                              "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = #EmpBank.branchunkid " & _
                               "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON cfbankbranch_master.bankgroupunkid = cfpayrollgroup_master.groupmasterunkid " & _
                          "WHERE 1 = 1 " & _
                                "AND ( #EmpBank.percentage > 0 OR #EmpBank.amount > 0 ) " & _
                                "/*AND prpayment_tran.paymenttranunkid IS NULL*/ "

                'Sohail (25 May 2021) -- Start
                'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                If mblnExportToEFTCityDirectTXT = False Then
                    StrQ &= " AND prpayment_tran.paymenttranunkid IS NULL  "
                End If
                'Sohail (25 May 2021) -- End

                If mintEmpBankId > 0 Then
                    StrQ &= " AND hrmsConfiguration..cfbankbranch_master.bankgroupunkid = @empbankgroupunkid "
                End If

                If mintEmpBranchId > 0 Then
                    StrQ &= " AND #EmpBank.branchunkid = @empbranchunkid "
                End If

            Next

            StrQ &= " DROP TABLE #cteEmp " & _
                       " DROP TABLE #EmpBank "

            If mintEmpBankId > 0 Then
                objDataOperation.AddParameter("@empbankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBankId)
            End If

            If mintEmpBranchId > 0 Then
                objDataOperation.AddParameter("@empbranchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBranchId)
            End If

            Dim dsEmpBank As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            objDataOperation.ClearParameters()
            'Sohail (22 Jan 2019) -- End

            '*** Uncomment for Comma Separated ****
            'StrQ = "WITH    cte ( employeeunkid, employeecode, surname, firstname, initial, amount, branchunkid, branchname, accountno, Id, GName, payperiodunkid, period_name, ROWNO, tranheadunkid ) AS (  " & _
            'Sohail (22 Jan 2019) -- Start
            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
            'StrQ = "SELECT  A.employeeunkid  " & _
            '            ", A.employeecode " & _
            '            ", A.surname " & _
            '            ", A.firstname " & _
            '            ", A.initial " & _
            '            ", SUM(A.amount) AS amount " & _
            '            ", A.branchunkid " & _
            '            ", A.branchname " & _
            '            ", A.accountno " & _
            '            ", A.Id " & _
            '            ", A.GName " & _
            '            ", A.payperiodunkid " & _
            '            ", A.period_name " & _
            '            ", A.ROWNO " & _
            '            ", A.tranheadunkid  "
            StrQ = "SELECT  A.employeeunkid  " & _
                        ", A.employeecode " & _
                        ", A.surname " & _
                        ", A.firstname " & _
                        ", A.initial " & _
                        ", SUM(A.amount) AS amount " & _
                        ", A.Id " & _
                        ", A.GName " & _
                        ", A.payperiodunkid " & _
                        ", A.period_name " & _
                        ", A.tranheadunkid  "
            'Sohail (22 Jan 2019) -- End

            If mintMembershipId > 0 Then
                StrQ &= " , A.membershipunkid " & _
                        " , A.membershipno "
            End If
            StrQ &= " FROM ( "

            For i = 0 To marrDatabaseName.Count - 1
                strDatabaseName = marrDatabaseName(i)

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'StrInnerQ &= " SELECT    ISNULL(hremployee_master.employeeunkid, 0) employeeunkid " & _
                '             ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
                '             ", ISNULL(hremployee_master.surname, '') surname " & _
                '             ", ISNULL(hremployee_master.firstname, '') firstname " & _
                '             ", ISNULL(hremployee_master.othername, '') initial " & _
                '             ", CAST(CASE modeid WHEN 2 THEN (prpayrollprocess_tran.amount * percentage) / 100 ELSE prpayrollprocess_tran.amount END AS DECIMAL(36, " & decDecimalPlaces & ")) AS amount " & _
                '             ", cfcommon_period_tran.period_name " & _
                '             ", prtnaleave_tran.payperiodunkid " & _
                '             ", ISNULL(premployee_bank_tran.branchunkid, 0) AS branchunkid " & _
                '             ", ISNULL(cfbankbranch_master.branchname, '') AS branchname " & _
                '             ", ISNULL(accountno, '') AS accountno " & _
                '             ", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY edperiod.end_date DESC ) AS ROWNO "
                StrInnerQ &= " SELECT    ISNULL(hremployee_master.employeeunkid, 0) employeeunkid " & _
                             ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
                             ", ISNULL(hremployee_master.surname, '') surname " & _
                             ", ISNULL(hremployee_master.firstname, '') firstname " & _
                             ", ISNULL(hremployee_master.othername, '') initial " & _
                             ", CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS amount " & _
                             ", cfcommon_period_tran.period_name " & _
                             ", prtnaleave_tran.payperiodunkid "
                'Sohail (22 Jan 2019) -- End

                If mintModeId = 99999 Then
                    StrInnerQ &= " , 0 AS tranheadunkid "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= ", ISNULL(cmexpense_master.expenseunkid, ISNULL(crretireexpense.expenseunkid, -1)) AS tranheadunkid "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    StrInnerQ &= " , ISNULL(prtranhead_master.tranheadunkid,0) tranheadunkid "
                    'Sohail (23 Jun 2015) - [", DENSE_RANK() OVER ( PARTITION BY prpayrollprocess_tran.employeeunkid, cfcommon_period_tran.end_date ORDER BY edperiod.end_date DESC ) AS ROWNO "]
                End If

                If mintMembershipId > 0 Then
                    StrInnerQ &= " , ISNULL(hremployee_meminfo_tran.membershipunkid,0) membershipunkid " & _
                                 " , ISNULL(hremployee_meminfo_tran.membershipno,'') membershipno "

                End If


                If mintViewIndex > 0 Then
                    StrInnerQ &= mstrAnalysis_Fields
                Else
                    StrInnerQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'StrInnerQ &= " FROM     " & strDatabaseName & "..prpayrollprocess_tran " & _
                '             "  JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
                '             "  LEFT JOIN " & strDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '             "  LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                '             "  LEFT JOIN " & strDatabaseName & "..premployee_bank_tran ON prpayrollprocess_tran.employeeunkid = premployee_bank_tran.employeeunkid " & _
                '             "  LEFT JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
                '             "  LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran AS edperiod ON edperiod.periodunkid = premployee_bank_tran.periodunkid "
                StrInnerQ &= " FROM     " & strDatabaseName & "..prpayrollprocess_tran " & _
                             "  JOIN " & strDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
                             "  LEFT JOIN " & strDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "  LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "
                'Sohail (22 Jan 2019) -- End

                If mintModeId = 99999 Then
                    StrInnerQ &= " LEFT JOIN  " & strDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid AND prpayrollprocess_tran.loanadvancetranunkid > 0 "
                    'Sohail (23 Jun 2015) -- Start
                    'Enhancement - Claim Request Expense on E&D Detail Reports.
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= " LEFT JOIN " & strDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                 " LEFT JOIN " & strDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                 " LEFT JOIN " & strDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                        "AND prpayrollprocess_tran.crprocesstranunkid > 0 " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                 "LEFT JOIN " & strDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid " & _
                                        "AND prpayrollprocess_tran.crretirementprocessunkid > 0 "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                    'Sohail (23 Jun 2015) -- End
                Else
                    StrInnerQ &= " LEFT JOIN  " & strDatabaseName & "..prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                End If

                If mintMembershipId > 0 Then

                    StrInnerQ &= " LEFT JOIN " & strDatabaseName & "..hremployee_Meminfo_tran ON prpayrollprocess_tran.employeeunkid = hremployee_Meminfo_tran.employeeunkid AND hremployee_Meminfo_tran.membershipunkid = " & mintMembershipId & " AND hremployee_Meminfo_tran.isactive = 1 "
                End If

                StrInnerQ &= mstrAnalysis_Join

                If xDateJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xDateJoinQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrInnerQ &= xAdvanceJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                If xUACQry.Trim.Length > 0 Then
                    StrInnerQ &= xUACQry
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'StrInnerQ &= " WHERE  prtnaleave_tran.isvoid = 0 " & _
                '             "  AND prpayrollprocess_tran.isvoid = 0 AND ISNULL(premployee_bank_tran.isvoid, 0) = 0 "
                StrInnerQ &= " WHERE  prtnaleave_tran.isvoid = 0 " & _
                            "  AND prpayrollprocess_tran.isvoid = 0 "
                'Sohail (22 Jan 2019) -- End

                If mintModeId = 99999 Then
                    StrInnerQ &= " AND lnloan_advance_tran.isvoid = 0 AND prpayrollprocess_tran.loanadvancetranunkid > 0 AND isloan = 0 "
                ElseIf mintModeId = 99998 Then
                    StrInnerQ &= " AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                 " AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                 " AND ISNULL(crretireexpense.isactive, 1) = 1 "
                    'Sohail (09 Jun 2021) - [crretireexpense]
                Else
                    'Sohail (22 Jan 2019) -- Start
                    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                    'StrInnerQ &= " AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    '             " AND edperiod.modulerefid = " & enModuleReference.Payroll & _
                    '             " AND edperiod.end_date <=  cfcommon_period_tran.end_date "
                    StrInnerQ &= " AND ISNULL(prtranhead_master.isvoid, 0) = 0 "
                    'Sohail (22 Jan 2019) -- End
                End If

                If mblnIncludeNegativeHeads = True Then
                    StrInnerQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0) <> 0 "
                Else
                    StrInnerQ &= " AND ISNULL(prpayrollprocess_tran.amount, 0)  > 0 "
                End If

                If mintModeId = 99999 Then
                    'Do Nothing
                ElseIf mintModeId = 99998 Then
                    'Do Nothing
                Else
                    'Sohail (22 Jan 2019) -- Start
                    'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                    'StrInnerQ &= " AND CONVERT(CHAR(8), edperiod.end_date, 112) <= '" & strEndDate & "' "
                    'Do Nothing
                    'Sohail (22 Jan 2019) -- End
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrInnerQ &= " AND " & mstrAdvance_Filter
                End If

                If mblnIsActive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrInnerQ &= xDateFilterQry
                    End If
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrInnerQ &= " AND " & xUACFiltrQry
                'End If
                'S.SANDEEP [15 NOV 2016] -- END

                If i = 0 Then
                    Call FilterTitleAndFilterQuery()
                End If

                StrInnerQ &= Me._FilterQuery

            Next

            StrQ &= StrInnerQ

            'Sohail (22 Jan 2019) -- Start
            'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
            'StrQ &= "  ) AS A WHERE  ROWNO = 1 " & _
            '        "GROUP BY A.employeeunkid " & _
            '              ", A.employeecode " & _
            '              ", A.surname " & _
            '              ", A.firstname " & _
            '              ", A.initial " & _
            '              ", A.branchunkid " & _
            '              ", A.branchname " & _
            '              ", A.accountno " & _
            '              ", A.Id " & _
            '              ", A.GName " & _
            '              ", A.payperiodunkid " & _
            '              ", A.period_name " & _
            '              ", A.ROWNO " & _
            '              ", A.tranheadunkid "
            StrQ &= "  ) AS A WHERE  1 = 1 " & _
                    "GROUP BY A.employeeunkid " & _
                          ", A.employeecode " & _
                          ", A.surname " & _
                          ", A.firstname " & _
                          ", A.initial " & _
                          ", A.Id " & _
                          ", A.GName " & _
                          ", A.payperiodunkid " & _
                          ", A.period_name " & _
                          ", A.tranheadunkid "
            'Sohail (22 Jan 2019) -- End

            If mintMembershipId > 0 Then
                StrQ &= ", A.membershipunkid " & _
                        ", A.membershipno "
            End If

            '*** Uncomment for Comma Separated ****
            'StrQ &= "   ) " & _
            '    "SELECT  employeeunkid  " & _
            '          ", employeecode " & _
            '          ", surname " & _
            '          ", firstname " & _
            '          ", initial " & _
            '          ", amount " & _
            '          ", branchname = ISNULL(STUFF((SELECT   ', ' + branchname " & _
            '                                       "FROM     CTE AS b " & _
            '                                       "WHERE    a.employeeunkid = b.employeeunkid " & _
            '                                "FOR   XML PATH('') , ROOT('MyString') , TYPE) .value('/MyString[1]' , 'varchar(max)'), 1, 2 , ''), '') " & _
            '          ", accountno = ISNULL(STUFF((SELECT    ', ' + accountno " & _
            '                                      "FROM      cte AS b " & _
            '                                      "WHERE     a.employeeunkid = b.employeeunkid " & _
            '                               "FOR   XML PATH('')  , ROOT('MyString') , TYPE) .value('/MyString[1]' , 'varchar(max)'), 1, 2 , ''), '') " & _
            '          ", Id " & _
            '          ", GName " & _
            '          ", payperiodunkid " & _
            '          ", period_name " & _
            '          ", ROWNO " & _
            '          ", tranheadunkid " & _
            '    "FROM    CTE AS a " & _
            '    "GROUP BY employeeunkid  " & _
            '          ", employeecode " & _
            '          ", surname " & _
            '          ", firstname " & _
            '          ", initial " & _
            '          ", amount " & _
            '          ", Id " & _
            '          ", GName " & _
            '          ", payperiodunkid " & _
            '          ", period_name " & _
            '          ", ROWNO " & _
            '          ", tranheadunkid "

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtList As DataTable = New DataView(dsList.Tables(0), "", "Gname", DataViewRowState.CurrentRows).ToTable()

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim objED As New clsEarningDeduction
            Dim strPrevGroup As String = ""
            Dim decSubTotal As Decimal = 0
            Dim strPrevPeriod As String = ""
            Dim decPeriodTotal As Decimal = 0

            For Each dtRow As DataRow In dtList.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("GName")
                rpt_Row.Item("Column2") = dtRow.Item("employeecode")
                'Sohail (11 Feb 2021) -- Start
                'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
                'rpt_Row.Item("Column3") = dtRow.Item("surname")
                If mblnShowEmpNameInSingleColumn = False Then
                rpt_Row.Item("Column3") = dtRow.Item("surname")
                Else
                    Dim strName As String = Microsoft.VisualBasic.Strings.Replace(mstrEmpNameFormat, "#firstname#", dtRow.Item("firstname").ToString, , , CompareMethod.Text)
                    strName = Microsoft.VisualBasic.Strings.Replace(strName, "#middlename#", dtRow.Item("initial").ToString, , , CompareMethod.Text)
                    strName = Microsoft.VisualBasic.Strings.Replace(strName, "#surname#", dtRow.Item("surname").ToString, , , CompareMethod.Text)
                    rpt_Row.Item("Column3") = strName
                End If
                'Sohail (11 Feb 2021) -- End
                rpt_Row.Item("Column4") = dtRow.Item("firstname")
                rpt_Row.Item("Column5") = dtRow.Item("initial")
                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'rpt_Row.Item("Column7") = dtRow.Item("branchname")
                'rpt_Row.Item("Column9") = dtRow.Item("accountno")
                'Sohail (22 Jan 2019) -- End
                rpt_Row.Item("Column10") = dtRow.Item("period_name")


                If CInt(dtRow.Item("tranheadunkid")) <> mintHeadID Then Continue For

                rpt_Row.Item("Column6") = Format(CDec(dtRow("amount")), mstrfmtCurrency)
                rpt_Row.Item("Column81") = CDec(dtRow("amount"))

                If mintViewIndex > 0 Then
                    If strPrevGroup <> dtRow.Item("GName").ToString Then
                        decSubTotal = CDec(Format(dtRow("amount"), mstrfmtCurrency))
                    Else
                        decSubTotal += CDec(Format(dtRow("amount"), mstrfmtCurrency))
                    End If
                    strPrevGroup = dtRow.Item("GName").ToString
                End If

                rpt_Row.Item("Column8") = Format(decSubTotal, mstrfmtCurrency)


                If strPrevPeriod <> dtRow.Item("period_name").ToString Then
                    decPeriodTotal = 0
                    decPeriodTotal = CDec(Format(dtRow("amount"), mstrfmtCurrency))
                Else
                    decPeriodTotal += CDec(Format(dtRow("amount"), mstrfmtCurrency))
                End If

                strPrevPeriod = dtRow.Item("period_name").ToString
                rpt_Row.Item("Column11") = Format(decPeriodTotal, mstrfmtCurrency)

                If mintMembershipId > 0 Then
                    Dim drMemberShip() As DataRow = dtList.Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND membershipunkid = " & mintMembershipId)
                    If drMemberShip.Length > 0 Then
                        rpt_Row.Item("Column12") = drMemberShip(0)("membershipno").ToString()
                    Else
                        rpt_Row.Item("Column12") = ""
                    End If
                Else
                    rpt_Row.Item("Column12") = ""
                End If

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Dim dtTran As DataTable = New DataView(dsEmpBank.Tables(0), "employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND payperiodunkid = " & CInt(dtRow.Item("payperiodunkid")) & " ", "priority", DataViewRowState.CurrentRows).ToTable
                If dtTran.Rows.Count > 0 Then
                    Dim decBaseAmt As Decimal = CDec(dtRow.Item("amount"))
                    Dim drArray As DataRow = Nothing

                    For Each dsRow As DataRow In dtTran.Rows
                        rpt_Row.Item("Column7") = dsRow.Item("branchname")
                        rpt_Row.Item("Column9") = dsRow.Item("accountno").ToString
                        rpt_Row.Item("Column21") = dsRow.Item("branchcode").ToString
                        rpt_Row.Item("Column22") = dsRow.Item("BankGroupCode").ToString
                        rpt_Row.Item("Column23") = dsRow.Item("BankGroupName").ToString
                        rpt_Row.Item("Column24") = dsRow.Item("SortCode").ToString
                        rpt_Row.Item("Column25") = dsRow.Item("SwiftCode").ToString
                        rpt_Row.Item("Column26") = dsRow.Item("CMP_BankGroupCode").ToString
                        rpt_Row.Item("Column27") = dsRow.Item("CMP_BankGroupName").ToString
                        rpt_Row.Item("Column28") = dsRow.Item("CMP_branchcode").ToString
                        rpt_Row.Item("Column29") = dsRow.Item("CMP_branchname").ToString
                        'Sohail (25 May 2021) -- Start
                        'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                        'rpt_Row.Item("Column30") = dsRow.Item("CMP_Account").ToString
                        If mblnExportToEFTCityDirectTXT = False Then
                        rpt_Row.Item("Column30") = dsRow.Item("CMP_Account").ToString
                        Else
                            rpt_Row.Item("Column30") = mstrCompanyAccountNo
                        End If
                        'Sohail (25 May 2021) -- End
                        rpt_Row.Item("Column31") = dsRow.Item("CMP_SortCode").ToString
                        rpt_Row.Item("Column32") = dsRow.Item("CMP_SwiftCode").ToString
                        'Sohail (12 May 2021) -- Start
                        'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                        rpt_Row.Item("Column61") = dsRow.Item("isclearing").ToString
                        rpt_Row.Item("Column62") = dsRow.Item("currency_sign").ToString
                        'Sohail (12 May 2021) -- End

                        Dim lstPerc As Integer = (From p In dtTran Where (CInt(p.Item("modeid")) = CInt(enPaymentBy.Percentage))).Count()
                        If lstPerc > 0 Then
                            rpt_Row.Item("Column6") = Format(decBaseAmt * CDec(dsRow.Item("percentage")) / 100, mstrfmtCurrency)
                            rpt_Row.Item("Column81") = decBaseAmt * CDec(dsRow.Item("percentage")) / 100
                            drArray = rpt_Data.Tables("ArutiTable").NewRow
                            drArray.ItemArray = rpt_Row.ItemArray
                            rpt_Data.Tables("ArutiTable").Rows.Add(drArray)
                        Else
                            Dim lst As List(Of DataRow) = (From p In dtTran Where CInt(p.Item("modeid")) = enPaymentBy.Value).DefaultIfEmpty.ToList
                            Dim decAmt As Decimal = decBaseAmt
                            Dim decPriorityTotal As Decimal = (From p In lst Select CDec(p.Item("amount"))).DefaultIfEmpty.Sum
                            If decPriorityTotal < decBaseAmt Then
                                Dim intMinPriority As Integer = (From p In lst Select CInt(p.Item("priority"))).DefaultIfEmpty.Min
                                Dim decOtherTotal As Decimal = (From p In lst Where CInt(p.Item("priority")) <> intMinPriority Select CDec(p.Item("Amount"))).DefaultIfEmpty.Sum

                                For Each dRow As DataRow In lst
                                    rpt_Row.Item("Column7") = dRow.Item("branchname")
                                    rpt_Row.Item("Column9") = dRow.Item("accountno").ToString
                                    rpt_Row.Item("Column21") = dsRow.Item("branchcode").ToString
                                    rpt_Row.Item("Column22") = dsRow.Item("BankGroupCode").ToString
                                    rpt_Row.Item("Column23") = dsRow.Item("BankGroupName").ToString
                                    rpt_Row.Item("Column24") = dsRow.Item("SortCode").ToString
                                    rpt_Row.Item("Column25") = dsRow.Item("SwiftCode").ToString
                                    rpt_Row.Item("Column26") = dsRow.Item("CMP_BankGroupCode").ToString
                                    rpt_Row.Item("Column27") = dsRow.Item("CMP_BankGroupName").ToString
                                    rpt_Row.Item("Column28") = dsRow.Item("CMP_branchcode").ToString
                                    rpt_Row.Item("Column29") = dsRow.Item("CMP_branchname").ToString
                                    'Sohail (25 May 2021) -- Start
                                    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                                    'rpt_Row.Item("Column30") = dsRow.Item("CMP_Account").ToString
                                    If mblnExportToEFTCityDirectTXT = False Then
                                    rpt_Row.Item("Column30") = dsRow.Item("CMP_Account").ToString
                                    Else
                                        rpt_Row.Item("Column30") = mstrCompanyAccountNo
                                    End If
                                    'Sohail (25 May 2021) -- End
                                    rpt_Row.Item("Column31") = dsRow.Item("CMP_SortCode").ToString
                                    rpt_Row.Item("Column32") = dsRow.Item("CMP_SwiftCode").ToString
                                    'Sohail (12 May 2021) -- Start
                                    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                                    rpt_Row.Item("Column61") = dsRow.Item("isclearing").ToString
                                    rpt_Row.Item("Column62") = dsRow.Item("currency_sign").ToString
                                    'Sohail (12 May 2021) -- End

                                    If CInt(dRow.Item("priority")) = intMinPriority Then
                                        rpt_Row.Item("Column6") = Format(decBaseAmt - decOtherTotal, mstrfmtCurrency)
                                        rpt_Row.Item("Column81") = decBaseAmt - decOtherTotal
                                    Else
                                        rpt_Row.Item("Column6") = Format(CDec(dRow.Item("Amount")), mstrfmtCurrency)
                                        rpt_Row.Item("Column81") = CDec(dRow.Item("Amount"))
                                    End If
                                    drArray = rpt_Data.Tables("ArutiTable").NewRow
                                    drArray.ItemArray = rpt_Row.ItemArray
                                    rpt_Data.Tables("ArutiTable").Rows.Add(drArray)
                                Next
                            Else
                                Dim decBalance As Decimal = decAmt
                                For Each dRow As DataRow In lst
                                    If decAmt <= 0 Then Exit For

                                    rpt_Row.Item("Column7") = dRow.Item("branchname")
                                    rpt_Row.Item("Column9") = dRow.Item("accountno").ToString
                                    rpt_Row.Item("Column21") = dsRow.Item("branchcode").ToString
                                    rpt_Row.Item("Column22") = dsRow.Item("BankGroupCode").ToString
                                    rpt_Row.Item("Column23") = dsRow.Item("BankGroupName").ToString
                                    rpt_Row.Item("Column24") = dsRow.Item("SortCode").ToString
                                    rpt_Row.Item("Column25") = dsRow.Item("SwiftCode").ToString
                                    rpt_Row.Item("Column26") = dsRow.Item("CMP_BankGroupCode").ToString
                                    rpt_Row.Item("Column27") = dsRow.Item("CMP_BankGroupName").ToString
                                    rpt_Row.Item("Column28") = dsRow.Item("CMP_branchcode").ToString
                                    rpt_Row.Item("Column29") = dsRow.Item("CMP_branchname").ToString
                                    'Sohail (25 May 2021) -- Start
                                    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                                    'rpt_Row.Item("Column30") = dsRow.Item("CMP_Account").ToString
                                    If mblnExportToEFTCityDirectTXT = False Then
                                    rpt_Row.Item("Column30") = dsRow.Item("CMP_Account").ToString
                                    Else
                                        rpt_Row.Item("Column30") = mstrCompanyAccountNo
                                    End If
                                    'Sohail (25 May 2021) -- End
                                    rpt_Row.Item("Column31") = dsRow.Item("CMP_SortCode").ToString
                                    rpt_Row.Item("Column32") = dsRow.Item("CMP_SwiftCode").ToString
                                    'Sohail (12 May 2021) -- Start
                                    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                                    rpt_Row.Item("Column61") = dsRow.Item("isclearing").ToString
                                    rpt_Row.Item("Column62") = dsRow.Item("currency_sign").ToString
                                    'Sohail (12 May 2021) -- End

                                    If decAmt > CDec(dRow.Item("Amount")) Then
                                        rpt_Row.Item("Column6") = Format(CDec(dRow.Item("Amount")), mstrfmtCurrency)
                                        rpt_Row.Item("Column81") = CDec(dRow.Item("Amount"))
                                    Else
                                        rpt_Row.Item("Column6") = Format(decAmt, mstrfmtCurrency)
                                        rpt_Row.Item("Column81") = decAmt
                                    End If

                                    decAmt -= CDec(dRow.Item("Amount"))

                                    drArray = rpt_Data.Tables("ArutiTable").NewRow
                                    drArray.ItemArray = rpt_Row.ItemArray
                                    rpt_Data.Tables("ArutiTable").Rows.Add(drArray)
                                Next
                            End If
                            Exit For

                        End If

                    Next
                Else
                    'Sohail (25 May 2021) -- Start
                    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
                    rpt_Row.Item("Column7") = ""
                    rpt_Row.Item("Column9") = ""
                    rpt_Row.Item("Column21") = ""
                    rpt_Row.Item("Column22") = ""
                    rpt_Row.Item("Column23") = ""
                    rpt_Row.Item("Column24") = ""
                    rpt_Row.Item("Column25") = ""
                    rpt_Row.Item("Column26") = ""
                    rpt_Row.Item("Column27") = ""
                    rpt_Row.Item("Column28") = ""
                    rpt_Row.Item("Column29") = ""
                    If mblnExportToEFTCityDirectTXT = False Then
                        rpt_Row.Item("Column30") = "'"
                    Else
                        rpt_Row.Item("Column30") = mstrCompanyAccountNo
                    End If
                    rpt_Row.Item("Column31") = ""
                    rpt_Row.Item("Column32") = ""
                    rpt_Row.Item("Column61") = "False"
                    rpt_Row.Item("Column62") = ""
                    'Sohail (25 May 2021) -- End
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                End If
                'Sohail (22 Jan 2019) -- End

            Next

            'Sohail (12 May 2021) -- Start
            'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
            If mblnExportToEFTCityDirectTXT = True Then

                mblnIsExportEFTCitiDirectSuccess = ExportToEFTCitiDirectTXT(rpt_Data.Tables(0), intCompanyUnkid, mstrfmtCurrency, strExportReportPath, blnOpenReportAfterExport) = False

                Return Nothing
            End If
            'Sohail (12 May 2021) -- End

            objRpt = New ArutiReport.Designer.rptEDDetailBankAccount


            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(CDec(rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")), mstrfmtCurrency))
            Else
                ReportFunction.TextChange(objRpt, "txtGrandTotalValue", Format(0, mstrfmtCurrency))
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 31, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mintMembershipId <= 0 Then
                Call ReportFunction.EnableSuppress(objRpt, "txtMembershipNo", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column121", True)

                objRpt.ReportDefinition.ReportObjects("txtBranch").Width = 2620
                objRpt.ReportDefinition.ReportObjects("Column71").Width = 2620

                objRpt.ReportDefinition.ReportObjects("txtAccountNo").Left = 8210
                objRpt.ReportDefinition.ReportObjects("Column92").Left = 8210

            Else
                Call ReportFunction.TextChange(objRpt, "txtMembershipNo", Language.getMessage(mstrModuleName, 28, "Membership No."))
            End If

            'Sohail (11 Feb 2021) -- Start
            'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
            If mblnShowEmpNameInSingleColumn = True Then
                Call ReportFunction.EnableSuppress(objRpt, "txtFirstName", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtInitial", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column41", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column51", True)

                objRpt.ReportDefinition.ReportObjects("txtSurName").Width = objRpt.ReportDefinition.ReportObjects("txtFirstName").Width + objRpt.ReportDefinition.ReportObjects("txtInitial").Width
                objRpt.ReportDefinition.ReportObjects("Column31").Width = objRpt.ReportDefinition.ReportObjects("Column41").Width + objRpt.ReportDefinition.ReportObjects("Column51").Width
            End If
            'Sohail (11 Feb 2021) -- End

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 22, "Emp No."))
            'Sohail (11 Feb 2021) -- Start
            'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
            'Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Surname"))
            If mblnShowEmpNameInSingleColumn = False Then
            Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 10, "Surname"))
            Else
                Call ReportFunction.TextChange(objRpt, "txtSurName", Language.getMessage(mstrModuleName, 34, "Employee Name"))
            End If
            'Sohail (11 Feb 2021) -- End
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "First Name"))
            Call ReportFunction.TextChange(objRpt, "txtInitial", Language.getMessage(mstrModuleName, 12, "Initial"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 13, "Amount"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 14, "Bank Branch"))
            Call ReportFunction.TextChange(objRpt, "txtAccountNo", Language.getMessage(mstrModuleName, 30, "Account No."))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 15, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 17, "Page :"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 18, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 20, "Sub Count :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalCount", Language.getMessage(mstrModuleName, 21, "Total Count :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 25, "Period :"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodTotal", Language.getMessage(mstrModuleName, 26, "Period Total:"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodCount", Language.getMessage(mstrModuleName, 27, "Period Count:"))



            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & "  [ " & "(" & mstrHeadCode & "), " & mstrHeadName & " ] ")
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)
                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                Dim arrList As New ArrayList(mstrEFTCustomXLSColumnsIds.Split(","))
                dsList = (New clsMasterData).GetComboListForEFTCustomColumns("List", False)
                Dim dic As New Dictionary(Of Integer, String)
                If dsList.Tables(0).Rows.Count > 0 Then
                    dic = (From p In dsList.Tables(0).AsEnumerable() Select New With {Key .id = CInt(p.Item("id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(y) y.Name)
                End If
                'Sohail (22 Jan 2019) -- End

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Emp No.")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                'Sohail (11 Feb 2021) -- Start
                'SCANIA TZ Enhancement : OLD-273 : E&D Detailed report with Bank Account to include Full Employee Name.
                'mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
                If mblnShowEmpNameInSingleColumn = False Then
                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "Surname")
                Else
                    mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 34, "Employee Name")
                End If
                'Sohail (11 Feb 2021) -- End
                mdtTableExcel.Columns("Column3").SetOrdinal(mintColumn)
                mintColumn += 1

                If mblnShowEmpNameInSingleColumn = False Then 'Sohail (11 Feb 2021)
                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 11, "First Name")
                mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 12, "Initial")
                mdtTableExcel.Columns("Column5").SetOrdinal(mintColumn)
                mintColumn += 1
                End If 'Sohail (11 Feb 2021)

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                'mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "Bank Branch")
                'mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
                'mintColumn += 1

                'mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 30, "Account No.")
                'mdtTableExcel.Columns("Column9").SetOrdinal(mintColumn)
                'mintColumn += 1

                'If mintMembershipId > 0 Then
                '    mdtTableExcel.Columns("Column12").Caption = Language.getMessage(mstrModuleName, 28, "Membership No.")
                '    mdtTableExcel.Columns("Column12").SetOrdinal(mintColumn)
                '    mintColumn += 1
                'End If
                'Sohail (22 Jan 2019) -- End

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 13, "Amount")
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 4, "Period :")
                mdtTableExcel.Columns("Column10").SetOrdinal(mintColumn)
                mintColumn += 1

                'Sohail (22 Jan 2019) -- Start
                'SLMC Enhancement - Support Issue # 2593 - 74.1 - Need option to select the required columns as in the “Bank Payment List” Report type "EFT Advance Salary CSV" to show bank details custom columns on E&D Detail with Bank Account Report.
                For Each id As String In arrList

                    Select Case CInt(id)

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_CODE
                            mdtTableExcel.Columns("Column26").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column26").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_NAME
                            mdtTableExcel.Columns("Column27").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column27").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.COMP_BRANCH_CODE
                            mdtTableExcel.Columns("Column28").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column28").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.COMP_BRANCH_NAME
                            mdtTableExcel.Columns("Column29").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column29").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_SORT_CODE
                            mdtTableExcel.Columns("Column31").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column31").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_SWIFT_CODE
                            mdtTableExcel.Columns("Column32").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column32").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_ACCOUNT_NO
                            mdtTableExcel.Columns("Column30").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column30").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_CODE
                            mdtTableExcel.Columns("Column22").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column22").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_NAME
                            mdtTableExcel.Columns("Column23").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column23").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BRANCH_CODE
                            mdtTableExcel.Columns("Column21").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column21").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BRANCH_NAME
                            mdtTableExcel.Columns("Column7").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column7").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_SORT_CODE
                            mdtTableExcel.Columns("Column24").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column24").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_SWIFT_CODE
                            mdtTableExcel.Columns("Column25").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column25").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_ACCOUNT_NO
                            mdtTableExcel.Columns("Column9").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column9").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMPLOYEE_CODE
                            mdtTableExcel.Columns("Column2").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                            mintColumn += 1

                        Case enEFT_EFT_Custom_Columns.EMPLOYEE_NAME


                        Case enEFT_EFT_Custom_Columns.EMP_BANK_AMOUNT


                        Case enEFT_EFT_Custom_Columns.DESCRIPTION


                        Case enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO
                            mdtTableExcel.Columns("Column12").Caption = dic.Item(CInt(id))
                            mdtTableExcel.Columns("Column12").SetOrdinal(mintColumn)
                            mintColumn += 1

                    End Select

                Next
                'Sohail (22 Jan 2019) -- End

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column1").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next


            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function
    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (12 May 2021) -- Start
    'Scania Kenya Enhancement : OLD-381 : Scania Kenya- Salary Advance EFT.
    Private Function ExportToEFTCitiDirectTXT(ByVal dtTable As DataTable, ByVal intCompanyUnkid As Integer, ByVal strFmtCurrency As String, ByVal strExportReportPath As String, ByVal blnOpenReportAfterExport As Boolean) As Boolean
        Dim strBuilder As New StringBuilder
        Dim objConfig As New clsConfigOptions
        Dim objCompany As New clsCompany_Master
        Dim strCompanyName As String = ""
        Try

            objCompany._Companyunkid = intCompanyUnkid
            objConfig._Companyunkid = intCompanyUnkid

            strCompanyName = objCompany._Name

            For Each dtRow As DataRow In dtTable.Rows
                strBuilder.Append("") 'Placeholder
                If objConfig._EFTCitiDirectCountryCode <> "" Then
                    strBuilder.Append("#" & objConfig._EFTCitiDirectCountryCode.Trim & "") 'Country Code
                Else
                    strBuilder.Append("#TZ") 'Country Code
                End If
                strBuilder.Append("#DFT") 'Payment Method 
                strBuilder.Append("#" & eZeeDate.convertDate(objConfig._CurrentDateAndTime)) 'Value Date
                If objConfig._EFTCitiDirectSkipPriorityFlag = False Then
                    If CBool(dtRow.Item("Column61")) = True Then
                        If CDec(dtRow.Item("Column6")) >= 10000000 Then
                            strBuilder.Append("#Y") 'Priority Flag
                        Else
                            strBuilder.Append("#N") 'Priority Flag
                        End If
                    Else
                        strBuilder.Append("#Y") 'Priority Flag
                    End If
                Else
                    strBuilder.Append("#") 'Priority Flag
                End If
                strBuilder.Append("#") 'PreFormat Group Code
                strBuilder.Append("#") 'Preformat Code
                strBuilder.Append("#") 'Payment Type
                strBuilder.Append("#" & dtRow.Item("Column62").ToString) 'Payment Currency
                strBuilder.Append("#" & Rounding.BRound(CDec(dtRow.Item("Column6")), objConfig._RoundOff_Type).ToString(strFmtCurrency).Replace(",", "")) 'Payment Amount
                strBuilder.Append("#") 'Debit Party Account or Other ID Type
                strBuilder.Append("#" & dtRow.Item("Column30").ToString) 'Debit Account
                strBuilder.Append("#") 'Debit Party Name 
                strBuilder.Append("#") 'Debit Party Advice Type
                strBuilder.Append("#") 'Debit Bank Routing Code
                strBuilder.Append("#") 'Debit Bank Name
                strBuilder.Append("#") 'Debit Bank Address Line 1
                strBuilder.Append("#") 'Debit Bank Address Line 2
                strBuilder.Append("#") 'Debit Bank Address Line 3 
                strBuilder.Append("#") 'Placeholder
                strBuilder.Append("#") 'Number of Credit Parties

                If objConfig._EFTCitiDirectChargesIndicator.Trim <> "" Then
                    strBuilder.Append("#" & objConfig._EFTCitiDirectChargesIndicator.Trim & "") 'Charges Indicator 
                Else
                    strBuilder.Append("#") 'Charges Indicator
                End If

                strBuilder.Append("#") 'Processing Date
                strBuilder.Append("#") 'Debit Account Currency
                strBuilder.Append("#" & dtRow.Item("Column2").ToString) 'Transaction Reference Number
                strBuilder.Append("#") 'Beneficiary Reference
                strBuilder.Append("#") 'Confidential
                strBuilder.Append("#") 'Intra-Company
                strBuilder.Append("#") 'Placeholder
                strBuilder.Append("#") 'Transaction Type
                strBuilder.Append("#") 'Entry Description
                strBuilder.Append("#") 'Individual Company ID
                strBuilder.Append("#") 'Pre Note 
                strBuilder.Append("#") 'Funds Type 
                strBuilder.Append("#") 'Ordering Party ID Type
                strBuilder.Append("#") 'Ordering Party ID
                strBuilder.Append("#") 'Ordering Party Name
                strBuilder.Append("#") 'Ordering Party Address Line1
                strBuilder.Append("#") 'Ordering Party Address Line 2
                strBuilder.Append("#") 'Ordering Party Address Line 3
                strBuilder.Append("#") 'Credit Account
                strBuilder.Append("#") 'Beneficiary Is
                strBuilder.Append("#") 'Beneficiary Account or Other ID Type
                strBuilder.Append("#" & dtRow.Item("Column9").ToString) 'Beneficiary Account or Other ID
                If mblnShowEmpNameInSingleColumn = False Then
                    strBuilder.Append("#" & (dtRow.Item("Column4").ToString & " " & dtRow.Item("Column5").ToString & " " & dtRow.Item("Column3").ToString).Replace("  ", " ")) 'Beneficiary Name
                Else
                    strBuilder.Append("#" & dtRow.Item("Column3").ToString.Replace("  ", " ")) 'Beneficiary Name
                End If
                strBuilder.Append("#") 'Beneficiary Advice Type
                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                'strBuilder.Append("#") 'Beneficiary Address Line 1
                'strBuilder.Append("#") 'Beneficiary Address Line 2
                strBuilder.Append("#" & objCompany._Address1) 'Beneficiary Address Line 1
                strBuilder.Append("#" & objCompany._State_Name) 'Beneficiary Address Line 2
                'Sohail (27 Jul 2021) -- End
                strBuilder.Append("#") 'Beneficiary Address Line 3
                strBuilder.Append("#") 'Beneficiary Bank Routing Method
                strBuilder.Append("#" & dtRow.Item("Column24").ToString) 'Beneficiary Bank Routing Code
                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                'strBuilder.Append("#") 'Beneficiary Bank Account or Other ID Type
                'strBuilder.Append("#") 'Beneficiary Bank Account or Other ID
                'strBuilder.Append("#") 'Beneficiary Bank Advice Type
                'strBuilder.Append("#") 'Beneficiary Bank Name
                'strBuilder.Append("#") 'Beneficiary Bank Address Line 1
                'strBuilder.Append("#") 'Beneficiary Bank Address Line 2
                'strBuilder.Append("#") 'Beneficiary Bank Address Line 3
                'strBuilder.Append("#") 'First Intermediary Bank Routing Method
                'strBuilder.Append("#") 'First Intermediary Bank Routing Code
                'strBuilder.Append("#") 'First Intermediary Bank Name
                'strBuilder.Append("#") 'First Intermediary Bank Address Line 1
                'strBuilder.Append("#") 'First Intermediary Bank Address Line 2
                'strBuilder.Append("#") 'First Intermediary Bank Address Line 3
                'strBuilder.Append("#") 'Second Intermediary Bank Account or Other ID Type
                'strBuilder.Append("#") 'Second Intermediary Bank Account or Other ID
                'strBuilder.Append("#") 'Second Intermediary Bank Advice Type
                'strBuilder.Append("#") 'Second Intermediary Bank Name
                'strBuilder.Append("#") 'Second Intermediary Bank Address- Line 1
                'strBuilder.Append("#") 'Second Intermediary Bank Address-line 2
                'strBuilder.Append("#") 'Second Intermediary Bank Address-line 3

                'If objConfig._EFTCitiDirectAddPaymentDetail = True Then
                '    strBuilder.Append("#" & Strings.Left(Language.getMessage(mstrModuleName, 35, "Salary payment for the period of") & " " & mstrPeriodName, 35)) 'Payment Details Line 1
                '    strBuilder.Append("#" & Strings.Mid(Language.getMessage(mstrModuleName, 35, "Salary payment for the period of") & " " & mstrPeriodName, 36))  'Payment Details Line 2
                '    strBuilder.Append("#" & Strings.Left(Language.getMessage(mstrModuleName, 36, "By Order of") & " " & strCompanyName, 35)) 'Payment Details Line 3
                '    strBuilder.Append("#" & Strings.Mid(Language.getMessage(mstrModuleName, 36, "By Order of") & " " & strCompanyName, 36)) 'Payment Details Line 4
                'Else
                '    strBuilder.Append("#") 'Payment Details Line 1
                '    strBuilder.Append("#")  'Payment Details Line 2
                '    strBuilder.Append("#") 'Payment Details Line 3
                '    strBuilder.Append("#") 'Payment Details Line 4
                'End If
                'strBuilder.Append("#") 'Bank Details Line 1
                'strBuilder.Append("#") 'Bank Details Line 2
                'strBuilder.Append("#") 'Bank Details Line 3
                'strBuilder.Append("#") 'Bank Details Line 4
                'strBuilder.Append("#") 'Bank Details Line 5
                'strBuilder.Append("#") 'Bank Details Line 6
                'strBuilder.Append("#") 'Pre-Advice
                'strBuilder.Append("#") 'Pre-Advice Details
                'strBuilder.Append("#") 'Addenda Information
                'strBuilder.Append("#") 'Memo Details
                'strBuilder.Append("#") 'Tax Sub Form Account Code (2)
                'strBuilder.Append("#") 'Tax Sub Form Amount Paid (2)
                'strBuilder.Append("#") 'Tax Sub Form Details (3)
                'strBuilder.Append("#") 'Tax Sub Form Due Amount (3)
                'If objConfig._EFTCitiDirectAddPaymentDetail = True Then
                '    strBuilder.Append("#") 'Tax Sub Form Code and Date(3)
                '    strBuilder.Append("#") 'Tax Sub Form Due Date (3) 
                'Else
                '    strBuilder.Append("#" & Language.getMessage(mstrModuleName, 35, "Salary payment for the period of") & " " & mstrPeriodName) 'Tax Sub Form Code and Date(3)
                '    strBuilder.Append("#" & Language.getMessage(mstrModuleName, 37, "By Order of") & " " & dtRow.Item("Column30").ToString) 'Tax Sub Form Due Date (3) 
                'End If
                'Sohail (27 Jul 2021) -- End

                strBuilder.Append(vbCrLf)
            Next


            If SaveTextFile(strExportReportPath, strBuilder) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportToEFTCitiDirectTXT; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb.ToString.Trim)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
    'Sohail (12 May 2021) -- End

#End Region


    
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Mode :")
            Language.setMessage(mstrModuleName, 3, "Head :")
            Language.setMessage(mstrModuleName, 4, "Period :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 9, "Emp No")
            Language.setMessage(mstrModuleName, 10, "Surname")
            Language.setMessage(mstrModuleName, 11, "First Name")
            Language.setMessage(mstrModuleName, 12, "Initial")
            Language.setMessage(mstrModuleName, 13, "Amount")
            Language.setMessage(mstrModuleName, 14, "Bank Branch")
            Language.setMessage(mstrModuleName, 15, "Printed By :")
            Language.setMessage(mstrModuleName, 16, "Printed Date :")
            Language.setMessage(mstrModuleName, 17, "Page :")
            Language.setMessage(mstrModuleName, 18, "Sub Total :")
            Language.setMessage(mstrModuleName, 19, "Grand Total :")
            Language.setMessage(mstrModuleName, 20, "Sub Count :")
            Language.setMessage(mstrModuleName, 21, "Total Count :")
            Language.setMessage(mstrModuleName, 22, "Emp No.")
            Language.setMessage(mstrModuleName, 23, "From Period :")
            Language.setMessage(mstrModuleName, 24, "To Period :")
            Language.setMessage(mstrModuleName, 25, "Period :")
            Language.setMessage(mstrModuleName, 26, "Period Total:")
            Language.setMessage(mstrModuleName, 27, "Period Count:")
            Language.setMessage(mstrModuleName, 28, "Membership No.")
            Language.setMessage(mstrModuleName, 29, "Membership :")
            Language.setMessage(mstrModuleName, 30, "Account No.")
            Language.setMessage(mstrModuleName, 31, "Prepared By :")
            Language.setMessage(mstrModuleName, 32, "Employee Bank :")
            Language.setMessage(mstrModuleName, 33, "Employee Branch :")
			Language.setMessage(mstrModuleName, 34, "Employee Name")
			Language.setMessage(mstrModuleName, 35, "Salary payment for the period of")
			Language.setMessage(mstrModuleName, 36, "By Order of")
			Language.setMessage(mstrModuleName, 37, "By Order of")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
