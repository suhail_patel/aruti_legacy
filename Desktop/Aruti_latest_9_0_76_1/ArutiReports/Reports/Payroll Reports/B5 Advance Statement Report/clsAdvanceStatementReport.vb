'************************************************************************************************************************************
'Class Name : clsAdvanceStatementReport.vb
'Purpose    :
'Date       : 23/02/2018
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

Public Class clsAdvanceStatementReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAdvanceStatementReport"
    Private mstrReportId As String = enArutiReport.B5_Advance_Statement_Report  '202
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mstrAdvanceVoucherNo As String = ""
    Private mdtVoucherFromDate As Date = Nothing
    Private mdtVouncherToDate As Date = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintMemoStatusID As Integer = 0
    Private mstrMemoStatus As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrOrderByQuery As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _AdvanceVoucherNo() As String
        Set(ByVal value As String)
            mstrAdvanceVoucherNo = value
        End Set
    End Property

    Public WriteOnly Property _VoucherFromDate() As Date
        Set(ByVal value As Date)
            mdtVoucherFromDate = value
        End Set
    End Property

    Public WriteOnly Property _VouncherToDate() As Date
        Set(ByVal value As Date)
            mdtVouncherToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _MemoStatusId() As Integer
        Set(ByVal value As Integer)
            mintMemoStatusID = value
        End Set
    End Property

    Public WriteOnly Property _MemoStatus() As String
        Set(ByVal value As String)
            mstrMemoStatus = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrAdvanceVoucherNo = ""
            mdtVoucherFromDate = Nothing
            mdtVouncherToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintMemoStatusID = 0
            mstrMemoStatus = ""
            mstrOrderByQuery = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrAnalysis_OrderBy = ""
            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try


            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mstrAdvanceVoucherNo.Trim.Length > 0 Then
                Me._FilterQuery &= " AND lnloan_process_pending_loan.application_no = @application_no "
                objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdvanceVoucherNo)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Advance Voucher No: ") & " " & mstrEmployeeName & " "
            End If

            If mdtVoucherFromDate <> Nothing AndAlso mdtVouncherToDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112) >= @FromDate  AND  CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112) < = @ToDate "
                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtVoucherFromDate))
                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtVouncherToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Voucher From Date: ") & " " & mdtVoucherFromDate.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Voucher To Date: ") & " " & mdtVouncherToDate.Date & " "
            End If

            If mintMemoStatusID > 0 Then
                Me._FilterQuery &= " AND lnloan_advance_tran.loan_statusunkid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMemoStatusID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Memo Status: ") & " " & mstrMemoStatus & " "
            End If

            'If mintViewIndex > 0 Then
            '    mstrOrderByQuery &= " ORDER BY " & mstrAnalysis_OrderBy_GName & ",employeecode"
            'Else
            '    mstrOrderByQuery &= " ORDER BY employeecode "
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal mdtFromDate As Date, ByVal mdtToDate As Date, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If xUserUnkid <= 0 Then
                xUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = xUserUnkid


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtFromDate, mdtToDate, True, False, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtToDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtToDate, xDatabaseName)

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()



            StrQ = " SELECT " & _
                      " lnloan_process_pending_loan.processpendingloanunkid " & _
                      " ,lnloan_process_pending_loan.application_no " & _
                      " ,lnloan_process_pending_loan.employeeunkid " & _
                      " ,lnloan_advance_tran.loanadvancetranunkid " & _
                      " ,ISNULL(hremployee_master.employeecode, '') AS Employeecode " & _
                      " ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      " ,lnloan_process_pending_loan.application_date " & _
                      " ,Dept.name AS Department " & _
                      " ,Sec.name AS Section " & _
                      " ,SecGrp.name AS SectionGrp " & _
                      " ,lnloan_process_pending_loan.emp_remark " & _
                      " ,lnloan_process_pending_loan.approved_amount AS PaidAmt " & _
                      " ,ISNULL(SUM(prpayment_tran.amount),0.00) AS receipt " & _
                      " ,(lnloan_process_pending_loan.approved_amount - ISNULL(SUM(prpayment_tran.amount),0.00)) AS balance " & _
                      " ,lnloan_advance_tran.loan_statusunkid " & _
                      ", '' AS other_remark " & _
                      " ,CASE " & _
                              "WHEN lnloan_advance_tran.loan_statusunkid = 1 THEN @InProgress " & _
                              "WHEN lnloan_advance_tran.loan_statusunkid = 2 THEN @Onhold " & _
                              "WHEN lnloan_advance_tran.loan_statusunkid = 4 THEN @Completed " & _
                      " END Memo "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM lnloan_process_pending_loan " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                         " JOIN (SELECT " & _
                         "            departmentunkid " & _
                         "           ,sectionunkid " & _
                         "           ,sectiongroupunkid " & _
                         "           ,employeeunkid " & _
                         "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate) AS rno " & _
                         "         FROM hremployee_transfer_tran " & _
                         "         WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @ToDate) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master AS Dept ON Alloc.departmentunkid = Dept.departmentunkid " & _
                         " LEFT JOIN hrsection_master AS Sec ON Alloc.sectionunkid = Sec.sectionunkid " & _
                         " LEFT JOIN hrsectiongroup_master AS SecGrp ON Alloc.departmentunkid = SecGrp.sectiongroupunkid " & _
                         " LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                         " LEFT JOIN prpayment_tran ON lnloan_advance_tran.employeeunkid = prpayment_tran.employeeunkid AND prpayment_tran.referencetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
                         " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.ADVANCE & " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.RECEIVED & _
                         " AND prpayment_tran.isvoid = 0 "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= "  WHERE lnloan_process_pending_loan.isvoid = 0 AND lnloan_advance_tran.isvoid = 0 " & _
                         " AND lnloan_process_pending_loan.isloan = 0  "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            StrQ &= " GROUP BY " & _
                         " lnloan_process_pending_loan.processpendingloanunkid " & _
                         ", lnloan_process_pending_loan.application_no " & _
                         ",lnloan_process_pending_loan.employeeunkid " & _
                         ",lnloan_advance_tran.loanadvancetranunkid " & _
                         ",ISNULL(hremployee_master.employeecode, '') " & _
                         ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                         ",lnloan_process_pending_loan.application_date " & _
                         ",Dept.name " & _
                         ",Sec.name " & _
                         ",SecGrp.name " & _
                         ",lnloan_process_pending_loan.emp_remark " & _
                         ",lnloan_process_pending_loan.approved_amount " & _
                         ",lnloan_advance_tran.loan_statusunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If

            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))
            Dim dsEmployee As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Rows.Count > 0 Then

                Dim objApproval As New clsloanapproval_process_Tran
                For Each dr In dsEmployee.Tables(0).Rows
                    Dim dsLoanList As DataSet = objApproval.GetApprovalTranList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtVoucherFromDate _
                                                                                                           , mdtVouncherToDate, xUserModeSetting, xOnlyApproved, False, "List", CInt(dr("employeeunkid")) _
                                                                                                           , CInt(dr("processpendingloanunkid")))

                    dsLoanList.Tables(0).DefaultView.Sort = "priority DESC"
                    Dim dtTable As DataTable = dsLoanList.Tables(0).DefaultView.ToTable()

                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        dr("other_remark") = dtTable.Rows(0)("remark")
                    End If

                    dr("application_date") = CDate(dr("application_date")).ToShortDateString()

                Next
                dsEmployee.Tables(0).AcceptChanges()
            End If


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell
            Dim strBuilder As New StringBuilder
            Dim mdtTableExcel As DataTable = dsEmployee.Tables(0)

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("processpendingloanunkid") Then
                mdtTableExcel.Columns.Remove("processpendingloanunkid")
            End If

            If mdtTableExcel.Columns.Contains("loanadvancetranunkid") Then
                mdtTableExcel.Columns.Remove("loanadvancetranunkid")
            End If

            If mdtTableExcel.Columns.Contains("loan_statusunkid") Then
                mdtTableExcel.Columns.Remove("loan_statusunkid")
            End If

            If mdtTableExcel.Columns.Contains("Id") Then
                mdtTableExcel.Columns.Remove("Id")
            End If


            mdtTableExcel.Columns("application_no").Caption = Language.getMessage(mstrModuleName, 101, "Form No")

            mdtTableExcel.Columns("Employee").SetOrdinal(mdtTableExcel.Columns("application_no").Ordinal + 1)
            mdtTableExcel.Columns("Employee").Caption = Language.getMessage(mstrModuleName, 102, "Name of Worker")

            mdtTableExcel.Columns("GName").SetOrdinal(mdtTableExcel.Columns("Employee").Ordinal + 1)

            mdtTableExcel.Columns("Section").SetOrdinal(mdtTableExcel.Columns("GName").Ordinal + 1)
            mdtTableExcel.Columns("Section").Caption = Language.getMessage(mstrModuleName, 103, "Contractor")

            mdtTableExcel.Columns("Employeecode").SetOrdinal(mdtTableExcel.Columns("Section").Ordinal + 1)
            mdtTableExcel.Columns("Employeecode").Caption = Language.getMessage(mstrModuleName, 104, "Employee Code")

            mdtTableExcel.Columns("Department").SetOrdinal(mdtTableExcel.Columns("Employeecode").Ordinal + 1)
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 105, "Department")

            mdtTableExcel.Columns("SectionGrp").SetOrdinal(mdtTableExcel.Columns("Department").Ordinal + 1)
            mdtTableExcel.Columns("SectionGrp").Caption = Language.getMessage(mstrModuleName, 106, "Supervisor")

            mdtTableExcel.Columns("application_date").SetOrdinal(mdtTableExcel.Columns("SectionGrp").Ordinal + 1)
            mdtTableExcel.Columns("application_date").Caption = Language.getMessage(mstrModuleName, 107, "Pmt Voucher Date")

            mdtTableExcel.Columns("PaidAmt").SetOrdinal(mdtTableExcel.Columns("application_date").Ordinal + 1)
            mdtTableExcel.Columns("PaidAmt").Caption = Language.getMessage(mstrModuleName, 108, "Amt Paid")

            mdtTableExcel.Columns("receipt").SetOrdinal(mdtTableExcel.Columns("PaidAmt").Ordinal + 1)
            mdtTableExcel.Columns("receipt").Caption = Language.getMessage(mstrModuleName, 109, "Receipt Amt")

            mdtTableExcel.Columns("balance").SetOrdinal(mdtTableExcel.Columns("receipt").Ordinal + 1)
            mdtTableExcel.Columns("balance").Caption = Language.getMessage(mstrModuleName, 110, "Balance")

            mdtTableExcel.Columns("emp_remark").SetOrdinal(mdtTableExcel.Columns("balance").Ordinal + 1)
            mdtTableExcel.Columns("emp_remark").Caption = Language.getMessage(mstrModuleName, 111, "Excuse Duty Remarks")

            mdtTableExcel.Columns("other_remark").SetOrdinal(mdtTableExcel.Columns("emp_remark").Ordinal + 1)
            mdtTableExcel.Columns("other_remark").Caption = Language.getMessage(mstrModuleName, 112, "Other Remarks")

            mdtTableExcel.Columns("Memo").SetOrdinal(mdtTableExcel.Columns("other_remark").Ordinal + 1)
            mdtTableExcel.Columns("Memo").Caption = Language.getMessage(mstrModuleName, 113, "Memo")


            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                'Dim strGrpCols As String() = {"GName", "application_no"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
                'Dim strGrpCols As String() = {"application_no"}
                'strarrGroupColumns = strGrpCols
            End If

            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee:")
            Language.setMessage(mstrModuleName, 2, "Advance Voucher No:")
            Language.setMessage(mstrModuleName, 3, "Voucher From Date:")
            Language.setMessage(mstrModuleName, 4, "Voucher To Date:")
            Language.setMessage(mstrModuleName, 5, "Memo Status:")
            Language.setMessage("clsMasterData", 96, "In Progress")
            Language.setMessage("clsMasterData", 97, "On Hold")
            Language.setMessage("clsMasterData", 100, "Completed")
            Language.setMessage(mstrModuleName, 101, "Form No")
            Language.setMessage(mstrModuleName, 102, "Name of Worker")
            Language.setMessage(mstrModuleName, 103, "Contractor")
            Language.setMessage(mstrModuleName, 104, "Employee Code")
            Language.setMessage(mstrModuleName, 105, "Department")
            Language.setMessage(mstrModuleName, 106, "Supervisor")
            Language.setMessage(mstrModuleName, 107, "Pmt Voucher Date")
            Language.setMessage(mstrModuleName, 108, "Amt Paid")
            Language.setMessage(mstrModuleName, 109, "Receipt Amt")
            Language.setMessage(mstrModuleName, 110, "Balance")
            Language.setMessage(mstrModuleName, 111, "Excuse Duty Remarks")
            Language.setMessage(mstrModuleName, 112, "Other Remarks")
            Language.setMessage(mstrModuleName, 113, "Memo")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
