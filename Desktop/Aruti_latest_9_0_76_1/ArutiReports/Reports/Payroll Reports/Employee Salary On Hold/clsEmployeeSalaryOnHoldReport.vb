'************************************************************************************************************************************
'Class Name : clsOverDeduction_NetPay_Report.vb
'Purpose    :
'Date       : 05/12/2014
'Written By : Suhail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Suhail
''' </summary>
Public Class clsEmployeeSalaryOnHoldReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeSalaryOnHoldReport"
    Private mstrReportId As String = enArutiReport.Employee_Salary_On_Hold_Report
    Dim objDataOperation As clsDataOperation
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mstrFilter As String = ""
    Private mstrOrderByQuery As String = ""

    Private mblnIsActive As Boolean = True
    Private mstrUserAccessFilter As String = ""
    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
#End Region

#Region " Properties "

    Public WriteOnly Property _Employee_Name() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Employee_Id() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _Period_Name() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Period_Id() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property



    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeName = ""
            mintEmployeeId = 0
            mstrPeriodName = ""
            mintPeriodId = 0
            mstrFilter = ""
            mstrOrderByQuery = ""
            mblnIsActive = True

            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""
        Try
            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period :") & " " & mstrPeriodName & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Order By :") & " " & Me.OrderByDisplay & " "
                If mintViewIndex > 0 Then
                    mstrOrderByQuery &= "ORDER BY GNAME, " & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell


        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intGroupColumn As Integer = 0

        '            If mintViewIndex > 0 Then
        '                intGroupColumn = mdtTableExcel.Columns.Count
        '                Dim strGrpCols As String() = {"column4"}
        '                strarrGroupColumns = strGrpCols

        '            End If


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If


        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", , , True, rowsArrayHeader, rowsArrayFooter, , , True)

        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim strarrGroupColumns As String() = Nothing
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell


                If mdtTableExcel IsNot Nothing Then

                    Dim intGroupColumn As Integer = 0

                    If mintViewIndex > 0 Then
                        intGroupColumn = mdtTableExcel.Columns.Count
                        Dim strGrpCols As String() = {"column4"}
                        strarrGroupColumns = strGrpCols

                    End If


                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", "", , , True, rowsArrayHeader, rowsArrayFooter, , , True)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:generateReportNew ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Reports Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Public Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 3, "Employee Code")))
            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            'If mblnFirstNamethenSurname = True Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            'End If
            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            'Sohail (01 Sep 2021) -- End
            iColumn_DetailReport.Add(New IColumn("balanceamount", Language.getMessage(mstrModuleName, 5, "Amount")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim strQ As String = String.Empty
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0

    '        objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal

    '        If mstrUserAccessFilter.Trim = "" Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        strQ = "SELECT  prtnaleave_tran.employeeunkid  " & _
    '                      ", employeecode " & _
    '                      ", payperiodunkid " & _
    '                      ", period_code " & _
    '                      ", period_name " & _
    '                      ", CAST(balanceamount AS DECIMAL(36, " & decDecimalPlaces & ")) AS balanceamount "

    '        If mblnFirstNamethenSurname = True Then
    '            strQ &= ", REPLACE(ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, ''), '  ', ' ') AS employeename  "
    '        Else
    '            strQ &= ", REPLACE(ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, ''), '  ', ' ') AS employeename  "
    '        End If

    '        If mintViewIndex > 0 Then
    '            strQ &= mstrAnalysis_Fields
    '        Else
    '            strQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        strQ &= "FROM    prtnaleave_tran " & _
    '                        "LEFT JOIN prpayment_tran ON tnaleavetranunkid = referencetranunkid " & _
    '                                                    "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
    '                                                    "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
    '                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

    '        strQ &= mstrAnalysis_Join

    '        strQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
    '                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
    '                        "AND cfcommon_period_tran.isactive = 1 " & _
    '                        "AND modulerefid = " & enModuleReference.Payroll & " " & _
    '                        "AND payperiodunkid = @payperiodunkid " & _
    '                        "AND prpayment_tran.paymenttranunkid IS NULL "

    '        If mintEmployeeId > 0 Then
    '            strQ &= "AND prtnaleave_tran.employeeunkid = @employeeunkid "
    '        End If

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIsActive = False Then
    '            strQ &= "                    AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                             "AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                             "AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                             "AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If mstrUserAccessFilter.Length > 0 Then
    '            strQ &= mstrUserAccessFilter
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        strQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(strQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim strPrevGroup As String = ""
    '        Dim decTotal As Decimal = 0
    '        Dim decAnalysisTotal As Decimal = 0
    '        For Each dtRow As DataRow In dsList.Tables(0).Rows

    '            Dim rpt_Rows As DataRow
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("employeecode")
    '            rpt_Rows.Item("Column2") = dtRow.Item("employeename")
    '            rpt_Rows.Item("Column3") = Format(CDec(dtRow.Item("balanceamount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column81") = CDec(Format(CDec(dtRow.Item("balanceamount")), GUI.fmtCurrency))
    '            rpt_Rows.Item("Column4") = dtRow.Item("GName")

    '            If mintViewIndex > 0 Then
    '                If strPrevGroup <> dtRow.Item("GName").ToString Then
    '                    decAnalysisTotal = CDec(Format(dtRow("balanceamount"), mstrfmtCurrency))
    '                Else
    '                    decAnalysisTotal += CDec(Format(dtRow("balanceamount"), mstrfmtCurrency))
    '                End If
    '                strPrevGroup = dtRow.Item("GName").ToString
    '            End If
    '            decTotal += CDec(Format(dtRow("balanceamount"), mstrfmtCurrency))

    '            rpt_Rows.Item("Column5") = Format(decAnalysisTotal, mstrfmtCurrency)
    '            rpt_Rows.Item("Column6") = Format(decTotal, mstrfmtCurrency)

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

    '        Next

    '        objRpt = New ArutiReport.Designer.rptEmployeeSalaryOnHoldReport

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex <= 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GHAnalysisBy", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GFAnalysisBy", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 3, "Employee Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 5, "Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 10, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 11, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)

    '            Dim mintColumn As Integer = 0

    '            mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 3, "Employee Code")
    '            mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 4, "Employee Name")
    '            mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 5, "Amount")
    '            mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
    '            mintColumn += 1

    '            If mintViewIndex > 0 Then
    '                mdtTableExcel.Columns("Column4").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
    '                mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
    '                mintColumn += 1
    '            End If


    '            For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                mdtTableExcel.Columns.RemoveAt(mintColumn)
    '            Next


    '        End If


    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim strQ As String = String.Empty
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodStartDate, mdtPeriodEndDate, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, strDatabaseName)


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            strQ = "SELECT   hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode  "

            If mblnFirstNamethenSurname = True Then
                strQ &= ", REPLACE(ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, ''), '  ', ' ') AS employeename  "
            Else
                strQ &= ", REPLACE(ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, ''), '  ', ' ') AS employeename  "
            End If

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", 0 AS Id, '' AS GName "
            End If

            strQ &= "INTO #TableEmp " & _
                     "FROM hremployee_master "

            strQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    strQ &= " AND " & xUACFiltrQry
            'End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter & " "
            End If
            'Sohail (01 Sep 2021) -- End

            strQ &= "SELECT  prtnaleave_tran.employeeunkid  " & _
                          ", employeecode " & _
                          ", payperiodunkid " & _
                          ", period_code " & _
                          ", period_name " & _
                          ", CAST(balanceamount AS DECIMAL(36, " & decDecimalPlaces & ")) AS balanceamount "

            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            'If mblnFirstNamethenSurname = True Then
            '    strQ &= ", REPLACE(ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, ''), '  ', ' ') AS employeename  "
            'Else
            '    strQ &= ", REPLACE(ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, ''), '  ', ' ') AS employeename  "
            'End If
            strQ &= ", employeename "
            'Sohail (01 Sep 2021) -- End

            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            'If mintViewIndex > 0 Then
            '    strQ &= mstrAnalysis_Fields
            'Else
            '    strQ &= ", 0 AS Id, '' AS GName "
            'End If
            strQ &= ", #TableEmp.Id, #TableEmp.GName "
            'Sohail (01 Sep 2021) -- End

            strQ &= "FROM    prtnaleave_tran " & _
                            "JOIN #TableEmp ON #TableEmp.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            "LEFT JOIN prpayment_tran ON tnaleavetranunkid = referencetranunkid " & _
                                                        "AND referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                        "AND paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "
            'Sohail (01 Sep 2021) - [LEFT JOIN hremployee_master]=[JOIN #TableEmp]

            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            'strQ &= mstrAnalysis_Join

            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END
            'Sohail (01 Sep 2021) -- End

            strQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND modulerefid = " & enModuleReference.Payroll & " " & _
                            "AND payperiodunkid = @payperiodunkid " & _
                            "AND prpayment_tran.paymenttranunkid IS NULL "

            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            'If mintEmployeeId > 0 Then
            '    strQ &= "AND prtnaleave_tran.employeeunkid = @employeeunkid "
            'End If

            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry & " "
            '    End If
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACFiltrQry.Trim.Length > 0 Then
            ''    strQ &= " AND " & xUACFiltrQry & " "
            ''End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If
            'Sohail (01 Sep 2021) -- End

            Call FilterTitleAndFilterQuery()

            strQ &= mstrOrderByQuery

            'Sohail (01 Sep 2021) -- Start
            'Sheer Logic Issue : AH-3877 : Salary on Hold Report takes more than 10 minutes to generate.
            strQ &= " DROP TABLE #TableEmp "
            'Sohail (01 Sep 2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim strPrevGroup As String = ""
            Dim decTotal As Decimal = 0
            Dim decAnalysisTotal As Decimal = 0
            For Each dtRow As DataRow In dsList.Tables(0).Rows

                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = Format(CDec(dtRow.Item("balanceamount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column81") = CDec(Format(CDec(dtRow.Item("balanceamount")), GUI.fmtCurrency))
                rpt_Rows.Item("Column4") = dtRow.Item("GName")

                If mintViewIndex > 0 Then
                    If strPrevGroup <> dtRow.Item("GName").ToString Then
                        decAnalysisTotal = CDec(Format(dtRow("balanceamount"), mstrfmtCurrency))
                    Else
                        decAnalysisTotal += CDec(Format(dtRow("balanceamount"), mstrfmtCurrency))
                    End If
                    strPrevGroup = dtRow.Item("GName").ToString
                End If
                decTotal += CDec(Format(dtRow("balanceamount"), mstrfmtCurrency))

                rpt_Rows.Item("Column5") = Format(decAnalysisTotal, mstrfmtCurrency)
                rpt_Rows.Item("Column6") = Format(decTotal, mstrfmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            objRpt = New ArutiReport.Designer.rptEmployeeSalaryOnHoldReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GHAnalysisBy", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GFAnalysisBy", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 6, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 7, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 8, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 9, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 3, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 5, "Amount"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 10, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 11, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 3, "Employee Code")
                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 4, "Employee Name")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 5, "Amount")
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column4").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column4").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next


            End If


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period :")
            Language.setMessage(mstrModuleName, 2, "Employee :")
            Language.setMessage(mstrModuleName, 3, "Employee Code")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Amount")
            Language.setMessage(mstrModuleName, 6, "Prepared By :")
            Language.setMessage(mstrModuleName, 7, "Checked By :")
            Language.setMessage(mstrModuleName, 8, "Approved By :")
            Language.setMessage(mstrModuleName, 9, "Received By :")
            Language.setMessage(mstrModuleName, 10, "Sub Total :")
            Language.setMessage(mstrModuleName, 11, "Grand Total :")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "Order By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
