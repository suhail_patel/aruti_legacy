'************************************************************************************************************************************
'Class Name : clsAct_TimesheetReport.vb
'Purpose    :
'Date       :30-Jul-2013
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsAct_TimesheetReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAct_TimesheetReport"
    Private mstrReportId As String = enArutiReport.ActivityTimesheetReport   '120
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mdtDate1 As Date = Nothing
    Private mdtDate2 As Date = Nothing
    Private mstrEmployeeIds As String = String.Empty
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnIsSummaryIncluded As Boolean = False
    Private mblnIsSummaryByActivity As Boolean = False
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Properties "

    Public WriteOnly Property _Date1() As Date
        Set(ByVal value As Date)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As Date
        Set(ByVal value As Date)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeIds() As String
        Set(ByVal value As String)
            mstrEmployeeIds = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _IsSummaryIncluded() As Boolean
        Set(ByVal value As Boolean)
            mblnIsSummaryIncluded = value
        End Set
    End Property

    Public WriteOnly Property _IsSummaryByActivity() As Boolean
        Set(ByVal value As Boolean)
            mblnIsSummaryByActivity = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mstrEmployeeIds = String.Empty
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mblnIsSummaryIncluded = False
            mblnIsSummaryByActivity = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Try
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 100, "Date From : ") & mdtDate1.Date & " " & _
                               Language.getMessage(mstrModuleName, 101, "to ") & mdtDate2.Date

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt


        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False, True)

            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                          , ByVal xUserUnkid As Integer _
                                          , ByVal xYearUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsMst As New DataSet
        Dim dsTrn As New DataSet
        Dim dsSummary As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            StrQ = "WITH MYCTE AS " & _
                     "(   SELECT CAST('" & eZeeDate.convertDate(mdtDate1) & "' AS DATETIME) DateValue " & _
                     "UNION ALL " & _
                        "SELECT  DateValue + 1 FROM MYCTE WHERE   DateValue + 1 <= '" & eZeeDate.convertDate(mdtDate2) & "' " & _
                     ")SELECT " & _
                     "     CONVERT(CHAR(8),DateValue,112) AS oDate " & _
                     "FROM MYCTE " & _
                     "OPTION (MAXRECURSION 0) "

            dsMst = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT " & _
                   "  ISNULL(CONVERT(CHAR(8),activity_date,112),'') AS iDate " & _
                   " ,employeecode AS ECode "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            ' " ,firstname+' '+surname AS EName " "
            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= " ,ISNULL(ISNULL(practivity_master.code,practivity_master.name) + ' / ' + CAST(CONVERT(DECIMAL(10,2),activity_value) AS NVARCHAR(MAX)),'') AS iAct_Done " & _
                   " ,hremployee_master.employeeunkid  " & _
                   " , activity_value AS aValue " & _
                   " ,ISNULL(practivity_master.code,practivity_master.name) iActivity " & _
                   "FROM hremployee_master " & _
                   " LEFT JOIN prpayactivity_tran ON hremployee_master.employeeunkid = prpayactivity_tran.employeeunkid AND isvoid = 0 " & _
                   " LEFT JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                   " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate1))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate2))

            dsTrn = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Dim dtTab As DataTable = Nothing
            Dim iDiciTotalName As New Dictionary(Of String, String)
            Dim iDiciTotal As New Dictionary(Of String, String)
            If mblnIsSummaryIncluded = True Then
                StrQ = "SELECT "
                'S.SANDEEP [ 26 MAR 2014 ] -- START
                ' " employeecode + ' - ' + (firstname+' '+surname) AS iCode "
                If mblnFirstNamethenSurname = True Then
                    StrQ &= "	 employeecode + ' - ' + (firstname+' '+surname) AS iCode "
                Else
                    StrQ &= "	 employeecode + ' - ' + (surname+' '+firstname) AS iCode "
                End If
                'S.SANDEEP [ 26 MAR 2014 ] -- END

                StrQ &= "    ,hremployee_master.employeeunkid "
                If mblnIsSummaryByActivity Then
                    StrQ &= "	,ISNULL(iActivity,'') AS iTotal " & _
                            "	,ISNULL(CAST(aValue AS DECIMAL(10,2)),0) AS aValue " & _
                            "   ,ISNULL(UoM,'') AS UoM "
                Else
                    StrQ &= "	,ISNULL(iUoM,'') AS iTotal " & _
                            "	,ISNULL(CAST(uValue AS DECIMAL(10,2)),0) AS aValue "
                End If
                StrQ &= "FROM hremployee_master "
                If mblnIsSummaryByActivity Then
                    StrQ &= "JOIN " & _
                            "( " & _
                            "	SELECT employeeunkid AS iAEmpId " & _
                            "		  ,practivity_master.code AS iActivity " & _
                            "		  ,SUM(activity_value) AS aValue " & _
                            "         ,prunitmeasure_master.name AS UoM " & _
                            "	FROM prpayactivity_tran " & _
                            "		JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
                            "       JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
                            "	WHERE isvoid = 0 AND CONVERT(CHAR(8),activity_date,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' " & _
                            "	GROUP BY employeeunkid,practivity_master.code,prunitmeasure_master.name " & _
                            ") AS Act ON Act.iAEmpId = hremployee_master.employeeunkid "
                Else
                    StrQ &= "JOIN " & _
                            "( " & _
                            "	SELECT employeeunkid AS iUEmpId " & _
                            "		  ,prunitmeasure_master.code AS iUoM " & _
                            "		  ,SUM(activity_value) AS uValue " & _
                            "	FROM prpayactivity_tran " & _
                            "		JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
                            "		JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
                            "	WHERE isvoid = 0 AND CONVERT(CHAR(8),activity_date,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' " & _
                            "	GROUP BY employeeunkid,prunitmeasure_master.code " & _
                            ") AS UoM ON UoM.iUEmpId = hremployee_master.employeeunkid "
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE 1 = 1 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                dsSummary = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If
                dtTab = New DataView(dsSummary.Tables("List"), "employeeunkid IN(" & mstrEmployeeIds & ")", "", DataViewRowState.CurrentRows).ToTable
                For Each dSumm As DataRow In dtTab.Rows
                    If iDiciTotalName.ContainsKey(dSumm.Item("iCode")) = False Then
                        iDiciTotalName.Add(dSumm.Item("iCode"), dSumm.Item("iTotal") & vbCrLf)
                    Else
                        iDiciTotalName(dSumm.Item("iCode")) &= dSumm.Item("iTotal") & vbCrLf
                    End If
                    If mblnIsSummaryByActivity Then
                        If iDiciTotal.ContainsKey(dSumm.Item("iCode")) = False Then
                            iDiciTotal.Add(dSumm.Item("iCode"), dSumm.Item("aValue") & Space(2) & dSumm.Item("UoM") & vbCrLf)
                        Else
                            iDiciTotal(dSumm.Item("iCode")) &= dSumm.Item("aValue") & Space(2) & dSumm.Item("UoM") & vbCrLf
                        End If
                    Else
                        If iDiciTotal.ContainsKey(dSumm.Item("iCode")) = False Then
                            iDiciTotal.Add(dSumm.Item("iCode"), dSumm.Item("aValue") & vbCrLf)
                        Else
                            iDiciTotal(dSumm.Item("iCode")) &= dSumm.Item("aValue") & vbCrLf
                        End If
                    End If
                Next
            End If

            Dim dtFinalTab As DataTable = Nothing
            dtFinalTab = New DataView(dsTrn.Tables("List"), "employeeunkid IN(" & mstrEmployeeIds & ")", "EName", DataViewRowState.CurrentRows).ToTable

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCounter As Integer = 2 : Dim rpt_Rows As DataRow = Nothing : Dim sEDetails As String = "" : Dim sCode As String = ""

            Call FilterTitleAndFilterQuery()

            For Each eRow As DataRow In dtFinalTab.Rows
                If sEDetails <> eRow.Item("ECode").ToString & " - " & eRow.Item("EName") Then
                    sEDetails = eRow.Item("ECode").ToString & " - " & eRow.Item("EName")
                    iCounter = 2 : sCode = eRow.Item("ECode").ToString
                Else
                    Continue For
                End If
                For Each dRow As DataRow In dsMst.Tables("List").Rows
                    If iCounter = 2 Then rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    rpt_Rows.Item("Column1") = sEDetails
                    If mblnIsSummaryIncluded = True Then
                        If iDiciTotalName.ContainsKey(sEDetails) = True Then
                            rpt_Rows.Item("Column10") = iDiciTotalName(sEDetails)
                        Else
                            rpt_Rows.Item("Column10") = ""
                        End If
                        If iDiciTotal.ContainsKey(sEDetails) = True Then
                            rpt_Rows.Item("Column11") = iDiciTotal(sEDetails)
                        Else
                            rpt_Rows.Item("Column11") = ""
                        End If
                    End If

                    Dim dtmp() As DataRow = dtFinalTab.Select("ECode='" & sCode & "' AND iDate = '" & dRow.Item("oDate") & "'")
                    rpt_Rows.Item("Column" & iCounter.ToString) = eZeeDate.convertDate(dRow.Item("oDate").ToString).ToShortDateString

                    'Pinkal (23-Sep-2014) -- Start
                    'Enhancement -  PAY_A CHANGES IN PPA
                    rpt_Rows.Item("Column1" & iCounter) &= WeekdayName(Weekday(eZeeDate.convertDate(dRow.Item("oDate").ToString)), False, FirstDayOfWeek.Sunday) & vbCrLf
                    'Pinkal (23-Sep-2014) -- End

                    If dtmp.Length > 0 Then
                        For iRow As Integer = 0 To dtmp.Length - 1
                            rpt_Rows.Item("Column7" & iCounter) &= iRow + 1 & ". " & dtmp(iRow).Item("iAct_Done") & vbCrLf
                        Next
                        iCounter += 1
                    Else
                        iCounter += 1
                    End If
                    If iCounter > 8 Then
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                        iCounter = 2
                    End If
                Next
                If iCounter <> 2 Then
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows) : iCounter = 2
                End If
            Next

            objRpt = New ArutiReport.Designer.rptAct_TimesheetReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription" _
                                        )

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmp", Language.getMessage(mstrModuleName, 102, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 103, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 104, "Printed Date :"))

            If mblnIsSummaryIncluded = True Then
                If mblnIsSummaryByActivity = True Then
                    Call ReportFunction.TextChange(objRpt, "txtSummary", Language.getMessage(mstrModuleName, 105, "Summary - By Activity"))
                Else
                    Call ReportFunction.TextChange(objRpt, "txtSummary", Language.getMessage(mstrModuleName, 106, "Summary - By UoM"))
                End If
            Else
                Call ReportFunction.TextChange(objRpt, "txtSummary", "")
            End If

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Date From :")
            Language.setMessage(mstrModuleName, 101, "to")
            Language.setMessage(mstrModuleName, 102, "Employee :")
            Language.setMessage(mstrModuleName, 103, "Printed By :")
            Language.setMessage(mstrModuleName, 104, "Printed Date :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
