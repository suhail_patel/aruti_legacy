'************************************************************************************************************************************
'Class Name : clsPayementVoucherReport.vb
'Purpose    :
'Date       : 23/02/2018
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

Public Class clsPaymentVoucherReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPaymentVoucherReport"
    Private mstrReportId As String = enArutiReport.B5_Payment_Voucher_Report  '203
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mstrAdvanceVoucherNo As String = ""
    Private mdtVoucherFromDate As Date = Nothing
    Private mdtVouncherToDate As Date = Nothing
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintUserUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrOrderByQuery As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _AdvanceVoucherNo() As String
        Set(ByVal value As String)
            mstrAdvanceVoucherNo = value
        End Set
    End Property

    Public WriteOnly Property _VoucherFromDate() As Date
        Set(ByVal value As Date)
            mdtVoucherFromDate = value
        End Set
    End Property

    Public WriteOnly Property _VouncherToDate() As Date
        Set(ByVal value As Date)
            mdtVouncherToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrAdvanceVoucherNo = ""
            mdtVoucherFromDate = Nothing
            mdtVouncherToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrOrderByQuery = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrOrderByQuery = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrAnalysis_OrderBy = ""
            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mstrAdvanceVoucherNo.Trim.Length > 0 Then
                Me._FilterQuery &= " AND lnloan_process_pending_loan.application_no = @application_no "
                objDataOperation.AddParameter("@application_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdvanceVoucherNo)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Advance Voucher No: ") & " " & mstrEmployeeName & " "
            End If

            If mdtVoucherFromDate <> Nothing AndAlso mdtVouncherToDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112) >= @FromDate  AND  CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112) < = @ToDate "
                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtVoucherFromDate))
                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtVouncherToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Voucher From Date: ") & " " & mdtVoucherFromDate.Date & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Voucher To Date: ") & " " & mdtVouncherToDate.Date & " "
            End If

            'If mintViewIndex > 0 Then
            'mstrOrderByQuery &= " ORDER BY " & mstrAnalysis_OrderBy_GName & ",lnloan_process_pending_loan.application_no"
            'Else
            mstrOrderByQuery &= " ORDER BY lnloan_process_pending_loan.application_no "
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If xCompanyUnkid <= 0 Then
                xCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, ByVal mdtFromDate As Date, ByVal mdtToDate As Date, ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If xUserUnkid <= 0 Then
                xUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = xUserUnkid


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtFromDate, mdtToDate, True, False, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtToDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtToDate, xDatabaseName)

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()



            ' ONLY SHOW ASSIGNED ADVANCE DATA.


            StrQ = " SELECT " & _
                       " lnloan_process_pending_loan.processpendingloanunkid " & _
                      ", lnloan_process_pending_loan.application_no " & _
                      ", lnloan_process_pending_loan.application_date " & _
                      ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      ", Dept.name AS Department " & _
                      ", SecGrp.name AS SectionGrp " & _
                      ", Sec.name AS Section " & _
                      ", ISNULL(lnloan_process_pending_loan.approved_amount,0.00) AS AmtPaid " & _
                      ", lnloan_process_pending_loan.emp_remark "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM lnloan_process_pending_loan " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                         " JOIN (SELECT " & _
                         "            departmentunkid " & _
                         "           ,sectionunkid " & _
                         "           ,sectiongroupunkid " & _
                         "           ,employeeunkid " & _
                         "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate) AS rno " & _
                         "         FROM hremployee_transfer_tran " & _
                         "         WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @ToDate) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master AS Dept ON Alloc.departmentunkid = Dept.departmentunkid " & _
                         " LEFT JOIN hrsection_master AS Sec ON Alloc.sectionunkid = Sec.sectionunkid " & _
                         " LEFT JOIN hrsectiongroup_master AS SecGrp ON Alloc.departmentunkid = SecGrp.sectiongroupunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "  WHERE lnloan_process_pending_loan.isvoid = 0  And lnloan_process_pending_loan.isloan = 0 AND lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.ASSIGNED

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            Dim dsEmployee As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim objDashboard As New clsDashboard_Class

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsEmployee.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("application_no").ToString()
                rpt_Row.Item("Column10") = CDate(dtRow.Item("application_date")).ToShortDateString()
                rpt_Row.Item("Column2") = dtRow.Item("Employee").ToString()
                rpt_Row.Item("Column3") = dtRow.Item("EmployeeCode").ToString()
                rpt_Row.Item("Column4") = dtRow.Item("Department").ToString()
                rpt_Row.Item("Column5") = dtRow.Item("SectionGrp").ToString()
                rpt_Row.Item("Column6") = dtRow.Item("Section").ToString()
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("AmtPaid").ToString()), GUI.fmtCurrency)

                Dim strNumToWords As String = objDashboard.NumToWord(CInt(dtRow.Item("AmtPaid")))
                Dim sword As String = dtRow.Item("AmtPaid").ToString().Substring(InStr(dtRow.Item("AmtPaid").ToString(), "."))
                If CInt(sword) > 0 Then
                    strNumToWords += " AND " & sword & " / 1" & New String("0", sword.Length)
                End If

                rpt_Row.Item("Column8") = strNumToWords
                rpt_Row.Item("Column9") = dtRow.Item("emp_remark").ToString()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objDashboard = Nothing


            objRpt = New ArutiReport.Designer.rptB5PaymentVoucher

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtVoucherReceipt", Language.getMessage(mstrModuleName, 1, "ACCIDENTAL PAYMENT VOUCHER AGAINST RECEIPT"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 2, "DATE :"))
            Call ReportFunction.TextChange(objRpt, "txtFNO", Language.getMessage(mstrModuleName, 3, "F.NO :"))
            Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 4, "NAME :"))
            Call ReportFunction.TextChange(objRpt, "txtIDNo", Language.getMessage(mstrModuleName, 5, "ID NO :"))
            Call ReportFunction.TextChange(objRpt, "txtDept", Language.getMessage(mstrModuleName, 6, "DEPT :"))
            Call ReportFunction.TextChange(objRpt, "txtSuperVisor", Language.getMessage(mstrModuleName, 7, "SUPERVISOR :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployer", Language.getMessage(mstrModuleName, 8, "EMPLOYER :"))
            Call ReportFunction.TextChange(objRpt, "txtAmountPaid", Language.getMessage(mstrModuleName, 9, "AMOUNT PAID :"))
            Call ReportFunction.TextChange(objRpt, "txtInwords", Language.getMessage(mstrModuleName, 10, "IN WORDS :"))
            Call ReportFunction.TextChange(objRpt, "txtAccReason", Language.getMessage(mstrModuleName, 11, "REASON OF ACCIDENT :"))
            Call ReportFunction.TextChange(objRpt, "txtMPSign", Language.getMessage(mstrModuleName, 12, "MP SIGN"))
            Call ReportFunction.TextChange(objRpt, "txtHRSign", Language.getMessage(mstrModuleName, 13, "HR SIGN"))


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 15, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ACCIDENTAL PAYMENT VOUCHER AGAINST RECEIPT")
            Language.setMessage(mstrModuleName, 2, "DATE :")
            Language.setMessage(mstrModuleName, 3, "F.NO :")
            Language.setMessage(mstrModuleName, 4, "NAME :")
            Language.setMessage(mstrModuleName, 5, "ID NO :")
            Language.setMessage(mstrModuleName, 6, "DEPT :")
            Language.setMessage(mstrModuleName, 7, "SUPERVISOR :")
            Language.setMessage(mstrModuleName, 8, "EMPLOYER :")
            Language.setMessage(mstrModuleName, 9, "AMOUNT PAID :")
            Language.setMessage(mstrModuleName, 10, "IN WORDS :")
            Language.setMessage(mstrModuleName, 11, "REASON OF ACCIDENT :")
            Language.setMessage(mstrModuleName, 12, "MP SIGN")
            Language.setMessage(mstrModuleName, 13, "HR SIGN")
            Language.setMessage(mstrModuleName, 14, "Printed By :")
            Language.setMessage(mstrModuleName, 15, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, "Employee:")
            Language.setMessage(mstrModuleName, 17, "Advance Voucher No:")
            Language.setMessage(mstrModuleName, 18, "Voucher From Date:")
            Language.setMessage(mstrModuleName, 19, "Voucher To Date:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
