'Class Name : clsPayrollReconciliationTotalsReport.vb
'Purpose    :
'Date       :9/21/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Vimal M. Gohil
''' Last Language No : 16
''' </summary>
Public Class clsPayrollTotalVariance
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayrollTotalVariance"
    Private mstrReportId As String = enArutiReport.PayrollTotalVariance
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintPeriodId1 As Integer = 0
    Private mstrPeriodName1 As String = ""

    Private mintPeriodId2 As Integer = 0
    Private mstrPeriodName2 As String = ""

    'Sohail (20 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintTranHeadTypeId As Integer = 0
    Private mstrTranHeadTypeName As String = ""
    Private mdicTranHeadType As New Dictionary(Of Integer, String)
    'Sohail (20 Jun 2012) -- End

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    'Sohail (21 Jul 2012) -- End
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mintFromYearUnkId As Integer
    Private mintToYearUnkId As Integer
    'Sohail (21 Aug 2015) -- End

    Private mstrOrderByQuery As String = ""


    'Pinkal (15-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    'Pinkal (15-Jan-2013) -- End

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintBaseCurrencyId As Integer = 0
    'Pinkal (24-May-2013) -- End


    'Anjan (07 Jun 2018) -- Start
    'Internal Enhancement - Including Ignore Zero Variance setting.
    Private mblnIgnoreZeroVariance As Boolean = False
    'Anjan (04 Jun 2018) -- End

    'Hemant (22 Jan 2019) -- Start
    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
    Private mblnShowVariancePercentageColumns As Boolean = False
    'Hemant (22 Jan 2019) -- End
    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Private mdtFromPeriodStart As Date
    Private mdtFromPeriodEnd As Date
    Private mdtToPeriodStart As Date
    Private mdtToPeriodEnd As Date
    Private mstrInfoHeadIDs As String = String.Empty
    'Sohail (13 Jan 2020) -- End

#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId1() As Integer
        Set(ByVal value As Integer)
            mintPeriodId1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName1() As String
        Set(ByVal value As String)
            mstrPeriodName1 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId2() As Integer
        Set(ByVal value As Integer)
            mintPeriodId2 = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName2() As String
        Set(ByVal value As String)
            mstrPeriodName2 = value
        End Set
    End Property

    'Sohail (20 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _TranHeadTypeId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadTypeId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadTypeName() As String
        Set(ByVal value As String)
            mstrTranHeadTypeName = value
        End Set
    End Property

    Public WriteOnly Property _DicTranHeadType() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicTranHeadType = value
        End Set
    End Property
    'Sohail (20 Jun 2012) -- End

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ToDatabaseName() As String
        Set(ByVal value As String)
            mstrToDatabaseName = value
        End Set
    End Property
    'Sohail (21 Jul 2012) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public WriteOnly Property _FromYearUnkId() As Integer
        Set(ByVal value As Integer)
            mintFromYearUnkId = value
        End Set
    End Property

    Public WriteOnly Property _ToYearUnkId() As Integer
        Set(ByVal value As Integer)
            mintToYearUnkId = value
        End Set
    End Property
    'Sohail (21 Aug 2015) -- End


    'Pinkal (15-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Pinkal (15-Jan-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrencyId = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End


    'Anjan (07 Jun 2018) -- Start
    'Internal Enhancement - Including Ignore Zero Variance setting.
    Public WriteOnly Property _IgnoreZeroVariance() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZeroVariance = value
        End Set
    End Property
    'Anjan (04 Jun 2018) -- End

    'Hemant (22 Jan 2019) -- Start
    'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
    Public WriteOnly Property _ShowVariancePercentageColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnShowVariancePercentageColumns = value
        End Set
    End Property
    'Hemant (22 Jan 2019) -- End

    'Sohail (13 Jan 2020) -- Start
    'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
    Public WriteOnly Property _FromPeriodStart() As Date
        Set(ByVal value As Date)
            mdtFromPeriodStart = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodEnd() As Date
        Set(ByVal value As Date)
            mdtFromPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodStart() As Date
        Set(ByVal value As Date)
            mdtToPeriodStart = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodEnd() As Date
        Set(ByVal value As Date)
            mdtToPeriodEnd = value
        End Set
    End Property

    Public WriteOnly Property _InfoHeadIDs() As String
        Set(ByVal value As String)
            mstrInfoHeadIDs = value
        End Set
    End Property
    'Sohail (13 Jan 2020) -- End

    Private mblnIncludeActiveEmployee As Boolean = False
    Public WriteOnly Property _IncludeActiveEmployee() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeActiveEmployee = value
        End Set
    End Property
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintPeriodId1 = 0
            mstrPeriodName1 = ""

            mintPeriodId2 = 0
            mstrPeriodName2 = ""

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            mintTranHeadTypeId = 0
            mstrTranHeadTypeName = ""
            'Sohail (20 Jun 2012) -- End

            'Sohail (21 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            mstrFromDatabaseName = FinancialYear._Object._DatabaseName
            mstrToDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (21 Jul 2012) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mintFromYearUnkId = 0
            mintToYearUnkId = 0
            'Sohail (21 Aug 2015) -- End

            mstrOrderByQuery = ""

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            mdtFromPeriodStart = Nothing
            mdtFromPeriodEnd = Nothing
            mdtToPeriodStart = Nothing
            mdtToPeriodEnd = Nothing
            mstrInfoHeadIDs = ""
            'Sohail (13 Jan 2020) -- End

            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Pinkal (15-Jan-2013) -- End


            'Anjan (07 Jun 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting.
            mblnIgnoreZeroVariance = True
            'Anjan (04 Jun 2018) -- End

            mblnIncludeActiveEmployee = False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQurey()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = "" 'Sohail (20 Jun 2012)
        Try

            If mintPeriodId1 > 0 AndAlso mintPeriodId2 > 0 Then
                'Vimal (27 Nov 2010) -- Start 
                objDataOperation.AddParameter("@Period1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId1)
                objDataOperation.AddParameter("@Period2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId2)
                'Vimal (27 Nov 2010) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, " Period From : ") & " " & mstrPeriodName1 & " " & _
                                   Language.getMessage(mstrModuleName, 9, " To : ") & " " & mstrPeriodName2 & " "


                'Sandeep [ 25 MARCH 2011 ] -- Start
                'Issue : Period Opening Balance
                objDataOperation.AddParameter("@TrnCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Opening B/F"))
                objDataOperation.AddParameter("@TrnName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Opening B/F"))
                'Sandeep [ 25 MARCH 2011 ] -- End 

            End If

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            If mintTranHeadTypeId > 0 Then
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadTypeId)
            End If
            'Sohail (20 Jun 2012) -- End

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= ") AS a ORDER BY TranHeadTypeId,a.GName1," & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, " Order by : ") & " " & Me.OrderByDisplay
            Else
                mstrOrderByQuery &= ") AS a ORDER BY TranHeadTypeId,a.GName1 "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQurey; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (15-Jan-2013) -- Start
        '    'Enhancement : TRA Changes

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    'Pinkal (15-Jan-2013) -- End


        '    'Pinkal (24-May-2013) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'objRpt = Generate_DetailReport()

        '    'Pinkal (24-May-2013) -- End


        '    If Not IsNothing(objRpt) Then


        '        'Pinkal (15-Jan-2013) -- Start
        '        'Enhancement : TRA Changes


        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDic As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
        '        Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total")
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell

        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

        '            If mintViewIndex > 0 Then
        '                Dim strGrpCols As String() = {"column9", "column11"}
        '                strarrGroupColumns = strGrpCols
        '                intCurrencyColumn -= 1
        '            Else
        '                Dim strGrpCols As String() = {"column9"}
        '                strarrGroupColumns = strGrpCols
        '                intCurrencyColumn -= 1
        '            End If


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", "", Nothing, "", False, rowsArrayHeader, rowsArrayFooter, objDic, Nothing, False)

        '    End If

        '    'Pinkal (15-Jan-2013) -- End


        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions

        Try


            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid
            objConfig._Companyunkid = xCompanyUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'objRpt = Generate_DetailReport(mintUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, True, True, objConfig._CurrencyFormat, xBaseCurrencyId)
            objRpt = Generate_DetailReport(mintUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False, True, objConfig._CurrencyFormat, xBaseCurrencyId)
            'Sohail (13 Jan 2020) -- End
            Rpt = objRpt


            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 23, "Grand Total :")
                Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then

                    Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

                    If mintViewIndex > 0 Then
                        Dim strGrpCols As String() = {"column9", "column11"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    Else
                        Dim strGrpCols As String() = {"column9"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    End If


                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", "", Nothing, "", False, rowsArrayHeader, rowsArrayFooter, objDic, Nothing, False)

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property
    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Vimal (27 Nov 2010) -- Start 
            'iColumn_DetailReport.Add(New IColumn("TranCode", Language.getMessage(mstrModuleName, 1, "Tran Code")))
            'iColumn_DetailReport.Add(New IColumn("TranName", Language.getMessage(mstrModuleName, 2, "Tran Name")))
            'iColumn_DetailReport.Add(New IColumn("Period1", Language.getMessage(mstrModuleName, 3, "Period1")))
            'iColumn_DetailReport.Add(New IColumn("Amount1", Language.getMessage(mstrModuleName, 4, "Amount1")))
            'iColumn_DetailReport.Add(New IColumn("Period2", Language.getMessage(mstrModuleName, 5, "Period2")))
            'iColumn_DetailReport.Add(New IColumn("Amount2", Language.getMessage(mstrModuleName, 6, "Amount2")))
            'iColumn_DetailReport.Add(New IColumn("Variance", Language.getMessage(mstrModuleName, 7, "Variance")))
            'Vimal (27 Nov 2010) -- End

            'Sohail (20 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            iColumn_DetailReport.Add(New IColumn("TranCode", Language.getMessage(mstrModuleName, 1, "Tran Code")))
            iColumn_DetailReport.Add(New IColumn("TranName", Language.getMessage(mstrModuleName, 2, "Tran Name")))
            iColumn_DetailReport.Add(New IColumn("Amount1", Language.getMessage(mstrModuleName, 3, "Period_1 Amount")))
            iColumn_DetailReport.Add(New IColumn("Amount2", Language.getMessage(mstrModuleName, 4, "Period_2 Amount")))
            iColumn_DetailReport.Add(New IColumn("Variance", Language.getMessage(mstrModuleName, 5, "Variance")))
            'Sohail (20 Jun 2012) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    Private Function Generate_DetailReport(ByVal xUserUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strfmtCurrency As String _
                                           , ByVal intBase_CurrencyId As Integer _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xUserUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, intBase_CurrencyId]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            'Sohail (17 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            mintBaseCurrencyId = intBase_CurrencyId 'Sohail (21 Aug 2015)
            If mintBaseCurrencyId <= 0 Then
                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            End If
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'Pinkal (24-May-2013) -- End

            'Sohail (17 Aug 2012) -- End


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'StrQ = "SELECT " & _
            '            " CASE WHEN Table1.TranId = -1 THEN @TrnCode ELSE tabletranhead.trnheadcode END AS TranCode " & _
            '            ",CASE WHEN Table1.TranId = -1 THEN @TrnName ELSE tabletranhead.trnheadname END AS TranName " & _
            '              ", CASE WHEN Table1.TranId = -1 THEN 99999 ELSE tabletranhead.trnheadtype_id END AS TranHeadTypeId " & _
            '              ", ISNULL(Table1.PeriodName,'') AS Period1 " & _
            '              ", ISNULL(Table1.Amount, 0) AS Amount1 " & _
            '              ", ISNULL(Table2.PeriodName,'') AS Period2 " & _
            '              ", ISNULL(Table2.Amount, 0) AS Amount2 " & _
            '              ", ISNULL(Table2.Amount, 0) - ISNULL(Table1.Amount, 0) AS Variance " & _
            '        "FROM " & _
            '        "( " & _
            '            "SELECT " & _
            '                 "Periodid " & _
            '                          ", PeriodName " & _
            '                          ", TranId " & _
            '                          ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
            '            "FROM " & _
            '            "( " & _
            '                "SELECT " & _
            '                     "cfcommon_period_tran.periodunkid AS Periodid " & _
            '                                      ", cfcommon_period_tran.period_name AS PeriodName " & _
            '                                      ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
            '                                      ", prpayrollprocess_tran.amount AS Amount " & _
            '                              "FROM     " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
            '                                        "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                                        "JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
            '                                        "JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
            '                              "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                        "AND cfcommon_period_tran.isactive = 1 " & _
            '                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
            '                                        "AND cfcommon_period_tran.periodunkid = @Period1 "

            ''Sohail (20 Jun 2012) -- Start
            ''TRA - ENHANCEMENT
            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= " AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            'End If
            ''Sohail (20 Jun 2012) -- End

            'StrQ &= "UNION ALL " & _
            '               "SELECT " & _
            '                     "cfcommon_period_tran.periodunkid AS Periodid " & _
            '                    ",cfcommon_period_tran.period_name AS PeriodName " & _
            '                    ",-1 AS TranId " & _
            '                    ",prtnaleave_tran.openingbalance AS Amount " & _
            '               "FROM " & mstrFromDatabaseName & "..prtnaleave_tran " & _
            '                    "JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
            '                "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                    "AND cfcommon_period_tran.isactive = 1 " & _
            '                                        "AND cfcommon_period_tran.periodunkid = @Period1 " & _
            '                            ") AS TranTable " & _
            '            "GROUP BY  Periodid,PeriodName,TranId " & _
            '                ") AS Table1 " & _
            '        "FULL OUTER JOIN " & _
            '        "( " & _
            '            "SELECT " & _
            '                 "Periodid " & _
            '                                          ", PeriodName " & _
            '                                          ", TranId " & _
            '                                          ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
            '            "FROM " & _
            '            "( " & _
            '                "SELECT " & _
            '                     "cfcommon_period_tran.periodunkid AS Periodid " & _
            '                                                      ", cfcommon_period_tran.period_name AS PeriodName " & _
            '                                                      ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
            '                                                      ", prpayrollprocess_tran.amount AS Amount " & _
            '                                              "FROM     " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
            '                                                        "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
            '                                                        "JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
            '                                                        "JOIN " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
            '                "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
            '                    "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
            '                                                        "AND cfcommon_period_tran.isactive = 1 " & _
            '                                                        "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
            '                                                        "AND cfcommon_period_tran.periodunkid = @Period2 "

            ''Sohail (20 Jun 2012) -- Start
            ''TRA - ENHANCEMENT
            'If mintTranHeadTypeId > 0 Then
            '    StrQ &= " AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            'End If
            ''Sohail (20 Jun 2012) -- End

            'StrQ &= "UNION ALL " & _
            '                "SELECT " & _
            '                     "cfcommon_period_tran.periodunkid AS Periodid " & _
            '                    ",cfcommon_period_tran.period_name AS PeriodName " & _
            '                    ",-1 AS TranId " & _
            '                ",prtnaleave_tran.openingbalance AS Amount " & _
            '                "FROM " & mstrToDatabaseName & "..prtnaleave_tran " & _
            '                    "JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
            '                "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                    "AND cfcommon_period_tran.isactive = 1 " & _
            '                                                        "AND cfcommon_period_tran.periodunkid = @Period2 " & _
            '                                            ") AS TranTable " & _
            '            "GROUP BY  Periodid,PeriodName,TranId " & _
            '                                ") AS Table2 ON Table1.TranId = Table2.TranId " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '            "SELECT " & _
            '                 "tranheadunkid " & _
            '                                  ", trnheadcode " & _
            '                                  ", trnheadname " & _
            '                                  ", trnheadtype_id " & _
            '                            "FROM    " & mstrToDatabaseName & "..prtranhead_master " & _
            '                            "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '        ") AS TableTranHead ON TableTranHead.tranheadunkid = Table1.TranId OR TableTranHead.tranheadunkid = Table2.TranId "
            ''Sandeep [ 25 MARCH 2011 ] -- End 


            Dim mstrField As String = ""

            If mstrAnalysis_Fields.Trim.Length > 0 Then
                mstrField = mstrAnalysis_Fields.Trim.Replace("AS Id", "").Replace("AS GName", "").Substring(1) & ","
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrFromDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrFromDatabaseName, xUserUnkid, xCompanyUnkid, mintFromYearUnkId, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrFromDatabaseName)
            'Sohail (21 Aug 2015) -- End

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            Dim strEmpQ As String = ""

            strEmpQ = "SELECT  hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                            ", hremployee_master.appointeddate " & _
                            ", Alloc.classunkid " & _
                            ", ISNULL(hrclasses_master.name, '') AS classname " & _
                            ", ERECAT.jobunkid " & _
                            ", ISNULL(JM.job_name,'') AS jobname "

            If mintViewIndex > 0 Then
                strEmpQ &= mstrAnalysis_Fields
            Else
                strEmpQ &= ", 0 AS Id, '' AS GName "
            End If

            strEmpQ &= "INTO    #TableEmp " & _
                      "FROM   " & mstrToDatabaseName & "..hremployee_master" & _
                      "   LEFT JOIN " & _
                      "   ( " & _
                      "      SELECT " & _
                      "           classunkid " & _
                      "          ,employeeunkid " & _
                      "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "      FROM " & mstrToDatabaseName & "..hremployee_transfer_tran " & _
                      "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @enddate " & _
                      "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      "LEFT JOIN " & mstrToDatabaseName & "..hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                      "LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "     Cat.CatEmpId " & _
                      "    ,Cat.jobgroupunkid " & _
                      "    ,Cat.jobunkid " & _
                      "    ,Cat.CEfDt " & _
                      "    ,Cat.RECAT_REASON " & _
                      "   FROM " & _
                      "   ( " & _
                      "       SELECT " & _
                      "            ECT.employeeunkid AS CatEmpId " & _
                      "           ,ECT.jobgroupunkid " & _
                      "           ,ECT.jobunkid " & _
                      "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                      "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                      "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                      "       FROM " & mstrToDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                      "           LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                      "           LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                      "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @enddate " & _
                      "   ) AS Cat WHERE Cat.Rno = 1 " & _
                      ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                      "LEFT JOIN " & mstrToDatabaseName & "..hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid "

            strEmpQ &= mstrAnalysis_Join.Replace("'" & eZeeDate.convertDate(xPeriodEnd) & "'", "@enddate")

            If xDateJoinQry.Trim.Length > 0 Then
                strEmpQ &= xDateJoinQry.Replace("'" & eZeeDate.convertDate(xPeriodEnd) & "'", "@enddate").Replace(mstrFromDatabaseName, mstrToDatabaseName)
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strEmpQ &= xUACQry.Replace("'" & eZeeDate.convertDate(xPeriodEnd) & "'", "@enddate").Replace(mstrFromDatabaseName, mstrToDatabaseName)
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strEmpQ &= xAdvanceJoinQry.Replace("'" & eZeeDate.convertDate(xPeriodEnd) & "'", "@enddate").Replace(mstrFromDatabaseName, mstrToDatabaseName)
            End If

            strEmpQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strEmpQ &= " AND " & xUACFiltrQry.Replace("'" & eZeeDate.convertDate(xPeriodEnd) & "'", "@enddate").Replace("'" & eZeeDate.convertDate(xPeriodStart) & "'", "@startdate")
                End If
            End If

            'If xIncludeIn_ActiveEmployee = False Then
            If mblnIncludeActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strEmpQ &= xDateFilterQry.Replace("'" & eZeeDate.convertDate(xPeriodEnd) & "'", "@enddate").Replace("'" & eZeeDate.convertDate(xPeriodStart) & "'", "@startdate")
                End If
            End If

            StrQ = strEmpQ.Replace(mstrToDatabaseName, mstrFromDatabaseName).Replace("@enddate", "@fromenddate").Replace("@startdate", "@fromstartdate").Replace("#TableEmp", "#TableFromEmp")
            StrQ &= strEmpQ.Replace("@enddate", "@toenddate").Replace("@startdate", "@tostartdate").Replace("#TableEmp", "#TableToEmp")
            'Sohail (13 Jan 2020) -- End

            StrQ &= " SELECT *	 FROM " & _
                       "( " & _
                            "SELECT /*" & _
                                " CASE WHEN Table1.TranId = -1 THEN @TrnCode ELSE Table1.trnheadcode END AS TranCode " & _
                                ",CASE WHEN Table1.TranId = -1 THEN @TrnName ELSE Table1.trnheadname END AS TranName " & _
                                  ", CASE WHEN Table1.TranId = -1 THEN 99999 ELSE Table1.trnheadtype_id END AS TranHeadTypeId*/ " & _
                                  " ISNULL(Table1.trnheadcode, Table2.trnheadcode) AS TranCode " & _
                                  ", ISNULL(Table1.trnheadname, Table2.trnheadname) AS TranName " & _
                                  ", ISNULL(Table1.trnheadtype_id, Table2.trnheadtype_id) AS TranHeadTypeId " & _
                          ", ISNULL(Table1.PeriodName,'') AS Period1 " & _
                          ", ISNULL(Table1.Amount, 0) AS Amount1 " & _
                                  ", ISNULL(Table1.cnt, 0) AS cnt1 " & _
                          ", ISNULL(Table2.PeriodName,'') AS Period2 " & _
                          ", ISNULL(Table2.Amount, 0) AS Amount2 " & _
                                  ", ISNULL(Table2.cnt, 0) AS cnt2 " & _
                          ", ISNULL(Table2.Amount, 0) - ISNULL(Table1.Amount, 0) AS Variance " & _
                                  ", ISNULL(Table2.cnt, 0) - ISNULL(Table1.cnt, 0) AS VarianceCnt " & _
                         ", CASE WHEN ISNULL(Table1.Amount, 0) > 0 THEN (((ISNULL(Table2.Amount, 0) - ISNULL(Table1.Amount, 0)) ) * 100 / ISNULL(Table1.Amount, 0)) ELSE 0 END AS PerVariance " & _
                         ", CASE WHEN ISNULL(Table1.cnt, 0) > 0 THEN (((ISNULL(Table2.cnt, 0) - ISNULL(Table1.cnt, 0))  ) * 100 / ISNULL(Table1.cnt, 0) )  ELSE 0 END AS PerVarianceCnt " & _
                         ",ISNULL(Table1.Id, table2.Id) AS Id1 " & _
                         ",ISNULL(Table1.GName,Table2.GName) AS GName1 " & _
                         ",ISNULL(Table2.Id,Table1.Id) AS Id2 " & _
                         ",ISNULL(Table2.GName,Table1.GName) AS GName2 " & _
                    "FROM " & _
                    "( " & _
                        "SELECT " & _
                             "Periodid " & _
                                      ", PeriodName " & _
                                      ", TranId " & _
                                      ", trnheadcode " & _
                                      ", trnheadname " & _
                                      ", trnheadtype_id " & _
                                      ", moduleid " & _
                                      ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", COUNT(DISTINCT employeeunkid) AS cnt " & _
                                     ",Id,GName " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                 "cfcommon_period_tran.periodunkid AS Periodid " & _
                                                  ", cfcommon_period_tran.period_name AS PeriodName " & _
                                          ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                          ", prtranhead_master.trnheadcode " & _
                                          ", prtranhead_master.trnheadname " & _
                                          ", prtranhead_master.trnheadtype_id " & _
                                          ", 1 AS moduleid " & _
                                                 ", prpayrollprocess_tran.amount AS Amount "
            'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
            'Hemant (22 Jan 2019) - [PerVariance, PerVarianceCnt]
            'Sohail (21 Jan 2017) - [cnt1, cnt2, VarianceCnt, employeeunkid]
            'Sohail (17 Dec 2016) - [trnheadcode, trnheadname, trnheadtype_id] 

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableFromEmp.Id, #TableFromEmp.GName "
            'Sohail (13 Jan 2020) -- End

            StrQ &= "FROM     " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                                    "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                    "JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                    "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prpayrollprocess_tran.employeeunkid "
            'Sohail (13 Jan 2020) - [LEFT JOIN  " & mstrFromDatabaseName & "..hremployee_master]=[JOIN    #TableFromEmp]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'StrQ &= mstrAnalysis_Join

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND cfcommon_period_tran.isactive = 1 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND cfcommon_period_tran.periodunkid = @Period1 " & _
                                                    "AND prpayrollprocess_tran.amount <> 0 "
            'Sohail (22 Jun 2020) - [AND prpayrollprocess_tran.amount <> 0]

            If mintTranHeadTypeId > 0 Then
                StrQ &= " AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            End If

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            If mintTranHeadTypeId = 0 AndAlso mstrInfoHeadIDs.Trim <> "" Then
                StrQ &= " AND (prtranhead_master.trnheadtype_id <> " & enTranHeadType.Informational & " OR  prtranhead_master.tranheadunkid IN (" & mstrInfoHeadIDs & ") ) "
            End If
            If mintTranHeadTypeId = enTranHeadType.Informational AndAlso mstrInfoHeadIDs.Trim <> "" Then
                StrQ &= " AND prtranhead_master.tranheadunkid IN (" & mstrInfoHeadIDs & ") "
            End If
            'Sohail (13 Jan 2020) -- End

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= "UNION ALL " & _
                           "SELECT " & _
                                 "cfcommon_period_tran.periodunkid AS Periodid " & _
                                ",cfcommon_period_tran.period_name AS PeriodName " & _
                                ", prtnaleave_tran.employeeunkid " & _
                                ",-1 AS TranId " & _
                                ", @TrnCode AS trnheadcode " & _
                                ", @TrnName AS trnheadname " & _
                                ", 99999 AS trnheadtype_id " & _
                                ", 1 AS moduleid " & _
                                ", CAST(prtnaleave_tran.openingbalance AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "
            'Sohail (02 Jan 2017) - [employeeunkid]
            'Sohail (17 Dec 2016) - [trnheadcode, trnheadname, trnheadtype_id] 

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableFromEmp.Id, #TableFromEmp.GName "
            'Sohail (13 Jan 2020) -- End

            StrQ &= "FROM " & mstrFromDatabaseName & "..prtnaleave_tran " & _
                                "JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prtnaleave_tran.employeeunkid "
            'Sohail (13 Jan 2020) - [LEFT JOIN  " & mstrFromDatabaseName & "..hremployee_master]=[JOIN    #TableFromEmp]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'StrQ &= mstrAnalysis_Join

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "AND cfcommon_period_tran.isactive = 1 " & _
                                "AND cfcommon_period_tran.periodunkid = @Period1 " & _
                                "AND prtnaleave_tran.openingbalance <> 0 "
            'Sohail (22 Jun 2020) - [AND prtnaleave_tran.openingbalance <> 0]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If
            'Sohail (13 Jan 2020) -- End

            'Sohail (17 Dec 2016) -- Start
            'KBC Enhancement - Issue# 27 - 64.1 - Include Claim Request data in both Payroll Variance report and Payroll Total Variance report.
            StrQ &= "  UNION ALL " & _
                      "SELECT    cfcommon_period_tran.periodunkid AS Periodid  " & _
                              ", cfcommon_period_tran.period_name AS PeriodName " & _
                              ", prpayrollprocess_tran.employeeunkid " & _
                              ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                              ", ISNULL(cmexpense_master.code, ISNULL(crretireexpense.code, ' ')) AS trnheadcode " & _
                              ", ISNULL(cmexpense_master.name, ISNULL(crretireexpense.name, ' ')) AS trnheadname " & _
                              ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN cmexpense_master.trnheadtype_id ELSE prpayrollprocess_tran.add_deduct END AS trnheadtype_id " & _
                              ", 2 AS moduleid " & _
                              ", prpayrollprocess_tran.amount AS Amount "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (02 Jan 2017) - [employeeunkid]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableFromEmp.Id, #TableFromEmp.GName "
            'Sohail (13 Jan 2020) -- End

            StrQ &= "             FROM      " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                            "JOIN " & mstrFromDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                            "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                            "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                            "LEFT JOIN " & mstrFromDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                            "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                            "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prpayrollprocess_tran.employeeunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (13 Jan 2020) - [LEFT JOIN  " & mstrFromDatabaseName & "..hremployee_master]=[JOIN    #TableFromEmp]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'StrQ &= mstrAnalysis_Join

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= "              WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                            "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                                            "AND cfcommon_period_tran.periodunkid = @Period1 " & _
                                            "AND prpayrollprocess_tran.amount <> 0 "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (22 Jun 2020) - [AND prpayrollprocess_tran.amount <> 0]

            If mintTranHeadTypeId > 0 Then
                StrQ &= " AND cmexpense_master.trnheadtype_id = @trnheadtype_id "
            End If

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If
            ''Sohail (17 Dec 2016) -- End
            'Sohail (13 Jan 2020) -- End

            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , mstrToDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, mstrToDatabaseName, xUserUnkid, xCompanyUnkid, mintToYearUnkId, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, mstrToDatabaseName)
            'Sohail (21 Aug 2015) -- End

            StrQ &= ") AS TranTable " & _
                        "GROUP BY GName, Periodid,PeriodName,TranId, trnheadcode, trnheadname, trnheadtype_id, moduleid, Id " & _
                            ") AS Table1 " & _
                    "FULL OUTER JOIN " & _
                    "( " & _
                        "SELECT " & _
                             "Periodid " & _
                                                      ", PeriodName " & _
                                                      ", TranId " & _
                                                      ", trnheadcode " & _
                                                      ", trnheadname " & _
                                                      ", trnheadtype_id " & _
                                                      ", moduleid " & _
                                                      ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                                      ", COUNT(DISTINCT employeeunkid) AS cnt " & _
                                                      ", Id, GName  " & _
                        "FROM " & _
                        "( " & _
                            "SELECT " & _
                                 "cfcommon_period_tran.periodunkid AS Periodid " & _
                                                                  ", cfcommon_period_tran.period_name AS PeriodName " & _
                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                                  ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                  ", prtranhead_master.trnheadcode " & _
                                  ", prtranhead_master.trnheadname " & _
                                  ", prtranhead_master.trnheadtype_id " & _
                                  ", 1 AS moduleid " & _
                                  ", prpayrollprocess_tran.amount AS Amount "
            'Sohail (27 Apr 2021) - [SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))] = [SUM(amount)]
            'Sohail (02 Jan 2017) - [employeeunkid]
            'Sohail (17 Dec 2016) - [trnheadcode, trnheadname, trnheadtype_id, trnheadtype_id]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableToEmp.Id, #TableToEmp.GName "
            'Sohail (13 Jan 2020) -- End

            StrQ &= "FROM     " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                                                    "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                    "JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                                    "JOIN " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                    "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prpayrollprocess_tran.employeeunkid "
            'Sohail (13 Jan 2020) - [LEFT JOIN  " & mstrToDatabaseName & "..hremployee_master]=[JOIN    #TableToEmp]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'StrQ &= mstrAnalysis_Join

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                                                    "AND cfcommon_period_tran.isactive = 1 " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                    "AND cfcommon_period_tran.periodunkid = @Period2 " & _
                                                                    "AND prpayrollprocess_tran.amount <> 0 "
            'Sohail (22 Jun 2020) - [AND prpayrollprocess_tran.amount <> 0]

            If mintTranHeadTypeId > 0 Then
                StrQ &= " AND prtranhead_master.trnheadtype_id = @trnheadtype_id "
            End If

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            If mintTranHeadTypeId = 0 AndAlso mstrInfoHeadIDs.Trim <> "" Then
                StrQ &= " AND (prtranhead_master.trnheadtype_id <> " & enTranHeadType.Informational & " OR  prtranhead_master.tranheadunkid IN (" & mstrInfoHeadIDs & ") ) "
            End If
            If mintTranHeadTypeId = enTranHeadType.Informational AndAlso mstrInfoHeadIDs.Trim <> "" Then
                StrQ &= " AND prtranhead_master.tranheadunkid IN (" & mstrInfoHeadIDs & ") "
            End If
            'Sohail (13 Jan 2020) -- End

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= "UNION ALL " & _
                            "SELECT " & _
                                 "cfcommon_period_tran.periodunkid AS Periodid " & _
                                ",cfcommon_period_tran.period_name AS PeriodName " & _
                                ", prtnaleave_tran.employeeunkid " & _
                                ",-1 AS TranId " & _
                                ", @TrnCode AS trnheadcode " & _
                                ", @TrnName AS trnheadname " & _
                                ", 99999 AS trnheadtype_id " & _
                                ", 1 AS moduleid " & _
                            ",CAST(prtnaleave_tran.openingbalance AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "
            'Sohail (02 Jan 2017) - [employeeunkid]
            'Sohail (17 Dec 2016) - [trnheadcode, trnheadname, trnheadtype_id] 

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableToEmp.Id, #TableToEmp.GName "
            'Sohail (13 Jan 2020) -- End

            StrQ &= "FROM " & mstrToDatabaseName & "..prtnaleave_tran " & _
                                "JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prtnaleave_tran.employeeunkid "
            'Sohail (13 Jan 2020) - [LEFT JOIN  " & mstrToDatabaseName & "..hremployee_master]=[JOIN    #TableToEmp]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'StrQ &= mstrAnalysis_Join

            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "AND cfcommon_period_tran.isactive = 1 " & _
                                "AND cfcommon_period_tran.periodunkid = @Period2 " & _
                                "AND prtnaleave_tran.openingbalance <> 0 "
            'Sohail (22 Jun 2020) - [AND prtnaleave_tran.openingbalance <> 0]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            ''Sohail (21 Aug 2015) -- Start
            ''Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If
            ''Sohail (21 Aug 2015) -- End
            'Sohail (13 Jan 2020) -- End

            'Sohail (17 Dec 2016) -- Start
            'KBC Enhancement - Issue# 27 - 64.1 - Include Claim Request data in both Payroll Variance report and Payroll Total Variance report.
            StrQ &= "  UNION ALL " & _
                      "SELECT    cfcommon_period_tran.periodunkid AS Periodid  " & _
                              ", cfcommon_period_tran.period_name AS PeriodName " & _
                              ", prpayrollprocess_tran.employeeunkid " & _
                              ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                              ", ISNULL(cmexpense_master.code, ISNULL(crretireexpense.code, ' ')) AS trnheadcode " & _
                              ", ISNULL(cmexpense_master.name, ISNULL(crretireexpense.name, ' ')) AS trnheadname " & _
                              ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN cmexpense_master.trnheadtype_id ELSE prpayrollprocess_tran.add_deduct END AS trnheadtype_id " & _
                              ", 2 AS moduleid " & _
                              ", prpayrollprocess_tran.amount AS Amount "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (02 Jan 2017) - [employeeunkid]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #TableToEmp.Id, #TableToEmp.GName "
            'Sohail (13 Jan 2020) -- End

            StrQ &= "             FROM      " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                                            "JOIN " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                            "JOIN " & mstrToDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                            "LEFT JOIN " & mstrToDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                            "LEFT JOIN " & mstrToDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                            "LEFT JOIN " & mstrToDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                            "LEFT JOIN " & mstrToDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                            "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prpayrollprocess_tran.employeeunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (13 Jan 2020) - [LEFT JOIN  " & mstrToDatabaseName & "..hremployee_master]=[JOIN    #TableToEmp]

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'StrQ &= mstrAnalysis_Join

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= "              WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                            "AND prtnaleave_tran.isvoid = 0 " & _
                                            "AND cfcommon_period_tran.isactive = 1 " & _
                                            "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                                            "AND cfcommon_period_tran.periodunkid = @Period2 " & _
                                            "AND prpayrollprocess_tran.amount <> 0 "
            'Sohail (22 Jun 2020) - [AND prpayrollprocess_tran.amount <> 0]

            If mintTranHeadTypeId > 0 Then
                StrQ &= " AND cmexpense_master.trnheadtype_id = @trnheadtype_id "
            End If

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If
            ''Sohail (17 Dec 2016) -- End
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS TranTable " & _
                      "GROUP BY GName,Periodid,PeriodName,TranId, trnheadcode, trnheadname, trnheadtype_id, moduleid, Id " & _
                ") AS Table2 ON Table1.TranId = Table2.TranId AND Table1.Id = Table2.Id AND Table1.moduleid = Table2.moduleid "


            'Anjan (07 Jun 2018) -- Start
            'Internal Enhancement - Including Ignore Zero Variance setting.
            If mblnIgnoreZeroVariance = True Then
                StrQ &= " WHERE 1 = 1 AND ISNULL(Table2.Amount, 0) - ISNULL(Table1.Amount, 0) <> 0 "
                'OR ISNULL(Table1.trnheadtype_id, Table2.trnheadtype_id) = 99999) "
            End If
            'Anjan (04 Jun 2018) -- End

            StrQ &= "/*LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                             "tranheadunkid " & _
                                              ", trnheadcode " & _
                                              ", trnheadname " & _
                                              ", trnheadtype_id " & _
                                        "FROM    " & mstrToDatabaseName & "..prtranhead_master " & _
                                        "WHERE   ISNULL(isvoid, 0) = 0 " & _
                    ") AS TableTranHead ON TableTranHead.tranheadunkid = Table1.TranId OR TableTranHead.tranheadunkid = Table2.TranId*/ "


            'Pinkal (15-Jan-2013) -- End



            Call FilterTitleAndFilterQurey()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            StrQ &= " DROP TABLE #TableFromEmp " & _
                    " DROP TABLE #TableToEmp "

            objDataOperation.AddParameter("@fromstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodStart))
            objDataOperation.AddParameter("@fromenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEnd))
            objDataOperation.AddParameter("@tostartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodStart))
            objDataOperation.AddParameter("@toenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodEnd))
            'Sohail (13 Jan 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            'Sohail (21 Jan 2017) -- Start
            'CCBRT Enhancement - 64.1 - Show employee count for from period, to period and variance.
            rpt_Data.Tables("ArutiTable").Columns("Column84").DataType = System.Type.GetType("System.Int64")
            rpt_Data.Tables("ArutiTable").Columns("Column85").DataType = System.Type.GetType("System.Int64")
            rpt_Data.Tables("ArutiTable").Columns("Column86").DataType = System.Type.GetType("System.Int64")
            'Sohail (21 Jan 2017) -- End

            'Hemant (22 June 2019) -- Start
            'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
            'Hemant (27 June 2019) -- Start
            'ISSUE : Error Occurs : "Arithmetic operation resulted in an overflow."
            'Dim intTotalAmount1 As Integer = 0
            'Dim intTotalCount1 As Integer = 0
            'Dim intTotalAmount2 As Integer = 0
            'Dim intTotalCount2 As Integer = 0
            'Dim intTotalVarianceAmt As Integer = 0
            Dim decTotalAmount1 As Decimal = 0
            Dim intTotalCount1 As Integer = 0
            Dim decTotalAmount2 As Decimal = 0
            Dim intTotalCount2 As Integer = 0
            Dim decTotalVarianceAmt As Decimal = 0
            'Hemant (27 June 2019) -- End
            Dim intTotalVarianceCnt As Integer = 0
            Dim intOldHeadTypeId As Integer = 0
            'Hemant (22 June 2019) -- End


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                'Hemant (22 June 2019) -- Start
                'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
                If intOldHeadTypeId <> CInt(dtRow.Item("TranHeadTypeId")) Then
                    'Hemant (27 June 2019) -- Start
                    'ISSUE : Error Occurs : "Arithmetic operation resulted in an overflow."
                    'intTotalAmount1 = 0
                    'intTotalCount1 = 0
                    'intTotalAmount2 = 0
                    'intTotalCount2 = 0
                    'intTotalVarianceAmt = 0
                    decTotalAmount1 = 0
                    intTotalCount1 = 0
                    decTotalAmount2 = 0
                    intTotalCount2 = 0
                    decTotalVarianceAmt = 0
                    'Hemant (27 June 2019) -- End
                    intTotalVarianceCnt = 0
                    intOldHeadTypeId = 0
                End If

                'Hemant (22 June 2019) -- End
                rpt_Rows.Item("Column1") = dtRow.Item("TranCode")
                rpt_Rows.Item("Column2") = dtRow.Item("TranName")
                rpt_Rows.Item("Column3") = dtRow.Item("Period1")
                'rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Amount1")), GUI.fmtCurrency)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column4") = Format(dtRow.Item("Amount1"), GUI.fmtCurrency)
                rpt_Rows.Item("Column4") = Format(dtRow.Item("Amount1"), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column5") = dtRow.Item("Period2")
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Amount2")), GUI.fmtCurrency)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column6") = Format(dtRow.Item("Amount2"), GUI.fmtCurrency)
                rpt_Rows.Item("Column6") = Format(dtRow.Item("Amount2"), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'rpt_Rows.Item("Column7") = Format(CDec(dtRow.Item("Variance")), GUI.fmtCurrency)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column7") = Format(dtRow.Item("Variance"), GUI.fmtCurrency)
                rpt_Rows.Item("Column7") = Format(dtRow.Item("Variance"), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'Sohail (20 Jun 2012) -- Start
                'TRA - ENHANCEMENT
                rpt_Rows.Item("Column8") = dtRow.Item("TranHeadTypeId")


                'Pinkal (15-Jan-2013) -- Start
                'Enhancement : TRA Changes
                If Not IsDBNull(dtRow.Item("TranHeadTypeId")) Then

                    If mdicTranHeadType.ContainsKey(CInt(dtRow.Item("TranHeadTypeId"))) = True Then
                        rpt_Rows.Item("Column9") = mdicTranHeadType.Item(CInt(dtRow.Item("TranHeadTypeId")))
                    Else
                        rpt_Rows.Item("Column9") = dtRow.Item("TranName")
                    End If
                Else
                    rpt_Rows.Item("Column9") = dtRow.Item("TranName")
                End If

                rpt_Rows.Item("Column10") = dtRow.Item("Id1")
                rpt_Rows.Item("Column11") = dtRow.Item("GName1")
                rpt_Rows.Item("Column12") = dtRow.Item("Id2")
                rpt_Rows.Item("Column13") = dtRow.Item("GName2")
                rpt_Rows.Item("Column81") = dtRow.Item("Amount1")
                rpt_Rows.Item("Column82") = dtRow.Item("Amount2")
                rpt_Rows.Item("Column83") = dtRow.Item("Variance")

                'Pinkal (15-Jan-2013) -- End

                'Sohail (20 Jun 2012) -- End

                'Sohail (21 Jan 2017) -- Start
                'CCBRT Enhancement - 64.1 - Show employee count for from period, to period and variance.
                rpt_Rows.Item("Column14") = dtRow.Item("cnt1")
                rpt_Rows.Item("Column15") = dtRow.Item("cnt2")
                rpt_Rows.Item("Column16") = dtRow.Item("VarianceCnt")
                rpt_Rows.Item("Column84") = dtRow.Item("cnt1")
                rpt_Rows.Item("Column85") = dtRow.Item("cnt2")
                rpt_Rows.Item("Column86") = dtRow.Item("VarianceCnt")
                'Sohail (21 Jan 2017) -- End

                'Hemant (22 Jan 2019) -- Start
                'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
                rpt_Rows.Item("Column17") = CDec(dtRow.Item("PerVariance")).ToString("0.00")
                rpt_Rows.Item("Column18") = dtRow.Item("PerVarianceCnt")
                rpt_Rows.Item("Column87") = CDec(dtRow.Item("PerVariance")).ToString("0.00")
                rpt_Rows.Item("Column88") = dtRow.Item("PerVarianceCnt")
                'Hemant (22 Jan 2019) -- End

                'Hemant (22 June 2019) -- Start
                'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
                'Hemant (27 June 2019) -- Start
                'ISSUE : Error Occurs : "Arithmetic operation resulted in an overflow."
                'intTotalAmount1 += CInt(Format(dtRow.Item("Amount1"), strfmtCurrency))
                'intTotalCount1 += CInt(dtRow.Item("cnt1"))
                'intTotalAmount2 += CInt(Format(dtRow.Item("Amount2"), strfmtCurrency))
                'intTotalCount2 += CInt(dtRow.Item("cnt2"))
                'intTotalVarianceAmt += CInt(Format(dtRow.Item("Variance"), strfmtCurrency))
                'intTotalVarianceCnt += CInt(dtRow.Item("VarianceCnt"))
                'rpt_Rows.Item("Column19") = Format(intTotalAmount1, strfmtCurrency)
                'rpt_Rows.Item("Column21") = Format(intTotalAmount2, strfmtCurrency)
                'rpt_Rows.Item("Column20") = intTotalCount1
                'rpt_Rows.Item("Column22") = intTotalCount2
                'rpt_Rows.Item("Column23") = Format(intTotalVarianceAmt, strfmtCurrency)
                decTotalAmount1 += CDec(Format(dtRow.Item("Amount1"), strfmtCurrency))
                intTotalCount1 += CInt(dtRow.Item("cnt1"))
                decTotalAmount2 += CDec(Format(dtRow.Item("Amount2"), strfmtCurrency))
                intTotalCount2 += CInt(dtRow.Item("cnt2"))
                decTotalVarianceAmt += CDec(Format(dtRow.Item("Variance"), strfmtCurrency))
                intTotalVarianceCnt += CInt(dtRow.Item("VarianceCnt"))
                rpt_Rows.Item("Column19") = Format(decTotalAmount1, strfmtCurrency)
                rpt_Rows.Item("Column21") = Format(decTotalAmount2, strfmtCurrency)
                rpt_Rows.Item("Column20") = intTotalCount1
                rpt_Rows.Item("Column22") = intTotalCount2
                rpt_Rows.Item("Column23") = Format(decTotalVarianceAmt, strfmtCurrency)
                'Hemant (27 June 2019) -- End
                rpt_Rows.Item("Column24") = intTotalVarianceCnt
                intOldHeadTypeId = CInt(dtRow.Item("TranHeadTypeId"))
                'Hemant (22 June 2019) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            Dim dsNewStaffSalary As DataSet
            Dim dsOldStaffSalary As DataSet
            Dim dsSalaryChange As DataSet
            Dim dsSummary As DataSet
            Dim TotalAmtNewStaff As Decimal
            Dim TotalAmtOldStaff As Decimal
            Dim TotalPrevAmt As Decimal
            Dim TotalCurrAmt As Decimal


            '*** New Staff
            StrQ = strEmpQ

            StrQ = strEmpQ.Replace(mstrToDatabaseName, mstrFromDatabaseName).Replace("@enddate", "@fromenddate").Replace("@startdate", "@fromstartdate").Replace("#TableEmp", "#TableFromEmp")

            StrQ &= "SELECT  prpayrollprocess_tran.employeeunkid " & _
                          ", #TableFromEmp.employeecode " & _
                          ", #TableFromEmp.employeename " & _
                          ", #TableFromEmp.classunkid " & _
                          ", #TableFromEmp.classname " & _
                          ", #TableFromEmp.jobunkid " & _
                          ", #TableFromEmp.jobname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS Amount " & _
                   "INTO     #Prev " & _
                   "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period1 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                          ", #TableFromEmp.employeecode " & _
                          ", #TableFromEmp.employeename " & _
                          ", #TableFromEmp.classunkid " & _
                          ", #TableFromEmp.classname " & _
                          ", #TableFromEmp.jobunkid " & _
                          ", #TableFromEmp.jobname "

            StrQ &= strEmpQ.Replace("@enddate", "@toenddate").Replace("@startdate", "@tostartdate").Replace("#TableEmp", "#TableToEmp")

            StrQ &= "SELECT  prpayrollprocess_tran.employeeunkid " & _
                          ", #TableToEmp.employeecode " & _
                          ", #TableToEmp.employeename " & _
                          ", #TableToEmp.classunkid " & _
                          ", #TableToEmp.classname " & _
                          ", #TableToEmp.jobunkid " & _
                          ", #TableToEmp.jobname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS Amount " & _
                   "INTO     #Curr " & _
                   "FROM    " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period2 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                          ", #TableToEmp.employeecode " & _
                          ", #TableToEmp.employeename " & _
                          ", #TableToEmp.classunkid " & _
                          ", #TableToEmp.classname " & _
                          ", #TableToEmp.jobunkid " & _
                          ", #TableToEmp.jobname "

            StrQ &= "SELECT  #Curr.employeeunkid " & _
                          ", #Curr.employeecode " & _
                          ", #Curr.employeename " & _
                          ", #Curr.classunkid " & _
                          ", #Curr.classname " & _
                          ", #Curr.jobunkid " & _
                          ", #Curr.jobname " & _
                          ", #Curr.amount AS Amount " & _
                   "FROM    #Curr " & _
                            "LEFT JOIN  #Prev ON #Curr.employeeunkid = #Prev.employeeunkid "

            StrQ &= "WHERE   #Prev.employeeunkid IS NULL "

            StrQ &= "DROP TABLE #TableFromEmp " & _
                    "DROP TABLE #TableToEmp " & _
                    "DROP TABLE #Prev " & _
                    "DROP TABLE #Curr "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Period1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId1)
            objDataOperation.AddParameter("@Period2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId2)
            objDataOperation.AddParameter("@fromstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodStart))
            objDataOperation.AddParameter("@fromenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEnd))
            objDataOperation.AddParameter("@tostartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodStart))
            objDataOperation.AddParameter("@toenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodEnd))

            dsNewStaffSalary = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtNewStaffSalary As DataTable = rpt_Data.Tables("ArutiTable").Clone
            TotalAmtNewStaff = (From p In dsNewStaffSalary.Tables(0) Select (CDec(p.Item("Amount")))).Sum
            For Each dsRow As DataRow In dsNewStaffSalary.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = dtNewStaffSalary.NewRow

                rpt_Rows.Item("Column1") = CInt(dsRow.Item("employeeunkid"))
                rpt_Rows.Item("Column2") = dsRow.Item("employeecode")
                rpt_Rows.Item("Column3") = dsRow.Item("employeename")
                rpt_Rows.Item("Column4") = dsRow.Item("jobname")
                rpt_Rows.Item("Column5") = dsRow.Item("classname")
                rpt_Rows.Item("Column11") = Format(0, strfmtCurrency)
                rpt_Rows.Item("Column12") = Format(CDec(dsRow.Item("Amount")), strfmtCurrency)
                rpt_Rows.Item("Column13") = Format(CDec(dsRow.Item("Amount")), strfmtCurrency)
                rpt_Rows.Item("Column21") = Format(0, strfmtCurrency)
                rpt_Rows.Item("Column22") = Format(TotalAmtNewStaff, strfmtCurrency)
                rpt_Rows.Item("Column23") = Format(TotalAmtNewStaff, strfmtCurrency)

                dtNewStaffSalary.Rows.Add(rpt_Rows)

            Next

            '** Omitted Staff
            StrQ = strEmpQ.Replace(mstrToDatabaseName, mstrFromDatabaseName).Replace("@enddate", "@fromenddate").Replace("@startdate", "@fromstartdate").Replace("#TableEmp", "#TableFromEmp")

            StrQ &= "SELECT  prpayrollprocess_tran.employeeunkid " & _
                          ", #TableFromEmp.employeecode " & _
                          ", #TableFromEmp.employeename " & _
                          ", #TableFromEmp.classunkid " & _
                          ", #TableFromEmp.classname " & _
                          ", #TableFromEmp.jobunkid " & _
                          ", #TableFromEmp.jobname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS Amount " & _
                   "INTO     #Prev " & _
                   "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period1 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                          ", #TableFromEmp.employeecode " & _
                          ", #TableFromEmp.employeename " & _
                          ", #TableFromEmp.classunkid " & _
                          ", #TableFromEmp.classname " & _
                          ", #TableFromEmp.jobunkid " & _
                          ", #TableFromEmp.jobname "

            StrQ &= strEmpQ.Replace("@enddate", "@toenddate").Replace("@startdate", "@tostartdate").Replace("#TableEmp", "#TableToEmp")

            StrQ &= "SELECT  prpayrollprocess_tran.employeeunkid " & _
                          ", #TableToEmp.employeecode " & _
                          ", #TableToEmp.employeename " & _
                          ", #TableToEmp.classunkid " & _
                          ", #TableToEmp.classname " & _
                          ", #TableToEmp.jobunkid " & _
                          ", #TableToEmp.jobname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS Amount " & _
                   "INTO     #Curr " & _
                   "FROM    " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period2 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                          ", #TableToEmp.employeecode " & _
                          ", #TableToEmp.employeename " & _
                          ", #TableToEmp.classunkid " & _
                          ", #TableToEmp.classname " & _
                          ", #TableToEmp.jobunkid " & _
                          ", #TableToEmp.jobname "

            StrQ &= "SELECT  #Prev.employeeunkid " & _
                          ", #Prev.employeecode " & _
                          ", #Prev.employeename " & _
                          ", #Prev.classunkid " & _
                          ", #Prev.classname " & _
                          ", #Prev.jobunkid " & _
                          ", #Prev.jobname " & _
                          ", #Prev.amount AS Amount " & _
                   "FROM    #Prev " & _
                            "LEFT JOIN  #Curr ON #Curr.employeeunkid = #Prev.employeeunkid "

            StrQ &= "WHERE   #Curr.employeeunkid IS NULL "

            StrQ &= "DROP TABLE #TableFromEmp " & _
                    "DROP TABLE #TableToEmp " & _
                    "DROP TABLE #Prev " & _
                    "DROP TABLE #Curr "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Period1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId1)
            objDataOperation.AddParameter("@Period2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId2)
            objDataOperation.AddParameter("@fromstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodStart))
            objDataOperation.AddParameter("@fromenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEnd))
            objDataOperation.AddParameter("@tostartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodStart))
            objDataOperation.AddParameter("@toenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodEnd))

            dsOldStaffSalary = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtOldStaffSalary As DataTable = rpt_Data.Tables("ArutiTable").Clone
            TotalAmtOldStaff = (From p In dsOldStaffSalary.Tables(0) Select (CDec(p.Item("Amount")))).Sum
            For Each dsRow As DataRow In dsOldStaffSalary.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = dtOldStaffSalary.NewRow

                rpt_Rows.Item("Column1") = CInt(dsRow.Item("employeeunkid"))
                rpt_Rows.Item("Column2") = dsRow.Item("employeecode")
                rpt_Rows.Item("Column3") = dsRow.Item("employeename")
                rpt_Rows.Item("Column4") = dsRow.Item("jobname")
                rpt_Rows.Item("Column5") = dsRow.Item("classname")
                rpt_Rows.Item("Column11") = Format(CDec(dsRow.Item("Amount")), strfmtCurrency)
                rpt_Rows.Item("Column12") = Format(0, strfmtCurrency)
                rpt_Rows.Item("Column13") = Format(CDec(dsRow.Item("Amount")), strfmtCurrency)
                rpt_Rows.Item("Column21") = Format(TotalAmtOldStaff, strfmtCurrency)
                rpt_Rows.Item("Column22") = Format(0, strfmtCurrency)
                rpt_Rows.Item("Column23") = Format(TotalAmtOldStaff, strfmtCurrency)

                dtOldStaffSalary.Rows.Add(rpt_Rows)

            Next

            '** Previous month salaries for those whose Salary change is done in current month
            StrQ = strEmpQ.Replace("@enddate", "@toenddate").Replace("@startdate", "@tostartdate").Replace("#TableEmp", "#TableToEmp")

            'StrQ &= "SELECT DISTINCT prsalaryincrement_tran.employeeunkid " & _
            '        "INTO #TableSal " & _
            '        "FROM " & mstrToDatabaseName & "..prsalaryincrement_tran " & _
            '        "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
            '        "WHERE prsalaryincrement_tran.isvoid = 0 " & _
            '          "AND prsalaryincrement_tran.isfromemployee = 0 " & _
            '          "AND prsalaryincrement_tran.periodunkid = @Period2 "

            StrQ &= strEmpQ.Replace(mstrToDatabaseName, mstrFromDatabaseName).Replace("@enddate", "@fromenddate").Replace("@startdate", "@fromstartdate").Replace("#TableEmp", "#TableFromEmp")

            StrQ &= "SELECT  prpayrollprocess_tran.employeeunkid " & _
                          ", #TableFromEmp.employeecode " & _
                          ", #TableFromEmp.employeename " & _
                          ", #TableFromEmp.classunkid " & _
                          ", #TableFromEmp.classname " & _
                          ", #TableFromEmp.jobunkid " & _
                          ", #TableFromEmp.jobname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS Amount " & _
                   "INTO    #Prev " & _
                   "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "/*JOIN    #TableSal ON #TableSal.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                   "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period1 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                          ", #TableFromEmp.employeecode " & _
                          ", #TableFromEmp.employeename " & _
                          ", #TableFromEmp.classunkid " & _
                          ", #TableFromEmp.classname " & _
                          ", #TableFromEmp.jobunkid " & _
                          ", #TableFromEmp.jobname "


            StrQ &= "SELECT  prpayrollprocess_tran.employeeunkid " & _
                          ", #TableToEmp.employeecode " & _
                          ", #TableToEmp.employeename " & _
                          ", #TableToEmp.classunkid " & _
                          ", #TableToEmp.classname " & _
                          ", #TableToEmp.jobunkid " & _
                          ", #TableToEmp.jobname " & _
                          ", SUM(prpayrollprocess_tran.amount) AS Amount " & _
                   "INTO    #Curr " & _
                   "FROM    " & mstrToDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrToDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "/*JOIN    #TableSal ON #TableSal.employeeunkid = prpayrollprocess_tran.employeeunkid*/ " & _
                   "JOIN    #TableToEmp ON #TableToEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrToDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period2 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                          ", #TableToEmp.employeecode " & _
                          ", #TableToEmp.employeename " & _
                          ", #TableToEmp.classunkid " & _
                          ", #TableToEmp.classname " & _
                          ", #TableToEmp.jobunkid " & _
                          ", #TableToEmp.jobname "

            StrQ &= "SELECT  #Prev.employeeunkid " & _
                         ", #Prev.employeecode " & _
                         ", #Prev.employeename " & _
                         ", #Prev.classunkid " & _
                         ", #Prev.classname " & _
                         ", #Prev.jobunkid " & _
                         ", #Prev.jobname " & _
                         ", #Prev.amount AS PrevAmount " & _
                         ", #Curr.amount AS CurrAmount " & _
                  "FROM    #Prev " & _
                           "JOIN  #Curr ON #Curr.employeeunkid = #Prev.employeeunkid "

            StrQ &= "WHERE   1 = 1 " & _
                    "AND  #Prev.amount <> #Curr.amount "

            StrQ &= "DROP TABLE #TableFromEmp " & _
                    "DROP TABLE #TableToEmp " & _
                    "DROP TABLE #Prev " & _
                    "DROP TABLE #Curr " & _
                    "/*DROP TABLE #TableSal*/ "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Period1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId1)
            objDataOperation.AddParameter("@Period2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId2)
            objDataOperation.AddParameter("@fromstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodStart))
            objDataOperation.AddParameter("@fromenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEnd))
            objDataOperation.AddParameter("@tostartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodStart))
            objDataOperation.AddParameter("@toenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToPeriodEnd))

            dsSalaryChange = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtSalaryChange As DataTable = rpt_Data.Tables("ArutiTable").Clone
            TotalPrevAmt = (From p In dsSalaryChange.Tables(0) Select (CDec(p.Item("PrevAmount")))).Sum
            TotalCurrAmt = (From p In dsSalaryChange.Tables(0) Select (CDec(p.Item("CurrAmount")))).Sum
            For Each dsRow As DataRow In dsSalaryChange.Tables(0).Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = dtSalaryChange.NewRow

                rpt_Rows.Item("Column1") = CInt(dsRow.Item("employeeunkid"))
                rpt_Rows.Item("Column2") = dsRow.Item("employeecode")
                rpt_Rows.Item("Column3") = dsRow.Item("employeename")
                rpt_Rows.Item("Column4") = dsRow.Item("jobname")
                rpt_Rows.Item("Column5") = dsRow.Item("classname")
                rpt_Rows.Item("Column11") = Format(CDec(dsRow.Item("PrevAmount")), strfmtCurrency)
                rpt_Rows.Item("Column12") = Format(CDec(dsRow.Item("CurrAmount")), strfmtCurrency)
                rpt_Rows.Item("Column13") = Format(CDec(dsRow.Item("CurrAmount")) - CDec(dsRow.Item("PrevAmount")), strfmtCurrency)
                rpt_Rows.Item("Column21") = Format(TotalPrevAmt, strfmtCurrency)
                rpt_Rows.Item("Column22") = Format(TotalCurrAmt, strfmtCurrency)
                rpt_Rows.Item("Column23") = Format(TotalCurrAmt - TotalPrevAmt, strfmtCurrency)

                dtSalaryChange.Rows.Add(rpt_Rows)

            Next

            '*** Summary
            StrQ = strEmpQ.Replace(mstrToDatabaseName, mstrFromDatabaseName).Replace("@enddate", "@fromenddate").Replace("@startdate", "@fromstartdate").Replace("#TableEmp", "#TableFromEmp")

            StrQ &= "SELECT  ISNULL(SUM(prpayrollprocess_tran.amount), 0) AS Amount " & _
                   "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                   "JOIN    #TableFromEmp ON #TableFromEmp.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                   "LEFT JOIN    " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (21 May 2020) - # 0004710 - [SUM(prpayrollprocess_tran.amount)]=[ISNULL(SUM(prpayrollprocess_tran.amount), 0)]

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @Period1 " & _
                            "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "

            StrQ &= "DROP TABLE #TableFromEmp "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Period1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId1)
            objDataOperation.AddParameter("@fromstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodStart))
            objDataOperation.AddParameter("@fromenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromPeriodEnd))

            dsSummary = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dtSummary As DataTable = rpt_Data.Tables("ArutiTable").Clone
            For Each dsRow As DataRow In dsSummary.Tables(0).Rows
                Dim rpt_Rows As DataRow

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = mstrPeriodName1 & " " & Language.getMessage(mstrModuleName, 41, "BASIC SALARY")
                rpt_Rows.Item("Column2") = Format(CDec(dsRow.Item("Amount")), strfmtCurrency)
                rpt_Rows.Item("Column3") = "1" 'IsBold
                dtSummary.Rows.Add(rpt_Rows)

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 42, "NEW EMPLOYEES")
                rpt_Rows.Item("Column2") = Format(TotalAmtNewStaff, strfmtCurrency)
                rpt_Rows.Item("Column3") = "0" 'IsBold
                dtSummary.Rows.Add(rpt_Rows)

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 43, "OMITTED STAFF")
                rpt_Rows.Item("Column2") = "(" & Format(TotalAmtOldStaff, strfmtCurrency) & ")"
                rpt_Rows.Item("Column3") = "0" 'IsBold
                dtSummary.Rows.Add(rpt_Rows)

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 44, "CHANGES EARNINGS")
                rpt_Rows.Item("Column3") = "1" 'IsBold
                rpt_Rows.Item("Column2") = ""
                dtSummary.Rows.Add(rpt_Rows)

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 45, "PREVIOUS AMOUNT")
                rpt_Rows.Item("Column2") = "(" & Format(TotalPrevAmt, strfmtCurrency) & ")"
                rpt_Rows.Item("Column3") = "0" 'IsBold
                dtSummary.Rows.Add(rpt_Rows)

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = Language.getMessage(mstrModuleName, 46, "CURRENT AMOUNT")
                rpt_Rows.Item("Column2") = Format(TotalCurrAmt, strfmtCurrency)
                rpt_Rows.Item("Column3") = "0" 'IsBold
                dtSummary.Rows.Add(rpt_Rows)

                rpt_Rows = dtSummary.NewRow
                rpt_Rows.Item("Column1") = mstrPeriodName2 & " " & Language.getMessage(mstrModuleName, 41, "BASIC SALARY")
                rpt_Rows.Item("Column2") = Format(CDec(Format(CDec(dsRow.Item("Amount")), strfmtCurrency)) + CDec(Format(TotalAmtNewStaff, strfmtCurrency)) - CDec(Format(TotalAmtOldStaff, strfmtCurrency)) - CDec(Format(TotalPrevAmt, strfmtCurrency)) + CDec(Format(TotalCurrAmt, strfmtCurrency)), strfmtCurrency)
                rpt_Rows.Item("Column3") = "1" 'IsBold
                dtSummary.Rows.Add(rpt_Rows)

            Next
            'Sohail (13 Jan 2020) -- End

            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            'objRpt = New ArutiReport.Designer.rptPayrollTotalVariance
            If mblnShowVariancePercentageColumns = True Then
                objRpt = New ArutiReport.Designer.rptPayrollTotalVarianceWithPercentage
            Else
                objRpt = New ArutiReport.Designer.rptPayrollTotalVariance
            End If
            'Hemant (22 Jan 2019) -- End

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 24, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 12, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 13, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 14, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            objRpt.SetDataSource(rpt_Data)

            'Sohail (13 Jan 2020) -- Start
            'NMB Enhancement # : Informational head selection with save settings option and previuos month and current month salaries summary and detail on payroll total variance report.
            If mblnShowVariancePercentageColumns = True Then
                objRpt.Subreports("rptSubNewStaff2").SetDataSource(dtNewStaffSalary)
                objRpt.Subreports("rptSubNewStaff1").SetDataSource(dtNewStaffSalary)
                objRpt.Subreports("rptSubOldStaff2").SetDataSource(dtOldStaffSalary)
                objRpt.Subreports("rptSubOldStaff1").SetDataSource(dtOldStaffSalary)
                objRpt.Subreports("rptSalaryChange1").SetDataSource(dtSalaryChange)
                objRpt.Subreports("rptSalaryChange2").SetDataSource(dtSalaryChange)
                objRpt.Subreports("rptSummary").SetDataSource(dtSummary)

                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtNewStaff", Language.getMessage(mstrModuleName, 28, "NEW STAFF FOR THE MONTH OF") & " " & mstrPeriodName2.ToUpper)
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtTotal", Language.getMessage(mstrModuleName, 29, "Total"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtSrNo", Language.getMessage(mstrModuleName, 30, "SNo"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtEmpCode", Language.getMessage(mstrModuleName, 31, "EmpFn"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtEmpName", Language.getMessage(mstrModuleName, 32, "Employee Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtDesignation", Language.getMessage(mstrModuleName, 33, "Designation"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtDepartment", Language.getMessage(mstrModuleName, 34, "Department"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtPrevious", Language.getMessage(mstrModuleName, 35, "Previous"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtCurrent", Language.getMessage(mstrModuleName, 36, "Current"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff2"), "txtDifference", Language.getMessage(mstrModuleName, 37, "Difference"))

                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtNewStaff", Language.getMessage(mstrModuleName, 28, "NEW STAFF FOR THE MONTH OF") & " " & mstrPeriodName2.ToUpper)
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtTotal", Language.getMessage(mstrModuleName, 29, "Total"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtSrNo", Language.getMessage(mstrModuleName, 30, "SNo"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtEmpCode", Language.getMessage(mstrModuleName, 31, "EmpFn"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtPrevious", Language.getMessage(mstrModuleName, 35, "Previous"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtCurrent", Language.getMessage(mstrModuleName, 36, "Current"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubNewStaff1"), "txtDifference", Language.getMessage(mstrModuleName, 37, "Difference"))

                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtOmittedStaff", Language.getMessage(mstrModuleName, 38, "OMITTED STAFF FOR THE MONTH OF") & " " & mstrPeriodName2.ToUpper)
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtTotal", Language.getMessage(mstrModuleName, 29, "Total"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtSrNo", Language.getMessage(mstrModuleName, 30, "SNo"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtEmpCode", Language.getMessage(mstrModuleName, 31, "EmpFn"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtEmpName", Language.getMessage(mstrModuleName, 32, "Employee Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtDesignation", Language.getMessage(mstrModuleName, 33, "Designation"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtDepartment", Language.getMessage(mstrModuleName, 34, "Department"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtPrevious", Language.getMessage(mstrModuleName, 35, "Previous"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtCurrent", Language.getMessage(mstrModuleName, 36, "Current"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff2"), "txtDifference", Language.getMessage(mstrModuleName, 37, "Difference"))

                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtOmittedStaff", Language.getMessage(mstrModuleName, 38, "OMITTED STAFF FOR THE MONTH OF") & " " & mstrPeriodName2.ToUpper)
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtTotal", Language.getMessage(mstrModuleName, 29, "Total"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtSrNo", Language.getMessage(mstrModuleName, 30, "SNo"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtEmpCode", Language.getMessage(mstrModuleName, 31, "EmpFn"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtPrevious", Language.getMessage(mstrModuleName, 35, "Previous"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtCurrent", Language.getMessage(mstrModuleName, 36, "Current"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSubOldStaff1"), "txtDifference", Language.getMessage(mstrModuleName, 37, "Difference"))

                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtOmittedStaff", Language.getMessage(mstrModuleName, 39, "CHANGE EARNINGS FOR THE MONTH OF") & " " & mstrPeriodName2.ToUpper)
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtTotal", Language.getMessage(mstrModuleName, 29, "Total"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtSrNo", Language.getMessage(mstrModuleName, 30, "SNo"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtEmpCode", Language.getMessage(mstrModuleName, 31, "EmpFn"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtEmpName", Language.getMessage(mstrModuleName, 32, "Employee Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtDesignation", Language.getMessage(mstrModuleName, 33, "Designation"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtDepartment", Language.getMessage(mstrModuleName, 34, "Department"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtPrevious", Language.getMessage(mstrModuleName, 35, "Previous"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtCurrent", Language.getMessage(mstrModuleName, 36, "Current"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange1"), "txtDifference", Language.getMessage(mstrModuleName, 37, "Difference"))

                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtOmittedStaff", Language.getMessage(mstrModuleName, 39, "CHANGE EARNINGS FOR THE MONTH OF") & " " & mstrPeriodName2.ToUpper)
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtTotal", Language.getMessage(mstrModuleName, 29, "Total"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtSrNo", Language.getMessage(mstrModuleName, 30, "SNo"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtEmpCode", Language.getMessage(mstrModuleName, 31, "EmpFn"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtPrevious", Language.getMessage(mstrModuleName, 35, "Previous"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtCurrent", Language.getMessage(mstrModuleName, 36, "Current"))
                Call ReportFunction.TextChange(objRpt.Subreports("rptSalaryChange2"), "txtDifference", Language.getMessage(mstrModuleName, 37, "Difference"))

                Call ReportFunction.TextChange(objRpt.Subreports("rptSummary"), "txtChecking", Language.getMessage(mstrModuleName, 40, "CHECKING BASIC SALARY FOR") & " " & mstrPeriodName2.ToUpper)
            End If
            'Sohail (13 Jan 2020) -- End

            Call ReportFunction.TextChange(objRpt, "txtPeriodName1", mstrPeriodName1)
            Call ReportFunction.TextChange(objRpt, "txtPeriodName2", mstrPeriodName2)
            Call ReportFunction.TextChange(objRpt, "txtTranCode", Language.getMessage(mstrModuleName, 16, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtTranName", Language.getMessage(mstrModuleName, 17, "Transaction Head"))
            Call ReportFunction.TextChange(objRpt, "txtVariance", Language.getMessage(mstrModuleName, 18, "Variance"))
            'Sohail (21 Jan 2017) -- Start
            'CCBRT Enhancement - 64.1 - Show employee count for from period, to period and variance.
            Call ReportFunction.TextChange(objRpt, "txtCount1", Language.getMessage(mstrModuleName, 20, "Count"))
            Call ReportFunction.TextChange(objRpt, "txtCount2", Language.getMessage(mstrModuleName, 21, "Count"))
            Call ReportFunction.TextChange(objRpt, "txtCount3", Language.getMessage(mstrModuleName, 22, "Count"))
            'Sohail (21 Jan 2017) -- End

            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            If mblnShowVariancePercentageColumns = True Then
                Call ReportFunction.TextChange(objRpt, "txtVariancePercentage", Language.getMessage(mstrModuleName, 25, "Variance(%)"))
                Call ReportFunction.TextChange(objRpt, "txtCountPercentage", Language.getMessage(mstrModuleName, 26, "Count(%)"))
            End If
            'Hemant (22 Jan 2019) -- End

            'Hemant (22 June 2019) -- Start
            'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 27, "Total :"))
            'Hemant (22 June 2019) -- End

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 19, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 6, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 7, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            'Pinkal (15-Jan-2013) -- Start
            'Enhancement : TRA Changes
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
            End If




            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 1, "Tran Code")
                mdtTableExcel.Columns("Column1").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 17, "Transaction Head")
                mdtTableExcel.Columns("Column2").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column9").Caption = ""
                mdtTableExcel.Columns("Column9").SetOrdinal(mintColumn)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = mstrPeriodName1
                mdtTableExcel.Columns("Column81").SetOrdinal(mintColumn)
                mintColumn += 1

                'Sohail (21 Jan 2017) -- Start
                'CCBRT Enhancement - 64.1 - Show employee count for from period, to period and variance.
                mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 20, "Count")
                mdtTableExcel.Columns("Column84").SetOrdinal(mintColumn)
                mintColumn += 1
                'Sohail (21 Jan 2017) -- End

                mdtTableExcel.Columns("Column82").Caption = mstrPeriodName2
                mdtTableExcel.Columns("Column82").SetOrdinal(mintColumn)
                mintColumn += 1

                'Sohail (21 Jan 2017) -- Start
                'CCBRT Enhancement - 64.1 - Show employee count for from period, to period and variance.
                mdtTableExcel.Columns("Column85").Caption = Language.getMessage(mstrModuleName, 21, "Count")
                mdtTableExcel.Columns("Column85").SetOrdinal(mintColumn)
                mintColumn += 1
                'Sohail (21 Jan 2017) -- End

                mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 5, "Variance")
                mdtTableExcel.Columns("Column83").SetOrdinal(mintColumn)
                mintColumn += 1

                'Sohail (21 Jan 2017) -- Start
                'CCBRT Enhancement - 64.1 - Show employee count for from period, to period and variance.
                mdtTableExcel.Columns("Column86").Caption = Language.getMessage(mstrModuleName, 22, "Count")
                mdtTableExcel.Columns("Column86").SetOrdinal(mintColumn)
                mintColumn += 1
                'Sohail (21 Jan 2017) -- End

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column11").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Replace(":", "").Trim, mstrReport_GroupName)
                    mdtTableExcel.Columns("Column11").SetOrdinal(mintColumn)
                    mintColumn += 1
                End If



                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next

            End If

            'Pinkal (15-Jan-2013) -- End

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

#Region "Messages"
    '2, "Code"
    '3, "Transaction Head"
    '4, "Variance"
    '5, "Printed By :"
    '6, "Printed Date :"
    '7, "Page :"
    '8, " Period From : "
    '9, " To : "
    '10, " Order by : "
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Tran Code")
            Language.setMessage(mstrModuleName, 2, "Tran Name")
            Language.setMessage(mstrModuleName, 3, "Period_1 Amount")
            Language.setMessage(mstrModuleName, 4, "Period_2 Amount")
            Language.setMessage(mstrModuleName, 5, "Variance")
            Language.setMessage(mstrModuleName, 6, "Printed Date :")
            Language.setMessage(mstrModuleName, 7, "Page :")
            Language.setMessage(mstrModuleName, 8, " Period From :")
            Language.setMessage(mstrModuleName, 9, " To :")
            Language.setMessage(mstrModuleName, 10, " Order by :")
            Language.setMessage(mstrModuleName, 11, "Sub Total")
            Language.setMessage(mstrModuleName, 12, "Checked By :")
            Language.setMessage(mstrModuleName, 13, "Approved By :")
            Language.setMessage(mstrModuleName, 14, "Received By :")
            Language.setMessage(mstrModuleName, 15, "Opening B/F")
            Language.setMessage(mstrModuleName, 16, "Code")
            Language.setMessage(mstrModuleName, 17, "Transaction Head")
            Language.setMessage(mstrModuleName, 18, "Variance")
            Language.setMessage(mstrModuleName, 19, "Printed By :")
            Language.setMessage(mstrModuleName, 20, "Count")
            Language.setMessage(mstrModuleName, 21, "Count")
            Language.setMessage(mstrModuleName, 22, "Count")
            Language.setMessage(mstrModuleName, 23, "Grand Total :")
            Language.setMessage(mstrModuleName, 24, "Prepared By :")
            Language.setMessage(mstrModuleName, 25, "Variance(%)")
            Language.setMessage(mstrModuleName, 26, "Count(%)")
            Language.setMessage(mstrModuleName, 27, "Total :")
            Language.setMessage(mstrModuleName, 28, "NEW STAFF FOR THE MONTH OF")
            Language.setMessage(mstrModuleName, 29, "Total")
            Language.setMessage(mstrModuleName, 30, "SNo")
            Language.setMessage(mstrModuleName, 31, "EmpFn")
            Language.setMessage(mstrModuleName, 32, "Employee Name")
            Language.setMessage(mstrModuleName, 33, "Designation")
            Language.setMessage(mstrModuleName, 34, "Department")
            Language.setMessage(mstrModuleName, 35, "Previous")
            Language.setMessage(mstrModuleName, 36, "Current")
            Language.setMessage(mstrModuleName, 37, "Difference")
            Language.setMessage(mstrModuleName, 38, "OMITTED STAFF FOR THE MONTH OF")
            Language.setMessage(mstrModuleName, 39, "CHANGE EARNINGS FOR THE MONTH OF")
            Language.setMessage(mstrModuleName, 40, "CHECKING BASIC SALARY FOR")
            Language.setMessage(mstrModuleName, 41, "BASIC SALARY")
            Language.setMessage(mstrModuleName, 42, "NEW EMPLOYEES")
            Language.setMessage(mstrModuleName, 43, "OMITTED STAFF")
            Language.setMessage(mstrModuleName, 44, "CHANGES EARNINGS")
            Language.setMessage(mstrModuleName, 45, "PREVIOUS AMOUNT")
            Language.setMessage(mstrModuleName, 46, "CURRENT AMOUNT")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
