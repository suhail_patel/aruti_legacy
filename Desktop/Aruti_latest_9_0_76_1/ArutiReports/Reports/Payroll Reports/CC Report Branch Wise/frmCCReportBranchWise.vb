#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCCReportBranchWise

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCCReportBranchWise"
    Private objCCBranchWise As clsCCReportBranchWise

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty

    Private mstrBranchIDs As String = String.Empty
    Private mstrBranchNAMEs As String = String.Empty

    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty

    Private mstrFromDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrAdvanceFilter As String = String.Empty

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Private marrCostCenterReportBranchwiseGroupHeadsIds As New ArrayList
    Private mstrTransactionHeadIds As String = String.Empty
    'Hemant (15 June 2019) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        objCCBranchWise = New clsCCReportBranchWise(User._Object._Languageunkid,Company._Object._Companyunkid)
        objCCBranchWise.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Branch = 1
        'Hemant (15 June 2019) -- Start
        'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
        IncludeInactiveEmployee = 2
        IgnoreZeroValueHeads = 3
        TransactionHeadIds = 4
        'Hemant (15 June 2019) -- End
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objCC As New clscostcenter_master
        Dim objExRate As New clsExchangeRate
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet

        Try

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With
            objperiod = Nothing

            dsList = objCC.getComboList("List", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If

            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            dsList = objMaster.getComboListForHeadType("HeadType")
            Dim dr As DataRow = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -1
            dr.Item("Name") = Language.getMessage(mstrModuleName, 8, "Loan")
            dsList.Tables("HeadType").Rows.Add(dr)
            dr = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -2
            dr.Item("Name") = Language.getMessage(mstrModuleName, 9, "Advance")
            dsList.Tables("HeadType").Rows.Add(dr)
            dr = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -3
            dr.Item("Name") = Language.getMessage(mstrModuleName, 10, "Savings")
            dsList.Tables("HeadType").Rows.Add(dr)
            dr = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -4
            dr.Item("Name") = Language.getMessage(mstrModuleName, 11, "Claims & Expenses")
            dsList.Tables("HeadType").Rows.Add(dr)
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("HeadType")
                .SelectedValue = 0
            End With
            'Hemant (15 June 2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objCC = Nothing
            objExRate = Nothing
            objMaster = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objBranch As New clsStation
        Dim dsList As DataSet
        Dim lvItem As ListViewItem
        Try

            dsList = objBranch.GetList("List", False)

            For Each dsRow As DataRow In dsList.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("stationunkid"))

                If mstrBranchIDs.Trim <> "" Then
                    If mstrBranchIDs.Split(",").Contains(dsRow.Item("stationunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If

                lvItem.SubItems.Add(dsRow.Item("code").ToString)

                lvItem.SubItems.Add(dsRow.Item("name").ToString)

                RemoveHandler lvBranch.ItemChecked, AddressOf lvBranch_ItemChecked
                lvBranch.Items.Add(lvItem)
                AddHandler lvBranch.ItemChecked, AddressOf lvBranch_ItemChecked
            Next


            If lvBranch.Items.Count > 6 Then
                colhName.Width = 200 - 18
            Else
                colhName.Width = 200
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objBranch = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            'objCCBranchWise.setDefaultOrderBy(0)
            'txtOrderBy.Text = objCCBranchWise.OrderByDisplay

            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod

            cboCostCenter.SelectedValue = 0
            chkInactiveemp.Checked = False

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            chkIgnorezeroHead.Checked = True

            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            mstrCurrency_Rate = ""

            mstrAdvanceFilter = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.CostCenter_BranchWise_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Branch
                            mstrBranchIDs = dsRow.Item("transactionheadid").ToString

                            'Hemant (15 June 2019) -- Start
                            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IgnoreZeroValueHeads
                            chkIgnorezeroHead.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TransactionHeadIds
                            mstrTransactionHeadIds = dsRow.Item("transactionheadid").ToString
                            'Hemant (15 June 2019) -- End


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheck As Boolean)
        Try
            For Each lvItem As ListViewItem In lvBranch.Items
                lvItem.Checked = blnCheck
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Return False
            'Nilay (18-Mar-2015) -- End

            If cboPeriod.SelectedValue <= 0 Then 'Nilay (18-Mar-2015) -- ElseIf cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No exchange rate defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
                'ElseIf lvBranch.CheckedItems.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one Branch from the list."), enMsgBoxStyle.Information)
                '    lvBranch.Focus()
                '    Return False
            End If

            Dim lstID As List(Of String) = (From p In lvBranch.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            mstrBranchIDs = String.Join(",", lstID.ToArray)
            Dim lstNAME As List(Of String) = (From p In lvBranch.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhName.Index).Text)).ToList
            mstrBranchNAMEs = String.Join(",", lstNAME.ToArray)


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Private Sub FillCustomHeads()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            Dim objLoan As New clsLoan_Scheme
            Dim objSaving As New clsSavingScheme
            Dim objExpense As New clsExpense_Master
            Dim dsLoanIN As DataSet = Nothing
            Dim dsLoanNotIN As DataSet = Nothing
            Dim dsSavingIN As DataSet = Nothing
            Dim dsSavingNotIN As DataSet = Nothing
            Dim dsCRNotIN As DataSet = Nothing
            Dim dsCRIN As DataSet = Nothing
            Dim dtTable As New DataTable
            Dim lvItem As ListViewItem
            lvCustomTranHead.Items.Clear()

            marrCostCenterReportBranchwiseGroupHeadsIds.Clear()
            If mstrTransactionHeadIds.Trim <> "" Then
                marrCostCenterReportBranchwiseGroupHeadsIds.AddRange(mstrTransactionHeadIds.Split(","))
            End If

            Dim lvArray As New List(Of ListViewItem)
            lvCustomTranHead.BeginUpdate()

            Dim arrHead() As String = (From p As String In marrCostCenterReportBranchwiseGroupHeadsIds Where (IsNumeric(p)) Select (p.ToString)).ToArray
            Dim arrLoan() As String = (From p As String In marrCostCenterReportBranchwiseGroupHeadsIds Where (p.StartsWith("Loan")) Select (p.Substring(4).ToString)).ToArray
            Dim arrAdvance() As String = (From p As String In marrCostCenterReportBranchwiseGroupHeadsIds Where (p.StartsWith("Advance")) Select (p.Substring(7).ToString)).ToArray
            Dim arrSaving() As String = (From p As String In marrCostCenterReportBranchwiseGroupHeadsIds Where (p.StartsWith("Saving")) Select (p.Substring(6).ToString)).ToArray
            Dim arrCR() As String = (From p As String In marrCostCenterReportBranchwiseGroupHeadsIds Where (p.StartsWith("CR")) Select (p.Substring(2).ToString)).ToArray
            Dim aLoan As New Dictionary(Of String, DataRow)
            Dim aAdvance As New Dictionary(Of String, DataRow)
            Dim aSaving As New Dictionary(Of String, DataRow)
            Dim aCR As New Dictionary(Of String, DataRow)
            Dim strNonHeadFilter As String = ""
            If CInt(cboTrnHeadType.SelectedValue) < 0 Then
                strNonHeadFilter = " AND 1 = 2 "
            End If

            Dim ds As DataSet
            If arrHead.Length > 0 Then
                'Hemant (12 Jul 2019) -- Start
                'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) - Gross Pay, Total Deduction, Net Pay & Total Net Pay are appearing in the report all are not required items.
                'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", marrCostCenterReportBranchwiseGroupHeadsIds.ToArray(Type.GetType("System.String"))) & ")" & strNonHeadFilter)
                'ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", marrCostCenterReportBranchwiseGroupHeadsIds.ToArray(Type.GetType("System.String"))) & ")")
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", arrHead) & ")" & strNonHeadFilter)
                ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", arrHead) & ")")
                'Hemant (12 Jul 2019) -- End               
            Else
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , If(strNonHeadFilter.Trim <> "", strNonHeadFilter.Substring(4), ""))
                ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , )
            End If
            Dim a As Dictionary(Of String, DataRow) = (From p In ds.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) x.ID, Function(y) y.DATAROW)
            If arrLoan.Length > 0 Then
                dsLoanNotIN = objLoan.getComboList(False, "Loan", , "lnloan_scheme_master.loanschemeunkid NOT IN (" & String.Join(",", arrLoan) & ") ")
                dsLoanIN = objLoan.getComboList(False, "Loan", , "lnloan_scheme_master.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") ")
                dsLoanNotIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanNotIN.Tables(0).Columns("Code").ColumnName = "code"

                dsLoanIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanIN.Tables(0).Columns("Code").ColumnName = "code"

                aLoan = (From p In dsLoanIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "Loan" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aLoan).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -1 Then
                dsLoanNotIN = objLoan.getComboList(False, "Loan")
                dsLoanNotIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanNotIN.Tables(0).Columns("Code").ColumnName = "code"
            End If

            'Saving
            If arrSaving.Length > 0 Then
                dsSavingNotIN = objSaving.getComboList(False, "Saving", , "svsavingscheme_master.savingschemeunkid NOT IN (" & String.Join(",", arrSaving) & ") ")
                dsSavingIN = objSaving.getComboList(False, "Saving", , "svsavingscheme_master.savingschemeunkid  IN (" & String.Join(",", arrSaving) & ") ")
                dsSavingNotIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"

                dsSavingIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"

                aSaving = (From p In dsSavingIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "Saving" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aSaving).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                dsSavingNotIN = objSaving.getComboList(False, "Saving")
                dsSavingNotIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"
            End If

            'Claim & Request
            If arrCR.Length > 0 Then
                'Hemant (12 Jul 2019) -- Start
                'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) - Gross Pay, Total Deduction, Net Pay & Total Net Pay are appearing in the report all are not required items.
                'dsCRNotIN = objExpense.getComboList(-1, False, "CR", "cmclaim_request_master.crmasterunkid NOT IN (" & String.Join(",", arrCR) & ") ")
                'dsCRIN = objExpense.getComboList(-1, False, "CR", , "cmclaim_request_master.crmasterunkid  IN (" & String.Join(",", arrCR) & ") ")
                'dsCRNotIN.Tables(0).Columns("crmasterunkid").ColumnName = "tranheadunkid"

                'dsCRIN.Tables(0).Columns("crmasterunkid").ColumnName = "tranheadunkid"
                'aCR = (From p In dsCRNotIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "CR" + x.ID, Function(y) y.DATAROW)

                'Hemant (30 Jul 2019) -- Start
                'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  informational heads are needed on report
                'dsCRNotIN = objExpense.getComboList(-1, False, "CR", , , , "cmexpense_master.expenseunkid NOT IN (" & String.Join(",", arrCR) & ") ")
                'dsCRIN = objExpense.getComboList(-1, False, "CR", , , , "cmexpense_master.expenseunkid  IN (" & String.Join(",", arrCR) & ") ")
                dsCRNotIN = objExpense.getComboList(-1, False, "CR", , , , " trnheadtype_id IN ( " & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.Informational & " ) AND cmexpense_master.expenseunkid NOT IN (" & String.Join(",", arrCR) & ") ")
                dsCRIN = objExpense.getComboList(-1, False, "CR", , , , " trnheadtype_id IN ( " & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.Informational & " ) AND cmexpense_master.expenseunkid  IN (" & String.Join(",", arrCR) & ") ")
                'Hemant (30 Jul 2019) -- End               
                dsCRNotIN.Tables(0).Columns("id").ColumnName = "tranheadunkid"
                dsCRIN.Tables(0).Columns("id").ColumnName = "tranheadunkid"
                dsCRNotIN.Tables(0).Columns("Code").ColumnName = "code"
                dsCRIN.Tables(0).Columns("Code").ColumnName = "code"
                dsCRNotIN.Tables(0).Columns("Name").ColumnName = "name"
                dsCRIN.Tables(0).Columns("Name").ColumnName = "name"
                Dim dvCRIN As DataView = New DataView(dsCRIN.Tables("CR"))
                dsCRIN.Tables.Remove(dsCRIN.Tables("CR"))
                dsCRIN.Merge(dvCRIN.ToTable("CR", False, "tranheadunkid", "code", "name"))
                Dim dvCRNotIN As DataView = New DataView(dsCRNotIN.Tables("CR"))
                dsCRNotIN.Tables.Remove(dsCRNotIN.Tables("CR"))
                dsCRNotIN.Merge(dvCRNotIN.ToTable("CR", False, "tranheadunkid", "code", "name"))
                aCR = (From p In dsCRIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "CR" + x.ID, Function(y) y.DATAROW)
                'Hemant (12 Jul 2019) -- End
                a = a.Union(aCR).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -4 Then
                'Hemant (30 Jul 2019) -- Start
                'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  informational heads are needed on report
                'dsCRNotIN = objExpense.getComboList(-1, False, "CR")
                dsCRNotIN = objExpense.getComboList(-1, False, "CR", , , , " trnheadtype_id IN ( " & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.Informational & " ) ")
                'Hemant (30 Jul 2019) -- End
                dsCRNotIN.Tables(0).Columns("id").ColumnName = "tranheadunkid"
                'Hemant (12 Jul 2019) -- Start
                'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) - Gross Pay, Total Deduction, Net Pay & Total Net Pay are appearing in the report all are not required items.
                dsCRNotIN.Tables(0).Columns("Code").ColumnName = "code"
                dsCRNotIN.Tables(0).Columns("Name").ColumnName = "name"
                'Hemant (12 Jul 2019) -- End
            End If

            If dsLoanNotIN IsNot Nothing Then
                Dim dc As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc.DefaultValue = -1
                dsLoanNotIN.Tables(0).Columns.Add(dc)
                dsLoanNotIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsLoanIN IsNot Nothing Then
                Dim dc1 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc1.DefaultValue = -1
                dsLoanIN.Tables(0).Columns.Add(dc1)
                dsLoanIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsSavingNotIN IsNot Nothing Then
                Dim dc3 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc3.DefaultValue = -3
                dsSavingNotIN.Tables(0).Columns.Add(dc3)
                dsSavingNotIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsSavingIN IsNot Nothing Then
                Dim dc4 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc4.DefaultValue = -3
                dsSavingIN.Tables(0).Columns.Add(dc4)
                dsSavingIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsCRNotIN IsNot Nothing Then
                Dim dc5 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc5.DefaultValue = -4
                dsCRNotIN.Tables(0).Columns.Add(dc5)
                dsCRNotIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsCRIN IsNot Nothing Then
                Dim dc6 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc6.DefaultValue = -4
                dsCRIN.Tables(0).Columns.Add(dc6)
                dsCRIN.Tables(0).Columns("code").SetOrdinal(1)
            End If
            dtTable.Merge(dsList.Tables(0), True)
            If dsLoanNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -1 Then
                    dtTable.Merge(dsLoanNotIN.Tables(0), True)
                End If
            End If

            If dsSavingNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                    dtTable.Merge(dsSavingNotIN.Tables(0), True)
                End If
            End If

            If dsCRNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -4 Then
                    dtTable.Merge(dsCRNotIN.Tables(0), True)
                End If
            End If

            'Advance
            Dim dr_Advance As DataRow = dtTable.NewRow
            dr_Advance.Item("tranheadunkid") = 1
            dr_Advance.Item("code") = Language.getMessage(mstrModuleName, 9, "Advance")
            dr_Advance.Item("name") = Language.getMessage(mstrModuleName, 9, "Advance")
            dr_Advance.Item("trnheadtype_id") = -2

            If arrAdvance.Length > 0 Then
                a.Add("Advance1", dr_Advance)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -2 Then
                dtTable.Rows.Add(dr_Advance)
            End If

            dtTable = New DataView(dtTable, "", "name", DataViewRowState.CurrentRows).ToTable
            Dim intPos As Integer = 0
            For Each itm In marrCostCenterReportBranchwiseGroupHeadsIds
                Dim dr As DataRow = dtTable.NewRow
                dr.ItemArray = a.Item(itm).ItemArray
                dtTable.Rows.InsertAt(dr, intPos)
                intPos += 1
            Next

            For Each dsRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(dsRow.Item("code").ToString)
                lvItem.SubItems.Add(dsRow.Item("name").ToString)
                lvItem.SubItems.Add(dsRow.Item("trnheadtype_id").ToString)
                If arrHead.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) > 0 Then 'Transaction heads
                    lvItem.Checked = True
                ElseIf arrLoan.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -1 Then 'Loan
                    lvItem.Checked = True
                ElseIf arrAdvance.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -2 Then 'Advance
                    lvItem.Checked = True
                ElseIf arrSaving.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -3 Then 'Saving
                    lvItem.Checked = True
                    'Hemant (12 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) - Gross Pay, Total Deduction, Net Pay & Total Net Pay are appearing in the report all are not required items.
                ElseIf arrCR.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -4 Then 'CR
                    lvItem.Checked = True
                    'Hemant (12 Jul 2019) -- End
                Else
                    lvItem.Checked = False
                End If
                lvArray.Add(lvItem)
            Next

            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            lvCustomTranHead.Items.AddRange(lvArray.ToArray)
            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked

            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            End If
            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged

            If lvCustomTranHead.Items.Count > 15 Then
                colhCName.Width = 240 - 20
            Else
                colhCName.Width = 240
            End If
            lvArray = Nothing
            lvCustomTranHead.EndUpdate()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCustomHeads", mstrModuleName)
        End Try
    End Sub
    'Hemant (15 June 2019) -- End


#End Region

#Region " Form's Events "

    Private Sub frmCCReportBranchWise_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCCBranchWise = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCCReportBranchWise_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCCReportBranchWise_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            chkIgnorezeroHead.Checked = True
            Call FillCombo()
            Call ResetValue()
            Call FillList()
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            marrCostCenterReportBranchwiseGroupHeadsIds.Clear()
            If mstrTransactionHeadIds.Trim <> "" Then
                marrCostCenterReportBranchwiseGroupHeadsIds.AddRange(mstrTransactionHeadIds.Split(","))
            End If
            FillCustomHeads()
            'Hemant (15 June 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCCReportBranchWise_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCCReportBranchWise_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            ElseIf e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCCReportBranchWise_KeyDown", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Try

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            If lvCustomTranHead.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Please select atleast one transaction head in order to generate report."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Hemant (15 June 2019) -- End

            objCCBranchWise.SetDefaultValue()


            objCCBranchWise._BranchIDs = mstrBranchIDs
            objCCBranchWise._BranchNAMEs = mstrBranchNAMEs

            objCCBranchWise._EmployeeId = cboEmployee.SelectedValue
            objCCBranchWise._EmployeeName = cboEmployee.Text

            objCCBranchWise._PeriodId = cboPeriod.SelectedValue
            objCCBranchWise._PeriodName = cboPeriod.Text

            objCCBranchWise._IsActive = chkInactiveemp.Checked

            objCCBranchWise._CostCenterId = cboCostCenter.SelectedValue
            objCCBranchWise._CostCenterName = cboCostCenter.Text

            objCCBranchWise._ViewByIds = mstrStringIds
            objCCBranchWise._ViewIndex = mintViewIdx
            objCCBranchWise._ViewByName = mstrStringName
            objCCBranchWise._Analysis_Fields = mstrAnalysis_Fields
            objCCBranchWise._Analysis_Join = mstrAnalysis_Join
            objCCBranchWise._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCCBranchWise._Report_GroupName = mstrReport_GroupName

            objCCBranchWise._IgnoreZeroHeads = chkIgnorezeroHead.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objCCBranchWise._PeriodStartDate = objPeriod._Start_Date

            objCCBranchWise._PeriodEndDate = objPeriod._End_Date

            objCCBranchWise._Ex_Rate = mdecEx_Rate
            objCCBranchWise._Currency_Sign = mstrCurr_Sign
            objCCBranchWise._Currency_Rate = mstrCurrency_Rate

            objCCBranchWise._FromDatabaseName = mstrFromDatabaseName

            objCCBranchWise._Advance_Filter = mstrAdvanceFilter

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            Dim xCSVHeads As String = String.Empty
            xCSVHeads = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0, p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1, "Loan" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2, "Advance" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3, "Saving" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -4, "CR" + p.Tag.ToString, "").ToString).ToString).ToString).ToString).ToString)).ToArray)
            objCCBranchWise._CostCenterReportBranchwiseGroupHeadsIds = xCSVHeads

            Dim lstUnSelected As List(Of String) = (From lvItem In lvCustomTranHead.Items.Cast(Of ListViewItem)() Where lvItem.Checked = False Select (lvItem.Tag.ToString)).ToList
            Dim strUnSelectedIDs As String = ""
            If lstUnSelected.Count > 0 Then
                strUnSelectedIDs = String.Join(",", lstUnSelected.ToArray)
            End If
            objCCBranchWise._UnSelectedHeadIDs = strUnSelectedIDs
            'Hemant (15 June 2019) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objCCBranchWise.Generate_DetailReport()
            objCCBranchWise.Generate_DetailReport(mstrFromDatabaseName, User._Object._Userunkid, objPeriod._Yearunkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'For intHeadType As Integer = 1 To 1
            Dim xCheckedItems As String = String.Empty
            xCheckedItems = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0, p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1, "Loan" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2, "Advance" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3, "Saving" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -4, "CR" + p.Tag.ToString, "").ToString).ToString).ToString).ToString).ToString)).ToArray)
            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                'Hemant (15 June 2019) -- End
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.CostCenter_BranchWise_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Branch
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrBranchIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CostCenter_BranchWise_Report, 0, 0, intHeadType)

                        'Hemant (15 June 2019) -- Start
                        'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                    Case enHeadTypeId.IncludeInactiveEmployee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkInactiveemp.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CostCenter_BranchWise_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.IgnoreZeroValueHeads
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkIgnorezeroHead.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CostCenter_BranchWise_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TransactionHeadIds
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = xCheckedItems

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CostCenter_BranchWise_Report, 0, 0, intHeadType)
                        'Hemant (15 June 2019) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCCReportBranchWise.SetMessages()
            objfrm._Other_ModuleNames = "clsCCReportBranchWise"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvCustomTranHead.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvCustomTranHead.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvCustomTranHead.Items(SelIndex - 1)
                lvCustomTranHead.Items(SelIndex - 1) = lvCustomTranHead.Items(SelIndex).Clone
                lvCustomTranHead.Items(SelIndex) = tItem.Clone
                lvCustomTranHead.Items(SelIndex - 1).Selected = True
                lvCustomTranHead.Items(SelIndex - 1).EnsureVisible()
            End If
            lvCustomTranHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvCustomTranHead.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvCustomTranHead.SelectedIndices.Item(0)
                If SelIndex = lvCustomTranHead.Items.Count - 1 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvCustomTranHead.Items(SelIndex + 1)
                lvCustomTranHead.Items(SelIndex + 1) = lvCustomTranHead.Items(SelIndex).Clone
                lvCustomTranHead.Items(SelIndex) = tItem.Clone
                lvCustomTranHead.Items(SelIndex + 1).Selected = True
                lvCustomTranHead.Items(SelIndex + 1).EnsureVisible()
            End If
            lvCustomTranHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'Hemant (15 June 2019) -- End

#End Region

#Region " Other Control's Events "
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 5, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (16 Mar 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = ""
                End If
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try

    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objCCBranchWise.setOrderBy(0)
            txtOrderBy.Text = objCCBranchWise.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Private Sub txtSearchCHeads_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchCHeads.TextChanged
        Try
            If lvCustomTranHead.Items.Count <= 0 Then Exit Sub
            lvCustomTranHead.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvCustomTranHead.FindItemWithText(txtSearchCHeads.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvCustomTranHead.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchCHeads_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Hemant (15 June 2019) -- End
#End Region

#Region " List View Events "
    Private Sub lvBranch_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvBranch.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvBranch.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvBranch.CheckedItems.Count < lvBranch.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvBranch.CheckedItems.Count = lvBranch.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvBranch_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Private Sub lvCustomTranHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvCustomTranHead.ItemChecked
        Try
            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            End If

            marrCostCenterReportBranchwiseGroupHeadsIds.Clear()
            
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0) Select (p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1) Select ("Loan" + p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2) Select ("Advance" + p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3) Select ("Saving" + p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -4) Select ("CR" + p.Tag.ToString)).ToArray)
            
            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvCustomTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub
    'Hemant (15 June 2019) -- End

#End Region

#Region " Checkbox Events "
    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call CheckAll(CBool(objchkAllPeriod.Checked))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Private Sub objchkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheckAll.CheckedChanged
        Try
            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            For Each lvItem As ListViewItem In lvCustomTranHead.Items
                lvItem.Checked = objchkCheckAll.CheckState
            Next

            marrCostCenterReportBranchwiseGroupHeadsIds.Clear()
            
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0) Select (p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1) Select ("Loan" + p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2) Select ("Advance" + p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3) Select ("Saving" + p.Tag.ToString)).ToArray)
            marrCostCenterReportBranchwiseGroupHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -4) Select ("CR" + p.Tag.ToString)).ToArray)
            
            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (15 June 2019) -- End

#End Region

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
#Region " Combobox Events "
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Call FillCustomHeads()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Hemant (15 June 2019) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
            Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.Name, Me.chkIgnorezeroHead.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.gbCustomSetting.Text = Language._Object.getCaption(Me.gbCustomSetting.Name, Me.gbCustomSetting.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.colhCCode.Text = Language._Object.getCaption(CStr(Me.colhCCode.Tag), Me.colhCCode.Text)
			Me.colhCName.Text = Language._Object.getCaption(CStr(Me.colhCName.Tag), Me.colhCName.Text)
			Me.colhCHeadTypeID.Text = Language._Object.getCaption(CStr(Me.colhCHeadTypeID.Tag), Me.colhCHeadTypeID.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Period is compulsory infomation. Please select period to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, No exchange rate defined for this currency for the period selected.")
            Language.setMessage(mstrModuleName, 5, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Sorry, Please select atleast one transaction head in order to generate report.")
			Language.setMessage(mstrModuleName, 8, "Loan")
			Language.setMessage(mstrModuleName, 9, "Advance")
			Language.setMessage(mstrModuleName, 10, "Savings")
			Language.setMessage(mstrModuleName, 11, "Claims & Expenses")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
