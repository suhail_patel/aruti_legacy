'************************************************************************************************************************************
'Class Name : clsCCReportBranchWise.vb
'Purpose    :
'Date       :15/03/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter


''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsCCReportBranchWise
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCCReportBranchWise"
    Private mstrReportId As String = enArutiReport.CostCenter_BranchWise_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""


    Private mblnIsActive As Boolean = True

    Private mintCostCenterId As Integer = -1
    Private mstrCostCenterName As String = String.Empty

    Private mstrBranchIDs As String
    Private mstrBranchNAMEs As String

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mblnIgnorezeroHeads As Boolean = False

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurrency_Rate As String = String.Empty

    Private mstrFromDatabaseName As String

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    Private mstrAdvance_Filter As String = String.Empty

    Private mstrCurrentDatabaseName As String = FinancialYear._Object._DatabaseName
    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mblnSetPayslipPaymentApproval As Boolean = ConfigParameter._Object._SetPayslipPaymentApproval
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mstrUserAccessFilter As String = ""
    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    'Private mDicCostCenter As Dictionary(Of Integer, String)
    Private mstrCostCenterReportBranchwiseGroupHeadsIds As String = String.Empty
    Private mstrUnSelectedHeadIDs As String = String.Empty
    'Hemant (15 June 2019) -- End 
#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property


    Public WriteOnly Property _CostCenterId() As Integer
        Set(ByVal value As Integer)
            mintCostCenterId = value
        End Set
    End Property

    Public WriteOnly Property _CostCenterName() As String
        Set(ByVal value As String)
            mstrCostCenterName = value
        End Set
    End Property

    Public WriteOnly Property _BranchIDs() As String
        Set(ByVal value As String)
            mstrBranchIDs = value
        End Set
    End Property

    Public WriteOnly Property _BranchNAMEs() As String
        Set(ByVal value As String)
            mstrBranchNAMEs = value
        End Set
    End Property


    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IgnoreZeroHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnorezeroHeads = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Rate() As String
        Set(ByVal value As String)
            mstrCurrency_Rate = value
        End Set
    End Property

    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _CurrentDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrentDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _SetPayslipPaymentApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnSetPayslipPaymentApproval = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Hemant (15 June 2019) -- Start
    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
    Public WriteOnly Property _CostCenterReportBranchwiseGroupHeadsIds() As String
        Set(ByVal value As String)
            mstrCostCenterReportBranchwiseGroupHeadsIds = value
        End Set
    End Property

    Public WriteOnly Property _UnSelectedHeadIDs() As String
        Set(ByVal value As String)
            mstrUnSelectedHeadIDs = value
        End Set
    End Property
    'Hemant (15 June 2019) -- End
#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintPeriodId = -1
            mstrPeriodName = ""

            mblnIsActive = True


            mintCostCenterId = -1
            mstrCostCenterName = ""

            mstrBranchIDs = ""
            mstrBranchNAMEs = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mblnIgnorezeroHeads = False

            mstrCurrency_Sign = ""
            mdecEx_Rate = 0
            mstrCurrency_Rate = ""

            mstrFromDatabaseName = FinancialYear._Object._DatabaseName

            mstrAdvance_Filter = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Period : ") & " " & mstrPeriodName & " "
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Branch : ") & " " & mstrBranchNAMEs & " "

            If mintCostCenterId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Cost Center :") & " " & mstrCostCenterName & " "
            End If

            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Currency :") & " " & mstrCurrency_Sign & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Exchange Rate:") & " " & CDbl(mdecEx_Rate) & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function IgnoreZeroHead(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption
SetColumnCount:
            intColCount = objDataReader.Columns.Count
            For j As Integer = 0 To intColCount - 1
                If mblnIgnorezeroHeads Then

                    If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "GrpID" _
                           AndAlso objDataReader.Columns(j).ColumnName <> "GrpName" AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" Then

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = 0 OR " & objDataReader.Columns(j).ColumnName & " = 0.00")
                            If drRow.Length = objDataReader.Rows.Count Then
                                objDataReader.Columns.RemoveAt(j)
                                GoTo SetColumnCount
                            End If
                        End If

                    End If
                End If

            Next

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreZeroHead; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeeunkid,'')", Language.getMessage(mstrModuleName, 9, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 10, "Employee Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As Date _
                                     , ByVal xPeriodEnd As Date _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                     , ByVal blnApplyUserAccessFilter As Boolean _
                                     , ByVal strFmtCurrency As String _
                                     , ByVal intBase_CurrencyId As Integer _
                                     , ByVal strExportReportPath As String _
                                     , ByVal blnOpenAfterExport As Boolean _
                                     )
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, intBase_CurrencyId, strExportReportPath]

        Dim dsPayroll As New DataSet
        Dim dsDeduction As New DataSet
        Dim dtFinalTable As DataTable
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim objActivity As New clsActivity_Master
        Dim objExpense As New clsExpense_Master 'Sohail (12 Nov 2014)
        Try


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter.Trim = "" Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mstrFromDatabaseName = xDatabaseName
            mstrCurrentDatabaseName = xDatabaseName
            mstrfmtCurrency = strFmtCurrency
            mintBase_CurrencyId = intBase_CurrencyId
            mstrExportReportPath = strExportReportPath
            mblnOpenAfterExport = blnOpenAfterExport
            'Sohail (21 Aug 2015) -- End


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            dtFinalTable = New DataTable("Payroll")
            Dim dCol As DataColumn

            dtFinalTable.Columns.Add("PeriodId", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinalTable.Columns.Add("Period", System.Type.GetType("System.String")).DefaultValue = ""

            dCol = New DataColumn("CCCode")
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "Cost Center Code")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("CCName")
            dCol.Caption = Language.getMessage(mstrModuleName, 12, "Cost Center Name")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("GrpID")
            dCol.Caption = Language.getMessage(mstrModuleName, 13, "GroupID")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("GrpName")
            dCol.Caption = Language.getMessage(mstrModuleName, 14, "GroupName")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("HeadCount")
            dCol.Caption = Language.getMessage(mstrModuleName, 15, "Head Count")
            dCol.DataType = System.Type.GetType("System.Int64")
            dtFinalTable.Columns.Add(dCol)

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            Dim blnShowGrossPayColumn = False
            Dim blnShowTotalDeductionColumn = False
            Dim xHeadVal() As String = (From p As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",") Where (IsNumeric(p)) Select (p.ToString)).ToArray
            Dim arrLoan() As String = (From p As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",") Where (p.StartsWith("Loan")) Select (p.Substring(4).ToString)).ToArray
            Dim arrAdvance() As String = (From p As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",") Where (p.StartsWith("Advance")) Select (p.Substring(7).ToString)).ToArray
            Dim arrSaving() As String = (From p As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",") Where (p.StartsWith("Saving")) Select (p.Substring(6).ToString)).ToArray
            Dim arrCR() As String = (From p As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",") Where (p.StartsWith("CR")) Select (p.Substring(2).ToString)).ToArray

            If xHeadVal.Length <= 0 Then
                Dim tmp() As String = {"-999"}
                xHeadVal = tmp
            End If
            'Hemant (15 June 2019) -- End

            StrQ = "SELECT  ISNULL(prtranhead_master.trnheadname, '') AS Tname " & _
                          ", ISNULL(prtranhead_master.tranheadunkid, '') AS tranheadunkid " & _
                    "FROM    " & mstrCurrentDatabaseName & "..prtranhead_master " & _
                    "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                            "AND prtranhead_master.tranheadunkid IN ( " & String.Join(",", xHeadVal) & " ) " & _
                    "ORDER BY prtranhead_master.tranheadunkid "
            'Hemant (15 June 2019)

            Dim dsList As New DataSet
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("Tname")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            dsList = objActivity.getComboList("Activity", False, , enTranHeadType.EarningForEmployees)
            If dsList.Tables("Activity").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Activity").Rows
                    dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (29 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
            'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            Dim dsCR As DataSet = Nothing
            If arrCR.Length > 0 Then
                dsCR = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.EarningForEmployees)
            End If
            'Hemant (15 June 2019) -- End
            'Sohail (29 Mar 2017) -- End
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'If dsList.Tables("Expense").Rows.Count > 0 Then
            '    For Each dtRow As DataRow In dsList.Tables("Expense").Rows
            '        dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
            '        dCol.DataType = System.Type.GetType("System.Decimal")
            '        dCol.DefaultValue = 0
            '        dCol.Caption = dtRow.Item("name")

            '        dtFinalTable.Columns.Add(dCol)
            '    Next
            'End If
            For Each strID As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",")
                If strID.StartsWith("CR") = True Then
                    Dim dr() As DataRow = Nothing
                    dr = dsCR.Tables(0).Select("id = " & CInt(strID.Substring(2)) & " ")
                    If dr.Length > 0 Then
                        dCol = New DataColumn("ColumnCR" & dr(0).Item("id").ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dr(0).Item("name").ToString
                        dtFinalTable.Columns.Add(dCol)
                    End If
                End If
            Next
            'Hemant (15 June 2019) -- End

            'Sohail (12 Nov 2014) -- End

            '------------------ Add Gross Pay Column
            dCol = New DataColumn("TGP")
            dCol.Caption = "Gross Pay"
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            ''/* Deduction Part */

            StrQ = "SELECT  ISNULL(prtranhead_master.trnheadname, '') AS Tname " & _
                          ", ISNULL(prtranhead_master.tranheadunkid, '') AS tranheadunkid " & _
                    "FROM    " & mstrCurrentDatabaseName & "..prtranhead_master " & _
                    "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                             "AND prtranhead_master.tranheadunkid IN ( " & String.Join(",", xHeadVal) & " ) " & _
                    "ORDER BY prtranhead_master.tranheadunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.Caption = dtRow.Item("Tname")
                    dCol.DefaultValue = 0
                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            dsList = objActivity.getComboList("Activity", False, , enTranHeadType.DeductionForEmployee)
            If dsList.Tables("Activity").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("Activity").Rows
                    dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = dtRow.Item("name")

                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            'Sohail (29 Mar 2017) -- Start
            'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
            'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            If arrCR.Length > 0 Then
                dsCR = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
            End If
            'Hemant (15 June 2019) -- End
            'Sohail (29 Mar 2017) -- End
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'If dsList.Tables("Expense").Rows.Count > 0 Then
            '    For Each dtRow As DataRow In dsList.Tables("Expense").Rows
            '        dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
            '        dCol.DataType = System.Type.GetType("System.Decimal")
            '        dCol.DefaultValue = 0
            '        dCol.Caption = dtRow.Item("name")

            '        dtFinalTable.Columns.Add(dCol)
            '    Next
            'End If
            For Each strID As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",")
                If strID.StartsWith("CR") = True Then
                    Dim dr() As DataRow = Nothing
                    dr = dsCR.Tables(0).Select("id = " & CInt(strID.Substring(2)) & " ")
                    If dr.Length > 0 Then
                        dCol = New DataColumn("ColumnCR" & dr(0).Item("id").ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dr(0).Item("name").ToString
                        dtFinalTable.Columns.Add(dCol)
                    End If
                End If
            Next
            'Hemant (15 June 2019) -- End

            'Sohail (12 Nov 2014) -- End

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            ''------------ Loan
            'dCol = New DataColumn("Loan")
            'dCol.Caption = "Loan"
            'dCol.DefaultValue = 0
            'dCol.DataType = System.Type.GetType("System.Decimal")
            'dtFinalTable.Columns.Add(dCol)

            ''------------ Advance
            'dCol = New DataColumn("Advance")
            'dCol.Caption = "Advance"
            'dCol.DefaultValue = 0
            'dCol.DataType = System.Type.GetType("System.Decimal")
            'dtFinalTable.Columns.Add(dCol)

            ''------------ Savings
            'dCol = New DataColumn("Savings")
            'dCol.Caption = "Savings"
            'dCol.DefaultValue = 0
            'dCol.DataType = System.Type.GetType("System.Decimal")
            'dtFinalTable.Columns.Add(dCol)
            Dim dsLoan As DataSet = Nothing
            Dim dsSaving As DataSet = Nothing

            If arrLoan.Length > 0 Then
                Dim objLoan As New clsLoan_Scheme
                dsLoan = objLoan.getComboList(False, "List", , "lnloan_scheme_master.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") ")
            End If
            If arrSaving.Length > 0 Then
                Dim objSaving As New clsSavingScheme
                dsSaving = objSaving.getComboList(False, "List", , "svsavingscheme_master.savingschemeunkid IN (" & String.Join(",", arrSaving) & ") ")
            End If
            For Each strID As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",")
                Dim dr() As DataRow = Nothing
                If strID.StartsWith("Loan") = True Then
                    dr = dsLoan.Tables(0).Select("loanschemeunkid = " & CInt(strID.Substring(4)) & " ")
                    If dr.Length > 0 Then

                        dCol = New DataColumn("ColumnLoan" & dr(0).Item("loanschemeunkid").ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dr(0).Item("name").ToString
                        dtFinalTable.Columns.Add(dCol)
                    End If
                ElseIf strID.StartsWith("Advance") = True Then

                    dCol = New DataColumn("ColumnAdvance1")
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dCol.Caption = Language.getMessage(mstrModuleName, 19, "Advance")
                    dtFinalTable.Columns.Add(dCol)

                ElseIf strID.StartsWith("Saving") = True Then
                    dr = dsSaving.Tables(0).Select("savingschemeunkid = " & CInt(strID.Substring(6)) & " ")
                    If dr.Length > 0 Then
                        dCol = New DataColumn("ColumnSavings" & dr(0).Item("savingschemeunkid").ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dr(0).Item("name").ToString
                        dtFinalTable.Columns.Add(dCol)
                    End If

                End If

            Next
            'Hemant (15 June 2019) -- End

            '------------------ Add Total Deduction Column
            dCol = New DataColumn("TDD")
            dCol.DefaultValue = 0
            dCol.Caption = "Total Deduction"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            ''/* Informational Head Part */

            'Hemant (30 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Informational heads are needed on report
            StrQ = "SELECT  ISNULL(prtranhead_master.trnheadname, '') AS Tname " & _
                        ", ISNULL(prtranhead_master.tranheadunkid, '') AS tranheadunkid " & _
                  "FROM    " & mstrCurrentDatabaseName & "..prtranhead_master " & _
                  "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                          "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.Informational & ", " & enTranHeadType.EmployersStatutoryContributions & " ) " & _
                           "AND prtranhead_master.tranheadunkid IN ( " & String.Join(",", xHeadVal) & " ) " & _
                  "ORDER BY prtranhead_master.tranheadunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.Caption = dtRow.Item("Tname")
                    dCol.DefaultValue = 0
                    dtFinalTable.Columns.Add(dCol)
                Next
            End If

            If arrCR.Length > 0 Then
                dsCR = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.Informational)
            End If

            For Each strID As String In mstrCostCenterReportBranchwiseGroupHeadsIds.Split(",")
                If strID.StartsWith("CR") = True Then
                    Dim dr() As DataRow = Nothing
                    dr = dsCR.Tables(0).Select("id = " & CInt(strID.Substring(2)) & " ")
                    If dr.Length > 0 Then
                        dCol = New DataColumn("ColumnCR" & dr(0).Item("id").ToString)
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dr(0).Item("name").ToString
                        dtFinalTable.Columns.Add(dCol)
                    End If
                End If
            Next
            'Hemant (30 Jul 2019) -- End

            '------------------ Add Net Pay Column
            dCol = New DataColumn("NetPay")
            dCol.DefaultValue = 0
            dCol.Caption = "Net Pay"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Net B/F Column
            dCol = New DataColumn("Openingbalance")
            dCol.DefaultValue = 0
            dCol.Caption = "Net B/F"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Total Net Pay Column
            dCol = New DataColumn("TotNetPay")
            dCol.DefaultValue = 0
            dCol.Caption = "Total Net Pay"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            'Sohail (08 Mar 2017) -- Start
            'Issue - 64.1 - Head count and Head Amount Total  on Cost Center Report Branch wise was not matching with Payroll Summary / Payroll Report head count and Total Amount.
            '------------------ Add Net Pay Rounding Adjustment Column
            Dim objHead As New clsTransactionHead
            Dim intNetPayRoundingAdjustmentID As Integer = objHead.GetNetPayRoundingAdjustmentHeadID()
            If intNetPayRoundingAdjustmentID > 0 Then
                objHead._Tranheadunkid(mstrCurrentDatabaseName) = intNetPayRoundingAdjustmentID
                dCol = New DataColumn("Column" & intNetPayRoundingAdjustmentID.ToString)
                dCol.DefaultValue = 0
                dCol.Caption = objHead._Trnheadname
                dCol.DataType = System.Type.GetType("System.Decimal")
                dtFinalTable.Columns.Add(dCol)
            End If
            'Sohail (08 Mar 2017) -- End

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            StrQ = "SELECT  PeriodId " & _
                         ", period_name AS PeriodName " & _
                         ", payroll.stationunkid BranchID " & _
                         ", ISNULL(hrstation_master.code, '') AS BranchCode " & _
                         ", ISNULL(hrstation_master.name, '') AS BranchName " & _
                         ", payroll.costcenterunkid AS CCID " & _
                         ", ISNULL(prcostcenter_master.costcentercode, '') AS CCCode " & _
                         ", ISNULL(prcostcenter_master.costcentername, '') AS CCName " & _
                         ", TranId " & _
                         ", (Amount * " & mdecEx_Rate & ") AS Amount " & _
                         ", Mid " & _
                         ", (Openingbalance * " & mdecEx_Rate & ") AS Openingbalance "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= "FROM    ( "
            StrQ &= " SELECT DISTINCT " & _
                                        "payperiodunkid AS PeriodId " & _
                                      ", T.stationunkid " & _
                                      ", C.costcenterunkid " & _
                                      ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                                      ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                      ", 10 AS Mid " & _
                                      ", 0 AS Openingbalance " & _
                              "FROM      " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                        "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN " & mstrCurrentDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                  "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "
            'Sohail (08 Mar 2017) - [, openingbalance AS Openingbalance] = [, 0 AS Openingbalance]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End


            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                            "AND (prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") " & _
            '                            "AND payperiodunkid = @periodunkid "
            StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                        "AND ((prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " AND prtranhead_master.tranheadunkid IN ( " & String.Join(",", xHeadVal) & " ) ) OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") " & _
                                        "AND payperiodunkid = @periodunkid "
            'Hemant (15 June 2019) -- End
            'Sohail (08 Mar 2017) - [AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "] = [AND (prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR prtranhead_master.typeof_id = " & enTypeOf.NET_PAY_ROUNDING_ADJUSTMENT & ") ]

            If mstrBranchIDs.Trim <> "" Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                'Sohail (21 Aug 2015) -- End
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            If mintCostCenterId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
                'Sohail (21 Aug 2015) -- End
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid " & _
                                  ", prpayrollprocess_tran.tranheadunkid " & _
                                  "/*, openingbalance*/ " & _
                    "UNION ALL " & _
                    "SELECT    payperiodunkid AS PeriodId " & _
                            ", T.stationunkid " & _
                            ", C.costcenterunkid " & _
                            ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                            ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                            ", 0 AS Mid " & _
                            ", 0 AS Openingbalance " & _
                   "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrCurrentDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                      "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & " ) " & _
                            "AND payperiodunkid = @periodunkid "
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            StrQ &= "   AND prtranhead_master.tranheadunkid IN ( " & String.Join(",", xHeadVal) & " ) "
            'Hemant (15 June 2019) -- End
            If mstrBranchIDs.Trim <> "" Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                'Sohail (21 Aug 2015) -- End
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintCostCenterId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid " & _
                                  ", prpayrollprocess_tran.tranheadunkid " & _
                                  "/*, openingbalance*/ "

            'Hemant (30 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Informational heads are needed on report
            StrQ &= "UNION ALL " & _
                    "SELECT    payperiodunkid AS PeriodId " & _
                            ", T.stationunkid " & _
                            ", C.costcenterunkid " & _
                            ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                            ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                            ", 0 AS Mid " & _
                            ", 0 AS Openingbalance " & _
                   "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrCurrentDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                      "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                           "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.Informational & "," & enTranHeadType.EmployersStatutoryContributions & ") " & _
                            "AND payperiodunkid = @periodunkid "

            StrQ &= "   AND prtranhead_master.tranheadunkid IN ( " & String.Join(",", xHeadVal) & " ) "

            If mstrBranchIDs.Trim <> "" Then
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mintCostCenterId > 0 Then
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
            End If

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid " & _
                                  ", prpayrollprocess_tran.tranheadunkid "

            'Hemant (30 Jul 2019) -- End

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            If arrLoan.Length > 0 Then
                'Hemant (15 June 2019) -- End
                StrQ &= "UNION ALL " & _
                    "SELECT    payperiodunkid AS PeriodId " & _
                            ", T.stationunkid " & _
                            ", C.costcenterunkid " & _
                            ", lnloan_advance_tran.loanschemeunkid AS TranId " & _
                            ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                            ", 1 AS Mid " & _
                            ", 0 AS Openingbalance " & _
                    "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
                'Hemant (15 June 2019)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid AS costcenterunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                "AND lnloan_advance_tran.isloan = 1 " & _
                                    "AND payperiodunkid = @periodunkid " & _
                                    "AND lnloan_advance_tran.loanschemeunkid IN (" & String.Join(",", arrLoan) & ")"

                If mstrBranchIDs.Trim <> "" Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                    StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                    'Sohail (21 Aug 2015) -- End
                End If

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If


                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintCostCenterId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                    StrQ &= " AND C.costcenterunkid = @CostCenterId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= " GROUP BY  payperiodunkid " & _
                                      ", T.stationunkid " & _
                                      ", C.costcenterunkid " & _
                                          "/*, openingbalance*/ "
                'Hemant (15 June 2019) -- Start
                'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                StrQ &= ", lnloan_advance_tran.loanschemeunkid "
            End If

            If arrAdvance.Length > 0 Then
                'Hemant (15 June 2019) -- End

                StrQ &= "UNION ALL " & _
                    "SELECT    payperiodunkid AS PeriodId " & _
                            ", T.stationunkid " & _
                            ", C.costcenterunkid " & _
                                ", lnloan_advance_tran.loanschemeunkid AS TranId " & _
                            ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                            ", 2 AS Mid " & _
                            ", 0 AS Openingbalance " & _
                    "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                            "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "JOIN " & mstrFromDatabaseName & "..lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                        "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 "
                'Hemant (15 June 2019)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid AS costcenterunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                                "AND lnloan_advance_tran.isloan = 0 " & _
                                    "AND payperiodunkid  = @periodunkid " & _
                                    "AND lnloan_advance_tran.loanschemeunkid IN (" & String.Join(",", arrAdvance) & ")" 'Hemant (15 June 2019)

                If mstrBranchIDs.Trim <> "" Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                    StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                    'Sohail (21 Aug 2015) -- End
                End If

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintCostCenterId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                    StrQ &= " AND C.costcenterunkid = @CostCenterId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= " GROUP BY    payperiodunkid " & _
                                        ", T.stationunkid " & _
                                        ", C.costcenterunkid " & _
                                        "/*, openingbalance*/ " & _
                                            ",  lnloan_advance_tran.loanschemeunkid "
                'Hemant (15 June 2019) -- Start
                'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            End If

            If arrSaving.Length > 0 Then


                StrQ &= "UNION ALL " & _
                            "SELECT    prtnaleave_tran.payperiodunkid AS PeriodId " & _
                                ", T.stationunkid " & _
                                ", C.costcenterunkid " & _
                                    ", svsaving_tran.savingschemeunkid AS TranId " & _
                                ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                ", 3 AS Mid " & _
                                ", 0 AS Openingbalance " & _
                        "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "JOIN " & mstrFromDatabaseName & "..svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
                                                            "AND ISNULL(svsaving_tran.isvoid, 0) = 0 "
                'Hemant (15 June 2019) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid AS costcenterunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.savingtranunkid > 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                                        "AND svsaving_tran.savingschemeunkid IN (" & String.Join(",", arrSaving) & ")"  'Hemant (15 June 2019)
                If mstrBranchIDs.Trim <> "" Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                    StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                    'Sohail (21 Aug 2015) -- End
                End If

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '                  " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintCostCenterId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                    StrQ &= " AND C.costcenterunkid = @CostCenterId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= " GROUP BY    prtnaleave_tran.payperiodunkid " & _
                                    ", T.stationunkid " & _
                                    ", C.costcenterunkid " & _
                                        "/*, openingbalance*/ " & _
                                        ", svsaving_tran.savingschemeunkid "
            End If 'Hemant (15 June 2019)

            StrQ &= "UNION ALL " & _
                         "SELECT    payperiodunkid AS PeriodId " & _
                                 ", T.stationunkid " & _
                                 ", C.costcenterunkid " & _
                                 ", practivity_master.activityunkid AS TranId " & _
                                 ", SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                 ", CASE practivity_master.trnheadtype_id WHEN 1 THEN 11 ELSE 12 END AS Mid " & _
                                 ", 0 AS Openingbalance " & _
                         "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                 "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..practivity_master ON practivity_master.activityunkid =  prpayrollprocess_tran.activityunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                 "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                 "AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") " & _
                                 "AND payperiodunkid = @periodunkid "
            'Sohail (19 Aug 2014) - [AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") ]

            If mstrBranchIDs.Trim <> "" Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                'Sohail (21 Aug 2015) -- End
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'If mstrUserAccessFilter.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintCostCenterId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid " & _
                                  ", practivity_master.activityunkid " & _
                                  ", practivity_master.trnheadtype_id " & _
                                  ", prpayrollprocess_tran.activityunkid " & _
                                  "/*, openingbalance*/ "

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            StrQ &= "UNION ALL " & _
                         "SELECT    payperiodunkid AS PeriodId " & _
                                 ", T.stationunkid " & _
                                 ", C.costcenterunkid " & _
                                 ", ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS TranId " & _
                                 ", SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                                 ", CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 1 THEN 14 ELSE 15 END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN 14 ELSE 15 END END AS Mid " & _
                                 ", 0 AS Openingbalance " & _
                         "FROM   " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                 "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                 "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                 "LEFT JOIN " & mstrFromDatabaseName & "..cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                 "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                                 "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                                 "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                                 "AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                                 "AND 1 = CASE WHEN prpayrollprocess_tran.crprocesstranunkid > 0 AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.Informational & ") THEN 1 WHEN prpayrollprocess_tran.crretirementprocessunkid > 0 THEN 1 ELSE 0 END " & _
                                 "AND payperiodunkid = @periodunkid "
            'Sohail (09 Jun 2021) - [crretireexpense]
            'Hemant (30 Jul 2019) -- ["AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") " & _ --> "AND cmexpense_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.Informational & ") " & _ ]
            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            If arrCR.Length > 0 Then
                StrQ &= "AND  (cmexpense_master.expenseunkid IN (" & String.Join(",", arrCR) & ") OR crretireexpense.expenseunkid IN (" & String.Join(",", arrCR) & "))"
            Else
                StrQ &= " AND 1 = 2 "
            End If
            'Hemant (15 June 2019) -- End


            If mstrBranchIDs.Trim <> "" Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                'Sohail (21 Aug 2015) -- End
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'If mstrUserAccessFilter.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintCostCenterId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid " & _
                                  ", cmexpense_master.expenseunkid " & _
                                  ", cmexpense_master.trnheadtype_id " & _
                                  ", crretireexpense.expenseunkid " & _
                                  ", prpayrollprocess_tran.add_deduct " & _
                                  "/*, openingbalance*/ "
            'Sohail (09 Jun 2021) - [crretireexpense]
            'Sohail (12 Nov 2014) -- End

            StrQ &= "UNION ALL " & _
                        "SELECT    payperiodunkid AS PeriodId " & _
                                ", T.stationunkid " & _
                                ", C.costcenterunkid " & _
                                ", -888 AS TranId " & _
                                ", COUNT(DISTINCT prpayrollprocess_tran.employeeunkid) AS Amount " & _
                                ", -88 AS Mid " & _
                                ", 0 AS Openingbalance " & _
                       "FROM    " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                                "JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN " & mstrCurrentDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                          "AND ISNULL(prtranhead_master.isvoid, 0) = 0 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                "AND payperiodunkid = @periodunkid "

            If mstrBranchIDs.Trim <> "" Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= "AND stationunkid IN (" & mstrBranchIDs & ") "
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
                'Sohail (21 Aug 2015) -- End
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            'Sohail (08 Mar 2017) -- Start
            'Issue - 64.1 - Head count and Head Amount Total  on Cost Center Report Branch wise was not matching with Payroll Summary / Payroll Report head count and Total Amount.
            If mblnIgnorezeroHeads = True Then
                StrQ &= " AND prtnaleave_tran.total_amount + prtnaleave_tran.openingbalance <> 0 "
            End If
            'Sohail (08 Mar 2017) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'If mstrUserAccessFilter.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintCostCenterId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND hremployee_master.costcenterunkid = @CostCenterId "
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid " & _
                                  "/*, openingbalance*/ "

            'Sohail (08 Mar 2017) -- Start
            'Issue - 64.1 - Head count and Head Amount Total  on Cost Center Report Branch wise was not matching with Payroll Summary / Payroll Report head count and Total Amount.
            StrQ &= "UNION ALL " & _
                        "SELECT    payperiodunkid AS PeriodId " & _
                                ", T.stationunkid " & _
                                ", C.costcenterunkid " & _
                                ", -889 AS TranId " & _
                                ", 0 AS Amount " & _
                                ", -89 AS Mid " & _
                                ", SUM(CAST(prtnaleave_tran.openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) AS Openingbalance " & _
                       "FROM    " & mstrFromDatabaseName & "..prtnaleave_tran " & _
                                "JOIN " & mstrCurrentDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE   prtnaleave_tran.isvoid = 0 " & _
                                "AND payperiodunkid = @periodunkid "

            If mstrBranchIDs.Trim <> "" Then
                StrQ &= "AND T.stationunkid IN (" & mstrBranchIDs & ") "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mintCostCenterId > 0 Then
                StrQ &= " AND C.costcenterunkid = @CostCenterId "
            End If

            StrQ &= " GROUP BY  payperiodunkid " & _
                                  ", T.stationunkid " & _
                                  ", C.costcenterunkid "
            'Sohail (08 Mar 2017) -- End

            StrQ &= ") AS payroll " & _
                    "LEFT JOIN " & mstrCurrentDatabaseName & "..hrstation_master ON hrstation_master.stationunkid = payroll.stationunkid " & _
                    "LEFT JOIN " & mstrCurrentDatabaseName & "..prcostcenter_master ON prcostcenter_master.costcenterunkid = payroll.costcenterunkid " & _
                    "JOIN " & mstrCurrentDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                                                 "AND cfcommon_period_tran.isactive = 1 " & _
                                                 "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "

            'StrQ &= mstrAnalysis_Join


            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " WHERE " & mstrAdvance_Filter
            'End If

            'If mintViewIndex > 0 Then
            '    If Me.OrderByQuery <> "" Then
            '        StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", cfcommon_period_tran.end_date, hrstation_master.name, prcostcenter_master.costcentercode, " & Me.OrderByQuery & ", TranId "
            '    Else
            '        StrQ &= " ORDER BY " & mstrAnalysis_OrderBy & ", cfcommon_period_tran.end_date, hrstation_master.name, prcostcenter_master.costcentercode " & ", TranId "
            '    End If
            'Else
            '    If Me.OrderByQuery <> "" Then
            '        StrQ &= "ORDER BY cfcommon_period_tran.end_date, hrstation_master.name, prcostcenter_master.costcentercode, " & Me.OrderByQuery & ", TranId "
            '    Else
            '        StrQ &= "ORDER BY cfcommon_period_tran.end_date, hrstation_master.name, prcostcenter_master.costcentercode " & ", TranId "
            '    End If
            'End If
            'Sohail (08 Mar 2017) -- Start
            'Issue - 64.1 - Head count and Head Amount Total  on Cost Center Report Branch wise was not matching with Payroll Summary / Payroll Report head count and Total Amount.
            'StrQ &= "ORDER BY cfcommon_period_tran.end_date, hrstation_master.name, prcostcenter_master.costcentercode " & ", TranId "
            StrQ &= "ORDER BY cfcommon_period_tran.end_date, hrstation_master.name, ISNULL(prcostcenter_master.costcentername, ''), prcostcenter_master.costcentercode " & ", TranId "
            'Sohail (08 Mar 2017) -- End

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintCostCenterId > 0 Then
                objDataOperation.AddParameter("@CostCenterId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterId)
            End If

            dsPayroll = objDataOperation.ExecQuery(StrQ, "payroll")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'Hemant (12 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) - Gross Pay, Total Deduction, Net Pay & Total Net Pay are appearing in the report all are not required items.
            'For Each strID In mstrUnSelectedHeadIDs.Split(",")
            '    If dtFinalTable.Columns.Contains("Column" & strID) Then
            '        dtFinalTable.Columns.Remove("Column" & strID)
            '    End If
            'Next
            'Hemant (12 Jul 2019) -- End
            'Hemant (15 June 2019) -- End

            Dim strKey As String = ""
            Dim strPrevKey As String = ""
            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsPayroll.Tables("payroll").Rows.Count

            For i As Integer = 0 To intRowCount - 1

                drRow = dsPayroll.Tables("payroll").Rows(i)

                strKey = drRow.Item("PeriodId").ToString & "_" & drRow.Item("BranchID").ToString & "_" & drRow.Item("CCID")

                If strPrevKey <> strKey Then
                    rpt_Row = dtFinalTable.NewRow

                    rpt_Row.Item("PeriodId") = drRow.Item("PeriodId")
                    rpt_Row.Item("Period") = drRow.Item("PeriodName")

                    rpt_Row.Item("CCCode") = drRow.Item("CCCode")
                    rpt_Row.Item("CCName") = drRow.Item("CCName")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency) 'Sohail (18 Jun 2013)
                    rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    rpt_Row.Item("GrpID") = drRow.Item("BranchId")
                    rpt_Row.Item("GrpName") = drRow.Item("BranchName")
                Else
                    rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)
                End If


                If drRow.Item("Mid").ToString = "10" Then 'Earning
                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(Format(drRow.Item("Amount"), mstrfmtCurrency))
                    'Hemant (15 June 2019) -- Start
                    'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                    If intNetPayRoundingAdjustmentID <> CInt(drRow.Item("TranId")) Then
                        blnShowGrossPayColumn = True
                    End If
                    'Hemant (15 June 2019) -- End
                ElseIf drRow.Item("Mid").ToString = "11" Then 'Activity Earning
                    rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    blnShowGrossPayColumn = True 'Hemant (15 June 2019)
                    'Sohail (12 Nov 2014) -- Start
                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                ElseIf drRow.Item("Mid").ToString = "14" Then 'CR Expense Earning
                    rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    blnShowGrossPayColumn = True 'Hemant (15 June 2019)
                    'Sohail (12 Nov 2014) -- End

                    'Sohail (07 Jun 2021) -- Start
                    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                ElseIf drRow.Item("Mid").ToString = "16" Then 'Claim Retirement Expense Earning
                    rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                    blnShowGrossPayColumn = True
                    'Sohail (07 JUn 2021) -- End

                ElseIf drRow.Item("Mid").ToString = "-88" Then 'Head Count
                    'rpt_Row.Item("HeadCount") = CInt(drRow.Item("Amount"))
                    'Sohail (06 Mar 2017) -- Start
                    'Issue - 64.1 - Head count on Cost Center Report Branch wise was not matching with Payroll Summary head count.
                    If IsDBNull(rpt_Row.Item("HeadCount")) = True Then
                        rpt_Row.Item("HeadCount") = CInt(drRow.Item("Amount"))
                    Else
                        rpt_Row.Item("HeadCount") = CInt(rpt_Row.Item("HeadCount")) + CInt(drRow.Item("Amount"))
                    End If
                    'Sohail (06 Mar 2017) -- End

                    'Sohail (08 Mar 2017) -- Start
                    'Issue - 64.1 - Head count and Head Amount Total  on Cost Center Report Branch wise was not matching with Payroll Summary / Payroll Report head count and Total Amount.
                ElseIf drRow.Item("Mid").ToString = "-89" Then 'Opening Balance
                    'Do Nothing

                    'Sohail (08 Mar 2017) -- End
                Else
                    If drRow.Item("Mid").ToString = "1" Then   'LOAN
                        'Hemant (15 June 2019) -- Start
                        'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                        'rpt_Row.Item("Loan") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        rpt_Row.Item("ColumnLoan" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'Hemant (15 June 2019) -- End
                    ElseIf drRow.Item("Mid").ToString = "2" Then 'ADVANCE
                        'Hemant (15 June 2019) -- Start
                        'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                        'rpt_Row.Item("Advance") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        rpt_Row.Item("ColumnAdvance1") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'Hemant (15 June 2019) -- End
                    ElseIf drRow.Item("Mid").ToString = "3" Then 'SAVINGS
                        'Hemant (15 June 2019) -- Start
                        'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
                        'rpt_Row.Item("Savings") = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        rpt_Row.Item("ColumnSavings" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'Hemant (15 June 2019) -- End
                    ElseIf drRow.Item("Mid").ToString = "12" Then 'Activity Deduction
                        rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                    ElseIf drRow.Item("Mid").ToString = "15" Then 'CR Expense Deduction
                        rpt_Row.Item("ColumnCR" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                        'Sohail (12 Nov 2014) -- End

                    Else 'Deduction
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), mstrfmtCurrency)
                    End If
                    rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                    blnShowTotalDeductionColumn = True 'Hemant (15 June 2019)
                End If

                rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), mstrfmtCurrency)

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), GUI.fmtCurrency)
                rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End

                If strPrevKey <> strKey Then
                    dtFinalTable.Rows.Add(rpt_Row)
                End If
                strPrevKey = strKey
            Next

            'Hemant (15 June 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) : Add the Custom Settings  which will allow selection of the Transaction Heads to be shown in the report and non-selected transactions shouldn’t appear.
            'Hemant (12 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT#3863(GOOD NEIGHBORS) - Gross Pay, Total Deduction, Net Pay & Total Net Pay are appearing in the report all are not required items.
            'If blnShowGrossPayColumn = False Then
            '    dtFinalTable.Columns.Remove("TGP")
            'End If
            'If blnShowTotalDeductionColumn = False Then
            '    dtFinalTable.Columns.Remove("TDD")
            'End If
            If dtFinalTable.Columns.Contains("TGP") Then
                dtFinalTable.Columns.Remove("TGP")
            End If

            If dtFinalTable.Columns.Contains("TDD") Then
                dtFinalTable.Columns.Remove("TDD")
            End If

            If dtFinalTable.Columns.Contains("TotNetPay") Then
                dtFinalTable.Columns.Remove("TotNetPay")
            End If

            If dtFinalTable.Columns.Contains("NetPay") Then
                dtFinalTable.Columns.Remove("NetPay")
            End If

            If dtFinalTable.Columns.Contains("Column" & intNetPayRoundingAdjustmentID.ToString) Then
                dtFinalTable.Columns.Remove("Column" & intNetPayRoundingAdjustmentID.ToString)
            End If

            If dtFinalTable.Columns.Contains("Openingbalance") Then
                dtFinalTable.Columns.Remove("Openingbalance")
            End If
            'Hemant (12 Jul 2019) -- End
            'Hemant (15 June 2019) -- End

            dtFinalTable.AcceptChanges()


            Call FilterTitleAndFilterQuery()


            Dim strGTotal As String = Language.getMessage(mstrModuleName, 8, "Grand Total :")
            Dim strSubTotal As String = ""
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            If mblnIgnorezeroHeads Then
                mdtTableExcel = IgnoreZeroHead(dtFinalTable)
            Else
                mdtTableExcel = dtFinalTable
            End If


            'START TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

            mdtTableExcel.Columns.RemoveAt(0)
            'If mintViewIndex <= 0 Then
            '    mdtTableExcel.Columns.Remove("GrpID")
            '    mdtTableExcel.Columns.Remove("GrpName")
            'Else
            mdtTableExcel.Columns.Remove("GrpID")
            mdtTableExcel.Columns.Remove("Period")
            mdtTableExcel.Columns.Remove("CCCode")
            mdtTableExcel.Columns("GrpName").Caption = "Branch :" ' mstrReport_GroupName
            Dim strGrpCols As String() = {"GrpName"}
            strarrGroupColumns = strGrpCols
            strSubTotal = Language.getMessage(mstrModuleName, 7, "Sub Total :")
            'End If

            'END TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

            If mintEmployeeId > 0 Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName, "s12bw")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                rowsArrayHeader.Add(row)
            End If


            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            row = New WorksheetRow()
            If ConfigParameter._Object._IsShowPreparedBy = True Then
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Prepared By :") & "_____________________________", "s8bw")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By :") & "_____________________________", "s8bw")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Checked By :") & "_____________________________", "s8bw")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
            End If


            rowsArrayFooter.Add(row)


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, _ReportName & "(" & mstrCurrency_Sign & ")", Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName, " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, _ReportName & "(" & mstrCurrency_Sign & ")", Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName, " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, Nothing)
            'Sohail (21 Aug 2015) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
        Finally
            objActivity = Nothing
            objExpense = Nothing
            'Sohail (12 Nov 2014) -- End
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Period :")
            Language.setMessage(mstrModuleName, 3, "Branch :")
            Language.setMessage(mstrModuleName, 4, "Cost Center :")
            Language.setMessage(mstrModuleName, 5, "Currency :")
            Language.setMessage(mstrModuleName, 6, "Exchange Rate:")
            Language.setMessage(mstrModuleName, 7, "Sub Total :")
            Language.setMessage(mstrModuleName, 8, "Grand Total :")
            Language.setMessage(mstrModuleName, 9, "Employee Code")
            Language.setMessage(mstrModuleName, 10, "Employee Name")
            Language.setMessage(mstrModuleName, 11, "Cost Center Code")
            Language.setMessage(mstrModuleName, 12, "Cost Center Name")
            Language.setMessage(mstrModuleName, 13, "GroupID")
            Language.setMessage(mstrModuleName, 14, "GroupName")
            Language.setMessage(mstrModuleName, 15, "Head Count")
            Language.setMessage(mstrModuleName, 16, "Prepared By :")
            Language.setMessage(mstrModuleName, 17, "Approved By :")
            Language.setMessage(mstrModuleName, 18, "Checked By :")
            Language.setMessage(mstrModuleName, 19, "Advance")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
